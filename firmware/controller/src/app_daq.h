

#ifndef __APP_DAQ_H_INCLUDED__
#define __APP_DAQ_H_INCLUDED__


/* describe DAQ registers */
#define DAQ_DESC		\
{				\
 .pci_bar = 1,			\
 .pci_rw_off = 0x200,		\
 .pci_ro_off = 0x220		\
}


/* describe DMA hardware resources */
/* EDMA_FLAG_MIF for DDR */
/* EDMA_FLAG_BRAM for bram */
/* TODO: remove, this should be extracted from hardware */
#define DMA_DESC		\
{				\
 .pci_bar = 1,			\
 .pci_off = 0x400,		\
 .ebs_seg = 1,			\
 .ebs_off = 0x200,		\
 .flags = EDMA_FLAG_MIF		\
}


/* DSOURCE_IT(symbolic_id, hardware_id, width) */
/*
#define DSOURCE_LIST		\
 DSOURCE_IT("IN1", 0, UINT64),	\
 DSOURCE_IT("IN2", 1, UINT64),	\
 DSOURCE_IT("IN3", 2, UINT64),	\
 DSOURCE_IT("IN4", 3, UINT64),	\
 DSOURCE_IT("IN5", 4, UINT64),	\
 DSOURCE_IT("IN6", 5, UINT64),	\
 DSOURCE_IT("CALC1", 6, UINT64),\
 DSOURCE_IT("CALC2", 7, UINT64)
*/


#endif // __APP_DAQ_H_INCLUDED__
