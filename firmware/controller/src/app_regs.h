/* ---------------------------------------------------------------------
 * File: app_regs.h
 *
 * Description: This file defines the various lists of FPGA resources
 *              specific to the application:
 *    - APPREG_LIST : list of FPGA registers
 *    - APPBIT_LIST : list of special function bits
 *    - APPMEM_LIST : list of RAM blocks
 *    - APPFIFO_LIST: list of EBONE FIFOs
 *    - APPDMA_LIST : list of EBONE DMA controllers
 *
 * Usage notes:
 *   1.- The list names above are fixed and cannot be changed
 *   2.- Unused lists can be removed/deleted from this file
 *   3.- If the DANCE application is named <appname>, this file
 *       should be renamed as <appname>_reg.h to be automatically
 *       included by the DANCE default makefiles
 * ---------------------------------------------------------------------
 */


#ifndef __APP_REGS_H_INCLUDED__
#define __APP_REGS_H_INCLUDED__

#define COMMDEVICE_LIST  \
   ETHERCOMM("eth0", COMMDEV_REQUIRED) \
   TCPCOMM(5000, 5000, 0)        \
   HTTPCOMM(80, 0)   \

#define EXTBUS_LIST \
   PCIEXTBUS(PCIE, "10ee:eb01") \
   I2CEXTBUS(I2CQ7, "/dev/i2c-2") \
   I2CEXTBUS(I2CQ7100, "/dev/i2c-0") \


#define BASEADDR_LIST \
   BASEADDR(BAR0,  PCIE, 0,  0,  _RW) \
   BASEADDR(BAR1,  PCIE, 1,  0,  _RW) \
   BASEADDR(BAR2,  PCIE, 2,  0,  _RW) \
   BASEADDR(BAR3,  PCIE, 3,  0,  _RW) \


#define EXTDEVICE_LIST \
   PCIE_FPGA(MAINFPGA, PCIE, BAR1, 0x0000, 0) \
   I2C_COMMLED(I2CQ7,  0x25, PCA9500) \
   I2C_BOARDID(BASE,   I2CQ7, 0x24, PCA9500) \
   BASEI2CDEV(GPIO,    I2CQ7, 0x27, PCA9500, 0) \



/* ---------------------------------------------------------------------
 * APPREG_LIST must contain a list the hardware registers that will be
 *   used by the instrument specific code.
 *
 * The list must include instances of the following macros:
 *   - REG32DEF()  to declare individual 32-bit registers
 *   - BLKDEF()    to declare arbitrary blocks of registers defined
 *                    by subsequent xxxxx_BLKINFO() macros
 *
 * Individual instrument registers are declared as:
 *
 *   REG32DEF(CR1, BAR1, 0x0000, _RW, _BASIC_REG)
 *            |    |     |       |    |
 *            |    |     |       |   Register type for selection purposes:
 *            |    |     |       |     _BASIC_REG
 *            |    |     |       |     _SPECIAL_REG
 *            |    |     |      Access mode: {_RO | _WO | _RW}
 *            |    |    Offset
 *            |   Base address (defined in BASEADDR_LIST)
 *           Register id (used as register name)
 *
 * Each instance of a register block must be declared as:
 *
 *   BLKDEF(I2C1, I2CBLK, BAR1, 0x0200, _RW, _BASIC_REG)
 *           |    |       |      |      |    |
 *           |    |       |      |      |   Register type for selection purposes:
 *           |    |       |      |      |     _BASIC_REG
 *           |    |       |      |      |     _SPECIAL_REG
 *           |    |       |      |     Access mode: {_RO | _WO | _RW}
 *           |    |       |     Offset
 *           |    |      Base address (BAR0 to BAR5 are predefined)
 *           |   Block type id (requires definition of the 'block type')
 *          Instance id (used as prefix in the final register ids)
 *
 * Each different 'block type' used in BLKDEF() macros requires the definition
 *   of a dedicated xxxx_BLKINFO() macro where the string 'xxxx' must be
 *   replaced by the particular block type id. The xxxx_BLKINFO() macro consist
 *   of a list of registers defined by BLKREG32() macros as follows
 *
 *   xxxx_BLKINFO(args...) \
 *       BLKREG32( ... ) \
 *         ...
 *       BLKREG32( ... ) \
 *
 *   And each BLKREG32() instance defines an individual register as follows:
 *
 *      BLKREG32(_DPM, 0x04, _RO, args)
 *                |    |     |    |
 *                |    |     |   Mandatory arg (must match xxxx_BLKINFO())
 *                |    |    Access mode: {_RO | _WO | _RW}
 *                |   Offset
 *               Block Register id (used as suffix in the final register ids)
 *
 * Usage notes:
 *   1.- The final ids of the  registers defined though register blocks are
 *       built by concatenating the id of the block instance and the id of the
 *       block register.
 *   2.- ...
 */
#define APPREG_LIST \
   REG32DEF(VAPPR,        BAR1, 0x0200, _RW, _BASIC_REG) \
   REG32DEF(VADJR,        BAR1, 0x0204, _RW, _BASIC_REG) \
   REG32DEF(LEDCTRL,      BAR1, 0x0208, _RW, _BASIC_REG) \
   REG32DEF(REGRW0,       BAR1, 0x0200, _RW, _BASIC_REG) \
   REG32DEF(REGRW1,       BAR1, 0x0204, _RW, _BASIC_REG) \
   REG32DEF(REGRW2,       BAR1, 0x0208, _RW, _BASIC_REG) \
   REG32DEF(REGRW3,       BAR1, 0x020c, _RW, _BASIC_REG) \
   REG32DEF(REGRW4,       BAR1, 0x0210, _RW, _BASIC_REG) \
   REG32DEF(REGRW5,       BAR1, 0x0214, _RW, _BASIC_REG) \
   REG32DEF(REGRW6,       BAR1, 0x0218, _RW, _BASIC_REG) \
   REG32DEF(REGRW7,       BAR1, 0x021c, _RW, _BASIC_REG) \
   REG32DEF(REGRO0,       BAR1, 0x0220, _RO, _BASIC_REG) \
   REG32DEF(REGRO1,       BAR1, 0x0224, _RO, _BASIC_REG) \
   REG32DEF(REGRO2,       BAR1, 0x0228, _RO, _BASIC_REG) \
   REG32DEF(REGRO3,       BAR1, 0x022c, _RO, _BASIC_REG) \
   REG32DEF(REGRO4,       BAR1, 0x0230, _RO, _BASIC_REG) \
   REG32DEF(REGRO5,       BAR1, 0x0234, _RO, _BASIC_REG) \
   REG32DEF(REGRO6,       BAR1, 0x0238, _RO, _BASIC_REG) \
   REG32DEF(REGRO7,       BAR1, 0x023c, _RO, _BASIC_REG) \


/*
   REGBLOCK(I2C1, I2CREGS, TRIG_BASE, 0x0200,    _BASIC_REG) \

#define I2CREGS_BLKDEF(...) \
    BLKREG32(_DPM,  0x04, _RO, __VA_ARGS__) \
    BLKREG32(_TOTO, 0x08, _RO, __VA_ARGS__) \
    BLKREG32(STA,   0x0c, _RW, __VA_ARGS__) \
*/

/* ----------------------------------------------------------
 * Special register bits
 *
 * BITDEF(MYBIT,  CR1,  4)
 *        |       |     |
 *        |       |    Bit number (0 to 31)
 *        |       |
 *        |      Register
 *        |
 *       Bit id (used as bit name)
 */
#define APPBIT_LIST \
   BITDEF(MYBIT, GSR2,  bit_n)

/* ----------------------------------------------------------
 * Fixed address RAM blocks (BRAM, DDR3, etc.)
 *
 * MEMDEF(RAM00, BAR1, 0xF000, 0x10000, _D32, _RW, _BASIC_REG)
 *        |       |     |       |        |     |       |
 *        |       |     |       |        |     |      Register type:
 *        |       |     |       |        |     |      Still to be defined
 *        |       |     |       |        |     Access mode: {_RO | _WO | _RW}
 *        |       |     |       |       Data unit in bytes: _D8  = 1
 *        |       |     |       |                           _D16 = 2
 *        |       |     |       |                           _D32 = 4
 *        |       |     |       |                           _D64 = 8
 *        |       |     |      Number of data values
 *        |       |    Offset
 *        |      Base address
 *       RAM block id (used as RAM block name)
 */
#define APPMEM_LIST \
   MEMDEF(MYRAM, BAR2, 0x0000, 0x0200, _D64, _RW, _BASIC_REG)

/* ----------------------------------------------------------
 * EBONE FIFOs
 *
 * FIFODEF(MYFIFO, _RO,   32, 0x1000, 0x0010)
 *         |       |      |   |       |
 *         |       |      |   |      Irq vector
 *         |       |      |  Depth in data values
 *         |       |    Width in bits
 *         |      Access mode: {_RO | _WO}
 *        FIFO id (used as FIFO name)
 */
#define FIFO_LIST \
   FIFODEF(MYFIFO, _RW, width, depth, irq_vector)

/* Macros to define FIFO resources:
     FIFOSTATUS(MYFIFO, FSTAT1)
     FIFOCONTROL(MYFIFO, FCTRL1)
     FIFODATA(MYFIFO, FDATA0)
     FIFOCLRBIT(MYFIFO, MYBIT)
     FIFOIRQFLG(MYFIFO, YOURBIT)
*/

/* ----------------------------------------------------------
 * EBONE DMA controllers
 *
 * DMADEF(MYDMA,  1, 0xF000, 32, 0x0010   0)
 *        |       |  |       |   |        |
 *        |       |  |       |   |       Flags
 *        |       |  |       |   Irq vector
 *        |       |  |       Bus width in bits
 *        |       | Offset
 *        |      Base address
 *       DMA id (used as DMA name)
 */
#define DMA_LIST \
   DMADEF(MYDMA, base_addr, offset, 32, irq_vector, flags)


#endif // __APP_REGS_H_INCLUDED__

