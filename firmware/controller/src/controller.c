
#include <dancemain.h>

#include "dintef.h"


DANCE_APPNAME("DINTEF0");
DANCE_VERSION(0, 1);



/************************************/
/* instrument configuration routine */
/************************************/

void partial_exec_qDCONFIG(void) {
}



/***************************/
/* instrument main routine */
/***************************/

int init_application(void) {
   REG_WRITE(VAPPR, 0x8000005c);
   REG_WRITE(VADJR, 0x80000068);
   
   // REG_VALUE(VADJR);

   if (setup_dma())
      LOG_ERROR("setup DMA failed\n");

   return 0;
}



