

#ifndef __APP_CONF_H_INCLUDED__
#define __APP_CONF_H_INCLUDED__

#include <stdint.h>

/*
   Global Configuration Setup

   APPCONF_MODE: an operating mode (default to CONF_MODE_ALL_COMMS)
   APPCONF_TIMER: a timer in milliseconds. default to 0 (immediate). -1 to disable
   APPCONF_MEDIA: a value to select storage media (default to SYS_FLASH)
*/
#define APPCONF_MODE CONF_MODE_ALL_COMMS
#define APPCONF_TIMER 0
// #define APPCONF_MEDIA CONF_MEDIA_FILE


/*
  APPCONF_LIST: a list of configuration variables defined as

     CONF_SCALAR(name, type, default_value):      // defines a single scalar variable
    or
     CONF_ARRAY(name, count, type, default_value) // defines an array of variables

  The application, can access the variable using appconf_<name>
  where <name> the name defined here
*/


typedef struct {
#define CHFLAG_ENABLE (1 << 0)
   uint32_t flags;    // CHFLAG_ENABLE is called state in documentation
   uint32_t src;      // index of the source channel, or (uint32_t)-1
   uint32_t dir;      // direction: 0 for input, 1 for output
   uint32_t type;     // channel mode in documentation, in CHTYPE_xxx */  
 
   union {            // type specific
      struct {
            uint32_t mult;
         } quad;
      struct {
         uint32_t edge;
         uint32_t updown;
      } pulse;
      struct {
         uint32_t nbit;
         uint32_t freq;
         uint32_t delay;
         uint32_t role;
         uint32_t coding;
         uint32_t framing;
      } ssi;
      struct {
         uint32_t nbit;
         uint32_t freq;
      } biss;
      struct {
         uint32_t nbit;
         uint32_t mode;
         uint32_t freq;
      } endat;
      struct {
         uint32_t nbit;
         uint32_t tgap;
      } hssl;
   } u;      
} pepu_chconf_t;


/* default to 0 for OFF type */
static const pepu_chconf_t pepu_default_chconf = {
   .flags = 0,
   .src = (uint32_t)-1,
   .dir = 0,
   .type = 0
};

static const uint32_t pepu_conf_version = 0xdead0001;


#define APPCONF_LIST \
   CONF_SCALAR(version,            uint32_t,      pepu_conf_version)   \
   CONF_ARRAY(chconf, 8,           pepu_chconf_t, pepu_default_chconf) \
   CONF_ARRAY(lineqs, (8 + 1) * 8, double,        0.0)                 \


#endif // __APP_CONF_H_INCLUDED__ 
