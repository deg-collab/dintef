
#include <dance.h>

#include "dintef.h"
// dma related stuff
#include "libedma.h"
#include "libebuf.h"
#include "libuirq.h"

static int           dma_is_valid = 0;
static edma_handle_t dma_edma; // dma handle

int setup_dma(void) {
   struct edma_desc desc;
   uirq_handle_t    uirq;
   const char      *vid_did   = "10ee:eb01";


   /* initialize ebuf and edma libraries */
   if (ebuf_init_lib(EBUF_FLAG_PHYSICAL)) goto on_error_0;
   if (uirq_init_lib()) goto on_error_1;
   if (edma_init_lib()) goto on_error_2;

   /* get access to kernel driver handling IRQs */
   if (uirq_init(&uirq, vid_did)) {
      LOG_ERROR("Initialisation of EBONE irq failed.\n");
      goto on_error_3;
   }

   if(uirq_open(&uirq)) {
      LOG_ERROR("Access to EBONE irq failed.\n");
      goto on_error_3;
   }

   /* open edma handle */
   edma_init_desc(&desc);
   desc.pci_id  = vid_did;
   desc.pci_bar = 1;
   desc.pci_off = 0x600;
   desc.ebs_seg = 1;
   desc.ebs_off = 0x200;
   /* TODO: issue when using IRQs, the kernel gets them but libedma not */
   /*
   desc.flags = EDMA_FLAG_FIFO | EDMA_FLAG_IRQ;
   */
   desc.flags = EDMA_FLAG_FIFO;
   desc.uirq = &uirq;
   if (edma_open(&dma_edma, &desc)) {
      LOG_ERROR("Initialisation of EDMA failed.\n");
      goto on_error_3;
   }
   dma_is_valid = 1;

   return 0;

 on_error_3:
   edma_fini_lib();
 on_error_2:
   uirq_fini_lib();
 on_error_1:
   ebuf_fini_lib();
 on_error_0:
   return -1;
}


// ?*DMA "pI"
//
void exec_qbDMA(void) {
   size_t size = I_INTEGER();   // the byte count

   static ebuf_handle_t ebuf = EBUF_INITIALISE;
   mblock_t*            mblk;
   const size_t         size_max = 1024 * 1024 * 250; // empirical value tested on a Q7 with 384MB of RAM (500MB??)

   LOG_APP("size: %zd bytes\n", size);

   /* use the ebuf contain to check if transfer is in progress */
   if (ebuf_get_size(&ebuf) != 0) {
      errorf("previous transfer still in progress");
      return;
   }

   /* minimum check on DMA initialization */
   if (dma_is_valid == 0) {
      errorf("DMA is not properly initialized");
      return;
   }

   if(size > size_max) {
      errorf("Size requested too large, max: %zd kB", size_max / 1024);
      return;
   }


   /* mblock must be created dynamically because it'll be freed by runtime */
   /* mblock_create() now calls internally ebuf_alloc() */
   mblk = mblock_create(NULL, &ebuf, size, MEM_TYPE_EBUF_PHYSMEM);
   if (mblk == NULL) {
      errorf("Internal memory allocation failed");
      goto on_error;
   }
   /*
    * TODO: requesting a too large value, crashes the libpmem.ko without
    * freeing memory -> controller restart mandatory
    */

   /* blocking call until end of DMA transfer */
   if (edma_read(&dma_edma, 0, &ebuf, 0, size)) {
      errorf("DMA read failed");
      goto on_error;
   }

   send_binary_buffer(mblk, sizeof(uint8_t), size,  \
                      BIN_FLG_NOCHKSUM | BIN_FLG_LITTLENDIAN);

   answerf("Command was successful");
   return;

 on_error:
   if (ebuf_get_size(&ebuf) != 0)
      ebuf_free(&ebuf);
   return;
}
