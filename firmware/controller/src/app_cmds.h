/*---------------------------------------------------------------------------
 *
 * DANCE application specific commands
 *
 */

#ifndef __APP_CMDS_H_INCLUDED__
#define __APP_CMDS_H_INCLUDED__

#define fmtqbDATA "T#CLEAR | " fmtINT
#define fmtqbDMA  "pI"

#define APPMENU_LIST  \
   MENU_IT(qbDMA,  fmtqbDMA,  fmtNONE, _B, "General DMA read command") 

/* list of units */

#define APPUNITS \
   DANCEUNITS(BITS) \

#define BITS_UNITS \
   UNITDEF(bits,  1.0) \

/* list of boolean definitions */

#define APPBOOLEAN \
   BOOLDEF(YES,  NO)   \
   BOOLDEF(On,   Off)  \
   BOOLDEF(RUN,  STOP) \


/* list of choice lists */

/*
#define APPLISTS        \
   DANCELIST(CHIN)      \
   DANCELIST(CHOUT)     \
*/


#endif // __APP_CMDS_H_INCLUDED__
