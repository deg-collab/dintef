#---------------------------------------------------------------------
#
DANCE_SDK_PLATFORM ?= kontron_type10

#---------------------------------------------------------------------
#
DANCE_SDK_ARCH           ?= x86_32
DANCE_SDK_KERNEL_RELEASE ?= linux-3.6.11
DANCE_SDK_KERNEL_DIR     ?= $(DANCE_SDK_ROOT)/kernel/$(DANCE_SDK_PLATFORM)/$(DANCE_SDK_KERNEL_RELEASE)
DANCE_SDK_CROSS_COMPILE  ?= $(DANCE_SDK_ROOT)/toolchain/i686-nptl-linux-gnu/bin/i686-nptl-linux-gnu-
DANCE_SDK_DEPS_DIR       ?= $(DANCE_SDK_ROOT)/deps/i686-nptl-linux-gnu

DANCE_SDK_CFLAGS  +=
DANCE_SDK_LDFLAGS +=