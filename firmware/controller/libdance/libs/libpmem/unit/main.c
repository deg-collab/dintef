#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <sys/types.h>

#include "libpmem.h"

#define PMEM_PERROR() \
do { printf("[!] %s,%d\n", __FILE__, __LINE__); } while (0)

int main(int ac, char** av)
{
  const size_t size = 128 * 1024 * 1024;
  uintptr_t* pages;
  size_t npage;
  size_t i;
  uint8_t* buf[2];

  if (pmem_init_lib())
  {
    PMEM_PERROR();
    goto on_error_0;
  }

  npage = pmem_size_to_pages(size);
  pages = malloc(npage * sizeof(uintptr_t));

  if (pmem_alloc_pages(pages, npage))
  {
    PMEM_PERROR();
    goto on_error_1;
  }

  printf("-- page allocation success\n");
  for (i = 0; i < npage; ++i) printf("[%02zu] 0x%016" PRIxPTR "\n", i, pages[i]);
  printf("\n");

  if (pmem_map_pages((void**)&buf[0], pages, npage))
  {
    PMEM_PERROR();
    goto on_error_2;
  }

  printf("-- page mapping [0] success: %p\n", buf[0]);

  if (pmem_map_pages((void**)&buf[1], pages, npage))
  {
    PMEM_PERROR();
    goto on_error_3;
  }

  printf("-- page mapping [1] success: %p\n", buf[1]);

  for (i = 0; i < size; ++i)
  {
    buf[0][i] = (uint8_t)i;
  }

  for (i = 0; i < size; ++i)
  {
    if (buf[1][i] != (uint8_t)i)
    {
      printf("invalid at %zu (0x%02x)\n", i, buf[1][i]);
      break ;
    }
  }

  printf("-- verification success\n");

  pmem_unmap_pages(buf[1], npage);
 on_error_3:
  pmem_unmap_pages(buf[0], npage);
 on_error_2:
  pmem_free_pages(pages, npage);
 on_error_1:
  free(pages);
  pmem_fini_lib();
 on_error_0:
  return 0;
}
