#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/utsname.h>
#include "libpmem.h"
#include "k/pmem_ioctl.h"


#define PMEM_CONFIG_DEBUG 1

#if PMEM_CONFIG_DEBUG
#include <stdio.h>
#define PMEM_PRINTF(__s, ...)  \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define PMEM_PERROR() \
do { printf("[!] %s,%d\n", __FILE__, __LINE__); } while (0)
#define PMEM_ASSERT(__x) \
do { if (!(__x)) printf("[!] %d\n", __LINE__); } while(0)
#else
#define PMEM_PRINTF(__s, ...)
#define PMEM_PERROR()
#define PMEM_ASSERT(__x)
#endif


size_t pmem_page_size;
static int pmem_dev_fd = -1;

static char pmem_dir_path[32];
static char pmem_dev_path[32];

static int exec_common(const char** av)
{
  int status;
  pid_t pid;

  pid = fork();
  if (pid == -1) return -1;

  if (pid == 0)
  {
    /* child */

    const int fd = open("/dev/null", O_WRONLY);

    if (fd != -1)
    {
      dup2(fd, STDOUT_FILENO);
      dup2(fd, STDERR_FILENO);

      close(fd);
    }

    execve(av[0], (char**)av, NULL);

    /* not reached or error */
    exit(-1);
  }

  if (waitpid(pid, &status, 0) == -1)
  {
    kill(pid, SIGKILL);
    return -1;
  }

  if (WIFEXITED(status) == 0) return -1;
  return (int)WEXITSTATUS(status);
}

static int exec_insmod(const char* kpath)
{
  const char* av[] = { "/sbin/insmod", kpath, NULL };
  return exec_common(av);
}

static int exec_rmmod(const char* kname)
{
  const char* av[] = { "/sbin/rmmod", kname, NULL };
  return exec_common(av);
}

static int load_kmod(const char* kname, const char* kmod_dir)
{
  /* load kernel module */

  struct utsname un;
  struct stat st;
  char kpath[128];

  if (kmod_dir == NULL)
  {
    if (uname(&un)) goto on_error_0;
    snprintf
    (
     kpath, sizeof(kpath),
     "/lib/modules/%s/dance/%s.ko", un.release, kname
    );
  }
  else
  {
    snprintf
    (
     kpath, sizeof(kpath),
     "%s/%s.ko", kmod_dir, kname
    );
  }
  kpath[sizeof(kpath) - 1] = 0;

  if (stat(kpath, &st)) goto on_error_0;

  return exec_insmod(kpath);

 on_error_0:
  return -1;
}

static int unload_kmod(const char* kname)
{
  return exec_rmmod(kname);
}

int pmem_init_lib_with_dir(const char* dir_path, const char* kmod_dir)
{
/* TODO: weird, not the good values!! */
#if 0
  static const int dev_major = PMEM_DEV_MAJOR;
  static const int dev_minor = PMEM_DEV_MINOR;
#else
  static const int dev_major = 3760;
  static const int dev_minor = 0;
#endif

  int fd;
  int err;
  pmem_ioctl_mem_info_t info;
  dev_t dev;

  /* ok to fail */

  load_kmod("pmem", kmod_dir);

  /* create dev node if does not exist */

  err = strlen(dir_path);
  if ((size_t)err >= sizeof(pmem_dir_path))
  {
    PMEM_PERROR();
    goto on_error_0;
  }

#define PMEM_SUFFIX "/pmem"
  if (((size_t)err + sizeof(PMEM_SUFFIX) - 1) >= sizeof(pmem_dev_path))
  {
    PMEM_PERROR();
    goto on_error_0;
  }

  strcpy(pmem_dir_path, dir_path);
  memcpy(pmem_dev_path, dir_path, (size_t)err);
  strcpy(pmem_dev_path + (size_t)err, PMEM_SUFFIX);

  errno = 0;
  if (mkdir(dir_path, S_IRUSR | S_IWUSR | S_IXUSR))
  {
    if (errno != EEXIST)
    {
      PMEM_PERROR();
      goto on_error_0;
    }
  }

  errno = 0;
  dev = makedev(dev_major, dev_minor);
  if (mknod(pmem_dev_path, S_IRUSR | S_IWUSR | S_IFCHR, dev))
  {
    if (errno != EEXIST)
    {
      PMEM_PERROR();
      goto on_error_1;
    }
  }

  fd = open(pmem_dev_path, O_RDWR);
  if (fd == -1)
  {
    PMEM_PERROR();
    goto on_error_2;
  }

  if (ioctl(fd, PMEM_IOCTL_MEM_GET_INFO, &info))
  {
    PMEM_PERROR();
    goto on_error_3;
  }

  pmem_dev_fd = fd;
  pmem_page_size = info.page_size;

  /* success */
  return 0;

 on_error_3:
  close(fd);
 on_error_2:
  unlink(pmem_dev_path);
 on_error_1:
  rmdir(pmem_dir_path);
 on_error_0:
  return -1;
}

int pmem_init_lib(void)
{
  return pmem_init_lib_with_dir("/dev/dance", NULL);
}

int pmem_fini_lib(void)
{
  /* assume pmem_init_lib(); */

  close(pmem_dev_fd);
  pmem_dev_fd = -1;

  unlink(pmem_dev_path);

  /* do not rmdir */
  /* rmdir(pmem_dir_path); */

  unload_kmod("pmem");

  return 0;
}


/* TOREMOVE */
int pmem_get_fd(void)
{
  return pmem_dev_fd;
}
/* TOREMOVE */


int pmem_alloc_pages(uintptr_t* pages, size_t page_count)
{
  pmem_ioctl_mem_alloc_pages_t arg;

  arg.nid = -1;
  arg.page_count = page_count;
  arg.pages = pages;

  if (ioctl(pmem_dev_fd, PMEM_IOCTL_MEM_ALLOC_PAGES, &arg))
  {
    PMEM_PERROR();
    return -1;
  }

  return 0;
}


int pmem_free_pages(const uintptr_t* pages, size_t page_count)
{
  pmem_ioctl_mem_free_pages_t arg;

  arg.page_count = page_count;
  arg.pages = pages;

  if (ioctl(pmem_dev_fd, PMEM_IOCTL_MEM_FREE_PAGES, &arg))
  {
    PMEM_PERROR();
    return -1;
  }

  return 0;
}


int pmem_sync_pages(const uintptr_t* pages, size_t page_count)
{
  pmem_ioctl_mem_sync_pages_t arg;

  arg.page_count = page_count;
  arg.pages = pages;

  if (ioctl(pmem_dev_fd, PMEM_IOCTL_MEM_SYNC_PAGES, &arg))
  {
    PMEM_PERROR();
    return -1;
  }

  return 0;
}


int pmem_map_pages
(void** vaddr, const uintptr_t* pages, size_t page_count)
{
  const size_t size = pmem_pages_to_size(page_count);
  pmem_ioctl_mem_map_pages_t arg;

  arg.vaddr = (uintptr_t)mmap
  (
   NULL, size,
   PROT_READ | PROT_WRITE,
   MAP_SHARED | MAP_NORESERVE | MAP_ANONYMOUS,
   -1,
   0
  );

  if (arg.vaddr == (uintptr_t)MAP_FAILED)
  {
    PMEM_PERROR();
    return -1;
  }

  arg.page_count = page_count;
  arg.pages = pages;

  if (ioctl(pmem_dev_fd, PMEM_IOCTL_MEM_MAP_PAGES, &arg))
  {
    PMEM_PERROR();
    munmap((void*)arg.vaddr, size);
    return -1;
  }

  *vaddr = (void*)arg.vaddr;

  return 0;
}


int pmem_unmap_pages(void* vaddr, size_t page_count)
{
  const size_t size = pmem_pages_to_size(page_count);
  pmem_ioctl_mem_unmap_pages_t arg;

  arg.vaddr = (uintptr_t)vaddr;
  arg.page_count = page_count;

  if (ioctl(pmem_dev_fd, PMEM_IOCTL_MEM_UNMAP_PAGES, &arg))
  {
    PMEM_PERROR();
    return -1;
  }

  munmap(vaddr, size);

  return 0;
}


int pmem_set_limit32(void)
{
  const int fd = pmem_dev_fd;
  pmem_ioctl_mem_info_t info;

  if (ioctl(fd, PMEM_IOCTL_MEM_GET_INFO, &info))
  {
    PMEM_PERROR();
    return -1;
  }

  info.limit32 = 1;

  if (ioctl(fd, PMEM_IOCTL_MEM_SET_INFO, &info))
  {
    PMEM_PERROR();
    return -1;
  }

  return 0;
}
