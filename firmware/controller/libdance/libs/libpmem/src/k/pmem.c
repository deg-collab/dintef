#include <linux/init.h>
#include <linux/pci.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/poll.h>
#include <linux/mm.h>
#include <linux/mmzone.h>
#include <linux/rmap.h>
#include <linux/pagemap.h>
#include <asm/mman.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/pci.h>
#include <linux/ioport.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#if defined(CONFIG_FREESCALE_IMX6)
#include <asm/tlbflush.h>
#include <asm/cacheflush.h>
#include <asm/outercache.h>
#endif

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19))
typedef unsigned long uintptr_t;
#endif
#include "pmem_ioctl.h"


/* contiguous physical memory allocators (pmem_xxx api) */
/* http://lwn.net/Articles/396702 */
/* http://lwn.net/Articles/447405 */


/* linux module interface */

MODULE_DESCRIPTION("PMEM driver");
MODULE_AUTHOR("PMEM team");
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
MODULE_LICENSE("Dual BSD/GPL");
#endif

#define PMEM_NAME KBUILD_MODNAME
#define PMEM_TAG "[ " PMEM_NAME " ] "
#define PMEM_ENTER() printk(PMEM_TAG "%s\n", __FUNCTION__)
#define PMEM_ASSERT(__x)


/* per {driver,dev,file} types */

typedef struct pmem_node
{
  struct pmem_node* next;
  uintptr_t paddr;
  size_t size;
} pmem_node_t;

typedef struct pmem_dev
{
  /* char device is contained for o(1) lookup */
  struct cdev cdev;
} pmem_dev_t;

typedef struct pmem_file
{
  pmem_dev_t* dev;
  pmem_node_t* pmem_head;
} pmem_file_t;

typedef struct pmem_driver
{
  /* device storage */
  pmem_dev_t dev;

  /* device number */
  int major_minor;

} pmem_driver_t;


/* PMEM per driver data (global, singleton) */

static pmem_driver_t pmem_driver;

static inline void pmem_driver_init(pmem_driver_t* d)
{
}

static pmem_dev_t* pmem_dev_alloc(void)
{
  return &pmem_driver.dev;
}

static inline void pmem_dev_free(pmem_dev_t* dev)
{
}


/* allocation flags */

static gfp_t pmem_flags = GFP_ATOMIC;


/* allocator type */

#define CONFIG_PMEM_DYNAMIC 1
#define CONFIG_PMEM_BOOT 0


#if CONFIG_PMEM_BOOT

/* boot time reserved memory (ie. memmap=128M$256M command line option) */
static const size_t pmem_reserved_size = 128 * 1024 * 1024;
static const uintptr_t pmem_reserved_paddr = 256 * 1024 * 1024;

static int pmem_alloc
(pmem_node_t** head, uintptr_t* paddr, size_t size, int nid)
{
  /* TODO: bitmap allocator */
  if (size > pmem_reserved_size) return -ENOMEM;
  *paddr = pmem_reserved_paddr;
  return 0;
}

static void pmem_free(pmem_node_t** head, uintptr_t paddr, size_t size)
{
  /* TODO: bitmap allocator */
}

static void pmem_free_nodes(void)
{
}

#elif CONFIG_PMEM_DYNAMIC

/* LINUX on demand physical memory allocator */

static int del_pmem_node(pmem_node_t** head, uintptr_t paddr)
{
  /* delete a node given paddr */

  struct pmem_node* pos = *head;
  struct pmem_node* prev = NULL;

  while (pos)
  {
    if (pos->paddr == paddr)
    {
      if (prev) prev->next = pos->next;
      else *head = pos->next;
      kfree(pos);
      return 0;
    }

    prev = pos;
    pos = pos->next;
  }

  /* not found */
  return -1;
}

static int add_pmem_node(pmem_node_t** head, uintptr_t paddr, size_t size)
{
  /* add a node for paddr, size */

  struct pmem_node* const pos = kmalloc(sizeof(struct pmem_node), GFP_KERNEL);
  if (pos == NULL) return -1;

  pos->paddr = paddr;
  pos->size = size;
  pos->next = *head;
  *head = pos;

  return 0;
}

static int pmem_alloc
(pmem_node_t** head, uintptr_t* paddr, size_t size, int nid)
{
  const unsigned long order = get_order(size);
  unsigned long vaddr;

  if (nid < 0) /* node does not matter */
  {
    vaddr = __get_free_pages(pmem_flags, order);
    if (vaddr == 0) return -ENOMEM;
  }
  else /* allocate on node nid */
  {
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19))
    return -EINVAL;
#else
    struct page* const page = alloc_pages_node(nid, pmem_flags, order);
    if (page == NULL) return -ENOMEM;
    vaddr = (unsigned long)page_address(page);
#endif
  }

  *paddr = (uintptr_t)virt_to_phys((void*)(uintptr_t)vaddr);
  PMEM_ASSERT(*paddr);

  if (add_pmem_node(head, *paddr, size))
  {
    free_pages(vaddr, order);
    *paddr = 0;
    return -ENOMEM;
  }

  return 0;
}

static void pmem_free(pmem_node_t** head, uintptr_t paddr, size_t size)
{
  const unsigned long order = get_order(size);
  const unsigned long vaddr = (unsigned long)(uintptr_t)phys_to_virt(paddr);

  /* equivalent to __free_pages(virt_to_page(paddr, order) */
  free_pages(vaddr, order);

  del_pmem_node(head, paddr);
}

static void pmem_free_nodes(pmem_node_t* head)
{
  /* free all the nodes in pmem list */
  while (head != NULL) pmem_free(&head, head->paddr, head->size);
}

#else /* invalid allocator */
#error "no physical memory allocation strategy defined"
#endif


/* ioctl handler */

static void unlock_user_pages_common
(struct page** pages, size_t npages, unsigned int is_rw)
{
  size_t i;

  for (i = 0; i < npages; ++i)
  {
    struct page* const page = pages[i];
    if (is_rw && !PageReserved(page)) SetPageDirty(page);
    put_page(page);
  }

  kfree(pages);
}

static inline void unlock_user_pages_ro(struct page** pages, size_t npages)
{
  unlock_user_pages_common(pages, npages, 0);
}

static inline void unlock_user_pages_rw(struct page** pages, size_t npages)
{
  unlock_user_pages_common(pages, npages, 1);
}

static int lock_user_pages_common
(
 uintptr_t addr, size_t size,
 unsigned int is_rw,
 struct page*** pages,
 size_t* npages
)
{
  /* http://lwn.net/Articles/28548 */
  /* ldd3_15_performing_direct_io */
  /* drivers/scsi/st.c */

  const size_t off = addr & (PAGE_SIZE - 1);
  int err;

  size += off;
  addr -= off;

  *npages = (size / PAGE_SIZE) + (off ? 1 : 0);

  *pages = kmalloc((*npages) * sizeof(struct page*), GFP_KERNEL);
  if (*pages == NULL) return -ENOMEM;

  down_read(&current->mm->mmap_sem);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,6,0)
  err = get_user_pages(addr, *npages, is_rw? FOLL_WRITE : 0, *pages, NULL);
#else
  err = get_user_pages
    (current, current->mm, addr, *npages, is_rw, 0, *pages, NULL);
#endif

  up_read(&current->mm->mmap_sem);

  if (err != (int)*npages)
  {
    unlock_user_pages_common(*pages, *npages, is_rw);
    return -1;
  }

  return 0;
}

static inline int lock_user_pages_ro
(uintptr_t addr, size_t size, struct page*** pages, size_t* npages)
{
  return lock_user_pages_common(addr, size, 0, pages, npages);
}

static inline int lock_user_pages_rw
(uintptr_t addr, size_t size, struct page*** pages, size_t* npages)
{
  return lock_user_pages_common(addr, size, 1, pages, npages);
}

#ifdef HAVE_UNLOCKED_IOCTL
long pmem_file_ioctl
(struct file* file, unsigned int cmd, unsigned long uaddr)
{
  __attribute__((unused)) struct inode* const inode = file->f_path.dentry->d_inode;
#else
static int pmem_ioctl
(struct inode* inode, struct file* file, unsigned int cmd, unsigned long uaddr)
{
#endif

  int err;

  switch (cmd)
  {
  case PMEM_IOCTL_MEM_GET_INFO:
    {
      pmem_ioctl_mem_info_t info;

#define PMEM_PAGE_SIZE (4 * 1024 * 1024)

      info.page_size = PMEM_PAGE_SIZE;

      if (pmem_flags & GFP_DMA32) info.limit32 = 1;
      else info.limit32 = 0;

      if (copy_to_user((void*)(uintptr_t)uaddr, &info, sizeof(info)))
      {
        err = -EINVAL;
        break ;
      }

      err = 0;

      break ;
    }

  case PMEM_IOCTL_MEM_SET_INFO:
    {
      pmem_ioctl_mem_info_t info;

      if (copy_from_user(&info, (void*)(uintptr_t)uaddr, sizeof(info)))
      {
        err = -EFAULT;
        break ;
      }

      // info.page_size = PMEM_PAGE_SIZE;

      if (info.limit32) pmem_flags |= GFP_DMA32;
      else pmem_flags &= ~GFP_DMA32;

      err = 0;

      break ;
    }

  case PMEM_IOCTL_MEM_ALLOC_PAGES:
    {
      pmem_file_t* const pmem_file = file->private_data;

      pmem_ioctl_mem_alloc_pages_t arg;
      struct page** user_pages;
      size_t user_npages;
      size_t user_size;
      uintptr_t user_addr;
      size_t i;
      size_t j;

      if (copy_from_user(&arg, (void*)(uintptr_t)uaddr, sizeof(arg)))
      {
        err = -EFAULT;
        break ;
      }

      /* TODO: check args */

      user_size = arg.page_count * sizeof(uintptr_t);
      user_addr = (uintptr_t)arg.pages;
      if (lock_user_pages_rw(user_addr, user_size, &user_pages, &user_npages))
      {
        err = -EFAULT;
        break ;
      }

      err = 0;

      for (i = 0; i < arg.page_count; ++i)
      {
        err = pmem_alloc
            (&pmem_file->pmem_head, &arg.pages[i], PMEM_PAGE_SIZE, arg.nid);
        if (err)
        {
          for (j = 0; j < i; ++j)
            pmem_free(&pmem_file->pmem_head, arg.pages[i], PMEM_PAGE_SIZE);
          break ;
        }
      }

      unlock_user_pages_rw(user_pages, user_npages);

      break ;
    }

  case PMEM_IOCTL_MEM_FREE_PAGES:
    {
      pmem_file_t* const pmem_file = file->private_data;

      pmem_ioctl_mem_free_pages_t arg;
      struct page** user_pages;
      size_t user_npages;
      size_t user_size;
      uintptr_t user_addr;
      size_t i;

      if (copy_from_user(&arg, (void*)(uintptr_t)uaddr, sizeof(arg)))
      {
        err = -EFAULT;
        break ;
      }

      user_size = arg.page_count * sizeof(uintptr_t);
      user_addr = (uintptr_t)arg.pages;
      if (lock_user_pages_ro(user_addr, user_size, &user_pages, &user_npages))
      {
        err = -EFAULT;
        break ;
      }

      for (i = 0; i < arg.page_count; ++i)
        pmem_free(&pmem_file->pmem_head, arg.pages[i], PMEM_PAGE_SIZE);

      unlock_user_pages_ro(user_pages, user_npages);

      err = 0;

      break ;
    }

  case PMEM_IOCTL_MEM_SYNC_PAGES:
#if 0 /* TODO */
    {
      pmem_ioctl_mem_sync_pages_t arg;
      struct page** user_pages;
      size_t user_npages;
      size_t user_size;
      uintptr_t user_addr;
      size_t i;

      if (copy_from_user(&arg, (void*)(uintptr_t)uaddr, sizeof(arg)))
      {
        err = -EFAULT;
        break ;
      }

      user_size = arg.page_count * sizeof(uintptr_t);
      user_addr = (uintptr_t)arg.pages;
      if (lock_user_pages_ro(user_addr, user_size, &user_pages, &user_npages))
      {
        err = -EFAULT;
        break ;
      }

      flush_write_buffers();

      for (i = 0; i < arg.page_count; ++i)
      {
        pci_dma_sync_single_for_device
            (dev->pci_dev, arg.pages[i], PMEM_PAGE_SIZE, PCI_DMA_BIDIRECTIONAL);
      }

      unlock_user_pages_ro(user_pages, user_npages);

      err = 0;

      break ;
    }
#else
    {
#if defined(CONFIG_FREESCALE_IMX6)
      __cpuc_flush_kern_all();
      __cpuc_flush_user_all();
      outer_flush_all();
      local_flush_tlb_all();
#endif
      err = 0;
      break ;
    }
#endif /* TODO */

  case PMEM_IOCTL_MEM_MAP_PAGES:
    {
      pmem_ioctl_mem_map_pages_t arg;
      struct page** user_pages;
      size_t user_npages;
      size_t user_size;
      uintptr_t user_addr;
      struct vm_area_struct* vma;
      void* uvaddr;
      size_t i;

      if (copy_from_user(&arg, (void*)(uintptr_t)uaddr, sizeof(arg)))
      {
        err = -EFAULT;
        break ;
      }

      user_size = arg.page_count * sizeof(uintptr_t);
      user_addr = (uintptr_t)arg.pages;
      if (lock_user_pages_ro(user_addr, user_size, &user_pages, &user_npages))
      {
        err = -EFAULT;
        break ;
      }

      uvaddr = (void*)arg.vaddr;

      /* remap on physical pages */

      /* TODO: down_write(&current->mm->mmap_sem); */

      /* http://www.makelinux.com/books/lkd2/ch14lev1sec3 */
      vma = find_vma(current->mm, (unsigned long)(uintptr_t)uvaddr);
      PMEM_ASSERT(vma);

#ifndef VM_RESERVED
# define VM_RESERVED (VM_DONTEXPAND | VM_DONTDUMP)
#endif
      /* linux-2.6.29/drivers/char/s3c_mem.c */
      vma->vm_flags |= VM_RESERVED;
      vma->vm_flags |= VM_IO;
      vma->vm_flags |= VM_PFNMAP;

#if defined(CONFIG_FREESCALE_IMX6)
      vma->vm_page_prot = pgprot_dmacoherent(vma->vm_page_prot);
#endif

      err = 0;
      for (i = 0; i < arg.page_count; ++i)
      {
        const size_t vma_off = i * PMEM_PAGE_SIZE;

        err = remap_pfn_range
        (
        vma, vma->vm_start + vma_off,
        /* non ioremap: page_to_pfn(virt_to_page(kvaddr)) */
        /* all, but phys_to_page unavail: phys_to_page(paddr) */
        arg.pages[i] >> PAGE_SHIFT,
        PMEM_PAGE_SIZE,
        vma->vm_page_prot
        );

        if (err) break ;
      }

      /* TODO: up_write(&current->mm->mmap_sem); */

      unlock_user_pages_ro(user_pages, user_npages);

      if (err)
      {
        err = -EFAULT;
        break ;
      }

      arg.vaddr = (uintptr_t)uvaddr;
      if (copy_to_user((void*)(uintptr_t)uaddr, &arg, sizeof(arg)))
      {
        err = -EFAULT;
        break ;
      }

      break ;
    }

  case PMEM_IOCTL_MEM_UNMAP_PAGES:
    {
      /* munmap operation done by user */
      err = 0;
      break ;
    }

  default:
    {
      err = -ENOSYS;
      break ;
    }
  }

  return err;
}

/* LINUX file interface */

static int pmem_file_open(struct inode* inode, struct file* file)
{
  pmem_dev_t* const dev = container_of(inode->i_cdev, pmem_dev_t, cdev);
  pmem_file_t* file_data;

  file_data = kmalloc(sizeof(pmem_file_t), GFP_KERNEL);
  if (file_data == NULL)
  {
    return -ENOMEM;
  }

  file_data->dev = dev;
  file_data->pmem_head = NULL;
  file->private_data = file_data;

  return 0;
}

static int pmem_file_close(struct inode* inode, struct file* file)
{
  pmem_file_t* const pmem_file = file->private_data;

  pmem_free_nodes(pmem_file->pmem_head);
  kfree(file->private_data);
  file->private_data = NULL;

  return 0;
}

static struct file_operations pmem_fops =
{
 .owner = THIS_MODULE,
#ifdef HAVE_UNLOCKED_IOCTL
 .unlocked_ioctl = pmem_file_ioctl,
#else
 .ioctl = pmem_ioctl,
#endif
 .open = pmem_file_open,
 .release = pmem_file_close
};

static int pmem_cdev_init(pmem_dev_t* dev)
{
  /* register the chardev and associated file operations */

  const dev_t devno = pmem_driver.major_minor;

  cdev_init(&dev->cdev, &pmem_fops);
  dev->cdev.owner = THIS_MODULE;

  /* nothing to do on failure */
  return cdev_add(&dev->cdev, devno, 1);
}

static inline void pmem_cdev_fini(pmem_dev_t* dev)
{
  cdev_del(&dev->cdev);
}


/* LINUX module interface */

static int  __init pmem_init(void);
static void __exit pmem_exit(void);


#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,5,0)
module_init(pmem_init);
module_exit(pmem_exit);
#endif


static int __init pmem_init(void)
{
  pmem_dev_t* pmem_dev;
  dev_t first_dev;
  int err = -1;

  pmem_driver_init(&pmem_driver);

#if 0
  /* dynamic allocation */
  err = alloc_chrdev_region(&first_dev, 0, 1, PMEM_NAME);
#else
  /* static allocation */
  first_dev = MKDEV(PMEM_DEV_MAJOR, PMEM_DEV_MINOR);
  err = register_chrdev_region(first_dev, 1, PMEM_NAME);
#endif

  if (err < 0)
  {
    goto on_error_0;
  }

  pmem_driver.major_minor = first_dev;

  pmem_dev = pmem_dev_alloc();
  if (pmem_dev == NULL)
  {
    err = -ENOMEM;
    goto on_error_1;
  }

  err = pmem_cdev_init(pmem_dev);
  if (err)
  {
    goto on_error_2;
  }

  /* success */
  return 0;

 on_error_2:
  pmem_dev_free(pmem_dev);
 on_error_1:
  unregister_chrdev_region(first_dev, 1);
 on_error_0:
  return err;
}


static void __exit pmem_exit(void)
{
  const dev_t first_dev = pmem_driver.major_minor;
  pmem_dev_t* const pmem_dev = &pmem_driver.dev;

  pmem_cdev_fini(pmem_dev);
  pmem_dev_free(pmem_dev);
  unregister_chrdev_region(first_dev, 1);
}
