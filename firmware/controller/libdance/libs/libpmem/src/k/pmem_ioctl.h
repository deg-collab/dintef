#ifndef PMEM_IOCTL_H_INCLUDED
# define PMEM_IOCTL_H_INCLUDED

#define PMEM_DEV_MAJOR 0x1b0
#define PMEM_DEV_MINOR 0

#define PMEM_IOCTL_MAGIC  'p'


/* memory ioctls */

#define PMEM_IOCTL_MEM_BASE 0x000
#define PMEM_IOCTL_MEM_GET_INFO \
  _IO(PMEM_IOCTL_MAGIC, PMEM_IOCTL_MEM_BASE + 0)
#define PMEM_IOCTL_MEM_SET_INFO \
  _IO(PMEM_IOCTL_MAGIC, PMEM_IOCTL_MEM_BASE + 1)
#define PMEM_IOCTL_MEM_ALLOC_PAGES \
  _IO(PMEM_IOCTL_MAGIC, PMEM_IOCTL_MEM_BASE + 2)
#define PMEM_IOCTL_MEM_FREE_PAGES \
  _IO(PMEM_IOCTL_MAGIC, PMEM_IOCTL_MEM_BASE + 3)
#define PMEM_IOCTL_MEM_SYNC_PAGES \
  _IO(PMEM_IOCTL_MAGIC, PMEM_IOCTL_MEM_BASE + 4)
#define PMEM_IOCTL_MEM_MAP_PAGES \
  _IO(PMEM_IOCTL_MAGIC, PMEM_IOCTL_MEM_BASE + 5)
#define PMEM_IOCTL_MEM_UNMAP_PAGES \
  _IO(PMEM_IOCTL_MAGIC, PMEM_IOCTL_MEM_BASE + 6)

typedef struct pmem_ioctl_mem_info
{
  size_t page_size;
  unsigned int limit32;
} pmem_ioctl_mem_info_t;

typedef struct pmem_ioctl_mem_alloc_pages
{
  int nid;
  size_t page_count;
  uintptr_t* pages;
} pmem_ioctl_mem_alloc_pages_t;

typedef struct pmem_ioctl_mem_free_pages
{
  size_t page_count;
  const uintptr_t* pages;
} pmem_ioctl_mem_free_pages_t;

typedef struct pmem_ioctl_mem_sync_pages
{
  size_t page_count;
  const uintptr_t* pages;
} pmem_ioctl_mem_sync_pages_t;

typedef struct pmem_ioctl_mem_map_pages
{
  uintptr_t vaddr;
  size_t page_count;
  const uintptr_t* pages;
} pmem_ioctl_mem_map_pages_t;

typedef struct pmem_ioctl_mem_unmap_pages
{
  uintptr_t vaddr;
  size_t page_count;
} pmem_ioctl_mem_unmap_pages_t;


#endif /* PMEM_IOCTL_H_INCLUDED */
