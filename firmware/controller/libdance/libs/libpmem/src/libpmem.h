#ifndef LIBPMEM_H_INCLUDED
# define LIBPMEM_H_INCLUDED


#include <stdint.h>
#include <sys/types.h>


int pmem_init_lib_with_dir(const char*, const char*);
int pmem_init_lib(void);
int pmem_fini_lib(void);
int pmem_alloc_pages(uintptr_t*, size_t);
int pmem_free_pages(const uintptr_t*, size_t);
int pmem_sync_pages(const uintptr_t*, size_t);
int pmem_map_pages(void**, const uintptr_t*, size_t);
int pmem_unmap_pages(void*, size_t);
int pmem_set_limit32(void);

extern size_t pmem_page_size;

static inline size_t pmem_size_to_pages(size_t size)
{
  const size_t page_mask = pmem_page_size - (size_t)1;
  const size_t npages = size / pmem_page_size;
  return npages + ((size & page_mask) ? 1 : 0);
}

static inline size_t pmem_pages_to_size(size_t npages)
{
  return npages * pmem_page_size;
}


#endif /* ! LIBPMEM_H_INCLUDED */
