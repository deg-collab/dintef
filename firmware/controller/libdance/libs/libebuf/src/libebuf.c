#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stddef.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include "libebuf.h"
#include "libpmem.h"


#define EBUF_CONFIG_DEBUG 1
#if (EBUF_CONFIG_DEBUG == 1)
#include <stdio.h>
#define PRINTF(__s, ...) do { printf(__s, ## __VA_ARGS__); } while (0)
#define PERROR() printf("[!] %s:%d\n", __FILE__, __LINE__)
#define ASSUME(__x) do { if (!(__x)) printf("[!] %s:%d\n", __FILE__, __LINE__); } while (0)
#else
#define PRINTF(__s, ...)
#define PERROR()
#define ASSUME(__x)
#endif /* EAIO_CONFIG_DEBUG */


/* exported routines */

static ebuf_flags_t ebuf_lib_flags = EBUF_FLAG_DEFAULT;

void ebuf_init_desc(ebuf_desc_t* desc)
{
  desc->flags = EBUF_FLAG_DEFAULT;
  desc->kmod_dir = NULL;
}

ebuf_err_t ebuf_init_lib_with_desc(const ebuf_desc_t* desc)
{
  ebuf_lib_flags = desc->flags;

  if (desc->flags & EBUF_FLAG_PHYSICAL)
  {
    if (pmem_init_lib_with_dir("/dev/dance", desc->kmod_dir))
      return EBUF_ERR_FAILURE;
  }

  return EBUF_ERR_SUCCESS;
}

ebuf_err_t ebuf_init_lib(ebuf_flags_t flags)
{
  ebuf_desc_t desc;
  ebuf_init_desc(&desc);
  desc.flags = flags;
  return ebuf_init_lib_with_desc(&desc);
}

ebuf_err_t ebuf_fini_lib(void)
{
  if (ebuf_lib_flags & EBUF_FLAG_PHYSICAL)
  {
    if (pmem_fini_lib() == -1) return EBUF_ERR_FAILURE;
  }

  return EBUF_ERR_SUCCESS;
}

static void clear_handle(ebuf_handle_t* buf)
{
  buf->uaddr = NULL;
  buf->usize = 0;
  buf->paddr = NULL;
  buf->flags = EBUF_FLAG_DEFAULT;
}

ebuf_err_t ebuf_alloc_handle(ebuf_handle_t** buf)
{
   *buf = malloc(sizeof(ebuf_handle_t));

   if (*buf == NULL)
      return EBUF_ERR_FAILURE;

   clear_handle(*buf);
   return EBUF_ERR_SUCCESS;
}

ebuf_err_t ebuf_free_handle(ebuf_handle_t* buf)
{
   ebuf_err_t ret = EBUF_ERR_SUCCESS;

   if (buf->uaddr != NULL)
      ret = ebuf_free(buf);

   free(buf);
   return ret;
}

static unsigned int is_physical(ebuf_flags_t flags)
{
  return (unsigned int)(flags & EBUF_FLAG_PHYSICAL);
}

ebuf_err_t ebuf_alloc(ebuf_handle_t* buf, size_t size, ebuf_flags_t flags)
{
  ASSUME((flags & EBUF_FLAG_USER) == 0);

  clear_handle(buf);

  if (is_physical(flags))
  {
    const size_t npage = pmem_size_to_pages(size);

    buf->paddr = malloc(npage * sizeof(uintptr_t));
    if (buf->paddr == NULL)
    {
      PERROR();
      return EBUF_ERR_FAILURE;
    }

    if (pmem_alloc_pages(buf->paddr, npage))
    {
      PERROR();
      free(buf->paddr);
      return EBUF_ERR_FAILURE;
    }

    if (pmem_map_pages(&buf->uaddr, buf->paddr, npage))
    {
      PERROR();
      pmem_free_pages(buf->paddr, npage);
      free(buf->paddr);
      return EBUF_ERR_FAILURE;
    }
  }
  else
  {
    /* use malloc free */
    buf->uaddr = malloc(size);
    if (buf->uaddr == NULL)
    {
      PERROR();
      return EBUF_ERR_FAILURE;
    }
  }

  buf->flags = flags;
  buf->usize = size;

  return EBUF_ERR_SUCCESS;
}

ebuf_err_t ebuf_free(ebuf_handle_t* buf)
{
  if (buf->flags & (EBUF_FLAG_USER | EBUF_FLAG_REF))
  {
    /* reference user data or another buffer */
    return EBUF_ERR_SUCCESS;
  }
  else if (is_physical(buf->flags))
  {
    /* reference kernel physical memory */

    const size_t npage = pmem_size_to_pages(buf->usize);

    if (pmem_unmap_pages(buf->uaddr, npage))
    {
      /* do not return, buffer no longer valid */
      PERROR();
    }

    if (pmem_free_pages(buf->paddr, npage))
    {
      /* do not return, buffer no longer valid */
      PERROR();
    }

    free(buf->paddr);
  }
  else
  {
    /* reference malloced memory */
    free(buf->uaddr);
  }
  
  clear_handle(buf);
  return EBUF_ERR_SUCCESS;
}

ebuf_err_t ebuf_sync(ebuf_handle_t* buf, size_t off, size_t size)
{
  /* insert any required memory barrier */
  __sync_synchronize();

#if defined(CONFIG_FREESCALE_IMX6)
  if (is_physical(buf->flags))
  {
    /* flush the cache BEFORE AND AFTER access from the device */
    char* const addr = (char*)buf->uaddr + off;
    __clear_cache(addr, addr + size);
  }
#endif

  return EBUF_ERR_SUCCESS;
}

ebuf_err_t ebuf_allocv
(ebuf_handle_t* bufs, size_t* sizes, ebuf_flags_t flags, size_t n)
{
  ebuf_err_t err = EBUF_ERR_FAILURE;
  size_t i;

  for (i = 0; i < n; ++i)
  {
    err = ebuf_alloc(&bufs[i], sizes[i], flags);
    if (err != EBUF_ERR_SUCCESS)
    {
      PERROR();
      goto on_error;
    }
  }

  return EBUF_ERR_SUCCESS;

 on_error:
  /* bufs[i] not allocated */
  ebuf_freev(bufs, i);
  return err;
}

ebuf_err_t ebuf_freev(ebuf_handle_t* bufs, size_t n)
{
  ebuf_err_t err = EBUF_ERR_SUCCESS;
  size_t i;

  for (i = 0; i < n; ++i)
  {
    if (ebuf_free(&bufs[i]) != EBUF_ERR_SUCCESS)
    {
      /* error but continue */
      err = EBUF_ERR_FAILURE;
      PERROR();
    }
  }

  return err;
}

ebuf_err_t ebuf_init_from_user(ebuf_handle_t* buf, void* data, size_t size)
{
  clear_handle(buf);

  buf->uaddr = data;
  buf->usize = size;
  buf->flags = EBUF_FLAG_USER;

  return EBUF_ERR_SUCCESS;
}
