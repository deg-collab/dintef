#ifndef LIBEBUF_H_INCLUDED
# define LIBEBUF_H_INCLUDED 1


#include <stdint.h>
#include <sys/types.h>


/* errors */

typedef enum
{
  EBUF_ERR_SUCCESS = 0,
  EBUF_ERR_FAILURE,
  EBUF_ERR_SIZE,
  EBUF_ERR_IMPL
} ebuf_err_t;


/* flags */

typedef enum
{
  EBUF_FLAG_DEFAULT = 0,
  EBUF_FLAG_PHYSICAL = 1 << 1,
  EBUF_FLAG_USER = 1 << 2,
  EBUF_FLAG_REF = 1 << 3
} ebuf_flags_t;


/* ebuf handle */

typedef struct ebuf_handle
{
  /* allocation flags */
  ebuf_flags_t flags;

  /* user address, size */
  void* uaddr;
  size_t usize;

  /* physical address */
  uintptr_t* paddr;

} ebuf_handle_t;

#define EBUF_INITIALISE {  \
   .flags = EBUF_FLAG_DEFAULT, \
   .uaddr = NULL, \
   .usize = 0,  \
   .paddr = NULL, \
}

/* ebuf description */

typedef struct ebuf_desc
{
  ebuf_flags_t flags;
  const char* kmod_dir;
} ebuf_desc_t;


/* exported routines */

void ebuf_init_desc(ebuf_desc_t*);

ebuf_err_t ebuf_init_lib(ebuf_flags_t);
ebuf_err_t ebuf_init_lib_with_desc(const ebuf_desc_t*);
ebuf_err_t ebuf_fini_lib(void);

ebuf_err_t ebuf_alloc_handle(ebuf_handle_t**);
ebuf_err_t ebuf_free_handle(ebuf_handle_t*);
ebuf_err_t ebuf_alloc(ebuf_handle_t*, size_t, ebuf_flags_t);
ebuf_err_t ebuf_free(ebuf_handle_t*);
ebuf_err_t ebuf_allocv(ebuf_handle_t*, size_t*, ebuf_flags_t, size_t);
ebuf_err_t ebuf_freev(ebuf_handle_t*, size_t);

ebuf_err_t ebuf_sync(ebuf_handle_t*, size_t, size_t);

ebuf_err_t ebuf_init_from_user(ebuf_handle_t*, void*, size_t);

static inline size_t ebuf_get_size(const ebuf_handle_t* buf)
{
  return buf->usize;
}

static inline void* ebuf_get_data(const ebuf_handle_t* buf)
{
  return buf->uaddr;
}

static inline uintptr_t ebuf_get_paddr(const ebuf_handle_t* buf)
{
  /* note: returns only the first physical address */
  return buf->paddr[0];
}


#endif /* ! LIBEBUF_H_INCLUDED */
