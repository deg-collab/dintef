#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include "libebuf.h"
#include "libpmem.h"

#define PERROR() \
do { printf("[!] %s,%d\n", __FILE__, __LINE__); } while (0)

int main(int ac, char** av)
{
  const size_t size = 128 * 1024 * 1024;
  size_t npage;
  ebuf_handle_t buf;
  uint8_t* data;
  size_t i;
  size_t j;

  if (ebuf_init_lib(EBUF_FLAG_PHYSICAL))
  {
    PERROR();
    goto on_error_0;
  }

  if (ebuf_alloc(&buf, size, EBUF_FLAG_PHYSICAL))
  {
    PERROR();
    goto on_error_1;
  }

  /* fill the buffer individual page using pmem */
  /* note: libpmem initialized by libebuf */
  npage = pmem_size_to_pages(size);
  for (i = 0; i < npage; ++i)
  {
    if (pmem_map_pages((void**)&data, buf.paddr + i, 1))
    {
      PERROR();
      goto on_error_2;
    }

    for (j = 0; j < pmem_page_size; ++j) data[j] = (uint8_t)(i + j);

    pmem_unmap_pages(data, 1);
  }

  /* check buffer contents using ebuf mapping */
  data = ebuf_get_data(&buf);
  for (i = 0; i < size; ++i)
  {
    const uint8_t x = (uint8_t)(i / pmem_page_size + i % pmem_page_size);
    if (x != data[i])
    {
      PERROR();
      printf("invalid at %u, 0x%02x != 0x%02x\n", (unsigned int)i, x, data[i]);
      goto on_error_2;
    }
  }

  printf("x\n");

 on_error_2:
  ebuf_free(&buf);
 on_error_1:
  pmem_fini_lib();
 on_error_0:
  return 0;
}
