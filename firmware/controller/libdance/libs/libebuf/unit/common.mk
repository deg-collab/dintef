

DANCE_SDK_PLATFORM ?= seco_imx6

LIBDANCE_HOME ?= ../../../..

LIBEBUF_DIR := ../../src
LIBEDMA_DIR := $(LIBDANCE_HOME)/libs/libedma/src
LIBUIRQ_DIR := $(LIBDANCE_HOME)/libs/libuirq/src
LIBPMEM_DIR := $(LIBDANCE_HOME)/libs/libpmem/src
LIBEPCI_DIR := $(LIBDANCE_HOME)/libs/libepci/src
LIBPCI_DIR = $(DANCE_SDK_DEPS_DIR)/lib

DEPENDENCIES_DIRS := \
      $(LIBEBUF_DIR)  \
      $(LIBEPCI_DIR)  \

HEADER_DIRS = \
      $(LIBEBUF_DIR)  \
      $(LIBPMEM_DIR)  \
      $(LIBEPCI_DIR)  \

LIBRARY_DIRS = \
      $(LIBEBUF_DIR)  \
      $(LIBEPCI_DIR)  \
      $(LIBPCI_DIR)  \
      $(LIBPMEM_DIR)  \

LDLIBS := -lebuf -lepci -lpci -lpmem

include $(LIBDANCE_HOME)/build/libtool.mk
