#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <sched.h>
#include <sys/types.h>
#include "libpmem.h"
#include "libebuf.h"
#include "libepci.h"


static char* const device_id = "10ee:eb01";


/* ebs_unit */

static void ebm0_write_ctrl0(epcihandle_t h, uint32_t x)
{
#define EBM0_CTRL0 0x00000000
  epci_wr32_reg(h, EBM0_CTRL0, x);
}

static uint32_t ebm0_read_ctrl0(epcihandle_t h)
{
  uint32_t x;
  epci_rd32_reg(h, EBM0_CTRL0, &x);
  return x;
}

static int enable_32bits(void)
{
  epcihandle_t bar;
  uint32_t x;

  bar = epci_open(device_id, NULL, 0);
  if (bar == EPCI_BAD_HANDLE) return -1;

  x = ebm0_read_ctrl0(bar);
  x &= ~(1 << 19);
  ebm0_write_ctrl0(bar, x);

  epci_close(bar);

  return 0;
}

static void set_probe(void)
{
  static const uint32_t value = (11 << 15) | (10 << 10) | (9 << 5) | (8 << 0);
  epcihandle_t bar;
  bar = epci_open(device_id, NULL, 0);
  epci_wr32_reg(bar, 0x0004, value);
  epci_close(bar);
}

static void enable_bus_mastering(void)
{
#if defined(CONFIG_FREESCALE_IMX6)
  /* enable bus mastering in command register */
  epcihandle_t conf = epci_open_config(device_id, NULL);
  uint32_t x;
  epci_rd_config(conf, 0x4, sizeof(uint32_t), &x);
  x |= 1 << 2;
  epci_wr_config(conf, 0x4, sizeof(uint32_t), &x);
  epci_rd_config(conf, 0x4, sizeof(uint32_t), &x);
  epci_close(conf);
#endif
}


/* main */

int main(int ac, char** av)
{
  static const size_t off = 0x100;
  const unsigned int n = (ac > 1) ? atoi(av[1]) : (unsigned int)-1;
  epcihandle_t bar;
  ebuf_handle_t buf;
  uint32_t k;

  bar = epci_open(device_id, NULL, 0x01);
  if (bar == EPCI_BAD_HANDLE)
  {
    goto on_error_0;
  }

  if (ebuf_init_lib(EBUF_FLAG_PHYSICAL))
  {
    goto on_error_1;
  }

  if (pmem_set_limit32())
  {
    goto on_error_2;
  }

  if (enable_32bits())
  {
    goto on_error_2;
  }

  if (ebuf_alloc(&buf, 0x1000, EBUF_FLAG_PHYSICAL))
  {
    goto on_error_2;
  }

  set_probe();

  enable_bus_mastering();

  /* addr */
  epci_wr32_reg(bar, off + 1 * sizeof(uint32_t), (uint32_t)ebuf_get_paddr(&buf));

  for (k = 0; 1; ++k)
  {
    /* key */
    epci_wr32_reg(bar, off + 2 * sizeof(uint32_t), k);

    *(volatile uint32_t*)ebuf_get_data(&buf) = 0;

    /* sync before access */
    ebuf_sync(&buf, 0, ebuf_get_size(&buf));

    /* start */
    epci_wr32_reg(bar, off + 0 * sizeof(uint32_t), 1 << 31);

    usleep(100);

    /* check */
    if (*(volatile uint32_t*)ebuf_get_data(&buf) != k)
    {
      printf("error at 0x%08x\n", k);
      break ;
    }

    if ((k % 10000) == 0)
    {
      printf(".");
      fflush(stdout);
    }

    if (k == n) break ;
  }

  printf("done\n");

  ebuf_free(&buf);
 on_error_2:
  ebuf_fini_lib();
 on_error_1:
  epci_close(bar);
 on_error_0:
  return 0;
}
