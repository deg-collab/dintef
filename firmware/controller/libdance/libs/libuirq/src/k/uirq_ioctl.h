#ifndef UIRQ_IOCTL_H_INCLUDED
#define UIRQ_IOCTL_H_INCLUDED


typedef struct uirq_ioctl_irqcontext
{
  /* captured registers */
  uint32_t ebm0_stat1;
} uirq_ioctl_irqcontext_t;


typedef struct uirq_ioctl_irqmask
{
  /* use ebm0_stat1 to identify interrupt source */
  unsigned int use_ebm0_stat1;
  /* which irq a given file wants to clear upon capture */
  uint32_t mask;
} uirq_ioctl_irqmask_t;


#define UIRQ_IOCTL_MAGIC  0
#define UIRQ_IOCTL_GET_IRQCONTEXT _IOR(UIRQ_IOCTL_MAGIC, 0, uintptr_t)
#define UIRQ_IOCTL_SET_IRQMASK _IOW(UIRQ_IOCTL_MAGIC, 1, uintptr_t)


#endif /* UIRQ_IOCTL_H_INCLUDED */
