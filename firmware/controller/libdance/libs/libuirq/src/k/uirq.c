#include <linux/init.h>
#include <linux/pci.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/poll.h>
#include <linux/mm.h>
#include <linux/mmzone.h> /* MAX_ORDER */
#include <linux/rmap.h>
#include <asm/mman.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/pci.h>
#include <linux/ioport.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <asm/io.h>

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19))
typedef unsigned long uintptr_t;
#endif
#include "uirq_ioctl.h"


/* only for debugging purpose, do not define in release mode */
#define UIRQ_ASSERT(__x)


/* per device info */
typedef struct uirq_dev
{
  /* char device is contained for o(1) lookup */
  struct cdev cdev;

  /* mapped base addresses and sizes */
  uintptr_t vadr[6];
  size_t len[6];

  /* interrupt backed context */
  struct irq_context
  {
    volatile uint32_t ebm0_stat1;
  } irq_context;

  /* interrupt waiting queue */
  wait_queue_head_t wq;

  /* fixme: disable_irq_quirk */
  volatile long unhandled_irq_quirk;

  volatile unsigned int use_ebm0_stat1;

  /* irq vector */
  unsigned int pci_irq;

} uirq_dev_t;


/* per file info */
typedef struct uirq_file
{
  uirq_dev_t* dev;
  uint32_t irq_mask;
} uirq_file_t;


/* low level operations for UIRQ hardware */

/* offsets and base addresses are specific to a given design */
/* UIRQ master#0 */
#define EBM0_BAR 0x00
#define EBM0_OFFSET 0x00
/* fast transmitter */
#define EBFT_BAR 0x01
#define EBFT_OFFSET 0x400
/* memory bank #3 */
#define MB3_BAR 0x03
#define MB3_OFFSET 0x00

/* uirq master#0 (ebm0_pcie_vx6_a.pdf) */
#define EBM0_REG_CTRL0 0x00
#define EBM0_REG_CTRL1 0x04
#define EBM0_REG_CTRL2 0x08
#define EBM0_REG_CTRL3 0x0c
#define EBM0_REG_STAT0 0x10
#define EBM0_REG_STAT1 0x14
#define EBM0_REG_DEBUG 0x18
#define EBM0_REG_TDESC 0x1c
#define EBM0_REG_ERR1 0x24
#define EBM0_REG_SCLR0 0x140
#define EBM0_REG_SCLR1 0x144

/* ebm0 control0 register bit offsets */
#define EBM0_CTRL0_GIE 0

/* ebm0 status1 register bit offsets */
#define EBM0_STAT1_EBFT_INT 0

/* fast transmitter configuration (ebm_ebft.pdf) */
#define EBFT_REG_D0_SRCE 0x00
#define EBFT_REG_D0_SMIF 0x04
#define EBFT_REG_D0_RESERVED 0x08
#define EBFT_REG_D0_DLSW 0x0c
#define EBFT_REG_D0_DMSW 0x10
#define EBFT_REG_D0_CTRL 0x14
#define EBFT_REG_D1_SRCE 0x18
#define EBFT_REG_D1_SMIF 0x1c
#define EBFT_REG_D1_RESERVED 0x20
#define EBFT_REG_D1_DLSW 0x24
#define EBFT_REG_D1_DMSW 0x28
#define EBFT_REG_D1_CTRL 0x2c
#define EBFT_REG_PSIZE 0x30
#define EBFT_REG_GCSR 0x34
#define EBFT_REG_D0_STAT 0x38
#define EBFT_REG_D1_STAT 0x3c


/* internal registers io wrappers */

static inline uint32_t make_mask1(uint32_t x)
{
  return 1 << x;
}

static inline uint32_t read_uint32(uintptr_t addr)
{
  return (uint32_t)readl((void*)addr);
}

static inline void write_uint32(uint32_t data, uintptr_t addr)
{
  writel(data, (void*)addr);
}

static inline uint32_t ebm0_read_ctrl0(uirq_dev_t* dev)
{
  const uintptr_t addr = dev->vadr[EBM0_BAR] + EBM0_OFFSET + EBM0_REG_CTRL0;
  return read_uint32(addr);
}

static inline void ebm0_write_ctrl0(uirq_dev_t* dev, uint32_t data)
{
  const uintptr_t addr = dev->vadr[EBM0_BAR] + EBM0_OFFSET + EBM0_REG_CTRL0;
  return write_uint32(data, addr);
}

static inline uint32_t ebm0_read_stat0(uirq_dev_t* dev)
{
  const uintptr_t addr = dev->vadr[EBM0_BAR] + EBM0_OFFSET + EBM0_REG_STAT0;
  return read_uint32(addr);
}

static inline uint32_t ebm0_read_stat1(uirq_dev_t* dev)
{
  const uintptr_t addr = dev->vadr[EBM0_BAR] + EBM0_OFFSET + EBM0_REG_STAT1;
  return read_uint32(addr);
}

static inline uint32_t ebm0_read_err1(uirq_dev_t* dev)
{
  const uintptr_t addr = dev->vadr[EBM0_BAR] + EBM0_OFFSET + EBM0_REG_ERR1;
  return read_uint32(addr);
}

static inline uint32_t ebft_read_d0_stat(uirq_dev_t* dev)
{
  const uintptr_t addr = dev->vadr[EBFT_BAR] + EBFT_OFFSET + EBFT_REG_D0_STAT;
  return read_uint32(addr);
}

static inline void ebm0_write_sclr0(uirq_dev_t* dev, uint32_t data)
{
  const uintptr_t addr = dev->vadr[EBM0_BAR] + EBM0_OFFSET + EBM0_REG_SCLR0;
  return write_uint32(data, addr);
}

static inline void ebm0_write_sclr1(uirq_dev_t* dev, uint32_t data)
{
  const uintptr_t addr = dev->vadr[EBM0_BAR] + EBM0_OFFSET + EBM0_REG_SCLR1;
  return write_uint32(data, addr);
}

/* on interupt, capture ebm0_stat1 and wait queue */

#if defined(CONFIG_FREESCALE_IMX6)
#define __sync_synchronize() __asm__ __volatile__ ("":::"memory")
#endif /* CONFIG_FREESCALE_IMX6 */

static irqreturn_t irq_handler
(
 int irq, void* dev_id
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19))
 , struct pt_regs* do_not_use /* may be NULL */
#endif
)
{
  uirq_dev_t* const dev = dev_id;
  unsigned int do_wake;
  uint32_t ebm0_stat1;

  if (dev->unhandled_irq_quirk)
  {
    printk("unhandled_irq_quirk\n");
    return IRQ_RETVAL(1);
  }

  UIRQ_ASSERT(dev->vadr[EBM0_BAR]);

  /* acknowledge the interrupt */

  /* ebm0 status1 register, self clear bits */
  ebm0_stat1 = ebm0_read_stat1(dev);

  /* check ored bit */
  if (ebm0_stat1 & (1 << 31))
  {
    /* mask slave for ebone broadcall */
    ebm0_write_sclr0(dev, ebm0_stat1 & ~(1 << 31));

#if defined(CONFIG_FREESCALE_IMX6)
    /* msi disabled, acknowledge legacy interrupt bit */
    ebm0_write_sclr1(dev, 1 << 0);
#endif
  }

  do_wake = 0;
  if (dev->use_ebm0_stat1)
  {
    /* check ored bit */
    if (ebm0_stat1 & (1 << 31))
    {
      /* capture context before waking */
      /* use non destructive OR operation */
      __sync_synchronize();
      dev->irq_context.ebm0_stat1 |= ebm0_stat1;
      do_wake = 1;
    }
  }
  else
  {
    /* set all ones */
    dev->irq_context.ebm0_stat1 |= (uint32_t)-1;
    do_wake = 1;
  }

  if (do_wake)
  {
    wake_up_interruptible(&dev->wq);
    return IRQ_HANDLED;
  }

  return IRQ_NONE;
}

int uirq_match_pci_ids(unsigned int vid, unsigned int did)
{
  if (vid == PCI_VENDOR_ID_XILINX)
  {
    if ((did == 0xeb01) || (did == 0xeb02)) return 0;
  }

  return -1;
}


static void uirq_first_read_fixup(uirq_dev_t* dev)
{
  /* there is a known bug in the UIRQ FPGA hardware which requires a dummy
     read to be done which always returns 0xffffffff. otherwise, the first
     read is likely to be done by the interrupt handler for status1 register
     which would consider an interrupt while there is not.
   */
  ebft_read_d0_stat(dev);
}


static void uirq_clear_ctrl0_gie(uirq_dev_t* dev)
{
  /* clear global interrupt enable bit */
  const uint32_t data = ebm0_read_ctrl0(dev);
  ebm0_write_ctrl0(dev, data & ~make_mask1(EBM0_CTRL0_GIE));
}


static void uirq_dev_init(uirq_dev_t* dev)
{
  const unsigned int nbar = sizeof(dev->vadr) / sizeof(dev->vadr[0]);
  unsigned int i;

  for (i = 0; i < nbar; ++i)
  {
    dev->vadr[i] = 0;
    dev->len[i] = 0;
  }

  init_waitqueue_head(&dev->wq);
  dev->irq_context.ebm0_stat1 = 0;
  dev->unhandled_irq_quirk = 0;
  dev->use_ebm0_stat1 = 0;
}


/* linux module interface, not compiled if not standing alone */

MODULE_DESCRIPTION("UIRQ driver");
MODULE_AUTHOR("UIRQ team");
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
MODULE_LICENSE("Dual BSD/GPL");
#endif


#define UIRQ_NAME KBUILD_MODNAME
#define UIRQ_TAG "[ " UIRQ_NAME " ] "
#define UIRQ_ENTER() printk(UIRQ_TAG "%s\n", __FUNCTION__)
#define UIRQ_ASSERT(__x)


/* UIRQ per driver data (global, singleton)
 */

typedef struct atomic32
{
  volatile uint32_t x;
} atomic32_t;

static inline void atomic32_init(atomic32_t* x)
{
  /* not a synced access */
  x->x = 0;
}

static inline uint32_t atomic32_or(atomic32_t* x, uint32_t val)
{
  /* return the previous value */
#if defined(CONFIG_FREESCALE_IMX6)
  const uint32_t xx = x->x;
  x->x |= val;
  return xx;
#else
  return __sync_fetch_and_or(&x->x, val);
#endif
}

static inline uint32_t atomic32_and(atomic32_t* x, uint32_t val)
{
  /* return the previous value */
#if defined(CONFIG_FREESCALE_IMX6)
  const uint32_t xx = x->x;
  x->x &= val;
  return xx;
#else
  return __sync_fetch_and_and(&x->x, val);
#endif
}

static inline uint32_t atomic32_read(atomic32_t* x)
{
  /* not a synced access */
  return x->x;
}

typedef struct uirq_driver
{
  /* device storage. must be < 32. */
#define UIRQ_DEV_COUNT 2
  uirq_dev_t devs[UIRQ_DEV_COUNT];

  /* allocated devices bitmap. 0 means free. */
  atomic32_t bits;

  /* pmem spans minors [0,0] */
#define UIRQ_MINOR_BASE 1

  /* device major,minor number */
  dev_t major_minor;

} uirq_driver_t;

static uirq_driver_t uirq_driver;

static inline void uirq_driver_init(uirq_driver_t* d)
{
  atomic32_init(&d->bits);
}

static inline uint32_t dev_to_bit32(uirq_dev_t* dev)
{
  /* return the device bit position in uirq_driver.bits */
  return (uint32_t)
    ((uintptr_t)dev - (uintptr_t)uirq_driver.devs) / sizeof(uirq_dev_t);
}

static inline uirq_dev_t* bit32_to_dev(uint32_t x)
{
  return &uirq_driver.devs[x];
}

static uirq_dev_t* uirq_dev_alloc(void)
{
  /* work on a captured value, try to set and commit any free bit */

  const uint32_t ndev = UIRQ_DEV_COUNT;

  uint32_t mask;
  uint32_t x;
  uint32_t i;

  x = atomic32_read(&uirq_driver.bits);

  for (i = 0; i < ndev; ++i, x >>= 1)
  {
    /* no more bit avail */
    mask = (1 << (ndev - i)) - 1;
    if ((x & mask) == mask) return NULL;

    /* current bit avail */
    if ((x & 1) == 0)
    {
      /* set and check for lost race */
      mask = 1 << i;
      if ((atomic32_or(&uirq_driver.bits, mask) & mask) == 0)
      {
	/* perform any software zeroing needed */
	uirq_dev_t* const dev = bit32_to_dev(i);
	uirq_dev_init(dev);
	return dev;
      }

      /* otherwise someone took it in between */
    }
  }

  return NULL;
}

static inline void uirq_dev_free(uirq_dev_t* dev)
{
  const uint32_t mask = ~(1 << dev_to_bit32(dev));
  atomic32_and(&uirq_driver.bits, mask);
}

static inline unsigned int uirq_is_dev_allocated(uirq_dev_t* dev)
{
  /* non atomic access */
  return atomic32_read(&uirq_driver.bits) & (1 << dev_to_bit32(dev));
}

static inline unsigned int uirq_is_dev_valid(uirq_dev_t* dev)
{
  /* check if the device address is valid */
  if ((uintptr_t)dev < (uintptr_t)&uirq_driver.devs[0]) return 0;
  if ((uintptr_t)dev >= (uintptr_t)&uirq_driver.devs[UIRQ_DEV_COUNT]) return 0;
  return 1;
}

static void uirq_dev_unmap_bars
(uirq_dev_t* uirq_dev, struct pci_dev* pci_dev __attribute__((unused)))
{
  const unsigned int nbar = sizeof(uirq_dev->vadr) / sizeof(uirq_dev->vadr[0]);

  unsigned int i;

  for (i = 0; i < nbar; ++i)
  {
    if (uirq_dev->vadr[i])
    {
      pci_release_region(pci_dev, (int)i);
      iounmap((void*)uirq_dev->vadr[i]);
      uirq_dev->vadr[i] = 0;
      uirq_dev->len[i] = 0;
    }
  }
}

static int uirq_dev_map_bars(uirq_dev_t* uirq_dev, struct pci_dev* pci_dev)
{
  const unsigned int nbar = sizeof(uirq_dev->vadr) / sizeof(uirq_dev->vadr[0]);
  int err = 0;
  unsigned int i;

  for (i = 0; i < nbar; ++i)
  {
    const unsigned long flags = pci_resource_flags(pci_dev, i);
    const uintptr_t addr = (uintptr_t)pci_resource_start(pci_dev, i);
    const size_t len = (size_t)pci_resource_len(pci_dev, i);

    if ((err == 0) && (flags & IORESOURCE_MEM) && (addr && len))
    {
      if ((err = pci_request_region(pci_dev, (int)i, UIRQ_NAME)))
      {
	/* continue with error set */
	uirq_dev->len[i] = 0;
	uirq_dev->vadr[i] = 0;
      }
      else
      {
	uirq_dev->vadr[i] = (uintptr_t)ioremap_nocache(addr, len);
	uirq_dev->len[i] = len;
	if (uirq_dev->vadr[i] == 0)
	{
	  /* continue but zero remaining entries for unmapping */
	  uirq_dev->len[i] = 0;
	  err = -ENOMEM;
	}
      }
    }
    else
    {
      uirq_dev->vadr[i] = 0;
      uirq_dev->len[i] = 0;
    }
  }

  if (err) uirq_dev_unmap_bars(uirq_dev, pci_dev);
  return err;
}


/* PCI interface */

DEFINE_PCI_DEVICE_TABLE(uirq_pci_ids) =
{
  /* uirq */
  { PCI_DEVICE( PCI_VENDOR_ID_XILINX, 0xeb01 ) },
  { PCI_DEVICE( PCI_VENDOR_ID_XILINX, 0xeb02 ) },
  { 0, }
};

/* used to export symbol for use by userland */
MODULE_DEVICE_TABLE(pci, uirq_pci_ids);

/* PCI error recovery */

static pci_ers_result_t uirq_pci_error_detected
(struct pci_dev* pci_dev, pci_channel_state_t state)
{
  printk(UIRQ_TAG "uirq_pci_error_detected(0x%08x)\n", state);

  if (state == pci_channel_io_perm_failure)
    return PCI_ERS_RESULT_DISCONNECT;

  /* disable an request a slot reset */

  pci_disable_device(pci_dev);

  return PCI_ERS_RESULT_NEED_RESET;
}

static pci_ers_result_t uirq_pci_mmio_enabled(struct pci_dev* pci_dev)
{
  printk(UIRQ_TAG "pci_mmio_enabled\n");
  return PCI_ERS_RESULT_RECOVERED;
}

static pci_ers_result_t uirq_pci_link_reset(struct pci_dev* pci_dev)
{
  printk(UIRQ_TAG "uirq_pci_link_reset\n");
  return PCI_ERS_RESULT_RECOVERED;
}

static pci_ers_result_t uirq_pci_slot_reset(struct pci_dev* pci_dev)
{
  printk(UIRQ_TAG "uirq_pci_slot_reset\n");
  return PCI_ERS_RESULT_RECOVERED;
}

static void uirq_pci_resume(struct pci_dev* pci_dev)
{
  printk(UIRQ_TAG "uirq_pci_resume\n");
}

static struct pci_error_handlers uirq_err_handler =
{
  .error_detected = uirq_pci_error_detected,
  .mmio_enabled = uirq_pci_mmio_enabled,
  .slot_reset = uirq_pci_slot_reset,
  .link_reset = uirq_pci_link_reset,
  .resume = uirq_pci_resume
};

static int uirq_cdev_init(uirq_dev_t* dev);
static inline void uirq_cdev_fini(uirq_dev_t* dev);

/* linux-source-3.2/Documentation/PCI/pci.txt recommends __devinit */

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19))
static inline int pci_is_enabled(struct pci_dev *pdev)
{
  return 0;
}
#endif

#if (LINUX_VERSION_CODE < KERNEL_VERSION(3,14,28))
static int __devinit uirq_pci_probe
#else
static int uirq_pci_probe
#endif
(struct pci_dev* pci_dev, const struct pci_device_id* dev_id)
{
  uirq_dev_t* uirq_dev;
  int err;

  /* allocate device structure
   */

  if ((uirq_dev = uirq_dev_alloc()) == NULL)
  {
    err = -ENOMEM;
    goto on_error_0;
  }

  uirq_dev->unhandled_irq_quirk = 1;

  pci_set_drvdata(pci_dev, uirq_dev);

#if 1 /* FIXME: do not enable twice */
  if (pci_is_enabled(pci_dev) == 0)
#endif /* FIXME */
    if ((err = pci_enable_device(pci_dev)))
      goto on_error_1;

#if defined(CONFIG_FREESCALE_IMX6)
  pci_set_master(pci_dev);
#endif /* CONFIG_FREESCALE_IMX6 */

  /* map the device bars
   */

  if ((err = uirq_dev_map_bars(uirq_dev, pci_dev)))
  {
    printk("[!] uirq_dev_map() == %d\n", err);
    goto on_error_2;
  }

  uirq_first_read_fixup(uirq_dev);
  uirq_clear_ctrl0_gie(uirq_dev);

  /* install interrupt handler
   */
#if !defined(CONFIG_FREESCALE_IMX6)
  if ((err = pci_enable_msi(pci_dev)))
  {
    printk("[!] pci_enable_msi() == %d\n", err);
    goto on_error_2;
  }
#endif

  /* printk("uirq: pci_enable_msi() == %d\n", pci_dev->irq); */

#if (LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,18))
# define IRQ_SHARED_FLAG IRQF_SHARED
#else
# define IRQ_SHARED_FLAG SA_SHIRQ
#endif
  err = request_irq
  (
   pci_dev->irq,
   irq_handler,
   IRQ_SHARED_FLAG,
   UIRQ_NAME,
   (void*)uirq_dev
  );

  if (err)
  {
    printk("[!] request_irq() == %d\n", err);
    goto on_error_3;
  }

  uirq_dev->pci_irq = pci_dev->irq;

  /* initialize char device */

  if ((err = uirq_cdev_init(uirq_dev)))
  {
    printk("[!] uirq_cdev_init() == %d\n", err);
    goto on_error_4;
  }

  uirq_dev->unhandled_irq_quirk = 0;

  /* success */

  return 0;

 on_error_4:
  free_irq(pci_dev->irq, uirq_dev);
 on_error_3:
#if !defined(CONFIG_FREESCALE_IMX6)
  pci_disable_msi(pci_dev);
#endif
 on_error_2:
  uirq_dev_unmap_bars(uirq_dev, pci_dev);
  pci_disable_device(pci_dev);
 on_error_1:
  uirq_dev_free(uirq_dev);
  pci_set_drvdata(pci_dev, NULL);
 on_error_0:
  return err;
}

#if (LINUX_VERSION_CODE < KERNEL_VERSION(3,14,28))
static void __devexit uirq_pci_remove(struct pci_dev* pci_dev)
#else
static void uirq_pci_remove(struct pci_dev* pci_dev)
#endif
{
  uirq_dev_t* const uirq_dev = pci_get_drvdata(pci_dev);

  UIRQ_ASSERT(uirq_dev);

  uirq_cdev_fini(uirq_dev);

  uirq_dev->unhandled_irq_quirk = 1;
  /* previous write must be seen before any access to dev->vadr[i] */
  __sync_synchronize();

  uirq_clear_ctrl0_gie(uirq_dev);

  free_irq(pci_dev->irq, uirq_dev);
#if !defined(CONFIG_FREESCALE_IMX6)
  pci_disable_msi(pci_dev);
#endif

  uirq_dev_unmap_bars(uirq_dev, pci_dev);

  pci_disable_device(pci_dev);

  pci_set_drvdata(pci_dev, NULL);

  uirq_dev_free(uirq_dev);
}

static struct pci_driver uirq_pci_driver =
{
  .name = UIRQ_NAME,
  .id_table = uirq_pci_ids,
  .probe = uirq_pci_probe,
#if (LINUX_VERSION_CODE < KERNEL_VERSION(3,14,28))
  .remove = __devexit_p(uirq_pci_remove),
#else
  .remove = uirq_pci_remove,
#endif
  .err_handler = &uirq_err_handler
};

static int uirq_pci_init(void)
{
  return pci_register_driver(&uirq_pci_driver);
}

static void uirq_pci_fini(void)
{
  pci_unregister_driver(&uirq_pci_driver);
}


/* LINUX file interface */

static int uirq_file_open(struct inode* inode, struct file* file)
{
  uirq_dev_t* const dev = container_of(inode->i_cdev, uirq_dev_t, cdev);
  uirq_file_t* file_data;

  if (!(uirq_is_dev_valid(dev) && uirq_is_dev_allocated(dev)))
  {
    return -EFAULT;
  }

  file_data = kmalloc(sizeof(uirq_file_t), GFP_KERNEL);
  if (file_data == NULL)
  {
    return -ENOMEM;
  }

  file_data->dev = dev;
  file_data->irq_mask = 0;

  file->private_data = file_data;

  return 0;
}

static int uirq_file_close(struct inode* inode, struct file* file)
{
  /* uirq_dev_t* const dev = file->private_data; */
  /* UIRQ_ASSERT(dev); */

  kfree(file->private_data);
  file->private_data = NULL;

  return 0;
}

#ifdef HAVE_UNLOCKED_IOCTL
static long uirq_file_ioctl
(struct file* file, unsigned int cmd, unsigned long uaddr)
{
  __attribute__((unused)) struct inode* const inode = file->f_dentry->d_inode;
#else
static int uirq_ioctl
(struct inode* inode, struct file* file, unsigned int cmd, unsigned long uaddr)
{
#endif

  uirq_file_t* const uirq_file = file->private_data;
  uirq_dev_t* const uirq_dev = uirq_file->dev;
  int err;

  UIRQ_ASSERT(uirq_dev);

  switch (cmd)
  {
  case UIRQ_IOCTL_GET_IRQCONTEXT:
    {
      /* get and clear the last captured irq context */
      /* use uirq_file->irq_mask to clear irqs */

      uirq_ioctl_irqcontext_t arg;

      if (copy_from_user(&arg, (void*)(uintptr_t)uaddr, sizeof(arg)))
      {
	err = -EFAULT;
	break ;
      }

      disable_irq(uirq_dev->pci_irq);

      __sync_synchronize();
      arg.ebm0_stat1 = uirq_dev->irq_context.ebm0_stat1;
      uirq_dev->irq_context.ebm0_stat1 &= ~uirq_file->irq_mask;

      enable_irq(uirq_dev->pci_irq);

      if (copy_to_user((void*)(uintptr_t)uaddr, &arg, sizeof(arg)))
      {
	err = -EFAULT;
	break ;
      }

      /* success */
      err = 0;
      break ;
    }

  case UIRQ_IOCTL_SET_IRQMASK:
    {
      /* set the mask of irqs to be cleared as a GET_IRQCONTEXT is done */

      uirq_ioctl_irqmask_t arg;

      if (copy_from_user(&arg, (void*)(uintptr_t)uaddr, sizeof(arg)))
      {
	err = -EFAULT;
	break ;
      }

      uirq_dev->use_ebm0_stat1 = arg.use_ebm0_stat1;
      uirq_file->irq_mask = arg.mask;

      /* success */
      err = 0;
      break ;
    }

  default:
    {
      err = -ENOSYS;
      break ;
    }
  }

  return err;
}

static unsigned int uirq_file_poll
(struct file* file, struct poll_table_struct* pts)
{
  uirq_file_t* const uirq_file = file->private_data;
  uirq_dev_t* const dev = uirq_file->dev;
  unsigned int poll_mask = 0;

  if (dev->irq_context.ebm0_stat1 & uirq_file->irq_mask)
    return POLLIN | POLLRDNORM;

  poll_wait(file, &dev->wq, pts);

  if (dev->irq_context.ebm0_stat1 & uirq_file->irq_mask)
    poll_mask |= POLLIN | POLLRDNORM;

  return poll_mask;
}

static struct file_operations uirq_fops =
{
 .owner = THIS_MODULE,
#ifdef HAVE_UNLOCKED_IOCTL
 .unlocked_ioctl = uirq_file_ioctl,
#else
 .ioctl = uirq_file_ioctl,
#endif
 .open = uirq_file_open,
 .poll = uirq_file_poll,
 .release = uirq_file_close
};

static int uirq_cdev_init(uirq_dev_t* dev)
{
  /* register the chardev and associated file operations */

  const int minor = (int)(UIRQ_MINOR_BASE + dev_to_bit32(dev));
  const dev_t devno = MKDEV(MAJOR(uirq_driver.major_minor), minor);

  cdev_init(&dev->cdev, &uirq_fops);
  dev->cdev.owner = THIS_MODULE;

  /* nothing to do on failure */
  return cdev_add(&dev->cdev, devno, 1);
}

static inline void uirq_cdev_fini(uirq_dev_t* dev)
{
  cdev_del(&dev->cdev);
}


/* LINUX module interface */

static int  __init uirq_init(void);
static void __exit uirq_exit(void);


#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,5,0)
module_init(uirq_init);
module_exit(uirq_exit);
#endif


static int __init uirq_init(void)
{
  static const unsigned int ndev = UIRQ_DEV_COUNT;
  dev_t first_dev;
  int err;

  uirq_driver_init(&uirq_driver);

  /* references */
  /* http://hg.berlios.de/repos/kedr/file/b0f4d9d02d35/sources/examples/sample_target/cfake.c */
  /* http://lists.kernelnewbies.org/pipermail/kernelnewbies/2011-May/001660.html */

#if 0
  /* dynamic allocation */
  err = alloc_chrdev_region(&first_dev, UIRQ_MINOR_BASE, ndev, UIRQ_NAME);
#else
  /* static allocation */
  first_dev = MKDEV(0xeb0, UIRQ_MINOR_BASE);
  err = register_chrdev_region(first_dev, ndev, UIRQ_NAME);
#endif

  if (err < 0)
  {
    return err;
  }

  uirq_driver.major_minor = first_dev;

  if ((err = uirq_pci_init()))
  {
    unregister_chrdev_region(first_dev, ndev);
    return err;
  }

  return 0;
}

static void __exit uirq_exit(void)
{
  /* note: uirq_pci_remove is called when unregistering pci driver */
  static const unsigned int ndev = UIRQ_DEV_COUNT;
  const dev_t first_dev = uirq_driver.major_minor;
  uirq_pci_fini();
  unregister_chrdev_region(first_dev, ndev);
}
