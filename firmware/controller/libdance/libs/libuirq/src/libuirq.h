#ifndef LIBUIRQ_H_INCLUDED
#define LIBUIRQ_H_INCLUDED


#include <stdint.h>


struct epcihandle_s;


typedef struct uirq_handle {
   int          dev_fd;
   struct epcihandle_s* ebm0_bar;
   unsigned int use_ebm0_stat1;
   uint32_t     irq_mask;

   const char*  pci_id;  // typically "10ee:eb01"

#define UIRQ_ISR_MAX_COUNT 8
   size_t isr_count;

   struct {
      void (*fn)(void*);
      void* data;
      uint32_t mask;
   } isr[UIRQ_ISR_MAX_COUNT];

} uirq_handle_t;


int uirq_init_lib_with_dir(const char*, const char*);
int uirq_init_lib(void);
int uirq_fini_lib(void);
int uirq_init(uirq_handle_t*, const char*);
int uirq_open(uirq_handle_t*);
int uirq_close(uirq_handle_t*);
int uirq_set_mask(uirq_handle_t*, uint32_t, unsigned int);
int uirq_clear_mask(uirq_handle_t*, uint32_t);
int uirq_poll(uirq_handle_t*, uint32_t*);
int uirq_wait(uirq_handle_t*, unsigned int, uint32_t*);
int uirq_register_isr(uirq_handle_t*, uint32_t, void (*)(void*), void*);
int uirq_unregister_isr(uirq_handle_t*, uint32_t);
int uirq_handle_irq(uirq_handle_t*);

static inline int uirq_get_fd(const uirq_handle_t* uirq) {
   return uirq->dev_fd;
}

static inline int uirq_ack(uirq_handle_t* uirq) {
   /* wrapper around poll */
   uint32_t x;
   return uirq_poll(uirq, &x);
}

#endif /* LIBUIRQ_H_INCLUDED */
