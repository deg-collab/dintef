#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/wait.h>
#include <sys/utsname.h>
#include "libuirq.h"
#include "k/uirq_ioctl.h"
#include "libepci.h"


#define CONFIG_LIBUIRQ_DEBUG 1
#if (CONFIG_LIBUIRQ_DEBUG == 1)
#include <stdio.h>
#define UIRQ_ASSUME(__x) \
do { if (!(__x)) printf("[!] %d\n", __LINE__); } while (0)
#define UIRQ_PRINTF(__s, ...) \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define UIRQ_PERROR() \
do { printf("[!] %s,%d\n", __FILE__, __LINE__); } while (0)
#else
#define UIRQ_ASSUME(__x)
#define UIRQ_PRINTF(__s, ...)
#define UIRQ_PERROR()
#endif


static char uirq_dir_path[32];
static char uirq_dev_path[32];

static int exec_common(const char** av) {
   int status;
   pid_t pid;

   pid = fork();
   if (pid == -1) return -1;

   if (pid == 0) {
      /* child */

      const int fd = open("/dev/null", O_WRONLY);

      if (fd != -1) {
         dup2(fd, STDOUT_FILENO);
         dup2(fd, STDERR_FILENO);
         close(fd);
      }
      execve(av[0], (char**)av, NULL);
      /* not reached or error */
      exit(-1);
   }

   if (waitpid(pid, &status, 0) == -1) {
      kill(pid, SIGKILL);
      return -1;
   }

  if (WIFEXITED(status) == 0) return -1;
  return (int)WEXITSTATUS(status);
}

static int exec_insmod(const char* kpath) {
   const char* av[] = { "/sbin/insmod", kpath, NULL };
   return exec_common(av);
}

static int exec_rmmod(const char* kname) {
   const char* av[] = { "/sbin/rmmod", kname, NULL };
   return exec_common(av);
}

static int load_kmod(const char* kname, const char* kmod_dir) {
   /* load kernel module */

   struct utsname un;
   struct stat st;
   char kpath[128];

   if (kmod_dir == NULL) {
      if (uname(&un)) goto on_error_0;
      snprintf(kpath, sizeof(kpath), "/lib/modules/%s/dance/%s.ko", un.release, kname);
   } else {
      snprintf(kpath, sizeof(kpath), "%s/%s.ko", kmod_dir, kname);
   }
   kpath[sizeof(kpath) - 1] = 0;

   if (stat(kpath, &st)) goto on_error_0;

   return exec_insmod(kpath);

 on_error_0:
   return -1;
}

__attribute__((unused))
static int unload_kmod(const char* kname) {
   return exec_rmmod(kname);
}

int uirq_init_lib_with_dir(const char* dir_path, const char* kmod_dir) {
   static const int dev_major = 0xeb0;
   static const int dev_minor = 1;

   dev_t dev;
   size_t len;

   /* ok to fail */

   load_kmod("uirq", kmod_dir);

   /* create dev node if does not exists */

   len = strlen(dir_path);
   if (len >= sizeof(uirq_dir_path)) {
      UIRQ_PERROR();
      goto on_error_0;
   }

#define UIRQ_SUFFIX "/uirq"
   if ((len + sizeof(UIRQ_SUFFIX) - 1) >= sizeof(uirq_dev_path)) {
      UIRQ_PERROR();
      goto on_error_0;
   }

   strcpy(uirq_dir_path, dir_path);
   memcpy(uirq_dev_path, dir_path, len);
   strcpy(uirq_dev_path + len, UIRQ_SUFFIX);

   errno = 0;
   if (mkdir(dir_path, S_IRUSR | S_IWUSR | S_IXUSR)) {
      if (errno != EEXIST) {
         UIRQ_PERROR();
         goto on_error_0;
      }
   }

   errno = 0;
   dev = makedev(dev_major, dev_minor);
   if (mknod(uirq_dev_path, S_IRUSR | S_IWUSR | S_IFCHR, dev)) {
      if (errno != EEXIST) {
         UIRQ_PERROR();
         goto on_error_1;
      }
   }
   /* success */
   return 0;

 on_error_1:
   rmdir(uirq_dir_path);
 on_error_0:
   return -1;
}

int uirq_init_lib(void) {
   return uirq_init_lib_with_dir("/dev/dance", NULL);
}

int uirq_fini_lib(void) {
   /* assume irq_init success */

   unlink(uirq_dev_path);

#if 0
   /* FIXME: should unload, but IRQ wont work at reload */
   unload_kmod("uirq");
#endif

   return 0;
}

int uirq_init(uirq_handle_t* uirq, const char* pci_id) {
   if (pci_id == NULL) { // typically pci_id should be "10ee:eb01"
      UIRQ_PERROR();
      return -1;
   } else {
      uirq->pci_id = pci_id;
      return 0;
   }
}

int uirq_open(uirq_handle_t* uirq) {
   uint32_t x;

   if (uirq->pci_id == NULL) {
      UIRQ_PERROR();
      goto on_error_0;
   }

   uirq->ebm0_bar = epci_open(uirq->pci_id, NULL, 0);
   if (uirq->ebm0_bar == EPCI_BAD_HANDLE) {
      UIRQ_PERROR();
      goto on_error_0;
   }

   uirq->dev_fd = open(uirq_dev_path, O_RDWR);
   if (uirq->dev_fd == -1) {
      UIRQ_PERROR();
      goto on_error_1;
   }

   uirq->use_ebm0_stat1 = 0;
   uirq->irq_mask = 0;
   uirq->isr_count = 0;

   /* enable ebone slave and ebft irqs */
   /* ebm0 documentation: ebm0_pcie_a.pdf */

   /* control register 0 */
   /* ebft interrupt enable (bit 8) */
   /* ebone slave interrupt enable (bit 9) */
   /* global interrupt enable (bit 31) */
   /* which slave triggers an interrupt can be known */
   /* using status register 1 (offset 0x14) */

   epci_rd32_reg(uirq->ebm0_bar, 0x0, &x);
   x |= (1 << 31) | (1 << 9) | (1 << 8);
   epci_wr32_reg(uirq->ebm0_bar, 0x0, x);

   /* success */
   return 0;

 on_error_1:
   epci_close(uirq->ebm0_bar);
 on_error_0:
   return -1;
}

int uirq_close(uirq_handle_t* uirq) {
   epci_close(uirq->ebm0_bar);
   close(uirq->dev_fd);
   return 0;
}

static int set_mask(uirq_handle_t* uirq, uint32_t m, unsigned int use_ebm0_stat1) {
   /* if use_ebm0_stat1 = 1, ebm0_stat1 is used in */
   /* the interrupt handler to retrieve the interrupt */
   /* source and subsequently wake the process. */
   /* otherwise, ebm0_stat1 is not used and  a process */
   /* is notified regardless the source. */

   uirq_ioctl_irqmask_t irqmask;

   irqmask.mask = m;
   irqmask.use_ebm0_stat1 = use_ebm0_stat1;
   if (ioctl(uirq->dev_fd, UIRQ_IOCTL_SET_IRQMASK, &irqmask)) {
      UIRQ_PERROR();
      return -1;
   }
   uirq->irq_mask = m;
   uirq->use_ebm0_stat1 = use_ebm0_stat1;
   return 0;
}

int uirq_set_mask(uirq_handle_t* uirq, uint32_t m, unsigned int use_ebm0_stat1) {
   return set_mask(uirq, uirq->irq_mask | m, 1);
}

int uirq_clear_mask(uirq_handle_t* uirq, uint32_t m) {
   return set_mask(uirq, uirq->irq_mask & ~m, 1);
}

int uirq_poll(uirq_handle_t* uirq, uint32_t* mask) {
   uirq_ioctl_irqcontext_t c;

   if (ioctl(uirq->dev_fd, UIRQ_IOCTL_GET_IRQCONTEXT, &c)) {
      UIRQ_PERROR();
      return -1;
   }

   *mask = c.ebm0_stat1 & uirq->irq_mask;

   return 0;
}

static void ms_to_timeval(int ms, struct timeval* tv) {
   const unsigned int us = (unsigned int)ms * 1000;
   tv->tv_sec = us / 1000000;
   tv->tv_usec = us % 1000000;
}

int uirq_wait(uirq_handle_t* uirq, unsigned int ms, uint32_t* mask) {
   const int fd = uirq->dev_fd;

   fd_set fds;
   struct timeval to;
   struct timeval* to_pointer;
   int err;

   /* FIXME: LINUX updates to_pointer contents, but not portable */
   to_pointer = NULL;
   if (ms != (unsigned int)-1) {
      ms_to_timeval(ms, &to);
      to_pointer = &to;
   }

   for (*mask = 0; *mask == 0; ) {
      FD_ZERO(&fds);
      FD_SET(fd, &fds);

      err = select(fd + 1,  &fds, NULL, NULL, to_pointer);
      if (err < 0) {
         if ((errno != EINTR) && (errno != EAGAIN)) {
            UIRQ_PERROR();
            return -1;
         }
      } else if (err == 0) {
         /* timeout */
         UIRQ_PERROR();
         return -2;
      }

      if (uirq_poll(uirq, mask)) {
         UIRQ_PERROR();
         return -1;
      }
   }
   return 0;
}

int uirq_register_isr(uirq_handle_t* uirq, uint32_t mask, void (*fn)(void*), void* data) {
   const size_t i = uirq->isr_count;

#define UIRQ_ISR_MAX_COUNT 8
   if (i >= UIRQ_ISR_MAX_COUNT) return -1;

   uirq->isr[i].fn = fn;
   uirq->isr[i].data = data;
   uirq->isr[i].mask = mask;

   if (uirq_set_mask(uirq, mask, 1)) {
      UIRQ_PERROR();
      return -1;
   }

   ++uirq->isr_count;

   return 0;
}

int uirq_unregister_isr(uirq_handle_t* uirq, uint32_t mask) {
   /* TODO: vector slot not deallocated */

   size_t i;

   for (i = 0; i != uirq->isr_count; ++i) {
      if ((uirq->isr[i].mask & mask) == 0) continue ;
      uirq_clear_mask(uirq, mask);
      uirq->isr[i].mask = 0;
   }
   return 0;
}

int uirq_handle_irq(uirq_handle_t* uirq) {
   size_t i;
   uint32_t mask;

   /* retrieve mask indicating EBONE IRQs */

   if (uirq_poll(uirq, &mask)) {
      UIRQ_PERROR();
      return -1;
   }

   /* execute isr that register for the mask */

   for (i = 0; mask && (i != uirq->isr_count); ++i) {
      if ((uirq->isr[i].mask & mask) == 0) continue ;
      uirq->isr[i].fn(uirq->isr[i].data);
      mask &= ~uirq->isr[i].mask;
   }
   return 0;
}
