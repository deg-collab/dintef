#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <linux/reboot.h>
#include <sys/reboot.h>
#include <sys/time.h>
#include "libefspi.h"
#include "libepci.h"

typedef struct cmdline_info
{
#define CMDLINE_FLAG_IS_READ           (1 << 0)
#define CMDLINE_FLAG_IS_SEBONE         (1 << 1)
#define CMDLINE_FLAG_IS_PATH           (1 << 2)
#define CMDLINE_FLAG_IS_OFFSET         (1 << 3)
#define CMDLINE_FLAG_IS_HEADER         (1 << 4)
#define CMDLINE_FLAG_IS_SIZE           (1 << 5)
#define CMDLINE_FLAG_IS_ERASE          (1 << 6)
#define CMDLINE_FLAG_IS_SEBONE_SEG     (1 << 7)
#define CMDLINE_FLAG_IS_SEBONE_OFF     (1 << 8)
#define CMDLINE_FLAG_IS_SEBONE_DLY     (1 << 9)
#define CMDLINE_FLAG_IS_REBOOT_CPU     (1 << 10)
#define CMDLINE_FLAG_IS_RELOAD_ICAP    (1 << 11)
#define CMDLINE_FLAG_IS_RESET_CPLD     (1 << 12)
#define CMDLINE_FLAG_IS_TIME           (1 << 13)
#define CMDLINE_FLAG_IS_PCI_ID         (1 << 14)
#define CMDLINE_FLAG_IS_HEADER_DANCE   (1 << 15)
#define CMDLINE_FLAG_IS_READ_ID        (1 << 16)
#define CMDLINE_FLAG_EFSPI_COMP_V23    (1 << 17)
#define CMDLINE_FLAG_EXTSEG_ADDR       (1 << 18)
  unsigned int flags;
  const char* path;
  size_t off;
  size_t size;
  size_t sebone_seg;
  size_t sebone_off;
  unsigned int sebone_delay;
  const char* pci_id;
  size_t extseg_addr;
} cmdline_info_t;


const char app_ident[] =
#include "_idfile.h"
;

int debug = 0;
#define LOG(...)                         \
do {                                     \
  if(debug) {                            \
    printf("EFSPI: "__VA_ARGS__);        \
  }                                      \
} while (0)

#define LOG_ERROR(...)                   \
do {                                     \
  printf("EFSPI ERROR: "__VA_ARGS__);\
} while (0)

static void do_usage(const char* s)
{
   printf("================\n%s\n", app_ident);
   printf("================\n");
   printf("command line options:\n");
   printf("-a the action:\n");
   printf(" erase\n");
   printf(" read\n");
   printf(" read_id\n");
   printf(" write\n");
   printf(" reload_icap: reload fpga flash using ICAP module\n");
   printf(" reset_cpld: reset the CPU using CPLD\n");
   printf(" reboot_cpu: reboot the CPU using software method\n");
   printf(" time: print operation completion time\n");
   printf("-f the filename, must be a bitstream\n");
   printf("-m the mode: local or sebone (default to local)\n");
   printf("-sebone_seg: <segment> to locate ebs_sebXX (default to 3)\n");
   printf("-sebone_off: <offset> to locate ebs_sebXX (default to 0)\n");
   printf("-sebone_delay: <time_us> sebone transaction delay in us (default to 0 us)\n");
   printf("-fspi_comp: <version> 2.3 or last. Activate compatibility for an ebs_fspi version (default is last)\n");
   printf("-o the offset, in bytes: <offset> or header or header_dance (default to 0)\n");
   printf("-s the size, in bytes (optional)\n");
   printf("-pci_id: <xxxx:xxxx> the PCI device id (optional)\n");
   printf("-extseg_addr <segment> : use extended address and select the segment id (optional)\n");
   printf("-d [-d] increase debug print out(optional)\n");
   printf("examples:\n");
   printf("write a bitstream: %s -a write -f file.bit -d\n", s);
   printf("write and reload fpga: %s -a write -f file.bit -a reload_icap\n", s);
   printf("write and reboot cpu: %s -a write -f file.bit -a reboot_cpu\n", s);
   printf("read the header: %s -a read -o header\n", s);
}


pthread_t     dot_thread;
int           dot_alive;

void *dot_loop(void *notused)
{
 int i;
 for(i = 0; dot_alive; i++)
 {
  if(i >= 60)
  {
    printf("\r%-60s\r", "");
    i = 0;
  }
  printf(".");
  fflush(stdout);
  sleep(1);
 }

 pthread_exit(NULL);
}

void dot_start(void)
{
  /* do not mess with debug messages */
  if(debug)
    return;

  dot_alive = 1;
  pthread_create(&dot_thread, NULL, dot_loop, NULL);
  pthread_detach(dot_thread);
}

void dot_end(void)
{
  if(debug)
    return;
  dot_alive = 0;
  pthread_join(dot_thread, NULL);
  printf("\r%-60s\r", "");
  fflush(stdout);
}





static int do_erase(efspi_handle_t* efspi, const cmdline_info_t* info)
{
  if (info->flags & (CMDLINE_FLAG_IS_OFFSET | CMDLINE_FLAG_IS_SIZE))
  {
    /* erase sector */

    size_t off;
    size_t size;

    off = 0;
    if (info->flags & CMDLINE_FLAG_IS_OFFSET) off = info->off;

    size = 0;
    if (info->flags & CMDLINE_FLAG_IS_SIZE) size = info->size;

    if (efspi_erase_sector(efspi, off, size))
    {
      LOG_ERROR("unable to erase memory in sector mode\n");
      return -1;
    }
  }
  else
  {
    /* erase bulk */
    if (efspi_erase_bulk(efspi))
    {
      LOG_ERROR("unable to erase memory in bulk mode\n");
      return -1;
    }
  }

  return 0;
}

static int do_read(efspi_handle_t* efspi, const cmdline_info_t* info)
{
   size_t i;
   size_t off;
   size_t size;
   uint8_t* buf;
   int err = -1;

   
   // Select the segment of extended address
   if (info->flags & CMDLINE_FLAG_EXTSEG_ADDR) {
      if(enable_rdextaddr(efspi, info->extseg_addr))
         goto on_error_0;
   }
   if (info->flags & CMDLINE_FLAG_IS_OFFSET)
   {
      if (info->flags & CMDLINE_FLAG_IS_HEADER)
         efspi_get_last_sector_offset(efspi, &off);
      else
         off = info->off;
   }
   else
   {
      off = 0;
   }

   if (info->flags & CMDLINE_FLAG_IS_SIZE)
   {
      size = info->size;
   }
   else if (info->flags & CMDLINE_FLAG_IS_HEADER)
   {
      size = efspi_get_page_size(efspi) * 2;
   }
   else
   {
      LOG_ERROR("no read size specified\n");
      goto on_error_0;
   }

   buf = malloc(size);
   if (buf == NULL)
   {
      LOG_ERROR("unable to allocate %zubytes\n", size);
      goto on_error_0;
   }

   if (efspi_read_buf(efspi, buf, off, size))
   {
      const unsigned int xoff = (unsigned int)off;
      const unsigned int xsize = (unsigned int)size;
      LOG_ERROR("efspi_read_buf(0x%x, 0x%x) fails\n", xoff, xsize);
      goto on_error_1;
   }

   printf("\n");
   if (info->flags & CMDLINE_FLAG_IS_HEADER)
   {
      int hdrsize;
#define BUFFER_SIZE 512
      uint8_t outbuf[BUFFER_SIZE];
      hdrsize = get_pkgheader(outbuf, buf, BUFFER_SIZE, size);

      if (info->flags & CMDLINE_FLAG_IS_HEADER_DANCE) {
         printf("%s", outbuf + HEADER_FIELDINFO_SIZE);
      }
      else {
         /* printable string */
         for (i = hdrsize; i < size; ++i)
         {
             if (isprint(buf[i]))
               printf("%c", buf[i]);
         }
      }
   }
   else
   {
      /* dump hexa */
      for (i = 0; i < size; ++i)
      {
         if ((i % 16) == 0)
         {
            const unsigned int xoffi = off + i;
            printf("\n0x%08x: ", xoffi);
         }
         printf("%02x", buf[i]);
      }
   }
   printf("\n");

   /* success */
   err = 0;

   on_error_1:
   free(buf);
   on_error_0:
   return err;
#undef BUFFER_SIZE
}

static int do_read_id(efspi_handle_t* efspi, const cmdline_info_t* info)
{
   int err = -1;
   uint32_t x;

   efspi_read_id(efspi, &x);
   printf("memory ids: 0x%08x\n", x);

   /* success */
   err = 0;

   return err;
}

static int do_write(efspi_handle_t* efspi, const cmdline_info_t* info)
{
  int err = -1;

   // Select the segment of extended address
   if (info->flags & CMDLINE_FLAG_EXTSEG_ADDR) {
      if (enable_wrextaddr(efspi, info->extseg_addr))
         goto on_error_0;
   }

   if (info->flags & CMDLINE_FLAG_IS_PATH)
   {
      if (efspi_erase_write_bit(efspi, info->path))
      {
         LOG_ERROR("unable to program memory with bit file: %s\n", info->path);
         goto on_error_0;
      }

      err = 0;
   }
   else
   {
      /* write a contiguous pattern from off to size */
      /* assume the memory previously erased */

      const size_t page_size = efspi_get_page_size(efspi);

      uint8_t* buf;
      size_t i;
      size_t off;
      size_t size;

      off = 0;
      if (info->flags & CMDLINE_FLAG_IS_OFFSET) off = info->off;

      size = 0;
      if (info->flags & CMDLINE_FLAG_IS_SIZE) size = info->size;

      buf = malloc(page_size * sizeof(uint8_t));
      if (buf == NULL)
      {
         LOG_ERROR("unable to allocate %dbytes\n", page_size * sizeof(uint8_t));
         goto on_error_0;
      }

      for (i = 0; i != page_size; ++i) buf[i] = (uint8_t)i;

      for (i = 0; i < size; i += page_size, off += page_size)
      {
         if (efspi_write_buf(efspi, buf, off, page_size))
         {
      LOG_ERROR("unable to write memory at sector %zu\n", off);
      goto on_error_1;
         }
      }

      err = 0;

   on_error_1:
      free(buf);
   }

   on_error_0:
      return err;
}

static void on_write(size_t i, size_t n)
{
  LOG("writing page %zu on %zu\r", i, n);
  fflush(stdout);
}

static void on_endwr(void)
{
  printf("\r%-60s\r", "");
}

static void on_erase(void)
{
  LOG("erasing memory (2mn)...\n");
}

static int get_size_t(const char* s, size_t* x)
{
  int base = 10;
  if ((strlen(s) > 2) && (s[0] == '0') && (s[1] == 'x')) base = 16;
  *x = strtoul(s, NULL, base);
  return 0;
}

static void sync_reboot_cpu(void)
{
  sync();
  reboot(LINUX_REBOOT_CMD_RESTART);
}

static void reload_icap(void)
{
  /* tell the fpga to reload its bitstream stored in flash */
  /* a cpu reboot may be required to renegotiate with the endpoint */
  /* a cpu reboot may automatically occur, cf hardware documentation */

  epcihandle_t bar;

  bar = epci_open("10ee:eb01", NULL, 0);
  if (bar == EPCI_BAD_HANDLE) return ;

  /* write magic to self clear register */
  epci_wr32_reg(bar, 0x140 + 0x0c, 0x5599aa66);

  epci_close(bar);
}

static void reset_cpld(void)
{
  static const uint32_t ctrl_off = 0x00;
  epcihandle_t bar;

  /* it reboots cpu, sync */
  sync();

  bar = epci_open("10ee:eb01", NULL, 0);
  if (bar == EPCI_BAD_HANDLE) return ;

  epci_wr32_reg(bar, ctrl_off + 0x0c, 0x80000000);
  epci_wr32_reg(bar, ctrl_off + 0x08, 0xeb0110ee);

  epci_close(bar);
}

int main(int ac, const char** av)
{
   efspi_handle_t efspi;
   efspi_desc_t desc;
   cmdline_info_t info;
   unsigned int is_action;
   struct timeval tm_start;
   struct timeval tm_stop;
   struct timeval tm_diff;
   size_t i;
   int err = -1;

   is_action = 0;
   info.flags = 0;

   /* count debug options at the end of argins */
   for (i = (ac-1); i > 0; i--)
   {
      if (strcmp(av[i], "-d") == 0)
      {
        debug++;
        ac--;
      }
      else
        break ;
   }

   /* get command line info */
   if ((ac & 1) == 0)
   {
      printf("invalid command line count\n");
      goto on_invalid_cmdline;
   }

   for (i = 1; i < ac; i += 2)
   {
      const char* const key = av[i + 0];
      const char* const val = av[i + 1];

      if (strcmp(key, "-a") == 0)
      {
         if (strcmp(val, "time") == 0)
         {
            info.flags |= CMDLINE_FLAG_IS_TIME;
         }
         else if (strcmp(val, "reboot_cpu") == 0)
         {
            info.flags |= CMDLINE_FLAG_IS_REBOOT_CPU;
         }
         else if (strcmp(val, "reload_icap") == 0)
         {
            info.flags |= CMDLINE_FLAG_IS_RELOAD_ICAP;
         }
         else if (strcmp(val, "reset_cpld") == 0)
         {
            info.flags |= CMDLINE_FLAG_IS_RESET_CPLD;
         }
            else if (strcmp(val, "read") == 0)
         {
            info.flags |= CMDLINE_FLAG_IS_READ;
         }
         else if (strcmp(val, "read_id") == 0)
         {
            info.flags |= CMDLINE_FLAG_IS_READ_ID;
         }
         else if (strcmp(val, "erase") == 0)
         {
            info.flags |= CMDLINE_FLAG_IS_ERASE;
         }
         else if (strcmp(val, "write"))
         {
            printf("invalid action: %s\n", val);
            goto on_invalid_cmdline;
         }
         is_action = 1;
      }
      else if (strcmp(key, "-f") == 0)
      {
         info.path = val;
         info.flags |= CMDLINE_FLAG_IS_PATH;
      }
      else if (strcmp(key, "-m") == 0)
      {
         if (strcmp(val, "sebone") == 0)
         {
            info.flags |= CMDLINE_FLAG_IS_SEBONE;
         }
         else if (strcmp(key, "local"))
         {
            printf("invalid mode: %s\n", val);
            goto on_invalid_cmdline;
         }
      }
      else if (strcmp(key, "-sebone_seg") == 0)
      {
         if (get_size_t(val, &info.sebone_seg))
         {
            printf("invalid segment: %s\n", val);
            goto on_invalid_cmdline;
         }
         info.flags |= CMDLINE_FLAG_IS_SEBONE_SEG;
      }
      else if (strcmp(key, "-sebone_off") == 0)
      {
         if (get_size_t(val, &info.sebone_off))
         {
            printf("invalid offset: %s\n", val);
            goto on_invalid_cmdline;
         }
         info.flags |= CMDLINE_FLAG_IS_SEBONE_OFF;
      }
      else if (strcmp(key, "-sebone_delay") == 0)
      {
         info.flags |= CMDLINE_FLAG_IS_SEBONE_DLY;
         info.sebone_delay = strtoul(val, NULL, 10);
      }
      else if (strcmp(key, "-fspi_comp") == 0)
      {
         if (strcmp(val, "2.3") == 0)
            info.flags |= CMDLINE_FLAG_EFSPI_COMP_V23;
      }
      else if (strcmp(key, "-o") == 0)
      {
         if (strcmp(val, "header") == 0)
         {
            info.flags |= CMDLINE_FLAG_IS_HEADER;
         }
         else if (strcmp(val, "header_dance") == 0)
         {
            info.flags |= CMDLINE_FLAG_IS_HEADER_DANCE | CMDLINE_FLAG_IS_HEADER;
         }
         else if (get_size_t(val, &info.off))
         {
            printf("invalid offset: %s\n", val);
            goto on_invalid_cmdline;
         }
         info.flags |= CMDLINE_FLAG_IS_OFFSET;
      }
      else if (strcmp(av[i], "-s") == 0)
      {
         if (get_size_t(val, &info.size))
         {
            printf("invalid size: %s\n", val);
            goto on_invalid_cmdline;
         }
         info.flags |= CMDLINE_FLAG_IS_SIZE;
      }
      else if (strcmp(key, "-extseg_addr") == 0) {
         if (get_size_t(val, &info.extseg_addr))
         {
            printf("invalid offset: %s\n", val);
            goto on_invalid_cmdline;
         } else
            info.flags |= CMDLINE_FLAG_EXTSEG_ADDR;
      }
      else if (strcmp(key, "-pci_id") == 0)
      {
         info.pci_id = val;
         info.flags |= CMDLINE_FLAG_IS_PCI_ID;
      }
      else
      {
         printf("invalid option: %s\n", key);
         goto on_invalid_cmdline;
      }
   }

   if (is_action == 0)
   {
      on_invalid_cmdline:
      do_usage(av[0]);
      goto on_error_0;
   }

   if (info.flags & CMDLINE_FLAG_IS_TIME)
   {
      gettimeofday(&tm_start, NULL);
   }

   /* open efspi */
   LOG("grant access to SPI via PCIe\n");
   if (efspi_init_lib())
   {
      LOG_ERROR("unable to initialize libefspi\n");
      goto on_error_0;
   }

   /* need two -d to get lib loggin */
   if (debug > 1)
   {
      if (efspi_log_lib(1))
      {
         LOG_ERROR("invalid debug level\n");
         goto on_error_0;
      }
   }

   efspi_init_desc(&desc);
   desc.erase_fn = on_erase;
   desc.write_fn = on_write;
   desc.endwr_fn = on_endwr;

   if (info.flags & CMDLINE_FLAG_IS_PCI_ID)
   {
      desc.pci_id = info.pci_id;
   }

   /* default sebone location */
   if (info.flags & CMDLINE_FLAG_IS_SEBONE)
   {
      desc.flags |= EFSPI_FLAG_IS_SEBONE;

      desc.pci_bar = 3;
      if (info.flags & CMDLINE_FLAG_IS_SEBONE_SEG)
         desc.pci_bar = info.sebone_seg;

      desc.pci_off = 0x0000;
      if (info.flags & CMDLINE_FLAG_IS_SEBONE_OFF)
      {
         /* ebone offset to host translation */
         desc.pci_off = info.sebone_off * sizeof(uint32_t);
      }

      if (info.flags & CMDLINE_FLAG_IS_SEBONE_DLY)
      {
         desc.sebone_delay = info.sebone_delay;
      }
   }

   if (info.flags & CMDLINE_FLAG_EFSPI_COMP_V23)
      desc.flags |= EFSPI_FLAG_COMP_V23;

   if (efspi_open(&efspi, &desc))
   {
      LOG_ERROR("unable to access SPI through PCIe\n");
      goto on_error_1;
   }

   /* TODO: initialize sebone */

   if (info.flags & CMDLINE_FLAG_IS_READ)
   {
      LOG("about to read memory...\n");
      if (do_read(&efspi, &info))
         goto on_error_2;
      LOG("read done\n");
   }
   else if (info.flags & CMDLINE_FLAG_IS_READ_ID)
   {
      LOG("about to read memory id...\n");
      if (do_read_id(&efspi, &info))
         goto on_error_2;
      LOG("read done\n");
   }
   else if (info.flags & CMDLINE_FLAG_IS_ERASE)
   {
      LOG("about to erase memory...\n");
      dot_start();
      err = do_erase(&efspi, &info);
      dot_end();
      if (err)
         goto on_error_2;
      LOG("erase done\n");
   }
   else
   {
      LOG("about to write memory...\n");
      dot_start();
      err = do_write(&efspi, &info);
      dot_end();
      if (err)
         goto on_error_2;
      LOG("write done\n");
   }

   /* success */
   err = 0;

   on_error_2:
      LOG("closing access to SPI via PCIe\n");
      efspi_close(&efspi);
   on_error_1:
      efspi_fini_lib();
   on_error_0:
      if (err == 0)
      {
         if (info.flags & CMDLINE_FLAG_IS_TIME)
         {
            gettimeofday(&tm_stop, NULL);
            timersub(&tm_stop, &tm_start, &tm_diff);
            printf("time (in seconds): %lu\n", tm_diff.tv_sec);
         }

         if (info.flags & CMDLINE_FLAG_IS_RESET_CPLD) reset_cpld();
         if (info.flags & CMDLINE_FLAG_IS_RELOAD_ICAP) reload_icap();
         if (info.flags & CMDLINE_FLAG_IS_REBOOT_CPU) sync_reboot_cpu();
      }

   if (!(info.flags & CMDLINE_FLAG_IS_READ) && 
       !(info.flags & CMDLINE_FLAG_IS_READ_ID))
   {
      if (err) printf("failure\n");
      else printf("success\n");
   }
   return err;
}
