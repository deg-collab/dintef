#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "libepci.h"
#include "libefspi.h"


/* datasheets */
/* SPANSION_S25FL064P: S25FL064P_00.pdf */
/* NUMONYX_N25Q128: N25Q_128_3_Volt_with_boot_sector.pdf */

/* vendor, device ids */

#define CONCAT_VID_PID(__vid, __pid) (((__vid) << 16) | (__pid))
/* dcore */
#define SPANSION_S25FL064P_ID CONCAT_VID_PID(0x01, 0x0216)
/* dub */
#define NUMONYX_M25P128_ID CONCAT_VID_PID(0x20, 0x2018)
/* kc705 */
#define NUMONYX_N25Q128_ID CONCAT_VID_PID(0x20, 0xba18)
/* ac701 */
#define NUMONYX_N25Q256_ID CONCAT_VID_PID(0x20, 0xba19)
/* smartpix frontend  */
#define SPANSION_S25FL256S_ID CONCAT_VID_PID(0x01, 0x0219)
/* smartpix backend, techway */
#define SPANSION_S25FL512S_ID CONCAT_VID_PID(0x01, 0x0220)
/* smartpix backend, ALVEO */
#define MICRON_MT25QU01GBBB_ID CONCAT_VID_PID(0x20, 0xbb21)
/* tiger */
#define MACRONIX_MX25L6435E_ID CONCAT_VID_PID(0xc2, 0x2017)
/* HSM */
#define MACRONIX_MX25L12835F_ID CONCAT_VID_PID(0xc2, 0x2018)
/* DBLOCK C */
#define MACRONIX_MX25L25635F_ID CONCAT_VID_PID(0xc2, 0x2019)

int libefspi_debug = 0;
#define LOG(...)                            \
do {                                        \
  if(libefspi_debug) {                      \
    printf("    LIBEFSPI: "__VA_ARGS__);    \
  }                                         \
} while (0)

#define LOG_ERROR(...)                      \
do {                                        \
    printf("LIBEFSPI ERROR: "__VA_ARGS__);  \
} while (0)

#define PERROR() \
printf("[!] %s:%d\n", __FILE__, __LINE__)
#define ASSUME(__x) \
do { if (!(__x)) printf("[!] %s:%d\n", __FILE__, __LINE__); } while (0)

#define MAX_WAIT_LOOP 6000

/* register offsets from ebs_fspi.pdf */

#define EFSPI_OFF_WDAT 0x00
#define EFSPI_OFF_COUNT 0x04
#define EFSPI_OFF_INSN 0x08
/* 0x0c unused */
#define EFSPI_OFF_EDGE 0x10
#define EFSPI_OFF_STAT 0x14
#define EFSPI_OFF_RDAT 0x18
/* 0x1c unused */

/* status masks */
#define EFSPI_MASK_SER_STOPPED (1 << 30)
#define EFSPI_MASK_EDGE_COUNT (1 << 31)

/* register access routines */

static void fixme_sleep_sebone(efspi_handle_t* efspi)
{
   if ((efspi->flags & EFSPI_FLAG_IS_SEBONE)
         && (efspi->sebone_delay > 0)) {
         usleep(efspi->sebone_delay);
   }
}

static void read_stat_reg(efspi_handle_t* efspi, uint32_t* x)
{
  const size_t off = efspi->pci_off + EFSPI_OFF_STAT;
  epci_rd32_reg(efspi->pci_bar, off, x);
  fixme_sleep_sebone(efspi);
}

static void wait_ser(efspi_handle_t* efspi)
{
   // wait serialization done
   if (efspi->flags & EFSPI_FLAG_COMP_V23) {
      // tempo for bugs in ebs_fspi <= 2.3
      usleep(10);
   } else {
      int idx = 0;
      uint32_t x;

      for(idx = 0; idx < MAX_WAIT_LOOP; idx++)
      {
         read_stat_reg(efspi, &x);
         if (x & EFSPI_MASK_SER_STOPPED) break;
      }
   }
}

static void write_wdat_reg(efspi_handle_t* efspi, uint32_t x)
{
  const size_t off = efspi->pci_off + EFSPI_OFF_WDAT;
  epci_wr32_reg(efspi->pci_bar, off, x);
  fixme_sleep_sebone(efspi);
}

static void write_count_reg(efspi_handle_t* efspi, uint32_t x)
{
  const size_t off = efspi->pci_off + EFSPI_OFF_COUNT;
  epci_wr32_reg(efspi->pci_bar, off, x);
  fixme_sleep_sebone(efspi);
}

static void write_insn_reg(efspi_handle_t* efspi, uint32_t x)
{
  const size_t off = efspi->pci_off + EFSPI_OFF_INSN;
  epci_wr32_reg(efspi->pci_bar, off, x);
  fixme_sleep_sebone(efspi);
}

static void read_edge_reg(efspi_handle_t* efspi, uint32_t* x)
{
  const size_t off = efspi->pci_off + EFSPI_OFF_EDGE;
  epci_rd32_reg(efspi->pci_bar, off, x);
  fixme_sleep_sebone(efspi);
}

static void read_rdat_reg(efspi_handle_t* efspi, uint32_t* x)
{
  const size_t off = efspi->pci_off + EFSPI_OFF_RDAT;
  epci_rd32_reg(efspi->pci_bar, off, x);
  fixme_sleep_sebone(efspi);
}


/* register based functionnality routines */

static uint32_t get_status(efspi_handle_t* efspi)
{
   /* NUMONYX_N25Q128, 9.1.22 */

   uint32_t x;

   write_count_reg(efspi, 0x00000001);
   write_insn_reg(efspi, 0x00000010);
   write_wdat_reg(efspi, 0x05000000);

   if (efspi->flags & EFSPI_FLAG_COMP_V23) {
      usleep(100);
   }

   wait_ser(efspi);

   read_rdat_reg(efspi, &x);

   return x;
}

static void wait_status_wip(efspi_handle_t* efspi)
{
  /* wait for a write, erase or program cycle to end */
  while (get_status(efspi) & (1 << 0)) ;
}

#if 0 /* do not work */
static void wait_edge(efspi_handle_t* efspi)
{
  /* wait until edge valid */

  uint32_t x;

  while (1)
  {
    read_stat_reg(efspi, &x);
    if (x & EFSPI_MASK_EDGE_COUNT) break;
  }
}
#endif

static void wait_edge_count(efspi_handle_t* efspi, uint32_t count)
{
  uint32_t x;
  int idx = 0;

  for(idx = 0; idx < MAX_WAIT_LOOP; idx++)
   {
      read_edge_reg(efspi, &x);
      if (x == count) break ;
   }
}

static void enable_write(efspi_handle_t* efspi)
{
  /* wren instruction */
  /* NUMONYX_N25Q128, 9.1.9 */

  write_count_reg(efspi, 0x00000001);
  write_insn_reg(efspi, 0x00000008);
  write_wdat_reg(efspi, 0x06000000);

   if (efspi->flags & EFSPI_FLAG_COMP_V23)
      usleep(500);

  wait_ser(efspi);
}

static void enable_quadspi(efspi_handle_t* efspi)
{
   LOG("enabling SPIx4\n");


   switch (efspi->vd_id)
   {
   case MACRONIX_MX25L6435E_ID:
   case MACRONIX_MX25L12835F_ID:
   case MACRONIX_MX25L25635F_ID:
   case MICRON_MT25QU01GBBB_ID:
      enable_write(efspi);

      write_count_reg(efspi, 0x00000001);
      write_insn_reg(efspi, 0x00000010);
      write_wdat_reg(efspi, 0x01400000);

      wait_ser(efspi);
      wait_status_wip(efspi);
      break ;
   default:
      LOG("memory not supporting SPIx4\n");
      break ;
   }
}

efspi_err_t enable_wrextaddr(efspi_handle_t* efspi, size_t addrseg)
{
   uint32_t addr = (addrseg & 0x07) << 16;
   switch (efspi->vd_id)
   {
      case MICRON_MT25QU01GBBB_ID:
         LOG("enabling write extended address register to segment 0x%x\n", addrseg);
         enable_write(efspi);
         write_count_reg(efspi, 0x00000001);
         write_insn_reg(efspi, 0x00000008);
         write_wdat_reg(efspi, 0xC5000000 | addr);

         wait_ser(efspi);
         wait_status_wip(efspi);
         return EFSPI_ERR_SUCCESS;
         break ;
      default:
         LOG("memory not supporting read extended addresses\n");
         return EFSPI_ERR_FAILURE;
         break ;
      }
}

efspi_err_t enable_rdextaddr(efspi_handle_t* efspi, size_t addrseg)
{
   uint32_t addr = (addrseg & 0x07) << 16;

   switch (efspi->vd_id)
   {
      case MICRON_MT25QU01GBBB_ID:
         LOG("enabling read extended address register\n");
         enable_write(efspi);
         write_count_reg(efspi, 0x00000001);
         write_insn_reg(efspi, 0x00000010);
         write_wdat_reg(efspi, 0xC8000000 | addr);
         
         wait_ser(efspi);
         wait_status_wip(efspi);

         return EFSPI_ERR_SUCCESS;
         break ;
      default:
         LOG("memory not supporting read extended addresses\n");
         return EFSPI_ERR_FAILURE;
         break ;
   }
}

__attribute__((unused))
static void disable_write(efspi_handle_t* efspi)
{
  /* wrdi instruction */
  /* NUMONYX_N25Q128, 9.1.10 */

  write_count_reg(efspi, 0x00000001);
  write_insn_reg(efspi, 0x00000008);
  write_wdat_reg(efspi, 0x04000000);

  wait_ser(efspi);
}

static void erase_bulk(efspi_handle_t* efspi)
{
  /* memory bulk erase */

  enable_write(efspi);

  write_count_reg(efspi, 0x00000001);
  write_insn_reg(efspi, 0x00000008);
  write_wdat_reg(efspi, 0xc7000000);

  wait_ser(efspi);

  wait_status_wip(efspi);
}

static void erase_sector(efspi_handle_t* efspi, uint32_t addr)
{
  /* erase the sector at addr */
  /* NUMONYX_N25Q128, table 14 */

  enable_write(efspi);

  write_count_reg(efspi, 0x00000001);
  write_insn_reg(efspi, 0x00000020);
  write_wdat_reg(efspi, 0xd8000000 | addr);

  wait_ser(efspi);

  wait_status_wip(efspi);
}

static void set_page_program(efspi_handle_t* efspi, uint32_t addr)
{
  /* set address of the page to be written */
  /* NUMONYX_N25Q128, 9.1.11 */

  enable_write(efspi);

  write_count_reg(efspi, 0x00000041);
  write_insn_reg(efspi, 0x00000020);
  write_wdat_reg(efspi, 0x02000000 | addr);

  wait_ser(efspi);
}

static uint32_t get_vd_id(efspi_handle_t* efspi)
{
  /* rdid instruction */
  /* NUMONYX_N25Q128, 9.1.1 */

  uint32_t x;

  write_count_reg(efspi, 0x00000001);
  write_insn_reg(efspi, 0x00000020);
  write_wdat_reg(efspi, 0x9f000000);

  wait_ser(efspi);

  read_rdat_reg(efspi, &x);

  return x & 0x00ffffff;
}

efspi_err_t efspi_read_id(efspi_handle_t* efspi, uint32_t *x)
{
  *x = get_vd_id(efspi);
  return EFSPI_ERR_SUCCESS;
}


static uint32_t read_uint24(efspi_handle_t* efspi, uint32_t off)
{
  uint32_t x;

  write_count_reg(efspi, 0x00000001);
  write_insn_reg(efspi, 0x00000040);
  write_wdat_reg(efspi, 0x0b000000 | off);

  wait_ser(efspi);

  read_rdat_reg(efspi, &x);

  return x & 0x00ffffff;
}

static void read_uint32(efspi_handle_t* efspi, uint32_t off, uint8_t* p)
{
  const uint32_t hi = read_uint24(efspi, off + 0) << 8;
  const uint32_t lo = read_uint24(efspi, off + 1) << 0;
  const uint32_t x = hi | (lo & 0xff);

  p[0] = (uint8_t)(x >> 24);
  p[1] = (uint8_t)(x >> 16);
  p[2] = (uint8_t)(x >> 8);
  p[3] = (uint8_t)(x >> 0);
}

/* library constructors */

efspi_err_t efspi_init_lib(void)
{
  return EFSPI_ERR_SUCCESS;
}

efspi_err_t efspi_fini_lib(void)
{
  return EFSPI_ERR_SUCCESS;
}

efspi_err_t efspi_log_lib(int level)
{
  if(level >= 0)
  {
    libefspi_debug = level;
    return EFSPI_ERR_SUCCESS;
  }
  return EFSPI_ERR_FAILURE;
}




/* fixme: ebone must be reset before flash access */

static int reset_ebone(const char* pci_id, unsigned int is_sebone)
{
  static const size_t prb_off = 0x04;

  epcihandle_t bar0;
  uint32_t x;
  int err = -1;

  bar0 = epci_open((char*)pci_id, NULL, 0);
  if (bar0 == EPCI_BAD_HANDLE) goto on_error_0;

  epci_rd32_reg(bar0, prb_off, &x);
  usleep(100);

  if (is_sebone)
  {
    /* remote reset using serial ebone */
    /* disable broadcall */
    /* NOTE: this was used as broadcalls generate uneeded */
    /* traffic that may disturb or penalize the operation. */
    /* BUT it discards the ctl_off register, esp. IRQ settings */
    /* and may conflict with a concurrent DMA setting. Newer */
    /* designs disable broadcall forwarding, dixit CH. */
    /* static const size_t ctl_off = 0x00; */
    /* epci_wr32_reg(bar0, ctl_off, 0x00000000); */

#if 0 /* disabled, application specific */
    /* remote ebone reset */
    static const size_t ebs_off = 0x80;
    epcihandle_t bar1 = epci_open(pci_id, NULL, 1);
    epci_wr32_reg(bar1, ebs_off, 0x00000002);
    epci_close(bar1);
#endif
  }
  else
  {
    /* local ebone reset */

    /* write magic to self clear register */
    epci_wr32_reg(bar0, 0x140 + 0x0c, 1 << 31);
  }

  usleep(250000);

  err = 0;

  epci_close(bar0);
 on_error_0:
  return err;
}

/* open close efspi handle */
efspi_err_t efspi_open(efspi_handle_t* efspi, const efspi_desc_t* desc)
{
   /* fixme: ebone must be reset */
   efspi->flags = desc->flags;

   if (reset_ebone(desc->pci_id, efspi->flags & EFSPI_FLAG_IS_SEBONE))
   {
      LOG_ERROR("unable to reset EBONE\n");
      goto on_error_0;
   }

   /* open pci bar */
   efspi->pci_bar = epci_open((char*)desc->pci_id, NULL, desc->pci_bar);
   if (efspi->pci_bar == EPCI_BAD_HANDLE)
   {
      LOG_ERROR("unable to access PCI (Hint: FPGA loaded?)\n");
      goto on_error_0;
   }

   efspi->pci_off = desc->pci_off;
   efspi->sebone_delay = desc->sebone_delay;

   efspi->erase_fn = desc->erase_fn;
   efspi->write_fn = desc->write_fn;
   efspi->endwr_fn = desc->endwr_fn;

   /* hardware info */

   efspi->vd_id = get_vd_id(efspi);
   LOG("memory ids: 0x%08x\n", efspi->vd_id);
   switch (efspi->vd_id)
   {
   case SPANSION_S25FL064P_ID:
   case MACRONIX_MX25L6435E_ID:
      /* 64 Mbits */
      efspi->mem_size = (64 * 1024 * 1024) / 8;
      efspi->sector_size = 64 * 1024;
      break ;

   case NUMONYX_M25P128_ID:
   case NUMONYX_N25Q128_ID:
      /* 128 Mbits */
      efspi->mem_size = (128 * 1024 * 1024) / 8;
      efspi->sector_size = 64 * 1024;
      break ;

   case MACRONIX_MX25L12835F_ID:
      /* 128 Mbits */
      efspi->mem_size = (128 * 1024 * 1024) / 8;
      efspi->sector_size = 4 * 1024;
      break ;

   case NUMONYX_N25Q256_ID:
   case MACRONIX_MX25L25635F_ID:
      /* 256 Mbits */
   #if 0
      /* FIXME: offset encoded in 24 bits */
      efspi->mem_size = (256 * 1024 * 1024) / 8;
   #else
      efspi->mem_size = (128 * 1024 * 1024) / 8;
   #endif
      efspi->sector_size = 64 * 1024;
      break ;

   case SPANSION_S25FL256S_ID:
      /* 256 Mbits */
   #if 0
      /* FIXME: offset encoded in 24 bits */
      efspi->mem_size = (256 * 1024 * 1024) / 8;
   #else
      efspi->mem_size = (128 * 1024 * 1024) / 8;
   #endif
      /* WARNING: for the same memory id there are
         2 sector size, check the reference on the 
         memory case: R0==64K R1=256K */
      efspi->sector_size = 256 * 1024;
      efspi->sector_size = 64 * 1024;
      break ;

   case SPANSION_S25FL512S_ID:
      /* 512 Mbits */
   #if 0
      /* FIXME: offset encoded in 24 bits */
      efspi->mem_size = (512 * 1024 * 1024) / 8;
   #else
      efspi->mem_size = (128 * 1024 * 1024) / 8;
   #endif
      efspi->sector_size = 256 * 1024;
      break ;

   case MICRON_MT25QU01GBBB_ID:
      efspi->mem_size = (1024 * 1024 * 1024) / 8;
      efspi->sector_size = 64 * 1024;
      break;

   default:
      LOG_ERROR("unrecognized ids: 0x%08x\n", efspi->vd_id);
      goto on_error_1;
      break ;
   }

   #define EFSPI_PAGE_SIZE 256
   efspi->page_size = EFSPI_PAGE_SIZE;

   return EFSPI_ERR_SUCCESS;

   on_error_1:
      epci_close(efspi->pci_bar);
   on_error_0:
      return EFSPI_ERR_FAILURE;
}

efspi_err_t efspi_close(efspi_handle_t* efspi)
{
  epci_close(efspi->pci_bar);
  return EFSPI_ERR_SUCCESS;
}

/* access checking routines */

static size_t mod_page_size(const efspi_handle_t* efspi, size_t x)
{
  return x & (efspi->page_size - 1);
}

static size_t mod_sector_size(const efspi_handle_t* efspi, size_t x)
{
  return x & (efspi->sector_size - 1);
}

static uint32_t mod_uint32(uint32_t x)
{
  return x & (uint32_t)(sizeof(uint32_t) - 1);
}

static int check_access(const efspi_handle_t* efspi, size_t off, size_t size)
{
  /* underflow */
  if ((off + size) < off)
  {
    LOG_ERROR("invalid offset+size range\n");
    return -1;
  }

  /* overflow */
  if ((off + size) > efspi->mem_size)
  {
    LOG_ERROR("overflow on offset+size\n");
    return -1;
  }

  return 0;
}

__attribute__((unused))
static int check_read_access
(const efspi_handle_t* efspi, size_t off, size_t size)
{
  if (mod_uint32(off))
  {
    LOG_ERROR("invalid offset\n");
    return -1;
  }

  if (mod_uint32(size))
  {
    LOG_ERROR("invalid size value\n");
    return -1;
  }

  return check_access(efspi, off, size);
}

__attribute__((unused))
static int check_write_access
(const efspi_handle_t* efspi, size_t off, size_t size)
{
  if (mod_page_size(efspi, off))
  {
    LOG_ERROR("invalid offset\n");
    return -1;
  }

  if (mod_page_size(efspi, size))
  {
    LOG_ERROR("invalid size value\n");
    return -1;
  }
  return check_access(efspi, off, size);
}

static int check_erase_access
(const efspi_handle_t* efspi, size_t off, size_t size)
{
  if (mod_sector_size(efspi, off))
  {
    LOG_ERROR("invalid offset\n");
    return -1;
  }

  if (mod_sector_size(efspi, size))
  {
    LOG_ERROR("invalid size value\n");
    return -1;
  }

  return check_access(efspi, off, size);
}

/* erase the memory */

efspi_err_t efspi_erase_sector(efspi_handle_t* efspi, size_t off, size_t size)
{
  /* off the offset, in bytes */
  /* size the size, in bytes */

  uint32_t i;
  uint32_t j;

  if (check_erase_access(efspi, off, size))
  {
    return EFSPI_ERR_FAILURE;
  }

  i = (uint32_t)off;
  j = (uint32_t)(off + size);
  for (; i < j; i += efspi->sector_size) erase_sector(efspi, i);

  return EFSPI_ERR_SUCCESS;
}

efspi_err_t efspi_erase_bulk(efspi_handle_t* efspi)
{
  /* erase the whole memory using bulk mode */
  erase_bulk(efspi);
  return EFSPI_ERR_SUCCESS;
}


/* read write flash from to memory buffer */

static uint32_t get_swap_uint32(const uint8_t* p)
{
  /* get uint32 from memory. swap the bytes, not matter the architecture. */

  return
    (((uint32_t)p[0]) << 24) |
    (((uint32_t)p[1]) << 16) |
    (((uint32_t)p[2]) << 8) |
    (((uint32_t)p[3]) << 0);
}

efspi_err_t efspi_write_buf
(efspi_handle_t* efspi, const void* buf, size_t off, size_t size)
{
  /* off the offset, in bytes */
  /* size the size, in bytes */

  /* NOTE: this routine assumes sectors previously erased */

  const size_t npage = size / efspi->page_size;
  const size_t nword = efspi->page_size / sizeof(uint32_t);
  const uint8_t* p = (const uint8_t*)buf;
  uint8_t rem_buf[EFSPI_PAGE_SIZE];
  size_t rem_size;
  uint32_t page_addr;
  size_t i;
  size_t j;

  /* foreach page */
  page_addr = off;
  for (i = 0; i < npage; ++i, page_addr += efspi->page_size)
  {
    if (efspi->write_fn && ((i & 0xff) == 0)) efspi->write_fn(i, npage);

    set_page_program(efspi, page_addr);

    /* foreach word32 */
    for (j = 0; j < nword; ++j, p += sizeof(uint32_t))
      write_wdat_reg(efspi, get_swap_uint32(p));

    wait_edge_count(efspi, (efspi->page_size + sizeof(uint32_t)) * 8);

    wait_status_wip(efspi);
  }
  efspi->endwr_fn();

  /* remaining partial page */
  /* use temporary buffer, otherwise bus error on arm */
  /* note: still use nword, not rem_size */
  rem_size = mod_page_size(efspi, size);
  if (rem_size)
  {
    memcpy(rem_buf, p, rem_size);
    p = rem_buf;

    set_page_program(efspi, page_addr);

    /* foreach word32 */
    for (j = 0; j < nword; ++j, p += sizeof(uint32_t))
      write_wdat_reg(efspi, get_swap_uint32(p));

    wait_edge_count(efspi, (efspi->page_size + sizeof(uint32_t)) * 8);

    wait_status_wip(efspi);
  }

  return EFSPI_ERR_SUCCESS;
}

efspi_err_t efspi_read_buf
(efspi_handle_t* efspi, void* buf, size_t off, size_t size)
{
  /* off the offset, in bytes */
  /* size the size, in bytes */

  const size_t n = size / sizeof(uint32_t);
  uint8_t* p = (uint8_t*)buf;
  size_t i;

  if (check_read_access(efspi, off, size))
  {
    return EFSPI_ERR_FAILURE;
  }

  for (i = 0; i < n; ++i, p += sizeof(uint32_t), off += sizeof(uint32_t))
    read_uint32(efspi, (uint32_t)off, p);

  return EFSPI_ERR_SUCCESS;
}


/* erase then write flash from xilinx bit file */

typedef struct bit_handle
{
  uint8_t hdr_buf[EFSPI_PAGE_SIZE];
  size_t  hdr_buf_size;
  uint8_t headerkw[EFSPI_PAGE_SIZE*2];
  size_t  headerkw_size;

  uint8_t* file_buf;
  size_t file_size;

  size_t data_off;
  size_t data_size;

} bit_handle_t;

static void* mmap_ronly(int fd, size_t size)
{
  return mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
}

// This function will extract the bitstream package header if one avalaible
int get_pkgheader(uint8_t* dest, uint8_t* src, size_t dst_size, size_t src_size) {
   int nread = 0;

   if(src_size >= HEADER_FIELDINFO_SIZE) {
      uint16_t umagic = src[0] << 8;
      umagic |= src[1];
      if(umagic == HEADER_MAGICKEY ) {
         nread = src[2] << 8;
         nread |= src[3];
         nread += HEADER_FIELDINFO_SIZE;
         if(dst_size > nread) {
            memcpy(dest, src, nread);
         } else {
            nread = 0;
         }
      }
   }

   return nread;
}

static int bit_open(bit_handle_t* bit, const char* path, size_t page_size)
{
   /*
    * This is only for very old XC4000 FPGAs
    * http://www.fpga-faq.com/FAQ_Pages/0026_Tell_me_about_bit_files.htm
    *
   #define BIT_SYNC_PATTERN "\xff\xff\xff\xff\xaa\x99\x55\x66"
    */
   #define BIT_SYNC_PATTERN "\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\x00\x00\x00\xbb\x11\x22\x00\x44\xff\xff\xff\xff\xff\xff\xff\xff\xaa\x99\x55\x66"
   #define BIT_SYNC_SIZE (sizeof(BIT_SYNC_PATTERN) - 1)

   size_t i;
   size_t j;
   int fd;
   struct stat stat;

   /* open and map the file */

   fd = open(path, O_RDONLY);
   if (fd == -1)
   {
      LOG_ERROR("unable to open file: %s\n", path);
      goto on_error_0;
   }

   if (fstat(fd, &stat))
   {
      LOG_ERROR("unable to read file: %s\n", path);
      goto on_error_1;
   }

   /* mapped size is one page larger because of rounding up */
   bit->file_size = stat.st_size;
   bit->file_buf = mmap_ronly(fd, bit->file_size);
   if (bit->file_buf == (uint8_t*)MAP_FAILED)
   {
      LOG_ERROR("unable to read file: %s\n", path);
      goto on_error_1;
   }

   // Extract package header if exist
   bit->headerkw_size = get_pkgheader(bit->headerkw, bit->file_buf, EFSPI_PAGE_SIZE*2, bit->file_size);

   // Remove package header
   bit->file_buf += bit->headerkw_size;
   bit->file_size -= bit->headerkw_size;
   /* loop for sync pattern in the first page */
   for (i = 0; (i + BIT_SYNC_SIZE) <= page_size; ++i)
   {
      if (memcmp(bit->file_buf + i, BIT_SYNC_PATTERN, BIT_SYNC_SIZE) == 0)
      break ;
   }

   if ((i + BIT_SYNC_SIZE) > page_size)
   {
      LOG_ERROR("unable to parse bit file header\n");
      goto on_error_2;
   }

   bit->data_off = i;
   bit->data_size = bit->file_size - i;

   memset(bit->hdr_buf, 0, sizeof(bit->hdr_buf));

   j = 0;
   for (i = 0; i < bit->data_off; ++i)
   {
      bit->hdr_buf[j++] = bit->file_buf[i];
      if (j == sizeof(bit->hdr_buf))
         break ;
   }
   bit->hdr_buf_size = j; // real size

   close(fd);

   return 0;

   on_error_2:
      munmap(bit->file_buf, bit->file_size);
   on_error_1:
      close(fd);
   on_error_0:
      return -1;
}

static int bit_close(bit_handle_t* bit)
{
  munmap(bit->file_buf, bit->file_size);
  return 0;
}

#if 0 /* debugging */
efspi_err_t efspi_print_bit_info(efspi_handle_t* efspi, const char* path)
{
  bit_handle_t bit;
  size_t i;

  if (bit_open(&bit, path, efspi->page_size))
  {
    PERROR();
    return EFSPI_ERR_FAILURE;
  }

  printf("file_size: 0x%08lx\n", bit.file_size);
  printf("data_off : 0x%08lx\n", bit.data_off);
  printf("data_size: 0x%08lx\n", bit.data_size);

  printf("header   : ");
  for (i = 0; (i < sizeof(bit.hdr_buf)) && bit.hdr_buf[i]; ++i)
    printf("%c", bit.hdr_buf[i]);
  printf("\n");

  bit_close(&bit);

  return EFSPI_ERR_SUCCESS;
}
#endif /* debugging */

#define BUFFER_SIZE 24
#define DEFAULT_IDENT_VALUE "Unknown"

typedef struct hdr_xilinx {
   uint8_t name[BUFFER_SIZE];
   uint8_t userid[BUFFER_SIZE];
   uint8_t ref[BUFFER_SIZE];
   uint8_t tool_vers[BUFFER_SIZE];
   uint8_t bit_date[BUFFER_SIZE];
   uint8_t bit_hour[BUFFER_SIZE];
   uint32_t bit_raw_size;
} hdr_xilinx_t;


static int parse_field(uint8_t* dest, uint8_t* src
                        , uint8_t delim
                        , size_t dst_len, size_t src_len) {
   uint8_t* poutput = dest;
   uint8_t* pinput = src;
   int size = 0;

   while (*pinput != delim
            && dst_len > 0
            && src_len > 0) {
      *poutput = *pinput;
      poutput++;
      dst_len--;
      src_len--;
      pinput++;
      size++;
   }
   if(dst_len > 0)
      *poutput = '\0' ;

   return size;
}

static int extract_xilinx_hdr(hdr_xilinx_t* dst_hdr, uint8_t* src_raw_hdr, size_t src_size) {
   int iret = 0;
   int size = 0;
   int field_size = 0;
   unsigned int uidx = 0;
   uint8_t buff[BUFFER_SIZE];
   uint8_t* pinput = src_raw_hdr;
#define INCR_SRC() pinput++; src_size--;

   if (src_size <= 0) {
      memcpy(dst_hdr->name, DEFAULT_IDENT_VALUE, strlen(DEFAULT_IDENT_VALUE));
      dst_hdr->name[strlen(DEFAULT_IDENT_VALUE)] = '\0';
      memcpy(dst_hdr->userid, DEFAULT_IDENT_VALUE, strlen(DEFAULT_IDENT_VALUE));
      dst_hdr->userid[strlen(DEFAULT_IDENT_VALUE)] = '\0';
      memcpy(dst_hdr->ref, DEFAULT_IDENT_VALUE, strlen(DEFAULT_IDENT_VALUE));
      dst_hdr->ref[strlen(DEFAULT_IDENT_VALUE)] = '\0';
      memcpy(dst_hdr->tool_vers, DEFAULT_IDENT_VALUE, strlen(DEFAULT_IDENT_VALUE));
      dst_hdr->tool_vers[strlen(DEFAULT_IDENT_VALUE)] = '\0';
      memcpy(dst_hdr->bit_date, DEFAULT_IDENT_VALUE, strlen(DEFAULT_IDENT_VALUE));
      dst_hdr->bit_date[strlen(DEFAULT_IDENT_VALUE)] = '\0';
      dst_hdr->bit_hour[0] = '\0';

      iret = -1;
   } else {
#define XILINX_FIELD1_SIZE 13
      for(uidx = 0; uidx < XILINX_FIELD1_SIZE; uidx++) {
         INCR_SRC()
      }
      // Field 2
      if (*pinput == 'a') {
         // Field 3 get field lenght in 2 bytes
         INCR_SRC()
         field_size = *pinput << 8;
         INCR_SRC()
         field_size |= *pinput;
         INCR_SRC()

         if (field_size < src_size) {
            // - bitstream filename
            size = parse_field(dst_hdr->name, pinput, ';', BUFFER_SIZE, field_size);
            pinput = pinput + size + 1;
            field_size = field_size - size - 1;
            // - UserID
            // - Vivado version
            for (uidx = 0; uidx < 2; uidx++) {
               size = parse_field(buff, pinput, '=', BUFFER_SIZE, field_size);
               pinput = pinput + size + 1;
               field_size = field_size - size - 1;
               if (strcmp((char *)buff, "UserID") == 0) {
                  size = parse_field(dst_hdr->userid, pinput, ';', BUFFER_SIZE, field_size);
               } else if (strcmp((char *)buff, "Version") == 0){
                  size = parse_field(dst_hdr->tool_vers, pinput, '\0', BUFFER_SIZE, field_size - 1);
               }
               pinput = pinput + size + 1;
               field_size = field_size - size - 1;
            }

            // Field 4
            // FPGA reference
            if (*pinput == 'b') {
               INCR_SRC()
               field_size = *pinput << 8;
               INCR_SRC()
               field_size |= *pinput;
               INCR_SRC()
               size = parse_field(dst_hdr->ref, pinput, '\0', BUFFER_SIZE, field_size);
               pinput = pinput + size + 1;
            }

            // Date field
            if (*pinput == 'c') {
               INCR_SRC()
               field_size = *pinput << 8;
               INCR_SRC()
               field_size |= *pinput;
               INCR_SRC()
               size = parse_field(dst_hdr->bit_date, pinput, '\0', BUFFER_SIZE, field_size);
               pinput = pinput + size + 1;
            }

            // time field
            if (*pinput == 'd') {
               INCR_SRC()
               field_size = *pinput << 8;
               INCR_SRC()
               field_size |= *pinput;
               INCR_SRC()
               size = parse_field(dst_hdr->bit_hour, pinput, '\0', BUFFER_SIZE, field_size);
               pinput = pinput + size + 1;
            }

            // size field
            if (*pinput == 'e') {
               INCR_SRC()
               dst_hdr->bit_raw_size = 0;
               int i;
               for(i=3;i>=0;i--) {
                  dst_hdr->bit_raw_size |= (*pinput) << (i*8);
                  INCR_SRC()
               }
            }
         }
      }
   }
   return iret;
}

static size_t add_rcskeyword(char** buf, size_t *size, const char* key, const char* value) {
   int nchar = snprintf(*buf, *size, "%c%s: %s %c\n", '$', key, value, '$');
   *size -= nchar;
   *buf += nchar;
   return *size;
}

static int compute_header(uint8_t* dest
                           , hdr_xilinx_t* dst_hdr
                           , size_t dst_size
                           , size_t dst_off ) {
   char*   poutput = (char*)dest + dst_off;
   size_t  size = dst_size - dst_off;
   char    strvalue[256];

#define INCR_OUTPUT() poutput++; dst_size--;

   if ( dst_off <= 0) { // No header package

      // create header separator
      *poutput = HEADER_MAGICKEY >> 8;
      INCR_OUTPUT()
      *poutput = HEADER_MAGICKEY & 0xFF;
      INCR_OUTPUT()
      *poutput = ' ';
      INCR_OUTPUT()
      *poutput = ' ';
      INCR_OUTPUT()

      add_rcskeyword(&poutput, &size, "Type", "BITSTREAM");
      add_rcskeyword(&poutput, &size, "Name", (char*)dst_hdr->name);
//      add_rcskeyword(&poutput, &size, "URL", DEFAULT_IDENT_VALUE);
//      add_rcskeyword(&poutput, &size, "Revrange", DEFAULT_IDENT_VALUE);
//      add_rcskeyword(&poutput, &size, "User", DEFAULT_IDENT_VALUE);
   }
   snprintf(strvalue, sizeof(strvalue), "%s %s", dst_hdr->bit_date, dst_hdr->bit_hour);
   add_rcskeyword(&poutput, &size, "Date", strvalue);
   snprintf(strvalue, sizeof(strvalue), "%s - %s", dst_hdr->bit_date, dst_hdr->ref);
   add_rcskeyword(&poutput, &size, "Platform", strvalue);
   add_rcskeyword(&poutput, &size, "Tools", (char*)dst_hdr->tool_vers);
   snprintf(strvalue, sizeof(strvalue), "%d", dst_hdr->bit_raw_size);
   add_rcskeyword(&poutput, &size, "RawBitSize", strvalue);

   // update length to header
   poutput = (char*)dest;
   size = strlen(poutput + HEADER_FIELDINFO_SIZE);

   poutput[2] = size >> 8;
   poutput[3] = size & 0xFF;

   return size + HEADER_FIELDINFO_SIZE;
}

efspi_err_t efspi_erase_write_bit(efspi_handle_t* efspi, const char* path)
{
   /* erase the whole memory */
   /* write the bit file at begining of the memory */
   /* write the header string at the end of memory */

   bit_handle_t bit;
   size_t off;
   efspi_err_t err = EFSPI_ERR_FAILURE;
   uint8_t buffhead[EFSPI_PAGE_SIZE*4];

   hdr_xilinx_t hdr_xilinx;
   void (*write_fn)(size_t, size_t);

   /* get bit file before erasing */
   if (bit_open(&bit, path, efspi->page_size)) {
      goto on_error_0;
   }
   LOG("bit file size     : %zd\n", bit.file_size);
   LOG("xilinx header size: %zd\n", bit.hdr_buf_size);
   LOG("dance  header size: %zd\n", bit.headerkw_size);
   LOG("data offset       : %zd\n", bit.data_off);

   // Check size before update
   // (leave 1 sector size for header)
   if (bit.file_size > (efspi->mem_size - efspi->sector_size)) {
      LOG_ERROR("bit file (%zubytes) too large  for memory (%zubytes)\n",
         bit.file_size, (efspi->mem_size - efspi->sector_size));
      goto on_error_0;
   }

   // Add DAnCE identification header in string format
   extract_xilinx_hdr(&hdr_xilinx, bit.hdr_buf, bit.hdr_buf_size);
   LOG("from xilinx header...\n");
   LOG("    name          : %s\n", hdr_xilinx.name);
   LOG("    userid        : %s\n", hdr_xilinx.userid);
   LOG("    ref           : %s\n", hdr_xilinx.ref);
   LOG("    tool version  : %s\n", hdr_xilinx.tool_vers);
   LOG("    bit syn date  : %s\n", hdr_xilinx.bit_date);
   LOG("    bit syn hour  : %s\n", hdr_xilinx.bit_hour);
   LOG("    bit raw size  : %d + header = bit file size\n",\
      hdr_xilinx.bit_raw_size);

   bit.headerkw_size = compute_header(bit.headerkw, &hdr_xilinx, EFSPI_PAGE_SIZE*2, bit.headerkw_size);

   // check if there enough place for header
   // One sector is reserved for the header
   if ((bit.headerkw_size + bit.hdr_buf_size) > efspi->sector_size) {
      LOG_ERROR("not enough room in memory for header info\n");
      goto on_error_1;
   }

   // Update FPGA
   // Erase memory
   LOG("start erasing memory\n");
   if (efspi->erase_fn) efspi->erase_fn();
   erase_bulk(efspi);
   LOG("erasing memory done\n");

   // The FPGA bit may have been generated with optional SPIx4
   enable_quadspi(efspi);

   LOG("start writing bitstream into memory\n");
   // Write the bitstream without header at the beginning of the memory
   if (efspi_write_buf(efspi, bit.file_buf + bit.data_off, 0, bit.data_size)) {
      LOG_ERROR("unable to write bitstream into memory\n");
      goto on_error_1;
   }
   LOG("writing bitstream into memory done\n");

   // write bit header in last sector
   // disable the write_fn, otherwise users get disturbed by the output.
   write_fn = efspi->write_fn;
   efspi->write_fn = NULL;
   efspi_get_last_sector_offset(efspi, &off);

   memcpy(buffhead, bit.headerkw, bit.headerkw_size);
   memcpy(buffhead + bit.headerkw_size, bit.hdr_buf, bit.hdr_buf_size);

   //efspi_erase_sector(efspi, off, efspi->sector_size); // for debug

   LOG("start writing identification into memory\n");
   if (efspi_write_buf(efspi, buffhead, off, bit.headerkw_size + bit.hdr_buf_size)) {
      // not a hard error, continue
      LOG_ERROR("unable to write identification into memory\n");
   }
   LOG("writing identification into memory done\n");

   efspi->write_fn = write_fn;

  /* success */
  err = EFSPI_ERR_SUCCESS;

 on_error_1:
  bit_close(&bit);
 on_error_0:
  return err;
}


/* header offset */

efspi_err_t efspi_get_last_sector_offset(efspi_handle_t* efspi, size_t* off)
{
  /* return the last sector offset, in bytes */

  /* should be efspi->mem_size - efspi->sect_size, but previous */
  /* soft did not compute it correctly so we switch case */

  efspi_err_t err = EFSPI_ERR_SUCCESS;
  switch (efspi->vd_id)
  {
  case SPANSION_S25FL064P_ID:
  case MACRONIX_MX25L6435E_ID:
    *off = 0x007f0000;
    break ;

  case MACRONIX_MX25L12835F_ID:
    *off = 0x00fff000;
    break ;

  case NUMONYX_M25P128_ID:
  case NUMONYX_N25Q128_ID:
    *off = 0x00fc0000;
    break ;

  case SPANSION_S25FL256S_ID:
  case SPANSION_S25FL512S_ID:
  case NUMONYX_N25Q256_ID:
  case MACRONIX_MX25L25635F_ID:
  case MICRON_MT25QU01GBBB_ID:
    *off = efspi->mem_size - efspi->sector_size;
    break ;

  default:
    /* should not occur, checked in libefspi_open */
    LOG_ERROR("unknown memory ids\n");
    err = EFSPI_ERR_FAILURE;
    break ;
  }

  return err;
}

int efspi_extract_header(efspi_handle_t* efspi_h, uint8_t* header, size_t hdr_size) {
   size_t off;
   uint8_t buffhead[HEADER_MAX_SIZE];
   int nread = -1;
   int icpt = 0;

   efspi_get_last_sector_offset(efspi_h, &off);

   if (efspi_read_buf(efspi_h, buffhead, off, HEADER_MAX_SIZE) == EFSPI_ERR_SUCCESS) {
      uint16_t umagic = buffhead[0] << 8;
      umagic |= buffhead[1];

      // Check header magic
      if(umagic == HEADER_MAGICKEY ) {
         nread = buffhead[2] << 8;
         nread |= buffhead[3];
         if(hdr_size > nread) {
            memcpy(header, buffhead + HEADER_FIELDINFO_SIZE, nread);
         } else {
            nread = -1;
         }
      } else {
         hdr_xilinx_t xilinx_hdr;
         // Header xilinx
         // Count printable string
         for (icpt = 0; icpt < HEADER_MAX_SIZE; ++icpt)
         {
            if (!isprint(buffhead[icpt]))
               break;
         }
         extract_xilinx_hdr(&xilinx_hdr, buffhead, icpt);
         nread = compute_header(header, &xilinx_hdr, HEADER_MAX_SIZE, 0);
         memmove(header, header + HEADER_FIELDINFO_SIZE, nread - HEADER_FIELDINFO_SIZE + 1);
      }
   }

   return nread;
}

#undef BUFFER_SIZE
