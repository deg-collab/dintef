#ifndef LIBEFSPI_H_INCLUDED
#define LIBEFSPI_H_INCLUDED


#include <stdint.h>
#include <sys/types.h>
#include "libepci.h"


#define HEADER_MAGICKEY       0xCAFE
#define HEADER_FIELDINFO_SIZE 4
#define HEADER_MAX_SIZE       1024

typedef struct efspi_desc
{
  const char* pci_id;
  size_t pci_bar;
  size_t pci_off;
  unsigned int sebone_delay;

  void (*write_fn)(size_t, size_t);
  void (*endwr_fn)(void);
  void (*erase_fn)(void);

#define EFSPI_FLAG_IS_SEBONE (1 << 0)
#define EFSPI_FLAG_COMP_V23  (1 << 1) // Compatibility for ebs_fspi <= 2.3
  unsigned int flags;

} efspi_desc_t;


typedef struct efspi_handle
{
  epcihandle_t pci_bar;
  size_t pci_off;
  unsigned int sebone_delay;

  unsigned int flags;

  /* memory size, in bytes */
  size_t mem_size;

  /* page (write unit) size, in bytes */
  size_t page_size;

  /* sector (erase unit) size, in bytes*/
  size_t sector_size;

  /* vendor device id */
  uint32_t vd_id;

  /* operation callbacks */
  void (*write_fn)(size_t, size_t);
  void (*endwr_fn)(void);
  void (*erase_fn)(void);

} efspi_handle_t;


typedef enum efspi_err
{
  EFSPI_ERR_SUCCESS = 0,
  EFSPI_ERR_FAILURE
} efspi_err_t;


/* exported routines */

efspi_err_t efspi_init_lib(void);
efspi_err_t efspi_fini_lib(void);
efspi_err_t efspi_log_lib(int);
efspi_err_t efspi_open(efspi_handle_t*, const efspi_desc_t*);
efspi_err_t efspi_close(efspi_handle_t*);
efspi_err_t efspi_erase_sector(efspi_handle_t*, size_t, size_t);
efspi_err_t efspi_erase_bulk(efspi_handle_t*);
efspi_err_t efspi_write_buf(efspi_handle_t*, const void*, size_t, size_t);
efspi_err_t efspi_read_buf(efspi_handle_t*, void*, size_t, size_t);
efspi_err_t efspi_read_id(efspi_handle_t*, uint32_t *);
efspi_err_t efspi_erase_write_bit(efspi_handle_t*, const char*);
efspi_err_t efspi_get_last_sector_offset(efspi_handle_t*, size_t*);
efspi_err_t enable_wrextaddr(efspi_handle_t*, size_t);
efspi_err_t enable_rdextaddr(efspi_handle_t*, size_t);

int efspi_extract_header(efspi_handle_t*, uint8_t*, size_t);
int get_pkgheader(uint8_t*, uint8_t*, size_t, size_t);


/* static inlined routines */

static inline efspi_err_t efspi_init_desc(efspi_desc_t* desc)
{
  static const efspi_desc_t default_desc =
  {
    "10ee:eb01",
    0x01,
    0x00,
    0,
    NULL,
    NULL,
    0
  };

  *desc = default_desc;

  return EFSPI_ERR_SUCCESS;
}

static inline efspi_err_t efspi_open_default(efspi_handle_t* efspi)
{
  /* open efspi handle using default description */

  efspi_desc_t desc;
  efspi_init_desc(&desc);
  return efspi_open(efspi, &desc);
}

static inline size_t efspi_get_page_size(const efspi_handle_t* efspi)
{
  return efspi->page_size;
}

static inline size_t efspi_get_sector_size(const efspi_handle_t* efspi)
{
  return efspi->sector_size;
}

static inline size_t efspi_get_mem_size(const efspi_handle_t* efspi)
{
  return efspi->mem_size;
}


#endif /* ! LIBEFSPI_H_INCLUDED */
