#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include "libefifo.h"
#include "libebuf.h"
#include "libeaio.h"
#include "libefifo_helper.h"


#define FAIL() do { printf("[!] %s, %u\n", __FUNCTION__, __LINE__); } while(0)


/* perf related routines */

#include <sched.h>

static double cycles_per_ns = 0;

static inline double cycles_to_ns(uint64_t cycles)
{
  return (double)cycles / cycles_per_ns;
}

static uint64_t add_cycles(uint64_t start, uint64_t stop)
{
  if (stop >= start) return stop - start;
  return stop + UINT64_MAX - start;
}

static void nsleep(unsigned long ns)
{
  usleep(ns / 1000);
}

static inline uint64_t rdtsc(void)
{
  uint32_t a;
  uint32_t d;

  __asm__ __volatile__("rdtsc": "=a" (a), "=d" (d));

  return ((uint64_t)a) | (((uint64_t)d) << 32);
}

static void init_perf(void)
{
  const pid_t pid = getpid();
  unsigned int delay_ns = 50000;
  unsigned int i;
  uint64_t sum;

  struct sched_param old_param;
  struct sched_param new_param;
  int old_sched;

  sched_getparam(pid, &old_param);
  old_sched = sched_getscheduler(pid);

  sched_getparam(pid, &new_param);
  new_param.sched_priority = sched_get_priority_max(SCHED_FIFO);
  sched_setscheduler(pid, SCHED_FIFO, &new_param);
  
  if (sched_getscheduler(pid) != SCHED_FIFO)
  {
    printf("unable to set scheduler (insufficient priv)\n");
    exit(-1);
  }
  sched_getparam(pid, &new_param);
  if (new_param.sched_priority != sched_get_priority_max(SCHED_FIFO))
  {
    printf("invalid prio\n");
    exit(-1);
  }

  sum = 0;

  for (i = 0; i < 1000; ++i)
  {
    uint64_t x;
    uint64_t xx;

    x = rdtsc();
    nsleep(delay_ns);
    xx = rdtsc();

    sum += add_cycles(x, xx);
  }

  cycles_per_ns = (double)sum / (double)(delay_ns * i);

  sched_setscheduler(pid, old_sched, &old_param);
  sched_setparam(pid, &old_param);
}

static void print_perf(uint64_t start_tsc, uint64_t data_tsc, uint64_t wait_tsc)
{
  const uint64_t lat_delta = add_cycles(start_tsc, data_tsc);
  const uint64_t compl_delta = add_cycles(start_tsc, wait_tsc);
  printf("%lf %lf\n", cycles_to_ns(lat_delta), cycles_to_ns(compl_delta));
}


/* wrapped on_aio_xxx routines for perf measurement */

typedef struct wrapped_aio
{
  eaio_handle_t saved_aio;
  uint64_t tsc;
} wrapped_aio_t;

static eaio_err_t on_aio_start(void* opaque)
{
  wrapped_aio_t* const w = opaque;
  return w->saved_aio.on_start(w->saved_aio.on_opaque);
}

static eaio_err_t on_aio_stop(void* opaque)
{
  wrapped_aio_t* const w = opaque;
  return w->saved_aio.on_stop(w->saved_aio.on_opaque);
}

static eaio_err_t on_aio_poll(void* opaque, size_t* size, eaio_flags_t flags)
{
  wrapped_aio_t* const w = opaque;
  return w->saved_aio.on_poll(w->saved_aio.on_opaque, size, flags);
}

static eaio_err_t on_aio_data
(void* opaque, ebuf_handle_t* buf, size_t off, size_t* size, eaio_flags_t flags)
{
  wrapped_aio_t* const w = opaque;
  if (w->tsc == 0) w->tsc = rdtsc();
  return w->saved_aio.on_data(w->saved_aio.on_opaque, buf, off, size, flags);
}

static efifo_err_t wrap_aio(wrapped_aio_t* w, eaio_handle_t* aio)
{
  memcpy(&w->saved_aio, aio, sizeof(eaio_handle_t));
  aio->on_opaque = w;
  aio->on_start = on_aio_start;
  aio->on_stop = on_aio_stop;
  aio->on_poll = on_aio_poll;
  aio->on_data = on_aio_data;
  return EFIFO_ERR_SUCCESS;
}


/* main */

int main(int ac, char** av)
{
#define AIO_COUNT 1
  efifo_handle_t fifo[AIO_COUNT];
  eaio_handle_t aio[AIO_COUNT];
  ebuf_handle_t buf[AIO_COUNT];
  wrapped_aio_t wrap[AIO_COUNT];
  uint64_t start_tsc[AIO_COUNT];
  uint64_t data_tsc[AIO_COUNT];
  uint64_t wait_tsc[AIO_COUNT];

  static const size_t size = 256;

  size_t aio_count;
  eaio_err_t err;
  size_t i;

  /* initialize ebuf library */
  if (ebuf_init_lib(EBUF_FLAG_DEFAULT) != EBUF_ERR_SUCCESS)
  {
    FAIL();
    goto on_error_0;
  }

  /* initialize eaio library, enable threaded scheduler */
  if (eaio_init_lib(EAIO_FLAG_SCHED_THREAD) != EAIO_ERR_SUCCESS)
  {
    FAIL();
    goto on_error_1;
  }

  /* initialize perf info */
  init_perf();

  /* open fifo, wrapped read aio, allocate bufs */
  aio_count = 0;
  for (i = 0; i < AIO_COUNT; ++i)
  {
    eaio_handle_t* const this_aio = &aio[i];
    ebuf_handle_t* const this_buf = &buf[i];
    efifo_handle_t* const this_fifo = &fifo[i];
    wrapped_aio_t* const this_wrap = &wrap[i];

    /* open and wrap aio */

    if (open_efifo(this_fifo, i) != EFIFO_ERR_SUCCESS)
    {
      FAIL();
      goto on_error_2;
    }

    efifo_open_read_aio(this_fifo, this_aio);
    eaio_set_thread_sched(this_aio);
    eaio_set_wait_size(this_aio, size);

    if (ebuf_alloc(this_buf, size, EBUF_FLAG_DEFAULT) != EBUF_ERR_SUCCESS)
    {
      eaio_close(this_aio);
      efifo_close(this_fifo);
      FAIL();
      goto on_error_2;
    }

    eaio_set_buffer(this_aio, this_buf);

    wrap_aio(this_wrap, this_aio);

    ++aio_count;
  }

  /* start aios and wait for wait_size */
  while (1)
  {
    /* start the aios */
    for (i = 0; i < AIO_COUNT; ++i)
    {
      /* start writer */
      start_efifo_writer(&fifo[i], i, 32);

      start_tsc[i] = rdtsc();
      wrap[i].tsc = 0;
      err = eaio_start_discrete(&aio[i], size);
      if (err != EAIO_ERR_SUCCESS)
      {
	FAIL();
	goto on_error_2;
      }
    }

    /* wait for aios to complete */
    for (i = 0; i < AIO_COUNT; ++i)
    {
      /* wait for aio to complete OR wait_size bytes to be available */
      err = eaio_wait(&aio[i], NULL, NULL);
      wait_tsc[i] = rdtsc();
      data_tsc[i] = wrap[i].tsc;

      if (err != EAIO_ERR_SUCCESS)
      {
	FAIL();
	goto on_error_2;
      }

      print_perf(start_tsc[i], data_tsc[i], wait_tsc[i]);

      /* stop the aio, so that non concurrent access to wrap[i].tsc */
      eaio_stop(&aio[i], NULL, NULL);
    }
  }

 on_error_2:
  for (i = 0; i < aio_count; ++i)
  {
    eaio_stop(&aio[i], NULL, NULL);
    eaio_close(&aio[i]);
    ebuf_free(&buf[i]);
    efifo_close(&fifo[i]);
  }
  eaio_fini_lib();
 on_error_1:
  ebuf_fini_lib();
 on_error_0:
  return 0;
}
