#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include "libepci.h"
#include "libebuf.h"
#include "libefifo.h"
#include "libeaio.h"
#include "libefifo_helper.h"


#define FAIL() do { printf("[!] %s, %u\n", __FUNCTION__, __LINE__); } while(0)


/* check the buffer for a known pattern */

static inline uint64_t make_value(uint64_t x)
{
  union
  {
    uint8_t arr[sizeof(uint64_t)];
    uint64_t val;
  } y;

  y.arr[0] = (uint8_t)x;
  y.arr[1] = (uint8_t)(x >> 8);
  y.arr[2] = 0;
  y.arr[3] = 0;
  y.arr[4] = 0;
  y.arr[5] = 0;
  y.arr[6] = (uint8_t)x;
  y.arr[7] = (uint8_t)(x >> 8);

  return y.val;
}

static int check_burst_64(const uint64_t* burst, size_t count)
{
  /* check a burst of count items, 64 bits each */

  size_t i;

  for (i = 0; i < count; ++i, ++burst)
  {
    const uint64_t x = make_value(count - i);
    if (x != *burst)
    {
      printf("invalid value at %u: 0x%llx != 0x%llx\n", i, *burst, x);
      return -1;
    }
  }

  return 0;
}

static int check_buf_64(const uint64_t* buf, size_t size, size_t burst_count)
{
  /* check a buffer of size bytes. each burst is burst_count * 64 bits items. */

  const size_t n = size / (sizeof(uint64_t) * burst_count);

  size_t i;

  for (i = 0; i < n; ++i, buf += burst_count)
  {
    if (check_burst_64(buf, burst_count))
    {
      printf("nburst == %u on %u\n", i, n);
      return -1;
    }
  }

  return 0;
}


/* main */

int main(int ac, char** av)
{
  static const size_t large_size = 128 * 1024;
  static const size_t back_size = 8 * 1024;
  static const size_t burst_size = 32; /* 2^16 max */

  static const unsigned int fifo_index = 0;

  eaio_handle_t aio;
  efifo_handle_t fifo;
  eaio_err_t err;
  size_t compl_size;
  void* compl_data;
  ebuf_handle_t compl_buf[2];
  ebuf_handle_t back_buf;
  size_t large_off;
  uint8_t* large_buf;
  size_t i;

  /* initialize ebuf library */
  if (ebuf_init_lib(EBUF_FLAG_DEFAULT) != EBUF_ERR_SUCCESS)
  {
    FAIL();
    goto on_error_0;
  }

  /* initialize eaio library, enable threaded scheduler */
  if (eaio_init_lib(EAIO_FLAG_SCHED_THREAD) != EAIO_ERR_SUCCESS)
  {
    FAIL();
    goto on_error_1;
  }

  /* open fifo */
  if (open_efifo(&fifo, fifo_index) != EFIFO_ERR_SUCCESS)
  {
    FAIL();
    goto on_error_2;
  }

  /* allocate backing buffer */
  if (ebuf_alloc(&back_buf, back_size, EBUF_FLAG_DEFAULT) != EBUF_ERR_SUCCESS)
  {
    FAIL();
    goto on_error_3;
  }

  /* open configure read aio */
  if (efifo_open_read_aio(&fifo, &aio) != EFIFO_ERR_SUCCESS)
  {
    FAIL();
    goto on_error_4;
  }

  eaio_set_thread_sched(&aio);
  eaio_set_buffer(&aio, &back_buf);
  eaio_set_wait_size(&aio, burst_size * efifo_get_width(&fifo));

  /* allocate a large buffer to hold all the data */
  large_buf = malloc(large_size);
  if (large_buf == NULL)
  {
    FAIL();
    goto on_error_5;
  }

  /* start the aio */
  if (eaio_start_continuous(&aio) != EAIO_ERR_SUCCESS)
  {
    FAIL();
    goto on_error_6;
  }

  /* start fifo writer */
  start_efifo_writer(&fifo, fifo_index, burst_size);

  /* read until large_buf full */
  large_off = 0;
  while (1)
  {
    /* wait for at least wait_size to be available */
    err = eaio_wait(&aio, &compl_buf[0], &compl_buf[1]);
    if (err != EAIO_ERR_SUCCESS)
    {
      FAIL();
      goto on_error_6;
    }

    /* copy completed buffers to large buffer */
    for (i = 0; i < 2; ++i)
    {
      compl_data = ebuf_get_data(&compl_buf[i]);
      compl_size = ebuf_get_size(&compl_buf[i]);

      if ((large_off + compl_size) >= large_size)
	compl_size = large_size - large_off;

      memcpy(large_buf + large_off, compl_data, compl_size);

      ebuf_free(&compl_buf[i]);

      /* advance offset */
      large_off += compl_size;
      if (large_off == large_size) break ;
    }

    printf("-- compl %u on %u\n", large_off, large_size);

    if (large_off >= large_size) break ;
  }

  /* stop the aio */
  err = eaio_stop(&aio, NULL, NULL);
  if (err != EAIO_ERR_SUCCESS)
  {
    FAIL();
    goto on_error_6;
  }

  /* check large buf */
  if (check_buf_64((const uint64_t*)large_buf, large_off, burst_size) == 0)
  {
    printf("test passed\n");
  }

 on_error_6:
  free(large_buf);
 on_error_5:
  eaio_close(&aio);
 on_error_4:
  ebuf_free(&back_buf);
 on_error_3:
  efifo_close(&fifo);
 on_error_2:
  eaio_fini_lib();
 on_error_1:
  ebuf_fini_lib();
 on_error_0:
  return 0;
}
