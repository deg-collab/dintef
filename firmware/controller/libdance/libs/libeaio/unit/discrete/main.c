#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include "libebuf.h"
#include "libeaio.h"
#include "common.h"

static int check_data(const uint8_t* data, size_t size)
{
  size_t i;

  for (i = 0; i != size; ++i)
  {
    if (data[i] != (uint8_t)i)
    {
      printf("[!] check_data@%lu\n", i);
      return -1;
    }
  }

  return 0;
}

/* main */

int main(int ac, char** av)
{
#define AIO_COUNT 1
  eaio_handle_t aios[AIO_COUNT];
  ebuf_handle_t bufs[AIO_COUNT];
  dummy_t dummies[AIO_COUNT];
  eaio_err_t err;
  size_t compl_size;
  void* compl_data;
  ebuf_handle_t compl_buf[2];
  const size_t buffer_size = 32;
  size_t i;

  /* initialize ebuf library */
  if (ebuf_init_lib(EBUF_FLAG_DEFAULT) != EBUF_ERR_SUCCESS) goto on_error_0;

  /* initialize eaio library */
  err = eaio_init_lib(EAIO_FLAG_SCHED_THREAD);
  if (err != EAIO_ERR_SUCCESS) goto on_error_1;

  /* allocate bufs */
  for (i = 0; i < AIO_COUNT; ++i)
  {
    eaio_handle_t* const aio = &aios[i];
    ebuf_handle_t* const buf = &bufs[i];
    dummy_t* const dummy = &dummies[i];

    /* allocate a buffer for this aio */
    ebuf_alloc(buf, buffer_size, EBUF_FLAG_DEFAULT);

    /* dummy context */
    dummy->id = i;

    /* open and configure aio */
    eaio_open(aio);
    eaio_set_thread_sched(aio);
    eaio_set_buffer(aio, buf);
    eaio_set_wait_size(aio, buffer_size);
    eaio_set_wait_timeout(aio, EAIO_TIMEOUT_NONE);
    aio->on_start = on_aio_start;
    aio->on_stop = on_aio_stop;
    aio->on_poll = on_aio_poll;
    aio->on_data = on_aio_data;
    aio->on_opaque = dummy;
    aio->user_opaque = dummy;
  }

  while (1)
  {
    /* start the aio */
    for (i = 0; i < AIO_COUNT; ++i)
    {
      err = eaio_start_discrete(&aios[i], buffer_size);
      if (err != EAIO_ERR_SUCCESS) goto on_error_2;
    }

    for (i = 0; i < AIO_COUNT; ++i)
    {
      dummy_t* const dummy = eaio_get_user_opaque(&aios[i]);
      int check_err;

      /* wait for aio to complete OR wait_size bytes to be available */
      err = eaio_wait(&aios[i], &compl_lbuf, &compl_rbuf);
      if (err != EAIO_ERR_SUCCESS) goto on_error_2;

      compl_data = ebuf_get_data(&compl_buf[0]);
      compl_size = ebuf_get_size(&compl_buf[0]);
      printf("dummy[%u].compl_size == %lu\n", dummy->id, compl_size);
      check_err = check_data(compl_data, compl_size);

      if (ebuf_get_size(&compl_buf[1]) != 0)
      {
	printf("invalid rbuf, should be zero\n");
	check_err = -1;
      }

      ebuf_free(&compl_buf[0]);
      ebuf_free(&compl_buf[1]);

      if (check_err) break ;
    }
  }

 on_error_2:
  for (i = 0; i < AIO_COUNT; ++i)
  {
    eaio_stop(&aios[i], NULL, NULL);
    eaio_close(&aios[i]);
    ebuf_free(&bufs[i]);
  }
  eaio_fini_lib();
 on_error_1:
  ebuf_fini_lib();
 on_error_0:
  return 0;
}
