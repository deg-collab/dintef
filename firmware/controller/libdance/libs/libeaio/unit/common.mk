

DANCE_SDK_PLATFORM ?= seco_imx6

LIBDANCE_HOME ?= ../../../..

LIBEAIO_DIR := ../../src
LIBEBUF_DIR := $(LIBDANCE_HOME)/libs/libebuf/src
LIBEPCI_DIR := $(LIBDANCE_HOME)/libs/libepci/src
LIBPCI_DIR = $(DANCE_SDK_DEPS_DIR)/lib
LIBPMEM_DIR := $(LIBDANCE_HOME)/libs/libpmem/src
LIBEFIFO_DIR := $(LIBDANCE_HOME)/libs/libefifo/src
LIBUIRQ_DIR := $(LIBDANCE_HOME)/libs/libuirq/src

DEPENDENCIES_DIRS := \
      $(LIBEAIO_DIR)  \
#      $(LIBEBUF_DIR)  \

HEADER_DIRS = \
      ../common  \
      $(LIBEAIO_DIR)  \
      $(LIBEBUF_DIR)  \
      $(LIBEPCI_DIR)  \
      $(LIBEFIFO_DIR)  \
      $(LIBUIRQ_DIR)  \
               
LIBRARY_DIRS = \
      $(LIBEAIO_DIR)  \
      $(LIBEBUF_DIR)  \
      $(LIBEPCI_DIR)  \
      $(LIBPCI_DIR)  \
      $(LIBPMEM_DIR)  \
      $(LIBEFIFO_DIR)  \

LDLIBS := -leaio -lebuf -lepci -lpci -lpmem -lefifo -lpthread

include $(LIBDANCE_HOME)/build/libtool.mk
