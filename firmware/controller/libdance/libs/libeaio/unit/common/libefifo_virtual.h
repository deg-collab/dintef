#ifndef LIBEFIFO_VIRTUAL_H_INCLUDED
#define LIBEFIFO_VIRTUAL_H_INCLUDED 1


#include <stdint.h>
#include <string.h>
#include <sys/types.h>


/* static configuration */

#ifndef EFIFO_CONFIG_AIO
#define EFIFO_CONFIG_AIO 1
#endif /* EFIFO_CONFIG_AIO */



/* forward declarations */
typedef struct epcihandle_s* epcihandle_t;
typedef struct ebuf_handle ebuf_handle_t;
#if EFIFO_CONFIG_AIO
typedef struct eaio_handle eaio_handle_t;
#endif /* EFIFO_CONFIG_AIO */


/* error codes */

typedef enum
{
  EFIFO_ERR_SUCCESS = 0,
  EFIFO_ERR_FAILURE,
  EFIFO_ERR_SIZE,
  EFIFO_ERR_ADDR,
  EFIFO_ERR_DESC,
  EFIFO_ERR_TIMEOUT,
  EFIFO_ERR_MODE,
  EFIFO_ERR_METHOD,
  EFIFO_ERR_ALREADY,
  EFIFO_ERR_IMPL
} efifo_err_t;


/* timeout special values */

#define EFIFO_TIMEOUT_NONE ((unsigned int)-1)
#define EFIFO_TIMEOUT_ZERO ((unsigned int)0)


/* flags */

typedef enum
{
  EFIFO_FLAG_READ = 1 << 0,
  EFIFO_FLAG_WRITE = 1 << 1,
  EFIFO_FLAG_READWRITE = EFIFO_FLAG_READ | EFIFO_FLAG_WRITE,
  EFIFO_FLAG_IRQ = 1 << 2
} efifo_flag_t;


/* fifo description */

typedef struct efifo_desc
{
  /* item width, in byte. must be multiple of sizeof(uint32_t) */
  size_t width;
  /* fifo item count */
  size_t depth;

  efifo_flag_t flags;

  size_t irq_threshold;

  /* associated epci bar handle */
  epcihandle_t epci_handle;

  /* control, {r,w}data, status registers */
  size_t ctrl_reg_off;
  size_t rdat_reg_off;
  size_t wdat_reg_off;
  size_t stat_reg_off;

  /* ebone slave id */
  uint32_t ebone_slave_id;

} efifo_desc_t;


/* fifo handle */

typedef struct efifo_handle
{
  /* EFIFO_FLAG_xxx */
  uint32_t flags;

  /* item width, in uint32_t words */
  size_t width;
  /* fifo item count */
  size_t depth;

  size_t threshold;

  /* io operation timeout, in milliseconds */
  unsigned int timeout;

  /* control, {r,w}data, status registers */
  epcihandle_t epci_handle;
  size_t ctrl_reg_off;
  size_t rdat_reg_off;
  size_t wdat_reg_off;
  size_t stat_reg_off;

  /* ebone driver, interrupt signaling */
  int ebone_fd;

  /* ebone slave id */
  uint32_t ebone_slave_id;

  /* ebone slave mirq mask (not bit position) */
  uint32_t ebone_mirq;

  /* identifier */
  unsigned int id;

  size_t size;

  /* cycle counters */
  uint64_t start;
  uint64_t read;
  uint64_t compl;

} efifo_handle_t;


/* exported routines */

efifo_err_t efifo_open(efifo_handle_t*, const efifo_desc_t*);
efifo_err_t efifo_close(efifo_handle_t*);

efifo_err_t efifo_read(efifo_handle_t*, ebuf_handle_t*, size_t, size_t*);
efifo_err_t efifo_write(efifo_handle_t*, ebuf_handle_t*, size_t, size_t*);

efifo_err_t efifo_open_read_aio(efifo_handle_t*, eaio_handle_t*);
efifo_err_t efifo_open_write_aio(efifo_handle_t*, eaio_handle_t*);

/* accessors */

static inline void efifo_set_timeout(efifo_handle_t* fifo, unsigned int timeout)
{
  fifo->timeout = timeout;
}

static inline unsigned int efifo_get_timeout(efifo_handle_t* fifo)
{
  return fifo->timeout;
}

size_t efifo_get_threshold(efifo_handle_t* fifo)
{
  return fifo->threshold;
}

void efifo_set_threshold(efifo_handle_t* fifo, size_t threshold)
{
  fifo->threshold = threshold;
}

size_t efifo_get_occupancy(efifo_handle_t*);

/* inlined routines */

static inline size_t efifo_get_depth(const efifo_handle_t* fifo)
{
  /* return the fifo depth, in item count */
  return fifo->depth;
}

static inline size_t efifo_get_width(const efifo_handle_t* fifo)
{
  /* return the item width, in bytes */
  return fifo->width * sizeof(uint32_t);
}

static inline void efifo_init_desc(efifo_desc_t* desc)
{
  /* initialize a desc with default, invalid values */
  memset(desc, 0, sizeof(efifo_desc_t));
}

static inline unsigned int efifo_is_empty(efifo_handle_t* fifo)
{
  return efifo_get_occupancy(fifo) == 0;
}

static inline unsigned int efifo_is_full(efifo_handle_t* fifo)
{
  return efifo_get_occupancy(fifo) == fifo->depth;
}

static inline uint32_t efifo_get_mirq(const efifo_handle_t* fifo)
{
  /* assume fifo->flags & EFIFO_FLAG_IRQ */
  return fifo->ebone_mirq;
}

static inline size_t efifo_count_to_size
(const efifo_handle_t* fifo, size_t count)
{
  /* note:  get_width() returns a byte count */
  return count * efifo_get_width(fifo);
}

static inline size_t efifo_size_to_count
(const efifo_handle_t* fifo, size_t size)
{
  /* warning: count rounded down */
  /* note:  get_width() returns a byte count */
  return size / efifo_get_width(fifo);
}


#endif /* LIBEFIFO_VIRTUAL_H_INCLUDED */
