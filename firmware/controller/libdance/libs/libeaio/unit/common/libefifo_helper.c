#include <stdint.h>
#include <sys/types.h>
#include "libepci.h"
#include "libefifo.h"

/* open close a fifo, by index */

static epcihandle_t epci_handle = EPCI_BAD_HANDLE;

efifo_err_t open_efifo(efifo_handle_t* fifo, unsigned int index)
{
  static const unsigned int is_irq = 0;

  efifo_desc_t desc;

  if (epci_handle == EPCI_BAD_HANDLE)
  {
    static const unsigned int bar = 1;
    epci_handle = epci_open("10ee:eb01", NULL, bar);
    if (epci_handle == EPCI_BAD_HANDLE) return EFIFO_ERR_FAILURE;
  }

  efifo_init_desc(&desc);
  desc.epci_handle = epci_handle;

  switch (index)
  {
  case 0:
    desc.flags = EFIFO_FLAG_READ;
    if (is_irq)
    {
      desc.flags |= EFIFO_FLAG_IRQ;
      desc.irq_threshold = 4;
    }
    desc.width = sizeof(uint64_t);
    desc.depth = 1024;
    desc.ctrl_reg_off = 0x44;
    desc.rdat_reg_off = 0x48;
    desc.stat_reg_off = 0x5c;
    desc.ebone_slave_id = 0;
    break ;

  case 1:
    desc.flags = EFIFO_FLAG_READ;
    if (is_irq)
    {
      desc.flags |= EFIFO_FLAG_IRQ;
      desc.irq_threshold = 4;
    }
    desc.width = sizeof(uint64_t);
    desc.depth = 1024;
    desc.ctrl_reg_off = 0x64;
    desc.rdat_reg_off = 0x68;
    desc.stat_reg_off = 0x7c;
    desc.ebone_slave_id = 0; /* not available yet */
    break ;

  case 2:
    desc.epci_handle = epci_handle;
    desc.flags = EFIFO_FLAG_READWRITE;
    if (is_irq)
    {
      desc.flags |= EFIFO_FLAG_IRQ;
      desc.irq_threshold = 4;
    }
    desc.width = sizeof(uint32_t);
    desc.depth = 1024;
    desc.wdat_reg_off = 0x80;
    desc.ctrl_reg_off = 0x84;
    desc.rdat_reg_off = 0x88;
    desc.stat_reg_off = 0x8c;
    desc.ebone_slave_id = 0; /* not available yet */
    break ;

  default: break ;
  }

  return efifo_open(fifo, &desc);
}


extern void efifo_set_reset_bit(efifo_handle_t*);
extern void efifo_clear_reset_bit(efifo_handle_t*);

static void write_fsm0_ctl(epcihandle_t h, uint32_t x)
{
  /* write fsm0 control register */
  epci_wr32_reg(h, 0x40, x);
}

static void write_fsm1_ctl(epcihandle_t h, uint32_t x)
{
  /* write fsm1 control register */
  epci_wr32_reg(h, 0x60, x);
}

void start_efifo_writer
(efifo_handle_t* fifo, size_t index, size_t bsize)
{
  /* start the writing fsm */
  /* count, the item burst size */

#define DELAY_UNIT_1_US 0x0
#define DELAY_UNIT_10_US 0x1
#define DELAY_UNIT_100_US 0x2
#define DELAY_UNIT_1_MS 0x3
#define DELAY_UNIT_SINGLE_BURST 0x0
#define NEXT_DELAY_SINGLE_BURST 0x0

  static const uint32_t delay_unit = DELAY_UNIT_1_MS;
  static const uint32_t next_delay = 10;
  static const uint32_t intra_clk = 0; /* 255 max */

  void (*fn)(epcihandle_t, uint32_t);

  epcihandle_t const eh = fifo->epci_handle;

  /* fsm setup prolog */
  efifo_set_reset_bit(fifo);

    /* setup fsm */
  if (index == 0) fn = write_fsm0_ctl;
  else /* if (index == 1) */ fn = write_fsm1_ctl;
  fn(eh, (delay_unit << 30) | (next_delay << 24) | (intra_clk << 16) | bsize);

  /* fsm setup epilog */
  efifo_clear_reset_bit(fifo);
}
