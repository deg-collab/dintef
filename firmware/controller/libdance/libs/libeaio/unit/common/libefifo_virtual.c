#include "libefifo_virtual.h"
#include "libeaio.h"
#include "libebuf.h"

efifo_err_t efifo_open(efifo_handle_t* fifo, const efifo_desc_t* desc)
{
}

efifo_err_t efifo_close(efifo_handle_t* fifo)
{
}

efifo_err_t efifo_read
(efifo_handle_t* fifo, ebuf_handle_t* buf, size_t off, size_t* count)
{
}

efifo_err_t efifo_write
(efifo_handle_t* fifo, ebuf_handle_t* buf, size_t off, size_t* count)
{
}

efifo_err_t efifo_open_read_aio(efifo_handle_t* fifo, eaio_handle_t* aio)
{
}

efifo_err_t efifo_open_write_aio(efifo_handle_t* fifo, eaio_handle_t* aio)
{
}

size_t efifo_get_occupancy(efifo_handle_t* fifo)
{
}
