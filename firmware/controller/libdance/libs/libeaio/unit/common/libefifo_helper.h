#ifndef LIBEFIFO_HELPER_H_INCLUDED
#define LIBEFIFO_HELPER_H_INCLUDED


#include <sys/types.h>
#include "libefifo.h"

efifo_err_t open_efifo(efifo_handle_t*, unsigned int);
void start_efifo_writer(efifo_handle_t*, size_t, size_t);


#endif /* ! LIBEFIFO_HELPER_H_INCLUDED */
