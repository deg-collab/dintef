# DANCE_SDK_PLATFORM = kontron_type10

include ${DANCE_SDK_ROOT}/build/top.mk

# install directories
LOCAL_INSTALL_DIR ?= $(shell pwd)/install
SDK_INSTALL_DIR ?= $(DANCE_SDK_DEPS_DIR)

O_FILES := $(C_FILES:.c=.o)

# libeaio devel
LIBEAIO_DEV_CFLAGS := -I../../../../components/libeaio/src
LIBEAIO_DEV_LFLAGS := -L../../../../components/libeaio/src

# libebuf devel
LIBEBUF_DEV_CFLAGS := -I../../../../components/libebuf/src
LIBEBUF_DEV_LFLAGS := -L../../../../components/libebuf/src

# libefifo devel
LIBEFIFO_DEV_CFLAGS := -I../../../../components/libefifo/src
LIBEFIFO_DEV_LFLAGS := -L../../../../components/libefifo/src

# libepci devel
LIBEPCI_DEV_CFLAGS := -I../../../../components/libepci/src
LIBEPCI_DEV_LFLAGS := -L../../../../components/libepci/src

# libpci devel
LIBPCI_DEV_CFLAGS := -I$(DANCE_SDK_DEPS_DIR)/include
LIBPCI_DEV_LFLAGS := -L$(DANCE_SDK_DEPS_DIR)/lib

# all devel

ALL_DEV_CFLAGS := \
 $(LIBEAIO_DEV_CFLAGS) \
 $(LIBEBUF_DEV_CFLAGS) \
 $(LIBEFIFO_DEV_CFLAGS) \
 $(LIBEPCI_DEV_CFLAGS) \
 $(LIBPCI_DEV_CFLAGS)

ALL_DEV_LFLAGS := \
 $(LIBEAIO_DEV_LFLAGS) \
 $(LIBEBUF_DEV_LFLAGS) \
 $(LIBEFIFO_DEV_LFLAGS) \
 $(LIBEPCI_DEV_LFLAGS) \
 $(LIBPCI_DEV_LFLAGS)

CC := $(DANCE_SDK_CROSS_COMPILE)gcc
LD := $(CC)

.PHONY: all clean

all: a.out

a.out: $(O_FILES)
	$(CC) -static -o a.out $(O_FILES) -L. $(ALL_DEV_LFLAGS) -leaio -lebuf -lefifo -lepci -lpci -lrt -lpthread

%.o: %.c
	$(CC) -Wall -fPIC -O2 -I../common -I. $(ALL_DEV_CFLAGS) -c -o $@ $<

clean:
	rm -f $(O_FILES)
	rm -f a.out
