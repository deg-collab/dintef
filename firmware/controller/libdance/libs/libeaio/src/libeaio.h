#ifndef LIBEAIO_H_INCLUDED
# define LIBEAIO_H_INCLUDED 1


#include <sys/types.h>

#ifndef EAIO_CONFIG_THREAD
#define EAIO_CONFIG_THREAD 1
#endif

#if (EAIO_CONFIG_THREAD == 1)
#include <pthread.h>
#endif

#include "libebuf.h"


/* error codes */

typedef enum
{
  EAIO_ERR_SUCCESS = 0,
  EAIO_ERR_FAILURE,
  EAIO_ERR_TIMEOUT,
  EAIO_ERR_FOUND,
  EAIO_ERR_IMPL,
  EAIO_ERR_STATUS
} eaio_err_t;


/* flags */

typedef enum
{
  EAIO_FLAG_DEFAULT = 0,

  /* scheduling flags */
  EAIO_FLAG_SCHED_THREAD = 1 << 0,
  EAIO_FLAG_SCHED_IRQ = 1 << 1,

  /* status update flags */
  EAIO_FLAG_STAT_POLL = 1 << 2,
  EAIO_FLAG_STAT_IRQ = 1 << 3,

  /* direction flag, exclusive */
  EAIO_FLAG_READ = 1 << 4,
  EAIO_FLAG_WRITE = 1 << 5,
} eaio_flags_t;


/* aio status */

typedef enum
{
  EAIO_STATUS_SUCCESS = 0,
  EAIO_STATUS_ERROR,
  EAIO_STATUS_STOPPED,
  EAIO_STATUS_PROGRESS
} eaio_status_t;


/* handle */

typedef struct eaio_handle
{
  /* operation flags */
  eaio_flags_t flags;

  /* internal operation status */
  eaio_status_t status;

  /* completion status, updated only in wait or stop */
  eaio_status_t compl_status;

  /* related buffer */
  ebuf_handle_t* buf;

  /* start end offsets */
  size_t soff;
  size_t eoff;

  /* aio is completed when compl_size bytes transfered */
  size_t compl_size;

  /* size unblocking a wait call, in bytes */
  size_t wait_size;

  /* timeout unblocking a wait call, in milliseconds */
  unsigned int wait_timeout;

  /* start the operation */
  eaio_err_t (*on_start)(void*);

  /* stop the operation */
  eaio_err_t (*on_stop)(void*);

  /* equivalent to select */
  eaio_err_t (*on_poll)(void*, size_t*, eaio_flags_t);

  /* advance the operation by no more than size bytes */
  eaio_err_t (*on_data)(void*, ebuf_handle_t*, size_t, size_t*, eaio_flags_t);

  /* on_xxx opaque data pointer */
  void* on_opaque;

  /* user opaque data pointer */
  void* user_opaque;

#if (EAIO_CONFIG_THREAD == 1)
  /* threaded queue entry */
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  struct eaio_handle* volatile next;
  volatile unsigned int do_stop;
#endif /* EAIO_CONFIG_THREAD */

} eaio_handle_t;


/* timeout special values */

#define EAIO_TIMEOUT_NONE ((unsigned int)-1)
#define EAIO_TIMEOUT_ZERO ((unsigned int)0)


/* exported routines */

/* library constructors */

eaio_err_t eaio_init_lib(eaio_flags_t);
eaio_err_t eaio_fini_lib(void);

/* handle constructors */

eaio_err_t eaio_open(eaio_handle_t*);
eaio_err_t eaio_close(eaio_handle_t*);

/* scheduling method setters */

eaio_err_t eaio_set_sched(eaio_handle_t*, eaio_flags_t);

static inline eaio_err_t eaio_set_thread_sched(eaio_handle_t* aio)
{
  return eaio_set_sched(aio, EAIO_FLAG_SCHED_THREAD);
}

static inline eaio_err_t eaio_set_irq_sched(eaio_handle_t* aio)
{
  return eaio_set_sched(aio, EAIO_FLAG_SCHED_IRQ);
}

/* status update method setters */

eaio_err_t eaio_set_stat(eaio_handle_t*, eaio_flags_t);

static inline eaio_err_t eaio_set_poll_stat(eaio_handle_t* aio)
{
  return eaio_set_stat(aio, EAIO_FLAG_STAT_POLL);
}

static inline eaio_err_t eaio_set_irq_stat(eaio_handle_t* aio)
{
  return eaio_set_stat(aio, EAIO_FLAG_STAT_IRQ);
}

/* aio control */

eaio_err_t eaio_start_continuous(eaio_handle_t*);
eaio_err_t eaio_start_discrete(eaio_handle_t*, size_t);
eaio_err_t eaio_stop(eaio_handle_t*, ebuf_handle_t*, ebuf_handle_t*);
eaio_err_t eaio_continue(eaio_handle_t*);
eaio_err_t eaio_wait(eaio_handle_t*, ebuf_handle_t*, ebuf_handle_t*);
eaio_err_t eaio_wait_any(eaio_handle_t**, ebuf_handle_t*, ebuf_handle_t*);

/* internal setters */

static inline void eaio_set_buffer(eaio_handle_t* aio, ebuf_handle_t* buf)
{
  aio->buf = buf;
}

static inline void eaio_set_user_opaque(eaio_handle_t* aio, void* opaque)
{
  aio->user_opaque = opaque;
}

static inline void* eaio_get_user_opaque(eaio_handle_t* aio)
{
  return aio->user_opaque;
}

static inline void eaio_set_wait_timeout(eaio_handle_t* aio, unsigned int timeout)
{
  aio->wait_timeout = timeout;
}

static inline void eaio_set_wait_size(eaio_handle_t* aio, size_t size)
{
  aio->wait_size = size;
}

static inline eaio_status_t eaio_get_compl_status(eaio_handle_t* aio)
{
  return aio->compl_status;
}


#endif /* ! LIBEAIO_H_INCLUDED */
