#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#include "libeaio.h"
#include "libebuf.h"

#if (EAIO_CONFIG_THREAD == 1)
#include <errno.h>
#include <time.h>
#include <sched.h>
#include <pthread.h>
#endif /* EAIO_CONFIG_THREAD */


#define EAIO_CONFIG_DEBUG 1
#if (EAIO_CONFIG_DEBUG == 1)
#include <stdio.h>
#define PRINTF(__s, ...) do { printf(__s, ## __VA_ARGS__); } while (0)
#define PERROR() printf("[!] %s:%d\n", __FILE__, __LINE__)
#define ASSUME(__x) do { if (!(__x)) printf("[!] %s:%d\n", __FILE__, __LINE__); } while (0)
#else
#define PRINTF(__s, ...)
#define PERROR()
#define ASSUME(__x)
#endif /* EAIO_CONFIG_DEBUG */


#if 0 /* eram aio handlers */

static eaio_err_t on_aio_start(void* opaque)
{
  eram_handle_t* const ram = opaque;
  return EAIO_ERR_IMPL;
}

static eaio_err_t on_aio_stop(void* opaque)
{
  eram_handle_t* const ram = aio->opaque;
  return EAIO_ERR_IMPL;
}

static eaio_err_t on_aio_poll
(void* opaque, size_t* rsize, size_t* wsize, size_t* esize)
{
  eram_handle_t* const ram = opaque;
  return EAIO_ERR_IMPL;
}

static eaio_err_t on_aio_data
(void* opaque, ebuf_handle_t* buf, size_t off, size_t* size, eaio_flag_t flags)
{
  eram_handle_t* const ram = opaque;
  return EAIO_ERR_IMPL;
}

eram_err_t eram_open_aio(eram_handle_t* eram, eaio_handle_t* aio)
{
  if (eaio_open(aio) != EAIO_ERR_SUCCESS)
    return EFIFO_ERR_FAILURE;

  aio->on_start = on_aio_start;
  aio->on_stop = on_aio_stop;
  aio->on_poll = on_aio_poll;
  aio->on_data = on_aio_data;
  aio->opaque = eram;

  return ERAM_ERR_SUCCESS;
}

#endif /* eram aio handlers */


#if (EAIO_CONFIG_THREAD == 1)

/* aio auxiliary threaded queue */

typedef struct eaio_queue
{
  /* align on cache line size to avoid false sharing */
  struct
  {
    eaio_handle_t* volatile head;
    eaio_handle_t* volatile tail;
    volatile unsigned int is_done;
  } line __attribute__((aligned(64)));

  pthread_mutex_t mutex;
  pthread_cond_t cond;

#if 0 /* TODO_WAIT_ANY */
  pthread_mutex_t any_mutex;
  pthread_cond_t any_cond;
  volatile unsigned int any_word;
#endif /* TODO_WAIT_ANY */

  pthread_t thread;

} eaio_queue_t;

static eaio_queue_t eaio_queue;

static void* eaio_queue_entry(void*);

static eaio_err_t eaio_queue_init(eaio_queue_t* q)
{
  q->line.head = NULL;
  q->line.tail = NULL;

  q->line.is_done = 0;

  pthread_mutex_init(&q->mutex, NULL);
  pthread_cond_init(&q->cond, NULL);

#if 0 /* TODO_WAIT_ANY */
  pthread_mutex_init(&q->any_mutex, NULL);
  pthread_cond_init(&q->any_cond, NULL);
  q->any_word = 0;
#endif /* TODO_WAIT_ANY */

  if (pthread_create(&q->thread, NULL, eaio_queue_entry, q))
    return EAIO_ERR_FAILURE;

  return EAIO_ERR_SUCCESS;
}

static void eaio_queue_fini(eaio_queue_t* q)
{
  void* unused_retval;

  /* signal the queue thread */
  pthread_mutex_lock(&q->mutex);
  q->line.is_done = 1;
  pthread_cond_signal(&q->cond);
  pthread_mutex_unlock(&q->mutex);

  pthread_join(q->thread, &unused_retval);

  pthread_cond_destroy(&q->cond);
  pthread_mutex_destroy(&q->mutex);

#if 0 /* TODO_WAIT_ANY */
  pthread_cond_destroy(&q->any_cond);
  pthread_mutex_destroy(&q->any_mutex);
#endif /* TODO_WAIT_ANY */
}

static eaio_err_t eaio_queue_push(eaio_queue_t* q, eaio_handle_t* aio)
{
  /* push at tail */

  pthread_mutex_lock(&q->mutex);

  /* empty list */
  if (q->line.head == NULL) q->line.head = aio;
  else q->line.tail->next = aio;
  q->line.tail = aio;
  aio->next = NULL;

  __sync_synchronize();

  pthread_cond_signal(&q->cond);

  pthread_mutex_unlock(&q->mutex);

  return EAIO_ERR_SUCCESS;
}

static void eaio_queue_peek_locked
(eaio_queue_t* q, eaio_handle_t** head, eaio_handle_t** tail)
{
  *head = q->line.head;
  *tail = q->line.tail;
  q->line.head = NULL;
  q->line.tail = NULL;
}

static void eaio_queue_peek
(eaio_queue_t* q, eaio_handle_t** head, eaio_handle_t** tail)
{
  /* do not lock no aio available */
  if (q->line.head == NULL)
  {
    *head = NULL;
    *tail = NULL;
    return ;
  }

  pthread_mutex_lock(&q->mutex);
  eaio_queue_peek_locked(q, head, tail);
  pthread_mutex_unlock(&q->mutex);
}

static void eaio_queue_pop
(eaio_queue_t* q, eaio_handle_t** head, eaio_handle_t** tail)
{
  /* lock before waiting to avoid race condition */
  pthread_mutex_lock(&q->mutex);

  while (1)
  {
    /* is_done priority over push */
    if (q->line.is_done == 1)
    {
      *head = NULL;
      *tail = NULL;
      break ;
    }

    eaio_queue_peek_locked(q, head, tail);
    if (*head != NULL) break ;

    /* mutex unlocked at entry */
    pthread_cond_wait(&q->cond, &q->mutex);
    /* mutex relocked at exit */
  }

  pthread_mutex_unlock(&q->mutex);
}

static void complete_aio
(
 eaio_handle_t* aio, eaio_status_t status,
 eaio_handle_t* pre,
 eaio_handle_t** head, eaio_handle_t** tail
)
{
  /* unlink the aio before signaling */
  if (pre == NULL) *head = aio->next;
  else pre->next = aio->next;
  if (aio->next == NULL) *tail = pre;

  /* complete the aio */
  pthread_mutex_lock(&aio->mutex);
  aio->status = status;
  pthread_cond_signal(&aio->cond);
  pthread_mutex_unlock(&aio->mutex);
}

static void update_aio_eoff(eaio_handle_t* aio, size_t eoff)
{
  /* update the aio eoff index */
  pthread_mutex_lock(&aio->mutex);
  aio->eoff = eoff;
  pthread_cond_signal(&aio->cond);
  pthread_mutex_unlock(&aio->mutex);
}

static unsigned int is_discrete_aio(const eaio_handle_t* aio)
{
  /* non zero if discrete (ie. non continuous) */

  return (aio->compl_size != 0);
}

#if 0 /* TODO_WAIT_ANY */
static void signal_any(eaio_queue_t* q)
{
  pthread_mutex_lock(&q->any_lock);
  q->any_word = 1;
  pthread_cond_signal(&q->any_cond);
  pthread_mutex_unlock(&q->any_lock);
}
#endif /* TODO_WAIT_ANY */

static void* eaio_queue_entry(void* arg)
{
  eaio_queue_t* const q = arg;
  eaio_handle_t* head = NULL;
  eaio_handle_t* tail = NULL;
  eaio_handle_t* new_head;
  eaio_handle_t* new_tail;
  eaio_handle_t* aio;
  eaio_handle_t* pre;
  eaio_handle_t* nex;
  eaio_status_t status;

#if 0 /* unused, rt max scheduling policy */

  /* warning: using a realtime scheduling policy reduces latency */
  /* but prevent the waiting thread to be scheduled. thus, the */
  /* scheduling parmaters should be adjusted if the occupancy */
  /* becomes higher than a given threshold. */

  /* note: setting the thread scheduling priority in pthread_create */
  /* attributes did not work, so we do this here */

  static const int policy = SCHED_FIFO;
  struct sched_param param;
  param.sched_priority = sched_get_priority_max(policy);
  pthread_setschedparam(pthread_self(), policy, &param);

#endif /* unused */

  while (1)
  {
    /* peek or wait for a new aio to occur */
    new_head = NULL;
    new_tail = NULL;
    if (head == NULL) eaio_queue_pop(q, &new_head, &new_tail);
    else eaio_queue_peek(q, &new_head, &new_tail);

    /* check if not done */
    if (q->line.is_done == 1) break ;

    /* start every newly pushed aios */
    pre = NULL;
    for (aio = new_head; aio != NULL; aio = nex)
    {
      /* save next as aio may become invalid */
      nex = aio->next;

      if (aio->on_start(aio->on_opaque) != EAIO_ERR_SUCCESS)
      {
	complete_aio(aio, EAIO_STATUS_ERROR, pre, &new_head, &new_tail);
	/* do not update pre pointer */
	continue ;
      }

      pre = aio;
    }

    /* merge new list to current list */
    if (new_head != NULL)
    {
      /* merge the 2 lists */
      if (head == NULL) head = new_head;
      else tail->next = new_head;
      tail = new_tail;
    }

    /* continue aios, one chunk at a time */
    pre = NULL;
    for (aio = head; aio != NULL; aio = nex)
    {
      eaio_err_t err;
      size_t size;
      size_t eoff;

      /* save next as aio may become invalid */
      nex = aio->next;

      /* check if stopped */
      if (aio->do_stop == 1)
      {
	aio->on_stop(aio->on_opaque);
	status = EAIO_STATUS_STOPPED;
	goto do_complete_aio;
      }

      /* check if data available */
      err = aio->on_poll(aio->on_opaque, &size, aio->flags);
      if (err != EAIO_ERR_SUCCESS)
      {
	status = EAIO_STATUS_ERROR;
	goto do_complete_aio;
      }

      /* work on a local copy, visible at update only */
      eoff = aio->eoff;

      /* in discrete mode, check for aio->size overflow */
      if (is_discrete_aio(aio))
      {
	if ((eoff + size) > aio->compl_size) size = aio->compl_size - eoff;
      }
      else /* continuous mode */
      {
	/* in ring buffer mode, eoff to buf->usize is actually 0 */
	if (eoff == aio->buf->usize) eoff = 0;

	/* continuous mode, check for buf->usize overflow */
	if ((eoff + size) > aio->buf->usize) size = aio->buf->usize - eoff;
      }

      if (size)
      {
	err = aio->on_data(aio->on_opaque, aio->buf, eoff, &size, aio->flags);

	/* add the size before checking for error */
	eoff += size;
	update_aio_eoff(aio, eoff);

#if 0 /* TODO_WAIT_ANY */
	signal_any(q);
#endif /* TODO_WAIT_ANY */

	/* let a chance for a waiting thread to schedule */
	sched_yield();

	if (err != EAIO_ERR_SUCCESS)
	{
	  status = EAIO_STATUS_ERROR;
	  goto do_complete_aio;
	}

	/* in discrete mode, check for all bytes transfered */
	if (is_discrete_aio(aio))
	{
	  if (eoff == aio->compl_size)
	  {
	    status = EAIO_STATUS_SUCCESS;
	    goto do_complete_aio;
	  }
	}
      }

      /* update previous aio and continue without completing */
      pre = aio;
      continue ;

    do_complete_aio:
      complete_aio(aio, status, pre, &head, &tail);
#if 0 /* TODO_WAIT_ANY */
      signal_any(q);
#endif /* TODO_WAIT_ANY */

      /* let a chance for a waiting thread to schedule */
      sched_yield();
    }
  }

  return NULL;
}

#endif /* EAIO_CONFIG_THREAD */


/* library constructors */

static eaio_flags_t eaio_lib_flags = EAIO_FLAG_DEFAULT;

eaio_err_t eaio_init_lib(eaio_flags_t flags)
{
  if (flags == EAIO_FLAG_DEFAULT)
  {
    flags = EAIO_FLAG_SCHED_THREAD | EAIO_FLAG_SCHED_IRQ | EAIO_FLAG_STAT_IRQ;
  }

  eaio_lib_flags = flags;

  if (flags & EAIO_FLAG_SCHED_THREAD)
  {
#if (EAIO_CONFIG_THREAD == 1)
    if (eaio_queue_init(&eaio_queue) == -1)
      goto on_error_0;
#else /* EAIO_CONFIG_THREAD == 0 */
    return EAIO_ERR_FAILURE;
#endif /* EAIO_CONFIG_THREAD */
  }

  if (flags & (EAIO_FLAG_SCHED_IRQ | EAIO_FLAG_STAT_IRQ))
  {
    /* TODO: ensure driver is loaded */
    /* TODO: open ebone fd */
  }

  return EAIO_ERR_SUCCESS;

 on_error_0:
  return EAIO_ERR_FAILURE;
}

eaio_err_t eaio_fini_lib(void)
{
#if (EAIO_CONFIG_THREAD == 1)
  if (eaio_lib_flags & EAIO_FLAG_SCHED_THREAD)
  {
    eaio_queue_fini(&eaio_queue);
  }
#endif /* EAIO_CONFIG_THREAD */

  return EAIO_ERR_SUCCESS;
}


/* aio handle constructor */

eaio_err_t eaio_open(eaio_handle_t* aio)
{
  aio->flags = EAIO_FLAG_DEFAULT;
  aio->status = EAIO_STATUS_PROGRESS;
  aio->compl_status = EAIO_STATUS_PROGRESS;
  aio->buf = NULL;
  aio->soff = 0;
  aio->eoff = 0;
  aio->compl_size = 0;
  aio->wait_size = 0;
  aio->wait_timeout = EAIO_TIMEOUT_NONE;
  aio->on_start = NULL;
  aio->on_stop = NULL;
  aio->on_poll = NULL;
  aio->on_data = NULL;
  aio->on_opaque = NULL;
  aio->user_opaque = NULL;

#if (EAIO_CONFIG_THREAD == 1)
  if (eaio_lib_flags & EAIO_FLAG_SCHED_THREAD)
  {
    pthread_mutex_init(&aio->mutex, NULL);
    pthread_cond_init(&aio->cond, NULL);
    aio->next = NULL;
    aio->do_stop = 0;
  }
#endif /* EAIO_CONFIG_THREAD */

  return EAIO_ERR_SUCCESS;
}

eaio_err_t eaio_close(eaio_handle_t* aio)
{
#if (EAIO_CONFIG_THREAD == 1)
  if (eaio_lib_flags & EAIO_FLAG_SCHED_THREAD)
  {
    pthread_cond_destroy(&aio->cond);
    pthread_mutex_destroy(&aio->mutex);
  }
#endif /* EAIO_CONFIG_THREAD */

  return EAIO_ERR_SUCCESS;
}

/* accessors */

eaio_err_t eaio_set_sched(eaio_handle_t* aio, eaio_flags_t flags)
{
  aio->flags |= flags;

  return EAIO_ERR_SUCCESS;
}

eaio_err_t eaio_set_stat(eaio_handle_t* aio, eaio_flags_t flags)
{
  aio->flags |= flags;

  return EAIO_ERR_SUCCESS;
}

/* aio control routines */

static eaio_err_t start_common(eaio_handle_t* aio)
{
  aio->status = EAIO_STATUS_PROGRESS;
  aio->compl_status = EAIO_STATUS_PROGRESS;

  /* could have been set by prior eaio_stop */
  aio->do_stop = 0;

#if (EAIO_CONFIG_THREAD == 1)
  if (aio->flags & EAIO_FLAG_SCHED_THREAD)
  {
    const eaio_err_t err = eaio_queue_push(&eaio_queue, aio);
    if (err != EAIO_ERR_SUCCESS) return err;
  }
  else
#endif /* EAIO_CONFIG_THREAD */
  if (aio->flags & EAIO_FLAG_SCHED_IRQ)
  {
    /* do the transfer here */
    return aio->on_start(aio);
  }

  return EAIO_ERR_SUCCESS;
}

eaio_err_t eaio_continue(eaio_handle_t* aio)
{
  /* already completed */
  if (aio->compl_status != EAIO_STATUS_STOPPED)
    return EAIO_ERR_STATUS;

  return start_common(aio);
}

static eaio_err_t start_continuous_or_discrete(eaio_handle_t* aio)
{
  /* clear any field that could have been previously set */

  aio->soff = 0;
  aio->eoff = 0;

  return start_common(aio);
}

eaio_err_t eaio_start_continuous(eaio_handle_t* aio)
{
  return start_continuous_or_discrete(aio);
}

eaio_err_t eaio_start_discrete(eaio_handle_t* aio, size_t size)
{
  aio->compl_size = size;
  return start_continuous_or_discrete(aio);
}

static eaio_status_t get_aio_status(const eaio_handle_t* aio)
{
  /* concurrently written */
  return *(volatile eaio_status_t*)&aio->status;
}

static size_t get_aio_eoff(const eaio_handle_t* aio)
{
  /* concurrently written */
  return *(volatile size_t*)&aio->eoff;
}

static void make_ref_buf(ebuf_handle_t* buf, void* uaddr, size_t size)
{
  buf->flags = EBUF_FLAG_DEFAULT | EBUF_FLAG_REF;
  buf->uaddr = uaddr;
  buf->usize = size;
  buf->paddr = NULL;
}

static int consume_compl_buf
(eaio_handle_t* aio, size_t eoff, ebuf_handle_t* lbuf, ebuf_handle_t* rbuf)
{
  /* since the backing buffer is managed as a ring, the resulting */
  /* available buffer can be splitted into 2 buffers: lbuf, rbuf */
  /* if the resulting buffer is not splitted, rbuf is be zero sized */

  /* skip the lbuf if NULL */
  if (lbuf != NULL)
  {
    /* retrieve the completed buffer from [ aio->soff, eoff ] */

    ASSUME(rbuf != NULL);

    if (aio->soff < eoff)
    {
      /* lbuf only is valid */
      make_ref_buf(lbuf, aio->buf->uaddr + aio->soff, eoff - aio->soff);
      make_ref_buf(rbuf, NULL, 0);
    }
    else /* aio->soff >= eoff */
    {
      /* lbuf rbuf valid */
      const size_t lsize = aio->buf->usize - aio->soff;
      const size_t rsize = eoff;
      make_ref_buf(lbuf, aio->buf->uaddr + aio->soff, lsize);
      make_ref_buf(rbuf, aio->buf->uaddr, rsize);
    }
  }

  /* update soff */
  aio->soff = eoff;

  return 0;
}

eaio_err_t eaio_stop
(eaio_handle_t* aio, ebuf_handle_t* lbuf, ebuf_handle_t* rbuf)
{
  eaio_err_t err = EAIO_ERR_SUCCESS;

#if (EAIO_CONFIG_THREAD == 1)
  if (aio->flags & EAIO_FLAG_SCHED_THREAD)
  {
    pthread_mutex_lock(&aio->mutex);

    /* stop and wait for completion */
    aio->do_stop = 1;

    while (1)
    {
      aio->compl_status = get_aio_status(aio);
      if (aio->compl_status != EAIO_STATUS_PROGRESS) break ;
      pthread_cond_wait(&aio->cond, &aio->mutex);
    }

    pthread_mutex_unlock(&aio->mutex);

    /* consume the buffer. aio is stopped, ok to access eoff without lock */
    if (consume_compl_buf(aio, get_aio_eoff(aio), lbuf, rbuf))
    {
      err = EAIO_ERR_FAILURE;
      goto on_error;
    }

    err = EAIO_ERR_SUCCESS;
  }
  else
#endif /* EAIO_CONFIG_THREAD */
  if (aio->flags & EAIO_FLAG_SCHED_IRQ)
  {
    err = EAIO_ERR_IMPL;
    goto on_error;
  }

 on_error:
  return err;
}

static void timeout_to_timespec(struct timespec* ts, unsigned int tm)
{
  /* tm in milliseconds */
  clock_gettime(CLOCK_REALTIME, ts);
  ts->tv_sec += tm / 1000;
  ts->tv_nsec += (tm % 1000) * 1000;
}

eaio_err_t eaio_wait
(eaio_handle_t* aio, ebuf_handle_t* lbuf, ebuf_handle_t* rbuf)
{
  /* wait for the aio to complete or to contain at least size bytes */

  eaio_err_t err = EAIO_ERR_FAILURE;

#if (EAIO_CONFIG_THREAD == 1)
  if (aio->flags & EAIO_FLAG_SCHED_THREAD)
  {
    const size_t wait_size = aio->wait_size;
    const unsigned int timeout = aio->wait_timeout;
    struct timespec ts;
    size_t avail_size;
    int wait_errno;
    size_t eoff;

    if (timeout != EAIO_TIMEOUT_NONE) timeout_to_timespec(&ts, timeout);

    pthread_mutex_lock(&aio->mutex);

    while (1)
    {
      wait_errno = 0;

      /* compute available size under lock */
      eoff = get_aio_eoff(aio);
      if (eoff >= aio->soff) avail_size = eoff - aio->soff;
      else avail_size = aio->buf->usize - aio->soff + eoff;

      /* wait on completion or available size >= size */
      if (avail_size >= wait_size) break ;

      aio->compl_status = get_aio_status(aio);
      if (aio->compl_status != EAIO_STATUS_PROGRESS) break ;

      if (timeout == EAIO_TIMEOUT_NONE)
      {
	wait_errno = pthread_cond_wait(&aio->cond, &aio->mutex);
      }
      else
      {
	wait_errno = pthread_cond_timedwait(&aio->cond, &aio->mutex, &ts);
	if (wait_errno == ETIMEDOUT) break ;
      }
    }

    pthread_mutex_unlock(&aio->mutex);

    if (wait_errno != 0)
    {
      err = EAIO_ERR_TIMEOUT;
      goto on_error;
    }

    /* consume the buffer */
    if (consume_compl_buf(aio, eoff, lbuf, rbuf))
    {
      err = EAIO_ERR_FAILURE;
      goto on_error;
    }

    /* success */
    err = EAIO_ERR_SUCCESS;
  }
  else
#endif /* EAIO_CONFIG_THREAD */
  if (aio->flags & EAIO_FLAG_SCHED_IRQ)
  {
    /* IRQ already scheduled */
    err = EAIO_ERR_IMPL;
    goto on_error;
  }

 on_error:
  return err;
}

#if 0 /* TODO_WAIT_ANY */
eaio_err_t eaio_wait_any
(eaio_handle_t** aio, ebuf_handle_t* lbuf, ebuf_handle_t* rbuf)
{
  /* wait for any aio to complete or to contain at least size bytes */

  eaio_err_t err = EAIO_ERR_FAILURE;

#if (EAIO_CONFIG_THREAD == 1)
  if (aio->flags & EAIO_FLAG_SCHED_THREAD)
  {
    /* wait for any event to be signaled */

    eaio_queue_t* const q = &eaio_queue;
    struct timespec ts;
    int wait_errno;
    size_t i;

    if (timeout != EAIO_TIMEOUT_NONE) timeout_to_timespec(&ts, timeout);

    pthread_mutex_lock(&q->any_mutex);

    while (q->any_word == 0)
    {
      if (timeout == EAIO_TIMEOUT_NONE)
      {
	wait_errno = pthread_cond_wait(&q->any_cond, &q->any_mutex);
      }
      else
      {
	wait_errno = pthread_cond_timedwait(&q->any_cond, &q->any_mutex, &ts);
	if (wait_errno == ETIMEDOUT) break ;
      }
    }

    /* reset any_word under lock */
    q->any_word = 0;

    pthread_mutex_unlock(&q->any_mutex);

    /* look for any signaled aio */

    /* make it volatile to account for concurrent updates */
    size_t avail_size;
    int wait_errno;
    size_t eoff;

    pthread_mutex_lock(&aio->mutex);

    while (1)
    {
      wait_errno = 0;

      /* compute available size under lock */
      eoff = get_aio_eoff(aio);
      if (eoff >= aio->soff) avail_size = eoff - aio->soff;
      else avail_size = aio->buf->usize - aio->soff + eoff;

      /* wait on completion or available size >= size */
      if (avail_size >= size) break ;
      else if (get_aio_status(aio) != EAIO_STATUS_PROGRESS) break ;

      if (timeout == EAIO_TIMEOUT_NONE)
      {
	wait_errno = pthread_cond_wait(&aio->cond, &aio->mutex);
      }
      else
      {
	struct timespec ts;

	clock_gettime(CLOCK_REALTIME, &ts);
	ts.tv_sec += timeout / 1000;
	ts.tv_nsec += (timeout % 1000) * 1000;

	wait_errno = pthread_cond_timedwait(&aio->cond, &aio->mutex, &ts);
	if (wait_errno == ETIMEDOUT) break ;
      }
    }

    pthread_mutex_unlock(&aio->mutex);

    if (wait_errno != 0)
    {
      err = EAIO_ERR_TIMEOUT;
      goto on_error;
    }

    /* consume the buffer */
    if (consume_compl_buf(aio, eoff, buf))
    {
      err = EAIO_ERR_FAILURE;
      goto on_error;
    }

    /* success */
    err = EAIO_ERR_SUCCESS;
  }
  else
#endif /* EAIO_CONFIG_THREAD */
  if (aio->flags & EAIO_FLAG_SCHED_IRQ)
  {
    /* IRQ already scheduled */
    err = EAIO_ERR_IMPL;
    goto on_error;
  }

 on_error:
  return err;
}
#endif /* TODO_WAIT_ANY */
