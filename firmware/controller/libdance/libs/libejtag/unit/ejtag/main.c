#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/reboot.h>
#include <sys/reboot.h>
#include "libejtag.h"


int debug = 0;
#define LOG(...)  LOG_L(0, ##__VA_ARGS__)
#define LOG0(...) LOG_L(0, ##__VA_ARGS__)
#define LOG1(...) LOG_L(1, ##__VA_ARGS__)
#define LOG_L(lvl, _fmt, ...)            \
do {                                     \
  if(debug) {                            \
    ejtag_log_write("EJTAG", lvl, __FILE__, __LINE__, _fmt, ##__VA_ARGS__); \
  }                                      \
} while (0)

#define LOG_ERROR(_fmt, ...)             \
do {                                     \
  ejtag_log_write("EJTAG ERROR", 0, __FILE__, __LINE__, _fmt, ##__VA_ARGS__); \
} while (0)



static void sync_reboot_cpu(void)
{
  sync();
  reboot(LINUX_REBOOT_CMD_RESTART);
}


typedef struct cmd_info
{
#define CMD_FLAG_HELP (1 << 0)
#define CMD_FLAG_SCAN (1 << 1)
#define CMD_FLAG_PROG (1 << 2)
#define CMD_FLAG_REBOOT (1 << 3)
#define CMD_FLAG_BIT (1 << 4)
#define CMD_FLAG_FT232H (1 << 5)
#define CMD_FLAG_I2C (1 << 6)
#define CMD_FLAG_AUTO (1 << 7)
#define CMD_FLAG_POS (1 << 8)
#define CMD_FLAG_FREQ (1 << 9)
  uint32_t flags;
  const char* bit;
  uint32_t pos;
  unsigned int freq;
} cmd_info_t;


static uint32_t arg_to_uint32(const char* s)
{
  int base = 10;
  if ((strlen(s) > 2) && (s[0] == '0') && (s[1] == 'x')) base = 16;
  return (uint32_t)strtoul(s, NULL, base);
}


static int get_cmd_info(cmd_info_t* cmd, int ac, char** av)
{
  size_t i;

  cmd->flags = 0;
  cmd->bit = NULL;
  cmd->pos = 0;
  cmd->freq = 0;

  if (ac == 0) goto on_error;

  /* count debug options at the end of argins */
  for (i = (ac-1); i > 0; i--)
  {
    if (strcmp(av[i], "-d") == 0)
    {
      debug++;
      ac--;
    }
    else
      break ;
  }


  if (ac % 2) goto on_error;

  for (i = 0; i != ac; i += 2)
  {
    const char* const k = av[i + 0];
    const char* const v = av[i + 1];

    if (strcmp(k, "-a") == 0)
    {
      if (strcmp(v, "help") == 0) cmd->flags |= CMD_FLAG_HELP;
      else if (strcmp(v, "scan") == 0) cmd->flags |= CMD_FLAG_SCAN;
      else if (strcmp(v, "prog") == 0) cmd->flags |= CMD_FLAG_PROG;
      else if (strcmp(v, "reboot") == 0) cmd->flags |= CMD_FLAG_REBOOT;
      else goto on_error;
    }
    else if (strcmp(k, "-t") == 0)
    {
      if (strcmp(v, "ft232h") == 0) cmd->flags |= CMD_FLAG_FT232H;
      else if (strcmp(v, "i2c") == 0) cmd->flags |= CMD_FLAG_I2C;
      else if (strcmp(v, "auto") == 0) cmd->flags |= CMD_FLAG_AUTO;
      else goto on_error;
    }
    else if (strcmp(k, "-f") == 0)
    {
      cmd->flags |= CMD_FLAG_BIT;
      cmd->bit = v;
    }
    else if (strcmp(k, "-p") == 0)
    {
      cmd->flags |= CMD_FLAG_POS;
      cmd->pos = arg_to_uint32(v);
    }
    else if (strcmp(k, "-freq") == 0)
    {
      cmd->flags |= CMD_FLAG_FREQ;
      cmd->freq = (unsigned int)arg_to_uint32(v);
    }
    else
    {
      goto on_error;
    }
   }

  return 0;

 on_error:
  cmd->flags |= CMD_FLAG_HELP;
  return -1;
}

const char app_ident[] =
#include "_idfile.h"
;

static void do_help(const char* s)
{
  printf("================\n%s\n", app_ident);
  printf("================\n");
  printf("command line options\n");
  printf("-a <action>\n");
  printf("-a scan       : scan the JTAG chain\n");
  printf("-a prog       : program the FPGA\n");
  printf("-a reboot     : reboot the CPU after programming\n");
  printf("-a help       : print this help\n");
  printf("-t <link_type>\n");
  printf("-t auto       : automatically detect link type (default)\n");
  printf("-t ft232h     : use FT232H\n");
  printf("-t i2c        : use I2C\n");
  printf("-f <filename> : the bitstream path\n");
  printf("-p <chain_pos>: the JTAG chain position (default to 0)\n");
  printf("-freq <freq>  : the JTAG frequency, in Hz (default to max)\n");
  printf("-d [-d]       : increase debug print out(optional)\n");
  printf("\n");
  printf("* examples\n");
  printf("scan the JTAG chain: %s -a scan -t ft232h\n", s);
  printf("program the FPGA: %s -a prog -t ft232h -f file.bit -a reboot\n", s);
}


int main(int ac, char** av)
{
  int err = -1;
  ejtag_handle_t ejtag;
  ejtag_desc_t desc;
  const ejtag_dev_info_t* di;
  cmd_info_t cmd;
  size_t n;
  size_t i;

  err = get_cmd_info(&cmd, ac - 1, av + 1);
  if (cmd.flags & CMD_FLAG_HELP) do_help(*av);
  if (err) goto on_error_0;

  err = -1;

  if (ejtag_init_lib()) goto on_error_0;

  /* need two -d to get lib loggin */
  if (debug > 1)
  {
    if (ejtag_log_lib((debug - 1)))
    {
       LOG_ERROR("invalid debug level\n");
       goto on_error_0;
    }
  }


  ejtag_init_desc(&desc);

  if (cmd.flags & CMD_FLAG_FT232H)
  {
    desc.flags |= EJTAG_FLAG_FTDI;
  }
  else if (cmd.flags & CMD_FLAG_I2C)
  {
    desc.flags |= EJTAG_FLAG_I2C;
  }
  else
  {
    /* default to CMD_FLAG_AUTO */
    const uint32_t x = ejtag_probe_link();
    if (x == 0)
    {
      LOG_ERROR("could not detect link type");
      goto on_error_1;
    }

    desc.flags |= x;
  }

  if (cmd.flags & CMD_FLAG_FREQ)
  {
    desc.jtag_freq = cmd.freq;
  }

  LOG("Getting access to JTAG...");
  if (ejtag_open(&ejtag, &desc))
  {
    LOG_ERROR("unable to get access to JTAG");
    goto on_error_1;
  }

  if (cmd.flags & CMD_FLAG_SCAN)
  {
    LOG("Scanning JTAG chain...");
    if (ejtag_detect_chain(&ejtag, &di, &n))
    {
      LOG_ERROR("unable to get JTAG chain");
      goto on_error_2;
    }

    printf("%zu devices detected in JTAG chain:\n", n);
    for (i = 0; i != n; ++i)
    {
      const uint32_t id_code = di[i].id_code;
      const char* const name = ejtag_id_code_to_name(&ejtag, id_code);
      size_t         ir_len  = di[i].ir_len;
      printf("pos: %zu, id: 0x%08x, name: %s, IR len: %dbits\n", 
        i, id_code, name, ir_len);
    }
  }
  else if (cmd.flags & CMD_FLAG_PROG)
  {
    size_t pos;

    if ((cmd.flags & CMD_FLAG_BIT) == 0)
    {
      LOG_ERROR("missing bit file");
      do_help(*av);
      goto on_error_2;
    }

    if (cmd.flags & CMD_FLAG_POS)
    { 
      LOG("Scanning JTAG chain...");
      if (ejtag_detect_chain(&ejtag, &di, &n))
      {
        LOG_ERROR("unable to get JTAG chain");
        goto on_error_2;
      }
      if ((cmd.pos < 0) || (cmd.pos >= n))
      {
        LOG_ERROR("invalid chain position");
        goto on_error_2;
      }
  
      pos = cmd.pos;
    } 
    else 
    {
      pos = 0;
    }

    LOG("Programming FPGA...");
    if (ejtag_program_bit_file(&ejtag, pos, cmd.bit))
    {
      LOG_ERROR("programming failed");
      goto on_error_2;
    }
  }
  else
  {
    LOG_ERROR("missing action");
    do_help(*av);
    goto on_error_2;
  }

  err = 0;

 on_error_2:
  ejtag_close(&ejtag);
 on_error_1:
  ejtag_fini_lib();
 on_error_0:

  if (!(cmd.flags & CMD_FLAG_SCAN))
  {
    if (err) printf("failure\n");
    else printf("success\n");
  }

  if (err == 0)
  {
    if (cmd.flags & CMD_FLAG_REBOOT) sync_reboot_cpu();
  }

  return err;
}
