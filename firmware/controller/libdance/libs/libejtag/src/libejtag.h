#ifndef LIBEJTAG_H_INCLUDED
#define LIBEJTAG_H_INCLUDED


#include <stdint.h>
#include <sys/types.h>


typedef enum
{
  EJTAG_TAP_STATE_TEST_LOGIC_RESET = 0,
  EJTAG_TAP_STATE_RUN_TEST_IDLE = 1,
  EJTAG_TAP_STATE_SELECT_DR_SCAN = 2,
  EJTAG_TAP_STATE_CAPTURE_DR = 3,
  EJTAG_TAP_STATE_SHIFT_DR = 4,
  EJTAG_TAP_STATE_EXIT1_DR = 5,
  EJTAG_TAP_STATE_PAUSE_DR = 6,
  EJTAG_TAP_STATE_EXIT2_DR = 7,
  EJTAG_TAP_STATE_UPDATE_DR = 8,
  EJTAG_TAP_STATE_SELECT_IR_SCAN = 9,
  EJTAG_TAP_STATE_CAPTURE_IR = 10,
  EJTAG_TAP_STATE_SHIFT_IR = 11,
  EJTAG_TAP_STATE_EXIT1_IR = 12,
  EJTAG_TAP_STATE_PAUSE_IR = 13,
  EJTAG_TAP_STATE_EXIT2_IR = 14,
  EJTAG_TAP_STATE_UPDATE_IR = 15,
  EJTAG_TAP_STATE_INVALID = 999
} ejtag_tap_state_t;


typedef struct ejtag_desc
{
#define EJTAG_FLAG_FTDI (1 << 0)
#define EJTAG_FLAG_I2C (1 << 1)
  uint32_t flags;

  size_t jtag_pos;
  unsigned int jtag_freq;

  union
  {
    struct
    {
    } ftdi;

    struct
    {
    } i2c;

  } u;

} ejtag_desc_t;


typedef struct ejtag_dev_info
{
  uint32_t id_code;
  uint32_t id_cmd;
  size_t ir_len;
} ejtag_dev_info_t;


typedef struct ejtag_handle
{
  /* from desc flags */
  uint32_t flags;

  /* io context */
  /* TODO: use correct function pointer prototypes */
#define EJTAG_IO_CHUNK_SIZE 128
  uint8_t io_ones[EJTAG_IO_CHUNK_SIZE];
  uint8_t io_zeros[EJTAG_IO_CHUNK_SIZE];
  uint8_t io_tms_buf[EJTAG_IO_CHUNK_SIZE];
  size_t io_tms_len;
  void* io_impl;
  void (*io_close)();
  void (*io_flush)();
  void (*io_usleep)();
  void (*io_shift_tdi_tdo)();
  void (*io_shift_tdi)();
  void (*io_shift_tdo)();
  void (*io_shift)();
  void (*io_set_tms)();
  void (*io_flush_tms)();
  void (*io_txrx_block)();
  void (*io_tx_tms)();
  void (*io_settype)();

  /* jtag context */
  ejtag_tap_state_t jtag_curr_state;
  ejtag_tap_state_t jtag_post_dr_state;
  ejtag_tap_state_t jtag_post_ir_state;
#define EJTAG_NOT_INIT -1
  int jtag_device_index;
#define EJTAG_MAX_NUM_DEVICES 32
  ejtag_dev_info_t jtag_devices[EJTAG_MAX_NUM_DEVICES];
  int jtag_num_devices;
  unsigned int jtag_shift_dr_incomplete;

} ejtag_handle_t;



/* exported routines */

int ejtag_log_write(
  const char   *klass,
  int           lvl,
  const char   *file,
  unsigned int  nline, 
  const char   *fmt, ...);
int ejtag_init_lib(void);
void ejtag_fini_lib(void);
int ejtag_log_lib(int);
int ejtag_foreach_dev(int(*)(const ejtag_desc_t*, void*), void*);
uint32_t ejtag_probe_link(void);
void ejtag_init_desc(ejtag_desc_t*);
int ejtag_open(ejtag_handle_t*, const ejtag_desc_t*);
int ejtag_open_default(ejtag_handle_t*);
void ejtag_close(ejtag_handle_t*);
int ejtag_detect_chain(ejtag_handle_t*, const ejtag_dev_info_t**, size_t*);
const char* ejtag_id_code_to_name(ejtag_handle_t*, uint32_t);
int ejtag_program_raw_data(ejtag_handle_t*, size_t, const uint8_t*, size_t);
int ejtag_program_bit_data(ejtag_handle_t*, size_t, const uint8_t*, size_t);
int ejtag_program_bit_file(ejtag_handle_t*, size_t, const char*);



#endif /* ! LIBEJTAG_H_INCLUDED */
