#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdarg.h>
#include "libejtag.h"
#include "libusb/libusb/libusb.h"
#include "libftdi1-1.3/src/ftdi.h"

int libejtag_debug = 0;

int ejtag_log_write(
  const char   *klass, 
  int           lvl,
  const char   *file, 
  unsigned int  nline, 
  const char   *fmt, ...)
{ 
#define LINESZ 160
  char line[LINESZ];
  va_list ap;
  int  ret = -1;
  int  off = 0;
  int  n;
  const char klass_lib[] = "LIB";
  int  islib;

  /* called from lib */
  for (n = 0, islib = 1; n < (sizeof(klass_lib) - 1); n++) {
    if (klass[n] != klass_lib[n]) {
        islib = 0;
        break;
    }
  }

  /* prepare minimum part of log line */
  n = snprintf(line, LINESZ, "%*s[%s]@%s:%d ", 
    (islib?(lvl * 4):0), "",
    klass, file, nline);

  /* not handling truncation */
  if ((n < 0) || (n >= LINESZ)) {
    goto on_error;
  }
  off += n;

  /* variable part of log line */
  va_start(ap, fmt);
  n = vsnprintf(line + off, LINESZ - off, fmt, ap);
  va_end(ap);

  /* always to stdout */
  printf("%s\n", line);
  ret = 0;

on_error:
  return ret;
}

#define LOG(...)  LOG_L(1, ##__VA_ARGS__)
#define LOG1(...) LOG_L(1, ##__VA_ARGS__)
#define LOG2(...) LOG_L(2, ##__VA_ARGS__)
#define LOG_L(lvl, _fmt, ...)                      \
do {                                        \
  if(libejtag_debug >= lvl) {                      \
    ejtag_log_write("LIBEJTAG", lvl, __FILE__, __LINE__, _fmt, ##__VA_ARGS__); \
  }                                         \
} while (0)


#define LOG_ERROR(_fmt, ...)                      \
do {                                        \
    ejtag_log_write("LIBEJTAG ERROR", 0, __FILE__, __LINE__, _fmt, ##__VA_ARGS__); \
} while (0)



/* generic io routines */

static void io_base_set_tms
(ejtag_handle_t* ejtag, unsigned int val)
{
  /* check_ok */

  if (ejtag->io_tms_len + 1 > EJTAG_IO_CHUNK_SIZE * 8)
  {
    ejtag->io_flush_tms(ejtag, 0);
  }

  if (val)
  {
    const size_t tms_len = ejtag->io_tms_len;
    ejtag->io_tms_buf[tms_len / 8] |= (1 << (tms_len & 0x7));
  }

  ++ejtag->io_tms_len;
}


static void io_base_flush_tms
(ejtag_handle_t* ejtag, unsigned int force)
{
  /* check_ok */

  if (ejtag->io_tms_len)
  {
    ejtag->io_tx_tms(ejtag, ejtag->io_tms_buf, ejtag->io_tms_len, force);
  }

  memset(ejtag->io_tms_buf, 0, EJTAG_IO_CHUNK_SIZE);
  ejtag->io_tms_len = 0;
}


static void io_base_shift_tdi_tdo
(
 ejtag_handle_t* ejtag,
 const uint8_t* tdi, uint8_t* tdo, size_t len, unsigned int last
)
{
  /* check_ok */

  if (len == 0) return ;
  ejtag->io_flush_tms(ejtag, 0);
  ejtag->io_txrx_block(ejtag, tdi, tdo, len, last);
}


static void io_base_shift_tdi
(ejtag_handle_t* ejtag, const uint8_t* tdi, size_t len, unsigned int last)
{
  /* check_ok */

  ejtag->io_shift_tdi_tdo(ejtag, tdi, NULL, len, last);
}


static void io_base_shift_tdo
(ejtag_handle_t* ejtag, uint8_t* tdo, size_t len, unsigned int last)
{
  /* check_ok */

  ejtag->io_shift_tdi_tdo(ejtag, NULL, tdo, len, last);
}


static void io_base_shift
(ejtag_handle_t* ejtag, unsigned int is_tdi, size_t length, unsigned int last)
{
  /* check_ok */

  int len = (int)length;
  uint8_t* const block = is_tdi ? ejtag->io_ones : ejtag->io_zeros;

  ejtag->io_flush_tms(ejtag, 0);

  while (len > EJTAG_IO_CHUNK_SIZE * 8)
  {
    ejtag->io_txrx_block(ejtag, block, NULL, EJTAG_IO_CHUNK_SIZE * 8, 0);
    len -= EJTAG_IO_CHUNK_SIZE * 8;
  }

  ejtag->io_shift_tdi_tdo(ejtag, block, NULL, len, last);
}


static void io_base_flush(ejtag_handle_t* ejtag)
{
}


static void io_base_usleep(ejtag_handle_t* ejtag, unsigned int usec)
{
  ejtag->io_flush_tms(ejtag, 0);
  ejtag->io_flush(ejtag);
  usleep(usec);
}


static void io_base_settype(ejtag_handle_t* ejtag, int subtype)
{
}


/* i2c io routines */


#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>


#define IO_I2C_TDI_MASK (1 << 2)
#define IO_I2C_TCK_MASK (1 << 3)
#define IO_I2C_TMS_MASK (1 << 4)
#define IO_I2C_TDO_MASK (1 << 5)


typedef struct
{
  int fd;
  uint8_t val;
  uint8_t saddr;
  uint8_t* rw_buf;
  size_t rw_nbit;
} io_i2c_handle_t;


static int io_i2c_readwrite
(io_i2c_handle_t* i2c, struct i2c_rdwr_ioctl_data* rdwr)
{
  /* http://lxr.free-electrons.com/source/drivers/i2c/i2c-core.c#L2892 */

  errno = 0;
  ioctl(i2c->fd, I2C_RDWR, rdwr);
  if (errno) return -1;

  return 0;
}

static int io_i2c_read
(io_i2c_handle_t* i2c, uint8_t* buf, size_t len)
{
  /* http://lxr.free-electrons.com/source/drivers/i2c/i2c-core.c#L2892 */

  struct i2c_rdwr_ioctl_data rdwr;
  struct i2c_msg msg[2];
  uint8_t cmd = buf[0];

  rdwr.msgs = msg;
  rdwr.nmsgs = 2;

  msg[0].addr = i2c->saddr;
  msg[0].flags = 0;
  msg[0].len = 1;
  msg[0].buf = &cmd;

  msg[1].addr = i2c->saddr;
  msg[1].flags = I2C_M_RD;
  msg[1].len = len;
  msg[1].buf = buf;

  return io_i2c_readwrite(i2c, &rdwr);
}

static int io_i2c_write
(io_i2c_handle_t* i2c, uint8_t* buf, size_t len)
{
  /* http://lxr.free-electrons.com/source/drivers/i2c/i2c-core.c#L2892 */

  struct i2c_rdwr_ioctl_data rdwr;
  struct i2c_msg msg[1];

  rdwr.msgs = msg;
  rdwr.nmsgs = 1;

  msg[0].addr = i2c->saddr;
  msg[0].flags = 0;
  msg[0].buf = buf;
  msg[0].len = len;

  return io_i2c_readwrite(i2c, &rdwr);
}

static int io_i2c_get_dir(io_i2c_handle_t* i2c, uint8_t* dir)
{
  uint8_t buf[1];

  buf[0] = 0x03;
  if (io_i2c_read(i2c, buf, 1)) return -1;

  *dir = buf[0];

  return 0;
}

static int io_i2c_set_dir(io_i2c_handle_t* i2c, uint8_t dir)
{
  uint8_t buf[2];

  buf[0] = 0x03;
  buf[1] = dir;
  if (io_i2c_write(i2c, buf, 2)) return -1;

  return 0;
}

static int io_i2c_get_val(io_i2c_handle_t* i2c, uint8_t* x)
{
  uint8_t buf[1];

  buf[0] = 0x00;
  if (io_i2c_read(i2c, buf, 1)) return -1;

  *x = buf[0];

  return 0;
}

static int io_i2c_set_val(io_i2c_handle_t* i2c, uint8_t x)
{
  uint8_t buf[2];

  buf[0] = 0x01;
  buf[1] = x;
  if (io_i2c_write(i2c, buf, 2)) return -1;

  return 0;
}

static void io_i2c_pulse_tck(io_i2c_handle_t* i2c)
{
  uint8_t buf[3];

  buf[0] = 0x01;
  buf[1] = i2c->val & ~IO_I2C_TCK_MASK;
  buf[2] = i2c->val | IO_I2C_TCK_MASK;
  io_i2c_write(i2c, buf, 3);

  /* final value */
  i2c->val |= IO_I2C_TCK_MASK;
}

__attribute__((unused))
static int io_i2c_tx_block
(
 ejtag_handle_t* ejtag,
 const uint8_t* tdi_buf, size_t tdi_size, unsigned int is_last
)
{
  /* fast version. goal is to limit ioctl syscalls by using large buffers */

  /* tdi_buf contains bytes, but tdi_len is in bits */
  /* xxx_size and xxx_pos are in bits */

  io_i2c_handle_t* const i2c = ejtag->io_impl;

  const size_t rw_size = i2c->rw_nbit;
  uint8_t* const rw_buf = i2c->rw_buf;
  uint8_t* rw_val;
  size_t rw_pos;

  const uint8_t* tdi_val;
  size_t tdi_pos;

  size_t i;
  size_t j;

  /* foreach rw_buf */

  tdi_pos = 0;
  tdi_val = tdi_buf;

  while (tdi_pos != tdi_size)
  {
    /* foreach byte */

    size_t jj = tdi_size - tdi_pos;
    if (jj > rw_size) jj = rw_size;

    /* command starts the rw buffer */

    rw_buf[0] = 0x01;
    rw_pos = 0;
    rw_val = rw_buf + 1;

    for (j = 0; j < jj; j += 8, ++tdi_val)
    {
      /* foreach bit */

      size_t ii = tdi_size - tdi_pos;
      if (ii > 8) ii = 8;

      for (i = 0; i != ii; ++i, rw_val += 2, ++rw_pos, ++tdi_pos)
      {
	if (*tdi_val & (1 << i)) i2c->val |= IO_I2C_TDI_MASK;
	else i2c->val &= ~IO_I2C_TDI_MASK;
	if ((tdi_pos == (tdi_size - 1)) && is_last) i2c->val |= IO_I2C_TMS_MASK;
	else i2c->val &= ~IO_I2C_TMS_MASK;
	rw_val[0] = i2c->val & ~IO_I2C_TCK_MASK;
	rw_val[1] = i2c->val | IO_I2C_TCK_MASK;
	/* final value */
	i2c->val |= IO_I2C_TCK_MASK;
      }
    }

    /* transmit rw_buf */
    /* add 1 for command */
    /* multiply 2 for tck pulse */

    if (io_i2c_write(i2c, rw_buf, 1 + 2 * rw_pos)) return -1;
  }

  return 0;
}

static void io_i2c_txrx_block
(
 ejtag_handle_t* ejtag,
 const uint8_t* tdi, uint8_t* tdo, size_t len, unsigned int is_last
)
{
  io_i2c_handle_t* const i2c = ejtag->io_impl;
  size_t i;
  uint8_t tdi_val = 0;

  if ((tdi != NULL) && (tdo == NULL))
  {
    /* fast version */
    io_i2c_tx_block(ejtag, tdi, len, is_last);
  }
  else
  {
    /* slow version */
    for (i = 0; i != len; ++i)
    {
      if (tdi != NULL)
      {
	if ((i % 8) == 0) tdi_val = tdi[i / 8];
	if (tdi_val & 0x01) i2c->val |= IO_I2C_TDI_MASK;
	else i2c->val &= ~IO_I2C_TDI_MASK;
	tdi_val = tdi_val >> 1;
      }

      if ((i == (len - 1)) && is_last) i2c->val |= IO_I2C_TMS_MASK;
      else i2c->val &= ~IO_I2C_TMS_MASK;

      io_i2c_pulse_tck(i2c);

      if (tdo != NULL)
      {
	if ((i % 8) == 0) tdo[i / 8] = 0;
	io_i2c_get_val(i2c, &i2c->val);
	if (i2c->val & IO_I2C_TDO_MASK) tdo[i / 8] |= (1 << (i % 8));
      }
    }
  }

  /* ensure tck is low */

  i2c->val &= ~IO_I2C_TCK_MASK;
  io_i2c_set_val(i2c, i2c->val);
}


static void io_i2c_tx_tms
(ejtag_handle_t* ejtag, uint8_t* pat, size_t length, unsigned int force)
{
  /* xc3sprog/ioparport.cpp/tx_tms */

  io_i2c_handle_t* const i2c = ejtag->io_impl;
  uint8_t tms_val = 0;
  size_t i;

  for (i = 0; i != length; ++i)
  {
    if ((i % 8) == 0) tms_val = pat[i / 8];

    if (tms_val & 0x01) i2c->val |= IO_I2C_TMS_MASK;
    else i2c->val &= ~IO_I2C_TMS_MASK;

    io_i2c_pulse_tck(i2c);

    tms_val = tms_val >> 1;
  }

  /* ensure tck is low */

  i2c->val &= ~IO_I2C_TCK_MASK;
  io_i2c_set_val(i2c, i2c->val);
}


static void io_i2c_close(ejtag_handle_t* ejtag)
{
  io_i2c_handle_t* const i2c = ejtag->io_impl;
  close(i2c->fd);
  free(i2c->rw_buf);
  free(i2c);
}


static int io_i2c_open_slave(io_i2c_handle_t* i2c, int baddr, int saddr)
{
  /* baddr the bus address */
  /* saddr the slave address */

  char path[32];

  snprintf(path, sizeof(path), "/dev/i2c-%u", baddr);
  path[sizeof(path) - 1] = 0;

  i2c->fd = open(path, O_RDWR);
  if (i2c->fd == -1) goto on_error_0;

  if (ioctl(i2c->fd, I2C_SLAVE, saddr) < 0) goto on_error_1;
  i2c->saddr = saddr;

  return 0;

 on_error_1:
  close(i2c->fd);
 on_error_0:
  return -1;
}


static int io_i2c_open(ejtag_handle_t* ejtag, const ejtag_desc_t* desc)
{
  io_i2c_handle_t* i2c;
  uint8_t dir;

  i2c = malloc(sizeof(io_i2c_handle_t));
  if (i2c == NULL) goto on_error_0;

  /* rw_nbit must be multiple of 8 due to io_i2c_tx_block assumptions */
  /* 1 added for the command */
  /* 2 mult since 2 tck pulses per bit */
  /* total size must hold in uint16_t */
  /* kernel limits total size to 8192 (drivers/i2c/i2c-dev.c#L240) */

  i2c->rw_nbit = 2 * 2048 - 8;
  i2c->rw_buf = malloc(1 + i2c->rw_nbit * 2);
  if (i2c->rw_buf == NULL) goto on_error_1;

  /* 0x25 is default DAnCE address */
  if (io_i2c_open_slave(i2c, 2, 0x25)) goto on_error_2;
  if (io_i2c_get_dir(i2c, &dir))
  {
    /* pepu_a fix */
    /* 0x20 on schematics, but we need to use 0x38 */
    close(i2c->fd);
    if (io_i2c_open_slave(i2c, 2, 0x38)) goto on_error_2;
    if (io_i2c_get_dir(i2c, &dir)) goto on_error_3;
  }

  dir &= ~(IO_I2C_TDI_MASK | IO_I2C_TCK_MASK | IO_I2C_TMS_MASK);
  dir |= IO_I2C_TDO_MASK;
  if (io_i2c_set_dir(i2c, dir)) goto on_error_3;

  io_i2c_get_val(i2c, &i2c->val);
  i2c->val &= ~(IO_I2C_TCK_MASK | IO_I2C_TMS_MASK | IO_I2C_TDI_MASK);
  io_i2c_set_val(i2c, i2c->val);

  ejtag->io_impl = i2c;
  ejtag->io_close = io_i2c_close;
  ejtag->io_flush = io_base_flush;
  ejtag->io_usleep = io_base_usleep;
  ejtag->io_shift_tdi_tdo = io_base_shift_tdi_tdo;
  ejtag->io_shift_tdi = io_base_shift_tdi;
  ejtag->io_shift_tdo = io_base_shift_tdo;
  ejtag->io_shift = io_base_shift;
  ejtag->io_set_tms = io_base_set_tms;
  ejtag->io_flush_tms = io_base_flush_tms;
  ejtag->io_txrx_block = io_i2c_txrx_block;
  ejtag->io_tx_tms = io_i2c_tx_tms;
  ejtag->io_settype = io_base_settype;

  return 0;

 on_error_3:
  close(i2c->fd);
 on_error_2:
  free(i2c->rw_buf);
 on_error_1:
  free(i2c);
 on_error_0:
  return -1;
}


/* ftdi io routines */

typedef struct
{
  unsigned int must_deinit;
  struct ftdi_context ftdi;
#define IO_FTDI_TX_BUF 4096
  uint8_t usbuf[IO_FTDI_TX_BUF];
  size_t buflen;
  size_t bptr;
  int subtype;
  int retries;
  unsigned int device_has_fast_clock;
  unsigned int tck_freq;
} io_ftdi_handle_t;


static size_t io_ftdi_readusb(ejtag_handle_t*, uint8_t*, size_t);


static void io_ftdi_mpsse_send(ejtag_handle_t* ejtag)
{
  /* check_ok */

  io_ftdi_handle_t* const io_ftdi = ejtag->io_impl;
  struct ftdi_context* const ftdi = &io_ftdi->ftdi;
  int written;

  if (io_ftdi->bptr == 0) return ;

  written = ftdi_write_data(ftdi, io_ftdi->usbuf, io_ftdi->bptr);
  if (written != (int)io_ftdi->bptr)
  {
    LOG_ERROR("ftdi_write_data() failed");
  }

  io_ftdi->bptr = 0;
}


static void io_ftdi_mpsse_add_cmd
(ejtag_handle_t* ejtag, const uint8_t* buf, size_t len)
{
  /* check_ok */

  io_ftdi_handle_t* const io_ftdi = ejtag->io_impl;

  if (io_ftdi->bptr + len + 1 >= IO_FTDI_TX_BUF) io_ftdi_mpsse_send(ejtag);
  memcpy(io_ftdi->usbuf + io_ftdi->bptr, buf, len);
  io_ftdi->bptr += len;
}


static void io_ftdi_deinit(ejtag_handle_t* ejtag)
{
  /* before shutdown, we must wait until everything is shifted out */
  /* do this by temporary enabling loopback mode, write something */
  /* and wait until we can read it back */

  io_ftdi_handle_t* const io_ftdi = ejtag->io_impl;
  struct ftdi_context* const ftdi = &io_ftdi->ftdi;

  static uint8_t tbuf[16] =
  {
    SET_BITS_LOW, 0xff, 0x00,
    SET_BITS_HIGH, 0xff, 0x00,
    LOOPBACK_START,
    MPSSE_DO_READ|MPSSE_READ_NEG|MPSSE_DO_WRITE|MPSSE_WRITE_NEG|MPSSE_LSB,
    0x04, 0x00,
    0xaa, 0x55, 0x00, 0xff, 0xaa,
    LOOPBACK_END
  };

  /* do not deinit twice */
  if (io_ftdi->must_deinit == 0) return ;
  io_ftdi->must_deinit = 0;

  io_ftdi_mpsse_add_cmd(ejtag, tbuf, 16);
  if (io_ftdi_readusb(ejtag, tbuf, 5) != 5) {
    LOG_ERROR("FTDI read buffer failed");
  }

  ftdi_set_bitmode(ftdi, 0, BITMODE_RESET);
  ftdi_usb_reset(ftdi);
  ftdi_usb_close(ftdi);
  ftdi_deinit(ftdi);
}


static size_t io_ftdi_readusb
(ejtag_handle_t* ejtag, uint8_t* rbuf, size_t len)
{
  /* check_ok */

  io_ftdi_handle_t* const io_ftdi = ejtag->io_impl;
  struct ftdi_context* const ftdi = &io_ftdi->ftdi;

  uint8_t buf[1] = { SEND_IMMEDIATE };
  size_t read = 0;
  int length = (int)len;
  int timeout = 0;
  int last_errno;
  int last_read;

  io_ftdi_mpsse_add_cmd(ejtag, buf, 1);
  io_ftdi_mpsse_send(ejtag);

  last_read = ftdi_read_data(ftdi, rbuf, length);
  if (last_read > 0) read += last_read;

  while (((int)read < length) && (timeout < 1000))
  {
    last_errno = 0;
    ++io_ftdi->retries;
    last_read = ftdi_read_data(ftdi, rbuf + read, length - read);
    if (last_read > 0) read += last_read;
    else last_errno = errno;
    ++timeout;
  }

  if (timeout >= 1000)
  {
    LOG_ERROR("FTDI timeout reading data");
    if (last_errno) goto on_error;
  }

  if (last_read < 0)
  {
    LOG_ERROR("FTDI reading data failed");
    goto on_error;
  }

  return read;

 on_error:
  io_ftdi_deinit(ejtag);
  return (size_t)-1;
}


static void io_ftdi_flush(ejtag_handle_t* ejtag)
{
  /* check_ok */

  io_ftdi_mpsse_send(ejtag);
}


static void io_ftdi_usleep
(ejtag_handle_t* ejtag, unsigned int usec)
{
  /* check_ok */

  /* short delays may be prolonged by flush causing an */
  /* additional frame sent out on a next microframe */
  /* so make the FTDI toggle TCK for delays < 20 ms */

  io_ftdi_handle_t* const io_ftdi = ejtag->io_impl;

  ejtag->io_flush_tms(ejtag, 0);

  if (usec < 20000)
  {
    const unsigned int tck_freq = io_ftdi->tck_freq;
    unsigned int ticks;

    ticks = (usec * (tck_freq / 100) + (tck_freq / 100) - 1) / (1000000 / 100);
    if (io_ftdi->device_has_fast_clock)
    {
      uint8_t buf[3] = { 0x8f };
      if (ticks > 8 )
      {
	buf[1] = ((ticks / 8) >> 0 ) & 0xff;
	buf[2] = ((ticks / 8) >> 8 ) & 0xff;
	io_ftdi_mpsse_add_cmd(ejtag, buf, 3);
	ticks %= 8;
      }
      if (ticks)
      {
	buf[0] = 0x8e;
	buf[1] = ticks - 1;
	io_ftdi_mpsse_add_cmd(ejtag, buf, 2);
      }
    }
    else
    {
      uint8_t buf[3];
      if (ticks > 8)
      {
	buf[0] = MPSSE_DO_WRITE | MPSSE_LSB | MPSSE_WRITE_NEG;
	buf[1] = ((ticks / 8) >> 0) & 0xff;
	buf[2] = ((ticks / 8) >> 8) & 0xff;
	io_ftdi_mpsse_add_cmd(ejtag, buf, 3);
	buf[0] = 0;
	while (ticks > 8)
	{
	  io_ftdi_mpsse_add_cmd(ejtag, buf, 1);
	  ticks -= 8;
	}
      }
      if (ticks)
      {
	buf[0] = MPSSE_DO_WRITE | MPSSE_LSB | MPSSE_BITMODE | MPSSE_WRITE_NEG;
	buf[1] = ticks - 1;
	buf[2] = 0;
	io_ftdi_mpsse_add_cmd(ejtag, buf, 1);
      }
    }
  }
  else
  {
    ejtag->io_flush(ejtag);
    usleep(usec);
  }
}


static void io_ftdi_txrx_block
(
 ejtag_handle_t* ejtag,
 const uint8_t* tdi, uint8_t* tdo, size_t len, unsigned int is_last
)
{
  /* check_ok */

  uint8_t rbuf[IO_FTDI_TX_BUF];
  const uint8_t* tmpsbuf = tdi;
  uint8_t* tmprbuf = tdo;
  /* if we need to shift state, treat the last bit separate */
  size_t rem = is_last ? len - 1 : len;
  uint8_t buf[IO_FTDI_TX_BUF];
  /* we need the preamble */
  size_t buflen = IO_FTDI_TX_BUF - 3;
  size_t rembits;

  /* out on -ve edge, in on +ve edge */
  if (rem / 8 > buflen)
  {
    while (rem / 8 > buflen)
    {
      /* full chunks */
      buf[0] = ((tdo)?(MPSSE_DO_READ |MPSSE_READ_NEG):0)
	|((tdi)?MPSSE_DO_WRITE:0)|MPSSE_LSB|MPSSE_WRITE_NEG;
      buf[1] = ((buflen-1) >> 0) & 0xff; /* low lenbth byte */
      buf[2] = ((buflen-1) >> 8) & 0xff; /* high lenbth byte */
      io_ftdi_mpsse_add_cmd(ejtag, buf, 3);
      if (tdi)
      {
	io_ftdi_mpsse_add_cmd(ejtag, tmpsbuf, buflen);
	tmpsbuf += buflen;
      }
      rem -= buflen * 8;
      if (tdo)
      {
	if (io_ftdi_readusb(ejtag, tmprbuf, buflen) != buflen)
	{
	  LOG_ERROR("FTDI unable to read data from USB");
	}
	tmprbuf += buflen;
      }
    }
  }

  rembits = rem % 8;
  rem = rem - rembits;
  if ((rem % 8) != 0) {
    LOG_ERROR("wrong number of bits to shift");
  }
  buflen = rem / 8;
  if (rem)
  {
    buf[0] = ((tdo)?(MPSSE_DO_READ|MPSSE_READ_NEG):0)
      |((tdi)?MPSSE_DO_WRITE:0)|MPSSE_LSB|MPSSE_WRITE_NEG;
    buf[1] = ((buflen - 1) >> 0) & 0xff; /* low length byte */
    buf[2] = ((buflen - 1) >> 8) & 0xff; /* high length byte */
    io_ftdi_mpsse_add_cmd(ejtag, buf, 3);
    if (tdi)
    {
      io_ftdi_mpsse_add_cmd(ejtag, tmpsbuf, buflen);
      tmpsbuf += buflen;
    }
  }

  if (buflen >= (IO_FTDI_TX_BUF - 4))
  {
    /* no space for the last data. send and evenually read as we */
    /* handle whole bytes, we can use the receive buffer direct */
    if (tdo)
    {
      io_ftdi_readusb(ejtag, tmprbuf, buflen);
      tmprbuf += buflen;
    }
    buflen = 0;
  }

  if (rembits)
  {
    /* clock Data Bits Out on -ve Clock Edge LSB First (no Read) */
    /* (use if TCK/SK starts at 0) */
    buf[0] = ((tdo)?(MPSSE_DO_READ|MPSSE_READ_NEG):0)
      |((tdi)?MPSSE_DO_WRITE:0)|MPSSE_LSB|MPSSE_BITMODE|MPSSE_WRITE_NEG;
    /* length: only one byte left */
    buf[1] = rembits - 1;
    io_ftdi_mpsse_add_cmd(ejtag, buf, 2);
    if (tdi) io_ftdi_mpsse_add_cmd(ejtag, tmpsbuf, 1);
    ++buflen;
  }

  if (is_last)
  {
    unsigned int lastbit = 0;
    if (tdi) lastbit = (*tmpsbuf & (1 << rembits));
    /* TMS/CS with LSB first on -ve TCK/SK edge, read on +ve edge */
    /* - use if TCK/SK is set to 0 */
    buf[0] = MPSSE_WRITE_TMS|((tdo)?(MPSSE_DO_READ|MPSSE_READ_NEG):0)|
      MPSSE_LSB|MPSSE_BITMODE|MPSSE_WRITE_NEG;
    buf[1] = 0; /* only one bit */
    buf[2] = lastbit ? 0x81 : 1; /* TMS set */
    io_ftdi_mpsse_add_cmd(ejtag, buf, 3);
    ++buflen;
  }

  if (tdo)
  {
    if (!is_last)
    {
      io_ftdi_readusb(ejtag, tmprbuf, buflen);
      /* last bits for incomplete byte must get shifted down */
      if (rembits) tmprbuf[buflen - 1] = tmprbuf[buflen - 1] >> (8 - rembits);
    }
    else
    {
      /* we need to handle the last bit. It's much faster to */
      /* read into an extra buffer than to issue two USB reads */
      io_ftdi_readusb(ejtag, rbuf, buflen);
      if (!rembits)
      {
	rbuf[buflen - 1] = (rbuf[buflen - 1] & 0x80) ? 1 : 0;
      }
      else
      {
	/* TDO Bits are shifted downwards, so align them */
	/* We only shift TMS once, so the relevant bit is bit 7 (0x80) */
	rbuf[buflen - 2] = rbuf[buflen - 2] >> (8 - rembits) |
	  ((rbuf[buflen - 1] & 0x80) >> (7 - rembits));
	--buflen;
      }
      memcpy(tmprbuf, rbuf, buflen);
    }
  }
}


static void io_ftdi_tx_tms
(ejtag_handle_t* ejtag, uint8_t* pat, size_t length, unsigned int force)
{
  /* check_ok */

  uint8_t buf[3] =
  {
    MPSSE_WRITE_TMS | MPSSE_LSB | MPSSE_BITMODE | MPSSE_WRITE_NEG,
    0,
    pat[0]
  };

  int len = (int)length;
  int i = 0;
  int j = 0;

  if (len == 0) return ;

  while (len > 0)
  {
    /* warning: Bug in FT2232L(D?, H not!). */
    /* With 7 bits TMS shift, static TDO */
    /* value gets set to TMS on last TCK edge */

    buf[1] = (len > 6) ? 5 : (len - 1);
    buf[2] = 0x80;
    for (i = 0; i < (int)(buf[1] + 1); ++i)
    {
      buf[2] |= (((pat[j >> 3] & (1 << (j & 0x7))) ? 1 : 0) << i);
      ++j;
    }
    len -= (buf[1] + 1);
    io_ftdi_mpsse_add_cmd(ejtag, buf, 3);
  }

  if (force) io_ftdi_mpsse_send(ejtag);
}


static void io_ftdi_settype
(ejtag_handle_t* ejtag, int subtype)
{
  /* check_ok */

  io_ftdi_handle_t* const io_ftdi = ejtag->io_impl;
  io_ftdi->subtype = subtype;
}


static void io_ftdi_close(ejtag_handle_t* ejtag)
{
  io_ftdi_handle_t* const io_ftdi = ejtag->io_impl;
  io_ftdi_deinit(ejtag);
  free(io_ftdi);
}


static int io_ftdi_open(ejtag_handle_t* ejtag, const ejtag_desc_t* desc)
{
  io_ftdi_handle_t* io_ftdi;
  struct ftdi_context* ftdi;

  uint8_t buf1[5];
  uint8_t buf[9] =
  {
    SET_BITS_LOW, 0x00, 0x0b,
    TCK_DIVISOR, 0x03, 0x00,
    SET_BITS_HIGH, 0x00, 0x00
  };

  const char* const description = NULL;
  const unsigned int vendor = 0x0403;
  const unsigned int product = 0x6014;
  const unsigned int channel = 0;
  const unsigned int dbus_data = 0;
  const unsigned int dbus_en = 0xb;
  const unsigned int cbus_data = 0;
  const unsigned int cbus_en = 0;
  unsigned int divisor;
  int res;
  const unsigned int freq = desc->jtag_freq;
  const char* const serial = NULL;

  io_ftdi = malloc(sizeof(io_ftdi_handle_t));
  if (io_ftdi == NULL) goto on_error_0;

  ejtag->io_impl = io_ftdi;
  ejtag->io_close = io_ftdi_close;
  ejtag->io_flush = io_ftdi_flush;
  ejtag->io_usleep = io_ftdi_usleep;
  ejtag->io_shift_tdi_tdo = io_base_shift_tdi_tdo;
  ejtag->io_shift_tdi = io_base_shift_tdi;
  ejtag->io_shift_tdo = io_base_shift_tdo;
  ejtag->io_shift = io_base_shift;
  ejtag->io_set_tms = io_base_set_tms;
  ejtag->io_flush_tms = io_base_flush_tms;
  ejtag->io_txrx_block = io_ftdi_txrx_block;
  ejtag->io_tx_tms = io_ftdi_tx_tms;
  ejtag->io_settype = io_ftdi_settype;

  io_ftdi->bptr = 0;
  io_ftdi->retries = 0;

  /* from ioftdi.cpp, IOFtdi::Init */

  /* set for now. If we have a fast device, correct later */
  /* freq = 0 means max rate, 3 MHz for now */
  if ((freq == 0) || (freq >= 6000000)) divisor = 0;
  else divisor = 6000000 / freq - ((6000000 & freq) ? 0 : 1);
  if (divisor > 0xffff) divisor = 0xffff;

  buf[4] = divisor & 0xff;
  buf[5] = (divisor >> 8) & 0xff;

  ftdi = &io_ftdi->ftdi;
  if (ftdi_init(ftdi))
  {
    LOG_ERROR("unable to open FTDI lib");
    goto on_error_1;
  }
  io_ftdi->must_deinit = 1;

  res = ftdi_set_interface(ftdi, (enum ftdi_interface)channel);
  if (res < 0)
  {
    LOG_ERROR("FTDI set interface failed");
    goto on_error_2;
  }

  res = ftdi_usb_open_desc(ftdi, vendor, product, description, serial);
  if (res == 0)
  {
    res = ftdi_set_bitmode(ftdi, 0x00, BITMODE_RESET);
    if (res < 0)
    {
      LOG_ERROR("FTDI set bit mode RESET failed");
      goto on_error_2;
    }

    res = ftdi_usb_purge_buffers(ftdi);
    if (res < 0)
    {
      LOG_ERROR("FTDI purge buffers failed");
      goto on_error_2;
    }

    res = ftdi_set_latency_timer(ftdi, 1);
    if ( res <0)
    {
      LOG_ERROR("FTDI set latentcy timer failed");
      goto on_error_2;
    }

    res = ftdi_set_bitmode(ftdi, 0xfb, BITMODE_MPSSE);
    if (res< 0)
    {
      LOG_ERROR("FTDI set bit mode MPSSE failed");
      goto on_error_2;
    }

    /* FIXME: Without this read, consecutive runs on the FT2232H may hang */
    ftdi_read_data(ftdi, buf1, 5);

    /* Check if we have a fast clock cabable device*/
    switch(ftdi->type)
    {
    case TYPE_2232H:
    case TYPE_4232H:
#ifdef DRIVE_OPEN_COLLECTOR
    case TYPE_232H:
#endif
      io_ftdi->device_has_fast_clock = 1;
      break;
    default:
      io_ftdi->device_has_fast_clock = 0;
    }
  }
  else
  {
    LOG_ERROR("unable to open USB descritptor");
    goto on_error_2;
  }

  /* Prepare for JTAG operation */
  buf[1] |= dbus_data;
  buf[2] |= dbus_en;
  buf[7] = cbus_data;
  buf[8] = cbus_en;

  io_ftdi_mpsse_add_cmd(ejtag, buf, 9);
  io_ftdi_mpsse_send(ejtag);

  /* On H devices, use the non-divided clock */
  if (io_ftdi->device_has_fast_clock && ((freq == 0) || (freq > 458)))
  {
    /* freq = 0 means max rate, 30 MHz for now */
    if ((freq == 0) || (freq >= 30000000)) divisor = 0;
    else divisor = 30000000 / freq - ((30000000 % freq) ? 0 : 1);
    if (divisor > 0xffff) divisor = 0xffff;
#ifndef DIS_DIV_5
#define DIS_DIV_5 0x8a
#endif
    buf[0] = DIS_DIV_5;
    buf[1] = TCK_DIVISOR;
    buf[2] = (divisor >> 0) & 0xff;
    buf[3] = (divisor >> 8) & 0xff;
    io_ftdi_mpsse_add_cmd(ejtag, buf, 4);
    io_ftdi_mpsse_send(ejtag);
    io_ftdi->tck_freq = 30000000 / (1 + divisor);
  }
  else
  {
    io_ftdi->tck_freq = 6000000 / (1 + divisor);
  }

  /* success */
  return 0;

 on_error_2:
  ftdi_deinit(ftdi);
 on_error_1:
  free(io_ftdi);
 on_error_0:
  return -1;
}


/* generic io routines */

static int io_open(ejtag_handle_t* ejtag, const ejtag_desc_t* desc)
{
  /* check_ok */

  int err;

  memset(ejtag->io_ones, 0xff, EJTAG_IO_CHUNK_SIZE);
  memset(ejtag->io_zeros, 0x00, EJTAG_IO_CHUNK_SIZE);
  memset(ejtag->io_tms_buf, 0x00, EJTAG_IO_CHUNK_SIZE);
  ejtag->io_tms_len = 0;

  if (ejtag->flags & EJTAG_FLAG_FTDI) {
    LOG("Using FTDI");
    err = io_ftdi_open(ejtag, desc);
  } else if (ejtag->flags & EJTAG_FLAG_I2C) {
    LOG("Using I2C");
    err = io_i2c_open(ejtag, desc);
  } else {
    err = -1;
  }
  if (err) goto on_error_0;

  return 0;

 on_error_0:
  LOG_ERROR("Unable to access JTAG");
  return -1;
}


static void io_close(ejtag_handle_t* ejtag)
{
  /* check_ok */

  ejtag->io_close(ejtag);
}


/* jtag chained device specific information */
/* from xc3sprog/devlist.txt */

typedef struct
{
  uint32_t id_code;
  size_t ir_len;
  uint32_t id_cmd;
  const char* dev_name;
} id_info_t;


static const id_info_t* id_code_to_info(uint32_t id_code)
{
  /* check_ok */

  static const id_info_t id_info[] =
  {
    { 0x03622093, 6, 0x009, "XC7S6" },
    { 0x03620093, 6, 0x009, "XC7S15" },
    { 0x037C4093, 6, 0x009, "XC7S25" },
    { 0x0362F093, 6, 0x009, "XC7S50" },
    { 0x037C8093, 6, 0x009, "XC7S75" },
    { 0x037C7093, 6, 0x009, "XC7S100" },

    { 0x037C3093, 6, 0x009, "XC7A12T" },
    { 0x0362E093, 6, 0x009, "XC7A15T" },
    { 0x037C2093, 6, 0x009, "XC7A25T" },
    { 0x0362d093, 6, 0x009, "XC7A35T" },
    { 0x0362C093, 6, 0x009, "XC7A50T" },
    { 0x03632093, 6, 0x009, "XC7A75T" },
    { 0x03631093, 6, 0x009, "XC7A100T" },
    { 0x03636093, 6, 0x009, "XC7A200T" },

    { 0x03642093, 6, 0x009, "XC7K30T" },
    { 0x03647093, 6, 0x009, "XC7K70T" },
    { 0x0364c093, 6, 0x009, "XC7K160T" },
    { 0x03651093, 6, 0x009, "XC7K325T" },
    { 0x03656093, 6, 0x009, "XC7K410T" },
    { 0x03747093, 6, 0x009, "XC7K355T" },
    { 0x03751093, 6, 0x009, "XC7K480T" },
    { 0x03752093, 6, 0x009, "XC7K420T" },

    { 0x03667093, 6, 0x009, "XC7VX330T" },
    { 0x03671093, 6, 0x009, "XC7V585T" },
    { 0x03682093, 6, 0x009, "XC7VX415T" },
    { 0x03687093, 6, 0x009, "XC7VX485T" },
    { 0x03691093, 6, 0x009, "XC7VX690T" },
    { 0x03692093, 6, 0x009, "XC7VX550T" },
    { 0x03696093, 6, 0x009, "XC7VX980T" },

    { 0x03722093, 6, 0x009, "XC7Z010" },
    { 0x03727093, 6, 0x009, "XC7Z020" },
    { 0x0372c093, 6, 0x009, "XC7Z030" },
    { 0x03731093, 6, 0x009, "XC7Z045" },
    { 0x03736093, 6, 0x009, "XC7Z100" },

    { 0x04244093, 10, 0x3c9, "XC6VLX75T(L)" },
    { 0x0424a093, 10, 0x3c9, "XC6VLX130T(L)" },
    { 0x0424c093, 10, 0x3c9, "XC6VLX195T(L)" },
    { 0x04250093, 10, 0x3c9, "XC6VLX240T(L)" },
    { 0x04252093, 10, 0x3c9, "XC6VLX365T(L)" },
    { 0x04256093, 10, 0x3c9, "XC6VLX550T(L)" },
    { 0x0423a093, 10, 0x3c9, "XC6VLX760(L)" },

    { 0x04286093, 10, 0x3c9, "XC6VSX315T(L)" },
    { 0x04288093, 10, 0x3c9, "XC6VSX475T(L)" },

    { 0x042c4093, 10, 0x3c9, "XC6VCX75T" },
    { 0x042ca093, 10, 0x3c9, "XC6VCX130T" },
    { 0x042cc093, 10, 0x3c9, "XC6VCX195T" },
    { 0x042d0093, 10, 0x3c9, "XC6VCX240T" },

    { 0x04000093, 6, 0x009, "XC6SLX4" },
    { 0x04001093, 6, 0x009, "XC6SLX9" },
    { 0x04002093, 6, 0x009, "XC6SLX16" },
    { 0x04004093, 6, 0x009, "XC6SLX25" },
    { 0x04024093, 6, 0x009, "XC6SLX25T" },
    { 0x04008093, 6, 0x009, "XC6SLX45" },
    { 0x04028093, 6, 0x009, "XC6SLX45T" },
    { 0x0400e093, 6, 0x009, "XC6SLX75" },
    { 0x0402e093, 6, 0x009, "XC6SLX75T" },
    { 0x04011093, 6, 0x009, "XC6SLX100" },
    { 0x04031093, 6, 0x009, "XC6SLX100T" },
    { 0x0401d093, 6, 0x009, "XC6SLX150" },
    { 0x0403d093, 6, 0x009, "XC6SLX150T" }
  };

  static const size_t n = sizeof(id_info) / sizeof(id_info[0]);
  size_t i;

  id_code &= 0x0fffffff;

  for (i = 0; i != n; ++i)
  {
    if (id_info[i].id_code == id_code) return &id_info[i];
  }

  return NULL;
}


__attribute__((unused))
static uint32_t id_code_to_family(uint32_t id_code)
{
  /* check_ok */

  return ((id_code >> 21) & 0x7f);
}


__attribute__((unused))
static uint32_t id_code_to_manufacturer(uint32_t id_code)
{
  /* check_ok */

  return ((id_code >> 1) & 0x3ff);
}


/* device independant jtag logic */
/* implemented from xc3sprog/jtag.cpp */

static int jtag_open(ejtag_handle_t* ejtag)
{
  /* check_ok */

  ejtag->jtag_curr_state = EJTAG_TAP_STATE_INVALID;
  ejtag->jtag_post_dr_state = EJTAG_TAP_STATE_RUN_TEST_IDLE;
  ejtag->jtag_post_ir_state = EJTAG_TAP_STATE_RUN_TEST_IDLE;
  ejtag->jtag_device_index = EJTAG_NOT_INIT;
  ejtag->jtag_num_devices = EJTAG_NOT_INIT;
  ejtag->jtag_shift_dr_incomplete = 0;

  return 0;
}


static void jtag_close(ejtag_handle_t* ejtag)
{
  /* check_ok */
}


__attribute__((unused))
static const char* jtag_tap_state_to_name(ejtag_tap_state_t s)
{
  /* check_ok */

#define JTAG_TAP_STATE_CASE(__i) case EJTAG_TAP_STATE_ ## __i: return #__i

  switch(s)
  {
    JTAG_TAP_STATE_CASE(TEST_LOGIC_RESET);
    JTAG_TAP_STATE_CASE(RUN_TEST_IDLE);
    JTAG_TAP_STATE_CASE(SELECT_DR_SCAN);
    JTAG_TAP_STATE_CASE(CAPTURE_DR);
    JTAG_TAP_STATE_CASE(SHIFT_DR);
    JTAG_TAP_STATE_CASE(EXIT1_DR);
    JTAG_TAP_STATE_CASE(PAUSE_DR);
    JTAG_TAP_STATE_CASE(EXIT2_DR);
    JTAG_TAP_STATE_CASE(UPDATE_DR);
    JTAG_TAP_STATE_CASE(SELECT_IR_SCAN);
    JTAG_TAP_STATE_CASE(CAPTURE_IR);
    JTAG_TAP_STATE_CASE(SHIFT_IR);
    JTAG_TAP_STATE_CASE(EXIT1_IR);
    JTAG_TAP_STATE_CASE(PAUSE_IR);
    JTAG_TAP_STATE_CASE(EXIT2_IR);
    JTAG_TAP_STATE_CASE(UPDATE_IR);
    default: break;
  }

  return "INVALID";
}


static void jtag_tap_test_logic_reset(ejtag_handle_t* ejtag)
{
  /* check_ok */

  size_t i;

  for (i = 0; i != 5; ++i) ejtag->io_set_tms(ejtag, 1);
  ejtag->jtag_curr_state = EJTAG_TAP_STATE_TEST_LOGIC_RESET;
  ejtag->io_flush_tms(ejtag, 1);
}


static void jtag_set_tap_state
(ejtag_handle_t* ejtag, ejtag_tap_state_t state, int pre)
{
  /* check_ok */

  unsigned int tms;
  size_t i;

  while (ejtag->jtag_curr_state != state)
  {
    switch (ejtag->jtag_curr_state)
    {
    case EJTAG_TAP_STATE_TEST_LOGIC_RESET:
      switch (state)
      {
      case EJTAG_TAP_STATE_TEST_LOGIC_RESET:
	tms = 1;
	break;
      default:
	tms = 0;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_RUN_TEST_IDLE;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_RUN_TEST_IDLE:
      switch (state)
      {
      case EJTAG_TAP_STATE_RUN_TEST_IDLE:
	tms = 0;
	break;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_SELECT_DR_SCAN;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_SELECT_DR_SCAN:
      switch (state)
      {
      case EJTAG_TAP_STATE_CAPTURE_DR:
      case EJTAG_TAP_STATE_SHIFT_DR:
      case EJTAG_TAP_STATE_EXIT1_DR:
      case EJTAG_TAP_STATE_PAUSE_DR:
      case EJTAG_TAP_STATE_EXIT2_DR:
      case EJTAG_TAP_STATE_UPDATE_DR:
	tms = 0;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_CAPTURE_DR;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_SELECT_IR_SCAN;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_CAPTURE_DR:
      switch (state)
      {
      case EJTAG_TAP_STATE_SHIFT_DR:
	tms = 0;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_SHIFT_DR;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_EXIT1_DR;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_SHIFT_DR:
      switch (state)
      {
      case EJTAG_TAP_STATE_SHIFT_DR:
	tms = 0;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_EXIT1_DR;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_EXIT1_DR:
      switch (state)
      {
      case EJTAG_TAP_STATE_PAUSE_DR:
      case EJTAG_TAP_STATE_EXIT2_DR:
      case EJTAG_TAP_STATE_SHIFT_DR:
      case EJTAG_TAP_STATE_EXIT1_DR:
	tms = 0;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_PAUSE_DR;
	break;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_UPDATE_DR;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_PAUSE_DR:
      switch (state)
      {
      case EJTAG_TAP_STATE_PAUSE_DR:
	tms = 0;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_EXIT2_DR;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_EXIT2_DR:
      switch (state)
      {
      case EJTAG_TAP_STATE_SHIFT_DR:
      case EJTAG_TAP_STATE_EXIT1_DR:
      case EJTAG_TAP_STATE_PAUSE_DR:
	tms = 0;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_SHIFT_DR;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_UPDATE_DR;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_UPDATE_DR:
      switch (state)
      {
      case EJTAG_TAP_STATE_RUN_TEST_IDLE:
	tms = 0;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_RUN_TEST_IDLE;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_SELECT_DR_SCAN;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_SELECT_IR_SCAN:
      switch (state)
      {
      case EJTAG_TAP_STATE_CAPTURE_IR:
      case EJTAG_TAP_STATE_SHIFT_IR:
      case EJTAG_TAP_STATE_EXIT1_IR:
      case EJTAG_TAP_STATE_PAUSE_IR:
      case EJTAG_TAP_STATE_EXIT2_IR:
      case EJTAG_TAP_STATE_UPDATE_IR:
	tms = 0;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_CAPTURE_IR;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_TEST_LOGIC_RESET;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_CAPTURE_IR:
      switch (state)
      {
      case EJTAG_TAP_STATE_SHIFT_IR:
	tms = 0;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_SHIFT_IR;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_EXIT1_IR;
	break ;
      }
      break;

    case EJTAG_TAP_STATE_SHIFT_IR:
      switch (state)
      {
      case EJTAG_TAP_STATE_SHIFT_IR:
	tms = 0;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_EXIT1_IR;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_EXIT1_IR:
      switch (state)
      {
      case EJTAG_TAP_STATE_PAUSE_IR:
      case EJTAG_TAP_STATE_EXIT2_IR:
      case EJTAG_TAP_STATE_SHIFT_IR:
      case EJTAG_TAP_STATE_EXIT1_IR:
	tms = 0;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_PAUSE_IR;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_UPDATE_IR;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_PAUSE_IR:
      switch (state)
      {
      case EJTAG_TAP_STATE_PAUSE_IR:
	tms = 0;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_EXIT2_IR;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_EXIT2_IR:
      switch (state)
      {
      case EJTAG_TAP_STATE_SHIFT_IR:
      case EJTAG_TAP_STATE_EXIT1_IR:
      case EJTAG_TAP_STATE_PAUSE_IR:
	tms = 0;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_SHIFT_IR;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_UPDATE_IR;
	break ;
      }
      break ;

    case EJTAG_TAP_STATE_UPDATE_IR:
      switch (state)
      {
      case EJTAG_TAP_STATE_RUN_TEST_IDLE:
	tms = 0;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_RUN_TEST_IDLE;
	break ;
      default:
	tms = 1;
	ejtag->jtag_curr_state = EJTAG_TAP_STATE_SELECT_DR_SCAN;
	break ;
      }
      break ;

    default:
      jtag_tap_test_logic_reset(ejtag);
      tms = 1;
      break ;
    }

    ejtag->io_set_tms(ejtag, tms);
  }

  for (i = 0; i != (size_t)pre; ++i) ejtag->io_set_tms(ejtag, 0);
}


static void jtag_cycle_tck(ejtag_handle_t* ejtag, size_t n, unsigned int tdi)
{
  /* check_ok */

  LOG2("shifting %d's: %dclks", tdi, n);
  ejtag->io_shift(ejtag, tdi, n, 0);
}


static void jtag_next_tap_state(ejtag_handle_t* ejtag, unsigned int tms)
{
  /* check_ok */

  /* After shift data into the DR or IR we goto the next state */
  /* This function should only be called from the end of a shift function */

  if (ejtag->jtag_curr_state == EJTAG_TAP_STATE_SHIFT_DR)
  {
    /* If TMS was set then goto next state */
    if (tms) ejtag->jtag_curr_state = EJTAG_TAP_STATE_EXIT1_DR;
  }
  else if (ejtag->jtag_curr_state == EJTAG_TAP_STATE_SHIFT_IR)
  {
    /* If TMS was set then goto next state */
    if (tms) ejtag->jtag_curr_state = EJTAG_TAP_STATE_EXIT1_IR;
  }
  else
  {
    LOG_ERROR("unable to set next TAP state");

    /* We were in an unexpected state */
    jtag_tap_test_logic_reset(ejtag);
  }
}

static void jtag_shift_dr
(
 ejtag_handle_t* ejtag,
 const uint8_t* tdi, uint8_t* tdo, size_t length,
 unsigned int align, unsigned int exit
)
{
  /* check_ok */

  const int post = (int)ejtag->jtag_device_index;
  const unsigned int post_zero_and_exit = (post == 0) && exit;

  if (ejtag->jtag_device_index == EJTAG_NOT_INIT)
  {
    LOG_ERROR("libejtag not initialized");
    return ;
  }

  if (ejtag->jtag_shift_dr_incomplete == 0)
  {
    int pre = (int)(ejtag->jtag_num_devices - ejtag->jtag_device_index - 1);

    if (align)
    {
      pre = -post;
      while (pre <= 0) pre += (int)align;
    }

    /* We can combine the pre bits to reach the target device with the */
    /* TMS bits to reach the SHIFT-DR state, as the pre bit can be '0'*/
    jtag_set_tap_state(ejtag, EJTAG_TAP_STATE_SHIFT_DR, pre);
  }

  if ((tdi != NULL) && (tdo != NULL))
  {
    ejtag->io_shift_tdi_tdo(ejtag, tdi, tdo, length, post_zero_and_exit);
  }
  else if ((tdi != NULL) && (tdo == NULL))
  {
    ejtag->io_shift_tdi(ejtag, tdi, length, post_zero_and_exit);
  }
  else if ((tdi == NULL) && (tdo != NULL))
  {
    ejtag->io_shift_tdo(ejtag, tdo, length, post_zero_and_exit);
  }
  else
  {
    ejtag->io_shift(ejtag, 0, length, post_zero_and_exit);
  }

  /* If TMS is set the the state of the tap changes */
  jtag_next_tap_state(ejtag, post_zero_and_exit);

  if (exit)
  {
    ejtag->io_shift(ejtag, 0, post);

    if (post_zero_and_exit == 0) jtag_next_tap_state(ejtag, 1);
    jtag_set_tap_state(ejtag, ejtag->jtag_post_dr_state, 0);
    ejtag->jtag_shift_dr_incomplete = 0;
  }
  else
  {
    ejtag->jtag_shift_dr_incomplete = 1;
  }
}

static void jtag_shift_ir
(ejtag_handle_t* ejtag, const uint8_t* tdi, uint8_t* tdo)
{
  /* check_ok */

  size_t pre;
  size_t post;
  size_t i;

  if (ejtag->jtag_device_index == EJTAG_NOT_INIT) return ;

  LOG2("IR <------- : 0x%02X", *tdi);
  jtag_set_tap_state(ejtag, EJTAG_TAP_STATE_SHIFT_IR, 0);

  pre = 0;
  for (i = ejtag->jtag_device_index + 1; i != ejtag->jtag_num_devices; ++i)
  {
    /* Calculate number of pre BYPASS bits. */
    pre += ejtag->jtag_devices[i].ir_len;
  }

  post = 0;
  for (i = 0; i != ejtag->jtag_device_index; ++i)
  {
    /* Calculate number of post BYPASS bits. */
    post += ejtag->jtag_devices[i].ir_len;
  }

  LOG2("shifting 1's: %dclks", pre);
  ejtag->io_shift(ejtag, 1, pre, 0);

  LOG2("shifting TDI: %dclks", 
    ejtag->jtag_devices[ejtag->jtag_device_index].ir_len);
  if (tdo != NULL)
  {
    ejtag->io_shift_tdi_tdo
    (
     ejtag, tdi, tdo,
     ejtag->jtag_devices[ejtag->jtag_device_index].ir_len,
     post == 0
    );
    LOG2("TDO ------> : 0x%02X", *tdo);
  }
  else if (tdo == NULL)
  {
    ejtag->io_shift_tdi
    (
     ejtag, tdi,
     ejtag->jtag_devices[ejtag->jtag_device_index].ir_len,
     post == 0
    );
  }

  LOG2("shifting 1's: %dclks", post);
  ejtag->io_shift(ejtag, 1, post);

  jtag_next_tap_state(ejtag, 1);
  jtag_set_tap_state(ejtag, ejtag->jtag_post_ir_state, 0);
}


static uint32_t uint8_array_to_uint32(const uint8_t* x)
{
  /* check_ok */

  return
    ((uint32_t)x[3] << 24) |
    ((uint32_t)x[2] << 16) |
    ((uint32_t)x[1] << 8) |
    ((uint32_t)x[0] << 0);
}


static size_t jtag_get_chain(ejtag_handle_t* ejtag, unsigned int do_detect)
{
  /* check_ok */

  /* detect chain length on first start, return chain length else */

  if ((ejtag->jtag_num_devices == EJTAG_NOT_INIT) || do_detect)
  {
    size_t i, j, maxdev;
    uint8_t idx[4];
    uint8_t zero[4];
    uint32_t id;
    const id_info_t* id_info;
    const char* name;

    jtag_tap_test_logic_reset(ejtag);
    jtag_set_tap_state(ejtag, EJTAG_TAP_STATE_SHIFT_DR, 0);

    for (i = 0; i != 4; ++i) zero[i] = 0;

    /* due to daisy chain of TAPs, the first to have given its ID is the last */
    maxdev = EJTAG_MAX_NUM_DEVICES;
    for (i = 0, j = (maxdev-1); i != (maxdev/2); ++i, --j) {

      ejtag->io_shift_tdi_tdo(ejtag, zero, idx, 32, 0);

      id = uint8_array_to_uint32(idx);
      if ((id == 0) || (id == (uint32_t)-1)) break ;
      LOG("got id : 0x%08x", id);

      ejtag->jtag_devices[j].id_code = id;
      ejtag->jtag_devices[j].id_cmd = 0;
      ejtag->jtag_devices[j].ir_len = 0;

      id_info = id_code_to_info(id);
      if (id_info != NULL) {

         ejtag->jtag_devices[j].id_cmd = id_info->id_cmd;
         ejtag->jtag_devices[j].ir_len = id_info->ir_len;

         LOG2("pos log: %d", i);
         LOG2("IR len : %dbits", id_info->ir_len);

         name=ejtag_id_code_to_name(ejtag, id_info->id_code);
         LOG2("name   : %s", name);
      } else {
        LOG2("IR len : unknown");
        LOG2("name   : unknown");
      }
    }

    jtag_set_tap_state(ejtag, EJTAG_TAP_STATE_TEST_LOGIC_RESET, 0);

    LOG("devices: %d", i);
    ejtag->jtag_num_devices = i;


    /* return the chain to match the physical daisy chain of TAPs */
    j = maxdev - i;
    size_t unknowndev = 0;
    int    unknownpos = -1;
    size_t known_ir_len = 0;
    for (i = 0; i < ejtag->jtag_num_devices; ++i, ++j) {

      ejtag->jtag_devices[i].id_code = ejtag->jtag_devices[j].id_code;
      ejtag->jtag_devices[i].id_cmd  = ejtag->jtag_devices[j].id_cmd;
      ejtag->jtag_devices[i].ir_len  = ejtag->jtag_devices[j].ir_len;
      known_ir_len += ejtag->jtag_devices[i].ir_len;

      LOG2("pos phy: %d", i);
      if (ejtag->jtag_devices[i].ir_len != 0) 
      {
        LOG2("IR len : %dbits", ejtag->jtag_devices[i].ir_len);
        name=ejtag_id_code_to_name(ejtag, ejtag->jtag_devices[i].id_code);
        LOG2("name   : %s", name);
      } else {
        unknowndev++;
        unknownpos = i;
        LOG2("IR len : unknown");
        LOG2("name   : unknown");
      }

    }

    /* if more than one device is unknown, no way to guess their IR lengths */
    if (unknowndev > 1) {
      LOG_ERROR("unable to handle a chain with more than one unknown device");
      ejtag->jtag_num_devices = -1;
      goto normal_end;
    }
    if (unknowndev == 0) {
      goto normal_end;
    }

    /* measure IRs total length */
    LOG("measuring IRs total length");
    jtag_set_tap_state(ejtag, EJTAG_TAP_STATE_SHIFT_IR, 0);

    /* arbitrary numner of 1's but larger than expented total length */
    size_t  nbits = ejtag->jtag_num_devices * 64;
    uint8_t tdis[nbits / 8];
    uint8_t tdos[nbits / 8];
    for (i = 0; i < sizeof(tdis); ++i) tdis[i] = 0xff;

    LOG2("shifting 1's: %dclks", nbits);
    ejtag->io_shift_tdi_tdo(ejtag, tdis, tdos, nbits, 0);

    for (i = 0; i < sizeof(tdis); ++i) tdis[i] = 0x00;
    ejtag->io_shift_tdi_tdo(ejtag, tdis, tdos, nbits, 0);

    size_t  ir_len = 0;
    for (i = 0; i < sizeof(tdos); ++i) {
      if (tdos[i] == 0xff) {
        ir_len += 8;
        continue;
      }

      for (j = 0; j < 8; ++j, ir_len++) {
        if (((1 << j) & tdos[i]) == 0)
           break;
      }
    }
    ir_len -= known_ir_len;
    LOG("unknown IR measured to: %dbits", ir_len);
    ejtag->jtag_devices[unknownpos].ir_len  = ir_len;


    jtag_set_tap_state(ejtag, EJTAG_TAP_STATE_TEST_LOGIC_RESET, 0);
  }

normal_end:
  return ejtag->jtag_num_devices;
}


/* exported */

int ejtag_init_lib(void)
{
  return 0;
}


void ejtag_fini_lib(void)
{
}

int ejtag_log_lib(int level)
{
  if(level >= 0)
  {
    libejtag_debug = level;
    return 0;
  }
  return -1;
}



int ejtag_foreach_dev(int (*fn)(const ejtag_desc_t*, void*), void* data)
{
  return -1;
}


uint32_t ejtag_probe_link(void)
{
  /* return EJTAG_FLAG_FTDI or EJTAG_FLAG_I2C */

  struct ftdi_context ftdi;

  if (ftdi_init(&ftdi) == 0)
  {
    int ndev;
    static const unsigned int ft232h_vid = 0x0403;
    static const unsigned int ft232h_pid = 0x6014;
    struct ftdi_device_list* devs;
    ndev = ftdi_usb_find_all(&ftdi, &devs, ft232h_vid, ft232h_pid);
    if (ndev >= 0) ftdi_list_free(&devs);
    ftdi_deinit(&ftdi);
    if (ndev) return EJTAG_FLAG_FTDI;
  }

  return EJTAG_FLAG_I2C;
}


void ejtag_init_desc(ejtag_desc_t* desc)
{
  /* check_ok */

  desc->flags = 0;
  desc->jtag_pos = 0;
  desc->jtag_freq = 0;
}


int ejtag_open(ejtag_handle_t* ejtag, const ejtag_desc_t* desc)
{
  /* check_ok */

  ejtag->flags = desc->flags;
  if (jtag_open(ejtag)) goto on_error_0;
  if (io_open(ejtag, desc)) goto on_error_1;

  /* success */

  return 0;

 on_error_1:
  jtag_close(ejtag);
 on_error_0:
  return -1;
}


int ejtag_open_default(ejtag_handle_t* ejtag)
{
  /* check_ok */

  ejtag_desc_t desc;

  ejtag_init_desc(&desc);
  desc.flags |= EJTAG_FLAG_FTDI;
  return ejtag_open(ejtag, &desc);
}


void ejtag_close(ejtag_handle_t* ejtag)
{
  /* check_ok */

  io_close(ejtag);
  jtag_close(ejtag);
}


int ejtag_detect_chain
(ejtag_handle_t* ejtag, const ejtag_dev_info_t** d, size_t* n)
{
  /* check_ok */

  if (jtag_get_chain(ejtag, 1) == (size_t)-1) {
    LOG_ERROR("chain scanning failed");
    return -1;
  }

  *d = ejtag->jtag_devices;
  *n = ejtag->jtag_num_devices;

  return 0;
}


const char* ejtag_id_code_to_name(ejtag_handle_t* ejtag, uint32_t id_code)
{
  /* check_ok */

  const id_info_t* const info = id_code_to_info(id_code);
  if (info == NULL) return "unknown";
  return info->dev_name;
}


/* programming logic for xc3s fpgas */
/* from xc3sprog/progalgxc3s.{cpp,h} */

#define FAMILY_XC2S 0x03
#define FAMILY_XC2SE 0x05
#define FAMILY_XC2V 0x08
#define FAMILY_XC3S 0x0a
#define FAMILY_XC4VLX 0x0b
#define FAMILY_XC3SE 0x0e
#define FAMILY_XC4VFX 0x0f
#define FAMILY_XC4VSX 0x10
#define FAMILY_XC3SA 0x11
#define FAMILY_XC3SAN 0x13
#define FAMILY_XC5VLX 0x14
#define FAMILY_XC5VLXT 0x15
#define FAMILY_XC5VSXT 0x17
#define FAMILY_XC5VFXT 0x19
#define FAMILY_XC7 0x1b
#define FAMILY_XC3SD 0x1c
#define FAMILY_XC6S 0x20
#define FAMILY_XC5VTXT 0x22


static const uint8_t JPROGRAM[2] = { 0xcb, 0xff };
static const uint8_t CFG_IN[2] = { 0xc5, 0xff };
static const uint8_t JSHUTDOWN[2] = { 0xcd, 0xff };
static const uint8_t JSTART[2] = { 0xcc, 0xff };
static const uint8_t ISC_PROGRAM[2] = { 0xd1, 0xff };
static const uint8_t ISC_ENABLE[2]  = { 0xd0, 0xff };
static const uint8_t ISC_DISABLE[2] = { 0xd6, 0xff };
static const uint8_t BYPASS[2] = { 0xff, 0xff };
static const uint8_t ISC_DNA[1] = { 0x31 };


static void prog_enable(ejtag_handle_t* ejtag, size_t tck_len)
{
  /* check_ok */

  /* progalgxc3s.cpp, flow_enable */

  uint8_t data[1];

  jtag_shift_ir(ejtag, ISC_ENABLE, NULL);
  data[0] = 0x0;
  jtag_shift_dr(ejtag, data, NULL, 5, 0, 1);
  jtag_cycle_tck(ejtag, tck_len, 1);
}


static void prog_disable(ejtag_handle_t* ejtag, size_t tck_len)
{
  /* check_ok */

  /* progalgxc3s.cpp, flow_disable */

  uint8_t data[1];

  jtag_shift_ir(ejtag, ISC_DISABLE, NULL);
  jtag_cycle_tck(ejtag, tck_len, 1);
  jtag_shift_ir(ejtag, BYPASS, NULL);
  data[0] = 0x0;
  jtag_shift_dr(ejtag, data, NULL, 1, 0, 1);
  jtag_cycle_tck(ejtag, 1, 1);
}


__attribute__((unused))
static void prog_program
(ejtag_handle_t* ejtag, const uint8_t* data, size_t size, size_t trans_len)
{
  /* progalgxc3s.cpp, flow_array_program */

  size_t i;

  for (i = 0; i < size; i += trans_len)
  {
    jtag_shift_ir(ejtag, ISC_PROGRAM, NULL);
    jtag_shift_dr(ejtag, data + i / 8, NULL, trans_len, 0, 1);
    jtag_cycle_tck(ejtag, 1, 1);
  }
}


static void prog_program_legacy
(ejtag_handle_t* ejtag, const uint8_t* data, size_t size, size_t tck_len)
{
  /* check_ok */

  /* progalgxc3s.cpp, flow_program_legacy */

  uint8_t tmp[2];

  LOG1("bit raw size: %d", size);

  jtag_shift_ir(ejtag, JSHUTDOWN, NULL);
  jtag_cycle_tck(ejtag, tck_len, 1);
  jtag_shift_ir(ejtag, CFG_IN, NULL);

  LOG1("beg of data shifting in DR ------------");
  /* warning: size in bits, multiply by 8 */
  jtag_shift_dr(ejtag, data, NULL, size * 8, 0, 1);
  LOG1("end of data shifting in DR ------------");

  jtag_cycle_tck(ejtag, 1, 1);
  jtag_shift_ir(ejtag, JSTART, NULL);
  jtag_cycle_tck(ejtag, 2 * tck_len, 1);
  jtag_shift_ir(ejtag, BYPASS, NULL);
  tmp[0] = 0x0;
  jtag_shift_dr(ejtag, tmp, NULL, 1, 0, 1);
  jtag_cycle_tck(ejtag, 1, 1);
}


int ejtag_program_raw_data
(ejtag_handle_t* ejtag, size_t pos, const uint8_t* raw_data, size_t raw_size)
{
  /* pos the jtag chain position */

  const ejtag_dev_info_t* di;
  uint8_t buf[1] = { 0 };
  size_t i;
  size_t tck_len;
//  size_t trans_len;

  if (jtag_get_chain(ejtag, 1) == (size_t)-1)
  {
    LOG_ERROR("chain scanning failed");
    goto on_error_0;
  }

  /* select the device at pos */

  if (pos >= ejtag->jtag_num_devices)
  {
    LOG_ERROR("invalid chain position");
    goto on_error_0;
  }

  di = &ejtag->jtag_devices[pos];

  if (di->ir_len == 0)
  {
    /* ensure device is known */
    LOG_ERROR("unknown IR length");
    goto on_error_0;
  }

  ejtag->jtag_device_index = pos;

  /* check this is a xilinx fpga */

  LOG1("got id : 0x%08x", di->id_code);
  LOG2("IR len : %dbits", di->ir_len);
  if (id_code_to_manufacturer(di->id_code) != 0x049)
  {
    LOG_ERROR("target not a XILINX FPGA");
    goto on_error_0;
  }

  switch (id_code_to_family(di->id_code))
  {
  case FAMILY_XC3SE:
  case FAMILY_XC3SA:
  case FAMILY_XC3SAN:
  case FAMILY_XC3SD:
    tck_len = 16;
//    trans_len = 16;
    break ;

  case FAMILY_XC5VLX:
  case FAMILY_XC5VLXT:
  case FAMILY_XC5VSXT:
  case FAMILY_XC5VFXT:
  case FAMILY_XC5VTXT:
  case FAMILY_XC7:
    tck_len = 12;
//    trans_len = 32;
    break ;

  default:
    tck_len = 12;
//    trans_len = 64;
    break ;
  }

  prog_enable(ejtag, tck_len);

  jtag_shift_ir(ejtag, JPROGRAM, NULL);

  /* wait until configuration cleared */
  do
  {
    jtag_shift_ir(ejtag, CFG_IN, buf);
  }
  while (!(buf[0] & 0x10));

  /* As ISC_DNA only works on a unconfigured device, see AR #29977 */
  switch (id_code_to_family(di->id_code))
  {
  case 0x11: /* XC3SA */
  case 0x13: /* XC3SAN */
  case 0x1c: /* SC3SADSP */
  case 0x20: /* SC3SADSP */
    {
      uint8_t data[8];
      jtag_shift_ir(ejtag, ISC_ENABLE, NULL);
      jtag_shift_ir(ejtag, ISC_DNA, NULL);
      jtag_shift_dr(ejtag, NULL, data, 64, 0, 1);
      jtag_cycle_tck(ejtag, 1, 1);
    }
    break ;
  }

  /* use legacy, as large USB transfers are faster then chunks */
  LOG1("start programming...");
  prog_program_legacy(ejtag, raw_data, raw_size, tck_len);
  prog_disable(ejtag, tck_len);

  /* NOTE: Virtex7 devices do not support flow_program_legacy */
  /* according to the Xilinx IEEE 1532 files. However, my tests */
  /* with flow_array_program failed, while flow_program_legacy */
  /* appears to work just fine on XC7VX690T. (jorisvr) */

  LOG1("waiting for FPGA to comes up");
  for (i = 0; ((buf[0] & 0x23) != 0x21) && (i != 50); ++i)
  {
    jtag_shift_ir(ejtag, BYPASS, buf);
    ejtag->io_usleep(ejtag, 1000);
  }

  if (i == 50)
  {
    LOG_ERROR("Timeout waiting for FPGA up");
    return -1;
  }

  return 0;

 on_error_0:
  return -1;
}


/* bitstream programming */


#include <fcntl.h>
#include <ctype.h>
#include <sys/mman.h>
#include <sys/stat.h>


typedef struct bit_handle
{
  const uint8_t* file_data;
  size_t file_size;
  size_t file_pos;
} bit_handle_t;


static void* mmap_ronly(int fd, size_t size)
{
  return mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
}


static int bit_read_data
(bit_handle_t* bit, uint8_t** buf_data, size_t* buf_size)
{
  static const uint8_t bit_rev_table[256] =
  {
    0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0,
    0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0,
    0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8,
    0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8,
    0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4,
    0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
    0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec,
    0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc,
    0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2,
    0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2,
    0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea,
    0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
    0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6,
    0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6,
    0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee,
    0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe,
    0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1,
    0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
    0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9,
    0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9,
    0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5,
    0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5,
    0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed,
    0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
    0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3,
    0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3,
    0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb,
    0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb,
    0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7,
    0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
    0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef,
    0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff,
  };

  const uint8_t* const p = bit->file_data + bit->file_pos;
  const size_t n = bit->file_size - bit->file_pos;
  size_t size;
  size_t i;

  if (n < 4) return -1;

  size =
    (((size_t)p[0]) << 24) |
    (((size_t)p[1]) << 16) |
    (((size_t)p[2]) << 8) |
    (((size_t)p[3]) << 0);

  if (size > (n - 4)) return -1;

  *buf_data = malloc(size);
  if (*buf_data == NULL) return -1;
  *buf_size = size;

  for (i = 0; i != size; ++i) (*buf_data)[i] = bit_rev_table[p[4 + i]];

  bit->file_pos += 4 + size;

  return 0;
}


static int bit_skip_field(bit_handle_t* bit)
{
  const uint8_t* const p = bit->file_data + bit->file_pos;
  const size_t n = bit->file_size - bit->file_pos;
  size_t size;

  if (n < 2) return -1;

  size = (((size_t)p[0]) << 8) | (size_t)p[1];
  if (size > (n - 2)) return -1;

  bit->file_pos += 2 + size;

  return 0;
}


static int bit_read
(bit_handle_t* bit, uint8_t** buf_data, size_t* buf_size)
{
  /* xc3sprog, BitFile::readBitFile */

  *buf_data = NULL;
  *buf_size = 0;

  /* skip bit header */
  bit->file_pos = 13;

  while (bit->file_pos != bit->file_size)
  {
    const uint8_t k = bit->file_data[bit->file_pos++];
    switch (k)
    {
    case 'e': /* data */
      return bit_read_data(bit, buf_data, buf_size);
      break ;

    case 'a': /* ncd filename */
    case 'b': /* fpga partname */
    case 'c': /* date */
    case 'd': /* dtime */
    default:
      if (bit_skip_field(bit)) return -1;
      break ;
    }
  }

  /* no data read */
  return -1;
}


static int bit_open(bit_handle_t* bit, const char* path)
{
  int fd;
  struct stat stat;

  /* open and map the file */

  fd = open(path, O_RDONLY);
  if (fd == -1)
  {
    LOG_ERROR("unable to access file: %s", path);
    goto on_error_0;
  }

  if (fstat(fd, &stat))
  {
    LOG_ERROR("unable to open file: %s", path);
    goto on_error_1;
  }

  /* mapped size is one page larger because of rounding up */

  bit->file_size = stat.st_size;
  bit->file_data = mmap_ronly(fd, bit->file_size);
  if (bit->file_data == (uint8_t*)MAP_FAILED)
  {
    LOG_ERROR("unable to read file: %s", path);
    goto on_error_1;
  }

  close(fd);

  return 0;

 on_error_1:
  close(fd);
 on_error_0:
  return -1;
}


static int bit_close(bit_handle_t* bit)
{
  munmap((void*)bit->file_data, bit->file_size);
  return 0;
}


int ejtag_program_bit_data
(ejtag_handle_t* ejtag, size_t pos, const uint8_t* bit_data, size_t bit_size)
{
  /* use a dummy bit_handle from caller passed data and program */

  bit_handle_t bit;
  uint8_t* buf_data;
  size_t buf_size;
  int err = -1;

  bit.file_data = bit_data;
  bit.file_size = bit_size;

  if (bit_read(&bit, &buf_data, &buf_size))
  {
    LOG_ERROR("unable to read bit file");
    goto on_error_0;
  }

  if (ejtag_program_raw_data(ejtag, pos, buf_data, buf_size))
  {
    LOG_ERROR("unable to program FPGA");
    goto on_error_1;
  }

  err = 0;

 on_error_1:
  free(buf_data);
 on_error_0:
  return err;
}


int ejtag_program_bit_file
(ejtag_handle_t* ejtag, size_t pos, const char* bit_path)
{
  int err = -1;
  bit_handle_t bit;

  if (bit_open(&bit, bit_path))
  {
    LOG_ERROR("unable to use bit file");
    goto on_error_0;
  }

  if (ejtag_program_bit_data(ejtag, pos, bit.file_data, bit.file_size))
  {
    LOG_ERROR("unable to program bit file");
    goto on_error_1;
  }

  err = 0;

 on_error_1:
  bit_close(&bit);
 on_error_0:
  return err;
}
