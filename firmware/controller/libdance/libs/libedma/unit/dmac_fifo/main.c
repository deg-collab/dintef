#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include "libedma.h"
#include "libebuf.h"
#include "libepci.h"


#define CONFIG_DEBUG 1
#if CONFIG_DEBUG
#include <stdio.h>
#define PRINTF(__s, ...)  \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define PERROR() printf("[!] %d\n", __LINE__)
#define ASSERT(__x) if (!(__x)) printf("[!] %d\n", __LINE__)
#else
#define PRINTF(__s, ...)
#define PERROR()
#define ASSERT(__x)
#endif


/* fifo related */

static void write_gctrl(epcihandle_t pci, uint32_t x)
{
  epci_wr32_reg(pci, 0x80, x);
}

static void write_f1fsmc(epcihandle_t pci, uint32_t x)
{
  epci_wr32_reg(pci, 0x84, x);
}

static void flush_fifo(epcihandle_t pci)
{
  /* flush the fifo */

  write_gctrl(pci, 0xf);
  write_gctrl(pci, 0xd);
}

static void write_fifo(epcihandle_t pci)
{
#define DELAY_UNIT_1_US 0x0
#define DELAY_UNIT_10_US 0x1
#define DELAY_UNIT_100_US 0x2
#define DELAY_UNIT_1_MS 0x3
#define DELAY_UNIT_SINGLE_BURST 0x0
#define NEXT_DELAY_SINGLE_BURST 0x0

  static const uint32_t delay_unit = DELAY_UNIT_10_US;
  static const uint32_t next_delay = 32;
  static const uint32_t intra_clk = 0; /* 255 max */
/* #define FIFO_DEPTH 0x100 */
#define FIFO_DEPTH (2048 / 8)
#define FIFO_WIDTH (sizeof(uint64_t))
  const uint32_t depth = FIFO_DEPTH; /* 0xffff max */

  /* fsm setup prolog */

  write_gctrl(pci, 0xf);

  /* setup fsm */

  write_f1fsmc
    (pci, (delay_unit << 30) | (next_delay << 24) | (intra_clk << 16) | depth);

  /* fsm setup epilog */

  write_gctrl(pci, 0xd);
}


/* main */

int main(int ac, char** av)
{
  static const unsigned int bar = 1;

  edma_handle_t dma;
  edma_desc_t dma_desc;
  ebuf_handle_t buf;
  epcihandle_t pci;
  size_t mem_size;
  size_t i;
  int err = -1;

  if ((pci = epci_open("10ee:eb01", NULL, bar)) == EPCI_BAD_HANDLE)
  {
    PERROR();
    goto on_error_0;
  }

  /* allocate buffer */

  if (ebuf_init_lib(EBUF_FLAG_PHYSICAL))
  {
    PERROR();
    goto on_error_1;
  }

  mem_size = FIFO_DEPTH * FIFO_WIDTH;

  if (ebuf_alloc(&buf, mem_size, EBUF_FLAG_PHYSICAL))
  {
    PERROR();
    goto on_error_2;
  }

  memset(ebuf_get_data(&buf), 0x2a, mem_size);

  /* init dmac */

  if (edma_init_lib())
  {
    PERROR();
    goto on_error_3;
  }

  if (eaio_init_lib())
  {
    PERROR();
    goto on_error_4;
  }

  edma_init_desc(&dma_desc);

  dma_desc.pci_id = "10ee:eb01";
  dma_desc.pci_bar = 1;
  dma_desc.pci_off = 0x100;

  /* info should come from ram handle */
  dma_desc.flags |= EDMA_FLAG_MIF;
  dma_desc.ebs_seg = 0x0;
  dma_desc.ebs_off = 0x0;

  dma_desc.flags |= EDMA_FLAG_IRQ;

  dma_desc.soff = 0;
  dma_desc.ssize = mem_size;

  if (edma_open(&dma, &dma_desc))
  {
    PERROR();
    goto on_error_5;
  }

 redo:
  /* write read print fifo */

  write_fifo(pci);

  memset(ebuf_get_data(&buf), 0x2a, mem_size);
  edma_read(&dma, 0, &buf, 0, mem_size);

  for (i = 0; i != mem_size / sizeof(uint64_t); ++i)
  {
    const uint64_t x = ((const uint64_t*)ebuf_get_data(&buf))[i];
    printf("%016llx\n", x);
  }
  printf("\n");

  /* getchar(); */

  goto redo;

  err = 0;

  edma_close(&dma);
 on_error_5:
  eaio_fini_lib();
 on_error_4:
  edma_fini_lib();
 on_error_3:
  ebuf_free(&buf);
 on_error_2:
  ebuf_fini_lib();
 on_error_1:
  epci_close(pci);
 on_error_0:
  return err;
}
