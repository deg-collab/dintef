#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/select.h>
#include "libepci.h"
#include "libedma.h"
#include "libebuf.h"


/* configuration macros */
#define CONFIG_DEBUG 1


#if CONFIG_DEBUG
#include <stdio.h>
#define PRINTF(__s, ...)  \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define PERROR() printf("[!] %d\n", __LINE__)
#define ASSERT(__x) if (!(__x)) printf("[!] %d\n", __LINE__)
#else
#define PRINTF(__s, ...)
#define PERROR()
#deifne ASSERT(__x)
#endif


/* fill a memory. internal hackery still required that will be removed. */

static int fill_mif(size_t size)
{
  /* write the mif using utility port */

  static const unsigned int fifo_dw_count = 256;
  static const size_t ebs_reg1 = 0x200;
  static const uint32_t ut_cmd = 0x3;
  epcihandle_t bar1;
  uint32_t value;

  bar1 = epci_open("10ee:eb01", NULL, 1);
  if (bar1 == EPCI_BAD_HANDLE) return -1;

  epci_wr32_reg(bar1, (ebs_reg1 + 0x04), 0);

  /* ctrl_size */
  value = size / (sizeof(uint32_t) * fifo_dw_count);
  if ((size % (sizeof(uint32_t) * fifo_dw_count))) ++value;
  epci_wr32_reg(bar1, (ebs_reg1 + 0x08), value);

  /* ut_cmd_code */
  epci_wr32_reg(bar1, (ebs_reg1 + 0x00), ut_cmd);

  /* no operation completion hint, wait */
  usleep(100000);

  epci_close(bar1);

  return 0;
}

static void set_bram_mode(epcihandle_t bar0, uint32_t mode)
{
  uint32_t x;
  epci_rd32_reg(bar0, 0x0, &x);
  x &= 0xffffcfff;
  x |= mode << 12;
  epci_wr32_reg(bar0, 0x0, x);
}

static void make_pattern(uint8_t* p, size_t i, size_t w)
{
  for (; w; --w, ++p) *p = i;
}

static int fill_bram(size_t size)
{
  /* assume bram EBS_WIDTH is 32 bits */
  /* assume bram bar is 3 */

  static const size_t ebs_dwidth = sizeof(uint32_t);

  int err = -1;
  size_t i;
  epcihandle_t bar0;
  epcihandle_t bar3;

  bar0 = epci_open("10ee:eb01", NULL, 0);
  if (bar0 == EPCI_BAD_HANDLE)
  {
    PERROR();
    goto on_error_0;
  }

  bar3 = epci_open("10ee:eb01", NULL, 3);
  if (bar3 == EPCI_BAD_HANDLE)
  {
    PERROR();
    goto on_error_1;
  }

  /* disable bram slave mode */
  set_bram_mode(bar0, 0);

  for (i = 0; i < (size / ebs_dwidth); ++i)
  {
    uint32_t x;
    make_pattern((uint8_t*)&x, i, ebs_dwidth);
    epci_wr32_reg(bar3, i, x);
  }

  /* enable bram slave mode */
  set_bram_mode(bar0, 2);

  /* success */
  err = 0;

  epci_close(bar0);
 on_error_1:
  epci_close(bar3);
 on_error_0:
  return err;
}

static int fill_mem(const edma_desc_t* desc, size_t size)
{
  if (desc->flags & EDMA_FLAG_BRAM) return fill_bram(size);
  else if (desc->flags & EDMA_FLAG_MIF) return fill_mif(size);
  /* else */
  return -1;
}

static void make_pattern_ddr_128(uint8_t* p, size_t i)
{
  /* hardcoded pattern */
  *((uint32_t*)p + 0) = i;
  *((uint32_t*)p + 1) = i;
  *((uint32_t*)p + 2) = i;
  *((uint32_t*)p + 3) = i;
}

static int check_buf(edma_handle_t* dma, ebuf_handle_t* buf, size_t size)
{
  const uint8_t* const data = ebuf_get_data(buf);
  const size_t ebft_dwidth = dma->ebft_dwidth;
  size_t i;

  /* max pattern size */
  uint8_t pattern[512 / 8];

  for (i = 0; i < size / ebft_dwidth; ++i)
  {
    const size_t j = i * ebft_dwidth;

    make_pattern_ddr_128(pattern, i);

    if (memcmp(pattern, data + j, ebft_dwidth))
    {
      printf("error at %u\n", (unsigned int)i);
      {
	size_t k;
	for (k = 0; k < ebft_dwidth; ++k) printf("%02x", pattern[k]);
	printf("\n");
	for (k = 0; k < ebft_dwidth; ++k) printf("%02x", data[j + k]);
	printf("\n");
	return -1;
      }
    }
  }

  printf("x\n");

  return 0;
}


static void wait_any_aio(void)
{
  const int rfd = eaio_get_wait_fd();
  fd_set rds;
  uint8_t x;

  FD_ZERO(&rds);
  FD_SET(rfd, &rds);
  select(rfd + 1, &rds, NULL, NULL, NULL);
  read(rfd, &x, 1);

  return 0;
}


/* main */

int main(int ac, char** av)
{
  static const size_t mem_size = 32 * 1024;

  ebuf_handle_t buf;
  edma_handle_t dma;
  struct edma_desc desc;
  int err = -1;

  setup_probe();

  /* open edma */

  if (edma_init_lib())
  {
    PERROR();
    goto on_error_0;
  }

  edma_init_desc(&desc);

  desc.pci_id = "10ee:eb01";
  desc.pci_bar = 1;
  desc.pci_off = 0x400;

  /* info should come from ram handle */
  desc.flags = EDMA_FLAG_MIF;
  desc.ebs_seg = 0x0;
  desc.ebs_off = 0x200;

  if (edma_open(&dma, &desc))
  {
    PERROR();
    goto on_error_1;
  }

  /* allocate a buffer */

  if (ebuf_init_lib(EBUF_FLAG_PHYSICAL))
  {
    PERROR();
    goto on_error_2;
  }

  if (ebuf_alloc(&buf, mem_size, EBUF_FLAG_PHYSICAL))
  {
    PERROR();
    goto on_error_3;
  }

  memset(ebuf_get_data(&buf), 0x2a, mem_size);

  if (fill_mem(&desc, mem_size))
  {
    PERROR();
    goto on_error_4;
  }

  /* asynchronous readout */

  while (1)
  {
    /* wait for signal */
    wait_any_aio();

    /* read and check data */
    while (1)
    {
      size_t async_size;
      void* async_data;

      edma_get_async_data(&dma, &async_data, &async_size);

      /* wait for at least depth data */
      if (async_size < burst_size) break ;

      async_size = burst_size;

      if (check_buf(&dma, &buf, mem_size))
      {
	PERROR();
	goto on_error_4;
      }

      if (((++pass) % 1000) == 0) printf("%zu\n", pass);

      edma_seek_async_data(&dma, async_size);
    }
  }

  /* success */
  err = 0;

 on_error_4:
  ebuf_free(&buf);
 on_error_3:
  ebuf_fini_lib();
 on_error_2:
  edma_close(&dma);
 on_error_1:
  edma_fini_lib();
 on_error_0:
  return err;
}
