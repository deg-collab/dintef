#include <errno.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include "libedma.h"
#include "libebuf.h"
#include "libpmem.h"
#include "libuirq.h"
#include "libepci.h"


#define CONFIG_DEBUG 1
#if CONFIG_DEBUG
#include <stdio.h>
#define PRINTF(__s, ...)  \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define PERROR() printf("[!] %d\n", __LINE__)
#define ASSERT(__x) if (!(__x)) printf("[!] %d\n", __LINE__)
#else
#define PRINTF(__s, ...)
#define PERROR()
#define ASSERT(__x)
#endif


/* dma api */

typedef struct dma_mem
{
  /* int bytes */
  size_t size;

  /* in bytes */
  unsigned int ebs_dwidth;

} dma_mem_t;

static dma_mem_t ddr_mem = { 128 * 1024 * 1024, sizeof(uint32_t) };

/* WARNING */
/* bram ebs_dwidth must be sizeof(uint32_t) or pattern generation wont work */
static dma_mem_t bram_mem = { 64 * 1024, sizeof(uint32_t) };

typedef struct io_info
{
  /* wrapper around io information */

  /* handles */
  edma_handle_t* dma;
  ebuf_handle_t* buf;

  /* completion error */
  int err;

  /* transfer source dest offsets, size */
  size_t read_soff;
  size_t read_doff;
  size_t read_size;

  /* slave data width */
  size_t ebs_dwidth;

  /* statistics */
  uint64_t start_ticks;
  uint64_t stop_ticks;

} io_info_t;


/* helper routines */

static inline unsigned int dword_to_byte(unsigned int n)
{
#define DWORD_SIZE 4
  return n * DWORD_SIZE;
}

static inline unsigned int byte_to_dword(unsigned int n)
{
  /* warning: rounded down */
  return n / DWORD_SIZE;
}


__attribute__((unused)) static void reset_ebone(void)
{
  epcihandle_t bar;
  bar = epci_open("10ee:eb01", NULL, 0);
  epci_wr32_reg(bar, 0x140 + 0x0c, 1 << 31);
  epci_close(bar);

  /* warning: when ebone is reset, ddr calibration is done
     which can require several millisecond. removing this
     delay leads to an invalid first payload.
   */
  usleep(100000);
}

static void set_bram_mode(epcihandle_t bar0, uint32_t mode)
{
  uint32_t x;
  epci_rd32_reg(bar0, 0x0, &x);
  x &= 0xffffcfff;
  x |= mode << 12;
  epci_wr32_reg(bar0, 0x0, x);
}

/* memory read write related */

__attribute__((unused)) static inline int soft_write
(
 ebuf_handle_t* buf,
 epcihandle_t mem_bar, size_t mem_off, size_t mem_size,
 size_t ebs_dwidth
)
{
  void* const data = ebuf_get_data(buf);
  epci_wr32_blk(mem_bar, mem_off, 1, mem_size / sizeof(uint32_t), data);
  return 0;
}


/* retrieve ddr size */

static size_t get_ddr_size(epcihandle_t bar)
{
  /* mif_status register offset is 0x240 */
  /* return the size, in bytes */

  const size_t scale = 16 * 1024 * 1024;
  uint32_t x;
  epci_rd32_reg(bar, 0x240, &x);
  return ((size_t)(x & ((1 << 8) - 1))) * scale;
}

/* fill the memory port using utility port */

static unsigned int ut_is_running(epcihandle_t bar)
{
  /* mif_status register offset is 0x240 */
  /* ut_running bit offset is 20 */

  uint32_t x;
  epci_rd32_reg(bar, 0x240, &x);
  return x & (1 << 20);
}

static void ut_wait(epcihandle_t bar)
{
  while (ut_is_running(bar)) usleep(1000);
}

static void ut_stop_if_running(epcihandle_t bar)
{
  static const size_t ebs_reg1 = 0x200;

  if (ut_is_running(bar))
  {
#define CONFIG_UT_CMD 0x3
    epci_wr32_reg(bar, (ebs_reg1 + 0x00), CONFIG_UT_CMD);
    ut_wait(bar);
  }
}

static size_t get_ddr_wordsize(epcihandle_t bar)
{
  /* mif_status register offset is 0x240 */
  /* return the word size, in bytes */

  size_t word_size;
  uint32_t x;

  epci_rd32_reg(bar, 0x240, &x);
  word_size = (size_t)(1 << (2 + ((x >> 8) & 3)));
  return word_size;
}

static void ut_start
(epcihandle_t bar, size_t size, unsigned int is_loop)
{
  /* ut_port size is in fifo_dw_count * word_size units */

  static const unsigned int fifo_dw_count = 256;
  static const size_t ebs_reg1 = 0x200;
  const size_t word_size = get_ddr_wordsize(bar);
  uint32_t value;

  epci_wr32_reg(bar, (ebs_reg1 + 0x04), 0);

  /* ctrl_size */
  value = size / (word_size * fifo_dw_count);
  if ((size % (word_size * fifo_dw_count))) ++value;
  epci_wr32_reg(bar, (ebs_reg1 + 0x08), value);

  /* ut_cmd_code: fixed pattern + swapped word32 */
  value = CONFIG_UT_CMD;
  if (is_loop) value |= 1 << 3;
  epci_wr32_reg(bar, (ebs_reg1 + 0x00), value);
}

static int ut_write(epcihandle_t bar, size_t size, unsigned int is_utw)
{
  /* size, word_size in bytes */

  ut_stop_if_running(bar);
  ut_start(bar, size, 0);

  if (is_utw)
  {
    ut_wait(bar);
  }
  else
  {
    /* sleep a bit before passing */
    usleep(100000);
  }

  return 0;
}

/* fill memory with a known pattern */

static unsigned int seed = 0;

__attribute__((unused))
static void make_pattern(uint8_t* p, size_t i, size_t w)
{
  for (; w; --w, p += w) *(uint32_t*)p = (uint32_t)(seed + i);
}

static int fill_mem
(
 epcihandle_t bar[3],
 ebuf_handle_t* buf,
 size_t mem_off, size_t mem_size,
 size_t ebs_dwidth,
 unsigned int is_utw,
 unsigned int is_bram
)
{
  /* assume buf large enough */

  if (is_bram == 0)
  {
    ut_write(bar[1], mem_size, is_utw);
  }
  else /* bram, soft write */
  {
    uint8_t* const data = ebuf_get_data(buf);

    size_t i;
    size_t count;

    seed = 0x00000000;

    count = mem_size / sizeof(uint32_t);
    for (i = 0; i < count; ++i)
      *((uint32_t*)(data + i * sizeof(uint32_t))) = (uint32_t)i;

    /* BRAM in EBONE slave mode */
    set_bram_mode(bar[0], 0);

    soft_write(buf, bar[2], mem_off, mem_size, ebs_dwidth);

    /* BRAM in EBFT slave mode */
    set_bram_mode(bar[0], 2);
  }

  return 0;
}


/* memory printing */

static void print_buffer
(ebuf_handle_t* buf, size_t off, size_t size, size_t step)
{
  const uint8_t* p = ebuf_get_data(buf);
  size_t i;

  for (i = 0; i < size; ++i)
  {
    if ((i % step) == 0) printf("\n[ 0x%016zx ]: ", off + i);
    printf("%02x", p[off + i]);
  }
  printf("\n");
}


/* memory checking */

#define CONFIG_RESET_BYTE 0x2a

static void reset_buffer(ebuf_handle_t* buf)
{
  memset(ebuf_get_data(buf), CONFIG_RESET_BYTE, ebuf_get_size(buf));

#if defined(CONFIG_FREESCALE_IMX6)
  /* reread the memory, to be be sure cache is invalidated */
  const uint8_t* p = ebuf_get_data(buf);
  size_t i;
  for (i = 0; i < ebuf_get_size(buf); ++i, ++p)
  {
    if (*p != CONFIG_RESET_BYTE)
    {
      printf("reset buffer failed\n");
      fflush(stdout);
      exit(-1);
    }
  }
#endif /* CONFIG_FREESCALE_IMX6 */
}

__attribute__((unused))
static void make_pattern2(uint8_t* p, size_t i, size_t w)
{
  /* hardcoded pattern */
  /* the bram is 32 bit wide when written by soft but 64 bit wide */
  /* when read by dma. the second 32 bits are filled with a known */
  /* pattern */

  const size_t ww = w;
  for (w = ww; w; --w, ++p) *p = (uint8_t)((seed + i) & 0xffffffff);
  for (w = ww; w; --w, ++p) *p = (uint8_t)((seed + i) & 0x99999999);
  for (w = ww; w; --w, ++p) *p = (uint8_t)((seed + i) & 0xaaaaaaaa);
  for (w = ww; w; --w, ++p) *p = (uint8_t)((seed + i) & 0x55555555);
}

static uint8_t* make_ut_pattern(size_t w, size_t i, size_t o, size_t n)
{
  /* i is a 32 bits key repeated ii times so that: */
  /* sizeof(uint32_t) * ii = n */

  /* everything in bytes */
  /* w the ebft_dwidth */
  /* i the position */
  /* o the offset in pattern */
  /* n the size in pattern */

  static uint8_t pattern[512 / 8];
  uint32_t* pp;
  size_t oo;
  size_t ii;
  size_t j;

  memset(pattern, CONFIG_RESET_BYTE, sizeof(pattern));

  /* first 32 bits */
  oo = o % sizeof(uint32_t);
  if (oo)
  {
    pp = (uint32_t*)pattern + o / sizeof(uint32_t);
    *pp = i;
    memset(pp, CONFIG_RESET_BYTE, oo);

    if ((sizeof(uint32_t) - oo) > n)
    {
      memset(pattern + o + n, CONFIG_RESET_BYTE, sizeof(uint32_t));
      n = 0;
      oo = sizeof(uint32_t);
    }
    n -= sizeof(uint32_t) - oo;
    o += sizeof(uint32_t) - oo;
  }

  /* entire count */
  pp = (uint32_t*)(pattern + o);
  ii = n / sizeof(uint32_t);
  for (j = 0; j < ii; ++j, ++pp) *pp = i;
  o += ii * sizeof(uint32_t);
  n -= ii * sizeof(uint32_t);

  /* remaining */
  if (n)
  {
    *pp = i;
    memset(pattern + o + n, CONFIG_RESET_BYTE, sizeof(uint32_t) - n);
  }

  return pattern;
}

__attribute__((unused)) static void make_pattern_ddr_128(uint8_t* p, size_t i)
{
  /* hardcoded pattern */
#if 0
  *((uint32_t*)p + 0) = i & 0xffffffff;
  *((uint32_t*)p + 1) = i & 0x99999999;
  *((uint32_t*)p + 2) = i & 0xaaaaaaaa;
  *((uint32_t*)p + 3) = i & 0x55555555;
#else
  *((uint32_t*)p + 0) = i;
  *((uint32_t*)p + 1) = i;
  *((uint32_t*)p + 2) = i;
  *((uint32_t*)p + 3) = i;
#endif
}

static int mcmp(const uint8_t* a, const uint8_t* b, size_t n, size_t* e)
{
  /* compare memory, return -1 on mismatch or 0 */
  /* *e is equal to the last compared offset */

  for (*e = 0; *e != n; ++*e)
  {
    if (a[*e] != b[*e]) return -1;
  }

  return 0;
}

static int check_buf
(
 ebuf_handle_t* buf,
 size_t soff, size_t doff, size_t size,
 size_t ebs_dwidth, size_t ebft_dwidth,
 size_t* eoff
)
{
  /* eoff the error offset, if any */

  /* max pattern size */
  uint8_t* pattern;
  const uint8_t* data;
  size_t i;
  size_t n;
  size_t e = 0;
  size_t count = size / ebft_dwidth;

  data = ebuf_get_data(buf);

  /* check first separately */
  n = soff % ebft_dwidth;
  if (n)
  {
    i = ebft_dwidth - n;
    if (i > size) i = size;

    pattern = make_ut_pattern(ebft_dwidth, soff / ebft_dwidth, n, i);

    if (mcmp(pattern + n, data + doff, i, &e))
    {
      /* realign for error offset computation */
      data += doff;
      goto on_error;
    }

    soff += i;
    doff += i;
    size -= i;
  }

  /* use recomputed offset and size */
  i = soff / ebft_dwidth;
  data += doff;
  count = size / ebft_dwidth;
  for (n = 0; n < count; ++n, ++i, data += ebft_dwidth)
  {
    pattern = make_ut_pattern(ebft_dwidth, i, 0, ebft_dwidth);
    if (mcmp(pattern, data, ebft_dwidth, &e)) goto on_error;
  }
  size -= count * ebft_dwidth;

  /* check last separately */
  if (size)
  {
    pattern = make_ut_pattern(ebft_dwidth, i, 0, size);
    if (mcmp(pattern, data, size, &e)) goto on_error;
  }

  return 0;

 on_error:
#if 0
  n = i * ebft_dwidth;
  printf("error at %lu (off == %lu)\n", i, soff / ebft_dwidth);
  printf("read    : ");
  for (i = 0; i < ebft_dwidth; ++i) printf("%02x", data[i]);
  printf("\n");
  printf("expected: ");
  for (i = 0; i < ebft_dwidth; ++i) printf("%02x", pattern[i]);
  printf("\n");
#endif
  *eoff = (size_t)((uintptr_t)data - (uintptr_t)ebuf_get_data(buf)) + e;
  return -1;
}

static int check_io(io_info_t* io, size_t* eoff)
{
  const size_t soff = io->read_soff;
  const size_t doff = io->read_doff;
  const size_t size = io->read_size;

  if (io->err)
  {
    printf("io->err == -1\n");
    *eoff = (size_t)-1;
    return -1;
  }

  return check_buf
    (io->buf, soff, doff, size, io->ebs_dwidth, io->dma->ebft_dwidth, eoff);
}

static int check_underflow(io_info_t* io)
{
  const uint8_t* p = ebuf_get_data(io->buf);
  size_t i;

  for (i = 0; i < io->read_doff; ++i)
  {
    if (p[i] != CONFIG_RESET_BYTE)
    {
      printf("%s, error at %zu (0x%02x)\n", __FUNCTION__, i, p[i]);
      break ;
    }
  }

  return (i != io->read_doff) ? -1 : 0;
}

static int check_overflow(io_info_t* io)
{
  const uint8_t* p = ebuf_get_data(io->buf);
  const size_t size = ebuf_get_size(io->buf);
  size_t i;

  for (i = io->read_doff + io->read_size; i < size; ++i)
  {
    if (p[i] != CONFIG_RESET_BYTE)
    {
      printf("%s, error at %zu (0x%02x)\n", __FUNCTION__, i, p[i]);
      break ;
    }
  }

  return (i != size) ? -1 : 0;
}


/* benchmarking */

static inline double to_payload_mbps(unsigned long size, uint64_t usecs)
{
  /* convert size per usecs in megabyte per second.
     the returned value is for payload data only, not taking PCIE
     meta into account.
   */

  return (1000000 * (double)size) / ((double)usecs * 1024 * 1024);
}


static uint64_t get_cpu_hz(void);
static inline uint64_t rdtsc(void)
{
#ifdef CONFIG_FREESCALE_IMX6
  struct timeval tm;
  gettimeofday(&tm, NULL);
  return tm.tv_sec * 1000000 + tm.tv_usec;
#else
  uint32_t a, d;
  __asm__ __volatile__
  (
   "rdtsc \n\t"
   : "=a"(a), "=d"(d)
  );
  return ((uint64_t)d << 32) | a;
#endif
}

static inline uint64_t sub_ticks(uint64_t a, uint64_t b)
{
  if (a > b) return UINT64_MAX - a + b;
  return b - a;
}

static uint64_t get_cpu_hz(void)
{
  /* return the cpu freq, in mhz */

  static uint64_t cpu_hz = 0;
  unsigned int i;
  unsigned int n;
  struct timeval tms[3];
  uint64_t ticks[2];
  uint64_t all_ticks[10];

  if (cpu_hz != 0) return cpu_hz;

  n = sizeof(all_ticks) / sizeof(all_ticks[0]);
  for (i = 0; i < n; ++i)
  {
    gettimeofday(&tms[0], NULL);
    ticks[0] = rdtsc();
    while (1)
    {
      gettimeofday(&tms[1], NULL);
      timersub(&tms[1], &tms[0], &tms[2]);

      if (tms[2].tv_usec >= 9998)
      {
   ticks[1] = rdtsc();
   all_ticks[i] = sub_ticks(ticks[0], ticks[1]);
   break ;
      }
    }
  }

  cpu_hz = 0;
  for (i = 0; i < n; ++i) cpu_hz += all_ticks[i];
  cpu_hz *= 10;

  return cpu_hz;
}

static inline uint64_t ticks_to_us(uint64_t ticks)
{
  return (ticks * 1000000) / get_cpu_hz();
}

static size_t arg_to_size(const char* s)
{
  int base = 10;
  if ((strlen(s) > 2) && (s[0] == '0') && (s[1] == 'x')) base = 16;
  return (size_t)strtoul(s, NULL, base);
}

static void parse_interval(const char* s, size_t* lo, size_t* hi)
{
  /* -size size */
  /* -size lo:hi */

  char lbuf[32];
  size_t i;

  for (i = 0; (i != (sizeof(lbuf) - 1)) && s[i]; ++i)
  {
    if (s[i] == ':') break ;
    lbuf[i] = s[i];
  }
  lbuf[i] = 0;

  *lo = arg_to_size(lbuf);

  if (s[i] == ':') *hi = arg_to_size(s + i + 1);
  else *hi = *lo;
}

/* debugging probe setup */

__attribute__((unused)) static void setup_probe(const char* dev_id)
{
  /* const uint32_t value = (10 << 15) | (9 << 10) | (8 << 5) | (12 << 0); */
  static const uint32_t value = (11 << 15) | (10 << 10) | (9 << 5) | (8 << 0);
  epcihandle_t bar;
  bar = epci_open(dev_id, NULL, 0);
  epci_wr32_reg(bar, 0x0004, value);
  epci_close(bar);
}

/* command line */

#define CMD_FLAG_SOFF (((uint64_t)1) << 0)
#define CMD_FLAG_DOFF (((uint64_t)1) << 1)
#define CMD_FLAG_SIZE (((uint64_t)1) << 2)
#define CMD_FLAG_LOOP (((uint64_t)1) << 3)
#define CMD_FLAG_PRINT (((uint64_t)1) << 4)
#define CMD_FLAG_UT_WAIT (((uint64_t)1) << 5)
#define CMD_FLAG_UT_PAUSE (((uint64_t)1) << 6)
#define CMD_FLAG_UT_LOOP (((uint64_t)1) << 7)
#define CMD_FLAG_HELP (((uint64_t)1) << 8)
#define CMD_FLAG_DMA_POLL (((uint64_t)1) << 9)
#define CMD_FLAG_SOFF_ALIGN_4K (((uint64_t)1) << 10)
#define CMD_FLAG_SOFF_ALIGN_WORD (((uint64_t)1) << 11)
#define CMD_FLAG_DOFF_ALIGN_WORD (((uint64_t)1) << 12)
#define CMD_FLAG_EBS_BRAM (((uint64_t)1) << 13)
#define CMD_FLAG_EBS_SEG (((uint64_t)1) << 14)
#define CMD_FLAG_EBS_OFF (((uint64_t)1) << 15)
#define CMD_FLAG_WORD_SIZE (((uint64_t)1) << 16)
#define CMD_FLAG_SMEM_SIZE (((uint64_t)1) << 17)
#define CMD_FLAG_PCI_ID (((uint64_t)1) << 18)
#define CMD_FLAG_EBFT_SEG (((uint64_t)1) << 19)
#define CMD_FLAG_EBFT_OFF (((uint64_t)1) << 20)
#define CMD_FLAG_FILL_NONE (((uint64_t)1) << 21)
#define CMD_FLAG_FSCAN (((uint64_t)1) << 22)
#define CMD_FLAG_FIFO1 (((uint64_t)1) << 23)
#define CMD_FLAG_FIFO2 (((uint64_t)1) << 24)
#define CMD_FLAG_FIFO3 (((uint64_t)1) << 25)
#define CMD_FLAG_TIMER (((uint64_t)1) << 26)
#define CMD_FLAG_BLOCK_SIZE (((uint64_t)1) << 27)
#define CMD_FLAG_RESET_EBONE (((uint64_t)1) << 28)
#define CMD_FLAG_WRITE_STRIG (((uint64_t)1) << 29)
#define CMD_FLAG_F1TRIG (((uint64_t)1) << 30)
#define CMD_FLAG_F2TRIG (((uint64_t)1) << 31)
#define CMD_FLAG_F3TRIG (((uint64_t)1) << 32)
#define CMD_FLAG_CHECK (((uint64_t)1) << 33)


typedef struct
{
  uint64_t flags;
  size_t soff_lo;
  size_t soff_hi;
  size_t doff_lo;
  size_t doff_hi;
  size_t size_lo;
  size_t size_hi;
  size_t ut_pause;
  size_t ebs_seg;
  size_t ebs_off;
  size_t loop_count;
  size_t loop_delay;
  size_t word_size;
  size_t smem_size;
  const char* pci_id;
  size_t ebft_seg;
  size_t ebft_off;
  size_t fifo1_delay;
  size_t fifo2_delay;
  size_t fifo3_delay;
  size_t fscan_timer;
  size_t block_size;
  size_t f1trig_delay;
  size_t f2trig_delay;
  size_t f3trig_delay;
} cmd_info_t;

static int get_cmd_info(cmd_info_t* cmd, int ac, char** av)
{
  int i;

  cmd->flags = 0;
  cmd->soff_lo = 0;
  cmd->soff_hi = 0;
  cmd->doff_lo = 0;
  cmd->doff_hi = 0;
  cmd->size_lo = 0;
  cmd->size_hi = 0;
  cmd->ut_pause = 0;
  cmd->ebs_seg = 0;
  cmd->ebs_off = 0;
  cmd->loop_count = 0;
  cmd->loop_delay = 0;
  cmd->word_size = 0;
  cmd->smem_size = 0;
  cmd->ebft_seg = 0;
  cmd->ebft_off = 0;
  cmd->fifo1_delay = 0;
  cmd->fifo2_delay = 0;
  cmd->fifo3_delay = 0;
  cmd->fscan_timer = 0;
  cmd->block_size = 0;
  cmd->f1trig_delay = 0;
  cmd->f2trig_delay = 0;
  cmd->f3trig_delay = 0;

  cmd->flags |= CMD_FLAG_CHECK;

  if (ac == 0) goto on_help;
  if (ac % 2) goto on_help;

  for (i = 0; i != ac; i += 2)
  {
    const char* const k = av[i + 0];
    const char* const v = av[i + 1];

    if (strcmp(k, "-soff") == 0)
    {
      if (strcmp(v, "align_4k") == 0)
      {
   cmd->flags |= CMD_FLAG_SOFF_ALIGN_4K;
      }
      else if (strcmp(v, "align_word") == 0)
      {
   cmd->flags |= CMD_FLAG_SOFF_ALIGN_WORD;
      }
      else
      {
   cmd->flags |= CMD_FLAG_SOFF;
   parse_interval(v, &cmd->soff_lo, &cmd->soff_hi);
      }
    }
    else if (strcmp(k, "-doff") == 0)
    {
      if (strcmp(v, "align_word") == 0)
      {
   cmd->flags |= CMD_FLAG_DOFF_ALIGN_WORD;
      }
      else
      {
   cmd->flags |= CMD_FLAG_DOFF;
   parse_interval(v, &cmd->doff_lo, &cmd->doff_hi);
      }
    }
    else if (strcmp(k, "-size") == 0)
    {
      cmd->flags |= CMD_FLAG_SIZE;
      parse_interval(v, &cmd->size_lo, &cmd->size_hi);
    }
    else if (strcmp(k, "-loop") == 0)
    {
      cmd->flags |= CMD_FLAG_LOOP;
      if (strcmp(v, "no") == 0) cmd->loop_count = 1;
      else if (strcmp(v, "yes") == 0) cmd->loop_count = (size_t)-1;
      else cmd->loop_count = arg_to_size(v);
    }
    else if (strcmp(k, "-print") == 0)
    {
      if (strcmp(v, "yes") == 0) cmd->flags |= CMD_FLAG_PRINT;
    }
    else if ((strcmp(k, "-utw") == 0) || (strcmp(k, "-ut_wait") == 0))
    {
      if (strcmp(v, "yes") == 0) cmd->flags |= CMD_FLAG_UT_WAIT;
    }
    else if (strcmp(k, "-ut_pause") == 0)
    {
      cmd->flags |= CMD_FLAG_UT_PAUSE;
      cmd->ut_pause = arg_to_size(v);
    }
    else if (strcmp(k, "-ut_loop") == 0)
    {
      if (strcmp(v, "yes") == 0) cmd->flags |= CMD_FLAG_UT_LOOP;
    }
    else if (strcmp(k, "-dma_poll") == 0)
    {
      if (strcmp(v, "yes") == 0) cmd->flags |= CMD_FLAG_DMA_POLL;
    }
    else if (strcmp(k, "-ebs_type") == 0)
    {
      if (strcmp(v, "bram") == 0) cmd->flags |= CMD_FLAG_EBS_BRAM;
    }
    else if (strcmp(k, "-ebs_seg") == 0)
    {
      cmd->flags |= CMD_FLAG_EBS_SEG;
      cmd->ebs_seg = arg_to_size(v);
    }
    else if (strcmp(k, "-ebs_off") == 0)
    {
      cmd->flags |= CMD_FLAG_EBS_OFF;
      cmd->ebs_off = arg_to_size(v);
    }
    else if (strcmp(k, "-loop_delay") == 0)
    {
      cmd->loop_delay = arg_to_size(v);
    }
    else if (strcmp(k, "-word_size") == 0)
    {
      cmd->word_size = arg_to_size(v);
    }
    else if (strcmp(k, "-smem_size") == 0)
    {
      cmd->flags |= CMD_FLAG_SMEM_SIZE;
      cmd->smem_size = arg_to_size(v);
    }
    else if (strcmp(k, "-pci_id") == 0)
    {
      cmd->flags |= CMD_FLAG_PCI_ID;
      cmd->pci_id = v;
    }
    else if (strcmp(k, "-ebft_seg") == 0)
    {
      cmd->flags |= CMD_FLAG_EBFT_SEG;
      cmd->ebft_seg = arg_to_size(v);
    }
    else if (strcmp(k, "-ebft_off") == 0)
    {
      cmd->flags |= CMD_FLAG_EBFT_OFF;
      cmd->ebft_off = arg_to_size(v);
    }
    else if (strcmp(k, "-fill") == 0)
    {
      if (strcmp(v, "none") == 0) cmd->flags |= CMD_FLAG_FILL_NONE;
    }
    else if (strcmp(k, "-fscan") == 0)
    {
      if (strcmp(v, "yes") == 0) cmd->flags |= CMD_FLAG_FSCAN;
    }
    else if (strcmp(k, "-fifo1") == 0)
    {
      cmd->flags |= CMD_FLAG_FIFO1;
      cmd->fifo1_delay = arg_to_size(v);
    }
    else if (strcmp(k, "-fifo2") == 0)
    {
      cmd->flags |= CMD_FLAG_FIFO2;
      cmd->fifo2_delay = arg_to_size(v);
    }
    else if (strcmp(k, "-fifo3") == 0)
    {
      cmd->flags |= CMD_FLAG_FIFO3;
      cmd->fifo3_delay = arg_to_size(v);
    }
    else if (strcmp(k, "-timer") == 0)
    {
      cmd->flags |= CMD_FLAG_TIMER;
      cmd->fscan_timer = arg_to_size(v);
    }
    else if (strcmp(k, "-block_size") == 0)
    {
      cmd->flags |= CMD_FLAG_BLOCK_SIZE;
      cmd->block_size = arg_to_size(v);
    }
    else if (strcmp(k, "-reset_ebone") == 0)
    {
      if (strcmp(v, "yes") == 0) cmd->flags |= CMD_FLAG_RESET_EBONE;
    }
    else if (strcmp(k, "-write_strig") == 0)
    {
      if (strcmp(v, "yes") == 0) cmd->flags |= CMD_FLAG_WRITE_STRIG;
    }
    else if (strcmp(k, "-f1trig") == 0)
    {
      cmd->flags |= CMD_FLAG_F1TRIG;
      cmd->f1trig_delay = arg_to_size(v);
    }
    else if (strcmp(k, "-f2trig") == 0)
    {
      cmd->flags |= CMD_FLAG_F2TRIG;
      cmd->f2trig_delay = arg_to_size(v);
    }
    else if (strcmp(k, "-f3trig") == 0)
    {
      cmd->flags |= CMD_FLAG_F3TRIG;
      cmd->f3trig_delay = arg_to_size(v);
    }
    else if (strcmp(k, "-check") == 0)
    {
      cmd->flags &= ~CMD_FLAG_CHECK;
      if (strcmp(v, "yes") == 0) cmd->flags |= CMD_FLAG_CHECK;
    }
    else if (strcmp(k, "-help") == 0)
    {
    on_help:
      cmd->flags |= CMD_FLAG_HELP;
      return 0;
    }
    else
    {
      goto on_error;
    }
   }

  return 0;

 on_error:
  return -1;
}

static size_t rand_interval(size_t lo, size_t hi)
{
  /* generate in [lo,hi] */
  return lo + rand() % (1 + hi - lo);
}

/* toremove */
static void setup_debug_stuffs(const cmd_info_t* info)
{
  const char* dev_id;
  epcihandle_t bar;
  uint32_t x;

  if (info->flags & CMD_FLAG_PCI_ID) dev_id = info->pci_id;
  else dev_id = "10ee:eb01";

  bar = epci_open(dev_id, NULL, 1);

  x = 0;
  if (info->flags & CMD_FLAG_UT_PAUSE) x |= info->ut_pause << 4;

  epci_wr32_reg(bar, 0x020c, x);

  epci_close(bar);
}
/* toremove */

static void do_help(const cmd_info_t* cmd)
{
  printf("command line options:\n");
  printf("-help                : print this help\n");
  printf("-soff <off>          : the source offset, in bytes\n");
  printf("-soff min:max        : random source offset in interval [min:max]\n");
  printf("-soff align_4k       : align source offset on 4KB\n");
  printf("-soff align_word     : align source offset on words\n");
  printf("-doff <off>          : the destination offset, in bytes\n");
  printf("-doff min:max        : random dest offset in interval [min:max]\n");
  printf("-doff align_word     : align destination offset words\n");
  printf("-size size           : the transfer size, in bytes\n");
  printf("-size min:max        : random size in interval [min:max]\n");
  printf("-loop {yes,no}       : transfer infinitely until error\n");
  printf("-loop <count>        : do a given count of transfer\n");
  printf("-print {yes,no}      : dump the memory contents\n");
  printf("-ut_wait {yes,no}    : wait for the utport to end before reading\n");
  printf("-ut_pause <pause>    : set the utport pause (<= 15)\n");
  printf("-ut_loop {yes,no}    : enable the concurrent read write mode\n");
  printf("-dma_poll {yes,no}   : enable dma status polling\n");
  printf("-ebs_type {ddr,bram} : ebone slave type (default to ddr)\n");
  printf("-ebs_seg <segment>   : ebone slave segment (default to 1)\n");
  printf("-ebs_off <offset>    : ebone slave offset (default to 0x200)\n");
  printf("-loop_delay <delay>  : delay between loops, in microseconds\n");
  printf("-word_size <size>    : word size, in bytes (default to 16)\n");
  printf("-smem_size <size>    : source memory size size, in bytes\n");
  printf("-pci_id <xxxx:xxxx>  : PCI device ID\n");
  printf("-ebft_seg <segment>  : EBFT segment (default to 1)\n");
  printf("-ebft_off <offset>   : EBFT offset (in bytes, default to 0x400)\n");
  printf("-fill {none}         : memory filling method\n");
  printf("-fscan {yes,no}      : fscan mode\n");
  printf("-check {yes,no}      : disable memory check (default to yes)\n");
  printf("\n");

  printf("FSCAN supported options:\n");
  printf("-fscan {yes,no}      : fscan mode\n");
  printf("-pci_id <xxxx:xxxx>  : PCI device ID\n");
  printf("-size <size>         : the transfer size, in bytes, 4MB limit\n");
  printf("-fifo1 <delay>       : use fifo#1, FSM write delay in microsecond\n");
  printf("-fifo2 <delay>       : use fifo#2, FSM write delay in microsecond\n");
  printf("-fifo3 <delay>       : use fifo#3, FSM write delay in microsecond\n");
  printf("-timer <delay>       : timer delay, in millisecond\n");
  printf("-block_size <size>   : block size, in bytes\n");
  printf("-loop {yes,no}       : transfer infinitely until error\n");
  printf("-loop <count>        : do a given count of transfer\n");
  printf("-print {yes,no}      : dump the memory contents\n");
  printf("-reset_ebone {yes,no}: reset EBONE before starting\n");
  printf("-write_strig {yes,no}: write STRIG register on error\n");
  printf("-f1trig <delay>      : use f1trig, delay in millisecond\n");
  printf("-f2trig <delay>      : use f2trig, delay in millisecond\n");
  printf("-f3trig <delay>      : use f3trig, delay in millisecond\n");
  printf("\n");

  printf("notes:\n");
  printf(". hexadecimal (starting with 0x) can be used for numeric arguments\n");
  printf(". {yes,no} options default to no if not specified\n");
  printf(". -utw is still supported, BUT you should use -ut_wait\n");
}

static volatile unsigned int is_sigint = 0;

static void on_sigint(int x)
{
  is_sigint = 1;
}

/* main */

static int fscan_main(cmd_info_t*);

int main(int ac, char** av)
{
  dma_mem_t* mem;
  const char* dev_id;

#define ERR_TYPE_SUCCESS 0
#define ERR_TYPE_OTHER -1
#define ERR_TYPE_EBFT -2
#define ERR_TYPE_CHECK -3
#define ERR_TYPE_UNDERFLOW -4
#define ERR_TYPE_OVERFLOW -5
#define ERR_TYPE_CMDLINE -6
  int err = ERR_TYPE_OTHER;

  edma_handle_t dma;
  ebuf_handle_t buf;
  io_info_t io;
  epcihandle_t bar[3];
  edma_desc_t desc;
  unsigned int i;

  size_t max_off;

  size_t err_off = 0;

  size_t lo;
  size_t hi;

  size_t log2_word_size;

  uint64_t diff_ticks;
  uint64_t this_usecs;
  uint64_t total_usecs;

  cmd_info_t cmd;

  uirq_handle_t uirq;

  /* retrieve command line */
  if (get_cmd_info(&cmd, ac - 1, av + 1))
  {
    err = ERR_TYPE_CMDLINE;
    goto on_error_0;
  }

  if (cmd.flags & CMD_FLAG_HELP)
  {
    do_help(&cmd);
    err = ERR_TYPE_SUCCESS;
    goto on_error_0;
  }

  if (cmd.flags & CMD_FLAG_FSCAN)
  {
    err = fscan_main(&cmd);
    goto on_error_0;
  }

  if (cmd.flags & CMD_FLAG_PCI_ID) dev_id = cmd.pci_id;
  else dev_id = "10ee:eb01";

  /* setup_probe(dev_id); */

  /* open bars */
  bar[0] = epci_open(dev_id, NULL, 0);
  if (bar[0] == EPCI_BAD_HANDLE) goto on_error_0;
  bar[1] = epci_open(dev_id, NULL, 1);
  if (bar[1] == EPCI_BAD_HANDLE) goto on_error_1;
  bar[2] = epci_open(dev_id, NULL, cmd.ebs_seg);
  if (bar[2] == EPCI_BAD_HANDLE) goto on_error_2;

  /* initialize subsystems */

  if ((cmd.flags & CMD_FLAG_DMA_POLL) == 0)
  {
    if (uirq_init_lib()) goto on_error_3;
    if (uirq_init(&uirq, "10ee:eb01")) goto on_error_4;
    if (uirq_open(&uirq)) goto on_error_4;
  }

  if (ebuf_init_lib(EBUF_FLAG_PHYSICAL)) goto on_error_5;
  if (edma_init_lib()) goto on_error_6;

  if (cmd.flags & CMD_FLAG_EBS_BRAM) mem = &bram_mem;
  else mem = &ddr_mem;

  /* affect default segment offset values */

  if ((cmd.flags & CMD_FLAG_EBS_SEG) == 0)
  {
    cmd.ebs_seg = 1;
    cmd.flags |= CMD_FLAG_EBS_SEG;
  }

  if ((cmd.flags & CMD_FLAG_EBS_OFF) == 0)
  {
    cmd.ebs_off = 0x200;
    cmd.flags |= CMD_FLAG_EBS_OFF;
  }

  if (cmd.flags & CMD_FLAG_SMEM_SIZE)
  {
    mem->size = cmd.smem_size;
  }
  else
  {
    if ((cmd.flags & CMD_FLAG_EBS_BRAM) == 0)
    {
      mem->size = get_ddr_size(bar[1]);
    }
  }
  desc.ssize = mem->size;

  /* default lo,hi sizes */
  if ((cmd.flags & CMD_FLAG_SIZE) == 0)
  {
    cmd.size_lo = 1;
    cmd.size_hi = mem->size;
    cmd.flags |= CMD_FLAG_SIZE;
  }
  if (cmd.size_lo > mem->size) cmd.size_lo = mem->size;
  if (cmd.size_hi > mem->size) cmd.size_hi = mem->size;

  if ((cmd.flags & CMD_FLAG_SOFF) == 0)
  {
    cmd.soff_lo = 0;
    cmd.soff_hi = mem->size - 1;
    cmd.flags |= CMD_FLAG_SOFF;
  }

  if ((cmd.flags & CMD_FLAG_DOFF) == 0)
  {
    cmd.doff_lo = 0;
    cmd.doff_hi = mem->size - 1;
    cmd.flags |= CMD_FLAG_DOFF;
  }

  /* word size. default to 128 bits (16 bytes) */
  if ((cmd.flags & CMD_FLAG_WORD_SIZE) == 0)
  {
    cmd.flags |= CMD_FLAG_WORD_SIZE;
    cmd.word_size = 16;
  }
  /* log2_word_size = log2(cmd.word_size); */
  if (cmd.word_size == 16) log2_word_size = 4;
  else if (cmd.word_size == 8) log2_word_size = 3;
  else log2_word_size = 2;

  /* init DMAs */
  edma_init_desc(&desc);
  desc.pci_id = dev_id;

  if ((cmd.flags & CMD_FLAG_EBFT_SEG) == 0) desc.pci_bar = 1;
  else desc.pci_bar = cmd.ebft_seg;
  if ((cmd.flags & CMD_FLAG_EBFT_OFF) == 0) desc.pci_off = 0x400;
  else desc.pci_off = cmd.ebft_off;

  if (cmd.flags & CMD_FLAG_EBS_BRAM) desc.flags = EDMA_FLAG_BRAM;
  else desc.flags = EDMA_FLAG_MIF;

  /* info should come from ram handle */
  desc.ebs_seg = cmd.ebs_seg;
  desc.ebs_off = cmd.ebs_off;

  if ((cmd.flags & CMD_FLAG_DMA_POLL) == 0)
  {
    desc.flags |= EDMA_FLAG_IRQ;
    desc.uirq = &uirq;
  }
  else
  {
    desc.flags &= ~EDMA_FLAG_IRQ;
  }

  /* toremove */
  setup_debug_stuffs(&cmd);

  if (edma_open(&dma, &desc)) goto on_error_7;

  /* alloc buffer */
  if (ebuf_alloc(&buf, mem->size, EBUF_FLAG_PHYSICAL)) goto on_error_8;

  srand(getpid() * time(NULL));

  /* initialize the source memory */
  if ((cmd.flags & CMD_FLAG_FILL_NONE) == 0)
  {
    const unsigned int is_utw = cmd.flags & CMD_FLAG_UT_WAIT;
    const unsigned int is_bram = cmd.flags & CMD_FLAG_EBS_BRAM;
    const size_t moff = (size_t)cmd.ebs_off * sizeof(uint32_t);

    if (fill_mem(bar, &buf, moff, mem->size, mem->ebs_dwidth, is_utw, is_bram))
      goto on_error_9;

    /* start concurrent memory write */
    if (cmd.flags & CMD_FLAG_UT_LOOP) ut_start(bar[1], mem->size, 1);
  }

  /* default loop count */
  if ((cmd.flags & CMD_FLAG_LOOP) == 0) cmd.loop_count = 1;

  signal(SIGINT, on_sigint);

  for (i = 0; is_sigint == 0; ++i)
  {
    /* last loop reached */
    if ((cmd.loop_count != (size_t)-1) && (i == cmd.loop_count))
    {
      break ;
    }

    if (cmd.loop_delay)
    {
      usleep(cmd.loop_delay);
    }

    /* fill io info. randomize offset, size */
    io.dma = &dma;
    io.buf = &buf;

    /* source offset */
    io.read_soff = rand_interval(cmd.soff_lo, cmd.soff_hi);
    if (cmd.flags & CMD_FLAG_SOFF_ALIGN_4K)
    {
      /* 4KB = 1 << 12 */
      io.read_soff &= ~((1 << 12) - 1);
    }
    else if (cmd.flags & CMD_FLAG_SOFF_ALIGN_WORD)
    {
      io.read_soff &= ~((1 << log2_word_size) - 1);
    }
    desc.soff = io.read_soff;

    /* destination offset */
    io.read_doff = rand_interval(cmd.doff_lo, cmd.doff_hi);
    if (cmd.flags & CMD_FLAG_DOFF_ALIGN_WORD)
    {
      io.read_doff &= ~((1 << log2_word_size) - 1);
    }

    max_off = (io.read_soff > io.read_doff) ? io.read_soff : io.read_doff;

    if ((max_off + cmd.size_hi) > mem->size) hi = mem->size - max_off;
    else hi = cmd.size_hi;

    if ((max_off + cmd.size_lo) > mem->size) lo = mem->size - max_off;
    else lo = cmd.size_lo;

    io.read_size = rand_interval(lo, hi);

    io.ebs_dwidth = mem->ebs_dwidth;

    /* reset buffer contents */
    reset_buffer(&buf);

    /* actual transfer */
    io.start_ticks = rdtsc();
#if 1
    io.err = edma_read(&dma, io.read_soff, &buf, io.read_doff, io.read_size);
#else
    eaio_desc_t aio_desc;
    edma_init_aio_desc(&dma, &aio_desc);
    io.err = eaio_read_oneshot(&aio_desc, &buf, io.read_size);
#endif
    io.stop_ticks = rdtsc();

    /* stats */
    diff_ticks = sub_ticks(io.start_ticks, io.stop_ticks);
    this_usecs = ticks_to_us(diff_ticks);
    total_usecs += this_usecs;

    lo = io.read_soff;
    hi = io.read_soff + io.read_size;

    printf("-- %u\n", i);
    printf("time         : %llu\n", (unsigned long long)this_usecs);
    printf("range        : [ %zu - %zu [ of %zu\n", lo, hi, mem->size);
    printf("payload_mBps : %lf\n", to_payload_mbps(io.read_size, this_usecs));
    printf("soff         : 0x%zx\n", io.read_soff);
    printf("doff         : 0x%zx (0x%zx)\n", io.read_doff, buf.paddr[0]);
    printf("read_size    : 0x%zx\n", io.read_size);
    printf("\n");

    if (cmd.flags & CMD_FLAG_PRINT)
    {
      print_buffer(&buf, 0, io.read_doff + io.read_size + 16, 16);
      printf("\n");
    }

    if (cmd.flags & CMD_FLAG_CHECK)
    {
      if (check_io(&io, &err_off))
      {
         if (err_off == (size_t)-1)
         {
           /* TODO: edma_print_status_regs(&dma); */
           err = ERR_TYPE_EBFT;
         }
         else
         {
           err = ERR_TYPE_CHECK;
         }
         goto on_error_9;
         break ;
      }

      if (check_underflow(&io))
      {
         err = ERR_TYPE_UNDERFLOW;
         goto on_error_9;
         break ;
      }

      if (check_overflow(&io))
      {
         err = ERR_TYPE_OVERFLOW;
         goto on_error_9;
         break ;
      }
    }
  }

  /* success */
  err = 0;

 on_error_9:
  /* stop concurrent memory write */
  if (cmd.flags & CMD_FLAG_UT_LOOP) ut_stop_if_running(bar[1]);
  ebuf_free(&buf);
 on_error_8:
  edma_close(&dma);
 on_error_7:
  edma_fini_lib();
 on_error_6:
  ebuf_fini_lib();
 on_error_5:
  if ((cmd.flags & CMD_FLAG_DMA_POLL) == 0) uirq_close(&uirq);
 on_error_4:
  if ((cmd.flags & CMD_FLAG_DMA_POLL) == 0) uirq_fini_lib();
 on_error_3:
  epci_close(bar[2]);
 on_error_2:
  epci_close(bar[1]);
 on_error_1:
  epci_close(bar[0]);
 on_error_0:

  switch (err)
  {
  case ERR_TYPE_EBFT:
    printf("ERROR: EBFT STATUS\n");
    break ;
  case ERR_TYPE_CHECK:
    printf("ERROR: MEMORY CHECK, doff == 0x%zx\n", err_off);
    break ;
  case ERR_TYPE_UNDERFLOW:
    printf("ERROR: MEMORY UNDERFLOW\n");
    break ;
  case ERR_TYPE_OVERFLOW:
    printf("ERROR: MEMORY OVERFLOW\n");
    break ;
  case ERR_TYPE_CMDLINE:
    printf("ERROR: INVALID COMMAND LINE\n");
    break ;
  case ERR_TYPE_OTHER:
    printf("ERROR: xxx ERROR OCCURED\n");
    break ;
  default:
  case ERR_TYPE_SUCCESS:
    printf("SUCCESS\n");
    break ;
  }

  return err;
}


/* fscan main */

/* trigger fsm related */

static void write_strig(epcihandle_t pci)
{
  epci_wr32_reg(pci, 0xc0, 0xdeadbeef);
}

static void write_f1trig(epcihandle_t pci, uint32_t x)
{
  epci_wr32_reg(pci, 0xc4, x);
}

static void write_f2trig(epcihandle_t pci, uint32_t x)
{
  epci_wr32_reg(pci, 0xc8, x);
}

static void write_f3trig(epcihandle_t pci, uint32_t x)
{
  epci_wr32_reg(pci, 0xcc, x);
}

static void (*write_ftrig[])(epcihandle_t, uint32_t) =
{
  write_f1trig, write_f2trig, write_f3trig
};

static void enable_ftrig(epcihandle_t pci, size_t i, unsigned int delay)
{
  write_ftrig[i](pci, (1 << 7) | (uint32_t)delay);
}

static void disable_ftrig(epcihandle_t pci, size_t i)
{
  write_ftrig[i](pci, 0);
}


/* fifo fsm related */

static void write_gctrl(epcihandle_t pci, uint32_t x)
{
  epci_wr32_reg(pci, 0x80, x);
}

static void write_f1fsmc(epcihandle_t pci, uint32_t x)
{
  epci_wr32_reg(pci, 0x84, x);
}

__attribute__((unused))
static uint32_t read_f1stat(epcihandle_t pci)
{
  uint32_t x;
  epci_rd32_reg(pci, 0x90, &x);
  return x;
}

static void write_f2fsmc(epcihandle_t pci, uint32_t x)
{
  epci_wr32_reg(pci, 0x88, x);
}

__attribute__((unused))
static uint32_t read_f2stat(epcihandle_t pci)
{
  uint32_t x;
  epci_rd32_reg(pci, 0x94, &x);
  return x;
}

static void write_f3fsmc(epcihandle_t pci, uint32_t x)
{
  epci_wr32_reg(pci, 0x8c, x);
}

__attribute__((unused))
static uint32_t read_f3stat(epcihandle_t pci)
{
  uint32_t x;
  epci_rd32_reg(pci, 0x98, &x);
  return x;
}

static void disable_flush_fifo_fsms(epcihandle_t pci)
{
  write_gctrl(pci, 0xf);
}

static void setup_fifo_fsms
(
 epcihandle_t pci,
 unsigned int fifo1_us, unsigned int fifo2_us, unsigned int fifo3_us
)
{
#define DELAY_UNIT_1_US 0x0
#define DELAY_UNIT_10_US 0x1
#define DELAY_UNIT_100_US 0x2
#define DELAY_UNIT_1_MS 0x3

  uint32_t delay_unit;
  uint32_t delay_val;
  uint32_t gctrl_val;

  /* fsm setup prolog */

  write_gctrl(pci, 0xf);

  /* setup fsms */

  gctrl_val = 0xf;

  if (fifo1_us)
  {
    delay_unit = DELAY_UNIT_1_US;
    delay_val = fifo1_us;

    if (fifo1_us > 1000)
    {
      delay_unit = DELAY_UNIT_1_MS;
      delay_val = fifo1_us / 1000;
    }

    write_f1fsmc(pci, (delay_unit << 12) | (delay_val << 0));
    gctrl_val &= ~(1 << 1);
  }

  if (fifo2_us)
  {
    delay_unit = DELAY_UNIT_1_US;
    delay_val = fifo2_us;

    if (fifo2_us > 1000)
    {
      delay_unit = DELAY_UNIT_1_MS;
      delay_val = fifo2_us / 1000;
    }

    write_f2fsmc(pci, (delay_unit << 12) | (delay_val << 0));
    gctrl_val &= ~(1 << 2);
  }

  if (fifo3_us)
  {
    delay_unit = DELAY_UNIT_1_US;
    delay_val = fifo3_us;

    if (fifo3_us > 1000)
    {
      delay_unit = DELAY_UNIT_1_MS;
      delay_val = fifo3_us / 1000;
    }

    write_f3fsmc(pci, (delay_unit << 12) | (delay_val << 0));
    gctrl_val &= ~(1 << 3);
  }

  /* fsm setup epilog */

  write_gctrl(pci, gctrl_val);
}


/* libdance/runtime.c, select loop */

#include "libuirq.h"

static void ms_to_timeval(int ms, struct timeval* tv)
{
  const unsigned int us = (unsigned int)ms * 1000;
  tv->tv_sec = us / 1000000;
  tv->tv_usec = us % 1000000;
}

static int wait_irq(uirq_handle_t* uirq, unsigned int ms)
{
  const int fd = uirq_get_fd(uirq);

  fd_set fds;
  struct timeval to;
  struct timeval* to_pointer;
  int err;

  /* FIXME: LINUX updates to_pointer contents, but not portable */
  to_pointer = NULL;
  if (ms != (unsigned int)-1)
  {
    ms_to_timeval(ms, &to);
    to_pointer = &to;
  }

  FD_ZERO(&fds);
  FD_SET(fd, &fds);

 redo:
  errno = 0;
  err = select(fd + 1, &fds, NULL, NULL, to_pointer);
  if (err < 0)
  {
    if ((errno != EINTR) && (errno != EAGAIN))
    {
      PERROR();
      return -1;
    }

    goto redo;
  }
  else if (err == 0)
  {
    /* timeout */
    PERROR();
    return -2;
  }

  return 0;
}

extern int edma_xxx_start
(edma_handle_t*, uint32_t, size_t, uintptr_t, size_t, unsigned int);
extern int edma_xxx_restart
(edma_handle_t*, uint32_t, uintptr_t, size_t, unsigned int);
extern uint32_t edma_xxx_read_fsx_csr(edma_handle_t*, uint32_t);
extern uint32_t edma_xxx_read_fs_stat(edma_handle_t*);
extern uint32_t edma_xxx_read_fs_irqs(edma_handle_t*);
extern void edma_xxx_write_fs_irqs(edma_handle_t*, uint32_t);
extern void edma_xxx_enable_timer(edma_handle_t*, unsigned int);
extern void edma_xxx_disable_timer(edma_handle_t*);
extern size_t edma_xxx_get_occupancy(edma_handle_t*, uint32_t);
extern unsigned int edma_xxx_is_any_irq(uint32_t, uint32_t);
extern unsigned int edma_xxx_is_timer_irq(uint32_t);
extern unsigned int edma_xxx_ack_timer_irq(edma_handle_t*);

static void print_data(const uint8_t* data, size_t off, size_t size)
{
  size_t i;

  data += off;

  for (i = 0; i != size; ++i, ++data)
  {
    if ((i % 8) == 0)
    {
      if (i != 0) printf("\n");
      printf("[ 0x%08x ] ", (uint32_t)(off + i));
    }
    printf("%02x", *data);
  }

  printf("\n");
}

static int check_fifo_data
(
 ebuf_handle_t* buf, size_t off, size_t size,
 size_t fifo_width, uint16_t* pattern
)
{
  const uint8_t* const data = (const uint8_t*)ebuf_get_data(buf) + off;
  const size_t n = size / fifo_width;
  const uint8_t* p = data;
  uint16_t x;
  size_t i;

  for (i = 0; i != n; ++i, p += fifo_width, ++*pattern)
  {
    x = ((uint16_t)p[0] << 0) | ((uint16_t)p[1] << 8);

    if (x != *pattern)
    {
      printf("[!] ERROR\n");
      printf("data check error at offset 0x%08x\n", (uint32_t)(i * fifo_width));
      printf("pattern = 0x%08x, data = 0x%08x\n", *pattern, x);
      if (i) print_data(data, (i - 8) * fifo_width, (8 + fifo_width) * 64);
      else print_data(data, i * fifo_width, fifo_width * 64);
      return -1;
    }
  }

  /* ok */
  return 0;
}

static void daq_isr(void* p)
{
}


/* main */

__attribute__((unused))
static int disable_broadcall(void)
{
  static const char* const pci_id = "10ee:eb01";
  epcihandle_t bar;
  uint32_t x;

  bar = epci_open(pci_id, NULL, 0);
  epci_rd32_reg(bar, 0x0, &x);
  x &= ~(1 << 9);
  epci_wr32_reg(bar, 0x0, x);

  epci_close(bar);

  return 0;
}

static unsigned int is_contiguous(ebuf_handle_t* buf)
{
  size_t npages = pmem_size_to_pages(ebuf_get_size(buf));
  size_t i;

  for (i = 1; i != npages; ++i)
  {
    if ((buf->paddr[i - 1] + pmem_page_size) != buf->paddr[i]) return 0;
  }

  return 1;
}

int fscan_main(cmd_info_t* cmd)
{
#define FIFO_COUNT 3
#define FIFO_DEPTH 2048
#define FIFO_WIDTH sizeof(uint64_t)

  static const unsigned int bar = 1;
  const char* dev_id;

  edma_handle_t dma;
  ebuf_handle_t buf[FIFO_COUNT];
  size_t boff[FIFO_COUNT];
  size_t block_size;
  edma_desc_t dma_desc;
  epcihandle_t pci;
  uirq_handle_t uirq;
  size_t mem_size;
  int err = -1;
  size_t npass = 0;
  unsigned int is_trig[FIFO_COUNT] = { 0, };
  uint16_t pattern[FIFO_COUNT] = { 0, };
  uint32_t fs_irqs;
  uint32_t did;
  size_t size;
  uintptr_t paddr;

  if (cmd->flags & CMD_FLAG_PCI_ID) dev_id = cmd->pci_id;
  else dev_id = "10ee:eb01";
  if ((pci = epci_open(dev_id, NULL, bar)) == EPCI_BAD_HANDLE)
  {
    PERROR();
    goto on_error_0;
  }

  /* initialize uirq */

  system("insmod /tmp/uirq.ko");

  if (cmd->flags & CMD_FLAG_RESET_EBONE) reset_ebone();

  if (uirq_init_lib())
  {
    PERROR();
    goto on_error_1;
  }

  if (uirq_open(&uirq))
  {
    PERROR();
    goto on_error_2;
  }

  /* FIXME: CH request for testing */
  /* disable_broadcall(); */

  /* allocate buffer */

  if (ebuf_init_lib(EBUF_FLAG_PHYSICAL))
  {
    PERROR();
    goto on_error_3;
  }

  if (cmd->flags & CMD_FLAG_SIZE)
  {
    mem_size = cmd->size_lo;
  }
  else
  {
    /* multiple of (FIFO_DEPTH * FIFO_WIDTH) */
    mem_size = FIFO_DEPTH * FIFO_WIDTH * 256;
  }

  if ((cmd->flags & CMD_FLAG_BLOCK_SIZE) == 0) block_size = mem_size;
  else block_size = cmd->block_size;
  if (block_size > mem_size) block_size = mem_size;

  /* do it prior to starting dma */

  disable_flush_fifo_fsms(pci);

  /* init dmac */

  if (uirq_register_isr(&uirq, 1 << 0, daq_isr, NULL))
  {
    PERROR();
    goto on_error_4;
  }

  if (edma_init_lib())
  {
    PERROR();
    goto on_error_4;
  }

  edma_init_desc(&dma_desc);

  dma_desc.pci_id = "10ee:eb01";
  dma_desc.pci_bar = 1;
  dma_desc.pci_off = 0x400;

  /* info should come from ram handle */
  dma_desc.flags |= EDMA_FLAG_MIF;
  dma_desc.ebs_seg = 0x0;
  dma_desc.ebs_off = 0x0;

  dma_desc.flags |= EDMA_FLAG_IRQ;
  dma_desc.uirq = &uirq;

  dma_desc.soff = 0;
  dma_desc.ssize = mem_size;

  if (edma_open(&dma, &dma_desc))
  {
    PERROR();
    goto on_error_5;
  }

  if ((cmd->flags & (CMD_FLAG_FIFO1 | CMD_FLAG_FIFO2 | CMD_FLAG_FIFO3)) == 0)
  {
    cmd->flags |= CMD_FLAG_FIFO1;
    cmd->fifo1_delay = 10;
  }

  if (cmd->flags & CMD_FLAG_F1TRIG)
  {
    is_trig[0] = 1;
    enable_ftrig(pci, 0, cmd->f1trig_delay);
  }
  else
  {
    is_trig[0] = 0;
    disable_ftrig(pci, 0);
  }

  if (cmd->flags & CMD_FLAG_F2TRIG)
  {
    is_trig[1] = 1;
    enable_ftrig(pci, 1, cmd->f2trig_delay);
  }
  else
  {
    is_trig[1] = 0;
    disable_ftrig(pci, 1);
  }

  if (cmd->flags & CMD_FLAG_F3TRIG)
  {
    is_trig[2] = 1;
    enable_ftrig(pci, 2, cmd->f3trig_delay);
  }
  else
  {
    is_trig[2] = 0;
    disable_ftrig(pci, 2);
  }

  if (cmd->flags & CMD_FLAG_FIFO1)
  {
#define FIFO1_EBS_OFF 0x00
#define FIFO1_FSCAN_DID 0

    if (ebuf_alloc(&buf[0], mem_size, EBUF_FLAG_PHYSICAL))
    {
      PERROR();
      goto on_error_6;
    }

    if (is_contiguous(&buf[0]) == 0)
    {
      PERROR();
      goto on_error_7;
    }

    boff[0] = 0;

    memset(ebuf_get_data(&buf[0]), 0x2a, ebuf_get_size(&buf[0]));

    paddr = ebuf_get_paddr(&buf[0]);
    if (edma_xxx_start(&dma, FIFO1_FSCAN_DID, FIFO1_EBS_OFF, paddr, block_size, is_trig[0]))
    {
      PERROR();
      goto on_error_7;
    }
  }

  if (cmd->flags & CMD_FLAG_FIFO2)
  {
#define FIFO2_EBS_OFF 0x04
#define FIFO2_FSCAN_DID 1

    if (ebuf_alloc(&buf[1], mem_size, EBUF_FLAG_PHYSICAL))
    {
      PERROR();
      goto on_error_7;
    }

    if (is_contiguous(&buf[1]) == 0)
    {
      PERROR();
      goto on_error_7;
    }

    boff[1] = 0;

    memset(ebuf_get_data(&buf[1]), 0x2a, ebuf_get_size(&buf[1]));

    paddr = ebuf_get_paddr(&buf[1]);
    if (edma_xxx_start(&dma, FIFO2_FSCAN_DID, FIFO2_EBS_OFF, paddr, block_size, is_trig[1]))
    {
      PERROR();
      goto on_error_8;
    }
  }

  if (cmd->flags & CMD_FLAG_FIFO3)
  {
#define FIFO3_EBS_OFF 0x08
#define FIFO3_FSCAN_DID 2

    if (ebuf_alloc(&buf[2], mem_size, EBUF_FLAG_PHYSICAL))
    {
      PERROR();
      goto on_error_8;
    }

    if (is_contiguous(&buf[2]) == 0)
    {
      PERROR();
      goto on_error_9;
    }

    boff[2] = 0;

    memset(ebuf_get_data(&buf[2]), 0x2a, ebuf_get_size(&buf[2]));

    paddr = ebuf_get_paddr(&buf[2]);
    if (edma_xxx_start(&dma, FIFO3_FSCAN_DID, FIFO3_EBS_OFF, paddr, block_size, is_trig[2]))
    {
      PERROR();
      goto on_error_9;
    }
  }

  if (cmd->flags & CMD_FLAG_TIMER)
  {
    edma_xxx_enable_timer(&dma, (unsigned int)cmd->fscan_timer);
  }
  else
  {
    edma_xxx_disable_timer(&dma);
  }

  if ((cmd->flags & CMD_FLAG_FIFO1) == 0) cmd->fifo1_delay = 0;
  if ((cmd->flags & CMD_FLAG_FIFO2) == 0) cmd->fifo2_delay = 0;
  if ((cmd->flags & CMD_FLAG_FIFO3) == 0) cmd->fifo3_delay = 0;
  setup_fifo_fsms(pci, cmd->fifo1_delay, cmd->fifo2_delay, cmd->fifo3_delay);

 redo:
  /* wait for transfer to end */

  if (wait_irq(&uirq, (unsigned int)-1))
  {
    PERROR();

  on_transfer_error:
    if (cmd->flags & CMD_FLAG_WRITE_STRIG) write_strig(pci);

    printf("\n");
    printf("mem_size     : 0x%08x\n", (uint32_t)mem_size);
    printf("FSx_CSR      : 0x%08x\n", edma_xxx_read_fsx_csr(&dma, did));
    printf("FS_STAT      : 0x%08x\n", edma_xxx_read_fs_stat(&dma));
    printf("FS_IRQS      : 0x%08x\n", edma_xxx_read_fs_irqs(&dma));
    printf("F1STAT       : 0x%08x\n", read_f1stat(pci));
    printf("F2STAT       : 0x%08x\n", read_f2stat(pci));
    printf("F3STAT       : 0x%08x\n", read_f3stat(pci));
    goto on_error_9;
  }

  uirq_handle_irq(&uirq);

  fs_irqs = edma_xxx_read_fs_irqs(&dma);

  for (did = 0; did != FIFO_COUNT; ++did)
  {
    if ((did == 0) && ((cmd->flags & CMD_FLAG_FIFO1) == 0)) continue ;
    if ((did == 1) && ((cmd->flags & CMD_FLAG_FIFO2) == 0)) continue ;
    if ((did == 2) && ((cmd->flags & CMD_FLAG_FIFO3) == 0)) continue ;

    if (edma_xxx_is_any_irq(fs_irqs, did) == 0) continue ;

    size = edma_xxx_get_occupancy(&dma, did);
    if (size > block_size)
    {
      PERROR();
      goto on_transfer_error;
    }

    if (cmd->flags & CMD_FLAG_PRINT)
    {
      printf("\nfifo#%u, occupancy = %zu bytes", did + 1, size);
      print_buffer(&buf[did], boff[did], size, 8);
    }

    if (check_fifo_data(&buf[did], boff[did], size, FIFO_WIDTH, &pattern[did]))
    {
      printf("\ntransfer error for fifo#[%u]\n", did + 1);
      goto on_transfer_error;
    }

    ++npass;

    printf("\rtest passed (count = 0x%08x, size = %zu)", (uint32_t)npass, size);
    fflush(stdout);

    if (cmd->flags & CMD_FLAG_LOOP)
    {
      if ((cmd->loop_count != (size_t)-1) && (cmd->loop_count == npass))
      {
   printf("\n");
   goto on_success;
      }
    }

    memset(ebuf_get_data(&buf[did]) + boff[did], 0x2a, size);

    /* use block_size, even if only size transfered. no particular reason. */
    boff[did] += block_size;
    if ((boff[did] + block_size) > ebuf_get_size(&buf[did]))
    {
      boff[did] = 0;
    }

    paddr = ebuf_get_paddr(&buf[did]) + boff[did];
    edma_xxx_restart(&dma, did, paddr, block_size, is_trig[did]);
  }

  if (edma_xxx_is_timer_irq(fs_irqs)) edma_xxx_ack_timer_irq(&dma);

  goto redo;

 on_success:
  err = 0;

 on_error_9:
  if (cmd->flags & CMD_FLAG_FIFO3) ebuf_free(&buf[2]);
 on_error_8:
  if (cmd->flags & CMD_FLAG_FIFO2) ebuf_free(&buf[1]);
 on_error_7:
  if (cmd->flags & CMD_FLAG_FIFO1) ebuf_free(&buf[0]);
 on_error_6:
  edma_close(&dma);
 on_error_5:
  edma_fini_lib();
 on_error_4:
  ebuf_fini_lib();
 on_error_3:
  uirq_close(&uirq);
 on_error_2:
  uirq_fini_lib();
 on_error_1:
  epci_close(pci);
 on_error_0:
  return err;
}
