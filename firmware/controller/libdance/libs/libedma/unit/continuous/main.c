/* run with: */
/* ./main -soff 0 -smem_size 0x2000000 -doff 0 -size 0x2000000 */
/* -word_size 8 -ebs_type ddr -ebs_seg 1 -ebs_off 0x200 -ut_wait yes */


#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include "libedma.h"
#include "libebuf.h"
#include "libepci.h"


/* dma api */

typedef struct dma_mem
{
  /* int bytes */
  size_t size;

  /* in bytes */
  unsigned int ebs_dwidth;

} dma_mem_t;

static dma_mem_t ddr_mem = { 128 * 1024 * 1024, sizeof(uint32_t) };

/* WARNING */
/* bram ebs_dwidth must be sizeof(uint32_t) or pattern generation wont work */
static dma_mem_t bram_mem = { 64 * 1024, sizeof(uint32_t) };


/* helper routines */

static void set_bram_mode(epcihandle_t bar0, uint32_t mode)
{
  uint32_t x;
  epci_rd32_reg(bar0, 0x0, &x);
  x &= 0xffffcfff;
  x |= mode << 12;
  epci_wr32_reg(bar0, 0x0, x);
}

/* memory read write related */

__attribute__((unused)) static inline int soft_write
(ebuf_handle_t* buf, epcihandle_t mem_bar, size_t mem_size, size_t ebs_dwidth)
{
  void* const data = ebuf_get_data(buf);
  epci_wr32_blk(mem_bar, 0, 1, mem_size / (ebs_dwidth * sizeof(uint32_t)), data);
  return 0;
}


/* retrieve ddr size */

static size_t get_ddr_size(epcihandle_t bar)
{
  /* mif_status register offset is 0x240 */
  /* return the size, in bytes */

  const size_t scale = 16 * 1024 * 1024;
  uint32_t x;
  epci_rd32_reg(bar, 0x240, &x);
  return ((size_t)(x & ((1 << 8) - 1))) * scale;
}

/* fill the memory port using utility port */

static unsigned int ut_is_running(epcihandle_t bar)
{
  /* mif_status register offset is 0x240 */
  /* ut_running bit offset is 20 */

  uint32_t x;
  epci_rd32_reg(bar, 0x240, &x);
  return x & (1 << 20);
}

static void ut_wait(epcihandle_t bar)
{
  while (ut_is_running(bar)) usleep(1000);
}

static void ut_stop_if_running(epcihandle_t bar)
{
  static const size_t ebs_reg1 = 0x200;

  if (ut_is_running(bar))
  {
#define CONFIG_UT_CMD 0x3
    epci_wr32_reg(bar, (ebs_reg1 + 0x00), CONFIG_UT_CMD);
    ut_wait(bar);
  }
}

static size_t get_ddr_wordsize(void)
{
  /* mif_status register offset is 0x240 */
  /* return the word size, in bytes */

  epcihandle_t bar;
  size_t word_size;
  uint32_t x;

  bar = epci_open("10ee:eb01", NULL, 1);
  if (bar == EPCI_BAD_HANDLE) return sizeof(uint32_t);

  epci_rd32_reg(bar, 0x240, &x);
  word_size = (size_t)(1 << (2 + ((x >> 8) & 3)));

  epci_close(bar);

  return word_size;
}

static void ut_start
(epcihandle_t bar, size_t size, unsigned int is_loop)
{
  /* ut_port size is in fifo_dw_count * word_size units */

  static const unsigned int fifo_dw_count = 256;
  static const size_t ebs_reg1 = 0x200;
  const size_t word_size = get_ddr_wordsize();
  uint32_t value;

  epci_wr32_reg(bar, (ebs_reg1 + 0x04), 0);

  /* ctrl_size */
  value = size / (word_size * fifo_dw_count);
  if ((size % (word_size * fifo_dw_count))) ++value;
  epci_wr32_reg(bar, (ebs_reg1 + 0x08), value);

  /* ut_cmd_code: fixed pattern + swapped word32 */
  value = CONFIG_UT_CMD;
  if (is_loop) value |= 1 << 3;
  epci_wr32_reg(bar, (ebs_reg1 + 0x00), value);
}

static int ut_write(epcihandle_t bar, size_t size, unsigned int is_utw)
{
  /* size, word_size in bytes */

  ut_stop_if_running(bar);
  ut_start(bar, size, 0);

  if (is_utw)
  {
    ut_wait(bar);
  }
  else
  {
    /* sleep a bit before passing */
    usleep(100000);
  }

  return 0;
}

/* fill memory with a known pattern */

static unsigned int seed = 0;

__attribute__((unused))
static void make_pattern(uint8_t* p, size_t i, size_t w)
{
  for (; w; --w, p += w) *(uint32_t*)p = (uint32_t)(seed + i);
}

static int fill_mem
(
 epcihandle_t bar[3],
 ebuf_handle_t* buf, size_t mem_size, size_t ebs_dwidth,
 unsigned int is_utw,
 unsigned int is_bram
)
{
  /* assume buf large enough */

  if (is_bram == 0)
  {
    ut_write(bar[1], mem_size, is_utw);
  }
  else /* bram, soft write */
  {
    uint8_t* const data = ebuf_get_data(buf);

    size_t i;
    size_t count;

    seed = 0x00000000;

    count = mem_size / (sizeof(uint32_t) * ebs_dwidth);
    for (i = 0; i < count; ++i)
      *((uint32_t*)(data + i * sizeof(uint32_t))) = (uint32_t)i;

    /* BRAM in EBONE slave mode */
    set_bram_mode(bar[0], 0);

    soft_write(buf, bar[2], mem_size, ebs_dwidth);

    /* BRAM in EBFT slave mode */
    set_bram_mode(bar[0], 2);
  }

  return 0;
}


/* memory checking */

#define CONFIG_RESET_BYTE 0x2a

__attribute__((unused))
static void make_pattern2(uint8_t* p, size_t i, size_t w)
{
  /* hardcoded pattern */
  /* the bram is 32 bit wide when written by soft but 64 bit wide */
  /* when read by dma. the second 32 bits are filled with a known */
  /* pattern */

  const size_t ww = w;
  for (w = ww; w; --w, ++p) *p = (uint8_t)((seed + i) & 0xffffffff);
  for (w = ww; w; --w, ++p) *p = (uint8_t)((seed + i) & 0x99999999);
  for (w = ww; w; --w, ++p) *p = (uint8_t)((seed + i) & 0xaaaaaaaa);
  for (w = ww; w; --w, ++p) *p = (uint8_t)((seed + i) & 0x55555555);
}

static uint8_t* make_ut_pattern(size_t w, size_t i, size_t o, size_t n)
{
  /* i is a 32 bits key repeated ii times so that: */
  /* sizeof(uint32_t) * ii = n */

  /* everything in bytes */
  /* w the ebft_dwidth */
  /* i the position */
  /* o the offset in pattern */
  /* n the size in pattern */

  static uint8_t pattern[512 / 8];
  uint32_t* pp;
  size_t oo;
  size_t ii;
  size_t j;

  memset(pattern, CONFIG_RESET_BYTE, sizeof(pattern));

  /* first 32 bits */
  oo = o % sizeof(uint32_t);
  if (oo)
  {
    pp = (uint32_t*)pattern + o / sizeof(uint32_t);
    *pp = i;
    memset(pp, CONFIG_RESET_BYTE, oo);

    if ((sizeof(uint32_t) - oo) > n)
    {
      memset(pattern + o + n, CONFIG_RESET_BYTE, sizeof(uint32_t));
      n = 0;
      oo = sizeof(uint32_t);
    }
    n -= sizeof(uint32_t) - oo;
    o += sizeof(uint32_t) - oo;
  }

  /* entire count */
  pp = (uint32_t*)(pattern + o);
  ii = n / sizeof(uint32_t);
  for (j = 0; j < ii; ++j, ++pp) *pp = i;
  o += ii * sizeof(uint32_t);
  n -= ii * sizeof(uint32_t);

  /* remaining */
  if (n)
  {
    *pp = i;
    memset(pattern + o + n, CONFIG_RESET_BYTE, sizeof(uint32_t) - n);
  }

  return pattern;
}

__attribute__((unused)) static void make_pattern_ddr_128(uint8_t* p, size_t i)
{
  /* hardcoded pattern */
#if 0
  *((uint32_t*)p + 0) = i & 0xffffffff;
  *((uint32_t*)p + 1) = i & 0x99999999;
  *((uint32_t*)p + 2) = i & 0xaaaaaaaa;
  *((uint32_t*)p + 3) = i & 0x55555555;
#else
  *((uint32_t*)p + 0) = i;
  *((uint32_t*)p + 1) = i;
  *((uint32_t*)p + 2) = i;
  *((uint32_t*)p + 3) = i;
#endif
}

static int mcmp_and_erase
(const uint8_t* a, uint8_t* b, size_t n, size_t* e, uint8_t ebuf[8])
{
  /* compare memory, return -1 on mismatch or 0 */
  /* *e is equal to the last compared offset */
  /* memory is erased */

  for (*e = 0; *e != n; ++*e)
  {
    ebuf[(*e) % 8] = b[*e];
    if (a[*e] != b[*e]) return -1;
    b[*e] = 0x2a;
  }

  return 0;
}

static int check_buf
(
 ebuf_handle_t* buf,
 size_t soff, size_t ssize,
 size_t doff, size_t dsize,
 size_t size,
 size_t ebs_dwidth, size_t ebft_dwidth,
 size_t* eoff, uint8_t ebuf[8]
)
{
  /* soff the source memory offset */
  /* ssize the source memory size */
  /* doff the destination buffer offset */
  /* dsize the destination size offset */
  /* size the size to check relative to doff */
  /* eoff the error offset, if any */

  uint8_t* pattern;
  uint8_t* data;
  size_t i;
  size_t n;
  size_t e = 0;
  size_t count = size / ebft_dwidth;
  const size_t smod = ssize / ebft_dwidth;

  memset(ebuf, 0, 8);

  data = ebuf_get_data(buf);

  /* check first separately */
  n = soff % ebft_dwidth;
  if (n)
  {
    i = ebft_dwidth - n;
    if (i > size) i = size;

    pattern = make_ut_pattern(ebft_dwidth, soff / ebft_dwidth, n, i);

    if (mcmp_and_erase(pattern + n, data + doff, i % smod, &e, ebuf))
    {
      /* realign for error offset computation */
      data += doff;
      goto on_error;
    }

    soff += i;
    doff += i;
    size -= i;
  }

  /* use recomputed offset and size */
  i = soff / ebft_dwidth;
  data += doff;
  count = size / ebft_dwidth;
  for (n = 0; n < count; ++n, ++i, data += ebft_dwidth)
  {
    pattern = make_ut_pattern(ebft_dwidth, i % smod, 0, ebft_dwidth);
    if (mcmp_and_erase(pattern, data, ebft_dwidth, &e, ebuf)) goto on_error;
  }
  size -= count * ebft_dwidth;

  /* check last separately */
  if (size)
  {
    pattern = make_ut_pattern(ebft_dwidth, i % smod, 0, size);
    if (mcmp_and_erase(pattern, data, size, &e, ebuf)) goto on_error;
  }

  return 0;

 on_error:
#if 0
  n = i * ebft_dwidth;
  printf("error at %lu (off == %lu)\n", i, soff / ebft_dwidth);
  printf("read    : ");
  for (i = 0; i < ebft_dwidth; ++i) printf("%02x", data[i]);
  printf("\n");
  printf("expected: ");
  for (i = 0; i < ebft_dwidth; ++i) printf("%02x", pattern[i]);
  printf("\n");
#endif
  *eoff = (size_t)((uintptr_t)data - (uintptr_t)ebuf_get_data(buf)) + e;
  return -1;
}


/* benchmarking */

static inline double to_payload_mbps(unsigned long size, uint64_t usecs)
{
  /* convert size per usecs in megabyte per second.
     the returned value is for payload data only, not taking PCIE
     meta into account.
   */

  return (1000000 * (double)size) / ((double)usecs * 1024 * 1024);
}


static uint64_t get_cpu_hz(void);
static inline uint64_t rdtsc(void)
{
#ifdef CONFIG_FREESCALE_IMX6
  struct timeval tm;
  gettimeofday(&tm, NULL);
  return tm.tv_sec * 1000000 + tm.tv_usec;
#else
  uint32_t a, d;
  __asm__ __volatile__
  (
   "rdtsc \n\t"
   : "=a"(a), "=d"(d)
  );
  return ((uint64_t)d << 32) | a;
#endif
}

static inline uint64_t sub_ticks(uint64_t a, uint64_t b)
{
  if (a > b) return UINT64_MAX - a + b;
  return b - a;
}

static uint64_t get_cpu_hz(void)
{
  /* return the cpu freq, in mhz */

  static uint64_t cpu_hz = 0;
  unsigned int i;
  unsigned int n;
  struct timeval tms[3];
  uint64_t ticks[2];
  uint64_t all_ticks[10];

  if (cpu_hz != 0) return cpu_hz;

  n = sizeof(all_ticks) / sizeof(all_ticks[0]);
  for (i = 0; i < n; ++i)
  {
    gettimeofday(&tms[0], NULL);
    ticks[0] = rdtsc();
    while (1)
    {
      gettimeofday(&tms[1], NULL);
      timersub(&tms[1], &tms[0], &tms[2]);

      if (tms[2].tv_usec >= 9998)
      {
	ticks[1] = rdtsc();
	all_ticks[i] = sub_ticks(ticks[0], ticks[1]);
	break ;
      }
    }
  }

  cpu_hz = 0;
  for (i = 0; i < n; ++i) cpu_hz += all_ticks[i];
  cpu_hz *= 10;

  return cpu_hz;
}

static inline uint64_t ticks_to_us(uint64_t ticks)
{
  return (ticks * 1000000) / get_cpu_hz();
}

static size_t arg_to_size(const char* s)
{
  int base = 10;
  if ((strlen(s) > 2) && (s[0] == '0') && (s[1] == 'x')) base = 16;
  return (size_t)strtoul(s, NULL, base);
}

static void parse_interval(const char* s, size_t* lo, size_t* hi)
{
  /* -size size */
  /* -size lo:hi */

  char lbuf[32];
  size_t i;

  for (i = 0; (i != (sizeof(lbuf) - 1)) && s[i]; ++i)
  {
    if (s[i] == ':') break ;
    lbuf[i] = s[i];
  }
  lbuf[i] = 0;

  *lo = arg_to_size(lbuf);

  if (s[i] == ':') *hi = arg_to_size(s + i + 1);
  else *hi = *lo;
}

/* debugging probe setup */

__attribute__((unused)) static void setup_probe(void)
{
  /* const uint32_t value = (10 << 15) | (9 << 10) | (8 << 5) | (12 << 0); */
  static const uint32_t value = (11 << 15) | (10 << 10) | (9 << 5) | (8 << 0);
  static char* const device_id = "10ee:eb01";
  epcihandle_t bar;
  bar = epci_open(device_id, NULL, 0);
  epci_wr32_reg(bar, 0x0004, value);
  epci_close(bar);
}

/* command line */

#define CMD_FLAG_SOFF (1 << 0)
#define CMD_FLAG_DOFF (1 << 1)
#define CMD_FLAG_SIZE (1 << 2)
#define CMD_FLAG_LOOP (1 << 3)
#define CMD_FLAG_PRINT (1 << 4)
#define CMD_FLAG_UT_WAIT (1 << 5)
#define CMD_FLAG_UT_PAUSE (1 << 6)
#define CMD_FLAG_UT_LOOP (1 << 7)
#define CMD_FLAG_HELP (1 << 8)
#define CMD_FLAG_DMA_POLL (1 << 9)
#define CMD_FLAG_SOFF_ALIGN_4K (1 << 10)
#define CMD_FLAG_SOFF_ALIGN_WORD (1 << 11)
#define CMD_FLAG_DOFF_ALIGN_WORD (1 << 12)
#define CMD_FLAG_EBS_BRAM (1 << 13)
#define CMD_FLAG_EBS_SEG (1 << 14)
#define CMD_FLAG_EBS_OFF (1 << 15)
#define CMD_FLAG_WORD_SIZE (1 << 16)
#define CMD_FLAG_SMEM_SIZE (1 << 17)

typedef struct
{
  unsigned int flags;
  size_t soff_lo;
  size_t soff_hi;
  size_t doff_lo;
  size_t doff_hi;
  size_t size_lo;
  size_t size_hi;
  size_t ut_pause;
  size_t ebs_seg;
  size_t ebs_off;
  size_t loop_count;
  size_t loop_delay;
  size_t word_size;
  size_t smem_size;
} cmd_info_t;

static int get_cmd_info(cmd_info_t* cmd, int ac, char** av)
{
  int i;

  cmd->flags = 0;
  cmd->soff_lo = 0;
  cmd->soff_hi = 0;
  cmd->doff_lo = 0;
  cmd->doff_hi = 0;
  cmd->size_lo = 0;
  cmd->size_hi = 0;
  cmd->ut_pause = 0;
  cmd->ebs_seg = 0;
  cmd->ebs_off = 0;
  cmd->loop_count = 0;
  cmd->loop_delay = 0;
  cmd->word_size = 0;
  cmd->smem_size = 0;

  if (ac == 0) goto on_help;

  if (ac % 2) goto on_error;

  for (i = 0; i != ac; i += 2)
  {
    const char* const k = av[i + 0];
    const char* const v = av[i + 1];

    if (strcmp(k, "-soff") == 0)
    {
      if (strcmp(v, "align_4k") == 0)
      {
	cmd->flags |= CMD_FLAG_SOFF_ALIGN_4K;
      }
      else if (strcmp(v, "align_word") == 0)
      {
	cmd->flags |= CMD_FLAG_SOFF_ALIGN_WORD;
      }
      else
      {
	cmd->flags |= CMD_FLAG_SOFF;
	parse_interval(v, &cmd->soff_lo, &cmd->soff_hi);
      }
    }
    else if (strcmp(k, "-doff") == 0)
    {
      if (strcmp(v, "align_word") == 0)
      {
	cmd->flags |= CMD_FLAG_DOFF_ALIGN_WORD;
      }
      else
      {
	cmd->flags |= CMD_FLAG_DOFF;
	parse_interval(v, &cmd->doff_lo, &cmd->doff_hi);
      }
    }
    else if (strcmp(k, "-size") == 0)
    {
      cmd->flags |= CMD_FLAG_SIZE;
      parse_interval(v, &cmd->size_lo, &cmd->size_hi);
    }
    else if (strcmp(k, "-loop") == 0)
    {
      cmd->flags |= CMD_FLAG_LOOP;
      if (strcmp(v, "no") == 0) cmd->loop_count = 1;
      else if (strcmp(v, "yes") == 0) cmd->loop_count = (size_t)-1;
      else cmd->loop_count = arg_to_size(v);
    }
    else if (strcmp(k, "-print") == 0)
    {
      if (strcmp(v, "yes") == 0) cmd->flags |= CMD_FLAG_PRINT;
    }
    else if ((strcmp(k, "-utw") == 0) || (strcmp(k, "-ut_wait") == 0))
    {
      if (strcmp(v, "yes") == 0) cmd->flags |= CMD_FLAG_UT_WAIT;
    }
    else if (strcmp(k, "-ut_pause") == 0)
    {
      cmd->flags |= CMD_FLAG_UT_PAUSE;
      cmd->ut_pause = arg_to_size(v);
    }
    else if (strcmp(k, "-ut_loop") == 0)
    {
      if (strcmp(v, "yes") == 0) cmd->flags |= CMD_FLAG_UT_LOOP;
    }
    else if (strcmp(k, "-dma_poll") == 0)
    {
      if (strcmp(v, "yes") == 0) cmd->flags |= CMD_FLAG_DMA_POLL;
    }
    else if (strcmp(k, "-ebs_type") == 0)
    {
      if (strcmp(v, "bram") == 0) cmd->flags |= CMD_FLAG_EBS_BRAM;
    }
    else if (strcmp(k, "-ebs_seg") == 0)
    {
      cmd->flags |= CMD_FLAG_EBS_SEG;
      cmd->ebs_seg = arg_to_size(v);
    }
    else if (strcmp(k, "-ebs_off") == 0)
    {
      cmd->flags |= CMD_FLAG_EBS_OFF;
      cmd->ebs_off = arg_to_size(v);
    }
    else if (strcmp(k, "-loop_delay") == 0)
    {
      cmd->loop_delay = arg_to_size(v);
    }
    else if (strcmp(k, "-word_size") == 0)
    {
      cmd->word_size = arg_to_size(v);
    }
    else if (strcmp(k, "-smem_size") == 0)
    {
      cmd->flags |= CMD_FLAG_SMEM_SIZE;
      cmd->smem_size = arg_to_size(v);
    }
    else if (strcmp(k, "-help") == 0)
    {
    on_help:
      cmd->flags |= CMD_FLAG_HELP;
      return 0;
    }
    else
    {
      goto on_error;
    }
   }

  return 0;

 on_error:
  return -1;
}

/* toremove */
static void setup_debug_stuffs(const cmd_info_t* info)
{
  epcihandle_t bar = epci_open("10ee:eb01", NULL, 1);
  uint32_t x;

  x = 0;
  if (info->flags & CMD_FLAG_UT_PAUSE) x |= info->ut_pause << 4;

  epci_wr32_reg(bar, 0x020c, x);

  epci_close(bar);
}
/* toremove */

static void do_help(void)
{
  printf("command line options:\n");
  printf("-help                : print this help\n");
  printf("-soff <off>          : the source offset, in bytes\n");
  printf("-soff min:max        : random source offset in interval [min:max]\n");
  printf("-soff align_4k       : align source offset on 4KB\n");
  printf("-soff align_word     : align source offset on words\n");
  printf("-doff <off>          : the destination offset, in bytes\n");
  printf("-doff min:max        : random dest offset in interval [min:max]\n");
  printf("-doff align_word     : align destination offset words\n");
  printf("-size size           : the transfer size, in bytes\n");
  printf("-size min:max        : random size in interval [min:max]\n");
  printf("-loop {yes,no}       : transfer infinitely until error\n");
  printf("-loop <count>        : do a given count of transfer\n");
  printf("-print {yes,no}      : dump the memory contents\n");
  printf("-ut_wait {yes,no}    : wait for the utport to end before reading\n");
  printf("-ut_pause <pause>    : set the utport pause (<= 15)\n");
  printf("-ut_loop {yes,no}    : enable the concurrent read write mode\n");
  printf("-dma_poll {yes,no}   : enable dma status polling\n");
  printf("-ebs_type {ddr,bram} : ebone slave type (default to ddr)\n");
  printf("-ebs_seg <segment>   : ebone slave segment (default to 1)\n");
  printf("-ebs_off <offset>    : ebone slave offset (default to 0x200)\n");
  printf("-loop_delay <delay>  : delay between loops, in microseconds\n");
  printf("-word_size <size>    : word size, in bytes (default to 16)\n");
  printf("-smem_size <size>    : source memory size size, in bytes\n");
  printf("\n");
  printf("notes:\n");
  printf(". hexadecimal (starting with 0x) can be used for numeric arguments\n");
  printf(". {yes,no} options default to no if not specified\n");
  printf(". -utw is still supported, BUT you should use -ut_wait\n");
}

static volatile unsigned int is_sigint = 0;

static void on_sigint(int x)
{
  is_sigint = 1;
}


static double get_total_s(double start_s)
{
  struct timeval tv;
  double s;

  gettimeofday(&tv, NULL);
  s = (double)tv.tv_sec + (double)tv.tv_usec / 1000000.0;
  return s - start_s;
}


/* main */

int main(int ac, char** av)
{
  dma_mem_t* mem;
  const char* const dev_id = "10ee:eb01";

#define ERR_TYPE_SUCCESS 0
#define ERR_TYPE_OTHER -1
#define ERR_TYPE_EBFT -2
#define ERR_TYPE_CHECK -3
#define ERR_TYPE_UNDERFLOW -4
#define ERR_TYPE_OVERFLOW -5
#define ERR_TYPE_CMDLINE -6
  int err = ERR_TYPE_OTHER;

  edma_handle_t dma;
  ebuf_handle_t mbuf;
  ebuf_handle_t dbuf;
  eaio_handle_t* aio;
  epcihandle_t bar[3];
  edma_desc_t desc;
  uint64_t i;
  uint64_t total_size;

  size_t err_off = 0;

  eaio_desc_t aio_desc;
  size_t soff;
  size_t doff;

  cmd_info_t cmd;

  double start_s;

  /* setup_probe(); */

  /* retrieve command line */
  if (get_cmd_info(&cmd, ac - 1, av + 1))
  {
    err = ERR_TYPE_CMDLINE;
    goto on_error_0;
  }

  if (cmd.flags & CMD_FLAG_HELP)
  {
    do_help();
    err = ERR_TYPE_SUCCESS;
    goto on_error_0;
  }

  /* open bars */
  bar[0] = epci_open(dev_id, NULL, 0);
  if (bar[0] == EPCI_BAD_HANDLE) goto on_error_0;
  bar[1] = epci_open(dev_id, NULL, 1);
  if (bar[1] == EPCI_BAD_HANDLE) goto on_error_1;
  bar[2] = epci_open(dev_id, NULL, cmd.ebs_seg);
  if (bar[2] == EPCI_BAD_HANDLE) goto on_error_2;

  /* affect default segment offset values */
  if ((cmd.flags & CMD_FLAG_EBS_SEG) == 0)
  {
    cmd.ebs_seg = 1;
    cmd.flags |= CMD_FLAG_EBS_SEG;
  }
  if ((cmd.flags & CMD_FLAG_EBS_OFF) == 0)
  {
    cmd.ebs_off = 0x200;
    cmd.flags |= CMD_FLAG_EBS_OFF;
  }

  /* initialize subsystems */
  if (ebuf_init_lib(EBUF_FLAG_PHYSICAL)) goto on_error_3;
  if (edma_init_lib()) goto on_error_4;
  if (eaio_init_lib()) goto on_error_5;

  if (cmd.flags & CMD_FLAG_EBS_BRAM) mem = &bram_mem;
  else mem = &ddr_mem;

  if (cmd.flags & CMD_FLAG_SMEM_SIZE)
  {
    mem->size = cmd.smem_size;
  }
  else
  {
    if ((cmd.flags & CMD_FLAG_EBS_BRAM) == 0)
    {
      mem->size = get_ddr_size(bar[1]);
    }
  }
  desc.ssize = mem->size;

  /* default lo,hi sizes */
  if ((cmd.flags & CMD_FLAG_SIZE) == 0)
  {
    cmd.size_lo = 1;
    cmd.size_hi = mem->size;
    cmd.flags |= CMD_FLAG_SIZE;
  }

  if ((cmd.flags & CMD_FLAG_SOFF) == 0)
  {
    cmd.soff_lo = 0;
    cmd.soff_hi = mem->size - 1;
    cmd.flags |= CMD_FLAG_SOFF;
  }

  if ((cmd.flags & CMD_FLAG_DOFF) == 0)
  {
    cmd.doff_lo = 0;
    cmd.doff_hi = mem->size - 1;
    cmd.flags |= CMD_FLAG_DOFF;
  }

  /* word size. default to 128 bits (16 bytes) */
  if ((cmd.flags & CMD_FLAG_WORD_SIZE) == 0)
  {
    cmd.flags |= CMD_FLAG_WORD_SIZE;
    cmd.word_size = 16;
  }

  /* init DMAs */
  edma_init_desc(&desc);
  desc.pci_id = dev_id;
  desc.pci_bar = 1;
  desc.pci_off = 0x400;

  if (cmd.flags & CMD_FLAG_EBS_BRAM) desc.flags = EDMA_FLAG_BRAM;
  else desc.flags = EDMA_FLAG_MIF;

  /* info should come from ram handle */
  desc.ebs_seg = cmd.ebs_seg;
  desc.ebs_off = cmd.ebs_off;

  if ((cmd.flags & CMD_FLAG_DMA_POLL) == 0) desc.flags |= EDMA_FLAG_IRQ;

  /* toremove */
  setup_debug_stuffs(&cmd);

  desc.soff = 0;

  if (edma_open(&dma, &desc)) goto on_error_6;

  /* alloc buffers */
  if (ebuf_alloc(&mbuf, mem->size, EBUF_FLAG_PHYSICAL)) goto on_error_7;
  if (ebuf_alloc(&dbuf, cmd.size_hi, EBUF_FLAG_PHYSICAL)) goto on_error_7;

  srand(getpid() * time(NULL));

  /* initialize the source memory */
  const unsigned int is_utw = cmd.flags & CMD_FLAG_UT_WAIT;
  const unsigned int is_bram = cmd.flags & CMD_FLAG_EBS_BRAM;
  if (fill_mem(bar, &mbuf, mem->size, mem->ebs_dwidth, is_utw, is_bram))
    goto on_error_8;

  /* start concurrent memory write */
  if (cmd.flags & CMD_FLAG_UT_LOOP) ut_start(bar[1], mem->size, 1);

  /* default loop count */
  if ((cmd.flags & CMD_FLAG_LOOP) == 0) cmd.loop_count = 1;

  signal(SIGINT, on_sigint);

  aio = edma_get_default_aio(&dma);
  eaio_open(aio);
  edma_init_aio_desc(&dma, &aio_desc);
  aio_desc.buf = &dbuf;
  if (eaio_start(aio, &aio_desc)) goto on_error_8;

  start_s = get_total_s(0);

  total_size = 0;

  soff = 0;
  doff = 0;

  i = 0;

  while (is_sigint == 0)
  {
    static const uint64_t print_size = 512 * 1024 * 1024;

    void* p;
    size_t n;

    eaio_get_data(aio, &p, &n);

    if ((total_size / print_size) != ((total_size + n) / print_size))
    {
      static const uint64_t one_mb = 1 * 1024 * 1024;
      const double total_s = get_total_s(start_s);
      printf("total_size: %llu MB\n", (total_size + n) / one_mb);
      printf("bandwidth : %lf MB/s\n",
	     (double)(total_size + n) / (total_s * (double)one_mb));
      fflush(stdout);
    }

    if (n)
    {
      const size_t ssize = mem->size;
      const size_t dsize = ebuf_get_size(&dbuf);
      const size_t ebs_dwidth = mem->ebs_dwidth;
      const size_t ebft_dwidth = dma.ebft_dwidth;
      size_t eoff;
      uint8_t ebuf[8];

      const int check_err = check_buf
	(&dbuf, soff, ssize, doff, dsize, n, ebs_dwidth, ebft_dwidth, &eoff, ebuf);
      if (check_err)
      {
	size_t i;
	printf("check error at %zx, %zx, %zx\n", doff, doff + n, eoff);
	for (i = 0; i != sizeof(ebuf); ++i) printf("%02x", ebuf[i]);
	printf("\n");
	fflush(stdout);
	break ;
      }

      soff = (soff + n) % mem->size;
      doff = (doff + n) % ebuf_get_size(&dbuf);

      ++i;

      total_size += (uint64_t)n;

      eaio_seek_data(aio, n);
    }
  }

  if (eaio_get_state(aio) != EAIO_STATE_STOPPED) eaio_stop(aio);

  printf("total_size == 0x%llx, i == 0x%llx\n", total_size, i);

  /* success */
  err = 0;

  eaio_close(aio);
 on_error_8:
  /* stop concurrent memory write */
  if (cmd.flags & CMD_FLAG_UT_LOOP) ut_stop_if_running(bar[1]);
  ebuf_free(&dbuf);
  ebuf_free(&mbuf);
 on_error_7:
  edma_close(&dma);
 on_error_6:
  eaio_fini_lib();
 on_error_5:
  edma_fini_lib();
 on_error_4:
  ebuf_fini_lib();
 on_error_3:
  epci_close(bar[2]);
 on_error_2:
  epci_close(bar[1]);
 on_error_1:
  epci_close(bar[0]);
 on_error_0:

  switch (err)
  {
  case ERR_TYPE_EBFT:
    printf("ERROR: EBFT STATUS\n");
    break ;
  case ERR_TYPE_CHECK:
    printf("ERROR: MEMORY CHECK, doff == 0x%zx\n", err_off);
    break ;
  case ERR_TYPE_UNDERFLOW:
    printf("ERROR: MEMORY UNDERFLOW\n");
    break ;
  case ERR_TYPE_OVERFLOW:
    printf("ERROR: MEMORY OVERFLOW\n");
    break ;
  case ERR_TYPE_CMDLINE:
    printf("ERROR: INVALID COMMAND LINE\n");
    break ;
  case ERR_TYPE_OTHER:
    printf("ERROR: UNSPECIFIED ERROR OCCURED (driver not loaded...)\n");
    break ;
  default:
  case ERR_TYPE_SUCCESS:
    printf("SUCCESS\n");
    break ;
  }

  return err;
}
