#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/select.h>
#include "libepci.h"
#include "libedma.h"
#include "libeaio.h"
#include "libebuf.h"
#include "libuirq.h"


/* configuration macros */
#define CONFIG_DEBUG 1


#if CONFIG_DEBUG
#include <stdio.h>
#define PRINTF(__s, ...)  \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define PERROR() printf("[!] %d\n", __LINE__)
#define ASSERT(__x) if (!(__x)) printf("[!] %d\n", __LINE__)
#else
#define PRINTF(__s, ...)
#define PERROR()
#deifne ASSERT(__x)
#endif


static void print_buf(const uint8_t* x, size_t n)
{
  size_t i;

  for (i = 0; i != n; ++i)
  {
    if (i && ((i % 16) == 0)) printf("\n");
    printf("%02x", x[i]);
  }
  printf("\n");
}


/* main */

int main(int ac, char** av)
{
  static const size_t mem_size = 0x100;

  ebuf_handle_t buf;
  edma_handle_t dma;
  struct edma_desc desc;
  int err = -1;

  /* initialize subsystems */

  if (uirq_init_lib())
  {
    PERROR();
    goto on_error_0;
  }

  if (ebuf_init_lib(EBUF_FLAG_PHYSICAL))
  {
    PERROR();
    goto on_error_1;
  }

  if (edma_init_lib())
  {
    PERROR();
    goto on_error_2;
  }

  /* open edma */

  edma_init_desc(&desc);

  desc.pci_id = "10ee:eb01";
  desc.pci_bar = 1;
  desc.pci_off = 0x60 * 4;

  /* info should come from ram handle */
  desc.flags = EDMA_FLAG_BRAM | EDMA_FLAG_IRQ;
  desc.ebs_seg = 0x1;
  desc.ebs_off = 0x0;

  desc.soff = 0;
  desc.ssize = mem_size;

  if (edma_open(&dma, &desc))
  {
    PERROR();
    goto on_error_3;
  }

  /* allocate a buffer */

  if (ebuf_alloc(&buf, mem_size, EBUF_FLAG_PHYSICAL))
  {
    PERROR();
    goto on_error_4;
  }

  memset(ebuf_get_data(&buf), 0x2a, mem_size);

  /* asynchronous readout */

  /* read and check data */
  if (edma_read(&dma, 0, &buf, 0, mem_size))
  {
    PERROR();
    goto on_error_5;
  }

  print_buf(ebuf_get_data(&buf), mem_size);

  /* success */
  err = 0;

 on_error_5:
  ebuf_free(&buf);
 on_error_4:
  edma_close(&dma);
 on_error_3:
  edma_fini_lib();
 on_error_2:
  ebuf_fini_lib();
 on_error_1:
  uirq_fini_lib();
 on_error_0:
  return err;
}
