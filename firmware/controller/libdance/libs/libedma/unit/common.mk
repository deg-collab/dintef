

DANCE_SDK_PLATFORM ?= seco_imx6

LIBDANCE_HOME ?= ../../../..

LIBEDMA_DIR := ../../src
LIBEPCI_DIR := $(LIBDANCE_HOME)/libs/libepci/src
LIBPCI_DIR = $(DANCE_SDK_DEPS_DIR)/lib
LIBUIRQ_DIR := $(LIBDANCE_HOME)/libs/libuirq/src
LIBEBUF_DIR := $(LIBDANCE_HOME)/libs/libebuf/src
LIBPMEM_DIR := $(LIBDANCE_HOME)/libs/libpmem/src

DEPENDENCIES_DIRS := \
      $(LIBEDMA_DIR)  \

HEADER_DIRS = \
      $(LIBEDMA_DIR)  \
      $(LIBPMEM_DIR)  \
      $(LIBEBUF_DIR)  \
      $(LIBUIRQ_DIR)  \
      $(LIBEPCI_DIR)  \

LIBRARY_DIRS = \
      $(LIBEDMA_DIR) \
      $(LIBEPCI_DIR)  \
      $(LIBPCI_DIR)  \
      $(LIBUIRQ_DIR) \
      $(LIBEBUF_DIR)  \
      $(LIBPMEM_DIR)  \

LDLIBS := -ledma -lepci -lpci -luirq -lebuf -lpmem -pthread

include $(LIBDANCE_HOME)/build/libtool.mk
