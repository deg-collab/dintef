#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <sched.h>
#include <pthread.h>
#include <string.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include "libepci.h"
#include "libefifo.h"
#include "libebuf.h"
#include "libeaio.h"


#define EFIFO_CONFIG_DEBUG 1
#if (EFIFO_CONFIG_DEBUG == 1)
#include <stdio.h>
#define EFIFO_PRINTF(__s, ...) \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define EFIFO_PERROR() \
do { printf("[!] %s,%d\n", __FILE__,__LINE__); } while (0)
#define EFIFO_ASSUME(__x) \
do { if (!(__x)) printf("[!] %s,%d\n", __FILE__, __LINE__); } while (0)
#else
#define EFIFO_PRINTF(__s, ...)
#define EFIFO_PERROR()
#define EFIFO_ASSUME(__x)
#endif /* EFIFO_CONFIG_DEBUG */


/* atomic routines */

static inline uint32_t atomic_read(volatile uint32_t* p)
{
  __sync_synchronize();
  return *p;
}

static inline void atomic_write(volatile uint32_t* p, uint32_t x)
{
  __sync_synchronize();
  *p = x;
  __sync_synchronize();
}

static inline void atomic_or(volatile uint32_t* p, uint32_t x)
{
  atomic_write(p, atomic_read(p) | x);
}


/* low level routines. access the actual hardware. */

#define OCCUPANCY_SHIFT 0
#define IRQ_THRESHOLD_SHIFT 0
#define IRQ_ACK_SHIFT 29
#define IRQ_EN_SHIFT 30
#define RESET_SHIFT 31

#define OCCUPANCY_MASK (((1 << 16) - 1) << OCCUPANCY_SHIFT)
#define IRQ_THRESHOLD_MASK (((1 << 16) - 1) << (IRQ_THRESHOLD_SHIFT))
#define IRQ_ACK_MASK (1 << IRQ_ACK_SHIFT)
#define IRQ_EN_MASK (1 << IRQ_EN_SHIFT)
#define RESET_MASK (1 << RESET_SHIFT)

static inline void write_ctl(efifo_handle_t* fifo, uint32_t x)
{
  epci_wr32_reg(fifo->epci_handle, fifo->ctrl_reg_off, x);
}

static inline void read_ctl(efifo_handle_t* fifo, uint32_t* x)
{
  epci_rd32_reg(fifo->epci_handle, fifo->ctrl_reg_off, x);
}

static inline void or_ctl(efifo_handle_t* fifo, uint32_t mask)
{
  uint32_t x;
  read_ctl(fifo, &x);
  write_ctl(fifo, x | mask);
}

static inline void and_ctl(efifo_handle_t* fifo, uint32_t mask)
{
  uint32_t x;
  read_ctl(fifo, &x);
  write_ctl(fifo, x & mask);
}

static inline void read_sta(efifo_handle_t* fifo, uint32_t* x)
{
  epci_rd32_reg(fifo->epci_handle, fifo->stat_reg_off, x);
}

static void write_dat(efifo_handle_t* fifo, const uint32_t* x)
{
  /* write one item of size (h->width * sizeof(uint32_t)) */

  size_t off = fifo->wdat_reg_off;
  size_t i;

  if (fifo->write_width == sizeof(uint16_t))
  {
    for (i = 0; i < fifo->width; ++i, ++x, off += sizeof(uint32_t))
    {
      /* always successful, dont check for error */
      epci_wr32_reg(fifo->epci_handle, off, *x & 0xffff);
      epci_wr32_reg(fifo->epci_handle, off, *x >> 16);
    }
  }
  else /* assumed write_width == width */
  {
    for (i = 0; i < fifo->width; ++i, ++x, off += sizeof(uint32_t))
    {
      /* always successful, dont check for error */
      epci_wr32_reg(fifo->epci_handle, off, *x);
    }
  }
}

static void write_dat_n(efifo_handle_t* fifo, const uint32_t* x, size_t n)
{
  /* write n items of size (h->width * sizeof(uint32_t)) */

  for (; n; --n, x += fifo->width) write_dat(fifo, x);
}

static void read_dat(efifo_handle_t* fifo, uint32_t* x)
{
  /* read one item of size (h->width * sizeof(uint32_t)) */

  size_t off = fifo->rdat_reg_off;
  size_t i;

  for (i = 0; i < fifo->width; ++i, ++x, off += sizeof(uint32_t))
  {
    /* always successful, dont check for error */
    epci_rd32_reg(fifo->epci_handle, off, x);
  }
}

static void read_dat_n(efifo_handle_t* fifo, uint32_t* x, size_t n)
{
  /* read n items of size (h->width * sizeof(uint32_t)) */

  for (; n; --n, x += fifo->width) read_dat(fifo, x);
}

static void read_mirq(efifo_handle_t* fifo, uint32_t* mirq)
{
  uint32_t pos;
  read_sta(fifo, &pos);
  *mirq = 1 << ((pos >> 16) & 0xf);
}

static inline void clear_reset_bit(efifo_handle_t* fifo)
{
  and_ctl(fifo, ~RESET_MASK);
}

static inline void set_reset_bit(efifo_handle_t* fifo)
{
  or_ctl(fifo, RESET_MASK);
}

static inline void wait_ebone_clocks_x3(efifo_handle_t* fifo)
{
  /* 3 clocks @ 125mhz == 24 ns */
}

__attribute__((unused))
static inline void reset_fifo(efifo_handle_t* fifo)
{
  set_reset_bit(fifo);
  wait_ebone_clocks_x3(fifo);
  clear_reset_bit(fifo);
}

static inline void ack_irq(efifo_handle_t* fifo)
{
  or_ctl(fifo, IRQ_ACK_MASK);
}

static inline void enable_irq(efifo_handle_t* fifo, uint32_t threshold)
{
  and_ctl(fifo, ~(IRQ_ACK_MASK | IRQ_EN_MASK | IRQ_THRESHOLD_MASK));
  or_ctl(fifo, IRQ_EN_MASK | (threshold << IRQ_THRESHOLD_SHIFT));
}

static inline void disable_irq(efifo_handle_t* fifo)
{
  and_ctl(fifo, ~IRQ_EN_MASK);
}

static size_t get_occupancy(efifo_handle_t* fifo)
{
  uint32_t x;
  read_sta(fifo, &x);
  return (size_t)((x >> OCCUPANCY_SHIFT) & OCCUPANCY_MASK);
}

__attribute__((unused))
static size_t get_threshold(efifo_handle_t* fifo)
{
  uint32_t x;
  read_ctl(fifo, &x);
  return (size_t)((x >> IRQ_THRESHOLD_SHIFT) & IRQ_THRESHOLD_MASK);
}

void set_threshold(efifo_handle_t* fifo, size_t threshold)
{
  uint32_t x;
  read_ctl(fifo, &x);
  x &= ~IRQ_THRESHOLD_MASK;
  x |= threshold << IRQ_THRESHOLD_SHIFT;
}

static inline void slowdown_polling(void)
{
  /* avoid flooding pcie link with requests */
  usleep(1);
}


/* eaio operations */

static int do_eaio_reset(void* p)
{
  return 0;
}

static int do_eaio_start(void* p)
{
  /* half full irq */
  efifo_handle_t* const fifo = p;
  enable_irq(fifo, fifo->depth / 2 + (fifo->depth & 1));
  return 0;
}

static int do_eaio_stop(void* p)
{
  efifo_handle_t* const fifo = p;
  disable_irq(fifo);
  return 0;
}

static int do_eaio_setup_read
(void* p, ebuf_handle_t* buf, size_t boff, size_t* n)
{
  return 0;
}

static int do_eaio_compl_read
(void* p, ebuf_handle_t* buf, size_t boff, size_t* n)
{
  efifo_handle_t* const fifo = p;
  size_t rsize;
  const size_t ocount = get_occupancy(fifo);
  const size_t osize = ocount * efifo_get_width(fifo);
  const size_t bsize = ebuf_get_size(buf) - boff;

  /* TODO: this should be checked at init time using non debug code */
  EFIFO_ASSUME((bsize % fifo->width) == 0);

  /* adjust read size */
  /* read no more than requested and than occupancy */
  rsize = *n;
  if (rsize > osize) rsize = osize;

  read_dat_n(fifo, ebuf_get_data(buf) + boff, rsize / efifo_get_width(fifo));

  /* update read size */
  *n = rsize;

  return 0;
}

void efifo_init_aio_desc(efifo_handle_t* fifo, eaio_desc_t* desc)
{
  eaio_init_desc(desc);
  desc->flags |= EAIO_FLAG_THRESHOLD;
  desc->thresh = 1 + efifo_count_to_size(fifo, fifo->depth) / 2;
  desc->flags |= EAIO_FLAG_TIMEOUT;
  desc->tmout = 10000;
  desc->mirq = fifo->mirq;
  desc->handle = fifo;
  desc->reset_fn = do_eaio_reset;
  desc->start_fn = do_eaio_start;
  desc->stop_fn = do_eaio_stop;
  desc->setup_read_fn = do_eaio_setup_read;
  desc->compl_read_fn = do_eaio_compl_read;
}

int efifo_alloc_buf(efifo_handle_t* fifo, ebuf_handle_t* buf, size_t size)
{
  return ebuf_alloc(buf, size, EBUF_FLAG_DEFAULT);
}


/* global lib constructors. call once per process. */

int efifo_init_lib(void)
{
  return 0;
}

int efifo_fini_lib(void)
{
  return 0;
}


/* open close fifo handle */

void efifo_init_desc(efifo_desc_t* desc)
{
  /* initialize a desc with default, invalid values */
  desc->flags = 0;
  desc->write_width = 0;
}

int efifo_open(efifo_handle_t* fifo, const efifo_desc_t* desc)
{
  /* TODO: check mapping does not overflow bar size */

  /* fifo data granularity is 32 bits */
  if (desc->width & (sizeof(uint32_t) - 1))
  {
    EFIFO_PERROR();
    goto on_error_0;
  }

  /* capture description */

  fifo->flags = desc->flags;
  fifo->width = desc->width / sizeof(uint32_t);
  fifo->depth = desc->depth;

  /* assume desc->write_width a pow2 */
  /* write width, in bytes */
  if (desc->write_width && (desc->write_width != sizeof(uint16_t)))
  {
    EFIFO_PERROR();
    goto on_error_0;
  }

  fifo->write_width = desc->write_width;

  fifo->epci_handle = desc->epci_handle;
  fifo->ctrl_reg_off = desc->ctrl_reg_off;
  fifo->rdat_reg_off = desc->rdat_reg_off;
  fifo->wdat_reg_off = desc->wdat_reg_off;
  fifo->stat_reg_off = desc->stat_reg_off;

  fifo->slave_id = desc->ebone_slave_id;

  /* disable irq before reset */
  /* reseting the FIFO make the occupancy drop to 0 */
  /* possibly triggering an interrupt irq_en is 1 */

  disable_irq(fifo);
  read_mirq(fifo, &fifo->mirq);

  reset_fifo(fifo);

  return 0;

 on_error_0:
  return -1;
}

int efifo_close(efifo_handle_t* fifo)
{
  eaio_close(&fifo->aio);
  return 0;
}


/* exported: synchronous io operations */

/* low level synchronous io operations */
/* size is an inout parameter: in input, it contains the maxium size */
/* to be read or written. in output, it contains the resulting size. */

int efifo_read(efifo_handle_t* fifo, void* buf, size_t* count)
{
  /* count: how many width sized items to read */

  size_t rem_count;
  size_t occupancy;
  uint32_t* data;

  EFIFO_ASSUME(fifo->flags & EFIFO_FLAG_READ);

  data = (uint32_t*)buf;

  rem_count = *count;
  while (rem_count)
  {
  redo_occupancy:
    occupancy = get_occupancy(fifo);
    if (occupancy == 0)
    {
      slowdown_polling();
      goto redo_occupancy;
    }

    if (occupancy > rem_count) occupancy = rem_count;
    read_dat_n(fifo, data, occupancy);

    data += occupancy * fifo->width;
    rem_count -= occupancy;
  }

  return 0;
}

int efifo_write(efifo_handle_t* fifo, const void* buf, size_t* count)
{
  /* count: how many width sized items to read */

  size_t rem_count;
  size_t navail;
  const uint32_t* data;

  EFIFO_ASSUME(fifo->flags & EFIFO_FLAG_WRITE);

  data = (const uint32_t*)buf;

  rem_count = *count;
  while (rem_count)
  {
  redo_occupancy:
    navail = fifo->depth - efifo_get_occupancy(fifo);

    EFIFO_ASSUME((ssize_t)navail >= 0);

    if (navail == 0)
    {
      slowdown_polling();
      goto redo_occupancy;
    }

    if (navail > rem_count) navail = rem_count;
    write_dat_n(fifo, data, navail);

    data += navail * fifo->width;
    rem_count -= navail;
  }

  return 0;
}

int efifo_read_ebuf
(efifo_handle_t* fifo, ebuf_handle_t* buf, size_t off, size_t* size)
{
  /* WARNING: off is an offset in bytes */
  /* *size is in item size */
  return efifo_read(fifo, ebuf_get_data(buf) + off, size);
}

int efifo_write_ebuf
(efifo_handle_t* fifo, ebuf_handle_t* buf, size_t off, size_t* size)
{
  /* WARNING: off is an offset in bytes */
  /* *size is in item size */
  return efifo_write(fifo, ebuf_get_data(buf) + off, size);
}

size_t efifo_get_occupancy(efifo_handle_t* fifo)
{
  return get_occupancy(fifo);
}


/* exported: unit testing */

void efifo_set_reset_bit(efifo_handle_t* fifo)
{
  set_reset_bit(fifo);
}

void efifo_clear_reset_bit(efifo_handle_t* fifo)
{
  clear_reset_bit(fifo);
}
