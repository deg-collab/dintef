/* start of notes */

/* aio model: the upper layer (ie. fifo, dma) prepares the */
/* object accordingly to the description. then, it opens */
/* an aio handle that will be used to control the aio state. */
/* aios are scheduled in a dedicated thread. this thread */
/* notifies the runtime about aio state changes using pipe */
/* descriptor notifications. */

/* sequencing: the upper layer starts the aio using the */
/* handle previously obtained. the thread calls start_fn */
/* when it actually starts the aio so the upper layer has */
/* a chance to setup the hardware. then, the thread calls */
/* read_fn to prepare the reading of an actual data chunk */
/* and waits until an irq completes the io. when the irq */
/* occurs, the thread calls irq_fn so the upper layer can */
/* read data (if needed, ie. in fifo). */

/* oneshot and continuous modes: in oneshot mode, a size is */
/* specified. when the size is completed, the aio stops. in */
/* continuous mode, no size is specified and the aio runs */
/* until stopped explicitly by the runtime. the backing */
/* buffer is managed as a ring buffer, and overflows are */
/* are indicated by a flag. */

/* end of notes */


#include <pthread.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include "libeaio.h"
#include "libebuf.h"
#include "libuirq.h"
#include "libepci.h"


#define EAIO_CONFIG_DEBUG 1
#if (EAIO_CONFIG_DEBUG == 1)
#include <stdio.h>
#define EAIO_PRINTF(__s, ...) \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define EAIO_PERROR() \
do { printf("[!] %s,%d\n", __FILE__, __LINE__); } while (0)
#define EAIO_ASSUME(__x) \
do { if (!(__x)) printf("[!] %s,%d\n", __FILE__, __LINE__); } while (0)
#else
#define EAIO_PRINTF(__s, ...)
#define EAIO_PERROR()
#define EAIO_ASSUME(__x)
#endif /* EAIO_CONFIG_DEBUG */


/* atomic routines */

static inline uint32_t atomic_read(volatile uint32_t* p)
{
  __sync_synchronize();
  return *p;
}

static inline void atomic_write(volatile uint32_t* p, uint32_t x)
{
  __sync_synchronize();
  *p = x;
  __sync_synchronize();
}

static inline void atomic_or(volatile uint32_t* p, uint32_t x)
{
  atomic_write(p, atomic_read(p) | x);
}


/* bit mask routines */

static size_t mask_to_pos(uint32_t m)
{
  size_t i;
  for (i = 0; i != 31; ++i)
  {
    if ((1 << i) & m) break ;
  }
  return i;
}


/* global eaio thread and routines */

typedef struct eaio_thread
{
  /* wait io event pipe */
  int wait_pipe[2];

  /* msg posting pipe */
  int post_pipe[2];

  /* thread handle */
  pthread_t thread;

  /* async message posting and completion */
  pthread_mutex_t post_lock;
  pthread_mutex_t compl_lock;
  pthread_cond_t compl_cond;

  /* uirq context */
  uirq_handle_t uirq;
  int uirq_fd;
#define EAIO_UIRQ_NPOS 32
  eaio_handle_t* uirq_pos_to_aio[EAIO_UIRQ_NPOS];
  uint32_t uirq_all_mask;

  /* realtime scheduler reference counter */
  size_t realtime_refn;

} eaio_thread_t;


/* asynchronous messaging */

typedef struct async_msg
{
  volatile enum
  {
    OP_NONE = 0,
    OP_START,
    OP_STOP,
    OP_JOIN
  } op;

  struct
  {
    volatile uint32_t* compl_err;
    eaio_handle_t* aio;
  } start_stop;

  struct
  {
    unsigned int do_reset;
  } start;

} async_msg_t;

static void complete_start_stop_msg
(eaio_thread_t* thr, async_msg_t* msg, uint32_t err)
{
  pthread_mutex_lock(&thr->compl_lock);
  *msg->start_stop.compl_err = err;
  pthread_cond_signal(&thr->compl_cond);
  pthread_mutex_unlock(&thr->compl_lock);
}

static int post_wait_async_msg
(eaio_thread_t* thr, async_msg_t* msg)
{
  static const uint32_t not_compl_key = 0x2a2a2a2a;
  volatile uint32_t err = 0;

  if (msg->op != OP_JOIN)
  {
    /* protect as regard with concurrent posters */
    pthread_mutex_lock(&thr->post_lock);
    err = not_compl_key;
    msg->start_stop.compl_err = &err;
  }

  if (write(thr->post_pipe[1], msg, sizeof(async_msg_t)) != sizeof(async_msg_t))
  {
    EAIO_PERROR();
    err = (uint32_t)-1;
    goto on_error;
  }

  if (msg->op != OP_JOIN)
  {
    /* protect as regard with async thread */
    pthread_mutex_lock(&thr->compl_lock);
    while (1)
    {
      if (err != not_compl_key) break ;
      pthread_cond_wait(&thr->compl_cond, &thr->compl_lock);
    }
    pthread_mutex_unlock(&thr->compl_lock);
  }

 on_error:
  if (msg->op != OP_JOIN)
  {
    pthread_mutex_unlock(&thr->post_lock);
  }

  return (int)err;
}

static int post_start_msg
(eaio_thread_t* thr, eaio_handle_t* aio, unsigned int do_reset)
{
  async_msg_t msg;

  msg.op = OP_START;
  msg.start_stop.aio = aio;
  msg.start.do_reset = do_reset;

  return post_wait_async_msg(thr, &msg);
}

static int post_stop_msg(eaio_thread_t* thr, eaio_handle_t* aio)
{
  async_msg_t msg;

  msg.op = OP_STOP;
  msg.start_stop.aio = aio;

  return post_wait_async_msg(thr, &msg);
}

static int post_join_msg(eaio_thread_t* thr)
{
  async_msg_t msg;

  msg.op = OP_JOIN;

  return post_wait_async_msg(thr, &msg);
}


/* scheduler routines */

static int set_sched(int policy)
{
  /* NOTE: setting the thread scheduling priority in pthread_create */
  /* attributes did not work, so we do this here */

  /* NOTE: using realtime policy (SCHED_FIFO) reduce interrupt handling */
  /* latency but can starve the other threads if too much hw ints. */
  /* in this case, default policy (SCHED_OTHER) is be used. */

  struct sched_param param;

  param.sched_priority = sched_get_priority_max(policy);

  if (pthread_setschedparam(pthread_self(), policy, &param))
  {
    EAIO_PERROR();
    return -1;
  }

  return 0;
}

static int set_default_sched(void)
{
  return set_sched(SCHED_OTHER);
}

static int set_realtime_sched(void)
{
  return set_sched(SCHED_FIFO);
}


/* ebone related routines */

static int set_ebone_slave_interrupt(uint32_t mask)
{
  /* ebm0 documentation: ebm0_pcie_a.pdf */

  epcihandle_t bar0_handle;
  uint32_t x;

  bar0_handle = epci_open("10ee:eb01", NULL, 0);
  if (bar0_handle == EPCI_BAD_HANDLE)
  {
    EAIO_PERROR();
    return -1;
  }

  /* control register 0 */
  /* ebone slave interrupt enable (bit 9) */
  /* global interrupt enable (bit 31) */
  epci_rd32_reg(bar0_handle, 0x0, &x);
  x = (x & ~(1 << 9)) | mask;
  epci_wr32_reg(bar0_handle, 0x0, x);

  /* which slave triggers an interrupt can be known */
  /* using status register 1 (offset 0x14) */

  epci_close(bar0_handle);

  return 0;
}

static int enable_ebone_slave_interrupt(void)
{
  return set_ebone_slave_interrupt((1 << 31) | (1 << 9));
}

static int disable_ebone_slave_interrupt(void)
{
  return set_ebone_slave_interrupt(0);
}


/* thread entry point and related routines */

static eaio_thread_t g_eaio_thread;

static int setup_aio_read(eaio_handle_t* aio)
{
  /* setup the next read transfer. */

  /* read at most the backing buffer available size or */
  /* the oneshot remaining size, whichever the smallest. */

  const size_t rpos = (size_t)atomic_read(&aio->rpos);
  const size_t wpos = (size_t)atomic_read(&aio->wpos);

  if (rpos > wpos) aio->setup_size = rpos - wpos;
  else aio->setup_size = ebuf_get_size(aio->buf) - wpos;

  if (aio->flags & EAIO_FLAG_ONESHOT)
  {
    /* oneshot mode: read at most the remaining size */
    if (aio->setup_size > aio->oneshot_size)
      aio->setup_size = aio->oneshot_size;
  }

  if (aio->setup_read_fn(aio->handle, aio->buf, wpos, &aio->setup_size))
  {
    EAIO_PERROR();
    return -1;
  }

  return 0;
}

static int start_aio
(eaio_thread_t* thr, eaio_handle_t* aio, unsigned int do_reset)
{
  /* start an aio, setup irq context, setup first read transfer */

  if (aio->flags & EAIO_FLAG_REALTIME)
  {
    if ((thr->realtime_refn == 0) && set_realtime_sched())
    {
      EAIO_PERROR();
      goto on_error_0;
    }
    ++thr->realtime_refn;
  }

  if (uirq_set_mask(&thr->uirq, thr->uirq_all_mask | aio->irq_mask, 1))
  {
    EAIO_PERROR();
    goto on_error_1;
  }

  /* FIXME: enable slave interrupt if this is not the dma */
  /* FIXME: should not assume anything about the dma */
  if (aio->irq_mask & ~(1 << 0)) enable_ebone_slave_interrupt();

  thr->uirq_all_mask |= aio->irq_mask;
  thr->uirq_pos_to_aio[aio->irq_pos] = aio;

  if (do_reset && aio->reset_fn(aio->handle))
  {
    EAIO_PERROR();
    goto on_error_2;
  }

  if (aio->start_fn(aio->handle))
  {
    EAIO_PERROR();
    goto on_error_2;
  }

  atomic_write(&aio->state, EAIO_STATE_STARTED);

  if (setup_aio_read(aio))
  {
    EAIO_PERROR();
    goto on_error_3;
  }

  return 0;

 on_error_3:
  aio->stop_fn(aio->handle);
  atomic_write(&aio->state, EAIO_STATE_STOPPED);
 on_error_2:
  thr->uirq_all_mask &= ~aio->irq_mask;
  uirq_set_mask(&thr->uirq, thr->uirq_all_mask, 1);
  thr->uirq_pos_to_aio[aio->irq_pos] = NULL;
 on_error_1:
  if (aio->flags & EAIO_FLAG_REALTIME)
  {
    if ((--thr->realtime_refn) == 0) set_default_sched();
  }
 on_error_0:
  return -1;
}

static void stop_aio(eaio_thread_t* thr, eaio_handle_t* aio, uint32_t err)
{
  EAIO_ASSUME(atomic_read(&aio->state) == EAIO_STATE_STARTED);

  aio->stop_fn(aio->handle);

  thr->uirq_all_mask &= ~aio->irq_mask;

  /* FIXME: disable slave interrupt if this is the last slave */
  /* FIXME: should not assume anything about the dma */
  if ((thr->uirq_all_mask & ~(1 << 0)) == 0) disable_ebone_slave_interrupt();

  uirq_set_mask(&thr->uirq, thr->uirq_all_mask, 1);
  thr->uirq_pos_to_aio[aio->irq_pos] = NULL;

  if (aio->flags & EAIO_FLAG_REALTIME)
  {
    if ((--thr->realtime_refn) == 0) set_default_sched();
  }

  atomic_write(&aio->err, err);

  /* write aio->state last to commit */
  atomic_write(&aio->state, EAIO_STATE_STOPPED);
}

static const uint8_t eaio_compl_token = 0x00;
static const uint8_t eaio_join_token = 0xff;

static void* eaio_thread_entry(void* arg)
{
  eaio_thread_t* const thr = arg;
  async_msg_t msg;
  fd_set rds;
  int nfd;
  struct timeval* tmvp;
  struct timeval tmv;
  size_t osize;
  size_t rpos;
  size_t wpos;
  size_t i;
  uint32_t compl_err;
  uint32_t uirq_tmp_mask;
  size_t uirq_set_pos;

  if (uirq_init_lib())
  {
    EAIO_PERROR();
    goto on_error_0;
  }

  if (uirq_open(&thr->uirq))
  {
    EAIO_PERROR();
    goto on_error_1;
  }

  thr->uirq_fd = uirq_get_fd(&thr->uirq);
  for (i = 0; i != EAIO_UIRQ_NPOS; ++i) thr->uirq_pos_to_aio[i] = NULL;
  thr->uirq_all_mask = 0;
  thr->realtime_refn = 0;

  /* signal creation completion */
  write(thr->wait_pipe[1], &eaio_compl_token, 1);

  while (1)
  {
    /* fill read set with mbox and uirq */

    FD_ZERO(&rds);
    FD_SET(thr->post_pipe[0], &rds);
    nfd = thr->post_pipe[0] + 1;

    if (thr->uirq_all_mask)
    {
      /* global timeout for missed interrupts */

      tmv.tv_sec = 1;
      tmv.tv_usec = 0;
      tmvp = &tmv;

      FD_SET(thr->uirq_fd, &rds);
      if (thr->uirq_fd >= nfd) nfd = thr->uirq_fd + 1;
    }
    else
    {
      tmvp = NULL;
    }

    nfd = select(nfd, &rds, NULL, NULL, tmvp);
    if (nfd < 0)
    {
      EAIO_PERROR();
      continue ;
    }
    else if (nfd == 0)
    {
      /* timeout, check for all aios */
      uirq_tmp_mask = thr->uirq_all_mask;
      goto do_uirqs;
    }
    else if (FD_ISSET(thr->uirq_fd, &rds))
    {
      /* ebone slave irq */

      if (uirq_poll(&thr->uirq, &uirq_tmp_mask))
      {
   EAIO_PERROR();
   continue ;
      }

      /* mask not wanted interrupts */

      uirq_tmp_mask &= thr->uirq_all_mask;

      /* iterate over all uirq_tmp_mask bits set */
    do_uirqs:
      for (uirq_set_pos = 0; uirq_tmp_mask; ++uirq_set_pos)
      {
   const uint32_t uirq_set_mask = 1 << uirq_set_pos;
   eaio_handle_t* const aio = thr->uirq_pos_to_aio[uirq_set_pos];

   if ((uirq_set_mask & uirq_tmp_mask) == 0) continue ;

   uirq_tmp_mask &= ~uirq_set_mask;

   /* complete the read operation */

   wpos = atomic_read(&aio->wpos);

   if (aio->compl_read_fn(aio->handle, aio->buf, wpos, &aio->setup_size))
   {
     EAIO_PERROR();
     stop_aio(thr, aio, EAIO_ERR_FAILURE);
     continue ;
   }

   /* update wpos */

   wpos += aio->setup_size;
   if (wpos == ebuf_get_size(aio->buf)) wpos = 0;
   atomic_write(&aio->wpos, wpos);

   /* detect overflow condition */

   if (wpos == atomic_read(&aio->rpos))
        {
     stop_aio(thr, aio, EAIO_ERR_OVERFLOW);
     continue ;
   }

   if (aio->flags & EAIO_FLAG_ONESHOT)
   {
     aio->oneshot_size -= aio->setup_size;
     if (aio->oneshot_size == 0)
     {
       /* oneshot aio completed, signal runtime */
       write(thr->wait_pipe[1], &eaio_compl_token, 1);
       stop_aio(thr, aio, EAIO_ERR_SUCCESS);
       continue ;
     }
   }

   if (aio->flags & EAIO_FLAG_THRESHOLD)
   {
     /* signal the runtime threshold is reached */
     rpos = atomic_read(&aio->rpos);
     if (rpos <= wpos) osize = wpos - rpos;
     else osize = ebuf_get_size(aio->buf) - rpos + wpos;
     if (osize >= aio->thresh)
     {
       write(thr->wait_pipe[1], &eaio_compl_token, 1);
     }
   }

   /* setup next transfer */

   if (setup_aio_read(aio))
   {
     EAIO_PERROR();
     stop_aio(thr, aio, EAIO_ERR_FAILURE);
     continue ;
   }
      }
    }
    else if (FD_ISSET(thr->post_pipe[0], &rds))
    {
      /* new message */

      const ssize_t nread = read(thr->post_pipe[0], &msg, sizeof(async_msg_t));
      if (nread != sizeof(async_msg_t))
      {
         EAIO_PERROR();
         msg.op = OP_NONE;
      }

      switch (msg.op)
      {
         case OP_NONE:
           break ;

         case OP_START:
         {
           eaio_handle_t* const aio = msg.start_stop.aio;
           const unsigned int do_reset = msg.start.do_reset;

           compl_err = (uint32_t)-1;

           if (atomic_read(&aio->state) != EAIO_STATE_STOPPED)
           {
             EAIO_PERROR();
             goto on_msg_complete;
           }

           if (start_aio(thr, aio, do_reset))
           {
             EAIO_PERROR();
             goto on_msg_complete;
           }

           compl_err = 0;

           goto on_msg_complete;
           break ;
         }

         case OP_STOP:
         {
           eaio_handle_t* const aio = msg.start_stop.aio;

           compl_err = (uint32_t)-1;

           if (atomic_read(&aio->state) != EAIO_STATE_STARTED)
           {
             EAIO_PERROR();
             goto on_msg_complete;
           }

           stop_aio(thr, aio, EAIO_ERR_SUCCESS);
           compl_err = 0;

 on_msg_complete:
           complete_start_stop_msg(thr, &msg, compl_err);
           break ;
         }

         case OP_JOIN:
         {
           goto on_join;
           break ;
         }

         default:
         {
           EAIO_PERROR();
           break ;
         }
      } /* switch(msg.op) */
    }
  }

 on_join:
 on_error_1:
  uirq_close(&thr->uirq);
 on_error_0:
  uirq_fini_lib();

  /* signal sequential in case of error */
  write(thr->wait_pipe[1], &eaio_join_token, 1);

  return NULL;
}


static int eaio_thread_create(eaio_thread_t* thr)
{
  struct timeval tm;
  fd_set rds;
  uint8_t x;

  if (pipe(thr->wait_pipe))
  {
    EAIO_PERROR();
    goto on_error_0;
  }

  if (pipe(thr->post_pipe))
  {
    EAIO_PERROR();
    goto on_error_1;
  }

  /* asynchronous messaging */
  if (pthread_mutex_init(&thr->post_lock, NULL))
  {
    EAIO_PERROR();
    goto on_error_2;
  }

  if (pthread_mutex_init(&thr->compl_lock, NULL))
  {
    EAIO_PERROR();
    goto on_error_3;
  }

  if (pthread_cond_init(&thr->compl_cond, NULL))
  {
    EAIO_PERROR();
    goto on_error_4;
  }

  if (pthread_create(&thr->thread, NULL, eaio_thread_entry, thr))
  {
    EAIO_PERROR();
    goto on_error_5;
  }

  tm.tv_sec = 1;
  tm.tv_usec = 0;

  FD_ZERO(&rds);
  FD_SET(thr->wait_pipe[0], &rds);

  if (select(thr->wait_pipe[0] + 1, &rds, NULL, NULL, &tm) != 1)
  {
    EAIO_PERROR();
    goto on_error_6;
  }

  read(thr->wait_pipe[0], &x, 1);
  if (x != eaio_compl_token)
  {
    EAIO_PERROR();
    goto on_error_6;
  }

  return 0;

 on_error_6:
  post_join_msg(thr);
  pthread_join(thr->thread, NULL);
 on_error_5:
  pthread_cond_destroy(&thr->compl_cond);
 on_error_4:
  pthread_mutex_destroy(&thr->compl_lock);
 on_error_3:
  pthread_mutex_destroy(&thr->post_lock);
 on_error_2:
  close(thr->post_pipe[0]);
  close(thr->post_pipe[1]);
 on_error_1:
  close(thr->wait_pipe[0]);
  close(thr->wait_pipe[1]);
 on_error_0:
  return -1;
}


static int eaio_thread_join(eaio_thread_t* thr)
{
  if (post_join_msg(thr))
  {
    EAIO_PERROR();
    return -1;
  }

  pthread_join(thr->thread, NULL);

  close(thr->post_pipe[0]);
  close(thr->post_pipe[1]);

  close(thr->wait_pipe[0]);
  close(thr->wait_pipe[1]);

  pthread_cond_destroy(&thr->compl_cond);
  pthread_mutex_destroy(&thr->compl_lock);
  pthread_mutex_destroy(&thr->post_lock);

  return 0;
}


/* exported */


int eaio_init_lib(void)
{
  uirq_init(&g_eaio_thread.uirq, "10ee:eb01");

  if (eaio_thread_create(&g_eaio_thread))
  {
    EAIO_PERROR();
    goto on_error_0;
  }

  return 0;

 on_error_0:
  return -1;
}


void eaio_fini_lib(void)
{
  eaio_thread_join(&g_eaio_thread);
}


int eaio_get_wait_fd(void)
{
  /* return the fd that has to be waited on to know about io event */
  return g_eaio_thread.wait_pipe[0];
}


void eaio_init_desc(eaio_desc_t* desc)
{
  desc->flags = 0;
}


void eaio_open(eaio_handle_t* aio)
{
  atomic_write(&aio->state, EAIO_STATE_STOPPED);
}


void eaio_close(eaio_handle_t* aio)
{
  EAIO_ASSUME(atomic_read(&aio->state) == EAIO_STATE_STOPPED);
}


int eaio_start(eaio_handle_t* aio, const eaio_desc_t* desc)
{
  EAIO_ASSUME(atomic_read(&aio->state) == EAIO_STATE_STOPPED);

  aio->flags = desc->flags;
  aio->thresh = desc->thresh;
  aio->tmout = desc->tmout;
  atomic_write(&aio->wpos, 0);
  atomic_write(&aio->rpos, 0);
  atomic_write(&aio->err, EAIO_ERR_SUCCESS);
  aio->oneshot_size = desc->oneshot_size;
  aio->buf = desc->buf;
  aio->irq_mask = desc->mirq;
  aio->irq_pos = mask_to_pos(desc->mirq);
  aio->handle = desc->handle;
  aio->reset_fn = desc->reset_fn;
  aio->start_fn = desc->start_fn;
  aio->stop_fn = desc->stop_fn;
  aio->setup_read_fn = desc->setup_read_fn;
  aio->compl_read_fn = desc->compl_read_fn;

  /* there will be an error if aio already started */
  if (post_start_msg(&g_eaio_thread, aio, 1))
  {
    EAIO_PERROR();
    return -1;
  }

  return 0;
}


int eaio_stop(eaio_handle_t* aio)
{
  EAIO_ASSUME(atomic_read(&aio->state) == EAIO_STATE_STARTED);

  /* there will be an error if aio not started */
  if (post_stop_msg(&g_eaio_thread, aio))
  {
    EAIO_PERROR();
    return -1;
  }

  return 0;
}


int eaio_restart(eaio_handle_t* aio)
{
  /* restart after the aio has been stopped due to overflow */
  /* it keeps the state of the aio */

  EAIO_ASSUME(atomic_read(&aio->state) == EAIO_STATE_STOPPED);

  /* there will be an error if aio already started */
  if (post_start_msg(&g_eaio_thread, aio, 0))
  {
    EAIO_PERROR();
    return -1;
  }

  return 0;
}


int eaio_read_oneshot(eaio_desc_t* desc, ebuf_handle_t* buf, size_t size)
{
  /* single shot transfer of size bytes from current dma source offset */
  /* the description must be initialized from the upper layer (ie. dma or */
  /* fifo) and is modified by this routine. the buffer must be allocated */
  /* from the caller. */

  const int rfd = eaio_get_wait_fd();
  eaio_handle_t aio;
  uint8_t x;
  fd_set rds;
  int err = -1;

  EAIO_ASSUME(size <= ebuf_get_size(buf));

  eaio_open(&aio);

  desc->flags |= EAIO_FLAG_ONESHOT;
  desc->oneshot_size = size;
  desc->buf = buf;

  if (eaio_start(&aio, desc))
  {
    EAIO_PERROR();
    goto on_error_0;
  }

  while (1)
  {
    FD_ZERO(&rds);
    FD_SET(rfd, &rds);
    select(rfd + 1, &rds, NULL, NULL, NULL);
    read(rfd, &x, 1);

    if (atomic_read(&aio.state) == EAIO_STATE_STOPPED)
    {
      break ;
    }
  }

  if (aio.err)
  {
    EAIO_PERROR();
    goto on_error_1;
  }

  err = 0;

 on_error_1:
  eaio_close(&aio);
 on_error_0:
  return err;
}


/* asynchronous data accessors */

int get_contiguous_data(eaio_handle_t* aio, void** data, size_t* n)
{
  /* since the data is managed as a ring buffer, not all */
  /* the data may be contiguous. as the caller wants contiguous data, */
  /* the returned size may be less than the occupancy and 2 calls might. */
  /* be needed. refer to efifo_copy_async_data. */

  const size_t buf_size = ebuf_get_size(aio->buf);
  uint8_t* const buf_data = ebuf_get_data(aio->buf);
  const size_t rpos = (size_t)atomic_read(&aio->rpos);

  /* compute *n so to get only contiguous data */
  *n = eaio_get_occupancy(aio);
  if ((rpos + *n) > buf_size) *n = buf_size - rpos;

  *data = buf_data + rpos;

  return 0;
}

static inline unsigned int is_overflow(eaio_handle_t* aio)
{
  if (eaio_get_state(aio) == EAIO_STATE_STOPPED)
  {
    return eaio_get_err(aio) == EAIO_ERR_OVERFLOW;
  }

  return 0;
}

int eaio_get_data(eaio_handle_t* aio, void** data, size_t* n)
{
  /* get contiguous data, taking care the overflow condition */

  /* the idea is that an overflow occurs when rpos == wpos */
  /* this is how the writer detects the condition and stop the */
  /* aio. for the reader (ie. this function), rpos == wpos */
  /* may mean an empty or a full buffer (ie overflow condition). */
  /* to detect the later case, the reader first gets the data  */
  /* size. if the size is zero, then it detects the overflow */
  /* condition by checking the aio state, and must recompute */
  /* the contiguous data size. operations must follow this */
  /* order for correctness. */

  /* later on, the reader must restart the aio if an overflow */
  /* was detected. it is done in the seek_data function. if */

  int err;

  aio->is_overflow = 0;

  err = get_contiguous_data(aio, data, n);

  if (err || *n) return err;

  if ((*n == 0) && is_overflow(aio))
  {
    const size_t rpos = atomic_read(&aio->rpos);
    *data = ebuf_get_data(aio->buf) + rpos;
    *n = ebuf_get_size(aio->buf) - rpos;
    aio->is_overflow = 1;
  }

  return 0;
}

int eaio_seek_data(eaio_handle_t* aio, size_t size)
{
  /* update fifo->async_rpos */

  size_t new_rpos;
  size_t buf_size;

  if (aio->is_overflow)
  {
    atomic_write(&aio->rpos, 0);
    return eaio_restart(aio);
  }

  new_rpos = (size_t)atomic_read(&aio->rpos) + size;
  buf_size = ebuf_get_size(aio->buf);
  if (new_rpos >= buf_size) new_rpos -= buf_size;
  atomic_write(&aio->rpos, (uint32_t)new_rpos);

  if (is_overflow(aio))
  {
    /* an overflow occured somewhere in between and this seek_data */
    /* makes the overflow condition no longer valid. the next get_data */
    /* would thus not see the condition, and not restart the aio. thus */
    /* we restart the aio here */

    if (atomic_read(&aio->rpos) != atomic_read(&aio->wpos))
    {
      return eaio_restart(aio);
    }
  }

  return 0;
}

size_t eaio_get_occupancy(eaio_handle_t* aio)
{
  const size_t rpos = (size_t)atomic_read(&aio->rpos);
  const size_t wpos = (size_t)atomic_read(&aio->wpos);
  const size_t size = ebuf_get_size(aio->buf);

  if (rpos <= wpos) return wpos - rpos;
  return wpos + size - rpos;
}

uint32_t eaio_get_err(eaio_handle_t* aio)
{
  return atomic_read(&aio->err);
}

uint32_t eaio_get_state(eaio_handle_t* aio)
{
  return atomic_read(&aio->state);
}
