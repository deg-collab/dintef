#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include "libepci.h"
#include "libedma.h"
#include "libpmem.h"
#include "libebuf.h"
#include "libeaio.h"
#include "libuirq.h"


int libedma_debug = 0;
#include <stdio.h>

#define EDMA_PRINTF(...)             \
do {                                 \
  if(libedma_debug) {                \
    printf("[LIBEDMA] "__VA_ARGS__); \
  }                                  \
} while (0)

#define EDMA_PERROR()                \
do {                                 \
  printf("[!] %s,%d\n", __FILE__, __LINE__); \
} while (0)


/* endpoint master0 access, ebm0_pcie_a.pdf */

#define EBM0_CTRL0 0x00000000
#define EBM0_CTRL1 0x00000004
#define EBM0_CTRL2 0x00000008
#define EBM0_CTRL3 0x0000000c

#define EBM0_STAT0 0x00000010
#define EBM0_STAT1 0x00000014

#define EBM0_ERR0 0x00000020
#define EBM0_ERR1 0x00000024

#define EBM0_DBG0 0x00000030
#define EBM0_DBG1 0x00000034
#define EBM0_DBG2 0x00000038
#define EBM0_DBG3 0x0000003c

static void ebm0_write_ctrl0(epcihandle_t h, uint32_t x)
{
  epci_wr32_reg(h, EBM0_CTRL0, x);
}

static uint32_t ebm0_read_ctrl0(epcihandle_t h)
{
  uint32_t x;
  epci_rd32_reg(h, EBM0_CTRL0, &x);
  return x;
}

__attribute__((unused))
static void ebm0_write_stat0(epcihandle_t h, uint32_t x)
{
  epci_wr32_reg(h, EBM0_STAT0, x);
}

static uint32_t ebm0_read_stat0(epcihandle_t h)
{
  uint32_t x;
  epci_rd32_reg(h, EBM0_STAT0, &x);
  return x;
}

__attribute__((unused))
static void ebm0_write_stat1(epcihandle_t h, uint32_t x)
{
  epci_wr32_reg(h, EBM0_STAT1, x);
}

__attribute__((unused))
static uint32_t ebm0_read_stat1(epcihandle_t h)
{
  uint32_t x;
  epci_rd32_reg(h, EBM0_STAT1, &x);
  return x;
}

static void ebm0_enable_ebft_interrupt(epcihandle_t h)
{
  /* control register 0 */
  /* ebft interrupt enable (bit 8) */
  /* global interrupt enable (bit 31) */

  uint32_t x;

  x = ebm0_read_ctrl0(h);
  x |= (1 << 31) | (1 << 8);
  ebm0_write_ctrl0(h, x);
}

static void ebm0_disable_ebft_interrupt(epcihandle_t h)
{
  /* control register 0 */
  /* ebft interrupt enable (bit 8) */

  uint32_t x;

  x = ebm0_read_ctrl0(h);
  x &= ~(1 << 8);
  ebm0_write_ctrl0(h, x);
}


/* ebft access (ebm_ebft.pdf) */

static const uint32_t ebft_irq_mask = 1 << 0;

#define EBFT_D0_SRCE 0x00000000
#define EBFT_D0_SADRS 0x00000004
#define EBFT_D0_DLSW 0x0000000c
#define EBFT_D0_DMSW 0x00000010
#define EBFT_D0_CTRL 0x00000014
#define EBFT_D1_CTRL 0x0000002c
#define EBFT_PSIZE 0x00000030
#define EBFT_GSCR 0x00000034
#define EBFT_D0_STAT 0x00000038
#define EBFT_D1_STAT 0x0000003c

static void ebft_write_reg(edma_handle_t* h, size_t o, uint32_t x)
{
  epci_wr32_reg(h->epci_handle, h->reg_off + o, x);
}

static uint32_t ebft_read_reg(edma_handle_t* h, size_t o)
{
  uint32_t x;
  epci_rd32_reg(h->epci_handle, h->reg_off + o, &x);
  return x;
}

static void ebft_write_d0_srce(edma_handle_t* h, uint32_t x)
{
  ebft_write_reg(h, EBFT_D0_SRCE, x);
}

__attribute__((unused))
static uint32_t ebft_read_d0_srce(edma_handle_t* h)
{
  return ebft_read_reg(h, EBFT_D0_SRCE);
}

static void ebft_write_d0_sadrs(edma_handle_t* h, uint32_t x)
{
  ebft_write_reg(h, EBFT_D0_SADRS, x);
}

__attribute__((unused))
static uint32_t ebft_read_d0_sadrs(edma_handle_t* h)
{
  return ebft_read_reg(h, EBFT_D0_SADRS);
}

static void ebft_write_d0_dlsw(edma_handle_t* h, uint32_t x)
{
  ebft_write_reg(h, EBFT_D0_DLSW, x);
}

__attribute__((unused))
static uint32_t ebft_read_d0_dlsw(edma_handle_t* h)
{
  return ebft_read_reg(h, EBFT_D0_DLSW);
}

static void ebft_write_d0_dmsw(edma_handle_t* h, uint32_t x)
{
  ebft_write_reg(h, EBFT_D0_DMSW, x);
}

__attribute__((unused))
static uint32_t ebft_read_d0_dmsw(edma_handle_t* h)
{
  return ebft_read_reg(h, EBFT_D0_DMSW);
}

static void ebft_write_d0_ctrl(edma_handle_t* h, uint32_t x)
{
  ebft_write_reg(h, EBFT_D0_CTRL, x);
}

static void ebft_write_d1_ctrl(edma_handle_t* h, uint32_t x)
{
  ebft_write_reg(h, EBFT_D1_CTRL, x);
}

__attribute__((unused))
static uint32_t ebft_read_d0_ctrl(edma_handle_t* h)
{
  return ebft_read_reg(h, EBFT_D0_CTRL);
}

static void ebft_write_psize(edma_handle_t* h, uint32_t x)
{
  ebft_write_reg(h, EBFT_PSIZE, x);
}

__attribute__((unused))
static uint32_t ebft_read_psize(edma_handle_t* h)
{
  return ebft_read_reg(h, EBFT_PSIZE);
}

__attribute__((unused))
static void ebft_write_gscr(edma_handle_t* h, uint32_t x)
{
  ebft_write_reg(h, EBFT_GSCR, x);
}

__attribute__((unused))
static uint32_t ebft_read_gscr(edma_handle_t* h)
{
  return ebft_read_reg(h, EBFT_GSCR);
}

__attribute__((unused))
static void ebft_write_d0_stat(edma_handle_t* h, uint32_t x)
{
  ebft_write_reg(h, EBFT_D0_STAT, x);
}

static uint32_t ebft_read_d0_stat(edma_handle_t* h)
{
  return ebft_read_reg(h, EBFT_D0_STAT);
}

__attribute__((unused))
static void ebft_write_d1_stat(edma_handle_t* h, uint32_t x)
{
  ebft_write_reg(h, EBFT_D1_STAT, x);
}

__attribute__((unused))
static uint32_t ebft_read_d1_stat(edma_handle_t* h)
{
  return ebft_read_reg(h, EBFT_D1_STAT);
}


/* eaio routines */

static inline size_t mod2(size_t p, size_t q)
{
  return p & (q - (size_t)1);
}

static int do_eaio_reset(void* p)
{
  edma_handle_t* const dma = p;

  dma->aio_soff = dma->desc_soff;
  dma->aio_ssize = dma->desc_ssize;
  dma->aio_spos = dma->desc_soff;
  dma->aio_err = 0;

  return 0;
}

static int do_eaio_start(void* p)
{
  edma_handle_t* const dma = p;

  ebm0_enable_ebft_interrupt(dma->bar0_handle);
  ebft_read_d0_stat(dma);

  return 0;
}

static int do_eaio_stop(void* p)
{
  edma_handle_t* const dma = p;

  ebm0_disable_ebft_interrupt(dma->bar0_handle);

  return 0;
}

static int do_eaio_setup_read
(void* p, ebuf_handle_t* buf, size_t boff, size_t* n)
{
  /* setup the dma for a read of n bytes. */
  /* the actual read is done in eaio_irq. */

  /* buf the backing buffer */
  /* boff the buffer offset, in bytes */
  /* n the operation size, in bytes */

  edma_handle_t* const dma = p;

  const size_t page_pos = boff / pmem_page_size;
  const size_t poff = mod2(boff, pmem_page_size);
  uintptr_t paddr;
  size_t size;
  uint32_t x;

  /* transfer less than n bytes, no more than one page, do */
  /* not cross page boundary and do not overflow source */

  size = pmem_page_size - poff;

  if (size > *n) size = *n;

  if ((dma->aio_spos + size) > dma->aio_ssize)
    size = dma->aio_ssize - dma->aio_spos;

  *n = size;

  /* setup descriptor 0 */

  if (dma->desc_flags & EDMA_FLAG_MIF)
  {
    /* segment 1 assumed, overlapping enabled */
    static const uint32_t fifo_dw_count = 256;
    x = (fifo_dw_count << 16) | dma->desc_ebs_off;
    ebft_write_d0_srce(dma, x);
    ebft_write_d0_sadrs(dma, dma->aio_spos);
  }
  else if (dma->desc_flags & EDMA_FLAG_FIFO)
  {
    static const uint32_t fifo_dw_count = 2048;
    x = (fifo_dw_count << 16) | dma->desc_ebs_off;
    ebft_write_d0_srce(dma, x);
    ebft_write_d0_sadrs(dma, 0);
  }
  else
  {
    /* FPGA internal BRAM case */
    x = (dma->desc_ebs_seg << 28) | dma->desc_ebs_off;
    ebft_write_d0_srce(dma, x);
    ebft_write_d0_sadrs(dma, dma->aio_spos);
  }

  paddr = buf->paddr[page_pos] + poff;
  ebft_write_d0_dlsw(dma, paddr & ((uint32_t)-1));
  if (sizeof(void*) == sizeof(uint32_t)) x = 0;
  else x = ((uint64_t)paddr) >> 32;
  ebft_write_d0_dmsw(dma, x);

  /* descriptor 1 unused */

  ebft_write_d1_ctrl(dma, 0);

  if (dma->desc_flags & EDMA_FLAG_IRQ)
  {
    /* enable irq and flush any pending one */
    ebm0_enable_ebft_interrupt(dma->bar0_handle);
    ebft_read_d0_stat(dma);
  }

  /* synchronize the buffer */

  ebuf_sync(buf, boff, size);

  /* start the transfer */

  x = ((uint32_t)size) | (1 << 31);
  if (dma->desc_flags & EDMA_FLAG_MIF) x |= 3 << 26;
  if (dma->desc_flags & EDMA_FLAG_FIFO) x |= 2 << 26;
  ebft_write_d0_ctrl(dma, x);

  return 0;
}

static int do_eaio_compl_read
(void* p, ebuf_handle_t* buf, size_t boff, size_t* n)
{
  /* complete a previously setup read operation. */

  /* buf the backing buffer */
  /* boff the buffer offset, in bytes */
  /* n the operation size, inout argument, in bytes */

  edma_handle_t* const dma = p;
  uint32_t x;

  x = ebft_read_d0_stat(dma);
  if (x & (1 << 31))
  {
    /* still running, not completed */
    *n = 0;
    return 0;
  }

  if ((x >> 27) & 0xf)
  {
    /* completion error */
    dma->aio_err = x;
    *n = 0;
    return -1;
  }

#if 0
  /* retrieve the transfer size, in bytes */
  /* note: note having transfered the size initially requested */
  /* is actually an error condition. no need to check it here. */
  size_t size;
  size = (size_t)(x & ((1 << 24) - 1)) * dma->ebft_dwidth;
  if (size != *n)
  {
    dma->aio_err = (uint32_t)-1;
    *n = 0;
    return -1;
  }
#endif

  /* update the internal source pointer aio_spos */
  /* manage the source as a ring buffer */

  dma->aio_spos += *n;
  if (dma->aio_spos == dma->aio_ssize) dma->aio_spos = dma->aio_soff;

  return 0;
}

void edma_init_aio_desc(edma_handle_t* dma, eaio_desc_t* desc)
{
  eaio_init_desc(desc);
  desc->flags |= EAIO_FLAG_THRESHOLD;
  desc->thresh = 64 * 1024;
  desc->flags |= EAIO_FLAG_TIMEOUT;
  desc->tmout = 10000;
  desc->mirq = ebft_irq_mask;
  desc->handle = dma;
  desc->reset_fn = do_eaio_reset;
  desc->start_fn = do_eaio_start;
  desc->stop_fn = do_eaio_stop;
  desc->setup_read_fn = do_eaio_setup_read;
  desc->compl_read_fn = do_eaio_compl_read;
}

int edma_alloc_buf(edma_handle_t* dma, ebuf_handle_t* buf, size_t size)
{
  return ebuf_alloc(buf, size, EBUF_FLAG_PHYSICAL);
}


/* global lib constructors. call once per process. */

int edma_init_lib(void)
{
  return 0;
}

int edma_fini_lib(void)
{
  return 0;
}

int edma_log_lib(int level)
{
  if(level >=0) {
    libedma_debug = level;
    return 0;
  }
  return -1;
}


/* init open close */

void edma_init_desc(edma_desc_t* desc)
{
  desc->flags = 0;
}

int edma_open(edma_handle_t* dma, const edma_desc_t* desc)
{
  static const uint32_t psize_lut[] =
    { 128, 256, 512, 1024, 2048, 4096, 128, 128 };

  uint32_t x;

  /* initialize edma handle */

  dma->epci_handle = epci_open(desc->pci_id, NULL, desc->pci_bar);
  if (dma->epci_handle == EPCI_BAD_HANDLE)
  {
    EDMA_PERROR();
    goto on_error_0;
  }

  dma->reg_off = desc->pci_off;

  dma->desc_flags = desc->flags;
  dma->desc_ebs_seg = desc->ebs_seg;
  dma->desc_ebs_off = desc->ebs_off;
  dma->desc_soff = desc->soff;
  dma->desc_ssize = desc->ssize;

#if defined(CONFIG_FREESCALE_IMX6)
  {
    /* enable bus mastering in command register */

    epcihandle_t conf = epci_open_config((char*)desc->pci_id, NULL);

    if (conf == EPCI_BAD_HANDLE)
    {
      EDMA_PERROR();
      goto on_error_1;
    }

    epci_rd_config(conf, 0x4, sizeof(uint32_t), &x);
    x |= 1 << 2;
    epci_wr_config(conf, 0x4, sizeof(uint32_t), &x);
    epci_rd_config(conf, 0x4, sizeof(uint32_t), &x);

    epci_close(conf);
  }
#endif /* CONFIG_FREESCALE_IMX6 */

  /* disable both dma descriptors */

  EDMA_PRINTF("edma_open(): disable both fast transmitters\n");
  ebft_write_d0_ctrl(dma, 0);
  ebft_write_d1_ctrl(dma, 0);

  /* ebm0 setup */
  EDMA_PRINTF("edma_open(): configure fast transmitter #0\n");

  /* open bar0 handle for ebm0 setup */

  dma->bar0_handle = epci_open(desc->pci_id, NULL, 0);
  if (dma->bar0_handle == EPCI_BAD_HANDLE)
  {
    EDMA_PERROR();
    goto on_error_1;
  }

  /* retrieve the payload size and init control register */
  /* set TLP 64 bits mode to host width */

  x = ebm0_read_stat0(dma->bar0_handle);
  x = psize_lut[(x >> 24) & 0x7];
  ebft_write_psize(dma, x);
  dma->pcie_payload_size = (size_t)x;
  EDMA_PRINTF("edma_open(): payload: %dBytes\n", dma->pcie_payload_size);

  x = (x / sizeof(uint32_t)) << 20;
  if (sizeof(void*) == sizeof(uint64_t)) x |= 1 << 19;
  else x &= ~(1 << 19);
  ebm0_write_ctrl0(dma->bar0_handle, ebm0_read_ctrl0(dma->bar0_handle) | x);

  /* retrieve ebft_dwidth, in bytes */

  x = ebm0_read_stat0(dma->bar0_handle);
  dma->ebft_dwidth = sizeof(uint32_t) << ((x >> 6) & 3);
  EDMA_PRINTF("edma_open(): dwidth : %dBytes\n", dma->ebft_dwidth);

  if (desc->flags & EDMA_FLAG_IRQ)
  {
    /* set uirq mask and enable EBONE irqs */
    EDMA_PRINTF("edma_open(): setup IRQ\n");

    if (uirq_set_mask(desc->uirq, ebft_irq_mask, 1))
    {
      EDMA_PERROR();
      goto on_error_1;
    }

    dma->uirq = desc->uirq;

    ebm0_enable_ebft_interrupt(dma->bar0_handle);
  }
  else
  {
    /* disable edma irqs */
    EDMA_PRINTF("edma_open(): disable IRQ\n");

    ebm0_disable_ebft_interrupt(dma->bar0_handle);
  }

  return 0;

 on_error_1:
  epci_close(dma->epci_handle);
 on_error_0:
  return -1;
}

int edma_close(edma_handle_t* dma)
{
  if (dma->desc_flags & EDMA_FLAG_IRQ)
  {
    ebm0_disable_ebft_interrupt(dma->bar0_handle);
    uirq_clear_mask(dma->uirq, ebft_irq_mask);
  }

  epci_close(dma->bar0_handle);
  epci_close(dma->epci_handle);

  return 0;
}


/* synchronous read operation */

typedef struct edma_io
{
  /* use emem_handle_t* when libemem available */
#define EDMA_FLAG_MIF (1 << 0)
#define EDMA_FLAG_BRAM (1 << 1)
#define EDMA_FLAG_FIFO (1 << 2)
#define EDMA_FLAG_IRQ (1 << 3)
  unsigned int flags;

  /* ebone slave address: (off << 28) | segment */
  uint32_t ebs_seg;
  uint32_t ebs_off;

  /* destination buffer */
  ebuf_handle_t* buf;

  /* source (ie. bram, ddr...) and dest (ie. buffer) offset. */
  size_t trans_soff;
  size_t trans_doff;

  /* transfer offset, from 0 to trans_size. updated at each page. */
  size_t trans_pos;

  /* size to tranfer. set at init time. */
  size_t trans_size;

  /* size being transfered in the current page */
  size_t step_size;

  /* error reported at completion, if any */
  uint32_t compl_err;

} edma_io_t;

static void start_page_io(edma_handle_t* dma, edma_io_t* io)
{
  const size_t soff = io->trans_pos + io->trans_soff;
  const size_t doff = io->trans_pos + io->trans_doff;
  const size_t page_pos = doff / pmem_page_size;
  const size_t poff = mod2(doff, pmem_page_size);
  uintptr_t paddr;
  size_t size;
  uint32_t x;

  /* page physical addr */

  paddr = io->buf->paddr[page_pos] + poff;
  EDMA_PRINTF("start_page_io(): phyaddr: 0x%08x\n", paddr);


  /* transfer no more than one page and do not cross page boundary */

  size = pmem_page_size - poff;
  if (size > (io->trans_size - io->trans_pos))
    size = io->trans_size - io->trans_pos;

  /* setup descriptor 0 */

  if (io->flags & EDMA_FLAG_MIF)
  {
    /* segment 1 assumed, overlapping enabled */
    static const uint32_t fifo_dw_count = 256;
    x = (fifo_dw_count << 16) | io->ebs_off;
    ebft_write_d0_srce(dma, x);
    ebft_write_d0_sadrs(dma, soff);
  }
  else if (dma->desc_flags & EDMA_FLAG_FIFO)
  {
    static const uint32_t fifo_dw_count = 2048;
    x = (fifo_dw_count << 16) | io->ebs_off;
    ebft_write_d0_srce(dma, x);
    ebft_write_d0_sadrs(dma, 0);
  }
  else
  {
    /* FPGA internal BRAM case */
    x = (io->ebs_seg << 28) | io->ebs_off;
    ebft_write_d0_srce(dma, x);
    ebft_write_d0_sadrs(dma, soff);
  }

  ebft_write_d0_dlsw(dma, paddr & ((uint32_t)-1));
  if (sizeof(void*) == sizeof(uint32_t)) x = 0;
  else x = ((uint64_t)paddr) >> 32;
  ebft_write_d0_dmsw(dma, x);
  /* useless: ebft_write_d0_ctrl(dma, 0); */

  /* descriptor 1 unused */

  ebft_write_d1_ctrl(dma, 0);

  if (io->flags & EDMA_FLAG_IRQ)
  {
    /* enable irq and flush any pending one */
    ebm0_enable_ebft_interrupt(dma->bar0_handle);
    ebft_read_d0_stat(dma);
  }

  /* synchronize the buffer */

  ebuf_sync(io->buf, doff, size);

  /* start the transfer */

  uint32_t y;
  y = ebft_read_d0_stat(dma);
  EDMA_PRINTF("start_page_io(): D0_STAT: 0x%08x before conf\n", y);
  //x = ((uint32_t)size) | (1 << 31);
  x = ((uint32_t)size);
  EDMA_PRINTF("start_page_io(): size:    %dBytes\n", size);
  if (io->flags & EDMA_FLAG_MIF) x |= 3 << 26;
  /* TODO: inconsistent with D0_CTRL doc, should be 2 << 36 */
  if (io->flags & EDMA_FLAG_FIFO) x |= 3 << 26;
  ebft_write_d0_ctrl(dma, x);
  y = ebft_read_d0_stat(dma);
  EDMA_PRINTF("start_page_io(): D0_STAT: 0x%08x before start\n", y);
  x |= (1 << 31);
  ebft_write_d0_ctrl(dma, x);
  y = ebft_read_d0_stat(dma);
  EDMA_PRINTF("start_page_io(): D0_STAT: 0x%08x after start\n", y);

  io->step_size = size;
}

static int start_io
(
 edma_handle_t* dma,
 edma_io_t* io,
 size_t soff,
 ebuf_handle_t* buf, size_t boff,
 size_t size
)
{
  /* when emem_handle_t available, set io->emem = emem and remove flags */
  io->flags = dma->desc_flags;
  io->ebs_seg = dma->desc_ebs_seg;
  io->ebs_off = dma->desc_ebs_off;

  io->buf = buf;

  io->trans_soff = soff;
  io->trans_doff = boff;
  io->trans_pos  = 0;
  io->trans_size = size;

  start_page_io(dma, io);

  return 0;
}

static int get_io_status(edma_handle_t* dma, edma_io_t* io)
{
  /* return -1 if not yet completed */
  /* otherwise, return 0 and update io compl info */

  uint32_t x;

  x = ebft_read_d0_stat(dma);

  if (x & (1 << 31)) return -1;

  io->compl_err = (x >> 27) & 0xf;

#if 0
  size_t n;

  io->compl_size = (size_t)(x & ((1 << 24) - 1)) * dma->ebft_dwidth;

  /* FIXME: the transfered count is in ebft_dwidth units */
  /* FIXME: which may be greater than the actual count */
  /* FIXME: we substract the remaining of unaligned sizes */
  n = mod2(io->trans_size, dma->ebft_dwidth);
  if (n) io->compl_size -= dma->ebft_dwidth - n;

  /* FIXME: refer to doc, known limitations section */
  n = mod2(io->trans_size, dma->pcie_payload_size);
  if (n && (n <= dma->ebft_dwidth)) io->compl_size -= dma->ebft_dwidth;
#endif

  return 0;
}

static int wait_io_poll(edma_handle_t* dma, edma_io_t* io, unsigned int ms)
{
  unsigned int us = ms * 1000;
  EDMA_PRINTF("wait_io_poll() : waiting...\n");
  do {
    usleep(1);
    us--;
  } while ((us > 0) && (get_io_status(dma, io) == -1));

  uint32_t x;
  x = ebft_read_d0_stat(dma);
  EDMA_PRINTF("wait_io_poll() : D0_STAT: 0x%08x us:%u\n", x, ((ms*1000)-us));

  return 0;
}

static int wait_io_irq(edma_handle_t* dma, edma_io_t* io, unsigned int ms)
{
  uint32_t ebm0_stat1;

  EDMA_PRINTF("wait_io_irq() : waiting...\n");
  while (1)
  {
    const int err = uirq_wait(dma->uirq, ms, &ebm0_stat1);
    EDMA_PRINTF("wait_io_irq() : got IRQ\n");

    if (err == 0)
    {
      if ((ebm0_stat1 & ebft_irq_mask) && (get_io_status(dma, io) == 0))
	return 0;
      /* else redo */
    }
    else if (err == -2)
    {
      /* timeout, but try to get io status (missed interrupt) */
      EDMA_PERROR();
      get_io_status(dma, io);
      return -1;
    }
    else
    {
      EDMA_PERROR();
      return -1;
    }

    /* otherwise, redo */
  }

  return -1;
}

static int wait_io(edma_handle_t* dma, edma_io_t* io, unsigned int ms)
{
  struct timeval tm[2];

  while (1)
  {
    if (ms != (unsigned int)-1) gettimeofday(&tm[0], NULL);

    /* wait the current page io */
    if (io->flags & EDMA_FLAG_IRQ)
    {
      if (wait_io_irq(dma, io, ms))
      {
	EDMA_PERROR();
	return -1;
      }
    }
    else
    {
      if (wait_io_poll(dma, io, ms))
      {
	EDMA_PERROR();
	return -1;
      }
    }

    if (io->compl_err)
    {
      EDMA_PERROR();
      return -1;
    }

    /* update pos, check for end of transfer */
    io->trans_pos += io->step_size;
    if (io->trans_pos == io->trans_size) break ;

    /* update timeout */
    if (ms != (unsigned int)-1)
    {
      struct timeval tm_diff;
      unsigned int diff_ms;

      gettimeofday(&tm[1], NULL);
      timersub(&tm[1], &tm[0], &tm_diff);

      diff_ms = tm_diff.tv_sec * 1000 + tm_diff.tv_usec / 1000;
      if (diff_ms >= ms)
      {
	EDMA_PERROR();
	return -1;
      }

      ms -= diff_ms;
    }

    start_page_io(dma, io);
  }

  return 0;
}

int edma_read
(edma_handle_t* dma, size_t soff, ebuf_handle_t* buf, size_t boff, size_t size)
{
  edma_io_t io;

  if (start_io(dma, &io, soff, buf, boff, size))
  {
    EDMA_PERROR();
    return -1;
  }

  if (wait_io(dma, &io, 5000))
  {
    EDMA_PERROR();
    return -1;
  }

  return 0;
}


/* temporary descriptor based interface */


/* EBFT registers */

#define EBFT_FS_CTRL (12 * sizeof(uint32_t))
#define EBFT_FSx_CSR (16 * sizeof(uint32_t))
#define EBFT_FS_STAT (30 * sizeof(uint32_t))
#define EBFT_FS_IRQS (31 * sizeof(uint32_t))
#define EBFT_FSx_SADRS (112 * sizeof(uint32_t))

/* descriptor format */

#define EBFT_FSx_YZ (64 * sizeof(uint32_t))

#define EBFT_FSx_SRCE (0 * sizeof(uint32_t))
/* <15:0>: segment, #1 assumed */

#define EBFT_FSx_DLSW (1 * sizeof(uint32_t))
#define EBFT_FSx_DMSW (2 * sizeof(uint32_t))

#define EBFT_FSx_CTRL (3 * sizeof(uint32_t))
/* <19:0>: destination buffer size, in words, mod payload size */
/* <26>  : 0 for MIF, 1 for virtual FIFO */
/* <27>  : 0 for true FIFO, 1 for virtual FIFO */
/* <29>  : FIFO reset request */
/* <30>  : 0 for IRQ on full, 1 for IRQ on half */
/* <31>  : 0 for skip, 1 for descriptor valid */

__attribute__((unused))
static uint32_t ebft_read_fs_stat(edma_handle_t* h)
{
  return ebft_read_reg(h, EBFT_FS_STAT);
}

static uint32_t ebft_read_fs_irqs(edma_handle_t* h)
{
  return ebft_read_reg(h, EBFT_FS_IRQS);
}

static void ebft_write_fs_irqs(edma_handle_t* h, uint32_t x)
{
  ebft_write_reg(h, EBFT_FS_IRQS, x);
}

__attribute__((unused))
static uint32_t ebft_read_fs_ctrl(edma_handle_t* h)
{
  return ebft_read_reg(h, EBFT_FS_CTRL);
}

static void ebft_write_fs_ctrl(edma_handle_t* h, uint32_t x)
{
  ebft_write_reg(h, EBFT_FS_CTRL, x);
}

static void ebft_write_fsx_csr(edma_handle_t* h, uint32_t did, uint32_t x)
{
  ebft_write_reg(h, EBFT_FSx_CSR + did * sizeof(uint32_t), x);
}

static uint32_t ebft_read_fsx_csr(edma_handle_t* h, uint32_t did)
{
  return ebft_read_reg(h, EBFT_FSx_CSR + did * sizeof(uint32_t));
}

static void ebft_write_fsx_yz(edma_handle_t* h, size_t off, uint32_t x)
{
  ebft_write_reg(h, EBFT_FSx_YZ + off, x);
}

static void ebft_write_fsx_srce(edma_handle_t* h, uint32_t did, uint32_t x)
{
  const size_t off = EBFT_FSx_SRCE + did * 4 * sizeof(uint32_t);
  ebft_write_fsx_yz(h, off, x);
}

static void ebft_write_fsx_dlsw(edma_handle_t* h, uint32_t did, uint32_t x)
{
  const size_t off = EBFT_FSx_DLSW + did * 4 * sizeof(uint32_t);
  ebft_write_fsx_yz(h, off, x);
}

static void ebft_write_fsx_dmsw(edma_handle_t* h, uint32_t did, uint32_t x)
{
  const size_t off = EBFT_FSx_DMSW + did * 4 * sizeof(uint32_t);
  ebft_write_fsx_yz(h, off, x);
}

static void ebft_write_fsx_ctrl(edma_handle_t* h, uint32_t did, uint32_t x)
{
  const size_t off = EBFT_FSx_CTRL + did * 4 * sizeof(uint32_t);
  ebft_write_fsx_yz(h, off, x);
}

int edma_xxx_start
(
 edma_handle_t* dma, uint32_t did, size_t ebs_off,
 uintptr_t paddr, size_t size,
 unsigned int enable_trigger
)
{
  /* initialize a descriptor */
  /* did the descriptor identifier */

  uint32_t hi;
  uint32_t x;

  /* source */

  ebft_write_fsx_srce(dma, did, (uint32_t)ebs_off);

  /* destination */

  ebft_write_fsx_dlsw(dma, did, paddr & ((uint32_t)-1));
  if (sizeof(void*) == sizeof(uint32_t)) x = 0;
  else x = ((uint64_t)paddr) >> 32;
  ebft_write_fsx_dmsw(dma, did, x);

  /* enable the descriptor */
  x = size / dma->ebft_dwidth;
  if (enable_trigger) x |= 1 << 24;
  ebft_write_fsx_ctrl(dma, did, (1 << 31) | (uint32_t)x);

  /* hi is the highest possible descriptor plus one */
  x = ebft_read_fs_ctrl(dma);
  hi = x & ((1 << 4) - 1);
  if ((did + 1) > hi) hi = did + 1;

  /* this is important, otherwise does not work */
  ebft_write_fs_ctrl(dma, ~(1 << 31));
  x = (1 << 31) | (hi << 0);
  ebft_write_fs_ctrl(dma, x);

  return 0;
}

int edma_xxx_restart
(
 edma_handle_t* dma, uint32_t did,
 uintptr_t paddr, size_t size,
 unsigned int enable_trigger
)
{
  /* restart a stopped descriptor */

  uint32_t x;

  ebft_write_fsx_dlsw(dma, did, paddr & ((uint32_t)-1));
  if (sizeof(void*) == sizeof(uint32_t)) x = 0;
  else x = ((uint64_t)paddr) >> 32;
  ebft_write_fsx_dmsw(dma, did, x);

  x = size / dma->ebft_dwidth;
  if (enable_trigger) x |= 1 << 24;
  ebft_write_fsx_ctrl(dma, did, (1 << 31) | (uint32_t)x);

  /* acknowledge IRQ */

  ebft_write_fsx_csr(dma, did, 1 << 31);

  return 0;
}

uint32_t edma_xxx_read_fsx_csr(edma_handle_t* dma, uint32_t did)
{
  return ebft_read_fsx_csr(dma, did);
}

uint32_t edma_xxx_read_fs_stat(edma_handle_t* dma)
{
  return ebft_read_fs_stat(dma);
}

uint32_t edma_xxx_read_fs_irqs(edma_handle_t* dma)
{
  return ebft_read_fs_irqs(dma);
}

void edma_xxx_write_fs_irqs(edma_handle_t* dma, uint32_t x)
{
  ebft_write_fs_irqs(dma, x);
}

void edma_xxx_enable_timer(edma_handle_t* dma, unsigned int ms)
{
  uint32_t x;

  x = ebft_read_fs_ctrl(dma);
  x &= ~(0xfff << 4);
  x |= (1 << 30) | ((uint32_t)ms << 4);
  ebft_write_fs_ctrl(dma, x);
}

void edma_xxx_disable_timer(edma_handle_t* dma)
{
  ebft_write_fs_ctrl(dma, ebft_read_fs_ctrl(dma) & ~(1 << 30));
}

size_t edma_xxx_get_occupancy(edma_handle_t* dma, uint32_t did)
{
  const uint32_t x = edma_xxx_read_fsx_csr(dma, did);
  return (size_t)(x & ((1 << 20) - 1)) * dma->ebft_dwidth;
}

unsigned int edma_xxx_is_any_irq(uint32_t fs_irqs, uint32_t did)
{
  /* return non zero on half full, full, or timer */
#define EDMA_XXX_TIMER_IRQ_MASK (1 << 31)
  const uint32_t mask = EDMA_XXX_TIMER_IRQ_MASK | (3 << (did * 2));
  return (unsigned int)(fs_irqs & mask);
}

unsigned int edma_xxx_is_timer_irq(uint32_t fs_irqs)
{
  return (unsigned int)(fs_irqs & EDMA_XXX_TIMER_IRQ_MASK);
}

void edma_xxx_ack_timer_irq(edma_handle_t* dma)
{
  edma_xxx_write_fs_irqs(dma, 1 << 31);
}
