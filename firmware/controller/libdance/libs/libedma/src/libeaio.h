#ifndef LIBEAIO_H_INCLUDED
#define LIBEAIO_H_INCLUDED



#include <stdint.h>
#include <sys/types.h>
#include "libebuf.h"


typedef int (*eaio_reset_fn_t)(void*);
typedef int (*eaio_start_fn_t)(void*);
typedef int (*eaio_stop_fn_t)(void*);
typedef int (*eaio_setup_read_fn_t)(void*, ebuf_handle_t*, size_t, size_t*);
typedef int (*eaio_compl_read_fn_t)(void*, ebuf_handle_t*, size_t, size_t*);


typedef struct eaio_desc
{
#define EAIO_FLAG_NONE (0)
#define EAIO_FLAG_THRESHOLD (1 << 0)
#define EAIO_FLAG_TIMEOUT (1 << 1)
#define EAIO_FLAG_REALTIME (1 << 2)
#define EAIO_FLAG_ONESHOT (1 << 3)
  uint32_t flags;

  /* signaling threshold in bytes, if flag set */
  size_t thresh;
  
  /* signaling timeout in us, if flag set */
  unsigned int tmout;

  /* oneshot size in bytes, if flag set */
  size_t oneshot_size;

  /* backing buffer */
  ebuf_handle_t* buf;

  /* ebone slave irq mask */
  uint32_t mirq;

  /* fifo or dma handle and related routines */
  void* handle;
  eaio_start_fn_t reset_fn;
  eaio_start_fn_t start_fn;
  eaio_stop_fn_t stop_fn;
  eaio_setup_read_fn_t setup_read_fn;
  eaio_compl_read_fn_t compl_read_fn;

} eaio_desc_t;


typedef struct eaio_handle
{
  /* refer to description for comments */

  uint32_t flags;
  size_t thresh;
  unsigned int tmout;

  /* buffer and read write pointers, in bytes */
  ebuf_handle_t* buf;
  volatile uint32_t wpos;
  volatile uint32_t rpos;

#define EAIO_STATE_STARTED 0
#define EAIO_STATE_STOPPED 1
  volatile uint32_t state;

#define EAIO_ERR_SUCCESS 0
#define EAIO_ERR_OVERFLOW 1
#define EAIO_ERR_FAILURE ((uint32_t)-1)
  volatile uint32_t err;

  /* ebone slave mirq info */
  uint32_t irq_mask;
  uint32_t irq_pos;

  /* edma or fifo handle and related routines */
  void* handle;
  eaio_reset_fn_t reset_fn;
  eaio_start_fn_t start_fn;
  eaio_stop_fn_t stop_fn;
  eaio_setup_read_fn_t setup_read_fn;
  eaio_compl_read_fn_t compl_read_fn;

  /* oneshot size */
  size_t oneshot_size;

  /* the size initially passed to setup_read_fn */
  size_t setup_size;

  /* internally flag that captures the overflow state */
  unsigned int is_overflow;

} eaio_handle_t;


int eaio_init_lib(void);
void eaio_fini_lib(void);
int eaio_get_wait_fd(void);
void eaio_init_desc(eaio_desc_t*);
void eaio_open(eaio_handle_t*);
void eaio_close(eaio_handle_t*);
int eaio_start(eaio_handle_t*, const eaio_desc_t*);
int eaio_stop(eaio_handle_t*);
int eaio_restart(eaio_handle_t*);

int eaio_read_oneshot(eaio_desc_t*, ebuf_handle_t*, size_t);

int eaio_get_data(eaio_handle_t*, void**, size_t*);
int eaio_seek_data(eaio_handle_t*, size_t);
size_t eaio_get_occupancy(eaio_handle_t*);
uint32_t eaio_get_err(eaio_handle_t*);
uint32_t eaio_get_state(eaio_handle_t*);


#endif /* LIBEAIO_H_INCLUDED */
