#ifndef LIBEDMA_H_INCLUDED
# define LIBEDMA_H_INCLUDED


#include <stdint.h> 
#include <sys/types.h>
#include "libeaio.h"
#include "libepci.h"
#include "libebuf.h"
#include "libuirq.h"


typedef struct edma_desc
{
  /* flags in EDMA_FLAG_XXX. remove when libemem avail. */
#define EDMA_FLAG_MIF (1 << 0)
#define EDMA_FLAG_BRAM (1 << 1)
#define EDMA_FLAG_FIFO (1 << 2)
#define EDMA_FLAG_IRQ (1 << 3)
  uint32_t flags;

  /* ebone slave address: (off << 28) | segment */
  size_t ebs_off;
  size_t ebs_seg;

  /* source offset and size (from 0, in bytes) */
  size_t soff;
  size_t ssize;

  /* pci related information */
  const char* pci_id;
  size_t pci_bar;
  size_t pci_off;

  /* uirq related */
  uirq_handle_t* uirq;

} edma_desc_t;


typedef struct edma_handle
{
  epcihandle_t bar0_handle;
  epcihandle_t epci_handle;
  size_t reg_off;
  size_t ebft_dwidth;
  size_t pcie_payload_size;

  uint32_t desc_flags;
  size_t desc_ebs_off;
  size_t desc_ebs_seg;
  size_t desc_soff;
  size_t desc_ssize;

  /* default aio handle */
  eaio_handle_t aio;

  uirq_handle_t* uirq;

  /* aio state */
  /* the source offset, in bytes */
  size_t aio_soff;
  /* the source size (from 0), in bytes */
  size_t aio_ssize;
  /* the source position (from 0), in bytes */
  size_t aio_spos;
  /* last transfer error */
  uint32_t aio_err;

} edma_handle_t;


int edma_init_lib(void);
int edma_fini_lib(void);
int edma_log_lib(int);
void edma_init_desc(edma_desc_t*);
int edma_open(edma_handle_t*, const edma_desc_t*);
int edma_close(edma_handle_t*); 
int edma_read(edma_handle_t*, size_t, ebuf_handle_t*, size_t, size_t);

/* asynchronous io */
void edma_init_aio_desc(edma_handle_t*, eaio_desc_t*);

/* allocate an ebuf that suits edma related ios */
int edma_alloc_buf(edma_handle_t*, ebuf_handle_t*, size_t);

static inline eaio_handle_t* edma_get_default_aio(edma_handle_t* dma)
{
  return &dma->aio;
}


#endif /* LIBEDMA_H_INCLUDED */
