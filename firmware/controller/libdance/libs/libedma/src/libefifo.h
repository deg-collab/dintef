#ifndef LIBEFIFO_H_INCLUDED
# define LIBEFIFO_H_INCLUDED 1



#include <stdint.h>
#include <sys/types.h>
#include "libepci.h"
#include "libuirq.h"
#include "libebuf.h"
#include "libeaio.h"


/* timeout special values */

#define EFIFO_TIMEOUT_NONE ((unsigned int)-1)
#define EFIFO_TIMEOUT_ZERO ((unsigned int)0)


/* fifo description */

typedef struct efifo_desc
{
#define EFIFO_FLAG_READ (1 << 0)
#define EFIFO_FLAG_WRITE (1 << 1)
#define EFIFO_FLAG_READWRITE (EFIFO_FLAG_READ | EFIFO_FLAG_WRITE)
#define EFIFO_FLAG_IRQ (1 << 2)
  uint32_t flags;

  /* item width, in bytes. must be multiple of sizeof(uint32_t). */
  size_t width;

  /* fifo item count */
  size_t depth;

  /* write port width, in bytes. */
  size_t write_width;

  /* associated epci bar handle */
  epcihandle_t epci_handle;

  /* control, {r,w}data, status registers */
  size_t ctrl_reg_off;
  size_t rdat_reg_off;
  size_t wdat_reg_off;
  size_t stat_reg_off;

  /* ebone slave id */
  uint32_t ebone_slave_id;

} efifo_desc_t;


/* fifo handle */

typedef struct efifo_handle
{
  /* EFIFO_FLAG_xxx */
  uint32_t flags;

  /* item width, in uint32_t words */
  size_t width;
  /* fifo item count */
  size_t depth;

  /* write port width, in bytes. */
  size_t write_width;

  /* ebone slave irq mask */
  uint32_t mirq;

  /* control, {r,w}data, status registers */
  epcihandle_t epci_handle;
  size_t ctrl_reg_off;
  size_t rdat_reg_off;
  size_t wdat_reg_off;
  size_t stat_reg_off;

  /* ebone slave id */
  uint32_t slave_id;

  /* default aio handle */
  eaio_handle_t aio;

} efifo_handle_t;


/* exported routines */

/* library constructor */
int efifo_init_lib(void);
int efifo_fini_lib(void);

/* open close */
void efifo_init_desc(efifo_desc_t*);
int efifo_open(efifo_handle_t*, const efifo_desc_t*);
int efifo_close(efifo_handle_t*);

/* synchronous operations */
int efifo_read(efifo_handle_t*, void*, size_t*);
int efifo_write(efifo_handle_t*, const void*, size_t*);
int efifo_read_ebuf(efifo_handle_t*, ebuf_handle_t*, size_t, size_t*);
int efifo_write_ebuf(efifo_handle_t*, ebuf_handle_t*, size_t, size_t*);
size_t efifo_get_occupancy(efifo_handle_t*);

/* asynchronous io */
void efifo_init_aio_desc(efifo_handle_t*, eaio_desc_t*);

/* allocate an ebuf that suits efifo related ios */
int efifo_alloc_buf(efifo_handle_t*, ebuf_handle_t*, size_t);

#ifdef EFIFO_CONFIG_UNIT
/* used for unit testing */
void efifo_set_reset_bit(efifo_handle_t*);
void efifo_clear_reset_bit(efifo_handle_t*);
#endif /* EFIFO_CONFIG_UNIT */

/* inlined raw accessors */

static inline size_t efifo_get_depth(const efifo_handle_t* fifo)
{
  /* return the fifo depth, in item count */
  return fifo->depth;
}

static inline size_t efifo_get_width(const efifo_handle_t* fifo)
{
  /* return the item width, in bytes */
  return fifo->width * sizeof(uint32_t);
}

static inline unsigned int efifo_is_empty(efifo_handle_t* fifo)
{
  return efifo_get_occupancy(fifo) == 0;
}

static inline unsigned int efifo_is_full(efifo_handle_t* fifo)
{
  return efifo_get_occupancy(fifo) == fifo->depth;
}

static inline size_t efifo_count_to_size
(const efifo_handle_t* fifo, size_t count)
{
  /* note: efifo_get_width() returns a byte count */
  return count * efifo_get_width(fifo);
}

static inline size_t efifo_size_to_count
(const efifo_handle_t* fifo, size_t size)
{
  /* warning: count rounded down */
  /* note:  get_width() returns a byte count */
  return size / efifo_get_width(fifo);
}

/* retrieve the default aio handle */
static inline eaio_handle_t* efifo_get_default_aio(efifo_handle_t* fifo)
{
  return &fifo->aio;
}



#endif /* ! LIBEFIFO_H_INCLUDED */
