/* tool to manage efpak files */


#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <dirent.h>
#include <sys/time.h>
#include <pthread.h>
#include <signal.h>
#include "libefpak.h"
#include "disk.h"
#ifdef CONFIG_LIBDEEP
#include "libdeep.h"
#endif /* CONFIG_LIBDEEP */


/* Automatically updated by SVN but  need to set
 * the following property of the source file
 * "svn:keywords   Author Revision"
 */
static char SvnDate[] = "$Date: 2020-01-24 15:19:50 +0100 (Fri, 24 Jan 2020) $";
static char SvnRevision[] = "$Revision: 2146 $";

const char app_ident[] =
#include "_idfile.h"
;

#include <stdio.h>
#define PERROR()			\
do {					\
  printf("[!] %u\n", __LINE__);		\
  fflush(stdout);			\
} while (0)


int debug = 0;
#define LOG(...)                        \
do {					\
  if(debug) {                           \
    printf("    EFPAK: "__VA_ARGS__);   \
  }                                     \
} while (0)

#define LOG_ERROR(...)                  \
do {					\
  printf("error: "__VA_ARGS__);         \
} while (0)


#include <stdlib.h> // mkdtemp()
#include <string.h> // strlen()
char *_tempnam(const char *dir, const char *pfx)
{
  char  dname_template[] = "/tmp/efpak_XXXXXX";
  char *dname;
  char  fname[] = "pkg.efpak";
  char *fpath;

  dname = mkdtemp(dname_template);
  fpath = malloc(strlen(dname) + strlen(fname));
  sprintf(fpath, "%s/%s", dname, fname);
  return(fpath);
}

#include <unistd.h> // rmdir() unlink()
#include <libgen.h> // dirname()
int _unlink(char *fpath)
{
   unlink(fpath);
   rmdir(dirname(fpath));
   return(0);
}

static int do_list(int ac, const char** av)
{
  const char* const path = av[2];
  efpak_istream_t is;
  const efpak_header_t* h;
  int err = -1;
  size_t i;

  LOG("do_list()\n");

  if (ac != 3)
  {
    LOG_ERROR("missing arguments\n");
    goto on_error_0;
  }

  if (efpak_istream_init_with_file(&is, path))
  {
    LOG_ERROR("unable to open file: %s\n", path);
    goto on_error_0;
  }

  for (i = 0; 1; ++i)
  {
    if (efpak_istream_next_block(&is, &h))
    {
      LOG_ERROR("error parsing file: %s\n", path);
      goto on_error_1;
    }
    if (h == NULL) break ;

    printf("header[%zu]:\n", i);
    printf(".vers          : 0x%02x\n", h->vers);
    printf(".type          : 0x%02x\n", h->type);
    printf(".comp          : 0x%02x\n", h->comp);
    printf(".header_size   : %" PRIu64 "\n", h->header_size);
    printf(".comp_data_size: %" PRIu64 "\n", h->comp_data_size);
    printf(".raw_data_size : %" PRIu64 "\n", h->raw_data_size);

    switch (h->type)
    {
    case EFPAK_BTYPE_FORMAT:
      {
	const uint8_t* const s = h->u.format.signature;
	printf(".signature     : %c%c%c%c\n", s[0], s[1], s[2], s[3]);
	break ;
      }

    case EFPAK_BTYPE_DISK:
      {
	break ;
      }

    case EFPAK_BTYPE_PART:
      {
	printf(".part_id       : 0x%02x\n", h->u.part.part_id);
	printf(".fs_id         : 0x%02x\n", h->u.part.fs_id);
	break ;
      }

    case EFPAK_BTYPE_FILE:
      {
	const char* s = "invalid";
	size_t i;
	for (i = 0; i != h->u.file.path_len; ++i)
	{
	  if (h->u.file.path[i] == 0)
	  {
	    s = (const char*)h->u.file.path;
	    break ;
	  }
	}
	printf(".path          : %s\n", s);
	break ;
      }

    case EFPAK_BTYPE_HOOK:
      {
	const uint32_t wflags = h->u.hook.when_flags;
	const uint32_t eflags = h->u.hook.exec_flags;
	const char* s = "invalid";
	size_t i;

	printf(".wflags        :");
	if (wflags & EFPAK_HOOK_NOW) printf(" now");
	if (wflags & EFPAK_HOOK_PREX) printf(" prex");
	if (wflags & EFPAK_HOOK_POSTX) printf(" postx");
	if (wflags & EFPAK_HOOK_COMPL) printf(" compl");
	if (wflags & EFPAK_HOOK_MBR) printf(" mbr");
	printf("\n");

	printf(".eflags        :");
	if (eflags & EFPAK_HOOK_EXECVE) printf(" execve");
	printf("\n");

	for (i = 0; i != h->u.hook.path_len; ++i)
	{
	  if (h->u.hook.path[i] == 0)
	  {
	    s = (const char*)h->u.hook.path;
	    break ;
	  }
	}
	printf(".path          : %s\n", s);

	break ;
      }

    case EFPAK_BTYPE_META:
      {
	const char* s = "invalid";
	size_t i;
	for (i = 0; i != h->u.meta.name_len; ++i)
	{
	  if (h->u.meta.name[i] == 0)
	  {
	    s = (const char*)h->u.meta.name;
	    break ;
	  }
	}
	printf(".name          : %s\n", s);
	break ;
      }

    default:
      {
	break ;
      }
    }

    printf("\n");
  }

  err = 0;

 on_error_1:
  efpak_istream_fini(&is);
 on_error_0:
  return err;
}

static int do_version(int ac, const char** av)
{
  const char* const path = av[2];
  efpak_istream_t is;
  const efpak_header_t* h;
  int err = -1;
  unsigned int is_vers;
  size_t size;
  const char* s;
  size_t i;
  const uint8_t* data;

  LOG("do_version()\n");

  if (ac != 3)
  {
    LOG_ERROR("missing arguments\n");
    goto on_error_0;
  }

  if (efpak_istream_init_with_file(&is, path))
  {
    LOG_ERROR("unable to open file: %s\n", path);
    goto on_error_0;
  }

  for (is_vers = 0; (is_vers == 0); )
  {

    if (efpak_istream_next_block(&is, &h))
    {
      LOG_ERROR("error parsing file: %s\n", path);
      goto on_error_1;
    }
    if (h == NULL) break ;
    if (h->type != EFPAK_BTYPE_META) continue ;

    s = "invalid";
    for (i = 0; i != h->u.meta.name_len; ++i)
    {
      if (h->u.meta.name[i] == 0)
      {
	s = (const char*)h->u.meta.name;
	break ;
      }
    }

    if (strcmp(s, "version")) continue ;

    is_vers = 1;

    if (efpak_istream_start_block(&is))
    {
      LOG_ERROR("error parsing file: %s\n", path);
      goto on_error_1;
    }

    while (1)
    {
      size = (size_t)-1;
      if (efpak_istream_next(&is, &data, &size)) break ;
      if (size == 0) break ;
      write(STDOUT_FILENO, data, size);
    }

    efpak_istream_end_block(&is);

    write(STDOUT_FILENO, "\n", 1);
  }

  if (is_vers == 0) printf("no version found\n");

  err = 0;

 on_error_1:
  efpak_istream_fini(&is);
 on_error_0:
  return err;
}

static int do_create(int ac, const char** av)
{
  const char* const path = av[2];
  struct stat st;
  efpak_ostream_t os;

  LOG("do_create()\n");

  if (ac != 3)
  {
    LOG_ERROR("missing arguments\n");
    return -1;
  }

  /* must not already exist */
  errno = 0;
  if (stat(path, &st) == 0) errno = 0;
  if (errno != ENOENT)
  {
    LOG_ERROR("file aleady exists: %s\n", path);
    return -1;
  }

  if (efpak_ostream_init_with_file(&os, path))
  {
    LOG_ERROR("unable to create file\n");
    return -1;
  }
  efpak_ostream_fini(&os);
  return 0;
}

static int do_add_disk(int ac, const char** av)
{
  const char* const efpak_path = av[2];
  const char* const disk_path = av[3];

  efpak_ostream_t os;
  int err = -1;

  LOG("do_add_disk()\n");

  if (ac != 4)
  {
    LOG_ERROR("missing arguments\n");
    goto on_error_0;
  }

  if (efpak_ostream_init_with_file(&os, efpak_path))
  {
    LOG_ERROR("unable to open file: %s\n", efpak_path);
    goto on_error_0;
  }

  if (efpak_ostream_add_disk(&os, disk_path))
  {
    LOG_ERROR("unable to add file: %s\n", disk_path);
    goto on_error_1;
  }
  err = 0;

 on_error_1:
  efpak_ostream_fini(&os);
 on_error_0:
  return err;
}

static int do_add_part(int ac, const char** av)
{
  const char* const efpak_path = av[2];
  const char* const part_path = av[3];
  const char* const part_name = av[4];
  const char* fs_name = av[5];

  efpak_partid_t part_id;
  efpak_fsid_t fs_id;
  efpak_ostream_t os;
  int err = -1;

  LOG("do_add_part()\n");

  if (ac != 6)
  {
    if (ac != 5)
    {
      LOG_ERROR("missing arguments\n");
      goto on_error_0;
    }
    fs_name = NULL;
  }

  if (strcmp(part_name, "boot") == 0)
  {
    part_id = EFPAK_PARTID_BOOT;
    if (fs_name == NULL) fs_name = "vfat";
  }
  else if (strcmp(part_name, "root") == 0)
  {
    part_id = EFPAK_PARTID_ROOT;
    if (fs_name == NULL) fs_name = "squash";
  }
  else if (strcmp(part_name, "app") == 0)
  {
    part_id = EFPAK_PARTID_APP;
    if (fs_name == NULL) fs_name = "ext3";
  }
  else goto on_error_0;

  if (strcmp(fs_name, "vfat") == 0) fs_id = EFPAK_FSID_VFAT;
  else if (strcmp(fs_name, "squash") == 0) fs_id = EFPAK_FSID_SQUASH;
  else if (strcmp(fs_name, "ext2") == 0) fs_id = EFPAK_FSID_EXT2;
  else if (strcmp(fs_name, "ext3") == 0) fs_id = EFPAK_FSID_EXT3;
  else
  {
    LOG_ERROR("non supported partition fs type\n");
    goto on_error_0;
  }

  if (efpak_ostream_init_with_file(&os, efpak_path))
  {
    LOG_ERROR("unable to open file: %s\n", efpak_path);
    goto on_error_0;
  }

  if (efpak_ostream_add_part(&os, part_path, part_id, fs_id))
  {
    LOG_ERROR("unable to partition\n");
    goto on_error_1;
  }
  err = 0;

 on_error_1:
  efpak_ostream_fini(&os);
 on_error_0:
  return err;
}

static int do_add_file(int ac, const char** av)
{
  const char* const efpak_path = av[2];
  const char* const src_path = av[3];
  const char* const dst_path = av[4];

  efpak_ostream_t os;
  int err = -1;

  LOG("do_add_file()\n");

  if (ac != 5)
  {
    LOG_ERROR("missing arguments\n");
    goto on_error_0;
  }

  if (efpak_ostream_init_with_file(&os, efpak_path))
  {
    LOG_ERROR("unable to open file: %s\n", efpak_path);
    goto on_error_0;
  }
  if (efpak_ostream_add_file(&os, src_path, dst_path))
  {
    LOG_ERROR("unable to add file: %s\n", src_path);
    goto on_error_1;
  }
  err = 0;

 on_error_1:
  efpak_ostream_fini(&os);
 on_error_0:
  return err;
}

typedef struct
{
  efpak_ostream_t* os;

  /* source path */
  size_t spath_size;
  size_t spath_pos;
  char* spath_buf;

  /* destination path */
  size_t dpath_size;
  size_t dpath_pos;
  char* dpath_buf;

} add_dir_t;

static int add_dir_rec(add_dir_t* ad)
{
  DIR* dir;
  struct stat st;
  int err = -1;
  size_t i;

  if ((ad->spath_pos + 1) >= ad->spath_size)
  {
    PERROR();
    goto on_error_0;
  }
  ad->spath_buf[ad->spath_pos++] = '/';
  ad->spath_buf[ad->spath_pos] = 0;

  if ((ad->dpath_pos + 1) >= ad->dpath_size)
  {
    PERROR();
    goto on_error_0;
  }
  ad->dpath_buf[ad->dpath_pos++] = '/';
  ad->dpath_buf[ad->dpath_pos] = 0;

  dir = opendir(ad->spath_buf);
  if (dir == NULL)
  {
    PERROR();
    goto on_error_0;
  }

  while (1)
  {
      struct dirent* dep;

    errno = 0;
    dep = readdir(dir);
    if (dep == NULL)
    {
       if (errno != 0)
       {
         PERROR();
         goto on_error_1;
       }
       break;
    }

    if (strcmp(dep->d_name, ".") == 0) continue ;
    if (strcmp(dep->d_name, "..") == 0) continue ;

    for (i = 0; 1; ++i)
    {
      if (i == sizeof(dep->d_name))
      {
	PERROR();
	goto on_error_1;
      }

      if ((ad->spath_pos + i) == ad->spath_size)
      {
	PERROR();
	goto on_error_1;
      }

      if ((ad->dpath_pos + i) == ad->dpath_size)
      {
	PERROR();
	goto on_error_1;
      }

      ad->spath_buf[ad->spath_pos + i] = dep->d_name[i];
      ad->dpath_buf[ad->dpath_pos + i] = dep->d_name[i];

      if (dep->d_name[i] == 0) break ;
    }

    ad->spath_pos += i;
    ad->dpath_pos += i;

    if (stat(ad->spath_buf, &st))
    {
      PERROR();
      goto on_error_1;
    }

    if (S_ISREG(st.st_mode))
    {
      printf("adding %s, %s\n", ad->spath_buf, ad->dpath_buf);

      if (efpak_ostream_add_file(ad->os, ad->spath_buf, ad->dpath_buf))
      {
	PERROR();
	goto on_error_1;
      }
    }
    else if (S_ISDIR(st.st_mode))
    {
      if (add_dir_rec(ad))
      {
	PERROR();
	goto on_error_1;
      }
    }

    ad->spath_pos -= i;
    ad->dpath_pos -= i;
  }

  ad->spath_pos -= 1;
  ad->dpath_pos -= 1;

  err = 0;

 on_error_1:
  closedir(dir);
 on_error_0:
  return err;
}

static int do_add_dir(int ac, const char** av)
{
  const char* const efpak_path = av[2];
  const char* const src_path = av[3];
  const char* const dst_path = av[4];

  add_dir_t ad;
  efpak_ostream_t os;
  char spath_buf[512];
  char dpath_buf[512];
  int err = -1;

  LOG("do_add_dir()\n");

  if (ac != 5)
  {
    LOG_ERROR("missing arguments\n");
    goto on_error_0;
  }
  if (efpak_ostream_init_with_file(&os, efpak_path))
  {
    LOG_ERROR("unable to open file: %s\n", efpak_path);
    goto on_error_0;
  }

  ad.os = &os;

  ad.spath_size = sizeof(spath_buf);
  ad.spath_pos = strlen(src_path);
  if (ad.spath_pos >= ad.spath_size) goto on_error_1;
  memcpy(spath_buf, src_path, ad.spath_pos);
  spath_buf[ad.spath_pos] = 0;
  ad.spath_buf = spath_buf;

  ad.dpath_size = sizeof(dpath_buf);
  ad.dpath_pos = strlen(dst_path);
  if (ad.dpath_pos >= ad.dpath_size) goto on_error_1;
  memcpy(dpath_buf, dst_path, ad.dpath_pos);
  ad.dpath_buf = dpath_buf;

  if (add_dir_rec(&ad)) goto on_error_1;

  err = 0;

 on_error_1:
  efpak_ostream_fini(&os);
 on_error_0:
  return err;
}

static int do_add_hook(int ac, const char** av)
{
  const char* const efpak_path = av[2];
  const char* const dpath = av[3];
  const char* const flags = av[4];

  const char* const xpath = NULL;
  const uint32_t eflags = EFPAK_HOOK_EXECVE;

  static const struct
  {
    const char* s;
    uint32_t f;
  } pairs[] =
  {
    { "now", EFPAK_HOOK_NOW },
    { "prex", EFPAK_HOOK_PREX },
    { "postx", EFPAK_HOOK_POSTX },
    { "compl", EFPAK_HOOK_COMPL },
    { "mbr", EFPAK_HOOK_MBR }
  };

  static const size_t npairs = sizeof(pairs) / sizeof(pairs[0]);

  efpak_ostream_t os;
  uint32_t wflags;
  size_t i;
  size_t j;
  size_t k;
  int err = -1;

  LOG("do_add_hook()\n");

  if (ac != 5)
  {
    LOG_ERROR("missing arguments\n");
    goto on_error_0;
  }

  if (efpak_ostream_init_with_file(&os, efpak_path))
  {
    LOG_ERROR("unable to open file: %s\n", efpak_path);
    goto on_error_0;
  }

  wflags = 0;
  k = 0;
  for (i = 0; 1; ++i)
  {
    if ((flags[i] != ',') && (flags[i] != 0)) continue ;

    for (j = 0; j != npairs; ++j)
    {
      const size_t n = strlen(pairs[j].s);
      if (memcmp(pairs[j].s, flags + k, n) == 0) break ;
    }

    if (j == npairs) goto on_error_1;
    wflags |= pairs[j].f;

    if (flags[i] == 0) break ;
    k = i + 1;
  }

  if (efpak_ostream_add_hook(&os, dpath, xpath, wflags, eflags))
  {
    LOG_ERROR("unable to add hook to file: %s\n", efpak_path);
    goto on_error_1;
  }

  err = 0;

 on_error_1:
  efpak_ostream_fini(&os);
 on_error_0:
  return err;
}

static int do_add_meta(int ac, const char** av)
{
  const char* const efpak_path = av[2];
  const char* const meta_name = av[3];
  const char* const meta_val = av[4];

  const uint8_t* data = MAP_FAILED;
  size_t size;
  struct stat st;
  int fd;

  efpak_ostream_t os;
  int err = -1;

  LOG("do_add_meta()\n");

  if (ac != 5)
  {
    LOG_ERROR("missing arguments\n");
    goto on_error_0;
  }

  /* deduce if value is a string or a file */
  fd = open(meta_val, O_RDONLY);
  if (fd == -1)
  {
    /* assume string */
    size = strlen(meta_val);
    data = (const uint8_t*)meta_val;
  }
  else
  {
    if (fstat(fd, &st))
    {
      LOG_ERROR("error accessing file: %s\n", meta_val);
      goto on_error_1;
    }
    size = st.st_size;
    data = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
    if (data == MAP_FAILED)
    {
      LOG_ERROR("error writing file: %s\n", meta_val);
      goto on_error_1;
    }
  }

  if (efpak_ostream_init_with_file(&os, efpak_path))
  {
    LOG_ERROR("unable to open file: %s\n", efpak_path);
    goto on_error_2;
  }

  if (efpak_ostream_add_meta(&os, meta_name, data, size))
  {
    LOG_ERROR("unable to add meta to file: %s\n", efpak_path);
    goto on_error_3;
  }
  err = 0;

 on_error_3:
  efpak_ostream_fini(&os);
 on_error_2:
  if ((fd != -1) && (data != MAP_FAILED)) munmap((void*)data, size);
 on_error_1:
  if (fd != -1) close(fd);
 on_error_0:
  return err;
}

static int do_add_bit(int ac, const char** av)
{
  /* add a bitstream and corresponding install hook */

#define DEST_BIT_PATH "/tmp/top.bit"

  static const char hook_data_without_sebone[] =
    "#!/usr/bin/env sh\n"
    "efspi_path=/bin/dance/efspi\n"
    "$efspi_path -a write -f " DEST_BIT_PATH "\n"
    "err=$?\n"
    "rm " DEST_BIT_PATH "\n"
    "exit $err\n"
    ;

  static const char hook_data_with_sebone[] =
    "#!/usr/bin/env sh\n"
    "efspi_path=/bin/dance/efspi\n"
    "$efspi_path -a write -f " DEST_BIT_PATH " -m sebone\n"
    "err=$?\n"
    "rm " DEST_BIT_PATH "\n"
    "exit $err\n"
    ;

  static const char hook_data_with_jtag[] =
    "#!/usr/bin/env sh\n"
    "ejtag_path=/bin/dance/ejtag\n"
    "$ejtag_path -a prog -f " DEST_BIT_PATH "\n"
    "err=$?\n"
    "rm " DEST_BIT_PATH "\n"
    "exit $err\n"
    ;

  const char* self_path;
  const char* efpak_path;
  const char* bit_path;
  const char* new_av[5];
  const char* hook_data;
  size_t hook_size;
  char* hook_path;
  int fd;
  int err = -1;
  unsigned int is_sebone;
  unsigned int is_jtag;

  LOG("do_add_bit()\n");

  is_sebone = 0;
  is_jtag = 0;
  if (ac == 5)
  {
    if (strcmp(av[4], "sebone") == 0) is_sebone = 1;
    else if (strcmp(av[4], "jtag") == 0) is_jtag = 1;
  }
  else if (ac != 4)
  {
    LOG_ERROR("missing arguments\n");
    goto on_error_0;
  }

  self_path = av[0];
  efpak_path = av[2];
  bit_path = av[3];

  /* create the hook shell script */
  hook_path = _tempnam("/tmp", NULL);
  if (hook_path == NULL)
  {
    LOG_ERROR("unable to create temporary file\n");
    goto on_error_0;
  }
  fd = open(hook_path, O_RDWR | O_TRUNC | O_CREAT, 0755);
  if (fd == -1)
  {
    LOG_ERROR("unable to open temporary file\n");
    goto on_error_1;
  }

  if (is_jtag) hook_data = hook_data_with_jtag;
  else if (is_sebone) hook_data = hook_data_with_sebone;
  else hook_data = hook_data_without_sebone;
  hook_size = strlen(hook_data);

  if (write(fd, hook_data, hook_size) != (ssize_t)hook_size)
  {
    LOG_ERROR("unable to write to temporary file\n");
    goto on_error_2;
  }

  /* add_file */
  new_av[0] = self_path;
  new_av[1] = "add_file";
  new_av[2] = efpak_path;
  new_av[3] = bit_path;
  new_av[4] = DEST_BIT_PATH;
  if (do_add_file(5, new_av)) goto on_error_2;

  /* add_hook */
  new_av[0] = self_path;
  new_av[1] = "add_hook";
  new_av[2] = efpak_path;
  new_av[3] = hook_path;
  new_av[4] = "now";
  if (do_add_hook(5, new_av)) goto on_error_2;

  err = 0;

 on_error_2:
  close(fd);
  _unlink(hook_path);
 on_error_1:
  free(hook_path);
 on_error_0:
  return err;
}

static int do_extract(int ac, const char** av)
{
  const char* const efpak_path = av[2];
  const char* const dir_path = av[3];

  LOG("do_extract()\n");

  char full_path[256];
  const efpak_header_t* h;
  efpak_istream_t is;
  int err = -1;
  int fd;
  unsigned int n = 0;
  size_t size;
  const uint8_t* data;

  if (ac != 4)
  {
    LOG_ERROR("missing arguments\n");
    goto on_error_0;
  }

  errno = 0;
  if (mkdir(dir_path, 0755))
  {
    if (errno != EEXIST)
    {
      LOG_ERROR("error creating dir: %s\n", dir_path);
      goto on_error_0;
    }
  }

  if (efpak_istream_init_with_file(&is, efpak_path))
  {
    LOG_ERROR("unable to open file: %s\n", efpak_path);
    goto on_error_0;
  }

  while (1)
  {
    if (efpak_istream_next_block(&is, &h))
    {
      LOG_ERROR("error parsing file: %s\n", efpak_path);
      goto on_error_1;
    }
    if (h == NULL) break ;
    if (h->type == EFPAK_BTYPE_FORMAT) continue ;

    /* create the file */
    snprintf(full_path, sizeof(full_path), "%s/%04x", dir_path, n++);
    full_path[sizeof(full_path) - 1] = 0;
    fd = open(full_path, O_RDWR | O_TRUNC | O_CREAT, 0755);
    if (fd == -1)
    {
      LOG_ERROR("unable to create file: %s\n", full_path);
      goto on_error_1;
    }

    /* extract the block */
    if (efpak_istream_start_block(&is))
    {
      LOG_ERROR("unable to extract block from file: %s\n", efpak_path);
      close(fd);
      goto on_error_1;
    }

    while (1)
    {
      size = (size_t)-1;
      if (efpak_istream_next(&is, &data, &size))
      {
        LOG_ERROR("error parsing file: %s\n", efpak_path);
	close(fd);
	goto on_error_1;
      }

      if (size == 0) break ;

      if (write(fd, data, size) != (ssize_t)size)
      {
        LOG_ERROR("unable to write to file: %s\n", full_path);
	close(fd);
	goto on_error_1;
      }
    }

    close(fd);

    efpak_istream_end_block(&is);
  }

  err = 0;

 on_error_1:
  efpak_istream_fini(&is);
 on_error_0:
  return err;
}

static int do_install(int ac, const char** av)
{
  const char* const efpak_path = av[2];
  const char* disk_name = av[3];
  int err = -1;
  efpak_istream_t is;
  disk_handle_t disk;

  LOG("do_install()\n");

  if (ac != 4)
  {
    LOG_ERROR("missing arguments\n");
    goto on_error_0;
  }

  if (efpak_istream_init_with_file(&is, efpak_path))
  {
    LOG_ERROR("unable to open file: %s\n", efpak_path);
    goto on_error_0;
  }

  if (strcmp(disk_name, "root") == 0) err = disk_open_root(&disk);
  else err = disk_open_dev(&disk, disk_name);
  if (err)
  {
    LOG_ERROR("error accessing disk\n");
    goto on_error_1;
  }

  err = disk_install_with_efpak(&disk, &is);
  if (err)
  {
    LOG_ERROR("disk install failed\n");
    goto on_error_2;
  }

 on_error_2:
  disk_close(&disk);
 on_error_1:
  efpak_istream_fini(&is);
 on_error_0:
  return err;
}


#ifdef CONFIG_LIBDEEP
static deephandle_t get_connection(char *addr)
{
  LOG("get_connection()\n");

  deephandle_t dev;
  const char *answ;
  int port;
  int valid_ports[] = {5000, 5001, 5002};
  size_t i, n_ports = sizeof(valid_ports)/sizeof(int);

  for (i = 0; i < n_ports; i++)
  {
    port = valid_ports[i];
    deepdev_setparam(LIB_DEFAULTS, "PORT",  DDPAR(port));

    dev = deepdev_open((char*)addr);
    if (dev == BAD_HANDLE)
      continue;

    if (deepdev_query(dev, "?APPNAME", &answ) == DEEPDEV_OK)
      break;
    else
      dev = BAD_HANDLE;
  }

  return(dev);
}
#endif /* CONFIG_LIBDEEP */


static int do_reboot(int ac, const char** av)
{
  LOG("do_reboot()\n");

#ifdef CONFIG_LIBDEEP

  const char* const addr = av[1];
  const char* const type = av[2];

  static const int timeout = 5000;
  char cmd[128];
  deephandle_t dev;
  int err = -1;

  if (ac != 3)
  {
    LOG_ERROR("missing arguments\n");
    goto on_error_0;
  }

  dev = get_connection((char*)addr);
  if (dev == BAD_HANDLE)
  {
    LOG_ERROR("unable to get connection to: %s\n", addr);
    goto on_error_0;
  }

  deepdev_setparam(dev, "TIMEOUT", DDPAR(timeout));
  snprintf(cmd, sizeof(cmd), "#REBOOT %s DELAY 1", type);
  if (deepdev_cmd(dev, cmd) != DEEPDEV_OK)
  {
    LOG_ERROR("unable to execute command: %s\n", cmd);
    goto on_error_1;
  }

  /* success */
  err = 0;

 on_error_1:
  deepdev_close(dev);
 on_error_0:
  return err;

#else

  return -1;

#endif /* CONFIG_LIBDEEP */
}


static int do_send(int ac, const char** av)
{
  LOG("do_send()\n");

#ifdef CONFIG_LIBDEEP

  const char* const path = av[2];
  const char* addr = av[3];

  static const int timeout = 100000;
  struct timezone    tz_beg, tz_end;
  struct timeval     tv_beg, tv_end, tv_rel;

  deephandle_t dev;
  deepbindata_t data;
  char cmd[128];
  int err = -1;
  int fd;
  struct stat st;
  const char *answ;


  if (ac != 4)
  {
    LOG_ERROR("missing arguments\n");
    goto on_error_0;
  }

  dev = get_connection((char*)addr);
  if (dev == BAD_HANDLE)
  {
    LOG_ERROR("unable to get connection to: %s\n", addr);
    goto on_error_0;
  }

  deepdev_setparam(dev, "TIMEOUT", DDPAR(timeout));

  fd = open(path, O_RDONLY);
  if (fd == -1)
  {
    LOG_ERROR("unable to open file: %s\n", path);
    goto on_error_1;
  }

  if (fstat(fd, &st))
  {
    LOG_ERROR("unable to access file: %s\n", path);
    goto on_error_2;
  }

  data.bufsize = st.st_size;
  data.datasize = st.st_size;
  data.datatype = BIN_8;
  data.databuf = mmap(NULL, data.bufsize, PROT_READ, MAP_SHARED, fd, 0);
  if (data.databuf == MAP_FAILED)
  {
    LOG_ERROR("unable to read file: %s\n", path);
    goto on_error_2;
  }

  gettimeofday(&tv_beg, &tz_beg);
  timerclear(&tv_rel);

  LOG("%lu.%03lu Sending binary\n", tv_rel.tv_sec, tv_rel.tv_usec);
  snprintf(cmd, sizeof(cmd), "#*PROG");
  if (deepdev_bincmd(dev, cmd, &data) != DEEPDEV_OK)
  {
    LOG_ERROR("unable to execute command: %s\n", cmd);
    goto on_error_3;
  }

  /* wait for the end of the prog process */
  snprintf(cmd, sizeof(cmd), "?PROG");
  for(;;) {
    if (deepdev_query(dev, cmd, &answ) != DEEPDEV_OK)
    {
      /* old controller/libdance don't have the command */
      if (strcasecmp("Command not recognised", answ) >= 0) {
         LOG("WARNING: too old controller to get progression\n");
         goto on_success;
      } 
      LOG_ERROR("executing command: %s\n", cmd);
      LOG_ERROR("%s\n", answ);
      goto on_error_3;
    }

    gettimeofday(&tv_end, &tz_end);
    timersub(&tv_end, &tv_beg, &tv_rel);
    LOG("%lu.%06lu Prog status: \"%s\"\n", tv_rel.tv_sec, tv_rel.tv_usec, answ);

    if (strstr(answ, "DONE") != NULL) break;

    sleep(1);
    /* TODO: add timeout once we know a realistic value:
     *   updating LFS: 170 seconds with flash WR simulated
     */

  }


 on_success:
  err = 0;
 on_error_3:
  munmap(data.databuf, data.bufsize);
 on_error_2:
  close(fd);
 on_error_1:
  deepdev_close(dev);
 on_error_0:
  return err;

#else

  return -1;

#endif /* CONFIG_LIBDEEP */
}

static int do_send_bit_common(int ac, const char** av)
{
  LOG("do_send_bit_common()\n");

  /* create a temporary package with bitstream and send it */

  const char* self_path;
  const char* bit_path;
  const char* dev_addr;
  const char* prog_opt;
  const char* new_av[5];
  char* efpak_path;
  int err = -1;

  self_path = av[0];
  bit_path = av[2];
  dev_addr = av[3];
  prog_opt = av[4];

  efpak_path = _tempnam("/tmp", NULL);
  if (efpak_path == NULL)
  {
    LOG_ERROR("unable to create temporary file\n");
    goto on_error_0;
  }

  new_av[0] = self_path;
  new_av[1] = "create";
  new_av[2] = efpak_path;
  if (do_create(3, new_av)) goto on_error_1;

  new_av[0] = self_path;
  new_av[1] = "add_bit";
  new_av[2] = efpak_path;
  new_av[3] = bit_path;
  new_av[4] = prog_opt;
  if (do_add_bit(5, new_av)) goto on_error_2;

  new_av[0] = self_path;
  new_av[1] = "send";
  new_av[2] = efpak_path;
  new_av[3] = dev_addr;
  if (do_send(4, new_av)) goto on_error_2;

  /* reboot needed on jtag programming */
  if ((ac == 5) && (strcmp(prog_opt, "jtag") == 0))
  {
    new_av[0] = self_path;
    new_av[1] = dev_addr;
    new_av[2] = "CPU";
    if (do_reboot(3, new_av)) goto on_error_2;
  }

  err = 0;

 on_error_2:
  _unlink(efpak_path);
 on_error_1:
  free(efpak_path);
 on_error_0:
  return err;
}

static int do_send_bit_local(int ac, const char** av)
{
  const char* new_av[5];

  LOG("do_send_bit_local()\n");
  if (ac != 4)
  {
    LOG_ERROR("missing arguments\n");
    return -1;
  }

  new_av[0] = av[0];
  new_av[1] = av[1];
  new_av[2] = av[2];
  new_av[3] = av[3];
  new_av[4] = "";

  return do_send_bit_common(5, new_av);
}

static int do_send_bit_sebone(int ac, const char** av)
{
  const char* new_av[5];

  LOG("do_send_bit_sebone()\n");
  if (ac != 4)
  {
    LOG_ERROR("missing arguments\n");
    return -1;
  }

  new_av[0] = av[0];
  new_av[1] = av[1];
  new_av[2] = av[2];
  new_av[3] = av[3];
  new_av[4] = "sebone";

  return do_send_bit_common(5, new_av);
}

static int do_send_bit_jtag(int ac, const char** av)
{
  const char* new_av[5];

  LOG("do_send_bit_jtag()\n");
  if (ac != 4)
  {
    LOG_ERROR("missing arguments\n");
    return -1;
  }

  new_av[0] = av[0];
  new_av[1] = av[1];
  new_av[2] = av[2];
  new_av[3] = av[3];
  new_av[4] = "jtag";

  return do_send_bit_common(5, new_av);
}

static int do_send_file(int ac, const char** av)
{
  const char* self_path;
  const char* file_path;
  const char* dest_path;
  const char* dev_addr;
  const char* new_av[5];
  char* efpak_path;
  int err = -1;

  LOG("do_send_file()\n");

  if (ac != 5)
  {
    LOG_ERROR("missing arguments\n");
    return -1;
  }

  self_path = av[0];
  file_path = av[2];
  dest_path = av[3];
  dev_addr = av[4];

  efpak_path = _tempnam("/tmp", NULL);
  if (efpak_path == NULL)
  {
    LOG_ERROR("unable to create temporary file\n");
    goto on_error_0;
  }

  new_av[0] = self_path;
  new_av[1] = "create";
  new_av[2] = efpak_path;
  if (do_create(3, new_av)) goto on_error_1;

  new_av[0] = self_path;
  new_av[1] = "add_file";
  new_av[2] = efpak_path;
  new_av[3] = file_path;
  new_av[4] = dest_path;
  if (do_add_file(5, new_av)) goto on_error_2;

  new_av[0] = self_path;
  new_av[1] = "send";
  new_av[2] = efpak_path;
  new_av[3] = dev_addr;
  if (do_send(4, new_av)) goto on_error_2;

  err = 0;

 on_error_2:
  _unlink(efpak_path);
 on_error_1:
  free(efpak_path);
 on_error_0:
  return err;
}

static int do_send_controller(int ac, const char** av)
{
  const char* self_path;
  const char* file_path;
  const char* dev_addr;
  const char* new_av[5];
  int err = -1;

  LOG("do_send_controller()\n");

  if (ac != 4)
  {
    LOG_ERROR("missing arguments\n");
    return -1;
  }

  self_path = av[0];
  file_path = av[2];
  dev_addr = av[3];

  new_av[0] = self_path;
  new_av[1] = "send_file";
  new_av[2] = file_path;
  new_av[3] = "/app/bin/dance/main";
  new_av[4] = dev_addr;
  if (do_send_file(5, new_av)) goto on_error_0;

  new_av[0] = self_path;
  new_av[1] = dev_addr;
  new_av[2] = "SOFT";
  if (do_reboot(3, new_av)) goto on_error_0;

  err = 0;

 on_error_0:
  return err;
}

static int do_help(int ac, const char** av)
{
  const char* const usage =
    ". list package contents: \n"
    " efpak list \n"
    "\n"
    ". print package version: \n"
    " efpak version \n"
    "\n"
    ". create a new package: \n"
    " efpak create efpak_path \n"
    "\n"
    ". adding contents: \n"
    " efpak add_disk efpak_path disk_path \n"
    " efpak add_part efpak_path part_path {boot,root,app} fs_type \n"
    " efpak add_file efpak_path src_path dst_path \n"
    " efpak add_bit efpak_path src_path \n"
    " efpak add_dir efpak_path src_path dst_path \n"
    " efpak add_hook efpak_path data_path {now,prex,postx,compl,mbr} \n"
    " efpak add_meta efpak_path meta_name {file_path|data_string} \n"
    "\n"
    ". extracting contents: \n"
    " efpak extract efpak_path dest_dir \n"
    "\n"
    ". local disk install: \n"
    " efpak install efpak_path {root,disk_name(mmcblk0,sdd...)} \n"
    "\n"
    ". send package to instrument: \n"
    " efpak send efpak_path dev_addr \n"
    "\n"
    ". send bitstream to instrument: \n"
    " efpak send_bit bit_path dev_addr \n"
    " efpak send_sebone_bit bit_path dev_addr \n"
    " efpak send_jtag_bit bit_path dev_addr \n"
    "\n"
    ". send file to instrument: \n"
    " efpak send_file file_path dest_path dev_addr \n"
    "\n"
    ". send then restart controller program: \n"
    " efpak send_controller file_path dev_addr \n"
    "\n"
    ". increase debug print out:\n"
    " efpak xxxxx [-d [-d [-d]]] \n"
    ;

  printf("================\n%s\n", app_ident);
  printf("================\n");
  printf(". about efpak version:\n %s\n %s\n\n%s\n",
    SvnRevision, SvnDate, usage);

  return -1;
}

pthread_t     dot_thread;
int           dot_alive;

void *dot_loop(void *notused)
{
 int i;
 for(i = 0; dot_alive; i++)
 {
  if(i >= 60)
  {
    printf("\r%-60s\r", "");
    i = 0;
  }
  printf(".");
  fflush(stdout);
  sleep(1);
 }

 pthread_exit(NULL);
}

void dot_start(void)
{
  /* do not mess with debug messages */
  if(debug)
    return;

  dot_alive = 1;
  pthread_create(&dot_thread, NULL, dot_loop, NULL);
  pthread_detach(dot_thread);
}

void dot_end(void)
{
  if(debug)
    return;
  dot_alive = 0;
  pthread_join(dot_thread, NULL);
  printf("\r%-60s\r", "");
  fflush(stdout);
}

int main(int ac, const char** av)
{
  static const struct
  {
    const char* name;
    int (*fn)(int, const char**);
#define OP_FLAG_PRINT_STATUS (1 << 0)
    uint32_t flags;
  } ops[] =
  {
    { "list", do_list, OP_FLAG_PRINT_STATUS },
    { "version", do_version, 0 },
    { "create", do_create, OP_FLAG_PRINT_STATUS },
    { "add_disk", do_add_disk, OP_FLAG_PRINT_STATUS },
    { "add_part", do_add_part, OP_FLAG_PRINT_STATUS },
    { "add_file", do_add_file, OP_FLAG_PRINT_STATUS },
    { "add_bit", do_add_bit, OP_FLAG_PRINT_STATUS },
    { "add_dir", do_add_dir, OP_FLAG_PRINT_STATUS },
    { "add_hook", do_add_hook, OP_FLAG_PRINT_STATUS },
    { "add_meta", do_add_meta, OP_FLAG_PRINT_STATUS },
    { "extract", do_extract, OP_FLAG_PRINT_STATUS },
    { "install", do_install, OP_FLAG_PRINT_STATUS },
    { "send", do_send, OP_FLAG_PRINT_STATUS },
    { "send_bit", do_send_bit_local, OP_FLAG_PRINT_STATUS },
    { "send_sebone_bit", do_send_bit_sebone, OP_FLAG_PRINT_STATUS },
    { "send_jtag_bit", do_send_bit_jtag, OP_FLAG_PRINT_STATUS },
    { "send_file", do_send_file, OP_FLAG_PRINT_STATUS },
    { "send_controller", do_send_controller, OP_FLAG_PRINT_STATUS },
    { "help", do_help, 0 }
  };

  static const size_t n = sizeof(ops) / sizeof(ops[0]);
  size_t i;
  int err;

  /* count debug options at the end of argins */
  for (i = (ac-1); i > 0; i--)
  {
    if (strcmp(av[i], "-d") == 0)
    {
      debug++;
      ac--;
    }
    else
      break ;
  }
#ifdef CONFIG_LIBDEEP
  deepdev_setparam(LIB_DEFAULTS, "DEBUG", DDPAR(debug));
#endif

  if (ac <= 2)
  {
    i = n - 1;
    goto on_help;
  }



  for (i = 0; i != n; ++i)
  {
    if (strcmp(ops[i].name, av[1]) == 0) break ;
  }
  if (i == n) i = n - 1;

 on_help:
  LOG("%s\n", ops[i].name);
  dot_start();
  err = ops[i].fn(ac, av);
  dot_end();
  if (ops[i].flags & OP_FLAG_PRINT_STATUS)
  {
    if (err) printf("failure\n");
    else printf("success\n");
  }
  return err;
}
