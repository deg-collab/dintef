#!/bin/sh

# Module object to be loaded
#DD_PATH="/usr/sbin/"
DD_PATH="./"
DD="rscma"
DD_VERB=1

# Device descriptor name (can not be in /dev because RO file system)
DEV_DD="/dev/rscma"


case $1 in
   start)

      echo -n "Loading ${DD} driver.........."

      # first load the module of DD
      insmod ${DD_PATH}${DD}.ko v=${DD_VERB}
      if [ $? -eq 1 ]; then exit; fi

      # get the major number dynamically given to the module
      major=`cat /proc/devices|grep -i ${DD} | cut -d " " -f1`

      # create device descriptor files
      /bin/rm -f ${DEV_DD}* 2>/dev/null
      mknod ${DEV_DD} c ${major} 0
      if [ $? -eq 1 ]; then exit; fi
      chmod a+rw ${DEV_DD}

      echo "done"
      ;;

   stop)
      echo -n "Removing ${DD} driver.........."
      rmmod ${DD} 2>/dev/null
      if [ $? -eq 1 ]; then echo "fail"; else echo "done"; fi
      ;;

   restart)
      $0 stop
      sleep 1
      $0 start
      ;;

   *)
      echo "Usage:  $name {start | stop | restart}"
      ;;
esac      

