
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include "librscma.h"

#define __PERROR(str, ...) \
   printf("[!] %s,%d: " str ": %s\n", __FILE__, __LINE__, ## __VA_ARGS__,  strerror(errno))
#define LOG_TRACE \
   printf("[!] %s,%d: TRACE\n", __FILE__, __LINE__)


int main(int ac, char** av) {
   rscmatbl_t    table = RSCMA_TABLE_INITIALIZER;
   rscmabuf_t*   bufdata;
   uint8_t*      buf0;
   uint8_t*      buf1;
   size_t        blockn = 8;
   size_t        blocksz = 8*(4 * 1024 * 1024L);
   unsigned int  align_shift = 0;
   int           node_id = ANY_NODE;
   unsigned int  typflgs;
   size_t        i;

   if (rscma_load(NULL)) {
      __PERROR("failed to load module from default location");
      /* keep going anyhow, module may be already loaded in memory */
      if (rscmabuf_bufinfo(NULL, NULL, NULL, NULL, NULL, NULL)) {
         __PERROR("and cannot open device");
         goto on_error_0;
      } else {
         __PERROR("anyhow module was already in memory");
      }
   }
   printf("<<<<--------------------------\n");

   /*  alloc buffer using the first available method */
   bufdata = rscmabuf_alloc(blockn, blocksz, align_shift, node_id, RSCMA_ANY_METHOD | RSCMA_SIZE_IN_BLOCKS);
   printf(">>>-------------------------\n");

   if (bufdata == NULL) {
      __PERROR("buffer allocation failed");
      goto on_error_1;
   }
   typflgs = RSCMA_SIZE_IN_BLOCKS;
   rscmabuf_bufinfo(bufdata, &blockn, &blocksz, &align_shift, &node_id, &typflgs);

   printf("N of blocks: %zd\n", blockn);
   printf("Block size : 0x%016zx (%zd)\n", blocksz, blocksz);
   printf("Total size : 0x%016zx (%zd)\n", blockn * blocksz, blockn * blocksz);
   printf("Alignment  : 0x%016zx\n", (size_t)1 << align_shift);
   printf("Node ID    : %d\n", node_id);
   printf("FLAGS      : 0x%08x\n", typflgs);
   printf("\n");

   if (rscma_fill_table(&table, bufdata)) {
      __PERROR("buffer table retrieval failed");
      goto on_error_1;
   }

   printf("-- page allocation success: 0x%zx (%zd)\n", table.tot_size, table.tot_size);

   blockn = 0;
   printf("--------------------------\n");
   for (i = 0; i < table.nslots; ++i) {
      cmaslot_t* slot = &table.slot[i];
      uintptr_t paddr = slot->paddr;
      printf("---- [%zd: %d blocks @ 0x%" PRIxPTR "]\n", i, slot->nblocks, slot->paddr);
      printf("[%02zu] 0x%016" PRIxPTR "\n", blockn, paddr);
      blockn += slot->nblocks;
      paddr += slot->nblocks * table.block_size;
      if (slot->nblocks > 2) printf("     ...\n");
      if (slot->nblocks > 1)
         printf("[%02zu] 0x%016" PRIxPTR "\n", blockn - 1, paddr - table.block_size);
   }
   printf("--------------------------\n\n");
   rscma_empty_table(&table);
   printf("- empty table ------------\n\n");
//goto on_error_2;
/*
   if ((buf0 = rscmabuf_map(bufdata)) == NULL) {
      __PERROR("virtual memory mapping failed");
      goto on_error_2;
   }
   printf("-- page mapping [0] success: %p\n", buf0);

   if ((buf1 = rscmabuf_map(bufdata)) == NULL) {
      __PERROR("virtual memory mapping failed");
      goto on_error_3;
   }
   printf("-- page mapping [1] success: %p\n", buf1);

   for (i = 0; i < size; ++i) {
      buf0[i] = (uint8_t)i;
   }

   for (i = 0; i < size; ++i) {
      if (buf1[i] != (uint8_t)i) {
         printf("invalid at %zu (0x%02x)\n", i, buf1[i]);
         break ;
      }
   }
   printf("-- verification succeeded\n\n");
   rscmabuf_unmap(bufdata, buf1);
 on_error_3:
   rscmabuf_unmap(bufdata, buf0);
 on_error_2:
*/
   rscmabuf_free(bufdata);
 on_error_1:
//   rscma_unload();
 on_error_0:
   return 0;
}
