
#ifndef __LIBRSCMA_H_INCLUDED__
#define __LIBRSCMA_H_INCLUDED__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*

*/


// rscma types and utility macros

typedef struct rscmabuf rscmabuf_t;

typedef struct {
   uintptr_t paddr;
   uint32_t  nblocks;
} cmaslot_t;

typedef struct {
   size_t       tot_size;
   size_t       block_size;
   uint32_t     page_order;
   unsigned int nslots;
   cmaslot_t*   slot;
} rscmatbl_t;

#define RSCMA_TABLE_INITIALIZER {0, 0, 0, 0, NULL}



// rscma library API

// allocator type
#define RSCMA_STDPAGES        0x0080
#define RSCMA_BOOT_RESERVED   0x0040
#define RSCMA_CMA             0x0020
#define RSCMA_HUGE_PAGES      0x0010
#define RSCMA_ANY_METHOD      0x0000

// allocation flags
#define RSCMA_USE_DMA32BIT    0x0100
#define RSCMA_USE_NODE_ID     0x0200
#define RSCMA_PARTIAL_ALLOC   0x0400


#define RSCMA_SIZE_IN_BLOCKS  0x800000
#define RSCMA_SIZE_STRICT     0x400000
#define RSCMA_SIZE_RELAXED    0x200000

#define ANY_NODE        ((int)(-1))
#define CURRENT_NODE    ((int)(-2))

// parameters for persistence management
#define APPID_ANONYMOUS  ((int64_t)(-1))
#define NO_DELAY         ((int)0)
#define FOREVER          ((int)-1)


int   rscma_load(const char* module_path);
int   rscma_unload(void);

rscmabuf_t*
      rscmabuf_alloc(size_t size, size_t blksize, unsigned int align_shift,
                                        int node_id, unsigned int type_flags);
int   rscmabuf_free(rscmabuf_t* blkset);
int   rscmabuf_sync(const rscmabuf_t* blkset);
void* rscmabuf_map(const rscmabuf_t* blkset);
int   rscmabuf_unmap(const rscmabuf_t* blkset, void* vaddr);

int   rscmabuf_set_persistence(const rscmabuf_t* blkset, uint64_t app_id, int delay);

int   rscmabuf_bufinfo(const rscmabuf_t* blkset, size_t* size, size_t* blksize,
                                                 unsigned int* align_shift,
                                                 int* node_id, unsigned int* typflgs);
int   rscmabuf_datainfo(const rscmabuf_t* blkset, size_t* tot_size,
                                                  unsigned int* nbloks,
                                                  unsigned int* nslots);

rscmatbl_t* rscma_init_table(rscmatbl_t* table);
int         rscma_fill_table(rscmatbl_t* table, const rscmabuf_t* blkset);
int         rscma_empty_table(rscmatbl_t* table);


#endif /* __LIBRSCMA_H_INCLUDED__ */
