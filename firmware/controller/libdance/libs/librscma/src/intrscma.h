
#ifndef __INTRSCMA_H_INCLUDED__
#define __INTRSCMA_H_INCLUDED__

#include "librscma.h"

/* define RSCMA_DEBUG to activate debug, trace and assert messages */
#define RSCMA_DEBUG

#include "k/rscma_ioctl.h"


struct rscma_globals_s {
   int   dev_fd;
   char* dev_path;
};

extern struct rscma_globals_s rscma_globals;

int lib_check_and_init(void);
int lib_check_only(void);


// STATIC_ASSERT() macro to assert logic conditions at compile time
#define _ASSERT_CONCAT_(a, b) a##b
#define _ASSERT_CONCAT(a, b) _ASSERT_CONCAT_(a, b)
#define STATIC_ASSERT(condition) \
    enum { _ASSERT_CONCAT(static_assert_, __COUNTER__) = 1/(!!(condition)) };


#endif /* __INTRSCMA_H_INCLUDED__ */
