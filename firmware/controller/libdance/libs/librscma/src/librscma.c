
#include <stddef.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

#include "intrscma.h"

// assertions to guarantee consistency between library and kernel module
//
STATIC_ASSERT(__STDPAGES == RSCMA_STDPAGES)
STATIC_ASSERT(__BOOT_RESERVE == RSCMA_BOOT_RESERVED)
STATIC_ASSERT(__CMA == RSCMA_CMA)
STATIC_ASSERT(__HUGE_PAGES == RSCMA_HUGE_PAGES)

STATIC_ASSERT(__FLG_USE_DMA32BITS == RSCMA_USE_DMA32BIT)
STATIC_ASSERT(__FLG_USE_NODE_ID == RSCMA_USE_NODE_ID)
STATIC_ASSERT(__FLG_PARTIAL_ALLOC == RSCMA_PARTIAL_ALLOC)

STATIC_ASSERT(sizeof(cmaslot_t) >= sizeof(rscam_slot_t))


struct rscmabuf {
   uintptr_t    blkset_id;
   size_t       block_size;
   unsigned int block_count;
   uint32_t     align_shift;
   unsigned int slot_count;
   unsigned int flags;
   int          node_id;
   int          type;
//   uintptr_t*   block_ptr;
};

__attribute__((unused))
static int check_valid_mblock_size(size_t sz) {
   size_t szm = sz - 1;
   return (sz ^ szm) == (sz + szm)? 0 : -1;
}

static inline size_t total_buffer_size(const rscmabuf_t* blkset) {
   return blkset->block_size * blkset->block_count;
}


// ioctl calls to the kernel module
static int ioctl_get_mblockset_info(const rscmabuf_t* blkset, unsigned int* typflgs, size_t* blksize,
                                    unsigned int* align_shift, int* node_id) {
   rscma_ioctl_get_mblockset_info_t arg;

   arg.blkset_id = blkset? blkset->blkset_id : 0;
   arg.mblock_type = typflgs? *typflgs & __TYPEMASK : __ALLTYPES;
   arg.mblock_size = blksize? *blksize : 0;
   arg.mblock_count = 0;
   arg.mslot_count = 0;
   arg.flags = typflgs? *typflgs & __ALLFLAGS : 0;
   arg.align_shift = align_shift? *align_shift : 0;
   arg.node_id = node_id? *node_id : ANY_NODE;

   if (ioctl(rscma_globals.dev_fd, RSCMA_IOCTL_GET_MBLOCKSET_INFO, &arg)) {
      LOG_ERRNO();
   } else {
      if (typflgs) *typflgs = arg.mblock_type | arg.flags;
      if (blksize) *blksize = arg.mblock_size;
      if (align_shift) *align_shift = arg.align_shift;
      if (node_id) *node_id = arg.node_id;
   }
   return -errno;
}


static int ioctl_alloc_mblockset(rscmabuf_t* blkset,
                                 unsigned int type_flags,
                                 size_t blksize,
                                 size_t blk_n,
                                 size_t align_shift,
                                 int    node_id) {
   rscma_ioctl_alloc_mblockset_t arg;
LOG_TRACE;
   arg.mblock_type = type_flags & __TYPEMASK;
   arg.flags       = type_flags & __ALLFLAGS;
   arg.mblock_size = blksize;
   arg.mblock_count = blk_n;
   arg.align_shift = align_shift;
   arg.node_id = node_id;

   if (ioctl(rscma_globals.dev_fd, RSCMA_IOCTL_ALLOC_MBLOCKSET, &arg)) {
      LOG_ERRNO();
   } else {
      blkset->blkset_id = arg.blkset_id;

      blkset->type = arg.mblock_type;
      blkset->flags = arg.flags;
      blkset->block_size = arg.mblock_size;
      blkset->block_count = arg.mblock_count;
      blkset->node_id = arg.node_id;
      blkset->align_shift = arg.align_shift;
      blkset->slot_count = arg.mslot_count;
LOG_DEBUG("slot_count: %d\n", blkset->slot_count);
LOG_DEBUG("block_count: %d\n", blkset->block_count);
LOG_DEBUG("block_size: %zd\n", blkset->block_size);
   }
   return -errno;
}

static int ioctl_free_mblockset(const rscmabuf_t* blkset) {
   rscma_ioctl_free_mblockset_t arg;

   arg.blkset_id = blkset->blkset_id;

   if (ioctl(rscma_globals.dev_fd, RSCMA_IOCTL_FREE_MBLOCKSET, &arg)) {
      LOG_ERRNO();
   }
   return -errno;
}

static int ioctl_set_mblockset_persistence(const rscmabuf_t* blkset, uint64_t app_id, int delay) {
   rscma_ioctl_set_persistence_t arg;

   arg.blkset_id = blkset->blkset_id;
   arg.application_id = app_id;
   arg.delay = delay;

   if (ioctl(rscma_globals.dev_fd, RSCMA_IOCTL_SET_PERSISTENCE, &arg)) {
      LOG_ERRNO();
   }
   return -errno;
}

static int ioctl_get_mblockset_table(const rscmabuf_t* blkset, cmaslot_t* slots) {
   rscma_ioctl_get_mblockset_table_t arg;

   arg.blkset_id = blkset->blkset_id;
   arg.mslot_count = blkset->slot_count;
   arg.mslots = (rscam_slot_t*)slots;
   arg.mslots_size = blkset->slot_count * sizeof(cmaslot_t);

   if (ioctl(rscma_globals.dev_fd, RSCMA_IOCTL_GET_MBLOCKSET_TABLE, &arg)) {
      LOG_ERRNO();
   }
LOG_DEBUG("page_shift: %d\n", arg.page_shift);
   // re-copy/transfer data from arg.mslots[] to slots[] to accommodate by the
   // size differences between the rscam_slot_t and cmaslot_t and to convert to
   // actual physical addresses.
   // copy data backwards to not overwrite good data.
   while(arg.mslot_count--) {
      slots[arg.mslot_count].nblocks = arg.mslots[arg.mslot_count].nblocks;
      slots[arg.mslot_count].paddr =
            (uintptr_t)arg.mslots[arg.mslot_count].paddr_idx << arg.page_shift;
   }
   return -errno;
}

static int ioctl_map_mblockset(const rscmabuf_t* blkset, const void* uvaddr) {
   rscma_ioctl_map_mblockset_t arg;

   arg.blkset_id = blkset->blkset_id;
   arg.uvaddr = (uintptr_t)uvaddr;

   if (ioctl(rscma_globals.dev_fd, RSCMA_IOCTL_MAP_MBLOCKSET, &arg)) {
      LOG_ERRNO();
   }
   return -errno;
}

static int ioctl_sync_mblockset(const rscmabuf_t* blkset) {
   rscma_ioctl_sync_mblockset_t arg;

   arg.blkset_id = blkset->blkset_id;

   if (ioctl(rscma_globals.dev_fd, RSCMA_IOCTL_SYNC_MBLOCKSET, &arg)) {
      LOG_ERRNO();
   }
   return -errno;
}


rscmabuf_t* rscmabuf_alloc(size_t size, size_t blksize, unsigned int align_shift, int node_id, unsigned int type_flags) {
   rscmabuf_t* blkset;
   size_t      n_blocks;

   if (lib_check_and_init())
      return NULL;

   if (size == 0) {
      errno = EINVAL;
      LOG_ERRNO();
      goto on_error_0;
   }
LOG_TRACE;
   // check and update type, flags and blocksize (may be zero here)
   if (ioctl_get_mblockset_info(NULL, &type_flags, &blksize, &align_shift, &node_id))
      goto on_error_0;

   if (type_flags & RSCMA_SIZE_IN_BLOCKS) {
      n_blocks = size;
   } else {
      // blksize may have been updated by rscmabuf_bufinfo()
      n_blocks = ((size - 1) / blksize) + 1;
   }

LOG_TRACE;
   blkset = malloc(sizeof(rscmabuf_t));
   if (blkset == NULL) {
      LOG_ERRNO();
      goto on_error_0;
   }

   if (ioctl_alloc_mblockset(blkset, type_flags, blksize, n_blocks, align_shift, node_id))
      goto on_error_1;

LOG_TRACE;
   return blkset;

 on_error_1:
LOG_TRACE;
   free(blkset);
 on_error_0:
   return NULL;
}


int rscmabuf_free(rscmabuf_t* blkset) {
   if (lib_check_only())
      return -errno;

   if (ioctl_free_mblockset(blkset))
      return -errno;

   free(blkset);
   return -errno;
}


int rscmabuf_sync(const rscmabuf_t* blkset) {
   if (lib_check_only())
      return -errno;

   ioctl_sync_mblockset(blkset);
   return -errno;
}


void* rscmabuf_map(const rscmabuf_t* blkset) {
   const size_t size = total_buffer_size(blkset);
   void*        uvaddr;

   if (lib_check_only())
      return NULL;

   uvaddr = mmap(NULL, size, PROT_READ | PROT_WRITE,
                 MAP_SHARED | MAP_NORESERVE | MAP_ANONYMOUS,
                 -1, 0);
   if (uvaddr == MAP_FAILED) {
      LOG_ERRNO();
      return NULL;
   }

   if (ioctl_map_mblockset(blkset, uvaddr)) {
      int err = errno;
      munmap(uvaddr, size);
      errno = err;
      return NULL;
   }
   return uvaddr;
}


int rscmabuf_unmap(const rscmabuf_t* blkset, void* vaddr) {
   const size_t size = total_buffer_size(blkset);

   if (lib_check_only())
      return -errno;

   if (munmap(vaddr, size)) {
      LOG_ERRNO();
   } else {
      errno = 0;
   }
   return -errno;
}

int rscmabuf_set_persistence(const rscmabuf_t* blkset, uint64_t app_id, int delay) {
   if (lib_check_and_init())
      return -errno;

   ioctl_set_mblockset_persistence(blkset, app_id, delay);
   return -errno;
}

int rscmabuf_bufinfo(const rscmabuf_t* blkset, size_t* size, size_t* blksize, unsigned int* align_shift,
                                        int* node_id, unsigned int* typflgs) {
   if (lib_check_and_init())
      return -errno;

   if (blkset == NULL) {
      int          nodeid = node_id? *node_id : ANY_NODE;

      if (size) *size = 0;

      ioctl_get_mblockset_info(NULL, typflgs, blksize, align_shift, &nodeid);
      return -errno;

   } else {
      int size_in_blocks = typflgs && (*typflgs & RSCMA_SIZE_IN_BLOCKS);
      if (size)
         *size = size_in_blocks? blkset->block_count : blkset->block_count * blkset->block_size;

      if (blksize) *blksize = blkset->block_size;
      if (align_shift) *align_shift = blkset->align_shift;
      if (node_id) *node_id = blkset->node_id;
      if (typflgs) *typflgs = blkset->type | blkset->flags;

      return 0;
   }
}


int rscmabuf_datainfo(const rscmabuf_t* blkset, size_t* tot_size,
                                                unsigned int* nbloks,
                                                unsigned int* nslots) {
   if (tot_size)
      *tot_size = total_buffer_size(blkset);
   if (nbloks)
      *nbloks = blkset->block_count;
   if (nslots)
      *nslots = blkset->slot_count;
   return 0;
}



static int is_valid_and_empty(const rscmatbl_t* table) {
   return (table && table->slot == NULL && table->nslots == 0);
}

static int is_valid_and_busy(const rscmatbl_t* table) {
   return (table && table->slot != NULL && table->nslots != 0);
}

static void clean_table(rscmatbl_t* table) {
   table->tot_size = 0;
   table->block_size = 0;
   table->nslots = 0;
   table->slot = NULL;
}


rscmatbl_t* rscma_init_table(rscmatbl_t* table) {
   if (table == NULL) {
      table = malloc(sizeof(table));
      if (table == NULL) {
         errno = -ENOMEM;
         return NULL;
      }
   }
   clean_table(table);
   errno = 0;
   return table;
}

int rscma_fill_table(rscmatbl_t* table, const rscmabuf_t* blkset) {

   if (lib_check_only())
      return -errno;

   if (blkset == NULL) {
      clean_table(table);
      errno = 0;

   } else if (is_valid_and_empty(table)) {
      table->slot = malloc(blkset->slot_count * sizeof(*table->slot));
      if (table->slot == NULL) {
         errno = -ENOMEM;
         LOG_ERRNO();
      } else {
         if (ioctl_get_mblockset_table(blkset, table->slot)) {
            LOG_ERRNO();
            free(table->slot);
            table->slot = NULL;
         } else {
            table->tot_size = total_buffer_size(blkset);
            table->block_size = blkset->block_size;
            table->page_order = blkset->align_shift;
            table->nslots = blkset->slot_count;
         }
      }
   } else {
      errno = -EINVAL;
      LOG_ERRNO();
   }
   return -errno;
}


int rscma_empty_table(rscmatbl_t* table) {
   if (is_valid_and_busy(table)) {
      free(table->slot);
      clean_table(table);
      errno = 0;
   } else {
      errno = -EINVAL;
      LOG_ERRNO();
   }
   return -errno;
}
