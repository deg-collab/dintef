

#include <linux/init.h>
#include <linux/pci.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/poll.h>
#include <linux/mm.h>
#include <linux/mmzone.h>
#include <linux/rmap.h>
#include <linux/pagemap.h>
#include <asm/mman.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/pci.h>
#include <linux/ioport.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <asm/io.h>

#if defined(CONFIG_FREESCALE_IMX6)
#include <asm/tlbflush.h>
#include <asm/cacheflush.h>
#include <asm/outercache.h>
#endif /* CONFIG_FREESCALE_IMX6 */

#include "cmalloc.h"


/* contiguous physical memory allocators (rscma_xxx api) */
/* http://lwn.net/Articles/396702 */
/* http://lwn.net/Articles/447405 */


/* linux module interface */

MODULE_DESCRIPTION("RSCMA driver");
MODULE_AUTHOR("RSCMA team");
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,0)
MODULE_LICENSE("Dual BSD/GPL");
#endif

#define RSCMA_NAME KBUILD_MODNAME


/* per {driver,dev,file} types */



typedef struct rscma_dev {
   /* char device is contained for o(1) lookup */
   struct cdev cdev;
} rscma_dev_t;

typedef struct rscma_file {
   rscma_dev_t* dev;
   rscma_data_t data;
} rscma_file_t;

typedef struct rscma_driver {
   /* device storage */
   rscma_dev_t dev;

   /* device number */
   int major_minor;

} rscma_driver_t;


/* RSCMA per driver data (global, singleton) */

static rscma_driver_t rscma_driver;

static inline void rscma_driver_init(rscma_driver_t* d) {
}

static rscma_dev_t* rscma_dev_alloc(void) {
  return &rscma_driver.dev;
}

static inline void rscma_dev_free(rscma_dev_t* dev) {
}

/* blockset management functions */

static mblkset_t* find_mblockset(mblkset_t* head, uintptr_t id) {
   mblkset_t* blkset_id = (mblkset_t*)id;
   mblkset_t* blkset;

   /* find blockset identified by id */
   for (blkset = head; blkset != NULL && blkset != blkset_id; blkset = blkset->next)
      ;

   return blkset;
}

static int remove_mblockset(mblkset_t** head, mblkset_t* blkset) {
   /* delete a given blockset (assumes mblocks have been freed already) */

   mblkset_t* curblkset;
   mblkset_t* prev;

   for(prev = NULL, curblkset = *head; curblkset; prev = curblkset, curblkset = curblkset->next) {
      if (curblkset == blkset) {
         if (prev)
            prev->next = curblkset->next;
         else
            *head = curblkset->next;

         if (curblkset->mslot_table)
            kfree(curblkset->mslot_table);
         kfree(curblkset);
         return 0;
      }
   }
   /* not found */
   return -1;
}

static int add_mblockset(mblkset_t** head, size_t block_size,
                                           unsigned int align_shift,
                                           unsigned int block_count,
                                           unsigned int pflags,
                                           int node_id) {
   /* add a blockset entry with order, pflags, mblock_count */
   mblkset_t* blkset;

   blkset = kmalloc(sizeof(mblkset_t), GFP_KERNEL);
   if (blkset == NULL)
      return -1;


   blkset->mblock_count = block_count;
   blkset->mblock_size = block_size;
   blkset->align_shift = align_shift;

   blkset->mblock_type = 0;
   blkset->mblock_flags = pflags;
   blkset->node_id = node_id;
   blkset->mslot_count = 0;
   blkset->page_shift = 0;
   blkset->mslot_table = NULL;

   blkset->next = *head;
   *head = blkset;

   return 0;
}


static void free_mblockset(rscma_data_t* rscma_data, mblkset_t* blkset) {
   free_mblockset_data(rscma_data, blkset);
   remove_mblockset(&rscma_data->blkset_head, blkset);
}

static void free_all_mblocksets(rscma_data_t* rscma_data) {
   mblkset_t* head = rscma_data->blkset_head;

   /* free all the blocksets in the list */
   while (head != NULL)
      free_mblockset(rscma_data, head);
}


/* ioctl handler */

static void unlock_user_pages_common(struct page** upage_table, size_t npages, unsigned int is_rw) {
   size_t i;

   for (i = 0; i < npages; ++i) {
      struct page* const page = upage_table[i];

      down_read(&current->mm->mmap_sem);

      if (is_rw && !PageReserved(page))
         SetPageDirty(page);
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 6, 0)
      put_page(page);
#else
      page_cache_release(page);
#endif

      up_read(&current->mm->mmap_sem);
   }
   kfree(upage_table);
}

static inline void unlock_user_pages_ro(struct page** upage_table, size_t npages) {
   unlock_user_pages_common(upage_table, npages, 0);
}

static inline void unlock_user_pages_rw(struct page** upage_table, size_t npages) {
   unlock_user_pages_common(upage_table, npages, 1);
}

static int lock_user_pages_common(uintptr_t addr, size_t size, unsigned int is_rw,
                                  struct page*** upage_table, size_t* npages) {
   /* http://lwn.net/Articles/28548 */
   /* ldd3_15_performing_direct_io */
   /* drivers/scsi/st.c */

   const size_t off = addr & (PAGE_SIZE - 1);
   int err;

   size += off;
   addr -= off;

   *npages = ((size - 1) / PAGE_SIZE) + 1;

   *upage_table = kmalloc((*npages) * sizeof(struct page*), GFP_KERNEL);
   if (*upage_table == NULL) return -ENOMEM;

   down_read(&current->mm->mmap_sem);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,6,0)
   err = get_user_pages(addr, *npages, is_rw? FOLL_WRITE : 0, *upage_table, NULL);
#else
   err = get_user_pages(current, current->mm, addr, *npages, is_rw, 0, *upage_table, NULL);
#endif

   up_read(&current->mm->mmap_sem);

   if (err != (int)*npages) {
      unlock_user_pages_common(*upage_table, *npages, is_rw);
      return -1;
   }
   return 0;
}

static inline int lock_user_pages_ro(uintptr_t addr, size_t size,
                                     struct page*** upage_table, size_t* npages) {
   return lock_user_pages_common(addr, size, 0, upage_table, npages);
}

static inline int lock_user_pages_rw(uintptr_t addr, size_t size,
                                     struct page*** upage_table, size_t* npages) {
   return lock_user_pages_common(addr, size, 1, upage_table, npages);
}

// macros for repetitive code in the ioctl switch
//
#define COPY_ARG_FROM_USER(arg) \
   if (copy_from_user(&arg, (void*)(uintptr_t)uaddr, sizeof(arg))) { \
      LOG_ERROR("ioctl argument copy failed\n");  \
      err = -EFAULT; \
      break; \
   }

#define COPY_ARG_TO_USER(arg) \
   if (copy_to_user((void*)(uintptr_t)uaddr, &arg, sizeof(arg))) { \
      LOG_ERROR("ioctl argument copy failed\n");  \
      err = -EINVAL; \
      break; \
   }

#define OBTAIN_BLOCKSET_FROM_ID(bset, bset_id) \
   bset = find_mblockset(rscma_data->blkset_head, bset_id); \
   if (bset == NULL) { \
      LOG_ERROR("blockset identifier not valid\n");  \
      err = -EINVAL;  \
      break; \
   }


#ifdef HAVE_UNLOCKED_IOCTL
long rscma_file_ioctl(struct file* file, unsigned int cmd, unsigned long uaddr) {
   __attribute__((unused)) struct inode* const inode = file->f_path.dentry->d_inode;
#else
static int rscma_ioctl(struct inode* inode, struct file* file, unsigned int cmd, unsigned long uaddr) {
#endif

   rscma_data_t* const rscma_data = &(((rscma_file_t*)file->private_data)->data);
   int err = 0;

   switch (cmd) {
      case RSCMA_IOCTL_GET_MBLOCKSET_INFO: {
         rscma_ioctl_get_mblockset_info_t arg;

         COPY_ARG_FROM_USER(arg);

         if (arg.blkset_id == 0) {
            switch (arg.mblock_type) {
               case __STDPAGES:
                  if (arg.mblock_size && arg.mblock_size != LINUX_MAX_PAGEBLOCK_SIZE) {
                     err = -EINVAL;
                     break;
                  }
                  arg.mblock_size  = LINUX_MAX_PAGEBLOCK_SIZE;
                  arg.mblock_count = 0;
                  arg.mslot_count = 0;
                  arg.flags = 0;
                  break;

               case __BOOT_RESERVE:
               case __CMA:
               case __HUGE_PAGES:
                  break;

               default:
                  break;
            }
            if (err)
               break;
         } else {
            mblkset_t* blkset;

            OBTAIN_BLOCKSET_FROM_ID(blkset, arg.blkset_id);
            arg.mblock_size = blkset->mblock_size;
            arg.mblock_count = blkset->mblock_count;
            arg.mslot_count = blkset->mslot_count;
            arg.flags = blkset->mblock_flags;
            arg.mblock_type = blkset->mblock_type;
         }

         COPY_ARG_TO_USER(arg);
         break;
      }

      case RSCMA_IOCTL_ALLOC_MBLOCKSET: {
         rscma_ioctl_alloc_mblockset_t arg;
         mblkset_t*   mblockset;

         COPY_ARG_FROM_USER(arg);

         /* TODO: check args */
 /*
typedef struct {
   unsigned int     mblock_type;
   size_t           mblock_size;
   uint32_t         align_shift;
   size_t           mblock_count;
   size_t           mslot_count;
   unsigned int     flags;
   int              node_id;
   uintptr_t        blkset_id;
} rscma_ioctl_alloc_mblockset_t;
*/
LOG_VAR(arg.mblock_type, 0x%x);
LOG_VAR(arg.mblock_size, 0x%zx);
LOG_VAR(arg.align_shift, %d);
LOG_VAR(arg.mblock_count, %zd);
LOG_VAR(arg.mslot_count, %zd);
LOG_VAR(arg.flags, 0x%x);
LOG_VAR(arg.node_id, %d);
         if (add_mblockset(&rscma_data->blkset_head, arg.mblock_size,
                                                     arg.align_shift,
                                                     arg.mblock_count,
                                                     arg.flags,
                                                     arg.node_id)) {
            LOG_ERROR("out of memory\n");
            err = -ENOMEM;
            break;
         }
         mblockset = rscma_data->blkset_head;

         if (!arg.mblock_type) arg.mblock_type = __ALLTYPES;
/*
         if (arg.mblock_type & __BOOT_RESERVE) {
            if ((err = alloc_mblockset(rscma_data, mblockset, __BOOT_RESERVE)) == 0)
               goto on_succesful_alloc;
         }

         if (arg.mblock_type & __CMA) {
            if ((err = alloc_mblockset(rscma_data, mblockset, __CMA)) == 0)
               goto on_succesful_alloc;
         }

         if (arg.mblock_type & __HUGE_PAGES) {
            if ((err = alloc_mblockset(rscma_data, mblockset, __HUGE_PAGES)) == 0)
               goto on_succesful_alloc;
         }
*/

         if (arg.mblock_type & __STDPAGES) {
LOG_TRACE;
            if ((err = alloc_mblockset(rscma_data, mblockset, __STDPAGES)) == 0)
               goto on_succesful_alloc;
         }
LOG_DEBUG("mslot_count: %d / 0x%p\n", mblockset->mslot_count, mblockset->mslot_table);
         remove_mblockset(&rscma_data->blkset_head, mblockset);
         break;

 on_succesful_alloc:
LOG_TRACE;
         arg.mblock_type = mblockset->mblock_type;
         arg.align_shift = mblockset->align_shift;
         arg.mslot_count = mblockset->mslot_count;
         arg.mblock_count = mblockset->mblock_count;
         arg.blkset_id = (uintptr_t)mblockset;
         COPY_ARG_TO_USER(arg);

LOG_VAR(arg.mblock_type, 0x%x);
LOG_VAR(arg.mblock_size, %zd);
LOG_VAR(arg.align_shift, %d);
LOG_VAR(arg.mblock_count, %zd);
LOG_VAR(arg.mslot_count, %zd);
LOG_VAR(arg.flags, 0x%x);
LOG_VAR(arg.node_id, %d);

         break;
      }

      case RSCMA_IOCTL_FREE_MBLOCKSET: {
         rscma_ioctl_free_mblockset_t arg;
         mblkset_t*  mblockset;

         COPY_ARG_FROM_USER(arg);
         OBTAIN_BLOCKSET_FROM_ID(mblockset, arg.blkset_id);
         free_mblockset(rscma_data, mblockset);
         break;
      }

      case RSCMA_IOCTL_GET_MBLOCKSET_TABLE: {
         rscma_ioctl_get_mblockset_table_t arg;
         mblkset_t*    mblockset;
         struct page** user_pages;
         size_t        user_npages;
         size_t        user_size;
         uintptr_t     user_addr;

         COPY_ARG_FROM_USER(arg);
         OBTAIN_BLOCKSET_FROM_ID(mblockset, arg.blkset_id);

         if (arg.mslot_count != mblockset->mslot_count) {
            err = -EINVAL;
            break;
         }

         user_addr = (uintptr_t)arg.mslots;
         user_size = arg.mslots_size;
LOG_DEBUG("user_size: %zd / user_addr: 0x%lx\n", user_size, (uintptr_t)user_addr);
         if (lock_user_pages_rw(user_addr, user_size, &user_pages, &user_npages)) {
            err = -EFAULT;
            break;
         }

         memcpy(arg.mslots, mblockset->mslot_table, user_size);

         unlock_user_pages_rw(user_pages, user_npages);

         arg.page_shift = mblockset->page_shift;
         COPY_ARG_TO_USER(arg);
         break;
      }

      case RSCMA_IOCTL_MAP_MBLOCKSET: {
         rscma_ioctl_map_mblockset_t arg;
         struct vm_area_struct*      vma;
         uintptr_t   vma_addr;
         mblkset_t*  mblockset;
         size_t      mblock_size;
         size_t      i;

         COPY_ARG_FROM_USER(arg);
         OBTAIN_BLOCKSET_FROM_ID(mblockset, arg.blkset_id);
         mblock_size = mblockset->mblock_size;

         /* remap on physical mblocks */

         /* TODO: down_write(&current->mm->mmap_sem); */

         /* http://www.makelinux.com/books/lkd2/ch14lev1sec3 */
         vma = find_vma(current->mm, (unsigned long)arg.uvaddr);
         LOG_ASSERT(vma);

#ifndef VM_RESERVED
#   define VM_RESERVED (VM_DONTEXPAND | VM_DONTDUMP)
#endif
         /* linux-2.6.29/drivers/char/s3c_mem.c */
         vma->vm_flags |= VM_RESERVED;
         vma->vm_flags |= VM_IO;
         vma->vm_flags |= VM_PFNMAP;

#if defined(CONFIG_FREESCALE_IMX6)
         vma->vm_page_prot = pgprot_dmacoherent(vma->vm_page_prot);
#endif

         vma_addr = vma->vm_start;

         for (i = 0; i < mblockset->mslot_count; ++i) {
            uintptr_t phys_addr  = SPAGE_IDX_TO_PADDR(mblockset->mslot_table[i].paddr_idx, mblockset->page_shift);
            unsigned int nblocks = mblockset->mslot_table[i].nblocks;

            while (nblocks--) {
               err = remap_pfn_range(vma, vma_addr,
                                  /* non ioremap: page_to_pfn(virt_to_page(kvaddr)) */
                                  /* all, but phys_to_page unavail: phys_to_page(paddr) */
                                  phys_addr >> PAGE_SHIFT, mblock_size, vma->vm_page_prot);
               if (err) break;
               vma_addr += mblock_size;
               phys_addr += mblock_size;
            }
         }

         /* TODO: up_write(&current->mm->mmap_sem); */

         if (err) {
            LOG_ERROR("memory mapping failed\n");
            err = -EFAULT;
            break;
         }
         break;
      }

      case RSCMA_IOCTL_SYNC_MBLOCKSET: {
#if 0 /* TODO */
         rscma_ioctl_sync_mblockset_t arg;
         mblkset_t*  mblockset;
         size_t      mblock_size;
         size_t i;
         unsigned int page_shift;

         COPY_ARG_FROM_USER(arg);
         OBTAIN_BLOCKSET_FROM_ID(mblockset, arg.blkset_id);
         mblock_size = mblock_size;
         page_shift = mblockset->mblock_order + PAGE_SHIFT;

         flush_write_buffers();

         for (i = 0; i < mblockset->mslot_count; ++i) {
            uintptr_t phys_addr  = SPAGE_IDX_TO_PADDR(mblockset->mslot_table[i].paddr_idx, page_shift);
            unsigned int nblocks = mblockset->mslot_table[i].nblocks;

            while (nblocks--) {
               pci_dma_sync_single_for_device(dev->pci_dev, phys_addr,
                                              mblock_size, PCI_DMA_BIDIRECTIONAL);
               phys_addr += mblock_size;
            }
         }
#elif defined(CONFIG_FREESCALE_IMX6)
         __cpuc_flush_kern_all();
         __cpuc_flush_user_all();
         outer_flush_all();
         local_flush_tlb_all();

#endif /* TODO */
         break;
      }

      default: {
         LOG_ERROR("unknown ioctl command\n");
         err = -ENOSYS;
         break;
      }
   }
LOG_TRACE;
   return err;
}

/* LINUX file interface */

static int rscma_file_open(struct inode* inode, struct file* file) {
   rscma_dev_t* const dev = container_of(inode->i_cdev, rscma_dev_t, cdev);
   rscma_file_t* rscma_file;

   rscma_file = kmalloc(sizeof(rscma_file_t), GFP_KERNEL);
   if (rscma_file == NULL) {
      LOG_ERROR("device failed to open\n");
      return -ENOMEM;
   }

   rscma_file->dev = dev;
   rscma_file->data.blkset_head = NULL;
   file->private_data = rscma_file;

   return 0;
}

static int rscma_file_close(struct inode* inode, struct file* file) {
   rscma_file_t* const rscma_file = file->private_data;
   rscma_data_t* const rscma_data = &rscma_file->data;

   free_all_mblocksets(rscma_data);
   kfree(rscma_file);
   file->private_data = NULL;
   return 0;
}

static struct file_operations rscma_fops = {
   .owner = THIS_MODULE,
#ifdef HAVE_UNLOCKED_IOCTL
   .unlocked_ioctl = rscma_file_ioctl,
#else
   .ioctl = rscma_ioctl,
#endif
   .open = rscma_file_open,
   .release = rscma_file_close
};

static int rscma_cdev_init(rscma_dev_t* dev) {
   /* register the chardev and associated file operations */

   const dev_t devno = rscma_driver.major_minor;

   cdev_init(&dev->cdev, &rscma_fops);
   dev->cdev.owner = THIS_MODULE;

   /* nothing to do on failure */
   return cdev_add(&dev->cdev, devno, 1);
}

static inline void rscma_cdev_fini(rscma_dev_t* dev) {
   cdev_del(&dev->cdev);
}


/* LINUX module interface */

static int  __init rscma_init(void);
static void __exit rscma_exit(void);


#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,5,0)
module_init(rscma_init);
module_exit(rscma_exit);
#endif


static int __init rscma_init(void) {
   rscma_dev_t* rscma_dev;
   dev_t first_dev;
   int err = -1;

   rscma_driver_init(&rscma_driver);

   /* try first dynamic allocation */
   err = alloc_chrdev_region(&first_dev, 0, 1, RSCMA_NAME);
   if (err) {
      /* if fails, then static allocation */
      first_dev = MKDEV(DEF_RSCMA_MAJOR, DEF_RSCMA_MINOR);
      err = register_chrdev_region(first_dev, 1, RSCMA_NAME);
   }
   LOG_DEBUG("first_dev: %d:%d (0x%x)\n",  MAJOR(first_dev), MINOR(first_dev), first_dev);

   if (err < 0) {
      LOG_ERROR("device failed to register\n");
      goto on_error_0;
   }
   rscma_driver.major_minor = first_dev;

   rscma_dev = rscma_dev_alloc();
   if (rscma_dev == NULL) {  // this never  fails
      LOG_ERROR("device failed to allocate internal memory\n");
      err = -ENOMEM;
      goto on_error_1;
   }

   err = rscma_cdev_init(rscma_dev);
   if (err) {
      LOG_ERROR("device initialisation failed\n");
      goto on_error_2;
   }

   /* success */
   return 0;

 on_error_2:
   rscma_dev_free(rscma_dev);
 on_error_1:
   unregister_chrdev_region(first_dev, 1);
 on_error_0:
   return err;
}


static void __exit rscma_exit(void) {
   const dev_t first_dev = rscma_driver.major_minor;
   rscma_dev_t* const rscma_dev = &rscma_driver.dev;

   rscma_cdev_fini(rscma_dev);
   rscma_dev_free(rscma_dev);
   unregister_chrdev_region(first_dev, 1);
}
