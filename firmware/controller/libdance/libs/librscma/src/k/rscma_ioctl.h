
#ifndef __RSCMA_IOCTL_H_INCLUDED__
#define __RSCMA_IOCTL_H_INCLUDED__

/*
   This file is shared between the librscma library and the rscma kernel module
*/

#define DEF_RSCMA_MAJOR 0x1b1
#define DEF_RSCMA_MINOR 0

#define RSCMA_IOCTL_MAGIC  'p'
#define RSCMA_IOCTL_BASE 0x000


typedef struct {
   uint32_t paddr_idx;
   uint32_t nblocks;
} rscam_slot_t;


#define __STDPAGES           0x000080
#define __BOOT_RESERVE       0x000040
#define __CMA                0x000020
#define __HUGE_PAGES         0x000010

#define __FLG_USE_DMA32BITS  0x000100
#define __FLG_USE_NODE_ID    0x000200
#define __FLG_PARTIAL_ALLOC  0x000400

#define __FLG_SINGLE_SLOTS   0x000800

#define __FLG_SIZE_IN_BLOCKS 0x800000
#define __FLG_SIZE_STRICT    0x400000
#define __FLG_SIZE_RELAXED   0x200000

#define __TYPEMASK     0x0000ff
#define __ALLFLAGS     0xffff00

#define __ALLTYPES   __TYPEMASK // any type

/* driver ioctls */

#define RSCMA_IOCTL_GET_MBLOCKSET_INFO \
   _IO(RSCMA_IOCTL_MAGIC, RSCMA_IOCTL_BASE + 0)
#define RSCMA_IOCTL_ALLOC_MBLOCKSET \
   _IO(RSCMA_IOCTL_MAGIC, RSCMA_IOCTL_BASE + 1)
#define RSCMA_IOCTL_FREE_MBLOCKSET \
   _IO(RSCMA_IOCTL_MAGIC, RSCMA_IOCTL_BASE + 2)
#define RSCMA_IOCTL_SET_PERSISTENCE \
   _IO(RSCMA_IOCTL_MAGIC, RSCMA_IOCTL_BASE + 3)
#define RSCMA_IOCTL_GET_MBLOCKSET_TABLE \
   _IO(RSCMA_IOCTL_MAGIC, RSCMA_IOCTL_BASE + 4)
#define RSCMA_IOCTL_MAP_MBLOCKSET \
   _IO(RSCMA_IOCTL_MAGIC, RSCMA_IOCTL_BASE + 5)
#define RSCMA_IOCTL_SYNC_MBLOCKSET \
   _IO(RSCMA_IOCTL_MAGIC, RSCMA_IOCTL_BASE + 6)


typedef struct {
   uintptr_t        blkset_id;
   unsigned int     mblock_type;
   size_t           mblock_size;
   size_t           mblock_count;
   uint32_t         align_shift;
   uint32_t         page_shift;
   size_t           mslot_count;
   unsigned int     flags;
   int              node_id;
} rscma_ioctl_get_mblockset_info_t;

typedef struct {
   unsigned int     mblock_type;
   size_t           mblock_size;
   uint32_t         align_shift;
   size_t           mblock_count;
   size_t           mslot_count;
   unsigned int     flags;
   int              node_id;
   uintptr_t        blkset_id;
} rscma_ioctl_alloc_mblockset_t;

typedef struct {
   uintptr_t        blkset_id;
} rscma_ioctl_free_mblockset_t;

typedef struct {
   uintptr_t        blkset_id;
   uint64_t         application_id;
   int              delay;
} rscma_ioctl_set_persistence_t;

typedef struct {
   uintptr_t        blkset_id;
   size_t           mslot_count;
   uint32_t         page_shift;
   size_t           mslots_size;
   rscam_slot_t*    mslots;
} rscma_ioctl_get_mblockset_table_t;

typedef struct {
   uintptr_t        blkset_id;
} rscma_ioctl_sync_mblockset_t;

typedef struct {
   uintptr_t        blkset_id;
   uintptr_t        uvaddr;
} rscma_ioctl_map_mblockset_t;




// log and assert macros
//
#define _ERR_TAG "ERROR: "
#define __FNAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define __LOG_MSG(_pfx, _fmt, ...) do { \
      _LOG_PRINTF(_pfx "%s,%d: " _fmt, __FNAME__, __LINE__, ## __VA_ARGS__); } while(0)

#ifdef RSCMA_DEBUG
  #define _LOG_MSG(_pfx, _fmt, ...) __LOG_MSG(_pfx, _fmt, ## __VA_ARGS__)
#else
  #define _LOG_MSG(_pfx, _fmt, ...)
#endif /* RSCMA_DEBUG */

#ifdef __KERNEL__
   #define _LOG_PRINTF printk
   #define _LOG_ERR __LOG_MSG
   #define _LOG_TAG "[rscma] "
   #define _ERR_PRFX KERN_ERR _LOG_TAG _ERR_TAG
   #define _DBG_PRFX KERN_DEBUG _LOG_TAG
#else
   #define _LOG_PRINTF printf
   #define _LOG_ERR _LOG_MSG
   #define _LOG_TAG "[librscma] "
   #define _ERR_PRFX _LOG_TAG _ERR_TAG
   #define _DBG_PRFX _LOG_TAG
#endif /* __KERNEL__ */

                               /* macros to be used in the user code */
#define LOG_ERRNO() _LOG_ERR(_ERR_PRFX, "%s\n", strerror(errno))
#define LOG_ERROR(_fmt, ...) _LOG_ERR(_ERR_PRFX, _fmt, ## __VA_ARGS__)
#define LOG_DEBUG(_fmt, ...) _LOG_MSG(_DBG_PRFX, _fmt, ## __VA_ARGS__)
#define LOG_TRACE  LOG_DEBUG("TRACE\n")
#define LOG_ASSERT(_c) do { if (!(_c)) LOG_ERROR("ASSERTION: " #_c "\n"); } while(0)
#define LOG_VAR(var, vfmt) LOG_DEBUG(#var ": " #vfmt "\n", var)


#endif /* __RSCMA_IOCTL_H_INCLUDED__ */
