
#ifndef __CMALLOC_H_INCLUDED__
#define __CMALLOC_H_INCLUDED__

#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19))
typedef unsigned long uintptr_t;
#endif

// TODO: use phys_addr_t for paddr everywhere and "%pa"

/* define RSCMA_DEBUG to activate debug, trace and assert messages */
#define RSCMA_DEBUG

#include "../common/rscma_ioctl.h"

/* order and page block size related macros */
#define ORDER_TO_SIZE(order)      ((size_t)PAGE_SIZE << (order))
#define LINUX_MAX_PAGEBLOCK_SIZE  ORDER_TO_SIZE(MAX_ORDER - 1)

#define SPAGE_IDX_TO_PADDR(idx, page_shift) ((uintptr_t)(idx) << (page_shift))
#define PADDR_TO_SPAGEIDX(paddr, page_shift) ((paddr) >> (page_shift));


typedef struct mblkset {
   struct        mblkset* next;
   unsigned int  mblock_count;
   uint16_t      mblock_type;
   int16_t       node_id;
   uint16_t      mblock_flags;
   uint64_t      mblock_size;
   uint32_t      align_shift;
   uint32_t      page_shift;
   unsigned int  mslot_count;
   rscam_slot_t* mslot_table;
} mblkset_t;


typedef struct {
   mblkset_t*   blkset_head;
} rscma_data_t;


// functions in cmalloc.c
int alloc_mblockset(rscma_data_t* rscma_data, mblkset_t* mblockset, unsigned int type);
void free_mblockset_data(rscma_data_t* rscma_data, mblkset_t* mblockset);



#endif /* __CMALLOC_H_INCLUDED__ */
