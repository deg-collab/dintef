

#include <linux/init.h>
#include <linux/pci.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/poll.h>
#include <linux/mm.h>
#include <linux/mmzone.h>
#include <linux/rmap.h>
#include <linux/pagemap.h>
#include <asm/mman.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/interrupt.h>
#include <linux/wait.h>
#include <linux/pci.h>
#include <linux/ioport.h>
#include <linux/sched.h>
#include <asm/uaccess.h>
#include <asm/io.h>

#if defined(CONFIG_FREESCALE_IMX6)
#include <asm/tlbflush.h>
#include <asm/cacheflush.h>
#include <asm/outercache.h>
#endif /* CONFIG_FREESCALE_IMX6 */

#include "cmalloc.h"


/* contiguous physical memory allocators (rscma_xxx api) */
/* http://lwn.net/Articles/396702 */
/* http://lwn.net/Articles/447405 */



/* allocator type */


#ifdef CONFIG_RSCMA_BOOTRESERVE

/* boot time reserved memory (ie. memmap=128M$256M command line option) */
static const size_t rscma_reserved_size = 128 * 1024 * 1024;
static const uintptr_t rscma_reserved_paddr = 256 * 1024 * 1024;

static int alloc_mblock(uintptr_t* paddr, unsigned int order, unsigned int pflags, int nid) {
   /* TODO: bitmap allocator */
   if (size > rscma_reserved_size) return -ENOMEM;
   *paddr = rscma_reserved_paddr;
   return 0;
}

#endif

/* pre-reserved memory allocator */

#define BOOTRESERVE_PAGE_SHIFT PAGE_SHIFT
#define BOOTRESERVE_PAGE_SIZE  (1 << BOOTRESERVE_PAGE_SHIFT)


static int alloc_page_bootreserve(rscma_data_t* rscma_data, mblkset_t* mblockset, unsigned int alloc_order, uintptr_t* paddr) {
   return -1;
}

static void free_pages_bootreserve(rscma_data_t* rscma_data, mblkset_t* blkset,
                                                             unsigned int n_pages,
                                                             unsigned int alloc_order,
                                                             unsigned int page_index) {
}

/* CMA memory allocator */

static int alloc_page_cma(mblkset_t* mblockset, unsigned int alloc_order, uintptr_t* paddr) {
   return -1;
}

static void free_pages_cma(mblkset_t* mblockset, unsigned int n_pages,
                                                 unsigned int alloc_order,
                                                 unsigned int page_index) {
}

/* huge pages memory allocator */

static int alloc_page_hugepages(mblkset_t* mblockset, unsigned int alloc_order, uintptr_t* paddr) {
   return -1;
}

static void free_pages_hugepages(mblkset_t* mblockset, unsigned int n_pages,
                                                       unsigned int alloc_order,
                                                       unsigned int page_index) {
}

/* Linux standard physical memory allocator */

static int alloc_page_standard(mblkset_t* mblockset, unsigned int alloc_order, uintptr_t* paddr) {
   gfp_t      gfp_flags;
   uintptr_t  vaddr;

   if (mblockset->mblock_flags & __FLG_USE_DMA32BITS)
      gfp_flags = GFP_ATOMIC | GFP_DMA32;
   else
      gfp_flags = GFP_ATOMIC;

   if (mblockset->node_id < 0) { /* processor node does not matter */

      vaddr = __get_free_pages(gfp_flags, alloc_order);
      if (vaddr == (uintptr_t)NULL) {
         LOG_ERROR("out of memory\n");
         return -ENOMEM;
      }
   } else { /* allocate on cpu node node_id */
#if (LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19))
      LOG_ERROR("processor node selection not supported\n");  \
      return -EINVAL;
#else
      struct page* const page = alloc_pages_node(mblockset->node_id, gfp_flags, alloc_order);
      if (page == NULL) {
         LOG_ERROR("out of memory\n");
         return -ENOMEM;
      }
      vaddr = (uintptr_t)page_address(page);
#endif
   }
   *paddr = (uintptr_t)virt_to_phys((void*)vaddr);
//LOG_DEBUG("alloc page: 0x%016lx (phys) 0x%016lx (virt)\n", *paddr, vaddr);
   return 0;
}

static void free_pages_standard(mblkset_t* mblockset, unsigned int n_pages,
                                                      unsigned int alloc_order,
                                                      unsigned int page_index) {
   unsigned int page_shift = alloc_order + PAGE_SHIFT;

   while(n_pages--) {
      uintptr_t paddr = SPAGE_IDX_TO_PADDR(page_index++, page_shift);
      const unsigned long vaddr = (unsigned long)(uintptr_t)phys_to_virt(paddr);
//LOG_DEBUG("free 0x%016lx (phys) 0x%016lx (virt)\n", paddr, vaddr);
//return;
      /* equivalent to __free_pages(virt_to_page(vaddr, order) */
      free_pages(vaddr, alloc_order);
   }
}


static int alloc_single_page(rscma_data_t* rscma_data, mblkset_t* mblockset,
                          unsigned int alloc_order, uintptr_t* paddr, unsigned int type) {
   switch(type) {
      case __BOOT_RESERVE: return alloc_page_bootreserve(rscma_data, mblockset, alloc_order, paddr);
      case __CMA:          return alloc_page_cma(mblockset, alloc_order, paddr);
      case __HUGE_PAGES:   return alloc_page_hugepages(mblockset, alloc_order, paddr);
      case __STDPAGES:     return alloc_page_standard(mblockset, alloc_order, paddr);
      default: return -1;
   }
}

static int free_macropages(rscma_data_t* rscma_data, mblkset_t* mblockset, unsigned int n_pages, unsigned int alloc_order, unsigned int index, unsigned int type) {
   switch(type) {
      case __BOOT_RESERVE: free_pages_bootreserve(rscma_data, mblockset, n_pages, alloc_order, index); break;
      case __CMA:          free_pages_cma(mblockset, n_pages, alloc_order, index); break;
      case __HUGE_PAGES:   free_pages_hugepages(mblockset, n_pages, alloc_order, index); break;
      case __STDPAGES:     free_pages_standard(mblockset, n_pages, alloc_order, index); break;
      default:
         return -1;
   }
   return 0;
}


static int alloc_slot_table(mblkset_t* mblockset, unsigned int slot_count) {
   mblockset->mslot_table =
        kmalloc((slot_count * sizeof(*mblockset->mslot_table)), GFP_KERNEL);

   if (mblockset->mslot_table == NULL) {
      mblockset->mslot_count = 0;
      return -1;
   } else {
      mblockset->mslot_count = slot_count;
      return 0;
   }
}

static void free_slot_table(mblkset_t* mblockset) {
   kfree(mblockset->mslot_table);
   mblockset->mslot_table = NULL;
   mblockset->mslot_count = 0;
}


//===============================================================================================


typedef struct slot_s {
   struct slot_s* prev;
   struct slot_s* post;
   unsigned int   pages;
   rscam_slot_t   s;
} slot_t;

typedef struct spool_s {
   slot_t*  pool;
   slot_t*  first;
   slot_t*  last;
   slot_t*  empty;
   uint32_t max_slots;
   uint32_t used;
   uint32_t good_slots;
   uint32_t total_blocks;
   uint32_t pages_per_block;
   uint32_t pages_per_aligned_block;
   uint32_t align_mask;
   uint32_t align_shift;
} spool_t;


static inline slot_t* get_new_slot(spool_t* spool) {
   slot_t* slot = spool->empty;

   if (slot) {
      spool->empty = slot->post;
   } if (spool->used < spool->max_slots) {
      slot = &spool->pool[spool->used++];
   }
   return slot;
};

static void release_slot(spool_t* spool, slot_t* slot) {
   slot_t* prev = slot->prev;
   slot_t* post = slot->post;

   // remove from the current chain
   if (post)
      post->prev = prev;
   else
      spool->last = prev;

   if (prev)
      prev->post = post;
   else
      spool->first = post;

   // prepend to empty list
   slot->prev = NULL;
   slot->post = spool->empty;
   spool->empty = slot;
};


static inline unsigned int misaligned_pages(spool_t* spool, slot_t* slot) {
   if (slot->s.paddr_idx > 0) {
      unsigned int misaligned = (spool->align_mask - ((slot->s.paddr_idx - 1) & spool->align_mask));
      if (misaligned > slot->pages)
         return slot->pages;
      else
         return misaligned;
   } else
      return 0;
}

static inline int increment_counters(spool_t* spool, slot_t* slot) {
   unsigned int n_aligned_blocks;
   unsigned int new_blocks;
   int          n_pages = ++slot->pages;

   if (spool->align_mask) {
      unsigned int n_aligned_pages = n_pages - misaligned_pages(spool, slot);

      n_aligned_blocks = (n_aligned_pages & ~spool->align_mask) / spool->pages_per_aligned_block;
      if ((n_aligned_pages & spool->align_mask) >= spool->pages_per_block)
         n_aligned_blocks++;

   } else {
      n_aligned_blocks = n_pages / spool->pages_per_aligned_block;
   }

   new_blocks = n_aligned_blocks - slot->s.nblocks;

   if (new_blocks) {
      if (slot->s.nblocks == 0)
         spool->good_slots++;
      slot->s.nblocks = n_aligned_blocks;
      spool->total_blocks += new_blocks;
//LOG_DEBUG("slot: 0x%lx\n", (uintptr_t)slot);
//LOG_DEBUG("total_blocks: %d (%d / %d)\n", spool->total_blocks, slot->s.nblocks, slot->pages);
   }
   return 0;
}


static int add_new_slot(spool_t* spool, unsigned int page_index, slot_t* prev, slot_t* post) {
   slot_t* slot = get_new_slot(spool);

LOG_DEBUG("New slot: 0x%px\n", slot);
   if (!slot) return -1;

   slot->prev = prev;
   if (prev)
      prev->post = slot;
   else
      spool->first = slot;

   slot->post = post;
   if (post)
      post->prev = slot;
   else
      spool->last = slot;
/*
if (prev)
   LOG_DEBUG("   prev: 0x%px (between 0x%px - 0x%px)\n", prev, prev->prev, prev->post);
if (post)
   LOG_DEBUG("   post: 0x%px (between 0x%px - 0x%px)\n", post, post->prev, post->post);
*/
   slot->s.paddr_idx = page_index;
   slot->pages = 0;
   slot->s.nblocks = 0;
   return increment_counters(spool, slot);
}

int add_page_index(spool_t* spool, unsigned int page_index) {
   slot_t* slot;

   for (slot = spool->last; slot; slot = slot->prev) {
      unsigned int next_index = slot->s.paddr_idx + slot->pages;

      if (page_index >= next_index) {
         if (page_index == next_index)
            return increment_counters(spool, slot);
         else
            return add_new_slot(spool, page_index, slot, slot->post);

      } else if (slot->s.paddr_idx == (page_index + 1)) {
         slot_t* prevslot = slot->prev;

         if (prevslot && (prevslot->s.paddr_idx + prevslot->pages) == page_index) {
            if (slot->s.nblocks && prevslot->s.nblocks)  // if both are good
               spool->good_slots--;
            slot->pages += prevslot->pages;
            slot->s.nblocks += prevslot->s.nblocks;
            release_slot(spool, prevslot);
         } else {
            slot->s.paddr_idx = page_index;
         }
         return increment_counters(spool, slot);
      }
   }
   return add_new_slot(spool, page_index, NULL, spool->first);
}


static int alloc_multipages_slots(rscma_data_t* rscma_data, mblkset_t* mblockset,
                            unsigned int alloc_order, unsigned int pages_per_block,
                            unsigned int type, unsigned int max_slots) {
   slot_t*      slot;
   spool_t      slot_pool = {
      .max_slots = max_slots,
      .pool = NULL,
      .first = NULL,
      .last = NULL,
      .empty = NULL,
      .used = 0,
      .pages_per_block = pages_per_block,
      .pages_per_aligned_block = pages_per_block,
      .align_mask = 0,
      .align_shift = 0,
      .good_slots = 0,
      .total_blocks = 0,
   };
   unsigned int page_shift = alloc_order + PAGE_SHIFT;
   unsigned int excess_pages = 0;
   int          err = 0;
   uintptr_t    paddr;

   slot_pool.pool = kmalloc(max_slots * sizeof(*slot_pool.pool), GFP_KERNEL);
	if (slot_pool.pool == NULL)
		goto on_pool_error;

   if (mblockset->align_shift > page_shift) {
      unsigned int aligned_pages;

      slot_pool.align_shift = mblockset->align_shift - page_shift;
      aligned_pages = 1 << slot_pool.align_shift;
      slot_pool.align_mask = aligned_pages - 1;
      slot_pool.pages_per_aligned_block = ((pages_per_block - 1) / aligned_pages + 1) * aligned_pages;
      excess_pages = slot_pool.pages_per_aligned_block - pages_per_block;
   }

LOG_VAR(slot_pool.max_slots, %d);
LOG_VAR(alloc_order, %d);
LOG_VAR(slot_pool.pages_per_block, %d);
LOG_VAR(slot_pool.total_blocks, %d);
LOG_VAR(slot_pool.align_mask, 0x%08x);
   while(slot_pool.total_blocks < mblockset->mblock_count) {
      unsigned int page_index;

      err = alloc_single_page(rscma_data, mblockset, alloc_order, &paddr, type);
      if (err)
         break;

      page_index = paddr >> page_shift;
      LOG_ASSERT(page_index);
//LOG_DEBUG("page: %d / 0x%lx\n", page_index, paddr);
      if (add_page_index(&slot_pool, page_index)) {
         LOG_ERROR("slot pool full\n");
         err = -ENOMEM;
         break;
      }
   }
LOG_VAR(slot_pool.good_slots, %d);

LOG_VAR(mblockset->mblock_flags, 0x%x);
   if (!err || ((mblockset->mblock_flags & __FLG_PARTIAL_ALLOC) && slot_pool.total_blocks != 0)) {
      mblockset->mblock_count = slot_pool.total_blocks;
      // allocate slot table
      if (excess_pages)
         err = alloc_slot_table(mblockset, slot_pool.total_blocks);
      else
         err = alloc_slot_table(mblockset, slot_pool.good_slots);
   }

   if (err) {
      // if error, free all pages
      for (slot = slot_pool.first; slot; slot = slot->post)
         free_macropages(rscma_data, mblockset, slot->pages, alloc_order, slot->s.paddr_idx, type);

   } else {
      // if no error, free/release only unused pages
      for (slot = slot_pool.first; slot; slot = slot->post) {

         if (slot->s.nblocks == 0) {
            free_macropages(rscma_data, mblockset, slot->pages, alloc_order, slot->s.paddr_idx, type);
            slot->pages = 0;
            continue;

         } else {
            unsigned int page_offset;

            if (slot_pool.align_mask) {
               unsigned int misaligned = misaligned_pages(&slot_pool, slot);

               if (misaligned) {
                  free_macropages(rscma_data, mblockset, misaligned, alloc_order, slot->s.paddr_idx, type);
                  slot->s.paddr_idx += misaligned;
LOG_ASSERT(slot->pages > misaligned);
                  slot->pages -= misaligned;
               }
               if (slot->s.nblocks > 1) {
                  if (excess_pages) {
                     unsigned int i;

                     for (i = 1; i < slot->s.nblocks; i++) {
                        unsigned int idx = slot->s.paddr_idx + i * slot_pool.pages_per_aligned_block;
                        free_macropages(rscma_data, mblockset, excess_pages, alloc_order, idx - excess_pages, type);
                     }
                  }
               }
            }
            page_offset = slot->s.nblocks * slot_pool.pages_per_aligned_block - excess_pages;
LOG_ASSERT(slot->pages >= page_offset);
            if (slot->pages > page_offset) {
               free_macropages(rscma_data, mblockset, slot->pages - page_offset, alloc_order, slot->s.paddr_idx + page_offset, type);
            }
            slot->pages = slot->s.nblocks * pages_per_block;
         }
      }

      // and copy data from the temporary slot_pool into the 'final' mslot_table
      {
         unsigned int block_count = 0;
         rscam_slot_t* out_slot = mblockset->mslot_table;

         for (slot = slot_pool.first; slot; slot = slot->post) {
LOG_DEBUG("slot: 0x%lx\n", (uintptr_t)slot);
LOG_DEBUG("pages: %d  / nblocks: %d / 0x%lx\n", slot->pages, slot->s.nblocks, SPAGE_IDX_TO_PADDR(slot->s.paddr_idx, page_shift));
            if (slot->s.nblocks == 0) continue;
LOG_ASSERT(slot->pages >= slot->s.nblocks * slot_pool.pages_per_block);

            block_count += slot->s.nblocks;
LOG_ASSERT(block_count <= slot_pool.total_blocks);
            if (excess_pages) { // blocks are not consecutive: one per output slot
               unsigned int i;
               for (i = 0; i < slot->s.nblocks; i++) {
                  out_slot->nblocks = 1;
                  out_slot->paddr_idx = slot->s.paddr_idx + i * slot_pool.pages_per_aligned_block;
                  out_slot++;
               }
            } else {            // blocks are consecutive: keep grouped slots
               out_slot->nblocks = slot->s.nblocks;
               out_slot->paddr_idx = slot->s.paddr_idx;
               out_slot++;
            }
         }
LOG_ASSERT(mblockset->mslot_count == (out_slot - mblockset->mslot_table));
LOG_ASSERT(mblockset->mblock_count == block_count);
LOG_DEBUG("mblock_count: %d  / %d\n", mblockset->mblock_count, block_count);
      }
   }
LOG_TRACE;
 on_pool_error:
   kfree(slot_pool.pool);  // free temporary slot_pool
   return err;
}

int alloc_singlepage_slots(rscma_data_t* rscma_data, mblkset_t* mblockset,
                               unsigned int alloc_order, unsigned int type) {
   uintptr_t    paddr;
   unsigned int page_shift = alloc_order + PAGE_SHIFT;
   int          err = 0;
   size_t       i;

LOG_TRACE;
   // force values to one block per slot (slot_count = mblock_count)
   err = alloc_slot_table(mblockset, mblockset->mblock_count);
   if (err) {
      goto on_error_0;
   }

LOG_TRACE;
   for (i = 0; i < mblockset->mblock_count; ++i) {
      rscam_slot_t* slot = &mblockset->mslot_table[i];

      err = alloc_single_page(rscma_data, mblockset, alloc_order, &paddr, type);
      if (!err) {
         slot->nblocks = 1;
         slot->paddr_idx = PADDR_TO_SPAGEIDX(paddr, page_shift);
      } else {
         while(i--) {
            slot = &mblockset->mslot_table[i];
            free_macropages(rscma_data, mblockset, 1, alloc_order, slot->paddr_idx, type);
         }
         goto on_error_1;
      }
   }
LOG_TRACE;
   // update mslot_count
   mblockset->mslot_count = mblockset->mblock_count;
   return 0;

 on_error_1:
   free_slot_table(mblockset);
 on_error_0:
   return err;
}


static void calc_alloc_order(mblkset_t* mblockset, unsigned int min_order,
                                                   unsigned int max_order,
                                                   unsigned int* alloc_order, size_t* block_size) {
   size_t base_page_size = (1 << (min_order + PAGE_SHIFT));
   unsigned int n_base_pages = (mblockset->mblock_size - 1) / base_page_size + 1;
   unsigned int order = min_order;

   while ((n_base_pages & 1) == 0 && order < max_order) {
      order++;
      n_base_pages >>= 1;
   }
   *alloc_order = order;
   *block_size = (size_t)n_base_pages << (order + PAGE_SHIFT);
}

static int get_alloc_order(mblkset_t* mblockset, unsigned int type,
                              unsigned int* alloc_order, size_t* block_size) {
   unsigned int min_order;
   unsigned int max_order;

   switch(type) {
      case __STDPAGES:
         min_order = 0;
         max_order = MAX_ORDER - 1;
         break;

      case __BOOT_RESERVE:
      case __CMA:
      case __HUGE_PAGES:
      default:
         return -EINVAL;
   }

   calc_alloc_order(mblockset, min_order, max_order, alloc_order, block_size);

   if (*block_size != mblockset->mblock_size) {
      if (mblockset->mblock_flags & __FLG_SIZE_STRICT)
         return -EINVAL;
      if ((*block_size > 2 * mblockset->mblock_size) && !(mblockset->mblock_flags & __FLG_SIZE_RELAXED))
         return -EINVAL;
   }
   return 0;
}

int alloc_mblockset(rscma_data_t* rscma_data, mblkset_t* blkset, unsigned int type) {
   size_t       block_size;
   unsigned int alloc_order;
   int          err;

   err = get_alloc_order(blkset, type, &alloc_order, &block_size);

   if (!err) {
      size_t page_size = (size_t)PAGE_SIZE << alloc_order;
      size_t pages_per_block = (block_size - 1) / page_size + 1;

LOG_VAR((size_t)blkset->mblock_count, %zd);
LOG_VAR(block_size, 0x%zx);
LOG_VAR(page_size, 0x%zx);
LOG_VAR(pages_per_block, %zd);

      if (0 || (pages_per_block == 1 && (blkset->mblock_flags & __FLG_SINGLE_SLOTS)))
         err = alloc_singlepage_slots(rscma_data, blkset, alloc_order, __STDPAGES);
      else
         err = alloc_multipages_slots(rscma_data, blkset, alloc_order, pages_per_block, __STDPAGES, 1024);
LOG_TRACE;

      if (!err) {
         blkset->mblock_type = type;
         blkset->page_shift = alloc_order + PAGE_SHIFT;
         blkset->mblock_size = page_size * pages_per_block;
      }
   }
LOG_TRACE;
   return err;
}


void free_mblockset_data(rscma_data_t* rscma_data, mblkset_t* blkset) {
   unsigned int type = blkset->mblock_type;
   unsigned int alloc_order = blkset->page_shift - PAGE_SHIFT;
   unsigned int pages_per_block = blkset->mblock_size / (1 << blkset->page_shift);
   unsigned int pages_per_aligned_block;
   unsigned int i;

   if (blkset->align_shift > blkset->page_shift) {
      unsigned int aligned_pages = 1 << (blkset->align_shift - blkset->page_shift) ;
      pages_per_aligned_block = ((pages_per_block - 1) / aligned_pages + 1) * aligned_pages;
   } else
      pages_per_aligned_block = pages_per_block;


   /* free all the mblocks in the blockset */
   for (i = 0; i < blkset->mslot_count; ++i) {
      rscam_slot_t* slot = &blkset->mslot_table[i];

      if (pages_per_aligned_block != pages_per_block) {
         unsigned int blkn;
         for (blkn = 0; blkn < slot->nblocks; blkn++) {
            unsigned int mpage_idx = slot->paddr_idx + blkn * pages_per_aligned_block;
            free_macropages(rscma_data, blkset, pages_per_block, alloc_order, mpage_idx, type);
         }
      } else {
         unsigned int npages = slot->nblocks * pages_per_block;
         free_macropages(rscma_data, blkset, npages, alloc_order, slot->paddr_idx, type);
      }
   }
   blkset->mslot_count = 0;
}

