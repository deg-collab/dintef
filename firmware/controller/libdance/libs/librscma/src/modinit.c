
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/utsname.h>

#include "intrscma.h"


#define RSCMA_DEVDIR   "/dev/dance"
#define RSCMA_DEVNAME  "rscma"

#define RSCMA_DEVPATH  RSCMA_DEVDIR "/" RSCMA_DEVNAME


struct rscma_globals_s rscma_globals = {
   .dev_fd = -1,
   .dev_path = RSCMA_DEVPATH
};



static int exec_common(const char** av) {
   int status;
   pid_t pid;

   pid = fork();
   if (pid == -1) return -1;

   if (pid == 0) {
      /* child */

      const int fd = open("/dev/null", O_WRONLY);

      if (fd != -1) {
         dup2(fd, STDOUT_FILENO);
         dup2(fd, STDERR_FILENO);
         close(fd);
      }

      execve(av[0], (char**)av, NULL);

      /* not reached or error */
      exit(-1);
   }

   if (waitpid(pid, &status, 0) == -1) {
      kill(pid, SIGKILL);
      return -1;
   }

   if (WIFEXITED(status) == 0)
      return -1;

   return (int)WEXITSTATUS(status);
}

static int exec_insmod(const char* kpath) {
   const char* av[] = {"/sbin/insmod", kpath, NULL};
   return exec_common(av);
}

static int exec_rmmod(const char* kname) {
   const char* av[] = {"/sbin/rmmod", kname, NULL};
   return exec_common(av);
}

static int load_kmod(const char* kname, const char* kmod_dir) {
   /* load kernel module */

   struct utsname un;
   struct stat st;
   char kpath[128];

   if (kmod_dir == NULL) {
      if (uname(&un)) goto on_error_0;
      snprintf(kpath, sizeof(kpath), "/lib/modules/%s/dance/%s.ko", un.release, kname);
   } else {
      snprintf(kpath, sizeof(kpath), "%s/%s.ko", kmod_dir, kname);
   }
   kpath[sizeof(kpath) - 1] = 0;

   if (stat(kpath, &st)) goto on_error_0;

   return exec_insmod(kpath);

 on_error_0:
   return -1;
}

static int unload_kmod(const char* kname) {
   return exec_rmmod(kname);
}


static int find_dev_major(char* devname) {
   int   major = -1;
   FILE *procdev = fopen("/proc/devices", "r");

   if (procdev) {
      char line[100];

      while (fgets(line, sizeof(line), procdev)) {
         char* pt = strstr(line, devname);
         if (pt && isspace(*(pt - 1)) && isspace(*(pt + strlen(devname)))) {
            major = atoi(line);
            LOG_DEBUG("found %s major: %d\n", devname, major);
            return major;
         }
      }
   }
   return -1;
}


/* checks and tries to initialise the library when needed
    if force_load assumes that kernel module is not loaded
*/
static int rscma_checkinit(int force_load, const char* kmod_dir) {
   int   dev_major;
   int   fd;
   dev_t dev;


   if (rscma_globals.dev_fd >= 0) {
      if (force_load) {
         errno = EBUSY;
         LOG_ERRNO();
      } else {
         errno = 0;
      }
      goto on_error_0;
   }

   /* ok to fail here if not force_load, module may be already loaded */
   if (load_kmod(RSCMA_DEVNAME, kmod_dir)) {
      if (force_load) {
         LOG_ERRNO();
         goto on_error_0;
      }
   }

   /* create dev node, only if it does not exist */
   /* anyhow, subsequent errors cannot be silenced */


   if (mkdir(RSCMA_DEVDIR, S_IRUSR | S_IWUSR | S_IXUSR)) {
      if (errno != EEXIST) {
         LOG_ERRNO();
         goto on_error_0;
      }
   }

   dev_major = find_dev_major(RSCMA_DEVNAME);
   if (dev_major < 0) dev_major = DEF_RSCMA_MAJOR;

   dev = makedev(dev_major, DEF_RSCMA_MINOR);
   if (mknod(rscma_globals.dev_path, S_IRUSR | S_IWUSR | S_IFCHR, dev)) {
      if (errno != EEXIST) {
         LOG_ERRNO();
         goto on_error_1;
      }
   }

   fd = open(rscma_globals.dev_path, O_RDWR);
   if (fd == -1) {
      LOG_ERRNO();
      goto on_error_2;
   }

   /* success */
   rscma_globals.dev_fd = fd;
   errno = 0;
   return 0;

 on_error_2:
   unlink(rscma_globals.dev_path);
 on_error_1:
   rmdir(RSCMA_DEVDIR);
 on_error_0:
   return -errno;
}

/* checks and tries to initialise the library when needed
*/
int lib_check_and_init(void) {
   /* run rscma_checkinit in with force_load disabled */
   return rscma_checkinit(0, NULL);
}


/* returns error if library is not initialised
*/
int lib_check_only(void) {
   if (rscma_globals.dev_fd >= 0)
      return 0;
   else {
      errno = ENXIO;
      LOG_ERRNO();
      return -errno;
   }
}


/* loads kernel module and initialises the library
*/
int rscma_load(const char* kmod_dir) {
   /* run rscma_checkinit with force_load flag */
   return rscma_checkinit(1, kmod_dir);
}


/* tries to unload kernel module and shut down the library
*/
int rscma_unload(void) {

   if (lib_check_only())
      return -errno;

   close(rscma_globals.dev_fd);
   rscma_globals.dev_fd = -1;
   unlink(rscma_globals.dev_path);
   /* do not rmdir */
   /* rmdir(RSCMA_DEVDIR); */

   unload_kmod(RSCMA_DEVNAME);
   return 0;
}

