#ifndef LIBEFIFO_H_INCLUDED
# define LIBEFIFO_H_INCLUDED 1



#include <stdint.h>
#include <sys/types.h>
#include "libepci.h"
#include "libuirq.h"
#include "libebuf.h"


/* error codes */

typedef enum
{
  EFIFO_ERR_SUCCESS = 0,
  EFIFO_ERR_FAILURE,
  EFIFO_ERR_SIZE,
  EFIFO_ERR_ADDR,
  EFIFO_ERR_DESC,
  EFIFO_ERR_TIMEOUT,
  EFIFO_ERR_MODE,
  EFIFO_ERR_METHOD,
  EFIFO_ERR_ALREADY,
  EFIFO_ERR_IMPL
} efifo_err_t;


/* timeout special values */

#define EFIFO_TIMEOUT_NONE ((unsigned int)-1)
#define EFIFO_TIMEOUT_ZERO ((unsigned int)0)


/* fifo description */

typedef struct efifo_desc
{
#define EFIFO_FLAG_READ (1 << 0)
#define EFIFO_FLAG_WRITE (1 << 1)
#define EFIFO_FLAG_READWRITE (EFIFO_FLAG_READ | EFIFO_FLAG_WRITE)
#define EFIFO_FLAG_IRQ (1 << 2)
  uint32_t flags;

  /* item width, in bytes. must be multiple of sizeof(uint32_t). */
  size_t width;
  /* fifo item count */
  size_t depth;

  /* write port width, in bytes. */
  size_t write_width;

  /* associated epci bar handle */
  epcihandle_t epci_handle;

  /* control, {r,w}data, status registers */
  size_t ctrl_reg_off;
  size_t rdat_reg_off;
  size_t wdat_reg_off;
  size_t stat_reg_off;

  /* ebone slave id */
  uint32_t ebone_slave_id;

} efifo_desc_t;


/* fifo handle */

typedef struct efifo_handle
{
  /* EFIFO_FLAG_xxx */
  uint32_t flags;

  /* item width, in uint32_t words */
  size_t width;
  /* fifo item count */
  size_t depth;

  /* write port width, in bytes. */
  size_t write_width;

  /* control, {r,w}data, status registers */
  epcihandle_t epci_handle;
  size_t ctrl_reg_off;
  size_t rdat_reg_off;
  size_t wdat_reg_off;
  size_t stat_reg_off;

  /* ebone slave id */
  uint32_t slave_id;

  /* ebone slave mirq info */
  uint32_t irq_mask;
  uint32_t irq_pos;

  /* async signaling pipe */
  int async_pipe[2];

  /* async message posting and completion */
  pthread_mutex_t async_post_lock;
  pthread_mutex_t async_compl_lock;
  pthread_cond_t async_compl_cond;

  /* async buffer and read write pointers */
  /* size and pos are item counts */
  volatile uint32_t async_flags;
  volatile size_t async_thresh;
  volatile unsigned int async_tmout;
  uint8_t* volatile async_data;
  volatile size_t async_size;
  volatile uint32_t async_wpos;
  volatile uint32_t async_rpos;
  volatile uint32_t async_state;
#define EFIFO_ASYNC_ERR_SW_OVF (1 << 0)
  volatile uint32_t async_err;

} efifo_handle_t;


/* asynchronous operation description */

typedef struct
{
#define EFIFO_ASYNC_FLAG_READ (1 << 0)
#define EFIFO_ASYNC_FLAG_WRITE (1 << 1)
#define EFIFO_ASYNC_FLAG_EBUF (1 << 2)
#define EFIFO_ASYNC_FLAG_THRESHOLD (1 << 3)
#define EFIFO_ASYNC_FLAG_TIMEOUT (1 << 4)
#define EFIFO_ASYNC_FLAG_REALTIME (1 << 5)
  uint32_t flags;

  /* actually an ebuf if flag set */
  /* size is an item count */
  void* buf_data;
  size_t buf_size;

  /* signaling threshold in item count, if flag set */
  size_t thresh;
  
  /* signaling timeout in us, if flag set */
  unsigned int tmout;

} efifo_async_desc_t;


/* exported routines */

/* library constructor */
efifo_err_t efifo_init_lib(void);
efifo_err_t efifo_fini_lib(void);

/* open close */
void efifo_init_desc(efifo_desc_t*);
efifo_err_t efifo_open(efifo_handle_t*, const efifo_desc_t*);
efifo_err_t efifo_close(efifo_handle_t*);

/* synchronous operations */
efifo_err_t efifo_read(efifo_handle_t*, void*, size_t*);
efifo_err_t efifo_write(efifo_handle_t*, const void*, size_t*);
efifo_err_t efifo_read_ebuf(efifo_handle_t*, ebuf_handle_t*, size_t, size_t*);
efifo_err_t efifo_write_ebuf(efifo_handle_t*, ebuf_handle_t*, size_t, size_t*);
size_t efifo_get_occupancy(efifo_handle_t*);

/* async operations */
efifo_err_t efifo_init_async_desc(efifo_async_desc_t*);
efifo_err_t efifo_init_read_async_desc(efifo_async_desc_t*, void*, size_t);
efifo_err_t efifo_start_async(efifo_handle_t*, const efifo_async_desc_t*);
efifo_err_t efifo_stop_async(efifo_handle_t*);
efifo_err_t efifo_get_async_data(efifo_handle_t*, void**, size_t*);
efifo_err_t efifo_seek_async_data(efifo_handle_t*, size_t);
efifo_err_t efifo_copy_async_data(efifo_handle_t*, void*, size_t);
size_t efifo_get_async_occupancy(efifo_handle_t*);
size_t efifo_get_async_err(efifo_handle_t*);

#ifdef EFIFO_CONFIG_UNIT
/* used for unit testing */
void efifo_set_reset_bit(efifo_handle_t*);
void efifo_clear_reset_bit(efifo_handle_t*);
#endif /* EFIFO_CONFIG_UNIT */

/* inlined raw accessors */

static inline size_t efifo_get_depth(const efifo_handle_t* fifo)
{
  /* return the fifo depth, in item count */
  return fifo->depth;
}

static inline size_t efifo_get_width(const efifo_handle_t* fifo)
{
  /* return the item width, in bytes */
  return fifo->width * sizeof(uint32_t);
}

static inline unsigned int efifo_is_empty(efifo_handle_t* fifo)
{
  return efifo_get_occupancy(fifo) == 0;
}

static inline unsigned int efifo_is_full(efifo_handle_t* fifo)
{
  return efifo_get_occupancy(fifo) == fifo->depth;
}

static inline uint32_t efifo_get_irq_mask(const efifo_handle_t* fifo)
{
  /* assume fifo->flags & EFIFO_FLAG_IRQ */
  return fifo->irq_mask;
}

static inline size_t efifo_count_to_size
(const efifo_handle_t* fifo, size_t count)
{
  /* note:  get_width() returns a byte count */
  return count * efifo_get_width(fifo);
}

static inline size_t efifo_size_to_count
(const efifo_handle_t* fifo, size_t size)
{
  /* warning: count rounded down */
  /* note:  get_width() returns a byte count */
  return size / efifo_get_width(fifo);
}



#endif /* ! LIBEFIFO_H_INCLUDED */
