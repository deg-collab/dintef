#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <sched.h>
#include <pthread.h>
#include <string.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include "libepci.h"
#include "libefifo.h"
#include "libebuf.h"
#include "libuirq.h"


#define EFIFO_CONFIG_DEBUG 1
#if (EFIFO_CONFIG_DEBUG == 1)
#include <stdio.h>
#define PRINTF(__s, ...) do { printf(__s, ## __VA_ARGS__); } while (0)
#define PERROR() printf("[!] %d\n", __LINE__)
#define ASSUME(__x) do { if (!(__x)) printf("[!] %d\n", __LINE__); } while (0)
#else
#define PRINTF(__s, ...)
#define PERROR()
#define ASSUME(__x)
#endif /* EFIFO_CONFIG_DEBUG */


/* atomic routines */

static inline uint32_t atomic_read(volatile uint32_t* p)
{
  __sync_synchronize();
  return *p;
}

static inline void atomic_write(volatile uint32_t* p, uint32_t x)
{
  __sync_synchronize();
  *p = x;
  __sync_synchronize();
}

static inline void atomic_or(volatile uint32_t* p, uint32_t x)
{
  atomic_write(p, atomic_read(p) | x);
}


/* bit mask routines */

static size_t mask_to_pos(uint32_t m)
{
  size_t i;
  for (i = 0; i != 31; ++i)
  {
    if ((1 << i) & m) break ;
  }
  return i;
}


/* low level routines. access the actual hardware. */

#define OCCUPANCY_SHIFT 0
#define IRQ_THRESHOLD_SHIFT 0
#define IRQ_ACK_SHIFT 29
#define IRQ_EN_SHIFT 30
#define RESET_SHIFT 31

#define OCCUPANCY_MASK (((1 << 16) - 1) << OCCUPANCY_SHIFT)
#define IRQ_THRESHOLD_MASK (((1 << 16) - 1) << (IRQ_THRESHOLD_SHIFT))
#define IRQ_ACK_MASK (1 << IRQ_ACK_SHIFT)
#define IRQ_EN_MASK (1 << IRQ_EN_SHIFT)
#define RESET_MASK (1 << RESET_SHIFT)

static inline void write_ctl(efifo_handle_t* fifo, uint32_t x)
{
  epci_wr32_reg(fifo->epci_handle, fifo->ctrl_reg_off, x);
}

static inline void read_ctl(efifo_handle_t* fifo, uint32_t* x)
{
  epci_rd32_reg(fifo->epci_handle, fifo->ctrl_reg_off, x);
}

static inline void or_ctl(efifo_handle_t* fifo, uint32_t mask)
{
  uint32_t x;
  read_ctl(fifo, &x);
  write_ctl(fifo, x | mask);
}

static inline void and_ctl(efifo_handle_t* fifo, uint32_t mask)
{
  uint32_t x;
  read_ctl(fifo, &x);
  write_ctl(fifo, x & mask);
}

static inline void read_sta(efifo_handle_t* fifo, uint32_t* x)
{
  epci_rd32_reg(fifo->epci_handle, fifo->stat_reg_off, x);
}

static void write_dat(efifo_handle_t* fifo, const uint32_t* x)
{
  /* write one item of size (h->width * sizeof(uint32_t)) */

  size_t off = fifo->wdat_reg_off;
  size_t i;

  if (fifo->write_width == sizeof(uint16_t))
  {
    for (i = 0; i < fifo->width; ++i, ++x, off += sizeof(uint32_t))
    {
      /* always successful, dont check for error */
      epci_wr32_reg(fifo->epci_handle, off, *x & 0xffff);
      epci_wr32_reg(fifo->epci_handle, off, *x >> 16);
    }
  }
  else /* assumed write_width == width */
  {
    for (i = 0; i < fifo->width; ++i, ++x, off += sizeof(uint32_t))
    {
      /* always successful, dont check for error */
      epci_wr32_reg(fifo->epci_handle, off, *x);
    }
  }
}

static void write_dat_n(efifo_handle_t* fifo, const uint32_t* x, size_t n)
{
  /* write n items of size (h->width * sizeof(uint32_t)) */

  for (; n; --n, x += fifo->width) write_dat(fifo, x);
}

static void read_dat(efifo_handle_t* fifo, uint32_t* x)
{
  /* read one item of size (h->width * sizeof(uint32_t)) */

  size_t off = fifo->rdat_reg_off;
  size_t i;

  for (i = 0; i < fifo->width; ++i, ++x, off += sizeof(uint32_t))
  {
    /* always successful, dont check for error */
    epci_rd32_reg(fifo->epci_handle, off, x);
  }
}

static void read_dat_n(efifo_handle_t* fifo, uint32_t* x, size_t n)
{
  /* read n items of size (h->width * sizeof(uint32_t)) */

  for (; n; --n, x += fifo->width) read_dat(fifo, x);
}

static void read_mirq(efifo_handle_t* fifo, uint32_t* mirq)
{
  uint32_t pos;
  read_sta(fifo, &pos);
  *mirq = 1 << ((pos >> 16) & 0xf);
}

static inline void clear_reset_bit(efifo_handle_t* fifo)
{
  and_ctl(fifo, ~RESET_MASK);
}

static inline void set_reset_bit(efifo_handle_t* fifo)
{
  or_ctl(fifo, RESET_MASK);
}

static inline void wait_ebone_clocks_x3(efifo_handle_t* fifo)
{
  /* 3 clocks @ 125mhz == 24 ns */
}

__attribute__((unused))
static inline void reset_fifo(efifo_handle_t* fifo)
{
  set_reset_bit(fifo);
  wait_ebone_clocks_x3(fifo);
  clear_reset_bit(fifo);
}

static inline void ack_irq(efifo_handle_t* fifo)
{
  or_ctl(fifo, IRQ_ACK_MASK);
}

static inline void enable_irq(efifo_handle_t* fifo, uint32_t threshold)
{
  and_ctl(fifo, ~(IRQ_ACK_MASK | IRQ_EN_MASK | IRQ_THRESHOLD_MASK));
  or_ctl(fifo, IRQ_EN_MASK | (threshold << IRQ_THRESHOLD_SHIFT));
}

static inline void disable_irq(efifo_handle_t* fifo)
{
  and_ctl(fifo, ~IRQ_EN_MASK);
}

static int enable_ebone_slave_interrupt(efifo_handle_t* fifo)
{
  /* ebm0 documentation: ebm0_pcie_a.pdf */

  epcihandle_t bar0_handle;
  uint32_t x;

  bar0_handle = epci_open("10ee:eb01", NULL, 0);
  if (bar0_handle == EPCI_BAD_HANDLE) return -1;

  /* control register 0 */
  /* ebone slave interrupt enable (bit 9) */
  /* global interrupt enable (bit 31) */
  epci_rd32_reg(bar0_handle, 0x0, &x);
  x |= (1 << 31) | (1 << 9);
  epci_wr32_reg(bar0_handle, 0x0, x);

  /* which slave triggers an interrupt can be known */
  /* using status register 1 (offset 0x14) */

  epci_close(bar0_handle);

  return 0;
}

static size_t get_occupancy(efifo_handle_t* fifo)
{
  uint32_t x;
  read_sta(fifo, &x);
  return (size_t)((x >> OCCUPANCY_SHIFT) & OCCUPANCY_MASK);
}

__attribute__((unused))
static size_t get_threshold(efifo_handle_t* fifo)
{
  uint32_t x;
  read_ctl(fifo, &x);
  return (size_t)((x >> IRQ_THRESHOLD_SHIFT) & IRQ_THRESHOLD_MASK);
}

void set_threshold(efifo_handle_t* fifo, size_t threshold)
{
  uint32_t x;
  read_ctl(fifo, &x);
  x &= ~IRQ_THRESHOLD_MASK;
  x |= threshold << IRQ_THRESHOLD_SHIFT;
}

static inline void slowdown_polling(void)
{
  /* avoid flooding pcie link with requests */
  usleep(1);
}


/* asynchronous operation thread */

/* this thread is used to handle lowlevel interrupts and capture */
/* fifo data. a high priority is used to reduce latency. it signals */
/* the runtime as some condittions become ready: */
/* . threshold is reached, */
/* . timeout is reached, */
/* . fifo status changed (overflow, error ...). */

/* a message passing interface to communicate with the runtime. a */
/* global mbox contains an operation word that is used to post operation. */
/* this mbox can be used by multiple client thread concurrently. refer */
/* to xxx_async_msg and wrapper routines. */

/* warning: high scheduling is used to reduces latency, but can also */
/* starve other threads. we must ensure that high priority threads sleep */
/* until they actually have something to do. Esp., avoid polling. */

#define ASYNC_STATE_STARTED 0
#define ASYNC_STATE_STOPPED (!ASYNC_STATE_STARTED)

typedef struct async_msg
{
  volatile enum
  {
    OP_NONE = 0,
    OP_START,
    OP_STOP,
    OP_JOIN
  } op;

  struct
  {
    volatile uint32_t* compl_err;
    efifo_handle_t* fifo;
  } start_stop;

  struct
  {
    const efifo_async_desc_t* desc;
  } start;

} async_msg_t;

typedef struct
{
  int pipe[2];
} async_mbox_t;

static async_mbox_t g_async_mbox;

static void complete_start_stop_msg(async_msg_t* msg, uint32_t err)
{
  efifo_handle_t* const fifo = msg->start_stop.fifo;

  pthread_mutex_lock(&fifo->async_compl_lock);
  *msg->start_stop.compl_err = err;
  pthread_cond_signal(&fifo->async_compl_cond);
  pthread_mutex_unlock(&fifo->async_compl_lock);
}

static int post_wait_async_msg(async_msg_t* msg)
{
  static const uint32_t not_compl_key = 0x2a2a2a2a;
  async_mbox_t* const mbox = &g_async_mbox;
  volatile uint32_t err = 0;
  efifo_handle_t* const fifo = msg->start_stop.fifo;

  if ((msg->op == OP_START) || (msg->op == OP_STOP))
  {
    /* protect as regard with concurrent posters */
    pthread_mutex_lock(&fifo->async_post_lock);
    err = not_compl_key;
    msg->start_stop.compl_err = &err;
  }

  if (write(mbox->pipe[1], msg, sizeof(async_msg_t)) != sizeof(async_msg_t))
  {
    PERROR();
    err = (uint32_t)-1;
    goto on_error;
  }

  if ((msg->op == OP_START) || (msg->op == OP_STOP))
  {
    /* protect as regard with async thread */
    pthread_mutex_lock(&fifo->async_compl_lock);
    while (1)
    {
      pthread_cond_wait(&fifo->async_compl_cond, &fifo->async_compl_lock);
      if (err != not_compl_key) break ;
    }
    pthread_mutex_unlock(&fifo->async_compl_lock);
  }

 on_error:
  if ((msg->op == OP_START) || (msg->op == OP_STOP))
  {
    pthread_mutex_unlock(&fifo->async_post_lock);
  }

  return (int)err;
}

static int post_start_msg
(efifo_handle_t* fifo, const efifo_async_desc_t* desc)
{
  async_msg_t msg;

  msg.op = OP_START;
  msg.start_stop.fifo = fifo;
  msg.start.desc = desc;

  return post_wait_async_msg(&msg);
}

static int post_stop_msg(efifo_handle_t* fifo)
{
  async_msg_t msg;

  msg.op = OP_STOP;
  msg.start_stop.fifo = fifo;

  return post_wait_async_msg(&msg);
}

static int post_join_msg(void)
{
  async_msg_t msg;

  msg.op = OP_JOIN;

  return post_wait_async_msg(&msg);
}

static int set_sched(int policy)
{
  /* NOTE: setting the thread scheduling priority in pthread_create */
  /* attributes did not work, so we do this here */

  /* NOTE: using realtime policy (SCHED_FIFO) reduce interrupt handling */
  /* latency but can starve the other threads if too much hw ints. */
  /* in this case, default policy (SCHED_OTHER) is be used. */

  struct sched_param param;

  param.sched_priority = sched_get_priority_max(policy);

  if (pthread_setschedparam(pthread_self(), policy, &param))
  {
    PERROR();
    return -1;
  }

  return 0;
}

static int set_default_sched(void)
{
  return set_sched(SCHED_OTHER);
}

static int set_realtime_sched(void)
{
  return set_sched(SCHED_FIFO);
}

static pthread_t g_async_thread;

static void* async_thread_entry(void* arg)
{
  async_mbox_t* mbox = arg;
  async_msg_t msg;
  fd_set rds;
  int nfd;
  struct timeval* tmvp;
  uirq_handle_t uirq;
  int uirq_fd;
  efifo_handle_t* uirq_pos_to_fifo[32] = { NULL, };
  uint32_t uirq_all_mask = 0;
  uint32_t uirq_tmp_mask;
  size_t uirq_set_pos;
  unsigned int is_init_err = 0;
  size_t osize;
  size_t rsize;
  size_t rpos;
  size_t wpos;
  uint32_t compl_err;
  size_t realtime_refn = 0;

  if (uirq_init_lib())
  {
    PERROR();
    is_init_err = 1;
  }

  uirq_init(&uirq, "10ee:eb01");
  if ((is_init_err == 0) && uirq_open(&uirq))
  {
    PERROR();
    uirq_fini_lib();
    is_init_err = 1;
  }

  uirq_fd = uirq_get_fd(&uirq);

  while (1)
  {
    /* fill read set with mbox and uirq */

    FD_ZERO(&rds);

    FD_SET(mbox->pipe[0], &rds);
    nfd = mbox->pipe[0] + 1;

    if (uirq_all_mask)
    {
      /* TODO: timeout */
      /* TODO: per fifo timeout */
      /* TODO: must be relative the the last fifo read */
      tmvp = NULL;

      FD_SET(uirq_fd, &rds);
      if (uirq_fd >= nfd) nfd = uirq_fd + 1;
    }
    else
    {
      tmvp = NULL;
    }

    nfd = select(nfd, &rds, NULL, NULL, tmvp);
    if (nfd < 0)
    {
      PERROR();
      continue ;
    }
    else if (nfd == 0)
    {
      /* TODO: timeout */
      /* TODO: signal sequential thread */
      PERROR();
      continue ;
    }
    else if (FD_ISSET(uirq_fd, &rds))
    {
      /* fifo irq */

      if (uirq_poll(&uirq, &uirq_tmp_mask))
      {
         PERROR();
         continue ;
      }

      /* iterate over all uirq_tmp_mask bits set */

      for (uirq_set_pos = 0; uirq_tmp_mask; ++uirq_set_pos)
      {
         const uint32_t uirq_set_mask = 1 << uirq_set_pos;
         efifo_handle_t* const fifo = uirq_pos_to_fifo[uirq_set_pos];

         if ((uirq_set_mask & uirq_tmp_mask) == 0) continue ;

         uirq_tmp_mask &= ~uirq_set_mask;

         if (fifo == NULL)
         {
           /* spurious interrupt */
           PERROR();
           continue ;
         }

         /* update async buffer and wpos by reading fifo */
         /* the read size is called rsize. it is computed */
         /* so that it is the is the min between buffer */
         /* remaining space and fifo occupancy */

         rpos = atomic_read(&fifo->async_rpos);
         wpos = atomic_read(&fifo->async_wpos);
         if (rpos > wpos) rsize = rpos - wpos;
         else rsize = fifo->async_size - wpos + rpos;

         osize = get_occupancy(fifo);

         /* check for software ovf conditions */
         /* NOTE: hw does not give overflow information */

         if ((wpos + osize) > fifo->async_size)
         {
           atomic_or(&fifo->async_err, EFIFO_ASYNC_ERR_SW_OVF);
         }

         /* do not read more than occupancy */

         if (rsize > osize) rsize = osize;

         /* 2 read operations if ring buffer wrapping */

         if ((wpos + rsize) >= fifo->async_size)
         {
           const size_t this_rsize = fifo->async_size - wpos;
           const size_t scaled_off = wpos * efifo_get_width(fifo);
           uint32_t* const p = (uint32_t*)(fifo->async_data + scaled_off);

           read_dat_n(fifo, p, this_rsize);
           rsize -= this_rsize;
           wpos = 0;
         }

         if (rsize)
         {
           const size_t scaled_off = wpos * efifo_get_width(fifo);
           uint32_t* const p = (uint32_t*)(fifo->async_data + scaled_off);

           read_dat_n(fifo, p, rsize);
         }

         wpos += rsize;
         atomic_write(&fifo->async_wpos, wpos);

         /* signal the runtime threshold is reached */

         if (rpos <= wpos) osize = wpos - rpos;
         else osize = fifo->async_size - rpos + wpos;

         if (osize >= fifo->async_thresh)
         {
           if (write(fifo->async_pipe[1], "*", 1) != 1)
           {
             PERROR();
           }
         }
      }
    }
    else if (FD_ISSET(mbox->pipe[0], &rds))
    {
      /* new message */

      if (read(mbox->pipe[0], &msg, sizeof(async_msg_t)) != sizeof(async_msg_t))
      {
         PERROR();
         msg.op = OP_NONE;
      }

      switch (msg.op)
      {
         case OP_NONE:
            {
              break ;
            }

         case OP_START:
            {
              efifo_handle_t* const fifo = msg.start_stop.fifo;
              const efifo_async_desc_t* const desc = msg.start.desc;

              compl_err = (uint32_t)-1;

              if (is_init_err)
              {
                PERROR();
                goto on_msg_complete;
              }

              if (fifo->async_state == ASYNC_STATE_STARTED)
              {
                PERROR();
                goto on_msg_complete;
              }

              /* currently support only read operations */
              if (desc->flags & EFIFO_ASYNC_FLAG_WRITE)
              {
                PERROR();
                goto on_msg_complete;
              }

              /* TODO: this should work */
              if (desc->buf_size < fifo->depth)
              {
                PERROR();
                goto on_msg_complete;
              }

              if (enable_ebone_slave_interrupt(fifo) == -1)
              {
                PERROR();
                goto on_msg_complete;
              }

              if (uirq_set_mask(&uirq, uirq_all_mask | fifo->irq_mask, 1))
              {
                PERROR();
                goto on_msg_complete;
              }

              uirq_all_mask |= fifo->irq_mask;

              /* half full irq */

              enable_irq(fifo, fifo->depth / 2 + (fifo->depth & 1));

              if (desc->flags & EFIFO_ASYNC_FLAG_REALTIME)
              {
                if ((realtime_refn++) == 0) set_realtime_sched();
              }

              /*  initialize according to desc */

              fifo->async_flags = desc->flags;
              fifo->async_thresh = desc->thresh;
              fifo->async_tmout = desc->tmout;

              if (desc->flags & EFIFO_ASYNC_FLAG_EBUF)
              {
                fifo->async_data = ebuf_get_data(desc->buf_data);
              }
              else
              {
                fifo->async_data = desc->buf_data;
              }

              uirq_pos_to_fifo[fifo->irq_pos] = fifo;

              fifo->async_size = desc->buf_size;
              atomic_write(&fifo->async_wpos, 0);
              atomic_write(&fifo->async_rpos, 0);
              fifo->async_state = ASYNC_STATE_STARTED;
              atomic_write(&fifo->async_err, 0);

              compl_err = 0;

              goto on_msg_complete;
              break ;
            }

         case OP_STOP:
            {
              efifo_handle_t* const fifo = msg.start_stop.fifo;

              compl_err = (uint32_t)-1;

              if (fifo->async_state != ASYNC_STATE_STARTED)
              {
                PERROR();
                goto on_msg_complete;
              }

              disable_irq(fifo);

              if (fifo->async_flags & EFIFO_ASYNC_FLAG_REALTIME)
              {
                if ((--realtime_refn) == 0) set_default_sched();
              }

              uirq_pos_to_fifo[fifo->irq_pos] = NULL;
              uirq_all_mask &= ~fifo->irq_mask;

              fifo->async_state = ASYNC_STATE_STOPPED;

              compl_err = 0;

 on_msg_complete:
              complete_start_stop_msg(&msg, compl_err);
              break ;
            }

         case OP_JOIN:
            {
              goto on_join;
              break ;
            }

         default:
            {
              PERROR();
              break ;
            }
      } /* switch(msg.op) */
    }
  }

 on_join:
  if (is_init_err == 0)
  {
    uirq_close(&uirq);
    uirq_fini_lib();
  }

  return NULL;
}


/* exported: library constructor */

efifo_err_t efifo_init_lib(void)
{
  if (pipe(g_async_mbox.pipe))
  {
    PERROR();
    goto on_error_0;
  }

  if (pthread_create(&g_async_thread, NULL, async_thread_entry, &g_async_mbox))
  {
    PERROR();
    goto on_error_1;
  }

  return EFIFO_ERR_SUCCESS;

 on_error_1:
  close(g_async_mbox.pipe[0]);
  close(g_async_mbox.pipe[1]);
 on_error_0:
  return EFIFO_ERR_FAILURE;
}

efifo_err_t efifo_fini_lib(void)
{
  post_join_msg();
  close(g_async_mbox.pipe[0]);
  close(g_async_mbox.pipe[1]);
  pthread_join(g_async_thread, NULL);
  return EFIFO_ERR_SUCCESS;
}


/* exported: open close fifo handle */

void efifo_init_desc(efifo_desc_t* desc)
{
  /* initialize a desc with default, invalid values */
  memset(desc, 0, sizeof(efifo_desc_t));
}

efifo_err_t efifo_open(efifo_handle_t* fifo, const efifo_desc_t* desc)
{
  /* TODO: check mapping does not overflow bar size */

  /* fifo data granularity is 32 bits */
  if (desc->width & (sizeof(uint32_t) - 1))
  {
    PERROR();
    goto on_error_0;
  }

  /* capture description */

  fifo->flags = desc->flags;
  fifo->width = desc->width / sizeof(uint32_t);
  fifo->depth = desc->depth;

  /* assume desc->write_width a pow2 */
  /* write width, in bytes */
  if (desc->write_width && (desc->write_width != sizeof(uint16_t)))
  {
    PERROR();
    goto on_error_0;
  }

  fifo->write_width = desc->write_width;

  fifo->epci_handle = desc->epci_handle;
  fifo->ctrl_reg_off = desc->ctrl_reg_off;
  fifo->rdat_reg_off = desc->rdat_reg_off;
  fifo->wdat_reg_off = desc->wdat_reg_off;
  fifo->stat_reg_off = desc->stat_reg_off;

  fifo->slave_id = desc->ebone_slave_id;

  /* disable irq before reset */
  /* reseting the FIFO make the occupancy drop to 0 */
  /* possibly triggering an interrupt irq_en is 1 */

  disable_irq(fifo);

  read_mirq(fifo, &fifo->irq_mask);
  fifo->irq_pos = mask_to_pos(fifo->irq_mask);

  reset_fifo(fifo);

  if (pipe(fifo->async_pipe))
  {
    PERROR();
    goto on_error_0;
  }

  /* asynchronous messaging */
  if (pthread_mutex_init(&fifo->async_post_lock, NULL))
  {
    PERROR();
    goto on_error_1;
  }

  if (pthread_mutex_init(&fifo->async_compl_lock, NULL))
  {
    PERROR();
    goto on_error_2;
  }

  if (pthread_cond_init(&fifo->async_compl_cond, NULL))
  {
    PERROR();
    goto on_error_3;
  }

  fifo->async_data = NULL;
  fifo->async_size = 0;
  atomic_write(&fifo->async_wpos, 0);
  atomic_write(&fifo->async_rpos, 0);
  fifo->async_state = ASYNC_STATE_STOPPED;

  return EFIFO_ERR_SUCCESS;

 on_error_3:
  pthread_mutex_destroy(&fifo->async_compl_lock);
 on_error_2:
  pthread_mutex_destroy(&fifo->async_post_lock);
 on_error_1:
  close(fifo->async_pipe[0]);
  close(fifo->async_pipe[1]);
 on_error_0:
  return EFIFO_ERR_FAILURE;
}

efifo_err_t efifo_close(efifo_handle_t* fifo)
{
  close(fifo->async_pipe[0]);
  close(fifo->async_pipe[1]);

  pthread_cond_destroy(&fifo->async_compl_cond);
  pthread_mutex_destroy(&fifo->async_compl_lock);
  pthread_mutex_destroy(&fifo->async_post_lock);

  return EFIFO_ERR_SUCCESS;
}


/* exported: synchronous io operations */

/* low level synchronous io operations */
/* size is an inout parameter: in input, it contains the maxium size */
/* to be read or written. in output, it contains the resulting size. */

efifo_err_t efifo_read(efifo_handle_t* fifo, void* buf, size_t* count)
{
  /* count: how many width sized items to read */

  size_t rem_count;
  size_t occupancy;
  uint32_t* data;

  ASSUME(fifo->flags & EFIFO_FLAG_READ);

  data = (uint32_t*)buf;

  rem_count = *count;
  while (rem_count)
  {
  redo_occupancy:
    occupancy = get_occupancy(fifo);
    if (occupancy == 0)
    {
      slowdown_polling();
      goto redo_occupancy;
    }

    if (occupancy > rem_count) occupancy = rem_count;
    read_dat_n(fifo, data, occupancy);

    data += occupancy * fifo->width;
    rem_count -= occupancy;
  }

  return EFIFO_ERR_SUCCESS;
}

efifo_err_t efifo_write(efifo_handle_t* fifo, const void* buf, size_t* count)
{
  /* count: how many width sized items to read */

  size_t rem_count;
  size_t navail;
  const uint32_t* data;

  ASSUME(fifo->flags & EFIFO_FLAG_WRITE);

  data = (const uint32_t*)buf;

  rem_count = *count;
  while (rem_count)
  {
  redo_occupancy:
    navail = fifo->depth - efifo_get_occupancy(fifo);

    ASSUME((ssize_t)navail >= 0);

    if (navail == 0)
    {
      slowdown_polling();
      goto redo_occupancy;
    }

    if (navail > rem_count) navail = rem_count;
    write_dat_n(fifo, data, navail);

    data += navail * fifo->width;
    rem_count -= navail;
  }

  return EFIFO_ERR_SUCCESS;
}

efifo_err_t efifo_read_ebuf
(efifo_handle_t* fifo, ebuf_handle_t* buf, size_t off, size_t* size)
{
  /* WARNING: off is an offset in bytes */
  /* *size is in item size */
  return efifo_read(fifo, ebuf_get_data(buf) + off, size);
}

efifo_err_t efifo_write_ebuf
(efifo_handle_t* fifo, ebuf_handle_t* buf, size_t off, size_t* size)
{
  /* WARNING: off is an offset in bytes */
  /* *size is in item size */
  return efifo_write(fifo, ebuf_get_data(buf) + off, size);
}

size_t efifo_get_occupancy(efifo_handle_t* fifo)
{
  return get_occupancy(fifo);
}


/* exported: asynchronous operations */

efifo_err_t efifo_init_async_desc(efifo_async_desc_t* desc)
{
  desc->flags = 0;
  return EFIFO_ERR_SUCCESS;
}

efifo_err_t efifo_init_read_async_desc
(efifo_async_desc_t* desc, void* data, size_t size)
{
  /* size in item count */

  /* by default, a signal will be sent to the runtime when the buffer */
  /* is half full or 100 ms have elapsed since the last hardware irq */

  efifo_init_async_desc(desc);

  desc->flags |= EFIFO_ASYNC_FLAG_READ;
  desc->buf_data = data;
  desc->buf_size = size;

  desc->flags |= EFIFO_ASYNC_FLAG_THRESHOLD;
  desc->thresh = (size / 2) + (size & 1);

  desc->flags |= EFIFO_ASYNC_FLAG_TIMEOUT;
  desc->tmout = 100000;

  return EFIFO_ERR_SUCCESS;
}

efifo_err_t efifo_start_async
(efifo_handle_t* fifo, const efifo_async_desc_t* desc)
{
  /* there will be an error if fifo already started */
  if (post_start_msg(fifo, desc))
  {
    PERROR();
    return EFIFO_ERR_FAILURE;
  }

  return EFIFO_ERR_SUCCESS;
}

efifo_err_t efifo_stop_async(efifo_handle_t* fifo)
{
  /* there will be an error if fifo not started */
  if (post_stop_msg(fifo))
  {
    PERROR();
    return EFIFO_ERR_FAILURE;
  }

  return EFIFO_ERR_SUCCESS;
}

efifo_err_t efifo_get_async_data(efifo_handle_t* fifo, void** data, size_t* n)
{
  /* since the fifo async data is managed as a ring buffer, not all */
  /* the data may be contiguous. as the caller wants contiguous data, */
  /* the returned size may be less than the occupancy and 2 calls might. */
  /* be needed. refer to efifo_copy_async_data. */

  /* width in bytes */
  const size_t width = efifo_get_width(fifo);
  const size_t size = (size_t)atomic_read(&fifo->async_size);
  const size_t rpos = (size_t)atomic_read(&fifo->async_rpos);

  /* compute *n so to get only contiguous data */
  *n = efifo_get_async_occupancy(fifo);
  if ((rpos + *n) > size) *n = size - rpos;

  *data = (uint8_t*)fifo->async_data + rpos * width;

  return EFIFO_ERR_SUCCESS;
}

efifo_err_t efifo_seek_async_data(efifo_handle_t* fifo, size_t size)
{
  /* update fifo->async_rpos */

  size_t new_rpos = (size_t)atomic_read(&fifo->async_rpos) + size;

  if (new_rpos >= fifo->async_size)
  {
    new_rpos -= fifo->async_size;
  }

  atomic_write(&fifo->async_rpos, (uint32_t)new_rpos);

  return EFIFO_ERR_SUCCESS;
}

efifo_err_t efifo_copy_async_data(efifo_handle_t* fifo, void* data, size_t size)
{
  /* assume size <= efifo_get_async_occupancy */

  /* width in bytes */
  const size_t width = efifo_get_width(fifo);

  const size_t rpos = (size_t)atomic_read(&fifo->async_rpos);
  size_t lsize;

  if ((rpos + size) > fifo->async_size) lsize = fifo->async_size - rpos;
  else lsize = size;

  memcpy(data, fifo->async_data + rpos * width, lsize * width);

  if (lsize != size)
  {
    const size_t rsize = size - lsize;
    memcpy((uint8_t*)data + lsize * width , fifo->async_data, rsize * width);
  }

  efifo_seek_async_data(fifo, size);

  return EFIFO_ERR_SUCCESS;
}

size_t efifo_get_async_occupancy(efifo_handle_t* fifo)
{
  const size_t rpos = (size_t)atomic_read(&fifo->async_rpos);
  const size_t wpos = (size_t)atomic_read(&fifo->async_wpos);

  if (rpos <= wpos) return wpos - rpos;
  return wpos + fifo->async_size - rpos;
}

size_t efifo_get_async_err(efifo_handle_t* fifo)
{
  return atomic_read(&fifo->async_err);
}


/* exported: unit testing */

void efifo_set_reset_bit(efifo_handle_t* fifo)
{
  set_reset_bit(fifo);
}

void efifo_clear_reset_bit(efifo_handle_t* fifo)
{
  clear_reset_bit(fifo);
}
