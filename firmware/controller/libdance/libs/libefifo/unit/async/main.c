/* efifo unit testing */

/*
  hardware notes
  --------------

  ebone clock is 125mhz
  sync fifo clock is ebone clock
  async fifo clock is 200mhz

  fifos are all 1024 items large
  fifo[0] is 64bits wide, read, sync, irq
  fifo[1] is 64bits wide, read, async, noirq
  fifo[2] is 32bits wide, write, sync, noirq

  fsm procedure
  hold the fifo reset true
  configure the automaton
  deassert fifo reset

*/

/* TODO, refer to CH email: */
/* Un seul changement: */
/* FIFO #3 */
/* input port 16 bits. */
/* Donc il faut écrire deux fois (LS 16 bits) */
/* pour remplir une case 32 bits du fifo.  */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/select.h>
#include "libepci.h"
#include "libefifo.h"
#include "libebuf.h"


/* configuration macros */
#define CONFIG_DEBUG 1
#define CONFIG_TEST 1
#define CONFIG_BENCH 0
#define CONFIG_PROBES 0


#if CONFIG_DEBUG
#include <stdio.h>
#define PRINTF(__s, ...)  \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define PERROR() printf("[!] %d\n", __LINE__)
#define ASSERT(__x) if (!(__x)) printf("[!] %d\n", __LINE__)
#else
#define PRINTF(__s, ...)
#define PERROR()
#deifne ASSERT(__x)
#endif


/* statistics counter */

__attribute__((unused))
static int read_err_counters(uint32_t* err2, uint32_t* err3)
{
  /* bar0, 0x28: more than 1 word access 16 bits counter */
  /* bar0, 0x2c: 1 word TLP read counter 32 bits counter */

  epcihandle_t bar0_handle;

  bar0_handle = epci_open("10ee:eb01", NULL, 0);
  if (bar0_handle == EPCI_BAD_HANDLE) return -1;

  epci_rd32_reg(bar0_handle, 0x28, err2);
  epci_rd32_reg(bar0_handle, 0x2c, err3);

  epci_close(bar0_handle);

  return 0;
}


/* testing fsm */

extern void efifo_set_reset_bit(efifo_handle_t*);
extern void efifo_clear_reset_bit(efifo_handle_t*);

static void write_fsm0_ctl(epcihandle_t h, uint32_t x)
{
  /* write fsm0 control register */
  epci_wr32_reg(h, 0x80, x);
}

static void write_fsm1_ctl(epcihandle_t h, uint32_t x)
{
  /* write fsm1 control register */
  epci_wr32_reg(h, 0xa0, x);
}


/* buffer routines */

__attribute__((unused))
static void reset_buf(void* buf, size_t size)
{
  memset(buf, 0, size);
}

__attribute__((unused))
static void dump_buf(const void* buf, size_t depth, size_t width)
{
  const uint8_t* p = buf;

  size_t j;
  size_t i;

  for (j = 0; j < depth; ++j)
  {
    printf("[ %04x ] ", j);
    for (i = 0; i < width; ++i, ++p) printf("%02x", *p);
    printf("\n");
  }
}

static inline uint64_t make_value(uint64_t x)
{
  union
  {
    uint8_t arr[sizeof(uint64_t)];
    uint64_t val;
  } y;

  y.arr[0] = (uint8_t)x;
  y.arr[1] = (uint8_t)(x >> 8);
  y.arr[2] = 0;
  y.arr[3] = 0;
  y.arr[4] = 0;
  y.arr[5] = 0;
  y.arr[6] = (uint8_t)x;
  y.arr[7] = (uint8_t)(x >> 8);

  return y.val;
}

__attribute__((unused))
static int check_buf64(const void* buf, size_t depth)
{
  const uint64_t* p = buf;
  int err = 0;
  size_t i;

  for (i = 0; i < depth; ++i, ++p)
  {
    const uint64_t x = make_value(depth - i);
    if (x != *p)
    {
      printf("invalid value at %u: 0x%llx != 0x%llx\n", i, *p, x);
      err = -1;
      break ;
    }
  }

  return err;
}

static inline const unsigned int is_ronly(efifo_handle_t* h)
{
  return (h->flags & EFIFO_FLAG_WRITE) ? 0 : 1;
}

#if (CONFIG_BENCH == 1)
static void dummy_load_routine(void)
{
  volatile uint32_t i;
  for (i = 0; i < 75000; ++i) __asm__ __volatile__ ("nop\t\n");
}

static uint32_t read_sta1(void)
{
  epcihandle_t bar0_handle;
  uint32_t x;

  bar0_handle = epci_open("10ee:eb01", NULL, 0);
  if (bar0_handle == EPCI_BAD_HANDLE) return -1;
  epci_rd32_reg(bar0_handle, 0x14, &x);
  epci_close(bar0_handle);

  return x;
}

#endif /* CONFIG_BENCH */


#if (CONFIG_PROBES == 1)
static void setup_probes(void)
{
  /* use ep_interrupt signals */

  /* control register 1 (0x04) */

  const char* probe0_value = getenv("PROBE0");
  const char* probe1_value = getenv("PROBE1");
  const char* probe2_value = getenv("PROBE2");
  const char* probe3_value = getenv("PROBE3");

  epcihandle_t bar0_handle;
  uint32_t x;

  bar0_handle = epci_open("10ee:eb01", NULL, 0);
  if (bar0_handle == EPCI_BAD_HANDLE) return ;

  if ((probe0_value = getenv("PROBE0")) == NULL) probe0_value = "22";
  if ((probe1_value = getenv("PROBE1")) == NULL) probe1_value = "0";
  if ((probe2_value = getenv("PROBE2")) == NULL) probe2_value = "2";
  if ((probe3_value = getenv("PROBE3")) == NULL) probe3_value = "17";

  x = 0;
  x |= atoi(probe0_value) << 0; /* probe 0 */
  x |= atoi(probe1_value) << 5; /* probe 1 */
  x |= atoi(probe2_value) << 10; /* probe 2 */
  x |= atoi(probe3_value) << 15; /* probe 3 */
  epci_wr32_reg(bar0_handle, 0x04, x);

  epci_close(bar0_handle);
}
#endif /* CONFIG_PROBES */

__attribute__((unused)) static void reset_ebone(void)
{
  epcihandle_t bar0_handle;
  uint32_t x;

  bar0_handle = epci_open("10ee:eb01", NULL, 0);
  if (bar0_handle == EPCI_BAD_HANDLE) return ;

  /* pulse ctrl1 bit 30 */
  epci_rd32_reg(bar0_handle, 0x04, &x);
  x |= 1 << 30;
  epci_wr32_reg(bar0_handle, 0x04, x);
  usleep(100000);
  x &= ~(1 << 30);
  epci_wr32_reg(bar0_handle, 0x04, x);

  epci_close(bar0_handle);
}

static int test_fifo(efifo_handle_t handles[3], size_t fifo_index)
{
#define DELAY_UNIT_1_US 0x0
#define DELAY_UNIT_10_US 0x1
#define DELAY_UNIT_100_US 0x2
#define DELAY_UNIT_1_MS 0x3
#define DELAY_UNIT_SINGLE_BURST 0x0
#define NEXT_DELAY_SINGLE_BURST 0x0
#if 0
  static const uint32_t delay_unit = DELAY_UNIT_SINGLE_BURST;
  static const uint32_t next_delay = NEXT_DELAY_SINGLE_BURST;
#else
  static const uint32_t delay_unit = DELAY_UNIT_100_US;
  static const uint32_t next_delay = 5; /* 32 max */
#endif
  static const uint32_t intra_clk = 255; /* 255 max */
  const uint32_t burst_size = (uint32_t)4;
  const uint32_t fsm_ctl =
    (delay_unit << 30) | (next_delay << 24) | (intra_clk << 16) | burst_size;

  efifo_handle_t* const h = &handles[fifo_index]; 
  const unsigned int is_ro = is_ronly(h);

  const size_t depth = efifo_get_depth(h);

  /* width in bytes */
  const size_t width = efifo_get_width(h);

  const size_t buf_count = 8 * depth;
  const size_t buf_size = buf_count * width;

  ebuf_handle_t buf;

  efifo_async_desc_t desc;

  size_t pass = 0;

#if (CONFIG_BENCH == 1)
  size_t overflow_counter = 0;
#endif /* CONFIG_BENCH */

#if (CONFIG_PROBES == 1)
  setup_probes();
#endif /* CONFIG_PROBES */

  if (ebuf_init_lib(EBUF_FLAG_DEFAULT)) goto on_error_0;
  if (ebuf_alloc(&buf, buf_size, EBUF_FLAG_DEFAULT)) goto on_error_1;
  reset_buf(ebuf_get_data(&buf), ebuf_get_size(&buf));

  /* setup fsm in case of read only fifo */
  if (is_ro)
  {
    epcihandle_t const eh = h->epci_handle;
    void (*write_fsm_ctl)(epcihandle_t, uint32_t) = NULL;

    /* fsm setup prolog */
    efifo_set_reset_bit(h);

    /* setup fsm */
    if (fifo_index == 0) write_fsm_ctl = write_fsm0_ctl;
    else if (fifo_index == 1) write_fsm_ctl = write_fsm1_ctl;
    write_fsm_ctl(eh, fsm_ctl);

    /* fsm setup epilog */
    efifo_clear_reset_bit(h);
  }

  /* start async operation */
  efifo_init_read_async_desc(&desc, ebuf_get_data(&buf), buf_count);
  desc.thresh = depth;
  efifo_start_async(h, &desc);

  /* readout */
  while (1)
  {
    /* wait for signal */
    {
      const int rfd = h->async_pipe[0];
      fd_set rds;
      uint8_t x;
      FD_ZERO(&rds);
      FD_SET(rfd, &rds);
      select(rfd + 1, &rds, NULL, NULL, NULL);
      read(rfd, &x, 1);
    }

    /* read and check data */
    while (1)
    {
      size_t async_size;
      void* async_data;

      efifo_get_async_data(h, &async_data, &async_size);

      /* wait for at least depth data */
      if (async_size < burst_size) break ;

      async_size = burst_size;

      if (check_buf64(async_data, burst_size) == -1)
      {
	printf("error, %zu\n", pass);
	efifo_stop_async(h);
	goto on_error_2;
      }

      if (((++pass) % 1000) == 0)
      {
	printf("%zu 0x%08x\n", pass, efifo_get_async_err(h));
      }

      efifo_seek_async_data(h, async_size);
    }
  }

 on_error_2:
  ebuf_free(&buf);
 on_error_1:
  ebuf_fini_lib();
 on_error_0:
  return -1;
}


/* main */

int main(int ac, char** av)
{
  static const unsigned int bar = 1;
  epcihandle_t epci_handle;
#define EFIFO_COUNT 3
  efifo_handle_t efifo_handle[EFIFO_COUNT];
  struct efifo_desc efifo_desc[EFIFO_COUNT];
  int err = -1;

  /* open the bar */

  if ((epci_handle = epci_open("10ee:eb01", NULL, bar)) == EPCI_BAD_HANDLE)
  {
    PERROR();
    goto on_error_0;
  }

  /* initialize library */

  if (efifo_init_lib())
  {
    PERROR();
    goto on_error_1;
  }

  /* fifo[i] descriptions */

  efifo_init_desc(&efifo_desc[0]);
  efifo_desc[0].epci_handle = epci_handle;
  efifo_desc[0].flags = EFIFO_FLAG_READ;
  efifo_desc[0].width = sizeof(uint64_t);
  efifo_desc[0].depth = 1024;
  efifo_desc[0].ctrl_reg_off = 0x84;
  efifo_desc[0].rdat_reg_off = 0x88;
  efifo_desc[0].stat_reg_off = 0x9c;
  efifo_desc[0].ebone_slave_id = 0; /* not available yet */

  efifo_init_desc(&efifo_desc[1]);
  efifo_desc[1].epci_handle = epci_handle;
  efifo_desc[1].flags = EFIFO_FLAG_READ;
  efifo_desc[1].width = sizeof(uint64_t);
  efifo_desc[1].depth = 1024;
  efifo_desc[1].ctrl_reg_off = 0xa4;
  efifo_desc[1].rdat_reg_off = 0xa8;
  efifo_desc[1].stat_reg_off = 0xbc;
  efifo_desc[1].ebone_slave_id = 0; /* not available yet */

  efifo_init_desc(&efifo_desc[2]);
  efifo_desc[2].epci_handle = epci_handle;
  efifo_desc[2].flags = EFIFO_FLAG_READWRITE;
  efifo_desc[2].width = sizeof(uint32_t);
  /* TODO: when asymetric fifo fixed */
  efifo_desc[2].write_width = sizeof(uint16_t);
  efifo_desc[2].depth = 1024;
  efifo_desc[2].wdat_reg_off = 0xc0;
  efifo_desc[2].ctrl_reg_off = 0xc4;
  efifo_desc[2].rdat_reg_off = 0xc8;
  efifo_desc[2].stat_reg_off = 0xcc;
  efifo_desc[2].ebone_slave_id = 0; /* not available yet */

  /* open fifos */

  if (efifo_open(&efifo_handle[0], &efifo_desc[0]) != EFIFO_ERR_SUCCESS)
  {
    PERROR();
    goto on_error_2;
  }

  if (efifo_open(&efifo_handle[1], &efifo_desc[1]) != EFIFO_ERR_SUCCESS)
  {
    PERROR();
    goto on_error_3;
  }

  if (efifo_open(&efifo_handle[2], &efifo_desc[2]) != EFIFO_ERR_SUCCESS)
  {
    PERROR();
    goto on_error_4;
  }

  err = test_fifo(efifo_handle, 0);

  efifo_close(&efifo_handle[2]);
 on_error_4:
  efifo_close(&efifo_handle[1]);
 on_error_3:
  efifo_close(&efifo_handle[0]);
 on_error_2:
  efifo_fini_lib();
 on_error_1:
  epci_close(epci_handle);
 on_error_0:
  return err;
}
