/* efifo unit testing */

/*
  hardware notes
  --------------

  ebone clock is 125mhz
  sync fifo clock is ebone clock
  async fifo clock is 200mhz

  fifos are all 1024 items large
  fifo[0] is 64bits wide, read, sync, irq
  fifo[1] is 64bits wide, read, async, noirq
  fifo[2] is 32bits wide, write, sync, noirq

  fsm procedure
  hold the fifo reset true
  configure the automaton
  deassert fifo reset

*/

/* TODO, refer to CH email: */
/* Un seul changement: */
/* FIFO #3 */
/* input port 16 bits. */
/* Donc il faut écrire deux fois (LS 16 bits) */
/* pour remplir une case 32 bits du fifo.  */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include "libepci.h"
#include "libefifo.h"
#include "libebuf.h"


/* configuration macros */
#define CONFIG_DEBUG 1
#define CONFIG_TEST 1
#define CONFIG_BENCH 0
#define CONFIG_PROBES 0
#define CONFIG_IRQ 0


#if CONFIG_DEBUG
#include <stdio.h>
#define PRINTF(__s, ...)  \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define PERROR() printf("[!] %d\n", __LINE__)
#define ASSERT(__x) if (!(__x)) printf("[!] %d\n", __LINE__)
#else
#define PRINTF(__s, ...)
#define PERROR()
#deifne ASSERT(__x)
#endif


#if CONFIG_IRQ

/* statistics counter */

static int read_err_counters(uint32_t* err2, uint32_t* err3)
{
  /* bar0, 0x28: more than 1 word access 16 bits counter */
  /* bar0, 0x2c: 1 word TLP read counter 32 bits counter */

  epcihandle_t bar0_handle;

  bar0_handle = epci_open("10ee:eb01", NULL, 0);
  if (bar0_handle == EPCI_BAD_HANDLE) return -1;

  epci_rd32_reg(bar0_handle, 0x28, err2);
  epci_rd32_reg(bar0_handle, 0x2c, err3);

  epci_close(bar0_handle);

  return 0;
}

#endif /* CONFIG_IRQ */


/* testing fsm */

extern void efifo_set_reset_bit(efifo_handle_t*);
extern void efifo_clear_reset_bit(efifo_handle_t*);

static void write_fsm0_ctl(epcihandle_t h, uint32_t x)
{
  /* write fsm0 control register */
  epci_wr32_reg(h, 0x80, x);
}

static void write_fsm1_ctl(epcihandle_t h, uint32_t x)
{
  /* write fsm1 control register */
  epci_wr32_reg(h, 0xa0, x);
}


#if CONFIG_IRQ

#include "libuirq.h"

static int wait_irq(efifo_handle_t* h)
{
  uint32_t mask;

  while (1)
  {
    if (uirq_wait(&h->uirq, 1000, &mask))
    {
      /* error or timeout */

      uint32_t err2 = (uint32_t)-1;
      uint32_t err3 = (uint32_t)-1;

      read_err_counters(&err2, &err3);

      printf("--\n");
      printf("timeout\n");
      printf("occupancy  == %u\n", efifo_get_occupancy(h));
      printf("errX       == 0x%08x, 0x%08x\n", err2, err3);
      fflush(stdout);
      getchar();

      /* flush the fifo */
      efifo_set_reset_bit(h);
      efifo_clear_reset_bit(h);
    }

    if (mask & efifo_get_mirq(h)) break ;
  }

  return 0;
}

#endif /* CONFIG_IRQ */


/* buffer routines */

__attribute__((unused))
static void reset_buf(void* buf, size_t size)
{
  memset(buf, 0, size);
}

__attribute__((unused))
static void dump_buf(const void* buf, size_t depth, size_t width)
{
  const uint8_t* p = buf;

  size_t j;
  size_t i;

  for (j = 0; j < depth; ++j)
  {
    printf("[ %04x ] ", j);
    for (i = 0; i < width; ++i, ++p) printf("%02x", *p);
    printf("\n");
  }
}

static inline uint64_t make_value(uint64_t x)
{
  union
  {
    uint8_t arr[sizeof(uint64_t)];
    uint64_t val;
  } y;

  y.arr[0] = (uint8_t)x;
  y.arr[1] = (uint8_t)(x >> 8);
  y.arr[2] = 0;
  y.arr[3] = 0;
  y.arr[4] = 0;
  y.arr[5] = 0;
  y.arr[6] = (uint8_t)x;
  y.arr[7] = (uint8_t)(x >> 8);

  return y.val;
}

__attribute__((unused))
static int check_buf64(const void* buf, size_t depth)
{
  const uint64_t* p = buf;
  int err = 0;
  size_t i;

  for (i = 0; i < depth; ++i, ++p)
  {
    const uint64_t x = make_value(depth - i);
    if (x != *p)
    {
      printf("invalid value at %u: 0x%llx != 0x%llx\n", i, *p, x);
      err = -1;
    }
  }

  if (err == -1) getchar();

  return 0;
}

static inline const unsigned int is_ronly(efifo_handle_t* h)
{
  return (h->flags & EFIFO_FLAG_WRITE) ? 0 : 1;
}

#if (CONFIG_BENCH == 1)
static void dummy_load_routine(void)
{
  volatile uint32_t i;
  for (i = 0; i < 75000; ++i) __asm__ __volatile__ ("nop\t\n");
}

static uint32_t read_sta1(void)
{
  epcihandle_t bar0_handle;
  uint32_t x;

  bar0_handle = epci_open("10ee:eb01", NULL, 0);
  if (bar0_handle == EPCI_BAD_HANDLE) return -1;
  epci_rd32_reg(bar0_handle, 0x14, &x);
  epci_close(bar0_handle);

  return x;
}

#endif /* CONFIG_BENCH */


#if (CONFIG_PROBES == 1)
static void setup_probes(void)
{
  /* use ep_interrupt signals */

  /* control register 1 (0x04) */

  const char* probe0_value = getenv("PROBE0");
  const char* probe1_value = getenv("PROBE1");
  const char* probe2_value = getenv("PROBE2");
  const char* probe3_value = getenv("PROBE3");

  epcihandle_t bar0_handle;
  uint32_t x;

  bar0_handle = epci_open("10ee:eb01", NULL, 0);
  if (bar0_handle == EPCI_BAD_HANDLE) return ;

  if ((probe0_value = getenv("PROBE0")) == NULL) probe0_value = "22";
  if ((probe1_value = getenv("PROBE1")) == NULL) probe1_value = "0";
  if ((probe2_value = getenv("PROBE2")) == NULL) probe2_value = "2";
  if ((probe3_value = getenv("PROBE3")) == NULL) probe3_value = "17";

  x = 0;
  x |= atoi(probe0_value) << 0; /* probe 0 */
  x |= atoi(probe1_value) << 5; /* probe 1 */
  x |= atoi(probe2_value) << 10; /* probe 2 */
  x |= atoi(probe3_value) << 15; /* probe 3 */
  epci_wr32_reg(bar0_handle, 0x04, x);

  epci_close(bar0_handle);
}
#endif /* CONFIG_PROBES */

static void reset_ebone(void)
{
  epcihandle_t bar0_handle;
  uint32_t x;

  bar0_handle = epci_open("10ee:eb01", NULL, 0);
  if (bar0_handle == EPCI_BAD_HANDLE) return ;

  /* pulse ctrl1 bit 30 */
  epci_rd32_reg(bar0_handle, 0x04, &x);
  x |= 1 << 30;
  epci_wr32_reg(bar0_handle, 0x04, x);
  usleep(100000);
  x &= ~(1 << 30);
  epci_wr32_reg(bar0_handle, 0x04, x);

  epci_close(bar0_handle);
}

static int test_fifo(efifo_handle_t handles[3], size_t fifo_index)
{
#define DELAY_UNIT_1_US 0x0
#define DELAY_UNIT_10_US 0x1
#define DELAY_UNIT_100_US 0x2
#define DELAY_UNIT_1_MS 0x3
#define DELAY_UNIT_SINGLE_BURST 0x0
#define NEXT_DELAY_SINGLE_BURST 0x0
#if 0
  static const uint32_t delay_unit = DELAY_UNIT_SINGLE_BURST;
  static const uint32_t next_delay = NEXT_DELAY_SINGLE_BURST;
#else
  static const uint32_t delay_unit = DELAY_UNIT_10_US;
  static const uint32_t next_delay = 32;
#endif
  static const uint32_t intra_clk = 0; /* 255 max */

  efifo_handle_t* const h = &handles[fifo_index]; 
  const unsigned int is_ro = is_ronly(h);

  const uint32_t depth = 8; /* 0xffff max */

  const size_t width = efifo_get_width(h);

  /* if fifo is 32 bit wide, readwrite 2 vals at a time */
  const size_t mul = (width == sizeof(uint32_t)) ? 2 : 1;

  ebuf_err_t buf_err;

  size_t size;
  ebuf_handle_t buf;
  void* data;

#if (CONFIG_BENCH == 1)
  size_t overflow_counter = 0;
#endif /* CONFIG_BENCH */

#if (CONFIG_PROBES == 1)
  setup_probes();
#endif /* CONFIG_PROBES */

  /* allocate a buffer to contain at least depth * width items */

  buf_err = ebuf_init_lib(EBUF_FLAG_DEFAULT);
  if (buf_err != EBUF_ERR_SUCCESS) return -1;

  buf_err = ebuf_alloc(&buf, depth * width * sizeof(uint32_t), EBUF_FLAG_DEFAULT);
  if (buf_err != EBUF_ERR_SUCCESS) return -1;
  data = ebuf_get_data(&buf);

  /* setup fsm in case of read only fifo */

  if (is_ro)
  {
    epcihandle_t const eh = h->epci_handle;
    void (*write_fsm_ctl)(epcihandle_t, uint32_t) = NULL;

    /* fsm setup prolog */

    efifo_set_reset_bit(h);

    /* setup fsm */

    if (fifo_index == 0) write_fsm_ctl = write_fsm0_ctl;
    else if (fifo_index == 1) write_fsm_ctl = write_fsm1_ctl;

    write_fsm_ctl
      (eh, (delay_unit << 30) | (next_delay << 24) | (intra_clk << 16) | depth);

    /* fsm setup epilog */

    efifo_clear_reset_bit(h);
  }
  else
  {
    /* prefill write buffer.
       fifo[2] is a 32 bits wide fifo. as the readout buffer checking
       routines expects for a 64 bit wide fifo, we write 2 items at a
       time, making the buffer twice as large
    */

    size_t i;
    for (i = 0; i < depth; ++i) ((uint64_t*)data)[i] = make_value(depth - i);
  }

  while (1)
  {
    /* software writing fsm equivalent */

    size = depth * mul;

#if (CONFIG_TEST == 1)
    if (is_ro == 0)
    {
      size_t o;

      o = efifo_get_occupancy(h);
      if (o)
      {
	printf("non zero occupancy %u\n", o);
	goto on_error;
      }

      if (efifo_write_ebuf(h, &buf, 0, &size) != EFIFO_ERR_SUCCESS)
      {
	printf("efifo_write() != EFIFO_ERR_SUCCESS\n");
	goto on_error;
      }

      if (size != (depth * mul))
      {
	printf("size != (depth * mul)\n");
	goto on_error;
      }

      o = efifo_get_occupancy(h);
      if (o != size)
      {
	printf("invalid occupancy after write: 0x%04x != 0x%04x\n", o, size);

	if (o == (size_t)-1)
	{
	  printf("reseting ebone\n");
	  reset_ebone();
	}

	goto on_error;
      }
    }
#endif /* CONFIG_TEST */

    /* readout */

#if (CONFIG_TEST == 1)
    reset_buf(data, size * sizeof(uint32_t));
#endif /* CONFIG_TEST */

#if (CONFIG_BENCH == 1)
    if (efifo_is_full(h))
    {
      printf("overflow at %u, %u\n", overflow_counter, efifo_get_occupancy(h));
      printf("stat1: 0x%08x\n", read_sta1());
      overflow_counter = 0;
      goto on_error;
    }
    else
    {
      dummy_load_routine();
      ++overflow_counter;
    }
#endif /* CONFIG_BENCH */

#if CONFIG_IRQ
    if (h->flags & EFIFO_FLAG_IRQ)
    {
      if (wait_irq(h) == -1)
      {
	printf("wait_irq() == -1\n");
	goto on_error;
      }
    }
#endif /* CONFIG_IRQ */

    if (efifo_read_ebuf(h, &buf, 0, &size) != EFIFO_ERR_SUCCESS)
    {
      printf("efifo_read() != EFIFO_ERR_SUCCESS\n");
      goto on_error;
    }

    if (size != (depth * mul))
    {
      printf("size != depth * mul\n");
      goto on_error;
    }

#if CONFIG_TEST
    if (check_buf64(data, depth) == -1)
      goto on_error;
    printf("pass\n");
#endif /* CONFIG_TEST */
  }

 on_error:
  ebuf_free(&buf);
  return -1;
}


/* main */

int main(int ac, char** av)
{
  static const unsigned int bar = 1;
  epcihandle_t epci_handle;
#define EFIFO_COUNT 3
  efifo_handle_t efifo_handle[EFIFO_COUNT];
  struct efifo_desc efifo_desc[EFIFO_COUNT];
  int err = -1;

  /* open the bar */

  if ((epci_handle = epci_open("10ee:eb01", NULL, bar)) == EPCI_BAD_HANDLE)
  {
    PERROR();
    goto on_error_0;
  }

  /* initialize library */

  if (efifo_init_lib())
  {
    PERROR();
    goto on_error_1;
  }

  /* fifo[i] descriptions */

  efifo_init_desc(&efifo_desc[0]);
  efifo_desc[0].epci_handle = epci_handle;
#if CONFIG_IRQ
  efifo_desc[0].flags = EFIFO_FLAG_READ | EFIFO_FLAG_IRQ;
  efifo_desc[1].irq_threshold = 4;
#else
  efifo_desc[0].flags = EFIFO_FLAG_READ;
#endif
  efifo_desc[0].width = sizeof(uint64_t);
  efifo_desc[0].depth = 1024;
  efifo_desc[0].ctrl_reg_off = 0x84;
  efifo_desc[0].rdat_reg_off = 0x88;
  efifo_desc[0].stat_reg_off = 0x9c;
  efifo_desc[0].ebone_slave_id = 0; /* not available yet */

  efifo_init_desc(&efifo_desc[1]);
  efifo_desc[1].epci_handle = epci_handle;
#if CONFIG_IRQ
  efifo_desc[1].flags = EFIFO_FLAG_READ | EFIFO_FLAG_IRQ;
  efifo_desc[1].irq_threshold = 4;
#else
  efifo_desc[1].flags = EFIFO_FLAG_READ;
#endif
  efifo_desc[1].width = sizeof(uint64_t);
  efifo_desc[1].depth = 1024;
  efifo_desc[1].ctrl_reg_off = 0xa4;
  efifo_desc[1].rdat_reg_off = 0xa8;
  efifo_desc[1].stat_reg_off = 0xbc;
  efifo_desc[1].ebone_slave_id = 0; /* not available yet */

  efifo_init_desc(&efifo_desc[2]);
  efifo_desc[2].epci_handle = epci_handle;
#if CONFIG_IRQ
  efifo_desc[2].flags = EFIFO_FLAG_READWRITE | EFIFO_FLAG_IRQ;
  efifo_desc[2].irq_threshold = 4;
#else
  efifo_desc[2].flags = EFIFO_FLAG_READWRITE;
#endif
  efifo_desc[2].width = sizeof(uint32_t);
  /* TODO: when asymetric fifo fixed */
  efifo_desc[2].write_width = sizeof(uint16_t);
  efifo_desc[2].depth = 1024;
  efifo_desc[2].wdat_reg_off = 0xc0;
  efifo_desc[2].ctrl_reg_off = 0xc4;
  efifo_desc[2].rdat_reg_off = 0xc8;
  efifo_desc[2].stat_reg_off = 0xcc;
  efifo_desc[2].ebone_slave_id = 0; /* not available yet */

  /* open fifos */

  if (efifo_open(&efifo_handle[0], &efifo_desc[0]) != EFIFO_ERR_SUCCESS)
  {
    PERROR();
    goto on_error_2;
  }

  if (efifo_open(&efifo_handle[1], &efifo_desc[1]) != EFIFO_ERR_SUCCESS)
  {
    PERROR();
    goto on_error_3;
  }

  if (efifo_open(&efifo_handle[2], &efifo_desc[2]) != EFIFO_ERR_SUCCESS)
  {
    PERROR();
    goto on_error_4;
  }

#if CONFIG_IRQ
  /* acknowledge irqs */
  size_t i;
  for (i = 0; i < EFIFO_COUNT; ++i)
  {
    if (efifo_desc[i].flags & EFIFO_FLAG_IRQ)
      uirq_ack(&efifo_handle[i].uirq);
  }
#endif /* CONFIG_IRQ */

  getchar();

  err = test_fifo(efifo_handle, 0);

  efifo_close(&efifo_handle[2]);
 on_error_4:
  efifo_close(&efifo_handle[1]);
 on_error_3:
  efifo_close(&efifo_handle[0]);
 on_error_2:
  efifo_fini_lib();
 on_error_1:
  epci_close(epci_handle);
 on_error_0:
  return err;
}
