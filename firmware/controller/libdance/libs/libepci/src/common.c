//
// File: common.c
//
//

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>      // uint16_t, uint32_t, ...

#include "common.h"


//-------------------------------------------------------------------
// Parameter related definitions
//


#define DPAR(code, name, flags) {code, name, flags}
parentry_t par_list[] = { DPAR_LIST };
#undef DPAR

int        par_list_sz =(sizeof(par_list)/sizeof(parentry_t));





//-------------------------------------------------------------------
// Global library parameters
//
lpars_t lib_pars = {
  2,               // lib debug
  {
   4,              // driver debug
  }
};


