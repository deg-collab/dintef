//
// File: dancebuffer.c
//
//

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>      // uint16_t, uint32_t, ...


#include "common.h"
#include "dancebuffer.h"


//-------------------------------------------------------------------
// Handler related definitions
//

struct dancebuffer_s {
    int   signature;

    int   buf_type;
    int   buf_depth;
    int   data_width;
    void *data_buf;
};

#define DBUF_MAX_HANDLERS 50
struct dancebuffer_s lib_dbuf_list[DBUF_MAX_HANDLERS];


#define DBUF_HNDL_SIGNATURE 0xdefecada



//-------------------------------------------------------------------
// Library utilities
//


dancebuffer_t dbuf_new_handle(void) {
   int i;

   for (i = 0; i < DBUF_MAX_HANDLERS; i++) {
      if (lib_dbuf_list[i].signature != DBUF_HNDL_SIGNATURE) {
//         lib_dbuf_list[i].device_id = NULL;
         return(&lib_dbuf_list[i]);
      }
   }
   return(DANCEBUFFER_BAD_HANDLE);
}



//-------------------------------------------------------------------
// Library entries
//


//--------------------------------------
//
dancebuffer_t dancebuffer_alloc(
                 int           data_width,
                 int           buf_depth,
                 int          *type_flags,
                 void         *mem_ptr)
{
    dancebuffer_t dbuf;

    EPCI_DEBUGF_2("dancebuffer_alloc(width=%d, depth=%d)\n",data_width, buf_depth);

    // Get a new handle
    if ((dbuf = dbuf_new_handle()) == DANCEBUFFER_BAD_HANDLE) {
        // TODO
        //lib_error(EPCI_ERR, "fail to allocate handle");
        return(DANCEBUFFER_BAD_HANDLE);
    }


    // normal end
    return(dbuf);
}


//--------------------------------------
//
dancebuffer_t dancebuffer_bytealloc(
                 int           buf_depth)
{
    return(NULL);
}


//--------------------------------------
//
dancebuffer_t dancebuffer_realloc(
                 dancebuffer_t dbuf,
                 int           buf_depth)
{
    return(NULL);
}


//--------------------------------------
//
void          dancebuffer_free(
                 dancebuffer_t dbuf)
{
}
