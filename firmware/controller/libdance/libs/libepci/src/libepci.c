#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <ctype.h>
#include <pci/pci.h>
#include <sys/mman.h>
#include "common.h"
#include "libepci.h"

static const char* RcsRevision =
#ifdef WITHIN_LIBDANCE
    "same as LIBDANCE";
#else
    "$Revision: 2192 $";
#endif


/* libpci global context */

static unsigned int libpci_refn = 0;
static struct pci_access* libpci_access = NULL;

static struct pci_access* get_libpci_access(void)
{
  if (libpci_refn == 0)
  {
    libpci_access = pci_alloc();
    if (libpci_access == NULL) return NULL;

    pci_init(libpci_access);
    libpci_access->method = PCI_ACCESS_I386_TYPE1;
    pci_scan_bus(libpci_access);
  }

  ++libpci_refn;

  return libpci_access;
}

static void put_libpci_access(void)
{
  /* deallocate on last put */
  if (libpci_refn == 1)
  {
    pci_cleanup(libpci_access);
    libpci_access = NULL;
  }

  --libpci_refn;
}


/* error related routines */

#define	EPCI_ERROR_MSG_SZ 2048
static char error_msg[EPCI_ERROR_MSG_SZ];
static epcierror_t last_error = EPCI_OK;

epcierror_t lib_error(epcierror_t err, const char *fmt, ...)
{
  va_list arg_ptr;
  int len;

  va_start(arg_ptr, fmt);
  len = vsnprintf(error_msg, EPCI_ERROR_MSG_SZ, fmt, arg_ptr);
  error_msg[len] = 0;
  va_end(arg_ptr);

  EPCI_DEBUGF_1("ERROR: %s\n", error_msg);

  last_error = err;
  return err;
}

epcierror_t epci_error(const char** errmsg)
{
  *errmsg = error_msg;
  return last_error;
}


/* device open close routines */

static inline void clear_dev(epcihandle_t d)
{
  d->bar_size = 0;
  d->pci_dev = NULL;
}

static size_t get_bar_size(struct pci_access* a, struct pci_dev* d, size_t i)
{
  const enum pci_access_type saved_method = a->method;
  const int addr = PCI_BASE_ADDRESS_0 + (int)i * 4;
  uint32_t size;
  uint32_t saved_long;

  a->method = PCI_ACCESS_I386_TYPE1;

  saved_long = pci_read_long(d, addr);

  pci_write_long(d, addr, (uint32_t)-1);
  size = pci_read_long(d, addr);
#ifndef PCI_ADDR_FLAG_MASK
#define PCI_ADDR_FLAG_MASK 0xf
#endif
  size &= ~PCI_ADDR_FLAG_MASK;
  size = ~size + 1;

  pci_write_long(d, addr, saved_long);

  a->method = saved_method;

  return size;
}

#ifdef CONFIG_FREESCALE_IMX6

static unsigned int imx6_quirk_once = 0;

#include <signal.h>

static void on_sigbus(int x) {}

static void do_imx6_quirk(epcihandle_t bar, struct pci_dev* pci_dev)
{
  /* on imx6, lspci shows that the bar are disabled. we */
  /* enable the device using sysfs */

  /* the very first access to the device raises a bus */
  /* error. to fix this, we force a first access a catch */
  /* the corresponding bus error, if any. a cleaner fix */
  /* must be found */

  char sysfs_path[256];
  int fd;
  int n;
  uint32_t x;

  if (imx6_quirk_once == 1) return ;

  n = snprintf
  (
   sysfs_path, sizeof(sysfs_path) - 1,
   "/sys/bus/pci/devices/0000:%02x:%02x.%01x/enable",
   pci_dev->bus, pci_dev->dev, pci_dev->func
  );
  if (n > (sizeof(sysfs_path) - 1)) n = sizeof(sysfs_path) - 1;
  sysfs_path[n] = 0;

  fd = open(sysfs_path, O_WRONLY);
  if (fd == -1) return ;
  write(fd, "1", 1);
  close(fd);

  signal(SIGBUS, on_sigbus);
  epci_rd32_reg(bar, 0, &x);
  signal(SIGBUS, SIG_DFL);

  imx6_quirk_once = 1;
}

#endif /* CONFIG_FREESCALE_IMX6 */

epcihandle_t epci_open(const char* dev_ids, const char* dev_slot, int bar)
{
  epcihandle_t dev = NULL;
  int devmem_fd = -1;
  unsigned int is_err = 1;
  const char* pci_err;
  struct pci_access* pci_access;
  struct pci_dev* pci_dev;
  struct pci_filter pci_filter;
  char dev_ids_buf[32];
  char dev_slot_buf[32];
  size_t dev_ids_len;
  size_t dev_slot_len;

  dev = malloc(sizeof(struct epcihandle_s));
  if (dev == NULL)
  {
    lib_error(EPCI_ERR, "malloc failed");
    goto on_error_0;
  }

  clear_dev(dev);

  /* retrieve device info using libpci */

  pci_access = get_libpci_access();
  if (pci_access == NULL)
  {
    lib_error(EPCI_ERR, "pci_alloc() failed");
    goto on_error_1;
  }

  /* pci_filter_parse_xxx assume writable string */

  pci_filter_init(pci_access, &pci_filter);

  dev_ids_len = (dev_ids == NULL ? 0 : strlen(dev_ids));
  if (dev_ids_len)
  {
    if (dev_ids_len >= sizeof(dev_ids_buf))
    {
      lib_error(EPCI_ERR, "invalid device ids");
      goto on_error_2;
    }
    memcpy(dev_ids_buf, dev_ids, dev_ids_len);
    dev_ids_buf[dev_ids_len] = 0;

    pci_err = pci_filter_parse_id(&pci_filter, dev_ids_buf);
    if (pci_err != NULL)
    {
      lib_error(EPCI_ERR, pci_err);
      goto on_error_2;
    }
  }

  dev_slot_len = (dev_slot == NULL ? 0 : strlen(dev_slot));
  if (dev_slot_len)
  {
    if (dev_slot_len >= sizeof(dev_slot_buf))
    {
      lib_error(EPCI_ERR, "invalid device slot");
      goto on_error_2;
    }
    memcpy(dev_slot_buf, dev_slot, dev_slot_len);
    dev_slot_buf[dev_slot_len] = 0;

    pci_err = pci_filter_parse_slot(&pci_filter, dev_slot_buf);
    if (pci_err != NULL)
    {
      lib_error(EPCI_ERR, pci_err);
      goto on_error_2;
    }
  }

  for (pci_dev = pci_access->devices; pci_dev != NULL; pci_dev = pci_dev->next)
    if (pci_filter_match(&pci_filter, pci_dev))
      break ;

  if (pci_dev == NULL)
  {
    lib_error(EPCI_ERR, "pci device not found");
    goto on_error_3;
  }

  /* bar or config space handle */

  if (bar != EPCI_CONFIG_BAR)
  {
    pci_fill_info(pci_dev, PCI_FILL_IDENT | PCI_FILL_BASES);

    dev->bar_size = get_bar_size(pci_access, pci_dev, (size_t)bar);
    if (dev->bar_size == 0)
    {
      lib_error(EPCI_INVALID_SIZE, "invalid bar size");
      goto on_error_4;
    }

    dev->bar_paddr = (uintptr_t)pci_dev->base_addr[bar];

    /* map the device bar using /dev/mem */

    devmem_fd = open("/dev/mem", O_RDWR | O_SYNC);
    if (devmem_fd == -1)
    {
      lib_error(EPCI_ERR, "open('/dev/mem') failed");
      goto on_error_5;
    }

    dev->bar_vaddr = mmap
    (
     NULL, dev->bar_size,
     PROT_READ | PROT_WRITE,
     MAP_SHARED | MAP_NORESERVE,
     devmem_fd,
     dev->bar_paddr & ~(dev->bar_size - 1UL)
    );

    if (dev->bar_vaddr == (void*)MAP_FAILED)
    {
      lib_error(EPCI_ERR, "mmap failed");
      goto on_error_6;
    }

#ifdef CONFIG_FREESCALE_IMX6
    do_imx6_quirk(dev, pci_dev);
#endif /* CONFIG_FREESCALE_IMX6 */
  }
  else
  {
    /* config space handle */
    dev->pci_dev = pci_dev;
  }

  /* success */
  is_err = 0;

 on_error_6:
  if (devmem_fd != -1) close(devmem_fd);

 on_error_5:
 on_error_4:
 on_error_3:
 on_error_2:
  if (is_err)
  {
    put_libpci_access();
  on_error_1:
    free(dev);
  on_error_0:
    dev = EPCI_BAD_HANDLE;
  }

  return dev;
}

epcierror_t epci_close(epcihandle_t dev)
{
  if (dev->bar_size) munmap(dev->bar_vaddr, dev->bar_size);
  if (dev->pci_dev) put_libpci_access();
  free(dev);
  return EPCI_OK;
}


/* param routines */

static parentry_t* parse_parameter(const char* name)
{
  int i;

  /* Check if valid parameter name */
  for (i = 0; i < par_list_sz; ++i)
    if (!strcasecmp(par_list[i].name, name))
      return &par_list[i];

  lib_error(EPCI_ERR, "unknown parameter");

  return NULL;
}

epcierror_t epci_getparam(epcihandle_t dev, const char* name, epcipar_t* valptr)
{
  parentry_t* par;
  cpars_t* ptable;

  EPCI_DEBUGF_3("%s('%s')\n", __FUNCTION__, name);

  if (dev == EPCI_LIB_DEFAULTS) ptable = &lib_pars.def;
  else ptable = &dev->par;

  if (!(par = parse_parameter(name))) return EPCI_ERR;

  switch (par->code)
  {
  case pLIBVER: *valptr = (epcipar_t)RcsRevision; break ;
  case pDEBUG: *valptr = DDPAR(lib_pars.debug); break ;
  case pDRVDEBUG: *valptr = DDPAR(ptable->drvdebug); break ;
  default: return lib_error(EPCI_ERR, "parameter not implemented");
  }

  return EPCI_OK;
}

epcierror_t epci_setparam(epcihandle_t dev, const char* name, epcipar_t valptr)
{
  parentry_t* par;
  int intval;

  EPCI_DEBUGF_3("%s('%s')\n", __FUNCTION__, name);

  if (!(par = parse_parameter(name))) return EPCI_ERR;

  /* Check read only parameters */
  if (par->flags == _READ_ONLY) return lib_error(EPCI_ERR, "read only parameter");

  /* Set the parameter value */
  switch (par->code)
  {
  case pDEBUG:
    intval = DDPARtoINT(valptr);
    if ((intval < 0) || (intval > 6))
      return lib_error(EPCI_ERR, "invalid parameter value");
    lib_pars.debug = intval;
    break;

  default:
    return lib_error(EPCI_ERR, "parameter not implemented");
  }

  return EPCI_OK;
}


/* bar block accesses routines */

#define EPCI_WR_BLK_TEMPLATE(__w)  \
epcierror_t epci_wr ## __w ## _blk(epcihandle_t dev, int off, int step, size_t n, uint ## __w ## _t* val) { \
   step *= (__w / 8);  \
   for (; n--; off += step, val++)  \
      epci_wr ## __w ## _reg(dev, off, *val);  \
   return EPCI_OK;  \
}
EPCI_WR_BLK_TEMPLATE(8);
EPCI_WR_BLK_TEMPLATE(16);
EPCI_WR_BLK_TEMPLATE(32);
EPCI_WR_BLK_TEMPLATE(64);

epcierror_t epci_wr64R_blk(epcihandle_t dev, int off, int step, size_t n, uint64_t* val) {
   step *= sizeof(uint64_t);
   for (; n--; off += step, val++)
      epci_wr64R_reg(dev, off, *val);
   return EPCI_OK;
}

#define EPCI_RD_BLK_TEMPLATE(__w)        \
epcierror_t epci_rd ## __w ## _blk(epcihandle_t dev, int off, int step, size_t n, uint ## __w ## _t* val) {\
   step *= (__w / 8);  \
   for (; n--; off += step, val++) \
      epci_rd ## __w ## _reg(dev, off, val);      \
   return EPCI_OK;  \
}
EPCI_RD_BLK_TEMPLATE(8);
EPCI_RD_BLK_TEMPLATE(16);
EPCI_RD_BLK_TEMPLATE(32);
EPCI_RD_BLK_TEMPLATE(64);

epcierror_t epci_rd64R_blk(epcihandle_t dev, int off, int step, size_t n, uint64_t* val) {
   step *= sizeof(uint64_t);
   for (; n--; off += step, val++)
      epci_rd64R_reg(dev, off, val);
   return EPCI_OK;
}

/* configuration space io routines */

epcierror_t epci_rd_config(epcihandle_t dev, int off, int n, void* buf)
{
  /* assume dev->pci_dev != null */

  const int res = pci_read_block(dev->pci_dev, off, buf, n);
  if (res != 1) return lib_error(EPCI_ERR, "pci_read_block");
  return EPCI_OK;
}

epcierror_t epci_wr_config(epcihandle_t dev, int off, int n, void* buf)
{
  /* assume dev->pci_dev != null */

  const int res = pci_write_block(dev->pci_dev, off, buf, n);
  if (res != 1) return lib_error(EPCI_ERR, "pci_write_block");
  return EPCI_OK;
}
