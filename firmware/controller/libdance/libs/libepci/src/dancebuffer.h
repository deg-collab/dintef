/*-------------------------------------------------------------------*/
#ifndef _DANCEBUFFER_H
#define _DANCEBUFFER_H

#include <stdint.h>      // uint16_t, uint32_t, ...


//--------------------------------------
//  libdancebuffer specific

// Handle
typedef struct dancebuffer_s *dancebuffer_t;

// Special handle codes
#define DANCEBUFFER_BAD_HANDLE   ((dancebuffer_t) NULL)


// Utility macros
/* TODO:
#define DANCEBUFFERtoBYTE()
#define DANCEBUFFERtoBYTE()
#define DANCEBUFFERtoBYTE()
#define DANCEBUFFERtoBYTE()
*/





//--------------------------------------
//  libepci entries

dancebuffer_t dancebuffer_alloc(
                 int           data_width,
                 int           buf_depth,
                 int          *type_flags,
                 void         *mem_ptr);

dancebuffer_t dancebuffer_bytealloc(
                 int           buf_depth);

dancebuffer_t dancebuffer_realloc(
                 dancebuffer_t dbuf,
                 int           buf_depth);

void          dancebuffer_free(
                 dancebuffer_t dbuf);



#endif  /* _DANCEBUFFER_H */
