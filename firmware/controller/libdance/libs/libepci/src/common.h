/*-------------------------------------------------------------------*/
#ifndef _LIBCOMMON_H
#define _LIBCOMMON_H


//-------------------------------------------------------------------
// Parameter related definitions
//
typedef struct {
   int   code;
   char *name;
   int   flags;
} parentry_t;

#define _READ_WRITE 0x0001
#define _READ_ONLY  0x0002

#define DPAR_LIST \
  DPAR(pDEBUG,    "DEBUG",       _READ_WRITE), \
  DPAR(pDRVDEBUG, "DRVDEBUG",    _READ_WRITE), \
  DPAR(pLIBVER,   "LIBVER",      _READ_ONLY)

#define DPAR(code, name, flags) code
enum {DPAR_LIST};
#undef DPAR

extern parentry_t par_list[];
extern int        par_list_sz;



//-------------------------------------------------------------------
// Global library parameters
//

// Library parameters to be managed per connection
typedef struct cpars_s {
  int       drvdebug;
} cpars_t;

typedef struct lpars_s {
  int       debug;
  cpars_t   def;   // default 'per connection' parameters
} lpars_t;

extern lpars_t lib_pars;





//-------------------------------------------------------------------
// Debug related. Thanks for keeping EPCI_ prefix, not not pollute namespace.
//

//--------------------------------------
//
#define DEBUG_PREFIX "\tLIBEPCI: "
#define __EPCI_DPRINTF(n, ...) if (n<=lib_pars.debug) \
  printf(DEBUG_PREFIX __VA_ARGS__)
#define EPCI_DEBUGF_1(...)  __EPCI_DPRINTF(1, __VA_ARGS__)
#define EPCI_DEBUGF_2(...)  __EPCI_DPRINTF(2, __VA_ARGS__)
#define EPCI_DEBUGF_3(...)  __EPCI_DPRINTF(3, __VA_ARGS__)
#define EPCI_DEBUGF_4(...)  __EPCI_DPRINTF(4, __VA_ARGS__)
#define EPCI_DEBUGF_5(...)  __EPCI_DPRINTF(5, __VA_ARGS__)


#endif /* _LIBCOMMON_H */
