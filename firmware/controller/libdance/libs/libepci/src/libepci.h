#ifndef LIBEPCI_H_INCLUDED
#define LIBEPCI_H_INCLUDED


#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include "common.h"


/* epci bar handle */

/* forward declaration */
struct pci_dev;

struct epcihandle_s {
   /* unused for pci config space */
   void*     bar_vaddr;
   uintptr_t bar_paddr;
   size_t    bar_size;

   /* unused for pci bar */
   struct pci_dev* pci_dev;

   cpars_t   par;
};

typedef struct epcihandle_s *epcihandle_t;


/* Special handle codes */
#define EPCI_BAD_HANDLE   ((epcihandle_t)NULL)
#define EPCI_LIB_DEFAULTS ((epcihandle_t)(uintptr_t)-1)


/* Parameter related */
typedef const void *epcipar_t;

#define DDPAR(variable)      ((epcipar_t) &variable)
#define DDPARtoINT(pValPtr)  (*(int *)    pValPtr)
#define DDPARtoSTR(pValPtr)  ((char *)    pValPtr)
#define DDPARtoFILE(pValPtr) (*(FILE **)  pValPtr)


/* Binary data definitions */
typedef enum {
   BIN_8    = 1,
   BIN_16   = 2,
   BIN_32   = 4,
   BIN_64   = 8,
   BIN_64R  = 8 + 1
} epcidata_t;

/* error codes */
typedef enum {
   EPCI_OK = 0,
   EPCI_ERR,
   EPCI_INVALID_SIZE,
} epcierror_t;


/* exported routines */

epcihandle_t epci_open(const char* dev_id, const char* dev_loc, int bar);

static inline epcihandle_t epci_open_config(char* dev_id, char* dev_loc) {
   /* minus one is used to identify configuration space */
#define EPCI_CONFIG_BAR -1
   return epci_open(dev_id, dev_loc, EPCI_CONFIG_BAR);
}

epcierror_t epci_close(epcihandle_t dev);
epcierror_t epci_error(const char** errmsg);

epcierror_t epci_getparam(epcihandle_t dev, const char* name, epcipar_t* value);
epcierror_t epci_setparam(epcihandle_t dev, const char* name, epcipar_t value);

/* inlined single register access routines */

#define EPCI_RD_REG_TEMPLATE(__w)					 \
static inline epcierror_t epci_rd ## __w ## _reg			 \
(epcihandle_t dev, int off, uint ## __w ## _t* val)			 \
{									 \
   *val = *(volatile uint ## __w ## _t*)((uintptr_t)dev->bar_vaddr + off); \
   return EPCI_OK;							 \
}

EPCI_RD_REG_TEMPLATE(8);
EPCI_RD_REG_TEMPLATE(16);
EPCI_RD_REG_TEMPLATE(32);
/* TODO: MP 5thMay2020
    weird, the direct 64b access doesn't work any more (segmentation fault)
    nothing has changed since working release 2047
    changes in HDL/EBONE PCIe ??
    warning, not tested on 64b write or read/write block

EPCI_RD_REG_TEMPLATE(64);
*/

static inline epcierror_t epci_rd64_reg(epcihandle_t dev, int off, uint64_t* val) {
   *((uint32_t*)val) = *(volatile uint32_t*)((uintptr_t)dev->bar_vaddr + off);
   *((uint32_t*)val + 1) = *(volatile uint32_t*)((uintptr_t)dev->bar_vaddr + off + sizeof(uint32_t));
   return EPCI_OK;
}

static inline epcierror_t epci_rd64R_reg(epcihandle_t dev, int off, uint64_t* val) {
   *((uint32_t*)val + 1) = *(volatile uint32_t*)((uintptr_t)dev->bar_vaddr + off);
   *((uint32_t*)val) = *(volatile uint32_t*)((uintptr_t)dev->bar_vaddr + off + sizeof(uint32_t));
   return EPCI_OK;
}


#define EPCI_WR_REG_TEMPLATE(__w)					 \
static inline epcierror_t epci_wr ## __w ## _reg			 \
(epcihandle_t dev, int off, uint ## __w ## _t val)			 \
{									 \
   *(volatile uint ## __w ## _t*)((uintptr_t)dev->bar_vaddr + off) = val; \
   return EPCI_OK;							 \
}

EPCI_WR_REG_TEMPLATE(8);
EPCI_WR_REG_TEMPLATE(16);
EPCI_WR_REG_TEMPLATE(32);
EPCI_WR_REG_TEMPLATE(64);

#define TEST_64R
#ifdef TEST_64R
typedef union {
   uint64_t v64;
   uint32_t v32[2];
} u64_t;
#endif

static inline epcierror_t epci_wr64R_reg(epcihandle_t dev, int off, uint64_t val) {
#ifdef TEST_64R
   /*TODO: to be tested on real hardware */
   u64_t *wr = (u64_t *)&val;

   *(volatile uint32_t*)((uintptr_t)dev->bar_vaddr + off) = wr->v32[1];
   *(volatile uint32_t*)((uintptr_t)dev->bar_vaddr + off + sizeof(uint32_t)) = wr->v32[0]; 
#else
   /* getting "break strict-aliasing rules" warnings */
   uint32_t* ptr32 = (uint32_t*)&val;
   *(volatile uint32_t*)((uintptr_t)dev->bar_vaddr + off) = *(ptr32 + 1);
   *(volatile uint32_t*)((uintptr_t)dev->bar_vaddr + off + sizeof(uint32_t)) = *ptr32;
#endif
   return EPCI_OK;
}

/* full inlining if type known at compile time */

extern epcierror_t lib_error(epcierror_t err, const char *fmt, ...);

static inline epcierror_t epci_wr_reg(epcihandle_t dev, int off, epcidata_t type, void* val) {
   EPCI_DEBUGF_3("%s()\n", __FUNCTION__);

   switch (type) {
      case BIN_8:  return epci_wr8_reg(dev, off,  *(uint8_t*)val);
      case BIN_16: return epci_wr16_reg(dev, off, *(uint16_t*)val);
      case BIN_32: return epci_wr32_reg(dev, off, *(uint32_t*)val);
      case BIN_64: return epci_wr64_reg(dev, off, *(uint64_t*)val);
      case BIN_64R:return epci_wr64R_reg(dev, off, *(uint64_t*)val);
      default: break;
   }

   return lib_error(EPCI_ERR, "bad access width");
}

static inline epcierror_t epci_rd_reg(epcihandle_t dev, int off, epcidata_t type, void* val) {
   EPCI_DEBUGF_3("%s()\n", __FUNCTION__);

   switch (type) {
      case BIN_8:  return epci_rd8_reg(dev, off, (uint8_t*)val);
      case BIN_16: return epci_rd16_reg(dev, off, (uint16_t*)val);
      case BIN_32: return epci_rd32_reg(dev, off, (uint32_t*)val);
      case BIN_64: return epci_rd64_reg(dev, off, (uint64_t*)val);
      case BIN_64R:return epci_rd64R_reg(dev, off, (uint64_t*)val);
      default: break;
   }
   return lib_error(EPCI_ERR, "bad access width");
}

epcierror_t epci_wr8_blk(epcihandle_t dev, int off, int step, size_t n, uint8_t* val);
epcierror_t epci_wr16_blk(epcihandle_t dev, int off, int step, size_t n, uint16_t* val);
epcierror_t epci_wr32_blk(epcihandle_t dev, int off, int step, size_t n, uint32_t* val);
epcierror_t epci_wr64_blk(epcihandle_t dev, int off, int step, size_t n, uint64_t* val);
epcierror_t epci_wr64R_blk(epcihandle_t dev, int off, int step, size_t n, uint64_t* val);

static inline epcierror_t epci_wr_blk(epcihandle_t dev, int off, int step,
                                      size_t n, epcidata_t type, void* val) {
   EPCI_DEBUGF_3("%s()\n", __FUNCTION__);

   switch (type) {
      case BIN_8:  return epci_wr8_blk(dev, off, step, n, (uint8_t*)val);
      case BIN_16: return epci_wr16_blk(dev, off, step, n, (uint16_t*)val);
      case BIN_32: return epci_wr32_blk(dev, off, step, n, (uint32_t*)val);
      case BIN_64: return epci_wr64_blk(dev, off, step, n, (uint64_t*)val);
      case BIN_64R:return epci_wr64R_blk(dev, off, step, n, (uint64_t*)val);
      default: break;
   }

   return lib_error(EPCI_ERR, "unsupported data type");
}

epcierror_t epci_rd8_blk(epcihandle_t  dev, int off, int step, size_t n, uint8_t* val);
epcierror_t epci_rd16_blk(epcihandle_t dev, int off, int step, size_t n, uint16_t* val);
epcierror_t epci_rd32_blk(epcihandle_t dev, int off, int step, size_t n, uint32_t* val);
epcierror_t epci_rd64_blk(epcihandle_t dev, int off, int step, size_t n, uint64_t* val);
epcierror_t epci_rd64R_blk(epcihandle_t dev,int off, int step, size_t n, uint64_t* val);

static inline epcierror_t epci_rd_blk(epcihandle_t dev, int off, int step,
                                      size_t n, epcidata_t type, void* val) {
   EPCI_DEBUGF_3("%s()\n", __FUNCTION__);

   switch (type) {
      case BIN_8:  return epci_rd8_blk(dev, off, step, n, (uint8_t*)val);
      case BIN_16: return epci_rd16_blk(dev, off, step, n, (uint16_t*)val);
      case BIN_32: return epci_rd32_blk(dev, off, step, n, (uint32_t*)val);
      case BIN_64: return epci_rd64_blk(dev, off, step, n, (uint64_t*)val);
      case BIN_64R:return epci_rd64R_blk(dev,off, step, n, (uint64_t*)val);
      default: break;
   }
   return lib_error(EPCI_ERR, "unsupported data type");
}

/* configuration space io routines */

epcierror_t epci_rd_config(epcihandle_t dev, int off, int n, void* buf);
epcierror_t epci_wr_config(epcihandle_t dev, int off, int n, void* buf);


#endif  /* LIBEPCI_H_INCLUDED */
