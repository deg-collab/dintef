#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include "libepci.h"





//-------------------------------------------------------------------
//
//
typedef enum {
  UNKNOWN  = 0,
  DCORE    = 1,
  DUB      = 2,
} dance_t;



//-------------------------------------------------------------------
//
//
void title(char *txt) {
  char line[80];
  char bar[] = "-------------------------------------------------------";
  int  w;
 
  w=(60-strlen(txt))/2;
  if(w<5) w=5;
  bar[w]=0;

  snprintf(line,80,"\n%s%s%s\n",bar,txt,bar);
  printf("%s",line);
}



//-------------------------------------------------------------------
//
//
int main(int argc, char *argv[])
{
 epcihandle_t  dev;
 char          device_id[30] = "10ee:eb01"; // Xilinx FPGA
 int           base_addr     = 0;
 char         *errmsg;
 dance_t       board_type    = UNKNOWN;
 int           reg;
 int           ndata=10;
 int32_t       arr32[10];
 int32_t       val32;
 int           i;


 //--------------------------------------
 //
 if(argc > 1) { 
   if(!strcasecmp(argv[1],"--help") || !strcasecmp(argv[1],"-h")) {
     printf("Usage: %s VendorId:DeviceId\n\n", argv[0]);
     exit(0);
   }
   strncpy(device_id, argv[1], 30); 
 }

 title("Testing libepci calls");



 //--------------------------------------
 //
 printf("Opening connection to \"%s\"...\n",device_id);
 if((dev = epci_open(device_id, "", base_addr)) == EPCI_BAD_HANDLE) {
   printf("ERROR: unable to open connection, giving up\n");
   exit(-1);
 }
 printf("Got connection\n");


 //--------------------------------------
 //
 title("Testing library parameters");
 epcipar_t mypar;
 int       libdebug;
 char      bidon[]="Bidon";
 if(epci_getparam(EPCI_LIB_DEFAULTS,"libver",&mypar) == EPCI_ERR) {
    printf("ERROR: reading library parameter\n");
 } else {
    printf("RD lib version : %s\n",DDPARtoSTR(mypar));
 }

 if(epci_getparam(dev,"drvdebug",&mypar) == EPCI_ERR) {
    printf("ERROR: reading library parameter\n");
 } else {
    printf("RD driver debug: %d\n",DDPARtoINT(mypar));
 }

 if(epci_getparam(EPCI_LIB_DEFAULTS,"debug",&mypar) == EPCI_ERR) {
    printf("ERROR: reading library parameter\n");
 } else {
    printf("RD lib debug   : %d\n",DDPARtoINT(mypar));
 }
 libdebug=1;
 printf("WR lib debug   : %d\n",libdebug);
 if(epci_setparam(dev,"debug",DDPAR(libdebug)) == EPCI_ERR) {
    printf("ERROR: writing library parameter\n");
 }
 if(epci_getparam(EPCI_LIB_DEFAULTS,"debug",&mypar) == EPCI_ERR) {
    printf("ERROR: reading library parameter\n");
 } else {
    printf("RD lib debug   : %d\n",DDPARtoINT(mypar));
 }


 printf("WR lib version : %s\n",bidon);
 if(epci_setparam(EPCI_LIB_DEFAULTS,"libver",DDPAR(bidon)) == EPCI_ERR) {
    epci_error(&errmsg);
    printf("ERROR: writing library parameter: %s\n", errmsg);
 }

 //--------------------------------------
 //
 title("Testing register read");
 reg = 0x00;
 epci_rd_reg(dev, reg, BIN_32, &val32);
 printf("RD Reg 0x%04x: 0x%08x\n", reg, val32);

 reg = 0x1c;
 epci_rd_reg(dev, reg, BIN_32, &val32);
 printf("RD Reg 0x%04x: 0x%08x\n", reg, val32);
 epci_rd32_reg(dev, reg, &val32);
 printf("RD Reg 0x%04x: 0x%08x\n", reg, val32);
 switch(val32&0x0f) {
    case 1:
       board_type = DCORE;
       printf("Should be a DCORE board\n");
       break;
    case 2:
       board_type = DUB;
       printf("Should be a DUB board\n");
       break;
    default:
       printf("Unknown FPGA boardS\n");
       board_type = UNKNOWN;
 }

 //--------------------------------------
 //
 if(board_type == DCORE) {
   title("Testing register write");

   reg = 0x800;
   epci_rd32_reg(dev, reg, &val32);
   printf("RD Reg 0x%04x: 0x%08x\n", reg, val32);

   val32 = 0xdefecada;
   printf("WR Reg 0x%04x: 0x%08x\n", reg, val32);
   epci_wr_reg(dev, reg, BIN_32, &val32);

   val32 = 0x00;
   epci_rd32_reg(dev, reg, &val32);
   printf("RD Reg 0x%04x: 0x%08x\n", reg, val32);

   val32 = 0xaaaa5555;
   printf("WR Reg 0x%04x: 0x%08x\n", reg, val32);
   epci_wr32_reg(dev, reg, val32);

   val32 = 0x00;
   epci_rd32_reg(dev, reg, &val32);
   printf("RD Reg 0x%04x: 0x%08x\n", reg, val32);
 }

 //--------------------------------------
 //
 if(board_type == DCORE) {

   title("Testing block write");
   arr32[0]=0x00100001;
   for(i=1;i<ndata;i++) arr32[i] = (arr32[i-1] << 1);

   reg = 0x800;
   epci_wr_blk(dev, reg, ndata, BIN_32, arr32);
   printf("WR Blk 0x%04x:\n", reg);
   for(i=0;i<ndata;i++) printf("   0x%08x\n", arr32[i]);

   reg = 0x804;
   epci_rd32_reg(dev, reg, &val32);
   printf("RD Reg 0x%04x: 0x%08x\n", reg, val32);

   reg = 0x80c;
   epci_rd32_reg(dev, reg, &val32);
   printf("RD Reg 0x%04x: 0x%08x\n", reg, val32);
 }


 //--------------------------------------
 //
 if(board_type == DCORE) {

   title("Testing block read");
   for(i=0;i<ndata;i++) arr32[i] = 0x00;


   reg = 0x800;
   epci_rd_blk(dev, reg, ndata, BIN_32, arr32);
   printf("RD Blk 0x%04x:\n", reg);
   for(i=0;i<ndata;i++) printf("   0x%08x\n", arr32[i]);
 }


 //--------------------------------------
 //
 title("End of story");
 printf("\n\n");
 epci_close(dev);
 return(0);
}
