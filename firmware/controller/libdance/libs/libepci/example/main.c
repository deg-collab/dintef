#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include "libepci.h"

static double adc_to_temp(uint32_t x)
{
  return ((double)x * 503.975) / 1024.0 - 273.15;
}

int main(int ac, char** av)
{
  /* read the sysmon reigsters */
  /* refer to ebs_sysm.pdf, annexe A.1 */

  static char* const device_id = "10ee:eb01";
  static const int sysmon_bar = 0x01;
  static const size_t sysmon_off = 0x40;

  epcihandle_t h;

  uint32_t cur_adc;
  uint32_t min_adc;
  uint32_t max_adc;

  h = epci_open(device_id, NULL, sysmon_bar);
  if (h == EPCI_BAD_HANDLE) { printf("error\n"); return -1; }

  /* read on chip temperature sensors */

  epci_rd32_reg(h, sysmon_off + 0x00, &cur_adc);
  epci_rd32_reg(h, sysmon_off + 0x04, &max_adc);
  epci_rd32_reg(h, sysmon_off + 0x08, &min_adc);

  printf("%.2lf <= %.2lf <= %.2lf\n",
	 adc_to_temp(min_adc),
	 adc_to_temp(cur_adc),
	 adc_to_temp(max_adc));

  epci_close(h);

  return 0;
}
