/* ebone spi controller */
/* http://wikiserv.esrf.fr/del/images/0/01/Ebs_spi.pdf */


#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include "libepci.h"
#include "libespi.h"


#define CONFIG_DEBUG 1
#if (CONFIG_DEBUG == 1)
#include <stdio.h>
#define PRINTF(__s, ...) \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define PERROR() \
printf("[!] %s:%d\n", __FILE__, __LINE__)
#define ASSUME(__x) \
do { if (!(__x)) printf("[!] %s:%d\n", __FILE__, __LINE__); } while (0)
#else
#define PRINTF(__s, ...)
#define PERROR()
#define ASSUME(__x)
#endif /* CONFIG_DEBUG */


static int espi_read_ctrl(espi_handle_t* espi, uint32_t* x)
{
  static size_t off = 0x10;
  epci_rd32_reg(espi->pci_handle, espi->pci_off + off, x);
  return 0;
}

static int espi_write_ctrl(espi_handle_t* espi, uint32_t x)
{
  static size_t off = 0x10;
  epci_wr32_reg(espi->pci_handle, espi->pci_off + off, x);
  return 0;
}

static int espi_wait_ready(espi_handle_t* espi)
{
  static const uint32_t busy_mask = 1 << 8;

  uint32_t x;

  while (1)
  {
    espi_read_ctrl(espi, &x);
    if ((x & busy_mask) == 0) break ;
    usleep(100);
  }

  return 0;
}

static int espi_write_clkdiv(espi_handle_t* espi, uint32_t x)
{
  static size_t off = 0x14;
  epci_wr32_reg(espi->pci_handle, espi->pci_off + off, x);
  return 0;
}

static int espi_write_ss(espi_handle_t* espi, uint32_t x)
{
  static size_t off = 0x18;
  epci_wr32_reg(espi->pci_handle, espi->pci_off + off, x);
  return 0;
}

static int espi_read_rx0(espi_handle_t* espi, uint32_t* x)
{
  static size_t off = 0x0;
  epci_rd32_reg(espi->pci_handle, espi->pci_off + off, x);
  return 0;
}

static int espi_write_tx0(espi_handle_t* espi, uint32_t x)
{
  static size_t off = 0x0;
  epci_wr32_reg(espi->pci_handle, espi->pci_off + off, x);
  return 0;
}

static int espi_read_rx(espi_handle_t* espi, uint8_t* x, size_t n)
{
  static const uint32_t go_mask = 1 << 8;

  uint32_t xx;
  size_t i;

  for (i = 0; i != n; ++i, ++x)
  {
    espi_write_ctrl(espi, espi->ctrl_mask | go_mask | (uint32_t)8);
    espi_wait_ready(espi);
    espi_read_rx0(espi, &xx);
    *x = (uint8_t)xx;
  }

  return 0;
}

static int espi_write_tx(espi_handle_t* espi, const uint8_t* x, size_t n)
{
  static const uint32_t go_mask = 1 << 8;

  size_t i;

  for (i = 0; i != n; ++i, ++x)
  {
    espi_write_tx0(espi, (uint32_t)*x);
    espi_write_ctrl(espi, espi->ctrl_mask | go_mask | (uint32_t)8);
    espi_wait_ready(espi);
  }

  return 0;
}


/* exported */

int espi_init_lib(void)
{
  return 0;
}

int espi_fini_lib(void)
{
  return 0;
}

int espi_open(espi_handle_t* espi, const espi_desc_t* desc)
{
  epcihandle_t pci_handle;
  uint32_t fdiv;

  espi->flags = 0;

  if (desc->pci_handle != EPCI_BAD_HANDLE)
  {
    pci_handle = desc->pci_handle;
  }
  else
  {
    pci_handle = epci_open(desc->pci_id, NULL, desc->pci_bar);
    if (pci_handle == EPCI_BAD_HANDLE) goto on_error_0;
    espi->flags |= ESPI_FLAG_CLOSE_PCI;
  }

  espi->pci_handle = pci_handle;
  espi->pci_off = desc->pci_off;

  /* spi_fclk = ebone_fclk / ((div + 1) * 2) */
  /* fdiv = (ebone_fclk / (spi_fclk * 2)) - 1 */
  fdiv = desc->ebone_fclk / (desc->spi_fclk * 2) - 1;
  if ((fdiv == 0) || (fdiv > 0xffff)) goto on_error_1;

  if (espi_write_clkdiv(espi, (uint32_t)fdiv)) goto on_error_1;

  espi->ctrl_mask = 0;
  if (desc->spi_flags & ESPI_FLAG_RX_NEG) espi->ctrl_mask |= 1 << 9;
  if (desc->spi_flags & ESPI_FLAG_TX_NEG) espi->ctrl_mask |= 1 << 10;
  if (espi_write_ctrl(espi, espi->ctrl_mask)) goto on_error_1;

  if (espi_write_ss(espi, 0)) goto on_error_1;

  return 0;

 on_error_1:
  if (espi->flags & ESPI_FLAG_CLOSE_PCI) epci_close(espi->pci_handle);
 on_error_0:
  return -1;
}

int espi_close(espi_handle_t* espi)
{
  if (espi->flags & ESPI_FLAG_CLOSE_PCI) epci_close(espi->pci_handle);
  return 0;
}

int espi_cmd
(
 espi_handle_t* espi,
 const uint8_t* cbuf, size_t csize,
 const uint8_t* ibuf, size_t isize,
 uint8_t* obuf, size_t osize
)
{
  /* cbuf the command buffer */
  /* ibuf the input buffer, ie. command arguments if any. */
  /* obuf the output buffer */
  /* sizes in bytes */
  /* isize and osize can be 0 */

  ASSUME(csize != 0);

  espi_write_ss(espi, 1);
  espi_wait_ready(espi);
  espi_write_tx(espi, cbuf, csize);
  espi_write_tx(espi, ibuf, isize);
  espi_read_rx(espi, obuf, osize);
  espi_write_ss(espi, 0);

  return 0;
}
