#ifndef LIBESPI_H_INCLUDED
#define LIBESPI_H_INCLUDED


#include <stdint.h>
#include <sys/types.h>
#include "libepci.h"


typedef struct espi_handle
{
  epcihandle_t pci_handle;
  size_t pci_off;

#define ESPI_FLAG_CLOSE_PCI (1 << 0)
  uint32_t flags;

  uint32_t ctrl_mask;

} espi_handle_t;


typedef struct espi_desc
{
  const char* pci_id;
  size_t pci_bar;
  size_t pci_off;
  epcihandle_t pci_handle;

  size_t ebone_fclk;

  size_t spi_fclk;

#define ESPI_FLAG_TX_NEG (1 << 0)
#define ESPI_FLAG_RX_NEG (1 << 1)
  uint32_t spi_flags;

} espi_desc_t;


/* exported routines */

int espi_init_lib(void);
int espi_fini_lib(void);
int espi_open(espi_handle_t*, const espi_desc_t*);
int espi_close(espi_handle_t*);

int espi_cmd
(
 espi_handle_t*,
 const uint8_t*, size_t,
 const uint8_t*, size_t,
 uint8_t*, size_t
);


/* static inlined routines */

static inline int espi_init_desc(espi_desc_t* desc)
{
  static const espi_desc_t default_desc =
  {
    .pci_id = "10ee:eb01",
    .pci_bar = 0x01,
    .pci_off = 0x00,
    .pci_handle = EPCI_BAD_HANDLE,
    .ebone_fclk = 125000000,
    .spi_fclk =  125000,
    .spi_flags = 0
  };

  *desc = default_desc;

  return 0;
}

static inline int espi_open_default(espi_handle_t* espi)
{
  /* open espi handle using default description */

  espi_desc_t desc;
  espi_init_desc(&desc);
  return espi_open(espi, &desc);
}


/* spi slave specific */

extern const size_t fm25v01_size;

int fm25v01_read_data(espi_handle_t*, uint8_t*, size_t, size_t);
int fm25v01_write_data(espi_handle_t*, const uint8_t*, size_t, size_t);
int fm25v01_check_id(espi_handle_t*);
int fm25v01_init_espi_desc(espi_desc_t*);



#endif /* ! LIBESPI_H_INCLUDED */
