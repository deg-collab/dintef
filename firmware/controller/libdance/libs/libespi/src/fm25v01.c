/* fm25v01 serial fram */
/* http://www.cypress.com/file/136416/download */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include "libespi.h"


/* debug */

#define CONFIG_DEBUG 1
#if (CONFIG_DEBUG == 1)
#include <stdio.h>
#define PRINTF(__s, ...) \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define PERROR() \
printf("[!] %s:%d\n", __FILE__, __LINE__)
#define ASSUME(__x) \
do { if (!(__x)) printf("[!] %s:%d\n", __FILE__, __LINE__); } while (0)
#else
#define PRINTF(__s, ...)
#define PERROR()
#define ASSUME(__x)
#endif /* CONFIG_DEBUG */


/* FM25V01 routines */

#define FM25V01_OP_WRSR 0x01
#define FM25V01_OP_WRITE 0x02
#define FM25V01_OP_READ 0x03
#define FM25V01_OP_WRDI 0x04
#define FM25V01_OP_RDSR 0x05
#define FM25V01_OP_WREN 0x06
#define FM25V01_OP_FSTRD 0x0b
#define FM25V01_OP_RDID 0x9f

static const uint8_t fm25v01_id[] =
  { 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0xc2, 0x21, 0x00 };

const size_t fm25v01_size = 16 * 1024;

static int fm25v01_read_id(espi_handle_t* espi, uint8_t* id)
{
  const uint8_t cmd[] = { FM25V01_OP_RDID };
  return espi_cmd(espi, cmd, sizeof(cmd), NULL, 0, id, 9);
}

__attribute__((unused))
static int fm25v01_read_sr(espi_handle_t* espi, uint8_t* sr)
{
  const uint8_t cmd[] = { FM25V01_OP_RDSR };
  return espi_cmd(espi, cmd, sizeof(cmd), NULL, 0, sr, 1);
}

int fm25v01_read_data
(espi_handle_t* espi, uint8_t* data, size_t off, size_t size)
{
  uint8_t cmd[3];

  cmd[0] = FM25V01_OP_READ;
  cmd[1] = (uint8_t)((off >> 8) & 0xff);
  cmd[2] = (uint8_t)((off >> 0) & 0xff);
  return espi_cmd(espi, cmd, sizeof(cmd), NULL, 0, data, size);
}

int fm25v01_write_data
(espi_handle_t* espi, const uint8_t* data, size_t off, size_t size)
{
  uint8_t cmd[3];

  cmd[0] = FM25V01_OP_WREN;
  if (espi_cmd(espi, cmd, 1, NULL, 0, NULL, 0)) return -1;

  cmd[0] = FM25V01_OP_WRITE;
  cmd[1] = (uint8_t)((off >> 8) & 0xff);
  cmd[2] = (uint8_t)((off >> 0) & 0xff);
  if (espi_cmd(espi, cmd, 3, data, size, NULL, 0)) return -1;

  cmd[0] = FM25V01_OP_WRDI;
  if (espi_cmd(espi, cmd, 1, NULL, 0, NULL, 0)) return -1;

  return 0;
}

int fm25v01_check_id(espi_handle_t* espi)
{
  uint8_t id[sizeof(fm25v01_id)];
  if (fm25v01_read_id(espi, id)) return -1;
  if (memcmp(id, fm25v01_id, sizeof(fm25v01_id))) return -1;
  return 0;
}

int fm25v01_init_espi_desc(espi_desc_t* desc)
{
  desc->spi_fclk = 125000;
  desc->spi_flags = ESPI_FLAG_TX_NEG | ESPI_FLAG_RX_NEG;
  return 0;
}
