#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>
#include "libespi.h"


/* debug */

#define CONFIG_DEBUG 1
#if (CONFIG_DEBUG == 1)
#include <stdio.h>
#define PRINTF(__s, ...) \
do { printf(__s, ## __VA_ARGS__); } while (0)
#define PERROR() \
printf("[!] %s:%d\n", __FILE__, __LINE__)
#define ASSUME(__x) \
do { if (!(__x)) printf("[!] %s:%d\n", __FILE__, __LINE__); } while (0)
#else
#define PRINTF(__s, ...)
#define PERROR()
#define ASSUME(__x)
#endif /* CONFIG_DEBUG */


/* command line */

typedef struct cmdline_info
{
#define CMDLINE_FLAG_IS_READ (1 << 0)
#define CMDLINE_FLAG_IS_OFFSET (1 << 1)
#define CMDLINE_FLAG_IS_SIZE (1 << 2)
#define CMDLINE_FLAG_IS_PATTERN (1 << 3)
#define CMDLINE_FLAG_IS_EBONE_SEG (1 << 4)
#define CMDLINE_FLAG_IS_EBONE_OFF (1 << 5)
#define CMDLINE_FLAG_IS_EBONE_CLK (1 << 6)
#define CMDLINE_FLAG_IS_SPI_CLK (1 << 7)
#define CMDLINE_FLAG_IS_TIME (1 << 8)
  unsigned int flags;
  size_t off;
  size_t size;
  uint8_t* pattern_buf;
  size_t pattern_size;
  size_t ebone_seg;
  size_t ebone_off;
  size_t ebone_clk;
  size_t spi_clk;
} cmdline_info_t;

static void do_usage(const char* s)
{
  printf("command line options:\n");
  printf("-a the action:\n");
  printf(" read\n");
  printf(" write\n");
  printf("-o <offset>, the read or write offset in bytes (default to 0)\n");
  printf("-s <size>, the read or write size, in bytes (optional)\n");
  printf("-p <pattern>, the pattern (optional)\n");
  printf("-ebone_seg <segment>, the ebone segment (default to 1)\n");
  printf("-ebone_off <offset>, the ebone offset (default to 0xc00)\n");
  printf("-ebone_clk <freq>, ebone clock frequency (default to 62500000)\n");
  printf("-spi_clk <freq>, spi clock frequency (default to 125000)\n");
  printf("-time <yes,no>, print operation completion time (default to no)\n");
}

static void print_buf(const uint8_t* buf, size_t size, size_t off)
{
  size_t i;

  for (i = 0; i != size; ++i)
  {
    if ((i % 16) == 0)
    {
      if (i) printf("\n");
      printf("[0x%08x] ", (uint32_t)(off + i));
    }

    printf("%02x", buf[i]);
  }

  printf("\n");
}

static int do_read(espi_handle_t* espi, const cmdline_info_t* info)
{
  size_t off;
  size_t size;
  uint8_t* buf;
  int err = -1;

  off = 0;
  if (info->flags & CMDLINE_FLAG_IS_OFFSET) off = info->off;

  size = fm25v01_size;
  if (info->flags & CMDLINE_FLAG_IS_SIZE) size = info->size;

  buf = malloc(size);
  if (buf == NULL)
  {
    printf("allocation error\n");
    goto on_error_0;
  }

  if (fm25v01_read_data(espi, buf, off, size))
  {
    printf("fm25v01_read_data() error\n");
    goto on_error_1;
  }

  print_buf(buf, size, off);

  /* success */
  err = 0;

 on_error_1:
  free(buf);
 on_error_0:
  return err;
}

static int do_write(espi_handle_t* espi, const cmdline_info_t* info)
{
  size_t i;
  size_t off;
  size_t size;
  uint8_t pattern_byte;
  const uint8_t* pattern_buf;
  size_t pattern_size;
  size_t pattern_count;
  int err = -1;

  off = 0;
  if (info->flags & CMDLINE_FLAG_IS_OFFSET) off = info->off;

  size = fm25v01_size;
  if (info->flags & CMDLINE_FLAG_IS_SIZE) size = info->size;

  pattern_size = 1;
  pattern_buf = &pattern_byte;
  pattern_byte = 0x00;
  if (info->flags & CMDLINE_FLAG_IS_PATTERN)
  {
    pattern_buf = info->pattern_buf;
    pattern_size = info->pattern_size;
  }

  pattern_count = size / pattern_size;

  for (i = 0; i != pattern_count; ++i, ++pattern_byte, off += pattern_size)
  {
    if (fm25v01_write_data(espi, pattern_buf, off, pattern_size))
    {
      printf("fm25v01_write_data() error\n");
      goto on_error_0;
    }
  }

  if ((i * pattern_size) != size)
  {
    pattern_size = size - i * pattern_size;
    if (fm25v01_write_data(espi, pattern_buf, off, pattern_size))
    {
      printf("fm25v01_write_data() error\n");
      goto on_error_0;
    }
  }

  /* success */
  err = 0;

 on_error_0:
  return err;
}


/* main */

static int get_size_t(const char* s, size_t* x)
{
  int base = 10;
  if ((strlen(s) > 2) && (s[0] == '0') && (s[1] == 'x')) base = 16;
  *x = strtoul(s, NULL, base);
  return 0;
}

static int get_pattern(const char* s, uint8_t** buf, size_t* size)
{
  /* pattern must be and hexadecimal string */

  char tmp[3];
  size_t len;
  size_t i;

  len = strlen(s);
  if (len % 2) goto on_error_0;

  *size = len / 2;
  if (*size == 0) goto on_error_0;

  *buf = malloc(*size);
  if (*buf == NULL) goto on_error_0;

  tmp[2] = 0;
  for (i = 0; i != *size; ++i)
  {
    tmp[0] = s[i * 2 + 0];
    tmp[1] = s[i * 2 + 1];
    (*buf)[i] = (uint8_t)strtoul(tmp, NULL, 16);
  }

  return 0;

 on_error_0:
  return -1;
}

int main(int ac, const char** av)
{
  espi_handle_t espi;
  espi_desc_t desc;
  cmdline_info_t info;
  unsigned int is_action;
  struct timeval tm_start;
  struct timeval tm_stop;
  struct timeval tm_diff;
  size_t i;
  int err = -1;

  /* get command line info */

  info.flags = 0;
  is_action = 0;

  if ((ac & 1) == 0)
  {
    printf("invalid command line count\n");
    goto on_invalid_cmdline;
  }

  for (i = 1; i < ac; i += 2)
  {
    const char* const key = av[i + 0];
    const char* const val = av[i + 1];

    if (strcmp(key, "-a") == 0)
    {
      is_action = 1;
      if (strcmp(val, "read") == 0)
      {
	info.flags |= CMDLINE_FLAG_IS_READ;
      }
      else if (strcmp(val, "write"))
      {
	printf("invalid action: %s\n", val);
	goto on_invalid_cmdline;
      }
    }
    else if (strcmp(key, "-o") == 0)
    {
      if (get_size_t(val, &info.off))
      {
	printf("invalid offset: %s\n", val);
	goto on_invalid_cmdline;
      }

      info.flags |= CMDLINE_FLAG_IS_OFFSET;
    }
    else if (strcmp(av[i], "-s") == 0)
    {
      if (get_size_t(val, &info.size))
      {
	printf("invalid size: %s\n", val);
	goto on_invalid_cmdline;
      }

      info.flags |= CMDLINE_FLAG_IS_SIZE;
    }
    else if (strcmp(av[i], "-p") == 0)
    {
      if (info.flags & CMDLINE_FLAG_IS_PATTERN)
      {
	printf("cannot have several patterns\n");
	goto on_invalid_cmdline;
      }

      if (get_pattern(val, &info.pattern_buf, &info.pattern_size))
      {
	printf("error getting pattern\n");
	goto on_invalid_cmdline;
      }

      info.flags |= CMDLINE_FLAG_IS_PATTERN;
    }
    else if (strcmp(key, "-ebone_seg") == 0)
    {
      if (get_size_t(val, &info.ebone_seg))
      {
	printf("invalid segment: %s\n", val);
	goto on_invalid_cmdline;
      }

      info.flags |= CMDLINE_FLAG_IS_EBONE_SEG;
    }
    else if (strcmp(key, "-ebone_off") == 0)
    {
      if (get_size_t(val, &info.ebone_off))
      {
	printf("invalid offset: %s\n", val);
	goto on_invalid_cmdline;
      }

      info.flags |= CMDLINE_FLAG_IS_EBONE_OFF;
    }
    else if (strcmp(key, "-ebone_clk") == 0)
    {
      if (get_size_t(val, &info.ebone_clk))
      {
	printf("invalid frequency: %s\n", val);
	goto on_invalid_cmdline;
      }

      info.flags |= CMDLINE_FLAG_IS_EBONE_CLK;
    }
    else if (strcmp(key, "-spi_clk") == 0)
    {
      if (get_size_t(val, &info.spi_clk))
      {
	printf("invalid frequency: %s\n", val);
	goto on_invalid_cmdline;
      }

      info.flags |= CMDLINE_FLAG_IS_SPI_CLK;
    }
    else if (strcmp(key, "-time") == 0)
    {
      if (strcmp(val, "yes") == 0)
      {
	info.flags |= CMDLINE_FLAG_IS_TIME;
      }
      else if (strcmp(val, "no"))
      {
	printf("invalid yes or no\n");
	goto on_invalid_cmdline;
      }
    }
    else
    {
      printf("invalid option: %s\n", key);
      goto on_invalid_cmdline;
    }
  }

  if (is_action == 0)
  {
  on_invalid_cmdline:
    do_usage(av[0]);
    goto on_error_0;
  }

  if (info.flags & CMDLINE_FLAG_IS_TIME)
  {
    gettimeofday(&tm_start, NULL);
  }

  /* open espi */

  if (espi_init_lib())
  {
    printf("espi_init_lib() error\n");
    goto on_error_0;
  }

  espi_init_desc(&desc);

  fm25v01_init_espi_desc(&desc);

  desc.pci_bar = 1;
  desc.pci_off = 0x3000;
  desc.ebone_fclk = 62500000;

  if (info.flags & CMDLINE_FLAG_IS_EBONE_SEG)
  {
    desc.pci_bar = info.ebone_seg;
  }

  if (info.flags & CMDLINE_FLAG_IS_EBONE_OFF)
  {
    desc.pci_off = info.ebone_off * sizeof(uint32_t);
  }

  if (info.flags & CMDLINE_FLAG_IS_EBONE_CLK)
  {
    desc.ebone_fclk = info.ebone_clk;
  }

  if (info.flags & CMDLINE_FLAG_IS_SPI_CLK)
  {
    desc.spi_fclk = info.spi_clk;
  }

  if (espi_open(&espi, &desc))
  {
    printf("espi_open_default() error\n");
    goto on_error_1;
  }

  if (fm25v01_check_id(&espi))
  {
    printf("fm25v01_check_id error\n");
    goto on_error_2;
  }

  if (info.flags & CMDLINE_FLAG_IS_READ)
  {
    if (do_read(&espi, &info)) goto on_error_2;
  }
  else
  {
    if (do_write(&espi, &info)) goto on_error_2;
  }
  
  /* success */
  err = 0;

 on_error_2:
  espi_close(&espi);
 on_error_1:
  espi_fini_lib();
 on_error_0:

  if (err == 0)
  {
    if (info.flags & CMDLINE_FLAG_IS_TIME)
    {
      gettimeofday(&tm_stop, NULL);
      timersub(&tm_stop, &tm_start, &tm_diff);
      printf("time (in seconds): %lu\n", tm_diff.tv_sec);
    }
  }

  if (info.flags & CMDLINE_FLAG_IS_PATTERN)
  {
    free(info.pattern_buf);
  }

  return err;
}
