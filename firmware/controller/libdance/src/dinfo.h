#ifndef DINFO_H_INCLUDED
#define DINFO_H_INCLUDED


#define DINFO_UPTIME     0
#define DINFO_UNAME      1
#define DINFO_HWIDS      2
#define DINFO_APPNAME    3
#define DINFO_VERSION    4
#define DINFO_ADDR       5
#define DINFO_NET        6
#define DINFO_CONTROLLER 7
#define DINFO_LIBDANCE   8
#define DINFO_FPGA       9
#define DINFO_JTAG       10
#define DINFO_LIST { \
   "UPTIME",  \
   "UNAME",   \
   "HWIDS",   \
   "APPNAME", \
   "VERSION", \
   "ADDR",    \
   "NET",     \
   "CONTROLLER", \
   "LIBDANCE", \
   "FPGA", \
   "JTAG"}

#define fmtqDINFO "oC#DINFO"

#define DINFO_MENU_LIST \
   MENU_IT(qDINFO, fmtqDINFO, fmtSTR, _I | _B, "Get instrument information") \
   MENU_IT(qAPPNAME, fmtNONE, fmtSTR, _I | _B, "Return application name") \
   MENU_IT(qVERSION, fmtNONE, fmtSTR, _I | _B, "Return application version") \
   MENU_IT(qADDR, fmtNONE, fmtSTR,    _I | _E, "")

#define DINFO_LISTS \
   DANCELIST(DINFO)


#endif /* DINFO_H_INCLUDED */
