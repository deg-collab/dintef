/*---------------------------------------------------------------------------
 * Handle the communication to low level hardware,
 * currently PCIe device
 *
 */
#include <unistd.h>
#include <string.h>
#include <ctype.h>       // isalpha()
#include <inttypes.h>

#include "libepci.h"

#include "libdance.h"

#include "errors.h"
#include "comm.h"
#include "extbuscomm.h"
#include "log.h"


#define _INIT_ON_START  (1 << 0)
#define _INIT_ON_APP    (1 << 1)

#define FAKEEXTBUS                    EXTBUS_DEF(FAKEBUS, _FAKE_BUS,  0, 0, 0, 0, 0)
#define PCIEXTBUS(name, id)           EXTBUS_DEF(name, _PCIE_BUS, id, 0, 0, 0, _INIT_ON_START)
#define PCIEXTBUS_DELAYED(name, id)   EXTBUS_DEF(name, _PCIE_BUS, id, 0, 0, 0, _INIT_ON_APP)
#define I2CEXTBUS(name, id)           EXTBUS_DEF(name, _EI2C_BUS, id, name, 0, -1, _INIT_ON_START)
#define I2CSUBBUS(name,ibus,iad,ityp) EXTBUS_DEF(name, _EI2C_BUS, "", ibus, iad, ityp, _INIT_ON_START)

#define EXTBUS_DEF(name, btype, id, ibus, iaddr, itype, iflag) { \
   .bus_id = name,   \
   .bname = #name,   \
   .bustype = btype, \
   .ident = id,      \
   .flags = iflag,     \
   .info = {         \
      .i2chandle = {        \
         .master = NULL,    \
         .master_id = ibus, \
         .busaddr = iaddr,  \
         .i2ctype = itype,  \
      } \
   } \
},


dancebus_t dancebus_tbl[] = {FAKEEXTBUS EXTBUS_LIST};
#undef  EXTBUS_DEF

#define EXTBUS_DEF(name, btype, id, ibus, iaddr, itype, iflag) btype |

dancebusdescr_t _dance_busdescr = {
   .bustbl = dancebus_tbl,
   .n_bus = sizeof(dancebus_tbl) / sizeof(dancebus_t),
   .bustypes = (EXTBUS_LIST 0),
};
#undef  EXTBUS_DEF

dancebusdescr_t *gdance_busdescr = &_dance_busdescr;

#define EXTDEVICE_DEF(name, bus_id, baddr, offset, type, flags) { \
   .devid = name, \
   .devname = #name, \
   .buspt = &dancebus_tbl[bus_id - FAKEBUS], \
   .baseaddr = baddr, \
   .address = offset, \
   .devtype = EXTDEV_ ## type, \
   .devflags = flags, \
},

danceextdev_t danceextdev_tbl[] = {FAKEEXTDEVICE EXTDEVICE_LIST};
danceextdevdesc_t danceextdev_desc = {
   .devtbl = danceextdev_tbl,
   .n_entries = sizeof(danceextdev_tbl) / sizeof(danceextdev_t),
};
#undef  EXTDEVICE_DEF

danceextdevdesc_t* gdance_extdevdesc = &danceextdev_desc;


// Declaration of the base address list
#define ENTRY_DEF(ident, baddress, offset, ndsize, dattype, acmod, flgs) { \
   .id = ident,         \
   .name = #ident,      \
   .baddr = baddress,   \
   .byteoffset = offset, \
   .ndata = ndsize,    \
   .dtype = dattype,   \
   .bytesize = (uint64_t)(ndsize) * (dattype), \
   .accmode = acmod, \
   .flags = flgs,   \
   .info = NULL},

// Declaration of the register list
dancereg_t dancebusreg_tbl[] = {BASEFAKE
                                BASEADDR_LIST
                                RASHPABADDR_LIST
                                DAQBADDR_LIST
                                APPREG_LIST
                                RASHPAREG_LIST
                                DAQREG_LIST
                                APPMEM_LIST};
#undef  ENTRY_DEF

danceregdescr_t _reg_descr = {
   .regtbl = dancebusreg_tbl,
   .n_entries  = sizeof(dancebusreg_tbl) / sizeof(dancereg_t)
};

danceregdescr_t* gdance_regdescr = &_reg_descr;

dancereg_t* dance_new_reg(unsigned int baddr, ssize_t offset, int dtype, size_t ndata) {
   static __thread dancereg_t reg;

   reg = *DANCE_BADDR_PT(baddr);

   reg.name = "anonymous";
   reg.offset += offset;
   if (dtype == BIN_64R) {
      reg.dtype = _D64;
      reg.flags = _D64R_REG;
   } else {
      reg.dtype = dtype? dtype : _D32;
      reg.flags = 0;
   }
   reg.ndata = ndata > 1? ndata : 1;
   reg.accmode = _ANY;

   return &reg;
}



#define EXTBUS_BUFF_LEN 256


//---------------------------------------------------------------------------
// Utilities
//

unsigned int combine_rw_access(unsigned int access1, unsigned int access2) {
   unsigned int access = access1 | access2;

   if (access & (_WO | _RO)) {
      access &= (_WO | _RO);
      if (access == (_WO | _RO))
         access = _NOACCESS;
   } else if (access & _RW)
      access = _RW;
   else
      access = _UNK;

   return(access);
}


static int update_reg_hdwinfo(dancebus_t* bus, reginfo_t* hdwinfo, int selector) {

   hdwinfo->bus = bus;
   hdwinfo->selector = selector;
   hdwinfo->bustype = bus->bustype;
//LOG_EXTBUS("update_reg_hdwinfo(): %s, %p, %d\n", bus->bname, hdwinfo, selector);
   switch (bus->bustype) {
      case _PCIE_BUS:
         if (selector < 0 || selector > PCI_NBARS) {
            LOG_ERROR("Invalid PCI BAR for bus %s\n", bus->bname);
            return -1;
         }
         hdwinfo->bushandle.pci = bus->info.pcihandles[selector];
         if (hdwinfo->bushandle.pci == EPCI_BAD_HANDLE && !IN_SIMULATION_MODE()) {
            LOG_ERROR("Unusable PCI BAR %d for bus %s\n", selector, bus->bname);
            return -1;
         }
         break;

      case _EI2C_BUS:
         if (selector < 0 || selector > 0x7f) {
            LOG_ERROR("Invalid I2C address for bus interface %s\n", bus->bname);
            return -1;
         }
         hdwinfo->bushandle.i2c = &bus->info.i2chandle;
         if (bus->info.i2chandle.master->ifd < 0 && !IN_SIMULATION_MODE()) {
            LOG_ERROR("Unusable I2C bus %s\n", bus->bname);
            return -1;
         }
//         TODO: // check selector/addr validity
         break;

      default:  // _FAKE_BUS
         break;
   }
   //   LOG_ERROR("blah, blah\n");
   return 0;
}

static int update_reg_address(dancereg_t* entry);

static int eval_regaddress(dancereg_t* entry, dancereg_t** baddrpt, size_t *offset, unsigned int *access, unsigned int count) {

//LOG_EXTBUS("eval_regaddress(): '%s', %d - %d\n", entry->name, entry->id, entry->baddr);
   if (++count > gdance_regdescr->n_entries) {  // to detect and abort (never-ending) cyclic references
      LOG_ERROR("loop in address references for register %s\n", entry->name);
      return(0);
   }
   if (IS_BASEADDR(entry->id) && IS_VALID_BUS_ID(entry->baddr)) {
     // this is a direct bus baseaddress

//LOG_EXTBUS("BASEADDRESS: '%s'\n", entry->name);
      if (entry->info == NULL) {   // if not done, initialise bus access.
         dancebus_t* bus = DANCE_BUS_PT(entry->baddr);  // bus id is coded in baddr
         int selector = entry->flags;                   // bus selector is coded in flags field

         entry->info = malloc(sizeof(reginfo_t));
         if (entry->info == NULL) {
            LOG_ERROR("memory error\n");
            return(0);
         }

         if (update_reg_hdwinfo(bus, entry->info, selector) < 0) {
            free(entry->info);
            entry->info = NULL;
            LOG_ERROR("problem resolving register address for %s\n", entry->name);
            return(0);
         }
      }
      *baddrpt = entry;
      *offset = entry->byteoffset;
      *access = entry->access;

   } else if (entry->baddr < gdance_regdescr->n_entries) {
//LOG_EXTBUS("REGISTER or indirect BASEADDR: '%s'\n", entry->name);
      if (eval_regaddress(DANCE_REG_PT(entry->baddr), baddrpt, offset, access, count)) {
         if (IS_BASEADDR(entry->id) && entry->flags == _IND_OFFSET) {  // indirect offset
            unsigned int regid = entry->byteoffset;  // register id stored in byteoffset
            if (!IS_REGISTER(regid) || !update_reg_address(DANCE_REG_PT(regid)))
               goto on_invalid_entry;
            entry->byteoffset = REG_VALUE(regid);
            entry->flags = _DIR_OFFSET;
         }
         *offset += entry->byteoffset;
         *access = combine_rw_access(*access, entry->access);
      } else
         goto on_invalid_entry;
   } else
      goto on_invalid_entry;

   return(1);

 on_invalid_entry:
   LOG_ERROR("invalid register entry %s\n", entry->name);
   return(0);
}

static int update_reg_address(dancereg_t* entry) {
   dancereg_t*  baddrpt;
   size_t       offset;
   unsigned int access;

//LOG_EXTBUS("Entry '%s'\n", entry->name);

   if (eval_regaddress(entry, &baddrpt, &offset, &access, 0)) {
      // success, update values
//LOG_INFO("BASEADDR: '%s (%d)'\n", baddrpt->name, baddrpt->id);
      entry->info = baddrpt->info;
      entry->offset = offset;
      entry->access = access;
      if (entry->ndata > 0) {
         entry->baddr = baddrpt->id;
      }
//      LOG_EXTBUS("Done '%s', %d 0x%04x %d %d %d %d 0x%08x\n", entry->name,
//             entry->baddr, entry->offset, entry->ndata, entry->dtype, entry->bytesize, entry->access, entry->flags);
      return(1);

   } else {
      LOG_ERROR("...... '%s'\n", entry->name);
      // error, use safe values...
      entry->info = DANCE_REG_PT(FAKEBADDR)->info;
      entry->offset = 0;
      entry->access = _NOACCESS;
      return(0);
   }
}


int dance_regtable_init(void) {
   danceregdescr_t* rd = gdance_regdescr;
   size_t           nentries = rd->n_entries;
   int              i;
   int              ret = 1;

   rd->first_baseaddr = nentries;
   rd->baseaddr_n = 0;
   rd->first_register = nentries;
   rd->register_n = 0;
   rd->first_memblock = nentries;
   rd->memblock_n = 0;

   for (i = 0; i < nentries; i++) {
      dancereg_t* entry = rd->regtbl + i;

LOG_EXTBUS("Entry %d: '%s', %d 0x%04x %d %d %d %d 0x%08x\n", i, entry->name,
             entry->baddr, entry->byteoffset, entry->ndata, entry->dtype, entry->bytesize, entry->accmode, entry->flags);
      if (rd->first_register == nentries) {
         // if not registers yet, check if baseaddres
         if (entry->ndata == 0) {
            // and it is baseaddress, increase counter and go for next entry.
            if (rd->baseaddr_n++ == 0)
               rd->first_baseaddr = i;
            if (!update_reg_address(entry))
               ret = 0;
            continue;
         }
         if (rd->baseaddr_n == 0) // if there are no baseaddreses
            rd->first_baseaddr = 0;
      }
      if (rd->first_memblock == nentries) {
         // if not memblocks yet
         if (entry->ndata == 1) {
            // and it is register, increase counter and go for next entry.
            if (rd->register_n++ == 0)
               rd->first_register = i;
            if (!update_reg_address(entry))
               ret = 0;
            continue;
         }
         if (rd->register_n == 0) // if there are no registers
            rd->first_register = rd->baseaddr_n;
      }
      // if we are here, entry is memblock
      // so, add it and go for next entry.
      if (rd->memblock_n++ == 0)
         rd->first_memblock = i;
      if (!update_reg_address(entry))
         ret = 0;
   }
   if (rd->baseaddr_n == 0)
      rd->first_baseaddr = 0;
   if (rd->register_n == 0)
      rd->first_register = rd->baseaddr_n;
   if (rd->memblock_n == 0)
      rd->first_memblock = rd->first_register + rd->register_n;
LOG_TRACE();
   return(ret);
}

void dance_regtable_fini(void) {
   danceregdescr_t* rd = gdance_regdescr;
   int              i, j;

   for (i = 0; i < rd->baseaddr_n; i++) {
      dancereg_t* baddr_pt = rd->regtbl + i;

      if (baddr_pt->info != NULL) {
         for (j = i + 1; j < rd->baseaddr_n; j++) {
            dancereg_t* next_pt = rd->regtbl + j;

            if (next_pt->info == baddr_pt->info)
               next_pt->info = NULL;
         }
         free(baddr_pt->info);
         baddr_pt->info = NULL;
      }
   }
}


static void pcibus_close(dancebus_t* bus) {
   int base_addr;

   LOG_EXTBUS("Closing PCIe device\n");
   for (base_addr = 0; base_addr < PCI_NBARS; base_addr++) {
      epcihandle_t pcihandle = bus->info.pcihandles[base_addr];

      if (pcihandle == EPCI_BAD_HANDLE) continue ;

      if (!IN_SIMULATION_MODE())
         epci_close(pcihandle);

      bus->info.pcihandles[base_addr] = EPCI_BAD_HANDLE;
   }
}


static int pcibus_init(dancebus_t* bus) {
   const char* device_viddid = bus->ident;
   int         base_addr;
   int         bar_cnt;
   epcihandle_t  dev;

   // Open connection to PCI device
   LOG_EXTBUS("Opening PCIe VID:DID: %s\n", device_viddid);

   // Cleanup any previous setup
   if(!extbus_set_debug(0))
      return(0);
   pcibus_close(bus);

   // Set default debug level
   if(!extbus_set_debug(0))
      return(0);


   // Get an access to all base addresses even if not implemented on
   // the hardware. The PCI layer allows this.
   for (bar_cnt = 0, base_addr = 0; base_addr < PCI_NBARS; base_addr++) {

//      LOG_EXTBUS("Opening base address: %d\n", base_addr);
      dev = EPCI_BAD_HANDLE;
      if (IN_SIMULATION_MODE() || (dev = epci_open(device_viddid, "", base_addr)) != EPCI_BAD_HANDLE)
         bar_cnt++;

LOG_EXTBUS("Opening base address: %d (%p)\n", base_addr, dev);
      bus->info.pcihandles[base_addr] = dev;
   }

   // Normal end
   return(bar_cnt > 0);
}

static void ei2cbus_close(dancebus_t* bus) {
   LOG_EXTBUS("Closing I2C master device\n");
   if (bus->info.i2chandle.master_id == bus->bus_id) {  // I2C main bus
      if (bus->info.i2chandle.master != NULL)
         i2c_fini(bus->info.i2chandle.master);
   }
   bus->info.i2chandle.master = NULL;
}

static int ei2cbus_init(dancebus_t* bus) {

   if (bus->info.i2chandle.master_id == bus->bus_id) {  // I2C main bus
      bus->info.i2chandle.master = malloc(sizeof(i2c_master_t));
      if (bus->info.i2chandle.master == NULL) {
         LOG_ERROR("error allocating I2C master\n");
         return -1;
      } else if (i2c_linux_init(bus->info.i2chandle.master, bus->ident) < 0) {
         LOG_ERROR("error initialising I2C bus %s\n", bus->ident);
         return -1;
      }

   } else {                 // I2C secondary bus
      if (!IS_VALID_BUS_ID(bus->info.i2chandle.master_id)) {
         LOG_ERROR("invalid master I2C bus for sub-bus %s\n", bus->ident);
         return -1;
      } else {
         dancebus_t* masterbus = DANCE_BUS_PT(bus->info.i2chandle.master_id);
         uint16_t    iaddr = bus->info.i2chandle.busaddr;

         if (masterbus->bustype != _EI2C_BUS || masterbus->info.i2chandle.master == NULL) {
            LOG_ERROR("I2C sub-bus %s has not a valid I2C master bus\n", bus->ident);
            return -1;
         } else if (iaddr < 0 || iaddr > 0x007f) {
            LOG_ERROR("invalid address %d for I2C sub-bus %s\n", iaddr, bus->ident);
            return -1;
         }
         bus->info.i2chandle.master = masterbus->info.i2chandle.master;
/*
         bus->info.i2chandle.i2ctype
*/
      }
   }
   return(0);
}

int dance_extbus_initbus(int index) {
   // This execute a single extbus initialization
   dancebus_t* bus = &gdance_busdescr->bustbl[index];
   switch (bus->bustype) {
      case _FAKE_BUS:
         break;

      case _PCIE_BUS:
         if (!pcibus_init(bus)) {
            LOG_ERROR("ERROR: unable to initialise PCI bus %s at %s\n", bus->bname, bus->ident);
            return 0;
         }
         break;

      case _EI2C_BUS:
         if (ei2cbus_init(bus) < 0) {
            LOG_ERROR("ERROR: unable to initialise I2C bus %s at %s\n", bus->bname, bus->ident);
            return 0;
         }
         break;

      default:
         LOG_ERROR("ERROR: unknown type for bus %s %s\n", bus->bname, bus->ident);
         return 0;
   }

   return 1;
}

int dance_extbus_init(void) {
   int i;

   for (i = 0; i < gdance_busdescr->n_bus; i++) {
      dancebus_t* bus = &gdance_busdescr->bustbl[i];
      if (bus->flags & _INIT_ON_START) {
         if(!dance_extbus_initbus(i))
            return 0;
      }
   }
   return 1;
}


void dance_extbus_close(void) {
   int           i;

   for (i = 0; i < gdance_busdescr->n_bus; i++) {
      dancebus_t* bus = DANCE_BUS_PT_INDEX(i);

      switch (bus->bustype) {
         case _PCIE_BUS: pcibus_close(bus); break;
         case _EI2C_BUS: ei2cbus_close(bus); break;
         default:  break;   // _FAKE_BUS
      }
   }
}


static dancebus_t* dance_extbus_find(const char* name, int bustype, int order) {
   int  i, count;

   for (i = count = 0; i < gdance_busdescr->n_bus; i++) {
      dancebus_t* bus = DANCE_BUS_PT_INDEX(i);

      if (name && strcmp(name, bus->bname) != 0)
         continue;
      if (bustype && bustype != bus->bustype)
         continue;
      if (order && order != ++count)
         continue;

      return bus;
   }
   return NULL;
}

static dancereg_t* baseaddress_find(const char* busname, int bustype, int busorder, int selector) {
   danceregdescr_t* rd = gdance_regdescr;
   dancebus_t*      bus = NULL;
   dancereg_t*      entry;
   int              nreg;

   if (busname || busorder) {
      bus = dance_extbus_find(busname, bustype, busorder);
      if (bus == NULL)
         return NULL;
   }

   entry = rd->regtbl + rd->first_baseaddr;
   for (nreg = rd->baseaddr_n; nreg--; entry++) {
      if (bus && entry->info->bus != bus)
         continue;
      if (bustype && entry->info->bustype != bustype)
         continue;
      if (selector >= 0 && entry->info->selector != selector)
         continue;

      return entry;
   }
   return NULL;
}


/*---------------------------------------------------------------------------
 *
 */
int extbus_set_debug(int level) {
   // Change library debug level
   if(epci_setparam(EPCI_LIB_DEFAULTS, "debug", DDPAR(level)) == EPCI_ERR) {
      const char* errmsg;
      epci_error(&errmsg);
      LOG_ERROR("ERROR: unable to set debug level: %s\n", errmsg);
      return(0);
   }
   // Normal end
   return(1);
}

/*-------------------------------------
 *
 */
static dancereg_t *find_regmem(const char *name, int type, int access) {
   int  first;
   int  n;
   int  idx;

   if (type == _ALL_REG) {
      first = gdance_regdescr->first_register;
      n = gdance_regdescr->register_n;
   } else if (type == _ALL_MEM) { //  _ALL_MEM
      first = gdance_regdescr->first_memblock;
      n = gdance_regdescr->memblock_n;
   } else { // _ANY_REGMEM
      first = 0;
      n = gdance_regdescr->n_entries;
   }

   LOG_EXTBUS("Looking for reg/mem \"%s\" type:0x%x access:%d\n", name, type, access);
   for (idx = first; n-- > 0; idx++) {
      dancereg_t *reg = DANCE_REG_PT(idx);

//      LOG_EXTBUS(" -> regmem: \"%s\" access:%d\n", reg->name, reg->access);
      if (strcmp(name, reg->name) == 0) {
         if (access == _NOACCESS || reg->access & access) {
            LOG_EXTBUS(" regmem found\n");
            return(reg);
         } else {
            return(NULL);
         }
      }
   }
   return(NULL);
}


dancereg_t* extbus_parse_regmem(const char *reg_id, int type, int access) {
   if (isalpha(*reg_id)) {
      dancereg_t* regmem = find_regmem(reg_id, type, _ANY);

      if (regmem == NULL)
         set_libdanceerrorf("Bad register or memory block name \'%s\'", reg_id);
      return regmem;

   } else {
      static __thread dancereg_t regmem;
      int             nchar = 0;
      const char*     idptr = reg_id;
      dancebus_t*     extbus = NULL;
      dancereg_t*     baddr_entry;
      char            busname[64];

      if (*idptr == ':') {  // parse busname
         int  i;
         char c;

         for (i = 0; i < sizeof(busname); i++) {
            c = *(++idptr);

            if (isspace(c))
               break;
            else if (c == ':') {
               ++idptr;
               busname[i] = c = 0;
               break;
            } else
               busname[i] = c;
         }
         if (c != 0)
            goto on_syntax_error;
         else {
            extbus = dance_extbus_find(busname, 0, 0);
            if (extbus == NULL)
               goto on_syntax_error;
         }
      }
      if (extbus == NULL) {
         // Get the first FPGA bustype
         if (gdance_busdescr->n_bus < 2)  // if only FAKEBUS, error
            goto on_syntax_error;
         else                              // otherwise use the first bus in the list
            extbus = DANCE_BUS_PT_INDEX(1);
      }

      if (type == _ALL_REG) {
         regmem.ndata = 1;
         if (sscanf(idptr, "%i:%zi%n", &regmem.baddr, &regmem.offset, &nchar) != 2 || *(idptr + nchar))
            goto on_syntax_error;
      } else {   //  _ALL_MEM
         if (sscanf(idptr, "%i:%zi:%zi%n", &regmem.baddr, &regmem.offset, &regmem.ndata, &nchar) != 3 || *(idptr + nchar))
            goto on_syntax_error;
      }

      baddr_entry = baseaddress_find(extbus->bname, 0, 0, regmem.baddr);

      if (baddr_entry == NULL) {
         set_libdanceerrorf("Bus selector or base address not declared or initialised");
         return NULL;
      } else {
         regmem.name = (char*)reg_id;
         regmem.dtype = _D32;
         regmem.access = access;
         regmem.info = baddr_entry->info;
         return &regmem;
      }

 on_syntax_error:
      if (type == _ALL_REG)
         set_libdanceerrorf("Bad register descriptor, should be \"[:busname:]selector:offset\"");
      else   //  _ALL_MEM
         set_libdanceerrorf("Bad register descriptor, should be \"[:busname:]selector:offset:size\"");
      return NULL;
   }
}

size_t extbus_sprint_regmem(dancereg_t* reg, char* print_buff, size_t buf_sz) {
   size_t len;

   switch (reg->info->bustype) {
      case _PCIE_BUS:
         len = snprintf(print_buff, buf_sz, "%1d:0x%04zx", DANCE_REG_PT(reg->baddr)->flags, reg->offset);
         break;
      case _EI2C_BUS:
         len = snprintf(print_buff, buf_sz, "ei2c:%04zx", reg->offset);
         break;
      default:
         len = snprintf(print_buff, buf_sz, "???");
         break;
   }
   return(len);
}

static void value_to_shadow(dancereg_t* regmem, const void* buff) {
   switch(regmem->dtype) {
      case _D8:  regmem->shadow.v8  = *((uint8_t*)buff);  break;
      case _D16: regmem->shadow.v16 = *((uint16_t*)buff); break;
      case _D32: regmem->shadow.v32 = *((uint32_t*)buff); break;
      case _D64: regmem->shadow.v64 = *((uint64_t*)buff); break;
      default: SYSTEM_ERROR(); break;
   }
}

static void shadow_to_value(dancereg_t* regmem, const void* buff) {
   switch(regmem->dtype) {
      case _D8:   *((uint8_t*)buff) = regmem->shadow.v8;  break;
      case _D16: *((uint16_t*)buff) = regmem->shadow.v16; break;
      case _D32: *((uint32_t*)buff) = regmem->shadow.v32; break;
      case _D64: *((uint64_t*)buff) = regmem->shadow.v64; break;
      default: SYSTEM_ERROR(); break;
   }
}


int extbus_read_regmem(dancereg_t* regmem, ssize_t offset, ssize_t rstep, size_t nvalues, void* buff, ssize_t bstep) {
   offset += regmem->offset;

//LOG_INFO("dtype: %d\n", regmem->dtype);
//LOG_INFO("rstep: %d\n", rstep);
//LOG_INFO("bstep: %d\n", bstep);
   if (IN_SIMULATION_MODE()) {
      ssize_t bufincr = bstep * regmem->dtype;
      size_t  n;

      for (n = nvalues; n--; buff += bufincr)
         shadow_to_value(regmem, buff);

   } else {
      switch(regmem->info->bustype) {
         case _PCIE_BUS:
            {
               epcihandle_t pcihandle = regmem->info->bushandle.pci;
               unsigned int dtype = (regmem->flags & _D64R_REG)? BIN_64R : regmem->dtype;

               if (nvalues <= 1) {  // nvalues == 0 or 1
                  size_t   algnoff = offset & ~(size_t)0x03;
                  uint32_t tmpval;
                  uint8_t  shift;

                  switch(dtype) {
                     case _D8:
                        epci_rd32_reg(pcihandle, algnoff, &tmpval);
                        shift = (offset & (size_t)0x03) * 8;
                        *(uint8_t*)buff = ((tmpval >> shift) & (uint32_t)0x00ff);
                        break;
                     case _D16:
                        epci_rd32_reg(pcihandle, algnoff, &tmpval);
                        if (offset & 0x02)
                           *(uint16_t*)buff = ((tmpval >> 16) & (uint32_t)0x00ffff);
                        else
                           *(uint16_t*)buff = (tmpval & (uint32_t)0x00ffff);
                        break;
                     case _D32:
                        epci_rd32_reg(pcihandle, algnoff, buff);
                        break;
                     case _D64:
                        epci_rd64_reg(pcihandle, algnoff, buff);
                        break;
                     case BIN_64R:
                        epci_rd64R_reg(pcihandle, algnoff, buff);
                        break;
                  }
/*
               } else if (bstep == 1) {
LOG_TRACE();
                  if (epci_rd_blk(pcihandle, offset, rstep, nvalues, regmem->dtype, buff) != EPCI_OK) {
                     errmsg_t errmsg;
                     if (epci_error(&errmsg) != EPCI_OK)
                        errmsg = "unknown error";
                     set_libdanceerrorf(errmsg);
                     goto on_error;
                  }
*/
               } else {
                  size_t  n;
                  ssize_t offincr = rstep * regmem->dtype;
                  ssize_t bufincr = bstep * regmem->dtype;

//LOG_INFO("dtype: %d\n", regmem->dtype);
//LOG_INFO("bstep: %d\n", bstep);
//LOG_INFO("buff: %p\n", buff);
//LOG_INFO("nvalues: %zd / offset: 0x%zx / offincr: %zd / bufincr: %zd\n", nvalues, offset, offincr, bufincr);
                  for (n = nvalues; n--; offset += offincr, buff += bufincr) {
//if (((n+1) & 0x0FFF) == 0) {
//LOG_INFO("n: %zd / buff: %p\n", n+1, buff);
//}
                     epci_rd_reg(pcihandle, offset, dtype, buff);
                  }
//LOG_INFO("n: %zd / buff: %p\n", n+1, buff);
               }
            }
            break;

         case _EI2C_BUS:
            set_libdanceerrorf("I2C bus access not implemented");
            goto on_error;
            {
//               int     i2c_handle;
               size_t  n;
               ssize_t offincr = rstep * regmem->dtype;
               ssize_t bufincr = bstep * regmem->dtype;

               for (n = nvalues; n--; offset += offincr, buff += bufincr) {
//                  ei2c_rd_reg(i2c_handle, offset, regmem->dtype, buff);
               }
            }
            break;

         default:
            set_libdanceerrorf("Bus not implemented");
            goto on_error;
      }
   }
//   LOG_EXTBUS("%zd %d-bit values read from register %s\n", nvalues, regmem->dtype * 8, regmem->name);
   return 0;

 on_error:
   LOG_ERROR("%s : Error reading %zd %d-bit values from register %s\n", libdanceerror(), nvalues, regmem->dtype * 8, regmem->name);
   return -1;
}

int extbus_write_regmem(dancereg_t* regmem, ssize_t offset, ssize_t rstep, size_t nvalues, const void* buff, ssize_t bstep) {

   value_to_shadow(regmem, buff);

   offset += regmem->offset;

   if (!IN_SIMULATION_MODE()) {
      switch(regmem->info->bustype) {
         case _PCIE_BUS:
            {
               epcihandle_t pcihandle = regmem->info->bushandle.pci;
               unsigned int dtype = (regmem->flags & _D64R_REG)? BIN_64R : regmem->dtype;

               if (nvalues <= 1) {  // nvalues == 0 or 1
                  size_t   algnoff = offset & ~(size_t)0x03;
                  uint32_t tmpval;
                  uint8_t  shift;

                  switch(dtype) {
                     case _D8:
                        epci_rd32_reg(pcihandle, algnoff, &tmpval);
                        shift = (offset & (size_t)0x03) * 8;
                        tmpval &= ~((uint32_t)0x00ff << shift);
                        tmpval |=  ((uint32_t)regmem->shadow.v8 << shift);
                        epci_wr32_reg(pcihandle, algnoff, tmpval);
                        break;
                     case _D16:
                        epci_rd32_reg(pcihandle, algnoff, &tmpval);
                        if (offset & 0x02) {
                           tmpval &= (uint32_t)0x0000ffff;
                           tmpval |= (uint32_t)regmem->shadow.v16 << 16;
                        } else {
                           tmpval &= (uint32_t)0xffff0000;
                           tmpval |= (uint32_t)regmem->shadow.v16;
                        }
                        epci_wr32_reg(pcihandle, algnoff, tmpval);
                        break;
                     case _D32:
                        epci_wr32_reg(pcihandle, algnoff, *(uint32_t*)buff);
                        break;
                     case _D64:
                        epci_wr64_reg(pcihandle, algnoff, *(uint64_t*)buff);
                        break;
                     case BIN_64R:
                        epci_wr64R_reg(pcihandle, algnoff, *(uint64_t*)buff);
                        break;
                  }
               } else if (bstep == 1) {
                  if (epci_wr_blk(pcihandle, offset, rstep, nvalues, dtype, (void*)buff) != EPCI_OK) {
                     errmsg_t errmsg;
                     if (epci_error(&errmsg) != EPCI_OK)
                        errmsg = "unknown error";
                     set_libdanceerrorf(errmsg);
                     goto on_error;
                  }
               } else {
                  size_t  n;
                  ssize_t offincr = rstep * regmem->dtype;
                  ssize_t bufincr = bstep * regmem->dtype;

                  for (n = nvalues; n--; offset += offincr, buff += bufincr)
                     epci_wr_reg(pcihandle, offset, dtype, (void*)buff);
               }
            }
            break;

         case _EI2C_BUS:
            set_libdanceerrorf("I2C bus access not implemented");
            goto on_error;
            {
//               int     i2c_handle;
               size_t  n;
               ssize_t offincr = rstep * regmem->dtype;
               ssize_t bufincr = bstep * regmem->dtype;

               for (n = nvalues; n--; offset += offincr, buff += bufincr) {
//                  ei2c_wr_reg(i2c_handle, offset, regmem->dtype, buff);
               }
            }
            break;

         default:
            set_libdanceerrorf("Bus not implemented");
            goto on_error;
      }
   }
//   LOG_EXTBUS("%zd %d-bit values written to regmem %s\n", nvalues, regmem->dtype * 8, regmem->name);
   return 0;

 on_error:
   LOG_ERROR("%s : Error writing %zd %d-bit values to regmem %s\n", libdanceerror(), nvalues, regmem->dtype * 8, regmem->name);
   return -1;
}


#define EXTBUS_READ_VALUE_TEMPLATE(__w)  \
uint ## __w ## _t extbus_read_value ## __w(dancereg_t* reg, ssize_t offset) {  \
   extbus_read_regmem(reg, offset, 0, 1, &reg->shadow.v ## __w, 0); \
   return reg->shadow.v ## __w; \
}

EXTBUS_READ_VALUE_TEMPLATE(8)
EXTBUS_READ_VALUE_TEMPLATE(16)
EXTBUS_READ_VALUE_TEMPLATE(32)
EXTBUS_READ_VALUE_TEMPLATE(64)

#define EXTBUS_WRITE_VALUE_TEMPLATE(__w)  \
void extbus_write_value ## __w(dancereg_t* reg, ssize_t offset, uint ## __w ## _t value) { \
   extbus_write_regmem(reg, offset, 0, 1, &value, 0);  \
}

EXTBUS_WRITE_VALUE_TEMPLATE(8)
EXTBUS_WRITE_VALUE_TEMPLATE(16)
EXTBUS_WRITE_VALUE_TEMPLATE(32)
EXTBUS_WRITE_VALUE_TEMPLATE(64)
