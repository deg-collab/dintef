#ifndef ETH_H_INCLUDED
#define ETH_H_INCLUDED


#include <stdint.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>


typedef struct {
#define ETH_FLAG_VALID       (1 << 0)
#define ETH_FLAG_DUPLEX_HALF (1 << 1)
#define ETH_FLAG_DUPLEX_FULL (1 << 2)
#define ETH_FLAG_DUPLEX_MASK (ETH_FLAG_DUPLEX_HALF | ETH_FLAG_DUPLEX_FULL)
#define ETH_FLAG_SPEED_10    (1 << 3)
#define ETH_FLAG_SPEED_100   (1 << 4)
#define ETH_FLAG_SPEED_1000  (1 << 5)
#define ETH_FLAG_SPEED_MASK (ETH_FLAG_SPEED_10 | ETH_FLAG_SPEED_100 | ETH_FLAG_SPEED_1000)
#define ETH_FLAG_LINK_UP     (1 << 6)
#define ETH_FLAG_LINK_DOWN   (1 << 7)
#define ETH_FLAG_LINK_CABLE  (1 << 8)
#define ETH_FLAG_LINK_MASK  (ETH_FLAG_LINK_UP | ETH_FLAG_LINK_DOWN | ETH_FLAG_LINK_CABLE)
#define ETH_FLAG_AUTONEG_ON  (1 << 9)
#define ETH_FLAG_AUTONEG_OFF (1 << 10)
#define ETH_FLAG_AUTONEG_MASK (ETH_FLAG_AUTONEG_ON | ETH_FLAG_AUTONEG_OFF)
#define ETH_FLAG_FIX_APPLIED (1 << 11)
#define ETH_FLAG_HWADDR      (1 << 12)
   uint32_t    eflags;

   const char* name;
   uint32_t    appflags;

   struct sockaddr hw_addr;

   int         ioctl_fd;
   struct ifreq ifr;

   int         nl_fd;

} eth_handle_t;


int eth_init(void);
void eth_fini(void);
int eth_set_info(eth_handle_t*);
int eth_handle_event(eth_handle_t*);
unsigned int eth_is_link_and_cable(const eth_handle_t*);
void eth_fix_half_duplex(eth_handle_t*);


#endif /* ETH_H_INCLUDED */
