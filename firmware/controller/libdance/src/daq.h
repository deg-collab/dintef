#ifndef DAQ_H_INCLUDED
#define DAQ_H_INCLUDED 1


#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <pthread.h>
#include "errors.h"
#include "lnklist.h"
#include "memory.h"
#include "libepci.h"
#include "libedma.h"
#include "libebuf.h"

#ifdef DSOURCE_LIST
   #define DAQ_USED 1
   #ifndef DAQBADDR_LIST
      #error "DAQ_BASE missing (hint: check app_daq.h)"
   #endif
#else
   #define DAQ_USED 0
   #define DAQBADDR_LIST \
      BASEADDR(DAQ_BASE, FAKEBUS, 1, 0x1c00, _RO)
#endif


#define DAQREG_LIST \
   REG32DEF(DAQ_SRC_SEL,   DAQ_BASE, 0x0000, _RW, _BASIC_REG) \
   REG32DEF(DAQ_TRIG_SEL,  DAQ_BASE, 0x0004, _RW, _BASIC_REG) \
   REG32DEF(DAQ_WPOS_TOP,  DAQ_BASE, 0x0008, _RW, _BASIC_REG) \
   REG32DEF(DAQ_CTL,       DAQ_BASE, 0x000c, _RW, _BASIC_REG) \
   REG32DEF(DAQ_CNT_TOP,   DAQ_BASE, 0x0010, _RW, _BASIC_REG) \
   REG32DEF(DAQ_CNT_CTL,   DAQ_BASE, 0x0014, _RW, _BASIC_REG) \
   REG32DEF(DAQ_DBG_RW,    DAQ_BASE, 0x0018, _RW, _BASIC_REG) \
   \
   REG32DEF(DAQ_SRC_MAX,   DAQ_BASE, 0x0020, _RO, _BASIC_REG) \
   REG32DEF(DAQ_SRC_WIDTH, DAQ_BASE, 0x0024, _RO, _BASIC_REG) \
   REG32DEF(DAQ_TRIG_MAX,  DAQ_BASE, 0x0028, _RO, _BASIC_REG) \
   REG32DEF(DAQ_WPOS_VAL,  DAQ_BASE, 0x002c, _RO, _BASIC_REG) \
   REG32DEF(DAQ_MEM_SIZE,  DAQ_BASE, 0x0030, _RO, _BASIC_REG) \
   REG32DEF(DAQ_STA,       DAQ_BASE, 0x0034, _RO, _BASIC_REG) \
   REG32DEF(DAQ_CNT_FREQ,  DAQ_BASE, 0x0038, _RO, _BASIC_REG) \
   REG32DEF(DAQ_DBG_RO,    DAQ_BASE, 0x003c, _RO, _BASIC_REG)



struct channel_s;
struct daq_handle_s;


/* dsource */

typedef struct dsource_desc
{
  /* warning: must follow DFMT_xxx order */
#define DSOURCE_FLAG_INT8 (1 << 0)
#define DSOURCE_FLAG_INT16 (1 << 1)
#define DSOURCE_FLAG_INT32 (1 << 2)
#define DSOURCE_FLAG_INT64 (1 << 3)
#define DSOURCE_FLAG_UINT8 (1 << 4)
#define DSOURCE_FLAG_UINT16 (1 << 5)
#define DSOURCE_FLAG_UINT32 (1 << 6)
#define DSOURCE_FLAG_UINT64 (1 << 7)
#define DSOURCE_FLAG_FLOAT (1 << 8)
#define DSOURCE_FLAG_DOUBLE (1 << 9)
#define DSOURCE_FLAG_FMT_MASK ((1 << 10) - 1)
#define DSOURCE_FLAG_FREE_SID (1 << 10)
#define DSOURCE_FLAG_GROUP (1 << 11)
#define DSOURCE_FLAG_FSCAL (1 << 12)
#define DSOURCE_FLAG_RESOL (1 << 13)
  uint32_t flags;

  /* symbolic identifier */
  const char* sid;

  /* hardware identifier. possibly more than one if group. */
  uint32_t hid;

  /* configured by the client */
  double fscal_or_resol;

} dsource_desc_t;

danceerr_t dsource_set_fscal(const char*, double);
danceerr_t dsource_set_resol(const char*, double);


/* dstream */

#define DSTREAM_ID_SIZE 12

#define DSTREAM_ID_INVALID "INVALID"
#define DSTREAM_ID_ASCII   "ASCII"
#define DSTREAM_ID_BINARY  "BINARY"
#define DSTREAM_ID_LOG     "LOG"

typedef struct dstream_handle_s
{
#define DSTREAM_FLAG_LOCAL  (1 << 0)
#define DSTREAM_FLAG_ENABLE (1 << 1)
  uint32_t flags;

  /* stream identifier */
  char id[DSTREAM_ID_SIZE + 1];

  /* sources enabled for this stream. 1 bit per source. */
  uint32_t dsource_hid;

  /* start and sampling triggers, 1 bit per trigger. */
#define DTRIG_ID_INVALID 0
#define DTRIG_ID_FREQ (1 << 0)
#define DTRIG_ID_DI1  (1 << 1)
#define DTRIG_ID_DI2  (1 << 2)
#define DTRIG_ID_SOFT (1 << 3)
#define DTRIG_ID_APP  (1 << 4)
  uint32_t dtrig_start;
  uint32_t dtrig_sampl;

  /* TODO: remove once control part discussed */
  uint32_t fsampl;
  double tsampl;
  uint32_t nsampl;

#if 0 /* TODO, currently in daq_handle */
  /* hardware pointers */
  uint32_t hid;
  uint32_t rpos;
  uint32_t wpos;
#else
  struct daq_handle_s* daq;
#endif /* TODO */

  /* dstream linked list */
  lnklist_item_t* item;

  /* list of channel_t*, lock protected */
  lnklist_t subscribers;
  pthread_mutex_t lock;

} dstream_handle_t;

typedef struct dstream_desc
{
  const char* id;
  uint32_t flags;
} dstream_desc_t;

void dstream_init_desc(dstream_desc_t*);
dstream_handle_t* dstream_create(const dstream_desc_t*);
danceerr_t dstream_destroy(dstream_handle_t*);
unsigned int dstream_is_subscriber(dstream_handle_t*, const struct channel_s*);
danceerr_t dstream_add_subscriber(dstream_handle_t*, struct channel_s*);
danceerr_t dstream_del_subscriber(dstream_handle_t*, struct channel_s*);
dstream_handle_t* dstream_get_by_id(const char*, struct channel_s*);
dstream_handle_t* dstream_get_log(void);
dstream_handle_t* dstream_get_binary(struct channel_s*);
dstream_handle_t* dstream_et_ascii(struct channel_s*);
unsigned int dstream_is_enabled(dstream_handle_t*);
danceerr_t dstream_framize_bufferlist(dstream_handle_t*, cbuflist_t*);
danceerr_t dstream_write_frame(dstream_handle_t*, dblock_t*);
danceerr_t dstream_printf(dstream_handle_t*, const char*, ...);


/* daq */

typedef struct
{
  const char* pci_id;
  size_t pci_bar;
  size_t pci_rw_off;
  size_t pci_ro_off;
} daq_desc_t;

danceerr_t daq_init(void);
void daq_fini(void);


/* application macros */

#define DSOURCE_IT(__sid, __hid, __fmt) { \
   .flags = DSOURCE_FLAG_ ## __fmt,	\
   .sid   = __sid,				\
   .hid   = 1 << __hid			\
}

#define DSOURCE_MAX_COUNT 32



#ifdef __INCLUDE_DANCE_DECLARATIONS

#ifdef DANCE_APP_DAQ_H
#include DANCE_APP_DAQ_H
#endif

dsource_desc_t g_dsource_descs[DSOURCE_MAX_COUNT] =
{
#ifdef DSOURCE_LIST
  DSOURCE_LIST,
#endif /* DSOURCE_LIST */
  { .flags = 0 }
};

#ifdef FLM_LEGACY_TOREMOVE
/* this is used to detect DAQ hardware is disabled */
#ifndef DAQ_DESC
#define DAQ_DESC { .pci_bar = -1 }
#endif
const daq_desc_t g_daq_desc = DAQ_DESC;
#endif /* FLM_LEGACY_TOREMOVE */

/* TODO: HDL 
 * currently hardcoded per instrument at HDL synthesis, 
 * should be got indirectly, from register hardcoded in EBONE,
 * like DAQ_BASE is got from DAQ_REGS_OFF register
 */
/* describe DMA hardware resources */
/* EDMA_FLAG_MIF for DDR */
/* EDMA_FLAG_BRAM for bram */
#if DAQ_USED

#define DMA_DESC                \
{                               \
 .pci_bar = 1,                  \
 .pci_off = 0x400,              \
 .ebs_seg = 1,                  \
 .ebs_off = 0x200,              \
 .flags = EDMA_FLAG_MIF         \
}

#endif /* DAC_USED */

#ifndef DMA_DESC
#define DMA_DESC { .pci_bar = -1 }
#endif

const edma_desc_t g_dma_desc = DMA_DESC;

#else /* ! __INCLUDE_DANCE_DECLARATIONS */

extern dsource_desc_t g_dsource_descs[DSOURCE_MAX_COUNT];
extern edma_desc_t g_dma_desc;
#ifdef FLM_LEGACY_TOREMOVE
extern daq_desc_t g_daq_desc;
#endif 

#endif /* __INCLUDE_DANCE_DECLARATIONS */


/* ---------------------------------------------------------------------
 * EXTDEVIE_LIST declaration:
 *
 * DMA_CTRL(DAQCHS,     FPGA, 0, EBFT, EDMA_FLAG_MIF)
 *          |           |     |  |     |
 *          |           |     |  |     Flags:
 *          |           |     |  |        EBFT_MIF, EBFT_BRAM, EBFT_FIFO
 *          |           |     |  Type:
 *          |           |     |     EBFT (Ebone Fast Transmitter)
 *          |           |     Address:
 *          |           |        0 or 1 (for EBFT)
 *          |           Bus Id (defined in EXTBUS_LIST)
 *          DMA controller Id (used as name)
 */

/* Hide libedma long names (used in DMA_CTRL(..., flags)) */
#define EBFT_MIF  EDMA_FLAG_MIF
#define EBFT_BRAM EDMA_FLAG_BRAM
#define EBFT_FIFO EDMA_FLAG_FIFO

/* List of supported DMA devices (used in DMA_CTRL(..., type, ...)) */
#define EXTDEV_EBFT 0


#define DSCOPE_LOCAL 0
#define DSCOPE_GLOBAL 1
#define DSCOPE_LIST { "LOCAL", "GLOBAL" }

#define DSTATE_DISABLE 0
#define DSTATE_ENABLE 1
#define DSTATE_LIST { "OFF", "ON" }

/* warning: must follow DSTREAM_FLAG_xxx order */
#define DFMT_INT8   0
#define DFMT_INT16  1
#define DFMT_INT32  2
#define DFMT_INT64  3
#define DFMT_UINT8  4
#define DFMT_UINT16 5
#define DFMT_UINT32 6
#define DFMT_UINT64 7
#define DFMT_FLOAT  8
#define DFMT_DOUBLE 9
#define DFMT_LIST				\
{						\
 "INT8", "INT16", "INT32", "INT64",		\
 "UINT8", "UINT16", "UINT32", "UINT64",		\
 "FLOAT", "DOUBLE"				\
}

#define fmtDSTRMID fmtLABEL
#define fmtDSRCID fmtLABEL
#define fmtTRIGID fmtLABEL

/* DSTREAM DSTRMID [ GLOBAL | LOCAL ] [ ON | OFF ] [ SRC nDSRCID ] */
#define fmtDSTREAM		\
 fmtDSTRMID			\
 "{"				\
  " oC#DSCOPE"			\
  " oC#DSTATE"			\
  " o(T#FSAMPL uI_U#FREQ)"	\
  " o(T#TSAMPL uF_U#TIME)"	\
  " o(T#NSAMPL uI)"		\
  " o(T#TRIG " fmtTRIGID " " fmtTRIGID ")" \
  " o(T#APPLY)"			\
  " o(T#FLUSH)"			\
  " o(T#DEL)"			\
  " o(T#STOP)"			\
  " o(T#SRC n" fmtDSRCID ")"	\
 "}"

/* ?DSTREAM [ DSTRMID ] [ READ <nsampl> ] */
#define fmtqDSTREAM		\
 "o("				\
  fmtDSTRMID			\
  " o(T#READ uI)"		\
  " o(T#NSAMPL)"		\
  " o(T#STATUS)"		\
 ")"

/* ?*DSTREAM DSTRMID [ READ NSAMPL ] */
#define fmtqbDSTREAM		\
 fmtDSTRMID			\
 " o(T#READ uI)"

/* DSOURCE DSRCID [ FMT <fmt> ] [ GROUP nDSRCID ] */
#define fmtDSOURCE		\
 fmtDSRCID			\
 "{"				\
  " o(T#FMT C#DFMT)"		\
  " o(T#FSCAL | T#RESOL F)"	\
  " o(T#GROUP n" fmtDSRCID ")"	\
 "}"


/* ?DSOURCE [ DSRCID ] [ NOGROUP ] */
#define fmtqDSOURCE		\
 "o( T#NOGROUP | " fmtDSRCID ")"


#define DAQ_MENU_LIST                                                 \
 MENU_IT(DSTREAM,   fmtDSTREAM,   fmtNONE, _B, "set dstream conf")    \
 MENU_IT(qDSTREAM,  fmtqDSTREAM,  fmtSTR,  _B, "get dstream conf")    \
 MENU_IT(qbDSTREAM, fmtqbDSTREAM, fmtNONE, _B, "read dstream buffer") \
 MENU_IT(DSOURCE,   fmtDSOURCE,   fmtNONE, _B, "set dsource conf")    \
 MENU_IT(qDSOURCE,  fmtqDSOURCE,  fmtSTR,  _B, "get dsource conf")    \
 MENU_IT(STRIG,     fmtNONE,      fmtNONE, _B, "software trigger") 


#define DAQ_LISTS	\
 DANCELIST(DSCOPE)	\
 DANCELIST(DSTATE)	\
 DANCELIST(DFMT)


/* command handlers */

void commexec_DSTREAM(void);
void commexec_qDSTREAM(void);
void commexec_qbDSTREAM(void);
void commexec_DSOURCE(void);
void commexec_qDSOURCE(void);
void commexec_STRIG(void);


#endif /* DAQ_H_INCLUDED */
