#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include "libdance.h"

#include "hwinfo.h"
#include "libefspi.h"
#include "libejtag.h"

/* instrument identification system */

#define HWINFO_MAX_COUNT 32
static hwinfo_t g_hwinfo[HWINFO_MAX_COUNT];
static size_t g_hwinfo_count = 0;

/* exported */

static int add_board_info(uint8_t x, const char *name)
{
  if (g_hwinfo_count == HWINFO_MAX_COUNT) return -1;
  memcpy(g_hwinfo[g_hwinfo_count].name, name, strlen(name));
  g_hwinfo[g_hwinfo_count].board_model = x >> 2;
  g_hwinfo[g_hwinfo_count].pcb_version = x & 3;
  ++g_hwinfo_count;

  return 0;
}

size_t answer_jtag_chain_id(unsigned int prompted) {
   size_t nitem = 0;
   unsigned int uidx = 0;
   ejtag_handle_t ejtag_h;
   ejtag_desc_t desc_jtag;
   const ejtag_dev_info_t* dev_jtag;
   const uint32_t link_type = ejtag_probe_link();

   if (ejtag_init_lib() == 0) {
      ejtag_init_desc(&desc_jtag);
      desc_jtag.flags |= link_type;
      if (ejtag_open(&ejtag_h, &desc_jtag) ==  0) {
         if (ejtag_detect_chain(&ejtag_h, &dev_jtag, &nitem) == 0) {
            if(!nitem)
               answerf("%sN/A\n", prompted? "   ":"");
            for(uidx = 0; uidx < nitem; uidx++) {
               const uint32_t id_code = dev_jtag[uidx].id_code;
               const char* const name = ejtag_id_code_to_name(&ejtag_h, id_code);
               answerf("%s[0x%08x] : %s\n", prompted? "   ":"", id_code, name);
            }
         }
         ejtag_close(&ejtag_h);
      }
   }
   return nitem;
}

size_t get_fpga_identification(const char* pci_ident
                              , int pci_bar
                              , int pci_off
                              , uint8_t* dest
                              , size_t bufsize) {
   efspi_handle_t efspi;
   efspi_desc_t desc;
   size_t size = 0;

   if (efspi_init_lib() == EFSPI_ERR_SUCCESS) {
      desc.pci_id = pci_ident;
      desc.pci_bar = pci_bar;
      desc.pci_off = pci_off;
      desc.write_fn = NULL;
      desc.erase_fn = NULL;

      if (desc.pci_id != NULL) {
         if (efspi_open(&efspi, &desc) == EFSPI_ERR_SUCCESS) {
            size = efspi_extract_header(&efspi, dest, bufsize);
            efspi_close(&efspi);
         }
      }
   }
   return size;
}

int get_board_ident() {
   int nboard = 0;
   uint8_t uval;
   int idx = 0;

   for(idx = 0; idx < gdance_extdevdesc->n_entries; idx++) {
      danceextdev_t* extdev = DANCE_EXTDEV_PT(idx);
      dancebus_t*    pbus = extdev->buspt;
      i2c_handle_t*  i2c_h = &(pbus->info.i2chandle);

      if (pbus->bustype == _EI2C_BUS
         && (extdev->devflags & _IDENT) == _IDENT) {
         // Get I2C handle
         if (i2c_basic_transaction(i2c_h, extdev->address, 0, 1, &uval, &uval) == 0) {
            add_board_info(uval, extdev->devname);
            nboard++;
         } else {
            LOG_ERROR("Identification device address [%s:%s:0x%02x] not found\n"
                     , pbus->bname, extdev->devname, extdev->address);
         }
      }
   }

   return nboard;
}

// check_eeprom_prog
// Desc : Check if the EEPROM for board ident is programed
int check_eeprom_prog() {
#define BUFF_SIZE 256
   int nprog = 0;
   int idx = 0;
   char rdbuff[BUFF_SIZE];
   for(idx = 0; idx < gdance_extdevdesc->n_entries; idx++) {
      danceextdev_t* dev = DANCE_EXTDEV_PT(idx);
      if (dev->devtype == EXTDEV_PCA9500
         && (dev->devflags & _IDENT) == _IDENT) {
         if (read_identprog(idx, rdbuff, BUFF_SIZE) > 0) {
            // TODO improve : check eeprom data
            nprog++;
         }
      }
   }
   return nprog;
}

int hwinfo_init(void) {
__attribute__((unused))
   int nboard_id = -1;

   g_hwinfo_count = 0;

   nboard_id = get_board_ident();

#ifdef CHECK_IDENT_EEPROM
   if (nboard_id > 0) {
      // Check EEPROM
      if (check_eeprom_prog() == nboard_id)
         LOG_INFO("Instrument EEPROM programmed\n");
      else {
         LOG_ERROR("Instrument EEPROM not programmed please contact support\n");
         return -1;
      }
   }
#endif

   return 0;
}


int hwinfo_get(const hwinfo_t** info, size_t* count)
{
  *info = g_hwinfo;
  *count = g_hwinfo_count;
  return 0;
}
