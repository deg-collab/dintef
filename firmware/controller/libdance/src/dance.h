/*---------------------------------------------------------------------------
 *  dance.h
 *
 *  To be included only by dance application specific files
 *  Files in libdance should always use exclusively libdance.h
 *
 */
#ifndef _DANCE_H_
#define _DANCE_H_


// Any dance header that defines data types potentially required by the application
//  should go here, before including dance_defs.h
// However the headers below should not use __INCLUDE_DANCE_DECLARATIONS

#include <memory.h>
#include <lnklist.h>
#include <dthread.h>


#include <libdance.h>

#endif // _DANCE_H_

