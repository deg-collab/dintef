#ifndef DEXT_H_INCLUDED
#define DEXT_H_INCLUDED


#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>


struct TCCState;


typedef struct dext_sym{
   const char* id;
   const char* decl;
   void*       addr;
} dext_sym_t;

#define DEXT_INVALID_SYM { .id = NULL, }


typedef struct dext_handle
{
#define DEXT_FLAG_FREE (1 << 0)
#define DEXT_FLAG_CODE_DIRTY (1 << 1)
#define DEXT_FLAG_LDSO_DIRTY (1 << 2)
#define DEXT_FLAG_INIT (1 << 3)
#define DEXT_FLAG_FINI (1 << 4)
#define DEXT_FLAG_ERR (1 << 5)
#define DEXT_FLAG_RUN (1 << 6)
  uint32_t flags;

  /* path of .c or .so */
  char* path;

  /* point to path */
  char* id;
  size_t id_len;

  /* source code */
  char* code;
  size_t code_len;

  /* tcc compilation state */
  struct TCCState* tcc;

  /* shared object */
  void* ldso;

  /* entry points */
  int (*init)(void*);
  void (*fini)(void*);
  void (*main)(void*);

} dext_handle_t;


int dext_init(void);
void dext_fini(void);


/* application macros */
#ifndef DEXTFUNC_LIST
   #define DEXT_USED 0
   #define DEXTFUNC_LIST
#else
   #define DEXT_USED 1
#endif


#define DEXTFUNC(__r, __fn, __arg1, ...) \
   extern __r __fn( __arg1, ##__VA_ARGS__ );
DEXTFUNC_LIST
#undef DEXTFUNC


#ifdef __INCLUDE_DANCE_DECLARATIONS

#define DEXTFUNC_DECLSTR(__r, __fn, ...) #__r " " #__fn "(" #__VA_ARGS__ ");"

#define DEXTFUNC(__r, __fn, __arg1, ...)  {  \
   .id = #__fn,   \
   .addr = __fn,	\
   .decl = DEXTFUNC_DECLSTR(__r, __fn, __arg1, ##__VA_ARGS__)  \
}, \

dext_sym_t g_app_dext_syms[] = {
   DEXTFUNC_LIST
   DEXT_INVALID_SYM
};
#undef DEXTFUNC
#undef DEXTFUNC_DECLSTR

#else // ! __INCLUDE_DANCE_DECLARATIONS

extern dext_sym_t g_app_dext_syms[];

#endif /* __INCLUDE_DANCE_DECLARATIONS */


/* menu */

#define fmtDEXTID fmtLABEL

/* DEXT <DEXTID> LINE <LINE> */
/* DEXT <DEXTID> CLEAR */
/* DEXT <DEXTID> COMP */
/* DEXT <DEXTID> DEL */
/* DEXT <DEXTID> EXEC [ FREQ <FREQ> ] [ ARGS ] */
#define fmtDEXT				\
 fmtDEXTID				\
 "{"					\
 "   (T#LINE " fmtSTR ")"		\
 " | (T#CLEAR)"				\
 " | (T#COMP)"				\
 " | (T#SAVE)"				\
 " | (T#DEL)"				\
 " | (T#EXEC o(T#FREQ uI_U#FREQ) onF)"	\
 " | (T#STOP)"				\
 "}"

/* ?DEXT [ <DEXTID> ] */
#define fmtqDEXT "o" fmtDEXTID

/* *DEXT <DEXTID> {C | SO} */
#define fmtbDEXT fmtDEXTID " (T#C | T#SO)"

#define DEXT_MENU_LIST						\
 MENU_IT(DEXT, fmtDEXT, fmtNONE, _B, "set dext conf")		\
 MENU_IT(qDEXT, fmtqDEXT, fmtSTR, _B, "get dext conf")		\
 MENU_IT(bDEXT, fmtbDEXT, fmtNONE, _B, "set dext contents")	\

#define DEXT_LISTS


#endif /* DEXT_H_INCLUDED */
