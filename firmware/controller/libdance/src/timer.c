#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "timer.h"
#include "lnklist.h"
#include "errors.h"


static int get_timeval(struct timeval* tv)
{
  /* use monotonic clock to retrieve time */
  /* must link with -lrt */

  /* getimeofday depends on system wide settings */
  /* (ie. ntpd ...) and may report past times, for */
  /* instance during time clock changes */

  struct timespec tp;
  const int err = clock_gettime(CLOCK_MONOTONIC, &tp);
  if (err) return err;
  tv->tv_sec = (unsigned long)tp.tv_sec;
  tv->tv_usec = tp.tv_nsec / 1000;
  return 0;
}

/* exported */

timerlist_t* timerlist_init(timerlist_t* tl)
{
  uint32_t flags = 0;

  if (tl == NULL)
  {
    tl = malloc(sizeof(timerlist_t));
    if (tl == NULL) return NULL;
    flags |= TIMERFLAG_IS_MALLOC;
  }

  lnklist_init(&tl->li, NULL);

  tl->flags = flags;

  return tl;
}

static lnklist_for_t free_timeritem(lnklist_item_t* list_it, void* p)
{
  timeritem_t* const timer_it = p;
  free(timer_it);
  return LNKLIST_CONT;
}

void timerlist_fini(timerlist_t* tl)
{
  lnklist_fini(&tl->li, free_timeritem, NULL);
  if (tl->flags & TIMERFLAG_IS_MALLOC) free(tl);
}

timeritem_t* timerlist_add_timer(timerlist_t* tl, timeritem_t* timer_it)
{
  /* add timer in the list. list is sorted by timer ms */
  /* assume timer_item fields are initialized, only rel_ms computed here */

  lnklist_item_t* list_it;
  timeritem_t* next_ti;
  unsigned int ms_sum;

  /* find the place to insert before */

  ms_sum = 0;
  for (list_it = tl->li.head; list_it != NULL; list_it = list_it->next)
  {
    timeritem_t* timer_cur = (timeritem_t*)list_it->itmdata;
    if ((ms_sum + timer_cur->rel_ms) > timer_it->abs_ms) break ;
    ms_sum += timer_cur->rel_ms;
  }

  /* update list reference time if added at head */

  if (list_it == tl->li.head) get_timeval(&tl->ref_tv);

  /* insert timer before list_it */

  if (list_it == NULL)
  {
    /* last timer */

    timer_it->list_it = lnklist_add_tail(&tl->li, timer_it);
    if (timer_it->list_it == NULL) return NULL;
    timer_it->rel_ms = timer_it->abs_ms - ms_sum;
  }
  else
  {
    timer_it->list_it = lnklist_add_before(&tl->li, list_it, timer_it);
    if (timer_it->list_it == NULL) return NULL;

    /* compute time relative to previous */

    timer_it->rel_ms = timer_it->abs_ms - ms_sum;

    /* recompute next relative time */

    next_ti = list_it->itmdata;
    next_ti->rel_ms = (ms_sum + next_ti->rel_ms) - timer_it->abs_ms;
  }

  return timer_it;
}

timeritem_t* timerlist_add
(timerlist_t* tl, unsigned int ms, timerfn_t fn, void* data, uint32_t flags)
{
  timeritem_t* it;

  it = malloc(sizeof(timeritem_t));
  if (it == NULL) return NULL;

  it->flags = flags;
  it->abs_ms = ms;
  it->exec_fn = fn;
  it->exec_data = data;

  if (timerlist_add_timer(tl, it) == NULL)
  {
    free(it);
    it = NULL;
  }

  return it;
}

timeritem_t* timerlist_add_periodic
(timerlist_t* tl, unsigned int ms, timerfn_t fn, void* data)
{
  return timerlist_add(tl, ms, fn, data, TIMERFLAG_PERIODIC);
}

timeritem_t* timerlist_add_oneshot
(timerlist_t* tl, unsigned int ms, timerfn_t fn, void* data)
{
  return timerlist_add(tl, ms, fn, data, TIMERFLAG_ONESHOT);
}

static timeritem_t* add_periodic_or_oneshot_timer
(timerlist_t* tl, timeritem_t* it, uint32_t type)
{
  it->abs_ms = it->rel_ms;

  it->flags &= ~(TIMERFLAG_ONESHOT | TIMERFLAG_PERIODIC);
  it->flags |= type;

  return timerlist_add_timer(tl, it);
}

timeritem_t* timerlist_add_periodic_timer
(timerlist_t* tl, timeritem_t* it)
{
  return add_periodic_or_oneshot_timer(tl, it, TIMERFLAG_PERIODIC);
}

timeritem_t* timerlist_add_oneshot_timer
(timerlist_t* tl, timeritem_t* it)
{
  return add_periodic_or_oneshot_timer(tl, it, TIMERFLAG_ONESHOT);
}

timeritem_t* timerlist_restart_oneshot
(timerlist_t* tl, timeritem_t* it)
{
  /* update the next timer, if exist */

  if (it->list_it->next != NULL)
  {
    timeritem_t* const next_timer = (timeritem_t*)it->list_it->next->itmdata;
    next_timer->rel_ms += it->rel_ms;
  }

  /* unlink the timer */

  lnklist_unlink_item(it->list_it);

  /* add back to list */

  return timerlist_add_timer(tl, it);
}

void timerlist_delete_timer(timeritem_t* it)
{
  /* mark the timer for deletion */
  /* actually delete at schedule time */

  /* TODO: for correctness should be atomic_or and relateds */
  it->flags |= TIMERFLAG_DELETED;
}

static inline unsigned long tv_to_ms(const struct timeval* tv)
{
  return tv->tv_sec * 1000 + tv->tv_usec / 1000;
}

static inline void ms_to_tv(struct timeval* tv, unsigned int ms)
{
  tv->tv_sec = ms / 1000;
  tv->tv_usec = (ms % 1000) * 1000;
}

static struct timeval* timerlist_get_time(timerlist_t* tl, timeritem_t* ti, struct timeval* now_tv)
{
  unsigned int rel_ms;
  struct timeval dif_tv;
  struct timeval rel_tv;

  /* compare ref + relative to now */

  ms_to_tv(&rel_tv, ti->rel_ms);
  timeradd(&tl->ref_tv, &rel_tv, &tl->newref_tv);
  if (timercmp(now_tv, &tl->newref_tv, >))
    return NULL;   // timer elapsed

  else
  {
    /* update ti->rel_ms */
    /* new_rel = rel - (now - ref) */

    timersub(now_tv, &tl->ref_tv, &dif_tv);
    rel_ms = ti->rel_ms - (unsigned int)tv_to_ms(&dif_tv);
  }

  ti->rel_ms = rel_ms;
  ms_to_tv(&tl->stor_tv, rel_ms);

  return &tl->stor_tv;
}

struct timeval* timerlist_get_next(timerlist_t* tl)
{
  timeritem_t*    ti;
  struct timeval  now_tv;
  struct timeval* next_rel_tv;

  /* TODO: lnklist head accessor */
//LOG_TRACE();
  if (tl->li.head == NULL) return NULL;
  ti = tl->li.head->itmdata;

//LOG_TRACE();
  get_timeval(&now_tv);
  next_rel_tv = timerlist_get_time(tl, ti, &now_tv);
  if (next_rel_tv == NULL)
  {
    /* timer elapsed. set tv_add for next update. */
    next_rel_tv = &tl->stor_tv;
    ti->rel_ms = 0;
    ms_to_tv(next_rel_tv, 0);
  }
  return next_rel_tv;
}

struct timeval* timerlist_exec_expired(timerlist_t* tl)
{
  timeritem_t*    ti;
  struct timeval  now_tv;
  struct timeval* next_rel_tv;

  get_timeval(&now_tv);
  while(tl->li.head != NULL)
  {
    ti = tl->li.head->itmdata;

    next_rel_tv = timerlist_get_time(tl, ti, &now_tv);
    if (next_rel_tv != NULL)
      return next_rel_tv;
    else
      timerlist_exec_next(tl);
  }
  return NULL;
}


void timerlist_exec_next(timerlist_t* tl)
{
  timeritem_t* it;

  /* TODO: add head accessor to lnklist */

  if (tl->li.head == NULL) return ;

  it = tl->li.head->itmdata;
  if ((it->flags & TIMERFLAG_DELETED) == 0) it->exec_fn(it->exec_data);

  /* update reference time here */

  tl->ref_tv = tl->newref_tv;

  /* delete from list, and free if oneshot or deleted */

  lnklist_free_item(tl->li.head);

  if (it->flags & (TIMERFLAG_ONESHOT | TIMERFLAG_DELETED))
  {
    /* delete timer */
    free(it);
  }
  else if (it->flags & TIMERFLAG_PERIODIC)
  {
    timerlist_add_timer(tl, it);
  }
}


#if 0 /* unit testing */

#include <stdio.h>
#include <errno.h>
#include <sys/select.h>

static void on_timer(void* user_data)
{
  const char* const s = user_data;
  struct timeval tm;

  get_timeval(&tm);

  printf("%s(%s, %lu.%lu)\n", __FUNCTION__, s, tm.tv_sec, tm.tv_usec);
}

int main(int ac, char** av)
{
  int err;
  timerlist_t tl;
  struct timeval* tv;
  lnklist_item_t* it;

  timerlist_init(&tl);
  timerlist_add_oneshot(&tl, 4000, on_timer, "oneshot_3_4000");
  timerlist_add_oneshot(&tl, 3000, on_timer, "oneshot_2_3000");
  timerlist_add_periodic(&tl, 1000, on_timer, "periodic_0_1000");
  timerlist_add_oneshot(&tl, 2000, on_timer, "oneshot_1_2000");
  timerlist_add_oneshot(&tl, 1000, on_timer, "oneshot_0_1000");
  timerlist_add_periodic(&tl, 2000, on_timer, "periodic_1_2000");

  for (it = tl.li.head; it; it = it->next)
  {
    timeritem_t* const ti = it->itmdata;
    printf("%s %u\n", (const char*)ti->exec_data, ti->rel_ms);
  }

  while (1)
  {
    tv = timerlist_get_next(&tl);

    errno = 0;
    err = select(0, NULL, NULL, NULL, tv);
    if (err == 0)
    {
      if ((errno != EAGAIN) && (errno != EWOULDBLOCK))
      {
        /* next timeout is ready */
        timerlist_exec_next(&tl);
      }
    }
  }

  timerlist_fini(&tl);

  return 0;
}

#endif /* unit testing */
