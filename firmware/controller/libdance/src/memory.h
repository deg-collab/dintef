/*---------------------------------------------------------------------------
 *  memory.h
 *
 *  To be included directly only by libdance source files
 *  Application files should only include either dance.h or dancemain.h
 *
 */
#ifndef _MEMORY_H_INCLUDED_
#define _MEMORY_H_INCLUDED_

#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <libebuf.h>

#include "errors.h"
#include "derr.h"
#include "lnklist.h"

#define MEM_DEFAULT           0
#define MEM_PERSISTENT       (1 << 0)  // used to flag an mblock or dblock as persistent
#define MEM_TRANFERRED       (1 << 1)  // used to instruct to consider a user provided
                                       //      memory buffer as internally allocated
#define MEM_DATA_ALLOC       (1 << 2)  // (internal use)
#define MEM_HANDLE_ALLOC     (1 << 3)  // (internal use)
#define MEM_MBLOCK_ALLOC     (1 << 4)  // (internal use)
#define MEM_DBLOCK_ALLOC     (1 << 5)  // (internal use)

#define MEM_TYPE_DEF_MALLOC    0       // used to select a standard system memory buffer (malloc)
#define MEM_TYPE_EBUF_PHYSMEM (1 << 8) // used to select a ebuf buffer of physical memory
#define MEM_TYPE_EBUF_MALLOC  (1 << 9) // used to select a ebuf buffer of standard system memory

#define MEM_TYPE             (MEM_TYPE_DEF_MALLOC | MEM_TYPE_EBUF_PHYSMEM | MEM_TYPE_EBUF_MALLOC)


#define DESTROY_DEFAULT     0
#define DESTROY_FORCE      (1 << 0)
#define DESTROY_DATA_ONLY  (1 << 1)
#define DESTROY_BUF_ONLY   (1 << 2)


typedef struct
{
   uint32_t  mbflags;
   size_t    mbsize;       // size of used memory in bytes
   void*     mbptr;        // pointer to used memory. Can be NULL
   void*     mem_handle;   // handle to lower level memory buffer
   size_t    alloc_size;   // size of allocated memory in bytes
   void*     alloc_ptr;    // pointer to allocated memory
} mblock_t;


typedef struct
{
   uint32_t       dbflags;
   mblock_t*      mblock;   // pointer to the actual memory block, cannot be NULL
   void*          first;    // points to beginning of valid data
   void*          last;     // points to end of valid data
   void*          end;      // points to end of allocated buffer
   lnklist_item_t item;     // to ease datablock linking
} dblock_t;


#ifdef __cplusplus
extern "C" {
#endif

mblock_t* mblock_create(mblock_t* mblock, void *data, size_t size, uint32_t flags);
mblock_t* mblock_release(mblock_t* mblock, int mode);
mblock_t* mblock_acquire(mblock_t* mblock);
mblock_t* mblock_resize(mblock_t* mblock, size_t newsize);
static inline
void*     mblock_ptr(mblock_t* mblock) {return mblock->mbptr;}
static inline
size_t    mblock_size(mblock_t* mblock) {return mblock->mbsize;}


dblock_t* dblock_create(dblock_t* dblk, mblock_t* mblock, ssize_t size, uint32_t flags);
dblock_t* dblock_release(dblock_t* dblk, int mode);
dblock_t* dblock_acquire(dblock_t* dblk);
size_t    dblock_resize(dblock_t* dblk, size_t newsize);
size_t    dblock_append(dblock_t* dblk, void* ptr, size_t size);
size_t    dblock_seek(dblock_t* dblk, size_t size);
static inline
void*     dblock_beg(dblock_t* dblk) {return dblk->first;}
static inline
void*     dblock_end(dblock_t* dblk) {return dblk->last;}
static inline
size_t    dblock_size(dblock_t* dblk) {return dblk->end - dblk->first;}
static inline
size_t    dblock_used(dblock_t* dblk) {return dblk->last - dblk->first;}
size_t    dblock_setused(dblock_t* dblk, size_t newused);
static inline
size_t    dblock_available(dblock_t* dblk) {return dblk->end - dblk->last;}
static inline
dblock_t* dblock_next(dblock_t* dblk) {return (dblk->item.next? dblk->item.next->itmdata:NULL);}
static inline
void      dblock_unlink(dblock_t* dblk) {lnklist_unlink_item(&dblk->item);}


typedef lnklist_t cbuflist_t;


cbuflist_t* combuflist_create(cbuflist_t* cbuflist, dblock_t* dblk);
void        combuflist_free(cbuflist_t* cbuflist);
danceerr_t  combuflist_append(cbuflist_t* cbuflist, dblock_t* dblk);
danceerr_t  combuflist_prepend(cbuflist_t* cbuflist, dblock_t* dblk);
size_t      combuflist_size(cbuflist_t* cbuflist);

/*
static inline
void        combuflist_set_ready(cbuflist_t* cbuflist) {*((void* volatile *)&cbuflist->lstdata = cbuflist;}
*/
static inline
int         combuflist_is_ready(cbuflist_t* cbuflist) {return cbuflist->lstdata != NULL;}


// debugging utilities
void  __rawdata_dump(void* data, size_t nbytes, int width);
void  __dblock_dump(dblock_t* dblk);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // _MEMORY_H_INCLUDED_
