#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include "libdance.h"

#include "i2cdevices.h"

/*
 * i2c_txd
 * Send data via I2C using cycle write (wait between write)
 */
 int i2c_txd_cycle( i2c_phandle_t i2c_h
                  , uint16_t addr
                  , uint16_t offset
                  , size_t nbytes
                  , const uint8_t* wr_data
                  , size_t block_size
                  , uint32_t delay_ms ) {
   int          iret = -1;
   unsigned int u_cpt1 = 0;
   unsigned int usize;
   uint8_t      curbuf[block_size + 1]; // First byte is offset

   for ( ; u_cpt1 < nbytes;  u_cpt1 += usize)
   {
      if ( (u_cpt1 + block_size) < nbytes )
         usize = block_size;
      else
         usize = nbytes - u_cpt1;

      memcpy(curbuf + 1, wr_data + u_cpt1, block_size);
      curbuf[0] = offset + u_cpt1;
      iret = i2c_basic_transaction( i2c_h, addr, block_size+1, 0, curbuf, curbuf );
      usleep( delay_ms * 1000 );
      if ( iret < 0 )
         return iret;
   }
   return iret;
}


/////////////////////////////////////////////
// PCA9500 access functions

#define PCA9500_MEM_ADDR_OFFSET 0x30
#define PCA9500_MEM_SIZE    256
#define PCA9500_PAGE_SIZE     4
#define PCA9500_WRITE_MS     10 // Need 10ms for writing to EEPROM

int PCA9500_mem_write(uint16_t devid, size_t nbytes, const uint8_t* data) {
   danceextdev_t* dev;
   i2c_handle_t* i2c_h;
   uint16_t      addr;

   if (!IS_VALID_EXTDEV_ID(devid)) {
      LOG_ERROR("Invalid PCA9500 device id: %d\n", devid);
      return -1;
   }
   dev = DANCE_EXTDEV_PT(devid);
   i2c_h = &dev->buspt->info.i2chandle;
   addr = dev->address + PCA9500_MEM_ADDR_OFFSET;
   return i2c_txd_cycle(i2c_h, addr, 0, nbytes, data, PCA9500_PAGE_SIZE, PCA9500_WRITE_MS);
}

int get_headersize(i2c_phandle_t i2c_h, uint16_t addr) {
   int size = -1;
   uint8_t rdbuf[HEADER_SIZE];
   uint8_t wrbuf[2];
   uint16_t curhead;
   // Send command reading offset to 0x00
   memset(wrbuf, 0, HEADER_SIZE);
   // Init transaction for EEPROM read
   memset(rdbuf, 0, HEADER_SIZE);

   if( i2c_basic_transaction(i2c_h, addr, 1, HEADER_SIZE, wrbuf, rdbuf) >= 0 ) {
      curhead = rdbuf[0] << 8;
      curhead |= rdbuf[1];
      if (curhead == HEADER_MAGICID)  {
         size = rdbuf[2] << 8;
         size = rdbuf[3];
      }
   } else {
      LOG_ERROR("Error while reading EEPROM header through I2C\n");
      return -2;
   }

   return size;
}

int read_identprog(uint16_t devid, char * rd_out, int nsize) {
   int nread = 0;
   danceextdev_t* dev;
   i2c_handle_t* i2c_h;
   uint16_t      addr;

   if (!IS_VALID_EXTDEV_ID(devid)) {
      LOG_ERROR("Invalid PCA9500 device id: %d\n", devid);
   } else {
      dev = DANCE_EXTDEV_PT(devid);
      i2c_h = &dev->buspt->info.i2chandle;
      addr = dev->address + PCA9500_MEM_ADDR_OFFSET;
      nread = get_headersize(i2c_h, addr);
      LOG_INFO("EEPROM info size : %d\n", nread);

      if (nread > 0 && nread < nsize) {
         uint8_t rdbuf[PCA9500_MEM_SIZE];
         uint8_t wrbuf = HEADER_SIZE;
         // Init transaction for EEPROM read
         memset(rdbuf, 0, PCA9500_MEM_SIZE);
         if (i2c_basic_transaction(i2c_h, addr, 1, nread, &wrbuf, rdbuf) >= 0) {
            memcpy(rd_out, rdbuf, nread);
            rd_out[nread] = '\0';
         } else {
            LOG_ERROR("Error while reading EEPROM header through I2C\n");
            return -1;
         }
      } else {
         LOG_ERROR("Error buffer size too small or no data in EEPROM\n");
         return -1;
      }
   }

   return nread;
}

/////////////////////////////////////////////

static int check_identpass(const char* passwd) {
   int iret = -1;

   iret = -1;
   if(passwd != NULL) {
      if(strcmp(EEPROM_KEYPASS, passwd) == 0)
         iret = 0;
   }

   return iret;
}
// PROG <BUS_ID>:<addr>:<offset> <data string> <password>
// "L S oS"
//
void commexec__IDENTPROG(void) {
   i2c_handle_t* i2c_h = NULL;
   const char*   lb_param = I_LABEL();
   const char*   s_data = I_STRING();
   const char*   s_passwd = I_STRING();

   uint16_t sl_addr, addr_offset;

   // Parsing argument
   if(isalpha(*lb_param)
      && strchr(lb_param, ':') == NULL) {
         int idx = 0;
         for(idx = 0; idx < gdance_extdevdesc->n_entries; idx++) {
            danceextdev_t* extdev = DANCE_EXTDEV_PT(idx);
            if (strcmp(lb_param, extdev->devname) == 0){
               if (extdev->devtype == EXTDEV_PCA9500
                  && (extdev->devflags & _IDENT) == _IDENT) {
                  i2c_h = &(extdev->buspt)->info.i2chandle;
                  sl_addr = extdev->address + PCA9500_MEM_ADDR_OFFSET;
                  addr_offset = 0x00;
                  break;
               } else {
                  errorf("Invalid PCA9500 identification device name: %s\n", lb_param);
               }
            }
         }
   } else {
      i2c_h = parse_i2cbus_args(lb_param, &sl_addr, &addr_offset);
   }

   if (i2c_h != NULL) {
      unsigned int data_size = strlen(s_data);
      int err = 0;
      // Build the writing buffer
      if (data_size <= (PCA9500_MEM_SIZE - HEADER_SIZE) ) {
         uint8_t data_buff[PCA9500_MEM_SIZE];

         if (get_headersize(i2c_h, sl_addr) >= 0) {
            // check password if memory is not empty
            err = check_identpass(s_passwd);
         }

         if (err == 0) {
            // Create header
            data_buff[0] = HEADER_MAGICID >> 8;
            data_buff[1] = HEADER_MAGICID & 0xFF;
            data_buff[2] = data_size >> 8;
            data_buff[3] = data_size & 0xFF;
            memcpy(data_buff + HEADER_SIZE, s_data, data_size);
            data_buff[data_size + HEADER_SIZE] = '\0';

            err = i2c_txd_cycle(i2c_h
                                 , sl_addr
                                 , addr_offset
                                 , data_size + HEADER_SIZE
                                 , data_buff
                                 , PCA9500_PAGE_SIZE
                                 , PCA9500_WRITE_MS );

            if(err < 0)
               errorf("Error while sending data");
            else {
               LOG_INFO("Writing EEPROM done (%s)\n", data_buff + HEADER_SIZE);
               answerf("Done");
            }
         } else {
            errorf("EEPROM not empty, incorrect password for update\n");
         }

      } else {
         errorf("Not enough space in memory (%d Bytes available)\n", PCA9500_MEM_SIZE);
      }
   } else {
      errorf("No device found (%s)\n", lb_param);
   }
}
