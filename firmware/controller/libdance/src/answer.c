/*---------------------------------------------------------------------------
 *
 * Implement special printf() functions, to deal with command and error answer
 *
 *---------------------------------------------------------------------------
 * $Revision: 2056 $
 *
 *
 * $Log: $
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <printf.h>

#include "memory.h"
#include "comm.h"
#include "errors.h"
#include "log.h"


#define MINPREFIX_SIZE    60
#define MINANSWER_SIZE   200
#define EXTANSWER_SIZE  8000

#define LEND_CHAR             '\n'
#define LEND                  "\n"
#define MULTILINE_BEGIN       "$"LEND
#define SPACE_MULTILINE_BEGIN " $"LEND
#define MULTILINE_END         "$"LEND
#define LEND_MULTILINE_END    LEND"$"LEND


void show_reply(void) {
   command_t* comm = get_current_command();

   LOG_COMM("main_prefix: "); __dblock_dump(comm->main_prefix);
   LOG_COMM("main_answ: "); __dblock_dump(comm->main_answ);
}

#define MAX_VALIST_ARGS 32
size_t consume_va_args(va_list* ap, const char* fmt)
{
   int types[MAX_VALIST_ARGS];
   size_t i;
   size_t n;

   n = parse_printf_format(fmt, MAX_VALIST_ARGS, types);
   if (n > MAX_VALIST_ARGS) return (size_t)-1;

   for (i = 0; i != n; ++i)
   {
      const int flag = types[i] & PA_FLAG_MASK;

      if (flag == PA_FLAG_PTR) goto pointer_case;

      switch (types[i] & ~PA_FLAG_MASK)
      {
         /* char, short promoted to int */
         case PA_CHAR:
         case PA_INT:
         {
            if (flag == PA_FLAG_LONG)           va_arg(*ap, long int);
            else if (flag == PA_FLAG_LONG_LONG) va_arg(*ap, long long int);
            else                                va_arg(*ap, int);
            break;
         }

         /* float promoted to double */
         case PA_FLOAT:
         case PA_DOUBLE:
         {
            if (flag == PA_FLAG_LONG) va_arg(*ap, long double);
            else                      va_arg(*ap, double);
            break ;
         }

         default:
         {
pointer_case:
            va_arg(*ap, void*);
         }
      }
   }

   return n;
}


void answer_check(command_t *comm) {
   char* first_ptr = dblock_beg(comm->main_answ);
   char* last_ptr = dblock_end(comm->main_answ) - 1;
   char* ptr;

LOG_TRACE();
   // look for and process EOL, manage multiline answers
   for (ptr = first_ptr; ptr <= last_ptr; ptr++) {
      if (*ptr == LEND_CHAR) {
         if (ptr < last_ptr) {                // Multiline answer
            dblock_append(comm->main_prefix, SPACE_MULTILINE_BEGIN, sizeof(SPACE_MULTILINE_BEGIN) - 1);

            if (*last_ptr != LEND_CHAR)
               dblock_append(comm->main_answ, LEND_MULTILINE_END, sizeof(LEND_MULTILINE_END) - 1);
            else
               dblock_append(comm->main_answ, MULTILINE_END, sizeof(MULTILINE_END) - 1);
            return;

         } else                               // EOL terminated single line answer
            goto on_single_line;
      }
   }
   // Not EOL terminated single line answer
   dblock_append(comm->main_answ, LEND, sizeof(LEND) - 1);  // add missing EOL

 on_single_line:
   if (!isspace(*first_ptr))
      dblock_append(comm->main_prefix, " ", 1);
//show_reply();
}


int vanswerf(const char *fmt, va_list ap) {
   dblock_t  *ansbuff = get_current_command()->main_answ;
   int        nchar;
   va_list    ap_cpy;

   if (ansbuff == NULL) {
      SYSTEM_ERROR();
      return(-1);
   }

   va_copy(ap_cpy, ap);  // store a copy of the initial va_list in ap_cpy

   while (1) { // Try to print in the allocated space
      size_t available = dblock_available(ansbuff) - sizeof(LEND_MULTILINE_END);

      nchar = vsnprintf(dblock_end(ansbuff), available, fmt, ap);

      if (nchar >= 0 && nchar < available) {
         dblock_setused(ansbuff, dblock_used(ansbuff) + nchar);
         goto on_exit;

      } else {
         size_t extsize = mblock_size(ansbuff->mblock) + EXTANSWER_SIZE;

         if (dblock_resize(ansbuff, extsize) != extsize) {
            // error, do something clever
            nchar = -1;
            goto on_exit;
         }
      }
      va_copy(ap, ap_cpy);  // restore the initial va_list in ap
   }
on_exit:
   va_end(ap_cpy);
   return(nchar);
}

int answerf(const char *fmt, ...) {
   va_list    ap;
   int        nchar;

   va_start(ap, fmt);
   nchar = vanswerf(fmt, ap);
   va_end(ap);
   return(nchar);
}


void answer_error_reply(command_t *comm) {
   dblock_setused(comm->main_answ, 0);
   answerf(" %s %s\n", ERROR_ANSWER, comm->errormsg);

LOG_TRACE();
TODO ; // store error message for subsequent ?ERRMSG query
}

void answer_noerror_reply(command_t *comm) {
   dblock_setused(comm->main_answ, 0);
   answerf(" %s\n", NOERROR_ANSWER);
}


// initialise answer buffer and build prefix string
void answer_prepare(command_t *comm) {
LOG_TRACE();
   // Mark binary output as not used
   CLEAR_BINARY_OUTPUT(comm);

   // Prepare initial buffer for command answer
   if (dblock_resize(comm->main_answ, MINANSWER_SIZE) != MINANSWER_SIZE) {
      // error, do something clever
      SYSTEM_ERROR();
      return;
   }
LOG_TRACE();
   // Prepare answer prefix
   if (dblock_resize(comm->main_prefix, MINPREFIX_SIZE) != MINPREFIX_SIZE) {
      // error, do something clever
      SYSTEM_ERROR();
      return;
   }

   if (comm->address_sz) {
      dblock_append(comm->main_prefix, comm->address, comm->address_sz);
      dblock_append(comm->main_prefix, ":", 1);

   } else if (comm->flags & CMDFLG_BROADCAST)
      dblock_append(comm->main_prefix, ":", 1);

   dblock_append(comm->main_prefix, comm->keyword, comm->keyword_sz);

LOG_TRACE();
//show_reply();
}


int _error_printf(command_t *comm , const char *fmt, va_list ap) {
   if (!(comm->flags & CMDFLG_ERROR)) {
      comm->flags &= ~CMDFLG_OK;
      comm->flags |= CMDFLG_ERROR;
      comm->errormsg_sz = 0;
   }
   comm->errormsg_sz += vsnprintf(comm->errormsg + comm->errormsg_sz, ERRMSG_MAXSIZE - comm->errormsg_sz, fmt, ap);

   return(comm->errormsg_sz);
}

int errorf(const char *fmt, ...) {
   va_list    ap;
   command_t *comm = get_current_command();
   int        nchar;

   va_start(ap, fmt);
   nchar = _error_printf(comm, fmt, ap);
   va_end(ap);

   return nchar;
}

int error_printf(command_t *comm, const char *fmt, ...) {
   va_list    ap;
   int        nchar;

   va_start(ap, fmt);
   nchar = _error_printf(comm, fmt, ap);
   va_end(ap);

   return nchar;
}

void answer_int8(uint8_t val8, int format) {
   switch (format) {
      case DFRMT_SIGNED:   answerf("%"PRId8, val8); break;
      case DFRMT_UNSIGNED: answerf("%"PRIu8, val8); break;
      default:         answerf("0x%02"PRIX8, val8); break;
   }
}

void answer_int16(uint16_t val16, int format) {
   switch (format) {
      case DFRMT_SIGNED:   answerf("%"PRId16, val16); break;
      case DFRMT_UNSIGNED: answerf("%"PRIu16, val16); break;
      default:         answerf("0x%04"PRIX16, val16); break;
   }
}

void answer_int32(uint32_t val32, int format) {
   switch (format) {
      case DFRMT_SIGNED:   answerf("%"PRId32, val32); break;
      case DFRMT_UNSIGNED: answerf("%"PRIu32, val32); break;
      default:         answerf("0x%08"PRIX32, val32); break;
   }
}
void answer_int64(uint64_t val64, int format) {
   switch (format) {
      case DFRMT_SIGNED:   answerf("%"PRId64, val64); break;
      case DFRMT_UNSIGNED: answerf("%"PRIu64, val64); break;
      default:         answerf("0x%016"PRIX64, val64); break;
   }
}
