#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/ip.h>
#include <net/if.h>
#include <linux/sockios.h>
#include <linux/ethtool.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

#include "libdance.h"

#include "eth.h"


static int eth_open(eth_handle_t* eth) {
   struct sockaddr_nl sa;

   eth->eflags = ETH_FLAG_VALID;

   strncpy(eth->ifr.ifr_name, eth->name, IFNAMSIZ);

   eth->ioctl_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
   if (eth->ioctl_fd == -1) goto on_error_0;

   eth->nl_fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
   if (eth->nl_fd == -1) goto on_error_1;

   memset(&sa, 0, sizeof(sa));
   sa.nl_family = AF_NETLINK;
   /* avoid addr already in use */
   /* sa.nl_pid = getpid(); */
   sa.nl_groups = RTMGRP_LINK;
   if (bind(eth->nl_fd, (struct sockaddr*)&sa, sizeof(sa))) goto on_error_2;

   return 0;

 on_error_2:
   close(eth->nl_fd);
 on_error_1:
   close(eth->ioctl_fd);
 on_error_0:
   eth->eflags = 0;
   return -1;
}


static void eth_close(eth_handle_t* eth) {
   close(eth->ioctl_fd);
   close(eth->nl_fd);
   eth->eflags = 0;
}


void eth_fix_half_duplex(eth_handle_t* eth) {

   /* NOTE MP 12Apr19: temporary move the eth config to system */
   return;

   /* fix an issue with bad router configuration */
   /* if the router did negotiate in half duplex */
   /* we force the interface to full, same speed */

   /* autoneg has to be disabled to apply the conf */

   const uint32_t m = ETH_FLAG_AUTONEG_ON | ETH_FLAG_DUPLEX_HALF;

   if ((eth->eflags & m) != m) return ;

   eth->eflags &= ~ETH_FLAG_AUTONEG_MASK;
   eth->eflags |= ETH_FLAG_AUTONEG_OFF;

   eth->eflags &= ~ETH_FLAG_DUPLEX_MASK;
   eth->eflags |= ETH_FLAG_DUPLEX_FULL;

   eth->eflags |= ETH_FLAG_FIX_APPLIED;

   eth_set_info(eth);
}


static int eth_get_info(eth_handle_t* eth) {
   struct ethtool_cmd cmd;

   /* interface hw address */

   eth->eflags &= ~ETH_FLAG_HWADDR;
   if (ioctl(eth->ioctl_fd, SIOCGIFHWADDR, &eth->ifr) != -1) {
      eth->eflags |= ETH_FLAG_HWADDR;
      memcpy(&eth->hw_addr, &eth->ifr.ifr_hwaddr, sizeof(eth->hw_addr));
   }

   /* interface eflags */

   if (ioctl(eth->ioctl_fd, SIOCGIFFLAGS, &eth->ifr) == -1) {
      /* not an error */
      eth->ifr.ifr_flags = 0;
   }

   eth->eflags &= ~ETH_FLAG_LINK_MASK;
   if (eth->ifr.ifr_flags & IFF_UP)
      eth->eflags |= ETH_FLAG_LINK_UP;
   else
      eth->eflags |= ETH_FLAG_LINK_DOWN;

   if (eth->ifr.ifr_flags & IFF_RUNNING)
      eth->eflags |= ETH_FLAG_LINK_CABLE;

   /* duplex, speed, autoneg */

   eth->ifr.ifr_data = (void *)&cmd;
   cmd.cmd = ETHTOOL_GSET;
   if (ioctl(eth->ioctl_fd, SIOCETHTOOL, &eth->ifr) == -1) {
      cmd.duplex = DUPLEX_UNKNOWN;
      cmd.speed = 0;
      cmd.speed_hi = 0;
   }

   eth->eflags &= ~ETH_FLAG_DUPLEX_MASK;
   switch (cmd.duplex) {
      case DUPLEX_HALF: eth->eflags |= ETH_FLAG_DUPLEX_HALF; break;
      case DUPLEX_FULL: eth->eflags |= ETH_FLAG_DUPLEX_FULL; break;
      default: break ;
   }

   eth->eflags &= ~ETH_FLAG_SPEED_MASK;
   switch (((uint32_t)cmd.speed_hi << 16) | ((uint32_t)cmd.speed)) {
      case 10: eth->eflags |= ETH_FLAG_SPEED_10; break;
      case 100: eth->eflags |= ETH_FLAG_SPEED_100; break;
      case 1000: eth->eflags |= ETH_FLAG_SPEED_1000; break;
      default: break ;
   }

   eth->eflags &= ~ETH_FLAG_AUTONEG_MASK;
   switch (cmd.autoneg) {
      case AUTONEG_ENABLE: eth->eflags |= ETH_FLAG_AUTONEG_ON; break;
      case AUTONEG_DISABLE: eth->eflags |= ETH_FLAG_AUTONEG_OFF; break;
      default: break ;
   }

   return 0;
}


static unsigned int eth_is_valid(const eth_handle_t* eth) {
   return (eth && (eth->eflags & ETH_FLAG_VALID));
}


/* exported */

#undef ETHERCOMM
#define ETHERCOMM(ethname, aflags) \
   .eflags = 0,     \
   .name = ethname,   \
   .appflags = aflags,

int eth_init(void) {
   static eth_handle_t eth_hndl = {
      COMMDEVICE_LIST
   };
   eth_handle_t* const eth = &eth_hndl;

   g_libdance.eth = eth;

   /* already init */
   if (eth_is_valid(eth)) return 0;

   if (eth_open(eth)) goto on_error_0;
   if (eth_get_info(eth)) goto on_error_1;

   /* NOTE MP 12Apr19: temporary move the eth config to system */

   /* force default configuration, autoneg on */
   /*
   if (eth->eflags & ETH_FLAG_AUTONEG_OFF) {
      eth->eflags &= ~ETH_FLAG_AUTONEG_MASK;
      eth->eflags |= ETH_FLAG_AUTONEG_ON;
      eth_set_info(eth);
   }
   */

   eth_fix_half_duplex(eth);

   if (eth_is_link_and_cable(eth)) // interface running and cable connected
      g_libdance.commlinks |= COMMLNK_ETH;

   return 0;

 on_error_1:
   eth_close(eth);
 on_error_0:
   return -1;
}


void eth_fini(void) {
   eth_handle_t* eth = g_libdance.eth;

   if (eth_is_valid(eth))
      eth_close(eth);
}


int eth_set_info(eth_handle_t* eth) {
   /* what can be set: ETH_FLAG_DUPLEX_xxx, ETH_FLAG_SPEED_xxx */

   struct ethtool_cmd cmd;

   /* retrieve current info for default values */

   eth->ifr.ifr_data = (void *)&cmd;
   cmd.cmd = ETHTOOL_GSET;
   if (ioctl(eth->ioctl_fd, SIOCETHTOOL, &eth->ifr) == -1) return -1;

   /* apply new info */

   if (eth->eflags & ETH_FLAG_DUPLEX_MASK) {
      if (eth->eflags & ETH_FLAG_DUPLEX_HALF) cmd.duplex = DUPLEX_HALF;
      else if (eth->eflags & ETH_FLAG_DUPLEX_FULL) cmd.duplex = DUPLEX_FULL;
   }

   if (eth->eflags & ETH_FLAG_SPEED_MASK) {
      uint32_t x = ((uint32_t)cmd.speed_hi << 16) | (uint32_t)cmd.speed;

      if (eth->eflags & ETH_FLAG_SPEED_10) x = 10;
      else if (eth->eflags & ETH_FLAG_SPEED_100) x = 100;
      else if (eth->eflags & ETH_FLAG_SPEED_1000) x = 1000;

      cmd.speed = x & 0xffff;
      cmd.speed_hi = (x >> 16) & 0xffff;
   }

   if (eth->eflags & ETH_FLAG_AUTONEG_MASK) {
      if (eth->eflags & ETH_FLAG_AUTONEG_OFF) cmd.autoneg = AUTONEG_DISABLE;
      else if (eth->eflags & ETH_FLAG_AUTONEG_ON) cmd.autoneg = AUTONEG_ENABLE;
   }

   eth->ifr.ifr_data = (void *)&cmd;
   cmd.cmd = ETHTOOL_SSET;
   if (ioctl(eth->ioctl_fd, SIOCETHTOOL, &eth->ifr) == -1) return -1;

   return 0;
}


int eth_handle_event(eth_handle_t* eth) {
   /* TODO: cleaner code needed (proper netlink message handling ...) */

   uint8_t buf[128];

   read(eth->nl_fd, buf, sizeof(buf));

   eth_get_info(eth);

   return 0;
}


unsigned int eth_is_link_and_cable(const eth_handle_t* eth) {
   const uint32_t m = ETH_FLAG_LINK_UP | ETH_FLAG_LINK_CABLE;
   return (unsigned int)((eth->eflags & m) == m);
}


void eth_print_info(const eth_handle_t* eth) {
   const char* s;

   printf("name  : %s\n", eth->ifr.ifr_name);

   if (eth->eflags & ETH_FLAG_LINK_UP) s = "up";
   else if (eth->eflags & ETH_FLAG_LINK_DOWN) s = "down";
   else s = "unknown";
   printf("link  : %s\n", s);

   if (eth->eflags & ETH_FLAG_LINK_CABLE) s = "yes";
   else s = "no";
   printf("cable : %s\n", s);

   if (eth->eflags & ETH_FLAG_DUPLEX_HALF) s = "half";
   else if (eth->eflags & ETH_FLAG_DUPLEX_FULL) s = "full";
   else s = "unknown";
   printf("duplex: %s\n", s);

   if (eth->eflags & ETH_FLAG_SPEED_10) s = "10";
   else if (eth->eflags & ETH_FLAG_SPEED_100) s = "100";
   else if (eth->eflags & ETH_FLAG_SPEED_1000) s = "1000";
   else s = "unknown";
   printf("speed : %s\n", s);

   if (eth->eflags & ETH_FLAG_AUTONEG_OFF) s = "off";
   else if (eth->eflags & ETH_FLAG_AUTONEG_ON) s = "on";
   else s = "unknown";
   printf("autoneg: %s\n", s);
}
