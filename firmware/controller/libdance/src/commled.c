/* communication LED */


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <sys/types.h>

#include "libdance.h"

#include "i2cdevices.h"
#include "commled.h"


const ledpattern_t ledOFF =   {.color_on = _COMMLED_OFF,   .time_on = 0};
const ledpattern_t ledGREEN = {.color_on = _COMMLED_GREEN, .time_on = 0};
const ledpattern_t ledRED =   {.color_on = _COMMLED_RED,   .time_on = 0};
const ledpattern_t ledBLUE =  {.color_on = _COMMLED_BLUE,  .time_on = 0};
const ledpattern_t ledBOTH =  {.color_on = _COMMLED_BOTH,  .time_on = 0};

const ledpattern_t ledBLINKRED =
                              {.color_on  = _COMMLED_RED, .time_on  = 200,
                               .color_off = _COMMLED_OFF, .time_off = 200,
                               .cycles = 0, .err_code = 0};
const ledpattern_t ledBLINKGREEN =
                              {.color_on  = _COMMLED_GREEN, .time_on  = 200,
                               .color_off = _COMMLED_OFF,   .time_off = 200,
                               .cycles = 0, .err_code = 0};
const ledpattern_t ledINITERROR =
                              {.color_on  = _COMMLED_RED, .time_on  = 500,
                               .color_off = _COMMLED_OFF, .time_off = 100,
                               .cycles = 0, .err_code = 0};
const ledpattern_t ledCOMMERROR =
                              {.color_on  = _COMMLED_RED, .time_on  = 150,
                               .color_off = _COMMLED_OFF, .time_off = 60,
                               .cycles = 8, .err_code = 0};



static void set_color(uint8_t color) {
   LOG_INFO("Setting COMMLED to color: %d\n", color);
   if (SMB_WRITEBYTE(NULL, COMMLED_DEV, I2C_CMD_OUT_RDWR, color) != 0)
      LOG_ERROR("Error while setting LED color\n");
}


static ledpattern_t* clone_pattern(const ledpattern_t* pattern) {
   ledpattern_t* new_pattern = malloc(sizeof(ledpattern_t));
   if (new_pattern != NULL) {
      *new_pattern = *pattern;
   }
   return new_pattern;
}


static void free_pattern(const ledpattern_t* pattern) {
   if (pattern != NULL)
      free((void*)pattern);
}


static void update_pattern_ptr(const ledpattern_t** pattern, uintptr_t newvalue) {
   if (*pattern != NULL)
      free_pattern(*pattern);
   *pattern = (const ledpattern_t*)newvalue;
}


static void commled_loop(dthrcode_t commcode) {
#define LED_PHASE_STATE_FLAG        0x01
#define LED_PHASE_ERRFBLINKS_FLAG   0x02
#define PHASE_ERRORSTATE_COUNT      2
#define ERRORCODE_PROCESSING()  if((*current)->err_code > 0) { \
                                 phase |= LED_PHASE_ERRFBLINKS_FLAG; \
                                 preblink_count = COMMLED_ERRFBLINKS_N; \
                                 errcode = (*current)->err_code * PHASE_ERRORSTATE_COUNT; \
                                 next = 1; \
                              }

   const ledpattern_t* mainpatt = NULL;
   const ledpattern_t* auxpatt  = NULL;
   const ledpattern_t* errpatt  = NULL;
   const ledpattern_t** current = &mainpatt;
   uint16_t phase = 0;
   uint16_t counts = 0;
   uint32_t t_ms = 0;
   uint16_t next = 0;

   uint16_t errcode = 0;
   uint16_t preblink_count = 0;

   while(1) {
      commcode = dthread_wait(t_ms);
      switch ((int)commcode) {
         case DTHRCMD_TIMEOUT:
            if (errcode > 1) {
               if (preblink_count) {
                  preblink_count--;
               } else {
                  errcode--;
               }
               phase ^= LED_PHASE_STATE_FLAG;
               if (preblink_count <= 0)
                  phase ^= LED_PHASE_ERRFBLINKS_FLAG;
            } else if (counts) {
               counts--;
               if (!counts) {
                  free_pattern(*current);
                  *current = NULL;
                  if (current != &mainpatt) {
                     current = &mainpatt;
                     next = 1;
                  } else {// current == &mainpatt
                     mainpatt = NULL;
                     mainpatt = clone_pattern(&ledOFF); // default pattern
                     set_color(_COMMLED_OFF);
                  }
               } else
                  phase ^= LED_PHASE_STATE_FLAG;
                  ERRORCODE_PROCESSING()

            } else {
               phase ^= LED_PHASE_STATE_FLAG;
               ERRORCODE_PROCESSING()
            }

            break;

         case COMMLED_SETMAIN:
            update_pattern_ptr(&mainpatt, DTHREAD_CMDDATA());
            if (current == &mainpatt)
               next = 1;
            break;

         case COMMLED_SETAUX:
            update_pattern_ptr(&auxpatt, DTHREAD_CMDDATA());
            if (current != &errpatt) {
               current = &auxpatt;
               next = 1;
            }
            break;

         case COMMLED_SETERROR:
            update_pattern_ptr(&errpatt, DTHREAD_CMDDATA());
            current = &errpatt;
            next = 1;
            break;

         case DTHRCMD_FINISH:
            // exit function
            return;

         case DTHRCMD_ERROR:
         case DTHRCMD_WAKEUP:
         default:
            break;
      }

      if (next) {
         phase = 0;
         counts = (*current)->cycles;
         errcode = (*current)->err_code * PHASE_ERRORSTATE_COUNT;
         preblink_count = 0;
         next = 0;

         if (errcode > 0) {
            phase |= LED_PHASE_ERRFBLINKS_FLAG;
            preblink_count = COMMLED_ERRFBLINKS_N;
         }
      }

      if (phase & LED_PHASE_ERRFBLINKS_FLAG) {
         if (phase & LED_PHASE_STATE_FLAG) {
            set_color(_COMMLED_GREEN);
         } else {
            set_color(_COMMLED_OFF);
         }
         t_ms = COMMLED_ERRFBLINKS_MS;
      } else if (phase == 0) {
         set_color((*current)->color_on);
         t_ms = (*current)->time_on;
      } else {  // phase == 1
         set_color((*current)->color_off);
         t_ms = (*current)->time_off;
      }
   }
}

static dthread_t commled_thread;

int commled_init(void) {
#define TCA9554_PINCONF_LED   0xfc
// TCA9554 pin configuration (Check datasheet of TCA9554)
// If a bit in this register is set to 1,the corresponding port pin is enabled
// as an input with a high-impedance output driver.
// If a bit in this register is cleared to 0,
// the corresponding port pin is enabled as an output

   // Configure PIN output
   if (SMB_WRITEBYTE(NULL, COMMLED_DEV, I2C_CMD_CONF_RDWR, TCA9554_PINCONF_LED) != 0) {
      SYSTEM_ERRORF("COMMLED initialisation failed\n");
      return -1;
   }

   if (dthread_start(&commled_thread, commled_loop, 0, DTHRFLG_SINGLERUN | DTHRFLG_STRICTTIME) < 0) {
      SYSTEM_ERRORF("Launching COMMLED thread failed\n");
      return -1;
   }
   return 0;
}

int commled_fini(void) {
   if (dthread_sendcommand(&commled_thread, DTHRCMD_FINISH, (uintptr_t)NULL, 0) < 0) {
      SYSTEM_ERRORF("ending COMMLED threadfailed");
      return -1;
   }

   /* wait for the end of the thread */
   while (dthread_is_running(&commled_thread)) {
      usleep(10);
   }

   return 0;
}


ledpattern_t commled_pattern(uint8_t on,  uint16_t ton_ms,
                             uint8_t off, uint16_t toff_ms,
                             uint16_t ncycles, uint8_t errcode) {
   ledpattern_t ledpatt;

   ledpatt.color_on  = on;
   ledpatt.time_on   = ton_ms;
   ledpatt.color_off = off;
   ledpatt.time_off  = toff_ms;
   ledpatt.cycles    = ncycles;
   ledpatt.err_code  = errcode;

   return ledpatt;
}

int commled_set_pattern(dthrcode_t cmd, const ledpattern_t* ledpatt) {
   ledpattern_t* pattern_pt = clone_pattern(ledpatt); // in two steps to avoid warnings...

   if (pattern_pt == NULL)
      return -1;

   if (dthread_sendcommand(&commled_thread, cmd, (uintptr_t)pattern_pt, 0) < 0) {
      free_pattern(pattern_pt);
      SYSTEM_ERRORF("setting COMMLED pattern failed");
      return -1;
   }
   return 0;
}
