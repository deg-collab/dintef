/*---------------------------------------------------------------------------
 *
 *
 */
#include <ctype.h>            // printf
#include <stdio.h>            // printf
#include <string.h>           // strncpy
#include <stdlib.h>           // malloc
#include <unistd.h>           // gethostname
#include <netdb.h>            // gethostbyname
#include <ifaddrs.h>          // getifaddrs

#include <string.h>
#include <time.h>

#include "libdance.h"

#include "errors.h"
#include "comm.h"
#include "menu.h"
#include "extbuscomm.h"
#include "parsing.h"
#include "disk.h"
#include "libefpak.h"


#define MAX_HOSTNAME_SZ 31

typedef struct
{
   char     hostname[MAX_HOSTNAME_SZ + 1];
   uint32_t address;
   uint32_t netmask;
   uint32_t gateway;
   uint32_t broadcast;
} ip_info_t;


#define IP_READ   0
#define IP_UPDATE 1

const char *empty_string = "";


/*---------------------------------------------------------------------------
 *
 */
int parse_ip(uint32_t *ipval, char *str)
{
   int c0, c1, c2, c3;

   if (sscanf(str, "%d.%d.%d.%d", &c3, &c2, &c1, &c0) == 4)
   {
      ((char *)ipval)[0] = c0;
      ((char *)ipval)[1] = c1;
      ((char *)ipval)[2] = c2;
      ((char *)ipval)[3] = c3;
      return(1);
   } else
      return(0);
}

/*---------------------------------------------------------------------------
 *
 */
void print_ipvalue(uint32_t ipval)
{
   unsigned char *ipchar = (unsigned char *)&ipval;

   answerf("%d.%d.%d.%d", ipchar[3], ipchar[2], ipchar[1], ipchar[0]);
}


/*---------------------------------------------------------------------------
 *
 */
void add_ipdata(char** buf, char* label, uint32_t ipval)
{
   unsigned char *ipchar = (unsigned char *)&ipval;

   *buf += sprintf(*buf, "%s %d.%d.%d.%d\n", label,
                         ipchar[3], ipchar[2], ipchar[1], ipchar[0]);
}


/*---------------------------------------------------------------------------
 *
 */
int process_ip_data(int what, ip_info_t* ip_info)
{
   struct ifaddrs*     ifaddr;
   struct ifaddrs*     ifa;
   struct sockaddr_in* ifa_in;

   if (what == IP_UPDATE)
   {
      // TODO: MP: implement in COMEX Linux a non pure DHCP client
      errorf("Currently not allowed with pure DHCP");
      return(0);
   }

   // Erase any previous setup
   memset(ip_info, 0, sizeof(ip_info_t));

   // Get current hostname
   gethostname(ip_info->hostname, MAX_HOSTNAME_SZ);

   // Get current IP setup
   if (getifaddrs(&ifaddr) == -1)
   {
      errorf("Cannot get IP setup");
      return(0);
   }
   for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
   {
      if(strcmp(ifa->ifa_name,"eth0"))
         continue;
      if(ifa->ifa_addr->sa_family != AF_INET)
         continue;

      ifa_in = ((struct sockaddr_in *)ifa->ifa_addr);
      ip_info->address   = ntohl(ifa_in->sin_addr.s_addr);

      ifa_in = ((struct sockaddr_in *)ifa->ifa_netmask);
      ip_info->netmask   = ntohl(ifa_in->sin_addr.s_addr);

      ifa_in = ((struct sockaddr_in *)ifa->ifa_broadaddr);
      ip_info->broadcast = ntohl(ifa_in->sin_addr.s_addr);

      // TODO: MP: how to find default gateway
   }
   freeifaddrs(ifaddr);

   // Normal end
   return(1);
}


/*---------------------------------------------------------------------------
 *
 */
void parse_new_ip(const char* params)
{
   ip_info_t ip_info;
//   int       label_id;
//   char     *par;
//   char     *ipval_str;
   uint32_t  ipval;
   int       newbroadcast = 0;

   if (!params || !*params)
   {
      errorf(MISPARMESSAGE);
      return;
   }
   // read current values in ip_info
   if (!process_ip_data(IP_READ, &ip_info))
      return;
/*
   // parse new values and load them into ip_info
   while ((par = parse_string(&params))) {
      label_id = parse_listparam(comparlisttable, comparlisttable_sz, cmd_comm_qIP, par);
      if (label_id < 0) {
         errorf("Bad IP configuration value '%s'", par);
         return;
      } else if (!(ipval_str = parse_string(&params))) {
         errorf("Missing IP configuration value for %s", par);
         return;
      } else if (label_id != IP_HOSTNAME && !parse_ip(&ipval, ipval_str))  {
         errorf("Bad %s configuration value: %s", par, ipval_str);
         return;
      } else {
         if (label_id == IP_HOSTNAME)
            strcpy(ip_info.hostname, ipval_str);
         else if (label_id == IP_ADDRESS)
            ip_info.address = ipval;
         else if (label_id == IP_NETMASK)
            ip_info.netmask = ipval;
         else if (label_id == IP_GATEWAY)
            ip_info.gateway = ipval;
         else if (label_id == IP_BROADCAST) {
            ip_info.broadcast = ipval;
            newbroadcast = 1;
         }
      }
   }
*/
   // Check consistency
   //  - is netmask well formed?
   ipval = ~ip_info.netmask;
   while (ipval & 0x01)
      ipval >>= 1;
   if (ipval)
   {
      errorf("Invalid subnetwork mask");
      return;
   }
   // - are address, gateway and netmask consistent?
   if ((ip_info.address & ip_info.netmask) != (ip_info.gateway & ip_info.netmask))
   {
      errorf("Gateway is not in the same subnetwork");
      return;
   }
   // Build broadcast if needed
   if (!newbroadcast)
      ip_info.broadcast = ip_info.address | ~ip_info.netmask;

   // Update the new IP configuration values
   process_ip_data(IP_UPDATE, &ip_info);
}



/*---------------------------------------------------------------------------
 *
 */
void answer_current_ip(int ipval)
{
   ip_info_t ip_info;

   if (!process_ip_data(IP_READ, &ip_info))
      return;

   switch (ipval)
   {
      case IP_HOSTNAME:
         answerf("%s", ip_info.hostname);
         break;

      case IP_ADDRESS:
         print_ipvalue(ip_info.address);
         break;

      case IP_NETMASK:
         print_ipvalue(ip_info.netmask);
         break;

      case IP_GATEWAY:
         print_ipvalue(ip_info.gateway);
         break;

      case IP_BROADCAST:
         print_ipvalue(ip_info.broadcast);
         break;

      default:
         answerf(  "HOSTNAME : %s", ip_info.hostname);
         answerf("\nADDRESS  : ");  print_ipvalue(ip_info.address);
         answerf("\nNETMASK  : ");  print_ipvalue(ip_info.netmask);
         answerf("\nGATEWAY  : ");  print_ipvalue(ip_info.gateway);
         answerf("\nBROADCAST: ");  print_ipvalue(ip_info.broadcast);
         answerf("\n");
         break;
   }
}


/*---------------------------------------------------------------------------
 *
 */

const char *doctyp_msg[N_DOCTYP] = {
    NULL,           // DOCTYP_TITLE
    "Description",  // DOCTYP_DESC
    "Syntax",       // DOCTYP_SYNTAX
    "Example(s)"    // DOCTYP_EX
};

int doctyp_idt[N_DOCTYP] = {
    0,              // DOCTYP_TITLE
    0,              // DOCTYP_DESC
    4,              // DOCTYP_SYNTAX
    4               // DOCTYP_EX
};

#define HELPF_BUFF_SIZE 4096
char *helpf_buff[N_DOCTYP];
int   helpf_msg_sz[N_DOCTYP];


void helpf_cleanup()
{
    size_t i;
    for (i = 0;i < N_DOCTYP;i++) {
        helpf_msg_sz[i] = 0;
        if (helpf_buff[i]) {
            free(helpf_buff[i]);
        }
        helpf_buff[i] = NULL;
    }
}

void helpf_init()
{
    helpf_cleanup();
}

int helpf_check(doctyp_t typ)
{
    if (helpf_buff[typ] == NULL) {
        helpf_buff[typ] = malloc(HELPF_BUFF_SIZE);
        if (helpf_buff[typ] == NULL) {
            return -1;
        }
    }
    return 0;
}

void helpf(doctyp_t typ, const char *fmt, ...)
{
    va_list ap;
    int     n __attribute__((unused));

    if (helpf_check(typ)) {
        LOG_ERROR("unable to allocate helpf buffer\n");
        return;
    }

    va_start(ap, fmt);
    n = vsnprintf(
        helpf_buff[typ] + helpf_msg_sz[typ],
        HELPF_BUFF_SIZE - helpf_msg_sz[typ],
        fmt, ap);
    va_end(ap);

    /* TODO: check truncation
    if (n > (HELPF_BUFF_SIZE - helpf_msg_sz[typ]))
    ... realloc() ...
    */
    helpf_msg_sz[typ] = strlen(helpf_buff[typ]);
}

void answerf_indent(int n, char *str)
{
    char *beg;
    char *end;

    for (beg = str;;)
    {
        end = strchr(beg, '\n');
        if (end != NULL) *end = 0x00;
        answerf("%*s%s\n", n, "", beg);
        if (end == NULL) break;
        beg = end + 1;
    }
}

void helpf_end()
{
    size_t i;
    for (i = 0;i < N_DOCTYP;i++) {
        if (helpf_msg_sz[i]) {
            if (doctyp_msg[i]) {
                answerf("%s:\n", doctyp_msg[i]);
            }
            answerf_indent(doctyp_idt[i], helpf_buff[i]);
        }
    }

    helpf_cleanup();
}

int helpf_is_doctyp(doctyp_t typ)
{
    return helpf_msg_sz[typ]?1:0;
}

#define MAX_CMD_SZ 32

char* unrework_comm_menu(char* comm_str)
{
   static char buff[MAX_CMD_SZ + 1];

   strncpy(buff, comm_str, MAX_CMD_SZ);
   buff[MAX_CMD_SZ] = 0;
   comm_str = buff;
   if (*comm_str == 'q') *comm_str++ = '?';
   if (*comm_str == 'b') *comm_str++ = '*';

   return(buff);
}

void answer_command_list(const menuitem_t* menu, int msize, int select)
{
   int selectflags = 0;

   switch(select)
   {
      case DEFAULT_CHOICE:
      case HELP_BASIC:
         selectflags = _BASIC_COMMAND;
         break;

      case HELP_EXPERT:
         selectflags = _EXPERT_COMMAND;
         break;

      case HELP_TEST:
         selectflags = _TEST_COMMAND;
         break;

      case HELP_ALL:
         selectflags = _BASIC_COMMAND | _EXPERT_COMMAND | _TEST_COMMAND;
         break;

      default:
         SYSTEM_ERROR();
         errorf(SYSERRORMESSAGE);
         break;
   }

   for ( ; msize-- > 0; menu++)
   {
      if (!(menu->flags & selectflags))     // not selected command
         continue;

      if (!*(menu->comm_str))                 // empty command keyword
         continue;

#define HELP_COL_SIZE 11
      answerf("%.*s : %s\n", HELP_COL_SIZE, unrework_comm_menu(menu->comm_str), menu->help);
   }
}

int answer_command_help(const menuitem_t* menu, int msize, const char *cmd)
{
   char *comm_str;
   for ( ; msize-- > 0; menu++)
   {

      if (!*(menu->comm_str))                 // empty command keyword
         continue;

      comm_str = unrework_comm_menu(menu->comm_str);

      if (strcasecmp(comm_str, cmd))
         continue;


      /* Optional controller help function */
      if (menu->help_funct) {

         /* Gather all help strings sorted by type */
         helpf_init();
         menu->help_funct();

         /* Controller command title overwrite the MENU_IT one */
         if (!helpf_is_doctyp(DOCTYP_TITLE)) {
            HELP_TITLE("%s\n", menu->help);
         }

         /* Answer all help strings at once */
         helpf_end();
      } else {
        /* Default command help answer */
        answerf("%s\n", menu->help);
      }

      return 0;

   }
   return -1;
}


/*---------------------------------------------------------------------------
 * Execute DANCE common commands
 */
void commexec_qHELP(void)
{
   id_choice_t help_option = I_CHOICE();
   const char *help_cmd    = I_LABEL();

   if (help_cmd == NULL) {
      answer_command_list(applicationmenu, applicationmenu_sz, help_option);
      answer_command_list(commonmenu,      commonmenu_sz,      help_option);
   } else {
      if (!answer_command_help(applicationmenu, applicationmenu_sz, help_cmd))
         return;
      if (!answer_command_help(commonmenu,      commonmenu_sz,      help_cmd))
         return;
      errorf("unknown command\n");
   }
}

void commexec_IP(void)
{
   parse_new_ip(I_STRING());
}

void commexec_qIP(void)
{
   answer_current_ip(I_CHOICE());
}

void commexec_ECHO(void)
{
   errorf("Not implemented");
}

void commexec_NOECHO(void)
{
   ; // do nothing
}

void commexec_COMMCLOSE(void)
{
//   channel_t* channel = get_current_command()->channel;
//   channel->close(channel);
}


static void reboot_cpu_fn(void* unused_args)
{
   LOG_COMM("Rebooting CPU\n");
   REQUEST_CPU_REBOOT();
}

static void reboot_soft_fn(void* unused_args)
{
   LOG_COMM("Rebooting SOFT\n");
   REQUEST_SOFT_REBOOT();
}

void help_REBOOT(void)
{
    HELP_SYNTAX("REBOOT {CPU | FPGA | SOFT} [DELAY <sec>]\n");

    HELP_EXAMPLE(
        ">>>REBOOT SOFT DELAY 0.2\n"
        ">>>REBOOT CPU\n"
    );
}

/*  "{ T#CPU | T#FPGA | T#SOFT o(T#DELAY uF100) }" */
void commexec_REBOOT(void)
{
#define REBOOT_FLAG_CPU  (1 << 0)
#define REBOOT_FLAG_FPGA (1 << 1)
#define REBOOT_FLAG_SOFT (1 << 2)
#define REBOOT_FLAG_ALL  ((1 << 3) - 1)
  unsigned int flags = 0;
  float        delay = 0.1; /* give time to return answer to client */


  if (I_NAMED_TAG("CPU"))   flags |= REBOOT_FLAG_CPU;
  if (I_NAMED_TAG("FPGA"))  flags |= REBOOT_FLAG_FPGA;
  if (I_NAMED_TAG("SOFT"))  flags |= REBOOT_FLAG_SOFT; 
  if (I_NAMED_TAG("DELAY")) delay = I_FLOAT();

  if (flags & REBOOT_FLAG_CPU) {
    LOG_COMM("Requesting CPU reboot in %.1f seconds\n", delay);
    add_oneshot_task((int)(delay * 1000), reboot_cpu_fn, NULL);
  } else if (flags & REBOOT_FLAG_SOFT) {
    LOG_COMM("Requesting SOFT reboot in %.1f seconds\n", delay);
    add_oneshot_task((int)(delay * 1000), reboot_soft_fn, NULL);
  } else if (flags & REBOOT_FLAG_SOFT) {
    /* TODO: add support to REBOOT_FLAG_FPGA once FPGAs are identificated */
    errorf("FPGA reboot not implemented yet");
  }
}

void commexec_DEBUG(void)
{
#ifdef LOG_IS_ENABLED
  id_integer_t dbglvl = I_INTEGER();
  if (dbglvl) log_enable_all(0);
  else log_disable_all();
#else
   errorf("LOG_IS_ENABLED not defined");
#endif
}

void commexec_qDEBUG(void)
{
#ifdef LOG_IS_ENABLED
  uint32_t klass;
  for (klass = 0; klass != LOG_CLASS_MAX; ++klass)
  {
    if (log_mask[klass]) break ;
  }
  O_INTEGER((klass == LOG_CLASS_MAX) ? 0 : 1);
#else
  errorf("LOG_IS_ENABLED not defined");
#endif
}


static int answer_register_value(dancereg_t *reg, int format) {
   if (extbus_read_regmem(reg, 0, 0, 1, &reg->shadow, 0)) {
      errorf("Reading register \'%s\' failed : %s", reg->name, libdanceerror());
      return -1;

   } else {
      LOG_EXTBUS("Read from register %s at addr %d:0x%04zX: 0x%08X\n",
                   reg->access == _UNK? "": reg->name, reg->baddr, reg->offset, reg->shadow.v32);

      switch (reg->dtype) {
         case _D8:   answer_int8(reg->shadow.v8,  format); break;
         case _D16: answer_int16(reg->shadow.v16, format); break;
         case _D32: answer_int32(reg->shadow.v32, format); break;
         case _D64: answer_int64(reg->shadow.v64, format); break;
         default:  answerf("???");
            break;
      }
      return 0;
   }
}

static void answer_register(const char *reg_id, int mode, int format, unsigned int nrepet) {

   if (!reg_id || !*reg_id) {  // If no register id, return the entire register list
      int idx;
      int first = gdance_regdescr->first_register;
      int next = first + gdance_regdescr->register_n;
      unsigned int maxlen = 8;

      // calculate max name length
      for (idx = first; idx < next; idx++) {
         dancereg_t *reg = &gdance_regdescr->regtbl[idx];
         int reglen = strlen(reg->name);
         if (reglen > maxlen) maxlen = reglen;
      }

      for (idx = first; idx < next; idx++) {
         dancereg_t *reg = DANCE_REG_PT(idx);
         char        buff[128];

         if (reg->info->bustype == _FAKE_BUS)
            continue;

         if (!(reg->flags & mode))
            continue;

         extbus_sprint_regmem(reg, buff, sizeof(buff) - 1);

         answerf("%*s [%s] : ", maxlen, reg->name, buff);
         if (reg->access == _WO)
            answerf("<write only>\n");
         else {
            if (answer_register_value(reg, format) == 0)
               answerf("\n");
         }
      }
   } else {  // parse specific register id
      dancereg_t* reg = extbus_parse_regmem(reg_id, _ALL_REG, _UNK);

      if (reg == NULL)
         errorf("%s", libdanceerror());
      else if (CANNOT_BE_READ(reg->access))
         errorf("Register \'%s\' is not readable", reg_id);
      else {
         while(nrepet--) {
            answer_register_value(reg, format);
            if (nrepet)
               answerf("\n");
         }
      }
   }
}

// "o{oC#DFRMT o(C#REG | (L opI))}"
// ?REG  [{ HEXA | SIGNED | UNSIGNED }] [{<reg_type> | (<reg_id> [<n_repet>])}]
void commexec_qREG(void)
{
   // Return an explicit error (TODO: disable instead all REG commands??)
   if (gdance_regdescr->register_n == 0) {
      errorf("No registers defined");
      return;
   }

   id_choice_t  format = I_CHOICE(); // display format
   id_choice_t  reg_type = I_CHOICE();
   const char*  reg_id = I_LABEL();
   id_integer_t n_repet = I_INTEGER();
   int          reg_flags;

   if (format == DEFAULT_CHOICE) format = DFRMT_HEXA;
   if (n_repet == DEFAULT_INTEGER) n_repet = 1;

   switch (reg_type) {
      case REG_ALL:     reg_flags = _ALL_REG;     break;
      case REG_EBONE:   reg_flags = _EBONE_REG;   break;
      case REG_APPL:    reg_flags = _APPL_REG;    break;
      case REG_SPECIAL: reg_flags = _SPECIAL_REG; break;
      case REG_BASIC:   reg_flags = _BASIC_REG;   break;
      default:          reg_flags = _ALL_REG;     break;  // REG_ALL or DEFAULT_CHOICE
   }
   answer_register(reg_id, reg_flags, format, n_repet);
}


static int select_memreg_block(const char* memreg_id, dancereg_t* memreg, int regtype, ssize_t memoffset, int rstep, ssize_t n_values) {
   dancereg_t* locmemreg;

   locmemreg = extbus_parse_regmem(memreg_id, regtype, _UNK);
   if (locmemreg == NULL) {
      errorf("%s", libdanceerror());
      return -1;
   } else {
      *memreg = *locmemreg;

      if (rstep) {
         if (memoffset < 0)
            memoffset += memreg->ndata;
         if (memoffset < 0 || memoffset >= memreg->ndata) {
            errorf("Invalid data offset value for \'%s\'", memreg_id);
            return -1;
         }

         if (n_values <= 0)
            n_values += (memreg->ndata - memoffset);
         if (n_values <= 0 || n_values > (memreg->ndata - memoffset)) {
            errorf("Wrong number of data values for memory block \'%s\'", memreg_id);
            return -1;
         }
      }

      /* // if memreg is aligned in unit, this is not needed...
      byteoffset = memreg->offset + memoffset * unit;
      if (((unit >= _D32) && !IS_ALIGNED_32(byteoffset)) ||
               ((unit == _D16) && !IS_ALIGNED_16(byteoffset))) {
         errorf("Bad memory alignment for %d-bit data", unit * 8);
         return NULL;
      }
      */
      memreg->offset += memoffset * memreg->dtype;
      memreg->ndata = n_values;

      LOG_EXTBUS("Selected %zu %d-bit values (%zu bytes)", memreg->ndata, memreg->dtype * 8, memreg->ndata * memreg->dtype);
      LOG_EXTBUS(" of memory %s at addr %d:0x%04zX\n", memreg->access == _UNK? "unknown": memreg_id, memreg->baddr, memreg->offset);

      return 0;
   }
}

static mblock_t* copy_to_mblock(dancereg_t* memreg, int rstep) {
   unsigned int unit = memreg->dtype;      // data unit in bytes
   size_t       n_values = memreg->ndata;
   mblock_t*    mblk;

   if (!(mblk = mblock_create(NULL, NULL, unit * n_values, MEM_DEFAULT))) {
      errorf("No enough memory - unit: %d, size: %d", unit, n_values);
      return NULL;
   }

   if (extbus_read_regmem(memreg, 0, rstep, n_values, mblock_ptr(mblk), 1)) {
      errorf("Error reading %zd-size register block: %s\n", n_values, libdanceerror());
      mblock_release(mblk, DESTROY_FORCE);
      return NULL;
   }
   return mblk;
}

// "L pI"
// ?*REG <reg_id>  <n_reads>
void commexec_qbREG(void) {

   // Return an explicit error (TODO: disable instead all REG commands??)
   if (gdance_regdescr->register_n == 0) {
      errorf("No registers defined");
      return;
   }

   dancereg_t    reg;
   const char*   reg_id = I_LABEL(); // id of the 32-bit register
   size_t        n_repeat = I_INTEGER(); // number or 32-bit values to read
   unsigned int  unit;                   // data unit in bytes
   unsigned int  offset = 0;             // address offset
   unsigned int  regstep = 0;            // step to increment register addresses
   mblock_t*     mblk;
   unsigned int  flags;

   if (select_memreg_block(reg_id, &reg, _ALL_REG, offset, regstep, n_repeat))
      return;
   else if (CANNOT_BE_READ(reg.access)) {
      errorf("Address \'%s\' is not readable", reg_id);
      return;
   }

   mblk = copy_to_mblock(&reg, regstep);
   if (mblk == NULL)
      return;

   unit = reg.dtype;

   LOG_COMM("Sending %zd x %d byte binary data items!!\n", n_repeat, (int)unit);
   flags = BIN_FLG_USECHKSUM | BIN_FLG_LITTLENDIAN;
   send_binary_buffer(mblk, unit, n_repeat, flags);
}

// "L I"
// REG <reg_name>  <value>
void commexec_REG(void)
{
   // Return an explicit error (TODO: disable instead all REG commands??)
   if (gdance_regdescr->register_n == 0) {
      errorf("No registers defined");
      return;
   }

   const char* reg_name = I_LABEL();
   uint64_t    val64 = I_INTEGER();
   dancereg_t* reg = extbus_parse_regmem(reg_name, _ALL_REG, _RD);

   if (reg == NULL)
      errorf("%s", libdanceerror());
   else
      extbus_write_regmem(reg, 0, 0, 1, &val64, 0);
}

// "L"
// *REG <reg_id> >>binary_data<<
void commexec_bREG(void)
{
   // Return an explicit error (TODO: disable instead all REG commands??)
   if (gdance_regdescr->register_n == 0) {
      errorf("No registers defined");
      return;
   }

   binary_t*   binfo;
   const char* reg_id = I_LABEL(); // id of the 32-bit register
   int         regstep = 0;              // step to increment register addresses
   uint32_t*   databuff;

   binfo = get_binary_com_info();

   if (!(databuff = malloc(binfo->unit * binfo->size))) {
      errorf("No enough memory - unit: %d, size: %d", binfo->unit, binfo->size);
      return;
   } else if (get_binary_data(databuff, binfo->unit, binfo->size) < 0) {
      errorf("Error reading binary data");
   } else if (binfo->unit != sizeof(uint32_t)) {
      errorf("Can only use 32-bit data");
   } else {
      dancereg_t* reg = extbus_parse_regmem(reg_id, _ALL_REG, _WR);
      if (reg == NULL) {
      } else {
         if (extbus_write_regmem(reg, 0, regstep, binfo->size, databuff, 1)) {
            LOG_ERROR("Error writing %d-size register block\n", binfo->size);
            errorf("%s", libdanceerror());
         }
      }
   }
   free(databuff);
}


// "o{oC#DFRMT o(L oI oI)}"
// ?MEM [{ HEXA | SIGNED | UNSIGNED }] [<mem_id> [<n_values> [<offset>]]]
void commexec_qMEM(void)
{
   id_choice_t format = I_CHOICE(); // display format
   const char *mem_id = I_LABEL();  // id of the memory block

   if (format == DEFAULT_CHOICE)
      format = DFRMT_HEXA;

   if (mem_id == DEFAULT_LABEL) {   // If no memory id, return the list of memory blocks
      int i = gdance_regdescr->first_memblock;
      int n = gdance_regdescr->memblock_n;
      for ( ; n-- > 0; i++)
      {
         dancereg_t* reg = DANCE_REG_PT(i);

         answerf("%8s [%1d:%04zx] - %zd %d-bit ", reg->name, reg->baddr,
                                                  reg->byteoffset, reg->ndata,
                                                  reg->dtype * 8);
         if (reg->access == _WO)
            answerf("write-only");
         else if (reg->access == _RO)
            answerf("read-only");
         else
            answerf("read/write");
         answerf(" records (0x%zx bytes)\n", reg->ndata * reg->dtype);
      }
   } else {
      id_integer_t size   = I_INTEGER(); // number or data elements to read
      id_integer_t offset = I_INTEGER(); // offset from mem block in data values
      unsigned int regstep = 1;
      dancereg_t   mem;
      mblock_t*    mblk;
      void*        value_pt;

      if (size == DEFAULT_INTEGER)
         size = 0;
      if (offset == DEFAULT_INTEGER)
         offset = 0;

      if (select_memreg_block(mem_id, &mem, _ALL_MEM, offset, regstep, size))
         return;
      else if (CANNOT_BE_READ(mem.access)) {
         errorf("Memory area \'%s\' is not writeable", mem_id);
         return;
      }

      mblk = copy_to_mblock(&mem, regstep);
      if (mblk == NULL)
         return;

//      value_pt = mblock_ptr(mblk) + (mem.offset - ALIGN_32(mem.offset));
      value_pt = mblock_ptr(mblk);

      for ( ; mem.ndata--; value_pt += mem.dtype) {
         switch (mem.dtype) {
            case _D8:  answer_int8(*(uint8_t *)value_pt, format); break;
            case _D16: answer_int16(*(uint16_t *)value_pt, format); break;
            case _D32: answer_int32(*(uint32_t *)value_pt, format); break;
            case _D64: answer_int64(*(uint64_t *)value_pt, format); break;
            default:  SYSTEM_ERROR(); errorf(SYSERRORMESSAGE); break;
         }
         answerf("\n");
      }
      mblock_release(mblk, DESTROY_FORCE);
   }
}


// "L oI oI"
// ?*MEM <mem_id>  [<n_values> [<offset>]]
void commexec_qbMEM(void) {
   const char*   mem_id = I_LABEL();   // id of the memoryblock
   id_integer_t  size = I_INTEGER();   // number or data elements to read
   id_integer_t  offset = I_INTEGER(); // offset from mem block in data values
   dancereg_t    mem;
   unsigned int  regstep = 1;
   unsigned int  flags;
   mblock_t*     mblk;

   if (size == DEFAULT_INTEGER)
      size = 0;
   if (offset == DEFAULT_INTEGER)
      offset = 0;

   if (select_memreg_block(mem_id, &mem, _ALL_MEM, offset, regstep, size))
      return;
   else if (CANNOT_BE_READ(mem.access)) {
      errorf("Memory area \'%s\' is not writeable", mem_id);
      return;
   }

   mblk = copy_to_mblock(&mem, regstep);
   if (mblk == NULL)
      return;

   LOG_COMM("Sending %zd x %d byte binary data items!!\n", mem.ndata, mem.dtype);
   flags = BIN_FLG_USECHKSUM | BIN_FLG_LITTLENDIAN;
   send_binary_buffer(mblk, mem.dtype, mem.ndata, flags);
}


// "L oI oI ((T#FILL oI) | T#CLEAR)"
// MEM  <mem_id> [<offset> [<data_values>]] {FILL [<value>] | CLEAR }
void commexec_MEM(void)
{
   const char*   mem_id = I_LABEL();  // id of the memoryblock
   id_integer_t  offset = I_INTEGER(); // offset from mem block in data values
   id_integer_t  size   = I_INTEGER(); // number or data elements to read
   id_integer_t  value;
   dancereg_t    mem;
   unsigned int  regstep = 1;

   if (offset == DEFAULT_INTEGER)
      size = offset = 0;
   else if (size == DEFAULT_INTEGER)
      size = 0;

   if (select_memreg_block(mem_id, &mem, _ALL_MEM, offset, regstep, size))
      return;
   else if (CANNOT_BE_WRITEN(mem.access)) {
      errorf("Memory area \'%s\' is not writeable", mem_id);
      return;
   }

   if (I_NAMED_TAG("FILL")) {
      value  = I_INTEGER();
      if (value == DEFAULT_INTEGER)
         value = 0;
   } else {  // CLEAR
      value = 0;
   }
   if (extbus_write_regmem(&mem, 0, regstep, mem.ndata, &value, 0)) {
      LOG_ERROR("Error writing %d in %d-size register block\n", (int)value, mem.ndata);
      errorf("%s", libdanceerror());
   }
}


// "L I"
// *MEM <mem_id> [<offset>] >>binary_data<<
void commexec_bMEM(void) {
   const char*  mem_id = I_LABEL();   // id of the memoryblock
   id_integer_t offset = I_INTEGER(); // offset from start of memory block
   binary_t*    binfo;
   void*        databuff;
   dancereg_t   mem;
   unsigned int regstep = 1;

   if (offset == DEFAULT_INTEGER)
      offset = 0;

   binfo = get_binary_com_info();

   if (!(databuff = malloc(binfo->unit * binfo->size))) {
      errorf("No enough memory - unit: %d, size: %d", binfo->unit, binfo->size);

   } else {
      if (get_binary_data(databuff, binfo->unit, binfo->size) < 0)
         errorf("Error reading binary data");
      else if (select_memreg_block(mem_id, &mem, _ALL_MEM, offset, regstep, binfo->size) == 0) {
         if (CANNOT_BE_WRITEN(mem.access))
            errorf("Memory area \'%s\' is not writeable", mem_id);
         else if (binfo->unit != mem.dtype)
            errorf("Wrong %d-bit data type", binfo->unit * 8);
         else {
            if (extbus_write_regmem(&mem, 0, regstep, binfo->size, databuff, 1)) {
               LOG_ERROR("Error writing %d-size register block\n", binfo->size);
               errorf("%s", libdanceerror());
            }
         }
      }
      free(databuff);
   }
}



static disk_handle_t disk = {.status = "DONE", .total2wr = 0};
static dthread_t prog_thread;
static efpak_istream_t prog_is; /* TODO: move that disk_handle_t ?? */
static void *prog_data;         /* TODO: move that disk_handle_t ?? */


void prog_loop(dthrcode_t commcode)
{
   disk_handle_t* diskp;

   LOG_INFO("PROG thread started\n");

   diskp = &disk;
#if 0
   size_t  msg_sz = 128;
   char    msg[msg_sz];
   int     i;
   int     iters = 3;
   for(i = 0;i < iters; i++)
   {
      /* WARNING: can not escape char % (bug of snprintf()+preprocessor ??)*/
      snprintf(msg, msg_sz, "%03d/100", (int)((100*i)/iters));
      DISK_UPDATE_STATUS(diskp, "%s", msg);
      LOG_INFO("%s\n",msg);
      sleep(1);
   }
#else
   if (disk_install_with_efpak(diskp, &prog_is))
   {
      /* add a default error status */
      if(!DISK_IS_ERROR(diskp))
        DISK_UPDATE_STATUS_ERR(diskp, "Install failed\n");

      LOG_ERROR(DISK_STATUS(diskp));
      goto on_error_1;
   }
#endif

   /* normal end */
   DISK_UPDATE_STATUS(diskp, "DONE");

 on_error_1:
   disk_close(diskp);
   efpak_istream_fini(&prog_is);
   free(prog_data);

   LOG_INFO("PROG thread finished\n");
}

void commexec_qPROG(void)
{
   disk_handle_t* diskp;

   diskp = &disk;
   if(DISK_IS_ERROR(diskp))
     errorf(DISK_STATUS(diskp));
   else
     answerf(DISK_STATUS(diskp));
}


void commexec_bPROG(void)
{
   /* update the firmware using the EFPAK data */
   /* currently, only disk update is supported */

   binary_t*    binfo;
   size_t	size;
   disk_handle_t* diskp;


   LOG_INFO("commexec_bPROG()\n");

   diskp = &disk;
   DISK_UPDATE_STATUS(diskp, "Started");
   DISK_CLR_ERROR(diskp);

   binfo = get_binary_com_info();
   size  = binfo->unit * binfo->size;
   LOG_INFO("binary data size: %zubytes\n", size);
   DISK_UPDATE_STATUS(diskp, "Getting binary: %zubytes", size);
   prog_data = malloc(size);
   if (prog_data == NULL)
   {
     DISK_UPDATE_STATUS_ERR(diskp, "Not enough memory: %zu", size);
     LOG_ERROR("%s\n", DISK_STATUS(diskp));
     errorf(DISK_STATUS(diskp));
     goto on_error_0;
   }

   if (get_binary_data(prog_data, binfo->unit, binfo->size) < 0)
   {
     DISK_UPDATE_STATUS_ERR(diskp, "Could not get binary data");
     LOG_ERROR("%s\n", DISK_STATUS(diskp));
     errorf(DISK_STATUS(diskp));
     goto on_error_1;
   }
   DISK_UPDATE_STATUS(diskp, "Got binary data");
   LOG_INFO("%s\n", DISK_STATUS(diskp));

   if (efpak_istream_init_with_mem(&prog_is, prog_data, size))
   {
     DISK_UPDATE_STATUS_ERR(diskp, "Could not init stream");
     LOG_ERROR("%s\n", DISK_STATUS(diskp));
     errorf(DISK_STATUS(diskp));
     goto on_error_1;
   }

   /* not an error on detector platforms */
   if (disk_open_root(&disk)) diskp = NULL;

   /* at this point we got from client everything needed for prog */
   if (dthread_start(&prog_thread, prog_loop, 0, DTHRFLG_SINGLERUN) < 0)
   {
     DISK_UPDATE_STATUS_ERR(diskp, "Could not launch PROG thread");
     LOG_ERROR("%s\n", DISK_STATUS(diskp));
     errorf(DISK_STATUS(diskp));
     goto on_error_2;
   }
   goto on_error_0;


 on_error_2:
   if (diskp != NULL) disk_close(diskp);
   efpak_istream_fini(&prog_is);
 on_error_1:
   free(prog_data);
 on_error_0:
   return ;
}
