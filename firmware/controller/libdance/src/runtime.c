#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/fcntl.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>

#include "libdance.h"

#include "runtime.h"
#include "comm.h"
#include "timer.h"
#include "log.h"
#include "libuirq.h"
#include "eth.h"
#include "commled.h"


// internal state of the runtime system
struct
{
   pthread_t       main_thread;
   io_handle_t*    signal_io;
   lnklist_t       ios;

   // main event timer list
   timerlist_t     timer_list;

   // timeout
   long            tmout_us;
   struct timespec tmout_abs;
   struct timeval  tmout_tmval;   // holds updated timeout timeval
   void*           tmout_data;

   // task queue stuff
   tqueue_handle_t tq;

   uirq_handle_t   uirq;

} runtime;


// io tools
//

int is_in_runtime_thread(void)
{
   return pthread_equal(pthread_self(), runtime.main_thread);
}

int do_nonblock(int fd, int set)
{
   int flags;

   errno = 0;
   flags = fcntl(fd, F_GETFL);
   if (errno) return -1;

   flags = set? flags | O_NONBLOCK : flags & ~O_NONBLOCK;

   fcntl(fd, F_SETFL, flags);
   if (errno) return -1;

   return 0;
}


io_handle_t* create_io_handle(int fd, iotype_t type, const io_ops_t *io_ops, void *udata)
{
   io_handle_t* new_io = malloc(sizeof(io_handle_t));

   if (new_io == NULL) goto on_error_0;

   if (pthread_mutex_init(&new_io->wlock, NULL)) goto on_error_1;

   new_io->fd  = fd;
   new_io->type = type;
   new_io->ops = io_ops;
   new_io->udata = udata;
   new_io->flags = 0;

   new_io->item = lnklist_add_head(&runtime.ios, new_io);
   if (new_io->item == NULL) goto on_error_2;

   return new_io;

 on_error_2:
   pthread_mutex_destroy(&new_io->wlock);
 on_error_1:
   free(new_io);
 on_error_0:
   return NULL;
}

__attribute__((unused))
static danceerr_t fini_io_buf(lnklist_item_t* item, void* data)
{
/*
  runtime_buf_t* const buf = item->data;
  if (buf->data != NULL) free(buf->data);
  free(buf);
*/
  return DANCE_OK;
}

void free_io_handle(io_handle_t* io)
{
   lnklist_free_item(io->item);
   pthread_mutex_destroy(&io->wlock);
   free(io);
}


void block_io_read(io_handle_t* io)
{
   __sync_or_and_fetch(&io->flags, IOFLAG_BLOCKED);
}

void unblock_io_read(io_handle_t* io)
{
   __sync_and_and_fetch(&io->flags, ~IOFLAG_BLOCKED);
   // wake up potentially sleeping runtime loop
   runtime_wakeup();
}



// ---------------------------------- signal I/O

static int signal_write(io_handle_t* io)
{
   LOG_ERROR("operation not implemented\n");
   return -1;
}

static int signal_read(io_handle_t* io)
{
   signal_t     signal;
   timeritem_t* timer_it;
   int          nrecv;

   while(1)
   {
      errno = 0;
      nrecv = read(io->fd, &signal, sizeof(signal_t));
      if (errno == EAGAIN)
         break;
      else if (nrecv != sizeof(signal_t))
      {
         LOG_ERROR("read error (errno == %d)\n", errno);
         break;
      }
      LOG_INFO("Runtime signal %d (%p) received\n", signal.sigtype, signal.arg);
      switch (signal.sigtype)
      {
         case SIGTYPE_COMMCLEANUP:
            do_command_cleanup(signal.arg);
            break;

         case SIGTYPE_ONESHOT:
            timer_it = signal.arg;
            timerlist_add_oneshot_timer(&runtime.timer_list, timer_it);
            break;

         case SIGTYPE_PERIODIC:
            timer_it = signal.arg;
            timerlist_add_periodic_timer(&runtime.timer_list, timer_it);
            break;

         default:  //   SIGTYPE_UNKNOWN, SIGTYPE_WAKEUP
            // do nothing, used just for wakeup
            break;
      }
   }
   return 0;
}

static int signal_close(io_handle_t* io)
{
   close(io->fd);
   close((int)(uintptr_t)io->udata);
   return 0;
}

static const io_ops_t signal_ops =
{
   .write_fn = signal_write,
   .read_fn = signal_read,
   .close_fn = signal_close
};

static danceerr_t add_signal_io(void)
{
   io_handle_t* io;
   int          fd[2];

   if (pipe(fd)) goto on_error_0;

   set_nonblock(fd[0]);

   io = create_io_handle(fd[0], IOTYPE_SIGNAL, &signal_ops, (void*)(uintptr_t)fd[1]);
   if (io == NULL) goto on_error_1;

   if (!lnklist_add_head(&runtime.ios, io)) goto on_error_2;

   runtime.signal_io = io;
   return DANCE_OK;

 on_error_2:
   free(io);
 on_error_1:
   close(fd[0]);
   close(fd[1]);
 on_error_0:
   return DANCE_ERROR;
}

danceerr_t runtime_send_signal(sigtype_t sigtype, void* arg)
{
   signal_t  signal;
   const int fd = (int)(uintptr_t)runtime.signal_io->udata;

   signal.sigtype = sigtype;
   signal.arg = arg;
   if (write(fd, &signal, sizeof(signal_t)) != sizeof(signal_t))
   {
      LOG_ERROR("write error (errno == %d)\n", errno);
      return DANCE_ERROR;
   }
   return DANCE_OK;
}


#if PCIBUS_USED

// UIRQ based IOs
// purpose is to handle hardware generated IRQs by a userland task

uirq_handle_t* runtime_get_uirq(void)
{
   // TODO: remove when no longer needed in daq/libedma
   return &runtime.uirq;
}

int register_isr(uint32_t mask, void (*fn)(void*), void* data)
{
   return uirq_register_isr(&runtime.uirq, mask, fn, data);
}

void unregister_isr(uint32_t mask)
{
   uirq_unregister_isr(&runtime.uirq, mask);
}

static int uirq_io_read(io_handle_t* io)
{
   uirq_handle_t* const uirq = io->udata;
   uirq_handle_irq(uirq);
   return 0;
}

static int uirq_io_close(io_handle_t* io)
{
   uirq_handle_t* const uirq = io->udata;
   uirq_close(uirq);
   uirq_fini_lib();
   return 0;
}

static const io_ops_t uirq_ops =
{
   .write_fn = NULL,
   .read_fn = uirq_io_read,
   .close_fn = uirq_io_close
};

static danceerr_t add_uirq_io(const char* pci_id)
{
   io_handle_t* io;
   int fd;

   if (uirq_init_lib()) goto on_error_0;
   if (uirq_init(&runtime.uirq, pci_id)) goto on_error_1;
   if (uirq_open(&runtime.uirq)) goto on_error_1;

   fd = uirq_get_fd(&runtime.uirq);
   io = create_io_handle(fd, IOTYPE_UIRQ, &uirq_ops, (void*)&runtime.uirq);
   if (io == NULL) goto on_error_2;

   if (lnklist_add_head(&runtime.ios, io) == 0) goto on_error_3;

   return DANCE_OK;

 on_error_3:
   free_io_handle(io);
 on_error_2:
   uirq_close(&runtime.uirq);
 on_error_1:
   uirq_fini_lib();
 on_error_0:
   return DANCE_ERROR;
}

#endif // PCIBUS_USED

danceerr_t del_uirq_io(void)
{
   /* TOREMOVE. cf. daq.c/daq_is_explicitly_managed */

   lnklist_item_t* it;
   io_handle_t* io;
   const int uirq_fd = uirq_get_fd(&runtime.uirq);

   for (it = runtime.ios.head; it != NULL; it = it->next)
   {
      io = it->itmdata;
      if (io->fd == uirq_fd) break ;
   }

   /* not in list */
   if (it == NULL) goto on_success;

   lnklist_free_item(it);
   free_io_handle(io);

 on_success:
   return DANCE_OK;
}


#if ETHERNET_USED

/* eth related ios */

extern void eth_print_info(const eth_handle_t*);

static int eth_io_read(io_handle_t* io)
{
   eth_handle_t* const eth = io->udata;

   eth_handle_event(eth);

   /* cable connection state. update commled. */

   if (eth_is_link_and_cable(eth)) {
      /* apply fixes, only if nic up and cable connected */
      eth_fix_half_duplex(eth);
      g_libdance.commlinks |= COMMLNK_ETH;

   } else {

      /* NOTE MP 12Apr19: temporary move the eth config to system */

      /* set autoneg back to default */
      /*
      if (eth->eflags & ETH_FLAG_FIX_APPLIED) {
         eth->eflags &= ~ETH_FLAG_FIX_APPLIED;

      } else if (eth->eflags & ETH_FLAG_AUTONEG_OFF) {
         eth->eflags &= ~ETH_FLAG_AUTONEG_MASK;
         eth->eflags |= ETH_FLAG_AUTONEG_ON;
         eth_set_info(eth);
      }
      */
      g_libdance.commlinks &= ~COMMLNK_ETH;
   }

   if (g_libdance.commlinks & COMMLNK_ETH)
      COMMLED_MAIN(ledGREEN);
   else
      COMMLED_MAIN(ledRED);

  return 0;
}

static int eth_io_close(io_handle_t* io)
{
   /* unused */
   io = io;
   return 0;
}

static const io_ops_t eth_ops =
{
   .write_fn = NULL,
   .read_fn = eth_io_read,
   .close_fn = eth_io_close
};

static danceerr_t add_eth_io(void)
{
   eth_handle_t* eth = g_libdance.eth;
   io_handle_t* io;
   int fd;

   if (eth == NULL) goto on_error_0;

   fd = eth->nl_fd;
   io = create_io_handle(fd, IOTYPE_ETH, &eth_ops, (void*)eth);
   if (io == NULL) goto on_error_0;

   if (lnklist_add_head(&runtime.ios, io) == 0) goto on_error_1;

   return DANCE_OK;

 on_error_1:
   free_io_handle(io);
 on_error_0:
   return DANCE_ERROR;
}

#endif // ETHERNET_USED

// -------------------------------------------
danceerr_t io_close(io_handle_t* io)
{
   io->ops->close_fn(io);
   free_io_handle(io);
   return DANCE_OK;
}


// maintain set across wait to avoid starvation

static lnklist_for_t fini_io_item(lnklist_item_t* it, void* data)
{
   io_close(it->itmdata);
   return LNKLIST_CONT;
}

danceerr_t io_fini(void)
{
   lnklist_fini(&runtime.ios, fini_io_item, NULL);
   return DANCE_OK;
}

danceerr_t io_init(void) {
   lnklist_init(&runtime.ios, NULL);

   runtime.tmout_us = 0;
   runtime.main_thread = pthread_self();

   if (add_signal_io()) {
      LOG_ERROR("add_signal_io() failed\n");
      return DANCE_ERROR;
   }

  // not an error to fail

#if PCIBUS_USED
   if (add_uirq_io("10ee:eb01"))
      LOG_ERROR("add_uirq_io() failed\n");
LOG_TRACE();
#endif // PCIBUS_USED

#if ETHERNET_USED
   if (add_eth_io())
      LOG_ERROR("add_eth_io() failed\n");
LOG_TRACE();
#endif // ETHERNET_USED

  return DANCE_OK;
}

// ------------------------- timeout functions

static void timeout_update(void)
{
   // increment absolute timeout with respect to current value

   runtime.tmout_abs.tv_sec += runtime.tmout_us / 1000000;
   runtime.tmout_abs.tv_nsec += (runtime.tmout_us % 1000000) * 1000;

   while (runtime.tmout_abs.tv_nsec > 1000000000)
   {
      runtime.tmout_abs.tv_sec += 1;
      runtime.tmout_abs.tv_nsec -= 1000000000;
   }
}

static void timeout_init(void)
{
   clock_gettime(CLOCK_REALTIME, &runtime.tmout_abs);
   timeout_update();
}

static long timeout_diff_now(void)
{
   long            diff_ns;
   struct timespec now;

   clock_gettime(CLOCK_REALTIME, &now);

   diff_ns = (long)runtime.tmout_abs.tv_sec - (long)now.tv_sec;
   diff_ns *= (long)1000000000;
   diff_ns += (long)runtime.tmout_abs.tv_nsec - (long)now.tv_nsec;

   return diff_ns;
}

#define TIMEOUT_RES_NS ((long)100000)

static unsigned int timeout_is_reached(void)
{
   // 100 us resolution
   static const long res_ns = TIMEOUT_RES_NS;

   if (runtime.tmout_us == 0)
      return 0;
   else if (timeout_diff_now() > res_ns)   // why this???
      return 0;
   else
      return 1;
}

__attribute__((unused))
static struct timeval* timeout_get_timeval(void)
{
   /* get timeval from now */

   static const long res_ns = TIMEOUT_RES_NS;
   long diff_ns;

   if (runtime.tmout_us == 0) return NULL;

   diff_ns = timeout_diff_now();
   if (diff_ns <= res_ns) diff_ns = 0;

   runtime.tmout_tmval.tv_sec = diff_ns / (long)1000000000;
   runtime.tmout_tmval.tv_usec = (diff_ns % (long)1000000000) / 1000;

   return &runtime.tmout_tmval;
}

danceerr_t set_runtime_timeout(unsigned int us, void* udata)
{
   runtime.tmout_us = (long)us;
   runtime.tmout_data = udata;
   timeout_init();

   return DANCE_OK;
}

// --------------- timer tasks

timeritem_t* create_timer_item(unsigned int ms, timerfn_t fn, void* arg)
{
   timeritem_t* timer_it = malloc(sizeof(timeritem_t));
   if (timer_it != NULL)
   {
      timer_it->flags = 0;
      timer_it->rel_ms = ms;
      timer_it->exec_fn = fn;
      timer_it->exec_data = arg;
   }
   return(timer_it);
}

timeritem_t* add_task(sigtype_t sigtype, unsigned int ms, timerfn_t fn, void* arg)
{
   if (is_in_runtime_thread())
   {
      if (sigtype == SIGTYPE_ONESHOT)
         return timerlist_add_oneshot(&runtime.timer_list, ms, fn, arg);
      else /* SIGTYPE_PERIODIC */
         return timerlist_add_periodic(&runtime.timer_list, ms, fn, arg);
   }
   else
   {
      timeritem_t* timer_it = create_timer_item(ms, fn, arg);
      if (timer_it != NULL)
         runtime_send_signal(sigtype, timer_it);
      return(timer_it);
   }
}

timeritem_t* add_oneshot_task(unsigned int ms, timerfn_t fn, void* arg)
{
   return(add_task(SIGTYPE_ONESHOT, ms, fn, arg));
}

timeritem_t* restart_oneshot_task(timeritem_t* it)
{
   /* NOTE: assume is_in_runtime_thread() == 1 */
   return timerlist_restart_oneshot(&runtime.timer_list, it);
}

timeritem_t* add_periodic_task(unsigned int ms, timerfn_t fn, void* arg)
{
   return(add_task(SIGTYPE_PERIODIC, ms, fn, arg));
}

/* the following functions add_xxxx_cmdtask() add tasks to be executed
   in the command execution thread */
typedef struct
{
   timerfn_t exec_fn;
   void*     exec_data;
   sigtype_t type;
} cmdtask_t;

void relay_silenttask_execution(cmdtask_t* reltsk) {
   relay_commexecution(reltsk->exec_fn, reltsk->exec_data, TASK_SILENT);
   if (reltsk->type == SIGTYPE_ONESHOT)
      free(reltsk);
}

timeritem_t* add_cmdtask(sigtype_t sigtype, unsigned int ms, timerfn_t fn, void* arg)
{
   cmdtask_t *reltsk = malloc(sizeof(cmdtask_t));

   if (reltsk == NULL) return(NULL);

   reltsk->exec_fn = fn;
   reltsk->exec_data = arg;
   reltsk->type = sigtype;
   return add_task(sigtype, ms, (timerfn_t)relay_silenttask_execution, reltsk);
}

timeritem_t* add_oneshot_cmdtask(unsigned int ms, timerfn_t fn, void* arg)
{
   return(add_cmdtask(SIGTYPE_ONESHOT, ms, fn, arg));
}

timeritem_t* add_periodic_cmdtask(unsigned int ms, timerfn_t fn, void* arg)
{
   return(add_cmdtask(SIGTYPE_PERIODIC, ms, fn, arg));
}


// --------------- task execution functions

void start_commexecution_thread(void)
{
   tqueue_attr_t attr = tqueue_default_attr;

   attr.tqflags |= TQUEUE_COMPL_FD;
   attr.compl_fd = runtime.signal_io->fd;
   attr.compl_signal = SIGTYPE_COMMCLEANUP;

   tqueue_init(&runtime.tq, &attr);
}

danceerr_t relay_commexecution(task_fn_t fn, void* data, uint32_t flags)
{
   if (tqueue_add(&runtime.tq, fn, data, flags) != DANCE_OK)
      return DANCE_ERROR;
   else
      return DANCE_OK;
}


// Wait for an I/O event or timeout
//
danceerr_t io_wait(io_info_t* info)
{
   fd_set          rset;
   fd_set          wset;
   int             fd_max;
   int             nfd;
   lnklist_item_t* pos;
   io_handle_t*    io;

   while (1)
   {
      FD_ZERO(&rset);
      FD_ZERO(&wset);
      fd_max = 0;

      for (pos = runtime.ios.head; pos != NULL; pos = pos->next)
      {
         io = pos->itmdata;

         if (timeout_is_reached())
            goto on_timeout;
         else if (io == runtime.signal_io)
            ; // do nothing for special ios
         else if (io->flags & IOFLAG_OPEN)    // only TCP server
         {
            io->flags &= ~IOFLAG_OPEN;
            info->io = io;
            info->flags = IOFLAG_OPEN;
            return DANCE_OK;
         }
         else if (io->flags & IOFLAG_CLOSE)
         {
            goto on_close;
         }
         else if (io->flags & IOFLAG_READ)
         {
            io->flags    &= ~IOFLAG_READ;
            info->io      = io;
            info->flags   = IOFLAG_READ;
            return DANCE_OK;
         }

         if (!(io->flags & IOFLAG_BLOCKED))
            FD_SET(io->fd, &rset);

TODO  // check if fd_max update is ok after dealing with blocked ios
         if (io->fd > fd_max)
            fd_max = io->fd;

         // complete write buffers which are empty
         if (io->type == IOTYPE_CHANNEL)
         {
            channel_t* chan = IOPTR_TO_CHANNEL(io);
            if (chan->curr_cbufflist != NULL)
            {
               pthread_mutex_lock(&io->wlock);
               if (chan->curr_cbufflist->head == NULL)
               {
                  combuflist_free(chan->curr_cbufflist);
                  if (chan->out_cbufflist.head == NULL)
                     chan->curr_cbufflist = NULL;
                  else
                  {
                     chan->curr_cbufflist = chan->out_cbufflist.head->itmdata;
                     lnklist_unlink_item(chan->out_cbufflist.head);
                  }
                  info->io = io;
                  info->flags = IOFLAG_WRITE;
                  pthread_mutex_unlock(&io->wlock);
                  return DANCE_OK;
               }
               else
               {
                  if (chan->curr_cbufflist->lstdata != NULL)
                  {
                     FD_SET(io->fd, &wset);
                  }
               }
               pthread_mutex_unlock(&io->wlock);
            }
         }
      }
LOG_INFO("Enter select()\n");
   redo_select:
      errno = 0;
      nfd = select(fd_max + 1, &rset, &wset, NULL, timerlist_get_next(&runtime.timer_list));
LOG_INFO("Exit select() fd=%d\n", nfd);
      if (IS_LOG(INFO, 0) && (nfd > 0))
      {
         int i;
         for (i = 0; i <= fd_max; i++)
         {
            if (FD_ISSET(i, &rset))
                LOG_INFO("RD on file desc #%d\n", i);
            if (FD_ISSET(i, &wset))
                LOG_INFO("WR on file desc #%d\n", i);
         }
      }

      if (IS_LOG(INFO, 0))
      {
         timerlist_t* tl;
         lnklist_item_t* list_it;
         int i = 1;

         tl = &runtime.timer_list;
         for (list_it = tl->li.head; list_it != NULL; list_it = list_it->next)
         {
            timeritem_t* timer_cur = (timeritem_t*)list_it->itmdata;
            LOG_INFO("timer_list item #d\n", i++);
            LOG_INFO("\tabs_ms : %u\n", timer_cur->abs_ms);
            LOG_INFO("\trel_ms : %u\n", timer_cur->rel_ms);
            LOG_INFO("\texec_fn: %p\n", timer_cur->exec_fn);
         }
         if (i == 1) {
            LOG_INFO("timer_list is empty\n");
         }
      }

      if (nfd < 0)
      {
         if (errno == EINTR) goto redo_select;
         LOG_ERROR("select error (errno == %d)\n", errno);
         return DANCE_ERROR;
      }
      else if (nfd == 0)
      {
         timerlist_exec_next(&runtime.timer_list);
         if (IS_REBOOT_REQUESTED())
            goto on_timeout;
      }

      for (pos = runtime.ios.head; pos != NULL; pos = pos->next)
      {
         io = pos->itmdata;

         if (FD_ISSET(io->fd, &wset))
         {
            if (io->ops->write_fn(io))
            {
               goto on_close;
            }
         }

// TODO: check if io->flags test is needed here ...
         if (FD_ISSET(io->fd, &rset) && !(io->flags & IOFLAG_BLOCKED))
         {
            if (io->ops->read_fn(io))
            {
               goto on_close;
            }
         }
         continue;
      }
   }
   return DANCE_OK;

 on_close:
   info->io = io;
   info->flags = IOFLAG_CLOSE;
   return DANCE_OK;

 on_timeout:
   timeout_update();
   info->io = runtime.tmout_data;
   info->flags = IOFLAG_TIMEOUT;
   return DANCE_OK;
}


// -------------------------- Main runtime functions

// Initialisation to be done before the application init
//
danceerr_t runtime_init(void)
{
   if (io_init() != DANCE_OK)
   {
      LOG_ERROR("io_init() failure\n");
      return DANCE_ERROR;
   }
   start_commexecution_thread();

   //   set_runtime_timeout(1000000, NULL);
   timerlist_init(&runtime.timer_list);

   return DANCE_OK;
}


//  main loop
//


danceerr_t runtime_loop(void)
{
   io_info_t   io_info;


   while (1)
   {
      // wait for next io related event
      if (io_wait(&io_info) != DANCE_OK)
      {
         LOG_ERROR("io_wait() failure\n");
         return DANCE_ERROR;
      }

      switch (io_info.flags)
      {
         case IOFLAG_OPEN:
            LOG_INFO("new connection\n");
            break ;

         case IOFLAG_READ:
         {
            LOG_INFO("channel read\n");
            if (io_info.io->type == IOTYPE_CHANNEL)
            {
               process_read_channel(IOPTR_TO_CHANNEL(io_info.io));
            }
            break ;
         }

         case IOFLAG_WRITE:
            LOG_INFO("new buffer written\n");
            break ;

         case IOFLAG_CLOSE:
            LOG_INFO("connection closed\n");
            io_close(io_info.io);
            break ;

         case IOFLAG_TIMEOUT:
            LOG_INFO("timeout elapsed\n");
            CHECK_FOR_REBOOT();
            break ;

         default:
            break ;
      }
   }
   set_runtime_timeout(0, NULL);

TODO  // review all this ending scheme...
//   io_close(server_io);      // is this needed?
   io_fini();
   return DANCE_OK;
}

