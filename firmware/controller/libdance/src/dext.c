/* dance extension */
/* libdance/doc/HOWTOs/dext-HOWTO.txt */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <math.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "libdance.h"

#include "libtcc.h"
#include "dext.h"
#include "dext_so.h"


#define DEXT_DIR "/app/bin/dance/dext/"
#define DEXT_DOT_SO ".so"
#define DEXT_DOT_C ".c"
/* must be max between all possible DEXT_DOT_xxx */
#define DEXT_DOT_XXX_MAX_SIZE 3


/* realtime tasking */

#include <signal.h>
#include <time.h>


typedef struct rtask_handle
{
  timer_t timerid;
  int signo;

#define DEXT_ARG_COUNT 5
  dext_handle_t* dext;
  double args[DEXT_ARG_COUNT];

} rtask_handle_t;

static rtask_handle_t g_rtask;

static int rtask_init(rtask_handle_t* rt)
{
  struct sigevent sev;

  rt->signo = SIGRTMAX;

  sev.sigev_notify = SIGEV_SIGNAL;
  sev.sigev_signo = rt->signo;
  sev.sigev_value.sival_ptr = (void*)rt;
  if (timer_create(CLOCK_REALTIME, &sev, &rt->timerid) == -1) return -1;

  rt->dext = NULL;

  return 0;
}

static void rtask_handler(int sig, siginfo_t* si, void* uc)
{
  rtask_handle_t* const rt = si->si_value.sival_ptr;
  /* if (rt->handler(rt->args)) signal(sig, SIG_IGN); */
  rt->dext->main(rt->args);
}

static int rtask_start
(rtask_handle_t* rt, dext_handle_t* dext, unsigned int us_period)
{
  struct itimerspec its;
  sigset_t mask;
  struct sigaction sa;
  uint64_t ns_period;

  /* block timer during initialization */

  sa.sa_flags = SA_SIGINFO;
  sa.sa_sigaction = rtask_handler;
  sigemptyset(&sa.sa_mask);
  if (sigaction(rt->signo, &sa, NULL) == -1) return -1;

  sigemptyset(&mask);
  sigaddset(&mask, rt->signo);
  if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1) return -1;

  /* enable and unlbock timer */

  rt->dext = dext;

  ns_period = (uint64_t)us_period * (uint64_t)1000;
  its.it_value.tv_sec = ns_period / (uint64_t)1000000000;
  its.it_value.tv_nsec = ns_period % (uint64_t)1000000000;
  its.it_interval.tv_sec = its.it_value.tv_sec;
  its.it_interval.tv_nsec = its.it_value.tv_nsec;
  if (timer_settime(rt->timerid, 0, &its, NULL) == -1) return -1;
  if (sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1) return -1;

  return 0;
}

static int rtask_stop(rtask_handle_t* rt)
{
  sigset_t mask;
  struct sigaction sa;

  sa.sa_flags = 0;
  sa.sa_handler = SIG_IGN;
  sigemptyset(&sa.sa_mask);
  if (sigaction(rt->signo, &sa, NULL) == -1) return -1;

  sigemptyset(&mask);
  sigaddset(&mask, rt->signo);
  if (sigprocmask(SIG_SETMASK, &mask, NULL) == -1) return -1;

  rt->dext = NULL;

  return 0;
}

static void rtask_fini(rtask_handle_t* rt)
{
  rtask_stop(rt);
  timer_delete(rt->timerid);
}

static unsigned int rtask_is_started(rtask_handle_t* rt)
{
  return rt->dext != NULL;
}


/* allocator, contains extensions opened in memory */

#define DEXT_MAX_COUNT 32
static dext_handle_t g_dext[DEXT_MAX_COUNT];

static unsigned int dext_is_free(const dext_handle_t* dext)
{
  return (dext->flags & DEXT_FLAG_FREE);
}

static dext_handle_t* dext_alloc(void)
{
  dext_handle_t* dext;
  size_t i;

  /* find free dext */
  dext = NULL;
  for (i = 0; (i != DEXT_MAX_COUNT) && (g_dext[i].id != NULL); ++i) ;
  if (i == DEXT_MAX_COUNT) return NULL;
  dext = &g_dext[i];

  /* clear free flag */
  dext->flags = 0;

  return dext;
}

static void dext_free(dext_handle_t* dext)
{
  dext->flags |= DEXT_FLAG_FREE;
}

static const char* dext_make_path(dext_handle_t* dext, const char* ext)
{
  strcpy(dext->path + sizeof(DEXT_DIR) - 1 + dext->id_len, ext);
  return dext->path;
}

/* shared object loading */

static int dext_load_so(dext_handle_t* dext)
{
  dext_ops_t ops;
  int (*f)(const dext_ops_t*);

  if (dext->ldso != NULL)
  {
    /* already loaded, check if reloading needed */

    if ((dext->flags & DEXT_FLAG_LDSO_DIRTY) == 0) return 0;

    dlclose(dext->ldso);
    dext->ldso = NULL;

    dext->flags |= DEXT_FLAG_LDSO_DIRTY;
  }

  dext->flags &= ~(DEXT_FLAG_INIT | DEXT_FLAG_FINI | DEXT_FLAG_ERR);

  dext->ldso = dlopen(dext_make_path(dext, DEXT_DOT_SO), RTLD_NOW);
  if (dext->ldso == NULL) goto on_error_0;

  dext->flags &= ~DEXT_FLAG_LDSO_DIRTY;

  /* resolve routines */

  f = dlsym(dext->ldso, "register_ops");
  if (f == NULL) goto on_error_1;

  dext->init = dlsym(dext->ldso, "init");
  dext->fini = dlsym(dext->ldso, "fini");

  dext->main = dlsym(dext->ldso, "main");
  if (dext->main == NULL) goto on_error_1;

  /* register ops */

  ops.vers = DEXT_OPS_VERS;

  ops.printf = printf;
  ops.malloc = malloc;
  ops.free = free;

  ops.sin = sin;
  ops.asin = asin;
  ops.cos = cos;
  ops.acos = acos;
  ops.tan = tan;
  ops.atan = atan;
  ops.pow = pow;
  ops.sqrt = sqrt;
  ops.fabs = fabs;

  if (f(&ops)) goto on_error_1;

  return 0;

 on_error_1:
  dlclose(dext->ldso);
 on_error_0:
  return -1;
}

/* code compilation */

static void on_tcc_error(void* err, const char* msg)
{
  /* skip this warning */
#define SOFTFP_WARN "<string>:1: warning: soft float ABI"
  if (strncmp(SOFTFP_WARN, msg, sizeof(SOFTFP_WARN) - 1) == 0) return ;
  /* LOG_ERROR("compilation error: %s\n", msg); */
  *(int*)err = -1;
}

static int dext_compile_code(dext_handle_t* dext, const dext_sym_t* app_syms)
{
  const dext_sym_t default_syms[] =
  {
    /* math symbols */
    { "sin", "double sin(double);", sin },
    { "asin", "double asin(double);", asin },
    { "cos", "double cos(double);", cos },
    { "acos", "double acos(double);", acos },
    { "tan", "double tan(double);", tan },
    { "atan", "double atan(double);", atan },
    { "pow", "double pow(double, double);", pow },
    { "sqrt", "double sqrt(double);", sqrt },
    { "fabs", "double fabs(double);", fabs },

    /* runtime symbols */

    /* last symbol */
    DEXT_INVALID_SYM
  };

  size_t nsym;
  size_t i;
  size_t j;
  size_t new_len;
  char* new_code;
  char* new_p;
  int err = -1;

  if (dext->tcc != NULL)
  {
    /* already compiled, check if recompil needed */

    if ((dext->flags & DEXT_FLAG_CODE_DIRTY) == 0) return 0;

    tcc_delete(dext->tcc);
    dext->tcc = NULL;

    dext->flags |= DEXT_FLAG_CODE_DIRTY;
  }

  dext->flags &= ~(DEXT_FLAG_INIT | DEXT_FLAG_FINI | DEXT_FLAG_ERR);

  /* allocate and setup tcc state */

  dext->tcc = tcc_new();
  if (dext->tcc == NULL) goto on_error_0;

  tcc_set_options(dext->tcc, "-static -nostdlib -nostdinc");
  tcc_set_output_type(dext->tcc, TCC_OUTPUT_MEMORY);

  /* prepend symbol decls to code */

  new_len = 0;

  for (i = 0; default_syms[i].id != NULL; ++i)
    new_len += strlen(default_syms[i].decl) + 1;

  nsym = i;

  if (app_syms != NULL)
  {
    for (i = 0; app_syms[i].id != NULL; ++i)
      new_len += strlen(app_syms[i].decl) + 1;
    nsym += i;
  }

  new_len += strlen(dext->code);
  new_code = malloc(new_len + 1);
  if (new_code == NULL) goto on_error_1;

  new_p = new_code;

  for (i = 0; default_syms[i].id != NULL; ++i)
  {
    const dext_sym_t* const sym = &default_syms[i];
    for (j = 0; sym->decl[j]; ++j, ++new_p) *new_p = sym->decl[j];
    *(new_p++) = '\n';
    tcc_add_symbol(dext->tcc, sym->id, sym->addr);
  }

  if (app_syms != NULL)
  {
    for (i = 0; app_syms[i].id != NULL; ++i)
    {
      const dext_sym_t* const sym = &app_syms[i];
      for (j = 0; sym->decl[j]; ++j, ++new_p) *new_p = sym->decl[j];
      *(new_p++) = '\n';
      tcc_add_symbol(dext->tcc, sym->id, sym->addr);
    }
  }

  strcpy(new_p, dext->code);

  /* compile and relocate the code */

  err = 0;
  tcc_set_error_func(dext->tcc, &err, on_tcc_error);
  if ((tcc_compile_string(dext->tcc, new_code) == -1) || (err))
  {
    err = -1;
    goto on_error_2;
  }

  if (tcc_relocate(dext->tcc, TCC_RELOCATE_AUTO) < 0)
  {
    err = -1;
    goto on_error_2;
  }

  /* resolve entry points */

  dext->main = tcc_get_symbol(dext->tcc, "main");
  if (dext->main == NULL)
  {
    err = -1;
    goto on_error_2;
  }

  dext->init = tcc_get_symbol(dext->tcc, "init");
  dext->fini = tcc_get_symbol(dext->tcc, "fini");

  /* commit */

  dext->flags &= ~DEXT_FLAG_CODE_DIRTY;

 on_error_2:
  free(new_code);

  if (err)
  {
  on_error_1:
    tcc_delete(dext->tcc);
    dext->tcc = NULL;
  }

 on_error_0:
  return err;
}

static int dext_append_code
(dext_handle_t* dext, const char* code, size_t add_nl)
{
  /* append code */

  const size_t code_len = strlen(code);
  const size_t new_len = dext->code_len + code_len + add_nl;

  dext->code = realloc(dext->code, new_len + 1);
  if (dext->code == NULL) return -1;

  memcpy(dext->code + dext->code_len, code, code_len);
  if (add_nl) dext->code[dext->code_len + code_len] = '\n';
  dext->code[new_len] = 0;
  dext->code_len = new_len;

  dext->flags |= DEXT_FLAG_CODE_DIRTY;

  return 0;
}

static void dext_clear_code(dext_handle_t* dext)
{
  /* clear extension code */

  if (dext->code != NULL) free(dext->code);
  dext->code = NULL;
  dext->code_len = 0;

  dext->flags &= DEXT_FLAG_CODE_DIRTY;
}

static int dext_store_file(const char* path, const void* data, size_t size)
{
  int fd;
  int err = -1;
  ssize_t nwrite;

  fd = open(path, O_RDWR | O_TRUNC | O_CREAT, 0755);
  if (fd == -1) goto on_error;

  nwrite = write(fd, data, size);

  close(fd);

  if (nwrite != (ssize_t)size) goto on_error;

  err = 0;

 on_error:
  return err;
}

static int dext_store_code(dext_handle_t* dext)
{
  const char* const path = dext_make_path(dext, DEXT_DOT_C);
  return dext_store_file(path, dext->code, dext->code_len);
}

static int dext_store_so(dext_handle_t* dext, const void* data, size_t size)
{
  const char* const path = dext_make_path(dext, DEXT_DOT_SO);
  return dext_store_file(path, data, size);
}


static int dext_stat_so(dext_handle_t* dext)
{
  const char* const path = dext_make_path(dext, DEXT_DOT_SO);
  struct stat st;
  if (stat(path, &st)) return -1;
  return 0;
}


dext_handle_t* dext_open(const char* id)
{
  dext_handle_t* dext;
  struct stat st;
  int fd;
  size_t path_len;
  size_t nread;

  dext = dext_alloc();
  if (dext == NULL) goto on_error_0;

  dext->flags |= DEXT_FLAG_CODE_DIRTY | DEXT_FLAG_LDSO_DIRTY;
  dext->code = NULL;
  dext->tcc = NULL;
  dext->ldso = NULL;

  /* allocate a path large enough to hold both extensions */

  dext->id_len = strlen(id);
  path_len = sizeof(DEXT_DIR) - 1 + dext->id_len + DEXT_DOT_XXX_MAX_SIZE;
  dext->path = malloc(path_len + 1);
  if (dext->path == NULL) goto on_error_1;
  dext->id = dext->path + sizeof(DEXT_DIR) - 1;

  memcpy(dext->path, DEXT_DIR, sizeof(DEXT_DIR) - 1);
  memcpy(dext->path + sizeof(DEXT_DIR) - 1, id, dext->id_len);

  /* read source contents if exist */

  fd = open(dext_make_path(dext, DEXT_DOT_C), O_RDONLY);
  if (fd == -1) goto on_success;

  if (fstat(fd, &st))
  {
    close(fd);
    goto on_error_2;
  }

  dext->code_len = st.st_size;
  dext->code = malloc(dext->code_len + 1);
  if (dext->code == NULL)
  {
    close(fd);
    goto on_error_2;
  }

  dext->code[dext->code_len] = 0;

  nread = (size_t)read(fd, dext->code, dext->code_len);
  close(fd);

  if (nread != dext->code_len) goto on_error_2;

 on_success:
  return dext;

 on_error_2:
  free(dext->path);
 on_error_1:
  dext_free(dext);
 on_error_0:
  return NULL;
}

void dext_close(dext_handle_t* dext)
{
  if (dext->flags & DEXT_FLAG_RUN)
  {
    rtask_handle_t* const rt = &g_rtask;
    rtask_stop(rt);
  }

  if (dext->fini != NULL)
  {
    if ((dext->flags & (DEXT_FLAG_FINI | DEXT_FLAG_ERR)) == 0)
    {
      dext->fini(NULL);
      dext->flags |= DEXT_FLAG_FINI;
    }
  }

  if (dext->code != NULL) free(dext->code);
  if (dext->tcc != NULL) tcc_delete(dext->tcc);
  if (dext->ldso != NULL) dlclose(dext->ldso);

  free(dext->path);

  dext_free(dext);
}

static int dext_delete(dext_handle_t* dext)
{
  /* delete an extension from disk */

  unlink(dext_make_path(dext, DEXT_DOT_C));
  unlink(dext_make_path(dext, DEXT_DOT_SO));

  return 0;
}

static dext_handle_t* dext_find(const char* id)
{
  /* find an extension by id */

  const size_t id_len = strlen(id);
  size_t i;

  for (i = 0; i != DEXT_MAX_COUNT; ++i)
  {
    dext_handle_t* const dext = &g_dext[i];
    if (dext_is_free(dext)) continue ;
    if (id_len != dext->id_len) continue ;
    if (memcmp(dext->id, id, id_len) == 0) return dext;
  }

  return NULL;
}


/* constructors */

int dext_init(void)
{
  size_t i;

  /* create dext directory if does not exist */
  mkdir(DEXT_DIR, 0755);

  for (i = 0; i != DEXT_MAX_COUNT; ++i)
  {
    dext_handle_t* const dext = &g_dext[i];
    dext->flags = DEXT_FLAG_FREE;
  }

  if (rtask_init(&g_rtask)) return -1;

  return 0;
}


void dext_fini(void)
{
  size_t i;

  if (rtask_is_started(&g_rtask)) rtask_stop(&g_rtask);
  rtask_fini(&g_rtask);

  for (i = 0; i != DEXT_MAX_COUNT; ++i)
  {
    dext_handle_t* const dext = &g_dext[i];
    if (dext_is_free(dext)) continue ;
    dext_close(dext);
  }
}


/* commands */

static int parse_dext_args(double* args, unsigned int n)
{
  unsigned int i;

  i = 0;
  while ((I_FMT_NUM() > 0) && (I_FMT_TYPE() == FMT_FLOAT))
  {
    if (i == n) return -1;
    args[i++] = I_FLOAT();
  }

  return 0;
}

void commexec_DEXT(void)
{
  /* parse */

  const char* const id = I_LABEL();
  dext_handle_t* dext;

  dext = dext_find(id);

  while (1)
  {
    const int fmt_num = I_FMT_NUM();

    if (fmt_num < 0) break ;

    if (fmt_num == 0)
    {
      I_NEXT_FMT();
      continue ;
    }

    switch (I_FMT_TYPE())
    {
    case FMT_TAG:
    {
      if (I_FMT_CHKNAME("LINE"))
      {
        if (dext == NULL)
        {
          dext = dext_open(id);
          if (dext == NULL)
          {
            errorf("could not create extension");
            return ;
          }
        }

        I_NEXT_FMT();

        if (dext_append_code(dext, I_STRING(), 1))
        {
          errorf("could not add line");
          return ;
        }
      }
      else if (I_FMT_CHKNAME("CLEAR"))
      {
        if (dext == NULL)
        {
          errorf("extension not found");
          return ;
        }

        I_NEXT_FMT();

        dext_clear_code(dext);
      }
      else if (I_FMT_CHKNAME("COMP"))
      {
        if (dext == NULL)
        {
          errorf("extension not found");
          return ;
        }

         if (dext->flags & DEXT_FLAG_RUN)
         {
          errorf("extension is running");
          return ;
         }

        I_NEXT_FMT();

        if (dext_compile_code(dext, g_app_dext_syms))
        {
          errorf("compilation error");
          return ;
        }
      }
      else if (I_FMT_CHKNAME("EXEC"))
      {
        double x[DEXT_ARG_COUNT];
         unsigned int is_rtask = 0;
         unsigned int us_period = 0;

        if (dext == NULL)
        {
           dext = dext_open(id);
           if (dext == NULL)
           {
             errorf("extension not found");
             return ;
           }
        }

         if (dext->flags & DEXT_FLAG_RUN)
         {
          errorf("extension already running");
          return ;
         }

         /* if so exists and dirty, reload it */

         if (dext_stat_so(dext) == 0)
         {
           if (dext_load_so(dext))
           {
             errorf("error loading shared object");
             return ;
           }
         }
         else if (dext->code != NULL)
         {
           if (dext_compile_code(dext, g_app_dext_syms))
           {
             errorf("compilation error");
             return ;
           }
         }
         else
         {
           errorf("nothing to execute");
           return ;
         }

         /* init extension */

         if ((dext->init != NULL) && ((dext->flags & DEXT_FLAG_INIT) == 0))
         {
           if (dext->init(NULL))
           {
             dext->flags |= DEXT_FLAG_ERR;
             errorf("dext init error");
             return ;
           }

           dext->flags |= DEXT_FLAG_INIT;
         }

        I_NEXT_FMT();

         /* FIXME: skip composite */
         while (I_FMT_NUM() == 0) I_NEXT_FMT();

         /* parse frequency */

         if ((I_FMT_NUM() > 0) && (I_FMT_TYPE() == FMT_TAG))
         {
           I_NEXT_FMT();

           us_period = (unsigned int) ceil(1000000.0 / (double)(I_INTEGER() * I_UNIT()));
           if (us_period == 0)
           {
             errorf("invalid frequency");
             return ;
           }

           is_rtask = 1;
         }

         if (parse_dext_args(x, DEXT_ARG_COUNT))
         {
          errorf("too many arguments");
          return ;
         }

         /* frequency specified, start task if not yet started */

         if (is_rtask)
         {
           rtask_handle_t* const rt = &g_rtask;

           if (rtask_is_started(rt))
           {
             errorf("task already started");
             return ;
           }

           memcpy(rt->args, x, sizeof(rt->args));

           if (rtask_start(rt, dext, us_period))
           {
             errorf("rtask start error");
             return ;
           }

           dext->flags |= DEXT_FLAG_RUN;
         }
         else
         {
           /* one shot */
           dext->main(x);
         }
      }
      else if (I_FMT_CHKNAME("SAVE"))
      {
         I_NEXT_FMT();

         if (dext_store_code(dext))
         {
           errorf("error storing code");
           return ;
         }
      }
      else if (I_FMT_CHKNAME("DEL"))
      {
         I_NEXT_FMT();

         if (dext == NULL)
         {
           dext = dext_open(id);
           if (dext == NULL)
           {
             errorf("error opening extension");
             return ;
           }
         }

         if (dext->flags & DEXT_FLAG_RUN)
         {
          errorf("extension running");
          return ;
         }

         if (dext_delete(dext))
         {
           errorf("deletion error");
           return ;
         }
      }
      else if (I_FMT_CHKNAME("STOP"))
      {
         rtask_handle_t* const rt = &g_rtask;

         I_NEXT_FMT();

         if (dext == NULL)
         {
           errorf("extension not found");
           return ;
         }

         if ((dext->flags & DEXT_FLAG_RUN) == 0)
         {
           errorf("extension not started");
           return ;
         }

         rtask_stop(rt);
         dext->flags &= ~DEXT_FLAG_RUN;
      }
      break ;
    }

    default:
    {
      errorf("invalid command");
      return ;
    }
    }
  }

}

void commexec_qDEXT(void)
{
  const char* const id = I_LABEL();

  if (id == DEFAULT_LABEL)
  {
    size_t i;
    DIR* dir;

    /* list in memory dexts */
    for (i = 0; i != DEXT_MAX_COUNT; ++i)
    {
      dext_handle_t* const dext = &g_dext[i];
      if (dext_is_free(dext)) continue ;
      dext->id[dext->id_len] = 0;
      answerf("%s\n", dext->id);
    }

    /* list dext stored on disk */
    dir = opendir(DEXT_DIR);
    if (dir == NULL) return ;
    while (1)
    {
      struct dirent* const dent = readdir(dir);
      size_t len;
      if (dent == NULL) break ;
      len = strlen(dent->d_name);
      if (len <= 2) continue ;
      if (strcmp(dent->d_name + len - 3, DEXT_DOT_SO) == 0) len -= 3;
      else if (strcmp(dent->d_name + len - 2, DEXT_DOT_C) == 0) len -= 2;
      else continue ;
      dent->d_name[len] = 0;
      answerf("%s\n", dent->d_name);
    }
    closedir(dir);
  }
  else
  {
    dext_handle_t* dext = dext_find(id);
    if (dext == NULL)
    {
      errorf("invalid id");
      return ;
    }

    if (dext->code != NULL) answerf("%s", dext->code);
    answerf("\n");
  }
}

void commexec_bDEXT(void)
{
  const char* const id = I_LABEL();
  dext_handle_t* dext;
  unsigned int is_c;
  binary_t* bin;
  unsigned char* data;
  size_t size;

  dext = dext_find(id);
  if (dext == NULL)
  {
    dext = dext_open(id);
    if (dext == NULL)
    {
      errorf("could not create extension");
      return ;
    }
  }

  is_c = 0;

  while (1)
  {
    const int fmt_num = I_FMT_NUM();

    if (fmt_num < 0) break ;

    if (fmt_num == 0)
    {
      I_NEXT_FMT();
      continue ;
    }

    switch (I_FMT_TYPE())
    {
    case FMT_TAG:
    {
      if (I_FMT_CHKNAME("C"))
      {
         is_c = 1;
         I_NEXT_FMT();
      }
      else if (I_FMT_CHKNAME("SO"))
      {
         I_NEXT_FMT();
      }
    }
    }
  }

  /* alloc one more for c code */

  bin = get_binary_com_info();
  size = bin->unit * bin->size;
  data = malloc(size + 1);
  if (data == NULL)
  {
    errorf("memory allocation error");
    return ;
  }

  if (get_binary_data(data, bin->unit, bin->size) < 0)
  {
    errorf("data retrieval error");
    free(data);
    return ;
  }

  if (is_c)
  {
    /* free existing code */

    if (dext->code != NULL) free(dext->code);

    data[size] = 0;
    dext->code = (char*)data;
    dext->code_len = size;

    dext->flags |= DEXT_FLAG_CODE_DIRTY;
  }
  else
  {
    /* store shared object on disk */

    const int err = dext_store_so(dext, data, size);
    free(data);
    if (err)
    {
      errorf("error storing the shared object");
      return ;
    }

    dext->flags |= DEXT_FLAG_LDSO_DIRTY;
  }
}
