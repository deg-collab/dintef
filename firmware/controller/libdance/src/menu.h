/* menu.h */


#ifndef _MENU_H_
#define _MENU_H_

#include "libdance.h"

#include "comm.h"
#include "commled.h"
#include "btest.h"
#include "dinfo.h"
#include "dconfig.h"


#ifndef APPMENU_LIST
   #define APPMENU_LIST
   #define APPCOMM_USED 0
#else
   #define APPCOMM_USED 1
#endif

/********************/
/* Menu definitions */
/********************/

#define _B (_BASIC_COMMAND)
#define _A (_ADVANCED_COMMAND)
#define _E (_EXPERT_COMMAND)
#define _T (_TEST_COMMAND)
#define _H (_HIDDEN_COMMAND)
#define _I (_INITFAIL_COMMAND)


#define fmtLABEL  "L"
#define fmtSTR    "S"
#define fmtNONE   NULL
#define fmtINT    "I"


/* TODO: use tag for now. long term will use a path */
/* TODO: based scheme to address system components */
#define fmtREBOOT "{ T#CPU | T#FPGA | T#SOFT  o(T#DELAY uF100) }"

#define fmtqREG  "o{oC#DFRMT o(C#REG | (L opI))}"
#define fmtqMEM  "o{oC#DFRMT o(L oI oI)}"
#define fmtMEM   "L oI oI ((T#FILL oI) | T#CLEAR)"
#define fmtqI2C  "o(L oL onuI256)"
#define fmtqHELP "o(C#HELP | L)"

#define COMMONMENU_LIST  \
   MENU_IT(qHELP,      fmtqHELP,   fmtSTR,    _I | _B, "Return list of commands [{BASIC | EXPERT | TEST | ALL}]") \
   MENU_IT( ECHO,      fmtNONE,    fmtNONE,   _I | _E, "Select interactive mode")  \
   MENU_IT( NOECHO,    fmtNONE,    fmtNONE,   _I | _E, "Cancel interactive mode")  \
   MENU_IT( COMMCLOSE, fmtNONE,    fmtNONE,        _E, "Close communications") \
   MENU_IT( IP,        fmtSTR,     fmtNONE,        _E, "Configure ethernet IP") \
   MENU_IT(qIP,        "oC#IP",    fmtSTR,         _E, "Return ethernet IP address") \
   MENU_IT( REBOOT,    fmtREBOOT,  fmtNONE,   _I | _E, "Reboot system component") \
   MENU_IT( DEBUG,     fmtINT,     fmtNONE,   _I | _E, "Set debug level") \
   MENU_IT(qDEBUG,     fmtNONE,    fmtINT,    _I | _E, "Return debug level") \
   MENU_IT(qREG,       fmtqREG,    fmtSTR,         _T, "Read register (ex:\"?REG 2:0x0c\" or \"?REG CSR3\")") \
   MENU_IT(qbREG,      "L pI",     fmtNONE,        _T, "Binary read register (ex:\"?*REG 2:0x0c 100\" or \"?*REG CSR3 100\")") \
   MENU_IT( REG,       "L I",      fmtNONE,        _T, "Write register (ex:\"REG 2:0x0c 0x10\" or \"REG CSR3 255\")") \
   MENU_IT( bREG,      "L",        fmtNONE,        _T, "Binary write to FPGA (ex:\"*REG 2:0x0c\" or \"*REG CSR3\")") \
   MENU_IT(qMEM,       fmtqMEM,    fmtSTR,         _T, "Dump data memory area (ex:\"?MEM ...\" or \"?MEM BRAM\")") \
   MENU_IT( MEM,       fmtMEM,     fmtNONE,        _T, "Write data in memory area (ex:\"MEM ...\" or \"MEM BRAM FILL 0\")") \
   MENU_IT(qbMEM,      "L I",      fmtNONE,        _T, "Binary memory read from FPGA (ex:\"?*MEM ...\" or \"?*MEM BRAM\")") \
   MENU_IT( bMEM,      "L",        fmtNONE,        _T, "Binary memory write to FPGA (ex:\"*MEM ...\" or \"*MEM BRAM\")") \
   MENU_IT( bPROG,     fmtNONE,    fmtNONE,   _I | _E, "Upload code module. Must be a gziped firmware image.") \
   MENU_IT( qPROG,     fmtNONE,    fmtSTR,    _I | _E, "Return current status of code module programmation.") \
   MENU_IT( _IDENTPROG, "L S oS",   fmtSTR,        _H, "Program identification on chip memory") \
   MENU_IT(qI2C,       fmtqI2C,    fmtSTR,         _T, "Process an I2C transaction.") \
   DAQ_MENU_LIST \
   DEXT_MENU_LIST \
   DERR_MENU_LIST \
   BTEST_MENU_LIST \
   DINFO_MENU_LIST \
   DCONFIG_MENU_LIST \
   RASHPA_MENU_LIST \


#define COMMBOOLEAN  \
   BOOLDEF(YES,  NO)   \
   BOOLDEF(Yes,  No)   \
   BOOLDEF(Y,    N)    \
   BOOLDEF(1,    0)    \
   BOOLDEF(ON,   OFF)  \
   BOOLDEF(On,   Off)  \
   BOOLDEF(TRUE, FALSE)\
   BOOLDEF(True, False)\
   BOOLDEF(T,    F)    \
   BOOLDEF(IN,   OUT)  \
   BOOLDEF(In,   Out)  \
   BOOLDEF(HIGH, LOW)  \
   BOOLDEF(High, Low)  \

#define HELP_BASIC    0
#define HELP_EXPERT   1
#define HELP_TEST     2
#define HELP_ALL      3
#define HELP_LIST {"BASIC", "EXPERT", "TEST", "ALL"}

#define DFRMT_HEXA       0
#define DFRMT_SIGNED     1
#define DFRMT_UNSIGNED   2
#define DFRMT_LIST  {"HEXA", "SIGNED", "UNSIGNED"}
#define DFRMT_BINARY     3

#define REG_ALL       0
#define REG_EBONE     1
#define REG_APPL      2
#define REG_BASIC     3
#define REG_SPECIAL   4
#define REG_LIST  {"ALL", "EBONE", "APPL", "BASIC", "SPECIAL"}

#define IP_HOSTNAME   0
#define IP_ADDRESS    1
#define IP_NETMASK    2
#define IP_GATEWAY    3
#define IP_BROADCAST  4
#define IP_LIST {"HOSTNAME", "ADDRESS", "NETMASK", "GATEWAY", "BROADCAST"}

#define POL_NORMAL   0
#define POL_INVERTED 1
#define POLARITY_LIST {"NORMAL", "INVERTED"}

#define COMMLISTS   \
   DANCELIST(HELP)     \
   DANCELIST(REG)      \
   DANCELIST(DFRMT)    \
   DANCELIST(IP)       \
   DANCELIST(POLARITY) \
   DAQ_LISTS	        \
   DEXT_LISTS	        \
   DERR_LISTS          \
   DINFO_LISTS         \
   RASHPA_LISTS         \
//   COMMLED_LISTS

#define TIME_UNITS \
   UNITDEF(sec, 1.0)   \
   UNITDEF(s,   1.0)   \
   UNITDEF(ms,  1e-3)  \
   UNITDEF(us,  1e-6)  \
   UNITDEF(ns,  1e-9)  \
   UNITDEF(ps,  1e-12) \
   UNITDEF(fs,  1e-15) \
   UNITDEF(as,  1e-18) \
   UNITDEF(min,    60) \
   UNITDEF(m,      60) \
   UNITDEF(hour, 3600) \
   UNITDEF(h,    3600) \
   UNITDEF(day,  24*3600)     \
   UNITDEF(d,    24*3600)     \
   UNITDEF(year, 365*24*3600) \
   UNITDEF(y,    365*24*3600) \

#define FREQ_UNITS \
   UNITDEF(Hz,  1.0)   \
   UNITDEF(kHz, 1e+3)  \
   UNITDEF(MHz, 1e+6)  \
   UNITDEF(GHz, 1e+9)  \
   UNITDEF(THz, 1e+12) \

#define DIST_UNITS \
   UNITDEF(m,  1.0)   \
   UNITDEF(mm, 1e-3)  \
   UNITDEF(um, 1e-6)  \
   UNITDEF(nm, 1e-9)  \
   UNITDEF(pm, 1e-12) \
   UNITDEF(fm, 1e-15) \
   UNITDEF(am, 1e-18) \
   UNITDEF(km, 1e+3)  \

#define VOLT_UNITS \
   UNITDEF(V,  1.0)   \
   UNITDEF(mV, 1e-3)  \
   UNITDEF(uV, 1e-6)  \
   UNITDEF(nV, 1e-9)  \
   UNITDEF(pV, 1e-12) \
   UNITDEF(fV, 1e-15) \
   UNITDEF(aV, 1e-18) \
   UNITDEF(kV, 1e+3)  \
   UNITDEF(MV, 1e+6)  \

#define CURR_UNITS \
   UNITDEF(A,  1.0)   \
   UNITDEF(mA, 1e-3)  \
   UNITDEF(uA, 1e-6)  \
   UNITDEF(nA, 1e-9)  \
   UNITDEF(pA, 1e-12) \
   UNITDEF(fA, 1e-15) \
   UNITDEF(aA, 1e-18) \
   UNITDEF(kA, 1e+3)  \
   UNITDEF(MA, 1e+6)  \

#define CAPA_UNITS \
   UNITDEF(F,  1.0)   \
   UNITDEF(mF, 1e-3)  \
   UNITDEF(uF, 1e-6)  \
   UNITDEF(nF, 1e-9)  \
   UNITDEF(pF, 1e-12) \
   UNITDEF(fF, 1e-15) \
   UNITDEF(aF, 1e-18) \

#define COMMUNITS    \
   DANCEUNITS(TIME)   \
   DANCEUNITS(FREQ)   \
   DANCEUNITS(DIST)   \
   DANCEUNITS(VOLT)   \
   DANCEUNITS(CURR)   \
   DANCEUNITS(CAPA)   \


#include "parsing.h"



#define FIRST_COMMONCOMM_CODE 10000

#define MENU_IT(it, infmt, outfmt, modes, help) cmd_comm_ ## it,
enum commoncommands {_FCCC = FIRST_COMMONCOMM_CODE - 1,
                      COMMONMENU_LIST  LAST_COMMONCOMM_CODE};
#undef MENU_IT

#define MENU_IT(it, infmt, outfmt, modes, help) extern void commexec_ ## it(void);
COMMONMENU_LIST
#undef MENU_IT

extern menuitem_t commonmenu[];
extern const int  commonmenu_sz;


// Generate first a unique command identifier
#define MENU_IT(it, inpar, outpar, modes, help) cmd_ ## it,
enum applicationcommands {APPMENU_LIST LAST_APPCOMM_CODE};
#undef MENU_IT

#define MENU_IT(it, inpar, outpar, modes, help) extern void exec_ ## it(void);
APPMENU_LIST
#undef MENU_IT

#define MENU_IT(it, inpar, outpar, modes, help) \
   extern void help_ ## it(void) __attribute__ ((weak));
APPMENU_LIST
COMMONMENU_LIST
#undef MENU_IT


#ifdef __INCLUDE_DANCE_DECLARATIONS

   // Declare boardmenu structure
   #define MENU_IT(it, infmt, outfmt, modes, helpstr) { \
      .item = cmd_ ## it,   \
      .comm_str = #it,      \
      .comm_len = sizeof(#it) - 1, \
      .comm_funct = (void (*)(void *))exec_ ## it, \
      .help_funct = (void (*)(void))help_ ## it, \
      .input_fmt = infmt,   \
      .output_fmt = outfmt, \
      .flags = modes,       \
      .help = helpstr       \
   },
   menuitem_t appmenu_normal[]  = {APPMENU_LIST};
   #undef MENU_IT

   menuitem_t *applicationmenu = (menuitem_t *)appmenu_normal;
   int         applicationmenu_sz = sizeof(appmenu_normal) / sizeof(menuitem_t);

#endif /* __INCLUDE_DANCE_DECLARATIONS */

extern menuitem_t *applicationmenu;
extern int         applicationmenu_sz;


#ifdef __INCLUDE_DANCE_DECLARATIONS

   #ifndef APPBOOLEAN
      #define APPBOOLEAN COMMBOOLEAN
   #endif

   #define BOOLDEF(truelbl, falselbl) #truelbl,
   const char *appboolean_true[] = {APPBOOLEAN};
   #undef BOOLDEF

   #define BOOLDEF(truelbl, falselbl) #falselbl,
   const char *appboolean_false[] = {APPBOOLEAN};
   #undef BOOLDEF

   #ifdef APPLISTS
      #define DANCELIST(lname) \
         const char *opt_ ## lname[] = lname ## _LIST; \
         const list_t  applst_ ## lname = {                  \
            .name    = #lname,                         \
            .options = opt_ ## lname,                  \
            .n       = sizeof(opt_ ## lname) / sizeof (char *) \
         };
      APPLISTS
      #undef DANCELIST

      #define DANCELIST(name) #name,
      const char *applist_names[] = {APPLISTS};
      #undef DANCELIST

      #define DANCELIST(name) &applst_ ## name,
      const list_t *app_arg_lists[]  = {APPLISTS};
      #undef DANCELIST
      arglists_t applists_data = {
         .n          = sizeof(applist_names) / sizeof (const char *),
         .names      = applist_names,
         .arglists   = app_arg_lists,
         .n_bool     = sizeof(appboolean_true) / sizeof (const char *),
         .truevalue  = appboolean_true,
         .falsevalue = appboolean_false,
      };
   #else
      arglists_t applists_data = {
         .n          = 0,
         .names      = NULL,
         .arglists   = NULL,
         .n_bool     = sizeof(appboolean_true) / sizeof (const char *),
         .truevalue  = appboolean_true,
         .falsevalue = appboolean_false,
      };
   #endif  /*  #ifdef APPLISTS  */

   arglists_t *applists = &applists_data;

   #ifdef APPUNITS
      #define UNITDEF(u, f) f,
      #define DANCEUNITS(lname) \
         const id_floatp_t appfactors_ ## lname[] = {lname ## _UNITS};
      APPUNITS
      #undef UNITDEF
      #undef DANCEUNITS

      #define UNITDEF(u, f) #u,
      #define DANCEUNITS(lname) \
      const char *appunits_ ## lname[] = {lname ## _UNITS}; \
      unittype_t  app_ulist_ ## lname = {           \
         .name    = #lname,                         \
         .n       = sizeof(appunits_ ## lname) / sizeof (char *),	\
         .units   = appunits_ ## lname,                \
         .factors = appfactors_ ## lname,              \
      };
      APPUNITS
      #undef UNITDEF
      #undef DANCEUNITS

      #define DANCEUNITS(lname) #lname,
      const char *app_unitnames[] = {APPUNITS};
      #undef DANCEUNITS

      #define DANCEUNITS(lname) &app_ulist_ ## lname,
      unittype_t *app_unitlists[] = {APPUNITS};
      #undef DANCEUNITS

      unitlists_t appunits_data = {
         .n_units    = sizeof(app_unitnames) / sizeof (const char *),
         .names      = app_unitnames,
         .unitlists  = app_unitlists
      };
      unitlists_t* appunits = &appunits_data;
   #else
      unitlists_t* appunits = NULL;

   #endif /*  #ifdef APPUNITS   */

#endif /*  #ifdef __INCLUDE_DANCE_DECLARATIONS  */

extern arglists_t*  applists;
extern unitlists_t* appunits;

#ifdef APPLISTS

   #define DANCELIST(lname) \
      extern const list_t applst_ ## lname;
   APPLISTS
   #undef DANCELIST

#endif /*  #ifdef APPLISTS  */


#define LISTNITEMS(lname) (applst_ ## lname.n)

#define LISTSTRVAL(lname, idx) \
  (((size_t)(idx) >= LISTNITEMS(lname)) ? "???" : applst_ ## lname.options[(size_t)(idx)])

#define LISTBOOLVAL(truestr, boolval) \
   find_boolean_str(applists, truestr, boolval)

const char* find_boolean_str(arglists_t *arglsts, const char* truename, int boolval);


// COMMON MENU declaration block
/////////////////////////////////////////////////////

#ifdef COMMONMENU_DECLARATION

   // Declare boardmenu structure
   #define MENU_IT(it, infmt, outfmt, modes, helpstr) { \
      .item = cmd_comm_ ## it,   \
      .comm_str = #it,           \
      .comm_len = sizeof(#it)-1, \
      .comm_funct = (void (*)(void *))commexec_ ## it, \
      .help_funct = (void (*)(void))help_ ## it, \
      .input_fmt = infmt,   \
      .output_fmt = outfmt, \
      .flags = _DANCE_COMMAND | modes, \
      .help = helpstr       \
   },

   menuitem_t commonmenu[]  = {COMMONMENU_LIST};
   const int  commonmenu_sz = sizeof(commonmenu) / sizeof(menuitem_t);
   #undef MENU_IT

   #define DANCELIST(lname) \
      const char *_opt_ ## lname[] = lname ## _LIST; \
      list_t  commlst_ ## lname = {     \
         .name = #lname ,               \
         .options = _opt_ ## lname,     \
         .n       = sizeof(_opt_ ## lname) / sizeof (char *) \
      };
   COMMLISTS
   #undef DANCELIST

#endif // COMMONMENU_DECLARATION


#define DANCELIST(lname) \
   extern list_t commlst_ ## lname;
COMMLISTS
#undef DANCELIST


#ifdef COMMONMENU_DECLARATION

   #define BOOLDEF(truelbl, falselbl) #truelbl,
   const char *commboolean_true[] = {COMMBOOLEAN};
   #undef BOOLDEF

   #define BOOLDEF(truelbl, falselbl) #falselbl,
   const char *commboolean_false[] = {COMMBOOLEAN};
   #undef BOOLDEF


   #define DANCELIST(lname) #lname,
   const char* commlist_names[] = {COMMLISTS};
   #undef DANCELIST

   #define DANCELIST(name) &commlst_ ## name,
   const list_t* comm_arg_lists[]  = {COMMLISTS};
   #undef DANCELIST

   arglists_t commlists_data = {
      .n        = sizeof(commlist_names) / sizeof (const char *),
      .names    = commlist_names,
      .arglists = comm_arg_lists,
      .n_bool     = sizeof(commboolean_true) / sizeof (const char *),
      .truevalue  = commboolean_true,
      .falsevalue = commboolean_false,
   };
   arglists_t* commlists = &commlists_data;

   #define UNITDEF(u, f) f,
   #define DANCEUNITS(lname) \
      const id_floatp_t commfactors_ ## lname[] = {lname ## _UNITS};
   COMMUNITS
   #undef UNITDEF
   #undef DANCEUNITS

   #define UNITDEF(u, f) #u,
   #define DANCEUNITS(lname) \
   const char *communits_ ## lname[] = {lname ## _UNITS}; \
   unittype_t comm_ulist_ ## lname = {          \
       .name    = #lname,                    \
       .n       = sizeof(communits_ ## lname) / sizeof (char *), \
       .units   = communits_ ## lname,       \
       .factors = commfactors_ ## lname,     \
     };
   COMMUNITS
   #undef UNITDEF
   #undef DANCEUNITS

   #define DANCEUNITS(lname) #lname,
   const char *comm_unitnames[] = {COMMUNITS};
   #undef DANCEUNITS

   #define DANCEUNITS(lname) &comm_ulist_ ## lname,
   unittype_t *comm_unitlists[] = {COMMUNITS};
   #undef DANCEUNITS

   unitlists_t communits_data = {
      .n_units    = sizeof(comm_unitnames) / sizeof (const char *),
      .names      = comm_unitnames,
      .unitlists  = comm_unitlists
   };
   unitlists_t* communits = &communits_data;

#endif // COMMONMENU_DECLARATION

#define COMMLISTNITEMS(lname) (commlst_ ## lname.n)

#define COMMLISTSTRVAL(lname, idx) \
  (((size_t)(idx) >= COMMLISTNITEMS(lname)) ? "???" : commlst_ ## lname.options[(size_t)(idx)])


extern arglists_t*  commlists;
extern unitlists_t* communits;

#endif //_MENU_H_


