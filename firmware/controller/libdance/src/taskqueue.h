#ifndef _TASKQUEUE_H_INCLUDED_
#define _TASKQUEUE_H_INCLUDED_


#include "lnklist.h"
#include "dance_signal.h"


typedef void (*task_fn_t)(void*);

typedef struct task_handle_s
{
#define TASK_NORMAL (0)
#define TASK_SILENT (1 << 0)
#define TASK_STICKY (1 << 1)
  volatile uint32_t tflags;
  task_fn_t         fn;
  void*             data;
  lnklist_item_t*   it;
  
} task_handle_t;

typedef struct tqueue_handle_s
{
  /* written on every task completion */
  int               compl_fd;
  sigtype_t         compl_signal;
  pthread_mutex_t*  compl_mutex;
  pthread_cond_t*   compl_cond;

  pthread_t         thread;

  /* concurrent task lists */
  pthread_mutex_t   task_mutex;
  pthread_cond_t    task_cond;
  lnklist_t         ready_list;
  lnklist_t         sticky_list;

#define TQUEUE_DONE       (1 << 0)
#define TQUEUE_COMPL_FD   (1 << 1)
#define TQUEUE_COMPL_COND (1 << 2)
  volatile uint32_t tqflags;

} tqueue_handle_t;

typedef struct tqueue_attr_s
{
  // TQUEUE_FLAG_xxx
  uint32_t         tqflags;

  // task completion signalling mechanism
  int              compl_fd;
  sigtype_t        compl_signal;
  pthread_mutex_t* compl_mutex;
  pthread_cond_t*  compl_cond;

} tqueue_attr_t;

static const tqueue_attr_t tqueue_default_attr =
{
  .tqflags = 0,
  .compl_signal = SIGTYPE_UNKNOWN
};


danceerr_t tqueue_init(tqueue_handle_t*, const tqueue_attr_t*);
danceerr_t tqueue_fini(tqueue_handle_t*);
danceerr_t tqueue_add(tqueue_handle_t*, task_fn_t, void*, uint32_t flags);
danceerr_t tqueue_add_sticky(tqueue_handle_t*, task_handle_t**, task_fn_t, void*);
danceerr_t tqueue_del_sticky(tqueue_handle_t*, task_handle_t*);
danceerr_t tqueue_set_ready(tqueue_handle_t*, task_handle_t*);

#endif // _TASKQUEUE_H_INCLUDED_ 
