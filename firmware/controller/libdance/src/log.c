#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include "log.h"


#ifdef LOG_IS_ENABLED

uint32_t log_mask[LOG_CLASS_MAX] = { 0, };

static volatile int log_fd = -1;
static struct timeval log_tv;
static const char* log_oname;
static char* log_line;
static pthread_mutex_t log_lock;

#define LOG_FLAG_NEWLINE (1 << 0)
static unsigned log_flags = LOG_FLAG_NEWLINE;


static int get_now_timeval(struct timeval* tv) {
   /* use monotonic clock to retrieve time */
   /* must link with -lrt */

   /* getimeofday depends on system wide settings */
   /* (ie. ntpd ...) and may report past times, for */
   /* instance during time clock changes */

   struct timespec tp;
   const int err = clock_gettime(CLOCK_MONOTONIC, &tp);
   if (err) return err;
   tv->tv_sec = (unsigned long)tp.tv_sec;
   tv->tv_usec = tp.tv_nsec / 1000;

   return 0;
}

static int get_rel_timeval(struct timeval* rel_tv) {
   struct timeval now_tv;
   get_now_timeval(&now_tv);
   timersub(&now_tv, &log_tv, rel_tv);
   return 0;
}

int log_init(void) {
   if (pthread_mutex_init(&log_lock, NULL)) goto on_error_0;

   log_line = malloc(LOG_LINE_SIZE * sizeof(char));
   if (log_line == NULL) goto on_error_1;

   if (log_set_output("stdout")) goto on_error_2;

   get_now_timeval(&log_tv);

   return 0;

 on_error_2:
   free(log_line);
 on_error_1:
   pthread_mutex_destroy(&log_lock);
 on_error_0:
   log_fd = -1;
   return -1;
}

void log_fini(void) {
   if (log_fd == -1)
      return;
   free(log_line);
   pthread_mutex_destroy(&log_lock);
   if (log_fd > 2)
      close(log_fd);
   log_fd = -1;
}

int log_set_output(const char* oname) {
  /* filename, "stdout" or "stderr" */

   int err = -1;

   pthread_mutex_lock(&log_lock);

   if (log_fd > STDERR_FILENO)
      close(log_fd);
   log_fd = -1;

   log_oname = oname;

   if (strcmp(oname, "stdout") == 0) {
      log_fd = STDOUT_FILENO;
   } else if (strcmp(oname, "stderr") == 0) {
      log_fd = STDERR_FILENO;
   } else {
      log_fd = open(oname, O_RDWR | O_TRUNC | O_CREAT, 0755);
      if (log_fd == -1)
         goto on_error;
   }

   err = 0;

 on_error:
   pthread_mutex_unlock(&log_lock);
   return err;
}

void log_enable(uint32_t klass, uint32_t level) {
   log_mask[klass] |= (1 << (level + 1)) - 1;
}

void log_enable_all(uint32_t level) {
   uint32_t klass;
   for (klass = 0; klass < LOG_CLASS_MAX; ++klass)
      log_enable(klass, level);
}

void log_disable(uint32_t klass) {
   log_mask[klass] = 0;
}

void log_disable_all(void) {
   uint32_t klass;
   for (klass = 0; klass < LOG_CLASS_MAX; ++klass)
      log_disable(klass);
}

int log_write(const char* klass, const char* file, unsigned int line, const char* fmt, ...) {
   va_list ap;
   size_t off = 0;
   int n;
   int err = -1;
   struct timeval tm_rel;
   const char* ptr;

   for (ptr = file; *ptr; ptr++) {
      if (*ptr == '/')
         file = ptr + 1;
   }

   pthread_mutex_lock(&log_lock);

   if (log_fd == -1) goto on_error;

   get_rel_timeval(&tm_rel);

   if (log_flags & LOG_FLAG_NEWLINE) {
      log_flags &= ~LOG_FLAG_NEWLINE;

      n = snprintf(log_line + off,
                   LOG_LINE_SIZE - off,
                   "[%s] @%s:%u 0x%08x %lu.%06lu ",
                   klass, file, line,
                   (unsigned int)pthread_self(),
                   tm_rel.tv_sec, tm_rel.tv_usec);

      if (n <= 0) goto on_error;

      if (n >= (LOG_LINE_SIZE - off)) {
         off = LOG_LINE_SIZE - 1;
         goto on_truncation;
      }
      off += (size_t)n;
   }

   va_start(ap, fmt);
   n = vsnprintf(log_line + off, LOG_LINE_SIZE - off, fmt, ap);
   va_end(ap);

   if (n < 0) goto on_error;

   if (n >= (LOG_LINE_SIZE - off)) {
      off = LOG_LINE_SIZE - 1;
      goto on_truncation;
   }

   off += (size_t)n;

   on_truncation:

   /* update flags */
   if (*fmt && (fmt[strlen(fmt) - 1] == '\n')) log_flags |= LOG_FLAG_NEWLINE;

   /* write to output */
   n = (int)write(log_fd, log_line, off);
   if ((size_t)n != off) goto on_error;

   err = 0;

   on_error:
   pthread_mutex_unlock(&log_lock);

   return err;
}

int log_read(char** s, size_t* n) {
   /* reopen the file */

   struct stat st;
   int err = -1;
   int fd;

   if (log_fd == -1) goto on_error_0;

   /* do not read stdout or stderr */
   if (log_fd <= STDERR_FILENO) goto on_error_0;

   fd = open(log_oname, O_RDONLY);
   if (fd == -1) goto on_error_0;

   if (fstat(fd, &st)) goto on_error_1;

   *s = malloc(st.st_size * sizeof(char));
   if (*s == NULL) goto on_error_1;

   *n = (size_t)read(log_fd, *s, st.st_size);
   if (*n != st.st_size) goto on_error_1;

   err = 0;

 on_error_1:
   close(fd);
 on_error_0:
   return err;
}


int log_parse_opt(const char* s) {
   /* supported format */
   /* out_name or class_name:level */
   /* where: */
   /* out_name an output name not in class_name */
   /* class_name a class name or ALL or a digit */
   /* level a digit starting from 0 */
   /* out_name is set to NULL if class_name and level are valid */

#define LOG_CLASS(__klass) #__klass,
   static const char* class_name[] = { LIBDANCE_LOG_CLASS_LIST LOG_CLASS_LIST };
#undef LOG_CLASS

   uint32_t           klass;
   uint32_t           level;
   char*              endp;
   size_t             len;
   const char         class_all[] = "ALL";

   if ((*s >= '0') && (*s <= '9')) {
      klass = strtoul(s, &endp, 10);
      s = endp;
      if (klass >= LOG_CLASS_MAX) return -1;
   } else {
      /* special class names */
      len = strlen(class_all);
      if (strncmp(s, class_all, len) == 0) {
         klass = (uint32_t)-1;
         /* allow level even for all classes */
         s += len;
      } else {
         /* known class or output name */
         for (klass = 0; klass <= LOG_CLASS_APP; ++klass) {
            len = strlen(class_name[klass]);
            if (strncmp(s, class_name[klass], len) == 0) {
               s += len;
               break ;
            }
         }
         /* output name */
         if (klass == (LOG_CLASS_APP + 1)) return log_set_output(s);
      }
   }

   /* optional level argin */
   if (*s == ':') ++s;

   level = (*s == 0)? 0 : strtoul(s, NULL, 10);

   if (level > LOG_LEVEL_MAX) return -1;

   if (klass == (uint32_t)-1)
      log_enable_all(level);
   else
      log_enable(klass, level);

   return 0;
}


#endif /* LOG_IS_ENABLED */


#if 0 /* unit */

#include <stdint.h>

int main(int ac, char** av) {
#define LOG_CLASS_I2C (LOG_CLASS_APP + 0)
#define LOG_CLASS_REG (LOG_CLASS_APP + 1)

   unsigned int i;

   LOG_INIT();

   log_parse_opt("TRACE:0");
   log_parse_opt("INFO:0");
   log_parse_opt("stdout");

   LOG_ENABLE(WARN, 0);
   LOG_ENABLE(ERROR, 0);

   LOG_ENABLE(I2C, 1);
   LOG_ENABLE(REG, 5);

   LOG_TRACE();
   usleep(1000000);
   LOG_TRACE();

   LOG_INFO("log line without value\n");
   LOG_INFO("log fubar\n");
   LOG_INFO("log with value %x %x\n", 0x2a, 0x2b);
   LOG_INFO("log line without value\n");

   LOG_INFO("split line:");
   for (i = 0; i != 4; ++i) LOG_INFO(" %u", i);
   LOG_INFO("\n");

   LOG_PRINTF(I2C, 0, "i2c level == 0\n");
   LOG_PRINTF(I2C, 1, "i2c level == 1\n");
   LOG_PRINTF(I2C, 2, "i2c level == 2\n");

   LOG_PRINTF(REG, 1, "reg level == 1\n");
   LOG_PRINTF(REG, 6, "reg level == 6\n");

   LOG_FINI();

   return 0;
}

#endif /* unit */
