

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "memory.h"
#include "log.h"
#include "derr.h"

#define _DATA_ALLOC   (1 << 0)
#define _LOCAL_ALLOC  (1 << 1)


// ---------------  Debug utilities

#define DEF_DUMP_WIDTH 16
void __rawdata_dump(void* data_beg, size_t nbytes, int pwidth)
{
   void* data_end = data_beg + nbytes;
   void* line_ptr;

   if (pwidth <= 0)       pwidth = DEF_DUMP_WIDTH;
   else if (pwidth >= 32) pwidth = 32;
   else if (pwidth >= 16) pwidth = 16;
   else if (pwidth >=  8) pwidth = 8;
   else if (pwidth >=  4) pwidth = 4;
   else if (pwidth >=  2) pwidth = 2;
   else                   pwidth = 1;

   line_ptr = (char*)((uintptr_t)data_beg - ((uintptr_t)data_beg % pwidth));

   for (; line_ptr <= data_end; line_ptr += pwidth)
   {
      int   i;
      void* ptr;

      printf(" %p:", line_ptr);
      for (i = 0; i < pwidth; i++)
      {
         ptr = line_ptr + i;
         if ((i % 4) == 0) printf(" ");
         if (ptr < data_beg || ptr >= data_end)
            printf("  ");
         else
            printf("%02x", *(char*)ptr);
      }
      printf(" : ");
      for (i = 0; i <= pwidth; i++)
      {
         ptr = line_ptr + i;
         if (ptr < data_beg || ptr >= data_end)
            printf(" ");
         else if (isprint(*(char*)ptr))
            printf("%c", *(char*)ptr);
         else
            printf(".");
      }
      printf("\n");
   }
}

//
void __dblock_dump(dblock_t* dblk)
{
   mblock_t* mblk;

   if (dblk == NULL)
   {
      printf(".... Null data block pointer\n");
      return;
   }
   mblk = dblk->mblock;
   printf("dblock @ %p  flags=0x%02x:\n", dblk, dblk->dbflags);
   if (mblk == NULL)
   {
      printf(".... Corrupted data block allocation\n");
      return;
   }
   printf("- mblock @ %p  flags=0x%02x:\n", mblk, mblk->mbflags);
   printf("- Data pointers:  first=%p  last=%p  end=%p\n", dblk->first, dblk->last, dblk->end);
   printf("- Storage size = %zd @ %p (alloc: %zd @ %p)\n", mblk->mbsize, mblk->mbptr, mblk->alloc_size, mblk->alloc_ptr);
   if (mblk->mbsize == 0 || mblk->mbptr == NULL)
   {
      if (mblk->mbsize != 0 || mblk->mbptr != NULL)
         printf(".... Corrupted memory block allocation\n");
      if (mblk->mbflags & MEM_DATA_ALLOC)
         printf(".... Corrupted memory block flags\n");
      if (dblk->first != NULL || dblk->last != NULL || dblk->end != NULL)
         printf(".... Corrupted data pointers\n");
      return;
   }
   if (mblk->mbptr < mblk->alloc_ptr || (mblk->alloc_ptr + mblk->alloc_size) < (mblk->mbptr + mblk->mbsize))
      printf(".... Inconsistent memory block allocation\n");
   if (dblk->first < mblk->mbptr || dblk->last < dblk->first || dblk->end < dblk->last ||
                                    dblk->end < (mblk->mbptr + mblk->mbsize))
   {
      printf(".... Inconsistent data pointers\n");
      return;
   }
   printf("- Data size:  skipped=%zd  usable=%zd (used=%zd  available=%zd)\n", dblk->first - mblk->mbptr,
           dblk->end - dblk->first, dblk->last - dblk->first, dblk->end - dblk->last);

   if (mblk == NULL)
   printf("- Used data: %zd (%p - %p)\n", dblk->last - dblk->first, dblk->first, dblk->last);
   __rawdata_dump(dblk->first, dblk->last - dblk->first, 0);
}

// ---------------  Memory block handling functions

#define _VALID_MEM_CREATE  (MEM_PERSISTENT | MEM_TYPE)


mblock_t* mblock_create(mblock_t* mblk, void *data, size_t size, uint32_t flags)
{
   uint32_t mem_type = flags & MEM_TYPE;
   uint32_t mem_transf = flags & MEM_TRANFERRED;   // memory provided by user but to be considered as internal

   flags &= _VALID_MEM_CREATE;  // restrict flags to only acceptable values

   if (mem_transf && data == NULL)
   {
      SYSTEM_ERROR();
      goto on_error;
   }

   if (mblk == NULL)
   {
      if ((mblk = malloc(sizeof(mblock_t))) == NULL)
      {
         SYSTEM_ERROR();
         goto on_error;
      }
      flags |= MEM_MBLOCK_ALLOC;
   }

   switch(mem_type)
   {
      case MEM_TYPE_DEF_MALLOC:
         mblk->mem_handle = NULL;
         break;

      case MEM_TYPE_EBUF_PHYSMEM:
      case MEM_TYPE_EBUF_MALLOC:
         {
            ebuf_handle_t* ebuf = data;

            if (ebuf == NULL)
            {
               if (ebuf_alloc_handle(&ebuf) != EBUF_ERR_SUCCESS)
               {
                  SYSTEM_ERROR();
                  goto on_error;
               }
               flags |= MEM_HANDLE_ALLOC;

            } else {
               data = ebuf_get_data(ebuf);
               if (data != NULL)
               {
                  if (size != 0 && size != ebuf_get_size(ebuf))
                  {
                     SYSTEM_ERROR();
                     goto on_error;
                  }
                  size = ebuf_get_size(ebuf);
               }
            }
            mblk->mem_handle = ebuf;
         }
         break;

      default: // not allowed
         SYSTEM_ERROR();
         goto on_error;
   }

   if (mem_transf)
   {
      switch(mem_type)
      {
         case MEM_TYPE_DEF_MALLOC:
            break;

         case MEM_TYPE_EBUF_PHYSMEM:
         case MEM_TYPE_EBUF_MALLOC:
            flags |= MEM_HANDLE_ALLOC;
            break;

         default:
            SYSTEM_ERROR();
            goto on_error;
      }
      flags |= MEM_DATA_ALLOC;
   }
   else if (data == NULL && size != 0)
   {
      switch(mem_type)
      {
         case MEM_TYPE_DEF_MALLOC:
            if ((data = malloc(size)) == NULL)
            {
               SYSTEM_ERROR();
               goto on_error;
            }
            break;

         case MEM_TYPE_EBUF_PHYSMEM:
         case MEM_TYPE_EBUF_MALLOC:
            {
               ebuf_handle_t *ebuf = mblk->mem_handle;
               uint32_t       ebuf_flags = (mem_type == MEM_TYPE_EBUF_PHYSMEM)? EBUF_FLAG_PHYSICAL : EBUF_FLAG_DEFAULT;

               if (ebuf_alloc(ebuf, size, ebuf_flags) != EBUF_ERR_SUCCESS)
               {
                  SYSTEM_ERROR();
                  goto on_error;
               }
               data = ebuf_get_data(ebuf);
            }
            break;

         default:
            SYSTEM_ERROR();
            goto on_error;
      }
      flags |= MEM_DATA_ALLOC;
   }

   mblk->alloc_ptr  = data;
   mblk->alloc_size = size;

   mblk->mbptr  = mblk->alloc_ptr;
   mblk->mbsize = mblk->alloc_size;
   mblk->mbflags = flags;
   return mblk;

 on_error:
   if (flags & MEM_HANDLE_ALLOC)
   {
      switch (mem_type) {
         case MEM_TYPE_EBUF_PHYSMEM:
         case MEM_TYPE_EBUF_MALLOC:
            ebuf_free_handle(mblk->mem_handle);
            break;
      }
   }
   if (flags & MEM_MBLOCK_ALLOC) free(mblk);
   return NULL;
}

mblock_t* mblock_release(mblock_t* mblk, int mode)
{
   uint32_t mem_type = mblk->mbflags & MEM_TYPE;

   if ((mblk->mbflags & MEM_PERSISTENT) && !(mode & DESTROY_FORCE))
      return mblk;

   if ((mblk->mbflags & MEM_DATA_ALLOC) && !(mode & DESTROY_BUF_ONLY))
   {
      if (mblk->alloc_ptr)
      {
         switch (mem_type) {
            case MEM_TYPE_DEF_MALLOC:
               free(mblk->alloc_ptr);
               break;

            case MEM_TYPE_EBUF_PHYSMEM:
            case MEM_TYPE_EBUF_MALLOC:
               if (mblk->mbflags & MEM_HANDLE_ALLOC)
               {
                  ebuf_free_handle(mblk->mem_handle);
                  mblk->mem_handle = NULL;
               }
               else
               {
                  ebuf_free(mblk->mem_handle);
               }
               break;

            default:
               SYSTEM_ERROR();
               break;
         }
         mblk->alloc_ptr = NULL;
         mblk->alloc_size = 0;
      }
   }

   if ((mblk->mbflags & MEM_MBLOCK_ALLOC) && !(mode & DESTROY_DATA_ONLY))
   {
      free(mblk);
      return NULL;
   }
   else
   {
      mblk->mbptr  = mblk->alloc_ptr;
      mblk->mbsize = mblk->alloc_size;
      return mblk;
   }
}


mblock_t* mblock_resize(mblock_t* mblk, size_t newsize)
{
   uint32_t mem_type = mblk->mbflags & MEM_TYPE;

   if (mem_type != MEM_TYPE_DEF_MALLOC)   // not supported for the time being
   {
      SYSTEM_ERROR();
      return NULL;
   }
   if (mblk->alloc_ptr != NULL && !(mblk->mbflags & MEM_DATA_ALLOC))
   {
      SYSTEM_ERROR();
      return NULL;
   }
   if (mblk->mbptr != mblk->alloc_ptr)
   {
      memmove(mblk->alloc_ptr, mblk->mbptr, mblk->mbsize);
      mblk->mbptr = mblk->alloc_ptr;
   }
   if (newsize != mblk->alloc_size)
   {
      void* data = realloc(mblk->alloc_ptr, newsize);

      if (data == NULL)
      {
         SYSTEM_ERROR();
         return NULL;
      }
      mblk->alloc_ptr  = data;
      mblk->alloc_size = newsize;
      mblk->mbptr  = mblk->alloc_ptr;
      mblk->mbsize = mblk->alloc_size;
   }
   mblk->mbflags |= MEM_DATA_ALLOC;
   return mblk;
}

mblock_t* mblock_acquire(mblock_t* mblk)
{
   if (mblk->mbflags & MEM_PERSISTENT)
   {
      SYSTEM_ERROR();
      return(NULL);
   }
   mblk->mbflags |= MEM_PERSISTENT;
   return mblk;
}


// ---------------  Communication buffer handling functions

dblock_t* dblock_create(dblock_t* dblk, mblock_t* mblock, ssize_t size, uint32_t flags)
{
   size_t used;

   flags &= _VALID_MEM_CREATE;

   if (dblk == NULL)
   {
      if ((dblk = malloc(sizeof(dblock_t))) == NULL)
      {
         SYSTEM_ERROR();
         return NULL;
      }
      flags |= MEM_DBLOCK_ALLOC;
   }

   if (mblock == NULL)
   {
      if ((mblock = mblock_create(NULL, NULL, size, flags)) == NULL)
      {
         SYSTEM_ERROR();
         if (flags & MEM_DBLOCK_ALLOC) free(dblk);
         return NULL;
      }
      used = 0;
   }
   else
      used = size < 0? mblock_size(mblock) : size;

   dblk->dbflags = flags;
   dblk->mblock  = mblock;
   dblk->first   = mblock_ptr(mblock);
   dblk->last    = dblk->first + used;
   dblk->end     = dblk->first + mblock_size(mblock);
   lnklist_init_item(&dblk->item, dblk);
   return dblk;
}

dblock_t* dblock_release(dblock_t* dblk, int mode)
{
   if ((dblk->dbflags & MEM_PERSISTENT) && !(mode & DESTROY_FORCE))
      return dblk;

   dblock_unlink(dblk);

   dblk->mblock = mblock_release(dblk->mblock, mode);

   if (dblk->dbflags & MEM_DBLOCK_ALLOC)
      free(dblk);

   return NULL;
}

size_t dblock_resize(dblock_t* dblk, size_t newsize)
{
   size_t skipped = dblk->first - mblock_ptr(dblk->mblock);
   size_t used = dblock_used(dblk);

   if (mblock_resize(dblk->mblock, newsize + skipped) == NULL)
      return dblock_size(dblk);

   if (used > newsize) used = newsize;

   dblk->first   = mblock_ptr(dblk->mblock) + skipped;
   dblk->last    = dblk->first + used;
   dblk->end     = dblk->first + newsize;

   return newsize;
}

size_t dblock_append(dblock_t* dblk, void* ptr, size_t size)
{
   size_t newsize = dblock_used(dblk) + size;

   if (newsize > dblock_size(dblk) && dblock_resize(dblk, newsize) != newsize)
      return 0;

   memcpy(dblk->last, ptr, size);
   dblk->last += size;
   return size;
}

dblock_t* dblock_acquire(dblock_t* dblk)
{
   if (dblk->dbflags & MEM_PERSISTENT)
   {
      SYSTEM_ERROR();
      return(NULL);
   }
   dblk->dbflags |= MEM_PERSISTENT;
   return dblk;
}

size_t dblock_seek(dblock_t* dblk, size_t skip)
{
   size_t used = dblock_used(dblk);

   if (skip > used) skip = used;
   dblk->first += skip;

   return skip;
}

size_t dblock_setused(dblock_t* dblk, size_t newused)
{
   if (newused > dblock_size(dblk))
      return dblock_used(dblk);

   dblk->last = dblk->first + newused;
   return newused;
}

/*
dblock_t* dblock_next(dblock_t* dblk)
{
   lnklist_item_t* next = dblk->item.next;
   return (next? next->itmdata : NULL);
}
*/

// -----------  Communication buffer list handling functions

cbuflist_t* combuflist_create(cbuflist_t* cbuflist, dblock_t* dblk)
{
   if (cbuflist != NULL)
      lnklist_init(cbuflist, NULL);
   else
   {
      cbuflist = lnklist_alloc(NULL);
      if (cbuflist == NULL)
      {
         SYSTEM_ERROR();
         return NULL;
      }
   }

   if (dblk != NULL)
      lnklist_append(cbuflist, &dblk->item);

   return cbuflist;
}

void combuflist_free(cbuflist_t* cbuflist)
{
   lnklist_item_t* item = cbuflist->head;

   while (item != NULL)
   {
      lnklist_item_t* next_item = item->next;
      dblock_release(item->itmdata, DESTROY_DEFAULT);
      item = next_item;
   }
   free(cbuflist);
}

danceerr_t combuflist_append(cbuflist_t* cbuflist, dblock_t* dblk)
{
   if (dblk != NULL)
      lnklist_append(cbuflist, &dblk->item);

   return DANCE_OK;
}

danceerr_t  combuflist_prepend(cbuflist_t* cbuflist, dblock_t* dblk)
{
   if (dblk != NULL)
      lnklist_prepend(cbuflist, &dblk->item);

   return DANCE_OK;
}

static lnklist_for_t add_item_size(lnklist_item_t* item, void* arg)
{
  size_t* const size = arg;
  dblock_t* const dblk = item->itmdata;
  *size += dblock_used(dblk);
  return LNKLIST_CONT;
}

size_t      combuflist_size(cbuflist_t* cbuflist)
{
  size_t size;
  size = 0;
  lnklist_foreach(cbuflist, add_item_size, &size);
  return size;
}
