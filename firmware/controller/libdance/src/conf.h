#ifndef CONF_H_INCLUDED
#define CONF_H_INCLUDED


#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>

#include "libdance.h"

// make sure that APPCONF_LIST is defined (although may be empty)
#ifndef APPCONF_LIST
#  define APPCONF_LIST
#endif

#define CONF_SCALAR(__name, __type, __val)          +1
#define CONF_ARRAY(__name, __count, __type, __val)  +1
#if (0 APPCONF_LIST)
   #define APPCONF_USED 1
#else
   #define APPCONF_USED 0
#endif
#undef CONF_SCALAR
#undef CONF_ARRAY


typedef struct {
   const char* name;
   size_t size;             // size per item
   size_t count;            // item count
   void (*init_fn)(void*);  // initialization routine
   void* valp;              // the application touched value
   void* storp;             // the stored value
} appconf_desc_t;

extern char g_appconf_path[];

/* mutually exclusive configuration modes */
/* ALL_COMMS: default, check for update after each command */
/* SKIP_QUERIES: do not check for update after queries */
/* EXPLICIT_ONLY: check for update only if conf_update called by application */
/* note that any commands can be marked in the menu to skip the update */
#define CONF_MODE_ALL_COMMS     0
#define CONF_MODE_SKIP_QUERIES  1
#define CONF_MODE_EXPLICIT_ONLY 2

/* reset configuration entries */
/* NEVER: tries not to reset the config entries if possible */
/* IFSIZE: entry is reset if item size changes */
/* IFCOUNT: entry is reset if item count changes */
/* ALWAYS: resets all entries */
/* ... */
#define CONF_RESET_NEVER          0
#define CONF_RESET_IFSIZE   (1 << 0)
#define CONF_RESET_IFCOUNT  (1 << 1)
#define CONF_RESET_ALWAYS   (1 << 2)


/* mutually exclusive media */
/* MEDIA_SYS_FLASH: default, use system flash */
/* MEDIA_FILE: /tmp/appconf file */
#define CONF_MEDIA_SYS_FLASH  (1 << 0)
#define CONF_MEDIA_FILE       (1 << 1)
#define CONF_MEDIA_MKDIRTREE  (1 << 2)
#define CONF_MEDIA_FILE_AUTO  CONF_MEDIA_FILE | CONF_MEDIA_MKDIRTREE


#if APPCONF_USED

extern appconf_desc_t* appconf_desc[];
extern const uint32_t  appconf_timer;
extern const uint32_t  appconf_mode;
extern       uint32_t  appconf_reset;
extern const uint32_t  appconf_media;
extern const char*     appconf_file;

#define CONF_SCALAR(__name, __type, __val)          extern __type appconf_ ## __name;
#define CONF_ARRAY(__name, __count, __type, __val)  extern __type appconf_ ## __name[__count];
APPCONF_LIST
#undef CONF_SCALAR
#undef CONF_ARRAY

int appconf_commit(void);
int appconf_erase(void);
int appconf_revert(void);

int conf_init(void);
int conf_commit(void);
int conf_erase(void);
int conf_revert(void);
int conf_restart_timer(unsigned int);

#endif  // APPCONF_USED

#endif // CONF_H_INCLUDED */
