/*---------------------------------------------------------------------------
 *
 */
#ifndef _EXTBUSCOMM_H_
#define _EXTBUSCOMM_H_

#include <stdint.h>

#include "comm.h"
#include "regdefs.h"


/*---------------------------------------------------------------------------
 * Prototypes
 */
int      dance_extbus_init(void);
int      dance_extbus_initbus(int index);
void     dance_extbus_close(void);

int      dance_regtable_init(void);
void     dance_regtable_fini(void);

int      extbus_set_debug(int);

dancereg_t *extbus_parse_regmem(const char *name, int type, int access);
size_t      extbus_sprint_regmem(dancereg_t* reg, char* print_buff, size_t buf_sz);

int      extbus_read_regmem(dancereg_t* regmem, ssize_t offset, ssize_t rstep, size_t nvalues, void* buff, ssize_t bstep);
int      extbus_write_regmem(dancereg_t* regmem, ssize_t offset, ssize_t rstep, size_t nvalues, const void* buff, ssize_t bstep);


uint8_t  extbus_read_value8(dancereg_t* reg,  ssize_t offset);
uint16_t extbus_read_value16(dancereg_t* reg, ssize_t offset);
uint32_t extbus_read_value32(dancereg_t* reg, ssize_t offset);
uint64_t extbus_read_value64(dancereg_t* reg, ssize_t offset);

void     extbus_write_value8(dancereg_t* reg,  ssize_t offset, uint8_t  value);
void     extbus_write_value16(dancereg_t* reg, ssize_t offset, uint16_t value);
void     extbus_write_value32(dancereg_t* reg, ssize_t offset, uint32_t value);
void     extbus_write_value64(dancereg_t* reg, ssize_t offset, uint64_t value);



/*---------------------------------------------------------------------------
 * Utilities
 */

#define REGOFFSET_VALUE(reg, offs)        extbus_read_value32(DANCE_REG_PT(reg), (offs))
#define REGOFFSET_WRITE(reg, offs, value) extbus_write_value32(DANCE_REG_PT(reg), (offs), (value))
#define REG_VALUE(reg)           REGOFFSET_VALUE((reg), 0)
#define REG_WRITE(reg, value)    REGOFFSET_WRITE((reg), 0, (value))
#define IS_REGBIT_SET(reg, mask) ((REG_VALUE(reg) & (mask)) != 0)
#define REG_WRITEMASK(reg, mask, val) REG_WRITE((reg), (REG_VALUE(reg) & ~(mask)) | ((val) & (mask)))
#define REGOFFSET_WRITEMASK(reg, offs, mask, val) \
                  REGOFFSET_WRITE((reg), (offs), ((REGOFFSET_VALUE(reg, offs) & ~(mask)) | ((val) & (mask))))

#define REG_SETBITS(reg, bits)   REG_WRITEMASK((reg), bits, 0xffffffff)
#define REG_CLRBITS(reg, bits)   REG_WRITEMASK((reg), bits, 0x00000000)


#define REG64OFFSET_VALUE(reg, offs)        extbus_read_value64(DANCE_REG_PT(reg), (offs))
#define REG64OFFSET_WRITE(reg, offs, value) extbus_write_value64(DANCE_REG_PT(reg), (offs), (value))
#define REG64_VALUE(reg)           REG64OFFSET_VALUE((reg), 0)
#define REG64_WRITE(reg, value)    REG64OFFSET_WRITE((reg), 0, (value))


#endif // _EXTBUSCOMM_H_
