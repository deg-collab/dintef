/*--------------------------------------------------------
 *
 *   $Revision: 2177 $
 *
 *   $Id: comm.c 2177 2020-02-27 15:34:37Z perez $
 *
 *------------------------------------------------------ */

#include <ctype.h>            //
#include <stdio.h>            //
#include <unistd.h>           //
#include <pthread.h>
#include <string.h>

#define COMMONMENU_DECLARATION

#include "libdance.h"

#include "comm.h"
#include "errors.h"
#include "log.h"
#include "daq.h"
#include "menu.h"


#define READBIN_TIMEOUT_SEC  3


// temporary thread static variable to be removed once
//   get_current_command() is properly implemented
//
static __thread command_t* current_command = NULL;

void set_current_command(command_t* comm) {
   current_command = comm;  // for the time being ...
}

command_t* get_current_command(void) {
   command_t* comm;
//   pid_t      thread_id = gettid();

   // obtain current command from thread_id
   comm = current_command;  // for the time being ...
   return(comm);
}


void show_comm(command_t* comm, char* text)
{
   LOG_COMM("%s\n", text);
   LOG_COMM("  comm->cmd     = \"%s\"\n", comm->cmd);
   LOG_COMM("  comm->address = \"%.*s\"\n", comm->address_sz, comm->address);
   LOG_COMM("  comm->keyword = \"%.*s\"\n", comm->keyword_sz, comm->keyword);
   LOG_COMM("  comm->params  = \"%s\"\n", comm->params);
   LOG_COMM("  comm->flags   = 0x%04x\n", comm->flags);
}

// -------------------------------------------------------------------

int initfail_bit = 0;
int initfail_hook(void)
{
    uint32_t errflags;
    int      idx;
    char     errstr[80] = "";

    /* Check libdance global errors */
    if ((errflags = g_libdance.errflags) == 0) {
        goto normal_end;
    }

    /* Let a change to app to fix its bad init */
    if ((errflags & INITFAIL_APP) && IS_APP_HOOK_INSTALLED()) {
        if (IS_CURRENT_APP()) {
            goto normal_end;
        }
    }

    /* Allowed commands even if libdance init failed */
    if (CURRENT_COMMFLAGS() & _INITFAIL_COMMAND) {
            goto normal_end;
    }

    /* Return to client an explicit message */
    for (idx = 0; idx <= INITFAIL_COUNT; idx++) {
        if (errflags & (1 << idx)) {
            int len = strlen(errstr);
            snprintf(
                errstr + len,           
                sizeof(errstr) - len, 
                "%s ", initfail_msgs[idx]
            );
        }
    }
    errorf("Command not allowed: init failed: %s", errstr);

    /* Deny command exec */
    return HOOKRET_NOMORE;

normal_end:
LOG_TRACE();
    return HOOKRET_CONTINUE;
}


int initfail_hook_init(void)
{
    int hook_n;
    int ret = DANCE_ERROR;

    /* Install libdance hook with higher priority */
    if ((hook_n = HOOK_ADD(initfail_hook)) < 0) {
        goto on_error;
    }
    initfail_bit = hook_n;

    ret = DANCE_OK;
on_error:
    return ret;
};


// -------------------------------------------------------------------


void forward_command(command_t* comm, char* commstr)
{
/*
   LOG_COMM("forward_command()\n");
   if (0) {  // if can forward commands
      // forward the command here
      comm->flags &= ~(CMDFLG_QUERY | CMDFLG_ACKNOWLEDGE);
   } else
      error_printf(comm, "Cannot forward commands (for the time being)");
   return;
*/
}

int init_io_data(menuitem_t* menu, size_t msize, arglists_t* arglists) {
   for ( ; msize--; menu++)
   {
      if (parse_format_string(&menu->inp_data, menu->input_fmt, arglists) != NOERROR)
      {
         LOG_ERROR("Bad input format string: %s\n", menu->input_fmt);
         return(DANCE_ERROR);
      }
      if (parse_format_string(&menu->out_data, menu->output_fmt, arglists) != NOERROR)
      {
         LOG_ERROR("Bad output format string: %s\n", menu->output_fmt);
         return(DANCE_ERROR);
      }
   }
   return(DANCE_OK);
}

int iodata_init(void) {
   if (init_io_data((menuitem_t*)commonmenu, commonmenu_sz, commlists) != DANCE_OK)
      return(DANCE_ERROR);

   if (init_io_data((menuitem_t*)applicationmenu, applicationmenu_sz, applists) != DANCE_OK)
      return(DANCE_ERROR);

   return(DANCE_OK);
}

void clone_comm_iodata(command_t* comm) {
   // clone io argument structures into comm
   comm->inp_data = clone_iodata(&comm->mentry->inp_data);
   comm->out_data = clone_iodata(&comm->mentry->out_data);
   if (comm->inp_data == NULL || comm->out_data == NULL)
   {
      SYSTEM_ERROR();
      error_printf(comm, SYSERRORMESSAGE);
   }
}

void free_comm_iodata(command_t* comm) {
   free_iodata(comm->inp_data);
   free_iodata(comm->out_data);
   free(comm->inp_data);
   free(comm->out_data);
}

void parse_input_parameters(command_t* comm)
{
   char*       params = comm->params;
   errmsg_t    err;

   err = parse_input_args(comm->inp_data, params);
   if (err)
   {
      error_printf(comm, err);
      LOG_ERROR("INPUT PARSING ERROR: %s\n", err);
      return;
   }
   LOG_COMM("Successful parsing of input arguments \"%s\"\n", params);

   reset_output_context();
}


/*     Looks for the first command in the list defined by menu and msize
 *     that matches comm_line.
 *
 *     msize is the size of the array of menuitems pointed by menu.
 *
 *     The function returns the pointer to the corresponding command in menu.
 *     If no command is found the function returns NULL.
 */
menuitem_t* menulookup(const menuitem_t* menu, size_t msize, char* comm_line, size_t kwrdsize)
{
   char   prefix[3];
   int    pfx_sz = 0;

   if (comm_line[pfx_sz] == '?') prefix[pfx_sz++] = 'q';
   if (comm_line[pfx_sz] == '*') prefix[pfx_sz++] = 'b';
   prefix[pfx_sz] = 0;

   for ( ; msize--; menu++)
   {
      char  *comm_str;
      int    idx;

      if (menu->comm_len != kwrdsize)         goto next_item;
      comm_str = menu->comm_str;
      for (idx = kwrdsize; idx-- > pfx_sz; )
         if (comm_str[idx] != comm_line[idx]) goto next_item;
      for ( ; idx >= 0; idx--)
         if (comm_str[idx] != prefix[idx])    goto next_item;
      return((menuitem_t *)menu);

 next_item:
      continue;
   }
   return(NULL);
}


static char* parse_flags(command_t* comm)
{
   char* comm_str;
   char* ptr;
   char  c;

   comm->address = "";
   comm->address_sz = 0;
   comm->keyword = "";
   comm->keyword_sz = 0;
   comm->params = "";
   // comm->flags is initialised during command extraction

   for (ptr = comm->cmd; isblank(*ptr); ptr++)
      ;
   if (*ptr == '#')
   {
      comm->flags |= CMDFLG_ACKNOWLEDGE;
      while (isblank(*(++ptr)))
         ;
   }
   comm_str = ptr;
   for (comm_str = ptr; (c = *ptr) && !isblank(c); ptr++)
   {
      if (c == '#')
      {
         error_printf(comm, BADSYNTAXMESSAGE);
         return(NULL);
      }
      else if (c == ':')
      {
         if (comm->flags & ~(CMDFLG_ACKNOWLEDGE | CMDFLG_ATOMIC))
         {
            // error
            error_printf(comm, BADSYNTAXMESSAGE);
            return(NULL);
         }
         else
         {
            comm->address = comm_str;
            comm->address_sz = ptr - comm_str;
            comm_str = ptr + 1;
            if (comm->address_sz)
            {
               char   *addr = comm->channel->address;
               size_t  sz = comm->channel->address_sz;
               if (comm->address_sz != sz || strncmp(comm->address, addr, sz) != 0)
               {
                  comm->flags |= CMDFLG_FORWARD;
               }
            }
            else
            {
               if (comm->flags & CMDFLG_ACKNOWLEDGE)
               {
                  error_printf(comm, BADABDCASTMESSAGE);
                  return(NULL);
               }
               else
                  comm->flags |= CMDFLG_BROADCAST;
            }
         }
      }
      else if (c == '?')
      {
         if (ptr != comm_str)
         {
            error_printf(comm, BADSYNTAXMESSAGE);
            return(NULL);
         }
         else if (comm->flags & CMDFLG_BROADCAST)
         {
            error_printf(comm, BADQBDCASTMESSAGE);
            return(NULL);
         }
         else
         {
            comm->flags |= CMDFLG_QUERY;
            comm->flags &= ~CMDFLG_ACKNOWLEDGE;
            if (*(comm_str + 1) == '*')
            {
               comm->flags |= CMDFLG_BINARY;
               ptr++;
            }
         }
      }
      else if (c == '*')
      {
         if (ptr != comm_str)
         {
            error_printf(comm, BADSYNTAXMESSAGE);
            return(NULL);
         }
         else if (comm->flags & CMDFLG_ATOMIC)
         {
            error_printf(comm, INVATOMICMESSAGE);
            return(NULL);
        }
         else
         {
            comm->flags |= CMDFLG_BINARY;
         }
      }
   }
   comm->keyword = comm_str;
   comm->keyword_sz = ptr - comm_str;
   comm->params = ptr;
   return(comm_str);
}

static void preparse_command(command_t* comm)
{
   if (!parse_flags(comm))   // syntax error
      return;

   if (comm->flags & CMDFLG_FORWARD)
   {
      error_printf(comm, "Command forwarding not supported");
   }
   else
   {
      if (!(comm->mentry = menulookup(applicationmenu, applicationmenu_sz, comm->keyword, comm->keyword_sz)))
      {
         if (!(comm->mentry = menulookup(commonmenu, commonmenu_sz, comm->keyword, comm->keyword_sz)))
         {
            error_printf(comm, BADCOMMMESSAGE);
            return;
         }
      }
   }
}

#define IS_SEMICOLON(c) ((c) == ';')
#define IS_END_OF_LINE(c) ((c) == '\n' || (c) == '\r' || (c) == 0)
#define IS_QUOTE(c) ((c) == '\'' || (c) == '\"')
#define IS_FIRST_CHAR(c) (isalnum(c) || (c) == '_' || (c) == '#' || \
                          (c) == ':' || (c) == '?' || (c) == '*')

// Looks for a complete command line in the buffer pointer by *buffptr
//  of length *buffsz.
// The function returns the length of the command line found excluding
//  the end-of-line, or zero if no complete command line was found.
//  In addition:
//    - *buffsz is always updated to contain the characters to consume
//    - if a command line was found, *buffptr points to the first character
//
size_t find_command_lines(char** buffptr, size_t* buffsz, uint16_t* flags)
{
   char*      ptr = *buffptr;
   size_t     nchars = *buffsz;
   size_t     beg_sz;
   size_t     line_sz;
   char       quote = 0;
   char       c = 0;

   beg_sz = 0;

 start_cmdline_search:

   // skip whitespaces at the begining of the line
   while (beg_sz < nchars && (isblank(c = ptr[beg_sz]) || iscntrl(c)))
      ++beg_sz;

   if (!IS_FIRST_CHAR(c))
   {
      // if not valid command line, discard characters until end-of-line
      for (line_sz = beg_sz; line_sz < nchars; c = ptr[++line_sz])
      {
         if (IS_END_OF_LINE(c))
         {
            beg_sz = ++line_sz;          // discard full line
            goto start_cmdline_search;   // and keep looking for
         }
      }
      *buffsz = beg_sz;   // discard only initial characters
      return(0);          // and return
   }

   *buffptr = ptr + beg_sz;

   for (line_sz = beg_sz; line_sz < nchars; c = ptr[++line_sz])
   {
      if (quote && c != 0)
      {
         if (c == quote) quote = 0;
      }
      else
      {
         if (IS_END_OF_LINE(c) || IS_SEMICOLON(c))
         {
            if (IS_SEMICOLON(c))
            {
               if (++line_sz < nchars)
               {
                  char*  next_buffptr = ptr + line_sz;
                  size_t next_buffsz = nchars - line_sz;

                  if (find_command_lines(&next_buffptr, &next_buffsz, flags) == 0)
                  {
                     *buffsz = beg_sz;
                     return(0);
                  }
                  else
                  {
                     *buffptr = ptr + beg_sz;
                     *buffsz = line_sz;
                     *flags = CMDFLG_OK | CMDFLG_ATOMIC;
                     return(line_sz - beg_sz - 1);
                  }
               }
            }
            else
            {
               size_t comm_sz = line_sz - beg_sz;
               // skip additional end-of-line chars
               while (++line_sz < nchars)
               {
                  c = ptr[line_sz];
                  if (!IS_END_OF_LINE(c))
                     break;
               }
               *buffsz = line_sz;
               *flags = CMDFLG_OK;
               return(comm_sz);
            }
         }
         else
         {
            if (IS_QUOTE(c))
               quote = c;
            else if (iscntrl(c))
               c = ' ';
            else
               c = toupper(c);
            ptr[line_sz] = c;
         }
      }
   }
   // if no complete line, just skip line begining
   *buffsz = beg_sz;
   return(0);
}


// returns: consumed characters in the read buffer
//
size_t extract_command(channel_t* channel, char* buffptr, size_t buffsz, command_t** comm)
{
   command_t* new_comm;
   uint16_t   flags;
   char*      cmdptr = buffptr;
   size_t     consumed = buffsz;
   size_t     comm_sz = find_command_lines(&cmdptr, &consumed, &flags);

LOG_COMM("comm_sz / consumed : %zd / %zd\n", comm_sz, consumed);
   if (comm_sz == 0)  // if no valid full command line, just skip
   {
      *comm = NULL;
      return(consumed);   // skip only line begining
   }
   new_comm = malloc(sizeof(command_t));
   if (new_comm == NULL)
   {
      SYSTEM_ERROR();
      *comm = NULL;
      return(consumed);
   }
   new_comm->channel = channel;
   new_comm->flags = flags;
   new_comm->cmd = cmdptr;
   cmdptr[comm_sz] = 0;       // make sure that command line is zero terminated
   preparse_command(new_comm);

   show_comm(new_comm, "my command");
   *comm = new_comm;
   return(consumed);
}

// -------------------------------------------------------

uint32_t calculate_binary_checksum(void* datapt, int unit, int size)
{
   uint32_t  checksum = 0;
   uintptr_t pt = (uintptr_t)datapt;

   switch(unit)
   {
      case 1:
         while(size--)
         {
            checksum += *(uint8_t *)pt;
            pt += unit;
         }
         break;

      case 2:
         while(size--)
         {
            checksum += *(uint16_t *)pt;
            pt += unit;
         }
         break;

      case 4:
         while(size--)
         {
            checksum += *(uint32_t *)pt;
            pt += unit;
         }
         break;

      case 8:
         while(size--)
         {
            checksum += *(uint64_t *)pt;
            pt += unit;
         }
         break;

      default:
         SYSTEM_ERROR();
         break;
   }
   return(checksum);
}

//  How to receive binary data:
//     get_binary_com_info();
//     get_binary_data();
//
//  integrity of binary reception is checked by complete_binary_reception()

binary_t *get_binary_com_info(void)
{
   command_t* comm = get_current_command();
   return(&(comm->inp_binary));
}

int get_binary_data(void* datapt, int unit, int size)
{
   command_t *comm = get_current_command();
   channel_t *channel = comm->channel;
   size_t     nbytes = size * unit;

   if (unit != comm->inp_binary.unit)
   {
      SYSTEM_ERROR();
      // Reception of binary data will be completed by complete_binary_reception();
      return(-1);
   }
   else if (size <= 0)
   {
      SYSTEM_ERROR();
      // Reception of binary data will be completed by complete_binary_reception();
      return(-1);
   }
   else if (nbytes > comm->inp_binary.bytes_to_transfer)
   {
      SYSTEM_ERROR();
      channel_readbin(channel, (uint8_t *)datapt, comm->inp_binary.bytes_to_transfer, READBIN_TIMEOUT_SEC);
      comm->inp_binary.bytes_to_transfer = 0;
      return(-1);
   }
   else
   {
      if (channel_readbin(channel, (uint8_t *)datapt, nbytes, READBIN_TIMEOUT_SEC) < 0)
      {
         LOG_ERROR("failed ... %zd\n", nbytes);
         return(-1);
      }
      comm->inp_binary.bytes_to_transfer -= nbytes;
      LOG_COMM("remaining ... %d bytes\n", comm->inp_binary.bytes_to_transfer);

      if (comm->inp_binary.bytes_to_transfer == 0)
         unblock_read_channel(channel, comm);

      if (!(comm->inp_binary.flags & BIN_FLG_NOCHKSUM))
      {
         comm->inp_binary.calc_checksum += calculate_binary_checksum(datapt, unit, size);
         if (comm->inp_binary.bytes_to_transfer == 0)
         {
            if (comm->inp_binary.calc_checksum != comm->inp_binary.checksum)
            {
               LOG_ERROR("wrong checksum: 0x%08x vs 0x%08x\n",
                         comm->inp_binary.calc_checksum, comm->inp_binary.checksum);
               return(-1);
            }
         }
      }
      return(comm->inp_binary.bytes_to_transfer);
   }
}

#define DUMMY_MAX 16000
void complete_binary_reception(void)
{
   command_t* comm = get_current_command();
   channel_t* channel = comm->channel;
   uint8_t    dummy[DUMMY_MAX];
   int        rem_bytes = comm->inp_binary.bytes_to_transfer;
   int        next;

   if (rem_bytes == 0)
      return;

   while(rem_bytes)
   {
      next = rem_bytes > DUMMY_MAX? DUMMY_MAX: rem_bytes;

      if (channel_readbin(channel, dummy, next, READBIN_TIMEOUT_SEC) != DANCE_OK)
         LOG_ERROR("complete_binary_reception(): bad binary header reception\n");

      rem_bytes -= next;
   }
   comm->inp_binary.bytes_to_transfer = 0;
}

void show_binary_header(binary_header_t* header)
{
   LOG_COMM("Signature: 0x%08x, %d\n", header->signature, header->signature);
   LOG_COMM("Size:      0x%08x, %d\n", header->size, header->size);
   LOG_COMM("Checksum:  0x%08x, %d\n", header->checksum, header->checksum);
}


danceerr_t init_binary_header(command_t* comm, int unit, int size, int flags, uint32_t chksum)
{
   if (IS_BINARY_OUTPUT_DONE(comm))
      SYSTEM_ERROR();
   else if (unit != 1 && unit != 2 && unit != 4 && unit != 8)
      errorf("Bad binary unit value %d (should be 1, 2, 4 or 8)", unit);
   else if ((flags & BIN_FLG_MASK) != flags)
      errorf("Bad binary transfer flags");
   else if (!IS_COMM_ERROR(comm))
   {
      binary_header_t* header;
      dblock_t*        dblk;

      dblk = dblock_create(NULL, NULL, sizeof(binary_header_t), MEM_DEFAULT);
      if (dblk == NULL)
         return DANCE_ERROR;

      dblock_setused(dblk, sizeof(binary_header_t));
      header = dblock_beg(dblk);
      header->signature = BIN_SIGNATURE_32 | flags | unit;
      header->size = size;
      header->checksum = chksum;

      comm->out_binary.unit = unit;
      comm->out_binary.size = size;
      comm->out_binary.bytes_to_transfer = size * unit;
      comm->out_binary.calc_checksum = 0;

      LOG_COMM("cbuff: %p, %p, %p\n", dblk, header, dblock_beg(dblk));
      show_binary_header(header);

      combuflist_append(comm->answ_list, dblk);
      return DANCE_OK;
   }
   return DANCE_ERROR;
}

int send_binary_buffer(mblock_t* mblk, int unit, int size, int flags)
{
   command_t* comm = get_current_command();
   dblock_t*  dblk;
   uint32_t   chksum;

   if (unit * size > mblock_size(mblk))
   {
      SYSTEM_ERROR();
      comm->out_binary.bytes_to_transfer = 0;
      return(-1);
   }
   if (flags & BIN_FLG_NOCHKSUM)
      chksum = 0xffffffff;
   else
      chksum = calculate_binary_checksum(mblock_ptr(mblk), unit, size);

   if (init_binary_header(comm, unit, size, flags, chksum) != DANCE_OK)
      return(-1);

   dblk = dblock_create(NULL, mblk, comm->out_binary.bytes_to_transfer, MEM_DEFAULT);

   if (dblk == NULL)
   {
      // return something reasonable
      mblock_release(mblk, DESTROY_DEFAULT);
      return(-1);
   }
   else
   {
//LOG_COMM("remain: %d, %p, %p\n", dblock_used(dblk), dblock_beg(dblk), mblk->data);
      combuflist_append(comm->answ_list, dblk);
      return(dblock_used(dblk));
   }
}

int send_binary_dummy(command_t* comm) {
   if (init_binary_header(comm, 1, 0, BIN_FLG_NOCHKSUM, 0) == DANCE_OK)
      return(0);
   else
      return(-1);

}

void command_reply(command_t* comm)
{
   int flags = comm->flags;
   dstream_handle_t* dstream;

   if (flags & CMDFLG_ERROR)
      answer_error_reply(comm);
   else if (flags & CMDFLG_QUERY)
   {
      if (flags & CMDFLG_BINARY)
      {
         if (!IS_BINARY_OUTPUT_DONE(comm))
            send_binary_dummy(comm);
         answer_check(comm);
/*
         if (!IS_BINARY_OUTPUT_DONE(comm))
         {
            error_printf(comm, NOTBINARYMESSAGE);
            answer_error_reply(comm);
         }
         else
         {
            answer_check(comm);
         }
*/
      }
      else if (dblock_used(comm->main_answ) == 0) // if query and no answer in buffer
      {
         error_printf(comm, NOTIMPLMESSAGE);
         answer_error_reply(comm);
      }
      else
      {
         answer_check(comm);
      }
   }
   else   // CMDFLG_ACKNOWLEDGE
      answer_noerror_reply(comm);

   if (flags & CMDFLG_DSTREAM)
   {
     // if the query was sent while dstream enabled
     // use the saved state in comm->flags, not the current one

     if (flags & CMDFLG_BINARY) dstream = comm->channel->binary_dstream;
     else dstream = comm->channel->ascii_dstream;
     dstream_framize_bufferlist(dstream, comm->answ_list);
   }

   channel_send_bufferlist(comm->answ_list);
}

// -------------------------------------------------------------------
size_t get_binary_header(command_t* comm)
{
   binary_header_t header;
   int             unit;

   // this needs a timeout
   if (channel_readbin(comm->channel, (void *)&header, sizeof(header), READBIN_TIMEOUT_SEC) < 0)
   {
      LOG_ERROR("Bad binary header reception\n");
      goto on_error;
   }

   if ((header.signature & BIN_SIGNATURE_MASK) != BIN_SIGNATURE_32)
   {
      LOG_ERROR("Bad signature: 0x%08x\n", header.signature);
      goto on_error;
   }

   if (header.size == 0)
   {
      unit = 1;    // unit does not matter in this case
   }
   else
   {
      unit = header.signature & BIN_UNIT_MASK;
      if (unit != 1 && unit != 2 && unit != 4 && unit != 8)
      {
         LOG_ERROR("Bad unit: 0x%08x\n", header.signature);
         goto on_error;
      }
   }

   comm->inp_binary.unit = unit;
   comm->inp_binary.flags = header.signature & BIN_FLG_MASK;
   comm->inp_binary.size = header.size;
   comm->inp_binary.checksum = header.checksum;
   comm->inp_binary.bytes_to_transfer = unit * header.size;
   comm->inp_binary.calc_checksum = 0;

   LOG_COMM("binary header: unit: %d, flags: 0x%x, size: %d, checksum: 0x%08x\n",
             unit, comm->inp_binary.flags, header.size, header.checksum);

   return(sizeof(header));

 on_error:
   comm->inp_binary.unit = 0;
   comm->inp_binary.flags = 0;
   comm->inp_binary.size = 0;
   comm->inp_binary.checksum = 0;
   comm->inp_binary.bytes_to_transfer = 0;
   comm->inp_binary.calc_checksum = 0;
   return(0);
}

void command_forward(command_t* comm)
{
   // forward_execute(); ???
   // how to deal with answer buffers??
   error_printf(comm, "Command forwarding not yet implemented");
}

void command_execute(command_t* comm)
{
   // if binary command (not query), get the data header
   if (IS_BINARY_COMMAND(comm))
   {
      if (!get_binary_header(comm))
      {
         error_printf(comm, DABFMTMESSAGE);
         return;
      }
   }

   if (comm->flags & CMDFLG_FORWARD)
   {
      command_forward(comm);
   }
   else
   {
      if (comm->flags & CMDFLG_BROADCAST)
         command_forward(comm);

      if (comm->mentry->flags & _DANCE_COMMAND)
         comm->param_lists = commlists;
      else
         comm->param_lists = applists;

      parse_input_parameters(comm);
      if (!(comm->flags & CMDFLG_ERROR))
      {
         if (!hook_precmdfilter(comm)) {
            comm->mentry->comm_funct(comm);
         }
         hook_postcmdfilter(comm);
         if (!(comm->flags & CMDFLG_ERROR) && (comm->flags & CMDFLG_QUERY)) {
            answer_output_data(comm->out_data);
         }
      }
   }
}


// completes parsing and execution of commands started to
//   being parsed by extract_command()
//
void complete_command(void* arg) {
   command_t* comm = arg;

   set_current_command(comm);

   if (IS_VALID_COMMAND(comm)) {
      clone_comm_iodata(comm);
      if (IS_ANSWER_COMMAND(comm)) {
         answer_prepare(comm);
         command_execute(comm);
         command_reply(comm);
      } else {
         command_execute(comm);
      }
      free_comm_iodata(comm);

   } else {  // an error happened

      COMMLED_AUX(ledCOMMERROR);

      if (IS_ANSWER_COMMAND(comm)) {
         answer_prepare(comm);
         command_reply(comm);
      }
   }
   if (IS_BINARY_COMMAND(comm))    // make sure that all binary data is swallowed
      complete_binary_reception();

TODO //  review error storage mechanism
   if (!IS_COMM_ERROR(comm))
      comm->errormsg_sz = 0;

   set_current_command(NULL);
}

// -------------------------------------------- endianess management

#if __BYTE_ORDER == __BIG_ENDIAN
uint64_t byte_swap64(uint64_t value)
{
   char retval[8];

   retval[0] = *((char *)&value + 7);
   retval[1] = *((char *)&value + 6);
   retval[2] = *((char *)&value + 5);
   retval[3] = *((char *)&value + 4);
   retval[4] = *((char *)&value + 3);
   retval[5] = *((char *)&value + 2);
   retval[6] = *((char *)&value + 1);
   retval[7] = *((char *)&value + 0);
   return(*((uint64_t *)&retval));
};

uint32_t byte_swap32(uint32_t value)
{
   char retval[4];

   retval[0] = *((char *)&value + 3)
   retval[1] = *((char *)&value + 2)
   retval[2] = *((char *)&value + 1)
   retval[3] = *((char *)&value + 0)
   return(*((uint32_t *)&retval))
};

uint16_t byte_swap16(uint16_t value)
{
   char retval[2];

   retval[0] = *((char *)&value + 1)
   retval[1] = *((char *)&value + 0)
   return(*((uint16_t *)&retval));
};
#endif
