/*---------------------------------------------------------------------------
 *
 * $Revision: 1899 $
 *
 * $Id: runtime.h 1899 2018-10-10 07:15:19Z fajardo $
 *
 *------------------------------------------------------------------------- */


#ifndef _RUNTIME_H_INCLUDED_
#define _RUNTIME_H_INCLUDED_


#include <stdint.h>
#include <sys/types.h>
#include <pthread.h>
#include "memory.h"
#include "lnklist.h"
#include "errors.h"
#include "taskqueue.h"
#include "timer.h"

struct io_handle_s;
struct channel_s;
struct dstream_handle_s;
struct http_info_s;

typedef enum
{
  IOTYPE_UNKNOWN = 0,
  IOTYPE_SIGNAL,
  IOTYPE_SERVERTCP,
  // Main channel ios
  IOTYPE_CHANNEL,
  // Secondary channel ios
  IOTYPE_FORWARD,
  // userland IRQs,
  IOTYPE_UIRQ,
  // ethernet related events
  IOTYPE_ETH
} iotype_t;


typedef int (*rtime_iofn_t)(struct io_handle_s*);

typedef struct
{
   const rtime_iofn_t write_fn;
   const rtime_iofn_t read_fn;
   const rtime_iofn_t close_fn;
} io_ops_t;


typedef struct io_handle_s
{
   iotype_t             type;
   int                  fd;
   pthread_mutex_t      wlock;
   volatile uint32_t    flags;
   const io_ops_t*      ops;
   void*                udata;
   lnklist_item_t*      item;
} io_handle_t;

#define IOPTR_TO_CHANNEL(ioptr) ((channel_t*)((ioptr)->udata))


#define IOFLAG_OPEN    (1 << 0)
#define IOFLAG_READ    (1 << 1)
#define IOFLAG_WRITE   (1 << 2)
#define IOFLAG_CLOSE   (1 << 3)
#define IOFLAG_TIMEOUT (1 << 4)
#define IOFLAG_BLOCKED (1 << 5)

typedef struct
{
   io_handle_t*      io;
   uint32_t          flags;
   struct channel_s* channel;

   union
   {
      struct
      {
         void* compl_data;
      } write;
   } u;
} io_info_t;


#define READBUF_SIZE      10240      // receiving buffer

typedef struct
{
   uint8_t   buf[READBUF_SIZE];
   uint8_t*  pos;
   size_t    used;
} read_buf_t;

typedef struct channel_s
{
   struct io_handle_s* io;
   struct http_info_s* http_info; // protocol specific info
   struct dstream_handle_s* ascii_dstream;
   struct dstream_handle_s* binary_dstream;
   char*               address;
   size_t              address_sz;
   read_buf_t          read_buffer;    // input buffer
   void*               block_comm;     // blocking command
   uint32_t            queued_comms;   // number of ongoing queued commands
   lnklist_t           out_cbufflist;  // list of output buffer lists
   cbuflist_t*         curr_cbufflist; // current output buffer list
} channel_t;

// in runtime.c
//----------------------------------------------------------

// runtime main functions
danceerr_t runtime_init(void);
danceerr_t runtime_loop(void);

int is_in_runtime_thread(void);

io_handle_t* create_io_handle(int fd, iotype_t type, const io_ops_t *io_ops, void *udata);
void free_io_handle(io_handle_t* io);

void block_io_read(io_handle_t* io);
void unblock_io_read(io_handle_t* io);

danceerr_t relay_commexecution(task_fn_t fn, void* data, uint32_t flags);

timeritem_t* add_oneshot_task(unsigned int ms, timerfn_t fn, void* arg);
timeritem_t* restart_oneshot_task(timeritem_t* it);
timeritem_t* add_periodic_task(unsigned int ms, timerfn_t fn, void* arg);
timeritem_t* add_oneshot_cmdtask(unsigned int ms, timerfn_t fn, void* arg);
timeritem_t* add_periodic_cmdtask(unsigned int ms, timerfn_t fn, void* arg);


// in channels.c
//----------------------------------------------------------
void process_read_channel(channel_t* channel);
danceerr_t channel_readbin(channel_t* ch, uint8_t* data, size_t size, int timeout);
void unblock_read_channel(channel_t* channel, void* block_comm);


// io tools

int do_nonblock(int fd, int set);
#define set_nonblock(fd) do_nonblock(fd, 1);
#define clr_nonblock(fd) do_nonblock(fd, 0);


// reboot related stuff

#define REBOOT_FLAG_CPU (1 << 0)
#define REBOOT_FLAG_FPGA (1 << 1)
#define REBOOT_FLAG_SOFT (1 << 2)

extern uint32_t reboot_flags;
void do_reboot(uint32_t);

#ifdef __INCLUDE_DANCE_DECLARATIONS
uint32_t reboot_flags = 0;
#endif

#define REQUEST_CPU_REBOOT() \
do { reboot_flags |= REBOOT_FLAG_CPU; } while(0)

#define REQUEST_SOFT_REBOOT() \
do { reboot_flags |= REBOOT_FLAG_SOFT; } while(0)

#define IS_REBOOT_REQUESTED() (reboot_flags != 0)

#define CHECK_FOR_REBOOT() \
do { if (IS_REBOOT_REQUESTED()) do_reboot(reboot_flags); } while(0)



#endif   // _RUNTIME_H_INCLUDED_
