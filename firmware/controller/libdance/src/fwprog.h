#ifndef __FWPROG_H_INCLUDED__
#define __FWPROG_H_INCLUDED__


#include <stdint.h>


#define PROG_SUCCESS    0
#define PROG_ERROR      -1

// prog_fpga_spi
// desc : Update the FPGA bitstream using SPI
// Example :
// dancebus_t* bus = NULL;
// bus = DANCE_BUS_PT_INDEX(1);
// prog_fpga_spi(bus->ident, 0x01, 0x00, "/tmp/bitstream.bit")
int prog_fpga_spi(const char* pci_ident
                  , int pci_bar
                  , int pci_off
                  , const char* bitpath);


// prog_fpga_jtag
// desc : Update the FPGA bitstream using JTAG
// Example :
// prog_fpga_jtag(1, "/tmp/bitstream.bit", 0);
int prog_fpga_jtag(uint16_t pos
                  , const char* bitpath
                  , uint32_t flags);

#endif // __FWPROG_H_INCLUDED__