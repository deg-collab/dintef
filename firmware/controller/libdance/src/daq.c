#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include <math.h>
#include <sys/types.h>

/* do not include dancemain.h to avoid linker multiple definitions */
#include "libdance.h"


/* TODO: implement and use extbus block based register access */
#include "libepci.h"
extern epcihandle_t dev_handles[6];

#include "libedma.h"
#include "libebuf.h"




/* low level layer */

/* The data acquisition unit (daq) collects data sources. */
/* Multiple sources are available (DAQ_SRC_MAX). A source */
/* is identified by an index. A particular source is selected */
/* for acquisition by setting the corresponding bit to 1 in a */
/* vector (DAQ_SRC_SEL). At most DAQ_SRC_MAX sources are */
/* available. Sources have a fixed width (DAQ_SRC_WIDTH, in bytes). */
/* Once the acquisition begins, sources are stored contiguously */
/* in memory based on their index (first index stored first). A */
/* particular sample consists of a set of sources. A write position */
/* pointer is maintained (DAQ_WPOS_VAL). It counts the number of */
/* samples written. Samples are written until the acquisition is */
/* stopped or DAQ_WPOS_TOP is reached. Sampling triggers are implemented */
/* and selected by their index using DAQ_TRIG_SEL and DAQ_TRIG_MAX. */
/* Currently, only one single trigger exists. It relies on a counter */
/* (CNT_FREQ, CNT_CTL, CNT_TOP). */


typedef struct daq_handle_s
{
  unsigned int is_valid;

  pthread_mutex_t lock;

  epcihandle_t epci;
  size_t pci_rw_off;
  size_t pci_ro_off;

  size_t rpos;

  mblock_t mblk;
  edma_handle_t dma;
  ebuf_handle_t buf;

  uint32_t src_max;
  uint32_t src_width;
  uint32_t mem_size;

} daq_handle_t;

static daq_handle_t g_daq = { .is_valid = 0 };



#ifdef FLM_LEGACY_TOREMOVE
/* ronly register access routines */

#define DAQ_SRC_MAX 0x00 /* number of selectable sources */
#define DAQ_SRC_WIDTH 0x04 /* in bytes */
#define DAQ_TRIG_MAX 0x08 /* number of selectable triggers */
#define DAQ_WPOS_VAL 0x0c /* in records */
#define DAQ_MEM_SIZE 0x10 /* in bytes */
#define DAQ_STA 0x14
#define CNT_FREQ 0x18 /* in Hz */
#define DBG_RO 0x1c

static int daq_read_reg(daq_handle_t* daq, size_t off, uint32_t* x)
{
  off += daq->pci_ro_off;
  if (epci_rd32_reg(daq->epci, off, x) == EPCI_ERR) return -1;
  return 0;
}

__attribute__((unused))
static int daq_read_reg64(daq_handle_t* daq, size_t off, uint64_t* x)
{
  uint32_t hi;
  uint32_t lo;

  if (epci_rd32_reg(daq->epci, off + 0, &hi) == EPCI_ERR) return -1;
  if (epci_rd32_reg(daq->epci, off + 4, &lo) == EPCI_ERR) return -1;

  *x = ((uint64_t)hi << 32) | (uint64_t)lo;

  return 0;
}

__attribute__((unused))
static int daq_read_reg_rw(daq_handle_t* daq, size_t off, uint32_t* x)
{
  off += daq->pci_rw_off;
  if (epci_rd32_reg(daq->epci, off, x) == EPCI_ERR) return -1;
  return 0;
}
#endif /* FLM_LEGACY_TOREMOVE */


static uint32_t daq_read_src_max(daq_handle_t* daq)
{
  uint32_t x;
#ifdef FLM_LEGACY_TOREMOVE
  daq_read_reg(daq, DAQ_SRC_MAX, &x);
#else
  x = REG_VALUE(DAQ_SRC_MAX);
#endif
  return x;
}

static uint32_t daq_read_src_width(daq_handle_t* daq)
{
  uint32_t x;
#ifdef FLM_LEGACY_TOREMOVE
  daq_read_reg(daq, DAQ_SRC_WIDTH, &x);
#else
  x = REG_VALUE(DAQ_SRC_WIDTH);
#endif
  return x;
}

__attribute__((unused))
static uint32_t daq_read_trig_max(daq_handle_t* daq)
{
  uint32_t x;
#ifdef FLM_LEGACY_TOREMOVE
  daq_read_reg(daq, DAQ_TRIG_MAX, &x);
#else
  x = REG_VALUE(DAQ_TRIG_MAX);
#endif
  return x;
}

static uint32_t daq_read_wpos_val(daq_handle_t* daq)
{
  uint32_t x;
#ifdef FLM_LEGACY_TOREMOVE
  daq_read_reg(daq, DAQ_WPOS_VAL, &x);
#else
  x = REG_VALUE(DAQ_WPOS_VAL);
#endif
  return x;
}

static uint32_t daq_read_mem_size(daq_handle_t* daq)
{
  uint32_t x;
#ifdef FLM_LEGACY_TOREMOVE
  daq_read_reg(daq, DAQ_MEM_SIZE, &x);
#else
  x = REG_VALUE(DAQ_MEM_SIZE);
#endif
  return x;
}

static uint32_t daq_read_sta(daq_handle_t* daq)
{
  uint32_t x;
#ifdef FLM_LEGACY_TOREMOVE
  daq_read_reg(daq, DAQ_STA, &x);
#else
  x = REG_VALUE(DAQ_STA);
#endif
  return x;
}

static uint32_t daq_read_cnt_freq(daq_handle_t* daq)
{
  uint32_t x;
#ifdef FLM_LEGACY_TOREMOVE
  daq_read_reg(daq, CNT_FREQ, &x);
#else
  x = REG_VALUE(DAQ_CNT_FREQ);
#endif
  return x;
}

__attribute__((unused))
static uint32_t daq_read_dbg_ro(daq_handle_t* daq)
{
  uint32_t x;
#ifdef FLM_LEGACY_TOREMOVE
  daq_read_reg(daq, DBG_RO, &x);
#else
  x = REG_VALUE(DAQ_DBG_RO);
#endif
  return x;
}



#ifdef FLM_LEGACY_TOREMOVE
/* rw register access routines */

#define DAQ_SRC_SEL 0x00
#define DAQ_TRIG_SEL 0x04
#define DAQ_WPOS_TOP 0x08 /* in records */
#define DAQ_CTL 0x0c
#define CNT_TOP 0x10 /* divides CNT_FREQ */
#define CNT_CTL 0x14
#define DBG_RW 0x18

static int daq_write_reg(daq_handle_t* daq, size_t off, uint32_t x)
{
  off += daq->pci_rw_off;
  if (epci_wr32_reg(daq->epci, off, x) == EPCI_ERR) return -1;
  return 0;
}

__attribute__((unused))
static int daq_write_reg64(daq_handle_t* daq, size_t off, uint64_t x)
{
  const uint32_t hi = (uint32_t)(x >> 32);
  const uint32_t lo = (uint32_t)(x & 0xffffffff);

  if (epci_wr32_reg(daq->epci, off + 0, hi) == EPCI_ERR) return -1;
  if (epci_wr32_reg(daq->epci, off + 4, lo) == EPCI_ERR) return -1;

  return 0;
}
#endif


static void daq_write_src_sel(daq_handle_t* daq, uint32_t x)
{
#ifdef FLM_LEGACY_TOREMOVE
  daq_write_reg(daq, DAQ_SRC_SEL, x);
#else
  REG_WRITE(DAQ_SRC_SEL, x);
#endif
}

static void daq_write_trig_sel(daq_handle_t* daq, uint32_t x)
{
#ifdef FLM_LEGACY_TOREMOVE
  daq_write_reg(daq, DAQ_TRIG_SEL, x);
#else
  REG_WRITE(DAQ_TRIG_SEL, x);
#endif
}

static void daq_write_wpos_top(daq_handle_t* daq, uint32_t x)
{
#ifdef FLM_LEGACY_TOREMOVE
  daq_write_reg(daq, DAQ_WPOS_TOP, x);
#else
  REG_WRITE(DAQ_WPOS_TOP, x);
#endif
}

static void daq_write_daq_ctl(daq_handle_t* daq, uint32_t x)
{
#ifdef FLM_LEGACY_TOREMOVE
  daq_write_reg(daq, DAQ_CTL, x);
#else
  REG_WRITE(DAQ_CTL, x);
#endif
}

static void daq_reset_wpos(daq_handle_t* daq)
{
  daq_write_daq_ctl(daq, 1 << 0);
  daq_write_daq_ctl(daq, 0);
}

static void daq_write_strig(daq_handle_t* daq)
{
  daq_write_daq_ctl(daq, 1 << 1);
  daq_write_daq_ctl(daq, 0);
}

static void daq_write_cnt_top(daq_handle_t* daq, uint32_t x)
{
#ifdef FLM_LEGACY_TOREMOVE
  daq_write_reg(daq, CNT_TOP, x);
#else
  REG_WRITE(DAQ_CNT_TOP, x);
#endif
}

__attribute__((unused))
static void daq_write_dbg_rw(daq_handle_t* daq, uint32_t x)
{
#ifdef FLM_LEGACY_TOREMOVE
  daq_write_reg(daq, DBG_RW, x);
#else
  REG_WRITE(DAQ_DBG_RW, x);
#endif
}

static int daq_disable(daq_handle_t* daq)
{
  /* disable all acquisition triggers */

  daq_write_trig_sel(daq, 0);

  return 0;
}

static size_t count_ones(uint32_t mask, size_t size)
{
  size_t n = 0;
  for (n = 0; size; --size, mask >>= 1) n += (size_t)(mask & 1);
  return n;
}

static int daq_setup
(
 daq_handle_t* daq,
 uint32_t src_sel,
 uint32_t trig_start, uint32_t trig_sampl,
 uint32_t fsampl, size_t nsampl
)
{
  /* src_sel the selected sources bitmap */
  /* trig_xxx the selected trigger bitmaps */
  /* fsampl the sampling frequency, in Hz or 0 */
  /* nsampl the sample count, in records or 0 for max */

  size_t src_nsel;
  size_t sampl_max;

  /* disable while configuring */

  daq_disable(daq);

  /* count and enable selected sources */

  if ((size_t)daq->src_max > 16) {
    errorf("dsource count too high");
    goto on_error;
  }

  src_nsel = count_ones(src_sel, (size_t)daq->src_max);
  daq_write_src_sel(daq, (src_nsel << 16) | src_sel);

  /* set maximum number of records */
  /* -1 for max to prevent counter overflow */
  sampl_max = (size_t)(daq->mem_size / (src_nsel * daq->src_width)) - 1;
  if (nsampl > sampl_max) {
    errorf("sample count too high");
    goto on_error;
  }

  if (nsampl == 0) nsampl = sampl_max;
  daq_write_wpos_top(daq, (uint32_t)nsampl);
  daq_reset_wpos(daq);
  daq->rpos = 0;

  /* setup user programmable frequency */
  if (fsampl != 0) {
    const uint32_t fcnt = daq_read_cnt_freq(daq);
    const uint32_t x = (uint32_t)ceil((double)fcnt / (double)fsampl);

    if (x >= (1 << 16)) {
      const uint32_t min_fsampl = 1 + (fcnt / (1 << 16));
      errorf("sampling frequency too low (min: %uHz)", min_fsampl);
      goto on_error;
    }

    daq_write_cnt_top(daq, x);
  }

  /* select start and sampling triggers */
  daq_write_trig_sel(daq, (trig_sampl << 8) | trig_start);

  return 0;

 on_error:
  return -1;
}

static daq_handle_t* daq_open(void)
{
  daq_handle_t* const daq = &g_daq;

  if (daq->is_valid == 0) return NULL;
  if (pthread_mutex_trylock(&daq->lock)) return NULL;
  return daq;
}

static void daq_close(daq_handle_t* daq)
{
  pthread_mutex_unlock(&daq->lock);
}



/* dsource */

static unsigned int dsource_is_valid(const dsource_desc_t* d)
{
  return d->flags != 0;
}

static dsource_desc_t* dsource_foreach
(int (*f)(dsource_desc_t*, void*), void* p)
{
  /* return the source that stopped iteration or NULL */

  dsource_desc_t* d = g_dsource_descs;
  size_t i;

  for (i = 0; i != DSOURCE_MAX_COUNT; ++d, ++i)
  {
    if (f(d, p)) return d;
  }

  return NULL;
}

static int cmp_dsource(dsource_desc_t* dsource, void* data)
{
  if (dsource_is_valid(dsource) == 0) return 0;
  if (strcmp(dsource->sid, data)) return 0;
  /* stop iteration on match */
  return -1;
}

static dsource_desc_t* dsource_get_by_sid(const char* sid)
{
  return dsource_foreach(cmp_dsource, (void*)sid);
}

static int find_invalid_dsource(dsource_desc_t* dsource, void* data)
{
  /* find first invalid dsource */
  if (dsource_is_valid(dsource) == 0) return -1;
  return 0;
}

static dsource_desc_t* dsource_create
(const char* sid, uint32_t flags, uint32_t hid, double fscal_or_resol)
{
  dsource_desc_t* const d = dsource_foreach(find_invalid_dsource, NULL);

  if (d == NULL) return NULL;

  d->flags = flags;
  d->hid = hid;
  d->fscal_or_resol = fscal_or_resol;

  d->sid = malloc(strlen(sid) + 1);
  if (d->sid == NULL)
  {
    /* release */
    d->flags = 0;
    return NULL;
  }
  strcpy((void*)d->sid, sid);
  d->flags |= DSOURCE_FLAG_FREE_SID;

  return d;
}

__attribute__((unused))
static void dsource_destroy(dsource_desc_t* d)
{
  if (d->flags & DSOURCE_FLAG_FREE_SID) free((void*)d->sid);
  d->flags = 0;
}

static danceerr_t dsource_set_fscal_or_resol
(const char* sid, double x, uint32_t flags)
{
  dsource_desc_t* const d = dsource_get_by_sid(sid);

  if (d == NULL) return DANCE_ERROR;

  d->flags &= ~(DSOURCE_FLAG_FSCAL | DSOURCE_FLAG_RESOL);
  d->flags |= flags;
  d->fscal_or_resol = x;

  return DANCE_OK;
}

danceerr_t dsource_set_fscal(const char* sid, double x)
{
  return dsource_set_fscal_or_resol(sid, x, DSOURCE_FLAG_FSCAL);
}

danceerr_t dsource_set_resol(const char* sid, double x)
{
  return dsource_set_fscal_or_resol(sid, x, DSOURCE_FLAG_RESOL);
}


/* dstream */

/* TODO: use hash table */
/* TODO: put in runtime instead of global */
static lnklist_t g_dstreams = LNKLIST_INITIALIZER;
dstream_handle_t* g_dstream_log;


static lnklist_for_t dstream_fini_item(lnklist_item_t* it, void* p)
{
  dstream_handle_t* const dstream = it->itmdata;
  dstream_destroy(dstream);
  return LNKLIST_CONT;
}


static void dstream_copy_id(char* a, const char* b)
{
  size_t i;

  for (i = 0; i != DSTREAM_ID_SIZE; ++i, ++a, ++b)
  {
    *a = *b;
    if (*b == 0) break ;
  }

  /* a size is DSTREAM_ID_SIZE + 1 */
  *a = 0;
}


void dstream_init_desc(dstream_desc_t* desc)
{
  desc->id = DSTREAM_ID_INVALID;
  desc->flags = 0;
}


static danceerr_t dstream_init(void)
{
  dstream_desc_t desc;

  lnklist_init(&g_dstreams, NULL);

  dstream_init_desc(&desc);
  desc.id = DSTREAM_ID_LOG;
  desc.flags = DSTREAM_FLAG_ENABLE;
  g_dstream_log = dstream_create(&desc);
  if (g_dstream_log == NULL) {
     lnklist_fini(&g_dstreams, dstream_fini_item, NULL);
     return DANCE_ERROR;
  } else
     return DANCE_OK;
}


static danceerr_t dstream_fini(void)
{
  lnklist_fini(&g_dstreams, dstream_fini_item, NULL);
  return DANCE_OK;
}


struct find_context
{
  const uintptr_t* args;

  /* the comparison function. return 0 if found. */
  int (*cmp_fn)(const uintptr_t*, void*);

  /* the item found, if any */
  lnklist_item_t* found_item;
};


static lnklist_for_t find_item_fn(lnklist_item_t* item, void* data)
{
  struct find_context* const fc = data;

  if (fc->cmp_fn(fc->args, item->itmdata) != 0) return LNKLIST_CONT;

  /* found, stop iteration */
  fc->found_item = item;
  return LNKLIST_STOP;
}


static lnklist_item_t* find_item
(
 lnklist_t* list,
 const uintptr_t* args,
 int (*cmp_fn)(const uintptr_t*, void*)
)
{
  struct find_context fc;

  fc.args = args;
  fc.cmp_fn = cmp_fn;
  fc.found_item = NULL;

  lnklist_foreach(list, find_item_fn, &fc);

  return fc.found_item;
}


static int cmp_dstream_id(const uintptr_t* args, void* p)
{
  dstream_handle_t* const dstream = p;
  const char* a = (const char*)args[0];
  const channel_t* const chan = (const channel_t*)args[1];
  const char* b = dstream->id;
  size_t i;

  for (i = 0; i != DSTREAM_ID_SIZE; ++i, ++a, ++b)
  {
    if (*a != *b) return -1;
    if (*a == 0) break ;
  }

  if ((dstream->flags & DSTREAM_FLAG_LOCAL) == 0)
  {
    /* global dstream */
    return 0;
  }

  /* local dstream, look if subscriber */

  if (chan != NULL)
  {
    if (dstream_is_subscriber(dstream, chan)) return 0;
  }

  return -1;
}


dstream_handle_t* dstream_get_by_id(const char* id, channel_t* chan)
{
  /* find a dstream. if chan not NULL, look only in subscribed streams. */
  /* use specialized dstream_get_xxx for fast lookup */

  lnklist_item_t* item;
  uintptr_t args[2];

  args[0] = (uintptr_t)id;
  args[1] = (uintptr_t)chan;

  item = find_item(&g_dstreams, args, cmp_dstream_id);
  if (item == NULL) return NULL;
  return (dstream_handle_t*)item->itmdata;
}


dstream_handle_t* dstream_get_log(void)
{
  return g_dstream_log;
}


dstream_handle_t* dstream_get_binary(channel_t* chan)
{
  return chan->binary_dstream;
}


dstream_handle_t* dstream_get_ascii(channel_t* chan)
{
  return chan->ascii_dstream;
}


static lnklist_for_t on_dstream_item(lnklist_item_t* item, void* p)
{
  int (*fn)(dstream_handle_t*, void*) = (void*)((uintptr_t*)p)[0];
  void* data = (void*)((uintptr_t*)p)[1];
  dstream_handle_t* const dstream = item->itmdata;

  if (fn(dstream, data))
    return LNKLIST_STOP;  // stop iteration
  else
    return LNKLIST_CONT;  // continue iteration
}


static void dstream_foreach
(int (*fn)(dstream_handle_t*, void*), void* data)
{
  uintptr_t x[2] = { (uintptr_t)fn, (uintptr_t)data };
  lnklist_foreach(&g_dstreams, on_dstream_item, (void*)x);
}


dstream_handle_t* dstream_create(const dstream_desc_t* desc)
{
  /* assume the stream does not already exist */

  dstream_handle_t* dstream;

  dstream = malloc(sizeof(dstream_handle_t));
  if (dstream == NULL) goto on_error_0;

  dstream->flags = desc->flags;
  dstream_copy_id(dstream->id, desc->id);
  dstream->dsource_hid = 0;
  dstream->dtrig_start = DTRIG_ID_INVALID;
  dstream->dtrig_sampl = DTRIG_ID_INVALID;
  dstream->fsampl = 0;
  dstream->tsampl = 0.0;
  dstream->nsampl = 0;
  dstream->daq = NULL;

  lnklist_init(&dstream->subscribers, NULL);
  if (pthread_mutex_init(&dstream->lock, NULL)) goto on_error_1;

  dstream->item = lnklist_add_tail(&g_dstreams, dstream);
  if (dstream->item == NULL) goto on_error_2;

  return dstream;

 on_error_2:
  pthread_mutex_destroy(&dstream->lock);
 on_error_1:
  lnklist_fini(&dstream->subscribers, NULL, NULL);
  free(dstream);
 on_error_0:
  return NULL;
}


static void dstream_stop(dstream_handle_t* dstream);

danceerr_t dstream_destroy(dstream_handle_t* dstream)
{
  pthread_mutex_destroy(&dstream->lock);
  lnklist_fini(&dstream->subscribers, NULL, NULL);
  lnklist_free_item(dstream->item);
  dstream_stop(dstream);
  free(dstream);
  return DANCE_OK;
}


static lnklist_for_t add_chan_item(lnklist_item_t* item, void* data)
{
  dstream_handle_t* const dstream = item->itmdata;
  channel_t* const        chan = data;
  lnklist_for_t           stop = LNKLIST_CONT;

  pthread_mutex_lock(&dstream->lock);
  if (lnklist_add_tail(&dstream->subscribers, chan) == NULL) stop = LNKLIST_STOP;
  pthread_mutex_unlock(&dstream->lock);

  return stop;
}


danceerr_t dstream_add_subscriber(dstream_handle_t* dstream, channel_t* chan)
{
  /* if dstream is null, means add chan to all streams */

  if (dstream == NULL) lnklist_foreach(&g_dstreams, add_chan_item, chan);
  else add_chan_item(dstream->item, chan);

  return DANCE_OK;
}


static int cmp_chan(const uintptr_t* args, void* cur_data)
{
  if ((channel_t*)args[0] == (channel_t*)cur_data) return 0;
  return -1;
}


static lnklist_for_t del_chan(lnklist_item_t* dstream_item, void* data)
{
  dstream_handle_t* const dstream = dstream_item->itmdata;
  uintptr_t looked_chan = (uintptr_t)data;
  lnklist_item_t* dchan_item;

  pthread_mutex_lock(&dstream->lock);

  dchan_item = find_item(&dstream->subscribers, &looked_chan, cmp_chan);
  if (dchan_item != NULL)
  {
    if (dstream->flags & DSTREAM_FLAG_LOCAL)
    {
      /* destroy full stream if local */
      pthread_mutex_unlock(&dstream->lock);
      dstream_destroy(dstream);
      goto on_success;
    }

    /* else */
    lnklist_free_item(dchan_item);
  }

  pthread_mutex_unlock(&dstream->lock);

  /* continue iteration */
 on_success:
  return LNKLIST_CONT;
}


danceerr_t dstream_del_subscriber(dstream_handle_t* dstream, channel_t* chan)
{
  /* if dstream is null, means remove chan from all streams */

  if (dstream == NULL) lnklist_foreach(&g_dstreams, del_chan, chan);
  else del_chan(dstream->item, chan);

  return DANCE_OK;
}


static unsigned int dstream_has_subscribers(dstream_handle_t* dstream)
{
  /* concurrency issues are OK here */
  return ((void volatile*)dstream->subscribers.head) != NULL;
}

unsigned int dstream_is_subscriber
(dstream_handle_t* dstream, const channel_t* chan)
{
  /* ASSUME(dstream != NULL); */

  uintptr_t arg = (uintptr_t)chan;
  lnklist_item_t* res;

  pthread_mutex_lock(&dstream->lock);
  res = find_item(&dstream->subscribers, &arg, cmp_chan);
  pthread_mutex_unlock(&dstream->lock);

  return (res == NULL) ? 0 : 1;
}

static unsigned int dstream_is_lone_subscriber
(dstream_handle_t* dstream, const channel_t* chan)
{
  unsigned int res;

  res = 0;

  pthread_mutex_lock(&dstream->lock);

  if (dstream->subscribers.head == NULL) goto on_done;
  if (dstream->subscribers.head->next != NULL) goto on_done;
  if (dstream->subscribers.head->itmdata != chan) goto on_done;

  res = 1;

 on_done:
  pthread_mutex_unlock(&dstream->lock);

  return res;
}

unsigned int dstream_is_enabled(dstream_handle_t* dstream)
{
  return dstream->flags & DSTREAM_FLAG_ENABLE;
}


static int dstream_flush(dstream_handle_t* dstream)
{
  daq_handle_t* const daq = dstream->daq;
  if (daq == NULL) return -1;
  daq_reset_wpos(daq);
  daq->rpos = 0;
  return 0;
}


static int dstream_apply(dstream_handle_t* dstream)
{
  uint32_t fsampl;
  uint32_t nsampl;
  int err;

  if (dstream->daq == NULL) {
    dstream->daq = daq_open();
    if (dstream->daq == NULL) {
      /* TODO: these are not system errors but client errors 
      DERR_PRINTS("DAQ not initialized or already in use");
      */
      errorf("DAQ not initialized or already in use");
      goto on_error_0;
    }
  }

  if (dstream->dsource_hid == 0) {
    errorf("dsource not specified");
    goto on_error_1;
  }

  if (dstream->dtrig_start == DTRIG_ID_INVALID) {
    errorf("start trigger not specified");
    goto on_error_1;
  }

  if (dstream->dtrig_sampl == DTRIG_ID_INVALID) {
    errorf("sampling trigger not specified");
    goto on_error_1;
  }

  if (dstream->dtrig_start & DTRIG_ID_FREQ) {
    errorf("invalid start trigger");
    goto on_error_1;
  }

  fsampl = 0;
  nsampl = dstream->nsampl;

  if (dstream->dtrig_sampl & DTRIG_ID_FREQ) {
    const double tsampl = dstream->tsampl;

    fsampl = dstream->fsampl;
    if (fsampl == 0) {
      errorf("invalid sampling frequency");
      goto on_error_1;
    }

    if (nsampl == 0) nsampl = (uint32_t)ceil((double)fsampl * tsampl);
  }

  err = daq_setup (
    dstream->daq,
    dstream->dsource_hid,
    dstream->dtrig_start, dstream->dtrig_sampl,
    fsampl, nsampl
  );

  /* error set by daq_setup */
  if (err) goto on_error_1;

  return 0;

 on_error_1:
  daq_close(dstream->daq);
  dstream->daq = NULL;

 on_error_0:
  return -1;
}


static void dstream_stop(dstream_handle_t* dstream)
{
  if (dstream->daq == NULL) return;
  daq_disable(dstream->daq);
  daq_close(dstream->daq);
  dstream->daq = NULL;
}


static size_t dstream_get_nsampl(dstream_handle_t* dstream)
{
  daq_handle_t* const daq = dstream->daq;
  if (daq == NULL) return 0;
  return (size_t)daq_read_wpos_val(daq) - daq->rpos;
}


static const char* dstream_get_status(dstream_handle_t* dstream)
{
  daq_handle_t* const daq = dstream->daq;
  uint32_t x;
  if (daq == NULL) return "NONE";
  x = daq_read_sta(daq);
  if (x & (1 << 0)) return "SAMPL";
  return "TRIG";
}


/* protocol */
/* reference: doc/HOWTOs/daq-HOWTO.txt */
/* fields are stored little endian */

typedef struct
{
#define DCHUNK_MAGIC 0xcabecafe
  uint32_t magic;

  /* header size */
  uint16_t header_size;

  /* versioning */
  uint8_t version;

  /* data size */
#define DCHUNK_DATA_MAX_SIZE UINT16_MAX
  uint16_t data_size;

  char stream_id[DSTREAM_ID_SIZE];

  /* first chunk of a frame */
#define DCHUNK_FLAG_FIRST (1 << 0)
  uint8_t flags;

  /* 0 is last chunk in frame */
  uint32_t rem_frame_size;

  /* raw data following */
  /* uint8_t data[1]; */

} __attribute__((packed)) dchunk_header_t;


static dblock_t* make_dchunk_header
(dstream_handle_t* dstream, size_t data_size)
{
  dblock_t* dblk;
  dchunk_header_t* dchunk;

  dblk = dblock_create(NULL, NULL, sizeof(dchunk_header_t), MEM_DEFAULT);
  if (dblk == NULL) return NULL;

  dchunk = (dchunk_header_t*)dblock_end(dblk);
  dchunk->magic = LITTLE_ENDIAN_32(DCHUNK_MAGIC);
  dchunk->version = 0;
  dchunk->header_size = LITTLE_ENDIAN_16(sizeof(dchunk_header_t));
  dchunk->data_size = LITTLE_ENDIAN_16((uint16_t)data_size);
  memcpy(dchunk->stream_id, dstream->id, DSTREAM_ID_SIZE);
  dchunk->flags = DCHUNK_FLAG_FIRST;
  dchunk->rem_frame_size = LITTLE_ENDIAN_32(0);

  dblock_setused(dblk, sizeof(dchunk_header_t));

  return dblk;
}

danceerr_t dstream_framize_bufferlist
(dstream_handle_t* dstream, cbuflist_t* cbufs)
{
  /* turn a bufferlist into a dframe */

  /* TODO: currently, a dblock is created and prepended to the */
  /* cbuf. we could detect this in init_answer_buffers and left */
  /* enough space in main_answ, removing the need to create an */
  /* additional dblock. */

  size_t data_size;
  dblock_t* dblk;

  data_size = combuflist_size(cbufs);
  if (data_size > DCHUNK_DATA_MAX_SIZE) goto on_error_0;

  dblk = make_dchunk_header(dstream, data_size);
  if (dblk == NULL) goto on_error_0;

  if (combuflist_prepend(cbufs, dblk)) goto on_error_1;

  return DANCE_OK;

 on_error_1:
  dblock_release(dblk, DESTROY_DEFAULT);
 on_error_0:
  return DANCE_ERROR;
}

static dblock_t* dblock_dup(dblock_t* dblk)
{
  /* fully duplicate a dblock */

  const size_t size = dblock_used(dblk);
  dblock_t* new_dblk;

  new_dblk = dblock_create(NULL, NULL, size, MEM_DEFAULT);
  if (new_dblk == NULL) return NULL;

  memcpy(dblock_beg(new_dblk), dblock_beg(dblk), size);
  dblock_setused(new_dblk, size);

  return new_dblk;
}

static lnklist_for_t write_frame_fn(lnklist_item_t* it, void* arg)
{
  /* TODO: optimize memory allocations and copies */

  dstream_handle_t* const dstream = ((void**)arg)[0];
  dblock_t* const data_dblk = ((void**)arg)[1];
  channel_t* const chan = it->itmdata;
  io_handle_t* const io = chan->io;
  dblock_t* header_dblk;
  dblock_t* new_data_dblk;
  cbuflist_t* cbl = NULL;
  size_t data_size;

  data_size = dblock_used(data_dblk);

  header_dblk = make_dchunk_header(dstream, data_size);
  if (header_dblk == NULL) goto on_error_0;

  new_data_dblk = dblock_dup(data_dblk);
  if (new_data_dblk == NULL) goto on_error_1;

  pthread_mutex_lock(&io->wlock);

  cbl = combuflist_create(NULL, NULL);
  if (cbl == NULL) goto on_error_2;
  if (combuflist_append(cbl, header_dblk)) goto on_error_2;
  if (combuflist_append(cbl, new_data_dblk)) goto on_error_2;

  if (chan->curr_cbufflist == NULL)
  {
    chan->curr_cbufflist = cbl;
  }
  else
  {
    if (lnklist_add_tail(&chan->out_cbufflist, cbl) == NULL)
      goto on_error_2;
  }

  channel_send_bufferlist(cbl);

  pthread_mutex_unlock(&io->wlock);
  return LNKLIST_CONT;

 on_error_2:
  pthread_mutex_unlock(&io->wlock);
  if (cbl != NULL) combuflist_free(cbl);
  dblock_release(new_data_dblk, DESTROY_DEFAULT);
 on_error_1:
  dblock_release(header_dblk, DESTROY_DEFAULT);
 on_error_0:
  return LNKLIST_STOP;
}


danceerr_t dstream_write_frame(dstream_handle_t* dstream, dblock_t* data_dblk)
{
  /* NOTE: data_dblk is duplicated, caller keeps ownership */

  void* arg[2] = { dstream, data_dblk };
  int stop;

  pthread_mutex_lock(&dstream->lock);
  stop = lnklist_foreach(&dstream->subscribers, write_frame_fn, arg);
  pthread_mutex_unlock(&dstream->lock);

  return stop == LNKLIST_STOP? DANCE_ERROR : DANCE_OK;
}

danceerr_t dstream_printf(dstream_handle_t* dstream, const char* fmt, ...)
{
  /* TODO: factorize answerf redundant code */

  static const size_t min_size = 256;

  va_list ap;
  dblock_t* dblk;
  int new_size;
  int cur_size;
  int err = -1;

  if (dstream_has_subscribers(dstream) == 0)
  {
    err = 0;
    goto on_error_0;
  }

  /* create dblock */

  dblk = dblock_create(NULL, NULL, min_size, MEM_DEFAULT);
  if (dblk == NULL) goto on_error_0;

 redo_vsnprintf:
  cur_size = (int)dblock_available(dblk);

  va_start(ap, fmt);
  new_size = vsnprintf(dblock_end(dblk), cur_size, fmt, ap);
  va_end(ap);

  if (new_size <= 0)
  {
    goto on_error_1;
  }
  else if (new_size >= cur_size)
  {
    if (dblock_resize(dblk, (size_t)new_size + 1) == (size_t)cur_size)
      goto on_error_1;
    goto redo_vsnprintf;
  }

  /* trailing zero is removed */
  dblock_setused(dblk, (size_t)new_size);

  if (dstream_write_frame(dstream, dblk)) goto on_error_1;

  /* success */
  err = 0;

 on_error_1:
  dblock_release(dblk, DESTROY_DEFAULT);
 on_error_0:
  return (err == 0) ? DANCE_OK : DANCE_ERROR;
}


/* TOREMOVE */
/* smartpix explicitly handles DMA until more adapted situation */
/* it conflicts with runtime uirq handling */
/* remove uirq item but keep resources (fd ...) for app purposes */

static unsigned int is_explicitly_managed = 0;

danceerr_t del_uirq_io(void);

danceerr_t daq_is_explicitly_managed(void)
{
  is_explicitly_managed = 1;
  del_uirq_io();
  return DANCE_OK;
}


#if PCIBUS_USED
extern uirq_handle_t* runtime_get_uirq(void);
#endif // PCIBUS_USED


danceerr_t daq_init(void)
{
  daq_handle_t* const daq = &g_daq;
  edma_desc_t dma_desc;
  size_t size;
  uint8_t* data;
  size_t i;

  daq->is_valid = 0;

  if (is_explicitly_managed == 1)
     goto on_success;

  /* invalidate sources past the last valid one */

  for (i = 0; i != DSOURCE_MAX_COUNT; ++i) {
    if (dsource_is_valid(&g_dsource_descs[i]) == 0) break ;
  }

  for (; i != DSOURCE_MAX_COUNT; ++i) {
    g_dsource_descs[i].flags = 0;
  }

  if (dstream_init()) {
    /* the only real error. others wont prevent the runtime to work. */
    return DANCE_ERROR;
  }

#ifdef FLM_LEGACY_TOREMOVE
  if (g_daq_desc.pci_bar == -1) {
    /* means daq hardware disabled */
    goto on_success;
  }
#endif /* FLM_LEGACY_TOREMOVE */

  pthread_mutex_init(&daq->lock, NULL);

#ifdef FLM_LEGACY_TOREMOVE
  /* init from instrument provided desc */
  /* TODO: temporary and dirty fix, to be removed asap !!*/
  /* TODO: replace pci_bar by resource declaration with macros */
  /* TODO: WARNING: based on hypothesis that PCIE is the first bus */
  LOG_DAQ("DAQ PCI BAR: %d\n", g_daq_desc.pci_bar);
  dancebus_t* bus = DANCE_BUS_PT_INDEX(g_daq_desc.pci_bar);  
  daq->epci = bus->info.pcihandles[g_daq_desc.pci_bar];
  if (daq->epci == EPCI_BAD_HANDLE) {
    LOG_ERROR("invalid epci handle\n");
    goto on_error_0;
  }

  daq->pci_rw_off = g_daq_desc.pci_rw_off;
  daq->pci_ro_off = g_daq_desc.pci_ro_off;
  LOG_DAQ("DAQ RW  off: 0x%04X\n", g_daq_desc.pci_rw_off);
  LOG_DAQ("DAQ RO  off: 0x%04X\n", g_daq_desc.pci_ro_off);
#endif /* FLM_LEGACY_TOREMOVE */

  daq->rpos = 0;

  daq->src_max = daq_read_src_max(daq);
  daq->src_width = daq_read_src_width(daq);
  daq->mem_size = daq_read_mem_size(daq);
  LOG_DAQ("DAQ SRC max: %d\n", daq->src_max);
  LOG_DAQ("DAQ SRC wdt: %d\n", daq->src_width);
  LOG_DAQ("DAQ MEM max: %dMB\n", daq->mem_size/(1024*1024));

  daq_disable(daq);

  /* initialize dma */

  if (ebuf_init_lib(EBUF_FLAG_PHYSICAL)) goto on_error_0;
  if (edma_init_lib()) goto on_error_1;
  if (IS_LOG(DAQ, 0)) edma_log_lib(1);

  edma_init_desc(&dma_desc);
  dma_desc.pci_id = "10ee:eb01";
  dma_desc.pci_bar = g_dma_desc.pci_bar;
  dma_desc.pci_off = g_dma_desc.pci_off;
  dma_desc.ebs_seg = g_dma_desc.ebs_seg;
  dma_desc.ebs_off = g_dma_desc.ebs_off;
  LOG_DAQ("DMA PCI BAR: %d\n",     g_dma_desc.pci_bar);
  LOG_DAQ("DMA PCI off: 0x%04X\n", g_dma_desc.pci_off);
  LOG_DAQ("DMA EBS seg: 0x%04X\n", g_dma_desc.ebs_seg);
  LOG_DAQ("DMA EBS off: 0x%04X\n", g_dma_desc.ebs_off);

#if 0 /* TODO */
  dma_desc.flags = g_dma_desc.flags | EDMA_FLAG_IRQ;
#else
  dma_desc.flags = g_dma_desc.flags;
#endif
  TODO /* investigate if the code to avoid is not larger than that */
#if PCIBUS_USED
  dma_desc.uirq = runtime_get_uirq();
#endif // PCIBUS_USED
  if (edma_open(&daq->dma, &dma_desc)) goto on_error_2;

  size = 32 * 1024 * 1024;
  if (size > (size_t)daq->mem_size) size = (size_t)daq->mem_size;
  LOG_DAQ("DAQ MEM    : %dMB\n", size/(1024*1024));
  if (ebuf_alloc(&daq->buf, size, EBUF_FLAG_PHYSICAL)) goto on_error_3;
  data = ebuf_get_data(&daq->buf);
  if (!mblock_create(&daq->mblk, data, size, MEM_PERSISTENT)) goto on_error_4;

  /* commit */

  daq->is_valid = 1;
  goto on_success;

 on_error_4:
  ebuf_free(&daq->buf);
 on_error_3:
  edma_close(&daq->dma);
 on_error_2:
  edma_fini_lib();
 on_error_1:
  ebuf_fini_lib();
 on_error_0:
  pthread_mutex_destroy(&daq->lock);

  /* wont prevent the runtime to work */
 on_success:
  return DANCE_OK;
}

void daq_fini(void)
{
  daq_handle_t* const daq = &g_daq;

  if (daq->is_valid == 0) return ;

  /* lock, invalid, unlock */
  if (pthread_mutex_lock(&daq->lock)) return ;
  daq->is_valid = 0;
  pthread_mutex_unlock(&daq->lock);
  pthread_mutex_destroy(&daq->lock);

  mblock_release(&daq->mblk, DESTROY_FORCE);
  ebuf_free(&daq->buf);
  edma_close(&daq->dma);
  edma_fini_lib();
  ebuf_fini_lib();

  dstream_fini();
}


/* commands */


/* DSTREAM */

typedef struct dstream_parse_info
{
#define DSTREAM_PARSE_FLAG_LOCAL (1 << 0)
#define DSTREAM_PARSE_FLAG_GLOBAL (1 << 1)
#define DSTREAM_PARSE_FLAG_ENABLE (1 << 2)
#define DSTREAM_PARSE_FLAG_DISABLE (1 << 3)
#define DSTREAM_PARSE_FLAG_APPLY (1 << 4)
#define DSTREAM_PARSE_FLAG_FLUSH (1 << 5)
#define DSTREAM_PARSE_FLAG_TRIG (1 << 6)
#define DSTREAM_PARSE_FLAG_SRC (1 << 7)
#define DSTREAM_PARSE_FLAG_FSAMPL (1 << 8)
#define DSTREAM_PARSE_FLAG_TSAMPL (1 << 9)
#define DSTREAM_PARSE_FLAG_NSAMPL (1 << 10)
#define DSTREAM_PARSE_FLAG_STOP (1 << 11)
#define DSTREAM_PARSE_FLAG_DEL (1 << 12)
  uint32_t flags;

  uint32_t dtrig_start;
  uint32_t dtrig_sampl;
  uint32_t dsource_hid;

  uint32_t fsampl;
  double tsampl;
  uint32_t nsampl;

} dstream_parse_info_t;

static int dstream_parse_dscope(dstream_parse_info_t* info, uint32_t x)
{
  if (x == DSCOPE_LOCAL) info->flags |= DSTREAM_PARSE_FLAG_LOCAL;
  else info->flags |= DSTREAM_PARSE_FLAG_GLOBAL;
  return 0;
}

static int dstream_parse_dstate(dstream_parse_info_t* info, uint32_t x)
{
  if (x == DSTATE_ENABLE) info->flags |= DSTREAM_PARSE_FLAG_ENABLE;
  else info->flags |= DSTREAM_PARSE_FLAG_DISABLE;
  return 0;
}

static int dstream_parse_trig(dstream_parse_info_t* info)
{
  size_t i;

  for (i = 0; i != 2; ++i)
  {
    const char* const s = I_LABEL();
    uint32_t x;

    if (strcmp(s, "FREQ") == 0) x = DTRIG_ID_FREQ;
    else if (strcmp(s, "DI1") == 0) x = DTRIG_ID_DI1;
    else if (strcmp(s, "DI2") == 0) x = DTRIG_ID_DI2;
    else if (strcmp(s, "SOFT") == 0) x = DTRIG_ID_SOFT;
    else if (strcmp(s, "APP") == 0) x = DTRIG_ID_APP;
    else {
      errorf("invalid TRIG source");
      return -1;
    }

    if (i == 0) info->dtrig_start = x;
    else info->dtrig_sampl = x;
  }

  return 0;
}

static int dstream_parse_tag(dstream_parse_info_t* info)
{
  if (I_NAMED_TAG("FSAMPL")) {
    info->flags |= DSTREAM_PARSE_FLAG_FSAMPL;
    info->fsampl = (uint32_t)I_INTEGER() * (uint32_t)I_UNIT();
  }

  if (I_NAMED_TAG("TSAMPL")) {
    info->flags |= DSTREAM_PARSE_FLAG_TSAMPL;
    info->tsampl = (double)I_FLOAT() * (double)I_UNIT();
  }

  if (I_NAMED_TAG("NSAMPL")) {
    info->flags |= DSTREAM_PARSE_FLAG_NSAMPL;
    info->nsampl = (uint32_t)I_INTEGER();
  }

  if (I_NAMED_TAG("APPLY")) {
    info->flags |= DSTREAM_PARSE_FLAG_APPLY;
  }

  if (I_NAMED_TAG("FLUSH")) {
    info->flags |= DSTREAM_PARSE_FLAG_FLUSH;
  }

  if (I_NAMED_TAG("TRIG")) {
    info->flags |= DSTREAM_PARSE_FLAG_TRIG;
    if (dstream_parse_trig(info)) return -1;
  }

  if (I_NAMED_TAG("STOP")) {
    info->flags |= DSTREAM_PARSE_FLAG_STOP;
  }
  
  if (I_NAMED_TAG("DEL")) {
    info->flags |= DSTREAM_PARSE_FLAG_DEL;
  }

  if (I_NAMED_TAG("SRC")) {
    info->flags |= DSTREAM_PARSE_FLAG_SRC;
    info->dsource_hid = 0;
    const char *lbl;

    while ((lbl = I_LABEL()) != NULL) {
      const dsource_desc_t* const dsource = dsource_get_by_sid(lbl);
      if (dsource == NULL) {
        errorf("invalid SRC signal");
        return -1;
      }
      info->dsource_hid |= dsource->hid;
    }
  }
  

  return 0;
}


/*
 "{"				\
  " oC#DSCOPE"			\
  " oC#DSTATE"			\
  " o(T#FSAMPL uI_U#FREQ)"	\
  " o(T#TSAMPL uF_U#TIME)"	\
  " o(T#NSAMPL uI)"		\
  " o(T#TRIG L L)"              \
  " o(T#APPLY)"			\
  " o(T#FLUSH)"			\
  " o(T#DEL)"			\
  " o(T#STOP)"			\
  " o(T#SRC nL)"	        \
 "}"
*/
static int dstream_parse(dstream_parse_info_t* info)
{
  info->flags = 0;

  if (dstream_parse_dscope(info, (uint32_t)I_CHOICE())) goto on_error;
  if (dstream_parse_dstate(info, (uint32_t)I_CHOICE())) goto on_error;
  if (dstream_parse_tag(info)) goto on_error;

  return 0;

 on_error:
  return -1;
}

/* "L { o() .... } " */
void commexec_DSTREAM(void)
{
  channel_t* const chan = get_current_command()->channel;
  const char* const id = I_LABEL();
  dstream_desc_t desc;
  dstream_handle_t* dstream;
  dstream_parse_info_t info;

  /* TODO: shouldn't be a default setup instead ? */
  memset(&info, 0, sizeof(info));

  if (id == DEFAULT_LABEL) {
    errorf("invalid STRMID");
    return ;
  }

  /* parse stream info */
  if (dstream_parse(&info)) {
    if (ERROR_NOTFOUND()) {
        errorf("command parsing error");
    }
    return ;
  }

  /* find stream or create new one */
  dstream = dstream_get_by_id(id, chan);

  if (info.flags & DSTREAM_PARSE_FLAG_DEL) {

    LOG_DAQ("deleting stream \"%s\"\n", id);

    if (dstream == NULL) {
      errorf("stream not found");
      return ;
    }

    dstream_destroy(dstream);

    return ;
  }

  /* stream not found, create new one */
  if (dstream == NULL) {

    LOG_DAQ("creating stream \"%s\"\n", id);
    dstream_init_desc(&desc);
    desc.id = id;

    /* create enabled, local by default */
    desc.flags = DSTREAM_FLAG_ENABLE | DSTREAM_FLAG_LOCAL;

    if (info.flags & DSTREAM_PARSE_FLAG_GLOBAL)
      desc.flags &= ~DSTREAM_FLAG_LOCAL;

    if (info.flags & DSTREAM_PARSE_FLAG_DISABLE) {

/* TODO: weird why this error occurs, due to default info.flags? */
#if 0
      if (info.flags & DSTREAM_PARSE_FLAG_GLOBAL)
      {
         /* all global streams are enabled */

         errorf("global stream cannot be locally disabled");
         return ;
      }
      desc.flags &= ~DSTREAM_FLAG_ENABLE;
#endif
      if (info.flags & DSTREAM_PARSE_FLAG_LOCAL) {
          desc.flags &= ~DSTREAM_FLAG_ENABLE;
      } else {
          /* force GLOBAL creation to ON */
          info.flags &= ~DSTREAM_PARSE_FLAG_DISABLE;
          info.flags |=  DSTREAM_PARSE_FLAG_ENABLE;

          LOG_DAQ("WARNING: global streams must be enabled\n");
      } 
    
    }

    dstream = dstream_create(&desc);
    if (dstream == NULL) {
      errorf("stream creation error");
      return ;
    }

    if (dstream_add_subscriber(dstream, chan)) {
      errorf("stream subscribing error");
      dstream_destroy(dstream);
      return ;
    }

  } else {

    LOG_DAQ("found stream \"%s\"\n", id);

    /* check configuration that cannot be modified after stream creation */
    if (info.flags & DSTREAM_PARSE_FLAG_LOCAL) {
      if ((dstream->flags & DSTREAM_FLAG_LOCAL) == 0)
         goto on_invalid_scope;
    } else if (info.flags & DSTREAM_PARSE_FLAG_GLOBAL) {
      if (dstream->flags & DSTREAM_FLAG_LOCAL) {
      on_invalid_scope:
         errorf("stream scope cannot be modified");
         return ;
      }
    }
  }

  if ((dstream->flags & DSTREAM_FLAG_LOCAL) == 0) {
    /* subscribe or unsubscribe to global stream */

    if (info.flags & DSTREAM_PARSE_FLAG_ENABLE) {
      if (dstream_is_subscriber(dstream, chan) == 0) {
         LOG_DAQ("adding subscriber\n");
         if (dstream_add_subscriber(dstream, chan)) {
           errorf("stream subscribing error");
           return ;
         }
      }
    } else if (info.flags & DSTREAM_PARSE_FLAG_DISABLE) {
      if (dstream_is_subscriber(dstream, chan)) {
         LOG_DAQ("deleting subscriber\n");
         if (dstream_del_subscriber(dstream, chan)) {
           errorf("stream unsubscribing error");
           return ;
         }
      }
    }

  } else {
    /* enable disable local stream */

    if (info.flags & DSTREAM_PARSE_FLAG_ENABLE) {
      LOG_DAQ("stream: ON\n");
      dstream->flags |= DSTREAM_FLAG_ENABLE;
    } else if (info.flags & DSTREAM_PARSE_FLAG_DISABLE) {
      LOG_DAQ("stream: OFF\n");
      dstream->flags &= ~DSTREAM_FLAG_ENABLE;
    }
  }

  /* modify configuration */

  info.flags &= ~(DSTREAM_PARSE_FLAG_LOCAL | DSTREAM_PARSE_FLAG_GLOBAL);
  info.flags &= ~(DSTREAM_PARSE_FLAG_ENABLE | DSTREAM_PARSE_FLAG_DISABLE);

  if (info.flags & (dstream_is_lone_subscriber(dstream, chan) == 0)) {
    errorf("multiple subscribers, stream cannot be modified");
    return ;
  }

  if (info.flags & DSTREAM_PARSE_FLAG_SRC) {
    LOG_DAQ("setting SRC\n");
    dstream->dsource_hid = info.dsource_hid;
  }

  if (info.flags & DSTREAM_PARSE_FLAG_FSAMPL) {
    LOG_DAQ("setting FSAMPL\n");
    dstream->fsampl = info.fsampl;
  }

  if (info.flags & DSTREAM_PARSE_FLAG_TSAMPL) {
    LOG_DAQ("setting TSAMPL\n");
    dstream->tsampl = info.tsampl;
  }

  if (info.flags & DSTREAM_PARSE_FLAG_NSAMPL) {
    LOG_DAQ("setting NSAMPL\n");
    dstream->nsampl = info.nsampl;
  }

  if (info.flags & DSTREAM_PARSE_FLAG_TRIG) {
    LOG_DAQ("setting TRIG\n");
    dstream->dtrig_start = info.dtrig_start;
    dstream->dtrig_sampl = info.dtrig_sampl;
  }

  if (info.flags & DSTREAM_PARSE_FLAG_APPLY) {
    LOG_DAQ("settings applied\n");
    if (dstream_apply(dstream)) {
      if (ERROR_NOTFOUND()) {
        errorf("apply error");
      }
      return ;
    }
  }

  if (info.flags & DSTREAM_PARSE_FLAG_FLUSH) {
    LOG_DAQ("flushing data\n");
    if (dstream_flush(dstream)) {
      errorf("flushing error");
      return ;
    }
  }

  if (info.flags & DSTREAM_PARSE_FLAG_STOP) {
    LOG_DAQ("stop requested\n");
    dstream_stop(dstream);
  }
}


/* qDSTREAM */

struct answer_dstream_args
{
  channel_t* chan;
  const char* stream_id;
  uint32_t dsource_hid;
  unsigned was_found;
};

static int answer_dsource_sid(dsource_desc_t* d, void* p)
{
  struct answer_dstream_args* const args = p;

  if (dsource_is_valid(d) == 0) return 0;
  if ((d->hid & args->dsource_hid) == 0) return 0;

  answerf(" %s", d->sid);

  /* clear processed sources */
  args->dsource_hid &= ~d->hid;

  /* no more sources or continue */
  if (args->dsource_hid == 0) return -1;
  return 0;
}

static int answer_dstream(dstream_handle_t* dstream, void* p)
{
  struct answer_dstream_args* const args = p;

  if (args->stream_id != NULL) {
    if (strcmp(dstream->id, args->stream_id)) return 0;
  }

  if (dstream->flags & DSTREAM_FLAG_LOCAL) {
    /* do not list local stream chan did not subscribe to */

    if (dstream_is_subscriber(dstream, args->chan) == 0) return 0;

    /* local stream. ON if enabled */

    answerf("%s", dstream->id);

    if (dstream_is_enabled(dstream)) answerf(" ON");
    else answerf(" OFF");

    answerf(" LOCAL");
  } else {
    /* global stream. ON if channel subscribed */

    answerf("%s", dstream->id);

    if (dstream_is_subscriber(dstream, args->chan)) answerf(" ON");
    else answerf(" OFF");

    answerf(" GLOBAL");
  }

  if (dstream->dtrig_start != DTRIG_ID_INVALID) {
    const char* start_trig;
    const char* sampl_trig;

    if (dstream->dtrig_start & DTRIG_ID_FREQ)      start_trig = "FREQ";
    else if (dstream->dtrig_start & DTRIG_ID_DI1)  start_trig = "DI1";
    else if (dstream->dtrig_start & DTRIG_ID_DI2)  start_trig = "DI2";
    else if (dstream->dtrig_start & DTRIG_ID_SOFT) start_trig = "SOFT";
    else if (dstream->dtrig_start & DTRIG_ID_APP)  start_trig = "APP";
    else start_trig = NULL;

    if (dstream->dtrig_sampl & DTRIG_ID_FREQ)      sampl_trig = "FREQ";
    else if (dstream->dtrig_sampl & DTRIG_ID_DI1)  sampl_trig = "DI1";
    else if (dstream->dtrig_sampl & DTRIG_ID_DI2)  sampl_trig = "DI2";
    else if (dstream->dtrig_sampl & DTRIG_ID_SOFT) sampl_trig = "SOFT";
    else if (dstream->dtrig_sampl & DTRIG_ID_APP)  sampl_trig = "APP";
    else sampl_trig = NULL;

    answerf(" TRIG %s", start_trig);
    if (sampl_trig != NULL) answerf(" %s", sampl_trig);
  }

  if (dstream->fsampl != 0) answerf(" FSAMPL %uHz", dstream->fsampl);
  if (dstream->tsampl != 0.0) answerf(" TSAMPL %lfs", dstream->tsampl);
  if (dstream->nsampl != 0) answerf(" NSAMPL %u", dstream->nsampl);

  if (dstream->dsource_hid) {
    answerf(" SRC");
    args->dsource_hid = dstream->dsource_hid;
    dsource_foreach(answer_dsource_sid, args);
  }

  answerf("\n");

  args->was_found = 1;

  /* stop iteration if one stream enum */
  if (args->stream_id != NULL) return -1;
  return 0;
}

static int answer_dstream_read
(
 dstream_handle_t* dstream,
 unsigned int is_bin,
 size_t nsampl
)
{
  daq_handle_t* const daq = dstream->daq;

  /* in samples */
  size_t wpos;

  /* in bytes */
  size_t roff;
  size_t rsize;

  size_t nsampl_max;
  size_t nsampl_avail;
  size_t wsampl;
  size_t src_count;

  if (daq == NULL) {
    errorf("DAQ not initialized or already in use");
    return -1;
  }

  /* compute sample width, in bytes */

  src_count = count_ones(dstream->dsource_hid, (size_t)daq->src_max);
  wsampl = (size_t)daq->src_width * src_count;

  /* compute sample count */

  wpos = (size_t)daq_read_wpos_val(daq);
  if (daq->rpos >= wpos) {
    /* nothing to read, not an error */
    return 0;
  }

  nsampl_avail = wpos - daq->rpos;

  if ((nsampl == 0) || (nsampl > nsampl_avail)) {
    nsampl = nsampl_avail;
  }

  nsampl_max = ebuf_get_size(&daq->buf) / wsampl;
  if (nsampl > nsampl_max) nsampl = nsampl_max;

  /* compute read offset and size */

  rsize = nsampl * wsampl;
  roff = daq->rpos * wsampl;

  /* read */

  if (edma_read(&daq->dma, roff, &daq->buf, 0, rsize)) {
    errorf("edma read failed");
    return -1;
  }

  /* update rpos */

  daq->rpos += nsampl;

  /* TODO: application specific code */

  if (is_bin) {
    daq->mblk.mbsize = rsize;
    send_binary_buffer(&daq->mblk, 1, rsize, BIN_FLG_NOCHKSUM);
  } else  {
    /* convert data */
    /* fixme: this should be done on a per source basis */
    /* fixme: the formating routine should be application specific */

    int64_t* p = (int64_t*)ebuf_get_data(&daq->buf);
    size_t i;
    for (i = 0; i != nsampl * src_count; ++i, ++p) {
      int64_t x = *p;
      if (x & ((uint64_t)1 << 47)) x |= ~(((uint64_t)1 << 47) - (uint64_t)1);
      answerf("%lf\n", (double)x / (double)256.0);
    }
  }

  return 0;
}

/* TODO: weird to be able to query several optional tags, shouldn't be OR ?*/
/* "o( L o(T#READ uI) o(T#NSAMPL) o(T#STATUS) )" */
void commexec_qDSTREAM(void)
{
  struct answer_dstream_args args;
  dstream_handle_t* dstream;
  unsigned int is_read;
  unsigned int is_nsampl;
  unsigned int is_status;
  size_t nsampl;

  args.chan = get_current_command()->channel;
  args.stream_id = NULL;
  args.was_found = 0;

  is_status = 0;
  is_nsampl = 0;
  is_read = 0;
  nsampl = 0;

  if (I_FMT_NUM()) {
    args.stream_id = I_LABEL();

    if (I_NAMED_TAG("READ")) {
      is_read = 1;
      nsampl  = I_INTEGER();
    }

    if (I_NAMED_TAG("NSAMPL")) {
      is_nsampl = 1;
    }

    if (I_NAMED_TAG("STATUS")) {
      is_status = 1;
    }
  }


  if (is_read || is_nsampl || is_status)
  {
    if (args.stream_id == NULL)
    {
      errorf("no stream specified");
      return ;
    }

    dstream = dstream_get_by_id(args.stream_id, args.chan);
    if (dstream == NULL)
    {
      errorf("stream not found");
      return ;
    }
    LOG_DAQ("found stream \"%s\"\n", args.stream_id);

    if (is_status)
    {
      LOG_DAQ("getting status\n");
      answerf("%s\n", dstream_get_status(dstream));
    }
    else if (is_read)
    {
      LOG_DAQ("reading samples: %d\n", nsampl);
      if (answer_dstream_read(dstream, 0, nsampl))
      {
         errorf("stream read error");
         return ;
      }
    }
    else if (is_nsampl)
    {
      LOG_DAQ("getting NSAMPL\n");
      answerf("%zu\n", dstream_get_nsampl(dstream));
    }
  }
  else
  {
    LOG_DAQ("all streams settings readout\n");

    /* enumerate streams */
    dstream_foreach(answer_dstream, &args);
    if (args.was_found == 0)
    {
      errorf("stream not found");
      return ;
    }
  }
}


/* "L o(T#READ uI)" */
void commexec_qbDSTREAM(void)
{
  channel_t* const chan = get_current_command()->channel;
  const char* const id = I_LABEL();

  dstream_handle_t* dstream;

  size_t nsampl;

  if (id == DEFAULT_LABEL) {
    errorf("invalid stream id");
    return ;
  }

  dstream = dstream_get_by_id(id, chan);
  if (dstream == NULL) {
    errorf("stream not found");
    return ;
  }
  LOG_DAQ("found stream \"%s\"\n", id);

  nsampl = 0;
  if (I_NAMED_TAG("READ")) {
    nsampl = (size_t)I_INTEGER();
    LOG_DAQ("reading samples: %d\n", nsampl);
  }

  if (answer_dstream_read(dstream, 1, nsampl)) {
    if (ERROR_NOTFOUND()) {
      errorf("stream read error");
    }
  }
}


/* "L { o(T#FMT C#DFMT) o(T#FSCAL | T#RESOL F) o(T#GROUP nL ) }" */
void commexec_DSOURCE(void)
{
  const char* const sid = I_LABEL();
  uint32_t flags;
  uint32_t hids;
  double fscal_or_resol;
  dsource_desc_t* d;

  if (sid == DEFAULT_LABEL)
  {
    errorf("invalid DSRCID");
    return ;
  }

  /* init from existing */

  flags = 0;
  hids = 0;
  fscal_or_resol = 0.0;

  d = dsource_get_by_sid(sid);
  if (d != NULL)
  {
    flags = d->flags;
    hids = d->hid;
    fscal_or_resol = d->fscal_or_resol;
    LOG_DAQ("found source \"%s\"\n", sid);
  }

  /* parse */

  if (I_NAMED_TAG("FMT")) {
    LOG_DAQ("setting FMT\n");
    flags &= ~DSOURCE_FLAG_FMT_MASK;
    flags |= 1 << (uint32_t)I_CHOICE();
  }

  if (I_NAMED_TAG("FSCAL")) {
    flags &= ~(DSOURCE_FLAG_FSCAL | DSOURCE_FLAG_RESOL);
    flags |= DSOURCE_FLAG_FSCAL;
    fscal_or_resol = (double)I_FLOAT();
    LOG_DAQ("setting FSCAL: %f\n", fscal_or_resol);
  }

  if (I_NAMED_TAG("RESOL")) {
    flags &= ~(DSOURCE_FLAG_FSCAL | DSOURCE_FLAG_RESOL);
    flags |= DSOURCE_FLAG_RESOL;
    fscal_or_resol = (double)I_FLOAT();
    LOG_DAQ("setting RESOL: %f\n", fscal_or_resol);
  }

  if (I_NAMED_TAG("GROUP")) {
    flags |= DSOURCE_FLAG_GROUP;
    const char *lbl;

    LOG_DAQ("setting GROUP\n");
    while ((lbl = I_LABEL()) != NULL)
    {
      const dsource_desc_t* const dsource = dsource_get_by_sid(lbl);
      if (dsource == NULL) {
        errorf("invalid DSRCID");
        return ;
      }
      LOG_DAQ("adding \"%s\"\n", dsource);
      hids |= dsource->hid;
    }
  }


  if (d == NULL) {
    /* create new dsource */
    LOG_DAQ("create source \"%s\"\n", sid);

    d = dsource_create(sid, flags, hids, fscal_or_resol);
    if (d == NULL) {
      errorf("dsource allocation error");
      return ;
    }
  } else {
    /* existing source, check incompatibilities */
    if (flags & DSOURCE_FLAG_GROUP) {
      /* group indicated but source not a group */
      if ((d->flags & DSOURCE_FLAG_GROUP) == 0) {
         errorf("dsource not a group");
         return ;
      }
    }
  }

  /* update */

  d->flags = flags;
  d->hid = hids;
  d->fscal_or_resol = fscal_or_resol;
}


/* qDSOURCE */

struct answer_dsource_args
{
  unsigned int is_nogroup;
  const char* sid;
  uint32_t hids;
};

static int answer_dgroup(dsource_desc_t* d, void* p)
{
  const struct answer_dsource_args* const args = p;

  if (dsource_is_valid(d) == 0) return 0;
  if (d->flags & DSOURCE_FLAG_GROUP) return 0;
  if ((args->hids & d->hid) == 0) return 0;

  answerf(" %s", d->sid);

  return 0;
}

static int answer_dsource(dsource_desc_t* d, void* p)
{
  struct answer_dsource_args* const args = p;
  const char* s;

  if (dsource_is_valid(d) == 0) return 0;
  if (args->is_nogroup && (d->flags & DSOURCE_FLAG_GROUP)) return 0;
  if ((args->sid != NULL) && (strcmp(args->sid, d->sid))) return 0;

  answerf("%s", d->sid);

  switch (d->flags & DSOURCE_FLAG_FMT_MASK)
  {
    case DSOURCE_FLAG_INT8:   s = "INT8";   break;
    case DSOURCE_FLAG_INT16:  s = "INT16";  break;
    case DSOURCE_FLAG_INT32:  s = "INT32";  break;
    case DSOURCE_FLAG_INT64:  s = "INT64";  break;
    case DSOURCE_FLAG_UINT8:  s = "UINT8";  break;
    case DSOURCE_FLAG_UINT16: s = "UINT16"; break;
    case DSOURCE_FLAG_UINT32: s = "UINT32"; break;
    case DSOURCE_FLAG_UINT64: s = "UINT64"; break;
    case DSOURCE_FLAG_FLOAT:  s = "FLOAT";  break;
    case DSOURCE_FLAG_DOUBLE: s = "DOUBLE"; break;
    default: s = NULL; break ;
  }

  if (s != NULL) answerf(" FMT %s", s);

  if (d->flags & DSOURCE_FLAG_FSCAL) {
    answerf(" FSCAL %lf", d->fscal_or_resol);
  } else if (d->flags & DSOURCE_FLAG_RESOL) {
    answerf(" RESOL %lf", d->fscal_or_resol);
  }

  if (d->flags & DSOURCE_FLAG_GROUP) {
    struct answer_dsource_args new_args;
    new_args.hids = d->hid;
    answerf(" GROUP");
    dsource_foreach(answer_dgroup, &new_args);
  }

  answerf("\n");

  return 0;
}


/* "o( L | T#NOGROUP)" */
void commexec_qDSOURCE(void)
{
  struct answer_dsource_args args;

  args.is_nogroup = 0;
  args.sid = NULL;
  args.hids = 0;

  if (I_FMT_NUM()) {
    if (I_NAMED_TAG("NOGROUP")) {
      args.is_nogroup = 1;
      LOG_DAQ("query NOGROUP\n");
    }

    const char* const l = I_LABEL();
    if (l != NULL) {
      args.sid = l;
      LOG_DAQ("query source: \"%s\"\n", args.sid);
    }
  } else {
    LOG_DAQ("query all sources\n");
  }

  dsource_foreach(answer_dsource, &args);
}

void commexec_STRIG(void)
{
  daq_handle_t* const daq = &g_daq;

  LOG_DAQ("software trig requested\n");

  if (daq->is_valid == 0) {
    errorf("DAQ context invalid");
    return ;
  }

  /* do not lock */
  daq_write_strig(daq);
}

