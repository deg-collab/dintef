#ifndef __TIMER_H_INCLUDED__
#define __TIMER_H_INCLUDED__


#include <stdint.h>
#include <sys/time.h>
#include "lnklist.h"


typedef void (*timerfn_t)(void*);


typedef struct timeritem
{
#define TIMERFLAG_PERIODIC (1 << 0)
#define TIMERFLAG_ONESHOT (1 << 1)
#define TIMERFLAG_DELETED (1 << 2)
  volatile uint32_t flags;

  /* absolute time, in milliseconds */
  unsigned int abs_ms;

  /* time relative to previous timer, in milliseconds */
  unsigned int rel_ms;

  /* user provided execution routine */
  timerfn_t exec_fn;
  void* exec_data;

  /* linked list item */
  lnklist_item_t* list_it;

} timeritem_t;


typedef struct timerlist
{
#define TIMERFLAG_IS_MALLOC (1 << 0)
  uint32_t flags;

  /* next reference time */
  struct timeval newref_tv;

  /* reference time */
  struct timeval ref_tv;

  /* storage for get_next */
  struct timeval stor_tv;

  /* timer item linked list */
  lnklist_t li;

} timerlist_t;


timerlist_t*    timerlist_init(timerlist_t*);
void            timerlist_fini(timerlist_t*);
timeritem_t*    timerlist_add_timer(timerlist_t*, timeritem_t*);
timeritem_t*    timerlist_add(timerlist_t*, unsigned int, timerfn_t, void*, uint32_t);
timeritem_t*    timerlist_add_periodic(timerlist_t*, unsigned int, timerfn_t, void*);
timeritem_t*    timerlist_add_oneshot(timerlist_t*, unsigned int, timerfn_t, void*);
timeritem_t*    timerlist_add_periodic_timer(timerlist_t*, timeritem_t*);
timeritem_t*    timerlist_add_oneshot_timer(timerlist_t*, timeritem_t*);
timeritem_t*    timerlist_restart_oneshot(timerlist_t*, timeritem_t*);
void            timerlist_delete_timer(timeritem_t*);
struct timeval* timerlist_get_next(timerlist_t*);
void            timerlist_exec_next(timerlist_t*);
struct timeval* timerlist_exec_expired(timerlist_t* tl);

#endif /* __TIMER_H_INCLUDED__ */
