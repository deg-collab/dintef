#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED


//#define USE_MYMALLOC

#ifdef LOG_IS_ENABLED

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>

#include "derr.h"

/* maximum log line size */
#define LOG_LINE_SIZE 1024

#define LIBDANCE_LOG_CLASS_LIST \
            LOG_CLASS(TRACE)  \
            LOG_CLASS(INFO)   \
            LOG_CLASS(ERROR)  \
            LOG_CLASS(WARN)   \
            LOG_CLASS(EXTBUS) \
            LOG_CLASS(COMM)   \
            LOG_CLASS(CMD)    \
            LOG_CLASS(CONF)   \
            LOG_CLASS(MALLOC) \
            LOG_CLASS(RASHPA) \
            LOG_CLASS(DAQ)    \
            LOG_CLASS(APP)    \

/* The application can declare its own categories by defining:
#define  LOG_CLASS_LIST     \
            LOG_CLASS(I2C)  \
            LOG_CLASS(FPGA) \
*/

#ifndef  LOG_CLASS_LIST
#  define LOG_CLASS_LIST
#endif

#define LOG_CLASS(__klass) LOG_CLASS_ ## __klass,
enum { LIBDANCE_LOG_CLASS_LIST LOG_CLASS_LIST  LOG_CLASS_MAX};
#undef LOG_CLASS


#define LOG_LEVEL_MAX 31

extern uint32_t log_mask[LOG_CLASS_MAX];

static inline unsigned int log_is_enabled(uint32_t klass, uint32_t level)
{
  return log_mask[klass] & (1 << level);
}

#define LOG_INIT() log_init()
#define LOG_FINI() log_fini()
#define LOG_SET_OUTPUT(__oname) log_set_output(__oname)
#define LOG_ENABLE(__klass, __level) log_enable(LOG_CLASS_ ## __klass, __level)
#define LOG_DISABLE(__klass) log_disable(LOG_CLASS_ ## __klass)

#define IS_LOG(__klass, __level) log_is_enabled(LOG_CLASS_ ## __klass, __level)

#define LOG_PRINTF(__klass, __level, __fmt, ...) \
do { \
 if (IS_LOG(__klass, __level)) \
 { \
   log_write(#__klass, __FILE__, __LINE__, __fmt, ##__VA_ARGS__); \
 } \
} while(0)

#define LOG_TRACE() LOG_PRINTF(TRACE, 0, "\n")
#define LOG_INFO_L(l, __fmt, ...) LOG_PRINTF(INFO, l, __fmt, ##__VA_ARGS__)
#define LOG_INFO(...)  LOG_INFO_L(0, ##__VA_ARGS__)
#define LOG_INFO0(...) LOG_INFO_L(0, ##__VA_ARGS__)
#define LOG_INFO1(...) LOG_INFO_L(1, ##__VA_ARGS__)
#define LOG_INFO2(...) LOG_INFO_L(2, ##__VA_ARGS__)

#define LOG_ERROR(__fmt, ...) \
do { \
 LOG_PRINTF(ERROR, 0, __fmt, ##__VA_ARGS__); \
 DERR_PRINTF(__fmt, ##__VA_ARGS__); \
} while(0)

#define LOG_PERROR()           LOG_PRINTF(ERROR,  0, "%s\n", strerror(errno))
#define LOG_WARN(__fmt, ...)   LOG_PRINTF(WARN,   0, __fmt, ##__VA_ARGS__)
#define LOG_EXTBUS(__fmt, ...) LOG_PRINTF(EXTBUS, 0, __fmt, ##__VA_ARGS__)

#define LOG_COMM_L(l, __fmt, ...)  LOG_PRINTF(COMM, l, __fmt, ##__VA_ARGS__)
#define LOG_COMM(...)  LOG_COMM_L(0, ##__VA_ARGS__)
#define LOG_COMM0(...) LOG_COMM_L(0, ##__VA_ARGS__)
#define LOG_COMM1(...) LOG_COMM_L(1, ##__VA_ARGS__)
#define LOG_COMM2(...) LOG_COMM_L(2, ##__VA_ARGS__)

#define LOG_CMD(__fmt, ...)    LOG_PRINTF(CMD,    0, __fmt, ##__VA_ARGS__)
#define LOG_CONF(__fmt, ...)   LOG_PRINTF(CONF,   0, __fmt, ##__VA_ARGS__)
#define LOG_MALLOC(__fmt, ...) LOG_PRINTF(MALLOC, 0, __fmt, ##__VA_ARGS__)
#define LOG_RASHPA(__fmt, ...) LOG_PRINTF(RASHPA, 0, __fmt, ##__VA_ARGS__)

#define LOG_DAQ_L(l, __fmt, ...)  LOG_PRINTF(DAQ, l, __fmt, ##__VA_ARGS__)
#define LOG_DAQ(...)  LOG_DAQ_L(0, ##__VA_ARGS__)
#define LOG_DAQ0(...) LOG_DAQ_L(0, ##__VA_ARGS__)
#define LOG_DAQ1(...) LOG_DAQ_L(1, ##__VA_ARGS__)
#define LOG_DAQ2(...) LOG_DAQ_L(2, ##__VA_ARGS__)

#define LOG_APP_L(l, __fmt, ...)  LOG_PRINTF(APP, l, __fmt, ##__VA_ARGS__)
#define LOG_APP(...)  LOG_APP_L(0, ##__VA_ARGS__)
#define LOG_APP0(...) LOG_APP_L(0, ##__VA_ARGS__)
#define LOG_APP1(...) LOG_APP_L(1, ##__VA_ARGS__)
#define LOG_APP2(...) LOG_APP_L(2, ##__VA_ARGS__)

int log_init(void);
void log_fini(void);
int log_set_output(const char*);
void log_enable(uint32_t, uint32_t);
void log_enable_all(uint32_t);
void log_disable(uint32_t);
void log_disable_all(void);
int log_write(const char*, const char*, unsigned int, const char*, ...);
int log_read(char**, size_t*);
int log_parse_opt(const char*);

/* use mymalloc if requested by USE_MYMALLOC */
#define MYMALLOC_LOG(file, line_n, __fmt, ...) log_write("MALLOC", file, line_n, __fmt,  ##__VA_ARGS__)
#include "mymalloc.h"

#else /* LOG_IS_ENABLED not defined */

#define LOG_INIT() (0)
#define LOG_FINI()
#define LOG_SET_OUTPUT(__oname) (0)
#define LOG_ENABLE(__klass, __level)
#define LOG_DISABLE(__klass)
#define LOG_PRINTF(__class, __level, __fmt, ...)

#define LOG_TRACE()
#define LOG_INFO(__fmt, ...)
#define LOG_ERROR(__fmt, ...)
#define LOG_PERROR()
#define LOG_WARN(__fmt, ...)
#define LOG_EXTBUS(__fmt, ...)
#define LOG_COMM(__fmt, ...)
#define LOG_CMD(__fmt, ...)
#define LOG_CONF(__fmt, ...)
#define LOG_MALLOC(__fmt, ...)
#define LOG_RASHPA(__fmt, ...)
#define LOG_DAQ(__fmt, ...)
#define LOG_APP(__fmt, ...)

#endif /* LOG_IS_ENABLED */

/* compatibility */
#define DEBUGF_1(...) LOG_PRINTF(APP, 0, ##__VA_ARGS__)
#define DEBUGF_2(...) LOG_PRINTF(APP, 1, ##__VA_ARGS__)
#define DEBUGF_3(...) LOG_PRINTF(APP, 2, ##__VA_ARGS__)
#define DEBUGF_4(...) LOG_PRINTF(APP, 3, ##__VA_ARGS__)
#define DEBUGF_5(...) LOG_PRINTF(APP, 5, ##__VA_ARGS__)


#endif /* LOG_H_INCLUDED */
