#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <fcntl.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include "libdance.h"

#include "i2c.h"


// warning: here we are overloading a flag defined in linux/i2c.h!
#define I2C_M_FORCESTOP I2C_M_NOSTART


#define MAX_TRLEN  256 + 2 // Add 2 for header
#define MIN_NMSGS 4

int extract_i2c_from_device(uint16_t devid, i2c_handle_t** i2c_h, int16_t* addr) {
   danceextdev_t* dev;
//LOG_INFO("devid: %d\n", devid);

   if (!IS_VALID_EXTDEV_ID(devid)) {
      LOG_ERROR("Invalid i2c device id: %d\n", devid);
      return -1;
   }
   dev = DANCE_EXTDEV_PT(devid);
   *i2c_h = &dev->buspt->info.i2chandle;
   *addr = dev->address;
   return 0;
}

//------------
static unsigned int parse_integer(const char** str
                                 , unsigned int defval
                                 , unsigned int maxval) {
   unsigned int value;
   int          nchar;
   const char*  fmt;

   if (**str == '0' && isdigit(*(*str + 1)))
      fmt = "%d%n";
   else
      fmt = "%i%n";

   if (sscanf(*str, fmt, &value, &nchar) < 1)
      value = defval;
   else
      *str += nchar;

   return (value <= maxval)? value : defval;
}

void i2c_empty_transaction(i2c_trdata_t* trdata) {
   if (trdata->internal) {
      if (trdata->msgs) {
         free(trdata->msgs);
         trdata->msgs = NULL;
      }
      trdata->nmaxmsgs = 0;
   }
   trdata->nmsgs = 0;
}


static int add_transaction(i2c_trans_t*  trans, i2c_trdata_t* trdata) {
   uint16_t        do_wr = (trans->trflags & _I2C_WR)? 1 : 0;
   uint16_t        do_rd = (trans->trflags & _I2C_RD)? 1 : 0;
   uint16_t        do_stop = (trans->trflags & _I2C_STOP)? 1 : 0;
   unsigned int    nmsgs = do_wr + do_rd;
   struct i2c_msg* msg;
   uint16_t        flags = (trans->trflags & _I2C_10BIT)? I2C_M_TEN : 0;

   if (trdata->msgs == NULL) {    // if no initial user allocation
      trdata->nmaxmsgs = MIN_NMSGS;
      msg = malloc(trdata->nmaxmsgs * sizeof(struct i2c_msg));
      if (msg == NULL) {
         trdata->nmaxmsgs = 0;
         LOG_ERROR("Error while allocate memory for I2C message\n");
         return -1;
      }
      trdata->msgs = msg;
      trdata->internal = 1;

   } else if (trdata->nmsgs == 0) {  // if allocated but first call
      msg = trdata->msgs;
      trdata->internal = 0;

   } else {                          // if not first call
      if (trdata->nmsgs + nmsgs > trdata->nmaxmsgs) {
         // if no space to hold all messages
         if (trdata->internal) { // if internal, reallocate
            trdata->nmaxmsgs += MIN_NMSGS;
            msg = realloc(trdata->msgs, trdata->nmaxmsgs * sizeof(struct i2c_msg));
            if (msg == NULL) {
               i2c_empty_transaction(trdata);
               LOG_ERROR("Error while allocate memory for I2C message\n");
               return -1;
            }
            trdata->msgs = msg;
         } else {                // if user allocated, error (fixed size)
            LOG_ERROR("I2C transaction size is greater than max message size\n");
            return -1;
         }
      }
      msg = (struct i2c_msg *)trdata->msgs + trdata->nmsgs;
   }

   if (do_wr) {
      msg->addr = trans->addr;
      if (do_stop && !do_rd)
         msg->flags = flags | I2C_M_FORCESTOP;
      else
         msg->flags = flags;
      msg->len = trans->wr_len;
      msg->buf = trans->wr_buf;
      trdata->nmsgs++;
      msg++;
   }

   if (do_rd) {
      msg->addr = trans->addr;
      if (do_stop)
         msg->flags = flags | I2C_M_RD | I2C_M_FORCESTOP;
      else
         msg->flags = flags | I2C_M_RD;

      if (trans->trflags & _I2C_RCNT) {
         msg->flags |= I2C_M_RECV_LEN;
         msg->len = 1;
      } else
         msg->len = trans->rd_len;

      msg->buf = trans->rd_buf;
      trdata->nmsgs++;
   }

   return trdata->nmsgs;
}



/************************************
 *
 * Syntax: " A[T][<a>|n] [W[<s>|n]] [R[<s>|n|?]] [S]"
 *
 * Notes:
 * - The main fields are as follows:
 *     #  A - the i2c address
 *     #  W - the transaction includes a write operation
 *     #  R - the transaction includes a read operation
 *     #  S - finish the transaction with a stop bit.
 *
 * - The '<a>' modifiers indicate an explicit i2c address. If none of <a> or
 *   the 'n' modifier are not present (see below), the default address is used.
 *
 * - The 'T' modifier indicates that the transaction must use 10-bit addressing.
 *   This modifier must follow the 'A' character in the address field.
 *
 * - The '<s>' modifiers indicate the number of bytes to read or write.
 *   They default to 1. A value of zero is possible in certain cases.
 *   For instance 'W3' instructs to write 3 bytes. 'W' and 'W1' are equivalent.
 *
 * - The 'n' modifier may be used instead of the '<a>' or '<s>' fields. In that
 *   case the i2c address or the data lengths are obtained from the 'addr',
 *   'wr_len' or 'rd_len' fields of the corresponding transaction entry.
 *   For instance 'Wn' instructs to write the number of bytes specified in the
 *   'wr_len' field supplied by the calling code.
 *
 * - The '?' modifier in a read field indicates that the number of data bytes in
 *   the read operation, up to 255, will be provided by the slave device as the
 *   first transmitted byte. This additional byte is not included in the total
 *   count.
 *
 * - The 'S' field instructs to finish the i2c transaction with an explicit
 *   stop bit. Therefore any subsequent transaction will be initiated with a
 *   regular start and not with a repeated start.
 *
 */
static int parse_descriptor(i2c_trdata_t*  trdata
                                , const char*  descr      // transaction description
                                , uint16_t     addr       // main slave i2c address
                                , i2c_trans_t* trlist     // list of transactions
                                , uint16_t     ntr)       // number or transactions in trlist
{
   i2c_trans_t* currtrans = NULL; // current transaction
   const char*  ptr = descr;
   char         c;

   trdata->nmsgs = 0;

   // Format: " A[T][<a>|n] [W[<s>|n]] [R[<s>|n|?]] [S]"

   while((c = toupper(*ptr++))) {
      if (isspace(c)) continue;

      if (!currtrans && c != 'A') {
         LOG_ERROR("Missing address field in I2C transaction descriptor\n");
         goto on_error;
      }
      if (c == 'A') {
         int maxaddr;
#define INVALID_I2C_ADDR  0xffff
#define MAX_I2C_10BITADDR 0x03ff
#define MAX_I2C_7BITADDR  0x007f

         if (ntr-- == 0) {
            LOG_ERROR("Missing instruction field in I2C transaction descriptor \'%s\'\n", descr);
            goto on_error;
         } else if (ntr < 0) {
            LOG_ERROR("Too many transactions in I2C descriptor \'%s\'\n", descr);
            goto on_error;
         }

         if (currtrans) {
            if (add_transaction(currtrans++, trdata) < 0) {
               LOG_ERROR("Error while building I2C transaction using multiple address\n");
               goto on_error;
            }
         } else
            currtrans = trlist;

         if (*ptr == 'T') {
            ptr++;
            currtrans->trflags = _I2C_10BIT;
            maxaddr = MAX_I2C_10BITADDR;
         } else {
            currtrans->trflags = 0;
            maxaddr = MAX_I2C_7BITADDR;
         }

         if (*ptr == 'n')
            ptr++;
         else {
            unsigned int impl_addr = parse_integer(&ptr, INVALID_I2C_ADDR, maxaddr);

            if (impl_addr != INVALID_I2C_ADDR)
               currtrans->addr = impl_addr;
            else
               currtrans->addr = addr;
         }

      } else if (c == 'W') {
         if (currtrans->trflags & (_I2C_WR | _I2C_RD | _I2C_STOP)) {
            LOG_ERROR("Syntax error while using write field\n");
            goto on_error;
         } else
            currtrans->trflags |= _I2C_WR;

         if (*ptr == 'n')
            ptr++;
         else
            currtrans->wr_len = parse_integer(&ptr, 1, MAX_TRLEN - 2);

      } else if (c == 'R') {
         if (currtrans->trflags & (_I2C_RD | _I2C_STOP)) {
            LOG_ERROR("Syntax error while using read field\n");
            goto on_error;
         } else
            currtrans->trflags |= _I2C_RD;

         if (*ptr == 'n')
            ptr++;
         else if (*ptr == '?') {
            ptr++;
            currtrans->trflags |= _I2C_RCNT;
         } else
            currtrans->rd_len = parse_integer(&ptr, 1, MAX_TRLEN - 2);

         if (currtrans->rd_len == 0 && (currtrans->trflags & _I2C_WR)) {
            LOG_ERROR("Reading 0 bytes is not compatible with a write operation\n");
            goto on_error;
         }

      } else if (c == 'S') {
         if (currtrans->trflags & _I2C_STOP) {
            LOG_ERROR("Repeated stop field\n");
            goto on_error;
         } else if ((currtrans->trflags & (_I2C_RD | _I2C_WR)) == 0) {
            LOG_ERROR("Stop field must terminate a valid transaction description\n");
            goto on_error;
         } else
            currtrans->trflags |= _I2C_STOP;

      } else {
         LOG_ERROR("Syntax error, unknown value\n");
         goto on_error;
      }
   }

   if (currtrans && currtrans->trflags != 0) {
      if (add_transaction(currtrans, trdata) < 0) {
         LOG_ERROR("Error while building I2C transaction\n");
         goto on_error;
      }
   }

   return 0;

 on_error:
   i2c_empty_transaction(trdata);
   return -1;
}

//------------

static int init_mutex(i2c_master_t* i2c_h) {
   i2c_h->mutex_in_use = 0;
   if (pthread_mutex_init(&i2c_h->mutex, NULL) != 0)
      return -1;
   else
      return 0;
}

int i2c_linux_init(i2c_master_t* i2c_h, const char* dev_id) {
   if (dev_id == NULL || dev_id[0] == 0) {
      return -1;
   }
   i2c_h->ifd = open(dev_id, O_RDWR);
   if (i2c_h->ifd < 0) {
      return -1;
   } else if (init_mutex(i2c_h) < 0) {
      return -1;
   } else {
      i2c_h->dev_file = dev_id;
      i2c_h->eaddr = 0;
      i2c_h->type = I2CDRVR_LINUX;
      return 0;
   }
}

int i2c_ebone_init(i2c_master_t* i2c_h) {
// TODO:  // to be implemented
   if (1) {
      return -1;
   } else if (init_mutex(i2c_h) < 0) {
      return -1;
   } else {
      i2c_h->ifd = -1;
      i2c_h->dev_file = NULL;
      i2c_h->type = I2CDRVR_EBONE;
      return 0;
   }
}

int i2c_fini(i2c_master_t* i2c_h) {
   switch (i2c_h->type) {
      case I2CDRVR_LINUX:
         if (i2c_h->ifd >= 0)
            close(i2c_h->ifd);
         i2c_h->dev_file = NULL;
         i2c_h->ifd = -1;
         break;

      case I2CDRVR_EBONE:
      default:
         return -1;
   }
   //pthread_mutex_destroy(i2c_h->mutex)
   i2c_h->mutex_in_use = 0;
   return 0;
}

int i2c_open( i2c_master_t* i2c_h )
{
   return 0;
}

int i2c_close( i2c_master_t* i2c_h )
{
   return i2c_fini(i2c_h);
}


static void lock_device(i2c_master_t* i2c_h) {
   pthread_mutex_lock(&i2c_h->mutex);
   i2c_h->mutex_in_use = 1;
}

static void unlock_device(i2c_master_t* i2c_h) {
   pthread_mutex_unlock(&i2c_h->mutex);
   // do not clear i2c_h->mutex_in_use, it is sticky!
}


static int do_i2c_transaction(i2c_handle_t* i2c_h, struct i2c_rdwr_ioctl_data* rdwr_data) {
   i2c_master_t* i2cmaster = i2c_h->master;

   switch (i2cmaster->type) {
      case I2CDRVR_LINUX:
         // Send transaction to device
         if (i2cmaster->ifd < 0)
            return -1;
         else if (ioctl(i2cmaster->ifd, I2C_RDWR, rdwr_data) < 0) {
            LOG_ERROR("Error while sending ioctl I2C_RDWR to %s\n", i2cmaster->dev_file);
            return -1;
         } else
            return 0;

      case I2CDRVR_EBONE:
      default:
         LOG_ERROR("Not a valid type of I2C master\n", i2cmaster->dev_file);
         return -1;
   }
}

int i2c_execute_transaction(i2c_handle_t* i2c_h, i2c_trdata_t* trdata) {
   struct i2c_rdwr_ioctl_data rdwr_data;
   int16_t         nmsgs;
   struct i2c_msg* msg = trdata->msgs;
   uint16_t        device_locked = 0;
   i2c_master_t*   i2cmaster = i2c_h->master;

   rdwr_data.nmsgs = 0;
   rdwr_data.msgs = msg;
   for (nmsgs = trdata->nmsgs; nmsgs > 0; nmsgs--) {
      ++rdwr_data.nmsgs;
      if (nmsgs > 1 && msg->flags & I2C_M_FORCESTOP) {
         // here only if not the last message, and a STOP requested...
         msg->flags &= ~I2C_M_FORCESTOP;
         if (!device_locked) {
            lock_device(i2cmaster);
            device_locked = 1;
         }
         if (do_i2c_transaction(i2c_h, &rdwr_data) < 0) {
            unlock_device(i2cmaster);
            return -1;
         }
         msg->flags |= I2C_M_FORCESTOP;  // restore flag
         // and prepare next block
         msg++;
         rdwr_data.nmsgs = 0;
         rdwr_data.msgs = msg;
      } else {
         msg++;
      }
   }
   // and now treat the last transaction with a different locking logic
   if (rdwr_data.nmsgs > 0) {
      // only lock the master if the mutex has been used before
      if (i2cmaster->mutex_in_use && !device_locked) {
         lock_device(i2cmaster);
         device_locked = 1;
      }
      if (do_i2c_transaction(i2c_h, &rdwr_data) < 0) {
         if (device_locked) unlock_device(i2cmaster);
         return -1;
      }
   }
   if (device_locked) unlock_device(i2cmaster);

   return 0;
}


int i2c_prepare_transaction(const char* descr,
                            uint16_t      addr,
                            i2c_trans_t*  trlist,
                            size_t        trcount,
                            i2c_trdata_t* trdata) {

   if (parse_descriptor(trdata, descr, addr, trlist, trcount)) {
      return -1;
   }
   // TODO?  additional verifications?

   return 0;
}

// Notes:
//   - buffer 'buff' is used for both sending and receiving data.
//   - a negative address 'addr' indicates 10-bit addressing.
//   - negative values of nwr or nwr indicate to skip the corresponding
//     part of the transaction.
//   - empty transactions can be issued with nwr=0,nrd=-1 (empty write)
//     or nwr=-1,nrd=0 (empty read).
//
int i2c_basic_transaction(i2c_handle_t* i2c_h, int16_t  addr,
                                               ssize_t  nwr,
                                               ssize_t  nrd,
                                               uint8_t* wrbuff,
                                               uint8_t* rdbuff) {
   struct i2c_msg  msglist[2];
   struct i2c_msg* msg = msglist;
   int             flags;
   int             do_wr = ((nwr > 0) || (nwr == 0 && nrd <= 0));
   int             do_rd = ((nrd > 0) || (nrd == 0 && !do_wr));
   i2c_trdata_t    trdata = {.nmaxmsgs = 2,
                             .msgs = msglist,
                             .nmsgs = do_wr + do_rd};

   if (i2c_h == NULL && extract_i2c_from_device(addr, &i2c_h, &addr) != 0)
     return -1;

   if (addr < 0) {
      addr = -addr;
      flags = I2C_M_TEN;
   } else
      flags = 0;

   if (do_wr) {
      msg->addr = addr;
      msg->flags = flags;
      msg->len = (nwr <= 0)? 0 : nwr;
      msg->buf = wrbuff;
      msg++;
   }
   if (do_rd) {
      msg->addr = addr;
      msg->flags = flags | I2C_M_RD;
      msg->len = (nrd <= 0)? 0 : nrd;
      msg->buf = rdbuff;
   }
   return i2c_execute_transaction(i2c_h, &trdata);
}


int i2c_adv_transaction(i2c_handle_t* i2c_h, const char*  descr,
                                             int16_t      addr,
                                             i2c_trans_t* trlist,
                                             size_t       trcount) {
   int          ret;
   i2c_trdata_t trdata = I2C_TRANSACTION_EMPTY;

   if (i2c_h == NULL && extract_i2c_from_device(addr, &i2c_h, &addr) != 0)
      return -1;

   if (i2c_prepare_transaction(descr, addr, trlist, trcount, &trdata) < 0) {
      return -1;
   }
   ret = i2c_execute_transaction(i2c_h, &trdata);
   i2c_empty_transaction(&trdata);
   return ret;
}

///////////////////////
// SMBus specific stuff

// SMBus PEC related functions

#define POLY    (0x1070U << 3)
static uint8_t crc8(uint16_t data) {
	int i;

	for (i = 0; i < 8; i++) {
		if (data & 0x8000)
			data = data ^ POLY;
		data = data << 1;
	}
	return (uint8_t)(data >> 8);
}

// Incremental CRC8 over count bytes in the array pointed to by p
static uint8_t smbus_pec(uint8_t crc, uint8_t *p, size_t count) {
	int i;

	for (i = 0; i < count; i++)
		crc = crc8((crc ^ p[i]) << 8);
	return crc;
}

// Assume a 7-bit address, which is reasonable for SMBus
static uint8_t smbus_msg_pec(uint8_t pec, struct i2c_msg *msg) {
	// The address will be sent first
	uint8_t addr = (msg->addr << 1) | !!(msg->flags & I2C_M_RD);
	pec = smbus_pec(pec, &addr, 1);

	// The data buffer follows
	return smbus_pec(pec, msg->buf, msg->len);
}

// Return -1 on CRC error
// decreases the message length to hide the CRC byte from the caller
static int smbus_check_pec(uint8_t cpec, struct i2c_msg *msg) {
	uint8_t rpec = msg->buf[--msg->len];
	cpec = smbus_msg_pec(cpec, msg);

	if (rpec != cpec) {
		LOG_INFO("Bad PEC 0x%02x vs. 0x%02x\n", rpec, cpec);
		return -1;
	} else
      return 0;
}


#define SMB_MAX_NCHAR 255

int i2c_smbus_command(i2c_handle_t* i2c_h, int16_t  addr,
                                       uint8_t  flags,
                                       uint8_t  cmd,
                                       uint8_t  nwr,
                                       uint8_t* wrbuff,
                                       uint8_t* nrd,
                                       uint8_t* rdbuff) {
   struct i2c_msg  msglist[2];
   struct i2c_msg* msg = msglist;
   int             trflags;
   int             do_wr = !(flags & SMB_NOWRITE);
   int             do_rd = ((nrd > 0) || !do_wr);
   uint8_t         buffer[SMB_MAX_NCHAR + 3];   // cmd + count + PEC
   uint8_t*        buff = buffer;
   uint8_t         pec_partial = 0;
   i2c_trdata_t    trdata = {.nmaxmsgs = 2,
                             .msgs = msglist,
                             .nmsgs = do_wr + do_rd};

   if (i2c_h == NULL && extract_i2c_from_device(addr, &i2c_h, &addr) != 0)
      return -1;

   if (addr < 0) {
      addr = -addr;
      trflags = I2C_M_TEN;
   } else
      trflags = 0;

   if (do_wr) {
      msg->addr = addr;
      msg->flags = trflags;
      if (flags & SMB_NOCMD) {
         msg->len = nwr;
         msg->buf = wrbuff;
      } else {
         msg->len = nwr + 1;
         msg->buf = buff;
         *buff++ = cmd;

         if (flags & SMB_WRCOUNT) {
            *buff++ = nwr;
            msg->len++;
         }
         if (nwr)
            memcpy(buff, wrbuff, nwr);
      }
      if (flags & SMB_PEC) {
         pec_partial = smbus_msg_pec(0, msg);
			if (!do_rd) { // Write only, add PEC byte
            msg->buf[msg->len] = pec_partial;
            msg->len++;
         }
      }
      msg++;
   }
   if (do_rd) {
      msg->addr = addr;
      msg->flags = trflags | I2C_M_RD;

      if (flags & (SMB_RDCOUNT | SMB_PEC)) {
         msg->buf = buffer;
         if (flags & SMB_RDCOUNT) {
            msg->len = 1;  // block length will be added by driver
            msg->flags |= I2C_M_RECV_LEN;
         } else
            msg->len = *nrd;

         if (flags & SMB_PEC)
            msg->len++;  // request additional byte
      } else {
         msg->len = *nrd;
         msg->buf = rdbuff;
      }
   }

   if (i2c_execute_transaction(i2c_h, &trdata) < 0)
      return -1;

   if (do_rd) {
      if ((flags & SMB_PEC) && smbus_check_pec(pec_partial, msg) < 0)
         return -1;

      if (flags & SMB_RDCOUNT) {
         *nrd = buffer[0];
         memcpy(rdbuff, &buffer[1], *nrd);
      }
   }
   return 0;
}



// -----
// ?I2C command part
static int extractstr(char* dest, const char* src, char delim) {
   char buff[64];
   char cur_char;
   int count = 0;

   while((cur_char = *src++)) {
      if (isspace(cur_char))
            continue;
      if(cur_char == delim) {
         break;
      } else {
         buff[count] = cur_char;
      }
      count++;
   }
   if ( count > 0 ) {
      strncpy(dest, buff, count);
      return count + 1;
   } else {
      return 0;
   }
}


i2c_handle_t* parse_i2cbus_args(const char* devlbl, uint16_t* addr, uint16_t* offset) {
#define DEFAULT_I2C_ADDR 0x00
#define DEFAULT_I2C_OFFSET 0x00
   dancebus_t* extbus = NULL;
   unsigned int ucpt = 0;
   unsigned int sladdr =  DEFAULT_I2C_ADDR;
   unsigned int cur_ofst =  DEFAULT_I2C_OFFSET;
   const char *ptr = devlbl;
   char busname[64];
   int idx = 0;
   int count = 0;
   char tempbuf[16];

   if (ptr != DEFAULT_LABEL) {
      // Bus id
      if (isalpha(*ptr)) {
         idx += extractstr(busname, ptr, ':');
         if( idx > 0) {
            busname[idx-1] = '\0';
            for(ucpt=0; ucpt < idx-1; ucpt++)
               toupper(busname[ucpt]);
         }
         // Get the I2C bus
         for (ucpt = 0; ucpt < gdance_busdescr->n_bus; ucpt++) {
            extbus = DANCE_BUS_PT_INDEX(ucpt);
            if(strcmp(extbus->bname, busname) == 0) {
               break;
            }
         }
      }

      // Slave address
      if(idx < strlen(ptr)) {
         count = extractstr(tempbuf, ptr + idx, ':');
         idx += count;
         if( idx > 0) {
            tempbuf[count-1] = '\0';
            const char* tmpptr = tempbuf;
            sladdr = parse_integer(&tmpptr, 0, 255);
         }
      }

      // offset
      if(offset != NULL && idx < strlen(ptr)) {
         count = extractstr(tempbuf, ptr + idx, ':');
         idx += count;
         if( idx > 0) {
            tempbuf[count-1] = '\0';
            const char* tmpptr = tempbuf;
            cur_ofst = parse_integer(&tmpptr, 0, 255);
         }
      }
   }

   // Get the first I2C bus
   if(extbus == NULL) {
      for (ucpt = 0; ucpt < gdance_busdescr->n_bus; ucpt++) {
         extbus = DANCE_BUS_PT_INDEX(ucpt);
         if(extbus->bustype == _EI2C_BUS) {
            break;
         }
      }
   }

   if(addr != NULL) *addr = sladdr;
   if(offset != NULL) *offset = cur_ofst;
   return extbus? &extbus->info.i2chandle : NULL;
}

static void answerf_i2c_extbus() {
   dancebus_t* extbus;
   unsigned int maxlen = 8;
   unsigned short cpt = 0;

   // calculate max name length
   for (cpt = 0; cpt < gdance_busdescr->n_bus; cpt++) {
      extbus = DANCE_BUS_PT_INDEX(cpt);
      if(extbus->bustype == _EI2C_BUS) {
         int reglen = strlen(extbus->bname);
         if (reglen > maxlen) maxlen = reglen;
      }
   }

   for(cpt = 0; cpt < gdance_busdescr->n_bus; cpt++) {
      extbus = DANCE_BUS_PT_INDEX(cpt);
      if(extbus->bustype == _EI2C_BUS)
         answerf("%*s [%s]\n", maxlen, extbus->bname, extbus->ident);
   }
}

static void answerf_i2c_cmd(i2c_trans_t *trlist, size_t trcount) {
   unsigned int uidx = 0;
   unsigned int uidx_buf = 0;

   for (uidx = 0; uidx < trcount; uidx++) {
      if (trlist[uidx].trflags & _I2C_WR) {
         answerf("%*d - [0x%02x] Writing %d bytes : { ", 4, uidx + 1, trlist[uidx].addr, trlist[uidx].wr_len);
         for (uidx_buf = 0; uidx_buf < trlist[uidx].wr_len; uidx_buf++) {
            answerf("0x%02x ", trlist[uidx].wr_buf[uidx_buf]);
         }
         answerf("}\n");
      }

      if (trlist[uidx].trflags & _I2C_RD) {
         answerf("%*d - [0x%02x] Reading %d bytes : { ", 4, uidx + 1, trlist[uidx].addr, trlist[uidx].rd_len);
         for (uidx_buf = 0; uidx_buf < trlist[uidx].rd_len; uidx_buf++) {
            answerf("0x%02x ", trlist[uidx].rd_buf[uidx_buf]);
         }
         answerf("}");
      }
      answerf("\n");
   }
}

// ?I2C [[[<BUS_ID>:]<addr>] <tr_list_descr> [<byte1> [<byte2> [...]]]]
// "o(L oL onuI256)"
//
void commexec_qI2C(void) {
   int is_args = I_BLOCK();
   const char*  lb_arg1 = I_LABEL();
   const char*  lb_arg2 = I_LABEL();
   int          n_bytes = I_FMT_NUM();
   unsigned int ucpt;
   dancebus_t*  extbus;

   // Return an explicit error (TODO: disable instead I2C commands??)
   for (ucpt = 0; ucpt < gdance_busdescr->n_bus; ucpt++) {
      extbus = DANCE_BUS_PT_INDEX(ucpt);
      if(extbus->bustype == _EI2C_BUS) {
            break;
      }
   }
   if (ucpt == gdance_busdescr->n_bus) {
      errorf("No I2C bus defined");
      return;
   }


   if ( is_args == 0) {
      answerf_i2c_extbus();
   } else {
      const char* dev_id;
      const char* descr;
      i2c_handle_t* i2c_h;
      bool isLbByte = false; // Trick to check if the label 2 is a writing byte
      uint8_t *arg_buff = NULL;
      unsigned int trCount = 0;
      int err = 0;
      uint16_t sl_addr;

      // Checking param in case of 2nd label is writing stream
      if (lb_arg2 == DEFAULT_LABEL
            || strlen(lb_arg2) == 0) {
         dev_id = DEFAULT_LABEL;
         descr = lb_arg1;
      } else {
         if ( isdigit(lb_arg2[0]) ) {
            // the label 2 is the 1st writing byte
            isLbByte = true;
            n_bytes++;
            dev_id = DEFAULT_LABEL;
            descr = lb_arg1;
         } else {
            dev_id = lb_arg1;
            descr  = lb_arg2;
         }
      }

      i2c_h = parse_i2cbus_args(dev_id, &sl_addr, NULL);

      if ( i2c_h != NULL ) {
         // Count the number of transactions
         for (ucpt = 0; ucpt < strlen(descr); ucpt++) {
            if (toupper(descr[ucpt]) == 'A')
               trCount++;
         }

         // Build the writing buffer
         if (n_bytes > 0) {
            if (!(arg_buff = malloc(n_bytes * sizeof(*arg_buff)))) {
               errorf("No enough memory - size: %d", n_bytes);
               return;
            } else {
               ucpt = 0;
               if (isLbByte) {
                  arg_buff[ucpt] = atoi(lb_arg2);
                  ucpt++;
               }
               while(ucpt < n_bytes) {
                  arg_buff[ucpt] = I_INTEGER();
                  ucpt++;
               }
            }
         }

         if (trCount > 0 ) {
            // Create transaction list
            unsigned int ucpt2;
            unsigned int wr_idx = 0;
            uint8_t wr_buffer[trCount][MAX_TRLEN];
            uint8_t rd_buffer[trCount][MAX_TRLEN];
            i2c_trans_t trlist[trCount];
            i2c_trdata_t trdata = I2C_TRANSACTION_EMPTY;

            // Init data
            for(ucpt =  0; ucpt < trCount; ucpt++) {
               memset(wr_buffer[ucpt], 0, MAX_TRLEN);
               memset(rd_buffer[ucpt], 0, MAX_TRLEN);
               trlist[ucpt].rd_len = 0;
               trlist[ucpt].wr_len = 0;
               trlist[ucpt].wr_buf = wr_buffer[ucpt];
               trlist[ucpt].rd_buf = rd_buffer[ucpt];
            }

            err = i2c_prepare_transaction(descr, sl_addr, trlist, trCount, &trdata);

            if (err >= 0) {
               for(ucpt =  0; ucpt < trCount; ucpt++) {
                  // fill write buffer
                  for(ucpt2 = 0; ucpt2 < trlist[ucpt].wr_len; ucpt2++) {
                     if (wr_idx < n_bytes) {
                        wr_buffer[ucpt][ucpt2] = arg_buff[wr_idx];
                        wr_idx++;
                     } else {
                        errorf("Incorrect number of writing data");
                        err = -1;
                        break;
                     }
                  }
                  if(err < 0)
                     break;
               }
            }
            else {
               errorf("Error while parsing description");
            }

            if(err >= 0) {
               if (i2c_execute_transaction(i2c_h, &trdata) >= 0) {
                  answerf_i2c_cmd(trlist, trCount);
               }  else {
                  errorf("Error while sending I2C transaction");
               }
            }

            // Free memory
            i2c_empty_transaction(&trdata);
         } else {
            errorf("Incorrect command description");
         }

         if (arg_buff != NULL)
            free(arg_buff);
      } else {
         errorf("No I2C bus found");
      }
   }
}
