#define _BSD_SOURCE
#define _LARGEFILE64_SOURCE

#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stddef.h>
#include <inttypes.h>
#include <errno.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <linux/fs.h>
#include <linux/limits.h>

#ifndef CONF_UNIT    // CONF_UNIT is set only for unit tests
#   include "timer.h"
#   include "runtime.h"
#   include "comm.h"
#endif

#include "conf.h"
#include "log.h"
#include "disk.h"


#define CONF_DECLARE(_name, _cnt, _type, _dval, _decl, _ptr) \
\
   static void appconf_init_ ## _name(void* valp) {     \
      const _type default_val = _dval; \
      _type *type_pt = valp;           \
      int    cnt = _cnt;               \
      while(cnt--)                     \
         memcpy(&type_pt[cnt], &default_val, sizeof(_type)); \
   } \
\
   _type _decl; \
\
   static appconf_desc_t appconf_desc_ ## _name = { \
      .name = #_name, \
      .size = sizeof(_type), \
      .count = _cnt,  \
      .init_fn = appconf_init_ ## _name, \
      .valp = _ptr,   \
      .storp = NULL,  \
   };


#define CONF_SCALAR(_name, _type, _dval) \
   CONF_DECLARE(_name, 1,    _type, _dval, appconf_ ## _name,      &appconf_ ## _name)
#define CONF_ARRAY(_name, _cnt, _type, _dval) \
   CONF_DECLARE(_name, _cnt, _type, _dval, appconf_ ## _name[_cnt], appconf_ ## _name)
APPCONF_LIST
#undef CONF_SCALAR
#undef CONF_ARRAY

#define CONF_SCALAR(__name, __type, __val)         &appconf_desc_##__name,
#define CONF_ARRAY(__name, __count, __type, __val) &appconf_desc_##__name,
appconf_desc_t* appconf_desc[] = {
  APPCONF_LIST
  NULL
};
#undef CONF_SCALAR
#undef CONF_ARRAY

#ifndef APPCONF_TIMER
#define APPCONF_TIMER 0
#endif
const uint32_t appconf_timer = APPCONF_TIMER;

#ifndef APPCONF_MODE
#define APPCONF_MODE CONF_MODE_ALL_COMMS
#endif
const uint32_t appconf_mode = APPCONF_MODE;

#ifndef APPCONF_RESET
#define APPCONF_RESET (CONF_RESET_IFSIZE | CONF_RESET_IFCOUNT)
#endif
uint32_t appconf_reset = APPCONF_RESET;

#ifndef APPCONF_MEDIA
#define APPCONF_MEDIA CONF_MEDIA_SYS_FLASH
#endif
const uint32_t appconf_media = APPCONF_MEDIA;

#ifndef APPCONF_FILE
#  define APPCONF_FILE "dconfig_%s_%s.cfg"
#  define CONFFILE_USED 0
#else
#  define CONFFILE_USED (1 << 0)
#endif

#ifndef APPCONF_PATH
#  define APPCONF_PATH ""
#  define CONFPATH_USED 0
#else
#  define CONFPATH_USED (1 << 1)
#endif
char g_appconf_path[PATH_MAX] = APPCONF_PATH;

/* types and global variables */

typedef struct conf_header {
   /* stored in big endian format */
#define CONF_HEADER_MAGIC 0xda5cac5e
   uint32_t magic;
   /* total size, header included */
   uint32_t totsize;
   uint32_t vers;
} __attribute__((packed)) conf_header_t;


typedef struct conf_entry {
   /* size is the full entry size, this structure included */
   uint32_t size;
   /* count the item count */
   uint32_t count;
   /* name_data, 0 terminated name followed by data */
   uint8_t  name_data[1];
} __attribute__((packed)) conf_entry_t;


typedef struct conf_handle {
   uint8_t* buf;
   size_t   totsize;
#define CONF_FLAG_IS_OPEN   (1 << 0)
#define CONF_FLAG_IS_VALID  (1 << 1)
#define CONF_FLAG_WAS_RESET (1 << 2)
   uint32_t flags;

   /* media related */

   union {
      struct {
         const char*   path;
      } file;

      struct {
         disk_handle_t disk;
         uint64_t      block_off;
         uint64_t      block_count;
      } sys_flash;

   } media;

   int (*media_open)(struct conf_handle*);
   int (*media_close)(struct conf_handle*);
   int (*media_read)(struct conf_handle*, void*, size_t);
   int (*media_write)(struct conf_handle*, const void*, size_t);

} conf_handle_t;


static conf_handle_t g_conf;


#ifdef CONF_UNIT /* unit testing */

const uint32_t appconf_media = CONF_MEDIA_FILE;

static void init_uint32_0(void* p) {
   *(uint32_t*)p = 0xdeadb00f;
}

static void init_uint16_1(void* p) {
   *(uint16_t*)p = 0xb00b;
}

static void init_uint32_2(void* p) {
   *(uint32_t*)p = 0xdeadb22f;
}

static void init_uint32_3(void* p) {
   *(uint32_t*)p = 0xdeadb33f;
}

static uint32_t d0_val;
static uint16_t d1_val;
static uint32_t d2_val;
static uint32_t d3_val[8];

static appconf_desc_t d0 = {
  .name = "d0",
  .size = sizeof(uint32_t),
  .count = 1,
  .init_fn = init_uint32_0,
  .valp = &d0_val,
};

static appconf_desc_t d1 = {
   .name = "d1",
   .size = sizeof(uint16_t),
   .count = 1,
   .init_fn = init_uint16_1,
   .valp = &d1_val,
};

static appconf_desc_t d2 = {
   .name = "d2",
   .size = sizeof(uint32_t),
   .count = 1,
   .init_fn = init_uint32_2,
   .valp = &d2_val,
};

static appconf_desc_t d3 = {
   .name = "d3",
   .size = sizeof(uint32_t),
   .count = 8,
   .init_fn = init_uint32_3,
   .valp = d3_val,
};

appconf_desc_t* appconf_desc[] = { &d0, &d1, &d2, &d3, NULL };

#endif /* CONF_UNT */


/* system flash media routines */

static int sys_flash_open(conf_handle_t* conf) {
   /* n the requested size, in bytes */

   static const size_t max_size = 4 * 1024 * 1024;

   disk_handle_t* const disk = &conf->media.sys_flash.disk;
   size_t last_i;
   size_t empty_off;
   size_t empty_count;
   size_t area_count;

   if (disk_open_root(disk)) goto on_error_0;

   if (disk->part_count <= 1) goto on_error_1;
   last_i = disk->part_count - 1;

   /* would overflow */
   if (max_size >= ((size_t)-1 / disk->block_size)) goto on_error_1;

   area_count = max_size / disk->block_size;
   if (max_size % disk->block_size) ++area_count;

   empty_off = disk->part_off[last_i] + disk->part_size[last_i];
   empty_count = disk->block_count - empty_off;
   if (area_count > empty_count) goto on_error_1;

   conf->media.sys_flash.block_off = disk->block_count - area_count;
   conf->media.sys_flash.block_count = area_count;

   return 0;

 on_error_1:
   disk_close(disk);
 on_error_0:
   errno = 0;
   return -1;
}

static int sys_flash_close(conf_handle_t* conf) {
   disk_close(&conf->media.sys_flash.disk);
   return 0;
}

static int sys_flash_read(conf_handle_t* conf, void* p, size_t n) {
   /* p the data pointer */
   /* n the data size, in bytes */

   disk_handle_t* const disk = &conf->media.sys_flash.disk;
   size_t rem_size = n % disk->block_size;
   size_t block_count = n / disk->block_size;

   if (block_count) {
      if (disk_read(disk, conf->media.sys_flash.block_off, block_count, p))
      return -1;
   }

   if (rem_size) {
      const size_t off = conf->media.sys_flash.block_off + block_count;
      uint8_t* const pp = (uint8_t*)p + (block_count * disk->block_size);
      uint8_t buf[DISK_BLOCK_SIZE];
      if (disk_read(disk, off, 1, buf)) return -1;
      memcpy(pp, buf, rem_size);
   }
   return 0;
}

static int sys_flash_write(conf_handle_t* conf, const void* p, size_t n) {
   /* p the data pointer */
   /* n the data size, in bytes */

   disk_handle_t* const disk = &conf->media.sys_flash.disk;
   size_t rem_size = n % disk->block_size;
   size_t block_count = n / disk->block_size;

   if (block_count) {
      if (disk_write(disk, conf->media.sys_flash.block_off, block_count, p))
         return -1;
   }

   if (rem_size) {
      const size_t off = conf->media.sys_flash.block_off + block_count;
      const uint8_t* const pp =
      (const uint8_t*)p + (block_count * disk->block_size);
      uint8_t buf[DISK_BLOCK_SIZE];
      memcpy(buf, pp, rem_size);
      if (disk_write(disk, off, 1, pp)) return -1;
   }
   return 0;
}


/* file media routines */

static int file_open(conf_handle_t* conf) {
   const char* const path = conf->media.file.path;
   struct stat st;

   errno = 0;
   if (stat(path, &st)) {
      /* WARNING: do not use PERROR, would overwrite errno */
      return -1;
   }

   return 0;
}

static int file_close(conf_handle_t* conf) {
   return 0;
}

static int file_read(conf_handle_t* conf, void* p, size_t n) {
   /* read write always start at 0 */

   ssize_t res;
   int fd;

   errno = 0;
   fd = open(conf->media.file.path, O_RDONLY);
   if (fd == -1) {
      LOG_ERROR("\n");
      return -1;
   }

   res = read(fd, p, n);

   close(fd);

   if (res != n) {
      LOG_ERROR("\n");
      return -1;
   }

   return 0;
}

static int file_write(conf_handle_t* conf, const void* p, size_t n) {
   int fd;
   ssize_t res;

   fd = open(conf->media.file.path, O_RDWR | O_TRUNC | O_CREAT, 0755);
   if (fd == -1) {
      LOG_ERROR("\n");
      return -1;
   }

   res = write(fd, p, n);
   close(fd);

   if (res != (ssize_t)n) {
      LOG_ERROR("\n");
      return -1;
   }
   return 0;
}


/* endianess */

static inline unsigned int is_little_endian(void) {
   /* TODO: hardcode using macros */
   static volatile const uint32_t i = 1;
   return *(volatile const uint8_t*)&i == 1;
}

static inline uint32_t conv_le_uint32(uint32_t ui) {
   uint32_t x = ui;

   if (is_little_endian() == 0) {
      x  = (ui & 0x000000ff) << 24;
      x |= (ui & 0x0000ff00) <<  8;
      x |= (ui & 0x00ff0000) >>  8;
      x |= (ui & 0xff000000) >> 24;
   }
   return x;
}


static size_t size_add_sat(size_t a, size_t b) {
   if (a > (SIZE_MAX - b)) return SIZE_MAX;
   return a + b;
}

__attribute__((unused))
static uint32_t uint32_add_sat(uint32_t a, uint32_t b) {
   if (a > (UINT32_MAX - b)) return UINT32_MAX;
   return a + b;
}

__attribute__((unused))
static uint32_t uint32_mul_sat(uint32_t a, uint32_t b) {
   uint16_t c;
   uint16_t d;
   uint32_t r;
   uint32_t s;

   if (a > b) return uint32_mul_sat(b, a);
   if (a > UINT16_MAX) return UINT32_MAX;

   c = b >> 16;
   d = UINT16_MAX & b;
   r = a * c;
   s = a * d;

   if (r > UINT16_MAX) return UINT32_MAX;
   r <<= 16;

   return uint32_add_sat(s, r);
}

static size_t entry_size_from_fields(size_t name_len, size_t item_size, size_t item_count) {
   static const size_t off = offsetof(conf_entry_t, name_data);
   return off + name_len + 1 + item_size * item_count;
}

static size_t entry_size_from_desc(const appconf_desc_t* d) {
   return entry_size_from_fields(strlen(d->name), d->size, d->count);
}

static int get_entry_info(const conf_entry_t* e, size_t max_size,
                          size_t* entry_size,
                          const char** name, size_t* name_len,
                          const uint8_t** data, size_t* item_size, size_t* item_count) {
   /* retrieve entry entry information from storage. */
   /* be careful to check all the sizes */

   /* max_size is the maximum entry size */
   /* entry_size the total entry size */
   /* name the entry name */
   /* data the data */
   /* item_size is the size of 1 individual item */
   /* item_count is the item count */

   size_t data_off;

   if (max_size < offsetof(conf_entry_t, name_data)) return -1;

   max_size -= offsetof(conf_entry_t, name_data);

   /* compute name length, excluding terminating 0 */
   *name = (const char*)e->name_data;
   for (*name_len = 0; e->name_data[*name_len]; ++*name_len)
      if (*name_len == max_size) return -1;
   max_size -= *name_len;

   /* terminating 0 */
   if (max_size == 0) return -1;
   max_size -= 1;

   /* get and check entry_size */
   *entry_size = (size_t)conv_le_uint32(e->size);
   data_off = offsetof(conf_entry_t, name_data) + *name_len + 1;
   if (data_off > *entry_size) return -1;

   /* item[size * count] */
   *data = (uint8_t*)e + data_off;
   *item_count = (size_t)conv_le_uint32(e->count);
   if (*item_count == 0) return -1;
   *item_size = (*entry_size - data_off) / *item_count;

   /* check the size */
   if (size_add_sat(data_off, *item_count * *item_size) != *entry_size)
      return -1;
   if (*entry_size > (max_size + data_off)) return -1;

   return 0;
}

static int init_common(conf_handle_t* conf) {
   conf_header_t header;
   size_t        i;
   size_t        j;
   size_t        conf_pos;
   uint8_t*      load_buf = NULL;
   size_t        load_size = 0;
   size_t        load_pos = 0;
   int           err = -1;

   conf->buf = NULL;
   conf->totsize = 0;
   conf->flags = 0;

   if (conf->media_open(conf)) {
      if (errno == ENOENT) goto skip_read;
      LOG_ERROR("\n");
      goto on_error_0;
   }

   conf->flags |= CONF_FLAG_IS_OPEN;

   if (conf->media_read(conf, &header, sizeof(header))) {
      LOG_ERROR("\n");
      goto on_error_1;
   }

   if (conv_le_uint32(header.magic) != CONF_HEADER_MAGIC)
      goto skip_read; /* conf did not previously exist */
   if (appconf_reset & CONF_RESET_ALWAYS)
      goto skip_read; /* conf reset explicitly requested */

   /* read/load stored data into (load_buf, load_size, load_pos) buffer */
   load_size = conv_le_uint32(header.totsize);
   if (load_size < sizeof(header)) {
      LOG_ERROR("corrupted config\n");
      goto on_error_1;
   }

   load_buf = malloc(load_size);
   if (load_buf == NULL) {
      LOG_ERROR("memory error\n");
      goto on_error_1;
   }

   if (conf->media_read(conf, load_buf, load_size)) {
      LOG_ERROR("\n");
      goto on_error_2;
   }
   load_pos = sizeof(conf_header_t);

 skip_read:

   /* compute total appconf totsize and allocate conf buffer */
   /* uses this loop to initialize desc->storp to NULL */

   conf->totsize = sizeof(conf_header_t);
   for (i = 0; appconf_desc[i] != NULL; ++i) {
      appconf_desc_t* const desc = appconf_desc[i];
      desc->storp = NULL;
      conf->totsize += entry_size_from_desc(desc);
   }

   conf->buf = malloc(conf->totsize);
   if (conf->buf == NULL) {
      LOG_ERROR("\n");
      goto on_error_2;
   }

   /* prepare conf buffer header */

   ((conf_header_t*)conf->buf)->magic = conv_le_uint32(CONF_HEADER_MAGIC);
   ((conf_header_t*)conf->buf)->vers = conv_le_uint32(0);
   ((conf_header_t*)conf->buf)->totsize = conv_le_uint32(conf->totsize);
   conf_pos = sizeof(conf_header_t);

   /* load the currently stored buffer (load_buf, load_pos) */
   /* for each entry, find the corresponding appconf_desc */
   /* if there is no appconf_desc, skip the entry */
   /* otherwise initialize the desc with stored value */
   /* a new buffer (conf->buf, conf_pos) is created along the way */

   while (load_pos < load_size) {
      const conf_entry_t* const load_entry = (const conf_entry_t*)(load_buf + load_pos);
      conf_entry_t*   config_entry;
      appconf_desc_t* desc;
      const char*     name;
      size_t          name_len;
      size_t          conf_esize;
      size_t          load_esize;
      const uint8_t*  data;
      size_t          item_size;
      size_t          item_count;
      size_t          min_size;
      size_t          min_count;
      uint8_t*        storp;
      uint8_t*        valp;
      const uint8_t*  itemp;

      /* get_entry_info and check if valid */
      if (get_entry_info(load_entry, load_size - load_pos, &load_esize,
                                                  &name, &name_len,
                                                  &data, &item_size, &item_count)) {
         LOG_ERROR("invalid entry\n");
         goto on_error_3;
      }

      /* look for appconf entry */
      for (i = 0; (desc = appconf_desc[i]) != NULL; ++i) {
         if (strcmp(desc->name, name) == 0) break ;
      }


      if ((desc == NULL) ||    /* entry has been deleted from appconf */
          ((desc->size != item_size) && (appconf_reset & CONF_RESET_IFSIZE)) ||
          ((desc->count != item_count) && (appconf_reset & CONF_RESET_IFCOUNT)))
          goto skip_entry;

       desc = appconf_desc[i];

      /* create the new entry at conf->buf + conf_pos */

      conf_esize = entry_size_from_fields(name_len, desc->size, desc->count);
      config_entry = (conf_entry_t*)(conf->buf + conf_pos);
      config_entry->size = conv_le_uint32(conf_esize);
      config_entry->count = conv_le_uint32(desc->count);
      strcpy((char*)config_entry->name_data, desc->name);
      desc->storp = config_entry->name_data + name_len + 1;

      /* first, initialize all the items. */
      /* it ensures default valid values, no matter size or count changes. */
      desc->init_fn((void*)desc->valp);
      desc->init_fn((void*)desc->storp);

      /* then, copy stored item into new buffer */
      /* take the minimum count and size */

      min_count = desc->count < item_count ? desc->count : item_count;
      min_size = desc->size < item_size ? desc->size : item_size;

      itemp = data;
      valp = desc->valp;
      storp = desc->storp;

      for (i = 0; i != min_count; ++i) {
         memcpy(storp, itemp, min_size);
         memcpy(valp, itemp, min_size);
         valp += desc->size;
         storp += desc->size;
         itemp += item_size;
      }

      conf_pos += conf_esize;

      /* next entry */
 skip_entry:
      load_pos += load_esize;
   }

   if (load_pos != load_size) {
      LOG_ERROR("\n");
      goto on_error_3;
   }

   /* add any new value (ie. storp == null) */

   for (i = 0; appconf_desc[i] != NULL; ++i) {
      appconf_desc_t* const desc = appconf_desc[i];
      conf_entry_t* const entry = (conf_entry_t*)(conf->buf + conf_pos);
      size_t entry_size;
      uint8_t* valp;
      uint8_t* storp;

      if (desc->storp != NULL) continue ;

//LOG_INFO("%s\n", desc->name);
      for (j = 0; desc->name[j]; ++j)
         entry->name_data[j] = desc->name[j];

      entry->name_data[j] = 0;
      entry_size = entry_size_from_fields(j, desc->size, desc->count);

      desc->storp = entry->name_data + j + 1;

      valp = desc->valp;
      storp = desc->storp;
//LOG_INFO("%d, %p, %p\n", desc->count, valp, storp);
      desc->init_fn((void*)valp);
      desc->init_fn((void*)storp);

      entry->size = conv_le_uint32((uint32_t)entry_size);
      entry->count = conv_le_uint32(desc->count);
      conf_pos += entry_size;
   }

   if (conf_pos != conf->totsize) {
      LOG_ERROR("\n");
      goto on_error_3;
   }

   /* store the configuration buffer */

   if (conf->media_write(conf, conf->buf, conf->totsize)) {
      LOG_ERROR("\n");
      goto on_error_3;
   }

   conf->flags |= CONF_FLAG_IS_VALID;

   err = 0;
 on_error_3:
   if (err && (conf->buf != NULL)) {
      free(conf->buf);
      conf->buf = NULL;
   }
 on_error_2:
   if (load_buf != NULL) free(load_buf);
   if (err == 0) return 0;
 on_error_1:
   /* on error, do not close the conf for conf_erase to work */
   /* conf->media_close(conf); */
 on_error_0:
   return -1;
}

__attribute__((unused))
static int init_file(const char* path) {
   conf_handle_t* const conf = &g_conf;
   conf->media.file.path = path;
   conf->media_open = file_open;
   conf->media_close = file_close;
   conf->media_read = file_read;
   conf->media_write = file_write;

   return init_common(conf);
}


static int init_sys_flash(void) {
   conf_handle_t* const conf = &g_conf;

   conf->media_open = sys_flash_open;
   conf->media_close = sys_flash_close;
   conf->media_read = sys_flash_read;
   conf->media_write = sys_flash_write;
LOG_TRACE();
   return init_common(conf);
}


static int do_mkdir(const char *path, mode_t mode)
{
   struct stat st;
   int status = 0;

   if (stat(path, &st) != 0) {
        /* Directory does not exist. EEXIST for race condition */
      if (errno != ENOENT
            || (mkdir(path, mode) && errno != EEXIST)) {
         LOG_ERROR("Creating folder %s failed\n", path);
         status = -1;
      }
   }
   else if (!S_ISDIR(st.st_mode)) {
      LOG_ERROR("%s: %s", path, strerror(ENOTDIR));
      errno = ENOTDIR;
      status = -1;
   }
   return status;
}

int mkpath(char *path, mode_t mode) {
   char *slash;
   int done = 0;

   slash = path;

   while (!done) {
      slash += strspn(slash, "/");
      slash += strcspn(slash, "/");

      done = (*slash == '\0');
      *slash = '\0';

      if (do_mkdir(path, mode) < 0)
         return -1;

      *slash = '/';
   }

   return 0;
}

int conf_init(void) {
   int      err;
   uint16_t fileout_flag = CONFPATH_USED | CONFFILE_USED;
   if (appconf_media & CONF_MEDIA_FILE) {
      char*       confpath = g_appconf_path;
      struct stat statdir;
      int         isfile = 0;

      if (strlen(g_appconf_path) > 0) {
         char * ext = strrchr(g_appconf_path, '.');
         err = stat(g_appconf_path, &statdir);

         if (S_ISREG(statdir.st_mode) == 1) {
            isfile = 1;
         } else {
            if (ext) {
               ext++;
               if (*ext == '/' || *ext == '\\' ) {
                  // no file extension
                  isfile = 0;
               } else {
                  // Assume that user give a file
                  isfile = 1;
               }
            } else {
               isfile = 0;
            }
         }
      }

      if (((fileout_flag & CONFPATH_USED)
         || (strlen(g_appconf_path) > 0))
         && (isfile == 0)) {
         confpath += strlen(g_appconf_path) - 1;
         if (*confpath++ != '/') {
            *confpath++ = '/';
         }
      } else if (strlen(g_appconf_path) <= 0){
         // Use path of the application in case there is no parameters
         char abs_exe_path[PATH_MAX];

         getcwd(abs_exe_path, sizeof(abs_exe_path));
         memcpy(confpath, abs_exe_path, strlen(abs_exe_path));
         confpath += strlen(abs_exe_path);
         *confpath++ = '/';
         memcpy(confpath, APPCONF_PATH, strlen(APPCONF_PATH));
         confpath += strlen(APPCONF_PATH);
      }

      if (appconf_media & CONF_MEDIA_MKDIRTREE) {
         if (mkpath(g_appconf_path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) < 0)
            return -1;
      }

      err = stat(g_appconf_path, &statdir);
      if (!err) {
         char filename[NAME_MAX];

         if (isfile == 0) {
            snprintf(filename, sizeof(filename), APPCONF_FILE, danceapp_name, g_libdance.addr);
            memcpy(confpath, filename, strlen(filename));
         }
      }
      err = init_file(g_appconf_path);

      if (!err)
         LOG_INFO("Using config file: %s\n", g_appconf_path);
      else
         LOG_ERROR("Cannot access config file at: %s\n", g_appconf_path);

   } else {
      err = init_sys_flash();
   }
   return err? -1 : 0;
}

int conf_fini(void) {
   conf_handle_t* const conf = &g_conf;

   if (conf->buf != NULL) {
      free(conf->buf);
      conf->buf = NULL;
   }

   if (conf->flags & CONF_FLAG_IS_OPEN) {
      conf->media_close(conf);
   }

   conf->flags = 0;

   return 0;
}

int conf_commit(void) {
   conf_handle_t* const conf = &g_conf;
   unsigned int is_dirty = 0;
   size_t i;

   if ((conf->flags & CONF_FLAG_IS_VALID) == 0) {
      LOG_ERROR("conf not valid\n");
      return -1;
   }

   for (i = 0; appconf_desc[i] != NULL; ++i) {
      const appconf_desc_t* const d = appconf_desc[i];
      const size_t size = d->size * d->count;

      /* compare value with shadow copy */
      if (memcmp(d->storp, d->valp, size))
      {
         is_dirty = 1;
         LOG_CONF("value %s changed\n", d->name);
         memcpy(d->storp, d->valp, size);
      }
   }

   if (is_dirty) {
      LOG_CONF("conf dirty\n");

      if (conf->media_write(conf, conf->buf, conf->totsize)) {
         LOG_ERROR("\n");
         return -1;
      }
   }

   return 0;
}

int conf_revert(void) {
   /* revert the working value to what is stored */

   conf_handle_t* const conf = &g_conf;
   size_t i;

   if ((conf->flags & CONF_FLAG_IS_VALID) == 0) {
      LOG_ERROR("conf not valid\n");
      return -1;
   }

   for (i = 0; appconf_desc[i] != NULL; ++i) {
      const appconf_desc_t* const d = appconf_desc[i];
      const size_t size = d->size * d->count;
      memcpy(d->valp, d->storp, size);
   }

   return 0;
}

int conf_erase(void) {
   conf_handle_t* const conf = &g_conf;
   conf_header_t h;

   if ((conf->flags & CONF_FLAG_IS_OPEN) == 0) {
      LOG_ERROR("conf not opened\n");
      return -1;
   }

   conf->flags &= ~CONF_FLAG_IS_VALID;

   h.magic = conv_le_uint32(CONF_HEADER_MAGIC);
   h.vers = conv_le_uint32(0);
   h.totsize = conv_le_uint32((uint32_t)sizeof(h));

   if (conf->media_write(conf, (const void*)&h, sizeof(conf_header_t))) {
      LOG_ERROR("conf->media_write()\n");
      return -1;
   }

   conf->flags |= CONF_FLAG_IS_VALID;

   return 0;
}

#ifndef CONF_UNIT

static timeritem_t* conf_timerit = NULL;

static void on_timer(void* p) {
   LOG_CONF("%s\n", __FUNCTION__);

   conf_commit();

   conf_timerit = NULL;
}


int conf_restart_timer(unsigned int ms) {
   /* create or update a oneshot timer */

   timeritem_t* new_timerit;

   LOG_CONF("%s\n", __FUNCTION__);

   if (conf_timerit == NULL) {
      conf_timerit = add_oneshot_task(ms, on_timer, NULL);
      new_timerit = conf_timerit;
   } else {
      new_timerit = restart_oneshot_task(conf_timerit);
      if (new_timerit != NULL) conf_timerit = new_timerit;
   }

   if (new_timerit == NULL) {
      LOG_ERROR("timerlist_add_restart_oneshot\n");
      return -1;
   }

   return 0;
}


int appconf_commit(void) {
   command_t* currcmd = get_current_command();

   LOG_CONF("%s\n", __FUNCTION__);
   if (currcmd == NULL)  // immediate commit
      conf_commit();
   else  // delayed commit
      currcmd->flags |= CMDFLG_COMMITCONF;
   return 0;
}

int appconf_erase(void) {
   /* this function is executed to reset the configuration */
   /* store if the loading fail (corrupted data ...). it can */
   /* be executed here (as opposed to appconf_commit), given */
   /* it is a special administrative only operation */
   LOG_CONF("%s\n", __FUNCTION__);
   return conf_erase();
}

int appconf_revert(void) {
   LOG_CONF("%s\n", __FUNCTION__);
   return conf_revert();
}

#endif /* CONF_UNIT */


#ifdef CONF_UNIT /* unit */

int main(int ac, char** av) {
   size_t i;
   size_t j;
   size_t k;

   LOG_INIT();
   LOG_SET_OUTPUT("stdout");
   log_enable_all(0);

   if (conf_init()) {
      LOG_ERROR("\n");
      return -1;
   }

   printf("reviewing the configuration\n");
   for (i = 0; appconf_desc[i] != NULL; ++i) {
      appconf_desc_t* const d = appconf_desc[i];

      printf("%s.valp\n", d->name);
      for (j = 0; j != d->count; ++j) {
         printf("0x%02x:", (uint8_t)j);
         for (k = 0; k != d->size; ++k)
            printf(" %02x", ((uint8_t*)d->valp)[j * d->size + k]);
         printf("\n");
      }

      printf("%s.storp\n", d->name);
      for (j = 0; j != d->count; ++j) {
         printf("0x%02x:", (uint8_t)j);
         for (k = 0; k != d->size; ++k)
            printf(" %02x", ((uint8_t*)d->storp)[j * d->size + k]);
         printf("\n");
      }
      printf("\n");
   }

   d0_val += 1;
   d3_val[3] += 1;

   conf_commit();

   conf_fini();

   return 0;
}

#endif /* unit */
