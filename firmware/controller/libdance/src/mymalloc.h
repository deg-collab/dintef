/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdance/trunk/src/mymalloc.h $
 * $Rev: 1855 $
 * $Date: 2018-07-31 20:06:12 +0200 (Tue, 31 Jul 2018) $
 *------------------------------------------------------------------------- */

#ifndef __MYMALLOC_H_INCLUDED__
#define __MYMALLOC_H_INCLUDED__

#ifdef USE_MYMALLOC

#include <stdlib.h>

#include "errors.h"

#ifndef MYMALLOC_LOG
#  define MYMALLOC_LOG(file, line, fmt, ...) printf("[MYMALLOC] @%s:%u: " fmt, file, line, ##__VA_ARGS__)
#endif

static inline void* my_malloc(const char* file, unsigned int line_n, size_t size) {
   void* newptr = malloc(size);
   MYMALLOC_LOG(file, line_n, "malloc(%zd) = %p\n", size, newptr);
   return newptr;
}

static inline void* my_calloc(const char* file, unsigned int line_n, size_t n_items, size_t size) {
   void* newptr = calloc(n_items, size);
   MYMALLOC_LOG(file, line_n, "calloc(%zd, %zd) = %p\n", n_items, size, newptr);
   return newptr;
}

static inline void* my_realloc(const char* file, unsigned int line_n, void* ptr, size_t size) {
   void* newptr = realloc(ptr, size);
   MYMALLOC_LOG(file, line_n, "realloc(%p, %zd) = %p\n", ptr, size, newptr);
   return newptr;
}

static inline void my_free(const char* file, unsigned int line_n, void* ptr) {
   MYMALLOC_LOG(file, line_n, "free(%p)\n", ptr);
   free(ptr);
}

#define malloc(size)           my_malloc(__FILE__, __LINE__, size)
#define calloc(n_items, size)  my_calloc(__FILE__, __LINE__, n_items, size)
#define realloc(ptr, size)     my_realloc(__FILE__, __LINE__, ptr, size)
#define free(ptr)              my_free(__FILE__, __LINE__, ptr)

#endif  // USE_MYMALLOC

#endif   // __MYMALLOC_H_INCLUDED__
