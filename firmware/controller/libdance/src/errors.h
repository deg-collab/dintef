/***************************************************************************
 *  File:  errors.h
 *
 ***************************************************************************/

#ifndef _ERRORS_H_INCLUDED_
#define _ERRORS_H_INCLUDED_

#include <stdio.h>

typedef const char *errmsg_t;


//********************************************************************

#define NOERROR             NULL
#define IS_ERROR(err)       ((err) != NOERROR)


#define NOERRORMESSAGE      "No Error"
#define SYSERRORMESSAGE     "System Error"
#define TEMPERRMESSAGE      "/Error/Error/Error/Error/"
#define BADSYNTAXMESSAGE    "Wrong command syntax"
#define INVATOMICMESSAGE    "Invalid atomic command"
#define MISPARMESSAGE       "Wrong number or bad parameter(s)"
#define TOOMANYPARMESSAGE   "Unexpected parameters"
#define INCOMPATPARMESSAGE  "Incompatible parameters"
#define BADPARMESSAGE       "Wrong parameter(s)"
#define BADRANGEMESSAGE     "Out of range parameter(s)"
#define BADADDRMESSAGE      "Bad board address"
#define NOTPRESENTMESSAGE   "Board is not present in the system"
#define BADCOMMMESSAGE      "Command not recognised"
#define NOTIMPLMESSAGE      "Command not implemented"
#define NOTBINARYMESSAGE    "Missing binary answer"
#define PARNOTIMPLMESSAGE   "Feature not implemented yet"
#define DABFMTMESSAGE       "Bad binary data format"
#define BIGDATAMESSAGE      "Too many binary data"
#define BADSIGMESSAGE       "Wrong binary signature"
#define CANQUERYTOUTMESSAGE "Internal answer time out"
#define BINABORTEDMESSAGE   "Binary commmunication aborted"
#define BADQBDCASTMESSAGE   "Cannot broadcast a query"
#define BADABDCASTMESSAGE   "Cannot acknowledge a broadcast"
#define BADIOFORMATMESSAGE  "Bad command I/O format string"
#define BADABDCASTMESSAGE   "Cannot acknowledge a broadcast"
#define NOTIDLEMESSAGE      "System is not idle"

#define MISSINGSTRMESSAGE   "Missing string or label"
#define TOOLONGSTRMESSAGE   "Too long string or label"
#define NOMEMORYMESSAGE     "Memory allocation error"

//********************************************************************

/*
errmsg_t set_current_error(errmsg_t errmsg);
void     flag_check_error(void);
void     reset_flag_check_error(void);
void     save_current_error(const menuitem_t *cmd);
void     clear_first_error(void);
void     reset_current_error(void);
*/

typedef enum
{
  DANCE_OK = 0,
  DANCE_ERROR,
//  RASHPA_ERR_NOT_FOUND,
//  RASHPA_ERR_BREAK,
} danceerr_t;

// defined in errors.c
int   set_libdanceerrorf(const char *fmt, ...);
char* libdanceerror(void);

#endif // _ERRORS_H_INCLUDED_
