#ifndef DERR_H_INCLUDED
#define DERR_H_INCLUDED


#include <stdint.h>


/* api */

int  derr_init(void);
void derr_fini(void);
int  derr_prints(const char*, uint32_t, const char*);
int  derr_printf(const char*, uint32_t, const char*, ...);

#define DERR_PRINTF(__fmt, ...) derr_printf(__FILE__, __LINE__, __fmt, ## __VA_ARGS__)
#define DERR_PRINTS(__s)        derr_prints(__FILE__, __LINE__, __s)
#define DERR_TRACE()            derr_prints(__FILE__, __LINE__, NULL)

#define SYSTEM_ERRORF(__fmt, ...) DERR_PRINTF(__fmt, ## __VA_ARGS__)
#define SYSTEM_ERROR()            DERR_PRINTS("internal system error")


/* commands */

#define DERRFLAG_ALL       (1<<0)
#define DERRFLAG_FIRST     (1<<1)
#define DERRFLAG_LAST      (1<<2)
#define DERRFLAGS_QH       (DERRFLAG_ALL | DERRFLAG_FIRST | DERRFLAG_LAST)

#define DERRFLAG_SYS       (1<<3)
#define DERRFLAG_APP       (1<<4)
#define DERRFLAG_NOCLEAR   (1<<5)
#define DERRFLAG_ANY       (1<<6)
#define DERRFLAG_NTOT      (1<<7)
#define DERRFLAG_NBUFF     (1<<8)
#define DERRFLAGS_QP       (DERRFLAG_NTOT | DERRFLAG_NBUFF | DERRFLAG_ANY)

#define DERRFLAG_LIST { \
   "ALL", "FIRST", "LAST", \
   "SYS", "APP", \
   "NOCLEAR", \
   "ANY", "NTOT", "NBUFF", \
}


#define DERR_MENU_LIST \
   MENU_IT(DERR,  "ouI",         fmtNONE,     _E, "simulate errors") \
   MENU_IT(qDERR, "oG#DERRFLAG", fmtSTR, _I | _B, "get instrument errors")	

#define DERR_LISTS  \
   DANCELIST(DERRFLAG) 



#endif /* DERR_H_INCLUDED */
