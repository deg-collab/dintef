#ifndef I2C_DEPRECATED_H_INCLUDED
#define I2C_DEPRECATED_H_INCLUDED


#include <stdint.h>
#include "i2c.h"

#define DEFAULT_I2C_BUS "/dev/i2c-2"
// I2C SMBUS Command code for TI TCA9554 component
#define I2C_CMD_IN_RD             0x00
#define I2C_CMD_OUT_RDWR          0x01
#define I2C_CMD_POLARITYINV_RDWR  0x02
#define I2C_CMD_CONF_RDWR         0x03

// Default commands
#define I2C_CMD_UNUSED            0x00

// EEPROM specifications
#define EEPROM_BYTES_PER_PAGE 256
#define EEPROM_WRITE_WAIT_MS  10 // Need 10ms for writing

typedef struct
{
  const char* master_path;
  uint32_t slave_addr;
} i2c_desc_t;

/*
typedef struct
{
  int fd;
} i2c_master_t;
*/

void i2c_init_desc(i2c_desc_t*);
void i2c_init_local_desc(i2c_desc_t*, uint32_t);
int i2c_open_desc(i2c_master_t*, const i2c_desc_t*);
void i2c_close_desc(i2c_master_t*);
int i2c_switch_slave(i2c_master_t*, i2c_desc_t*, uint32_t);

int i2c_write_uint8(i2c_master_t*, uint8_t, uint8_t);
int i2c_read_uint8(i2c_master_t*, uint8_t, uint8_t*);
int i2c_eprom_write( i2c_master_t*
                , unsigned int
                , const char*
                , unsigned int);
int i2c_eeprom_read( i2c_master_t*
                , unsigned int
                , char *
                , unsigned int);


#endif /* I2C_DEPRECATED_H_INCLUDED */