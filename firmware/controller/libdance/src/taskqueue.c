#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include "taskqueue.h"
#include "log.h"


// task handling thread

static void* tqueue_thread_entry(void* p)
{
   tqueue_handle_t* const tq = p;
   task_handle_t*         task;

   while (1)
   {
      // wait for new events, and execute

      pthread_mutex_lock(&tq->task_mutex);

      // until done or task to execute

      while (1)
      {
         if (tq->tqflags & TQUEUE_DONE)
         {
            pthread_mutex_unlock(&tq->task_mutex);
            goto on_done;
         }

         if ((void* volatile)tq->ready_list.head != NULL)
         {
            task = tq->ready_list.head->itmdata;
            lnklist_free_item(tq->ready_list.head);
            break ;
         }

         pthread_cond_wait(&tq->task_cond, &tq->task_mutex);
      }

      pthread_mutex_unlock(&tq->task_mutex);

      // execute and signal task completion
      // sticky tasks never complete
      // sticky tasks are not freed

      task->fn(task->data);

      if ((task->tflags & TASK_STICKY) == 0)
      {
         if ((tq->tqflags & TQUEUE_COMPL_FD) && !(task->tflags & TASK_SILENT))
         {
            LOG_INFO("Sending runtime signal %d (%p) [%d]\n", tq->compl_signal, task->data, task->tflags);
            runtime_send_signal(tq->compl_signal, task->data);
         }
         if (tq->tqflags & TQUEUE_COMPL_COND)
         {
            pthread_mutex_lock(tq->compl_mutex);
            pthread_cond_signal(tq->compl_cond);
            pthread_mutex_unlock(tq->compl_mutex);
         }
         free(task);
      }
   }

 on_done:
   return NULL;
}


// exported

static lnklist_for_t fini_sticky_item(lnklist_item_t* item, void* p)
{
   free(item->itmdata);
   return LNKLIST_CONT;
}

static lnklist_for_t fini_ready_item(lnklist_item_t* item, void* p)
{
   task_handle_t* const task = item->itmdata;
   if ((task->tflags & TASK_STICKY) == 0) free(item->itmdata);
   return LNKLIST_CONT;
}

danceerr_t tqueue_init(tqueue_handle_t* tq, const tqueue_attr_t* attr)
{
   lnklist_init(&tq->ready_list, NULL);
   lnklist_init(&tq->sticky_list, NULL);

   tq->compl_fd = attr->compl_fd;
   tq->compl_signal = attr->compl_signal;
   tq->compl_cond = attr->compl_cond;
   tq->compl_mutex = attr->compl_mutex;
   tq->tqflags = attr->tqflags;

   if (pthread_mutex_init(&tq->task_mutex, NULL))
   {
      goto on_error_0;
   }

   if (pthread_cond_init(&tq->task_cond, NULL))
   {
      goto on_error_1;
   }

   if (pthread_create(&tq->thread, NULL, tqueue_thread_entry, tq))
   {
      goto on_error_2;
   }

   return DANCE_OK;

 on_error_2:
   pthread_cond_destroy(&tq->task_cond);
 on_error_1:
   pthread_mutex_destroy(&tq->task_mutex);
 on_error_0:
   // fini ready list first as it references sticky tasks
   lnklist_fini(&tq->ready_list, fini_ready_item, NULL);
   lnklist_fini(&tq->sticky_list, fini_sticky_item, NULL);
   return DANCE_ERROR;
}

danceerr_t tqueue_fini(tqueue_handle_t* tq)
{
   // signal with the lock acquired
   pthread_mutex_lock(&tq->task_mutex);
   if ((tq->tqflags & TQUEUE_DONE) == 0)
   {
      tq->tqflags |= TQUEUE_DONE;
      pthread_cond_signal(&tq->task_cond);
   }
   pthread_mutex_unlock(&tq->task_mutex);

   pthread_join(tq->thread, NULL);

   pthread_mutex_destroy(&tq->task_mutex);
   pthread_cond_destroy(&tq->task_cond);

   // fini ready list first as it references sticky tasks
   lnklist_fini(&tq->ready_list, fini_ready_item, NULL);
   lnklist_fini(&tq->sticky_list, fini_sticky_item, NULL);

   return DANCE_OK;
}

static task_handle_t* alloc_task(uint32_t flags, task_fn_t fn, void* data)
{
   task_handle_t* task;

   task = malloc(sizeof(task_handle_t));
   if (task == NULL)
   {
      return NULL;
   }

   task->tflags = flags;
   task->fn = fn;
   task->data = data;
   task->it = NULL;

   return task;
}

danceerr_t tqueue_add(tqueue_handle_t* tq, task_fn_t fn, void* data, uint32_t flags)
{
   task_handle_t* task;

   task = alloc_task(flags, fn, data);
   if (task == NULL)
   {
      return DANCE_ERROR;
   }

   // add to the ready list
   pthread_mutex_lock(&tq->task_mutex);
   lnklist_add_tail(&tq->ready_list, task);
   pthread_cond_signal(&tq->task_cond);
   pthread_mutex_unlock(&tq->task_mutex);

   return DANCE_OK;
}

danceerr_t tqueue_add_sticky(tqueue_handle_t* tq,
                             task_handle_t** taskp,
                             task_fn_t fn, void* data)
{
   task_handle_t* task;

   task = alloc_task(TASK_STICKY, fn, data);
   if (task == NULL)
   {
      return DANCE_ERROR;
   }

   // add to the sticky list
   pthread_mutex_lock(&tq->task_mutex);
   lnklist_add_tail(&tq->sticky_list, task);
   task->it = tq->sticky_list.tail;
   pthread_mutex_unlock(&tq->task_mutex);

   *taskp = task;

   return DANCE_OK;
}

danceerr_t tqueue_set_ready(tqueue_handle_t* tq, task_handle_t* task)
{
   // move the task assumed sticky into the ready list

   pthread_mutex_lock(&tq->task_mutex);
   lnklist_add_head(&tq->ready_list, task);
   pthread_cond_signal(&tq->task_cond);
   pthread_mutex_unlock(&tq->task_mutex);

   return DANCE_OK;
}

static lnklist_for_t del_sticky_task(lnklist_item_t* item, void* arg)
{
   task_handle_t* const task = arg;

   if (item->itmdata == task) lnklist_free_item(item);

   return LNKLIST_CONT;
}

danceerr_t tqueue_del_sticky(tqueue_handle_t* tq, task_handle_t* task)
{
   // lazy deletion
   pthread_mutex_lock(&tq->task_mutex);

   lnklist_foreach(&tq->ready_list, del_sticky_task, task);
//   lnklist_free_item(task->it);

   pthread_mutex_unlock(&tq->task_mutex);

   free(task);

   return DANCE_OK;
}


#if 0 // unit test

#include <stdio.h>

static void task_fn(void* data)
{
   printf("> %s(%s)\n", __FUNCTION__, (const char*)data);
   usleep(1000000);
   printf("< %s(%s)\n", __FUNCTION__, (const char*)data);
}

static void wait(tqueue_handle_t* tq)
{
   printf("> %s\n", __FUNCTION__);

   pthread_mutex_lock(tq->compl_mutex);
   while ((void* volatile)tq->ready_list.head != NULL)
      pthread_cond_wait(tq->compl_cond, tq->compl_mutex);
   pthread_mutex_unlock(tq->compl_mutex);

   printf("< %s\n", __FUNCTION__);
}

int main(int ac, char** av)
{
   tqueue_handle_t tq;
   tqueue_attr_t attr;
   task_handle_t* sticky_task;
   pthread_mutex_t compl_mutex;
   pthread_cond_t compl_cond;
   int i;

   pthread_mutex_init(&compl_mutex, NULL);
   pthread_cond_init(&compl_cond, NULL);

   attr = tqueue_default_attr;

   attr.flags |= TQUEUE_COMPL_COND;
   attr.compl_cond = &compl_cond;
   attr.compl_mutex = &compl_mutex;

   tqueue_init(&tq, &attr);

   tqueue_add_sticky(&tq, &sticky_task, task_fn, "sticky");

   for (i = 0; i != ac; ++i) tqueue_add(&tq, task_fn, av[i], TASK_NORMAL);

   tqueue_set_ready(&tq, sticky_task);
   tqueue_del_sticky(&tq, sticky_task);

   wait(&tq);

   tqueue_fini(&tq);

   pthread_mutex_destroy(&compl_mutex);
   pthread_cond_destroy(&compl_cond);

   return 0;
}

#endif  // unit test
