#ifndef ALARM_H_INCLUDED
#define ALARM_H_INCLUDED

#include <stdint.h>

/*----------------------------------------------
 *
 */

typedef int (*hookfunc_t)(void);

typedef struct {
    int        type;   // HOOK_PRECMD, HOOK_POSTCMD or both
    hookfunc_t funct;
} hookfn_t;

typedef struct {
    uint32_t        precmdmask;
    uint32_t        postcmdmask;
    int             hookcount;
    hookfn_t        hookfuncs[32]; /* one hook per bit flag */
    pthread_mutex_t mutex;
} hooks_t;


/*----------------------------------------------
 *
 */
#define HOOK_PRECMD      (1 << 0)
#define HOOK_POSTCMD     (1 << 1)
#define HOOK_ALL         (HOOK_PRECMD | HOOK_POSTCMD)

#define HOOKRET_CONTINUE (0)
#define HOOKRET_NOMORE   (1)

#define HOOK_ADD(func)             hook_add(func)   // returns hook_n
#define HOOK_ENABLE(hook_n, type)  hook_set(hook_n, type)
#define HOOK_DISABLE(hook_n, type) hook_clr(hook_n, type)
#define HOOK_DISABLE_ALL(hook_n)   HOOK_DISABLE(hook_n, HOOK_ALL)

/*
  Use these macros (from comm.h) to explore the menu (command) table:
      CURRENT_COMMSTR()   // to obtain the command keyword (asciiz string)
      COMMAND_COMMFLAGS() // to obtain the command flags (uint32_t)
      COMMAND_COMMID()    // to obtain the command number (int)
*/

#define IS_APP_HOOK_INSTALLED()  (g_libdance.hooks.hookcount > 1)


/*----------------------------------------------
 *
 */
int  hook_init(void);
int  hook_add(hookfunc_t funct);
int  hook_precmdfilter(command_t* comm);
int  hook_postcmdfilter(command_t* comm);
void hook_set(int hook_n, int hooktype);
void hook_clr(int hook_n, int hooktype);


#endif /* ALARM_H_INCLUDED */

