//---------------------------------------------------------------------------
//
//
#ifndef _REGDEFS_H_INCLUDED_
#define _REGDEFS_H_INCLUDED_

#include "libepci.h"

#include "libdance.h"
#include "i2c.h"
#include "i2cdevices.h"
#include "daq.h"

#if defined(APPREG_LIST) || defined(APPMEM_LIST)
#  define APPREG_USED 1
#else
#  define APPREG_USED 0
#endif

#define _WO       0x01
#define _RW       0x02
#define _RO       0x04
#define _UNK      0x08
#define _NOACCESS 0x00

#define _WR (_WO | _RW)
#define _RD (_RO | _RW)
#define _KNOWN (_WO | _RW | _RO)
#define _ANY (_WO | _RW | _RO | _UNK)

#define IS_READABLE(mode)      (((mode) & _RD) != 0)
#define IS_WRITEABLE(mode)     (((mode) & _WR) != 0)
#define MAY_BE_READABLE(mode)  (((mode) & (_RD | _UNK)) != 0)
#define MAY_BE_WRITEABLE(mode) (((mode) & (_WR | _UNK)) != 0)
#define CANNOT_BE_READ(mode)   (((mode) & (_RD | _RW | _UNK)) == 0)
#define CANNOT_BE_WRITEN(mode) (((mode) & (_WR | _RW | _UNK)) == 0)

// Flags for EXTDEVICE_LIST
#define _IDENT 0x10

#define _ANY_REGMEM   0x00000000
#define _EBONE_REG    0x00010000
#define _BASIC_REG    0x00020000
#define _SPECIAL_REG  0x00040000
#define _D64R_REG     0x00080000
#define _APPL_REG     (_BASIC_REG | _SPECIAL_REG)
#define _ALL_REG      (_EBONE_REG | _APPL_REG)
#define _ALL_MEM      0x01000000

#define _D8   BIN_8    // 1
#define _D16  BIN_16   // 2
#define _D32  BIN_32   // 4
#define _D64  BIN_64   // 8

#define IS_ALIGNED_64(addr) (((addr) & 0x07) == 0)
#define IS_ALIGNED_32(addr) (((addr) & 0x03) == 0)
#define IS_ALIGNED_16(addr) (((addr) & 0x01) == 0)

#define ALIGN_64(addr) (((addr) + 0x07) & ~(uint64_t)0x07)
#define ALIGN_32(addr) (((addr) + 0x03) & ~(uint64_t)0x03)
#define ALIGN_16(addr) (((addr) + 0x01) & ~(uint64_t)0x01)

#define NUMARGS(...)  (sizeof((int[]){__VA_ARGS__})/sizeof(int))

#define _FAKE_BUS (1 << 0)
#define _PCIE_BUS (1 << 1)
#define _EI2C_BUS (1 << 2)

#define PCI_NBARS 6

typedef struct {
   int          bus_id;
   const char*  bname;
   int          bustype;
   char*        ident;
   unsigned int flags;
   union  {
      epcihandle_t pcihandles[PCI_NBARS];
      i2c_handle_t i2chandle;
   } info;
} dancebus_t;

typedef struct {
   dancebus_t*     bustbl;
   unsigned short  n_bus;
   int             bustypes;
} dancebusdescr_t;

typedef struct {
   dancebus_t* bus;
   int         selector;
   int         bustype;
   union  {
      epcihandle_t     pci;
      i2c_handle_t* i2c;
   }           bushandle;
} reginfo_t;

typedef struct {
   // this are constant definition params */
   int          id;
   char*        name;
   unsigned int baddr;
   size_t       byteoffset;
   uint64_t     bytesize;
   size_t       ndata;
   unsigned int dtype;
   unsigned int accmode;
   unsigned int flags;

   // this are calculated params for register access
   reginfo_t*         info;
   size_t             offset;
   unsigned int       access;
   union {
      uint8_t         v8;
      uint16_t        v16;
      uint32_t        v32;
      uint64_t        v64;
   } shadow;
} dancereg_t;

typedef struct {
   dancereg_t*     regtbl;
   unsigned short  n_entries;
   unsigned short  first_baseaddr;
   unsigned short  baseaddr_n;
   unsigned short  first_register;
   unsigned short  register_n;
   unsigned short  first_memblock;
   unsigned short  memblock_n;
} danceregdescr_t;

typedef struct {
   int            devid;
   const char*    devname;
   dancebus_t*    buspt;
   uint16_t       baseaddr;
   uint16_t       address;
   uint16_t       devtype;
   uint16_t       devflags;
} danceextdev_t;

typedef struct {
   danceextdev_t* devtbl;
   unsigned short n_entries;
} danceextdevdesc_t;

extern dancebusdescr_t*    gdance_busdescr;
extern danceregdescr_t*    gdance_regdescr;
extern danceextdevdesc_t*  gdance_extdevdesc;

//---------------------------------------------------------------------------
// List of communication interfaces

#ifndef COMMDEVICE_LIST
#  define COMMDEVICE_LIST
#endif

#define COMMDEV_REQUIRED  (1 << 0)
#define COMMDEV_MANDATORY (1 << 1)

#define ETHERCOMM(name, flags)
#define TCPCOMM(minport, maxport, flags)
#define HTTPCOMM(port, flags)

#undef ETHERCOMM
#define ETHERCOMM(name, flags) +1
#if ((0 COMMDEVICE_LIST) > 0)
   #define ETHERNET_USED 1
#else
   #define ETHERNET_USED 0
#endif
#undef ETHERCOMM
#define ETHERCOMM(name, flags)

#undef TCPCOMM
#define TCPCOMM(minport, maxport, flags) +1
#if ((0 COMMDEVICE_LIST) > 0)
   #define TCP_USED 1
#else
   #define TCP_USED 0
#endif
#undef TCPCOMM
#define TCPCOMM(minport, maxport, flags)

#undef HTTPCOMM
#define HTTPCOMM(port, flags) +1
#if ((0 COMMDEVICE_LIST) > 0)
   #define HTTP_USED 1
#else
   #define HTTP_USED 0
#endif
#undef HTTPCOMM
#define HTTPCOMM(port, flags)


//---------------------------------------------------------------------------
// External buses

#ifndef EXTBUS_LIST
#  define EXTBUS_LIST
#endif

#define PCIEXTBUS(name, id) +1
#define PCIEXTBUS_DELAYED(name, id) +1
#define I2CEXTBUS(name, id)
#if ((0 EXTBUS_LIST) > 0)
   #define PCIBUS_USED 1
#else
   #define PCIBUS_USED 0
#endif
#undef PCIEXTBUS
#undef PCIEXTBUS_DELAYED
#undef I2CEXTBUS

#define PCIEXTBUS(name, id)
#define PCIEXTBUS_DELAYED(name, id)
#define I2CEXTBUS(name, id) +1
#if ((0 EXTBUS_LIST) > 0)
   #define I2CBUS_USED 1
#else
   #define I2CBUS_USED 0
#endif
#undef PCIEXTBUS
#undef PCIEXTBUS_DELAYED
#undef I2CEXTBUS

#define EXTBUS_USED (PCIBUS_USED || I2CBUS_USED)


#define PCIEXTBUS(name, id)           name,
#define PCIEXTBUS_DELAYED(name, id)   name,
#define I2CEXTBUS(name, id)           name,
#define I2CSUBBUS(name,ibus,iad,ityp) name,
enum {FAKEBUS=3000, EXTBUS_LIST EXTBUS_MAXID};
#undef PCIEXTBUS
#undef PCIEXTBUS_DELAYED
#undef I2CEXTBUS
#undef I2CSUBBUS

#define DANCE_BUS_PT_INDEX(idx) (&gdance_busdescr->bustbl[idx])
#define DANCE_BUS_PT(bus_id) DANCE_BUS_PT_INDEX((bus_id) - FAKEBUS)
#define IS_VALID_BUS_ID(bus_id) ((bus_id) >= FAKEBUS && (bus_id) < (FAKEBUS + gdance_busdescr->n_bus))

//---------------------------------------------------------------------------
// Lists of base addresses and registers

#ifndef EXTDEVICE_LIST
#  define EXTDEVICE_LIST
#endif

#define PCIE_FPGA(fname, busid, baddr, off, flags)
#define I2C_COMMLED(bus_id, addr, type)
#define I2C_BOARDID(bname, bus_id, addr, type)
#define BASEI2CDEV(name, bus_id, addr, type, flags)
#define RASHPA_CTRL(rname, bid, addr, type)
#define DMA_CTRL(name, bid, addr, type, flags)

// check presence of PCIE_FPGAs in EXTDEVICE_LIST
#undef PCIE_FPGA
#define PCIE_FPGA(fname, busid, baddr, off, flags) +1
#if ((0 EXTDEVICE_LIST) > 0)
   #define FPGA_USED 1
#else
   #define FPGA_USED 0
#endif
#undef PCIE_FPGA
#define PCIE_FPGA(fname, busid, baddr, off, flags)

// check presence of I2C_COMMLED in EXTDEVICE_LIST
#undef I2C_COMMLED
#define I2C_COMMLED(bus_id, addr, type)  +1
#if ((0 EXTDEVICE_LIST) > 0)
   #define COMLED_USED 1
#else
   #define COMLED_USED 0
#endif
#undef I2C_COMMLED
#define I2C_COMMLED(bus_id, addr, type)

// check presence of I2C_BOARDID in EXTDEVICE_LIST
#undef I2C_BOARDID
#define I2C_BOARDID(bname, bus_id, addr, type)  +1
#if ((0 EXTDEVICE_LIST) > 0)
   #define BOARDID_USED 1
#else
   #define BOARDID_USED 0
#endif
#undef I2C_BOARDID
#define I2C_BOARDID(bname, bus_id, addr, type)

// check presence of RASHPA_CTRL in EXTDEVICE_LIST
#undef RASHPA_CTRL
#define RASHPA_CTRL(rname, bid, addr, type) +1
#if ((0 EXTDEVICE_LIST) > 0)
   #define RASHPA_USED 1
#else
   #define RASHPA_USED 0
#endif
// include rashpa.h only once RASHPA_USED has been initialised
#include "rashpa.h"

// check presence of DMA_CTRL in EXTDEVICE_LIST
#undef DMA_CTRL
#define DMA_CTRL(name, bid, addr, type, flags) +1
#if ((0 EXTDEVICE_LIST) > 0)
   #define DMA_CTRL_USED 1
#else
   #define DMA_CTRL_USED 0
#endif

#undef PCIE_FPGA
#undef I2C_COMMLED
#undef I2C_BOARDID
#undef BASEI2CDEV
#undef RASHPA_CTRL
#undef DMA_CTRL

#define FAKEEXTDEVICE                           EXTDEVICE_DEF(FAKEDEVICE, FAKEBUS, 0, 0 , UNKNOWN, 0)
#define PCIE_FPGA(name, bid, baddr, offs, flgs) EXTDEVICE_DEF(FPGA_ ## name, bid, baddr, 0, UNKNOWN, flgs)
#define I2C_COMMLED(bid, addr, type)            EXTDEVICE_DEF(COMMLED_DEV, bid, 0, addr, type, 0)
#define I2C_BOARDID(bname, bid, addr, type)     EXTDEVICE_DEF(BRD_ ## bname, bid, 0, addr, type, _IDENT)
#define BASEI2CDEV(name, bid, addr, type, flgs) EXTDEVICE_DEF(name, bid, 0, addr, type, flgs)
#define RASHPA_CTRL(rname, bid, addr, type)     EXTDEVICE_DEF(rname, FAKEBUS, 0, 0 , UNKNOWN, 0)
#define DMA_CTRL(name, bid, addr, type, flgs)   EXTDEVICE_DEF(DMA_ ## name, bid, 0, addr, type, flgs)

#define EXTDEVICE_DEF(name, bus_id, baddr, addr, type, flags) name,
enum {FAKEEXTDEVICE EXTDEVICE_LIST};
#undef  EXTDEVICE_DEF

#define DANCE_EXTDEV_PT(devid) (&gdance_extdevdesc->devtbl[devid])
#define DANCE_BUS_FROM_EXTDEV_PT(devid) (DANCE_BUS_PT(DANCE_EXTDEV_PT(devid)->buspt)
#define IS_VALID_EXTDEV_ID(devid) ((devid > FAKEDEVICE ) && (devid < gdance_extdevdesc->n_entries))

//---------------------------------------------------------------------------
// External hardware devices

// make sure that all the 'user' symbols are defined
#ifndef BASEADDR_LIST
#  define BASEADDR_LIST
#endif
#ifndef APPREG_LIST
#  define APPREG_LIST
#endif
#ifndef RASHPABADDR_LIST
#  define RASHPABADDR_LIST
#endif
#ifndef RASHPAREG_LIST
#  define RASHPAREG_LIST
#endif
#ifndef APPMEM_LIST
#  define APPMEM_LIST
#endif
#ifndef DAQBADDR_LIST
#  define DAQBADDR_LIST
#endif
#ifndef DAQREG_LIST
#  define DAQREG_LIST
#endif

/*   ---  Example of bus and base address definitions

#define EXTBUS_LIST \
   PCIEXTBUS(FPGA, "10ee:eb01")

#define BASEADDR_LIST \
   BASEADDR(BAR1,  FPGA, 1,  0,  _RW) \
   BASEADDR(BAR2,  FPGA, 2,  0,  _RW) \
   BASEINDREG(CH_BASE,    BAR1, CH_REGS_OFF,    _RW) \
   BASEINDREG(RDVAL_BASE, BAR1, RDVAL_REGS_OFF, _RW) \

*/

#define _DIR_OFFSET 0
#define _IND_OFFSET 1

#define BASEFAKE ENTRY_DEF(FAKEBADDR, FAKEBUS, 0, 0, 0, _RW, 0)
   // plain base address:  bus selector 'sel' is coded in flags field
#define BASEADDR(b, bus_id, sel, offset, acmod) ENTRY_DEF(b, bus_id, offset, 0, 0, acmod, sel)
   // relative base address:  reference to other base address
#define BASEREG(b, baddr, offset, acmod) ENTRY_DEF(b, baddr, offset, 0, 0, acmod, _DIR_OFFSET)
   // indirect base addressing (offset contained in a register
#define BASEINDREG(b, baddr, reg, acmod) ENTRY_DEF(b, baddr, reg,    0, 0, acmod, _IND_OFFSET)
   // addressing of EBONE registers via i2c: addr is coded in flags field
#define BASEI2CEBONE(b, bus_id, iaddr, acmod) ENTRY_DEF(b, bus_id, 0, 0, 0, acmod, iaddr)

//   BASEI2CEBONE(EBON1, I2C0, 0x33, _RW)

/*   ---  Example of definition of registers and register blocks

#define I2CREGS_BLKDEF(...)
   BLKREG32(_DPM,  0x04, _RO, __VA_ARGS__)
   BLKREG32(_TOTO, 0x08, _RO, __VA_ARGS__)

#define APPREG_LIST \
   REG32DEF(REG1,       BAR1, 0x1000, _RW, _BASIC_REG) \
   REG32DEF(REG1,       BAR1, 0x1000, _RW, _BASIC_REG) \
   ...
   REGBLOCK(I2C1, I2CREGS, BAR1, 0x0200, _BASIC_REG)
   ...
*/

#define REGDEF(reg, baddr, offset, dtype, acmod, flags)  ENTRY_DEF(reg, baddr, offset, 1, dtype, acmod, flags)
#define REG8DEF( r, baddr, offset, acmod, flags) REGDEF(r, baddr, offset, _D8,  acmod, flags)
#define REG16DEF(r, baddr, offset, acmod, flags) REGDEF(r, baddr, offset, _D16, acmod, flags)
#define REG32DEF(r, baddr, offset, acmod, flags) REGDEF(r, baddr, offset, _D32, acmod, flags)
#define REG64DEF(r, baddr, offset, acmod, flags) REGDEF(r, baddr, offset, _D64, acmod, flags)
#define REG64RDEF(r,baddr, offset, acmod, flags) REG64DEF(r, baddr, offset, acmod, (flags) | _D64R_REG)

#define MEMDEF(mem, baddr, offset, size, dtype, acmod, flags) ENTRY_DEF(mem, baddr, offset, size, dtype, acmod, flags)

#define REGBLOCK(pfx, btype, baddr, offset, flags)  btype ## _BLKDEF(pfx, baddr, offset, flags)

#define BLKREG8( sfx, offset, acmod, pfx, baddr, offblk, flags) REG8DEF( pfx ## sfx, baddr, offset + offblk, acmod, flags)
#define BLKREG16(sfx, offset, acmod, pfx, baddr, offblk, flags) REG16DEF(pfx ## sfx, baddr, offset + offblk, acmod, flags)
#define BLKREG32(sfx, offset, acmod, pfx, baddr, offblk, flags) REG32DEF(pfx ## sfx, baddr, offset + offblk, acmod, flags)
#define BLKREG64(sfx, offset, acmod, pfx, baddr, offblk, flags) REG64DEF(pfx ## sfx, baddr, offset + offblk, acmod, flags)


// Generate unique register identifiers for registers, memblocks and base addresses
#define ENTRY_DEF(id, baddr, offset, ndata, dtype, acmod, flags) id,
enum {BASEFAKE
      BASEADDR_LIST
      RASHPABADDR_LIST
      DAQBADDR_LIST
      APPREG_LIST
      RASHPAREG_LIST
      DAQREG_LIST
      APPMEM_LIST};
#undef  ENTRY_DEF


#define IS_BASEADDR(idx)     ((idx) >= 0 && idx < gdance_regdescr->baseaddr_n)
#define _IS_IN_RANGE(idx, first, n) ((idx) >= (first) && (idx) < ((first) + (n)))
#define IS_VALID_REGIDX(idx)  _IS_IN_RANGE(idx, gdance_regdescr->first_register, gdance_regdescr->register_n)
#define IS_VALID_MEMIDX(idx)  _IS_IN_RANGE(idx, gdance_regdescr->first_memblock, gdance_regdescr->memblock_n)
#define IS_VALID_ENTRIDX(idx) ((size_t)(idx) < gdance_regdescr->n_entries)
#define IS_REGISTER(idx)      (IS_VALID_ENTRIDX(idx) && DANCE_REG_PT(idx)->ndata == 1)

#define DANCE_REG_PT(reg_id) (&gdance_regdescr->regtbl[reg_id])
#define DANCE_BADDR_PT(baddr_id) (&gdance_regdescr->regtbl[baddr_id])
#define DANCE_REGMEM_PCI_HANDLE(rm_pt) ((rm_pt)->info->bushandle.pci)
#define DANCE_REGMEM_OFFSET(rm_pt)     ((rm_pt)->offset)
#define DANCE_REGMEM_SET_OFFSET(rm_pt, _offs) ((rm_pt)->offset = (_offs))
#define DANCE_REGMEM_DTYPE(rm_pt)      ((rm_pt)->dtype)
#define DANCE_REGMEM_SET_DTYPE(rm_pt, _dtyp)  ((rm_pt)->dtype = (_dtyp))

dancereg_t* dance_new_reg(unsigned int baddr, ssize_t offset, int dtype, size_t ndsize);
#define DANCE_NEWREG_PT(baddr_id, offset, dtype) dance_new_reg(baddr_id, offset, dtype, 1)


#endif   // _REGDEFS_H_INCLUDED_
