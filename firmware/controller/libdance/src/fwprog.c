#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <stdint.h>

#include "libdance.h"

#include "fwprog.h"
#include "libefspi.h"
#include "libejtag.h"


int prog_fpga_spi(const char* pci_ident
                  , int pci_bar
                  , int pci_off
                  , const char* bitpath) {
   int iret = PROG_ERROR;
   efspi_handle_t efspi;
   efspi_desc_t desc;

   if (efspi_init_lib() == EFSPI_ERR_SUCCESS) {
      desc.pci_id = pci_ident;
      desc.pci_bar = pci_bar;
      desc.pci_off = pci_off;
      desc.write_fn = NULL;
      desc.erase_fn = NULL;

      if (desc.pci_id != NULL) {
         if (efspi_open(&efspi, &desc) == EFSPI_ERR_SUCCESS) {
            if (efspi_erase_write_bit(&efspi, bitpath))
            {
               LOG_ERROR("Error while programming bitstream through SPI (file: %s)\n", bitpath);
            } else {
               iret = PROG_SUCCESS;
            }
         }
      }
   }
   return iret;
}

int prog_fpga_jtag(uint16_t pos
                  , const char* bitpath
                  , uint32_t flags) {
   ejtag_desc_t desc;
   ejtag_handle_t ejtag;

   ejtag_init_desc(&desc);

   if (flags != 0)
   {
      desc.flags |= flags;
   }
   else
   {
      const uint32_t autoflag = ejtag_probe_link();
      if (autoflag == 0)
      {
         LOG_ERROR("Could not detect JTAG link type\n");
         goto on_error_1;
      } else {
         desc.flags |= autoflag;
      }
   }

   if (ejtag_open(&ejtag, &desc))
   {
      LOG_ERROR("Error while openning JTAG device\n");
      goto on_error_1;
   }

   if (ejtag_program_bit_file(&ejtag, pos, bitpath))
   {
      LOG_ERROR("Error while programming bitstream through JTAG (file: %s)\n");
      goto on_error_1;
   }

   return PROG_ERROR;

   on_error_1:
      return PROG_ERROR;
}