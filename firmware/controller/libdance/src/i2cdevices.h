#ifndef __I2CDEVICES_H_INCLUDED__
#define __I2CDEVICES_H_INCLUDED__


#include <stdint.h>
#include "i2c.h"

// list of i2c known devices
#define EXTDEV_UNKNOWN  0
#define EXTDEV_PCA9500  1


// I2C SMBUS Command code for TI TCA9554 component
#define I2C_CMD_IN_RD             0x00
#define I2C_CMD_OUT_RDWR          0x01
#define I2C_CMD_POLARITYINV_RDWR  0x02
#define I2C_CMD_CONF_RDWR         0x03

// Default commands
#define I2C_SLAVECMD_UNUSED       0x00

// EEPROM writing info
#define HEADER_SIZE              4
#define HEADER_MAGICID           0xE59F
#define EEPROM_KEYPASS           "eraser41"

/* ---------------------------------------------------------------------
 * i2c_txd_rxd
 *    Parameters :
 *       - i2c_h : i2c_phandle_t, handler of the device.
 *       - addr : I2C address
 *       - offset : Slave offset
 *       - nbytes : size_t, bytes count to read.
 *       - wr_data : const uint8_t*, date information to send
 *       - block_size : size_t, Number of byte which can be transmit in SMBUS transaction.
 *       - delayms : delay between 2 cycles in milliseconds
 */
int i2c_txd_cycle( i2c_phandle_t i2c_h
                  , uint16_t addr
                  , uint16_t offset
                  , size_t nbytes
                  , const uint8_t* wr_data
                  , size_t block_size
                  , uint32_t delay_ms );

int read_identprog(uint16_t devid, char * rd_out, int nsize);

#endif /* __I2CDEVICES_H_INCLUDED__ */