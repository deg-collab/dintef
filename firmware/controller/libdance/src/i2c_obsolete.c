/* simple i2c implementation */
/* currently only local master based single byte synchronous transfers */


#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

#include "libdance.h"

#include "i2c_obsolete.h"

/* exported */

void i2c_init_desc(i2c_desc_t* desc)
{
  desc->master_path = NULL;
  desc->slave_addr = 0;
}


void i2c_init_local_desc(i2c_desc_t* desc, uint32_t slave_addr)
{
  i2c_init_desc(desc);
  desc->master_path = DEFAULT_I2C_BUS;
  desc->slave_addr = slave_addr;
}


int i2c_open_desc(i2c_master_t* i2c, const i2c_desc_t* desc)
{
  if (desc->master_path == NULL) goto on_error_0;
  i2c->ifd = open(desc->master_path, O_RDWR);
  if (i2c->ifd == -1) goto on_error_0;
  if (ioctl(i2c->ifd, I2C_SLAVE, (int)desc->slave_addr) < 0) goto on_error_1;
    return 0;
  on_error_1:
    close(i2c->ifd);
  on_error_0:
    return -1;
}


void i2c_close_desc(i2c_master_t* i2c)
{
  close(i2c->ifd);
}

int i2c_switch_slave(i2c_master_t* i2c, i2c_desc_t* desc, uint32_t slave_addr)
{
  int iret = 0;
  desc->slave_addr = slave_addr;
  if (i2c->ifd > 0)
  {

    // Switch i2c slave
    if ( ioctl(i2c->ifd, I2C_SLAVE, (int)desc->slave_addr) < 0 )
      iret = -1;
  }
  return iret;
}

static int i2c_readwrite_uint8
(i2c_master_t* i2c, uint8_t rw, uint8_t cmd, union i2c_smbus_data* x)
{
  struct i2c_smbus_ioctl_data arg;

  arg.read_write = rw;
  arg.command = cmd;
  arg.size = I2C_SMBUS_BYTE_DATA;
  arg.data = x;

  return ioctl(i2c->ifd, I2C_SMBUS, &arg);
}


int i2c_write_uint8(i2c_master_t* i2c, uint8_t cmd, uint8_t x)
{
  union i2c_smbus_data data;

  data.byte = x;

  return i2c_readwrite_uint8(i2c, I2C_SMBUS_WRITE, cmd, &data);
}


int i2c_read_uint8(i2c_master_t* i2c, uint8_t cmd, uint8_t* x)
{
  union i2c_smbus_data data;
  int err;

  err = i2c_readwrite_uint8(i2c, I2C_SMBUS_READ, cmd, &data);

  if (err) return err;

  *x = data.byte;

  return 0;
}


int i2c_eeprom_write( i2c_master_t* h_i2c
                , unsigned int u_offset
                , const char* u_cbuf
                , unsigned int u_len)
{
  int iret = 0;
  unsigned int u_cpt  = 0;

  unsigned int u_cpt2 = 0;
  unsigned int u_endpod;
  union i2c_smbus_data data;

  // Offset address start at 0x00 and max is 0xFF
  if ( u_len > EEPROM_BYTES_PER_PAGE
      || u_offset > (EEPROM_BYTES_PER_PAGE - 1) )
  {
    iret = -1;
  }
  else
  {
    if ( (u_len + u_offset) >= EEPROM_BYTES_PER_PAGE )
      u_endpod = EEPROM_BYTES_PER_PAGE;
    else
      u_endpod = u_len + u_offset;

    for ( u_cpt = u_offset; u_cpt < u_endpod; u_cpt++ )
    {
      data.byte = u_cbuf[u_cpt2];
      iret = i2c_readwrite_uint8(h_i2c, I2C_SMBUS_WRITE, u_cpt, &data );
      usleep(EEPROM_WRITE_WAIT_MS * 1000);
      if ( iret < 0 )
        return iret;
      u_cpt2++;
    }

    // add \0
    if ( u_cpt2 < EEPROM_BYTES_PER_PAGE )
    {
      data.byte = '\0';
      iret = i2c_readwrite_uint8(h_i2c, I2C_SMBUS_WRITE, u_cpt, &data );
      usleep(EEPROM_WRITE_WAIT_MS * 1000);
    }
  }

  return iret;
}

int i2c_eeprom_read( i2c_master_t* h_i2c
                , unsigned int u_offset
                , char *u_cbuf
                , unsigned int u_len )
{
  int iret = 0;
  unsigned int u_cpt  = 0;
  unsigned int u_cpt2 = 0;
  unsigned int u_endpod;
  union i2c_smbus_data data;

  // Offset address start at 0x00 and max is 0xFF
  if ( u_len > EEPROM_BYTES_PER_PAGE
      || u_offset > (EEPROM_BYTES_PER_PAGE -1) )
  {
    iret = -1;
  }
  else
  {
    if ( (u_len + u_offset) >= EEPROM_BYTES_PER_PAGE )
      u_endpod = EEPROM_BYTES_PER_PAGE;
    else
      u_endpod = u_len + u_offset;

    for( u_cpt = u_offset; u_cpt < u_endpod; u_cpt++ )
    {
      iret = i2c_readwrite_uint8(h_i2c, I2C_SMBUS_READ, u_cpt, &data );
      if ( iret < 0 )
        return iret;

      u_cbuf[u_cpt2] = data.byte;
      u_cpt2++;
    }
  }

  return iret;
}
