#ifndef HWINFO_H_INCLUDED
#define HWINFO_H_INCLUDED 1


#include <stdint.h>
#include <sys/types.h>
#include "i2cdevices.h"

/* exported */

typedef struct
{
  char name[32];
  uint8_t board_model;
  uint8_t pcb_version;
} hwinfo_t;

int hwinfo_init(void);
int hwinfo_get(const hwinfo_t**, size_t*);
int get_board_ident();
size_t get_fpga_identification(const char* pci_ident
                              , int pci_bar
                              , int pci_off
                              , uint8_t* dest
                              , size_t out_size);
size_t answer_jtag_chain_id(unsigned int prompted);

#endif /* HWINFO_H_INCLUDED */
