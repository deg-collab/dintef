/*---------------------------------------------------------------------------
 *
 * Functions used to implement ?DONFIG queries
 *
 *---------------------------------------------------------------------------
 * $Revision: 2198 $
 *
 */

#include <stdarg.h>
#include <time.h>

#include "libdance.h"


static void _dconfig_terminate(int* nchar)
{
   char* end_ptr = dblock_end(get_current_command()->main_answ) - 1;

   if (*end_ptr != '\n')
   {
       int nc = answerf("\n");
       if (nc < 0)
          *nchar = nc;
       else
          *nchar += nc;
   }
}

static void _dconfig_complete(int* nchar, const char* fmt, va_list ap)
{
   int nc;

   if (*nchar < 0)
      return;

   if (fmt == NULL || *fmt)
   {
      nc = answerf(" ");
       if (nc < 0)
          *nchar = nc;
       else
          *nchar += nc;

      if (nc < 0 || fmt == NULL)
         return;
   }

   nc = vanswerf(fmt, ap);
   if (nc < 0)
      *nchar = nc;
   else
   {
      *nchar += nc;
      _dconfig_terminate(nchar);
   }
}


int dconfig_commentf(const char* comment_fmt, ...)
{
   va_list ap;
   int     nchar;

   va_start(ap, comment_fmt);
   nchar = answerf("#");
   _dconfig_complete(&nchar, comment_fmt, ap);
   va_end(ap);
   return(nchar);
}


int dconfig_metadataf(const char* keyword, const char *value_fmt, ...)
{
   va_list ap;
   int     nchar;

   if (!keyword || !*keyword)
   {
      SYSTEM_ERROR();
      return(-1);
   }

   nchar = answerf("# %%%s%%", keyword);
   va_start(ap, value_fmt);
   _dconfig_complete(&nchar, value_fmt, ap);
   va_end(ap);
   return(nchar);
}


int dconfig_commandf(const char *cmd_fmt, ...)
{
   int     nchar = 0;
   va_list ap;

   if (cmd_fmt != NULL && *cmd_fmt)
   {
      va_start(ap, cmd_fmt);
      nchar = vanswerf(cmd_fmt, ap);
      va_end(ap);
      if (nchar < 0)
         goto on_exit;
   }
   _dconfig_terminate(&nchar);

on_exit:
   if (nchar < 0)
      SYSTEM_ERROR();
   return(nchar);
}


#define BINARY_DCONFIGFORMAT " // <%s> "

int dconfig_bincommandf(const char *bin_name, const char *cmd_fmt, const char *query_fmt, ...)
{
   int     nc, nchar = 0;
   va_list ap;

   if (bin_name == NULL || !*bin_name)
   {
      SYSTEM_ERROR();
      return(-1);
   }
   va_start(ap, query_fmt);

   if (cmd_fmt != NULL && *cmd_fmt)
   {
      if ((nchar = vanswerf(cmd_fmt, ap)) < 0)
         goto on_exit;
      else
      {
         if (consume_va_args(&ap, cmd_fmt) == (size_t)-1)
         {
            nchar = -1;
            goto on_exit;
         }
      }
   }

   nc = answerf(BINARY_DCONFIGFORMAT, bin_name);
   if (nc < 0)
   {
      nchar = nc;
      goto on_exit;
   }
   else
      nchar += nc;

   if (query_fmt != NULL && *query_fmt)
   {
      nc = vanswerf(query_fmt, ap);
      if (nc < 0)
         nchar = nc;
      else
      {
         nchar += nc;
         _dconfig_terminate(&nchar);
      }
   }
on_exit:
   va_end(ap);
   if (nchar < 0)
      SYSTEM_ERROR();
   return(nchar);
}


void dconfig_mandatory(void)
{
   time_t  now = time(NULL);

   dconfig_metadataf("APPNAME", "%s", danceapp_name);
   dconfig_metadataf("VERSION", NULL); commexec_qVERSION(); answerf("\n");
   dconfig_metadataf("DATE", "%s", asctime(localtime(&now)));
}


__attribute__ ((weakref ("partial_exec_qDCONFIG")))
static void app_exec_qDCONFIG(void);

void commexec_qDCONFIG(void)
{
   if (app_exec_qDCONFIG) {
      dconfig_mandatory();
      app_exec_qDCONFIG();
   } else {
      errorf(NOTIMPLMESSAGE);
   }
}
