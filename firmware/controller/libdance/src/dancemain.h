/*---------------------------------------------------------------------------
 *  dancemain.h
 *
 *  To be included only by the main dance application files
 *  Files in libdance should always use exclusively libdance.h
 *
 */
#ifndef _DANCEMAIN_H_INCLUDED_
#define _DANCEMAIN_H_INCLUDED_

#define __INCLUDE_DANCE_DECLARATIONS
#include "dance.h"

#endif // _DANCEMAIN_H_INCLUDED_

