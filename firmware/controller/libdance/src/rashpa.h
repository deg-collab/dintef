#ifndef __RASHPA_H_INCLUDED__
#define __RASHPA_H_INCLUDED__

#include "regdefs.h"  // required to get RASHPA_USED defined


#if RASHPA_USED

#include "rashpadefs.h"

#else

#define RASHPA_MENU_LIST
#define RASHPA_LISTS

#endif   // RASHPA_USED

#endif /* __RASHPA_H_INCLUDED__ */