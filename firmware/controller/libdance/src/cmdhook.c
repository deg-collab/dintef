/*---------------------------------------------------------------------------
 * Handle command hook control
 *
 */
#include "libdance.h"
#include "cmdhook.h"
#include "errors.h"

/*----------------------------------------------
 *
 */
int hook_init(void)
{
    hooks_t *hooks = &(g_libdance.hooks);
    int ret = DANCE_ERROR;

    /* Internals init */
    hooks->precmdmask = 0;
    hooks->postcmdmask = 0;
    hooks->hookcount = 0;

    if (pthread_mutex_init(&hooks->mutex, NULL)) {
        goto on_error;
    }

    ret = DANCE_OK;
on_error:
    return ret;
};


/*----------------------------------------------
 *
 */
int hook_add(hookfunc_t funct) {
    hooks_t *hooks = &(g_libdance.hooks);
    int hook_n;

    /* TODO: use bit mask to check which bits are used cf removing hook */
    /* TODO: shift app bits to have several higher priority bits for libdance */
    if (hooks->hookcount >= 32) {
        goto on_error;
    }
    hook_n = hooks->hookcount;

    hooks->hookfuncs[hook_n].funct = funct;
    hooks->hookfuncs[hook_n].type   = 0;
    hooks->hookcount++;
    return hook_n;

on_error:
    return -1;
}


/*----------------------------------------------
 *
 */
int hook_cmdfilter(int hooktype, command_t* comm, int* ret)
{
    hooks_t *hooks = &(g_libdance.hooks);
    uint32_t cmdmask;
    int bit;
    int ncalls = 0;

    *ret = HOOKRET_CONTINUE;
    cmdmask = (hooktype == HOOK_PRECMD)? hooks->precmdmask : hooks->postcmdmask;

    /* For all hooks */
    for (bit = 0; bit < hooks->hookcount; bit++) {
        /* Check if alarm set */
        if (cmdmask & (1 << bit)) {
            /* Check if hook registered */
            ncalls++;
            if (hooks->hookfuncs[bit].type & hooktype) {
               *ret = hooks->hookfuncs[bit].funct();
               /* HOOKRET_NOMORE return means stop calling following hooks */
               if (*ret == HOOKRET_NOMORE)
                  goto on_done;
            }
        }
    }
on_done:
    return ncalls;
}


int hook_precmdfilter(command_t* comm)
{
    int ret;

    if (hook_cmdfilter(HOOK_PRECMD, comm, &ret) > 0) {
        /* HOOKRET_NOMORE return means disabling next command exec */
        if (ret == HOOKRET_NOMORE) {
            /* Return error message if hook didn't */
            if (ERROR_NOTFOUND()) {  
                errorf("command not allowed\n");
            }
            return 1;
        }
    }
    return 0;
}

int hook_postcmdfilter(command_t* comm)
{
    int ret;

    return hook_cmdfilter(HOOK_POSTCMD, comm, &ret);
}


/*----------------------------------------------
 *
 */
void hook_set(int hook_n, int hooktype)
{
    hooks_t *hooks = &(g_libdance.hooks);

    pthread_mutex_lock(&hooks->mutex);

    if (hooktype & HOOK_PRECMD)
       hooks->precmdmask |= (1 << hook_n);
    
    if (hooktype & HOOK_POSTCMD)
       hooks->postcmdmask |= (1 << hook_n);

    hooks->hookfuncs[hook_n].type |= hooktype;
    pthread_mutex_unlock(&hooks->mutex);
}


/*----------------------------------------------
 *
 */
void hook_clr(int hook_n, int hooktype)
{
    hooks_t *hooks = &(g_libdance.hooks);

    pthread_mutex_lock(&hooks->mutex);
    if (hooktype & HOOK_PRECMD)
       hooks->precmdmask &= (uint32_t)(~(1 << hook_n));
    if (hooktype & HOOK_POSTCMD)
       hooks->postcmdmask &= (uint32_t)(~(1 << hook_n));

    hooks->hookfuncs[hook_n].type &= ~hooktype;
    pthread_mutex_unlock(&hooks->mutex);
}


