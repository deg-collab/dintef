#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/utsname.h>

#include "libdance.h"

#include "eth.h"
#include "hwinfo.h"
#include "dinfo.h"


static void answer_dinfo(const char* name, int what) {
   unsigned int prompted = (name != NULL);

   if (prompted) answerf("%s: ", name);

   switch (what) {
      case DINFO_UPTIME:
         answerf("%ld", (long)time(NULL));
         break;

      case DINFO_UNAME:
         {
            struct utsname udata;

            if (uname(&udata) == -1) {
               errorf("could not get uname info");
               return ;
            }
            answerf("%s %s %s %s %s", udata.sysname, udata.nodename,
                                        udata.release, udata.version, udata.machine);
         }
         break;

      case DINFO_HWIDS:
#if BOARDID_USED
         {
            const hwinfo_t* info = NULL;
            size_t count;
            size_t idx;

            if (hwinfo_get(&info, &count)) {
               LOG_ERROR("Could not get information\n");
               answerf("N/A");
               return ;
            }

            if (count == 0) {
               LOG_ERROR("No identifier found\n");
               answerf("N/A");
               return ;
            }
            if (prompted) answerf("\n");
            for (idx = 0; idx < count; idx++) {
               const hwinfo_t* const pinfo = &info[idx];
               answerf("%s %s [ %x %x ]\n", prompted? "   ":"", pinfo->name, pinfo->board_model, pinfo->pcb_version);
            }
            prompted = 0;
         }
#else
         answerf("N/A");
#endif
         break ;

      case DINFO_APPNAME:
         answerf("%s", danceapp_name);
         break ;

      case DINFO_VERSION:
         answerf("%02d.%02d", danceapp_major, danceapp_minor);
         break ;

      case DINFO_ADDR:
         answerf("%s", get_current_command()->channel->address);
         break ;

      case DINFO_NET:
#if ETHERNET_USED
         {
            const eth_handle_t* const eth = g_libdance.eth;
            const char* link_str;
            const char* cable_str;
            const char* duplex_str;
            const char* speed_str;
            const char* autoneg_str;
            char mac_str[32];

            if (eth == NULL) break;

            if (eth->eflags & ETH_FLAG_LINK_UP) link_str = "up";
            else if (eth->eflags & ETH_FLAG_LINK_DOWN) link_str = "down";
            else link_str = "unknown";

            if (eth->eflags & ETH_FLAG_LINK_CABLE) cable_str = "yes";
            else cable_str = "no";

            if (eth->eflags & ETH_FLAG_DUPLEX_HALF) duplex_str = "half";
            else if (eth->eflags & ETH_FLAG_DUPLEX_FULL) duplex_str = "full";
            else duplex_str = "unknown";

            if (eth->eflags & ETH_FLAG_SPEED_10) speed_str = "10";
            else if (eth->eflags & ETH_FLAG_SPEED_100) speed_str = "100";
            else if (eth->eflags & ETH_FLAG_SPEED_1000) speed_str = "1000";
            else speed_str = "unknown";

            if (eth->eflags & ETH_FLAG_AUTONEG_OFF) autoneg_str = "off";
            else if (eth->eflags & ETH_FLAG_AUTONEG_ON) autoneg_str = "on";
            else autoneg_str = "unknown";

            if (eth->eflags & ETH_FLAG_HWADDR) {
               snprintf(mac_str, sizeof(mac_str), "%02x:%02x:%02x:%02x:%02x:%02x",
                            (uint8_t)eth->hw_addr.sa_data[0],
                            (uint8_t)eth->hw_addr.sa_data[1],
                            (uint8_t)eth->hw_addr.sa_data[2],
                            (uint8_t)eth->hw_addr.sa_data[3],
                            (uint8_t)eth->hw_addr.sa_data[4],
                            (uint8_t)eth->hw_addr.sa_data[5]);
            } else {
               strcpy(mac_str, "unknown");
            }
            answerf("name=%s addr=%s link=%s cable=%s duplex=%s speed=%s autoneg=%s",
                eth->name, mac_str, link_str, cable_str, duplex_str, speed_str, autoneg_str);
         }
#endif // ETHERNET_USED
         break ;

      case DINFO_CONTROLLER:
      case DINFO_LIBDANCE:
      case DINFO_FPGA:
         {
            const char* ptr = (what == DINFO_CONTROLLER)? danceapp_ident : libdance_ident;
#define FPGAINFO_SIZE 512
            uint8_t fpgainfo[FPGAINFO_SIZE*gdance_extdevdesc->n_entries];
            const char* beg = NULL;
            char   c;
            int    len, idx;

            if(what == DINFO_FPGA) {
               uint8_t nfpga = 0;
               char* ptmpchar = (char *)fpgainfo;
               ptr = (char *)fpgainfo;
               size_t nsize = 0;

               for(idx = 0; idx < gdance_extdevdesc->n_entries; idx++) {
                  danceextdev_t* extdev = DANCE_EXTDEV_PT(idx);
                  dancebus_t*    pbus = extdev->buspt;
                  if (pbus->bustype == _PCIE_BUS
                     && (extdev->devflags & _IDENT) == _IDENT) {
                     nfpga++;
                     ptmpchar += snprintf(ptmpchar, 20, "$Index : %d $\n", nfpga);
                     nsize = get_fpga_identification(pbus->ident, extdev->baseaddr, extdev->address, (uint8_t*)ptmpchar, FPGAINFO_SIZE);
                     if(nsize <= 0) {
                        ptmpchar += snprintf(ptmpchar, 20, "$N/A $\n");
                     } else {
                        ptmpchar += nsize;
                     }
                  }
               }
               if (!nfpga)
                  ptmpchar += snprintf(ptmpchar, 20, "$N/A $\n");
            }
#undef FPGAINFO_SIZE
            if (prompted) answerf("\n");
            while((c = *ptr++) != 0) {
               if (c == '$' && beg == NULL) {
                  beg = ptr;
               } else if (c == ' ' && *ptr == '$') {
                  len = (ptr++ - beg) - 1;
                  answerf("%s%.*s\n", prompted? "   ":"", len, beg);
                  beg = NULL;
               }
            }
            prompted = 0;
         }
         break ;
      case DINFO_JTAG:
         {
            if (prompted) answerf("\n");
            answer_jtag_chain_id(prompted);
            prompted = 0;
         }
         break;
      default:
         SYSTEM_ERRORF("invalid dinfo key");
         break ;
   }
   if (prompted) answerf("\n");
}


void commexec_qAPPNAME(void) {
   answer_dinfo(NULL, DINFO_APPNAME);
}


void commexec_qVERSION(void) {
   answer_dinfo(NULL, DINFO_VERSION);
}


void commexec_qADDR(void) {
   answer_dinfo(NULL, DINFO_ADDR);
}


void commexec_qDINFO(void) {
   id_choice_t c = I_CHOICE();

   if (c == DEFAULT_CHOICE) {
      for (c = 0; c != commlst_DINFO.n; ++c)
         answer_dinfo(commlst_DINFO.options[c], c);
   } else
      answer_dinfo(NULL, c);
}
