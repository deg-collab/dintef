#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/fcntl.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <pthread.h>

#include "runtime.h"
#include "comm.h"
#include "log.h"
#include "conf.h"
#include "daq.h"


// -------------------- Buffer handling

void show_read_buf(read_buf_t* rbuf)
{
   size_t i;
   size_t len =  (rbuf->pos - rbuf->buf) + rbuf->used;

   if (len == 0) {
      LOG_COMM("<empty>\n");
      return;
   }

   for (i = 0; i < len; ++i)
      LOG_COMM("%2$*1$zd - 0x%3$02x %3$c\n", 5*(i >= (rbuf->pos - rbuf->buf)), i, rbuf->buf[i]);
   LOG_COMM("-------------\n");
}

void reset_read_buffer(read_buf_t* rbuf, size_t used)
{
   if (used) memmove(rbuf->buf, rbuf->pos, used);
   rbuf->pos = rbuf->buf;
   rbuf->used = used;
}

void update_read_buffer(uint32_t ncomms, read_buf_t* rbuf, uint8_t** rdata, size_t* rsize)
{
   uint8_t* data = rbuf->pos + rbuf->used;
   size_t   size = READBUF_SIZE - (data - rbuf->buf);
   // show_read_buf(rbuf);
   if (ncomms == 0)
   {
      if (rbuf->pos != rbuf->buf)
      {
         reset_read_buffer(rbuf, rbuf->used);
         data = rbuf->buf + rbuf->used;
         size = READBUF_SIZE - rbuf->used;
      }
      else if (size == 0)   // and rbuf->pos == rbuf->buf
      {
         // Buffer overflow, this should not happen ...
         SYSTEM_ERROR();
         rbuf->used = 0;
         data = rbuf->pos;
         size = READBUF_SIZE - (data - rbuf->buf);
      }
   }
   *rdata = data;
   *rsize = size;
   // show_read_buf(rbuf);
}

int consume_read_buffer(read_buf_t* rbuf, size_t nbytes, uint8_t* data)
{
   if (data) memcpy(data, rbuf->pos, nbytes);
   rbuf->pos += nbytes;
   rbuf->used -= nbytes;
   return 0;
}


static channel_t* create_channel(io_handle_t* io)
{
   channel_t* new_chan;
   dstream_desc_t desc;

   new_chan = malloc(sizeof(channel_t));
   if (new_chan == NULL) goto on_error_0;

   /* create and subscribe to private default streams */

   dstream_init_desc(&desc);
   desc.id = DSTREAM_ID_ASCII;
   desc.flags = DSTREAM_FLAG_LOCAL;
   new_chan->ascii_dstream = dstream_create(&desc);
   if (new_chan->ascii_dstream == NULL) goto on_error_1;
   if (dstream_add_subscriber(new_chan->ascii_dstream, new_chan)) goto on_error_2;

   dstream_init_desc(&desc);
   desc.id = DSTREAM_ID_BINARY;
   desc.flags = DSTREAM_FLAG_LOCAL;
   new_chan->binary_dstream = dstream_create(&desc);
   if (new_chan->binary_dstream == NULL) goto on_error_2;
   if (dstream_add_subscriber(new_chan->binary_dstream, new_chan)) goto on_error_3;

   new_chan->io = io;
   new_chan->http_info = NULL;
   new_chan->address = g_libdance.addr;
   new_chan->address_sz = strlen(new_chan->address);
   new_chan->read_buffer.used = 0;
   reset_read_buffer(&new_chan->read_buffer, 0);
   new_chan->block_comm = NULL;
   new_chan->queued_comms = 0;
   lnklist_init(&new_chan->out_cbufflist, NULL);
   new_chan->curr_cbufflist = NULL;

   return new_chan;

 on_error_3:
   dstream_destroy(new_chan->binary_dstream);
 on_error_2:
   dstream_destroy(new_chan->ascii_dstream);
 on_error_1:
   free(new_chan);
 on_error_0:
   return NULL;
}

static void destroy_channel(channel_t* chan)
{
   dstream_del_subscriber(NULL, chan);
   free(chan);
}

//-------------------------- socket management

static int open_nonblock_socket
(
 struct sockaddr_storage saddr_both[2], size_t size_both[2],
 int type, int proto,
 struct sockaddr_storage** saddr_used, size_t* size_used
)
{
  size_t i;
  int fd;

  for (i = 0; (i != 2) && (size_both[i]); ++i)
  {
    const struct sockaddr* const sa = (const struct sockaddr*)&saddr_both[i];
    fd = socket(sa->sa_family, type, proto);
    if (fd != -1) break ;
  }

  if ((i == 2) || (size_both[i] == 0))
  {
    SYSTEM_ERROR();
    return -1;
  }

  *saddr_used = &saddr_both[i];
  *size_used = size_both[i];

  if (fcntl(fd, F_SETFL, O_NONBLOCK))
  {
    SYSTEM_ERROR();
    close(fd);
    return -1;
  }

  if (fcntl(fd, F_SETFD, FD_CLOEXEC))
  {
    SYSTEM_ERROR();
    close(fd);
    return -1;
  }

  return fd;
}

static void close_tcp_socket(int fd)
{
   shutdown(fd, SHUT_RDWR);
   close(fd);
}

static int resolve_ip_addr
(
 struct sockaddr_storage saddr_both[2], size_t size_both[2],
 const char* addr, const char* port
)
{
   struct addrinfo  ai;
   struct addrinfo* aip = NULL;
   struct addrinfo* saved_aip = NULL;
   int              err = -1;
   size_t i;

   memset(&ai, 0, sizeof(ai));
   ai.ai_family = AF_UNSPEC;
   ai.ai_socktype = SOCK_STREAM;
   ai.ai_flags = AI_PASSIVE;

   if (getaddrinfo(addr, port, &ai, &aip))
   {
      SYSTEM_ERROR();
      goto on_error;
   }
   saved_aip = aip;

   size_both[0] = 0;
   size_both[1] = 0;
   i = 0;
   for (; (i != 2) && (aip != NULL); aip = aip->ai_next)
   {
     if ((aip->ai_family != AF_INET) && (aip->ai_family != AF_INET6)) continue ;
     if (aip->ai_addrlen == 0) continue ;
     memcpy(&saddr_both[i], aip->ai_addr, aip->ai_addrlen);
     size_both[i] = aip->ai_addrlen;
     ++i;
   }

   if (i == 0)
   {
      SYSTEM_ERROR();
      goto on_error;
   }

   err = 0;   // success
 on_error:
   if (saved_aip != NULL) freeaddrinfo(saved_aip);
   return err;
}


danceerr_t channel_readbin(channel_t* ch, uint8_t* data, size_t size, int timeout)
{
   // WARNING: if we get here channel reading has been blocked in the runtime loop

   // timeout in seconds
   // return 0 for success if n bytes read. -1 if timeout or error.

   io_handle_t* const io = ch->io;
   struct timeval tmout;
   fd_set rset;
   ssize_t nrecv;
   size_t off;

   /* first read from internal buffer */
   off = ch->read_buffer.used;
   if (off)
   {
      if (off > size) off = size;
      consume_read_buffer(&ch->read_buffer, off, data);
   }

   clr_nonblock(io->fd);

   while (off < size)
   {
      tmout.tv_sec = (unsigned long)timeout;
      tmout.tv_usec = 0;

      FD_ZERO(&rset);
      FD_SET(io->fd, &rset);

      if (select(io->fd + 1, &rset, NULL, NULL, &tmout) <= 0)
      {
         SYSTEM_ERROR();
         goto on_error;
      }

      nrecv = recv(io->fd, data + off, size - off, 0);
      if (nrecv <= 0)
      {
         SYSTEM_ERROR();
         goto on_error;
      }
      off += (size_t)nrecv;
   }
   set_nonblock(io->fd);
   return DANCE_OK;

 on_error:
   set_nonblock(io->fd);
   return DANCE_ERROR;
}

void request_channel_close(channel_t* channel)
{
//   if (channel->type == IOTYPE_CHANTCP)
      channel->io->flags |= IOFLAG_CLOSE;
}



void channel_send_bufferlist(cbuflist_t* cbufflist)
{
   // initialising list.lstdata to not NULL signals the
   //    runtime loop that the buffer is ready to transmit
   __sync_synchronize();
   *((void* volatile *)&cbufflist->lstdata) = cbufflist;
}

danceerr_t init_answer_buffers(channel_t* channel, command_t *comm)
{
   if (!IS_ANSWER_COMMAND(comm))
   {
      comm->main_prefix = NULL;
      comm->main_answ = NULL;
      return DANCE_OK;
   }

   comm->main_prefix = dblock_create(NULL, NULL, 0, MEM_DEFAULT);
   if (comm->main_prefix == NULL)
      return DANCE_ERROR;

   comm->main_answ = dblock_create(NULL, NULL, 0, MEM_DEFAULT);
   if (comm->main_answ == NULL)
      return DANCE_ERROR;

   comm->answ_list = combuflist_create(NULL, comm->main_prefix);
   if (comm->answ_list == NULL) {
      SYSTEM_ERROR();
      dblock_release(comm->main_prefix, DESTROY_DEFAULT);
      dblock_release(comm->main_answ, DESTROY_DEFAULT);
      comm->main_prefix = NULL;
      comm->main_answ = NULL;
      return DANCE_ERROR;
   }
   combuflist_append(comm->answ_list, comm->main_answ);

   pthread_mutex_lock(&channel->io->wlock);
   if (channel->curr_cbufflist == NULL)
      channel->curr_cbufflist = comm->answ_list;
   else
      lnklist_add_tail(&channel->out_cbufflist, comm->answ_list);
   pthread_mutex_unlock(&channel->io->wlock);
   return DANCE_OK;
}

void free_answer_buffers(channel_t* channel)
{
   lnklist_item_t* item = channel->out_cbufflist.head;

   pthread_mutex_lock(&channel->io->wlock);

   while (item != NULL)
   {
      lnklist_item_t* next_item = item->next;
      combuflist_free(item->itmdata);
      item = next_item;
   }

   if (channel->curr_cbufflist != NULL)
      combuflist_free(channel->curr_cbufflist);

   pthread_mutex_unlock(&channel->io->wlock);
}

void block_read_channel(channel_t* channel, void* block_comm)
{
   block_io_read(channel->io);   //io_info.io->flags |= IOFLAG_BLOCKED;
   channel->block_comm = block_comm;
}

void unblock_read_channel(channel_t* channel, void* block_comm)
{
   if (channel->block_comm == block_comm) {
      LOG_COMM("Unblocking channel read: %p\n", channel);
      channel->block_comm = NULL;
      unblock_io_read(channel->io);

      // commands may be in the read buffer after the binary contents. we set the io
      // as readable so that the main loop does not get stuck in the select
      if (channel->read_buffer.used)
         channel->io->flags |= IOFLAG_READ;
   }
}

#if APPCONF_USED

static unsigned int is_config_commit_required(command_t* comm) {
   if (appconf_mode == CONF_MODE_ALL_COMMS) {
      return 1;
   } else if (appconf_mode == CONF_MODE_SKIP_QUERIES) {
      if ((comm->flags & CMDFLG_QUERY) == 0)
         return 1;
      else if (comm->flags & CMDFLG_COMMITCONF)
         return 1;
   } else if (appconf_mode == CONF_MODE_EXPLICIT_ONLY) {
      if (comm->flags & CMDFLG_COMMITCONF)
         return 1;
   }
   return 0;
}

static void do_config_commit(void) {
   /* a commit is wanted. if the timer is immediate (0), do the */
   /* commit now. otherwise, schedule or update a oneshot timer. */
   if (appconf_timer == 0) {
      conf_commit();
   } else if (appconf_timer != (uint32_t)-1) {
      conf_restart_timer(appconf_timer);
   }
}

#endif // APPCONF_USED


void do_command_cleanup(command_t* comm)
{
   channel_t* channel = comm->channel;

   --channel->queued_comms;
   LOG_COMM("Do command cleanup: \"%.*s\"\n", comm->keyword_sz, comm->keyword);
   LOG_COMM("Remaining comms: %d\n", channel->queued_comms);

#if APPCONF_USED
   /* configuration commit logic */
   /* this is done here to avoid any concurrency issue */
   /* as regard with the runtime. some latency induced */
   /* but overall advantages. */
   if (is_config_commit_required(comm))
      do_config_commit();
#endif // APPCONF_USED

   if (IS_BINARY_COMMAND(comm))
      unblock_read_channel(channel, comm);

   free(comm);
}


void process_read_channel(channel_t* channel)
{
   read_buf_t* readbuf = &channel->read_buffer;

   // consume all the commands
   while (readbuf->used)
   {
      command_t* comm;
      size_t     line_sz = extract_command(channel, (char *)readbuf->pos, readbuf->used, &comm);

      if (line_sz == 0)
         break;

      LOG_CMD("%.*s\n", line_sz, (const char*)readbuf->pos);

      // consume regardless command is ok or not
      consume_read_buffer(readbuf, line_sz, NULL);

      if (comm == NULL)  // badly formed command
         continue;

      ++channel->queued_comms;

      // save the dstream state BEFORE command execution
      if (comm->flags & (CMDFLG_ACKNOWLEDGE | CMDFLG_BINARY | CMDFLG_QUERY))
      {
         dstream_handle_t* dstream;
         if (comm->flags & CMDFLG_BINARY)
            dstream = comm->channel->binary_dstream;
         else
            dstream = comm->channel->ascii_dstream;

         if (dstream_is_enabled(dstream)) comm->flags |= CMDFLG_DSTREAM;
      }

      if (IS_BINARY_COMMAND(comm))
         block_read_channel(channel, comm);

      if (init_answer_buffers(channel, comm) != DANCE_OK)
      {
         SYSTEM_ERROR();
         --channel->queued_comms;
         unblock_read_channel(channel, comm);
         free(comm);
         continue;
      }

      if (IS_VALID_COMMAND(comm) && IS_CONCURRENT_COMMAND(comm))
      {
         complete_command(comm);
         do_command_cleanup(comm);
      }
      else
      {
         if (relay_commexecution(complete_command, comm, TASK_NORMAL) != DANCE_OK)
         {
            SYSTEM_ERROR();
            free_answer_buffers(channel);
            do_command_cleanup(comm);
         }

         // do_command_cleanup() is always called after task completion signal

         if (IS_BINARY_COMMAND(comm)) return ;
      }
   }
}

// channel io basic operations

static int channel_io_read(io_handle_t* io)
{
   read_buf_t* rbuf = &(IOPTR_TO_CHANNEL(io)->read_buffer);
   uint32_t    ncomms = IOPTR_TO_CHANNEL(io)->queued_comms;
   uint8_t*    rdata;
   size_t      rsize;
   ssize_t     nrecv;

   update_read_buffer(ncomms, rbuf, &rdata, &rsize);

   if (rsize == 0)  // if buffer full, skip read
      return 0;

   {
      errno = 0;
      nrecv = recv(io->fd, rdata, rsize, 0);
      if (nrecv <= 0)
      {
         if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
            return 0;  // not an error
         else if (nrecv == 0)
         {
            /* WARNING: wait for ncomms to be 0 otherwise */
            /* the runtime will block in channel_io_close */
            /* as he is only the one decreasing the count */
            if (ncomms == 0) io->flags |= IOFLAG_CLOSE;
              return 0;
         }
         else
         {
            SYSTEM_ERROR();
            return -1;
         }
      }
      rbuf->used += nrecv;
      io->flags |= IOFLAG_READ;
   }
   return 0;
}


static int channel_io_write(io_handle_t* io)
{
   channel_t* chan = IOPTR_TO_CHANNEL(io);
   dblock_t*  dblk;
   int        err = -1;

   while ((dblk = chan->curr_cbufflist->head->itmdata) != NULL)
   {
      size_t     remain = dblock_used(dblk);
      size_t     off = 0;
      ssize_t    nsent;

#if 0 // uncomment for high bandwidth
      while (off != remain)
#endif
      {
         errno = 0;
         nsent = send(io->fd, dblock_beg(dblk) + off, remain - off, 0);
         if (nsent <= 0)
         {
            if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
            {
               // not an error
               if (off > 0 && dblock_seek(dblk, off) != off)
               {
                  SYSTEM_ERROR();
                  goto on_error;
               }
               else
                  break;  // exit send loop
            }
            else
            {
               SYSTEM_ERROR();
               goto on_error;
            }
         }
         off += (size_t)nsent;
      }
      if (dblock_seek(dblk, off) != off)
      {
         SYSTEM_ERROR();
         goto on_error;
      }
      if (dblock_used(dblk) != 0)
         goto on_success;
      dblock_release(dblk, DESTROY_DEFAULT);
      if (chan->curr_cbufflist->head == NULL)
         goto on_success;
   }

 on_success:
   err = 0;
 on_error:
   return err;
}


static int channel_io_close(io_handle_t* io)
{
   channel_t*      channel = IOPTR_TO_CHANNEL(io);

   close_tcp_socket(io->fd);

   // Wait command execution fully completes in other threads
   while (channel->queued_comms)
      usleep(100000);

   free_answer_buffers(channel);

   destroy_channel(channel);

   return 0;
}

static const io_ops_t channel_io_ops =
{
   .write_fn = channel_io_write,
   .read_fn =  channel_io_read,
   .close_fn = channel_io_close
};


//  ----- tcp server operations

static int tcp_server_write(io_handle_t* io)
{
   /* should not occur */
   SYSTEM_ERROR();
   return -1;
}

static int tcp_server_read(io_handle_t* io)
{
   int          enable = 1;
   int          new_fd;
   io_handle_t* new_io;
   const io_ops_t* const chan_ops = (io_ops_t*)io->udata;

   errno = 0;
   new_fd = accept(io->fd, NULL, NULL);
   if (new_fd == -1)
   {
      if ((errno == EWOULDBLOCK) || (errno == EAGAIN))
      {
         /* not an error */
         return 0;
      }
      SYSTEM_ERROR();
      goto on_error_0;
   }

   if (setsockopt(new_fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1)
   {
      SYSTEM_ERROR();
      goto on_error_1;
   }

   if (setsockopt(new_fd, SOL_TCP, TCP_NODELAY, (char*)&enable, sizeof(int)))
   {
      /* not an error */
      /* SYSTEM_ERROR(); */
      /* goto on_error_1; */
   }

   if (fcntl(new_fd, F_SETFL, O_NONBLOCK))
   {
      SYSTEM_ERROR();
      goto on_error_1;
   }

   if (fcntl(new_fd, F_SETFD, FD_CLOEXEC))
   {
      SYSTEM_ERROR();
      goto on_error_1;
   }

   /* create a new client */
   new_io = create_io_handle(new_fd, IOTYPE_CHANNEL, chan_ops, NULL);
   if (new_io == NULL)
   {
      SYSTEM_ERROR();
      goto on_error_1;
   }

   new_io->udata = create_channel(new_io);
   if (new_io->udata == NULL)
   {
      SYSTEM_ERROR();
      goto on_error_2;
   }
   new_io->flags |= IOFLAG_OPEN;
   return 0;

 on_error_2:
   free_io_handle(new_io);
 on_error_1:
   close_tcp_socket(new_fd);
 on_error_0:
   return -1;
}

static int tcp_server_close(io_handle_t* io)
{
  close_tcp_socket(io->fd);
  return 0;
}

static const io_ops_t tcp_server_ops =
{
  .write_fn = tcp_server_write,
  .read_fn = tcp_server_read,
  .close_fn = tcp_server_close
};

danceerr_t open_tcp_server
(
 io_handle_t** iop,
 const char* laddr, const char* lport,
 const io_ops_t* chan_ops
)
{
   // laddr,lport the local address to bind on, or NULL

   /* the resolver may use both ipv4 and ipv6, no matter what */
   /* the still to be bound network interface stack actually */
   /* supports. thus, we test both families */

   struct sockaddr_storage saddr_both[2];
   struct sockaddr_storage* saddr_used;
   size_t           size_both[2];
   size_t           size_used;
   io_handle_t*     io;
   int              enable = 1;
   int              fd;

   if (resolve_ip_addr(saddr_both, size_both, laddr, lport))
   {
      SYSTEM_ERROR();
      goto on_error_0;
   }

   fd = open_nonblock_socket
     (saddr_both, size_both, SOCK_STREAM, IPPROTO_TCP, &saddr_used, &size_used);
   if (fd == -1)
   {
      SYSTEM_ERROR();
      goto on_error_0;
   }

   if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == -1)
   {
      SYSTEM_ERROR();
      goto on_error_1;
   }

   if (bind(fd, (const struct sockaddr*)saddr_used, (socklen_t)size_used))
   {
      SYSTEM_ERROR();
      goto on_error_1;
   }

   if (listen(fd, 5))
   {
      SYSTEM_ERROR();
      goto on_error_1;
   }

   io = create_io_handle(fd, IOTYPE_SERVERTCP, &tcp_server_ops, (void*)chan_ops);

   if (!io)
   {
      SYSTEM_ERROR();
      goto on_error_1;
   }

   *iop = io;
   return DANCE_OK;

 on_error_1:
   close_tcp_socket(fd);
 on_error_0:
   return DANCE_ERROR;
}

danceerr_t tcp_init(const char* lport)
{
   io_handle_t*       server_io;

   if (lport == NULL) lport = "5000";

   if (open_tcp_server(&server_io, NULL, lport, &channel_io_ops) != DANCE_OK)
   {
      LOG_ERROR("open_tcp_server() failure\n");
      return DANCE_ERROR;
   }
   return DANCE_OK;
}


/* http server */

typedef struct http_string_s
{
  char* s;
  size_t n;
} http_string_t;

#define HTTP_STRING_INIT(__s) { __s, sizeof(__s) - 1 }
static const http_string_t http_string_slash_term = HTTP_STRING_INIT("/term");
static http_string_t http_string_root_dir;

static void http_string_init(http_string_t* hs, char* s, size_t n)
{
  hs->s = s;
  hs->n = n;
}

static void http_string_init2(http_string_t* a, const http_string_t* b)
{
  http_string_init(a, b->s, b->n);
}

static void http_string_init3(http_string_t* a, const char* b)
{
  http_string_init(a, (char*)b, strlen(b));
}

static int http_string_cmp(const http_string_t* a, const http_string_t* b)
{
  /* return 0 if exact match in both size and contents */
  if (a->n != b->n) return -1;
  return memcmp(a->s, b->s, a->n);
}

static int http_string_cmp2(const http_string_t* a, const http_string_t* b)
{
  /* return 0 if contents match. a size may be greater. */
  if (a->n < b->n) return -1;
  return memcmp(a->s, b->s, b->n);
}

static size_t http_string_rfind_char(const http_string_t* a, char c)
{
  size_t i;
  for (i = a->n; i; --i)
  {
    if (a->s[i - 1] == c) break ;
  }
  if (i == 0) return (size_t)-1;
  return i - 1;
}

static size_t http_string_find(const http_string_t* a, const http_string_t* b)
{
  size_t i;

  if (a->n < b->n) return (size_t)-1;

  for (i = 0; (i + b->n) <= a->n; ++i)
  {
    if (memcmp(a->s + i, b->s, b->n) == 0) return i;
  }

  return (size_t)-1;
}

static size_t http_string_find_char(const http_string_t* a, char c)
{
  http_string_t b;
  http_string_init(&b, &c, 1);
  return http_string_find(a, &b);
}

static void http_string_seek(http_string_t* s, size_t n)
{
  s->s += n;
  s->n -= n;
}

static void http_string_skip(http_string_t* a, const http_string_t* b)
{
  while (http_string_cmp2(a, b) == 0) http_string_seek(a, b->n);
}

static void http_string_skip_char(http_string_t* a, char c)
{
  http_string_t b;
  http_string_init(&b, &c, 1);
  http_string_skip(a, &b);
}

__attribute__((unused))
static const char* http_string_to_cstr(const http_string_t* s)
{
  static char buf[64];
  size_t n;
  size_t i;

  if (s->n >= sizeof(buf)) n = sizeof(buf) - 1;
  else n = s->n;

  for (i = 0; i != n; ++i) buf[i] = s->s[i];
  buf[i] = 0;

  return buf;
}

static dblock_t* http_string_to_dblock(const http_string_t* s)
{
  dblock_t* page;

  page = dblock_create(NULL, NULL, s->n, MEM_DEFAULT);
  if (page == NULL) return NULL;

  memcpy((uint8_t*)dblock_beg(page), s->s, s->n);
  dblock_setused(page, s->n);

  return page;
}

static void http_string_cat(http_string_t* a, const http_string_t* b)
{
  memcpy(a->s + a->n, b->s, b->n);
  a->n += b->n;
}

static uint8_t http_char_to_hex(char c)
{
  if ((c >= 'A') && (c <= 'F')) return (uint8_t)(0xa + c - 'A');
  else if ((c >= '0') && (c <= '9')) return (uint8_t)(c - '0');
  return 0;
}

static void http_decode_uri(http_string_t* uri)
{
  size_t i;
  size_t j;
  size_t k;

  j = 0;
  for (i = 0; i != uri->n; i += k)
  {
    char c;

    if (uri->s[i] == '%')
    {
      uint8_t x;
      k = uri->n - i;
      if (k < 3) goto on_normal_char;
      x = http_char_to_hex(uri->s[i + 1]) << 4;
      x |= http_char_to_hex(uri->s[i + 2]) << 0;
      c = (char)x;
      k = 3;
    }
    else if (uri->s[i] == '+')
    {
      c = ' ';
      k = 1;
    }
    else
    {
    on_normal_char:
      c = uri->s[i];
      k = 1;
    }

    uri->s[j++] = c;
  }

  uri->n = j;
}


typedef struct http_info_s
{
#define HTTP_FLAG_LOCK (1 << 0)
#define HTTP_FLAG_RQST_ERROR (1 << 1)
#define HTTP_FLAG_RESP_HEADER (1 << 2)
#define HTTP_FLAG_STATUS_CODE (1 << 3)
#define HTTP_FLAG_RESP_DATA (1 << 4)
  uint32_t flags;

  size_t req_size;

  /* parsed contents */
  http_string_t meth;
  http_string_t uri;
  http_string_t path;
  http_string_t query;
  http_string_t vers;

  unsigned int status_code;
  const char* content_type;

  dblock_t* data_header;
  dblock_t* data_footer;

} http_info_t;

static void http_set_status_code(http_info_t* hi, unsigned int n)
{
  hi->status_code = n;
  hi->flags |= HTTP_FLAG_STATUS_CODE;
}

static int http_parse_request(http_info_t* hi, const http_string_t* req)
{
  /* return 0 if query parsed, with successfully or not */
  /* https://www.w3.org/Protocols/rfc2616/rfc2616-sec5.html */

  static const http_string_t eol = HTTP_STRING_INIT("\r\n");
  static const http_string_t eor = HTTP_STRING_INIT("\r\n\r\n");

  int err = -1;
  http_string_t s;
  size_t i;

  /* assume the requests are sent in series */
  i = http_string_find(req, &eor);
  if (i == (size_t)-1) return -1;

  hi->req_size = i + eor.n;
  http_string_init(&s, req->s, hi->req_size);

  /* full request, return 0 from here */
  /* use flag to signal error */

  /* method sp request_uri sp http_version crlf */

  i = http_string_find_char(&s, ' ');
  if (i == (size_t)-1) goto on_parse_error;
  http_string_init(&hi->meth, s.s, i);
  http_string_seek(&s, i);

  http_string_skip_char(&s, ' ');
  i = http_string_find_char(&s, ' ');
  http_string_init(&hi->uri, s.s, i);
  http_string_seek(&s, i);

  /* decode uri */
  http_decode_uri(&hi->uri);

  i = http_string_find_char(&hi->uri, '?');
  if (i == (size_t)-1)
  {
    http_string_init(&hi->path, hi->uri.s, hi->uri.n);
    http_string_init(&hi->query, NULL, 0);
  }
  else
  {
    const size_t k = i + 1;
    http_string_init(&hi->path, hi->uri.s, i);
    http_string_init(&hi->query, hi->uri.s + k, hi->uri.n - k);
  }

  http_string_skip_char(&s, ' ');
  i = http_string_find(&s, &eol);
  if (i == (size_t)-1) goto on_parse_error;
  http_string_init(&hi->vers, s.s, i);
  http_string_seek(&s, i);

  http_string_seek(&s, eol.n);

  /* parsing success */
  err = 0;

 on_parse_error:
  if (err == 0) hi->flags &= ~HTTP_FLAG_RQST_ERROR;
  else hi->flags |= HTTP_FLAG_RQST_ERROR;
  return 0;
}

#include <fcntl.h>
#include <sys/stat.h>

static dblock_t* http_read_file(const http_string_t* path)
{
  int err = -1;
  int fd;
  dblock_t* page;
  struct stat st;
  size_t size;
  uint8_t* data;

  fd = open(path->s, O_RDONLY);
  if (fd == -1) goto on_error_0;

  if (fstat(fd, &st)) goto on_error_1;

  size = st.st_size;
  page = dblock_create(NULL, NULL, size, MEM_DEFAULT);
  if (page == NULL) goto on_error_1;
  dblock_setused(page, size);

  data = (uint8_t*)dblock_beg(page);
  while (size)
  {
    const size_t n = (size_t)read(fd, data, size);
    if (n <= 0) goto on_error_2;
    data += n;
    size -= n;
  }

  /* success */
  err = 0;

 on_error_2:
  if (err) dblock_release(page, DESTROY_DEFAULT);
 on_error_1:
  close(fd);
 on_error_0:
  if (err) page = NULL;
  return page;
}

static dblock_t* http_serve_error_page(http_info_t* hi)
{
  http_string_t s;
  size_t size;
  char buf[32];

  hi->content_type = "text/plain";

  size = snprintf(buf, sizeof(buf), "error: %u\n", hi->status_code);
  http_string_init(&s, buf, size);

  return http_string_to_dblock(&s);
}

static int http_serve_static_page(http_info_t* hi, io_handle_t* io)
{
  static const http_string_t dot_html = HTTP_STRING_INIT(".html");
  static const http_string_t dot_htm = HTTP_STRING_INIT(".htm");
  static const http_string_t dot_js = HTTP_STRING_INIT(".js");

  channel_t* const chan = IOPTR_TO_CHANNEL(io);
  dblock_t* page = NULL;
  char buf[64];
  http_string_t path;
  http_string_t ext;
  size_t i;

  if (hi->flags & HTTP_FLAG_STATUS_CODE)
  {
    goto serve_error;
  }

  if (http_string_root_dir.n == 0)
  {
    http_set_status_code(hi, 404);
    goto serve_error;
  }

  if ((hi->path.n + http_string_root_dir.n + 1) > sizeof(buf))
  {
    http_set_status_code(hi, 500);
    goto serve_error;
  }

  http_string_init(&path, buf, 0);
  http_string_cat(&path, &http_string_root_dir);
  http_string_cat(&path, &hi->path);
  buf[path.n] = 0;

  page = http_read_file(&path);
  if (page == NULL)
  {
    http_set_status_code(hi, 404);
    goto serve_error;
  }

  http_set_status_code(hi, 200);

  /* associate extension */
  i = http_string_rfind_char(&hi->path, '.');
  if (i == (size_t)-1) goto on_default_ext;
  http_string_init(&ext, hi->path.s + i, hi->path.n - i);

  if (http_string_cmp(&ext, &dot_html) == 0)
  {
    goto on_html_ext;
  }
  else if (http_string_cmp(&ext, &dot_htm) == 0)
  {
  on_html_ext:
    hi->content_type = "text/html";
  }
  else if (http_string_cmp(&ext, &dot_js) == 0)
  {
    hi->content_type = "text/javascript";
  }
  else
  {
  on_default_ext:
    hi->content_type = "text/plain";
  }

  /* always send something or http_io_write wont be called */
 serve_error:
  if (chan->curr_cbufflist == NULL)
  {
    chan->curr_cbufflist = combuflist_create(NULL, NULL);
    if (chan->curr_cbufflist == NULL)
    {
      if (page != NULL) dblock_release(page, DESTROY_DEFAULT);
      return -1;
    }
  }

  if (page == NULL)
  {
    page = http_serve_error_page(hi);
    if (page == NULL) return -1;
  }

  combuflist_append(chan->curr_cbufflist, page);
  channel_send_bufferlist(chan->curr_cbufflist);

  /* must consume rbuf */
  consume_read_buffer(&chan->read_buffer, hi->req_size, NULL);

  return 0;
}

/* path redirection and checks */

static void http_redirect_path(http_info_t* hi)
{
  static const http_string_t slash = HTTP_STRING_INIT("/");

  if (http_string_cmp(&hi->path, &slash) == 0)
  {
    /* redirect to /term */
    http_string_init2(&hi->path, &http_string_slash_term);
  }
}

static int http_check_path(http_info_t* hi)
{
  static const http_string_t dotdot = HTTP_STRING_INIT("..");
  const http_string_t* const path = &hi->path;
  size_t i;

  /* the path can not contain to prevent root escaping */
  if (path->n >= dotdot.n)
  {
    for (i = 0; (path->n - i) >= dotdot.n; ++i)
    {
      if (memcmp(path->s + i, dotdot.s, dotdot.n) == 0) return -1;
    }
  }

  return 0;
}

/* minimal web terminal */

static const http_string_t http_term_header = HTTP_STRING_INIT
(
"<html>"
"<head>"
"<style type='text/css'>"
" body { background: #333333; }"
" #dterm {"
"  border: 0px solid #000000;"
"  padding-top: 10px;"
"  padding-bottom: 10px;"
"  padding-left: 15px;"
"  font-family: courier;"
"  color: #ddddff;"
"  background: #333333;"
" }"
" #dprompt { font-weight: bold; }"
" .dstdin {"
"  border: 0px;"
"  background: #ccccff;"
"  width: 80ch;"
" }"
" .dstdout {"
"  border: 0px;"
"  font-family: courier;"
"  font-size: 16px;"
" }"
"</style>"
"</head>"
"<body>"
"<div id='dterm'>"
"<div class='dstdout'> *** DAnCE web terminal *** <br><br> </div>"
"<form method='get' action='/term'>"
"<span id='dprompt'> $&gt; </span>"
"<input type='text' name='s' class='dstdin' autocomplete='off' autofocus />"
"</form>"
"<pre class='dstdout'>"
);

static const http_string_t http_term_footer = HTTP_STRING_INIT
(
"</pre>"
"</div>"
"</body>"
"</html>"
);

static int http_serve_term(http_info_t* hi, io_handle_t* io)
{
  /* TODO: handle errors properly */

  static const http_string_t seq = HTTP_STRING_INIT("s=");
  static const http_string_t ofmt = HTTP_STRING_INIT("&f=text");

  channel_t* const chan = IOPTR_TO_CHANNEL(io);
  read_buf_t* const rbuf = &chan->read_buffer;
  uint8_t* const pos = rbuf->pos;
  dblock_t* header;
  dblock_t* footer;
  dblock_t* body;
  size_t qn;
  size_t i;
  unsigned int will_answer;

  http_set_status_code(hi, 200);

  /* look and strip ofmt */

  i = http_string_find(&hi->query, &ofmt);
  if (i != (size_t)-1)
  {
    hi->query.n = i;
    hi->content_type = "text/plain";
    header = NULL;
    footer = NULL;
    body = NULL;
  }
  else
  {
    hi->content_type = "text/html";
    header = http_string_to_dblock(&http_term_header);
    footer = http_string_to_dblock(&http_term_footer);
    body = NULL;
  }

  if (http_string_cmp2(&hi->query, &seq))
  {
    /* no command found or invalid */
  invalid_command:

    if (chan->curr_cbufflist == NULL)
    {
      chan->curr_cbufflist = combuflist_create(NULL, NULL);
      if (chan->curr_cbufflist == NULL) return -1;
    }

    if (header != NULL) combuflist_append(chan->curr_cbufflist, header);
    if (body != NULL) combuflist_append(chan->curr_cbufflist, body);
    if (footer != NULL) combuflist_append(chan->curr_cbufflist, footer);
    channel_send_bufferlist(chan->curr_cbufflist);
    consume_read_buffer(rbuf, hi->req_size, NULL);

    goto skip_query;
  }

  /* dance binary commands not supported over http */

  will_answer = 0;
  for (i = seq.n; i != hi->query.n; ++i)
  {
    const char c = (char)hi->query.s[i];
    if ((c == '#') || (c == '?')) will_answer = 1;
    if ((c != ' ') && (c != '#') && (c != '?')) break ;
  }

  if ((i != hi->query.n) && (hi->query.s[i] == '*'))
  {
    static const http_string_t not_supp =
      HTTP_STRING_INIT("binary commands not supported\n");
    body = http_string_to_dblock(&not_supp);
    goto invalid_command;
  }

  /* sent by http_io_write. body written by command handler. */

  if ((header != NULL) || (footer != NULL))
  {
    hi->data_header = header;
    hi->data_footer = footer;
    hi->flags |= HTTP_FLAG_RESP_DATA;
  }

  /* turn query into dance command */
  /* force ack if no answer */

  if (will_answer == 0)
  {
    pos[0] = '#';
    i = 1;
  }
  else
  {
    i = 0;
  }

  qn = hi->query.n - seq.n + i;
  memcpy(pos + i, hi->query.s + seq.n, qn);
  if (qn >= sizeof(rbuf->buf)) qn = sizeof(rbuf->buf) - 1;
  pos[qn] = '\n';
  rbuf->used = qn + 1;

  /* turn on IOFLAG_READ to enable DAnCE command processing */
  /* set it on success, esp. can not be used for static page */

  io->flags |= IOFLAG_READ;

 skip_query:
  return 0;
}

static int http_io_read(io_handle_t* io)
{
  channel_t* const chan = IOPTR_TO_CHANNEL(io);
  read_buf_t* const rbuf = &chan->read_buffer;
  http_info_t* hi;
  http_string_t s;
  int err;

  /* alloc very first http request */
  if (chan->http_info == NULL)
  {
    chan->http_info = malloc(sizeof(http_info_t));
    if (chan->http_info == NULL) return -1;
    chan->http_info->flags = 0;
  }

  hi = chan->http_info;

  /* actual tcp socket read */
  err = channel_io_read(io);
  if (err) return err;

  /* cannot handle 2 http requests concurrently */
  if (hi->flags & HTTP_FLAG_LOCK) return 0;

  /* turn off read flag as we only want HTTP full request */
  io->flags &= ~IOFLAG_READ;

  /* parse request, wait for full one */
  http_string_init(&s, (char*)rbuf->pos, rbuf->used);
  if (http_parse_request(hi, &s)) return 0;

  /* reset flags. lock until sent. */
  hi->flags = HTTP_FLAG_LOCK;

  if ((hi->flags & HTTP_FLAG_RQST_ERROR))
  {
    http_set_status_code(hi, 400);
    goto on_error;
  }

#if 0
  {
    printf("NEW_REQUEST\n");
    printf(".uri  : %s\n", http_string_to_cstr(&hi->uri));
    printf(".path : %s\n", http_string_to_cstr(&hi->path));
    printf(".query: %s\n", http_string_to_cstr(&hi->query));
  }
#endif

  http_redirect_path(hi);

  if (http_check_path(hi))
  {
    http_set_status_code(hi, 404);
    goto on_error;
  }

  if (http_string_cmp(&hi->path, &http_string_slash_term) == 0)
  {
    if (http_serve_term(hi, io)) goto on_error;
  }
  else
  {
  on_error:
    if (http_serve_static_page(hi, io)) return -1;
  }

  return 0;
}

static int http_io_write(io_handle_t* io)
{
  channel_t* const chan = IOPTR_TO_CHANNEL(io);
  http_info_t* const hi = chan->http_info;

  if (hi == NULL) goto on_error;

  if (hi->flags & HTTP_FLAG_RESP_DATA)
  {
    if (hi->data_header != NULL)
    {
      combuflist_prepend(chan->curr_cbufflist, hi->data_header);
    }
    if (hi->data_footer != NULL)
    {
      combuflist_append(chan->curr_cbufflist, hi->data_footer);
    }
    hi->flags &= ~HTTP_FLAG_RESP_DATA;
  }

  if ((hi->flags & HTTP_FLAG_RESP_HEADER) == 0)
  {
    size_t header_len = 256;
    dblock_t* const body = chan->curr_cbufflist->head->itmdata;
    dblock_t* header;
    const char* reason_phrase;

    if (body == NULL) goto on_error;

    /* build header */
    header = dblock_create(NULL, NULL, header_len, MEM_DEFAULT);
    if (header == NULL) goto on_error;

    if ((hi->flags & HTTP_FLAG_STATUS_CODE) == 0)
    {
      http_set_status_code(hi, 400);
    }

    if (hi->status_code == 200) reason_phrase = "OK";
    else if (hi->status_code == 400) reason_phrase = "Bad Request";
    else if (hi->status_code == 404) reason_phrase = "Not Found";
    else reason_phrase = "Unknown";

    header_len = snprintf
    (
     dblock_beg(header), header_len,
     "HTTP/1.1 %u %s\r\n"
     "Content-Length: %zu\r\n"
     "Content-Type: %s\r\n"
     "\r\n",
     hi->status_code, reason_phrase,
     combuflist_size(chan->curr_cbufflist),
     hi->content_type
    );

    dblock_setused(header, header_len);

    /* prepend dblock header */
    if (combuflist_prepend(chan->curr_cbufflist, header)) goto on_error;

    hi->flags |= HTTP_FLAG_RESP_HEADER;
  }

  channel_io_write(io);

  if (chan->curr_cbufflist->head == NULL)
  {
    /* all sent, reset flags (unlock ...) */
    hi->flags = 0;
  }

  return 0;

 on_error:
  return -1;
}

static int http_io_close(io_handle_t* io)
{
  channel_t* const chan = IOPTR_TO_CHANNEL(io);
  if (chan->http_info != NULL) free(chan->http_info);
  return channel_io_close(io);
}

static const io_ops_t http_channel_ops =
{
  .write_fn = http_io_write,
  .read_fn = http_io_read,
  .close_fn = http_io_close
};

danceerr_t http_init(void)
{
   /* root_dir the root directory or NULL */
   /* TODO: slash_redir where to redirect slash url */

   io_handle_t*       server_io;

#undef HTTPCOMM
#define HTTPCOMM(port, flags) #port
   char * lport = "" COMMDEVICE_LIST;
#undef HTTPCOMM
   char* root_dir = "";

   if (g_libdance.httpport)
      lport = g_libdance.httpport;

   http_string_init3(&http_string_root_dir, root_dir);

   LOG_INFO("HTTP server using port %s\n", lport);
   if (open_tcp_server(&server_io, NULL, lport, &http_channel_ops) != DANCE_OK)
   {
      LOG_ERROR("open_tcp_server() failure\n");
      return DANCE_ERROR;
   }
   return DANCE_OK;
}
