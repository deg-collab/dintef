#include <stdint.h>
#include <stdlib.h>

#include "libdance.h"


static struct
{
   binary_t binfo;
   int      failed;
   int      query;
   uint32_t my_checksum;
} last_binfo;


void commexec_bBTEST(void)
{
   uint32_t  ext_chksum = I_INTEGER();
   binary_t* binfo;
   int       unit;
   int       size;
   char*     ptr;

   binfo = get_binary_com_info();

   unit = binfo->unit;
   size = binfo->size;

   // store binary info in static (global) variable to be retrieved by ?BTESTINF
   last_binfo.binfo = *binfo;
   last_binfo.failed = 0;
   last_binfo.query = 0;

   if (!(ptr = malloc(unit * size)))
   {
      errorf("No enough memory - unit: %d, size: %d", binfo->unit, binfo->size);
      return;
   }

   if (get_binary_data(ptr, unit, size) < 0)
      errorf("Error reading binary data");
   else
   {
      last_binfo.my_checksum = calculate_binary_checksum(ptr, unit, size);
      if (ext_chksum != 0 && ext_chksum != last_binfo.my_checksum)
         errorf("Checksum mismatch in *BTEST command: 0x%08x (block) vs. 0x%08x (calc)",
                 ext_chksum, last_binfo.my_checksum);
   }
   free(ptr);
}

void commexec_qbBTEST(void) {
   int         size = I_INTEGER();
   int         unit;
   int         flags = 0;
   uint32_t    checksum = 0;
   mblock_t*   mblk;

   if (size < 0)
   {
      size = -size;
      flags = BIN_FLG_NOCHKSUM;
   }
   unit = (!(size % 8))? 8 : (!(size % 4))? 4 : (!(size % 2))? 2 : 1;

   mblk = mblock_create(NULL, NULL, unit * size, MEM_DEFAULT);

   if (mblk)
   {
      char *ptr = mblock_ptr(mblk);
      int   i;

      for (i = 0; i < unit * size; i++)
        *(ptr + i) = 'A' + i % 50;

LOG_TRACE();
      answerf("Sending %d x %d byte binary data items", size, unit);
LOG_TRACE();
      send_binary_buffer(mblk, unit, size, flags);

      last_binfo.failed = 0;

   }
   else
   {
LOG_TRACE();
      errorf("No enough memory - unit: %d, size: %d", unit, size);
      last_binfo.failed = 1;
   }
LOG_TRACE();
   // store binary info in static (global) variable to be retrieved by ?BTESTINF
   last_binfo.binfo.unit = unit;
   last_binfo.binfo.size = size;
   last_binfo.binfo.flags = flags;
   last_binfo.binfo.checksum = checksum;
   last_binfo.query = 1;
};


void commexec_qBTESTINF(void)
{
   answerf("Last command: %s  : %s\n", last_binfo.query? "?*BTEST" : "*BTEST",
             last_binfo.failed? "failed" : "succedeed");
   answerf("  block unit: %d byte(s)\n", last_binfo.binfo.unit);
   answerf("  block size: %d\n", last_binfo.binfo.size);
   answerf("  flags:      0x%x\n", last_binfo.binfo.flags);
   answerf("  checksum:   0x%08x\n", last_binfo.binfo.checksum);
}
