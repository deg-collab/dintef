#ifndef __COMMLED_H_INCLUDED__
#define __COMMLED_H_INCLUDED__


#include <stdint.h>

#include "regdefs.h"
#include "dthread.h"

#ifndef COMMLED_OFF
#define _COMMLED_OFF      0x04
#else
#define _COMMLED_OFF      COMMLED_OFF
#endif

#ifndef COMMLED_BLUE
#define _COMMLED_BLUE      0x05  // If LED is tricolor, otherwise can be off (pepu) or red blinking (necst)
#else
#define _COMMLED_BLUE      COMMLED_BLUE
#endif

#ifndef COMMLED_GREEN
#define _COMMLED_GREEN    0x06
#else
#define _COMMLED_GREEN    COMMLED_GREEN
#endif

#ifndef COMMLED_RED
#define _COMMLED_RED      0x07
#else
#define _COMMLED_RED      COMMLED_RED
#endif

#define _COMMLED_BLINK    0x05   // Blinking if no tricolor LED. Can be off (pepu)
#define _COMMLED_BOTH     _COMMLED_BLINK

/*
//typedef struct __attribute__((align(4), packed, may_alias)) {
typedef struct __attribute__((packed, may_alias)) {
   uint8_t   color_on  : 2;   // colour code
   uint16_t  time_on   :10;   // time in 10 ms units (0=forever)
   uint8_t   color_off : 2;
   uint16_t  time_off  :10;
   uint16_t  cycles    : 8;   // number of cycles (0=forever)
} ledpattern_t;
*/

#define COMMLED_ERRFBLINKS_N        10    // number of error code preamble fast blinks
#define COMMLED_ERRFBLINKS_MS       100   // time/ms for error code preamble fast blinks

typedef struct {
   uint8_t   err_code;   // error code
   uint8_t   cycles;     // number of cycles (0=forever)
   uint8_t   color_on;   // colour code
   uint8_t   color_off;
   uint16_t  time_on;    // time in 10 ms units (0=forever)
   uint16_t  time_off;
} ledpattern_t;

extern const ledpattern_t ledOFF;
extern const ledpattern_t ledGREEN;
extern const ledpattern_t ledRED;
extern const ledpattern_t ledBLUE;
extern const ledpattern_t ledBOTH;
extern const ledpattern_t ledBLINKRED;
extern const ledpattern_t ledBLINKGREEN;
extern const ledpattern_t ledINITERROR;
extern const ledpattern_t ledCOMMERROR;

#define COMMLED_SETMAIN  NEW_DTHRCMD(0) // main LED pattern command
#define COMMLED_SETAUX   NEW_DTHRCMD(1) // aux/tmp LED pattern command
#define COMMLED_SETERROR NEW_DTHRCMD(2) // error LED pattern command

#if COMLED_USED

ledpattern_t
    commled_pattern(uint8_t on,  uint16_t ton_ms,
                    uint8_t off, uint16_t toff_ms,
                    uint16_t ncycles, uint8_t errcode);

int commled_init(void);
int commled_fini(void);
int commled_set_pattern(dthrcode_t cmd, const ledpattern_t* ledpatt);

#define COMMLED_MAIN(pattern)  commled_set_pattern(COMMLED_SETMAIN, &pattern)
#define COMMLED_AUX(pattern)   commled_set_pattern(COMMLED_SETAUX, &pattern)
#define COMMLED_ERROR(pattern, errcode) do {\
                                             ledpattern_t errledpatt = pattern; \
                                             errledpatt.err_code = (errcode);  \
                                             commled_set_pattern(COMMLED_SETERROR, &errledpatt); \
                                        } while(0)

#else

#define COMMLED_MAIN(pattern)
#define COMMLED_AUX(pattern)
#define COMMLED_ERROR(pattern, errcode)
#define COMMLED_DEV -1

#endif // COMLED_USED


#endif /* __COMMLED_H_INCLUDED__ */
