
#include <stdarg.h>

#include "errors.h"

#define MAX_LASTERRLEN (256 - 1)

static __thread char last_error_msg[MAX_LASTERRLEN + 1];

char* libdanceerror(void) { return last_error_msg; }

int set_libdanceerrorf(const char *fmt, ...) {
   va_list    ap;
   int        nchar;

   va_start(ap, fmt);
   nchar = vsnprintf(last_error_msg, MAX_LASTERRLEN, fmt, ap);
   va_end(ap);
   return(nchar);
}

