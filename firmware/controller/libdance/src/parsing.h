
#ifndef _PARSING_H_
#define _PARSING_H_


#define _ISOC99_SOURCE


#include <stdlib.h>
#include <ctype.h>
#include <strings.h>
#include <limits.h>
#include <float.h>
#include <inttypes.h>
#include <stdbool.h>

#include "errors.h"

typedef struct {
   const char   *ptr;
   size_t        length;
} string_t;

typedef unsigned char  id_boolean_t;
typedef unsigned char  id_tag_t;
typedef unsigned char  id_choice_t;
/*
typedef bool           id_boolean_t;
typedef long           id_integer_t;
typedef id_integer_t   id_flglist_t;
typedef id_integer_t   id_hexa_t;
*/
typedef intmax_t       id_integer_t;
typedef uintmax_t      id_hexa_t;
typedef uintmax_t      id_flglist_t;

#define IDINT_MIN INTMAX_MIN
#define IDINT_MAX INTMAX_MAX

#define SCNinteger SCNiMAX
#define PRIinteger PRIiMAX
#define SCNhexa    SCNiMAX
#define PRIhexa    PRIXMAX
//#define SCNflglist SCNiMAX
//#define PRIflglist PRNXMAX

typedef long double    id_floatp_t;
typedef string_t       id_label_t;
typedef string_t       id_string_t;


typedef struct {
   const char    *name;
   unsigned char  n;
   const char * const *options;
} list_t;

typedef struct {
   unsigned char  n;
   const char   **names;
   const list_t **arglists;
   unsigned char  n_bool;
   const char   **truevalue;
   const char   **falsevalue;
} arglists_t;



typedef struct {
   const char        *name;
   unsigned char      n;
   const char       **units;
   const id_floatp_t *factors;
} unittype_t;

typedef struct {
   unsigned char n_units;
   const char  **names;
   unittype_t  **unitlists;
} unitlists_t;


//--------------------------------------------------------
#define FMT_BOOLEAN    0    //  B#<b> boolean
#define FMT_TAG        1    //  T#<t> constant tag
#define FMT_FLAGLIST   2    //  G#<l> symbolic flag in <l> list (G1, G3, ...)
#define FMT_CHOICE     3    //  C#<l> choice value in <l> list (C1, C2, ...)
#define FMT_INTEGER    4    //  Ii:j  signed integer of value in [i,j]
#define FMT_HEXA       5    //  Hs    unsigned s-bit hexadecimal (H16, H32, ...)
#define FMT_FLOAT      6    //  Ff:g  floating point (id_floatp_t) of value in [f,g]
#define FMT_UNIT       7    //  U#<u> unit factor (id_floatp_t) from <u> list
#define FMT_LABEL      8    //  Lz    label of length < z
#define FMT_STRING     9    //  Sz    string of length < z
#define FMT_SCOMPOSITE 10   //  ()    sorted composite type
#define FMT_UCOMPOSITE 11   //  {}    unsorted composite type
#define FMT_UNKNOWN    12   //  unknown type

#define FMT_NFORMATS   FMT_UNKNOWN

#define FMT_TYPES  "BTGCIHFULS({"
#define FMT_ETYPES FMT_TYPES "?"


#define FMT_PREFIX_OPTIONAL  'o'   //  'optional' prefix
#define FMT_PREFIX_VARIABLE  'n'   //  'undefined repetition' prefix
#define FMT_PREFIX_UNSIGNED  'u'   //  'unsigned value' ( >=0 )
#define FMT_PREFIX_POSITIVE  'p'   //  'positive value' ( >0 )


#define FMT_ID_BEG        '['       //  begining of entry identifiers
#define FMT_ID_END        ']'       //  end of entry identifiers
#define FMT_LISTNAME_PFX  '#'       //  prefix of FLAGLIST or CHOICE name

#define FMT_NO_SEPARATOR   0       //  separator for exclusive item
#define FMT_EXCLUSIVE_SEP  1       //  separator for exclusive item
#define FMT_ACONNECT_SEP   2       //  auto 'connected' separator
#define FMT_ECONNECT_SEP   3       //  empty 'connected' separator
#define FMT_SCONNECT_SEP   4       //  space 'connected' separator

#define FMT_SEPARATORS  " |_-+"

#define IS_INTEGER_FMT(fmt)     ((fmt) == FMT_INTEGER || (fmt) == FMT_HEXA)
#define IS_NUMERIC_FMT(fmt)     (IS_INTEGER_FMT(fmt) || (fmt) == FMT_FLOAT)
#define IS_TEXT_FMT(fmt)        ((fmt) == FMT_LABEL || (fmt) == FMT_STRING)
#define IS_SCOMPOSITE_FMT(fmt)  ((fmt) == FMT_SCOMPOSITE)
#define IS_UCOMPOSITE_FMT(fmt)  ((fmt) == FMT_UCOMPOSITE)
#define IS_COMPOSITE_FMT(fmt)   (IS_SCOMPOSITE_FMT(fmt) || IS_UCOMPOSITE_FMT(fmt))

extern const char fmt_types[];
extern const char fmt_etypes[];
extern const char fmt_separators[];

#define FMT_SCOMPOSITE_BEGIN  '('      //  beging character of sorted composite
#define FMT_SCOMPOSITE_END    ')'      //  end character of composite
#define FMT_UCOMPOSITE_BEGIN  '{'      //  beging character of unsorted composite
#define FMT_UCOMPOSITE_END    '}'      //  end character of unsorted composite

#define COMPOSITE_END_CHAR(fmt)  ((fmt) == FMT_SCOMPOSITE? FMT_SCOMPOSITE_END : FMT_UCOMPOSITE_END)
#define IS_COMPOSITE_END(c)      ((c) == FMT_SCOMPOSITE_END || (c) == FMT_UCOMPOSITE_END)

#define IS_CONNECTCHAR(c)        (strchr(fmt_separators + 1, c) != NULL)

#define FMT_UNIT_SEPARATOR    ':'      //  unit separator character

#define IS_INVALID_LBLCHAR(c) (isblank(c) || IS_CONNECTCHAR(c) || IS_COMPOSITE_END(c) || \
                           (c) == FMT_SCOMPOSITE_BEGIN || (c) == FMT_UCOMPOSITE_BEGIN || \
                           (c) == FMT_ID_BEG || (c) == FMT_ID_END || (c) == FMT_UNIT_SEPARATOR)

#define _OPTIONAL              (1 << 0)
#define _UNSORTED              (1 << 1)
#define _EXCLUSIVE_WITH_PREV   (1 << 2)
#define _EXCLUSIVE_WITH_NEXT   (1 << 3)
#define _INP_EMPTY_ALLOWED     (1 << 4)
#define _INP_WSPACE_ALLOWED    (1 << 5)
#define _LINKED_TO_NEXT        (1 << 6)
#define _MAY_TOUCH_NEXT        (1 << 7)
#define _OUT_WSPACE_SUPPRESSED (1 << 8)

#define _ISCONNECTED  (_INP_EMPTY_ALLOWED | _INP_WSPACE_ALLOWED)

#define _FMT_COMPARE_MASK   (_OPTIONAL)

#define IS_OPTIONAL(fmt) ((fmt)->flags & _OPTIONAL)
#define IS_UNSORTED(fmt) ((fmt)->flags & _UNSORTED)
#define IS_CONNECTED(fmt) ((fmt)->flags & _ISCONNECTED)
#define IS_EXCLUSIVE(fmt) ((fmt)->flags & _EXCLUSIVE_WITH_NEXT)
#define WAS_EXCLUSIVE(fmt) ((fmt)->flags & _EXCLUSIVE_WITH_PREV)
#define IS_EMPTY_ALLOWED(fmt) ((fmt)->flags & _INP_EMPTY_ALLOWED)
#define IS_WSPACE_ALLOWED(fmt) ((fmt)->flags & _INP_WSPACE_ALLOWED)
#define IS_NEXT_CONNECTED(fmt) ((fmt)->flags & _LINKED_TO_NEXT)
#define SUPPRESS_WSPACE(fmt) ((fmt)->flags & _OUT_WSPACE_SUPPRESSED)
#define MAY_TOUCHNEXT(fmt) ((fmt)->flags & _MAY_TOUCH_NEXT)

#define IS_NOTMANDATORY(fmt) (IS_OPTIONAL(fmt) || (((fmt)->flags & (_UNSORTED | _EXCLUSIVE_WITH_NEXT)) && !IS_CONNECTED(fmt)))
#define IS_MANDATORY(fmt)    (!IS_NOTMANDATORY(fmt))

/*

prefixes:
  <n> = n repetitions (default 1)
  n   = arbitrary number >= 1
  o   = optional  (with n, zero repetitions is valid)

prefixes for numeric values (only one allowed):
  u   = number >= 0
  p   = number > 0

separator characters:
  |   = exclusive
  _   = connection with automatic separator
  -   = connection with no space separator
  +   = connection withspace separator required

Ex:  "I 3F pI"
     "3(L1 F)"
     "o{oT#MYTAG pF}"

*/

#define fmtNONE    NULL
#define fmtBOOL    "B"
#define fmtINT     "I"
#define fmtUINT    "uI"
#define fmtPINT    "pI"
#define fmtFLOAT   "F"
#define fmtUFLOAT  "uF"
#define fmtPFLOAT  "pF"
#define fmtHEXA    "H"
#define fmtLABEL   "L"
#define fmtSTRING  "S"

typedef unsigned char ftype_t;
typedef unsigned char septype_t;

typedef struct {
   ftype_t        type;     /* FMT_XXXX                                    */
   unsigned short n_rep;    /* 0 = undefined                               */
   unsigned short flags;    /* flags                                       */
   septype_t      separator;/* separator code                              */
   char           uns_pos;  /* 'u' : unsigned (>=0) or 'p': positive (> 0) */
   union {
         int      maxlen;   /* max string or label length                  */
         int      compvalid;/* composite valid items                       */
         int      boolit;   /* bool item                                   */
         int      taglen;   /* tag length                                  */
         int      defunit;  /* default unit                                */
         int      nbits;    /* number of valid bits                        */
   }              sp;
   union {
      intmax_t      intval; /* min integer value                           */
      id_floatp_t   fltval; /* min floating point absolute value           */
   }              min;
   union {
      intmax_t      intval; /* max integer value                           */
      id_floatp_t   fltval; /* max floating point  value                   */
   }              max;
   void          *tlptr;    /* pointer to tag string, list or units structure */
   const char    *idptr;    /* pointer to id string                        */
   int            idlen;    /* length of id string                         */
} format_t;

#define DEFAULT_FORMAT { \
    .n_rep = 1,          \
    .flags = 0,          \
    .separator = 0,      \
    .uns_pos = 0,        \
    .type = FMT_UNKNOWN, \
    .sp.maxlen = 0,      \
    .min.intval = 0.0,   \
    .max.intval = 0.0,   \
    .tlptr = NULL,       \
    .idptr = NULL,       \
    .idlen = 0           \
   }


#define DEFAULT_BOOLEAN   2
#define DEFAULT_TAG       0
#define DEFAULT_FLAGLIST  UINTMAX_MAX
#define DEFAULT_CHOICE    UCHAR_MAX
#define DEFAULT_INTEGER   INTMAX_MIN
#define DEFAULT_HEXA      UINTMAX_MAX
#define DEFAULT_UNIT      DEFAULT_FLOAT
#define DEFAULT_FLOAT     LDBL_MIN
#define DEFAULT_LABEL     NULL
#define DEFAULT_STRING    NULL
#define DEFAULT_COMPOSITE 0

extern const char *empty_string;

typedef union id_item_u {
   id_boolean_t      boolean;
   id_tag_t          tag;
   id_flglist_t      flglist;
   id_choice_t       choice;
   id_integer_t      integer;
   id_hexa_t         hexa;
   id_floatp_t       floatp;
   id_choice_t       unit;
   id_label_t        label;
   id_string_t       string;
   struct inpdata_s *composite;
} id_item_t;

#define ID_DEF_NARGS  5
#define ID_DEF_NITEMS 5

typedef struct {
   struct inpdata_s *itl_parent;
   format_t          fmt;
   unsigned short    available;
   unsigned short    used;
   unsigned short    current;
   id_item_t         def_items[ID_DEF_NITEMS];
   id_item_t        *more_items;
} id_itemlist_t;

typedef struct inpdata_s {
   id_itemlist_t *iod_parent;
   unsigned short available;
   unsigned short used;
   unsigned short current;
   id_itemlist_t  def_itemlists[ID_DEF_NARGS];
   id_itemlist_t *more_itemlists;
} iodata_t;

typedef uint64_t itlmask_t;  // unsorted itemlist mask
#define MAX_ITEMLISTS   64   // max number of unsorted itemlists

//--------------------------------------------------------
//

void *search_parsing_data(ftype_t type, const char *str, int flags);

#define _SRCH_INP     (1 << 0)  //  search in input data structures
#define _SRCH_OUTP    (1 << 1)  //  search in output data structures

#define _SRCH_CURR    (1 << 2)  //  look only into current item
#define _SRCH_TYPE    (1 << 3)  //  check item type only
#define _SRCH_FMT     (1 << 4)  //  check full item format
#define _SRCH_ID      (1 << 5)  //  check entry id only
#define _SRCH_LN      (1 << 6)  //  check list name only

#define _SRCH_RST     (1 << 7)  //  reset current pointer before searching
#define _SRCH_ONE     (1 << 8)  //  do not advance to next format item
#define _SRCH_EXOPT   (1 << 9)  //  exclude optional non parsed items (only input data)
#define _SRCH_EXCOMP (1 << 10)  //  exclude composite entries

#define __RET_TYPE   (1 << 11)  //  return type of format element
#define __RET_NUM    (1 << 12)  //  return number of values in format element
#define __RET_CHKLN  (1 << 13)  //  return list name check
#define __RET_CHKID  (1 << 14)  //  return id check
#define __RET_VALUE  (1 << 15)  //  return actual value

#define __RET_MASK  (__RET_TYPE | __RET_NUM | __RET_CHKLN | __RET_CHKID | __RET_VALUE)

#define FMT_NOT_FOUND  -1
#define FMT_ERROR      -2


static inline int advance_parsing_format(int flags) {
   return(*(int *)search_parsing_data(FMT_UNKNOWN, NULL, flags | _SRCH_CURR | __RET_NUM) != FMT_NOT_FOUND);
}

#define I_NEXT_FMT() advance_parsing_format(_SRCH_INP)


#define _EACH_I_ (_SRCH_INP | _SRCH_CURR | _SRCH_ONE)

// The following macros return various info from the current item in the stack,
//   they do not perform any search and the pointer does not advance to the next.
//
// Returns the item type
#define I_FMT_TYPE()      *(int *)search_parsing_data(FMT_UNKNOWN, NULL, _EACH_I_ | __RET_TYPE)
// Returns the number of values parsed for the item
#define I_FMT_NUM()       *(int *)search_parsing_data(FMT_UNKNOWN, NULL, _EACH_I_ | __RET_NUM)
// Returns whether or not the item uses the given list name
#define I_FMT_CHKNAME(nm) *(int *)search_parsing_data(FMT_UNKNOWN, nm,   _EACH_I_ | __RET_CHKLN)
// Returns whether or not the item has the given ID string
#define I_FMT_CHKID(id)   *(int *)search_parsing_data(FMT_UNKNOWN, id,   _EACH_I_ | __RET_CHKID)


// The following macros look for the first item in the stack from the current position
//   that matches the format string fmt and return the number of values parsed for that item.
//
// Finds the item and moves to the next one
#define I_FMT(fmt)        *(int *)search_parsing_data(FMT_UNKNOWN, fmt, _SRCH_INP | __RET_NUM | _SRCH_ONE | _SRCH_FMT)
// Finds the item but the remains pointing to it
#define I_THIS_FMT(fmt)   *(int *)search_parsing_data(FMT_UNKNOWN, fmt, _SRCH_INP | __RET_NUM | _SRCH_ONE | _SRCH_FMT | _SRCH_CURR)



// The following macros look for the first item from the beginning of the stack that matches
//   one of three criteria and return the number of values parsed. The pointer moves to
//   the next item in the stack.
//
// Finds the first item in the stack that matches the format string fmt
#define I_FIRST_FMT(fmt) *(int *)search_parsing_data(FMT_UNKNOWN, fmt,_SRCH_INP | __RET_NUM | _SRCH_ONE | _SRCH_RST | _SRCH_FMT)
// Finds the first item in the stack that makes use of a list named nm
#define I_FIRST_NAME(nm) *(int *)search_parsing_data(FMT_UNKNOWN, nm, _SRCH_INP | __RET_NUM | _SRCH_ONE | _SRCH_RST | _SRCH_LN)
// Finds the first item in the stack that has the identifier id
#define I_FIRST_ID(id)   *(int *)search_parsing_data(FMT_UNKNOWN, id, _SRCH_INP | __RET_NUM | _SRCH_ONE | _SRCH_RST | _SRCH_ID)


#define _I_VALUE_ (_SRCH_INP | __RET_VALUE)

#define __I_____(_t, _f) (_t*)search_parsing_data(_f, NULL, _I_VALUE_ | _SRCH_TYPE)
#define __I_THIS(_t, _f) (_t*)search_parsing_data(_f, NULL, _I_VALUE_ | _SRCH_TYPE | _SRCH_CURR)
#define __I_FRST(_t, _f) (_t*)search_parsing_data(_f, NULL, _I_VALUE_ | _SRCH_TYPE | _SRCH_RST)
#define __I_NAME(_t, _f, nm) \
                         (_t*)search_parsing_data(_f, nm,   _I_VALUE_ | _SRCH_TYPE | _SRCH_LN | _SRCH_RST)
#define __I_ID__(_t, _f, id) \
                         (_t*)search_parsing_data(_f, id,   _I_VALUE_ | _SRCH_TYPE | _SRCH_ID | _SRCH_RST)


// The following macros look for an item of a given type and return the parsed value.
//   If the value has not been actually parsed, they return the corresponding default values.
//
// The macros I_<type>() , find the first item of that type from the current position.
// The macros I_THIS_<type>() , find the first item but keeps pointing to it.
// The macros I_FIRST_<type>() , find the first item but from the beginning of the stack.
// The macros I_NAMED_<type>(nm) , finds the first item of that type that has the name nm.
// The macros I_ID_<type>(id) , finds the first item of that type that has the identifier id.
//
#define I_BOOL()       *__I_____(id_boolean_t, FMT_BOOLEAN)
#define I_THIS_BOOL()  *__I_THIS(id_boolean_t, FMT_BOOLEAN)
#define I_FIRST_BOOL() *__I_FRST(id_boolean_t, FMT_BOOLEAN)
#define I_ID_BOOL(id)  *__I_ID__(id_boolean_t, FMT_BOOLEAN, id)

#define I_TAG()         *__I_____(const char, FMT_TAG)
#define I_THIS_TAG()    *__I_THIS(const char, FMT_TAG)
#define I_FIRST_TAG()   *__I_FRST(const char, FMT_TAG)
#define I_NAMED_TAG(nm) *__I_NAME(const char, FMT_TAG, nm)
#define I_ID_TAG(id)    *__I_ID__(const char, FMT_TAG, id)

#define I_FLGLIST()         *__I_____(id_flglist_t, FMT_FLAGLIST)
#define I_THIS_FLGLIST()    *__I_THIS(id_flglist_t, FMT_FLAGLIST)
#define I_FIRST_FLGLIST()   *__I_FRST(id_flglist_t, FMT_FLAGLIST)
#define I_NAMED_FLGLIST(nm) *__I_NAME(id_flglist_t, FMT_FLAGLIST, nm)
#define I_ID_FLGLIST(id)    *__I_ID__(id_flglist_t, FMT_FLAGLIST, id)

#define I_CHOICE()         *__I_____(id_choice_t, FMT_CHOICE)
#define I_THIS_CHOICE()    *__I_THIS(id_choice_t, FMT_CHOICE)
#define I_FIRST_CHOICE()   *__I_FRST(id_choice_t, FMT_CHOICE)
#define I_NAMED_CHOICE(nm) *__I_NAME(id_choice_t, FMT_CHOICE, nm)
#define I_ID_CHOICE(id)    *__I_ID__(id_choice_t, FMT_CHOICE, id)

#define I_INTEGER()       *__I_____(id_integer_t, FMT_INTEGER)
#define I_THIS_INTEGER()  *__I_THIS(id_integer_t, FMT_INTEGER)
#define I_FIRST_INTEGER() *__I_FRST(id_integer_t, FMT_INTEGER)
#define I_ID_INTEGER(id)  *__I_ID__(id_integer_t, FMT_INTEGER, id)

#define I_HEXA()       *__I_____(id_hexa_t, FMT_HEXA)
#define I_THIS_HEXA()  *__I_THIS(id_hexa_t, FMT_HEXA)
#define I_FIRST_HEXA() *__I_FRST(id_hexa_t, FMT_HEXA)
#define I_ID_HEXA(id)  *__I_ID__(id_hexa_t, FMT_HEXA, id)

#define I_FLOAT()       *__I_____(id_floatp_t, FMT_FLOAT)
#define I_THIS_FLOAT()  *__I_THIS(id_floatp_t, FMT_FLOAT)
#define I_FIRST_FLOAT() *__I_FRST(id_floatp_t, FMT_FLOAT)
#define I_ID_FLOAT(id)  *__I_ID__(id_floatp_t, FMT_FLOAT, id)

#define I_UNIT()         *__I_____(id_floatp_t, FMT_UNIT)
#define I_THIS_UNIT()    *__I_THIS(id_floatp_t, FMT_UNIT)
#define I_FIRST_UNIT()   *__I_FRST(id_floatp_t, FMT_UNIT)
#define I_NAMED_UNIT(nm) *__I_NAME(id_floatp_t, FMT_UNIT, nm)
#define I_ID_UNIT(id)    *__I_ID__(id_floatp_t, FMT_UNIT, id)

#define I_LABEL()        __I_____(const char, FMT_LABEL)
#define I_THIS_LABEL()   __I_THIS(const char, FMT_LABEL)
#define I_FIRST_LABEL()  __I_FRST(const char, FMT_LABEL)
#define I_ID_LABEL(id)   __I_ID__(const char, FMT_LABEL, id)

#define I_STRING()       __I_____(const char, FMT_STRING)
#define I_THIS_STRING()  __I_THIS(const char, FMT_STRING)
#define I_FIRST_STRING() __I_FRST(const char, FMT_STRING)
#define I_ID_STRING(id)  __I_ID__(const char, FMT_STRING, id)

#define I_BLOCK()       *__I_____(id_integer_t, FMT_UCOMPOSITE)
#define I_THIS_BLOCK()  *__I_THIS(id_integer_t, FMT_UCOMPOSITE)
#define I_FIRST_BLOCK() *__I_FRST(id_integer_t, FMT_UCOMPOSITE)
#define I_ID_BLOCK(id)  *__I_ID__(id_integer_t, FMT_UCOMPOSITE, id)

// ------------------------

#define O_NEXT_FMT() advance_parsing_format(_SRCH_OUTP)

#define _EACH_O_ (_SRCH_OUTP | _SRCH_ONE)

#define O_FMT_TYPE()    *(int *)search_parsing_data(FMT_UNKNOWN, NULL, _EACH_O_ | _SRCH_CURR | __RET_TYPE)
#define O_FMT_NUM()     *(int *)search_parsing_data(FMT_UNKNOWN, NULL, _EACH_O_ | _SRCH_CURR | __RET_NUM)
#define O_FMT_NAME(ln)  *(int *)search_parsing_data(FMT_UNKNOWN, ln,   _EACH_O_ | _SRCH_CURR | __RET_CHLN)
#define O_FMT_CHKID(id) *(int *)search_parsing_data(FMT_UNKNOWN, id,   _EACH_O_ | _SRCH_CURR | __RET_CHKID)

#define O_FMT(fmt)       *(int *)search_parsing_data(FMT_UNKNOWN, fmt, _EACH_O_ | __RET_NUM)
#define O_THIS_FMT(fmt)  *(int *)search_parsing_data(FMT_UNKNOWN, fmt, _EACH_O_ | __RET_NUM | _SRCH_CURR)
#define O_FIRST_FMT(fmt) *(int *)search_parsing_data(FMT_UNKNOWN, fmt, _EACH_O_ | __RET_NUM | _SRCH_RST)


#define __O_____   _SRCH_OUTP | _SRCH_TYPE
#define __O_THIS   _SRCH_OUTP | _SRCH_TYPE | _SRCH_CURR
#define __O_FRST   _SRCH_OUTP | _SRCH_TYPE | _SRCH_RST
#define __O_ID__   _SRCH_OUTP | _SRCH_ID   | _SRCH_RST

#define O_BOOL(v)          set_parsing_integer(__O_____, FMT_BOOLEAN, NULL, v)
#define O_THIS_BOOL(v)     set_parsing_integer(__O_THIS, FMT_BOOLEAN, NULL, v)
#define O_FIRST_BOOL(v)    set_parsing_integer(__O_FRST, FMT_BOOLEAN, NULL, v)
#define O_ID_BOOL(id, v)   set_parsing_integer(__O_ID__, FMT_BOOLEAN, id, v)

#define O_TAG(v)           set_parsing_integer(__O_____, FMT_TAG, NULL, v)
#define O_THIS_TAG(v)      set_parsing_integer(__O_THIS, FMT_TAG, NULL, v)
#define O_FIRST_TAG(v)     set_parsing_integer(__O_FRST, FMT_TAG, NULL, v)
#define O_ID_TAG(id, v)    set_parsing_integer(__O_ID__, FMT_TAG, id, v)

#define O_FLGLIST(v)        set_parsing_integer(__O_____, FMT_FLAGLIST, NULL, v)
#define O_THIS_FLGLIST(v)   set_parsing_integer(__O_THIS, FMT_FLAGLIST, NULL, v)
#define O_FIRST_FLGLIST(v)  set_parsing_integer(__O_FRST, FMT_FLAGLIST, NULL, v)
#define O_ID_FLGLIST(id, v) set_parsing_integer(__O_ID__, FMT_FLAGLIST, id, v)

#define O_CHOICE(v)        set_parsing_integer(__O_____, FMT_CHOICE, NULL, v)
#define O_THIS_CHOICE(v)   set_parsing_integer(__O_THIS, FMT_CHOICE, NULL, v)
#define O_FIRST_CHOICE(v)  set_parsing_integer(__O_FRST, FMT_CHOICE, NULL, v)
#define O_ID_CHOICE(id, v) set_parsing_integer(__O_ID__, FMT_CHOICE, id, v)

#define O_INTEGER(v)        set_parsing_integer(__O_____, FMT_INTEGER, NULL, v)
#define O_THIS_INTEGER(v)   set_parsing_integer(__O_THIS, FMT_INTEGER, NULL, v)
#define O_FIRST_INTEGER(v)  set_parsing_integer(__O_FRST, FMT_INTEGER, NULL, v)
#define O_ID_INTEGER(id, v) set_parsing_integer(__O_ID__, FMT_INTEGER, id, v)

#define O_HEXA(v)          set_parsing_integer(__O_____, FMT_HEXA, NULL, v)
#define O_THIS_HEXA(v)     set_parsing_integer(__O_THIS, FMT_HEXA, NULL, v)
#define O_FIRST_HEXA(v)    set_parsing_integer(__O_FRST, FMT_HEXA, NULL, v)
#define O_ID_HEXA(id, v)   set_parsing_integer(__O_ID__, FMT_HEXA, id, v)

#define O_FLOAT(v)         set_parsing_float(__O_____, FMT_FLOAT, NULL, v)
#define O_THIS_FLOAT(v)    set_parsing_float(__O_THIS, FMT_FLOAT, NULL, v)
#define O_FIRST_FLOAT(v)   set_parsing_float(__O_FRST, FMT_FLOAT, NULL, v)
#define O_ID_FLOAT(id, v)  set_parsing_float(__O_ID__, FMT_FLOAT, id, v)

#define O_UNIT(v)          set_parsing_float(__O_____, FMT_UNIT, NULL, v)
#define O_THIS_UNIT(v)     set_parsing_float(__O_THIS, FMT_UNIT, NULL, v)
#define O_FIRST_UNIT(v)    set_parsing_float(__O_FRST, FMT_UNIT, NULL, v)
#define O_ID_UNIT(id, v)   set_parsing_float(__O_ID__, FMT_UNIT, id, v)

#define O_LABEL(pt)        set_parsing_text(__O_____, FMT_LABEL, NULL, pt)
#define O_THIS_LABEL(pt)   set_parsing_text(__O_THIS, FMT_LABEL, NULL, pt)
#define O_FIRST_LABEL(pt)  set_parsing_text(__O_FRST, FMT_LABEL, NULL, pt)
#define O_ID_LABEL(id, v)  set_parsing_text(__O_ID__, FMT_LABEL, id, v)

#define O_STRING(pt)       set_parsing_text(__O_____, FMT_STRING, NULL, pt)
#define O_THIS_STRING(pt)  set_parsing_text(__O_THIS, FMT_STRING, NULL, pt)
#define O_FIRST_STRING(pt) set_parsing_text(__O_FRST, FMT_STRING, NULL, pt)
#define O_ID_STRING(id, v) set_parsing_text(__O_ID__, FMT_STRING, id, v)

#define O_BLOCK(id)       *(int *)search_parsing_data(FMT_UCOMPOSITE, id, __O_____ | __RET_NUM | _SRCH_ONE)
#define O_THIS_BLOCK(id)  *(int *)search_parsing_data(FMT_UCOMPOSITE, id, __O_THIS | __RET_NUM | _SRCH_ONE)
#define O_FIRST_BLOCK(id) *(int *)search_parsing_data(FMT_UCOMPOSITE, id, __O_FRST | __RET_NUM | _SRCH_ONE)
#define O_ID_BLOCK(id)    *(int *)search_parsing_data(FMT_UCOMPOSITE, id, __O_ID__ | __RET_NUM | _SRCH_ONE)

#define O_SKIP_BLOCK() ()


#define SET_OUTPCONTEXT(cmd) switch_output_context(cmd_ ## cmd)
#define CLR_OUTPCONTEXT()    release_output_context()
#define OUTCONTEXT_EXEC(cmd) do { \
                                 switch_output_context(cmd_ ## cmd); \
                                 exec_ ## cmd(); \
                                 release_output_context(); \
                                 answerf("\n"); \
                             } while(0)

int set_parsing_text(int flags,    ftype_t type, const char *id, const char *textstr);
int set_parsing_float(int flags,   ftype_t type, const char *id, id_floatp_t fvalue);
int set_parsing_integer(int flags, ftype_t type, const char *id, intmax_t ivalue);

void reset_output_context(void);
void switch_output_context(int cmd_id);
void release_output_context(void);

// ---------------------------------------------------------------
#define TO_UNITS   0
#define FROM_UNITS 1

#define UNITS_TO(t, u, value) unit_conversion(#t, #u, value, TO_UNITS)
#define UNITS_FROM(t, u, value) unit_conversion(#t, #u, value, FROM_UNITS)

id_floatp_t unit_conversion(char *type, char *unit, id_floatp_t value, int from);

// -------------------------------------------------------
#define GCC_DIAG_DO_PRAGMA(x) _Pragma (#x)
#define GCC_DIAG_PRAGMA(x) GCC_DIAG_DO_PRAGMA(GCC diagnostic x)
#define GCC_DIAG_OFF(x) GCC_DIAG_PRAGMA(push) \
                        GCC_DIAG_PRAGMA(ignored #x)
#define GCC_DIAG_FORMAT_OFF() GCC_DIAG_PRAGMA(push) \
                              GCC_DIAG_PRAGMA(ignored "-Wformat") \
                              GCC_DIAG_PRAGMA(ignored "-Wformat-extra-args")
#define GCC_DIAG_ON() GCC_DIAG_PRAGMA(pop)


errmsg_t parse_format_string(iodata_t *outdata, const char *out_format, arglists_t *arglsts);
errmsg_t parse_input_args(iodata_t *inpdata, const char *params);
void     answer_output_data(iodata_t *output_data);

void      show_io_format(int indent, iodata_t *iodata);
iodata_t *clone_iodata(iodata_t *iodata);
void      free_iodata(iodata_t *iodata);


#endif //_PARSING_H_

