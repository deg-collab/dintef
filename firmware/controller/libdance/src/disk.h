#ifndef DISK_H_INCLUDED
#define DISK_H_INCLUDED


#include <stdint.h>
#include <sys/types.h>
#include "libefpak.h"


typedef struct disk_handle
{
  /* WARNING: 64 bit types to avoid overflow with large files */

  char dev_path[256];
  char* dev_name;
  size_t name_size;
  int dev_maj;

  int fd;

#define DISK_BLOCK_SIZE 512
  uint64_t block_size;
  uint64_t block_count;

  size_t chs[3];

  /* off, size in blocks */
#define DISK_MAX_PART_COUNT 4
  uint64_t part_count;
  uint64_t part_off[DISK_MAX_PART_COUNT];
  uint64_t part_size[DISK_MAX_PART_COUNT];

#define DISK_MSG_SIZE 128
  char     status[DISK_MSG_SIZE];
  int      error;
  size_t   totalwr;
  size_t   total2wr;

} disk_handle_t;

/* TODO: add some mutex here but then a function will be needed */
#define DISK_UPDATE_STATUS(diskp, __fmt, ...) \
do { \
    snprintf(diskp->status, DISK_MSG_SIZE, __fmt, ##__VA_ARGS__); \
} while(0)

#define DISK_STATUS(diskp) diskp->status

/* ready for more detailed error codes */
#define DISK_IS_ERROR(diskp) (diskp->error != 0)
#define DISK_SET_ERROR(diskp, ...) \
do { \
    diskp->error = -1; \
} while(0)
#define DISK_CLR_ERROR(diskp) \
do { \
    diskp->error = 0; \
} while(0)
#define DISK_UPDATE_STATUS_ERR(diskp, ...) \
do { \
    DISK_UPDATE_STATUS(diskp, ##__VA_ARGS__); \
    DISK_SET_ERROR(diskp); \
} while(0)

int disk_open_root(disk_handle_t*);
int disk_open_dev(disk_handle_t*, const char*);
void disk_close(disk_handle_t*);
int disk_seek(disk_handle_t*, size_t);
int disk_write(disk_handle_t*, size_t, size_t, const uint8_t*);
int disk_read(disk_handle_t*, size_t, size_t, uint8_t*);
int disk_install_with_efpak(disk_handle_t*, efpak_istream_t*);


#endif /* DISK_H_INCLUDED */
