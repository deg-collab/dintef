/*---------------------------------------------------------------------------
 *
 * $Revision: 1855 $
 *
 * $Id: dance_signal.h 1855 2018-07-31 18:06:12Z fajardo $
 *
 *------------------------------------------------------------------------- */


#ifndef _DANCESIGNAL_H_INCLUDED_
#define _DANCESIGNAL_H_INCLUDED_

/*
#include <stdint.h>
#include <sys/types.h>
#include <pthread.h>
#include "memory.h"
#include "lnklist.h"
#include "taskqueue.h"
*/
#include "errors.h"


typedef enum
{
  SIGTYPE_UNKNOWN = 0,
  SIGTYPE_WAKEUP,
  SIGTYPE_ONESHOT,
  SIGTYPE_PERIODIC,
  SIGTYPE_COMMCLEANUP
} sigtype_t;


typedef struct
{
   sigtype_t sigtype;
   void*     arg;
} signal_t;

danceerr_t runtime_send_signal(sigtype_t sigtype, void* arg);
static inline
danceerr_t runtime_wakeup(void) {return runtime_send_signal(SIGTYPE_WAKEUP, NULL);}


#endif   // _DANCESIGNAL_H_INCLUDED_
