/*---------------------------------------------------------------------------
 *
 */
#ifndef _COMM_H_INCLUDED_
#define _COMM_H_INCLUDED_

#include <stdint.h>
#include <stdarg.h>
#include <sys/types.h>
#include "parsing.h"
#include "runtime.h"

/*---------------------------------------------------------------------------
 *
 */

typedef struct {
   int         item;
   char       *comm_str;
   size_t      comm_len;
   void      (*comm_funct)(void *);
   void      (*help_funct)(void);
   const char *input_fmt;
   const char *output_fmt;
   uint32_t    flags;
   char       *help;
   iodata_t    inp_data;
   iodata_t    out_data;
} menuitem_t;

// menuitem flags
#define _DANCE_COMMAND            0x80000000   // DANCE common command
#define _CONCURRENT_COMMAND       0x40000000   // Concurrent execution
#define _ALLOWED_DURING_DELAYED   0x10000000   // Allowed in delayed execution
#define _HIDDEN_COMMAND           0x08000000   // Do not show in ?HELP queries
#define _TEST_COMMAND             0x04000000   // Show only in ?HELP TEST queries
#define _EXPERT_COMMAND           0x02000000   // Show in ?HELP EXPERT
#define _ADVANCED_COMMAND         _EXPERT_COMMAND
#define _BASIC_COMMAND            0x01000000   // Show in ?HELP BASIC
#define _INITFAIL_COMMAND         0x00800000   // Allowed even if INITFAIL


// menuitem flags available for controller
/* Useless with MENU_IT() macro which need a constant
#define APP_COMMAND_FLG(v) app_command_flg(v)
int app_command_flg(int v) {
    if (v<2) {
        return(1 << (v - 1));
    } else {
        SYSTEM_ERRORF("invalid app command flag value\n");
        return(-1);
    }
}
*/
#define APP_COMMAND_FLG(v) _APP_COMMAND_FLG_ ##v
#define _APP_COMMAND_FLG_1        0x00000001
#define _APP_COMMAND_FLG_2        0x00000002
#define _APP_COMMAND_FLG_3        0x00000004
#define _APP_COMMAND_FLG_4        0x00000008


#define BIN_SIGNATURE_32     0xa5a50000
#define BIN_SIGNATURE_MASK   0xffff0000
#define BIN_UNIT_MASK        0x0000000f
#define BIN_FLG_USECHKSUM    0x00000000
#define BIN_FLG_NOCHKSUM     0x00000010
#define BIN_FLG_LITTLENDIAN  0x00000000
#define BIN_FLG_BIGENDIAN    0x00000020
#define BIN_FLG_MASK         (BIN_FLG_NOCHKSUM | BIN_FLG_BIGENDIAN)
#define HEADER_BYTES         (3 * 4)

// Byte ordering related

#if __BYTE_ORDER == __LITTLE_ENDIAN
  #define LITTLE_ENDIAN_64(value) (value)
  #define LITTLE_ENDIAN_32(value) (value)
  #define LITTLE_ENDIAN_16(value) (value)
#elif __BYTE_ORDER == __BIG_ENDIAN
  #define LITTLE_ENDIAN_64(value) byte_swap64(value)
  #define LITTLE_ENDIAN_32(value) byte_swap32(value)
  #define LITTLE_ENDIAN_16(value) byte_swap16(value)

  uint64_t byte_swap64(uint64_t value);
  uint32_t byte_swap32(uint32_t value);
  uint16_t byte_swap32(uint16_t value);

#else
  #error bad __BYTE_ORDER (or not defined)
#endif

int iodata_init(void);


typedef struct binary_header_s {
   uint32_t  signature;   //  the 16 MSB contains the SIGNATURE value
   uint32_t  size;
   uint32_t  checksum;
} binary_header_t;

typedef struct binary_s {
   uint32_t  unit;    // number of bytes per data item (1, 2, 4 or 8)
   uint32_t  size;    // number of data items (of size specified by unit)
   uint32_t  flags;
   uint32_t  checksum;
   uint32_t  bytes_to_transfer;  // number of bytes still to transfer
   uint32_t  calc_checksum;      // calculated checksum
} binary_t;


#define ERRMSG_MAXSIZE (512 - 1)

typedef struct command_s {
   channel_t  *channel;
   char       *cmd;
   char       *address;
   int         address_sz;
   char       *keyword;
   int         keyword_sz;
   char       *params;
   uint16_t    flags;
   menuitem_t *mentry;
   arglists_t *param_lists;
   iodata_t   *inp_data;
   iodata_t   *out_data;
   binary_t    inp_binary;
   binary_t    out_binary;
   char        errormsg[ERRMSG_MAXSIZE + 1];
   size_t      errormsg_sz;
   cbuflist_t* answ_list;
   dblock_t*   main_prefix;
   dblock_t*   main_answ;
} command_t;


void do_command_cleanup(command_t* comm);
void channel_send_bufferlist(cbuflist_t* cbufflist);

#define CMDFLG_OK            0x0000
#define CMDFLG_ERROR         0x0001
#define CMDFLG_BROADCAST     0x0002
#define CMDFLG_FORWARD       0x0004
#define CMDFLG_ACKNOWLEDGE   0x0008
#define CMDFLG_QUERY         0x0010
#define CMDFLG_BINARY        0x0020
#define CMDFLG_ATOMIC        0x0040
#define CMDFLG_COMMITCONF    0x0080
#define CMDFLG_DSTREAM       0x0100

#define IS_COMM_ERROR(cmptr) IS_COMM_ERROR_FLAGS((cmptr)->flags)
#define IS_COMM_ERROR_FLAGS(flags) ((flags) & CMDFLG_ERROR)

#define IS_BINARY_QUERY(cmptr) IS_BINARY_QUERY_FLAGS((cmptr)->flags)
#define IS_BINARY_QUERY_FLAGS(flags) \
        (((flags) & (CMDFLG_QUERY | CMDFLG_BINARY)) == (CMDFLG_QUERY | CMDFLG_BINARY))

#define IS_VALID_COMMAND(cmptr) IS_VALID_COMMAND_FLAGS((cmptr)->flags)
#define IS_VALID_COMMAND_FLAGS(flags) \
        (!((flags) & CMDFLG_ERROR))

#define IS_BINARY_COMMAND(cmptr) IS_BINARY_COMMAND_FLAGS((cmptr)->flags)
#define IS_BINARY_COMMAND_FLAGS(flags) \
        (((flags) & (CMDFLG_QUERY | CMDFLG_BINARY)) == CMDFLG_BINARY)

#define IS_ANSWER_COMMAND(cmptr) IS_ANSWER_COMMAND_FLAGS((cmptr)->flags)
#define IS_ANSWER_COMMAND_FLAGS(flags) \
        ((flags) & (CMDFLG_ACKNOWLEDGE | CMDFLG_QUERY))

#define IS_CONCURRENT_COMMAND(cmptr) \
        ((cmptr)->mentry->flags & _CONCURRENT_COMMAND)

#define IS_FORWARDED_COMMAND(cmptr) \
        ((cmptr)->flags & CMDFLG_FORWARD)

#define NOERROR_ANSWER    "OK"
#define ERROR_ANSWER      "ERROR"

#define CLEAR_BINARY_OUTPUT(comm)   do{(comm)->out_binary.unit = 0;} while(0)
#define IS_BINARY_OUTPUT_DONE(comm) ((comm)->out_binary.unit != 0)

typedef void (*cmdexec_t)(command_t *cmd_id);

void       set_current_command(command_t *comm);
command_t *get_current_command(void);
size_t     extract_command(channel_t* channel, char *ptr, size_t nchars, command_t **comm);
void       complete_command(void *comm);

#define ERROR_FOUND()    IS_COMM_ERROR(get_current_command())
#define ERROR_NOTFOUND() (!ERROR_FOUND())

#define COMMAND_COMMSTR(c)   ((c)->mentry->comm_str)
#define COMMAND_COMMFLAGS(c) ((c)->mentry->flags)
#define COMMAND_COMMID(c)    ((c)->mentry->item)

#define CURRENT_COMMSTR()   COMMAND_COMMSTR(get_current_command())
#define CURRENT_COMMFLAGS() COMMAND_COMMFLAGS(get_current_command())
#define CURRENT_COMMID()    COMMAND_COMMID(get_current_command())

#define IS_CURRENT_APP()    (CURRENT_COMMID() < LAST_APPCOMM_CODE)
#define IS_CURRENT_COMMON() (CURRENT_COMMID() > FIRST_COMMONCOMM_CODE)

uint32_t  calculate_binary_checksum(void *datapt, int unit, int size);
int       send_binary_buffer(mblock_t* mblck, int unit, int size, int flags);
binary_t *get_binary_com_info(void);
int       get_binary_data(void *datapt, int unit, int size);

// prototypes in answer.c

int errorf(const char *fmt, ...);
int error_printf(command_t *comm, const char *fmt, ...);

size_t consume_va_args(va_list *ap, const char *fmt);
int   vanswerf(const char *fmt, va_list ap);
int    answerf(const char *fmt, ...);
void   answer_error_reply(command_t *comm);
void   answer_noerror_reply(command_t *comm);
void   answer_prepare(command_t *comm);
void   answer_check(command_t *comm);

void answer_int8(uint8_t val8, int format);
void answer_int16(uint16_t val16, int format);
void answer_int32(uint32_t val32, int format);
void answer_int64(uint64_t val64, int format);
#define answer_unsigned8(v)  answer_int8(v, DFRMT_UNSIGNED)
#define answer_unsigned16(v) answer_int16(v, DFRMT_UNSIGNED)
#define answer_unsigned32(v) answer_int32(v, DFRMT_UNSIGNED)
#define answer_unsigned64(v) answer_int64(v, DFRMT_UNSIGNED)
#define answer_signed8(v)  answer_int8(v, DFRMT_SIGNED)
#define answer_signed16(v) answer_int16(v, DFRMT_SIGNED)
#define answer_signed32(v) answer_int32(v, DFRMT_SIGNED)
#define answer_signed64(v) answer_int64(v, DFRMT_SIGNED)
#define answer_hexa8(v)  answer_int8(v, DFRMT_HEXA)
#define answer_hexa16(v) answer_int16(v, DFRMT_HEXA)
#define answer_hexa32(v) answer_int32(v, DFRMT_HEXA)
#define answer_hexa64(v) answer_int64(v, DFRMT_HEXA)

#include "menu.h"  // DRFMT_ macros are defined in menu.h

void answer_system_error_info(void);

void  check_for_new_commands(void);

// prototypes in dconfig.c

int dconfig_commentf(const char *comment_fmt, ...);
int dconfig_metadataf(const char* keyword, const char *value_fmt, ...);
int dconfig_commandf(const char *cmd_fmt, ...);
int dconfig_bincommandf(const char *bin_name, const char *cmd_fmt, const char *query_fmt, ...);

void dconfig_mandatory(void);

extern void partial_exec_qDCONFIG(void);

typedef enum {
    DOCTYP_TITLE,
    DOCTYP_DESC,
    DOCTYP_SYNTAX,
    DOCTYP_EX,
    N_DOCTYP
} doctyp_t;

#define HELP_TITLE(__fmt, ...)   helpf(DOCTYP_TITLE,  __fmt, ##__VA_ARGS__)
#define HELP_DESC(__fmt, ...)    helpf(DOCTYP_DESC,   __fmt, ##__VA_ARGS__)
#define HELP_SYNTAX(__fmt, ...)  helpf(DOCTYP_SYNTAX, __fmt, ##__VA_ARGS__)
#define HELP_EXAMPLE(__fmt, ...) helpf(DOCTYP_EX,     __fmt, ##__VA_ARGS__)

void helpf(doctyp_t typ, const char *fmt, ...);


int initfail_hook(void);
int initfail_hook_init(void);
extern int initfail_bit;
#define INITFAIL_HOOK_ENABLE() HOOK_ENABLE(initfail_bit, HOOK_PRECMD)

#endif  // _COMM_H_INCLUDED_

