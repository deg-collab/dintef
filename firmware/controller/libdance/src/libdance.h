/*---------------------------------------------------------------------------
 *  libdance.h
 *
 *  To be included directly only by libdance source files
 *  Application files should only include either dance.h or dancemain.h
 *
 */
#ifndef _LIBDANCE_H_INCLUDED_
#define _LIBDANCE_H_INCLUDED_


#if 0
  #define TODO   TODO:
  #define TODO2  TODO2:
#else
  #define TODO
  #define TODO2
#endif


#define SET_SIMULATION_MODE() (g_libdance.simulmode = 1)

#ifdef NOHARDWARE
   #ifdef SIMULATION_FORBIDDEN
      #error Cannot define SIMULATION_FORBIDDEN with NOHARDWARE
   #endif
   #define IN_SIMULATION_MODE()  1
#elif defined(SIMULATION_FORBIDDEN)
   #define IN_SIMULATION_MODE()  0
#else
   #define SIMULATION_ALLOWED
   #define IN_SIMULATION_MODE()  (g_libdance.simulmode != 0)
#endif


#ifdef WITHIN_DANCE_CONTROLLER
   // Controller definition headers
   #include "dance_defs.h"
#endif

#include "errors.h"
#include "daq.h"
#include "dext.h"
#include "derr.h"
#include "eth.h"
#include "extbuscomm.h"
#include "conf.h"
#include "log.h"
#include "cmdhook.h"

// Error flags definition

// Lower bits more critical
// Upper bits less critical
#define INITFAIL_LIST \
    INITFAIL_ERR(RUNTIME,       0)  \
    INITFAIL_ERR(REGTABLE,      1)  \
    INITFAIL_ERR(IODATA,        2)  \
    INITFAIL_ERR(APP,           3)  \
    INITFAIL_ERR(EXTBUS,        4)  \
    INITFAIL_ERR(ERRORLOG,      5)  \
    INITFAIL_ERR(HWINFO,        6)  \
    INITFAIL_ERR(CONF,          7)  \
    INITFAIL_ERR(ETH,           8)  \
    INITFAIL_ERR(TCP,           9)  \
    INITFAIL_ERR(HTTP,          10) \
    INITFAIL_ERR(COMMLED,       11) \
    INITFAIL_ERR(DAQ,           12) \
    INITFAIL_ERR(DEXT,          13)

#ifdef __INCLUDE_DANCE_DECLARATIONS
#define INITFAIL_ERR(name, pr) const int INITFAIL_ ## name = (1<<pr);
INITFAIL_LIST
#undef  INITFAIL_ERR

#define INITFAIL_ERR(name, pr) const char INITFAIL_ ## name ## _MSG[] = #name;
INITFAIL_LIST
#undef  INITFAIL_ERR

#define INITFAIL_ERR(name, pr) INITFAIL_ ## name ## _MSG,
const char *initfail_msgs[] = { INITFAIL_LIST };
#undef  INITFAIL_ERR
const size_t INITFAIL_COUNT = (sizeof(initfail_msgs)/sizeof(char *));

#else

#define INITFAIL_ERR(name, pr) extern const int INITFAIL_ ## name;
INITFAIL_LIST
#undef  INITFAIL_ERR

#define INITFAIL_ERR(name, pr) extern const char INITFAIL_ ## name ## _MSG[];
INITFAIL_LIST
#undef  INITFAIL_ERR

extern const char   *initfail_msgs[];
extern const size_t  INITFAIL_COUNT;
#endif




extern struct dance_global_s {
#define INITFAIL_OK        0
   uint32_t       errflags;  // initialisation error flags

   hooks_t        hooks;

#define COMMLNK_ETH   (1 << 0)
   uint32_t       commlinks;

   char**         argv;
   eth_handle_t*  eth;

   char*          appname;
   char*          addr;
   char*          tcpport;
   char*          httpport;

   int            simulmode;
} g_libdance;


#ifdef __INCLUDE_DANCE_DECLARATIONS

#  define DANCE_APPNAME(name)               \
      const char danceapp_name[] = name; \
      const char danceapp_tag[]= "$APPNAME: "name" $\n"; \
      const char default_appaddr[]= "MY" name ;

   const char danceapp_ident[] =
#  include "_idfile.h"
   ;

#  define DANCE_VERSION(major, minor)   \
      const int  danceapp_major = (major); \
      const int  danceapp_minor = (minor)

#else

   extern const char danceapp_name[];
   extern const char danceapp_tag[];
   extern const char default_appaddr[];

   extern const int  danceapp_major;
   extern const int  danceapp_minor;

   extern const char danceapp_ident[];
   extern const char libdance_ident[];

#endif  // __INCLUDE_DANCE_DECLARATIONS


// functions to initialise communication sockets
int init_application(void);
void register_fini_application(void (*)(void));
danceerr_t tcp_init(const char *lport);
danceerr_t http_init(void);


// manage interrupt service routines
int register_isr(uint32_t, void (*)(void*), void*);
void unregister_isr(uint32_t);


#endif // _LIBDANCE_H_INCLUDED_
