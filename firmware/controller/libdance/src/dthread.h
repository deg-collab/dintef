
#ifndef __DTHREAD_H_INCLUDED__
#define __DTHREAD_H_INCLUDED__


/*
  Launches execution of a user function in a dedicated thread (dthread).
  The user function can be executed cyclically with a given period.
  Alternatively, the function can loop internally and call thread_wait().
  In both cases the function can receive messages from other threads to execute commands.

*/


typedef enum {
   DTHRCMD_TIMEOUT = -3, //
   DTHRCMD_INIT    = -2, // thread initialisation command
   DTHRCMD_ERROR   = -1, //
   // the negative commands above cannot be issued via dthread_sendcommand()
   DTHRCMD_WAKEUP  =  0, // wake up the thread
   DTHRCMD_FINISH,       // inform the user function of thread finalisation
   DTHRCMD_KILL,         // force thread finalisation (no info to user function)
   DTHRCMD_CMD,          // thread user command
   // DTHRCMD_CMD must be the last in this enum, the user may define
   //   its own commands by using  NEW_DTHRCMD(i)
} dthrcode_t;

#define NEW_DTHRCMD(i) ((dthrcode_t)(DTHRCMD_CMD + (i)))

struct dthread_s;

typedef void (*dthreadmain_t)(dthrcode_t cmd);

typedef struct {
   dthrcode_t code;
   uintptr_t  data;
} dthrcmd_t;

typedef struct dthread_s {
   pthread_t     thread;
   uint32_t      magic;
   dthreadmain_t main_fn;
   size_t        loop_ms;
   struct timespec last_tout;
   int           dthrflags;
#define DTHRFLG_DOINIT     (1 << 0)
#define DTHRFLG_NOWAIT     (1 << 1)
#define DTHRFLG_CONFIRMCMD (1 << 2)
#define DTHRFLG_SINGLERUN  (1 << 3)
#define DTHRFLG_NOCOMMANDS (1 << 4)
#define DTHRFLG_STRICTTIME (1 << 5)
   int           fd_in;
   int           fd_out;

   dthrcmd_t  cmd;
   pthread_cond_t* cond;
//   danceerr_t      errcode;
} dthread_t;


// macros to be used in user functions
#define DTHREAD_CMDCODE()    (dthread_current()->cmd.code))
#define DTHREAD_CMDDATA()    (dthread_current()->cmd.data)
#define DTHREAD_INTERVAL(ms) ((void*)(dthread_current()->loop_ms = (unsigned int)(ms)))
#define DTHREAD_KILL()       (dthread_current()->cmd.code = DTHRCMD_KILL)



int        dthread_start(dthread_t* dthread, dthreadmain_t main_fn, ssize_t loop_ms, int flags);
dthrcode_t dthread_wait(uint32_t ms);
int        dthread_sendcommand(dthread_t* dthread, dthrcode_t cmd, uintptr_t cmd_arg, int flags);
dthread_t* dthread_current(void);
int        dthread_is_running(dthread_t* dthread);

#endif // __DTHREAD_H_INCLUDED__

