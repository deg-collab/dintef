/*--------------------------------------------------------
 *
 *   $Revision: 2214 $
 *
 *   $Id: dancemain.c 2214 2020-05-18 13:03:35Z biv $
 *
 *------------------------------------------------------ */
#include <stdio.h>            // printf
#include <unistd.h>           // open() close()
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <signal.h>
#include <termios.h>
#include <pthread.h>
#include <errno.h>
#include <getopt.h>
#include <stdbool.h>
#include <sys/time.h>         // gettimeofday()
#include <linux/reboot.h>
#include <sys/reboot.h>

#include "libdance.h"

#include "comm.h"
#include "menu.h"
#include "extbuscomm.h"
#include "runtime.h"
#include "conf.h"
#include "daq.h"
#include "dext.h"
#include "hwinfo.h"
#include "commled.h"
#include "eth.h"
#include "derr.h"



/*---------------------------------------------------------------------------
 * Global resources
 */

// Dance default ADDR
#ifdef DANCE_DEFAULT_ADDR
   const char default_useraddr[] = DANCE_DEFAULT_ADDR;
   #define DEFAULT_ADDR default_useraddr
#else
   #define DEFAULT_ADDR default_appaddr
#endif // DANCE_DEFAULT_ADDR


struct dance_global_s g_libdance = {
   .errflags = 0,
   .commlinks = 0,
   .argv = NULL,
   .eth = NULL,
   .appname = (char*)danceapp_name,
   .addr = (char*)DEFAULT_ADDR,
   .tcpport = NULL,
   .httpport =  NULL,
   .simulmode = 0,
};


const char libdance_ident[] =
#include "_idfile.h"
;


/*---------------------------------------------------------------------------
 *
 */
static void free_dance(void) {
   if (g_libdance.addr != DEFAULT_ADDR)
      free(g_libdance.addr);

   if (g_libdance.tcpport)
      free(g_libdance.tcpport);

   if (g_libdance.httpport)
      free(g_libdance.httpport);
}
/*---------------------------------------------------------------------------
 *
 */
static void (*fini_application_fn)(void) = NULL;

void register_fini_application(void (*f)(void)) {
  fini_application_fn = f;
}


/*---------------------------------------------------------------------------
 *
 */
static void sig_handler(int signal) {
   LOG_INFO("Signal received: %d\n", signal);

   if (fini_application_fn != NULL) fini_application_fn();

//   close_sockcomm();
   dance_extbus_close();
   dance_regtable_fini();
   free_dance();
   exit(0);
}




/*---------------------------------------------------------------------------
 * reboot and reset related routines.
 *
 * rebooting has different meanings, hence the different routines:
 * . sync_reboot_cpu: system level LINUX reboot.
 *   complete any filesystem pending operations then reboot the CPU.
 *   it may not shut the power supply down, so auxiliary boards may not be reset.
 * . reload_icap: reload the FPGA bitstream using the Xilinx ICAP mechanism.
 *   it may reboot the CPU. see hw doc,
 * . reset_cpld: on DCOREs and DUBs, a CPLD is present that drives the CPU reset.
 *
 * Note that reseting a remote board (ie. serial ebone case) is application
 * specific, no mechanism has been defined in EBONE yet.
 */

#if 0 /* unused */

static void reload_icap(void)
{
  /* tell the fpga to reload its bitstream stored in flash */
  /* a cpu reboot may be required to renegotiate with the endpoint */
  /* a cpu reboot may automatically occur, cf hardware documentation */

  epcihandle_t bar;

  bar = epci_open("10ee:eb01", NULL, 0);
  if (bar == EPCI_BAD_HANDLE) return ;

  /* write magic to self clear register */
  epci_wr32_reg(bar, 0x140 + 0x0c, 0x5599aa66);

  epci_close(bar);
}

#endif /* unused */

#if 0 /* unused */

__attribute__((unused))
static void reset_cpld(void)
{
  static const uint32_t ctrl_off = 0x00;
  epcihandle_t bar;

  /* it reboots cpu, sync */
  sync();

  bar = epci_open("10ee:eb01", NULL, 0);
  if (bar == EPCI_BAD_HANDLE) return ;

  epci_wr32_reg(bar, ctrl_off + 0x0c, 0x80000000);
  epci_wr32_reg(bar, ctrl_off + 0x08, 0xeb0110ee);

  epci_close(bar);
}

#endif /* unused */

static void sync_reboot_cpu(void) {
   sync();
   reboot(LINUX_REBOOT_CMD_RESTART);
}

static void do_cpu_reboot(void) {
   LOG_INFO("CPU reboot now\n");
   // reboot the processor
   sync_reboot_cpu();
   while(1)
      ;
}


static void do_soft_reboot(void) {
  /* TODO: merge with sig_handler */
  /* TODO: should undo main, init_application ... */

  LOG_INFO("SOFT reboot now\n");

  if (fini_application_fn != NULL) fini_application_fn();


#if COMLED_USED
  commled_fini();
#endif

  /* close_sockcomm(); */
  dance_extbus_close();
  dance_regtable_fini();
  free_dance();

  /* restart software with original command */
  /* assume g_libdance.argv[0] contains full path */
  execve(g_libdance.argv[0], g_libdance.argv, NULL);

  /* execve failed */
  LOG_ERROR("SOFT reboot failed\n");
}

void do_reboot(uint32_t flags) {
  COMMLED_MAIN(ledOFF);
  if (flags & REBOOT_FLAG_CPU) do_cpu_reboot();
  else if (flags & REBOOT_FLAG_SOFT) do_soft_reboot();
  /* else, error */
}


#define COMM_USED (APPCOMM_USED || TCP_USED || HTTP_USED)


// initialize components
static int system_initialise(void) {
   int retval = INITFAIL_OK;

#if COMM_USED
   LOG_INFO("Initialising system error infrastructure\n");
   if (derr_init()) {
      LOG_ERROR("derr_init()\n");
      retval |= INITFAIL_ERRORLOG;
   }
#endif

#if EXTBUS_USED
   LOG_INFO("Initialising external bus(es)\n");
   if (!dance_extbus_init()) {
      LOG_ERROR("Initialisation of external buses failed.\n");
      retval |= INITFAIL_EXTBUS;

   } else {
#if APPREG_USED
      LOG_INFO("Initialising register/memory table\n");
      if (!dance_regtable_init()) {
         LOG_ERROR("Initialisation of external register table failed\n");
         retval |= INITFAIL_REGTABLE;
      }
#endif
   }
#endif // EXTBUS_USED

#if ETHERNET_USED
   LOG_INFO("Initialising Ethernet interface\n");
   if (eth_init() != 0) {
      LOG_ERROR("eth_init() failed\n");
      retval |= INITFAIL_ETH;
      // if failed, missing eth event features
   }
//   commled_update(COMMLED_FLAG_ETH, 1);
#endif

#if COMM_USED
   LOG_INFO("Initialising command infrastructure\n");
   if (iodata_init() != DANCE_OK) {
      LOG_ERROR("iodata_init()\n");
      retval |= INITFAIL_IODATA;
   }
#endif

#if APPCONF_USED
   LOG_INFO("Initialising app configuration infrastructure\n");
   if (conf_init()) {
      LOG_ERROR("conf_init() failed\n");
      // invalid conf but may want conf_erase
      retval |= INITFAIL_CONF;
   }
#endif

   LOG_INFO("Initialising runtime\n");
   if (runtime_init() != DANCE_OK) {
      LOG_ERROR("runtime_init() failed\n");
      retval |= INITFAIL_RUNTIME;
      return retval;
   }

   LOG_INFO("Initialising command hooks\n");
   if (hook_init() != DANCE_OK) {
      LOG_ERROR("hook_init() failed\n");
      retval |= INITFAIL_RUNTIME;
      return retval;
   }

   LOG_INFO("Initialising initfail hook\n");
   if (initfail_hook_init() != DANCE_OK) {
      LOG_ERROR("initfail_hook_init() failed\n");
      retval |= INITFAIL_RUNTIME;
      return retval;
   }

#if TCP_USED
   LOG_INFO("Initialising TCP server\n");
   if (tcp_init(g_libdance.tcpport) != DANCE_OK) {
      LOG_ERROR("Initialisation of TCP failed\n");
      retval |= INITFAIL_TCP;
   }
#endif

#if HTTP_USED
   LOG_INFO("Initialising HTTP server\n");
   if (http_init() != DANCE_OK) {
      LOG_ERROR("Initialisation of HTTP server failed.\n");
      retval |= INITFAIL_HTTP;
   }
#endif

   LOG_INFO("Initialising application specific stuff\n");
   if (init_application() != DANCE_OK) {
      LOG_ERROR("init_application() failed\n");
      retval |= INITFAIL_APP;
      return retval;
   }

   if (!(retval & INITFAIL_EXTBUS)) {
#if COMLED_USED
      LOG_INFO("Initialising COMM LED\n");
      // LED control via I2C bus
      if (commled_init() < 0) {
         LOG_ERROR("commled_init() failed\n");
         retval |= INITFAIL_COMMLED;
      }
#endif
      COMMLED_MAIN(ledRED);

#if BOARDID_USED
      LOG_INFO("Initialising hardware identification\n");
      if (hwinfo_init()) {
         LOG_ERROR("hwinfo_init() failed\n");
         retval |= INITFAIL_HWINFO;
      }
#endif
   }
   COMMLED_MAIN(ledGREEN);

#if DAQ_USED
   LOG_INFO("Initialising DAQ infrastructure\n");
   if (daq_init() != DANCE_OK) {
      LOG_ERROR("daq_init() failed\n");
      retval |= INITFAIL_DAQ;
   }
#endif

#if DEXT_USED
   LOG_INFO("Initialising extension interface\n");
   if (dext_init()) {
      LOG_ERROR("dext_init() failed\n");
      retval |= INITFAIL_DEXT;
   }
#endif

   return retval;
}

#define SIMULATION_IS_AN_OPTION() (!defined(NOHARDWARE) && !defined(SIMULATION_FORBIDDEN))

static void usage(bool doExit) {
   printf("\nUsage: %s [options]\n", danceapp_name);
   printf("      options:\n");
   printf("         -h --help        print out this help\n");
   printf("         -a --addr <ADDR> dance device identifier. Overrides ADDR default\n");
   printf("         -c --cfg <PATH> path where dance configuration is stored\n");
   printf("         -p --port <n>    set communication TCP port\n");
   printf("                    example of port:   5000\n");
   printf("                    example of client: \"telnet host 5000\"\n");
   printf("         -w --httpp <n>   controller listening HTTP port\n");
   printf("                    example of port:   80\n");
   printf("                    example of web url: \"http://host.esrf.fr\"\n");
#if SIMULATION_IS_AN_OPTION()
   printf("         -s --sim         simulation mode\n");
#endif /* SIMULATION_IS_AN_OPTION */
#ifdef LOG_IS_ENABLED
   printf("         -l --log[out_name or class:level] logging setup\n");
#endif /* LOG_IS_ENABLED */
   printf("\n");

   if (doExit) {
      free_dance();
      exit(-1);
   }
}


/*---------------------------------------------------------------------------
 *
 */
int main (int argc, char* argv[]) {
   const char         *port = NULL; // use default socket port number
   struct sigaction    sa;

#define ARG_OPTS ":p:w:a:c:l:sh"

   int idx_opt = 0;
   int curopt = 0;
   static struct option run_opts[] = {
         {"port", required_argument, 0, 'p'},
         {"httpp", required_argument, 0, 'w'},
         {"addr", required_argument, 0, 'a'},
         {"cfg", required_argument, 0, 'c'},
         {"log", required_argument, 0, 'l'},
         {"help", no_argument , 0, 'h'},
         {"sim", no_argument , 0, 's'},
         {0, 0, 0, 0}
    };

   if (LOG_INIT()) {
     printf("LOG_INIT() failed\n");
     exit(-1);
   }


   /* export main args for reboot functions */
   g_libdance.argv = argv;

   while((curopt = getopt_long(argc, argv, ARG_OPTS, run_opts, &idx_opt)) != -1) {
      switch(curopt) {
         case 'h':
            usage(true);
            break;
#ifdef LOG_IS_ENABLED
         case 'l':
            if (log_parse_opt(optarg)){
               printf("Invalid log option: %s\n", optarg);
               usage(false);
            }
            break;
#endif /* LOG_IS_ENABLED */
         case 'p':
            if(!atoi(optarg)) {
               printf("Invalid TCP port number argument: %s\n", port);
               usage(true);
            }
            g_libdance.tcpport = strdup(optarg);
            break;
         case 'w':
            if(!atoi(optarg)) {
               printf("Invalid HTTP port number argument: %s\n", port);
               usage(true);
            }
            g_libdance.httpport = strdup(optarg);
            break;
         case 'a':
            // get the addr string
            g_libdance.addr = strdup(optarg);
            break;
         case 'c':
            strncpy(g_appconf_path, optarg, PATH_MAX);
            break;
#if SIMULATION_IS_AN_OPTION()
         case 's':
            SET_SIMULATION_MODE();
            break;
#endif /* SIMULATION_IS_AN_OPTION */
         case '?':
         default:
            printf("Unknown parameter: %s", run_opts[idx_opt].name);
            if (optarg)
                printf(" with arguments: %s", optarg);
            printf("\n");
            usage(true);
            break;
      }
   }

   LOG_INFO("In simulation mode: %s\n", (IN_SIMULATION_MODE() ? "yes" : "no"));
   LOG_INFO("ADDR: %s\n", g_libdance.addr);
   LOG_INFO("TCP Port number: %s\n", ((g_libdance.tcpport != NULL) ? g_libdance.tcpport : "automatic"));
   LOG_INFO("HTTP Port number: %s\n", ((g_libdance.httpport != NULL) ? g_libdance.httpport : "automatic"));

   // Intercept ending signals, just in case...
   sa.sa_handler = sig_handler;
   sa.sa_flags   = 0;
   sigaction(SIGKILL,&sa,NULL);
   sigaction(SIGINT, &sa,NULL);
   sigaction(SIGQUIT,&sa,NULL);

   g_libdance.errflags = system_initialise();

   if (g_libdance.errflags == 0) {
      LOG_INFO("Initialisation succesfully completed\n");

   } else {
      int idx = 0;
      LOG_ERROR("Initialisation failed (0x%08x)\n", g_libdance.errflags);
      for (idx = 0; idx <= INITFAIL_COUNT; idx++) {
         // get and display the most critical error
         if (g_libdance.errflags & (1 << idx)) {
            COMMLED_ERROR(ledINITERROR, idx + 1);
            break;
         }
      }
      INITFAIL_HOOK_ENABLE();
   }

   LOG_INFO("Starting runtime loop");
   runtime_loop();

   return(0);
}

