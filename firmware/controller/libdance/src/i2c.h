#ifndef I2C_H_INCLUDED
#define I2C_H_INCLUDED

#include <stdint.h>


#define DEFAULT_I2C_BUS "/dev/i2c-2"


// Command status
#define I2C_CMD_ERROR     -1
#define I2C_CMD_IDLE      0
#define I2C_CMD_PROGRESS  1


typedef struct {
#define I2CDRVR_LINUX 0
#define I2CDRVR_EBONE 1
   int         type;
   int         ifd;
   const char* dev_file;
   uint16_t    eaddr;
   pthread_mutex_t  mutex;
   int              mutex_in_use;
} i2c_master_t;


typedef struct {
   int           master_id;
   i2c_master_t* master;
   uint16_t      busaddr;   // address of extender device
   uint16_t      i2ctype;
#define I2CBUS_MASTER 0
#define I2CBUS_EXT    1
} i2c_handle_t;

typedef i2c_handle_t* i2c_phandle_t;


typedef struct i2c_trans_s {
   uint16_t addr;     // slave address
   uint16_t wr_len;   // # of bytes to write
   uint8_t* wr_buf;   // write buffer
   uint16_t rd_len;   // # of bytes to read
   uint8_t* rd_buf;   // read buffer (can be equal to write buffer)
   uint16_t trflags;  // transaction flags
#define _I2C_WR     (1 << 0)
#define _I2C_RD     (1 << 1)
#define _I2C_STOP   (1 << 2)
#define _I2C_10BIT  (1 << 3)
#define _I2C_RCNT   (1 << 4)
} i2c_trans_t;


typedef struct i2c_trdata_s {
   void*    msgs;     //  pointer to table of i2c_msgs (struct i2c_msg*)
   uint16_t nmsgs;    //  number of initialised i2c_msgs
   uint16_t nmaxmsgs; //  number of allocated i2c_msgs
   uint16_t internal; //  flag internal allocation
} i2c_trdata_t;

// This macro can be used to initialise empty i2c_trdata_t structures
#define I2C_TRANSACTION_EMPTY {.msgs = NULL, .nmaxmsgs = 0, .nmsgs = 0}


int i2c_linux_init( i2c_master_t* i2c_h, const char* dev_id );
int i2c_ebone_init( i2c_master_t* i2c_h);
int i2c_fini(i2c_master_t* i2c_h);

int i2c_open( i2c_master_t* i2c_h );
int i2c_close( i2c_master_t* i2c_h );

// functions for generic access
int i2c_prepare_transaction(const char* descr,
                            uint16_t addr,
                            i2c_trans_t* trlist,
                            size_t trans_count,
                            i2c_trdata_t* trdata);

int i2c_execute_transaction(i2c_handle_t* i2c_h, i2c_trdata_t* rdwr_data);

void i2c_empty_transaction(i2c_trdata_t* trdata);


// utility functions
int i2c_adv_transaction(i2c_handle_t* i2c_h, const char*  descr,
                                             int16_t      addr,
                                             i2c_trans_t* trlist,
                                             size_t       trcount);

int i2c_basic_transaction(i2c_handle_t* i2c_h, int16_t  addr,
                                               ssize_t  nwr,
                                               ssize_t  nrd,
                                               uint8_t* wrbuff,
                                               uint8_t* rdbuff);


i2c_handle_t* parse_i2cbus_args(const char* devlbl, uint16_t* addr, uint16_t* offset);


// SMBus specific

#define SMB_NOCMD   (1 << 0)
#define SMB_NOWRITE (1 << 1)
#define SMB_WRCOUNT (1 << 2)
#define SMB_RDCOUNT (1 << 3)
#define SMB_PEC     (1 << 4)



int i2c_smbus_command(i2c_handle_t* i2c_h, int16_t  addr,
                                           uint8_t  flags,
                                           uint8_t  cmd,
                                           uint8_t  nwr,
                                           uint8_t* wrbuff,
                                           uint8_t* nrd,
                                           uint8_t* rdbuff);

// A partial set of macros to implement simple SMBus commands
//
#define SMB_QUICKWRITE(hndl, adr)  \
   i2c_smbus_command(hndl, adr, SMB_NOCMD, 0, 0, NULL, 0, NULL)

#define SMB_QUICKREAD(hndl, adr)  \
   i2c_smbus_command(hndl, adr, SMB_NOCMD | SMB_NOWRITE, 0, 0, NULL, 0, NULL)

#define SMB_SENDBYTE(hndl, adr, byte_var)  \
   i2c_smbus_command(hndl, adr, SMB_NOCMD, 0, 1, (uint8_t*)&byte_var, 0, NULL)

#define SMB_RECEIVEBYTE(hndl, adr, byte_pt)  \
   i2c_smbus_command(hndl, adr, SMB_NOCMD | SMB_NOWRITE, 0, 0, NULL, 1, (uint8_t*)byte_pt)

static inline int SMB_WRITEBYTE(i2c_handle_t* hndl, int16_t adr, uint8_t cmd, uint8_t byte_var) {
   return i2c_smbus_command(hndl, adr, 0, cmd, 1, &byte_var, 0, NULL);
}

static inline int SMB_READBYTE(i2c_handle_t* hndl, int16_t adr, uint8_t cmd, uint8_t* byte_pt) {
   uint8_t nrd = 1;
   return i2c_smbus_command(hndl, adr, 0, cmd, 0, NULL, &nrd, byte_pt);
}

static inline int SMB_WRITEWORD(i2c_handle_t* hndl, int16_t adr, uint8_t cmd, uint16_t word_var) {
   return i2c_smbus_command(hndl, adr, 0, cmd, 2, (uint8_t*)&word_var, 0, NULL);
}

static inline int SMB_READWORD(i2c_handle_t* hndl, int16_t adr, uint8_t cmd, uint16_t* word_pt) {
   uint8_t nrd = 2;
   return i2c_smbus_command(hndl, adr, 0, cmd, 0, NULL, &nrd, (uint8_t*)word_pt);
}


#endif /* I2C_H_INCLUDED */