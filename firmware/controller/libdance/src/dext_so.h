#ifndef DEXT_SO_H_INCLUDED
#define DEXT_SO_H_INCLUDED


typedef struct
{
#define DEXT_OPS_VERS 0
  unsigned int vers;

  /* libc */
  int (*printf)(const char*, ...);
  void* (*malloc)();
  void (*free)(void*);

  /* math */
  double (*sin)(double);
  double (*asin)(double);
  double (*cos)(double);
  double (*acos)(double);
  double (*tan)(double);
  double (*atan)(double);
  double (*pow)(double, double);
  double (*sqrt)(double);
  double (*fabs)(double);

  /* runtime */
  int (*register_menu)();

} dext_ops_t;


#ifdef DEXT_COMPILE_SO

static dext_ops_t g_ops;

int register_ops(const dext_ops_t* ops)
{
  if (ops->vers != DEXT_OPS_VERS) return -1;
  g_ops = *ops;
  return 0;
}

#define printf g_ops.printf
#define malloc g_ops.malloc
#define free g_ops.free

#define sin g_ops.sin
#define asin g_ops.asin
#define cos g_ops.cos
#define acos g_ops.acos
#define tan g_ops.tan
#define atan g_ops.atan
#define pow g_ops.pow
#define sqrt g_ops.sqrt
#define fabs g_ops.fabs

#endif /* DEXT_COMPILE_SO */


#endif /* DEXT_SO_H_INCLUDED */
