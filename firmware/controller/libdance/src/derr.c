#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>

#include "libdance.h"

#include "derr.h"


typedef struct {
#define DERR_FLAG_VALID (1 << 0)
#define DERR_FLAG_FREES (1 << 1)
   volatile uint32_t flags;
   struct timeval    tm;
   const char*       file;
   uint32_t          line;
   const char*       s;
} derr_item_t;


/* global state */

#define DERR_COUNT 32

#define DERR_FLAG_OVERFLOW (1 << 0)
#define DERR_FLAG_INIT (1 << 1)
static uint32_t        derr_flags;
static pthread_mutex_t derr_lock;
static volatile size_t derr_pos;
static derr_item_t     derr_buf[DERR_COUNT];
//static const size_t    derr_count = sizeof(derr_buf) / sizeof(derr_buf[0]);
struct timeval         derr_tm;
static uint32_t        derr_cur;
static uint32_t        derr_tot;

/* local functions prototypes */
static void derr_clear_item(derr_item_t* );


#define CYCLIC_BUFFER
static derr_item_t* derr_alloc_item(void) {
   derr_item_t* di;

   pthread_mutex_lock(&derr_lock);
   derr_tot++;
   derr_cur++;
#ifdef CYCLIC_BUFFER
   if (derr_pos != DERR_COUNT) {
      di = &derr_buf[derr_pos++];
      di->flags = 0;
   } else {
      derr_pos = 0;
      derr_flags |= DERR_FLAG_OVERFLOW;
      di = &derr_buf[derr_pos++];
      derr_clear_item(di);
   }
#else
   if (derr_pos != DERR_COUNT) {
      di = &derr_buf[derr_pos++];
      di->flags = 0;
   } else {
      di = NULL;
      derr_flags |= DERR_FLAG_OVERFLOW;
   }
#endif

   pthread_mutex_unlock(&derr_lock);
   return di;
}

static derr_item_t* derr_alloc_init_item(const char* file, uint32_t line,
                                         const char* msg, uint32_t flags) {
   derr_item_t*   derr;
   struct timeval tm_now;
   struct timeval tm_dif;

   // LOG_ERROR("%s,%u %s\n", file, line, (msg == NULL) ? "none" : msg);

   derr = derr_alloc_item();
   if (derr == NULL)
      return NULL;

   derr->flags = flags & ~DERR_FLAG_VALID;

   /* TODO: use dtime */
   /* time in seconds OK for error timing */
   /* more accurate is error specific */
   gettimeofday(&tm_now, NULL);
   timersub(&tm_now, &derr_tm, &tm_dif);
   derr->tm.tv_sec  = tm_dif.tv_sec;
   derr->tm.tv_usec = tm_dif.tv_usec;

   derr->file = file;
   derr->line = line;
   derr->s = msg;

   /* commit only once thread safe */
   derr->flags |= DERR_FLAG_VALID;
   return derr;
}


static unsigned int derr_is_init(void) {
   return (unsigned int)(derr_flags & DERR_FLAG_INIT);
}


/* exported */

int derr_init(void) {
   derr_flags = 0;

   if (pthread_mutex_init(&derr_lock, NULL))
      return -1;
   gettimeofday(&derr_tm, NULL);
   derr_flags |= DERR_FLAG_INIT;
   derr_pos = 0;
   derr_tot = 0;
   derr_cur = 0;
   return 0;
}

void derr_fini(void) {
   if (derr_is_init() == 0) return ;

   derr_pos = 0;
   pthread_mutex_destroy(&derr_lock);

   derr_flags = 0;
}

static void derr_clear_item(derr_item_t* const di) {
   if (di->flags & DERR_FLAG_VALID && di->flags & DERR_FLAG_FREES)
      free((void*)di->s);
   di->flags = 0;
}
static void derr_clear(void) {
   size_t i;

   // assume here derr_is_init
   pthread_mutex_lock(&derr_lock);

   for (i = 0; i != derr_pos; ++i) {
      derr_item_t* const di = &derr_buf[i];
      derr_clear_item(di);
   }
   derr_cur = 0;
   derr_pos = 0;
   derr_flags &= ~DERR_FLAG_OVERFLOW;
   pthread_mutex_unlock(&derr_lock);
}

int derr_prints(const char* file, uint32_t line, const char* msg) {
   if (derr_is_init() == 0 || derr_alloc_init_item(file, line, msg, 0) == NULL)
      return -1;
   else
      return 0;
}


int derr_printf(const char* file, uint32_t line, const char* fmt, ...) {
   char*        msg;
   size_t       max_n;
   va_list      ap;
   int          nchar;
   derr_item_t* di;
   int          err = -1;

   if (derr_is_init() == 0)
      goto on_error_0;

   va_start(ap, fmt);

   max_n = 128;
   msg = malloc(max_n * sizeof(char));
   if (msg == NULL) goto on_error_1;

   nchar = vsnprintf(msg, max_n, fmt, ap);

   if (nchar < 0) goto on_error_2;

   /* a return value of size or more means truncation */
   if (nchar >= max_n) {
      free(msg);

      /* return value excludes the terminating 0 */
      max_n = nchar + 1;
      msg = malloc(max_n * sizeof(char));
      if (msg == NULL) goto on_error_1;

      nchar = vsnprintf(msg, max_n, fmt, ap);
      if (nchar < 0) goto on_error_2;
   }

   di = derr_alloc_init_item(file, line, msg, DERR_FLAG_FREES);
   if (di == NULL) goto on_error_2;

   err = 0;

 on_error_2:
   if (err) free(msg);
 on_error_1:
   va_end(ap);
 on_error_0:
   return err;
}


void help_DERR(void) {
   HELP_SYNTAX("DERR [<number_of_fake_errors>]\n");
}

void commexec_DERR(void) {
   int nerr;
   size_t i, n;

   nerr = I_INTEGER();
   n = derr_tot + 1;
   for (i = 0;i < nerr;i++) {
      LOG_ERROR("fake error #%d\n", i + n);
   }
}


void help_qDERR(void) {
   HELP_SYNTAX("?DERR [ALL  FIRST LAST] [NOCLEAR]\n");
   HELP_SYNTAX("?DERR [ANY] [NTOT] [NBUFF] [NOCLEAR]\n");

   HELP_EXAMPLE(
      ">>>?DERR ANY NOCLEAR\n"
      "10\n"
   );
   HELP_EXAMPLE(
      "\n"
      ">>>?DERR ANY NTOT\n"
      "10 errors since last clear\n"
      "17 errors since restart\n"
   );
   HELP_EXAMPLE(
      "\n"
      ">>>?DERR LAST NOCLEAR\n"
      "io.c:1176 64.028713 mismatch in input configuration read back\n"
   );
   HELP_EXAMPLE(
      "\n"
      ">>>?DERR\n"
      "10 errors since last clear\n"
      "17 errors since restart\n"
      "10 errors in buffer\n"
      "io.c:1176 33.817230 mismatch in input configuration read back\n"
      "...\n"
      "io.c:1176 64.028713 mismatch in input configuration read back\n"
   );
}

extern const char *_opt_DERRFLAG[];
void commexec_qDERR(void) {
   id_integer_t args;
   size_t       n = 0;
   size_t       i = 0;
   int          single_qp;


   if (derr_is_init() == 0) {
      errorf("DERR subsystem not initialized");
      goto on_error;
   }

   /* get argin bit mask */
   args = I_FLGLIST();

   /* default values to all */
   if (args == DEFAULT_FLAGLIST) {
      args = DERRFLAG_ALL;
   }

   /* default if no human readable query */
   if (!(args & DERRFLAGS_QH) && !(args & DERRFLAGS_QP)) {
      args |= DERRFLAG_ALL;
   }

   for (i = 0;i < commlst_DERRFLAG.n;i++) {
      if (args & (1<<i)) {
         LOG_COMM1("Flag: %s\n", _opt_DERRFLAG[i]);
      }
   }

   /* detect single flag for shortest parsable answer */
   for (single_qp =(args & DERRFLAGS_QP);single_qp;) {
      if ((single_qp & DERRFLAG_NTOT) == single_qp)
         break;
      if ((single_qp & DERRFLAG_NBUFF) == single_qp)
         break;
      if ((single_qp & DERRFLAG_ANY) == single_qp)
         break;
      single_qp = 0;
   }

   /* check incompatible flags combinations */
   if (single_qp && (args & DERRFLAGS_QH))
      single_qp = 0;
   
   /* prepare human readable answer */
   if (args & DERRFLAG_ALL) {
      i = 0;
      n = derr_pos;
      if (derr_flags & DERR_FLAG_OVERFLOW) {
          i = derr_pos;
          n = 32;
      }
   }

   /* prepare human readable answer */
   if (args & DERRFLAG_FIRST) {
      i = 0;
      n = derr_pos?1:0;
      if (derr_flags & DERR_FLAG_OVERFLOW) {
          i = derr_pos;
          n = 1;
      } 
   }

   /* prepare human readable answer */
   if (args & DERRFLAG_LAST) {
      if (derr_pos) {
         i = derr_pos - 1;
         n = 1;
      } else {
         i = 0;
         n = 0;
         if (derr_flags & DERR_FLAG_OVERFLOW) {
            i = DERR_COUNT - 1;
            n = 1;
         }
      }
   }

   /* parsable minimum answer */
   if ((args & DERRFLAG_ANY) || (args & DERRFLAG_ALL)) {
      answerf("%d%s\n",
         derr_cur,
         (single_qp?"":" errors since last clear"));
   }

   /* parsable minimum answer */
   if ((args & DERRFLAG_NTOT) || (args & DERRFLAG_ALL)) {
      answerf("%d%s\n",
         derr_tot,
         (single_qp?"":" errors since restart"));
   }

   /* parsable minimum answer */
   if ((args & DERRFLAG_NBUFF) || (args & DERRFLAG_ALL)) {
      answerf("%d%s\n",
         (derr_flags & DERR_FLAG_OVERFLOW)?32:derr_pos,
         (single_qp?"":" errors in buffer"));
   }

   /* human readable answer */
   if ((args & DERRFLAGS_QH)) {
      if (n == 0) {
         answerf("NONE\n");
      } else {
         for (; n--; ++i) {
            if (i == DERR_COUNT) i = 0;
            const derr_item_t* const di = &derr_buf[i];
      
            if ((di->flags & DERR_FLAG_VALID) == 0) continue ;

            answerf("%s:%u %u.%06u", di->file, di->line, 
               di->tm.tv_sec, di->tm.tv_usec);

            if (di->s != NULL) answerf(" %s", di->s);
         }
      }
   }

   /* by default clear the history */
   if (!(args & DERRFLAG_NOCLEAR)) {
      derr_clear();
   }

on_error:
    return;
}


#if 0 /* unit */

int main(int ac, char** av)
{
   derr_init();

   DERR_PRINTF("invalid configuration for CNT%u", 12);
   DERR_PRINTS("invalid configuration for bar");
   DERR_TRACE();

   commexec_qDERR();

   derr_clear();

   derr_fini();

   return 0;
}

#endif /* unit */
