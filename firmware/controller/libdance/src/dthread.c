
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>

#include "dthread.h"
#include "runtime.h"
#include "log.h"
#include "derr.h"

static __thread dthread_t* gthr_dthread;

#define DTHREAD_MAGIC 0xd0d01234


int dthread_is_running(dthread_t* dthread) {
   return dthread->magic == DTHREAD_MAGIC;
}


dthread_t* dthread_current(void) { return gthr_dthread;}


int dthread_sendcommand(dthread_t* dthread, dthrcode_t cmd, uintptr_t data, int flags) {
   dthrcmd_t command = {.code = cmd, .data = data};

   if (!dthread_is_running(dthread)) {
      LOG_ERROR("target thread is not running\n");
      return -1;
   } else if (dthread->fd_in < 0) {
      LOG_ERROR("target thread does not listen for external commands\n");
      return -1;
   }
   if (write(dthread->fd_in, &command, sizeof(dthrcmd_t)) != sizeof(dthrcmd_t)) {
      LOG_ERROR("write error (errno == %d) in fd:%d\n", errno, dthread->fd_in);
      return -1;
   }
   return 0;
}


dthrcode_t dthread_wait(uint32_t ms) {
   dthread_t*      dthread = gthr_dthread;
   fd_set          rset;
   int             fd_max;
   int             nfd;
   struct timeval  tv;
   struct timeval* timeout;

   if (dthread->fd_out < 0) {
      usleep(ms * 1000);
      return DTHRCMD_TIMEOUT;
   }

   LOG_INFO("In dthread_wait(): %d\n", ms);

   if (ms > 0) {
      int32_t t_ms = ms;

      if (dthread->dthrflags & DTHRFLG_STRICTTIME) {
         struct timespec* last_t = &dthread->last_tout;
         struct timespec  now;

         clock_gettime(CLOCK_REALTIME, &now);

         if (last_t->tv_sec == 0 && last_t->tv_nsec == 0) {
            *last_t = now;
         } else {
            int pre_ms = (now.tv_sec - last_t->tv_sec) * 1000 +
                         (now.tv_nsec - last_t->tv_nsec) / 1000000;
            t_ms = ms - pre_ms;
            if (t_ms < 0) {   // if time overrun, reset dthread->last_tout
               *last_t = now;
               t_ms = 0;
               ms = 0;
            }
         }
      }
      tv.tv_sec = t_ms / 1000;
      tv.tv_usec = (t_ms % 1000) * 1000;
      timeout = &tv;
   } else
      timeout = NULL;

   FD_ZERO(&rset);
   FD_SET(dthread->fd_out, &rset);
   fd_max = dthread->fd_out;

 redo_select:
   LOG_INFO("Entering select()\n");
   errno = 0;
   nfd = select(fd_max + 1, &rset, NULL, NULL, timeout);

   if (nfd < 0) {
      if (errno == EINTR) goto redo_select;  // if interrupted by a signal handler, repeat
      LOG_ERROR("Select() error (errno == %d)\n", errno);
      goto on_error;

   } else if (nfd == 0) {  // timeout happened, process it
      LOG_INFO("timeout reached in select()\n");
      dthread->cmd.code = DTHRCMD_TIMEOUT;

      if (dthread->dthrflags & DTHRFLG_STRICTTIME) {
         dthread->last_tout.tv_sec += ms / 1000;
         dthread->last_tout.tv_nsec += (ms % 1000) * 1000000;
         if (dthread->last_tout.tv_nsec > 1000000000) {
            dthread->last_tout.tv_sec += 1;
            dthread->last_tout.tv_nsec -= 1000000000;
         }
      }
   } else if (FD_ISSET(dthread->fd_out, &rset)) {
      int  nrecv;

//      LOG_INFO("command received\n");
      errno = 0;
      nrecv = read(dthread->fd_out, &dthread->cmd, sizeof(dthrcmd_t));
      if (errno == EAGAIN) {
         goto on_error;
      } else if (nrecv != sizeof(dthrcmd_t)) {
         LOG_ERROR("read error (errno == %d)\n", errno);
         goto on_error;
      }
   } else {
      goto on_error;
   }
   LOG_INFO("returning command: %d (%"PRIdPTR")\n", dthread->cmd.code, dthread->cmd.data);
   return dthread->cmd.code;

 on_error:
   SYSTEM_ERROR();
   dthread->cmd.code = DTHRCMD_ERROR;
   return DTHRCMD_ERROR;
}


static int dthread_init(dthread_t* dthread, dthreadmain_t main_fn, size_t loop_ms, int flags) {
   dthread->magic = ~DTHREAD_MAGIC;
   dthread->main_fn = main_fn;
   dthread->loop_ms = loop_ms;
   dthread->dthrflags = flags;
   dthread->cmd.code = DTHRCMD_WAKEUP;
   dthread->cmd.data = 0;

   if (flags & DTHRFLG_NOCOMMANDS) { // flag no pipe
      dthread->fd_in = -1;
      dthread->fd_out = -1;

   } else {                          // open command pipe
      int fd[2];

      if (pipe(fd)) {
         LOG_ERROR("cannot open command pipe\n");
         return -1;
      }
      set_nonblock(fd[0]);
      dthread->fd_in = fd[1];
      dthread->fd_out = fd[0];
   }
   return 0;
}


static void dthread_fini(dthread_t* dthread) {
   dthread->magic = 0;
   if (dthread->fd_out >= 0) {
      close(dthread->fd_out);
      close(dthread->fd_in);
   }
}


static void* dthread_launcher(void* arg) {
   dthread_t* dthread = arg;

   // store dthread pointer in __thread global variable
   gthr_dthread = dthread;

   dthread->magic = DTHREAD_MAGIC;

   if (dthread->dthrflags & DTHRFLG_SINGLERUN) {
      dthread->main_fn(DTHRCMD_INIT);

   } else {
      if (dthread->dthrflags & DTHRFLG_DOINIT) {
         dthread->main_fn(DTHRCMD_INIT);
         if (dthread->cmd.code == DTHRCMD_KILL)
            goto on_thread_done;
      }
      while(1) {
         dthrcode_t cmd;

         if (dthread->dthrflags & DTHRFLG_NOWAIT) {
            cmd = DTHRCMD_TIMEOUT;
         } else {
            cmd = dthread_wait(dthread->loop_ms);
            if (cmd == DTHRCMD_KILL)
               goto on_thread_done;
         }

         dthread->main_fn(cmd);
         if (dthread->cmd.code == DTHRCMD_KILL ||
             dthread->cmd.code == DTHRCMD_FINISH)
            goto on_thread_done;
      }
   }

 on_thread_done:
   // some housekeeping and resource release
   dthread_fini(dthread);

   LOG_INFO("thread terminated\n");
   return NULL;
}


// Starts a new dance thread, returns -1 if fails
//
int dthread_start(dthread_t* dthread, dthreadmain_t main_fn, ssize_t loop_ms, int flags) {
   if (dthread_is_running(dthread)) {
      LOG_ERROR("dance thread is already running\n");
      return -1;

   } else if (dthread_init(dthread, main_fn, loop_ms, flags) != 0) {
      return -1;

   } else if (pthread_create(&dthread->thread, NULL, dthread_launcher, dthread) != 0 ) {
      LOG_ERROR("New thread creation failed\n");
      dthread_fini(dthread);
      return -1;
   }

   // wait for the thread start or terminate
   while(dthread->magic != DTHREAD_MAGIC && dthread->magic != 0)
      usleep(10);

   // thread was successfully started
   pthread_detach(dthread->thread);  // disassociate from parent
   return 0;
}

