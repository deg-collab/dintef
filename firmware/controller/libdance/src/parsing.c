
#include <string.h>

#include "parsing.h"
#include "comm.h"
#include "log.h"

const format_t default_format = DEFAULT_FORMAT;

const id_boolean_t _default_boolean = DEFAULT_BOOLEAN;
const id_tag_t     _default_tag = DEFAULT_TAG;
const id_flglist_t _default_flglist = DEFAULT_FLAGLIST;
const id_choice_t  _default_choice = DEFAULT_CHOICE;
const id_integer_t _default_integer = DEFAULT_INTEGER;
const id_hexa_t    _default_hexa = DEFAULT_HEXA;
const id_floatp_t  _default_floatp = DEFAULT_FLOAT;
const id_floatp_t  _default_unit = DEFAULT_UNIT;
const id_integer_t _default_composite = DEFAULT_COMPOSITE;

const char *emtpty_string =  "";  // Used as default value for LABEL and STRING types

const char fmt_types[] = FMT_TYPES;
const char fmt_etypes[] = FMT_ETYPES ;
const char fmt_separators[] = FMT_SEPARATORS;


void       free_composite_item(iodata_t *composite);
errmsg_t   do_parse_input_sequence(iodata_t *inpdata, const char **params, itlmask_t *fmtmask, int *done);

void       reset_iodata(iodata_t *iodata);
void       empty_iodata(iodata_t *iodata);

void       answer_output_composite(iodata_t *output_data);
void       empty_itemlist(id_itemlist_t *);


void init_itemlist(id_itemlist_t *itemlist, iodata_t *iodata, format_t *fmt) {
   if (fmt)
      itemlist->fmt = *fmt;
   else
      itemlist->fmt = default_format;

   itemlist->used  = 0;
   itemlist->current = 0;
   itemlist->available = ID_DEF_NITEMS;
   itemlist->more_items = NULL;
   itemlist->itl_parent = iodata;

   if (IS_COMPOSITE_FMT(fmt->type))
      itemlist->fmt.sp.compvalid = 0;
}

id_item_t *item_ptr(id_itemlist_t *itemlist, int idx) {
   if (idx < ID_DEF_NITEMS)
      return &itemlist->def_items[idx];
   else
      return &itemlist->more_items[idx - ID_DEF_NITEMS];
}

errmsg_t add_item_to_itemlist(id_itemlist_t *itemlist, id_item_t *item) {
   if (IS_COMPOSITE_FMT(itemlist->fmt.type) && itemlist->fmt.sp.compvalid == 0) {
      *item_ptr(itemlist, 0) = *item;
      itemlist->used = 1;
      itemlist->fmt.sp.compvalid = 1;

   } else {
      if (itemlist->used == itemlist->available) {
         id_item_t *more_items;

         more_items = realloc(itemlist->more_items, itemlist->available * sizeof(id_item_t));
         if (more_items == NULL)
            return(NOMEMORYMESSAGE);
         else {
            itemlist->more_items = more_items;
            itemlist->available += ID_DEF_NITEMS;
         }
      }
      *item_ptr(itemlist, itemlist->used) = *item;
      itemlist->used++;
      if (IS_COMPOSITE_FMT(itemlist->fmt.type))
         itemlist->fmt.sp.compvalid = itemlist->used;
   }
   return(NOERROR);
}

void empty_itemlist(id_itemlist_t *itemlist) {
   if (IS_COMPOSITE_FMT(itemlist->fmt.type)) {
      int i;

      LOG_COMM1("emptying composite itemlist\n");
      if (itemlist->used > 0) {
         empty_iodata(item_ptr(itemlist, 0)->composite);
         for (i = 1; i < itemlist->used; i++)
            free_composite_item(item_ptr(itemlist, i)->composite);
         itemlist->used = 1;
         itemlist->fmt.sp.compvalid = 0;
      }
   } else
      itemlist->used  = 0;

   itemlist->current = 0;
   itemlist->available = ID_DEF_NITEMS;
   free(itemlist->more_items);
   itemlist->more_items = NULL;
}


void free_itemlist(id_itemlist_t *itemlist) {
   if (IS_COMPOSITE_FMT(itemlist->fmt.type)) {
      int i;

      for (i = 0; i < itemlist->used; i++)
         free_composite_item(item_ptr(itemlist, i)->composite);
   }
   if (itemlist->more_items)
      free(itemlist->more_items);
//   init_itemlist(itemlist, NULL, NULL);
}

void show_item(char itemseparator, format_t *fmt, id_item_t *item) {
   switch(fmt->type) {
      case FMT_LABEL:
      case FMT_STRING:
         LOG_COMM1("\"%.*s\"", (int)item->label.length, item->label.ptr);
         break;

      case FMT_TAG:
         LOG_COMM1("%.*s", fmt->sp.taglen, (char *)fmt->tlptr);
         break;

      case FMT_BOOLEAN: {
         arglists_t *arglst = fmt->tlptr;
         int         idx = fmt->sp.boolit <= 0? 0 : fmt->sp.boolit;
         LOG_COMM1("%s", item->boolean? arglst->truevalue[idx] : arglst->falsevalue[idx]);
         break;
      }

      case FMT_INTEGER:
         LOG_COMM1("%" PRIinteger, item->integer);
         break;

      case FMT_HEXA:
         if (fmt->sp.nbits == 0)
            LOG_COMM1("0x%" PRIhexa, item->hexa);
         else {
            int len = (fmt->sp.nbits - 1) / 4 + 1;
            LOG_COMM1("0x%0*"PRIhexa, len, item->hexa);
         }
         break;

      case FMT_FLOAT:
         LOG_COMM1("%Lg", item->floatp);
         break;

      case FMT_UNIT:
         LOG_COMM1("%d:%s:%Lg", item->unit, ((unittype_t *)(fmt->tlptr))->units[item->unit], ((unittype_t *)(fmt->tlptr))->factors[item->unit]);
         break;

      case FMT_CHOICE:
         LOG_COMM1("%d:%s", item->choice, ((list_t *)(fmt->tlptr))->options[item->choice]);
         break;

      case FMT_FLAGLIST: {
            id_flglist_t bit ;
            int          i;
            list_t      *fmtlist = fmt->tlptr;

            LOG_COMM1("0x%" PRIhexa " :", item->flglist);
            for (bit = 1, i = 0; i < fmtlist->n; bit <<= 1, i++) {
               if (item->flglist & bit)
                  LOG_COMM1(" %s", fmtlist->options[i]);
            }
         }
         break;

      case FMT_SCOMPOSITE:
      case FMT_UCOMPOSITE:
      default:
         LOG_ERROR("SYSTEM ERROR\n");
         break;
   }
   if (itemseparator)
      LOG_COMM1("%c ", itemseparator);
}

#if 0
void show_format(format_t *fmt) {
   LOG_COMM1("---- Showing format info\n");
   LOG_COMM1("type = %c\n", fmt_etypes[fmt->type]);
   LOG_COMM1("n_rep = %d\n", (int)fmt->n_rep);
   LOG_COMM1("flags = 0x%02X\n", (int)fmt->flags);
//   LOG_COMM1("optional = %d\n", (int)fmt->optional);
   LOG_COMM1("uns_pos = %d\n", (int)fmt->uns_pos);
   LOG_COMM1("sp = %d\n", (int)fmt->sp);
   if (fmt->type == FMT_INTEGER) {
      LOG_COMM1("min.intval = %lld\n", (long long)fmt->min.intval);
      LOG_COMM1("max.intval = %lld\n", (long long)fmt->max.intval);
   } else if (fmt->type == FMT_FLOAT) {
      LOG_COMM1("min.fltval = %g\n", (float)fmt->min.fltval);
      LOG_COMM1("max.fltval = %g\n", (float)fmt->max.fltval);
   }
   if (fmt->type == FMT_TAG)
      LOG_COMM1("tlptr(tag) = %.*s\n", fmt->sp.taglen, (char *)fmt->tlptr);
   else if (fmt->type == FMT_BOOLEAN)
      LOG_COMM1("tlptr(boolean) = %s\n", ((arglists_t *)(fmt->tlptr))->truevalue[fmt->sp.boolit]);
   else if (fmt->type == FMT_UNIT)
      LOG_COMM1("tlptr(unit) = %s\n", ((unit_t *)(fmt->tlptr))->name);
   else if (fmt->tlptr)
      LOG_COMM1("tlptr(list) = %s\n", ((list_t *)(fmt->tlptr))->name);
   else
      LOG_COMM1("tlptr(null) = %s\n", fmt->tlptr);
}
#endif

void show_format_type(format_t *fmt) {
   if (fmt->idlen)
      LOG_COMM1("%c%.*s%c ", FMT_ID_BEG, fmt->idlen, fmt->idptr, FMT_ID_END);
   LOG_COMM1("%s%c", IS_OPTIONAL(fmt)? "o":"", fmt_types[fmt->type]);
   switch(fmt->type) {
      case FMT_BOOLEAN:
         if (fmt->sp.boolit >= 0)
            LOG_COMM1("#%s", ((arglists_t *)(fmt->tlptr))->truevalue[fmt->sp.boolit]);
         break;
      case FMT_TAG:
         LOG_COMM1("#%.*s", fmt->sp.taglen, (char *)fmt->tlptr);
         break;
      case FMT_CHOICE:
      case FMT_FLAGLIST:
         LOG_COMM1("#%s", ((list_t *)(fmt->tlptr))->name);
         break;
      case FMT_UNIT:
         LOG_COMM1("#%s", ((unittype_t *)(fmt->tlptr))->name);
         break;
      default:
         break;
   }
}

void _show_itemlist(int indent, id_itemlist_t *itemlist, int parent, int expand) {
   format_t *fmt = &itemlist->fmt;
   int i;

   if (parent) {
      iodata_t *parent = itemlist->itl_parent;
      LOG_COMM1("Container (current/used):  %d/%d\n", parent->current, parent->used);
   }
   if (fmt->separator)
      LOG_COMM1("%*s%c\n", 20 + indent, "", fmt_separators[fmt->separator]);

   if (IS_COMPOSITE_FMT(fmt->type)) {
      LOG_COMM1("Show items: %d/%d/%d/%d", fmt->sp.compvalid, fmt->n_rep, itemlist->current, itemlist->used);
      LOG_COMM1("%*s %s <0x%02x>: ", indent, "", "composite", fmt->flags);
      show_format_type(fmt); LOG_COMM1("%s\n", fmt->sp.compvalid > 0? "" : " <empty>");
      if (expand) {
         for (i = 0; i < itemlist->used; i++)
            show_io_format(indent + 2, item_ptr(itemlist, i)->composite);
      }
   } else {
      LOG_COMM1("Show items: %d/%d/%d/%d", itemlist->used, fmt->n_rep, itemlist->current, itemlist->used);
      LOG_COMM1("%*s %s ", indent, "", "format");
      show_format_type(fmt); LOG_COMM1(" <0x%02x>: ", fmt->flags);
      for (i = 0; i < itemlist->used; i++)
         show_item(i?',':' ', fmt, item_ptr(itemlist, i));
      LOG_COMM1("\n");
   }
}

void show_itemlist(id_itemlist_t *itemlist) {
   _show_itemlist(0, itemlist, 1, 0);
}

void show_itemlist_full(id_itemlist_t *itemlist) {
   _show_itemlist(0, itemlist, 1, 1);
}

id_itemlist_t *itemlist_ptr(iodata_t *iodata, int idx) {
   if (idx < ID_DEF_NARGS)
      return &iodata->def_itemlists[idx];
   else
      return &iodata->more_itemlists[idx - ID_DEF_NARGS];
}


id_itemlist_t *allocate_itemlist(iodata_t *iodata, format_t *fmt) {
   id_itemlist_t *itemlist;

   if (iodata->used == iodata->available) {
      itemlist = realloc(iodata->more_itemlists, iodata->available * sizeof(id_itemlist_t));
      if (itemlist == NULL)
         return(NULL);
      else {
         iodata->more_itemlists = itemlist;
         iodata->available += ID_DEF_NARGS;
      }
   }
   itemlist = itemlist_ptr(iodata, iodata->used);
   iodata->used++;
   init_itemlist(itemlist, iodata, fmt);
   return(itemlist);
}

void empty_iodata(iodata_t *iodata) {
   int i;

   for (i = 0; i < iodata->used; i++)
      empty_itemlist(itemlist_ptr(iodata, i));
}

void answer_output_item(id_item_t *item, format_t *fmt) {
   if (!SUPPRESS_WSPACE(fmt))
      answerf(" ");

   switch(fmt->type) {
      case FMT_LABEL:
      case FMT_STRING:
         answerf("%.*s", (int)item->label.length, item->label.ptr);
         break;

      case FMT_TAG:
         if (item->tag)
            answerf("%.*s", fmt->sp.taglen, (char *)fmt->tlptr);
         break;

      case FMT_BOOLEAN: {
         arglists_t *arglst = fmt->tlptr;
         int         idx = fmt->sp.boolit >= 0? fmt->sp.boolit : 0;
         answerf("%s", item->boolean? arglst->truevalue[idx] : arglst->falsevalue[idx]);
         break;
      }

      case FMT_INTEGER:
         answerf("%" PRIinteger, item->integer);
         break;

      case FMT_HEXA:
         answerf("0x%" PRIhexa, item->hexa);
         break;

      case FMT_FLOAT:
         answerf("%Lg", item->floatp);
         break;

      case FMT_UNIT:
         answerf("%s", ((unittype_t *)(fmt->tlptr))->units[item->unit]);
         break;

      case FMT_CHOICE:
         answerf("%s", ((list_t *)(fmt->tlptr))->options[item->choice]);
         break;

      case FMT_FLAGLIST: {
            id_flglist_t bit ;
            int          i;
            list_t      *fmtlist = fmt->tlptr;

            for (bit = 1, i = 0; i < fmtlist->n; bit <<= 1, i++) {
               if (item->flglist & bit)
                  answerf(" %s", fmtlist->options[i]);
            }
         }
         break;

      case FMT_SCOMPOSITE:
      case FMT_UCOMPOSITE:
         if (fmt->sp.compvalid > 0)
            answer_output_composite(item->composite);
         break;

      default:
         SYSTEM_ERROR();
         break;
   }
}


void answer_output_itemlist(id_itemlist_t *itemlist) {
   int i;

   for (i = 0; i < itemlist->used; i++)
      answer_output_item(item_ptr(itemlist, i), &itemlist->fmt);
}

void answer_output_composite(iodata_t *output_data) {
   int i;

   for (i = 0; i < output_data->used; i++)
      answer_output_itemlist(itemlist_ptr(output_data, i));
}


void init_iodata(iodata_t *iodata, id_itemlist_t *parent) {
   iodata->available = ID_DEF_NARGS;
   iodata->used = 0;
   iodata->current = 0;
   iodata->more_itemlists = NULL;
   iodata->iod_parent = parent;
}



void free_iodata(iodata_t *iodata) {
   int i;

   for (i = 0; i < iodata->used; i++)
      free_itemlist(itemlist_ptr(iodata, i));
   if (iodata->more_itemlists)
      free(iodata->more_itemlists);
}


void show_io_format(int indent, iodata_t *iodata) {
   int i;

   for (i = 0; i < iodata->used; i++) {
      _show_itemlist(indent, itemlist_ptr(iodata, i), 0, 1);
   }
}

void answer_output_data(iodata_t *output_data) {
   LOG_COMM1("Output format:\n");
   if (IS_LOG(COMM, 0)) show_io_format(0, output_data);

   answer_output_composite(output_data);
}


iodata_t *allocate_composite_item(id_itemlist_t *parent) {
   iodata_t *composite = malloc(sizeof(iodata_t));
   if (composite)
      init_iodata(composite, parent);

   return(composite);
}

void free_composite_item(iodata_t *composite) {
   free_iodata(composite);
   free(composite);
}


id_itemlist_t *clone_itemlist(id_itemlist_t *clonedlist, id_itemlist_t *itemlist) {
   int i;

   init_itemlist(clonedlist, itemlist->itl_parent, &itemlist->fmt);

   if (itemlist->more_items) {
      clonedlist->more_items = malloc((itemlist->available - ID_DEF_NITEMS) * sizeof(id_item_t));
      if (!clonedlist->more_items) {
         return(NULL);
      } else
         clonedlist->available = itemlist->available;
   }
   for (i = 0; i < itemlist->used; clonedlist->used = ++i) {
      id_item_t *item = item_ptr(clonedlist, i);

      if (IS_COMPOSITE_FMT(itemlist->fmt.type)) {
         item->composite = clone_iodata(item_ptr(itemlist, i)->composite);
         if (!item->composite) {
            free_itemlist(clonedlist);
            return(NULL);
         }
         item->composite->iod_parent = clonedlist;
      } else
         *item = *item_ptr(itemlist, i);
   }
   return(clonedlist);
}


iodata_t *clone_iodata(iodata_t *iodata) {
   int       i;
   iodata_t *newiodata = allocate_composite_item(iodata->iod_parent);

   if (newiodata == NULL)
      return(NULL);

   if (iodata->more_itemlists) {
      newiodata->more_itemlists = malloc((iodata->available - ID_DEF_NARGS) * sizeof(id_itemlist_t));
      if (!newiodata->more_itemlists) {
         free(newiodata);
         return(NULL);
      } else
         newiodata->available = iodata->available;
   }
   for (i = 0; i < iodata->used; newiodata->used = ++i) {
      id_itemlist_t *itlst = itemlist_ptr(iodata, i);
      id_itemlist_t *newitlst;

      newitlst = clone_itemlist(itemlist_ptr(newiodata, i), itlst);
      if (!newitlst) {
         free_composite_item(newiodata);
         return(NULL);
      }
      newitlst->itl_parent = newiodata;
   }
   reset_iodata(newiodata);
   return(newiodata);
}


int parse_integer(const char **params) {
   int nchar = 0;
   int ivalue;

   if (sscanf(*params, "%i%n", &ivalue, &nchar) < 1) {
      return(0);
   } else {
      *params += nchar;
      return(ivalue);
   }
}

int parse_integer_range(const char **params, format_t *fmt) {
   int      nchar = 0;
   int      naux = 0;
   intmax_t i1;
   intmax_t i2;

   sscanf(*params, "%"SCNiMAX"%n:%n%"SCNiMAX"%n", &i1, &nchar, &naux, &i2, &nchar);

   if (nchar == 0) {
      i1 = IDINT_MIN;
      i2 = IDINT_MAX;
      
   } else if (naux == 0) {
      i2 = i1;
      i1 = (i2 > 0)? -i2 : IDINT_MIN;
      
   } else {
      if (naux > nchar) {
         i2 = IDINT_MAX;
         nchar = naux;            
      }
   }
   *params += nchar;          
   if (i2 < i1) {
      intmax_t aux = i1;
      i1 = i2;
      i2 = aux;
   }
   fmt->min.intval = (fmt->uns_pos && i1 < 0)? 0 : i1;
   fmt->max.intval = i2;
   return(2);
}


int parse_float_range(const char **params, format_t *fmt) {
   int    nchar = 0;
   int    naux = 0;
   double f1;
   double f2;

   sscanf(*params, "%lg%n:%n%lg%n", &f1, &nchar, &naux, &f2, &nchar);

   if (naux >= nchar) {
      fmt->min.fltval = (fmt->uns_pos)? 0.0 : -DBL_MAX;
      fmt->max.fltval = DBL_MAX;
      return(0);
   } else {
      *params += nchar;
      if (!naux) {
         fmt->min.fltval = fmt->uns_pos? 0 : -f1;
         fmt->max.fltval = f1;
         return(1);
      } else {
         if (f2 < f1) {
            intmax_t aux = f1;
            f1 = f2;
            f2 = aux;
         }
         fmt->min.fltval = (fmt->uns_pos && f1 < 0)? 0 : f1;
         fmt->max.fltval = f2;
         return(2);
      }
   }
}



const char *parse_integer_value(format_t *fmt, const char **params, id_integer_t *intvalue, bool needspace) {
   int          nchar = 0;
   id_integer_t ivalue = 0;

//   if (sscanf(*params, " %ji %n", &intvalue, &nchar) < 1)
/*
GCC_DIAG_FORMAT_OFF()
   if (sscanf(*params, " %li%n", intvalue, &nchar) < 1) {
GCC_DIAG_ON()
*/
   LOG_COMM1("----- %d [%s]\n", (int)needspace, *params);
   if (sscanf(*params, "%"SCNinteger"%n", &ivalue, &nchar) < 1) {
      return(MISPARMESSAGE);
   } else if (needspace) {
      char c = *(*params + nchar);
      if (c != 0 && c != ' ')
         return(BADPARMESSAGE);
   }
   if (ivalue < fmt->min.intval || ivalue > fmt->max.intval ||
       (ivalue == 0 && fmt->uns_pos == FMT_PREFIX_POSITIVE)) {
      return(BADRANGEMESSAGE);
   } else {
      *intvalue = ivalue;
      *params = *params + nchar;
      return(NULL);
   }
}


const char *parse_float_value(format_t *fmt, const char **params, id_floatp_t *fvalue, bool needspace) {
   int         nchar = 0;
   double      dvalue;

//   if (sscanf(*params, "%Lf %n", fvalue, &nchar) < 1)
   if (sscanf(*params, "%lf%n", &dvalue, &nchar) < 1) {
      return(MISPARMESSAGE);
   } else if (needspace) {
      char c = *(*params + nchar);
      if (c != 0 && c != ' ')
         return(BADPARMESSAGE);
   }

   if (dvalue < fmt->min.fltval || dvalue > fmt->max.fltval ||
       (dvalue == 0 && fmt->uns_pos == FMT_PREFIX_POSITIVE)) {
      return(BADRANGEMESSAGE);
   } else {
      *fvalue = dvalue;
      *params = *params + nchar;
      return(NULL);
   }
}


unsigned int label_starts_by_prefix(const char *label, const char *prefix, int pfxlen) {
   const char *lbl = label;
   char        c;

   if (pfxlen == 0) {
      pfxlen = 1000;
   } else {
   }
   while (pfxlen-- && (c = toupper(*prefix++)) != 0) {
      if (c != *lbl++)
         return(0);
   }
   return(lbl - label);
}


#define IS_STRING 0
#define IS_LABEL  1

size_t extract_labelstring(const char **params, const char **string, bool islabel) {
   register char  c;
   register char  quote;

   for (; (c = **params) != 0; (*params)++) {
      if (!isblank(c)) {
         if (c == '\"' || c == '\'') {
            quote = c;
            *string = ++*params;
            for (; (c = **params) != 0; ++*params) {
               if (c == quote)
                  return(((*params)++) - *string);
            }
         } else {
            *string = (*params)++;
            for (; (c = **params) != 0; ++*params) {
               if (islabel && isblank(c))
                  return(*params - *string);
            }
         }
         return(*params - *string);
      }
   }
   *string = *params;
   return(0);
}

char *parse_labelstring(format_t *fmt, const char **params, string_t *string) {
   LOG_COMM1("--> [%s]\n", *params);
   string->length = extract_labelstring(params, &string->ptr, fmt->type == FMT_LABEL);
   LOG_COMM1("--> [%*s] %d\n", string->length, string->ptr, fmt->sp.maxlen);
   if (string->length == 0)
      return(MISSINGSTRMESSAGE);
   else if (fmt->sp.maxlen && string->length > fmt->sp.maxlen)
      return(TOOLONGSTRMESSAGE);
   else
      return(NULL);
}

char *parse_predef_label_value(format_t *fmt, const char **params, id_choice_t *listval, bool needspace) {
   string_t     lblstring;
   int          n;
   const char * const *option;

   if (fmt->type == FMT_UNIT) {
      unittype_t *fmtlist = fmt->tlptr;
      n = fmtlist->n;
      option = fmtlist->units;
   } else {  // FMT_CHOICE, FMT_FLAGLIST
      const list_t *fmtlist = fmt->tlptr;
      n = fmtlist->n;
      option = fmtlist->options;
   }

   lblstring.length = extract_labelstring(params, &lblstring.ptr, IS_LABEL);
   if (lblstring.length > 0) {
      int i;

      LOG_COMM1("--> %*s\n", lblstring.length, lblstring.ptr);
      for (i = 0; i < n; i++, option++) {
         int pfxlen = label_starts_by_prefix(lblstring.ptr, *option, 0);

         LOG_COMM1("--> %s [%d] %d\n", *option,  pfxlen, needspace);
         if (pfxlen > 0 && (!needspace || pfxlen == lblstring.length)) {
            *params = lblstring.ptr + pfxlen;
            *listval = i;
            return(NOERROR);
         }
      }
   }
   return(BADPARMESSAGE);
}


const char *parse_taglabel(format_t *fmt, const char **params, bool needspace) {
   const char *tag = fmt->tlptr;
   int         taglen = fmt->sp.taglen;
   const char *taglabel;
   int         length;

   length = extract_labelstring(params, &taglabel, IS_LABEL);

   if (label_starts_by_prefix(taglabel, tag, taglen) && (!needspace || length == taglen)) {
      *params = taglabel + taglen;
      return(NOERROR);
   } else
      return(MISSINGSTRMESSAGE);
}

int parse_boolean(const char *value, int length, int boolean_entry, arglists_t *arglist) {
   if (strncasecmp(value, arglist->truevalue[boolean_entry], length) == 0)
      return(1);
   else if (strncasecmp(value, arglist->falsevalue[boolean_entry], length) == 0)
      return(0);
   else
      return(-1);
}

const char *parse_boolean_value(format_t *fmt, const char **params, id_integer_t *boolvalue) {
   const char *boollabel;
   int         length;
   arglists_t *arglist = fmt->tlptr;

   length = extract_labelstring(params, &boollabel, IS_LABEL);
   if (length <= 0)
      return(MISSINGSTRMESSAGE);

   if (fmt->sp.boolit >= 0) {
      *boolvalue = parse_boolean(boollabel, length, fmt->sp.boolit, arglist);
      if (*boolvalue < 0)
         return(BADPARMESSAGE);
      else
         return(NULL);
   } else {
      int i;

      for (i = 0; i < arglist->n_bool; i++) {
         *boolvalue = parse_boolean(boollabel, length, i, arglist);
         if (*boolvalue >= 0)
            return(NULL);
      }
      return(BADPARMESSAGE);
   }
}


errmsg_t parse_input_value(id_item_t *item, const char **params, format_t *fmt) {
   const char *err;
   bool        needspace = !MAY_TOUCHNEXT(fmt);
   id_choice_t choice;
   const char  *lparams = *params;

   if (isblank(*lparams)) {
      if (IS_CONNECTED(fmt) && !IS_WSPACE_ALLOWED(fmt))
         return(BADPARMESSAGE);
      while(isblank(*++lparams))
         ;    // skip whitespaces
   } else {
      if (!IS_CONNECTED(fmt) || !IS_EMPTY_ALLOWED(fmt))
         return(BADPARMESSAGE);
   }

   switch(fmt->type) {
      case FMT_LABEL:
      case FMT_STRING:
         err = parse_labelstring(fmt, &lparams, &item->label);
         break;

      case FMT_TAG:
         err = parse_taglabel(fmt, &lparams, needspace);
         item->tag = (err == NOERROR);
         break;

      case FMT_BOOLEAN:
         err = parse_boolean_value(fmt, &lparams, &item->integer);
         break;

      case FMT_INTEGER:
      case FMT_HEXA:
         err = parse_integer_value(fmt, &lparams, &item->integer, needspace);
         if (fmt->type == FMT_HEXA)
            item->hexa = item->integer;
         break;

      case FMT_FLOAT:
         err = parse_float_value(fmt, &lparams, &item->floatp, needspace);
         break;

      case FMT_UNIT:
         needspace = 1;
         err = parse_predef_label_value(fmt, &lparams, &item->unit, needspace);
         break;

      case FMT_CHOICE:
         err = parse_predef_label_value(fmt, &lparams, &item->choice, needspace);
         break;

      case FMT_FLAGLIST:
         item->flglist = 0;
         needspace = 1;
         while (1) {
            const char* flgparams = lparams;
            if ((err = parse_predef_label_value(fmt, &flgparams, &choice, needspace)) != NULL)
               break;
            else {
               id_flglist_t flag = (1LL << choice);
               if (item->flglist & flag) {
                  *params = NULL;  // unrecoverable error even if optional param
                  return("Duplicated flag");
               } else {
                  lparams = flgparams;
                  item->flglist |= flag;
               }
            }
         }
         if (item->flglist != 0)
            err = NOERROR;
         break;

      default:
         SYSTEM_ERROR();
         err = SYSERRORMESSAGE;
         break;
   }
   if (err)
      LOG_COMM1("ERROR: \"%s\"\n", err);
   else
      *params = lparams;
//LOG_COMM1("{{%s}}\n", lparams);
   return(err);
}

errmsg_t process_format(format_t *fmt, id_itemlist_t *itemlist, const char **params, int unsorted) {
   const char *prev_params = *params;
   errmsg_t    err;
   int         k;

   LOG_COMM1(">>> [%s]\n", *params);
   LOG_COMM1(""); show_format_type(fmt); LOG_COMM1("\n");
   for (k = 0; !fmt->n_rep || k < fmt->n_rep; k++) {
      id_item_t   new_item;

      err = parse_input_value(&new_item, &prev_params, fmt);
      if (prev_params == NULL)
         goto on_error;
      if (err) {
         LOG_COMM1("error // fmt->n_rep = %d , k = %d\n", fmt->n_rep, k);
         if (fmt->n_rep || k == 0) {
            if (k > 0)
               empty_itemlist(itemlist);
            return(err);  // if error is not acceptable return it
         }
         break;
      } else {
         err = add_item_to_itemlist(itemlist, &new_item);
         if (err)
            goto on_error;
      }
   }
   *params = prev_params;
   return(NOERROR);

on_error:
   empty_itemlist(itemlist);
   *params = NULL;
   return(err);
}

const char *extract_label(const char **inp_format, int *len) {
   const char *prevptr = *inp_format;
   char        c;

   // allow label to start by an invalid character if escaped
   if (**inp_format == '\\' && *(*inp_format + 1)) {
      prevptr = *inp_format + 1;
      *inp_format += 2;
   }

   while((c = **inp_format) && !IS_INVALID_LBLCHAR(c)) {
      (*inp_format)++;
   }

   *len = *inp_format - prevptr;

   return(*len? prevptr : NULL);
}

int find_predef_label(const char *label, int n, const char **lbllist) {
   int i;

   for (i = 0; i < n; i++) {
      if (strcmp(label, lbllist[i]) == 0)
         return(i);
   }
   return(-1);
}

int find_predef_len_label(const char *label, int len, int n, const char **lbllist) {
   if (label) {
      int i;

      for (i = 0; i < n; i++) {
         if (strncmp(label, lbllist[i], len) == 0)
            return(i);
      }
   }
   return(-1);
}

int parse_predef_label(const char **inp_format, int n, const char **names) {
   const char *prevptr = *inp_format;
   int         len;
   const char *label = extract_label(inp_format, &len);
   int         i = find_predef_len_label(label, len, n, names);

   if (i < 0)
      *inp_format = prevptr;

   return(i);
}

const list_t *parse_optionlist(const char **inp_format, arglists_t *arglsts) {
   if (!arglsts)
      return(NULL);
   else {
      int i = parse_predef_label(inp_format, arglsts->n, arglsts->names);
      return(i < 0 ? NULL : arglsts->arglists[i]);
   }
}

unittype_t *find_unitlist(const char *utype) {
   int   list_n;

   if (appunits) {
      list_n = find_predef_label(utype, appunits->n_units, appunits->names);
      if (list_n >= 0)
         return(appunits->unitlists[list_n]);
   }
   list_n = find_predef_label(utype, communits->n_units, communits->names);

   return(list_n < 0 ? NULL : communits->unitlists[list_n]);
}

unittype_t *parse_unitlist(const char **inp_format) {
   int         len;
   const char *utype = extract_label(inp_format, &len);
   int         list_n;

   if (appunits) {
      list_n = find_predef_len_label(utype, len, appunits->n_units, appunits->names);
      if (list_n >= 0)
         return(appunits->unitlists[list_n]);
   }
   list_n = find_predef_len_label(utype, len, communits->n_units, communits->names);

   return(list_n < 0 ? NULL : communits->unitlists[list_n]);
}


void parse_default_unit(const char **inp_format, format_t *fmt) {

   if (**inp_format == FMT_UNIT_SEPARATOR) {
      unittype_t *ulist = fmt->tlptr;

      ++*inp_format;
      fmt->sp.defunit = parse_predef_label(inp_format, ulist->n, ulist->units);
   } else
      fmt->sp.defunit = -1;
}


const char* find_boolean_str(arglists_t *arglsts, const char* truename, int boolval) {
   int idx;

   for (idx = 0; idx < arglsts->n_bool; idx++) {
      if (strcmp(truename, arglsts->truevalue[idx]) == 0) {
         return boolval? truename : arglsts->falsevalue[idx];
      }
   }
   return NULL;
}


static int find_boolean_list_from_format(const char **inp_format, arglists_t *arglsts) {
   int         len;
   const char *blist = extract_label(inp_format, &len);

   if (!arglsts)
      return(-1);
   else
      return(find_predef_len_label(blist, len, arglsts->n_bool, arglsts->truevalue));
}


int seek_character(char c, const char **format_str, char *nextchar) {
   const char *ptr = *format_str;
   int         found = 0;
   int         white = 0;

   while(isblank(*ptr)) {
      white = 1;
      ptr++;
   }
   found = (*ptr == c);
   if (found) {
      white = 0;
      while(isblank(*++ptr))
         white = 1;
   }
   *nextchar = *ptr;
   *format_str = (*ptr && white)? ptr - 1 : ptr;
   return(found);
}


int next_format(const char **format_str, arglists_t *arglsts, format_t *fmt, format_t *compfmt, format_t *prevfmt) {
   const char  *auxptr;
   const char  *beg_ptr = NULL;
   const char  *fmt_ptr = *format_str;
   char         c;
   char         end_c = compfmt? COMPOSITE_END_CHAR(compfmt->type) : '\0';
   char         firstchar = *fmt_ptr;

   *fmt = default_format;

   // skip whitespaces
   while(isblank(c = *fmt_ptr))
      ++fmt_ptr;

   // check format string completion
   if (!c) {
      if (compfmt)
         goto on_bad_format;
      else
         goto on_completed;
   } else if (c == end_c) {
      ++fmt_ptr;
      goto on_completed;
   }
   beg_ptr = fmt_ptr;

   // treat first an eventual separator char
   if (compfmt && IS_UCOMPOSITE_FMT(compfmt->type))
      fmt->flags |= _UNSORTED;

   if ((auxptr = strchr(fmt_separators, c)) != NULL) {
      fmt->separator = auxptr - fmt_separators;
      c = *++fmt_ptr;

      if (fmt->separator == FMT_EXCLUSIVE_SEP) { // exclusive
         fmt->flags |= _EXCLUSIVE_WITH_PREV;
         if (prevfmt)
            prevfmt->flags |= _EXCLUSIVE_WITH_NEXT;
         else
            goto on_bad_format;

      } else {  // connected
         switch(fmt->separator) {
            case FMT_ECONNECT_SEP:
               fmt->flags |= _INP_EMPTY_ALLOWED;
               break;
            case FMT_SCONNECT_SEP:
               fmt->flags |= _INP_WSPACE_ALLOWED;
               break;
            case FMT_ACONNECT_SEP:
               fmt->flags |= _INP_EMPTY_ALLOWED | _INP_WSPACE_ALLOWED;
               break;
         }
         if (prevfmt) {
            prevfmt->flags |= _LINKED_TO_NEXT;
            if (fmt->flags & _INP_EMPTY_ALLOWED)
               prevfmt->flags |= _MAY_TOUCH_NEXT;
         }
         if (IS_EXCLUSIVE(prevfmt))
            fmt->flags |= _EXCLUSIVE_WITH_NEXT;
         if (!isblank(firstchar) && !isblank(c))
            fmt->flags |= _OUT_WSPACE_SUPPRESSED;
      }
      while(isblank(c))
         c = *++fmt_ptr;
   }

   while(1) {
      if (c == FMT_ID_BEG) {
         if (fmt->idptr)
            goto on_bad_format;
         else {
            fmt->idptr = ++fmt_ptr;
            while((c = *fmt_ptr) && (c != FMT_ID_END))
               ++fmt_ptr;
            if (!c)
               goto on_bad_format;
            fmt->idlen = fmt_ptr - fmt->idptr;
            c = *++fmt_ptr;
         }
      } else if (c == FMT_PREFIX_OPTIONAL) {
         if (IS_OPTIONAL(fmt))
            goto on_bad_format;
         else {
            fmt->flags |= _OPTIONAL;
            c = *++fmt_ptr;
         }
      } else if (c == FMT_PREFIX_VARIABLE) {
         if (fmt->n_rep != 1)
            goto on_bad_format;
         else {
            fmt->n_rep = 0;
            c = *++fmt_ptr;
         }
      } else if (c == FMT_PREFIX_UNSIGNED || c == FMT_PREFIX_POSITIVE) {
         if (fmt->uns_pos)
            goto on_bad_format;
         else {
            fmt->uns_pos = c;
            c = *++fmt_ptr;
         }
      } else if (isdigit(c)) {
         if (fmt->n_rep != 1)
            goto on_bad_format;
         else {
            fmt->n_rep = parse_integer(&fmt_ptr);
            if (fmt->n_rep == 0)
               goto on_bad_format;
            else
               c = *fmt_ptr;
         }
      } else
         break;
   }
   if ((auxptr = strchr(fmt_types, c)) != NULL) {
      fmt->type = auxptr - fmt_types;
      c = *++fmt_ptr;
   } else
      goto on_bad_format;

   if (fmt->uns_pos && !IS_NUMERIC_FMT(fmt->type))
      goto on_bad_format;

   if (IS_COMPOSITE_FMT(fmt->type)) {
      if (!IS_CONNECTED(fmt))
         fmt->flags |= _OUT_WSPACE_SUPPRESSED;

      if (!c || IS_COMPOSITE_END(c))
         goto on_bad_format;
      else
         goto on_success;

   } else if (fmt->type == FMT_FLAGLIST || fmt->type == FMT_CHOICE) {
      if (c != FMT_LISTNAME_PFX) {
         LOG_ERROR("Missing expected '#' in format declaration: \"%s\"\n", beg_ptr);
         goto on_bad_format;
      }
      ++fmt_ptr;
      fmt->tlptr = (void*)parse_optionlist(&fmt_ptr, arglsts);
      if (!fmt->tlptr) {
         LOG_ERROR("Missing list definition in \"%s\"\n", beg_ptr);
         goto on_bad_format;
      }
   } else if (fmt->type == FMT_TAG) {
      int length;
      if (c != FMT_LISTNAME_PFX) {
         LOG_ERROR("Missing expected '#' in format declaration: \"%s\"\n", beg_ptr);
         goto on_bad_format;
      }
      ++fmt_ptr;
      fmt->tlptr = (void *)extract_label(&fmt_ptr, &length);
      fmt->sp.taglen = length;

      if (!fmt->tlptr) {
         goto on_bad_format;
      }
   } else if (fmt->type == FMT_BOOLEAN) {
      fmt->uns_pos = FMT_PREFIX_UNSIGNED;
      fmt->tlptr = (void *)arglsts;
      if (c != FMT_LISTNAME_PFX)
         fmt->sp.boolit = -1;
      else {
         ++fmt_ptr;
         fmt->sp.boolit = find_boolean_list_from_format(&fmt_ptr, arglsts);
         if (fmt->sp.boolit < 0) {
            goto on_bad_format;
         }
      }
   } else if (fmt->type == FMT_UNIT) {
      if (c != FMT_LISTNAME_PFX) {
         LOG_ERROR("Missing expected '#' in format declaration: \"%s\"\n", beg_ptr);
         goto on_bad_format;
      }
      ++fmt_ptr;
      fmt->tlptr = parse_unitlist(&fmt_ptr);
      if (!fmt->tlptr) {
         LOG_ERROR("Missing list definition in \"%s\"\n", beg_ptr);
         goto on_bad_format;
      }
      parse_default_unit(&fmt_ptr, fmt);

   } else if (fmt->type == FMT_INTEGER) {
      parse_integer_range(&fmt_ptr, fmt);

   } else if (fmt->type == FMT_FLOAT) {
      parse_float_range(&fmt_ptr, fmt);

   } else if (fmt->type == FMT_HEXA || fmt->type == FMT_LABEL || fmt->type == FMT_STRING) {
      if (isdigit(c)) {
         fmt->sp.maxlen = parse_integer(&fmt_ptr);
      } else
         fmt->sp.maxlen = 0;
   }

   for (auxptr = fmt_ptr; isblank(*auxptr); ++auxptr)
      ;
   if (!*auxptr) {
      if (compfmt)
         goto on_bad_format;
      else
         goto on_completed;

   } else if (compfmt && *auxptr == end_c) {
      fmt_ptr = ++auxptr;
      goto on_completed;
   }

on_success:
   *format_str = fmt_ptr;
   //LOG_COMM1("on_success: \"%.*s\"\n", fmt_ptr - beg_ptr, fmt_ptr);
   return(1);

on_completed:
   *format_str = fmt_ptr;
   if (beg_ptr) {
      //LOG_COMM1("on_completed: \"%.*s\"\n", fmt_ptr - beg_ptr, fmt_ptr);
      return(2);
   } else {
      //LOG_COMM1("on_cleanup: \"%s\"\n", fmt_ptr);
      return(-1);
   }

on_bad_format:
   *format_str = beg_ptr;
   LOG_ERROR("Bad_format: \"%s\"\n", beg_ptr);
   return(0);
}


errmsg_t do_parse_unsorted_input_args(iodata_t *inpdata, const char **params, int *done) {
   errmsg_t    err;
   itlmask_t   aux_fmtmask;
   itlmask_t   fmtmask = 0;
   int         fmt_n;
   const char *prev_params = *params;

   *done = 0;
   do {
      int compdone;

      aux_fmtmask = fmtmask;
      // redo sequence parsing
      err = do_parse_input_sequence(inpdata, &prev_params, &fmtmask, &compdone);
      if (err)
         return(err);
      //LOG_COMM1("[0x%08llx : 0x%08llx]\n", fmtmask, aux_fmtmask);
   } while(fmtmask != aux_fmtmask);

   if (fmtmask == 0)
      return(NOERROR);

   // check if:
   //   - missing non-optional parameters
   //   - exclusive sequences with not a single item parsed
   // note that do_parse_input_sequence() does a big part of the job
   //
   aux_fmtmask = 0;
   for (fmt_n = 0; fmt_n < inpdata->used; fmt_n++) {
      format_t *fmt = &itemlist_ptr(inpdata, fmt_n)->fmt;
      itlmask_t itemlmask = 1 << fmt_n;
      int       prev_parsed = fmtmask & (itemlmask >> 1);

      // manage connectivity
      if (IS_CONNECTED(fmt)) {
         // fake this item to simplify subsequent exclusivity checks
         if (prev_parsed)
            fmtmask |= itemlmask;
         else  // previous not parsed, do not further check this one
            continue;
      }
      // manage exclusivity with previous items
      if (WAS_EXCLUSIVE(fmt)) {
         if (fmtmask & itemlmask)
            aux_fmtmask = 0;
         else {
            if (!prev_parsed)
               aux_fmtmask |= itemlmask; // flag it in the 'pending' mask
            else
               fmtmask |= itemlmask;
            // if is the last one and there are items pending
            if (!IS_EXCLUSIVE(fmt) && aux_fmtmask)
               return(MISPARMESSAGE);
         }
      } else if (IS_EXCLUSIVE(fmt)) {
         // if the first exclusive item in the sequence
         if (!(fmtmask & itemlmask))
            aux_fmtmask |= itemlmask; // flag it in the 'pending' mask

      } else if (!IS_OPTIONAL(fmt) && !(fmtmask & itemlmask))
         return(MISPARMESSAGE);
   }
   *done = 1;
   *params = prev_params;
   return(NOERROR);
}


errmsg_t do_parse_sorted_input_args(iodata_t *inpdata, const char **params, int *done) {
   return(do_parse_input_sequence(inpdata, params, NULL, done));
}

static int is_mandatory_itemlist(id_itemlist_t *itemlist) {
   format_t *fmt = &itemlist->fmt;
   TODO   // extend this to manage complex cases such as chains of connected items...
   return(IS_MANDATORY(fmt));
}

errmsg_t do_parse_input_sequence(iodata_t *inpdata,
                                 const char **params, itlmask_t *fmtmask, int *done) {
   errmsg_t    err;
   int         fmt_n;
   int         unsorted = (fmtmask != NULL);
   int         value_found = 0;
   itlmask_t   exclumask = 0;
   const char *prev_params = *params;

   LOG_COMM1("------------------------------\n");
   *done = 0;
   for (fmt_n = 0; fmt_n < inpdata->used; fmt_n++) {
      id_itemlist_t *itemlist = itemlist_ptr(inpdata, fmt_n);
      format_t      *fmt = &itemlist->fmt;
      itlmask_t      bitmask = 1 << fmt_n;
      int            already_parsed = unsorted? (*fmtmask & bitmask) : 0;
      int            prev_parsed = unsorted? (*fmtmask & (bitmask >> 1)) : value_found;
      int            prev_found = value_found;
      int            prev_excluded = (exclumask & (bitmask >> 1));

      value_found = 0;

      LOG_COMM1("Processing input format item type \'%c\' : \"%s\"\n", fmt_etypes[fmt->type], prev_params);

      if (IS_CONNECTED(fmt)) {
         if (WAS_EXCLUSIVE(fmt) && (prev_parsed || prev_excluded))
            exclumask |= bitmask;
         if (!prev_found)
            continue;
      } else if (WAS_EXCLUSIVE(fmt)) {
         if (prev_parsed || prev_excluded) {
            if (already_parsed) {
               LOG_ERROR("exclusive arguments found: %s\n", *params);
               return(INCOMPATPARMESSAGE);
            } else {
               exclumask |= bitmask;
               continue;
            }
         }
      }
      if (already_parsed) // format already succesfully parsed, skip
         continue;

      if (IS_COMPOSITE_FMT(fmt->type)) {
         int         k;

         for (k = 0; !fmt->n_rep || k < fmt->n_rep; k++) {
            id_item_t data_item;
            int       compdone;

            if (k < itemlist->used)
               data_item.composite = item_ptr(itemlist, k)->composite;
            else {
               data_item.composite = clone_iodata(item_ptr(itemlist, 0)->composite);
               if (data_item.composite == NULL) {
                  err = NOMEMORYMESSAGE;
                  goto on_system_error;
               }
               empty_iodata(data_item.composite);
            }

            if (fmt->type == FMT_SCOMPOSITE)
               err = do_parse_sorted_input_args(data_item.composite, &prev_params, &compdone);
            else  // fmt->type == FMT_UCOMPOSITE
               err = do_parse_unsorted_input_args(data_item.composite, &prev_params, &compdone);

            if (prev_params == NULL)     // this signals a system error
               goto on_system_error;

            if (err) {      // parsing error
               if (k < itemlist->used) { //   data_item.composite points to an existing entry
                  empty_iodata(data_item.composite);
               } else {                  //   data_item.composite was cloned
                  free_composite_item(data_item.composite);
               }
               if (fmt->n_rep || k == 0) {
                  // make sure that itemlist is empty
                  if (k > 0)
                     empty_itemlist(itemlist);

                  if (is_mandatory_itemlist(itemlist))
                     return(err);  // if error is not acceptable return it
               }
               break;
            } else {
               if (compdone) {
                  err = add_item_to_itemlist(itemlist, &data_item);
                  if (err) {  // a memory error may happen here only if data_item.composite was cloned
                     free_composite_item(data_item.composite);
                     goto on_itemlist_error;
                  }
               } else { // composite parsing not done (e.g. only optional items)
                  if (k < itemlist->used)  //   data_item.composite points to an existing entry
                     empty_iodata(data_item.composite);
                  else                     //   data_item.composite was cloned
                     free_composite_item(data_item.composite);

                  if (is_mandatory_itemlist(itemlist))
                     return(MISPARMESSAGE);  // if error is not acceptable return it
                  else
                     break;
               }
            }
            continue;
         }
         if (k == 0) // no single valid item found
            continue;

      } else {
         err = process_format(fmt, itemlist, &prev_params, unsorted);
         if (prev_params == NULL)      // unrecoverable system error
            goto on_itemlist_error;

         if (err) {
            if (is_mandatory_itemlist(itemlist)) {
               LOG_COMM1("Missing expected type \'%c\' input value\n", fmt_etypes[fmt->type]);
               return(err);           // if unacceptable error return
            } else
               continue;
         }
      }
      // we should get here only if the value has been succesfully parsed
      *done = 1;
      if (unsorted) {  // if unsorted and not connected only one format must be processed
        *fmtmask |= bitmask;  // tag/flag the format found
         if (!IS_NEXT_CONNECTED(fmt))  // if next is not connected, return
            goto on_success;
      }
      value_found = 1;
      continue;

on_itemlist_error:
      empty_itemlist(itemlist);
      goto on_system_error;
   }

on_success:
   if (*done)
      *params = prev_params;
   LOG_COMM1("<<<%s>>>\n", *params);
   return(NOERROR);

on_system_error:
   *params = NULL;        // this signals a system error
   return(err);
}


errmsg_t parse_input_args(iodata_t *inpdata, const char *params) {
   errmsg_t  err;
   int       done;

   LOG_COMM("=========================================================\n");
   LOG_COMM("Parsing input arguments: %s\n", params);

   if (inpdata->used > 0) {  // if any input format to process

      err = do_parse_sorted_input_args(inpdata, &params, &done);
      reset_iodata(inpdata);

      LOG_COMM("\n");
      LOG_COMM("=========================================================\n");
      LOG_COMM("Result of parsing:\n");
      show_io_format(0, inpdata);
      LOG_COMM("=========================================================\n");

      if (err != NOERROR)
         return(err);
   }
   // check for left overs in the input string
   while(isblank(*params))
      params++;    // skip any remaining whitespace

   if (*params)
      return(BADPARMESSAGE);
   else
      return(NOERROR);
}

errmsg_t do_parse_format(iodata_t *iodata, const char **format_str, arglists_t *arglsts, format_t *compfmt) {
   format_t  fmt;
   format_t *prev_fmt = NULL;
   int       newfmt;
   errmsg_t  err;

   //LOG_COMM1("Parsing format string: %s\n", *format_str);
   while ((newfmt = next_format(format_str, arglsts, &fmt, compfmt, prev_fmt))) {

      if (newfmt > 0) {  // there is a format to process
         id_itemlist_t *itemlist = allocate_itemlist(iodata, &fmt);
         if (itemlist == NULL)
            return(NOMEMORYMESSAGE);

         if (IS_COMPOSITE_FMT(fmt.type)) {
            id_item_t new_item;

            if ((new_item.composite = allocate_composite_item(itemlist)) == NULL)
               return(NOMEMORYMESSAGE);

            err = do_parse_format(new_item.composite, format_str, arglsts, &fmt);
            if (err) {
               free_composite_item(new_item.composite);
               return(err);
            }

            itemlist->fmt.flags = fmt.flags;

            err = add_item_to_itemlist(itemlist, &new_item);
            if (err) {
               free_composite_item(new_item.composite);
               return(err);
            }
            itemlist->fmt.sp.compvalid = 0;
         }
         prev_fmt = &itemlist->fmt;
      }
      if (newfmt == 1)  // there is still work to do.
         continue;

      // all the format items are done,
      // perform last consistency checks...
      if (compfmt && IS_UCOMPOSITE_FMT(compfmt->type) &&
                                 iodata->used > MAX_ITEMLISTS) {
         LOG_ERROR("Too many (%d) unsorted format elements\n", iodata->used);
         goto on_bad_format;
      }
      return(NOERROR);
   }
   // if we get here, an error happened
   LOG_ERROR("Bad i/o format string \"%s\"\n", *format_str);

on_bad_format:
   SYSTEM_ERROR();
   return(BADIOFORMATMESSAGE);
}

errmsg_t parse_format_string(iodata_t *iodata, const char *format_str, arglists_t *arglsts) {
   errmsg_t err;

   init_iodata(iodata, NULL);

   if (!format_str || !*format_str) {
      return(NOERROR);
   } else {
      err = do_parse_format(iodata, &format_str, arglsts, NULL);
      if (err) {   // do some probably unecessary housekeeping
         free_iodata(iodata);
         init_iodata(iodata, NULL);
      } else {
         reset_iodata(iodata);
         //LOG_COMM1("\n");show_io_format(0, iodata);
      }
      return(err);
   }
}

//--------------------------

void *default_value(char fmt_type) {
   switch(fmt_type) {
      case FMT_BOOLEAN:    return((void *)&_default_boolean);
      case FMT_TAG:        return((void *)&_default_tag);
      case FMT_FLAGLIST:   return((void *)&_default_flglist);
      case FMT_CHOICE:     return((void *)&_default_choice);
      case FMT_INTEGER:    return((void *)&_default_integer);
      case FMT_HEXA:       return((void *)&_default_hexa);
      case FMT_FLOAT:      return((void *)&_default_floatp);
      case FMT_UNIT :      return((void *)&_default_unit);
      case FMT_LABEL :     return((void *)DEFAULT_LABEL);
      case FMT_STRING:     return((void *)DEFAULT_LABEL);
      case FMT_SCOMPOSITE:
      case FMT_UCOMPOSITE: return((void *)&_default_composite);
      case FMT_UNKNOWN:    return(NULL);
      default:
         SYSTEM_ERROR();
         return(NULL);
   }
}

int choose_unit(format_t *fmt, id_floatp_t value) {
   float        factor = (float)value;
   unittype_t  *unitlist = fmt->tlptr;
   int          n = unitlist->n;


   for(n = 0; n < unitlist->n; n++) {
      if ((float)unitlist->factors[n] == factor) {  // compare at the lowest precision
//LOG_COMM1("Found unit %s:%s: %Lg\n", unitlist->name, unitlist->units[n], unitlist->factors[n]);
         return(n);
      }
   }
   return(-1);
}


/* --------------------------------------------------------------------- */

void reset_itemlist(id_itemlist_t *itlist) {
   if (IS_COMPOSITE_FMT(itlist->fmt.type))
      itlist->current = itlist->used;
   else
      itlist->current = 0;
}

void reset_iodata(iodata_t *iodata) {
   iodata->current = 0;
   if (iodata->used)  // Is this needed?  can be iodata->used == 0?
      reset_itemlist(itemlist_ptr(iodata, 0));
}


id_itemlist_t *advance_itemlist(id_itemlist_t *itlist) {
   iodata_t *iodata;

   if (IS_COMPOSITE_FMT(itlist->fmt.type)) {
      if (itlist->current == itlist->used)
         itlist->current = 0;
      else
         ++itlist->current;

      if (itlist->current < itlist->used) {
         iodata_t *childiodata = item_ptr(itlist, itlist->current)->composite;
         childiodata->current = 0;
         id_itemlist_t *childlist = itemlist_ptr(childiodata, 0);
         reset_itemlist(childlist);
         return(childlist);
      }
   }
   iodata = itlist->itl_parent;
   ++iodata->current;

   if (iodata->current < iodata->used) {
      itlist = itemlist_ptr(iodata, iodata->current);
      reset_itemlist(itlist);
      return(itlist);

   } else {
      itlist = iodata->iod_parent;
      if (itlist)
         return(advance_itemlist(itlist));
      else
         return(NULL);
   }
}

id_itemlist_t *advance_item(id_itemlist_t *cur_itlist, int flags) {
   // if current itemlist is fresh composite, move to next itemlist
   if (cur_itlist->used && cur_itlist->current == cur_itlist->used)
      return(advance_itemlist(cur_itlist));

   if (flags & _SRCH_OUTP) {
      int n_rep = cur_itlist->fmt.n_rep;
      ++cur_itlist->current;
      if (n_rep != 0 && n_rep == cur_itlist->current)
         cur_itlist = advance_itemlist(cur_itlist);

   } else {  // _SRCH_INP
      if (cur_itlist->used)
         ++cur_itlist->current;
      if (cur_itlist->current == cur_itlist->used)
         cur_itlist = advance_itemlist(cur_itlist);
   }
   return(cur_itlist);
}


id_itemlist_t *validated_itemlist(id_itemlist_t *itlist, int flags) {
   for ( ; itlist; itlist = advance_itemlist(itlist)) {
      if (IS_COMPOSITE_FMT(itlist->fmt.type)) {
         if ((flags & _SRCH_EXOPT) && IS_OPTIONAL(&itlist->fmt) && itlist->fmt.sp.compvalid == 0) {
            itlist->current = 0;  // this is needed to skip the whole content
            continue;
         }
         if (flags & _SRCH_EXCOMP)
            continue;
      } else {
         if ((flags & _SRCH_EXOPT) && IS_OPTIONAL(&itlist->fmt) && itlist->used == 0)
            continue;
      }
      break;
   }
   return(itlist);
}


id_itemlist_t *current_itemlist(iodata_t *iodata) {
   if (iodata->current < iodata->used) {
      id_itemlist_t *itlist = itemlist_ptr(iodata, iodata->current);

      if (IS_COMPOSITE_FMT(itlist->fmt.type) && itlist->current < itlist->used) {
         iodata_t *childiodata = item_ptr(itlist, itlist->current)->composite;
         return(current_itemlist(childiodata));
      } else
         return(itlist);
   } else
      return(NULL);
}


int formats_are_equal(format_t *fmt1, format_t *fmt2) {
   if (fmt1->type != fmt2->type) return(0);
   if (fmt1->n_rep != fmt2->n_rep) return(0);
   if ((fmt1->flags ^ fmt2->flags) & _FMT_COMPARE_MASK) return(0);
   // ignore exclusive flag for comparison purposes
   if (fmt1->uns_pos != fmt2->uns_pos) return(0);
   if (fmt1->sp.maxlen != fmt2->sp.maxlen) return(0);
//   if (fmt1->min != fmt2->min) return(0);
//   if (fmt1->max != fmt2->max) return(0);
   if (fmt1->tlptr && fmt2->tlptr) {
      if (fmt1->type == FMT_TAG) {

         return(strncmp(fmt1->tlptr, fmt2->tlptr, fmt1->sp.taglen) == 0);
      } else {
         return(fmt1->tlptr == fmt2->tlptr);
      }
   } else
      return(!fmt1->tlptr && !fmt2->tlptr);
}

int ftypes_are_equivalent(ftype_t fmttype1, ftype_t fmttype2) {
   return ((fmttype1 == fmttype2) ||
           (IS_COMPOSITE_FMT(fmttype1) && IS_COMPOSITE_FMT(fmttype2)));
}


static int is_named_fmt(format_t *fmt, const char *name, int length) {
   switch(fmt->type) {
      case FMT_BOOLEAN:
         if (fmt->sp.boolit >= 0)
         return(strcmp(name, ((arglists_t *)(fmt->tlptr))->truevalue[fmt->sp.boolit]) == 0);
      case FMT_TAG:
         return(length == fmt->sp.taglen && strncmp(name, fmt->tlptr, length) == 0);
      case FMT_CHOICE:
      case FMT_FLAGLIST:
         return(strcmp(name, ((list_t *)(fmt->tlptr))->name) == 0);
      case FMT_UNIT:
         return(strcmp(name, ((unittype_t *)(fmt->tlptr))->name) == 0);
      default:
         return(0);
   }
}

id_itemlist_t *search_itemlist(iodata_t *iodata, format_t *fmt, const char *id, int flags) {
   id_itemlist_t *itlist;
   int            idlen = id? strlen(id) : 0;

//LOG_COMM1("search_itemlist(): %c [%s] : 0x%04x\n", fmt? fmt_etypes[fmt->type] : '-', id, flags);
//LOG_COMM1("  current: %d\n", iodata->current);
//show_io_format(0, iodata);

   if (flags & _SRCH_RST)
      reset_iodata(iodata);
//LOG_TRACE();

   itlist = validated_itemlist(current_itemlist(iodata), flags);

   for (; itlist; itlist = validated_itemlist(advance_itemlist(itlist), flags)) {
//LOG_COMM1("Current item: "); show_itemlist(itlist);
      if ((flags & _SRCH_TYPE) && !ftypes_are_equivalent(fmt->type, itlist->fmt.type))
         continue;
      if ((flags & _SRCH_FMT) && !formats_are_equal(fmt, &itlist->fmt))
         continue;
      if ((flags & _SRCH_LN) && !(itlist->fmt.tlptr && is_named_fmt(&itlist->fmt, id, idlen)))
         continue;
      if ((flags & _SRCH_ID) && !(idlen && (itlist->fmt.idlen == idlen) &&
                                       (strncmp(id, itlist->fmt.idptr, idlen) == 0)))
         continue;

//LOG_COMM1("Item found\n"); show_itemlist(itlist);

         // item found, now decide whether or not advance to next one
         if (!(flags & _SRCH_ONE)) {
            if (flags & (__RET_VALUE | _SRCH_OUTP))
               advance_item(itlist, flags);
            else
               advance_itemlist(itlist);
         }
         return(itlist);
   }
   return(NULL);
}

void *ret_int_val(int value) {
   static int ret_int;
   ret_int = value;
   return (&ret_int);
}


int check_fmtln(const char *lname, format_t *fmt) {
   if (!fmt->tlptr)
      return(0);
   else if (fmt->type == FMT_TAG)
      return(strncmp(lname, fmt->tlptr, fmt->sp.taglen) == 0);
   else
      return(strcmp(lname, ((list_t *)(fmt->tlptr))->name) == 0);
}

int check_fmtid(const char *id, format_t *fmt) {
   if (!fmt->idptr)
      return(0);
   else
      return(strncmp(id, fmt->idptr, fmt->idlen) == 0);
}

void *search_error(format_t *fmt, int error_code, int flags) {
   switch (flags & __RET_MASK) {
      case __RET_TYPE:  return(ret_int_val(FMT_UNKNOWN));
      case __RET_NUM:   return(ret_int_val(error_code));
      case __RET_CHKLN:
      case __RET_CHKID: return(ret_int_val(0));
      case __RET_VALUE: return(default_value(fmt->type));
      default: return(NULL);
   }
}

static __thread arglists_t *fake_arglists;
static __thread iodata_t   *fake_outdata = NULL;

iodata_t *get_iodata(int flags, arglists_t **param_lists) {
   if (fake_outdata && (flags & _SRCH_OUTP)) {
      if (param_lists != NULL)
         *param_lists = fake_arglists;
      return(fake_outdata);

   } else {
      command_t     *comm = get_current_command();
      if (param_lists != NULL)
         *param_lists = comm->param_lists;
      return((flags & _SRCH_OUTP)? comm->out_data : comm->inp_data);
   }
}

void *search_parsing_data(ftype_t fmt_type, const char *str, int flags) {
   arglists_t    *param_lists;
   iodata_t      *iodata = get_iodata(flags, &param_lists);
   const char    *idln = NULL;
   format_t       lfmt = default_format;
   format_t      *fmt;
   id_itemlist_t *itlist;
   int            used;
   int            current;

   LOG_COMM1("search_parsing_data(): flags 0x%04X : %c : %s\n", flags, fmt_etypes[fmt_type], str);
   if (flags & (_SRCH_ID | __RET_CHKID | _SRCH_LN | __RET_CHKLN))
      idln = str;
   if (flags & _SRCH_TYPE)
      lfmt.type = fmt_type;
   else if (flags & _SRCH_FMT) {
      if (!next_format(&str, param_lists, &lfmt, NULL, NULL)) {
         LOG_ERROR("Invalid format string \"%s\"\n", str);
         SYSTEM_ERROR();
         return(search_error(&lfmt, FMT_ERROR, flags));
      }
   }

   itlist = search_itemlist(iodata, &lfmt, idln, flags);
   if (itlist == NULL) {
      LOG_COMM1("Missing requested type \'%c\' format item\n", fmt_etypes[lfmt.type]);
      return(search_error(&lfmt, FMT_NOT_FOUND, flags));
   }
   LOG_COMM1("Item found\n"); show_itemlist(itlist);
   fmt = &itlist->fmt;
   if (IS_COMPOSITE_FMT(fmt->type)) {
      if (fmt->sp.compvalid == 0) {
         used = 0;
         current = 0;
      } else {
         used = itlist->used;
         current = (itlist->current == itlist->used)? 0 : itlist->current + 1;
      }
   } else {
      used = itlist->used;
      current = itlist->current;
   }
   switch (flags & __RET_MASK) {
      case __RET_TYPE:  return(ret_int_val(fmt->type));
      case __RET_NUM:
         if (flags & _SRCH_OUTP)
            return(ret_int_val(fmt->n_rep > 0? fmt->n_rep - used : 0));
         else
            return(ret_int_val(used? (used - current) : 0));
      case __RET_CHKLN: return(ret_int_val(check_fmtln(idln, fmt)));
      case __RET_CHKID: return(ret_int_val(check_fmtid(idln, fmt)));
      case __RET_VALUE:
         if (used) {
            id_item_t *curritem = item_ptr(itlist,
                      (flags & _SRCH_ONE)? itlist->current : itlist->current - 1);
            if (IS_COMPOSITE_FMT(fmt->type)) {
               static id_integer_t in_use;
               in_use = used;
               return(&in_use);
            } else if (IS_TEXT_FMT(fmt->type)) {
               char *str = (char *)curritem->string.ptr;
               str[curritem->string.length] = 0;
               return(str);
            } else if (fmt->type == FMT_UNIT) {
               return((void *)&((unittype_t *)fmt->tlptr)->factors[curritem->unit]);
            } else
               return(curritem);
         } else {  // return default value
            if (fmt->type == FMT_UNIT && fmt->sp.defunit >= 0)
               return((void *)&((unittype_t *)fmt->tlptr)->factors[fmt->sp.defunit]);
            else
               return(default_value(fmt->type));
         }
      default: return(NULL);
   }
}

int extend_parsing_item(int flags, ftype_t fmt_type, const char *id, id_item_t *data) {
   iodata_t      *iodata = get_iodata(flags, NULL);
   format_t       fmt = default_format;
   id_itemlist_t *itlist;
   int            itempos;

   flags |= __RET_NUM;
   fmt.type = fmt_type;

   itlist = search_itemlist(iodata, &fmt, NULL, flags);
   if (itlist == NULL) {
      LOG_ERROR("Missing requested type \'%c\' format item\n", fmt_etypes[fmt.type]);
      SYSTEM_ERROR();
      return(FMT_NOT_FOUND);
   }
   itempos = (flags & _SRCH_ONE)? itlist->current : itlist->current - 1;

   if (IS_COMPOSITE_FMT(itlist->fmt.type)) {
TODO     ; // add/insert composite entries
   } else {
      if (itlist->fmt.type == FMT_UNIT) {
         data->unit = choose_unit(&itlist->fmt, data->floatp);
         if (data->unit < 0)
            return(FMT_NOT_FOUND);
      }
      if (itempos == itlist->used) {
         errmsg_t err = add_item_to_itemlist(itlist, data);
         if (err) {
            SYSTEM_ERROR();
            return(FMT_ERROR);
         }
      } else {
         id_item_t *item = item_ptr(itlist, itempos);
         *item = *data;
      }
      {  // Update cointainer blocks
         id_itemlist_t *uplist = itlist;

         while((uplist = uplist->itl_parent->iod_parent) != NULL) {
            if (uplist->fmt.sp.compvalid == 0) {
               uplist->fmt.sp.compvalid = 1;
            } else
               break;
         }
      }
      // Check non-optional consecutive linked items
      if (IS_NEXT_CONNECTED(&itlist->fmt)) {
         iodata_t      *parent = itlist->itl_parent;
         id_itemlist_t *nextitlist = itemlist_ptr(parent, parent->current + 1);
         format_t      *nfmt = &nextitlist->fmt;

         if (!IS_OPTIONAL(nfmt)) {
            id_item_t item;

            if (nfmt->type == FMT_TAG) {
               item.tag = 1;
               add_item_to_itemlist(nextitlist, &item);

            } else if (nfmt->type == FMT_UNIT && nfmt->sp.defunit >= 0) {
               item.unit = nfmt->sp.defunit;
               add_item_to_itemlist(nextitlist, &item);
            }
         }
      }
   }
   return(itlist->used - itempos - 1);
}


int set_parsing_integer(int flags, ftype_t fmt_type, const char *id, intmax_t ivalue) {
   id_item_t   item;

   switch(fmt_type) {
      case FMT_BOOLEAN:  item.boolean = ivalue; break;
      case FMT_TAG:      item.tag     = ivalue; break;
      case FMT_FLAGLIST: item.flglist = ivalue; break;
      case FMT_CHOICE:   item.choice  = ivalue; break;
      case FMT_INTEGER:  item.integer = ivalue; break;
      case FMT_HEXA:     item.hexa    = ivalue; break;
      default:
         SYSTEM_ERROR();
         break;
   }
   return(extend_parsing_item(flags, fmt_type, id, &item));
}

int set_parsing_float(int flags, ftype_t fmt_type, const char *id, id_floatp_t fvalue) {
   id_item_t  item;

   if (fmt_type == FMT_FLOAT)
      item.floatp = fvalue;
   else if (fmt_type == FMT_UNIT) {
      item.floatp = fvalue;
   }
   return(extend_parsing_item(flags, fmt_type, id, &item));
}

int set_parsing_text(int flags, ftype_t fmt_type, const char *id, const char *textstr) {
   id_item_t  item;

   item.string.ptr = textstr;
   item.string.length = strlen(textstr);
   return(extend_parsing_item(flags, fmt_type, id, &item));
}

//------------------------------

void reset_output_context(void) {
   if (fake_outdata) {
      free_iodata(fake_outdata);
      free(fake_outdata);
      fake_outdata = NULL;
   }
}

void switch_output_context(int cmd_id) {
   menuitem_t *mentry;

   reset_output_context();

   if (cmd_id < FIRST_COMMONCOMM_CODE) {
      mentry = (menuitem_t *)&applicationmenu[cmd_id];
      fake_arglists = applists;
   } else {
      mentry = (menuitem_t *)&commonmenu[cmd_id - FIRST_COMMONCOMM_CODE];
      fake_arglists = commlists;
   }
   LOG_COMM1("Switch parsing context to command \"%s\"\n", mentry->comm_str);
   fake_outdata = clone_iodata(&mentry->out_data);

   if (fake_outdata == NULL) {
      errmsg_t err = "Output format switching error";
      LOG_ERROR("%s\n", err);
      errorf(err);
   } else {
      if (IS_LOG(COMM, 0)) show_io_format(0, fake_outdata);
   }
}

void release_output_context(void) {
   if (fake_outdata)  answer_output_data(fake_outdata);
   reset_output_context();
}

//----------------------------

id_floatp_t unit_conversion(char *type, char *unit, id_floatp_t value, int from) {
   id_floatp_t factor;
   unittype_t *unittype = find_unitlist(type);
   int         unit_n;

   if (!unittype)
      return(DEFAULT_UNIT);

   unit_n = find_predef_label(unit, unittype->n, unittype->units);
   if (unit_n < 0)
      return(DEFAULT_UNIT);

   factor = unittype->factors[unit_n];
   return(from? value * factor : value / factor);
}
