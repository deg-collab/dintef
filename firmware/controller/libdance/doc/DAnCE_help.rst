=====================================================
 DAnCE built-in queries and commands HOWTO
=====================================================

:Author: $Author: biv $
:Date: $Date: 2018-09-13 08:57:57 +0200 (Thu, 13 Sep 2018) $
:Revision: $Revision: 1869 $
:Description: DAnCE built-in commands and queries.


.. contents::


1.  Introduction
================

The following document described the syntax of DEEP commands and queries which always available in DAnCE embedded software application.

| DAnCE compliant instruments talk to the external world by using the DEEP communication protocol.
| This is a simple protocol that can be easily transported through any full duplex communication channel. Most of DAnCE instruments rely on Ethernet/TCP sockets to implement the communication channel but the DEEP protocol is not restricted to any particular connection scheme.

| Check the newest version of this document here : https://deg-svn.esrf.fr/svn/libdance/dev/doc/DAnCE_help.rst

2. DEEP synchronous messages
============================

| The DEEP messages are always produced by the user of the DAnCE instrument, that we call 'the caller' in this documents and that may be either a computer program or human operator typing in a computer terminal. The messages are transmitted to the DAnCE instrument through the communication channel and must be of one of two types: commands or queries.
| Commands are messages that transmit information from the caller and that are never followed by an answer from the instrument.
| On the other hand, queries are messages that must necessarily return an answer to the caller.

| Both commands and queries are represented by ASCII strings starting by a message specific keyword that is followed by optional parameters that are treated as input arguments.
| The message keyword and the optional arguments are separated by white spaces.
| A command keyword consists of an alphanumerical string eventually preceded of a asterisk character ``*``. Commands starting by an asterisk must transmit a binary data block following the optional ASCII arguments.

| Query keywords are constructed following the same conventions than command keywords by they must in addition include a question mark character ``?`` as the first character of the message keyword. In this scheme binary queries are identified by the presence of the asterisk character between the question mark and the first alphanumeric character.
| Both types of queries, binary or not, return an answer to the caller that is an ASCII string that can be optionally empty. In addition to the ASCII answer, binary queries return a block of binary data to the caller.

| It is worth to note that in the DEEP protocol the same alphanumeric string may serve as basis for four different message keywords. For instance ``CONFIG``, ``*CONFIG``, ``?CONFIG`` and ``?*CONFIG`` are four different valid message keywords that could be implemented in the same DAnCE instrument as four complete independent messages: two commands and two queries.

| The alphanumeric strings in DEEP keywords must always begin with a letter and consist of a combination of letters (A-Z), numbers (0-9) and the underscore character '_'. The usage of other characters is forbidden. Although when they are sent by the caller, the DEEP messages are case insensitive, for the internal implementation in a DAnCE instruments the strings in both keywords and parameters must be represented always in uppercase.

3. Description of the syntax
============================

3.1 DEEP message
----------------

The name of the command or query is not case sensitive.

``?``
    Prefix DEEP message use for a query
``\*``
    Prefix DEEP message use for binary

.. note::
    | All binary commands (with the prefix ``*``) could not be execute via the command line.
    | Please use the ``libdeep`` available here : https://deg-svn.esrf.fr/svn/libdeep/dev
    | Sample code are also available here : https://deg-svn.esrf.fr/svn/libdeep/dev/c/test/

3.2 DEEP message argument
-------------------------

``<param>``
    Name of the parameter

``{}``
    Inside the ``{}`` unsorted group of parameters.

``()``
    Inside the ``()`` sorted group of parameters.

``[]``
    Inside the ``[]`` parameters are optional.

PLAIN-TEXT
    If a word is written without any brackets means that is a keyword (case insensitive).

``|``
    Exclusive group of parameters gather inside ``"``. For example, ``"<param1> | <param2>"`` means that only one of the two parameters can be called.


4. Instrument identification and configuration
==============================================

``?HELP [{"BASIC | EXPERT | TEST | ALL"}]``
    | Display available queries and command.
    | Each queries and commands are tagged into a group (BASIC, EXPERT, TEST or ALL).
    | The default parameters is ``BASIC``.

``?APPNAME``
    | Return the instrument name.

``?VERSION``
    | Return the version of the firmware.

``?DINFO ["UPTIME | UNAME | HWIDS | APPNAME | VERSION | ADDR | NET"]``
    | Dump general information of the instrument.

``?DCONFIG``
    | Display the instrument configuration.

``?IP``
    | Display network configuration, hostnamem, IP address, netmask address, gateway address and broadcast address.

``IP <IP_addr>``
    | Configure network of the instrument.

``REBOOT``
    | Reboot the instrument.

``*PROG``
    | Upload code module. Must be a gziped firmware image.

``DEBUG <level>``
    | Set debug level.
    | ``<level>`` : Number of debug level.


5. Registers and memories access
================================

``?REG { ["HEXA | SIGNED | UNSIGNED"] [<reg_type> | (<reg_id> [<n_repet>])] }``
    | Read the contents of a FPGA register in ASCII
    | ``["HEXA | SIGNED | UNSIGNED"]`` : Optional, use one of the keyword to format the display
    | ``<reg_type>`` : Optional, Depends on the instrument capabilities.
    | ``<reg_id>`` : Optional, name or address of the register. The list of the register's name can be displayed using ``?REG`` without parameters.
    | ``<n_repet>`` : (Not implemented) Optional argument of ``<reg_id>``, positive integer value n. Repet the query n times.

``?*REG <reg_id>  <n_reads>``
    | Read the contents of a FPGA register in binary
    | ``<reg_id>`` : Name or address of the register. The list of the register's name can be displayed using ``?REG`` without parameters.
    | ``<n_reads>`` : Number of 32 bits value to read.

``REG <reg_id> <value>``
    | Set a value to a register.
    | ``<reg_id>`` : Name or address of the register. The list of the register's name can be displayed using ``?REG`` without parameters.
    | ``<value>`` : Value of the settings.

``*REG <reg_id>``
    | Binary write to FPGA register.
    | ``<reg_id>`` : Name or address of the register. The list of the register's name can be displayed using ``?REG`` without parameters.

``?MEM { ["HEXA | SIGNED | UNSIGNED"] [(<mem_id> [<n_values>] [<offset>])] }``
    | Dump data memory area in ASCII
    | ``["HEXA | SIGNED | UNSIGNED"]`` : Optional, use one of the keyword to format the display
    | ``<mem_id>`` : Optional, name or address of memory area.
    | ``<n_values>`` :  Optional argument of ``<mem_id>``, size of data to read.
    | ``<offset>`` :  Optional argument of ``<mem_id>``, offset from mem block in data values.

``?*MEM <mem_id> <n_values>``
    | Read the memory area in binary
    | ``<mem_id>`` : Name or address of memory area.
    | ``<n_values>`` : Size of the memory area.

``MEM <mem_id> [offset] [n_data] ("CLEAR | FILL" [<value>])``
    | Write data to the memory area.
    | ``<mem_id>`` : Name or address of memory area.
    | ``[offset]`` : Optionnal, offset from memory block in data values.
    | ``[n_data]`` : Optionnal, number of data elements to read.
    | ``"CLEAR | FILL"`` : ``CLEAR`` is for clearing the memory area. ``FILL`` must be use with the argument `<value>`.
    | ``<value>`` : It is the ``FILL`` argument contains the value of the memory to write.

``*MEM <mem_id>``
    | Binary write to memory area.
    | ``<mem_id>`` : Name or address of memory area.
