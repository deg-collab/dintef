\documentclass{beamer}
\usetheme{Warsaw}

\usepackage[utf8]{inputenc} 
\usepackage{listings}

      
%% http://tex.stackexchange.com/questions/144448/color-a-text-line-in-a-code-lstlisting
%% https://www.sharelatex.com/learn/Code_listing
\lstset
{
 escapeinside={<@}{@>},
 numbers=left,
 captionpos=b,
 basicstyle=\tiny,
 numberstyle=\tiny\color{gray},
 backgroundcolor=\color{white},
 showlines=true
}


\setbeamertemplate{itemize item}[circle]


\definecolor{darkgreen}{RGB}{50,150,50}


\title{DAnCE controller development}
\author{lementec@esrf.fr}
\date{21/11/2016}


\begin{document}


\frame{\titlepage}


%%
%% Introduction

\begin{frame}
\frametitle{Introduction}

\begin{footnotesize}

DAnCE instruments expose their \textbf{functional interface} to clients
using \textbf{commands}. One important role of the \textbf{controller} is
to implement these commands.\newline

Hiding implementation details improves maintainability and evolution
\begin{itemize}
\item 1 HSM command: x7 softcores, registers, memories ...\newline
\end{itemize}

An important choice in DAnCE is to have a \textbf{powerful} controller
\begin{itemize}
\item GHz embedded CPU, MB of memory
\item Linux based software environment
\item \textbf{delegate complexity} to CPU (ie. remove from FPGA ...)
\end{itemize}

\end{footnotesize}

\end{frame}


%%
%% Architecture

\begin{frame}
\frametitle{Architecture}

\begin{figure}
\includegraphics[scale=0.2]{dia/main.png}
\end{figure}


\begin{footnotesize}

\textbf{libdance} assists the developper for common tasks
\begin{tiny}
\begin{itemize}
\item Command handling, hardware access, configuration ...\newline
\end{itemize}
\end{tiny}

Client environments are combination of \textbf{users} and \textbf{tools}
\begin{tiny}
\begin{itemize}
\item Beamline scientists, machine operator, developper ...
\item Telnet, Python, SPEC, TANGO ...\\
\end{itemize}
\end{tiny}

\end{footnotesize}

\end{frame}


%%
%% Structure

\begin{frame}
\frametitle{Structure}

\begin{footnotesize}
Most controllers share the same structure\newline
\begin{itemize}
\item Initializing communication layers
\item Initializing internal instrument specific state
\item Accessing hardware resources
\item Handling DAnCE commands
\item Managing the instrument configuration\newline
\end{itemize}
\end{footnotesize}

\end{frame}


%%
%% Initialization

\begin{frame}[fragile]
\frametitle{Initialization}

\noindent\begin{minipage}{.45\textwidth}
\begin{lstlisting}[title=\small{\textit{necst.c}}]
int <@\textbf{\textcolor{red}{init\_application}}@>(const char* port)
{
  /* network init */

  if (<@\textbf{\textcolor{blue}{tcp\_init}}@>(port) != DANCE_OK)
  {
    LOG_ERROR("TCP init error\n");
    return -1;
  }

  /* hardware init */

  if (<@\textbf{\textcolor{darkgreen}{extbus\_init}}@>("10ee:eb01") != DANCE_OK)
  {
    LOG_ERROR("PCIe init error\n");
    return -1;
  }

  /* instrument specific init */

  ...

  return 0;
}
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
\begin{scriptsize}
. \textcolor{red}{Main} routine\\
\begin{tiny}
.. called once to init instrument\\
\end{tiny}
. \textcolor{blue}{Network} communication\\
. \textcolor{darkgreen}{Hardware} communication\\
. Instrument specific\\
\begin{tiny}
.. internal structures\\
.. default states\\
.. ...
\end{tiny}
\end{scriptsize}
\end{minipage}

\end{frame}


%%
%% Hardware access - High level method

\begin{frame}[fragile]
\frametitle{Hardware access - High level method}

\noindent\begin{minipage}{.40\textwidth}
\begin{lstlisting}[title=\small{\textit{daam, dv9\_reg.h}}]

/* define application register list */

#define APPREG_LIST
 REGDEF(<@\textbf{\textcolor{red}{SPI\_CTRL}}@>, <@\textbf{\textcolor{blue}{BAR1}}@>, <@\textbf{\textcolor{darkgreen}{0x80}}@>, ...)
 REGDEF(SPI_AD66, BAR1, 0x84, ...)
 REGDEF(SPI_AD61, BAR1, 0x88, ...)
 REGDEF(SPI_LT62, BAR1, 0x8C, ...)
 ...

\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.50\textwidth}
\begin{scriptsize}
. Registers can be defined in xxx\_reg.h\\
. \textcolor{red}{Name}, \textcolor{blue}{base}, \textcolor{darkgreen}{offset}, infos\\
\begin{tiny}
.. prefered method to hide communication layer\\
.. requires static register addresses\\
.. also works for BRAMs, FIFOs ...\\
\end{tiny}
. Usage in code\\
\begin{tiny}
.. REG\_VALUE(SPI\_CTRL)\\
.. REG\_WRITE(SPI\_CTRL, value)\\
\end{tiny}
\end{scriptsize}
\end{minipage}

\end{frame}


%%
%% Hardware access - Raw method

\begin{frame}[fragile]
\frametitle{Hardware access - Low level method}

\noindent\begin{minipage}{.50\textwidth}
\begin{lstlisting}[title=\small{\textit{necst.c}}]

...

#include "libepci.h"
extern epcihandle_t dev_handles[6];

...

uint32_t necst_read_reg(size_t off)
{
  uint32_t x;

  <@\textbf{\textcolor{red}{epci\_rd32\_reg}}@>(dev_handles[1], off, &x);

  return x;
}

void necst_write_reg(size_t off, uint32_t x)
{
  <@\textbf{\textcolor{red}{epci\_wr32\_reg}}@>(dev_handles[1], off, x);
}

...

\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.40\textwidth}
\begin{scriptsize}
. Direct \textcolor{red}{PCIe} routines\\
\begin{tiny}
.. esp. useful with dynamic register banks\\
\end{tiny}
\end{scriptsize}
\end{minipage}

\end{frame}


%%
%% Commands - introduction

\begin{frame}
\frametitle{Commands - introduction}
\begin{footnotesize}

\textbf{Textual strings} sent to interact with the instrument
\begin{itemize}
\item Configuration
\item Control
\item Data acquisition
\item Administration\newline
\end{itemize}

They consist in a \textbf{name} and optional \textbf{arguments}
\begin{itemize}
\item \texttt{DICFG DI1 50OHM OFF}\newline
\end{itemize}

They can \textbf{return values} (known as queries)
\begin{itemize}
\item \texttt{?CNTVAL CNT4 LATCH}\newline
\end{itemize}

\end{footnotesize}
\end{frame}


\begin{frame}
\frametitle{Commands - introduction}
\begin{footnotesize}

Some commands are \textbf{generic}, and already implemented in
all instruments
\begin{itemize}
\item \texttt{?REG}
\item \texttt{PROG}
\item \texttt{REBOOT}
\item \texttt{?APPNAME}
\item \texttt{?DCONFIG}
\item ... \newline
\end{itemize}

Instrument \textbf{specific} commands must be implemented by the
developper

\end{footnotesize}
\end{frame}


\begin{frame}[fragile]
\frametitle{Commands - definition}

\noindent\begin{minipage}{.45\textwidth}
\begin{lstlisting}[title=\small{\textit{necst\_menu.h}}]
/* choice lists */

#define CNTNAME_LIST
 { "CNT1", "CNT2", "CNT3", ... }

#define FILTCLK_LIST
 { "10MHZ", "20MHZ", ... }

#define APPLISTS
 DANCELIST(CNTNAME)
 DANCELIST(FILTCLK)
 ...

/* command formats */

#define fmtqCNTVAL
 "C#CNTNAME o(T#LATCH | T#RUN)"

/* command menu */

#define APPMENU_LIST
 MENU_IT(<@\textbf{\textcolor{red}{qCNTVAL}}@>, <@\textbf{\textcolor{blue}{fmtqCNTVAL}}@>, <@\textbf{\textcolor{darkgreen}{"uI"}}@>, ...)
 MENU_IT(CNTCFG, fmtCNTCFG, fmtNONE, ...)
 MENU_IT(qCNTCFG, fmtNONE, fmtSTRING, ...)
 ...
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
\begin{scriptsize}
. Defined in xxx\_menu.h\\
. Syntax close to command specs\\
. \textcolor{red}{Name}, \textcolor{blue}{input}, \textcolor{darkgreen}{output}, infos\\
\begin{tiny}
.. 'q': query (ex: ?CNTVAL)\\
.. types: 'I' (integer), 'F' (float) ...\\
.. modifiers: 'u' (unsigned), 'o' (optional) ...\\
.. 'C': choice (ex: CNT4)\\
.. 'T': tag (ex: LATCH)\\
.. more ...
\end{tiny}
\end{scriptsize}
\end{minipage}

\end{frame}


%%
%% Commands - implementation

\begin{frame}[fragile]
\frametitle{Commands - implementation}

\noindent\begin{minipage}{.45\textwidth}
\begin{lstlisting}[title=\small{\textit{necst.c}}]
void <@\textbf{\textcolor{red}{exec\_qCNTVAL}}@>(void)
{
 size_t cnti;
 uint32_t val;
 int err;

 /* get counter index */
 cnti = (size_t)<@\textbf{\textcolor{blue}{I\_CHOICE}}@>();

 /* get the latched or running value */
 if (<@\textbf{\textcolor{blue}{I\_FMT\_NAME}}@>("LATCH"))
  err = get_cnt_latch(cnti, &val);
 else
  err = get_cnt_value(cnti, &val);

 /* handle errors */
 if (err == -1)
 {
  <@\textbf{\textcolor{darkgreen}{errorf}}@>("error getting value");
  return ;
 }

 /* success, send the value */
 <@\textbf{\textcolor{darkgreen}{answerf}}@>("%u\n", val);
}
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
\begin{scriptsize}
. Implemented by \textcolor{red}{functions}\\
. Must match the name in xxx\_menu.h\\
. Automatically called\\
. \textcolor{blue}{Routines} to extract arguments\\
.. \begin{tiny}I\_CHOICE, I\_FMT\_NAME, I\_INTEGER ...\end{tiny}\\
. \textcolor{darkgreen}{Routines} to send results (printf like)\\
.. \begin{tiny}answerf, errorf ...\end{tiny}\\
\end{scriptsize}
\end{minipage}

\end{frame}


%%
%% Configuration

\begin{frame}[fragile]
\frametitle{Configuration}

\noindent\begin{minipage}{.45\textwidth}
\begin{lstlisting}[title=\small{\textit{necst\_conf.h}}]

/* tune the configuration manager */
/* here: called after each commands */

#define APPCONF_MODE CONF_MODE_ALL_COMMS
#define APPCONF_TIMER 0


/* instrument specific structures */

typedef struct { ... } cntconf_t;
typedef struct { ... } diconf_t;
...


/* declare the configuration */

CONF_SCALAR(uint32_t, version, ...);
CONF_ARRAY(<@\textbf{\textcolor{red}{cntconf\_t}}@>, <@\textbf{\textcolor{blue}{cntconf}}@>, <@\textbf{\textcolor{darkgreen}{16}}@>, ...);
CONF_ARRAY(diconf_t, diconf, 2, ...);
...

#define APPCONF_LIST
 CONF_IT(version),
 CONF_IT(cntconf),
 CONF_IT(diconf),
 ...

\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
\begin{scriptsize}
. Backed in controller FLASH memory\\
\begin{tiny}
.. loaded at start\\
.. stored when modified\\
.. behavior can be changed\\
\end{tiny}
. \textcolor{red}{Type}, \textcolor{blue}{name}, \textcolor{darkgreen}{count}, infos\\
. Available in appconf\_xxx variables\\
\begin{tiny}
.. appconf\_version\\
.. appconf\_cntconf[2]\\
\end{tiny}
\end{scriptsize}
\end{minipage}

\end{frame}


%%
%% Data acquisition

\begin{frame}[fragile]
\frametitle{Data acquisition}

\noindent\begin{minipage}{.35\textwidth}
\begin{lstlisting}[title=\small{\textit{pepu\_daq.h}}]

/* describe EBS_DAQ */
#define <@\textbf{\textcolor{red}{DAQ\_DESC}}@>
{
 .pci_bar = 1,
 .pci_rw_off = 0x200,
 .pci_ro_off = 0x220
}


/* describe EBONE DMA */
#define <@\textbf{\textcolor{blue}{DMA\_DESC}}@>
{
 .pci_bar = 1,
 .pci_off = 0x400,
 .ebs_seg = 1,
 .ebs_off = 0x200,
 .flags = EDMA_FLAG_MIF
}


/* enumerate DAQ sources */
#define <@\textbf{\textcolor{darkgreen}{DSOURCE\_LIST}}@>
 DSOURCE_IT("IN1", 0, UINT64),
 DSOURCE_IT("IN2", 1, UINT64),
 ...
 DSOURCE_IT("CALC2", 7, UINT64)

\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.55\textwidth}
\begin{scriptsize}
. xxx\_daq.h automatically enables DAQ\\
\begin{tiny}
.. \textcolor{red}{EBS\_DAQ} registers\\
.. \textcolor{blue}{EBONE DMA} resources\\
.. \textcolor{darkgreen}{DSOURCES} information\\
\end{tiny}
. Generic DAQ commands\\
\begin{tiny}
.. DSTREAM: configure and control stream\\
.. ?DSTREAM: query stream info\\
.. ?DSOURCE: query possible sources\\
\end{tiny}
. Example in pepu/client/tbench/daq
\end{scriptsize}
\end{minipage}

\end{frame}


%%
%% DAnCE extensions

\begin{frame}[fragile]
\frametitle{DAnCE extensions}

\noindent\begin{minipage}{.45\textwidth}
\begin{lstlisting}[title=\small{\textit{pepu\_dext.h}}]

/* list internal functions that */
/* can be called by extensions */

DEXTFUNC_DECL1
(void, set_chan4, uint32_t);

DEXTFUNC_DECL1
(void, set_di1, uint32_t);

#define DEXTFUNC_LIST
 DEXTFUNC_IT(set_chan4),
 DEXTFUNC_IT(set_di1)

\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.55\textwidth}
\begin{scriptsize}
. Extensions are user defined routines\\
. Written in C language\\
. Compiled locally (.so) or on the instrument\\
. Executed periodically or during events\\
. xxx\_dext.h exports available functions\\
. Generic commands\\
\begin{tiny}
.. DEXT: send, execute an extension\\
.. ?DEXT: query extension info\\
.. ?DSOURCE: query possible sources\\
\end{tiny}
. Examples in pepu/client/tbench/dext
\end{scriptsize}
\end{minipage}

\end{frame}


%%
%% Other development tasks

\begin{frame}
\frametitle{Other development tasks}

\begin{footnotesize}
Refer to documentation/design\_docs/necst\_controller.doc
\begin{itemize}
\item Building: section 3
\item Installing: sections 4 and 6
\item Testing: section 5
\item Running: section 4
\end{itemize}
\end{footnotesize}

\end{frame}


%%
%% Debugging

\begin{frame}[fragile]
\frametitle{Debugging}

\noindent\begin{minipage}{.45\textwidth}
\begin{lstlisting}[title=\small{\textit{necst.c}}]
int init_application(const char* port)
{
  /* network init */

  if (tcp_init(port) != DANCE_OK)
  {
    <@\textbf{\textcolor{red}{LOG\_ERROR}}@>("TCP init error\n");
    return -1;
  }

  /* hardware init */

  if (extbus_init("10ee:eb01") != DANCE_OK)
  {
    <@\textbf{\textcolor{red}{LOG\_ERROR}}@>("PCIe init error\n");
    return -1;
  }

  /* instrument specific init */

  ...

  return 0;
}
\end{lstlisting}
\end{minipage}\hfill
\begin{minipage}{.45\textwidth}
\begin{scriptsize}
. \textcolor{red}{LOG\_xxx} routines\\
\begin{tiny}
.. LOG\_ERROR\\
.. LOG\_WARN\\
.. LOG\_COMM\\
.. LOG\_APP\\
.. ...\\
\end{tiny}
. Need -lXXX command line options\\
\begin{tiny}
.. -lERROR:0 for error messages\\
.. ...\\
\end{tiny}
\end{scriptsize}
\end{minipage}

\end{frame}


%%
%% Source code organization

\begin{frame}[fragile]
\frametitle{Source code organization}

\begin{footnotesize}
A typical instrument SVN repository is organized as follows\\
\begin{lstlisting}
$> ls ~/repo/svn/necst
client  documentation  firmware  hardware
\end{lstlisting}
\end{footnotesize}

\begin{footnotesize}
The controller source code is in firmware/controller\\
\end{footnotesize}

\begin{lstlisting}
$> ls ~/repo/svn/necst/firmware/controller
Makefile  necst.c  necst_conf.h  necst_menu.h  necst_reg.h
\end{lstlisting}

\begin{footnotesize}
Makefile assumes source files are prefixed by instrument name
\end{footnotesize}


\end{frame}


%%
%% Examples

\begin{frame}
\frametitle{Examples}

\begin{footnotesize}
The best documentation so far are existing instruments
\begin{itemize}
\item hsm, pepu necst: complex commands and features (IRQs, DAQ ...)
\item xnap, smrtpxdub: detectors
\item simdeep: basic, simulation purposes\newline
\end{itemize}

Also some HOWTOs in libdance/doc/HOWTOs (WIP)

\end{footnotesize}
\end{frame}


\end{document}
