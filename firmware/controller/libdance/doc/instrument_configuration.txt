Currently, a command is sent to the instrument (?INFO of ?CFG) and the
instrument returns a list of commands that, when reproduced by the client,
restore the instrument configuration. The advantage is that it is human
editable, and there is no need for additional utilities. However, it is
not well suited for binary parameters (2D detectors matrices ...).

What we proposed is to keep this scheme. Binary data are handled a bit
differently: a set of commands allow to store binary data by ID in the
instrument memory (ram, flash ...) . Then, the binary data can be referenced
by ID in the different commands. For instance:
STORE_BIN matrix.0 #local_filename
SET_IMAGE_MASK matrix.0

Main ideas from early meetings:
. no need for a particular client, should be run in SPEC ...
. better if the configuration is self contained
. schedule a meeting with BCU people to discuss about integration. we
want to have a solution to propose, and then see how it can be integrated
into their tools (LIMA ...)
