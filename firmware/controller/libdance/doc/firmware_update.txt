[ system disk organization ]
. partition
. mount points
. directory structure


[ image file format ]
. raw disk image
-> LFS output. used for initial disk install.
. packaged disk image: header, gzip compression
-> also an LFS output. used for firmware update.


[ update procedure ]
. the client sends a PROG command with the xxx.lfs.pak contents
. the instrument receives the package and update the disk partitions
. the instrument mounts the new filesystems in temporary location
. the instrument executes /new_root/etc/dance/update (either a command
list, shell script or binary)
. the client eventually reboots the system


-- todo

instruction file:
the update procedure can involve several actions. For instance,
smartpix may require to update both the system, the local FPGA and the remote
FPGA. Possible commands include:
. control flow
. builtin commands (FSPI ...)
. run arbitrary executable, possibly located in the new system
-> investigate embedded shell or interpreter
=> the install procedure is a shell script with a well defined location.
this script is forked in an environment with the SYS_ROOT set to the new
system being installed. SYS_ROOT can be different from the actual root.
this variable is set by the calling process, and can be different from the
current ROOT depending on the install stage.

identification:
every components of the system should be identifiable, and this identifier must
be known from the update procedure.

read / write persistent store tool:
there is no read write filesystem in the instrument. However, there is a flash
area that can be read and written. This area is known as the instrument store
area.
an example of store is the instrument configuration values.
binary tools and API are provided to access this store.
-> investigate the use of a filesystem instead of flash. this has not been done
for 2 reasons:
. synchronizing a filesystem is much longer than direct writing. the instrument
application cannot afford such latencies.
. when no firmware packaging format existed, we wanted to keep the disk file
continuous and the partition count limited. this could not be done when adding 
a new filesystem. review that.


--

update procedure:
. the UPDATE command is sent by the client with the firmware package sent
compressed as an argument.
. the instrument receives the package and update the disk. the new root
file system is available in /tmp/new_root.
. the instrument then executes the contents of /tmp/new_root/etc/dance/update
this file can be a shell script, a binary executable or a list of commands
to be executed within the context of the application (I doubt this last option
is really useful since shell script can be used. this is low priority, but the
possibility is left open).
. if appropriate, the client can then reboot the system

boot procedure:
. the system executes all the scripts in /etc/init.d
. /etc/dance/update is executed to let a chance for the system to continue
any pending update process.
