$Id: danceNotes-PAF.txt 1992 2018-12-11 18:43:55Z fajardo $

DANCE general notes
===================

general libdance:
  - mechanism to reserve/block resources that are used by asynchronous code
  - flag mutually exclusive commands

dance controller Makefile:
- include support for building pluggins

firmware reprogramming:

- package info description:
    1. include list of efpack actions (add_file, add_bit, ...) in butils.cfg
       # if possible simplify the parameters by using butils names (target, component...)
       # idea: consider a new command "efpak add_info" for this       
    2. butils augments the list of actions with additional info/metadata (version, platform, ...)
       and generates a full extended efpak file with metadata
       # generic mandatory modules (tools, drivers, ...) could be included in this phase
    3. execute a new "efpak build" command that takes the list of commands and executes them
       # the new command must include the command list as an additional block/file in the package


- multiple platforms/hardware/kernel versions:
   - define an ID (specific for reprogramming) for each platforms/hardware/kernel version
   - Include platforms/hardware/kernel IDs in each block of the package (or ANY for everybody)
     # this should include also the application ID 
     # incompatible blocks should be discarded at reprogramming
   - be very restrictive: this is different from software/firmware versions
   - pack multiple platforms/hardware in a single efpack package
      # opt 1:  packages of full packages: may have duplicated modules)
      # opt 2:  flat structure (no nesting: does not allow to split packages easily
    
- multiple software versions
    - manage version info: do not reprogram if no change (although also --force)
    - filtering/skipping could be done by means of efpack hooks
       this is different from hardware incompatibility
    
- instrument reprogramming procedure:
   - reprogramming = reflash + reinit (reboot | rerun | reset)
        # the tricky phase is reflashing
        # then {reboot | rerun | reset}       
   - do not reflash while transmitting efpak.
   - prevent PROG command execution (in other ports) during reflashing
   - complete cleanly any pending commands (all clients)
        # decide whether to close or not other comm ports(not the programmer')
        # how to inform the other clients? this could also be used for any RESET
   - use the new controller in the package to reflash:
        # the old controller (via *PROG) receives the efpak package, filters, discards
          blocks and saves a reduced efpak package that has to be reprogrammed.
        # once this is done, the old controller starts the new controller (as a hot upgrade)
        # the new controller detects the efpak package in disk and proceeds with the reflashing
        # at the end of the process the instrument is reset or rebooted (last efpack action?)


- PROG command:
   - In principle the *PROG command does not need any parameter. The type of prog approach (immediate
     reprog for development, or regular/normal/full firmware reprog) is deduced/extracted from the
     efpack package.
   - During regular/normal/full firmware reprog, the controller goes into a special prog state.
   - a ?PROG query must report if prog state and percentage of progres.
   - how to maintain the sockect connection alive: 
      1. open a system pipe between the old and new controllers
      2. the old controller goes into a state in which it only forwards the socket
         data to the new one (full duplex) and does nothing else
      3. the new controller must be able start using the pipe as comm channel

- FPGA reprogramming:
   - efspi (lib + tool + vhdl): programms FPGA PROM (flash)
       problem: vhdl evolves and may not be in sync with controller!! 
   - ejtag (lib + tool + usb-jtag chip): loads the FPGA RAM (with a proper efspi)
       limitation: not all instruments include the usb-jtag chip
   - issue: FPGA interrupts do not work after PCIe reenumeration (needs reboot) 
    
- system reprogramming:
   - basically use the current scheme already implemented in efpak:
        # flash the new system image in a reserved area of the flash
        # pivot the root file system to the new system
        # modify the botloader to point to the new system
   - possible variants/changes/improvements:
        # implement an option to pivot or not to the new root file system
        # implement an option to reboot or not at the end of the process
        # add a PROG option to reprogram in a different file system 
             this is for programming platforms with two flash memories
        # review the max size of the system partitions:
             not app is empty but takes 512MB!! 
- TODOs:
   - check that libdance can process binary input streams (very big files)
   - consider always two dance controllers listening on different ports:
        # the normal instrument controller 
        # a basic/generic/minimalistic to allow reprogramming in all cases   
      - review discuss the port numbering: can we stay like this forever?
   - system modifications:
        # add the two controllers (two copies of the basic one)
        # remove developer tools (efspi, jtag, ...) and add them to the efpak package

        
Application/libdance considerations:

- configuration of controller resources: 
   - support for multiple controller hardware configurations:
     - the hardware configuration is selected by an option passed in the command line (no dynamic change)
     - there is always one DEFAULT configuration
     - declare additional configuration names at the beginning of dance_defs.h  (SIMUL, RNICE, NOHARDWARE, ...)
     - insert macros in dance_defs.h that select the 'valid' configurations for the subsequent resources
         ACTIVE_CONFIG(DEFAULT, SIMUL)
         .... < resource declaration>
         ACTIVE_CONFIG(NOHARDWARE)
         .... < resource declaration>
     - may need predefined configuration names. In that case use aliases for SIMUL, RNICE, ...

   - consider url-like syntax for hardware resource identification:
          "pcie://.../../", "i2c://.../..." 

- INIT:
		derr_init()               : still incomplete:  management of previous error messages
		commled_init()            : management of COMM LED
		hwinfo_init()             : extracts/manages hardware identification
		conf_init()               : initialises non-volatile instrument configuration
		eth_init()                : manages Ethernet link (IP config, fix issues...) 
		runtime_init()            : controller runtime
          timerlist_init()      :   timers in runtime
		iodata_init()             : initialises command tables and parsing
		init_application()        :    application specific initialisation
		extbus_build_regtable()   : pre... (?)
		daq_init()                : DAQ
		dext_init()               : plug-in management (DANCE extensions)

   application:
      extbus_init()             : REG and MEM mapping
      tcp_init()                : TCP comm socket
      http_init()               : HTTP server 
      log_init()                : Log machinery
      uirq_init_lib()           : initialises interrupts
           uirq_register_isr()  :    register ISRs
      edma_init()               : initialise EBONE DMA
      tqueue_init()             : task queues
      
      decomp??                  : decompression tool
      
- Dependencies:
   comm --> conf  (to commit changes)

   conf --> disk  (direct access to flash disk, bypassing the file system)
   PROG --> disk

   commled --> i2c
   hwdinfo --> i2c

   REG, MEM, ...  --> extbuscomm  (to be reviewed)

   daq --> edma

   dext --> see HOW-TO
   decomp unused? used by disk?
   
- ERROR management (as started by derr)
   - include more info in errors (line, file, ...)
   - change dynamically the level of verbosity (with dedicated command)
   - report on previous errors (only last one, number of errors, ...)
   
- API for asynchronous termination
   - consider a standard mechanism to ease programming of delayed actions included
     in regular commands
   - include how to check the asynchronous termination (ISR, DMA, timer, ...)


    