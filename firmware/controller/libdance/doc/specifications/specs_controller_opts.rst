.. sectnum::
    :suffix: .


=========================================
  Dance controller application arguments
=========================================

:Info: Specification about what options are available for DAnCE controller application
:Revision: $Revision: 2138 $
:Date: $Date: 2019-11-28 17:14:08 +0100 (Thu, 28 Nov 2019) $

|

----------------

|

.. contents::

|

----------------

Introduction
============

| DAnCE controller can be launched by arguments that allows to enable functionnalities

Synopsis
========
``{~binary_path}/controller -h -a[name] -p[deep-port] -w[http-port] -l[class:level]``

[-h, --help] Online help
==========================

| Online help can be displayed by using the option ``-h`` or ``--help``.
    ``{~binary_path}/controller -h``

[-a,--addr] Controller name
==============================

``{~binary_path}/controller -a[name]``

| To identify the controller, it will be required to give a logical name for the controller.
| The option ``-a`` is used to define a custom name to the controller instance using argument ``[name]``.
| This name can be read throught DAnCE query ``?ADDR``.

[-c,--cfg] configuration file or path
=====================================

``{~binary_path}/controller -c [config_file or config_path]``

| A configuration file or path where configuration file a stored can be define with the option ``-c`` or ``--cfg``.
| Using a configuration path, the controller will load the configuration file name : ``dconfig_[APPNAME]_[ADDR].cfg``.
| ``[APPNAME]`` and ``[ADDR]`` will be replace by value used by the controller.

[-p, --port] TCP port
=====================

``{~binary_path}/controller -p[params]``

| Multiple instance of DAnCE controller can be launch on an instrument.
| Each controller needs its own listening TCP port for DEEP communication.
| Using the option ``-p, --port`` allows to customize TCP DEEP port.



Automatic port
--------------

``{~binary_path}/controller``

| Launching the controller without port argument will let the application to scan and find a usable TCP port for DEEP communication.
| By default, the port range is from 5000 to 5010.


Specify a port
--------------

``{~binary_path}/controller -p[port]``

| The option ``-p`` can be used to define the controller TCP DEEP port.
| By given the port number on the parameters ``[port]``, the controller will start will this port. If the port is used an error is raised.
| Note : The first available port will be used.

Specify a port range
--------------------
``{~binary_path}/controller -p[starting_port]:[step]``

| The option ``-p`` can be used to define the controller TCP DEEP port range.
| The parameter ``[starting_port]`` is the starting port number that the application use to start the scan.
| The parameter ``[step]`` defines the number of step to scan.
| For example ``{~binary_path}/controller -p5000,5``, the controller start searching available port from 5000 to 5005.
| Note : The first available port will be used.

[-w or --httpport] HTTP port
============================
``{~binary_path}/controller -w[params]``

| A http server is available on DAnCE controller.
| This server display a DEEP commands prompt.

Default port
------------

``{~binary_path}/controller``

| Launching the controller without port argument will use by default the port 80.

Specify a port
--------------
``{~binary_path}/controller -w[http-port]``

| The option ``-w`` can be used to define the controller HTTP port with the parameter ``[http-port]``.

Specify a port range
--------------------
``{~binary_path}/controller -w[starting_port]:[step]``

| The option ``-w`` can be used to define the controller HTTP port range.
| The parameter ``[starting_port]`` is the starting port number that the application use to start the scan.
| The parameter ``[step]`` defines the number of step to scan.
| For example ``{~binary_path}/controller -p8080,5``, the controller start searching available port from 8080 to 8085.
| Note : The first available port will be used.

[-l or --log] Logging messages
==============================
``{~binary_path}/controller -l[params]``

| Logging message can be activated by using the option ``-l``. The output will be on standard linux output
| Give in parameter ``[params]`` the log name or class:level.
| Here a list of available log name :

    - TRACE
    - INFO
    - ERROR
    - WARN
    - EXTBUS
    - COMM
    - CMD
    - CONF
    - MALLOC
    - APP