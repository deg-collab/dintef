[$Id: commands-binary-HOWTO.txt 2143 2019-12-13 12:44:56Z fajardo $]

DAnCE binary commands HOWTO
v0.00, 13 Dec 2019

How to implement DAnCE binary commands and queries.
______________________________________________

  Table of Contents

 0. Disclaimer
 1. Introduction
 2. libdance operation and specific data structures
 3. Examples of binary commands
 4. Examples of binary queries
 5. Further information


______________________________________________________________________

0.  Disclaimer

The following document is offered in good faith as assistance to develop DAnCE embedded instruments. No guarantee is provided that this information is accurate and no responsibility is accepted by the anonymous author for any loss of time or any damage caused in any way to any person or equipment, as a direct or indirect consequence of following these instructions.


1.  Introduction

The most recent version of this document can always be found at https://deg-svn.esrf.fr/svn/libdance/trunk/doc/HOWTOs/commands-binary-HOWTO.txt

There are no Japanese or French translations of this document.

This document provides basic information on how to implement instrument specific DEEP binary commands and queries in a DAnCE embedded software application. Such implementation is eased by the use of the facilities included in the libdance library that require to follow the conventions described in this document.

See https://deg-svn.esrf.fr/svn/libdance/trunk/doc/HOWTOs/commands-HOWTO.txt for further information about general command implementation in DAnCE controllers.


2. libdance operation and specific data structures




3. Examples of binary commands

case) --- There is no a pre-existing buffer to receive the data.

void commexec_qbMYBINCOMMAND(void) {
   binary_t* binfo = get_binary_com_info();    // get binary information
   size_t    size = binfo->unit * binfo->size; // the number of bytes received;
   void*     databuff;         // pointer to the buffer, to be initialised

   // allocate the memory needed to receive the binary data block
   if (!(databuff = malloc(size))) {
      errorf("No enough memory");
      return;
   } else if (get_binary_data(databuff, binfo->unit, binfo->size) < 0) {
      errorf("Error reading data");
   } else {
      // data is in the buffer at this point
      // do whatever is needed
   }
   // if you do not need the buffer it at this point, free the memory
   free(databuff);
}


4. Examples of binary queries

The following examples assume transferring bytes (_D8). If not, all teh calls to send_binary_buffer() must be modified accordingly.
The error checking is removed for simplicity.

case) --- Static buffer of known size.

    void commexec_qbMYBINQUERY(void) {
       void*     databuff; // pointer to the static buffer, already initialised
       size_t    size = [[ the size of the buffer]];
       mblock_t* mblk;

       mblk = mblock_create(NULL, databuff, size, MEM_DEFAULT);

       // and then just send it
       send_binary_buffer(mblk, _D8, size, BIN_FLG_NOCHKSUM);

       // and one should not free databuff, it is assumed to be static...
    }


case) --- Buffer allocated dynamically by the query.
      --- Size is known in advance.

    void commexec_qbMYBINQUERY(void) {
       void*     databuff; // pointer to the buffer, to be initialised
       size_t    size = [[ the size of the buffer, must be known in advance]];
       mblock_t* mblk;

       mblk = mblock_create(NULL, NULL, size, MEM_DEFAULT);

       databuff = mblock_ptr(mblk);  // a buffer of the given size

       // now fill the buffer in databuff somehow ...

       // and then just send it
       send_binary_buffer(mblk, _D8, size, BIN_FLG_NOCHKSUM);

       // and one should not free databuff here!! that will be done later.
    }


case) --- Buffer allocated dynamically by someone else .
      --- Size is not known until the allocation is done.

    void commexec_qbMYBINQUERY(void) {
       void*     databuff =  [[a malloc() allocated buffer by someone else]];
       size_t    size =      [[the size of the buffer obtained after allocation]];
       mblock_t* mblk;

       mblk = mblock_create(NULL, databuff , size, MEM_TRANSFERRED);

       // no need to copy data copy data just send the buffer
       send_binary_buffer(mblk, _D8, size, BIN_FLG_NOCHKSUM);

       // and one should not free databuff here!! that will be done later.
    }


5. Further information

