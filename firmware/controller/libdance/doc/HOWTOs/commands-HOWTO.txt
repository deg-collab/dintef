[$Id: commands-HOWTO.txt 2216 2020-05-28 07:53:38Z bideaud $]

DAnCE commands HOWTO
v0.00, 08 Feb 2015

How to declare and manage DAnCE commands.
______________________________________________

  Table of Contents

 0. Disclaimer
 1. Introduction
 2. DEEP synchronous messages
 3. Adding commands and queries
 4. Argument types and prefixes
 5. Argument separators
 6. Composite arguments
 7. Tag tokens and lists of symbolic strings
 8. Type specific format modifiers and considerations
 9. Command execution
10. Retrieving input arguments
11. Errors
12. Command output
13. Further information


______________________________________________________________________

0.  Disclaimer

The following document is offered in good faith as assistance to develop DAnCE embedded instruments. No guarantee is provided that this information is accurate and no responsibility is accepted by the anonymous author for any loss of time or any damage caused in any way to any person or equipment, as a direct or indirect consequence of following these instructions.


1.  Introduction

The most recent version of this document can always be found at https://deg-svn.esrf.fr/svn/libdance/trunk/doc/HOWTOs/commands-HOWTO.txt

There are no Japanese or French translations of this document.

This document provides basic information on how to implement instrument specific DEEP commands and queries in a DAnCE embedded software application. Such implementation is eased by the use of the facilities included in the libdance library that require to follow the conventions described in this document.

DAnCE compliant instruments talk to the external world by using the DEEP communication protocol. This is a simple protocol that can be easily transported through any full duplex communication channel. Most of DAnCE instruments rely on Ethernet/TCP sockets to implement the communication channel but the DEEP protocol is not restricted to any particular connection scheme.
Although the DEEP protocol provides support for asynchronous data transfers, the asynchronous capabilities are described somewhere else. This document only refers to the mechanisms aimed at implementing the basic control capabilities of DAnCE instruments by means of sets of ASCII formatted synchronous messages that send or receive either ASCII or binary data blocks to or from the instrument. The protocol also provides mechanisms to report acknowledgement or error messages to the caller. Even though some basic concepts are introduced below, the details of the DEEP protocol are out of the scope of this document, for a more detailed description see [Ref. needed].


2. DEEP synchronous messages

The DEEP messages are always produced by the user of the DAnCE instrument, that we call 'the caller' in this documents and that may be either a computer program or human operator typing in a computer terminal. The messages are transmitted to the DAnCE instrument through the communication channel and must be of one of two types: commands or queries.
Commands are messages that transmit information from the caller and that are never followed by an answer from the instrument. On the other hand, queries are messages that must necessarily return an answer to the caller.

It is to be noted that in both case an acknowledge is returned to the caller. An error is returned is the command or querry doesn't match the expected format. By default, a "OK" message is returned to the caller if the format matches the expected format. It is possible to explicitely return a custom error message using the "errorf("Message\n");" function, if, for example, the function associated to the given command fails.

Both commands and queries are represented by ASCII strings starting by a message specific keyword that is followed by optional parameters that are treated as input arguments. The message keyword and the optional arguments are separated by white spaces. A command keyword consists of an alphanumerical string eventually preceded of a asterisk character '*'. Commands starting by an asterisk must transmit a binary data block following the optional ASCII arguments.

Query keywords are constructed following the same conventions than command keywords by they must in addition include a question mark character '?' as the first character of the message keyword. In this scheme binary queries are identified by the presence of the asterisk character between the question mark and the first alphanumeric character. Both types of queries, binary or not, return an answer to the caller that is an ASCII string that can be optionally empty. In addition to the ASCII answer, binary queries return a block of binary data to the caller.

It is worth to note that in the DEEP protocol the same alphanumeric string may serve as basis for four different message keywords. For instance "CONFIG", "*CONFIG", "?CONFIG" and "?*CONFIG" are four different valid message keywords that could be implemented in the same DAnCE instrument as four complete independent messages: two commands and two queries.

The alphanumeric strings in DEEP keywords must always begin with a letter and consist of a combination of letters (A-Z), numbers (0-9) and the underscore character '_'. The usage of other characters is forbidden. Although when they are sent by the caller, the DEEP messages are case insensitive, for the internal implementation in a DAnCE instruments the strings in both keywords and parameters must be represented always in uppercase.


3. Adding instrument commands and queries

The commands and queries in a DAnCE device are declared by defining the APPMENU_LIST in the header file <instrument>_menu.h where <instrument> is the specific name of the DAnCE instrument. See the file https://deg-svn.esrf.fr/svn/_template/dev/src/template_menu.h as an example. The APPMENU_LIST must consist of a list of MENU_IT() preprocessor macro instances where each instance corresponds to one individual command or query definition according to the syntax:
  MENU_IT(<comId>, <inFormat>, <outFormat>, <comType>, <description>)

The argument <comId> must be a valid C language symbol that is used to identify the particular command or query across the program code. It should be constructed starting from the DEEP command ASCII keyword by replacing the eventual '?' and '*' initial symbols by the lowercase 'q' and 'b' characters respectively. Every <comId> identifier is associated to a matching exec_<comId>() function that must be defined by the instrument developer and that is called by the libdance library every time that the instrument receives that particular command or query.

Examples:

  DEEP keyword     comId    execution function  type of message
  ------------   ---------  ------------------  ------------------
     "CONFIG"      CONFIG     exec_CONFIG()     ASCII-only command
    "*CONFIG"     bCONFIG     exec_bCONFIG()    binary command
    "?CONFIG"     qCONFIG     exec_qCONFIG()    ASCII-only query
   "?*CONFIG"    qbCONFIG     exec_qbCONFIG()   binary query


The <inFormat> value in MENU_IT() macros must be a quoted ASCII string that describe the format of the input arguments of the DEEP message. At the reception of the command or query by the instrument, the argument string that follows the command or query keyword is parsed internally by the libdance runtime according to the <inFormat> string. The input string is considered composed by elementary arguments that once parsed and extracted are available in internal C variables to the command execution code. The types of arguments and the syntax of the format string <inFormat> is described in subsequent sections.

The <outFormat> value is a quoted ASCII string similar to <inFormat> but that describes the format of the answer produced by a DEEP query. Plain commands, as they not return any answer from the instrument to the caller, have a NULL <outFormat> value. The syntax and the available elementary argument descriptors in an <outFormat> string are the same than those used in <inFormat> strings for the parsing of the input parameters.

The <comType> argument is a preprocessor symbol that is used to classify the particular command or query. So far <comType> is only used to select or filter the instrument commands and queries returned by the standard "?HELP" query. Valid values for <comType> are either _B, _E, _T  that identify the commands respectively as 'Basic', 'Expert' or 'Test'.

The <description> value is a valid quoted string is only used for informative purposes when requested by the standard "?HELP" query. Some or all the commands of a DAnCE instrument may have the <description> value set to an empty string ("").


4. Argument types and prefixes

The input and output format strings described by <inFormat> and <outFormat> values in MENU_IT() macros are formed of a sequence of elementary argument descriptors separated in the most usual case by white spaces. The only exceptions to this rule is the use one of the special separator characters. The format strings may also include grouped sequences of arguments enclosed in brackets that are called composite arguments and that are parsed or generated as a whole.
The type of elementary arguments is identified by an uppercase letter code as it is summarised in the following table that also includes the two types of brackets used to define composite arguments:

    Type        Code   Description
    ----------  ----   -----------------------------------
    Boolean      B     Boolean value
    Integer      I     Signed or unsigned integer value
    Hexa         H     Unsigned hexadecimal value
    Float        F     Floating point value
    Unit         U     Unit factor
    Tag          T     Symbolic tag
    Flaglist     G     List of symbolic flags
    Choice       C     Selection in predefined list
    Label        L     Compact ASCII label (no white spaces)
    String       S     Arbitrary ASCII string
    Scomposite   ()    Sorted group of values
    Ucomposite   {}    Unsorted group of values


Both elementary and composite arguments may or, in some cases, have to be complemented with prefix and/or suffix modifiers that change or complete the way the argument is parsed. For instance the string "F uI" represents a floating point value followed by an unsigned integer.
There are two types of prefixes, repetition and numerical, as well as various kind of suffixes. Numerical prefixes and all the suffixes are specific to each type of elementary argument and are described separately in the corresponding sections below.

Repetition prefixes modify the number of times that an argument value appears in the input or output string. A repetition prefix can be an integer number that determines the number, by default one, of times that the argument is repeated in the input or output string. However the prefix may also indicate that an elementary argument is optional or that it may appear repeated an undefined number of times. The repetition prefixes are:

  Prefix  Meaning
  ------  -------------------------------
   'o'    Optional, the argument may be missing in the input string
   'n'    Arbitrary, the argument can be repeated a not null arbitrary number of times
   'on'   Optional+Arbitrary, the argument may be missing or arbitrarily repeated
  <int>   Repeated, the argument must be repeated exactly <int> times

Repetition prefixes can be used with any elementary or composite argument regardless of its type and can be combined with any type specific modifier. For instance the string "oF 3uI" represents an optional floating point value followed by three unsigned integers.

Numerical prefixes can only be used with numerical arguments and indicate if the arguments are unsigned or positive. Positive numerical arguments are unsigned values than cannot take the value zero. The numerical prefixes are:

  Prefix  Meaning
  ------  -------------------------------
   'u'    Unsigned, the argument cannot be a negative value ( >=0 )
   'p'    Positive, the argument must be greater than zero ( >0 )


5. Argument separators

The argument descriptors in the format strings are in general separated by whitespaces and/or by special separator characters. If the descriptors are separated only by any arbitrary number of whitespaces, consecutive arguments are considered with no any particular relationship between them. The use of separator characters permits to specify two types of argument relationship: exclusivity and dependency. The separator characters are the following:

  Separator  Meaning
  ---------  -------------------------------------
     '|'     Exclusivity, only one argument will be parsed in the sequence
     '_'     Dependency with automatic whitespaces for input parsing
     '-'     Dependency with no whitespaces for input parsing
     '+'     Dependency with mandatory whitespaces for input parsing

If consecutive argument descriptors are separated by the exclusivity character, all the arguments concerned are considered as mutually exclusive, i.e. only one of the arguments in the sequence will be parsed.

When argument descriptors are separated by dependency separators, the last arguments are considered dependent on the previous one. A dependent argument is only parsed if the previous one in the format string has been parsed succesfully. This feature is particularly useful in input strings when the previous argument is optional. In that case if the argument is missing the dependent argument descriptor is skipped.
A particularity of dependent arguments is that they may appear in the input or output strings without any whitespace separation from the previous argument. The behaviour may be different for input and output parsing. In the case or input parsing, the particular separator character used in the format description string determines one of three whitespace parsing rules. Whitespace parsing can be either forbidden (whitespaces separation is not allowed), mandatory (whitespaces are required) or automatic (the parser accepts any of both possibilities).
For output parsing, the use of whitespaces as separators between dependent arguments is not determined by the dependency separator used in the format description. Output arguments will be separated by whitespaces only if the separation between format descriptors includes also whitespaces in addition to the dependency separator.

[ADD SOME EXAMPLES TO ILLUSTRATE THIS MESS]


6. Composite arguments

Composite arguments are groups of other arguments that are defined as such when they are enclosed together in brackets within the format string. The composite arguments can be preceded of repetition prefixes and are treated and parsed as a whole. In this ways an ensemble of elementary arguments can be for instance declared as optional or repeated a given number of times. For instance the string "n(B I)" refers to an arbitrary number of pairs of boolen plus integer arguments.

Normal brackets are used to group arguments in a 'sorted' way, i.e. the arguments must appear in the input string in exactly the same order than in the format string. If the various arguments may appear in an arbitrary order, the composite argument should be declared as 'unsorted' by using curly brackets instead.

Composite arguments may be nested and therefore contain any combination of elementary arguments and other composite arguments.


7. Tag tokens and lists of symbolic strings

Each 'tag' argument is associated to a unique predefined single string token that must be included in the tag descriptor following the '#' character. For instance the argument "T#SIZE" refers to a 'tag' argument that must literally match the string token "SIZE".

The elementary arguments 'flaglist', 'choice' and 'unit' are also associated to predefined symbolic ASCII strings but in this case the symbolic strings are multiple. The strings are grouped in lists that have an unique name for identification purposes. In this way it is possible to associate the elementary argument to the list by using a descriptor including a suffix that consists of the '#' character followed by the name of the list.
For instance the argument descriptor "C#DIR" refers to a 'choice' argument where the input characters must match one of the symbolic strings included in the list named 'DIR'. The descriptor "U#TIME" refers to a 'unit' argument that used the list 'TIME'.

In the case of 'flaglist' and 'choice' descriptors, each list of symbolic strings must be defined by means of a <name>_LIST macro where <name> is the name of the list. The macro definition must follow:

#define <name>_LIST {"string1", "string2", ... "stringN"}

where "string1" to "stringN" are the symbolic strings in the list. All the DAnCE lists used in instrument commands must be declared in the predefined APPLISTS macro by including one DANCELIST(<name>) invocation per list. As a complete example, the following macro definitions declare two lists named 'POLARITY' and 'DIR':

#define APPLISTS       \
   DANCELIST(POLARITY) \
   DANCELIST(DIR)      \

#define POLARITY_LIST {"NORMAL", "INVERTED"}
#define POLARITY_NORMAL    0
#define POLARITY_INVERTED  1

#define DIR_LIST {"NONE", "FORWARD", "BACKWARD"}
#define DIR_NONE       0
#define DIR_FORWARD    1
#define DIR_BACKWARD   2

The previous example defines also integer valued symbolic values such as POLARITY_NORMAL or POLARITY_INVERTED for instance that can be used in the C code to refer to the elements of the list 'POLARITY'.

The lists of 'unit' arguments are declared by defining the macro APPUNITS as follows:

#define APPUNITS      \
   DANCEUNITS(WEIGHT) \
   DANCEUNITS(POWER)  \

#define WEIGHT_UNITS \
   UNITDEF(g,  1.0)  \
   UNITDEF(mg, 1e-3) \
   UNITDEF(kg, 1e+3) \

#define POWER_UNITS  \
   UNITDEF(w,  1e+0) \
   UNITDEF(mw, 1e-3) \
   UNITDEF(uw, 1e-6) \


Another type of elementary arguments that may optionally make use of symbolic lists is 'boolean'. The logic values in boolean arguments may also be associated to symbolic ASCII strings. In that case the asosciated list must have only two elements. It is possible to define multiple boolean lists by including several BOOLDEF(<trueId>, <falseId>) instances in the predefined APPBOLEAN macro as it is shown in the example:

#define APPBOOLEAN    \
   BOOLDEF(YES,  NO)  \
   BOOLDEF(On,   Off) \
   BOOLDEF(RUN,  STOP)\

Note that the identifiers of the true values ('YES', 'On' and 'RUN' in the previous example) must appear as the first argument of the BOOLDEF macros and are also used for identification of the individual boolean symbolic lists in the definitions of the elementary argument (see the description of 'boolean' modifiers below).


8. Type specific format modifiers and considerations

Each type of elementary argument must or may accept specific modifiers that affect how the parsing will be implemented. The considerations and modifiers that are specific to each type of argument are described below.


B : Boolean:
------------
This type of arguments are converted to integers that take the possible values 'not null' (true) or 'null' (false). Boolean arguments may be described in format strings as "B" or "B#<bTrue>" where <bTrue> must be one of the 'true' values defined in the boolean lists included in the APPBOOLEAN macro.
If a <bTrue> value is specified, the corresponding boolean list is used for parsing. For instance if the argument is especified as "B#YES" and the boolean list BOOLDEF(YES, NO) is included in APPBOOLEAN, the strings 'YES'and 'NO' will be used for parsing.

If not boolean list is specified with a boolean argument the behaviour depends on whether the parser is processing an input or an output string. With an input format string, the parser will try all the boolean lists included in APPBOOLEAN and will use the first value that matches the input characters. When parsing output strings the parser will use the first boolean list in APPBOOLEAN

If the APPBOOLEAN symbol is not defined for a particular instrument, the libdance library defaults to the following definition:

#define APPBOOLEAN  \
   BOOLDEF(1,    0)    \
   BOOLDEF(YES,  NO)   \
   BOOLDEF(Y,    N)    \
   BOOLDEF(On,   Off)  \
   BOOLDEF(True, False)\
   BOOLDEF(T,    F)    \


I : Integer:
------------
Arguments declared as integers accept the numeric 'u' (unsigned) and 'p' (positive) prefixes as well as <max> or <min>:<max> suffix modifiers. Possible integer formats are therefore "I", "I<max>", or "I<min>:<max>".
The difference between unsigned and positive integers resides in that while unsigned integers are strictly non-negative, positive integers must be also be greater than zero. In other words, zero is not a valid positive integer.
The <min> and <max> modifiers are positive integers that determine the minimum and maximum values of the argument. If <min> is not specified the range is considered to extend from -<max> to +<max>. If the numeric prefixes 'u' or 'p' are used in combination with range modifiers, the most restrictive applies.
 For instance the format "I80" corresponds to integers between -80 and +80 while the format "pI10" limits the acceptable of the argument to the range 1 to 10.


H : Hexa:
---------
"H", "H<nbits>"

Hexa not working.
But hexa recognized with INTEGER flag: "0x1234" is decoded as an hexa under the INTEGER flag

F : Float:
----------
Float argument descriptors accept the same modifiers and with the same meaning than integers. In addition to the numerical prefixes, the descriptors may include <max> or <min>:<max> suffix modifiers. Possible Float formats are therefore "F", "F<max>" or "F<min>:<max>" combined with the 'u' and 'p' prefixes.
The <min> and <max> modifiers can be floating point values and determine the minimum and maximum values of the argument. If <min> is not specified the valid range is considered to extend from -<max> to +<max>. If the numeric prefixes 'u' or 'p' are used in combination with range modifiers, the most restrictive applies.
For instance the format "F2.5" corresponds to numbers between -2.5 and +2.5.

U : Unit:
---------
"U#<unit>
Arguments declared as unit are converted to numeric value with a scale factor depending on the argin string given. The numeric value can be float or integer and must have been specified before the unit in the argument descriptor.
For instance with the descriptor "pF_U#POWER" the following argin string "1MW" will be parsed to 1e-3 resulting float value.

Some tips:
   - Between the type and the unit descriptors, a separator must be placed (see chapter 5).
   - In case of several units declared, it's better to bind the type and the unit in a composite argument, for instance "o(pF_U#POWER) o(pF_U#WEIGHT)"

There are some predefined units:

    Unit     Range
  ---------  ---------------------------------
   "TIME"    from "ps" to "YEAR"
   "FREQ"    from "Hz" to "THz"
   "DIST"    from "pm" to "km"
   "VOLTS"   from "pV" to "MV"
   "CURR"    from "pA" to "MA"
   "CAPA"    from "pF" to "F"


T : Tag:
--------
"T#<tag>"

T#FUU defines the tag 'FUU"
T## defines the tag '#'
T#: is not valid -> the tag ':' can not be used (since it is already used as unit separator ?)
T#. defines the tag '.'


G : Flaglist:
-------------
"G#<list>"


C : Choice:
-----------
"C#<list>"


L : Label:
----------
"L", "L<size>"


S : String:
-----------
"S", "S<size>"

/!\ size if the maximum size ; a smaller string will still be accepted


9. Command execution


10. Retrieving input arguments

There is a set of macro available for retrieving input argument :
   - The macros I_<type>() , find the first item of that type from the current position.
   - The macros I_THIS_<type>() , find the first item but keeps pointing to it.
   - The macros I_FIRST_<type>() , find the first item but from the beginning of the stack.
   - The macros I_NAMED_<type>(nm) , finds the first item of that type that has the name nm.
   - The macros I_ID_<type>(id) , finds the first item of that type that has the identifier id.



<type> depends on the argument need to retrieve, below a table of possible <type> matching with the argument:
Warning : macros are case sensitive.

    <type>      Code argument
    ----------  -------------
    BOOL          B
    INTEGER       I
    HEXA          H
    FLOAT         F
    UNIT          U
    TAG           T
    FLGLIST       G
    CHOICE        C
    LABEL         L
    STRING        S
    BLOCK         ()
    BLOCK         {}

B : Boolean:
------------
...

I : Integer:
------------
...

F : Float:
----------
...

U : Unit:
---------
...

Example: with argument descriptor "pF_U#FREQ",  the numeric argument can be
retrieved in variable with the code  "float var = I_FLOAT() * I_UNIT()"
For instance, the argin string "1.2KHZ" or "1.2 KHZ" will be parsed to resulting "1200.0"
float value.

T : Tag:
--------
...

G : Flaglist:
-------------
...

C : Choice:
-----------
...

L : Label:
----------
...

S : String:
-----------
...


11. Errors


12. Command output


13. Further information


14. Examples

- formated IP address:
( uI255-T#.-uI255-T#.-uI255-T#.-uI255 )




EXTRA - to be sorted

# to document DAnCE commands...

puts the following macros within the exec_ function (see Maestrio for more information)
HELP_SYNTAX()
HELP_EXAMPLE()
HELP_DESCRIPTION()

The defined additional help sections are printed when calling ?HELP [CMDNAME]



# getting errors that occured within DAnCE

?DERR
>> prints errors and clears the error memory

?DERR NOCLEAR
>> prints errors but doesn't clean the error memory