/* howto use it memory tracing */

/* 0. add the following flags to your instrument Makefile */
/* C_FILES += mtrace.c */
/* L_FLAGS += -Wl,--wrap,malloc */
/* L_FLAGS += -Wl,--wrap,free */
/* L_FLAGS += -Wl,--wrap,realloc */
/* L_FLAGS += -Wl,--wrap,calloc */

/* 1. make an unstripped binary */
/* $> make dontstrip */

/* 2. run the binary and redirect the output */
/* $> ./bin_name > /tmp/mtrace.o */

/* 3. use mtrace.py to check allocations */
/* $> mtrace.py /tmp/mtrace.o bin_name */


#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <sys/types.h>

void* __real_malloc(size_t);
void* __real_calloc(size_t, size_t);
void* __real_realloc(void*, size_t);
void __real_free(void*);

static size_t g_counter = 0;
static unsigned int g_id = 0;

typedef struct meta
{
  unsigned int id;
  size_t size;
  double pad;
  uint8_t data[0];
} meta_t;

static meta_t* to_meta(void* p)
{
  return (void*)((uint8_t*)p - offsetof(meta_t, data));
}

static uint32_t* to_canary(meta_t* m)
{
  return (uint32_t*)(m->data + m->size);
}

static int check_canary(meta_t* m)
{
#define CANARY_VAL 0xdeadbeef
#define CANARY_SIZE sizeof(uint32_t)
  if (*to_canary(m) != CANARY_VAL) return -1;
  return 0;
}

static void put_canary(meta_t* m)
{
  *to_canary(m) = CANARY_VAL;
}

void* __wrap_malloc(size_t size)
{
  meta_t* m;
  void* const r0 = __builtin_return_address(0);

  printf("%20s ", __FUNCTION__);

  m = __real_malloc(sizeof(meta_t) + size + CANARY_SIZE);
  if (m == NULL) return NULL;

  m->id = g_id++;
  m->size = size;
  put_canary(m);
  g_counter += size;

  printf("%08x %08x %p\n", m->id, (unsigned int)g_counter, r0);

  return (void*)m->data;
}

void __wrap_free(void* p)
{
  meta_t* m;

  if (p == NULL) return ;

  m = to_meta(p);

  printf("%20s %08x ", __FUNCTION__, m->id);

  g_counter -= m->size;

  if (check_canary(m)) printf("invalid_canary ");

  __real_free(m);

  printf("%08x\n", (unsigned int)g_counter);
}

void* __wrap_realloc(void* p, size_t size)
{
  meta_t* m;
  unsigned int id;
  void* const r0 = __builtin_return_address(0);

  if (size == 0)
  {
    if (p != NULL) __wrap_free(p);
    return NULL;
  }

  printf("%20s ", __FUNCTION__);

  if (p != NULL)
  {
    m = to_meta(p);
    g_counter -= m->size;
    id = m->id;
  }
  else
  {
    m = NULL;
    id = g_id++;
  }

  m = __real_realloc((void*)m, sizeof(meta_t) + size + CANARY_SIZE);
  if (m == NULL) return NULL;

  m->id = id;
  m->size = size;
  put_canary(m);
  g_counter += size;

  printf("%08x %08x %p\n", m->id, (unsigned int)g_counter, r0);

  return (void*)m->data;
}

void* __wrap_calloc(size_t nmemb, size_t size)
{
  meta_t* m;
  void* const r0 = __builtin_return_address(0);

  printf("%20s ", __FUNCTION__);

  m = __real_malloc(sizeof(meta_t) + nmemb * size + CANARY_SIZE);
  if (m == NULL) return NULL;

  m->id = g_id++;
  m->size = size;
  put_canary(m);
  g_counter += size;

  printf("%08x %08x %p\n", m->id, (unsigned int)g_counter, r0);

  return (void*)m->data;
}
