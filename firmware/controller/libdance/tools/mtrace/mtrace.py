#!/usr/bin/env python


# post process memory allocation trace
# av[1]: the trace file
# av[2]: the binary name for symbol names, optional


def sym_init(bin_name):
    import subprocess
    import tempfile
    import os
    import re

    try: f = tempfile.NamedTemporaryFile(delete = False)
    except: f = None
    if f == None: return None

    subprocess.call(['objdump', '-t', bin_name], stdout = f)

    sym_re = re.compile('^(\w+)\s+\w\s+\w\s+\.\w+\s+(\w+)\s+(\w+)')

    f.seek(0, 0)
    syms = []
    while True:
        l = f.readline()
        if len(l) == 0: break
        m = sym_re.match(l.rstrip())
        if m == None: continue
        if len(m.groups()) != 3: continue
        addr = int(m.groups()[0], 16)
        size = int(m.groups()[1], 16)
        name = m.groups()[2]
        syms.append((name, addr, size))

    os.remove(f.name)
    f.close()

    return syms


def sym_find_name(syms, addr):
    if syms != None:
        for s in syms:
            if (addr >= s[1]) and (addr <= (s[1] + s[2])):
                return s[0]
    return str(addr)


def sym_list(syms):
    for s in syms: print(s[0])
    return


def mtrace_open(mtrace_name, syms):
    import re

    try: f = open(mtrace_name, 'r')
    except: return None

    mtrace_re = re.compile('^\s+__wrap_(\w+)\s+(\w+)\s+(\w+)(\s+..(\w+))?')
    alloc_re = re.compile('^(m|re|c)alloc')
    free_re = re.compile('^free')

    mtrace = []

    while True:
        l = f.readline()
        if len(l) == 0: break
        m = mtrace_re.match(l.rstrip())
        if m == None: continue
        if len(m.groups()) < 3: continue

        name = m.groups()[0]
        _id = int(m.groups()[1], 16)
        counter = int(m.groups()[2], 16)

        if alloc_re.match(name):
            addr = int(m.groups()[3], 16)
            name = sym_find_name(syms, addr)
            mtrace.append([_id, name, 0])
        elif free_re.match(name):
            for m in mtrace:
                if m[0] != _id: continue
                # increment the free counter
                m[2] = m[2] + 1
        else:
            continue

    f.close()

    return mtrace


def mtrace_print(mtrace):
    for m in mtrace:
        print(str(m[0]) + ' ' + m[1] + ' ' + str(m[2]))
    return


def main(av):
    if len(av) == 3: syms = sym_init(av[2])
    else: syms = None

    if len(av) < 2: return -1
    mtrace = mtrace_open(av[1], syms)
    if mtrace == None: return -1

    mtrace_print(mtrace)

    return

        
import sys
main(sys.argv)
