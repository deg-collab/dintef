# usage:
# $ viva dblock_dmak_top

# configuration memory file
write_cfgmem -format mcs -interface spix4 -size 32 -loadbit "up 0 dblock_dmak_top.bit" -file dblock_dmak_top.mcs -force
