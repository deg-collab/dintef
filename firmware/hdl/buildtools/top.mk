# this makefile is the only one included by the user

# obtain the directory containing this file
BUILDTOOLS_ROOT := $(dir $(lastword $(MAKEFILE_LIST)))

# set default variables
DANCE_SDK_PLATFORM ?= $(platform)

# platform related. export to make it accessible from sub makefiles.
export DANCE_SDK_PLATFORM
include $(BUILDTOOLS_ROOT)/platform/platform.mk

DANCE_SDK_CFLAGS  +=
#DANCE_SDK_LDFLAGS += -L$(DANCE_SDK_DEPS_DIR)/libs
DANCE_SDK_LDFLAGS += -L$(DANCE_SDK_DEPS_DIR)/lib/dance
DANCE_SDK_LDFLAGS += -L$(DANCE_SDK_DEPS_DIR)/lib

CFLAGS_DANCE_DEFAULT  += -Wall -O2 -fPIC -pthread
