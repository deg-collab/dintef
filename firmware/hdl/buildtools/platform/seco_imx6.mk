#---------------------------------------------------------------------
#
DANCE_SDK_PLATFORM ?= seco_imx6

#---------------------------------------------------------------------
#
DANCE_SDK_ARCH           ?= arm_imx6
DANCE_SDK_KERNEL_RELEASE ?= linux-3.0.35
DANCE_SDK_KERNEL_DIR     ?= $(DANCE_SDK_ROOT)/kernel/$(DANCE_SDK_PLATFORM)/$(DANCE_SDK_KERNEL_RELEASE)
DANCE_SDK_CROSS_COMPILE  ?= $(DANCE_SDK_ROOT)/toolchain/arm-buildroot-linux-uclibcgnueabi/bin/arm-buildroot-linux-uclibcgnueabi-
DANCE_SDK_DEPS_DIR       ?= $(DANCE_SDK_ROOT)/deps/arm-buildroot-linux-uclibcgnueabi

DANCE_SDK_CFLAGS  += -DCONFIG_FREESCALE_IMX6=1
DANCE_SDK_LDFLAGS +=
