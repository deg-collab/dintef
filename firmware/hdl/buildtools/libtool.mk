# To be included in makefiles processing libdance libraries or executable tools
#
# The container makefile must set either the variable LIB_NAME or TOOL_NAME
#   to the name of the library or the executable respectively.
#

# obtain the directory containing this file
BUILDTOOLS_ROOT := $(dir $(lastword $(MAKEFILE_LIST)))

include ${BUILDTOOLS_ROOT}/top.mk
include ${BUILDTOOLS_ROOT}/dancetools.mk

ifneq ($(origin LIB_NAME), undefined)
# $(info ************ processing library $(LIB_NAME) ************)
undefine TOOL_NAME
SO_FILE := $(LIB_NAME).so
A_FILE  := $(LIB_NAME).a
MAIN_TARGETS := $(A_FILE) $(SO_FILE)
TARGET_NAME := library $(LIB_NAME)

else ifneq ($(origin TOOL_NAME), undefined)
# $(info ************ processing tool $TOOL_NAME ************)
undefine LIB_NAME
MAIN_TARGETS := $(TOOL_NAME)
TARGET_NAME := tool $(TOOL_NAME)

else
$(error ERROR - none of LIB_NAME and TOOL_NAME are defined)

endif

C_FILES ?= $(wildcard *.c)
H_FILES ?= $(wildcard *.h)

O_FILES := $(C_FILES:.c=.o)


BUILDING_DEBUG := $(findstring gdb,$(MAKECMDGOALS))


# flags
CFLAGS ?= $(CFLAGS_DANCE_DEFAULT)
CFLAGS := $(DANCE_SDK_CFLAGS) $(CFLAGS)
ifdef BUILDING_DEBUG
CFLAGS += -g -O0 -ggdb
endif
CFLAGS += $(foreach dir,$(HEADER_DIRS),-I$(dir))
ifdef WITHIN_LIBDANCE
CFLAGS += -DWITHIN_LIBDANCE
endif

LDFLAGS ?= $(LDFLAGS_EXTRA)
LDFLAGS += -g
LDFLAGS += $(foreach dir,$(LIBRARY_DIRS),-L$(dir)) $(DANCE_SDK_LDFLAGS)


#---------------------------------------------------------------

# print common_usage
#
define common_usage
	@echo "Usage: "
	@echo "    make build    : generate $(TARGET_NAME)"
	@echo "    make buildall : equivalent to  cleanall + build"
	@echo "    make buildgdb : equivalent to buildall with debug option"
	@echo "    make clean    : clean all locally generated files"
	@echo "    make cleanall : clean all both local files and dependencies"
endef

# process dependencies, $(1) contains the rule to apply
# called as: $(call process_dependencies,<rule>)
#
define process_dependencies
   @for DEPDIR in $(DEPENDENCIES_DIRS); do pwd; \
      echo "cd $$DEPDIR && $(MAKE) platform=$(DANCE_SDK_PLATFORM) $(1)" ; \
      (cd $$DEPDIR && $(MAKE) platform=$(DANCE_SDK_PLATFORM) $(1)) ; \
   done
endef

#---------------------------------------------------------------

.PHONY: usage lib build buildall clean cleanall deps_build deps_cleanall

usage:
	$(call common_usage,)
	@echo

common_usage:

deps_build:
	$(call process_dependencies,$(BUILD_RULE))

deps_cleanall:
	$(call process_dependencies,cleanall)

lib: build

build: override BUILD_RULE := build
build: deps_build $(MAIN_TARGETS)

buildgdb: override BUILD_RULE := buildgdb
buildgdb: deps_build $(MAIN_TARGETS)

buildall: cleanall build

clean :
	rm -f $(O_FILES)
	rm -f $(MAIN_TARGETS)

cleanall: deps_cleanall clean

ifdef LIB_NAME

lib: build

$(A_FILE): $(O_FILES)
	$(DANCE_SDK_CROSS_COMPILE)ar rv $@ $^

$(SO_FILE): $(O_FILES)
	$(DANCE_SDK_CROSS_COMPILE)gcc -shared $(LDFLAGS) $^ -o $@

else ifdef TOOL_NAME

tool: build

$(TOOL_NAME): ident $(O_FILES)
	$(DANCE_SDK_CROSS_COMPILE)gcc -static $(LDFLAGS) $(O_FILES) $(LDLIBS) -o $@
	$(delete_idfile)
ifndef BUILDING_DEBUG
	$(DANCE_SDK_CROSS_COMPILE)strip $@
endif

ident:
	@$(call create_idfile,TOOL,$(TOOL_NAME))
	@$(call add_rcskeyword,Platform,$(DANCE_SDK_PLATFORM))
	@$(call add_rcskeyword,Tools,`$(CC) --version | head -n1`)
	@$(show_lastrcskwfile)

endif


%.o: %.c $(H_FILES)
	$(DANCE_SDK_CROSS_COMPILE)gcc $(CFLAGS) -c -o $@ $<
