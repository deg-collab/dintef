#!/usr/bin/env bash

BUILD_DIR=build
[ ! -d $BUILD_DIR ] && mkdir $BUILD_DIR

DONE_FILE=$BUILD_DIR/___oar_is_done
DOT_SH=$BUILD_DIR/___oar_cmd.sh

[ -e $DONE_FILE ] && rm $DONE_FILE

cat ./build_local.sh > $DOT_SH
echo "touch $DONE_FILE" >> $DOT_SH
chmod 755 $DOT_SH

source profile_cae
xoar -s $DOT_SH

xterm -title xoar \
    -sb -sl 10000 \
    -e /bin/sh \
    -c "tail -n +1 -f $BUILD_DIR/xoar_stdout" &
until [ -f $DONE_FILE ]; do sleep 5; done
