-------------------------------------------------------------
--
-- File        : dblock_dmak_dcvr.vhd
-- Description : Digitally Controlled Variable Resistor
--               for VADJ and VAPP voltages control.
--
-------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.ebs_pkg.all;
use work.dblock_dmak_pkg.all;

entity dblock_dmak_dcvr is

port (
-- System clock and reset
  eb_sys_rst : in std_logic;
  eb_sys_clk : in std_logic;

-- Registers and selection
  VADJ_ON    : in std_logic;
  VADJ_DAT   : in std_logic_vector(9 downto 0);
  VAPP_ON    : in std_logic;
  VAPP_DAT   : in std_logic_vector(9 downto 0);

-- DCVR control
  DCVR_CS    : out std_logic;
  DCVR_CLK   : out std_logic;
  DCVR_SDI   : out std_logic;
  VADJ_TUNE  : out std_logic;
  VAPP_TUNE  : out std_logic;

-- Power Good control
  PG_VADJ    : in std_logic;
  PG_VAPP    : in std_logic;
  PG_C2M     : out std_logic;
  DB_PG_VAPP : out std_logic
);
end dblock_dmak_dcvr;

------------------------------------------------------------------------
architecture dblock_dmak_dcvr_rtl of dblock_dmak_dcvr is
------------------------------------------------------------------------

type dcvr_type       is (DCVR_INIT, DCVR_DAT, DCVR_CLKR, DCVR_CLKH, DCVR_CLKF, DCVR_END);
signal dcvr_state    : dcvr_type;
signal dcvr_counter  : std_logic_vector(3 downto 0);
signal VADJ_TUNE_int : std_logic;
signal VAPP_TUNE_int : std_logic;
signal dcvr_last_dat : std_logic;

-----
begin
-----

VADJ_TUNE <= VADJ_TUNE_int;
VAPP_TUNE <= VAPP_TUNE_int;

process
begin

  wait until rising_edge(eb_sys_clk);

    if (eb_sys_rst = '1') then
      DCVR_CS       <= '1';
      DCVR_CLK      <= '0';
      DCVR_SDI      <= '0';
      VADJ_TUNE_int <= '1';
      VAPP_TUNE_int <= '1';
      dcvr_last_dat <= '0';
      dcvr_counter  <= "1001";
      dcvr_state    <= DCVR_INIT;
    else
      case (dcvr_state) is

        when DCVR_INIT =>

          if (VADJ_ON = '1') then
            DCVR_CS       <= '0';
            VADJ_TUNE_int <= '0';
            dcvr_state    <= DCVR_DAT;
          elsif (VAPP_ON = '1') then
            DCVR_CS       <= '0';
            VAPP_TUNE_int <= '0';
            dcvr_state    <= DCVR_DAT;
          else
            dcvr_state <= DCVR_INIT;
          end if;

        when DCVR_DAT =>

          if (VADJ_TUNE_int = '0') then
            DCVR_SDI <= VADJ_DAT(to_integer(unsigned(dcvr_counter)));
          end if;
          if (VAPP_TUNE_int = '0') then
            DCVR_SDI <= VAPP_DAT(to_integer(unsigned(dcvr_counter)));
          end if;
          dcvr_counter <= std_logic_vector(unsigned(dcvr_counter) - 1);
          dcvr_state   <= DCVR_CLKR;

        when DCVR_CLKR =>

          DCVR_CLK   <= '1';
          dcvr_state <= DCVR_CLKH;

        when DCVR_CLKH =>

          dcvr_state <= DCVR_CLKF;

        when DCVR_CLKF =>

          DCVR_CLK <= '0';
          if (dcvr_last_dat = '1') then
            dcvr_last_dat <= '0';
            dcvr_counter  <= "1001";
            dcvr_state    <= DCVR_END;
          elsif (to_integer(unsigned(dcvr_counter)) = 0) then
            dcvr_last_dat <= '1';
            dcvr_state    <= DCVR_DAT;
          else
            dcvr_state <= DCVR_DAT;
          end if;

        when DCVR_END =>

          if (VADJ_TUNE_int = '0') then
            VADJ_TUNE_int <= '1';
          end if;
          if (VAPP_TUNE_int = '0') then
            VAPP_TUNE_int <= '1';
          end if;
          DCVR_CS    <= '1';
          dcvr_state <= DCVR_INIT;

        when others =>

          DCVR_CS       <= '1';
          DCVR_CLK      <= '0';
          DCVR_SDI      <= '0';
          VADJ_TUNE_int <= '1';
          VAPP_TUNE_int <= '1';
          dcvr_last_dat <= '0';
          dcvr_counter  <= "1001";
          dcvr_state    <= DCVR_INIT;

      end case;
    end if;

end process;

process
begin

  wait until rising_edge(eb_sys_clk);

    if (eb_sys_rst = '1') then
      PG_C2M     <= '0';
      DB_PG_VAPP <= '0';
    else
      PG_C2M     <= PG_VADJ;
      DB_PG_VAPP <= PG_VAPP;
    end if;

end process;

end dblock_dmak_dcvr_rtl; 
