-----------------------------------------------------
--
-- DBLOCK/DMAK general purpose frame.
-- Top file for full implementation.
--
-----------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.ebm0_pcie_b_pkg.all;
use work.ep_comp_pkg.all;
use work.eb_common_pkg.all;
use work.ebs_pkg.all;
use work.ebs_fspi_pkg.all;
use work.ebs_sysm_pkg.all;
use work.ebs_regs_pkg.all;
use work.ebs_fifo_pkg.all;
use work.ebm_ebft_pkg.all;
use work.dblock_dmak_pkg.all;

use work.sus_top_pkg.all;

--use work.readout.all;
--use work.readout_8b10b.all;

library unisim;
use unisim.vcomponents.IBUFGDS;
use unisim.vcomponents.IBUFDS_GTE2;

entity dblock_dmak_top is

generic (
-- ======================================================================
-- PCB/FPGA target parameters
-- ======================================================================
  XC7K_TYP             : STRING := "XC7K160T";                         -- Kintex-7 FPGA Target "XC7K160T" or "XC7K325T"
  CTRL_PCB             : STRING := "B";                                -- DBLOCK/CONTROL Printed Circuit Board A or B
  BASE_PCB             : STRING := "B";                                -- DMAK/BASE Printed Circuit Board A or B

-- ======================================================================
-- E-Bone and PCIe interface parameters
-- ======================================================================
  EB_CLK               : REAL := 8.0;                                  -- E-Bone clock period in ns
  EBFT_DWIDTH          : NATURAL := 64;                                -- FT (DMA) data width
  EBS_DWIDTH           : NATURAL := 32;                                -- Memory (E-bone) data width
  EBS_IN_AUX0          : NATURAL := 1;                                 -- 1st Auxiliary Analog Input number, System Monitor implementation
  EBS_IN_AUX1          : NATURAL := 2;                                 -- 2nd Auxiliary Analog Input number, System Monitor implementation
  BADR0_MEM            : INTEGER := 1;                                 -- 2Kbyte BADR0 Block Ram Memory implementation when 1
  WATCH_DOG            : INTEGER := 1;                                 -- Watch dog process implementation when 1
  EBS_AD_RNGE          : NATURAL := 16;                                -- Number of address bits to decode. Must be the same for all slaves mapped in the same segment
  TYPE_DESC            : NATURAL := 16#72000004#;                      -- Type description application and/or target dependant
  HARD_VERS            : NATURAL := 16#00000100#;                      -- Hardware version implemented, 1.0.0
  DEVICE_ID            : BIT_VECTOR := X"EB01";                        -- Device ID = E-Bone 01
  BC_PERIOD            : BIT_VECTOR := X"2";                           -- Broad-Call period (0:1us, 1:10us, 2:100us, 3:1ms)
  LINK_SPEED           : INTEGER := 1;                                 -- PCI-Express EndPoint Link Speed (1=2.5G, 2=5G)
  LINK_WIDTH           : INTEGER := 1;                                 -- PCI-Express EndPoint Lane numbers (1,4,8)
  SIM_FILE             : STRING := "sim/stimuli.txt";                  -- Simulation script
  STAT_COUNTING        : INTEGER := 1;                                 -- ep_stat module implementation when 1
  EBM_PCIE_PRB         : STRING := "OFF";                               -- ebm0_pcie_b dedicated probes unused when OFF

-- ======================================================================
-- DDR3 SDRAM Controlers parameters
-- ======================================================================
  C_S_AXI_ID_WIDTH     : INTEGER := 2;                                 -- Width of all master and slave ID signals, >= 1
  C_S_AXI_ADDR_WIDTH   : INTEGER := 28;                                -- Width of S_AXI_AWADDR, S_AXI_ARADDR, M_AXI_AWADDR and M_AXI_ARADDR for all SI/MI slots
  C_S_AXI_DATA_WIDTH   : INTEGER := 64;                                -- Width of WDATA and RDATA on SI slot, Must be <= APP_DATA_WIDTH
  USE_CS_PORT          : INTEGER := 1;                                 -- 1 when Chip Select (CS#) output is enabled, 0 when Chip Select (CS#) output is disabled
  RTT_NOM              : STRING := "60";                               -- RTT_NOM (ODT), "120" - RZQ/2, "60" - RZQ/4, "40" - RZQ/6
  SYSCLK_TYPE          : STRING := "DIFFERENTIAL";                     -- System clock type DIFFERENTIAL, SINGLE_ENDED, NO_BUFFER
  REFCLK_TYPE          : STRING := "USE_SYSTEM_CLOCK";                 -- Reference clock type DIFFERENTIAL, SINGLE_ENDED, NO_BUFFER, USE_SYSTEM_CLOCK
  UI_EXTRA_CLOCKS      : STRING := "FALSE";                            -- Generates extra clocks as 1/2, 1/4 and 1/8 of fabrick clock, based on GUI selection
  RST_ACT_LOW          : INTEGER := 1;                                 -- 1 for active low reset, 0 for active high reset
  DEBUG_PORT           : STRING := "OFF"                               -- "ON" Enable debug signals/controls, "OFF" Disable debug signals/controls
);

port (
-- ======================================================================
-- Memory Controller: DDR3_SDRAM
-- ======================================================================
-- synthesis translate_off
  ddr3_dq              : inout std_logic_vector(7 downto 0);           -- Bidirectionnal data bus
  ddr3_dqs_p           : inout std_logic_vector(0 downto 0);           -- Differential data strobe P part, edge-aligned output (read), centre-aligned input (write)
  ddr3_dqs_n           : inout std_logic_vector(0 downto 0);           -- Differential data strobe N part

  ddr3_addr            : out std_logic_vector(14 downto 0);            -- Address outputs to the DDR3/SDRAM, provide the row and column address
  ddr3_ba              : out std_logic_vector(2 downto 0);             -- Bank address outputs to the DDR3/SDRAM, define the bank to which the command is being applied
  ddr3_ras_n           : out std_logic;                                -- Row access output to the DDR3/SDRAM, along with WE and CS
  ddr3_cas_n           : out std_logic;                                -- Column access output to the DDR3/SDRAM, along with WE and CS
  ddr3_we_n            : out std_logic;                                -- Write enable (WE) output to the DDR3/SDRAM
  ddr3_reset_n         : out std_logic;                                -- Asynchronous reset output to the DDR3/SDRAM
  ddr3_ck_p            : out std_logic_vector(0 downto 0);             -- Differential clock P part, operations on positive and negative edges
  ddr3_ck_n            : out std_logic_vector(0 downto 0);             -- Differential clock N part
  ddr3_cke             : out std_logic_vector(0 downto 0);             -- Clock enable output to the DDR3/SDRAM
  ddr3_cs_n            : out std_logic_vector(0 downto 0);             -- Chip select active low output to the DDR3/SDRAM
  ddr3_dm              : out std_logic_vector(0 downto 0);             -- Data mask output to the DDR3/SDRAM
  ddr3_odt             : out std_logic_vector(0 downto 0);             -- On-die termination enabled (high) or disabled (low) applied to datas and data strobe

  ddr3_sys_clk_p       : in std_logic;                                 -- External differential clock input part P
  ddr3_sys_clk_n       : in std_logic;                                 -- External differential clock input part N
-- synthesis translate_on

-- ======================================================================
-- QSEVEN Connector I/O Pinout, PCIe 4 x Lanes
-- ======================================================================
  pcie_sys_rst_n       : in std_logic;                                 -- Asynchronous active low PCIe module reset input signal (from Q7 PCIe slot)
  pcie_sys_clk_p       : in std_logic;                                 -- 100MHz positive differential input clock signal (from Q7 PCIe slot)
  pcie_sys_clk_n       : in std_logic;                                 -- 100MHz negative differential input clock signal (from Q7 PCIe slot)
  pcie_wke_n           : out std_logic;                                -- PCIe Wake Event output, must be set to 1

  pci_exp_rxn          : in std_logic_vector(LINK_WIDTH - 1 downto 0); -- PCIe receive negative serial differential input signal (from Q7 PCIe slot)
  pci_exp_rxp          : in std_logic_vector(LINK_WIDTH - 1 downto 0); -- PCIe receive positive serial differential input signal (from Q7 PCIe slot)
  pci_exp_txn          : out std_logic_vector(LINK_WIDTH - 1 downto 0);-- PCIe transmit negative serial differential input signal (from Q7 PCIe slot)
  pci_exp_txp          : out std_logic_vector(LINK_WIDTH - 1 downto 0);-- PCIe transmit positive serial differential input signal (from Q7 PCIe slot)

-- ======================================================================
-- QSEVEN Connector I/O Pinout, Power Management and Misc. signals
-- ======================================================================
  QSV_RST_N            : out std_logic;                                -- Active low asynchronous Qseven module reset
  WD_TRIG_N            : out std_logic;                                -- Watchdog trigger signal, restarts the watchdog timer of the Qseven module
  THRM_TRIP_N          : in std_logic;                                 -- Thermal Trip indicates an overheating condition of the Qseven processor
  THRM_N               : out std_logic;                                -- Thermal Alarm active low signal generated by the external hardware

-- ======================================================================
-- FPGA I2C master core signals
-- ======================================================================
  FPGA_I2C_EN          : out std_logic;                                -- I2C data line output enable, FPGA becomes I2C master when low
  FPGA_I2C_CLK         : inout std_logic;                              -- I2C clock line
  FPGA_I2C_DAT         : inout std_logic;                              -- I2C data line

-- ======================================================================
-- DBRIDGE Signals except FMC/LPC part
-- ======================================================================
-- synthesis translate_off
  DB_GT_REFCLK_P       : in std_logic;                                 -- GTX transceiver positive differential input 200MHz reference clock
  DB_GT_REFCLK_N       : in std_logic;                                 -- GTX transceiver negative differential input 200MHz reference clock
  DB_GT_RX_P           : in std_logic_vector(3 downto 1);              -- GTX transceiver positive differential receiver line inputs
  DB_GT_RX_N           : in std_logic_vector(3 downto 1);              -- GTX transceiver negative differential receiver line inputs
  DB_GT_TX_P           : out std_logic_vector(3 downto 1);             -- GTX transceiver positive differential transmitter line outputs
  DB_GT_TX_N           : out std_logic_vector(3 downto 1);             -- GTX transceiver negative differential transmitter line outputs
-- synthesis translate_on

  DB_IO_MRCC_2V5_P     : in std_logic_vector(1 downto 0);              -- GP bidirectionnal differential positive 2.5V I/O standards signals, Multi-Region Clock Capable when input
  DB_IO_MRCC_2V5_N     : in std_logic_vector(1 downto 0);              -- GP bidirectionnal differential negative 2.5V I/O standards signals, Multi-Region Clock Capable when input
  DB_IO_MRCC_VAPP_P    : in std_logic_vector(1 downto 0);              -- GP bidirectionnal differential positive VAPP voltage I/O signals, Multi-Region Clock Capable when input
  DB_IO_MRCC_VAPP_N    : in std_logic_vector(1 downto 0);              -- GP bidirectionnal differential negative VAPP voltage I/O signals, Multi-Region Clock Capable when input
  DB_IO_SRCC_2V5_P     : inout std_logic_vector(3 downto 0);           -- GP bidirectionnal differential positive 2.5V I/O standards signals, Single-Region Clock Capable when input
  DB_IO_SRCC_2V5_N     : inout std_logic_vector(3 downto 0);           -- GP bidirectionnal differential negative 2.5V I/O standards signals, Single-Region Clock Capable when input
  DB_IO_SRCC_VAPP_P    : inout std_logic_vector(1 downto 0);           -- GP bidirectionnal differential positive VAPP voltage I/O signals, Single-Region Clock Capable when input
  DB_IO_SRCC_VAPP_N    : inout std_logic_vector(1 downto 0);           -- GP bidirectionnal differential negative VAPP voltage I/O signals, Single-Region Clock Capable when input

  DB_IO_2V5_P          : inout std_logic_vector(22 downto 0);          -- GP bidirectionnal differential positive 2.5V I/O standards signals
  DB_IO_2V5_N          : inout std_logic_vector(22 downto 0);          -- GP bidirectionnal differential negative 2.5V I/O standards signals
  DB_IO_VAPP_P         : inout std_logic_vector(19 downto 0);          -- GP bidirectionnal differential positive VAPP voltage I/O signals
  DB_IO_VAPP_N         : inout std_logic_vector(19 downto 0);          -- GP bidirectionnal differential negative VAPP voltage I/O signals
  DB_IO_2V5            : out std_logic_vector(9 downto 0);             -- GP bidirectionnal single-ended 2.5V I/O standards signals
  DB_IO_VAPP           : inout std_logic_vector(0 downto 0);           -- GP bidirectionnal single-ended VAPP voltage I/O signal

  TRST_L               : inout std_logic;                              -- JTAG active low reset signal

-- ======================================================================
-- DBRIDGE Signals FMC/LPC_MEZZANINE part
-- ======================================================================
  GBTCLK0_M2C_P        : in std_logic;                                 -- GTX transceiver positive differential input reference clock
  GBTCLK0_M2C_N        : in std_logic;                                 -- GTX transceiver negative differential input reference clock
  DP_M2C_P             : in std_logic_vector(0 downto 0);              -- GTX transceiver positive differential receiver line input
  DP_M2C_N             : in std_logic_vector(0 downto 0);              -- GTX transceiver negative differential receiver line input
-- synthesis translate_off
  DP_C2M_P             : out std_logic_vector(0 downto 0);             -- GTX transceiver positive differential transmitter line output
  DP_C2M_N             : out std_logic_vector(0 downto 0);             -- GTX transceiver negative differential transmitter line output
-- synthesis translate_on

  CLK_M2C_P            : in std_logic_vector(1 downto 0);              -- Differential positive clock signals, driven from the Mezzanine Module to the carrier card
  CLK_M2C_N            : in std_logic_vector(1 downto 0);              -- Differential negative clock signals, driven from the Mezzanine Module to the carrier card

  LA2_P                : in std_logic; --monitor output
  LA2_N                : in std_logic;
  LA4_P                : in std_logic; --ASIC first serial output
  LA4_N                : in std_logic;
  LA15_P                : out std_logic; --jtag tdo sent to ASIC
  LA15_N                : out std_logic;  
  LA31_P               : in std_logic; --PLL c2b
  LA31_N               : in std_logic;   
  LA33_P               : in std_logic; --PLL lol
  LA33_N               : in std_logic; 
  
  LA5_P                : out std_logic; --Run
  LA5_N                : out std_logic;
  LA9_P                : out std_logic; -- jtag res
  LA9_N                : out std_logic;
  LA11_P               : out std_logic; -- JTAG TMS
  LA11_N               : out std_logic;

  LA14_P               : out std_logic; -- LED
  LA14_N               : out std_logic;
  LA7_P                : out std_logic; -- T3 telegram command in 
  LA7_N                : out std_logic;

--  LA7_P               : in std_logic; -- ASIC second serial output 
--  LA7_N               : in std_logic;
  LA8_P               : in std_logic; -- JTAG TDI
  LA8_N               : in std_logic;

  LA19_P               : out std_logic; -- JTAG TCK
  LA19_N               : out std_logic;
  
  LA30_P			         : out std_logic; -- PLL Reset
  LA30_N			         : out std_logic;
  LA32_P			         : out std_logic; -- PLL input clock
  LA32_N			         : out std_logic;

  GA                   : in std_logic_vector(1 downto 0);              -- Geographical address of the mezzanine module used for I2C channel select
  PRSNT_M2C_L          : in std_logic;                                 -- Module present signal, mezzanine module present when low

-- ======================================================================
-- DBRIDGE Signals DLINK part (BASE A)
-- ======================================================================
  DB_DLINK_ACT         : out std_logic;                                -- Lines activity signal output
  DB_DLINK_TXEN        : out std_logic;                                -- Transmit enable output
  DB_DLINK_TX          : out std_logic_vector(DLINK_NB - 1 downto 0);  -- Transmit outputs lines
  DB_DLINK_RXEN        : out std_logic;                                -- Receive enable output
  DB_DLINK_RX          : in std_logic_vector(DLINK_NB - 1 downto 0);   -- Receive inputs lines

-- ======================================================================
-- DBRIDGE Signals power supplies control
-- ======================================================================
  PG_C2M               : out std_logic;                                -- Power Good Carrier to Mezzanine, asserted when power supplies are within tolerance
  DB_PG_VAPP           : out std_logic;                                -- Power Good Carrier to Application, asserted when power supply is within tolerance

-- ======================================================================
-- SPI Flash Memorie interface (FPGA Bitstream)
-- ======================================================================
  FLASH_CS             : out std_logic;                                -- SPI slave select (active Low)
  FLASH_WP             : out std_logic;                                -- Write Protect when low
  FLASH_HOLD           : out std_logic;                                -- Hold (pause) serial transfer when low
  FLASH_MOSI           : out std_logic;                                -- SPI master out slave in
  FLASH_MISO           : in std_logic;                                 -- SPI master in slave out

-- ======================================================================
-- PCIe BADR0 control signals
-- ======================================================================
  soft_wd_p            : out std_logic;                                -- Active high external watch dog reset signal, witdh=250ns, period=1s
  soft_reset_n         : out std_logic;                                -- Active low external reset on ctrl_regs(2) wr. operation (Device_ID & Vendor_ID), witdh=250ns
  status_led_p         : out std_logic_vector(1 downto 0);             -- Status led for software test: 00=off, 01=red, 10=green

-- ======================================================================
-- System Monitor analog inputs
-- ======================================================================
  VP_in                : in std_logic;                                 -- Positive input terminal of the dedicated differential analog input channel
  VN_in                : in std_logic;                                 -- Negative input terminal of the dedicated differential analog input channel
  VAUXP0               : in std_logic;                                 -- Positive input terminal of the differential auxiliary analog input channel 0
  VAUXN0               : in std_logic;                                 -- Negative input terminal of the differential auxiliary analog input channel 0
  VAUXP1               : in std_logic;                                 -- Positive input terminal of the differential auxiliary analog input channel 1
  VAUXN1               : in std_logic                                  -- Negative input terminal of the differential auxiliary analog input channel 1
);

end dblock_dmak_top;

------------------------------------------------------------------------
architecture dblock_dmak_top_rtl of dblock_dmak_top is
------------------------------------------------------------------------

------------------------------------------------------------------------
-- 1st E-bone, PCIe Master
------------------------------------------------------------------------

-- master #0 (slave #0 from transmitter)
signal eb_m0_clk       : std_logic;
signal eb_m0_rst       : std_logic;
signal eb_m0_brq       : std_logic;
signal eb_m0_bg        : std_logic;
signal eb_m0           : ebm32_typ;

-- Fast Transmitter
signal eb_ft_bg        : std_logic;
signal eb_ft_brq       : std_logic;
signal eb_ft_dxt       : std_logic_vector(EBFT_DWIDTH - 1 downto 0);
signal eb_ft           : ebft_typ;
signal eb_ft_dk        : std_logic;
signal eb_ft_err       : std_logic;

-- Fast Transmitter status
signal ebft_desc_stat0 : std_logic_vector(31 downto 0);
signal ebft_desc_stat1 : std_logic_vector(31 downto 0);
signal ebft_dma_stat   : std_logic_vector(31 downto 0);
signal fsc_stat        : std_logic_vector(31 downto 0);
signal fsc_irqs        : std_logic_vector(31 downto 0);
signal ebft_dma_count  : std_logic_vector(15 downto 0);
signal ebft_dma_eot    : std_logic;
signal ebft_dma_err    : std_logic;
signal ft_psize        : std_logic_vector(3 downto 0);






alias under_run_error  : std_logic is ebft_desc_stat0(29);
alias all_error_cases  : std_logic is ebft_desc_stat0(30);

-- master shared
signal eb_m            : ebs32_o_typ;

-- From Slave dedicated
signal ebs_o           : ebs32_oa_typ(EBS_SLV_NB downto 1);

-- To slave shared bus
signal ebs_i           : ebs32_i_typ;

-- Application extension signals
signal ext_ctrl_i      : ext_typ;

------------------------------------------------------------------------
-- 2nd E-bone, DACQ
------------------------------------------------------------------------

-- master #0 (slave #0 from transmitter)
signal eb2_m0_brq      : std_logic;
signal eb2_m0_bg       : std_logic;
signal eb2_m0_o        : ebm64_typ;

-- master shared
signal eb2_m_i         : ebs64_o_typ;

-- From Slave dedicated 
signal eb2_s_o         : ebs64_oa_typ(EBAS_SLV_NB downto 1);
signal ebx2_o          : ebx_oa_typ(EBAS_SLV_NB downto 1);

-- To slave shared bus
signal eb2_bmx         : std_logic;
signal eb2_s_i         : ebs64_i_typ;

signal ebx2_msg_set    : std_logic_vector(7 downto 0);
signal ebx2_msg_dat    : std_logic_vector(15 downto 0);
signal ebx2_m_dsz      : std_logic_vector(3 downto 0);

------------------------------------------------------------------------
-- USER
------------------------------------------------------------------------

signal regs_o_s3       : std32_a((REGS_NB / 2) - 1 downto 0);
signal regs_i_s3       : std32_a(REGS_NB - 1 downto (REGS_NB / 2));
signal regs_wak_s3     : std_logic_vector((REGS_NB / 2) - 1 downto 0);
signal regs_rak_s3     : std_logic_vector((REGS_NB / 2) - 1 downto 0);
signal regs_ofst_s3    : std_logic_vector(Nlog2(REGS_NB) - 1 downto 0);
signal regs_ofsrd_s3   : std_logic;
signal regs_ofswr_s3   : std_logic;

signal regs_o_s4       : std32_a((REGS_NB / 2) - 1 downto 0);
signal regs_i_s4       : std32_a(REGS_NB - 1 downto (REGS_NB / 2));
signal regs_wak_s4     : std_logic_vector((REGS_NB / 2) - 1 downto 0);
signal regs_rak_s4     : std_logic_vector((REGS_NB / 2) - 1 downto 0);
signal regs_ofst_s4    : std_logic_vector(Nlog2(REGS_NB) - 1 downto 0);
signal regs_ofsrd_s4   : std_logic;
signal regs_ofswr_s4   : std_logic;

signal soft_reset      : std_logic;
signal ep_link_up      : std_logic;
signal ep_1_lane       : std_logic;
signal ep_4_lane       : std_logic;
signal ep_8_lane       : std_logic;
signal ep_probe        : std_logic_vector(3 downto 0);
signal ap_probe        : std_logic_vector(3 downto 0);

signal db_sys_clk      : std_logic;
signal temp_alarm      : std_logic;
signal fpga_eos        : std_logic;
signal grefclk         : std_logic;
signal grefclk2        : std_logic;

signal DCVR_CS_int     : std_logic;
signal DCVR_CLK_int    : std_logic;
signal DCVR_SDI_int    : std_logic;
signal VADJ_TUNE_int   : std_logic;
signal VAPP_TUNE_int   : std_logic;
signal PG_C2M_int      : std_logic;
signal DB_PG_VAPP_int  : std_logic;

signal TRIGIN_P        : std_logic_vector(DIO_NB - 1 downto 0);
signal TRIGIN_N        : std_logic_vector(DIO_NB - 1 downto 0);
signal TRIGIN_PBUTTON  : std_logic_vector(DIO_NB - 1 downto 0);
signal TRIGIN_50OHM    : std_logic_vector(DIO_NB - 1 downto 0);
signal TRIGIN_LED      : std_logic_vector(DIO_NB - 1 downto 0);
signal TRIGOUT_P       : std_logic_vector(DIO_NB - 1 downto 0);
signal TRIGOUT_N       : std_logic_vector(DIO_NB - 1 downto 0);
signal TRIGIN_prb      : std_logic_vector(DIO_NB - 1 downto 0);

signal user_clk        : std_logic;
signal user_en         : std_logic;
signal user_dat        : std_logic_vector(EBFT_DWIDTH - 1 downto 0);
signal user_cnt        : std16;

signal fifo_arst       : std_logic;
signal fifo_arst_comb  : std_logic;
signal wr_cnt          : std16;
signal wr_full         : std_logic;
signal wr_afull        : std_logic;
signal rd_en           : std_logic;
signal rd_empty        : std_logic;
signal rd_cnt          : std16;
signal fifo_count      : std16;
signal wr_counter      : std16;
signal loop_counter    : std16;
signal eb2_fifo_da     : std_logic_vector(EBFT_DWIDTH - 1 downto 0);



signal sus_jtag_wrapper_clk              : std_logic; 
signal sus_jtag_wrapper_rst_n		 : std_logic; 
signal sus_jtag_wrapper_rst		 : std_logic; 
signal sus_jtag_wrapper_tck              : std_logic;            
signal sus_jtag_wrapper_tms              : std_logic; 
signal sus_jtag_wrapper_td_out_to_asic   : std_logic; 
signal sus_jtag_wrapper_td_in_from_asic  : std_logic; 


signal sus_dyn_pll_clk  : std_logic; 
signal sus_dyn_pll_clk_by5  : std_logic; 
signal sus_dyn_data_out_valid	         : std_logic; 
signal sus_dyn_data_out_64		 : std_logic_vector(63 downto 0);
signal sus_dyn_asic_run		         : std_logic; 
signal sus_dyn_asic_res_n		         : std_logic; 
signal sus_dyn_asic_ser0_dout		 : std_logic;	
signal sus_dyn_asic_ser1_dout		 : std_logic;	

signal sus_dyn_asic_mon                      : std_logic;
signal sus_dyn_cycle_done                    : std_logic;
signal sus_dyn_cycle_done_delayed            : std_logic;

signal sus_daq_fifo_wr_en           : std_logic;
signal sus_T3_telegram_cmd_to_asic  : std_logic;
signal sus_T3_data_from_asic        : std_logic;
signal sus_T3_tele_pattern          : std_logic_vector(581 downto 0);        
signal sus_T3_tele_fifo_wclk        : std_logic;      
signal sus_T3_tele_do_init_link     : std_logic;   
signal sus_T3_tele_link_detected    : std_logic;   



-----
begin
-----

assert ((XC7K_TYP ="XC7K160T") or (XC7K_TYP = "XC7K325T")) report "XC7K Target not correct !!!" severity failure;
assert ((CTRL_PCB = "A") or (CTRL_PCB = "B")) report "Control PCB Target not correct !!!" severity failure;
assert ((BASE_PCB = "A") or (BASE_PCB = "B")) report "Base PCB Target not correct !!!" severity failure;
sus_jtag_wrapper_clk <= user_clk;
sus_jtag_wrapper_rst <= not sus_jtag_wrapper_rst_n;
--sus_jtag_wrapper_clk <= eb_m0_clk;




--------------------------------------------------------------------------------------------------------
ebm0: ebm0_pcie_b                                                      -- PCIe endpoint: E-Bone Master#0
--------------------------------------------------------------------------------------------------------
generic map (
  EB_CLK           => EB_CLK,
  EBFT_DWIDTH      => EBFT_DWIDTH,
  BADR0_MEM        => BADR0_MEM,
  WATCH_DOG        => WATCH_DOG,
  TYPE_DESC        => TYPE_DESC,
  HARD_VERS        => HARD_VERS,
  DEVICE_ID        => DEVICE_ID,
  BC_PERIOD        => BC_PERIOD,
  LINK_SPEED       => LINK_SPEED,
  LINK_WIDTH       => LINK_WIDTH,
  SIM_FILE         => SIM_FILE,
  STAT_COUNTING    => STAT_COUNTING,
  EBM_PCIE_PRB     => EBM_PCIE_PRB
)
port map (
-- PCIe Endpoint dedicated
--------------------------
  sys_clk_p        => pcie_sys_clk_p,
  sys_clk_n        => pcie_sys_clk_n,
  sys_reset_n      => pcie_sys_rst_n,

  pci_exp_rxn      => pci_exp_rxn,
  pci_exp_rxp      => pci_exp_rxp,
  pci_exp_txn      => pci_exp_txn,
  pci_exp_txp      => pci_exp_txp,

  ep_link_up       => ep_link_up,
  ep_1_lane        => ep_1_lane,
  ep_4_lane        => ep_4_lane,
  ep_8_lane        => ep_8_lane,

  soft_wd          => soft_wd_p,
  soft_reset       => soft_reset,
  status_led       => status_led_p,

  ep_probe         => ep_probe,
  ap_probe         => ap_probe,

-- Master #0
------------
  eb_m0_clk_o      => eb_m0_clk,
  eb_m0_rst_o      => eb_m0_rst,
  eb_m0_brq_o      => eb_m0_brq,
  eb_m0_bg_i       => eb_m0_bg,
  eb_bmx_i         => ebs_i.eb_bmx,

  eb_m0_o          => eb_m0,
  eb_m_i           => eb_m,

-- Fast Transmitter
-------------------
  eb_ft_bg_i       => eb_ft_bg,
  eb_ft_dxt_i      => eb_ft_dxt,
  eb_ft_i          => eb_ft,
  eb_ft_dk_o       => eb_ft_dk,
  eb_ft_err_o      => eb_ft_err,

-- Application extension signals
--------------------------------
  ext_ctrl_i       => ext_ctrl_i,
  ext_data_ack_o   => open,
  payload_size_o   => ft_psize

);

ext_ctrl_i.ext_addr_src <= (others => '0');
ext_ctrl_i.ext_data_flg <= '0';
ext_ctrl_i.ext_data_src <= (others => '0');

-------------------------------------------------------------------------------------------------------------
core1: ebs_core_32                                                     -- 1st E-bone (PCIe) core interconnect
-------------------------------------------------------------------------------------------------------------
generic map (
  EBS_TMO_DK   => 12
)
port map (
  eb_clk_i     => eb_m0_clk,
  eb_rst_i     => eb_m0_rst,

-- Fast Transmitter
-------------------
  eb_ft_bg_o   => eb_ft_bg,
  eb_ft_brq_i  => eb_ft_brq,

-- Master #0 dedicated 
----------------------
  eb_m0_bg_o   => eb_m0_bg,
  eb_m0_brq_i  => eb_m0_brq,
  eb_m0_i      => eb_m0,

-- Masters shared bus
---------------------
  eb_m_o       => eb_m,

-- From Slaves (array) 
  eb_s_i       => ebs_o,

-- To Slave shared bus
----------------------
  eb_s_o       => ebs_i
);

------------------------------------------------------------------------- 1st E-BONE SLAVES (EBS_SLV_NB) IMPLEMENTATION AREA BEGIN
----------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
fspi_i: ebs_fspi                                                       -- E-Bone slave 1, BADR1 SPI flash implementation
------------------------------------------------------------------------------------------------------------------------
generic map (
  USE_RS_EDGE  => 0,                                                   -- User CCLK falling edge when 0 (default)
  BUFR_DIVIDE  => "8",                                                 -- EB_CLK frequency divisor for SPI flash memory clock
  TYPE_DESC    => TYPE_DESC,                                           -- Type description target and/or application dependant
  EBS_AD_RNGE  => EBS_AD_RNGE,                                         -- long adressing range
  EBS_AD_BASE  => 1,                                                   -- segment 1 (BADR1)
  EBS_AD_SIZE  => 8,                                                   -- size in segment
  EBS_AD_OFST  => 16#00#,                                              -- offset in segment
  EBS_MIRQ     => (others => '0')                                      -- Message IRQ
)
port map (
-- E-bone interface
-------------------
  eb_clk_i     => eb_m0_clk,
  ebs_i        => ebs_i,
  ebs_o        => ebs_o(1),

-- SPI1 interface 
-----------------
  fspi_cs_o    => FLASH_CS,                                            -- SPI slave select (active Low)
  fspi_sck_o   => open,                                                -- SPI clock
  fspi_mosi_o  => FLASH_MOSI,                                          -- SPI master out slave in
  fspi_miso_i  => FLASH_MISO,                                          -- SPI master in slave out

  fpga_eos_o   => fpga_eos                                             -- End of Startup. Active High output
);

FLASH_HOLD <= '1';
FLASH_WP   <= '1';

---------------------------------------------------------------------------------------------------------------------------------------
sysm_i: ebs_sysm                                                       -- E-Bone slave 2, BADR1 System Monitor Registers implementation
---------------------------------------------------------------------------------------------------------------------------------------
generic map (
  EBS_AD_RNGE  => EBS_AD_RNGE,                                         -- long adressing range
  EBS_AD_BASE  => 1,                                                   -- segment 1 (BADR1)
  EBS_AD_SIZE  => 16,                                                  -- size in segment
  EBS_AD_OFST  => 16#10#,                                              -- offset in segment
  EBS_IN_AUX0  => EBS_IN_AUX0,                                         -- 1st Auxiliary Analog Input number
  EBS_IN_AUX1  => EBS_IN_AUX1,                                         -- 2nd Auxiliary Analog Input number
  TYPE_DESC    => TYPE_DESC                                            -- Type description target and/or application dependant
)
port map (
-- E-bone interface
-------------------
  eb_clk_i     => eb_m0_clk,
  ebs_i        => ebs_i,
  ebs_o        => ebs_o(2),

-- System monitor analog inputs
-------------------------------
  VP_in        => VP_in,
  VN_in        => VN_in,
  VAUXP0_in    => VAUXP0,
  VAUXN0_in    => VAUXN0,
  VAUXP1_in    => VAUXP1,
  VAUXN1_in    => VAUXN1,

-- Temperature alarm
--------------------
  temp_alarm_o => temp_alarm
);

--------------------------------------------------------------------------------------------------------------------------------------
fmc_regs_i: ebs_regs                                                   -- E-Bone slave 3, FMC dedicated BADR1 Registers implementation
--------------------------------------------------------------------------------------------------------------------------------------
generic map (
  EBS_AD_RNGE  => EBS_AD_RNGE,                                         -- long adressing range
  EBS_AD_BASE  => 1,                                                   -- segment 1 (BADR1)
  EBS_AD_SIZE  => REGS_NB,                                             -- size in segment (REGS_NB)
  EBS_AD_OFST  => 16#80#,                                              -- offset in segment
  EBS_MIRQ     => (others => '0'),                                     -- Message IRQ
  REG_RO_SIZE  => (REGS_NB / 2),                                       -- read only reg. number (REGS_NB / 2)
  REG_WO_BITS  => 0                                                    -- reg. 0 self clear bit number
)
port map (
-- E-bone interface
-------------------
  eb_clk_i     => eb_m0_clk,
  ebs_i        => ebs_i,
  ebs_o        => ebs_o(3),

-- Register interface
---------------------
  regs_o       => regs_o_s3,                                           -- R/W registers external outputs, regs_o[(REGS_NB / 2) - 1 downto 0]
  regs_i       => regs_i_s3,                                           -- read only register external inputs, regs_i[REGS_NB - 1 downto (REGS_NB / 2)]
  regs_irq_i   => '0',                                                 -- interrupt request
  regs_iak_o   => open,                                                -- interrupt handshake
  regs_ofsrd_o => regs_ofsrd_s3,                                       -- read burst offset enable
  regs_ofswr_o => regs_ofswr_s3,                                       -- write burst offset enable
  regs_ofs_o   => regs_ofst_s3                                         -- offset
);

gene_regs_ack_s3 : for i in regs_wak_s3'range generate
  regs_rak_s3(i) <= '1' when ((regs_ofsrd_s3 = '1') and (unsigned(regs_ofst_s3) = i)) else '0';
  regs_wak_s3(i) <= '1' when ((regs_ofswr_s3 = '1') and (unsigned(regs_ofst_s3) = i)) else '0';
end generate gene_regs_ack_s3;

--------------------------------------------------------------------------------------------------------------------------------------
dma_regs_i: ebs_regs                                                   -- E-Bone slave 4, DMA dedicated BADR1 Registers implementation
--------------------------------------------------------------------------------------------------------------------------------------
generic map (
  EBS_AD_RNGE  => EBS_AD_RNGE,                                         -- long adressing range
  EBS_AD_BASE  => 1,                                                   -- segment 1 (BADR1)
  EBS_AD_SIZE  => REGS_NB,                                             -- size in segment (REGS_NB)
  EBS_AD_OFST  => 16#100#,                                             -- offset in segment
  EBS_MIRQ     => (others => '0'),                                     -- Message IRQ
  REG_RO_SIZE  => (REGS_NB / 2),                                       -- read only reg. number (REGS_NB / 2)
  REG_WO_BITS  => 0                                                    -- reg. 0 self clear bit number
)
port map (
-- E-bone interface
-------------------
  eb_clk_i     => eb_m0_clk,
  ebs_i        => ebs_i,
  ebs_o        => ebs_o(4),

-- Register interface
---------------------
  regs_o       => regs_o_s4,                                           -- R/W registers external outputs, regs_o[(REGS_NB / 2) - 1 downto 0]
  regs_i       => regs_i_s4,                                           -- read only register external inputs, regs_i[REGS_NB - 1 downto (REGS_NB / 2)]
  regs_irq_i   => '0',                                                 -- interrupt request
  regs_iak_o   => open,                                                -- interrupt handshake
  regs_ofsrd_o => regs_ofsrd_s4,                                       -- read burst offset enable
  regs_ofswr_o => regs_ofswr_s4,                                       -- write burst offset enable
  regs_ofs_o   => regs_ofst_s4                                         -- offset
);



gene_regs_ack_s4 : for i in regs_wak_s4'range generate
  regs_rak_s4(i) <= '1' when ((regs_ofsrd_s4 = '1') and (unsigned(regs_ofst_s4) = i)) else '0';
  regs_wak_s4(i) <= '1' when ((regs_ofswr_s4 = '1') and (unsigned(regs_ofst_s4) = i)) else '0';
end generate gene_regs_ack_s4;

regs_i_s4(8)  <= std_logic_vector(to_unsigned(FIFO_SIZE, 32));
regs_i_s4(9)  <= GND16 & fifo_size_msk & wr_counter(Nlog2(FIFO_SIZE / 8) - 1 downto 0);
regs_i_s4(10) <= wr_full & "000000000000000" & fifo_size_msk & wr_cnt(Nlog2(FIFO_SIZE / 8) - 1 downto 0);
regs_i_s4(11) <= rd_empty & "000000000000000" & fifo_size_msk & rd_cnt(Nlog2(FIFO_SIZE / 8) - 1 downto 0);
regs_i_s4(12) <= ebft_desc_stat0;
regs_i_s4(13) <= ebft_dma_stat;
regs_i_s4(14) <= GND16 & ebft_dma_count;
regs_i_s4(15) <= ft_psize & GND8 & GND4 & loop_counter;

fifo_arst <= regs_o_s4(4)(0);



--fifo_arst <= '1' when ((eb_m0_rst = '1') or (regs_o_s4(7)(31) = '1')) else '0';
user_cnt <= fifo_size_msk & wr_cnt(Nlog2(FIFO_SIZE / 8) - 1 downto 0);



------------------------------------------------------------------------------------------------------------------------------------
ebft_i: ebm_ebft_64                                                    -- E-Bone slave 5, DACQ Fast Transmitter (DMA) implementation
------------------------------------------------------------------------------------------------------------------------------------
generic map (
  EBS_AD_RNGE    => EBS_AD_RNGE,                                       -- long adressing range
  EBS_AD_BASE    => 1,                                                 -- segment 1 (BADR1)
  EBS_AD_SIZE    => 32,                                                -- size in segment
  EBS_AD_OFST    => 16#180#,                                           -- offset in segment
  EBX_MSG_MID    => 1,                                                 -- message master identifier
  EBFT_DBOUND    => 4096,                                              -- Dest. boundady crossing 2**N (0=none)
  EBFT_DA64      => TRUE,                                              -- Dest. Adrs. 32/64 bits
  EBFT_MEMS      => TRUE,                                              -- memory support (or not)
  EBFT_FCLK      => 125,                                               -- Clock frequency in MHz (for timer)
  EBFT_FSCAN     => 0,                                                 -- FIFO scan desc. number max. 12
  EBFT_ASYNCH    => FALSE,                                             -- true when different clock domains
  EBFT_ABRQ      => FALSE                                              -- true for asynchronous arbitration
)
port map (
-- E-bone slave interface
  eb_clk_i       => eb_m0_clk,                                         -- system clock
  ebs_i          => ebs_i,
  ebs_o          => ebs_o(5),

-- External control ports @eb_clk
  cmd_go_i       => '0',                                               -- go command
  cmd_flush_i    => '0',                                               -- flush command
  cmd_abort_i    => '0',                                               -- abort command
  cmd_reset_i    => '0',                                               -- MIF reset command

-- External status ports @eb_clk
  d0_stat_o      => ebft_desc_stat0,                                   -- Descriptor #0 status register [31:0]
  d1_stat_o      => ebft_desc_stat1,                                   -- Descriptor #1 status register [31:0]
  fsc_irqs_o     => fsc_irqs,                                          -- fifo scan IRQ status register
  fsc_stat_o     => fsc_stat,                                          -- fifo scan status register
  dma_stat_o     => ebft_dma_stat,                                     -- Global status register [31:0]

-- E-bone Fast Transmitter
  ft_psize_i     => ft_psize,                                          -- FT encoded payload size
  eb_ft_clk_i    => eb_m0_clk,                                         -- FT and 2nd E-bone clock
  eb_ft_brq_o    => eb_ft_brq,                                         -- FT master bus request
  eb_ft_bg_i     => eb_ft_bg,                                          -- FT master bus grant
  eb_ft_dxt_o    => eb_ft_dxt,                                         -- FT data write
  eb_ft_o        => eb_ft,                                             -- FT controls
  eb_ft_dk_i     => eb_ft_dk,                                          -- FT data acknowledge
  eb_ft_err_i    => eb_ft_err,                                         -- FT bus error

-- (2nd) E-bone master interface
  eb2_ft_bg_i    => '0',                                               -- busy FT
  eb2_mx_brq_o   => eb2_m0_brq,                                        -- bus request
  eb2_mx_bg_i    => eb2_m0_bg,                                         -- bus grant
  eb2_bmx_i      => eb2_bmx,                                           -- busy some master (but FT)
  eb2_mx_o       => eb2_m0_o,                                          -- controls
  eb2_m_i        => eb2_m_i,                                           -- master shared

-- (2nd) E-bone extension master
  ebx2_msg_set_o => ebx2_msg_set,                                      -- message management
  ebx2_msg_dat_i => ebx2_msg_dat,                                      -- message data
  ebx2_dsz_i     => ebx2_m_dsz,                                        -- device data size

-- External triggers
  fs_trig_i      => (others => '0'),
  fs_tack_o      => open,

-- External status ports @eb_ft_clk
	dma_count_o    => ebft_dma_count,                                    -- FIFO requested word count [15:0]
  dma_eot_o      => ebft_dma_eot,                                      -- end of transfer
  dma_err_o      => ebft_dma_err                                       -- transfer aborted on error
);

------------------------------------------------------------------------- 1st E-BONE SLAVES (EBS_SLV_NB) IMPLEMENTATION AREA END
--------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
core2: ebs_core_64                                                     -- 2nd E-bone (DACQ) core interconnect
-------------------------------------------------------------------------------------------------------------
generic map (
  EBS_TMO_DK  => 10
)
port map (
  eb_clk_i    => eb_m0_clk,                                            -- system clock
  eb_rst_i    => eb_m0_rst,                                            -- synchronous system reset

-- Fast Transmitter
  eb_ft_bg_o  => open,                                                 -- bus grant
  eb_ft_brq_i => '0',                                                  -- bus request

-- Master #0 dedicated 
  eb_m0_bg_o  => eb2_m0_bg,                                            -- bus grant
  eb_m0_brq_i => eb2_m0_brq,                                           -- bus request
  eb_m0_i     => eb2_m0_o, 

-- Master shared bus
  eb_m_o      => eb2_m_i,

-- From Slave (array)  
  eb_s_i      => eb2_s_o,

-- To Slave shared bus
  eb_s_o      => eb2_s_i
);

--------------------------------------------------------------------------------------------
ebx: ebx_core1                                                         -- Message management
--------------------------------------------------------------------------------------------
port map (
-- Master
  ebx_msg_m_dat_o => ebx2_msg_dat,                                     -- message to (single) receiver 
  ebx_m_dsz_o     => ebx2_m_dsz,                                       -- device size, shared

-- Slaves (array) 
  ebx_s_i         => ebx2_o
);

------------------------------------------------------------------------- 2nd E-BONE SLAVES (EBAS_SLV_NB) IMPLEMENTATION AREA BEGIN
-----------------------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------
fifo_i: ebs_fifo_64                                                    -- E-Bone slave 1, FIFO_SIZE K-byte memory implementation
--------------------------------------------------------------------------------------------------------------------------------
generic map (
  EBS_AD_RNGE   => EBS_AD_RNGE,                                        -- long adressing range
  EBS_AD_BASE   => 1,                                                  -- segment 1 (BADR1)
  EBS_AD_SIZE   => 4,                                                  -- size in segment (32-bit word)
  EBS_AD_OFST   => 16#200#                                             -- offset in segment
)
port map (
-- E-bone slave interface
  eb_clk_i      => eb_m0_clk,
  ebs_i         => eb2_s_i,
  ebs_o         => eb2_s_o(1),
  ebx_o         => ebx2_o(1),
  ebx_msg_set_i => ebx2_msg_set,

-- FIFO memory interface
  mem_count_o   => fifo_count,                                         -- MIF requested count
  mem_din_o     => open,                                               -- FIFO data in
  mem_dout_i    => eb2_fifo_da,                                        -- FIFO data out
  mem_wr_o      => open,                                               -- FIFO write enable
  mem_rd_o      => rd_en,                                              -- FIFO read enable
  mem_rst_o     => open,                                               -- FIFO reset 
  mem_empty_i   => rd_empty,                                           -- FIFO empty status
  mem_full_i    => wr_full,                                            -- FIFO full status
  mem_rd_cnt_i  => rd_cnt                                              -- FIFO occupancy (may ground, but message in use)
);





--process(sus_dyn_pll_clk)
--begin
--    if rising_edge(sus_dyn_pll_clk) then
--        if(wr_afull = '1') then
--            sus_daq_fifo_wr_en <= '0';
--        else
--            sus_daq_fifo_wr_en <= sus_dyn_data_out_valid;
--        end if;
--    end if;
--end process;

--process(sus_dyn_pll_clk)
--begin
--    if rising_edge(sus_dyn_pll_clk) then
--        if(wr_afull = '1') then
--            sus_daq_fifo_wr_en <= '0';
--        else
--            sus_daq_fifo_wr_en <= sus_dyn_data_out_valid;
--        end if;
--    end if;
--end process;

sus_daq_fifo_wr_en <= sus_dyn_data_out_valid;

fifo_arst_comb <= fifo_arst or sus_jtag_wrapper_rst;


----------------------------------------------------------------------------------------------------------
fifo_a_i: fifo_a                                                       -- Asynchronous FIFO implementation
----------------------------------------------------------------------------------------------------------
generic map (
  DWIDTH    => EBFT_DWIDTH,                                            -- data width
  DWINP     => EBFT_DWIDTH,                                            -- port data in width
  ADEPTH    => FIFO_SIZE / 8,                                          -- FIFO depth
  RAMTYP    => "block"                                                 -- "auto", "block", "distributed"
)
port map (
--  arst      => sus_jtag_wrapper_rst,
  arst      => fifo_arst_comb, 
  wr_clk    => sus_T3_tele_fifo_wclk,
  wr_en     => sus_daq_fifo_wr_en,
--  wr_en     => sus_daq_fifo_wr_en,
  wr_dat    => sus_dyn_data_out_64,
  wr_cnt    => wr_cnt(Nlog2(FIFO_SIZE / 8) - 1 downto 0),
  wr_afull  => wr_afull,
  wr_full   => wr_full,

  rd_clk    => eb_m0_clk,
  rd_en     => rd_en,
  rd_dat    => eb2_fifo_da,
  rd_cnt    => rd_cnt(Nlog2(FIFO_SIZE / 8) - 1 downto 0),
  rd_aempty => open,
  rd_empty  => rd_empty
);

------------------------------------------------------------------------- 2nd E-BONE SLAVES (EBAS_SLV_NB) IMPLEMENTATION AREA END
---------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------
user_i : dblock_dmak_user                                              -- User data flow emulator implementation
----------------------------------------------------------------------------------------------------------------
generic map (
  EBFT_DWIDTH    => EBFT_DWIDTH
)
port map (
-- System clock and reset
  eb_sys_rst     => eb_m0_rst,
  eb_sys_clk     => eb_m0_clk,

-- Configuration registers
  count_reg_on   => regs_wak_s4(0),
  count_reg      => regs_o_s4(0),
  v_lsb_reg_on   => regs_wak_s4(1),
  v_lsb_reg      => regs_o_s4(1),
  v_msb_reg_on   => regs_wak_s4(2),
  v_msb_reg      => regs_o_s4(2),
  cntrl_reg_on   => regs_wak_s4(3),
  cntrl_reg      => regs_o_s4(3),
  wr_counter_o   => wr_counter(Nlog2(FIFO_SIZE / 8) - 1 downto 0),
  loop_counter_o => loop_counter,

-- User FIFO access
  user_wr        => user_cnt,
  user_full      => wr_full,
  user_afull     => wr_afull,
  user_clk       => user_clk,
  user_en        => user_en,
  user_data      => user_dat
);



-----------------------------------------------------------------------------------------------------------------------
sus_top_i : sus_top                                                    -- SuS Top module
-----------------------------------------------------------------------------------------------------------------------
port map (

-- EBONE INTERFACE ----------------------------------------------------
  eb_clk_i     				=> eb_m0_clk,
  ebs_i        				=> ebs_i,
  ebs_o                                 => ebs_o,


-- JTAG ---------------------------------------------------------------
  sus_jtag_wrapper_clk			=> sus_jtag_wrapper_clk,
                                                                         
  sus_jtag_wrapper_rst_n		=> sus_jtag_wrapper_rst_n,	  
                                                                         
  sus_jtag_wrapper_tck			=> sus_jtag_wrapper_tck,		  
  sus_jtag_wrapper_tms			=> sus_jtag_wrapper_tms,	  
  sus_jtag_wrapper_td_out_to_asic	=> sus_jtag_wrapper_td_out_to_asic,
  sus_jtag_wrapper_td_in_from_asic	=> sus_jtag_wrapper_td_in_from_asic,


-- Dynamic ASIC Control ------------------------------------------------


  sus_dyn_pll_clk               => sus_dyn_pll_clk,
  sus_dyn_pll_clk_by5           => sus_dyn_pll_clk_by5,
  sus_dyn_data_out_valid		=> sus_dyn_data_out_valid,
  sus_dyn_data_out_64			=> sus_dyn_data_out_64,	
                                                               
  sus_dyn_asic_run			=> sus_dyn_asic_run,	
  sus_dyn_asic_ser0_dout		=> sus_dyn_asic_ser0_dout,
  sus_dyn_asic_ser1_dout		=> sus_dyn_asic_ser1_dout,

  sus_dyn_asic_res_n			=> sus_dyn_asic_res_n,

  daq_fifo_wr_full              => wr_afull,
  sus_dyn_asic_mon              => sus_dyn_asic_mon,

  sus_dyn_cycle_done            => sus_dyn_cycle_done,
  sus_dyn_cycle_done_delayed      => sus_dyn_cycle_done_delayed,

-- T3 Controller & Telegram ------------------------------------------------

  sus_T3_telegram_cmd_to_asic    =>  sus_T3_telegram_cmd_to_asic,
  sus_T3_data_from_asic          =>  sus_T3_data_from_asic,
                                                                
--  sus_T3_tele_pattern            =>  sus_T3_tele_pattern,        
  sus_T3_tele_fifo_wclk          =>  sus_T3_tele_fifo_wclk      
                                                                
--  sus_T3_tele_do_init_link       =>  sus_T3_tele_do_init_link,   
--  sus_T3_tele_link_detected      =>  sus_T3_tele_link_detected  
);


-----------------------------------------------------------------------------------------------------------------------
dcvr_i : dblock_dmak_dcvr                                              -- VADJ and VAPP voltages control implementation
-----------------------------------------------------------------------------------------------------------------------
port map (
-- System clock and reset
-------------------------
  eb_sys_rst => eb_m0_rst,
  eb_sys_clk => eb_m0_clk,

-- Registers and selection
--------------------------
  VADJ_ON    => regs_wak_s3(1),
  VADJ_DAT   => regs_o_s3(1)(9 downto 0),
  VAPP_ON    => regs_wak_s3(0),
  VAPP_DAT   => regs_o_s3(0)(9 downto 0),

-- DCVR control
---------------
  DCVR_CS    => DCVR_CS_int,
  DCVR_CLK   => DCVR_CLK_int,
  DCVR_SDI   => DCVR_SDI_int,
  VADJ_TUNE  => VADJ_TUNE_int,
  VAPP_TUNE  => VAPP_TUNE_int,

-- Power Good control
---------------------
  PG_VADJ    => regs_o_s3(1)(31),
  PG_VAPP    => regs_o_s3(0)(31),
  PG_C2M     => PG_C2M_int,
  DB_PG_VAPP => DB_PG_VAPP_int
);

-----------------------------------------------------------------------------------------------------
triggers_i : dblock_dmak_dio                                           -- Triggers I/O implementation
-----------------------------------------------------------------------------------------------------
port map (
  TRIGIN_P       => TRIGIN_P,
  TRIGIN_N       => TRIGIN_N,
  TRIGIN_PBUTTON => TRIGIN_PBUTTON,
  TRIGIN_50OHM   => TRIGIN_50OHM,
  TRIGIN_LED     => TRIGIN_LED,
  TRIGOUT_P      => TRIGOUT_P,
  TRIGOUT_N      => TRIGOUT_N
);

-------------------------------------------------------------------------------------------------------------------------------
fmc_lpc_i : dblock_dmak_fmc                                            -- DBRIDGE Signals FMC/LPC_MEZZANINE part implementation
-------------------------------------------------------------------------------------------------------------------------------
port map (
  eb_m0_clk     			=> eb_m0_clk,

  GBTCLK0_M2C_P 			=> GBTCLK0_M2C_P,
  GBTCLK0_M2C_N 			=> GBTCLK0_M2C_N,
  DP_M2C_P      			=> DP_M2C_P,
  DP_M2C_N      			=> DP_M2C_N,
-- synthesis translate_off
  DP_C2M_P      			=> DP_C2M_P,
  DP_C2M_N      			=> DP_C2M_N,
-- synthesis translate_on

  CLK_M2C_P     			=> CLK_M2C_P,
  CLK_M2C_N     			=> CLK_M2C_N,

  LA_MON_P      			=> LA2_P,
  LA_MON_N      			=> LA2_N,
--  LA_SER0_P      			=> LA4_P,
--  LA_SER0_N      			=> LA4_N,
--  LA_SER1_P      			=> LA7_P,
--  LA_SER1_N      			=> LA7_N,
  LA_RUN_P      			=> LA5_P,
  LA_RUN_N      			=> LA5_N,
  LA_TDO_TO_ASIC_P     			=> LA15_P,
  LA_TDO_TO_ASIC_N     			=> LA15_N,
  LA_RES_P      			=> LA9_P,
  LA_RES_N      			=> LA9_N,
  LA_TMS_P      			=> LA11_P,
  LA_TMS_N      			=> LA11_N,
  LA_LED_P      			=> LA14_P,
  LA_LED_N      			=> LA14_N,
  LA_TDI_FROM_ASIC_P   			=> LA8_P,
  LA_TDI_FROM_ASIC_N   			=> LA8_N,
  LA_TCK_P      			=> LA19_P,
  LA_TCK_N      			=> LA19_N,
  LA_PLL_RESET_P    			=> LA30_P,
  LA_PLL_RESET_N    			=> LA30_N,
  LA_C2B_P      			=> LA31_P,
  LA_C2B_N      			=> LA31_N,
  LA_LOL_P      			=> LA33_P,
  LA_LOL_N      			=> LA33_N,

  
  LA_CLK_P      			=> LA32_P,
  LA_CLK_N      			=> LA32_N,
 
 
  LA_TELEGRAM_CMD_TO_ASIC_P =>  LA7_P,
  LA_TELEGRAM_CMD_TO_ASIC_N =>  LA7_N,

  LA_T3_DATA_FROM_ASIC_P    =>  LA4_P,
  LA_T3_DATA_FROM_ASIC_N    =>  LA4_N,
 
  GA            			=> GA,
  PRSNT_M2C_L   			=> PRSNT_M2C_L,
  regs_o_s3     			=> regs_o_s3,
  regs_i_s3     			=> regs_i_s3,
  sus_jtag_wrapper_tck            	=> sus_jtag_wrapper_tck,            
  sus_jtag_wrapper_tms                  => sus_jtag_wrapper_tms,            
  sus_jtag_wrapper_td_out_to_asic       => sus_jtag_wrapper_td_out_to_asic, 
  sus_jtag_wrapper_td_in_from_asic      => sus_jtag_wrapper_td_in_from_asic,
  sus_dyn_asic_run			=> sus_dyn_asic_run,    
  sus_dyn_asic_res_n			=> sus_dyn_asic_res_n,
  sus_dyn_asic_ser0_dout		=> sus_dyn_asic_ser0_dout,
  sus_dyn_asic_ser1_dout		=> sus_dyn_asic_ser1_dout,
  sus_dyn_asic_mon                      => sus_dyn_asic_mon,
  sus_dyn_pll_clk               => sus_dyn_pll_clk,


  sus_T3_telegram_cmd_to_asic  => sus_T3_telegram_cmd_to_asic, 
  sus_T3_data_from_asic        => sus_T3_data_from_asic
);



DB_DLINK_ACT  <= '0';
DB_DLINK_TXEN <= '0';
DB_DLINK_TX   <= (others => '0');
DB_DLINK_RXEN <= '0';

IBUFG_CLK2_i : IBUFGDS
port map (
  I  => DB_IO_MRCC_VAPP_P(0),
  IB => DB_IO_MRCC_VAPP_N(0),
  O  => db_sys_clk
);

QSV_RST_N <= '1';
WD_TRIG_N <= '1';
THRM_N <= THRM_TRIP_N;
FPGA_I2C_EN <= '1';

DB_IO_2V5(4) <= DCVR_CS_int;
DB_IO_2V5(0) <= DCVR_CLK_int;
DB_IO_2V5(1) <= DCVR_SDI_int;
DB_IO_2V5(2) <= VADJ_TUNE_int;
DB_IO_2V5(3) <= VAPP_TUNE_int;

TRIGIN_P <= DB_IO_2V5_P(19) & DB_IO_2V5_P(0);
TRIGIN_N <= DB_IO_2V5_N(19) & DB_IO_2V5_N(0);
TRIGIN_PBUTTON <= DB_IO_2V5_N(4) & DB_IO_2V5_P(4);
DB_IO_2V5_N(1) <= TRIGIN_50OHM(1);
DB_IO_2V5_P(1) <= TRIGIN_50OHM(0);
DB_IO_2V5_N(2) <= TRIGIN_LED(1);
DB_IO_2V5_P(2) <= TRIGIN_LED(0);
DB_IO_2V5_P(3) <= TRIGOUT_P(1);
DB_IO_2V5_N(3) <= TRIGOUT_N(1);
DB_IO_2V5_P(21) <= TRIGOUT_P(0);
DB_IO_2V5_N(21) <= TRIGOUT_N(0);

DB_IO_2V5(5) <= ep_probe(0);        -- TP1
DB_IO_2V5(7) <= ep_probe(1);        -- TP2
DB_IO_2V5(8) <= ep_probe(2);        -- TP3
DB_IO_2V5(9) <= ep_probe(3);        -- TP4


PG_C2M <= PG_C2M_int;
DB_PG_VAPP <= DB_PG_VAPP_int;

soft_reset_n <= not soft_reset;



ap_probe(0) <= sus_dyn_asic_run;
--ap_probe(1) <= sus_T3_telegram_cmd_to_asic;
--ap_probe(1) <= sus_T3_data_from_asic;
ap_probe(1) <= sus_dyn_asic_mon;
--ap_probe(1) <= sus_dyn_data_out_valid; 
--ap_probe(2) <= sus_dyn_asic_ser0_dout;
--ap_probe(2) <= sus_dyn_cycle_done;
--ap_probe(2) <= sus_dyn_pll_clk; 
ap_probe(2) <= sus_dyn_data_out_valid; 
--ap_probe(2) <= '1'; 
--ap_probe(2) <= sus_dyn_pll_clk_by5;
--ap_probe(3) <= sus_dyn_cycle_done_delayed;
ap_probe(3) <= sus_T3_tele_fifo_wclk;
--ap_probe(3) <= user_clk;


end dblock_dmak_top_rtl; 
