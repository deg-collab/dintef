`default_nettype none
import Readout::pattern_t;

// unit suitable for simulation:
// simple interfaces, no JTAG regs, LVDS receivers, etc.
module T3Controller #(
    parameter int P_FIFO_WIDTH = 32,
    parameter bit [P_FIFO_WIDTH-1:0] P_SYNC_WORD = 32'h01234567
) (
    input wire CLK_312,
    input wire RESET,

    // signals to/from SUS65T3
    output bit  T3_CMD_IN,
    input  wire T3_DATA_OUT,

    // control
    input wire pattern_t PATTERN,

    // data
    output bit FIFO_WCLK,
    input wire FIFO_FREE,
    output logic FIFO_WEN,
    output bit [P_FIFO_WIDTH-1 : 0] FIFO_WDATA,

    input  wire DO_INIT_LINK,
    output bit  LINK_DETECTED
);
  timeunit 1ns; timeprecision 10ps;

  import Readout::*;

  clock_t clock;

  ClockGen clk_I (
      .CLK_312,
      .REBOOT(1'b0),
      .CLOCK(clock),
      .ALL_LOCKED(),
      .CONFIG_DONE()
  );

  always_comb FIFO_WCLK = clock.CLK_31;

  // upstream data from ASIC
  push_receiver #(
      .P_FIFO_WIDTH(P_FIFO_WIDTH),
      .P_SYNC_WORD (P_SYNC_WORD)
  ) recv_I (
      .CLOCK(clock),
      .RESET(RESET),

      .ENABLE(1'b1),
      .DO_INIT_LINK,

      .SEROUT(T3_DATA_OUT),
      .WORD_SYNCED(LINK_DETECTED),
      .FIFO_WDATA,
      .FIFO_WEN,
      .FIFO_FREE
  );

  telegram_sender telegram_I (
      .CLOCK(clock),
      .RESET,

      .PATTERN,
      .T3_CMD_IN
  );

endmodule : T3Controller
`default_nettype wire
