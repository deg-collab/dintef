-------------------------------------------------------------
--
-- File        : dblock_dmak_fmc.vhd
-- Description : DBRIDGE Signals FMC/LPC_MEZZANINE part.
--
-------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.ebs_pkg.all;
use work.dblock_dmak_pkg.all;

library unisim;
use unisim.vcomponents.IBUFDS_GTE2;
use unisim.vcomponents.OBUFDS;
use unisim.vcomponents.IBUFDS;

entity dblock_dmak_fmc is

port (
  eb_m0_clk                         : in std_logic;                                        -- User clock from PCIe = 125MHz

  GBTCLK0_M2C_P                     : in std_logic;                                        -- GTX transceiver positive differential input reference clock
  GBTCLK0_M2C_N                     : in std_logic;                                        -- GTX transceiver negative differential input reference clock
  DP_M2C_P                          : in std_logic_vector(0 downto 0);                     -- GTX transceiver positive differential receiver line input
  DP_M2C_N                          : in std_logic_vector(0 downto 0);                     -- GTX transceiver negative differential receiver line input
-- synthesis tra                    nslate_off
  DP_C2M_P                          : out std_logic_vector(0 downto 0);                    -- GTX transceiver positive differential transmitter line output
  DP_C2M_N                          : out std_logic_vector(0 downto 0);                    -- GTX transceiver negative differential transmitter line output
-- synthesis tra                    nslate_on

  CLK_M2C_P                         : in std_logic_vector(1 downto 0);                     -- Differential positive clock signals, driven from the Mezzanine Module to the carrier card
  CLK_M2C_N                         : in std_logic_vector(1 downto 0);                     -- Differential negative clock signals, driven from the Mezzanine Module to the carrier card

 -- input
  LA_MON_P      	                : in std_logic;
  LA_MON_N      	                : in std_logic;
--  LA_SER0_P      	                : in std_logic;
--  LA_SER0_N      	                : in std_logic;
--  LA_SER1_P      	                : in std_logic;
--  LA_SER1_N      	                : in std_logic;
  LA_C2B_P      	                : in std_logic;
  LA_C2B_N      	                : in std_logic;
  LA_LOL_P      	                : in std_logic;
  LA_LOL_N      	                : in std_logic;
--  LA_TDO_P      : in std_logic;
--  LA_TDO_N      : in std_logic;
  LA_TDO_TO_ASIC_P                  : out std_logic;
  LA_TDO_TO_ASIC_N	                : out std_logic;

 
 -- output
  LA_LED_P      	                : out std_logic;
  LA_LED_N      	                : out std_logic;
  -- ASIC
  LA_RUN_P      	                : out std_logic;
  LA_RUN_N      	                : out std_logic;
  LA_RES_P      	                : out std_logic;
  LA_RES_N      	                : out std_logic;
  -- PLL
  LA_PLL_RESET_P                   	: out std_logic;
  LA_PLL_RESET_N                   	: out std_logic;
  LA_CLK_P      	                : out std_logic;
  LA_CLK_N      	                : out std_logic;
  -- JTAG
--  LA_TDI_P      : out std_logic;
--  LA_TDI_N      : out std_logic;
  LA_TDI_FROM_ASIC_P   	            : in std_logic;
  LA_TDI_FROM_ASIC_N  	            : in std_logic;
  LA_TCK_P      	                : out std_logic;
  LA_TCK_N      	                : out std_logic;
  LA_TMS_P      	                : out std_logic;
  LA_TMS_N      	                : out std_logic;


  -- SUS JTAG FIFO WRITE TEST 
  LA_FIFO_WRITE_P 	                : out std_logic;
  LA_FIFO_WRITE_N 	                : out std_logic;


  -- SUS T3 Control & Telegram
  LA_TELEGRAM_CMD_TO_ASIC_P         : out std_logic;
  LA_TELEGRAM_CMD_TO_ASIC_N         : out std_logic;

  LA_T3_DATA_FROM_ASIC_P            : in std_logic; 
  LA_T3_DATA_FROM_ASIC_N            : in std_logic;

  -- SUS JTAG 
--  LA_SUS_TCK_P 	: out std_logic;
--  LA_SUS_TCK_N 	: out std_logic;
--  LA_SUS_TMS_P 	: out std_logic;
--  LA_SUS_TMS_N 	: out std_logic;
--  LA_SUS_TDO_P 	: out std_logic;
--  LA_SUS_TDO_N 	: out std_logic;
--  LA_SUS_TDI_P 	: out std_logic;
--  LA_SUS_TDI_N 	: out std_logic;

  GA                                    : in std_logic_vector(1 downto 0);                     -- Geographical address of the mezzanine module used for I2C channel select
  PRSNT_M2C_L                           : in std_logic;                                        -- Module present signal, mezzanine module present when low

  regs_o_s3 			    	        : in std32_a((REGS_NB / 2) - 1 downto 0);              -- R/W registers[0..7] from EBONE ebs_regs module
  regs_i_s3     			            : out std32_a(REGS_NB - 1 downto (REGS_NB / 2));        -- R only registers[8..15] to EBONE ebs_regs module
  sus_fifo_write 			            : in std_logic;
  sus_jtag_wrapper_tck            	    : in std_logic;  
  sus_jtag_wrapper_tms                  : in std_logic;  
--  sus_jtag_wrapper_td_out_to_asic       : out std_logic;  
  sus_jtag_wrapper_td_out_to_asic       : in std_logic;  
--  sus_jtag_wrapper_td_in_from_asic      : in std_logic  
  sus_jtag_wrapper_td_in_from_asic      : out std_logic;  
--  regs_sus_dyn_rw			: in std32_a((REGS_NB / 2) - 1 downto 0);

  sus_dyn_asic_run			            : in std_logic;                      
  sus_dyn_asic_res_n			        : in std_logic;
  sus_dyn_asic_ser0_dout		        : out std_logic;
  sus_dyn_asic_ser1_dout        		: out std_logic;
  sus_dyn_asic_mon                      : out std_logic;
  sus_dyn_pll_clk                       : out std_logic;
  sus_dyn_cycle_done                    : in std_logic;

  sus_T3_telegram_cmd_to_asic           : in std_logic;  
  sus_T3_data_from_asic                 : out std_logic 
);

end dblock_dmak_fmc;

------------------------------------------------------------------------
architecture dblock_dmak_fmc_rtl of dblock_dmak_fmc is
------------------------------------------------------------------------

signal grefclk  : std_logic;
signal grefclk2 : std_logic;
signal ser : std_logic;
signal mon : std_logic;
signal c2b : std_logic;
signal lol : std_logic;
signal tdo : std_logic; 

-----
begin
-----

gtxbuf : IBUFDS_GTE2
port map (
  I     => GBTCLK0_M2C_P,
  IB    => GBTCLK0_M2C_N,
  CEB   => '0',
  O     => grefclk,
  ODIV2 => grefclk2
);



------------------------------
OBUFDS0_i : OBUFDS
port map (
  O  => LA_LED_P,
  OB => LA_LED_N,
  I  => regs_o_s3(2)(0)
);

OBUFDS1_i : OBUFDS
port map (
  O  => LA_RUN_P,
  OB => LA_RUN_N,
  I  => sus_dyn_asic_run
);

OBUFDS2_i : OBUFDS
port map (
  O  => LA_RES_P,
  OB => LA_RES_N,
  I  => sus_dyn_asic_res_n 
);

OBUFDS3_i : OBUFDS
port map (
  O  => LA_PLL_RESET_P,
  OB => LA_PLL_RESET_N,
  I  => regs_o_s3(2)(3)
);

--OBUFDS4_i : OBUFDS
--port map (
--  O  => LA_TDI_P,
--  OB => LA_TDI_N,
----  I  => regs_o_s3(2)(4)
--  I  => sus_jtag_wrapper_td_in_from_asic
--);
OBUFDS4_i : OBUFDS
port map (
  O  => LA_TDO_TO_ASIC_P,
  OB => LA_TDO_TO_ASIC_N,
--  I  => regs_o_s3(2)(4)
  I  => sus_jtag_wrapper_td_out_to_asic
);


OBUFDS5_i : OBUFDS
port map (
  O  => LA_TCK_P,
  OB => LA_TCK_N,
--  I  => regs_o_s3(2)(5)
  I  => sus_jtag_wrapper_tck
);

OBUFDS6_i : OBUFDS
port map (
  O  => LA_TMS_P,
  OB => LA_TMS_N,
--  I  => regs_o_s3(2)(6)
  I  => sus_jtag_wrapper_tms
);
------------------------------
OBUFDS7_i : OBUFDS
port map (
  O  => LA_CLK_P,
  OB => LA_CLK_N,
  I  => eb_m0_clk
);

OBUFDS8_i : OBUFDS
port map (
O  => LA_FIFO_WRITE_P,
OB => LA_FIFO_WRITE_N,
I  => sus_fifo_write
);

OBUFDS9_i : OBUFDS
port map (
O  => LA_TELEGRAM_CMD_TO_ASIC_P,
OB => LA_TELEGRAM_CMD_TO_ASIC_N,
I  => not sus_T3_telegram_cmd_to_asic  -- Invert cmd because input lvds receiver on ASIC is inverting
);



-- 
-- OBUFDS9_i : OBUFDS
-- port map (
-- O  => LA_SUS_TCK_P,
-- OB => LA_SUS_TCK_N,
-- I  => sus_jtag_wrapper_tck
-- );
-- 
-- OBUFDS10_i : OBUFDS
-- port map (
-- O  => LA_SUS_TMS_P,
-- OB => LA_SUS_TMS_N,
-- I  => sus_jtag_wrapper_tms
-- );
-- 
-- 
-- OBUFDS11_i : OBUFDS
-- port map (
-- O  => LA_SUS_TDO_P,
-- OB => LA_SUS_TDO_N,
-- I  => sus_jtag_wrapper_td_out_to_asic 
-- );
-- 
-- OBUFDS12_i : OBUFDS
-- port map (
-- O  => LA_SUS_TDI_P,
-- OB => LA_SUS_TDI_N,
-- I  => sus_jtag_wrapper_td_in_from_asic 
-- );

------------------------------

--IBUFDS0_i : IBUFDS
--port map (
--  I  => LA_SER0_P,
--  IB => LA_SER0_N,
--  O  => sus_dyn_asic_ser0_dout 
--);

--IBUFDS5_i : IBUFDS
--port map (
--  I  => LA_SER1_P,
--  IB => LA_SER1_N,
--  O  => sus_dyn_asic_ser1_dout 
--);



IBUFDS1_i : IBUFDS
port map (
  I  => LA_MON_P,
  IB => LA_MON_N,
  O  => sus_dyn_asic_mon
);

IBUFDS2_i : IBUFDS
port map (
  I  => LA_C2B_P,
  IB => LA_C2B_N,
  O  => c2b
);

IBUFDS3_i : IBUFDS
port map (
  I  => LA_LOL_P,
  IB => LA_LOL_N,
  O  => lol
);

--IBUFDS4_i : IBUFDS
--port map (
--  I  => LA_TDO_P,
--  IB => LA_TDO_N,
--  O  => sus_jtag_wrapper_td_out_to_asic
--);
IBUFDS4_i : IBUFDS
port map (
  I  => LA_TDI_FROM_ASIC_P,
  IB => LA_TDI_FROM_ASIC_N,
  O  => sus_jtag_wrapper_td_in_from_asic
);

IBUFDS6_i : IBUFDS
port map (
  I  => CLK_M2C_P(0),
  IB => CLK_M2C_N(0),
  O  => sus_dyn_pll_clk
);

IBUFDS7_i : IBUFDS
port map (
  I  => LA_T3_DATA_FROM_ASIC_P,
  IB => LA_T3_DATA_FROM_ASIC_N,
  O  => sus_T3_data_from_asic
);


--regs_i_s3(8) <= (0 => ser, 1 => mon, 2 => c2b, 3 => lol, 4 => tdo,  others => '0');
regs_i_s3(8) <= (0 => ser, 1 => '0', 2 => c2b, 3 => lol, 4 => tdo,  others => '0');

gene_regs_ro : for i in 9 to 15 generate
  regs_i_s3(i) <= (others => '0');
end generate;

end dblock_dmak_fmc_rtl;
