module clk_div5(
    input wire clk,
    output wire clk_by5
);



reg [2:0] clk_divcount_a, clk_divcount_b;
reg clk_by5_a, clk_by5_b;


always @(posedge clk) begin
    if(clk_divcount_a == 5) begin
        clk_divcount_a <= 1;
        clk_by5_a <= 1'b1;
    end else if(clk_divcount_a >= 2) begin 
        clk_divcount_a <= clk_divcount_a + 1;
        clk_by5_a <= 1'b0;
    end else begin
        clk_divcount_a <= clk_divcount_a + 1;
        clk_by5_a <= 1'b1;
    end
end

always @(negedge clk) begin 
    if(clk_divcount_b == 5) begin
        clk_divcount_b <= 1;
        clk_by5_b <= 1'b1;
    end else if(clk_divcount_b >= 2) begin
        clk_divcount_b <= clk_divcount_b + 1;
        clk_by5_b <= 1'b0;
    end else begin
        clk_divcount_b <= clk_divcount_b + 1;
        clk_by5_b <= 1'b1;
    end
end 
   
assign clk_by5 = clk_by5_a | clk_by5_b;

endmodule
