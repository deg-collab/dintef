-----------------------------------------------------
--
-- DBLOCK/DMAK general purpose frame.
--
-- Package file for components, constants, functions,
-- subtypes and types definitions.
--
-----------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

package dblock_dmak_pkg is

-------------------------------------------------
-- Constants, subtypes and types declaration area
-------------------------------------------------

constant M32K        : NATURAL := 32768;
constant M64K        : NATURAL := 65536;
constant FIFO_SIZE   : NATURAL := M32K * 1;                    -- 32KByte
constant FIFO_HSIZE  : INTEGER := ((M32K * 1) / 8) / 2;        -- 1/2 fifo size (64-bit word)
constant EBS_SLV_NB  : NATURAL := 8;                           -- EBone (PCIe) nb. of slaves
constant EBAS_SLV_NB : NATURAL := 1;                           -- EBone (DACQ) nb. of slaves
constant DLINK_NB    : INTEGER := 2;
constant DIO_NB      : INTEGER := 2;
constant REGS_NB     : INTEGER := 16;
constant REGS_NB_T3CONT : INTEGER := 25; 

subtype std1 is std_logic;
type std1_a is array(natural RANGE <>) of std1;

subtype fifo_size_msk_def is std_logic_vector(16 - Nlog2(FIFO_SIZE / 8) - 1 downto 0);
constant fifo_size_msk : fifo_size_msk_def := (others => '0');

----------------------------
-- Functions definition area
----------------------------

------------------------------
-- Components declaration area
------------------------------

------------------------------------------------------
component dblock_dmak_user  -- User data flow emulator
------------------------------------------------------
generic (
  EBFT_DWIDTH    : NATURAL := 64
);
port (
-- System clock and reset
  eb_sys_rst     : in std_logic;
  eb_sys_clk     : in std_logic;

-- Configuration registers
  count_reg_on   : in std_logic;
  count_reg      : in std32;
  v_lsb_reg_on   : in std_logic;
  v_lsb_reg      : in std32;
  v_msb_reg_on   : in std_logic;
  v_msb_reg      : in std32;
  cntrl_reg_on   : in std_logic;
  cntrl_reg      : in std32;
  wr_counter_o   : out std_logic_vector(Nlog2(FIFO_SIZE / 8) - 1 downto 0);
  loop_counter_o : out std16;

-- User FIFO access
  user_wr        : in std16;
  user_full      : in std_logic;
  user_afull     : in std_logic;
  user_clk       : out std_logic;
  user_en        : out std_logic;
  user_data      : out std_logic_vector(EBFT_DWIDTH - 1 downto 0)
);
end component;

-----------------------------------------------------------------------------------------------------
component dblock_dmak_dcvr  -- Digitally Controlled Variable Resistor, VADJ and VAPP voltages control
-----------------------------------------------------------------------------------------------------
port (
-- System clock and reset
  eb_sys_rst : in std_logic;
  eb_sys_clk : in std_logic;

-- Registers and selection
  VADJ_ON    : in std_logic;
  VADJ_DAT   : in std_logic_vector(9 downto 0);
  VAPP_ON    : in std_logic;
  VAPP_DAT   : in std_logic_vector(9 downto 0);

-- DCVR control
  DCVR_CS    : out std_logic;
  DCVR_CLK   : out std_logic;
  DCVR_SDI   : out std_logic;
  VADJ_TUNE  : out std_logic;
  VAPP_TUNE  : out std_logic;

-- Power Good control
  PG_VADJ    : in std_logic;
  PG_VAPP    : in std_logic;
  PG_C2M     : out std_logic;
  DB_PG_VAPP : out std_logic
);
end component;

----------------------------------------------------------------------
component dblock_dmak_dio   -- External general purpose Inputs/Outputs
----------------------------------------------------------------------
port (
  TRIGIN_P       : in std_logic_vector(DIO_NB - 1 downto 0);
  TRIGIN_N       : in std_logic_vector(DIO_NB - 1 downto 0);
  TRIGIN_PBUTTON : in std_logic_vector(DIO_NB - 1 downto 0);
  TRIGIN_50OHM   : out std_logic_vector(DIO_NB - 1 downto 0);
  TRIGIN_LED     : out std_logic_vector(DIO_NB - 1 downto 0);
  TRIGOUT_P      : out std_logic_vector(DIO_NB - 1 downto 0);
  TRIGOUT_N      : out std_logic_vector(DIO_NB - 1 downto 0)
);
end component;

---------------------------------------------------------------------
component dblock_dmak_fmc   -- DBRIDGE Signals FMC/LPC_MEZZANINE part
---------------------------------------------------------------------
port (
  eb_m0_clk     	: in std_logic;

  GBTCLK0_M2C_P 	: in std_logic;
  GBTCLK0_M2C_N 	: in std_logic;
  DP_M2C_P      	: in std_logic_vector(0 downto 0);
  DP_M2C_N      	: in std_logic_vector(0 downto 0);
-- synthesis translate_off
  DP_C2M_P      	: out std_logic_vector(0 downto 0);
  DP_C2M_N      	: out std_logic_vector(0 downto 0);
-- synthesis translate_on

  CLK_M2C_P     	: in std_logic_vector(1 downto 0);
  CLK_M2C_N     	: in std_logic_vector(1 downto 0);
  
   -- input
  LA_MON_P      	: in std_logic;
  LA_MON_N      	: in std_logic;
--  LA_SER0_P      	: in std_logic;
--  LA_SER0_N      	: in std_logic;
--  LA_SER1_P      	: in std_logic;
--  LA_SER1_N      	: in std_logic;
  LA_C2B_P      	: in std_logic;
  LA_C2B_N      	: in std_logic;
  LA_LOL_P      	: in std_logic;
  LA_LOL_N      	: in std_logic;
--  LA_TDO_P      : in std_logic;
--  LA_TDO_N      : in std_logic;
  LA_TDO_TO_ASIC_P      : out std_logic;
  LA_TDO_TO_ASIC_N     	: out std_logic;
  
 -- output
  LA_LED_P      	: out std_logic;
  LA_LED_N      	: out std_logic;
  -- ASIC
  LA_RUN_P      	: out std_logic;
  LA_RUN_N      	: out std_logic;
  LA_RES_P      	: out std_logic;
  LA_RES_N      	: out std_logic;
  -- PLL
  LA_PLL_RESET_P    	: out std_logic;
  LA_PLL_RESET_N    	: out std_logic;
  LA_CLK_P      	: out std_logic;
  LA_CLK_N      	: out std_logic;
  -- JTAG
--  LA_TDI_P      : out std_logic;
--  LA_TDI_N      : out std_logic;
  LA_TDI_FROM_ASIC_P   	: in std_logic;
  LA_TDI_FROM_ASIC_N   	: in std_logic;
  LA_TCK_P      	: out std_logic;
  LA_TCK_N      	: out std_logic;
  LA_TMS_P      	: out std_logic;
  LA_TMS_N      	: out std_logic;
 
  -- SUS JTAG FIFO WRITE TEST 
--  LA_FIFO_WRITE_P 	: out std_logic;
--  LA_FIFO_WRITE_N 	: out std_logic;
 
  -- SUS T3 Control & Telegram
  LA_TELEGRAM_CMD_TO_ASIC_P         : out std_logic;
  LA_TELEGRAM_CMD_TO_ASIC_N         : out std_logic;

  LA_T3_DATA_FROM_ASIC_P            : in std_logic; 
  LA_T3_DATA_FROM_ASIC_N            : in std_logic;




  -- SUS JTAG 
--  LA_SUS_TCK_P 	: out std_logic;
--  LA_SUS_TCK_N 	: out std_logic;
--  LA_SUS_TMS_P 	: out std_logic;
--  LA_SUS_TMS_N 	: out std_logic;
--  LA_SUS_TDO_P 	: out std_logic;
--  LA_SUS_TDO_N 	: out std_logic;
--  LA_SUS_TDI_P 	: out std_logic;
--  LA_SUS_TDI_N 	: out std_logic;


  GA            : in std_logic_vector(1 downto 0);
  PRSNT_M2C_L   : in std_logic;

  regs_o_s3     : in std32_a((REGS_NB / 2) - 1 downto 0);
  regs_i_s3     : out std32_a(REGS_NB - 1 downto (REGS_NB / 2));
--  sus_fifo_write			: in std_logic;
  sus_jtag_wrapper_tck            	: in std_logic;  
  sus_jtag_wrapper_tms                  : in std_logic;  
--  sus_jtag_wrapper_td_out_to_asic       : out std_logic;  
--  sus_jtag_wrapper_td_in_from_asic      : in std_logic  
  sus_jtag_wrapper_td_out_to_asic       : in std_logic;  
  sus_jtag_wrapper_td_in_from_asic      : out std_logic; 
--  regs_sus_dyn_rw			: in std32_a((REGS_NB / 2) - 1 downto 0);
  sus_dyn_asic_run			: in std_logic;                      
  sus_dyn_asic_res_n			: in std_logic;
  sus_dyn_asic_ser0_dout		: out std_logic;
  sus_dyn_asic_ser1_dout		: out std_logic;
  sus_dyn_asic_mon      		: out std_logic;
  sus_dyn_pll_clk               : out std_logic;

  sus_T3_telegram_cmd_to_asic           : in std_logic; 
  sus_T3_data_from_asic                 : out std_logic


);
end component;

end package dblock_dmak_pkg;
