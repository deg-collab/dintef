--------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.dblock_dmak_pkg.all;

library unisim;
use unisim.vcomponents.IBUFDS;
use unisim.vcomponents.OBUFDS;

entity dblock_dmak_dio is
port (
  TRIGIN_P       : in std_logic_vector(DIO_NB - 1 downto 0);
  TRIGIN_N       : in std_logic_vector(DIO_NB - 1 downto 0);
  TRIGIN_PBUTTON : in std_logic_vector(DIO_NB - 1 downto 0);
  TRIGIN_50OHM   : out std_logic_vector(DIO_NB - 1 downto 0);
  TRIGIN_LED     : out std_logic_vector(DIO_NB - 1 downto 0);
  TRIGOUT_P      : out std_logic_vector(DIO_NB - 1 downto 0);
  TRIGOUT_N      : out std_logic_vector(DIO_NB - 1 downto 0)
);

end dblock_dmak_dio;

------------------------------------------------------------------------
architecture dblock_dmak_dio_rtl of dblock_dmak_dio is
------------------------------------------------------------------------

signal TRIG_int : std1_a(DIO_NB - 1 downto 0);

------------------------------------------------------------------------
begin
------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
TRIG_IO_i : for i in DIO_NB - 1 downto 0 generate             -- LVDS In/Out buffers implementation
---------------------------------------------------------------------------------------------------

begin

  TRIGIN_LED(i) <= '0';
  TRIGIN_50OHM(i) <= '0';

  U0 : IBUFDS
  port map (
    O  => TRIG_int(i),
    I  => TRIGIN_P(i),
    IB => TRIGIN_N(i)
  );

  U1 : OBUFDS
  port map (
    O  => TRIGOUT_P(i),
    OB => TRIGOUT_N(i),
    I  => TRIG_int(i)
  );

---------------------------------------------------------------------------------------------------
end generate TRIG_IO_i;
---------------------------------------------------------------------------------------------------

end dblock_dmak_dio_rtl; 
