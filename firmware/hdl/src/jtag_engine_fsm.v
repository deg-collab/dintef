/**
 reg fsm_data_in_valid;
reg fsm_data_in_almost_empty;
reg fsm_continue;
reg fsm_data_done;
reg fsm_select_register;
reg fsm_force_res;
reg fsm_cmd_make_idle;
reg fsm_int_cmd;
reg fsm_enable;

wire fsm_dummy2;
wire fsm_dummy1;
wire fsm_dummy0;
wire fsm_data_in_shift_out;
wire fsm_store_cmd;
wire fsm_store_length1;
wire fsm_store_length2;
wire fsm_tms;
wire fsm_gen_clk;
wire fsm_send_instruction;
wire fsm_send_data;
wire fsm_serializer_clean_up;
wire fsm_trst;
wire fsm_idle;
wire fsm_dummy3;
wire fsm_store_data;

jtag_engine_fsm jtag_engine_fsm_I (

    .clk(), 
    .res_n(), 
	//-- Inputs
	.data_in_valid(fsm_data_in_valid), 
	.data_in_almost_empty(fsm_data_in_almost_empty), 
	.continue(fsm_continue), 
	.data_done(fsm_data_done), 
	.select_register(fsm_select_register), 
	.force_res(fsm_force_res), 
	.cmd_make_idle(fsm_cmd_make_idle), 
	.int_cmd(fsm_int_cmd), 
	.enable(fsm_enable), 

	//-- Outputs
	.dummy2(fsm_dummy2), 
	.dummy1(fsm_dummy1), 
	.dummy0(fsm_dummy0), 
	.data_in_shift_out(fsm_data_in_shift_out), 
	.store_cmd(fsm_store_cmd), 
	.store_length1(fsm_store_length1), 
	.store_length2(fsm_store_length2), 
	.tms(fsm_tms), 
	.gen_clk(fsm_gen_clk), 
	.send_instruction(fsm_send_instruction), 
	.send_data(fsm_send_data), 
	.serializer_clean_up(fsm_serializer_clean_up), 
	.trst(fsm_trst), 
	.idle(fsm_idle), 
	.dummy3(fsm_dummy3), 
	.store_data(fsm_store_data)
);

 */
//`include "/home/erdinger/chips/DSSC_F1/firmware/src/defines.v"
module jtag_engine_fsm ( 
    input wire clk, 
    input wire res_n, 

    // Inputs
    //------------ 
    input wire data_in_valid, 
    input wire data_in_almost_empty, 
    input wire continue, 
    input wire data_done, 
    input wire select_register, 
    input wire force_res, 
    input wire cmd_make_idle, 
    input wire int_cmd, 
    input wire enable, 

    // Outputs
    //------------ 
    output wire dummy2, 
    output wire dummy1, 
    output wire dummy0, 
    output wire data_in_shift_out, 
    output wire store_cmd, 
    output wire store_length1, 
    output wire store_length2, 
    output wire tms, 
    output wire gen_clk, 
    output wire send_instruction, 
    output wire send_data, 
    output wire serializer_clean_up, 
    output wire trst, 
    output wire idle, 
    output wire dummy3, 
    output wire store_data,

    output wire [15:0] o_current_state
 );

localparam idleState = 16'b0000000000000100;
localparam storeCmd = 16'b0001100000000000;
localparam storeL1 = 16'b0001010000000000;
localparam storeL2 = 16'b0001001000000000;
localparam mk_seldr = 16'b0000000110000000;
localparam mk_selir = 16'b0010000110000000;
localparam mk_capir = 16'b0010000010000000;
localparam mk_shir = 16'b0100000011000000;
localparam mk_ex1ir_stl1 = 16'b0110000110000000;
localparam mk_upir_stl2 = 16'b0100000110010000;
localparam mk_capdr = 16'b0100000010000000;
localparam mk_shdr = 16'b0110000010100000;
localparam mk_ex1dr = 16'b0100000110000001;
localparam mk_updr = 16'b0110000110010000;
localparam start = 16'b0000000110001000;
localparam mk_idle = 16'b0000000010000000;
localparam snd_inst = 16'b0000000011000000;
localparam snd_data = 16'b0000000010100001;
localparam wt_data4 = 16'b1000000000000000;
localparam wt_data1 = 16'b0010000000000000;
localparam wt_data2 = 16'b0100000000000000;
localparam rst1 = 16'b1110000110000000;
localparam rst2 = 16'b1000000110000000;
localparam rst3 = 16'b1010000110000000;
localparam rst4 = 16'b1100000110000000;
localparam rst5 = 16'b1110000110000010;
localparam wt_data5 = 16'b1010000000000000;
localparam wt_data6 = 16'b1100000000000000;
localparam wt_data3 = 16'b0110000000000000;

reg [15:0] current_state, next_state;
assign {dummy2, dummy1, dummy0, data_in_shift_out, store_cmd, store_length1, store_length2, tms, gen_clk, send_instruction, send_data, serializer_clean_up, trst, idle, dummy3, store_data} = current_state;
assign o_current_state = current_state;

wire [8:0] inputvector;
assign inputvector = {data_in_valid, data_in_almost_empty, continue, data_done, select_register, force_res, cmd_make_idle, int_cmd, enable};

assign curr_state = current_state;

always @(*) begin
  casex({inputvector, current_state})
    {9'bxxxxxxxx0, idleState},
    {9'b0xxxxxxxx, idleState}:   next_state = idleState;
    {9'b1xxxxxxx1, idleState}:   next_state = storeCmd;
    {9'bx0xxxx00x, storeCmd}:   next_state = storeL1;
    {9'bxxxxxx1xx, storeCmd}:   next_state = rst1;
    {9'bx1xxxx00x, storeCmd}:   next_state = wt_data1;
    {9'bxxxxxx01x, storeCmd}:   next_state = idleState;
    {9'bx0xxxxxxx, storeL1}:   next_state = storeL2;
    {9'bx1xxxxxxx, storeL1}:   next_state = wt_data2;
    {9'bxxxxxxxxx, storeL2}:   next_state = mk_seldr;
    {9'bxx0x1xxxx, mk_seldr},
    {9'bxx0xxxxxx, mk_seldr}:   next_state = mk_seldr;
    {9'bxx1x0xxxx, mk_seldr}:   next_state = mk_selir;
    {9'bxx1x1xxxx, mk_seldr}:   next_state = mk_capdr;
    {9'bxx0xxxxxx, mk_selir}:   next_state = mk_selir;
    {9'bxx1xxxxxx, mk_selir}:   next_state = mk_capir;
    {9'b0x0xxxxxx, mk_capir},
    {9'bxx0xxxxxx, mk_capir}:   next_state = mk_capir;
    {9'b1x1xxxxxx, mk_capir}:   next_state = mk_shir;
    {9'b0x1xxxxxx, mk_capir}:   next_state = wt_data5;
    {9'bxx0xxxxxx, mk_shir}:   next_state = mk_shir;
    {9'bxx1xxxxxx, mk_shir}:   next_state = snd_inst;
    {9'bxx0xxxxxx, mk_ex1ir_stl1}:   next_state = mk_ex1ir_stl1;
    {9'bxx1xxxxxx, mk_ex1ir_stl1}:   next_state = mk_upir_stl2;
    {9'b0x0xxxxxx, mk_upir_stl2},
    {9'bxx0xxxxxx, mk_upir_stl2}:   next_state = mk_upir_stl2;
    {9'b1x1xxxxxx, mk_upir_stl2}:   next_state = storeL1;
    {9'b0x1xxxxxx, mk_upir_stl2}:   next_state = wt_data1;
    {9'b0x0xxxxxx, mk_capdr},
    {9'bxx0xxxxxx, mk_capdr}:   next_state = mk_capdr;
    {9'b1x1xxxxxx, mk_capdr}:   next_state = mk_shdr;
    {9'b0x1xxxxxx, mk_capdr}:   next_state = wt_data3;
    {9'bxx0xxxxxx, mk_shdr}:   next_state = mk_shdr;
    {9'bxx1xxxxxx, mk_shdr}:   next_state = snd_data;
    {9'bxx0xxxxxx, mk_ex1dr}:   next_state = mk_ex1dr;
    {9'bxx1xxxxxx, mk_ex1dr}:   next_state = mk_updr;
    {9'bxx0xxxxxx, mk_updr}:   next_state = mk_updr;
    {9'bxx1xxxxxx, mk_updr}:   next_state = mk_idle;
    {9'bxx0xxxxxx, start}:   next_state = start;
    {9'bxx1xxxxxx, start}:   next_state = rst1;
    {9'bxx0xxxxxx, mk_idle}:   next_state = mk_idle;
    {9'bxx1xxxxxx, mk_idle}:   next_state = idleState;
    {9'bxx00xxxxx, snd_inst},
    {9'b1xx0xxxxx, snd_inst},
    {9'bxx0xxxxxx, snd_inst}:   next_state = snd_inst;
    {9'bxx11xxxxx, snd_inst}:   next_state = mk_ex1ir_stl1;
    {9'b0x10xxxxx, snd_inst}:   next_state = wt_data6;
    {9'bxx00xxxxx, snd_data},
    {9'b1xx0xxxxx, snd_data},
    {9'bxx0xxxxxx, snd_data}:   next_state = snd_data;
    {9'bxx11xxxxx, snd_data}:   next_state = mk_ex1dr;
    {9'b0x10xxxxx, snd_data}:   next_state = wt_data4;
    {9'b0xxxxxxxx, wt_data4}:   next_state = wt_data4;
    {9'b1xxxxxxxx, wt_data4}:   next_state = snd_data;
    {9'b0xxxxxxxx, wt_data1}:   next_state = wt_data1;
    {9'b1xxxxxxxx, wt_data1}:   next_state = storeL1;
    {9'b0xxxxxxxx, wt_data2}:   next_state = wt_data2;
    {9'b1xxxxxxxx, wt_data2}:   next_state = storeL2;
    {9'bxx0xxxxxx, rst1}:   next_state = rst1;
    {9'bxx1xxxxxx, rst1}:   next_state = rst2;
    {9'bxx0xxxxxx, rst2}:   next_state = rst2;
    {9'bxx1xxxxxx, rst2}:   next_state = rst3;
    {9'bxx0xxxxxx, rst3}:   next_state = rst3;
    {9'bxx1xxxxxx, rst3}:   next_state = rst4;
    {9'bxx0xxxxxx, rst4}:   next_state = rst4;
    {9'bxx1xxxxxx, rst4}:   next_state = rst5;
    {9'bxx0xxxxxx, rst5}:   next_state = rst5;
    {9'bxx1xxxxxx, rst5}:   next_state = mk_idle;
    {9'b0xxxxxxxx, wt_data5}:   next_state = wt_data5;
    {9'b1xxxxxxxx, wt_data5}:   next_state = mk_shir;
    {9'b0xxxxxxxx, wt_data6}:   next_state = wt_data6;
    {9'b1xxxxxxxx, wt_data6}:   next_state = snd_inst;
    {9'b0xxxxxxxx, wt_data3}:   next_state = wt_data3;
    {9'b1xxxxxxxx, wt_data3}:   next_state = mk_shdr;
    default:  next_state = start;
  endcase
end

`ifdef ASYNC_RES
 always @(posedge clk or negedge res_n ) begin
`else
 always @(posedge clk) begin
`endif
    if (!res_n)
    begin
        current_state <= start;
    end
    else begin
        current_state <= next_state;
    end
end

`ifdef CAG_COVERAGE
// synopsys translate_off

	// State coverage
	//--------

	//-- Coverage group declaration
	covergroup cg_states @(posedge clk);
		states : coverpoint current_state {
			bins idleState = {idleState};
			bins storeCmd = {storeCmd};
			bins storeL1 = {storeL1};
			bins storeL2 = {storeL2};
			bins mk_seldr = {mk_seldr};
			bins mk_selir = {mk_selir};
			bins mk_capir = {mk_capir};
			bins mk_shir = {mk_shir};
			bins mk_ex1ir_stl1 = {mk_ex1ir_stl1};
			bins mk_upir_stl2 = {mk_upir_stl2};
			bins mk_capdr = {mk_capdr};
			bins mk_shdr = {mk_shdr};
			bins mk_ex1dr = {mk_ex1dr};
			bins mk_updr = {mk_updr};
			bins start = {start};
			bins mk_idle = {mk_idle};
			bins snd_inst = {snd_inst};
			bins snd_data = {snd_data};
			bins wt_data4 = {wt_data4};
			bins wt_data1 = {wt_data1};
			bins wt_data2 = {wt_data2};
			bins rst1 = {rst1};
			bins rst2 = {rst2};
			bins rst3 = {rst3};
			bins rst4 = {rst4};
			bins rst5 = {rst5};
			bins wt_data5 = {wt_data5};
			bins wt_data6 = {wt_data6};
			bins wt_data3 = {wt_data3};
		}
	endgroup : cg_states

	//-- Coverage group instanciation
	cg_states state_cov_I;
	initial begin
		state_cov_I = new();
		state_cov_I.set_inst_name("state_cov_I");
	end

	// Transitions coverage
	//--------

	tc_stay_idle_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == idleState)|=> (current_state == idleState) );

	tc_store_command: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b1xxxxxxx1) &&(current_state == idleState)|=> (current_state == storeCmd));

	tc_store_length_LSB_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == storeCmd)|=> (current_state == storeL1) );

	tc_store_length_MSB_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == storeL1)|=> (current_state == storeL2) );

	tc_start_setting_instruction_register_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == storeL2)|=> (current_state == mk_seldr) );

	tc_go_to_SELECTIRSCAN: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1x0xxxx) &&(current_state == mk_seldr)|=> (current_state == mk_selir));

	tc_wait_for_clk_cycle_done_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == mk_seldr)|=> (current_state == mk_seldr) );

	tc_wait_for_clk_cycle_done2_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == mk_selir)|=> (current_state == mk_selir) );

	tc_go_to_CAPTUREIR: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == mk_selir)|=> (current_state == mk_capir));

	tc_wait_for_clk_cycle_done3_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == mk_capir)|=> (current_state == mk_capir) );

	tc_go_to_SHIFTIR: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b1x1xxxxxx) &&(current_state == mk_capir)|=> (current_state == mk_shir));

	tc_wait_for_clk_cycle_done4_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == mk_shir)|=> (current_state == mk_shir) );

	tc_send_last_data_go_to_EXIT1IR: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx11xxxxx) &&(current_state == snd_inst)|=> (current_state == mk_ex1ir_stl1));

	tc_wait_for_clk_cycle_done5_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == mk_ex1ir_stl1)|=> (current_state == mk_ex1ir_stl1) );

	tc_go_to_UPDATEIR: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == mk_ex1ir_stl1)|=> (current_state == mk_upir_stl2));

	tc_wait_for_clk_cycle_done6_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == mk_upir_stl2)|=> (current_state == mk_upir_stl2) );

	tc_go_to_SELECTDRSCAN: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b1x1xxxxxx) &&(current_state == mk_upir_stl2)|=> (current_state == storeL1));

	tc_go_to_CAPTUREDR: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1x1xxxx) &&(current_state == mk_seldr)|=> (current_state == mk_capdr));

	tc_wait_for_clk_cycle_done7_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == mk_capdr)|=> (current_state == mk_capdr) );

	tc_go_to_SHIFTDR: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b1x1xxxxxx) &&(current_state == mk_capdr)|=> (current_state == mk_shdr));

	tc_wait_for_clk_cycle_done8_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == mk_shdr)|=> (current_state == mk_shdr) );

	tc_send_last_data_go_to_EXIT1DR: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx11xxxxx) &&(current_state == snd_data)|=> (current_state == mk_ex1dr));

	tc_wait_for_clk_cycle_done9_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == mk_ex1dr)|=> (current_state == mk_ex1dr) );

	tc_go_to_UPDATEDR: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == mk_ex1dr)|=> (current_state == mk_updr));

	tc_wait_for_clk_cycle_done10_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == mk_updr)|=> (current_state == mk_updr) );

	tc_done_go_back_to_idle: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == mk_updr)|=> (current_state == mk_idle));

	tc_initial_reset: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == start)|=> (current_state == rst1));

	tc_wait_for_clk_cycle_done11_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == start)|=> (current_state == start) );

	tc_wait_for_clk_cycle_done12_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == mk_idle)|=> (current_state == mk_idle) );

	tc_wait_for_input: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == mk_idle)|=> (current_state == idleState));

	tc_send_instruction: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == mk_shir)|=> (current_state == snd_inst));

	tc_wait_for_instruction_sent_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == snd_inst)|=> (current_state == snd_inst) );

	tc_wait_for_clk_cycle_done13_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == snd_data)|=> (current_state == snd_data) );

	tc_send_data: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == mk_shdr)|=> (current_state == snd_data));

	tc_mk_reset: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxxxxxx1xx) &&(current_state == storeCmd)|=> (current_state == rst1));

	tc_wait_for_data_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == wt_data4)|=> (current_state == wt_data4) );

	tc_data_av: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b1xxxxxxxx) &&(current_state == wt_data4)|=> (current_state == snd_data));

	tc_no_data: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b0x10xxxxx) &&(current_state == snd_data)|=> (current_state == wt_data4));

	tc_data_in_almost_empty: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bx1xxxx00x) &&(current_state == storeCmd)|=> (current_state == wt_data1));

	tc_wait_for_data2_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == wt_data1)|=> (current_state == wt_data1) );

	tc_continue: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b1xxxxxxxx) &&(current_state == wt_data1)|=> (current_state == storeL1));

	tc_data_in_almost_empty2: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bx1xxxxxxx) &&(current_state == storeL1)|=> (current_state == wt_data2));

	tc_wait_for_data3_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == wt_data2)|=> (current_state == wt_data2) );

	tc_continue2: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b1xxxxxxxx) &&(current_state == wt_data2)|=> (current_state == storeL2));

	tc_wait_for_clk_cycle_done14_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == rst1)|=> (current_state == rst1) );

	tc_wait_for_clk_cycle_done15_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == rst2)|=> (current_state == rst2) );

	tc_wait_for_clk_cycle_done16_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == rst3)|=> (current_state == rst3) );

	tc_wait_for_clk_cycle_done17_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == rst4)|=> (current_state == rst4) );

	tc_wait_for_clk_cycle_done18_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == rst5)|=> (current_state == rst5) );

	tc_continue3: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == rst1)|=> (current_state == rst2));

	tc_continue4: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == rst2)|=> (current_state == rst3));

	tc_continue5: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == rst3)|=> (current_state == rst4));

	tc_continue6: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == rst4)|=> (current_state == rst5));

	tc_done: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxx1xxxxxx) &&(current_state == rst5)|=> (current_state == mk_idle));

	tc_int_cmd: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'bxxxxxx01x) &&(current_state == storeCmd)|=> (current_state == idleState));

	tc_no_data2: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b0x1xxxxxx) &&(current_state == mk_capir)|=> (current_state == wt_data5));

	tc_wait_for_data4_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == wt_data5)|=> (current_state == wt_data5) );

	tc_no_data3: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b0x1xxxxxx) &&(current_state == mk_capdr)|=> (current_state == wt_data3));

	tc_data_av2: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b1xxxxxxxx) &&(current_state == wt_data3)|=> (current_state == mk_shdr));

	tc_wait_for_data5_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == wt_data3)|=> (current_state == wt_data3) );

	tc_data_av3: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b1xxxxxxxx) &&(current_state == wt_data5)|=> (current_state == mk_shir));

	tc_data_av4: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b1xxxxxxxx) &&(current_state == wt_data6)|=> (current_state == snd_inst));

	tc_no_data4: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b0x10xxxxx) &&(current_state == snd_inst)|=> (current_state == wt_data6));

	tc_wait_for_data6_default: cover property( @(posedge clk) disable iff (!res_n) (current_state == wt_data6)|=> (current_state == wt_data6) );

	tc_no_data5: cover property( @(posedge clk) disable iff (!res_n)(inputvector ==? 9'b0x1xxxxxx) &&(current_state == mk_upir_stl2)|=> (current_state == wt_data1));

// synopsys translate_on
`endif


endmodule
