`timescale 1ns / 1ps
package Readout;

  typedef logic [63:0] Readout_data_t;

  typedef logic [63:0] FIFO_data_t;

  typedef struct packed {
    logic do_sync_link;
    logic enable_arm;
  } Readout_config_t;

  typedef struct {
    // 8b10b word clock
    logic CLK_31;
    logic CLK_62;
    // common multiple of word clock and coarse counter clock
    logic CLK_156;
    // 8b10b bit clock
    logic CLK_125;
    logic CLK_125_B;
    logic CLK_625;
    logic WORD_START;
  } clock_t;

  typedef struct packed {
    logic is_k;
    logic [7:0] value;
  } pattern_word_t;

  typedef struct packed {
    pattern_word_t [63:0] data;
    logic [5:0] pattern_length;
  } pattern_t;

  localparam pattern_word_t cSYNC_WORD = '{is_k : 1'b1, value : 8'h3C};
  localparam pattern_word_t cIDLE_WORD = '{is_k : 1'b1, value : 8'hBC};

endpackage : Readout

`default_nettype wire
