package readout_8b10b;

  typedef struct packed {
    logic is_k;
    logic is_valid;
    logic [7:0] value;
  } decoded_8b10b_t;

  localparam [7:0] cK28_1_value = 8'h3C;
  localparam [7:0] cK28_5_value = 8'hBC;

  typedef logic [9:0] encoded_8b10b_t;

endpackage : readout_8b10b
