`include "dev_if.v"

module DynamicAsicControl(
  input res_n,
  input clk,   // this must be the same as slow clock for the chip
  output       data_in_busy,        // full of internal FIFO
  input        data_in_write,
  input  [7:0] data_in,             // data from PC

  output        data_out_valid,
  output [15:0] data_out,
  //output [25:0] data_out,

  output asic_run,
  output asic_res,
  output init_jtag,
  input jtag_idle,
  output wire [5:0] o_current_state,
  output wire [5:0] o_next_state,
  output wire o_dev_in_fifo_empty,
  output wire cycle_done,


  input [11:0] conf_cycle_length,
  input [13:0] conf_num_words_to_readout,
  input asic_ser_dout,
  input conf_send_test_data
);

reg data_in_write_regged;
always @(posedge clk or negedge res_n) begin
  if (!res_n) begin
    data_in_write_regged = 1'b0;
  end else begin
    data_in_write_regged <= data_in_write;
  end
end

assign fifo_in_write = ~data_in_write_regged && data_in_write; 

wire [7:0] dev_in_fifo_dout;
AsyncDeviceFifo_65536 fifo_I(
  .wr_clk( clk ),
  .rd_clk( clk ),
  .rst( ~res_n ),
  .din( data_in ),
  .wr_en( fifo_in_write ),
  .rd_en( dev_in_fifo_rd_en ),
  .dout( dev_in_fifo_dout ),
  .full( data_in_busy ),
  .almost_full( ),
  .empty( dev_in_fifo_empty ),
  .almost_empty( dev_in_fifo_almost_empty )
);

// to prevent locks, no data sent from here... 
assign iom_data_valid = 1'b1;
assign iom_next_data_valid = 1'b1;

reg data_valid;
wire wait_done, data_start_detected, is_cmd_accept_state;

//typedef enum bit [7:0] {
localparam cASIC_RUN             = 8'd1;  // sIDLE -> sRESET(5clks) -> sINITJTAG(5clks) -> sSTART(serout 01) -> sDATA / sBLANK
localparam cASIC_STOP            = 8'd2;  // go back to idle from every state
localparam cASIC_RUN_RO          = 8'd3;  // same as cASIC_RUN, but conf_num_words_to_readout are sent out to output FIFO
localparam cASIC_RUN_RO_CONT     = 8'd4;  // same as cASIC_RUN_RO, but infinite words are sent to output FIFO (discarded if FIFO is full) 
//} tCmd;

//typedef enum bit [5:0] {
localparam sIDLE      = 6'b00_1000;  
localparam sRESET     = 6'b00_0001;  // resets ASIC
localparam sSTART     = 6'b00_0010;  // activates run command
localparam sINITJTAG  = 6'b00_0100;  // reset jtag engine to make asic jtag tap controller idle after reset
localparam sWAITJTAG  = 6'b10_0000;  // wait for jtag engine to become idle 
localparam sDATA      = 6'b01_0010;  // data valid
localparam sBLANK     = 6'b11_1010;  // data invalid
//} tState;
//tState current_state, next_state;

reg [5:0] current_state, next_state;
assign o_current_state = current_state;
assign o_next_state = next_state;
assign o_dev_in_fifo_empty = dev_in_fifo_empty;



assign asic_res            = current_state[0];
assign asic_run            = current_state[1];
assign init_jtag           = current_state[2];
assign is_cmd_accept_state = current_state[3];
assign en_cycle_cnt        = current_state[4];
assign cmd_asic_stop       = dev_in_fifo_dout == cASIC_STOP && !dev_in_fifo_empty;

wire [5:0] inputvector = {
  jtag_idle,
  data_valid,
  cmd_asic_stop,
  data_start_detected,
  wait_done,
  dev_in_fifo_empty
};

always @(*)
  casex({inputvector, current_state})
    {6'bxx_xxx0,sIDLE} : casex(dev_in_fifo_dout)
			                cASIC_RUN          : next_state = sRESET;
                            cASIC_RUN_RO       : next_state = sRESET;
                            cASIC_RUN_RO_CONT  : next_state = sRESET;
			                default            : next_state = sIDLE;
                       endcase
    {6'bxx_0x0x,sRESET}     : next_state = sRESET;
    {6'bxx_0x1x,sRESET}     : next_state = sINITJTAG;
    {6'b1x_0xxx,sINITJTAG}  : next_state = sINITJTAG;
    {6'b0x_0xxx,sINITJTAG}  : next_state = sWAITJTAG;

    // not going into start ... TODO: DEBUG
    //{6'b0x_0xxx,sWAITJTAG}  : next_state = sWAITJTAG;
    //{6'b1x_0xxx,sWAITJTAG}  : next_state = sSTART;
    {6'bxx_0xxx,sWAITJTAG}  : next_state = sSTART;

    {6'bxx_00xx,sSTART}     : next_state = sSTART;
    {6'bxx_01xx,sSTART}     : next_state = sDATA;
    {6'bx1_0xxx,sDATA}      : next_state = sDATA;
    {6'bx0_0xxx,sDATA}      : next_state = sBLANK;
    {6'bx0_0xxx,sBLANK}     : next_state = sBLANK;
    {6'bx1_0xxx,sBLANK}     : next_state = sDATA;
    default : next_state = sIDLE;
  endcase

assign dev_in_fifo_rd_en  = ( !dev_in_fifo_empty && is_cmd_accept_state) || cmd_asic_stop;

// state register
always @(posedge clk or negedge res_n)
  if (!res_n)
    current_state <= sIDLE;
  else
    current_state <= next_state;

reg [11:0] cycle_cnt;    // runs in sync with cycle_cnt in the chip but has an offset!
reg [2:0]  wait_cnt;     // wait length counter for reset / wait state
reg [15:0] data_in_sr;   // 4*(10+6) bit (4*(coarse + fine) cnts)
reg [5:0]  data_bit_cnt; // cnts the 64 data bits
reg [13:0] num_words_to_readout_remaining;
reg sending;
reg cont;


wire test_data_start;

assign test_data_start = (current_state == sSTART) ? conf_send_test_data : 1'b0;   // without this, the state machine is trapped in sSTART when sending test data, because there is no data start
assign wait_done = wait_cnt[2];
assign cycle_done    = cycle_cnt == 12'd1; // adjusted to run in phase with the asic cycle_done
assign res_cycle_cnt = cycle_cnt == 12'd1;
assign data_start_detected = (test_data_start || ({ asic_ser_dout, data_in_sr[15] } == 2'b10))
                             && current_state == sSTART;




wire pack_port_fifo_almost_empty;
assign package_part_valid = ~pack_port_fifo_almost_empty;
reg data_out_valid;

always @(posedge clk or negedge res_n) begin
  if (!res_n) begin
    cycle_cnt    <= 12'd0;
    wait_cnt     <= 3'd0;
    data_in_sr   <= 15'd0;
    data_bit_cnt <= 32'd1;
    data_valid   <= 1'b0;
    sending      <= 1'b0;
    num_words_to_readout_remaining <= 14'b0;
    data_out_valid <= 1'b0;
  end else begin
    wait_cnt <= wait_cnt[2] 
                  ? 3'b0 
                  : ( init_jtag || asic_res )
                    ? wait_cnt + 1'b1
                    : wait_cnt;
    cycle_cnt <= !asic_run || res_cycle_cnt || !en_cycle_cnt
                   ? conf_cycle_length                         
                   : cycle_cnt - 1;
    data_in_sr <= ( current_state == sSTART || cycle_done || data_valid ) 
                   ? {asic_ser_dout,data_in_sr[15:1]}
                   : data_in_sr;
    if ( data_start_detected || cycle_done ) begin
      data_valid <= 1'b1;
    end else if (data_bit_cnt == 6'd63) begin
      data_valid   <= 1'b0;
    end
    data_bit_cnt <= current_state == sSTART 
                      ? 5'd2
                      : data_valid 
                        ? data_bit_cnt + 1'b1
                        : 5'd1;                                   // FIX (David): Read out data was 1 bit too long which resulted in offset. Change from 5'd0 to 5'd1
    if ( ( current_state == sBLANK || current_state == sIDLE )
//         && ( dev_in_fifo_dout == cASIC_RUN_RO && !dev_in_fifo_empty ) && !sending ) begin
         && !dev_in_fifo_empty && !sending ) begin
      if(dev_in_fifo_dout == cASIC_RUN_RO) begin
        num_words_to_readout_remaining <= conf_num_words_to_readout;
        sending <= 1'b1;
        cont <= 1'b0;
      end else if(dev_in_fifo_dout == cASIC_RUN_RO_CONT) begin
        sending <= 1'b1;
        cont <= 1'b1;
      end
    end else if ( (num_words_to_readout_remaining == 14'd0) && (cont == 1'b0)) begin
      sending <= 1'b0;
      num_words_to_readout_remaining <= num_words_to_readout_remaining;
    end else if ( data_out_valid ) begin
      sending <= sending;
      if(cont == 1'b0) begin
        num_words_to_readout_remaining <= num_words_to_readout_remaining - 1'b1;
      end
    end
    data_out_valid <= ( sending && ( data_bit_cnt == 15 || data_bit_cnt == 31  || data_bit_cnt == 47 || data_bit_cnt == 63 ) ); // changed to sending out data valid for 4 words (4 FEs, instead of 2 as before)
  end
end

wire [5:0] fine;
wire [9:0] coarse;
//wire [25:0] data_out_int;
wire [15:0] data_out_int;

assign fine = data_in_sr[5:0];
assign coarse = data_in_sr[15:6];


// coarse counter data has to be delayed because system is pipelined. data in coarse counter
// corresponds to current integration window, data in fine counter to the one before. Thus,
// coarse counter has to be delayed by one cycle such that coarse and fine counter are from
// the same cycle

reg [9:0] coarse_delayed [3:0];
always@( posedge clk ) begin
    if( data_out_valid ) coarse_delayed <= {coarse_delayed[2:0], coarse};
    else coarse_delayed <= coarse_delayed;
end

assign data_out_int = {coarse_delayed[3], fine};

//assign data_out = conf_send_test_data ? {14'b0,num_words_to_readout_remaining} : data_in_sr;
assign data_out = conf_send_test_data ? {2'b0,num_words_to_readout_remaining} : data_out_int;

//DeviceOutFifo16to8 DeviceOutFifo16to8_PackPort_I(
//  .wr_clk(clk),
//  .rd_clk(if_clk),
//  .rst(~if_res_n),
//  .din(out_fifo_din),
//  .wr_en(pack_out_fifo_wr_en),
//  .rd_en(read_package_part),
//  .dout(package_data),
//  .full(out_fifo_full),
//  .almost_full(out_fifo_almost_full),
//  .empty(pack_port_fifo_empty),
//  .almost_empty(pack_port_fifo_almost_empty)
//);

endmodule
