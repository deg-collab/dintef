`include "dev_if.v"

module jtag_engine (
  input     wire            clk,
  input     wire            res_n,
  output    reg             busy,
  input     wire            enable,
  input     wire            ext_make_idle,
  output    wire            idle,
  output    wire [15:0]     o_current_state,

  `DEV_IF,
/*`ifdef DEBUG
   inout  [35:0] cmdProtIlaControl,
`endif*/
  output    wire            tck,
  output    wire            tms,
  output    wire            td_out_to_asic,
  input     wire            td_in_from_asic
);

// Data and control signals to the ASIC are changing on the falling edge of tck.
// Data from the ASIC (td_in_from_asic) is sampled on the rising edge of tck.

// NEW: There is a mechanism to set the JtagEngine busy.
// For the MM3 firmware, this feature is not used yet, the enable input of the
// jtag_engine is clamped high.
// For the F1, the enable of the jtag_engine is connected to the CmdProtocolEngine
// which must enable the JtagEngine through a special command. The CmdProtocolEngine
// then waits for the JtagEngine to finish. The JtagEngine is busy while it is not
// in the idle state, or if it is set permanently busy by a special command. This
// feature can be used to make it busy while programming several registers. If it
// is set busy by the special command it MUST also be set unbusy by a special command
// to move the CmdProtocolEngine back to idle, otherwise there is a deadlock.

`DEV_IN_FIFO_65536
`DEV_OUT_FIFO_65536

localparam cMAKE_IDLE  = 8'hFF;
localparam cSET_BUSY   = 8'h41;
localparam cUNSET_BUSY = 8'h40;

`define DIVIDE_CLK
localparam TCK_COUNTER_BITS = 4;

wire            fsm_gen_clk;
wire            fsm_continue;
wire            fsm_send_instruction;
wire            fsm_send_data;
reg             fsm_select_register;
wire            fsm_store_cmd;
wire            fsm_store_length1;
wire            fsm_store_length2;
wire            fsm_data_in_shift_out;
wire            fsm_data_done;
wire            fsm_trst;
wire            fsm_in_cmd_make_idle;
wire            fsm_in_int_cmd;
wire            fsm_store_data;
//wire            fsm_mk_reset;

wire            tck_rising;
wire            tck_falling;
reg             td_in_neg_clk;
wire            td_in_to_sample;
wire            td_in_from_asic_valid;
wire            enable_serializer;
wire            load_serializer;
wire            data_counter_reset;
reg     [2:0]   serializer_cnt;
reg     [2:0]   deserializer_cnt;
reg     [7:0]   serializer_sr;
reg     [7:0]   deserializer_reg;
reg     [7:0]   stored_cmd;
reg     [15:0]  stored_length;
reg     [15:0]  data_counter;
reg             dev_out_fifo_wr_en_reg;
//reg     [2:0]   reset_counter;

// ASIC sends data on negedge tck, data is sampled one clk cycle after negedge of tck 


// ============================== BEGIN TCK GENERATION ============================================
// Define DIVIDED_CLK if you want to divide clk for tck, if DIVIDED_CLK if not defined, clk is
// used directly for tck (through an ODDR2). tck is !clk to send all data and control signals on 
// the falling edge of tck because all signals are sampled on the rising edge in the ASIC. the tdo
`ifdef DIVIDE_CLK 

reg [TCK_COUNTER_BITS-1:0]   tck_counter;

always @ (posedge clk or negedge res_n)
  if (~res_n)
    tck_counter <= {TCK_COUNTER_BITS{1'b0}};
  else
    tck_counter <=  ((tck_counter == {TCK_COUNTER_BITS{1'b0}}) && ~fsm_gen_clk) ? tck_counter : tck_counter + 1'b1;

assign tck             = tck_counter[TCK_COUNTER_BITS-1];
assign tck_rising      = (tck_counter == (2**TCK_COUNTER_BITS-1) >> 1) && fsm_gen_clk;
assign tck_falling     = tck_counter == {TCK_COUNTER_BITS{1'b1}};
assign td_in_to_sample = td_in_from_asic;

`else


ODDR2 #(
  //.DDR_CLK_EDGE("OPPOSITE_EDGE"), // "OPPOSITE_EDGE" or "SAME_EDGE"
  .INIT(1'b1),       // Initial value of Q: 1'b0 or 1'b1
  .SRTYPE("ASYNC")   // Set/Reset type: "SYNC" or "ASYNC"
) TckODDR2_I(
  .Q(tck),      // 1-bit DDR output
  .C0(clk),          // 1-bit clock input
  .C1(~clk),         // 1-bit clock input
  .CE(fsm_gen_clk),  // 1-bit clock enable input
  .D0(1'b0),         // 1-bit data input (positive edge)
  .D1(1'b1),         // 1-bit data input (negative edge)
  .R(~res_n),        // 1-bit reset
  .S()               // 1-bit set
);

always @(negedge clk or negedge res_n)
  if (!res_n) 
    td_in_neg_clk <= 1'b0;
  else
    td_in_neg_clk <= td_in_from_asic;

assign tck_rising      = 1'b1;
assign tck_falling     = 1'b1;
assign td_in_to_sample = td_in_neg_clk;

`endif
// ================================ END TCK GENERATION ============================================

//TODO: in not divide mode data_counter_reset must be overwritten by fsm_data_done
assign data_counter_reset       = (data_counter==stored_length) && tck_falling;
assign fsm_continue             = /*fsm_mk_reset ? ((reset_counter==3'd4) && tck_falling) :*/ tck_falling;
//assign fsm_continue             = tck_falling;
assign fsm_in_cmd_make_idle     = ( (dev_in_fifo_dout == cMAKE_IDLE) && fsm_store_cmd );
assign fsm_in_int_cmd           = (dev_in_fifo_dout[6]) && fsm_store_cmd;
assign dev_in_fifo_rd_en        = fsm_data_in_shift_out || enable_serializer && (serializer_cnt==3'd6) || (fsm_data_done && serializer_cnt != 3'd7);
assign fsm_data_done            = (fsm_send_instruction || fsm_send_data) && data_counter_reset;
assign load_serializer          = (serializer_cnt == 3'b0);
assign enable_serializer        = (fsm_send_data || fsm_send_instruction) && fsm_continue;
assign td_in_from_asic_valid    = fsm_store_data && stored_cmd[7] && tck_rising;
assign td_out_to_asic           = serializer_sr[0];
assign dev_out_fifo_wr_en       = dev_out_fifo_wr_en_reg;
assign dev_out_fifo_din         = deserializer_reg;

always @(posedge clk or negedge res_n)
  if (!res_n) begin
    serializer_cnt         <= 3'b0;
    serializer_sr          <= 7'b0;
    dev_out_fifo_wr_en_reg <= 1'b0;
    data_counter           <= 16'b0;
    //reset_counter          <= 3'b0;
  end else begin
    serializer_cnt         <= data_counter_reset ? 3'b0  : enable_serializer ? serializer_cnt + 1'b1 : serializer_cnt;
    serializer_sr          <= enable_serializer ? (load_serializer ? dev_in_fifo_dout : {1'b0,serializer_sr[7:1]}) : serializer_sr;
    dev_out_fifo_wr_en_reg <= (serializer_cnt==3'd0) && td_in_from_asic_valid;
    data_counter           <= (data_counter_reset) ? 16'b0 : enable_serializer ? data_counter + 1'b1 : data_counter;
    //reset_counter          <= fsm_mk_reset ? tck_falling ? reset_counter + 1'b1 : reset_counter : 3'b0;
  end

// cannot use shift register because data is not aligned to bytes
always @(posedge clk or negedge res_n) 
  if (!res_n)
    deserializer_cnt <= 1'b0;
  else
    deserializer_cnt <= td_in_from_asic_valid ? !fsm_send_data ? 3'b0 : deserializer_cnt+1'b1 : deserializer_cnt;

genvar i;
generate
  for (i=0; i<8; i=i+1) begin : DeserializerReg
    always @(posedge clk) 
      deserializer_reg[i] <= (i==deserializer_cnt && td_in_from_asic_valid) ? td_in_to_sample : deserializer_reg[i];
  end
endgenerate

//Instantiating the JTAG engine FSM
jtag_engine_fsm jtag_engine_fsm_I ( 
  .clk(clk), 
  .res_n(res_n && !ext_make_idle), 
  .idle( idle ),
  .cmd_make_idle(fsm_in_cmd_make_idle),
  .int_cmd(fsm_in_int_cmd),
  .data_in_valid(~dev_in_fifo_empty), 
  .continue(fsm_continue), 
  .data_done(fsm_data_done), 
  .select_register(fsm_select_register), 
  .data_in_shift_out(fsm_data_in_shift_out),
  .store_cmd(fsm_store_cmd),
  .store_length1(fsm_store_length1),
  .store_length2(fsm_store_length2),
  .tms(tms),
  .trst(fsm_trst),
  .gen_clk(fsm_gen_clk), 
  .send_instruction(fsm_send_instruction), 
  .send_data(fsm_send_data),
  .data_in_almost_empty(dev_in_fifo_almost_empty),
  .enable(enable || busy),
  .store_data(fsm_store_data),
  //.make_reset(fsm_mk_reset),
  .force_res(1'b0),
  .o_current_state( o_current_state )
);


always @(posedge clk) begin
  if (!res_n) begin
    busy <= 1'b0;
  end else begin
    if (fsm_store_cmd && dev_in_fifo_dout==cSET_BUSY) begin
      busy <= 1'b1;
    end else if (fsm_store_cmd && dev_in_fifo_dout==cUNSET_BUSY) begin
      busy <= 1'b0;
    end else
      busy <= busy;
  end
end


//FSM helper logic
always @ (posedge clk or negedge res_n)
  if (~res_n)
    fsm_select_register <= 1'b0;
  else
    fsm_select_register <= (fsm_store_cmd) ? dev_in_fifo_dout[6] : (fsm_send_instruction) ? 1'b1 : (fsm_send_data) ? 1'b0 : fsm_select_register;
  

//storing logic for instruction / command and length
always @ (posedge clk or negedge res_n)
  if (~res_n) begin
    stored_cmd          <= 8'b0;
    stored_length       <= 16'b0;
  end else begin
    stored_cmd          <= (fsm_store_cmd) ? dev_in_fifo_dout: stored_cmd;
    stored_length[7:0]  <= (fsm_store_length1) ? dev_in_fifo_dout : stored_length[7:0];
    stored_length[15:8] <= (fsm_store_length2) ? dev_in_fifo_dout : stored_length[15:8];
  end
/*
`ifdef DEBUG
`ifdef SIMULATION
`else

wire [127:0] ila_trigger;
assign ila_trigger[46+:8]   = deserializer_reg;
assign ila_trigger[38+:8]   = stored_cmd;
assign ila_trigger[37]      = td_in_from_asic_valid;
assign ila_trigger[36]      = fsm_store_data;
assign ila_trigger[35]      = fsm_in_int_cmd;
assign ila_trigger[34]      = fsm_in_cmd_make_idle;
assign ila_trigger[33]      = fsm_trst;
assign ila_trigger[32]      = fsm_data_done;
assign ila_trigger[31]      = fsm_data_in_shift_out;
assign ila_trigger[30]      = fsm_store_length2;
assign ila_trigger[29]      = fsm_store_length1;
assign ila_trigger[28]      = fsm_store_cmd;
assign ila_trigger[27]      = fsm_select_register;
assign ila_trigger[26]      = fsm_send_data;
assign ila_trigger[25]      = fsm_send_instruction;
assign ila_trigger[24]      = fsm_continue;
assign ila_trigger[23]      = fsm_gen_clk;
assign ila_trigger[22]      = td_out_to_asic;
assign ila_trigger[21]      = tms;
assign ila_trigger[20]      = td_in_from_asic;
assign ila_trigger[19]      = dev_out_fifo_wr_en;
assign ila_trigger[18]      = dev_out_fifo_full;
assign ila_trigger[10+:8]   = dev_out_fifo_din;
assign ila_trigger[9]       = dev_in_fifo_rd_en;
assign ila_trigger[8]       = dev_in_fifo_rd_en;
assign ila_trigger[ 0+:8]   = dev_in_fifo_dout;


reg [127:0] ila_trigger_reg;
always @(posedge clk) ila_trigger_reg <= ila_trigger;
ILA ILA_I(
  .CONTROL(cmdProtIlaControl),
  .CLK(clk),
  .TRIG0(ila_trigger_reg[127:0])
);

`endif
`endif // SIMULATION
*/

endmodule
