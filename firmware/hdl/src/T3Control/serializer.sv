`default_nettype none
import Readout::pattern_word_t;
module serializer (
    input wire CLK,
    input wire RESET,
    input wire WORD_CLK,
    input wire WORD_RESET,
    input wire WORD_START,
    input wire ENABLE,

    output logic SEROUT,

    input wire pattern_word_t BYTE_IN
);
  timeunit 1ns; timeprecision 10ps;

  pattern_word_t data_sampled;

  logic [9:0] ebtb_out;
  logic [9:0] shifter;
  // the XAPP1122 encoder return the value such that it must be transmitted
  // MSB first.
  always_comb SEROUT = shifter[9];

  encode_8b10b_wrapper enc_I (
      .CLK (WORD_CLK),
      .DIN (data_sampled.value),
      .KIN (data_sampled.is_k),
      .DOUT(ebtb_out)
  );

  always_ff @(posedge WORD_CLK) begin : provide_word
    data_sampled <= BYTE_IN;
  end

  always_ff @(posedge CLK or posedge RESET) begin : main
    if (RESET) begin
      shifter <= 10'bXXXXXXXXX0;
    end else begin
      if (WORD_START) begin
        shifter <= ebtb_out;
      end else begin
        shifter <= {shifter[8:0], 1'bX};
      end
    end
  end

endmodule
`default_nettype wire
