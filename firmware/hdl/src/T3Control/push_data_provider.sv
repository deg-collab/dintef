`default_nettype none
import Readout::pattern_t;
import Readout::pattern_word_t;

module push_data_provider (
    input wire WORD_CLK,
    input wire WORD_RESET,

    input wire ENABLE,

    input wire pattern_t DATA_IN,
    output pattern_word_t DATA_OUT,
    output bit [15:0] SENT_COUNT,
    input wire RESET_STATS
);
  timeunit 1ns; timeprecision 10ps;

  import Readout::cIDLE_WORD;

  bit [5:0] current_word;

  always_ff @(posedge WORD_CLK) begin
    if (current_word >= DATA_IN.pattern_length - 1) begin
      current_word <= 6'd0;
      if (!RESET_STATS) SENT_COUNT <= SENT_COUNT + 1'd1;
    end else current_word <= current_word + 6'd1;

    if (RESET_STATS) SENT_COUNT <= 16'd0;
  end

  always_comb DATA_OUT = ENABLE ? DATA_IN.data[current_word] : cIDLE_WORD;
endmodule
`default_nettype wire
