`default_nettype none

import Readout::clock_t;
import Readout::pattern_t;

module telegram_sender (
    input clock_t CLOCK,
    input wire RESET,
    input wire pattern_t PATTERN,
    output bit T3_CMD_IN,
    output bit [15:0] SENT_COUNT,
    input wire RESET_STATS
);
  timeunit 1ns; timeprecision 10ps;

  import Readout::*;

  pattern_word_t ser_word;

  push_data_provider provider_I (
      .WORD_CLK(CLOCK.CLK_31),
      .WORD_RESET(RESET),
      .ENABLE(1'b1),
      .DATA_IN(PATTERN),
      .DATA_OUT(ser_word),
      .SENT_COUNT,
      .RESET_STATS
  );

  // command telegram to ASIC
  serializer ser_I (
      .CLK(CLOCK.CLK_125),
      .RESET(RESET),
      .WORD_CLK(CLOCK.CLK_31),
      .WORD_RESET(RESET),
      .WORD_START(CLOCK.WORD_START),
      .ENABLE(1'b1),
      .SEROUT(T3_CMD_IN),
      .BYTE_IN(ser_word)
  );

endmodule : telegram_sender
