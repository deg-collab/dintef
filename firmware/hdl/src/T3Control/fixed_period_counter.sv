// a timer that flags when the specified number of cycles is elapsed.
// it is automatically reset to 0 when counting is stopped.
// this is a periodic timer, i.e. IS_ELAPSED is high only for one clock cycle,
// then the counter is reset to 0 if not DO_RUN, or 1 if DO_RUN is active.
`default_nettype none
module fixed_period_counter #(
    parameter int P_END = 16
) (
    input  wire CLK,
    input  wire RESET,
    input  wire DO_RUN,
    output reg  IS_ELAPSED
);
  timeunit 1ns; timeprecision 10ps;

  localparam int lp_width = $clog2(P_END);
  localparam int lp_next_width = $clog2(P_END + 1);

  logic [lp_width-1:0] count;
  logic [lp_next_width-1:0] next_count;

  always_comb next_count = count + 1'd1;

  always_ff @(posedge CLK or posedge RESET) begin : main
    if (RESET) begin
      count <= {lp_width{1'b0}};
      IS_ELAPSED <= 1'b0;
    end else begin
      if (DO_RUN) begin
        if (count == P_END - 1) begin
          count <= {lp_width{1'b0}};
        end else begin
          // we know that this won't truncate.
          count <= next_count[lp_width-1:0];
        end
        IS_ELAPSED <= next_count == P_END;
      end else begin
        count <= {lp_width{1'b0}};
        IS_ELAPSED <= 1'b0;
      end
    end
  end : main
endmodule : fixed_period_counter
`default_nettype wire
