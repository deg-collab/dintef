// we do require a certain number of good received 8b10b codes, i.e. at least
// a really minimal guarantee on the bit error rate.
`default_nettype none
import readout_8b10b::*;

module phase_finder (
    input wire WORD_CLK,
    input wire WORD_RESET,

    // while this is high, we expect to receive only K28.5 characters!
    input  wire  DO_INIT_LINK,
    output logic LINK_IS_STABLE,

    input wire decoded_8b10b_t DECODED_IN,

    output logic [2:0] BIT_PHASE,
    output logic SHIFT_WORDS,
    
    output wire [3:0] current_state 

);
  timeunit 1ns; timeprecision 10ps;

  typedef enum logic [3:0] {
    //                           v DO_BIT_SKIP
    //                           |v LINK_IS_STABLE
    //                           ||v clear_counters
    ChangeAlignment = 4'b1010,
    Sample          = 4'b0000,
    Stable          = 4'b0100,
    StableClear     = 4'b0110,
    Running         = 4'b0111,
    Wait            = 4'b0011
  } state_t;

  state_t current_state;
  wire do_bit_skip = current_state[3];
  always_comb LINK_IS_STABLE = current_state[2];
  wire clear_counters = current_state[1];

  logic [1:0] wait_counter;
  logic good_received, bad_received;
  logic good_overflow, bad_overflow;

  logic [3:0] current_alignment;

  saturating_counter #(
      .P_WIDTH(5)
  ) good_counter_I (
      .CLK(WORD_CLK),
      .RESET(WORD_RESET),
      .ENABLE(good_received),
      .CLEAR(clear_counters),
      .COUNT(),
      .OVERFLOW(good_overflow)
  );

  saturating_counter #(
      .P_WIDTH(2)
  ) bad_counter_I (
      .CLK(WORD_CLK),
      .RESET(WORD_RESET),
      .ENABLE(bad_received),
      .CLEAR(clear_counters),
      .COUNT(),
      .OVERFLOW(bad_overflow)
  );

  always_comb SHIFT_WORDS = (current_alignment >= 4'd5);
  always_comb BIT_PHASE = (current_alignment % 3'd5);

  always_ff @(posedge WORD_CLK or posedge WORD_RESET) begin
    if (WORD_RESET) begin
      // wait for DO_INIT_LINK
      current_state <= Running;
      wait_counter <= 2'd0;
      current_alignment <= 4'd0;
    end else begin
      unique case (current_state)
        Wait: begin
          // a bit of time to settle
          wait_counter <= wait_counter + 1'd1;
          if (wait_counter == 2'd3) begin
            current_state <= Sample;
          end
        end
        Sample: begin
          if (good_overflow) current_state <= Stable;
          else if (bad_overflow) begin
            current_state <= ChangeAlignment;
          end
          wait_counter <= 2'd0;
        end
        ChangeAlignment: begin
          current_state <= Wait;
          if (current_alignment == 4'd9) begin
            current_alignment <= 4'd0;
          end else begin
            current_alignment <= current_alignment + 1'd1;
          end
        end
        Stable: begin
          if (!DO_INIT_LINK) current_state <= Running;
          if (bad_overflow) current_state <= Running;
          if (good_overflow) current_state <= StableClear;
        end
        StableClear: begin
          current_state <= Stable;
        end
        Running: begin
          if (DO_INIT_LINK) current_state <= Sample;
        end
      endcase
    end
  end

  always_comb begin
    // invalid char is always bad
    if (!DECODED_IN.is_valid) begin
      bad_received  = 1'b1;
      good_received = 1'b0;
    end else if (current_state == Sample) begin
      // in the sample state, we are strict, i.e. we expect only K28.5
      if (!DECODED_IN.is_k) begin
        bad_received  = 1'b1;
        good_received = 1'b0;
      end else if (DECODED_IN.value == cK28_5_value) begin
        bad_received  = 1'b0;
        good_received = 1'b1;
      end else begin
        bad_received  = 1'b1;
        good_received = 1'b0;
      end
    end else begin
      // in the stable state, we accept any good character
      bad_received  = 1'b0;
      good_received = 1'b1;
    end
  end

endmodule : phase_finder
`default_nettype wire
