`default_nettype none

import Readout::*;
import readout_8b10b::*;

module push_receiver #(
    parameter int P_FIFO_WIDTH = 32,
    parameter bit [P_FIFO_WIDTH-1:0] P_SYNC_WORD = 32'h01234567
) (
    input clock_t CLOCK,
    input wire RESET,

    input wire ENABLE,
    input wire DO_INIT_LINK,

    input wire SEROUT,
    output logic WORD_SYNCED,
    output bit [P_FIFO_WIDTH-1:0] FIFO_WDATA,
    //output logic FIFO_WEN,
    output reg FIFO_WEN,
    input wire FIFO_FREE,

    output wire [3:0] pf_current_state
);

  timeunit 1ns; timeprecision 10ps;

  decoded_8b10b_t decoded_in;
  //logic frame_available;
  wire frame_available;
  reg FIFO_WEN_int;
  reg [7:0] hit_count = 8'd0;
  logic [2:0] bit_phase;
  logic shift_words;

  phase_finder pf_I (
      .WORD_CLK  (CLOCK.CLK_31),
      .WORD_RESET(RESET),

      .DO_INIT_LINK,
      .LINK_IS_STABLE(WORD_SYNCED),

      .DECODED_IN(decoded_in),

      .BIT_PHASE  (bit_phase),
      .SHIFT_WORDS(shift_words),
  
      .current_state(pf_current_state)
  
  );

  input_framer_bit_skip framer_I (
      .CLOCK,
      .RESET,
      .WORD_RESET(RESET),

      .ENABLE,

      .UPLINK_SERIN(SEROUT),
      .BIT_PHASE(bit_phase),
      .SHIFT_WORDS(shift_words),
      // TODO: automatically select the better edge
      .SELECT_EDGE(1'b0),  // try 1'b1, if the link is unstable

      .DECODED_IN(decoded_in),

      .BAD_WORD_COUNT(),
      .BAD_WORD_COUNT_RESET(1'b0)
  );

  frame_receiver #(
      .P_BYTES(P_FIFO_WIDTH / 8),
      .P_SYNC_WORD(P_SYNC_WORD)
  ) recv_I (
      .WORD_CLK(CLOCK.CLK_31),
      .WORD_RESET(RESET),
      .ENABLE(ENABLE && !DO_INIT_LINK),
      .FRAME_OUT(FIFO_WDATA),
      .FRAME_AVAILABLE(frame_available),
      .DECODED_IN(decoded_in)
  );

  always_ff @(posedge CLOCK.CLK_31) begin
      FIFO_WEN_int <= frame_available;
  end
  assign FIFO_WEN = FIFO_WEN_int && FIFO_FREE;

endmodule : push_receiver
`default_nettype wire
