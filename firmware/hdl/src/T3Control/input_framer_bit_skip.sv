`default_nettype none
import readout_8b10b::*;
import Readout::clock_t;

module input_framer_bit_skip (
    input clock_t CLOCK,
    input wire RESET,
    input wire WORD_RESET,

    input wire ENABLE,

    // DATA INPUT
    // 8b10b bitstream received by the ASIC.
    input wire UPLINK_SERIN,
    // in the WORD_CLK domain
    input wire [2:0] BIT_PHASE,
    input wire SHIFT_WORDS,
    input wire SELECT_EDGE,

    // DATA OUTPUT
    output decoded_8b10b_t DECODED_IN,  // in CLK10 domain

    output wire [3:0] BAD_WORD_COUNT,
    input wire BAD_WORD_COUNT_RESET
);
  timeunit 1ns; timeprecision 10ps;

  logic [14:0] current_word[2];
  encoded_8b10b_t latched_word;
  encoded_8b10b_t latched_word_31;

  logic code_err, disp_err;

  logic [4:0] uplink_ddr[2];
  reg second_word = 1'b0;

  DDRReceiver receiver_I (
      .CLOCK,
      .RESET,
      .ENABLE,
      .BIT_PHASE,
      .UPLINK_SERIN,
      .UPLINK_DDR(uplink_ddr)
  );

  decode_8b10b_wrapper wrap_I (
      .CLK(CLOCK.CLK_31),
      .CE(ENABLE),
      .DIN({<<{latched_word_31}}),
      .DOUT(DECODED_IN.value),
      .KOUT(DECODED_IN.is_k),
      .CODE_ERR(code_err),
      .DISP_ERR(disp_err)
  );
  always_comb DECODED_IN.is_valid = !(code_err || disp_err);

  saturating_counter #(
      .P_WIDTH(4)
  ) bad_count_I (
      .CLK(CLOCK.CLK_31),
      .RESET(WORD_RESET),
      .CLEAR(BAD_WORD_COUNT_RESET),
      .ENABLE(!DECODED_IN.is_valid),
      .COUNT(BAD_WORD_COUNT),
      .OVERFLOW()
  );

  always @(posedge CLOCK.CLK_62) begin
    current_word[0] <= {uplink_ddr[0], current_word[0][14:5]};
    current_word[1] <= {uplink_ddr[1], current_word[1][14:5]};

    second_word <= ENABLE && (second_word ^ ENABLE);

    if (second_word) begin
      if (SHIFT_WORDS) begin
        latched_word <= current_word[SELECT_EDGE][5+:10];
      end else begin
        latched_word <= current_word[SELECT_EDGE][0+:10];
      end
    end
  end

  always_ff @(posedge CLOCK.CLK_31) begin
    latched_word_31 <= latched_word;
  end

endmodule : input_framer_bit_skip
`default_nettype wire

