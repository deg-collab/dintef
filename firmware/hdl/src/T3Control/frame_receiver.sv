`default_nettype none

import readout_8b10b::*;

// build a multi-byte frame from 8b10b words.
module frame_receiver #(
    parameter int P_BYTES = 4,
    parameter bit P_LEFT_TO_RIGHT = 1'b0,
    parameter decoded_8b10b_t P_SYNC_PATTERN = '{ is_k : 1'b1, is_valid : 1'b1, value : cK28_1_value },
    parameter bit [8*P_BYTES-1:0] P_SYNC_WORD = 32'h01234567
) (
    input wire WORD_CLK,
    input wire WORD_RESET,

    input wire ENABLE,

    input wire decoded_8b10b_t DECODED_IN,
    output reg [8 * P_BYTES - 1:0] FRAME_OUT,
    output reg FRAME_AVAILABLE
);
  timeunit 1ns; timeprecision 10ps;

  reg [8 * P_BYTES - 1:0] active_word;
  //logic word_complete;
  wire word_complete;
  // bit sync_received;
  reg sync_received;

  fixed_period_counter #(
      .P_END(P_BYTES)
  ) byte_counter_I (
      .CLK(WORD_CLK),
      .RESET(WORD_RESET),
      .DO_RUN(!DECODED_IN.is_k && DECODED_IN.is_valid),
      .IS_ELAPSED(word_complete)
  );

  always_ff @(posedge WORD_CLK or posedge WORD_RESET) begin : main
    if (WORD_RESET) begin
      sync_received <= 1'b0;
      FRAME_AVAILABLE <= 1'b0;
      FRAME_OUT <= {8 * P_BYTES{1'b0}};
      active_word <= {8 * P_BYTES{1'b0}};
    end else begin
      if (ENABLE) begin
        sync_received <= (DECODED_IN == P_SYNC_PATTERN);

        if (!P_LEFT_TO_RIGHT) begin
          active_word <= {active_word[P_BYTES*8-9:0], DECODED_IN.value};
        end else begin
          active_word <= {DECODED_IN.value, active_word[P_BYTES*8-1:8]};
        end

        FRAME_AVAILABLE <= (word_complete || sync_received);
        if (word_complete) begin
          FRAME_OUT <= active_word;
        end else if (sync_received) begin
          FRAME_OUT <= P_SYNC_WORD;
        end
      end
    end
  end : main
endmodule : frame_receiver

`default_nettype wire
