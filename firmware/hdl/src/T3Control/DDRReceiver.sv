// 10 bit DDR receiver after delay
`default_nettype none

import Readout::clock_t;

module DDRReceiver (
    input clock_t CLOCK,
    input wire RESET,
    input wire ENABLE,
    input wire [2:0] BIT_PHASE,
    input wire UPLINK_SERIN,
    // five bits per edge
    output logic [4:0] UPLINK_DDR[2]  // in CLK_62 domain
);
  timeunit 1ns; timeprecision 10ps;

  logic q[2];

  IDDR #(
      .DDR_CLK_EDGE("OPPOSITE_EDGE")
  ) io_I (
      .D (UPLINK_SERIN),
      .CE(ENABLE),
      .C (CLOCK.CLK_125),
      .S (1'b0),
      .R (1'b0),
      .Q1(q[0]),
      .Q2(q[1])
  );
  reg [2:0] count = 3'd0;
  // 5 bits required + 4 alternative phases
  logic [4+4:0] shifter[2];
  logic [4:0] uplink_fast[2];

  always @(posedge CLOCK.CLK_125 or posedge RESET) begin
    if (RESET) begin
      count <= 3'd0;
    end else begin
      if (count == 3'd4) begin
        count <= 3'd0;
      end else begin
        count <= count + 1'd1;
      end
    end
  end

  generate
    for (genvar i = 0; i < 2; i = i + 1) begin
      always @(posedge CLOCK.CLK_125) begin
        shifter[i][8:0] <= {q[i], shifter[i][8:1]};
        if (count == 3'd4) begin
          uplink_fast[i] <= shifter[i][BIT_PHASE+:5];
        end
      end
    end
  endgenerate

  always @(posedge CLOCK.CLK_62) begin
    UPLINK_DDR <= uplink_fast;
  end

endmodule : DDRReceiver
`default_nettype wire
