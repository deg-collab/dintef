`default_nettype none
import Readout::pattern_t;

// unit suitable for simulation:
// simple interfaces, no JTAG regs, LVDS receivers, etc.
module T3Controller #(
    parameter int P_FIFO_WIDTH = 64,
    //parameter int P_FIFO_WIDTH = 32,
    parameter bit [P_FIFO_WIDTH-1:0] P_SYNC_WORD = 64'h01234567
) (
    input wire CLK_125,
    input wire RESET,

    // signals to/from SUS65T3
    output bit  T3_CMD_IN,
    input  wire T3_DATA_OUT,

    // control
    // input wire pattern_t PATTERN,
    input wire [581:0] PATTERN,

    // data
//    output bit FIFO_WCLK,
//    output bit FIFO_WCLK,
    output wire FIFO_WCLK,
    input wire FIFO_FREE,
    output wire FIFO_WEN,
    output bit [P_FIFO_WIDTH-1 : 0] FIFO_WDATA,

    input  wire DO_INIT_LINK,
    output bit  LINK_DETECTED,


    input wire READOUT_SYNC_WORD,
    output wire [3:0] pf_current_state,
    output wire clockgen_locked




);
  timeunit 1ns; timeprecision 10ps;

  import Readout::*;

  clock_t clock;

  ClockGen clk_I (
      .CLK_125(CLK_125),
      .REBOOT(1'b0),
      .CLOCK(clock),
      .ALL_LOCKED(clockgen_locked),
      .CONFIG_DONE()
  );

  //always_comb FIFO_WCLK = clock.CLK_31;
  assign FIFO_WCLK = clock.CLK_31;

  wire FIFO_WEN_int;
  assign FIFO_WEN = READOUT_SYNC_WORD ? FIFO_WEN_int : (FIFO_WDATA == P_SYNC_WORD) ? 0 : FIFO_WEN_int;

  // upstream data from ASIC
  push_receiver #(
      .P_FIFO_WIDTH(P_FIFO_WIDTH),
      .P_SYNC_WORD (P_SYNC_WORD)
  ) recv_I (
      .CLOCK(clock),
      .RESET(RESET),

      .ENABLE(1'b1),
      .DO_INIT_LINK,

      .SEROUT(T3_DATA_OUT),
      .WORD_SYNCED(LINK_DETECTED),
      .FIFO_WDATA,
      .FIFO_WEN(FIFO_WEN_int),
      .FIFO_FREE,
      
      .pf_current_state(pf_current_state)
  
  );

  telegram_sender telegram_I (
      .CLOCK(clock),
      .RESET,

      .PATTERN,
      .T3_CMD_IN
  );

endmodule : T3Controller
`default_nettype wire
