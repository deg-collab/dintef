-------------------------------------------------------------
--
-- File        : dblock_dmak_user.vhd
-- Description : User data flow emulator
--
-------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

use work.ebs_pkg.all;
use work.dblock_dmak_pkg.all;

entity dblock_dmak_user is

generic (
  EBFT_DWIDTH    : NATURAL := 64
);
port (
-- System clock and reset
  eb_sys_rst     : in std_logic;
  eb_sys_clk     : in std_logic;

-- Configuration/Status registers
  count_reg_on   : in std_logic;
  count_reg      : in std32;
  v_lsb_reg_on   : in std_logic;
  v_lsb_reg      : in std32;
  v_msb_reg_on   : in std_logic;
  v_msb_reg      : in std32;
  cntrl_reg_on   : in std_logic;
  cntrl_reg      : in std32;
  wr_counter_o   : out std_logic_vector(Nlog2(FIFO_SIZE / 8) - 1 downto 0);
  loop_counter_o : out std16;

-- User FIFO access
  user_wr        : in std16;
  user_full      : in std_logic;
  user_afull     : in std_logic;
  user_clk       : out std_logic;
  user_en        : out std_logic;
  user_data      : out std_logic_vector(EBFT_DWIDTH - 1 downto 0)
);
end dblock_dmak_user;

------------------------------------------------------------------------
architecture dblock_dmak_user_rtl of dblock_dmak_user is
------------------------------------------------------------------------

------------------------
component mmcm_user_v6_0
------------------------
port (
  reset    : in std_logic;
  clk_in1  : in std_logic;
  clk_out1 : out std_logic;
  locked   : out std_logic
);
end component;

constant user_hfull  : INTEGER := (FIFO_SIZE / 16);

type user_type       is (USER_INIT, USER_ACK, USER_DAT, USER_ENL, USER_CMP, USER_WAIT, USER_END);
signal user_state    : user_type;

type strt_type       is (STRT_INIT, STRT_ACK, STRT_END);
signal strt_state    : strt_type;

signal user_data_int : std_logic_vector(EBFT_DWIDTH - 1 downto 0);
signal user_count    : std32;
signal user_v_lsb    : std32;
signal user_v_msb    : std32;
signal user_cntrl    : std32;
signal user_send     : std_logic;
signal user_ackn     : std_logic;
signal user_clk_int  : std_logic;
signal user_lck_int  : std_logic;
signal wr_counter    : std_logic_vector(Nlog2(FIFO_SIZE / 8) - 1 downto 0);
signal loop_counter  : std16;
signal count_reg_wr  : std_logic;
signal v_lsb_reg_wr  : std_logic;
signal v_msb_reg_wr  : std_logic;
signal cntrl_reg_wr  : std_logic;

signal user_start_s  : std_logic;
signal user_start_v  : std_logic_vector(2 downto 0);

alias user_start     : std_logic is user_cntrl(0);
alias user_stop      : std_logic is user_cntrl(1);

-----
begin
-----

mmcm_user_i : mmcm_user_v6_0
----------------------------
port map (
  reset    => eb_sys_rst,
  clk_in1  => eb_sys_clk,
  clk_out1 => user_clk_int,
  locked   => user_lck_int
);

user_clk <= user_clk_int;
user_data <= user_data_int;

process begin
  wait until rising_edge(eb_sys_clk);
    if (eb_sys_rst = '1') then
      count_reg_wr <= '0';
      v_lsb_reg_wr <= '0';
      v_msb_reg_wr <= '0';
      cntrl_reg_wr <= '0';
      user_count   <= (others => '0');
      user_v_lsb   <= (others => '0');
      user_v_msb   <= (others => '0');
      user_cntrl   <= (others => '0');
    else
      count_reg_wr <= count_reg_on;
      v_lsb_reg_wr <= v_lsb_reg_on;
      v_msb_reg_wr <= v_msb_reg_on;
      cntrl_reg_wr <= cntrl_reg_on;
      if (count_reg_wr = '1') then
        user_count <= count_reg;
      end if;
      if (v_lsb_reg_wr = '1') then
        user_v_lsb <= v_lsb_reg;
      end if;
      if (v_msb_reg_wr = '1') then
        user_v_msb <= v_msb_reg;
      end if;
      if (cntrl_reg_wr = '1') then
        user_cntrl <= cntrl_reg;
      end if;
    end if;
end process;

process begin
  wait until rising_edge(eb_sys_clk);
    if (eb_sys_rst = '1') then
      user_start_v <= (others => '0');
    else
      user_start_v(0) <= user_start;
      user_start_v(1) <= user_start_v(0);
      user_start_v(2) <= user_start_v(1);
      user_start_s    <= user_start_v(1) and (not user_start_v(2));
    end if;
end process;

process begin
  wait until rising_edge(eb_sys_clk);
    if (eb_sys_rst = '1') then
      user_send  <= '0';
      strt_state <= STRT_INIT;
    else
      case (strt_state) is

        when STRT_INIT =>

          if ((user_start_s = '1') and (user_stop = '0')) then
            user_send  <= '1';
            strt_state <= STRT_ACK;
          else
            strt_state <= STRT_INIT;
          end if;

        when STRT_ACK =>

          if (user_ackn = '1') then
            user_send  <= '0';
            strt_state <= STRT_END;
          else
            strt_state <= STRT_ACK;
          end if;

        when STRT_END =>

          if (user_start_s = '0') then
            strt_state <= STRT_INIT;
          else
            strt_state <= STRT_END;
          end if;

        when others =>

          user_send  <= '0';
          strt_state <= STRT_INIT;

      end case;
    end if;
end process;

process begin

  wait until rising_edge(user_clk_int);

    if (user_lck_int = '0') then
      user_en        <= '0';
      user_ackn      <= '0';
      user_data_int  <= (others => '0');
      wr_counter     <= (others => '0');
      wr_counter_o   <= (others => '0');
      loop_counter   <= (others => '0');
      loop_counter_o <= (others => '0');
      user_state     <= USER_INIT;
    else

      wr_counter_o <= wr_counter;
      loop_counter_o <= loop_counter;

      case (user_state) is

        when USER_INIT =>

          if (user_send = '1') then
            wr_counter   <= (others => '0');
            loop_counter <= (others => '0');
            user_state   <= USER_ACK;
          else
            user_state <= USER_INIT;
          end if;

        when USER_ACK =>

          user_ackn     <= '1';
          user_en       <= '1';
          user_data_int <= std_logic_vector(unsigned(user_v_msb)) & std_logic_vector(unsigned(user_v_lsb));
          user_state    <= USER_ENL;

        when USER_DAT =>

          user_en       <= '1';
          user_data_int <= std_logic_vector(unsigned(user_data_int) + 1);
          user_state    <= USER_ENL;

        when USER_ENL =>

          user_en    <= '0';
          wr_counter <= std_logic_vector(unsigned(wr_counter) + 1);
          user_state <= USER_CMP;

        when USER_CMP =>

          if (user_stop = '1') then
            user_state <= USER_END;
          elsif (user_afull = '1') then
            user_state <= USER_WAIT;
          else
            user_state <= USER_DAT;
          end if;

        when USER_WAIT =>

          if (user_stop = '1') then
            user_state <= USER_END;
          -- elsif (unsigned(user_wr) < to_unsigned(FIFO_HSIZE,16)) then
          elsif (user_afull = '0') then
            wr_counter   <= (others => '0');
            loop_counter <= std_logic_vector(unsigned(loop_counter) + 1);
            user_state   <= USER_DAT;
          else
            user_state <= USER_WAIT;
          end if;

        when USER_END =>

          user_ackn  <= '0';
          user_state <= USER_INIT;

        when others =>

          user_en        <= '0';
          user_ackn      <= '0';
          user_data_int  <= (others => '0');
          wr_counter     <= (others => '0');
          wr_counter_o   <= (others => '0');
          loop_counter   <= (others => '0');
          loop_counter_o <= (others => '0');
          user_state     <= USER_INIT;

      end case;
    end if;

end process;

end dblock_dmak_user_rtl; 
