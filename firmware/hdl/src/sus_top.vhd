-----------------------------------------------------
--
-- File: sus_top.vhd 
-- Description: Top Module for SuS implementations
--		Includes JTAG & parts of DAQ
--
--
-----------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;


use work.eb_common_pkg.all;
use work.ebs_regs_pkg.all;
use work.ebs_pkg.all;
use work.dblock_dmak_pkg.all;

--use work.readout.all;
--use work.readout_8b10b.all;



entity sus_top is



generic (
  EBS_AD_RNGE          : NATURAL := 16;                                -- Number of address bits to decode. Must be the same for all slaves mapped in the same segment
  EBFT_DWIDTH          : NATURAL := 64                                 -- FT (DMA) data width
);

port (

-- EBONE INTERFACE ----------------------------------------------------
  eb_clk_i     				: in std_logic;
  ebs_i        				: in ebs32_i_typ;
  ebs_o                     : out ebs32_oa_typ(EBS_SLV_NB downto 1);


-- JTAG ---------------------------------------------------------------
  sus_jtag_wrapper_clk			: in std_logic;  

  sus_jtag_wrapper_rst_n		: out std_logic;

  sus_jtag_wrapper_tck			: out std_logic;
  sus_jtag_wrapper_tms			: out std_logic;
  sus_jtag_wrapper_td_out_to_asic	: out std_logic;
  sus_jtag_wrapper_td_in_from_asic	: in std_logic;


-- Dynamic ASIC Control ------------------------------------------------

  sus_dyn_data_out_valid		: out std_logic;
  sus_dyn_data_out_64			: out std_logic_vector(63 downto 0);


  sus_dyn_asic_run			: out std_logic;
  sus_dyn_asic_ser0_dout		: in std_logic;
  sus_dyn_asic_ser1_dout		: in std_logic;

  sus_dyn_asic_res_n			: out std_logic;

  daq_fifo_wr_full              : in std_logic;
  sus_dyn_asic_mon              : in std_logic;

  sus_dyn_pll_clk               : in std_logic;
  sus_dyn_pll_clk_by5           : out std_logic;
  sus_dyn_cycle_done            : out std_logic;
  sus_dyn_cycle_done_delayed    : out std_logic;


-- T3 Controller & Telegram ------------------------------------------------

  sus_T3_telegram_cmd_to_asic  : out std_logic;
  sus_T3_data_from_asic        : in std_logic;

--  sus_T3_tele_pattern          : in std_logic_vector(581 downto 0);
  sus_T3_tele_fifo_wclk        : out std_logic

--  sus_T3_tele_do_init_link     : in std_logic;
--  sus_T3_tele_link_detected    : out std_logic
);

end sus_top;

------------------------------------------------------------------------
architecture sus_top_rtl of sus_top is
------------------------------------------------------------------------

------------------------------------------------------------------------
-- SIGNALS
------------------------------------------------------------------------


-- REGISTERS -----------------------------------------------------------
signal regs_jtag_rw		: std32_a((REGS_NB / 2) - 1 downto 0);
signal regs_jtag_ro		: std32_a(REGS_NB - 1 downto (REGS_NB / 2));
signal jtag_in_rw_stdlog	: std_logic_vector((REGS_NB / 2) - 1 downto 0);
signal jtag_out_ro_stdlog	: std_logic_vector(REGS_NB - 1 downto (REGS_NB / 2));
signal regs_wak_jtag	: std_logic_vector((REGS_NB / 2) - 1 downto 0);
signal regs_rak_jtag	: std_logic_vector((REGS_NB / 2) - 1 downto 0);
signal regs_ofst_jtag	: std_logic_vector(Nlog2(REGS_NB) - 1 downto 0);
signal regs_ofsrd_jtag	: std_logic;
signal regs_ofswr_jtag	: std_logic;



signal regs_dyn_rw		: std32_a((REGS_NB / 2) - 1 downto 0);
signal regs_dyn_ro		: std32_a(REGS_NB - 1 downto (REGS_NB / 2));
signal dyn_in_rw_stdlog	: std_logic_vector(7 downto 0);
signal dyn_out_ro_stdlog	: std_logic_vector(15 downto 0);
signal regs_wak_dyn 	: std_logic_vector((REGS_NB / 2) - 1 downto 0);
signal regs_rak_dyn		: std_logic_vector((REGS_NB / 2) - 1 downto 0);
signal regs_ofst_dyn	: std_logic_vector(Nlog2(REGS_NB) - 1 downto 0);
signal regs_ofsrd_dyn	: std_logic;
signal regs_ofswr_dyn	: std_logic;

-- JTAG ----------------------------------------------------------------
signal jtag_wrapper_clk              : std_logic; 
signal jtag_wrapper_rst_n            : std_logic; 
signal jtag_wrapper_rst            : std_logic; 


signal jtag_wrapper_data_out_read    : std_logic; 
signal jtag_wrapper_data_out_valid   : std_logic; 
signal jtag_wrapper_data_in_busy     : std_logic;  
signal jtag_wrapper_data_in_write    : std_logic; 
signal jtag_wrapper_tck              : std_logic;            
signal jtag_wrapper_tms              : std_logic; 
signal jtag_wrapper_td_out_to_asic   : std_logic; 
signal jtag_wrapper_td_in_from_asic  : std_logic; 

-- Dynamic Asic Control ------------------------------------------------
signal dyn_data_out_valid            : std_logic; 
signal dyn_data_out_valid_cnt        : integer range 0 to 5; 
signal dyn_data_out_words_left       : integer range 0 to 2048; 
signal sus_dyn_data_out_valid_int    : std_logic; 
signal dyn_data_in_busy              : std_logic; 	
signal dyn_data_in_write             : std_logic; 
signal dyn_data_in                   : std_logic_vector(7 downto 0); 
signal dyn_data_out                  : std_logic_vector(15 downto 0);
--signal dyn_data_out                  : std_logic_vector(25 downto 0);
signal dyn_data_out_64               : std_logic_vector(63 downto 0);
signal dyn_asic_run		        : std_logic; 
signal dyn_asic_res		        : std_logic; 
signal dyn_init_jtag	         	: std_logic; 
signal dyn_jtag_idle	         	: std_logic; 
signal dyn_o_current_state	        : std_logic_vector(5 downto 0); 
signal dyn_o_next_state		 	: std_logic_vector(5 downto 0);
signal dyn_o_dev_in_fifo_empty	 	: std_logic;
signal dyn_conf_cycle_length	 	: std_logic_vector(11 downto 0);	
signal dyn_conf_num_words_to_readout 	: std_logic_vector(13 downto 0);
signal dyn_asic_ser0_dout		: std_logic;	
signal dyn_asic_ser1_dout		: std_logic;	
signal dyn_conf_send_test_data       	: std_logic;
signal dyn_cycle_done           : std_logic;
--signal dyn_cycle_cnt              : integer range 0 to 2000;

signal dyn_pll_clk              : std_logic;
signal dyn_pll_clk_by5 : std_logic;
--signal dyn_pll_clk_by5_a : std_logic;
--signal dyn_pll_clk_by5_b : std_logic;

--signal dyn_pll_clk_divcount_a : integer range 0 to 5;
--signal dyn_pll_clk_divcount_b : integer range 0 to 5;

signal dyn_cycle_done_delayed     : std_logic;
--signal dyn_cycle_done_delay_reg   : std_logic_vector(1999 downto 0);
signal dyn_cycle_done_delay       : std_logic_vector(10 downto 0);
signal dyn_cycle_done_delay_skips : std_logic_vector(10 downto 0);
signal dyn_asic_mon             : std_logic;
--signal en_cycle_done_delayed      : std_ulogic;

signal dyn_delayed_cycle_single : std_logic;
signal dyn_delayed_cycle_done_en : std_logic;



-- T3 Controller & Telegram --------------------------------------------

signal regs_t3cont_rw	        :   std32_a(REGS_NB_T3CONT - 3 downto 0);
signal regs_t3cont_ro	        :   std32_a(REGS_NB_T3CONT - 1 downto REGS_NB_T3CONT - 2);


signal regs_wak_t3cont 	    : std_logic_vector(REGS_NB_T3CONT - 3 downto 0);
signal regs_rak_t3cont	    : std_logic_vector(1 downto 0);
signal regs_ofst_t3cont	    : std_logic_vector(Nlog2(REGS_NB_T3CONT) - 1 downto 0);
signal regs_ofsrd_t3cont    : std_logic;
signal regs_ofswr_t3cont	: std_logic;



signal T3_telegram_cmd_in       :   std_logic;
signal T3_data_out              :   std_logic;

signal T3_tele_pattern          :   std_logic_vector(581 downto 0);

signal T3_tele_fifo_wclk        :   std_logic;
signal T3_tele_fifo_free        :   std_logic;
signal T3_tele_fifo_wen         :   std_logic;
signal T3_tele_fifo_wdata       :   std_logic_vector(EBFT_DWIDTH-1 downto 0);

signal T3_tele_do_init_link     :   std_logic;
signal T3_tele_link_detected    :   std_logic;

signal T3_tele_ro_sync_word     :   std_logic;
signal T3_tele_pf_current_state    :   std_logic_vector (3 downto 0);
signal T3_tele_clockgen_locked     :   std_logic;




------------------------------------------------------------------------
begin
------------------------------------------------------------------------




jtag_wrapper_clk 		        <= sus_jtag_wrapper_clk;
sus_jtag_wrapper_rst_n		    <= jtag_wrapper_rst_n;	

jtag_wrapper_rst                <= not jtag_wrapper_rst_n; 

sus_jtag_wrapper_tck	 	    <= jtag_wrapper_tck;	  
sus_jtag_wrapper_tms		    <= jtag_wrapper_tms;	  
sus_jtag_wrapper_td_out_to_asic	<= jtag_wrapper_td_out_to_asic; 
jtag_wrapper_td_in_from_asic	<= sus_jtag_wrapper_td_in_from_asic;

--sus_dyn_data_out_valid		    <= sus_dyn_data_out_valid_int;
sus_dyn_data_out_valid		    <= T3_tele_fifo_wen;
--sus_dyn_data_out_64		        <= dyn_data_out_64;
sus_dyn_data_out_64		        <= T3_tele_fifo_wdata;
sus_dyn_asic_run		        <= dyn_asic_run;  
dyn_asic_ser0_dout		        <= sus_dyn_asic_ser0_dout;
dyn_asic_ser1_dout		        <= sus_dyn_asic_ser1_dout;

dyn_pll_clk                     <= sus_dyn_pll_clk;
sus_dyn_pll_clk_by5             <= dyn_pll_clk_by5;

sus_dyn_asic_res_n		        <= jtag_wrapper_rst_n AND (NOT dyn_asic_res);

sus_dyn_cycle_done_delayed      <= dyn_cycle_done_delayed; 
--dyn_cycle_done_delayed          <= dyn_cycle_done_delay_reg(to_integer(signed(dyn_cycle_done_delay))) and en_cycle_done_delayed;
sus_dyn_cycle_done              <= dyn_cycle_done;
--en_cycle_done_delayed           <= '1' when (dyn_cycle_cnt = to_integer(signed(dyn_cycle_done_delay_skips))) else '0';
--dyn_delayed_cycle_done_en       <= '1';


sus_T3_telegram_cmd_to_asic  <= T3_telegram_cmd_in;
--sus_T3_data_from_asic        <= T3_data_out;
T3_data_out                  <= sus_T3_data_from_asic;

--sus_T3_tele_pattern          <= T3_tele_pattern;
--T3_tele_pattern              <= sus_T3_tele_pattern;

--sus_T3_tele_do_init_link     <= T3_tele_do_init_link;
--T3_tele_do_init_link         <= sus_T3_tele_do_init_link;
--sus_T3_tele_link_detected    <= T3_tele_link_detected;
--T3_tele_link_detected        <= sus_T3_tele_link_detected;
sus_T3_tele_fifo_wclk          <= T3_tele_fifo_wclk;


-- REGISTERS -------------------------------------------------------------------------------------------------------------------------

jtag_regs_i : ebs_regs                                                   -- E-Bone slave 6, Wrapper registers for FPGA JTAG master engine
--------------------------------------------------------------------------------------------------------------------------------------
generic map (
  EBS_AD_RNGE  => EBS_AD_RNGE,                                         -- long adressing range
  EBS_AD_BASE  => 1,                                                   -- segment 1 (BADR1)
  EBS_AD_SIZE  => REGS_NB,                                             -- size in segment (REGS_NB)
  EBS_AD_OFST  => 16#380#,                                             -- offset in segment; Addresses in BADR1 are multiplied by 4. 
                                                                       -- Actual address of first register is 0x0380*4 = 0x0e00; second register is 0x0e04..

  EBS_MIRQ     => (others => '0'),                                     -- Message IRQ
  REG_RO_SIZE  => (REGS_NB / 2),                                       -- read only reg. number (REGS_NB / 2)
  REG_WO_BITS  => 0                                                    -- reg. 0 self clear bit number
)
port map (
-- E-bone interface
-------------------
  eb_clk_i     => eb_clk_i,
  ebs_i        => ebs_i,
  ebs_o        => ebs_o(6),

-- Register interface
---------------------
  regs_o       => regs_jtag_rw,                                     -- R/W registers external outputs, regs_o[(REGS_NB / 2) - 1 downto 0]
  regs_i       => regs_jtag_ro,                                     -- read only register external inputs, regs_i[REGS_NB - 1 downto (REGS_NB / 2)]
  regs_irq_i   => '0',                                                  -- interrupt request
  regs_iak_o   => open,                                                 -- interrupt handshake
  regs_ofsrd_o => regs_ofsrd_jtag,                                  -- read burst offset enable
  regs_ofswr_o => regs_ofswr_jtag,                                  -- write burst offset enable
  regs_ofs_o   => regs_ofst_jtag                                    -- offset
);

jtag_wrapper_rst_n	        	<= regs_jtag_rw(0)(0);  
jtag_wrapper_data_out_read 		<= regs_jtag_rw(0)(1);
jtag_wrapper_data_out_valid 	<= regs_jtag_ro(8)(0);
jtag_wrapper_data_in_busy		<= regs_jtag_ro(8)(1); 
jtag_wrapper_data_in_write		<= regs_jtag_rw(0)(2);
regs_jtag_ro(9)(7 downto 0)		<= jtag_out_ro_stdlog;
jtag_in_rw_stdlog			<= regs_jtag_rw(1)(7 downto 0);




dynamic_ctrl_i : ebs_regs                                           -- E-Bone slave 7, Wrapper registers for dynamic asic control 
--------------------------------------------------------------------------------------------------------------------------------------
generic map (
  EBS_AD_RNGE  => EBS_AD_RNGE,                                         -- long adressing range
  EBS_AD_BASE  => 1,                                                   -- segment 1 (BADR1)
  EBS_AD_SIZE  => REGS_NB,                                             -- size in segment (REGS_NB)
  EBS_AD_OFST  => 16#300#,                                             -- offset in segment; Actual address of first register is 0x0c00
  EBS_MIRQ     => (others => '0'),                                     -- Message IRQ
  REG_RO_SIZE  => (REGS_NB / 2),                                       -- read only reg. number (REGS_NB / 2)
  REG_WO_BITS  => 0                                                    -- reg. 0 self clear bit number
)
port map (
-- E-bone interface
-------------------
  eb_clk_i     => eb_clk_i,
  ebs_i        => ebs_i,
  ebs_o        => ebs_o(7),

-- Register interface
---------------------
  regs_o       => regs_dyn_rw,                                     -- R/W registers external outputs, regs_o[(REGS_NB / 2) - 1 downto 0]
  regs_i       => regs_dyn_ro,                                     -- read only register external inputs, regs_i[REGS_NB - 1 downto (REGS_NB / 2)]
  regs_irq_i   => '0',                                                 -- interrupt request
  regs_iak_o   => open,                                                -- interrupt handshake
  regs_ofsrd_o => regs_ofsrd_dyn,                                  -- read burst offset enable
  regs_ofswr_o => regs_ofswr_dyn,                                  -- write burst offset enable
  regs_ofs_o   => regs_ofst_dyn                                    -- offset
);

dyn_data_in_write			    <= regs_dyn_rw(0)(0);	
dyn_conf_send_test_data		    <= regs_dyn_rw(0)(1);	
dyn_in_rw_stdlog			    <= regs_dyn_rw(1)(7 downto 0);	
dyn_conf_cycle_length		    <= regs_dyn_rw(2)(27 downto 16);		
dyn_conf_num_words_to_readout	<= regs_dyn_rw(2)(13 downto 0);	
dyn_cycle_done_delay            <= regs_dyn_rw(3)(10 downto 0);
dyn_cycle_done_delay_skips      <= regs_dyn_rw(3)(21 downto 11);
dyn_delayed_cycle_done_en       <= regs_dyn_rw(3)(22);
dyn_delayed_cycle_single        <= regs_dyn_rw(3)(23);

regs_dyn_ro(8)(0)			<= dyn_data_out_valid;
regs_dyn_ro(8)(1)			<= dyn_data_in_busy;
regs_dyn_ro(8)(2)			<= dyn_asic_run;
regs_dyn_ro(8)(3)			<= dyn_asic_res;
regs_dyn_ro(8)(4)			<= dyn_asic_ser0_dout;
regs_dyn_ro(8)(5)		 	<= dyn_o_dev_in_fifo_empty; 
--regs_dyn_ro(9)(25 downto 0)		<= dyn_data_out;
regs_dyn_ro(9)(15 downto 0)		<= dyn_data_out;

regs_dyn_ro(10)(5 downto 0) 	<= dyn_o_current_state; 
regs_dyn_ro(11)(5 downto 0) 	<= dyn_o_next_state; 







T3_ctrl_regs_i : ebs_regs                                           -- E-Bone slave 8, Wrapper registers for T3 controller 
--------------------------------------------------------------------------------------------------------------------------------------
generic map (
  EBS_AD_RNGE  => EBS_AD_RNGE,                                         -- long adressing range
  EBS_AD_BASE  => 1,                                                   -- segment 1 (BADR1)
  EBS_AD_SIZE  => REGS_NB_T3CONT,                                      -- size in segment (20)
  EBS_AD_OFST  => 16#400#,                                             -- offset in segment; Actual address of first register is 0x0c00
  EBS_MIRQ     => (others => '0'),                                     -- Message IRQ
  REG_RO_SIZE  => 2,                                                   -- read only reg. number (REGS_NB / 2)
  REG_WO_BITS  => 0                                                    -- reg. 0 self clear bit number
)
port map (
-- E-bone interface
-------------------
  eb_clk_i     => eb_clk_i,
  ebs_i        => ebs_i,
  ebs_o        => ebs_o(8),

-- Register interface
---------------------
  regs_o       => regs_t3cont_rw,                                     -- R/W registers external outputs, regs_o[(REGS_NB / 2) - 1 downto 0]
  regs_i       => regs_t3cont_ro,                                     -- read only register external inputs, regs_i[REGS_NB - 1 downto (REGS_NB / 2)]
  regs_irq_i   => '0',                                                 -- interrupt request
  regs_iak_o   => open,                                                -- interrupt handshake
  regs_ofsrd_o => regs_ofsrd_t3cont,                                  -- read burst offset enable
  regs_ofswr_o => regs_ofswr_t3cont,                                  -- write burst offset enable
  regs_ofs_o   => regs_ofst_t3cont                                    -- offset
);



T3_tele_do_init_link        <=          regs_t3cont_rw(0)(0);
T3_tele_ro_sync_word        <=          regs_t3cont_rw(0)(1);
regs_t3cont_ro(REGS_NB_T3CONT - 2)(0)            <=          T3_tele_link_detected;
regs_t3cont_ro(REGS_NB_T3CONT - 1)(3 downto 0)            <=          T3_tele_pf_current_state;
regs_t3cont_ro(REGS_NB_T3CONT - 1)(4)            <=          T3_tele_clockgen_locked;


T3_tele_pattern(23 downto 0) <= regs_t3cont_rw(1)(23 downto 0);
T3_tele_pattern(581 downto 564) <= regs_t3cont_rw(22)(17 downto 0);

regs_t3cont_connection: for i in 1 to 20 generate
    T3_tele_pattern(23+27*i downto 23+27*(i-1)+1) <= regs_t3cont_rw(i+1)(26 downto 0);
end generate regs_t3cont_connection;

T3_tele_fifo_free <= not daq_fifo_wr_full;

--T3_tele_fifo_free <= '1';
-- TELEGRAM CONTROL ------------------------------------------------------------------------------------------------------------

telegram_control_i : T3Controller                                   -- Controller for new T3 digital part with telegram logic
---------------------------------------------------------------------------------------------------------------------------------
--generic map (
--    --P_FIFO_WIDTH = EBFT_DWIDTH 
--    p_fifo_width = EBFT_DWIDTH 
--)
port map (
    CLK_125                             => dyn_pll_clk,
    RESET                               => jtag_wrapper_rst,  

    -- signals to/from SUS65T3               
    T3_CMD_IN                           => T3_telegram_cmd_in,
    T3_DATA_OUT                         => T3_data_out,

    -- control 
    PATTERN                             => T3_tele_pattern,

    -- data
    FIFO_WCLK                           => T3_tele_fifo_wclk,
    FIFO_FREE                           => T3_tele_fifo_free,
    FIFO_WEN                            => T3_tele_fifo_wen,
    FIFO_WDATA                          => T3_tele_fifo_wdata,

    DO_INIT_LINK                        => T3_tele_do_init_link,
    LINK_DETECTED                       => T3_tele_link_detected,

    READOUT_SYNC_WORD                   => T3_tele_ro_sync_word,
    pf_current_state                    => T3_tele_pf_current_state,
    clockgen_locked                     => T3_tele_clockgen_locked
);



-- JTAG -------------------------------------------------------------------------------------------------------------------------

jtag_engine_ebone_reg : jtag_engine_ebone_reg_if                 -- SUS JTAG engine master wrapped in FIFOs 
---------------------------------------------------------------------------------------------------------------------------------
port map (
  clk                 => jtag_wrapper_clk,
  res_n               => jtag_wrapper_rst_n,
  data_out_read       => jtag_wrapper_data_out_read,
  data_out_valid      => jtag_wrapper_data_out_valid,
  data_out            => jtag_out_ro_stdlog,     
  data_in_busy        => jtag_wrapper_data_in_busy, 
  data_in_write       => jtag_wrapper_data_in_write,
  data_in             => jtag_in_rw_stdlog,      

  tck                 => jtag_wrapper_tck,             
  tms                 => jtag_wrapper_tms,
  td_out_to_asic      => jtag_wrapper_td_out_to_asic,
  td_in_from_asic     => jtag_wrapper_td_in_from_asic,

  ext_make_idle       => dyn_init_jtag,
  is_idle             => dyn_jtag_idle
);



clk_div5_i : clk_div5                            -- Dynamic ASIC control engine for data acquisition
---------------------------------------------------------------------------------------------------------------------------------
port map (
  clk				        => dyn_pll_clk,   
  clk_by5 			        => dyn_pll_clk_by5
);

-- Dynamic Asic Control ---------------------------------------------------------------------------------------------------------

dyn_asic_cntrl_i : DynamicAsicControl                            -- Dynamic ASIC control engine for data acquisition
---------------------------------------------------------------------------------------------------------------------------------
port map (
  res_n				=> jtag_wrapper_rst_n,
  clk				=> dyn_pll_clk_by5,   
  data_in_busy			=> dyn_data_in_busy,        
  data_in_write			=> dyn_data_in_write,
  data_in			=> dyn_in_rw_stdlog,             

  data_out_valid		=> dyn_data_out_valid,
  data_out			=> dyn_data_out,

  asic_run			=> dyn_asic_run,
  asic_res			=> dyn_asic_res,
  init_jtag			=> dyn_init_jtag,
  jtag_idle			=> dyn_jtag_idle,
  o_current_state		=> dyn_o_current_state,
  o_next_state			=> dyn_o_next_state,
  o_dev_in_fifo_empty		=> dyn_o_dev_in_fifo_empty,
  cycle_done            => dyn_cycle_done, 


  conf_cycle_length		=> dyn_conf_cycle_length,
  conf_num_words_to_readout	=> dyn_conf_num_words_to_readout,
  asic_ser_dout			=> dyn_asic_ser0_dout,
  conf_send_test_data		=> dyn_conf_send_test_data		
);



-- Output Fifo has 64 bit words. Words per FE are 16 bit wide. Generate 64bit word from 4 FEs via 64bit shift reg.
-- Generate data_valid (sus_dyn_data_out_valid_int) for FIFO after every 4th 16 bit word.
-- CAREFUL: This only works if the number of FEs is a multiple of 4. Otherwise, some fifo entries (<4) will be discarded


dyn_data_out_16_to_64 : packer_16_to_64_bit                             -- 16 to 64 bit converter to fit dynamic asic control output to fifo (this block has been encapsulated while cleaning up for T3 and has never been tested since then!) 
---------------------------------------------------------------------------------------------------------------------------------
port map (
  clk                   => dyn_pll_clk_by5,
  rst_n                 => jtag_wrapper_rst_n,
  data_in_16            => dyn_data_out,
  data_in_valid         => dyn_data_out_valid,
  
  data_out_64           => dyn_data_out_64,
  data_out_valid        => sus_dyn_data_out_valid_int
);


-- Delay cycle done to generate a trigger for sensor measurements which can be freely programmed
-- Also allow this trigger to be sent out only every nth sequencer cycle via cycle_cnt

cycle_done_delay_i : cycle_done_delay                            -- Dynamic ASIC control engine for data acquisition
---------------------------------------------------------------------------------------------------------------------------------
port map (
  clk				        => dyn_pll_clk_by5,   
  rst_n 			        => jtag_wrapper_rst_n,
  cycle_done		        => dyn_cycle_done,       

  cycle_done_delay          => dyn_cycle_done_delay,
  cycle_done_delay_skips    => dyn_cycle_done_delay_skips,

  single                    => dyn_delayed_cycle_single,
  enable                    => dyn_delayed_cycle_done_en,
  cycle_done_delayed        => dyn_cycle_done_delayed
  
);

end sus_top_rtl; 
--
