-------------------------------------------------------------------------------
--
-- Project     : E-bone - System Monitor
-- File        : sysm_regs.vhd
-- Description : System monitor component implementation
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

library unisim;
use unisim.vcomponents.BUFR;

entity sysm_regs is

generic (
  EBS_IN_AUX0   : natural := 0;                               -- 1st Auxiliary Analog Input number
  EBS_IN_AUX1   : natural := 0;                               -- 2nd Auxiliary Analog Input number
  TRGT_DESC     : NATURAL := 16#00#;                          -- Type target description
  ADC_LOW       : bit_vector(15 downto 0);
  ADC_HIGH      : bit_vector(15 downto 0)
);

port (
  clk           : in std_logic;
  rst           : in std_logic;

  VP_in         : in std_logic;                               -- Dedicated Analog Input Pair
  VN_in         : in std_logic;
  VAUXP0_in     : in std_logic;                               -- 1st Auxiliary Analog Input Pair
  VAUXN0_in     : in std_logic;
  VAUXP1_in     : in std_logic;                               -- 2nd Auxiliary Analog Input Pair
  VAUXN1_in     : in std_logic;

  sysmon_regs_o : out std32_a(15 downto 0);
  temp_alarm_o  : out std_logic
);
end sysm_regs;

architecture rtl of sysm_regs is

component sysmon_v2_0
generic (
  ADC_LOW             : BIT_VECTOR (15 downto 0);
  ADC_HIGH            : BIT_VECTOR (15 downto 0)
);
port (

  DADDR_IN            : in std_logic_vector(6 downto 0);      -- Address bus for the dynamic reconfiguration port
  DCLK_IN             : in std_logic;                         -- Clock input for the dynamic reconfiguration port
  DEN_IN              : in std_logic;                         -- Enable Signal for the dynamic reconfiguration port
  DI_IN               : in std_logic_vector(15 downto 0);     -- Input data bus for the dynamic reconfiguration port
  DWE_IN              : in std_logic;                         -- Write Enable for the dynamic reconfiguration port
  RESET_IN            : in std_logic;                         -- Reset signal for the System Monitor control logic
  VAUXP0              : in std_logic;                         -- Auxiliary Channel 0
  VAUXN0              : in std_logic;
  VAUXP1              : in std_logic;                         -- Auxiliary Channel 1
  VAUXN1              : in std_logic;
  VAUXP2              : in std_logic;                         -- Auxiliary Channel 2
  VAUXN2              : in std_logic;
  VAUXP3              : in std_logic;                         -- Auxiliary Channel 3
  VAUXN3              : in std_logic;
  VAUXP4              : in std_logic;                         -- Auxiliary Channel 4
  VAUXN4              : in std_logic;
  VAUXP5              : in std_logic;                         -- Auxiliary Channel 5
  VAUXN5              : in std_logic;
  VAUXP6              : in std_logic;                         -- Auxiliary Channel 6
  VAUXN6              : in std_logic;
  VAUXP7              : in std_logic;                         -- Auxiliary Channel 7
  VAUXN7              : in std_logic;
  VAUXP8              : in std_logic;                         -- Auxiliary Channel 8
  VAUXN8              : in std_logic;
  VAUXP9              : in std_logic;                         -- Auxiliary Channel 9
  VAUXN9              : in std_logic;
  VAUXP10             : in std_logic;                         -- Auxiliary Channel 10
  VAUXN10             : in std_logic;
  VAUXP11             : in std_logic;                         -- Auxiliary Channel 11
  VAUXN11             : in std_logic;
  VAUXP12             : in std_logic;                         -- Auxiliary Channel 12
  VAUXN12             : in std_logic;
  VAUXP13             : in std_logic;                         -- Auxiliary Channel 13
  VAUXN13             : in std_logic;
  VAUXP14             : in std_logic;                         -- Auxiliary Channel 14
  VAUXN14             : in std_logic;
  VAUXP15             : in std_logic;                         -- Auxiliary Channel 15
  VAUXN15             : in std_logic;
  BUSY_OUT            : out std_logic;                        -- ADC Busy signal
  CHANNEL_OUT         : out std_logic_vector(4 downto 0);     -- Channel Selection Outputs
  DO_OUT              : out std_logic_vector(15 downto 0);    -- Output data bus for dynamic reconfiguration port
  DRDY_OUT            : out std_logic;                        -- Data ready signal for the dynamic reconfiguration port
  EOC_OUT             : out std_logic;                        -- End of Conversion Signal
  EOS_OUT             : out std_logic;                        -- End of Sequence Signal
  OT_OUT              : out std_logic;                        -- Over-Temperature alarm output
  VCCAUX_ALARM_OUT    : out std_logic;                        -- VCCAUX-sensor alarm output
  VCCINT_ALARM_OUT    : out std_logic;                        -- VCCINT-sensor alarm output
  USER_TEMP_ALARM_OUT : out std_logic;                        -- Temperature-sensor alarm output
  VP_IN               : in std_logic;                         -- Dedicated Analog Input Pair
  VN_IN               : in std_logic

);

end component;

component xadc_v3_3 is
generic (
  ADC_LOW             : bit_vector(15 downto 0);
  ADC_HIGH            : bit_vector(15 downto 0)
);
port (
  DADDR_IN            : in std_logic_vector(6 downto 0);      -- Address bus for the dynamic reconfiguration port
  DCLK_IN             : in std_logic;                         -- Clock input for the dynamic reconfiguration port
  DEN_IN              : in std_logic;                         -- Enable Signal for the dynamic reconfiguration port
  DI_IN               : in std_logic_vector(15 downto 0);     -- Input data bus for the dynamic reconfiguration port
  DWE_IN              : in std_logic;                         -- Write Enable for the dynamic reconfiguration port
  RESET_IN            : in std_logic;                         -- Reset signal for the System Monitor control logic
  VAUXP0              : in std_logic;                         -- Auxiliary Channel 0
  VAUXN0              : in std_logic;
  VAUXP1              : in std_logic;                         -- Auxiliary Channel 1
  VAUXN1              : in std_logic;
  BUSY_OUT            : out std_logic;                        -- ADC Busy signal
  CHANNEL_OUT         : out std_logic_vector(4 downto 0);     -- Channel Selection Outputs
  DO_OUT              : out std_logic_vector(15 downto 0);    -- Output data bus for dynamic reconfiguration port
  DRDY_OUT            : out std_logic;                        -- Data ready signal for the dynamic reconfiguration port
  EOC_OUT             : out std_logic;                        -- End of Conversion Signal
  EOS_OUT             : out std_logic;                        -- End of Sequence Signal
  OT_OUT              : out std_logic;                        -- Over-Temperature alarm output
  VCCAUX_ALARM_OUT    : out std_logic;                        -- VCCAUX-sensor alarm output
  VCCINT_ALARM_OUT    : out std_logic;                        -- VCCINT-sensor alarm output
  USER_TEMP_ALARM_OUT : out std_logic;                        -- Temperature-sensor alarm output
  ALARM_OUT           : out std_logic;                        -- OR'ed output of all the Alarms
  VP_IN               : in std_logic;                         -- Dedicated Analog Input Pair
  VN_IN               : in std_logic
);

end component;

subtype std16          is std_logic_vector(15 downto 0);
subtype std24          is std_logic_vector(23 downto 0);
constant std16_null    : std16   := (others => '0');
constant std24_null    : std24   := (others => '0');
constant EBS_VERSION   : NATURAL := 16#00000025#;             -- version 2.5

type DRP_type          is (DRP_INIT, DRP_RD, DRP_RDY_WAIT, DRP_INCR_WAIT, DRP_TC, DRP_END);
signal DRP_state       : DRP_type;

type SYNC_type         is (SYNC_INIT, SYNC_WR, SYNC_END);
signal SYNC_state      : SYNC_type;

signal DRP_enable      : std_logic;
signal DRP_write       : std_logic;
signal DRP_Data_rdy    : std_logic;
signal DRP_address     : std_logic_vector(6 downto 0);
signal DRP_data_in     : std_logic_vector(15 downto 0);
signal DRP_data_out    : std_logic_vector(15 downto 0);

signal clk_drp         : std_logic;
signal Over_Temp_Alarm : std_logic;
signal VCCAUX_Alarm    : std_logic;
signal VCCINT_Alarm    : std_logic;
signal User_Temp_Alarm : std_logic;
signal End_Of_Sequ     : std_logic;
signal End_Of_Conv     : std_logic;
signal Busy_status     : std_logic;
signal Out_Inhibit     : std_logic;
signal ADC_channel     : std_logic_vector(4 downto 0);
signal tamp_regs       : std16_a(63 downto 0);
signal sysmon_regs     : std32_a(15 downto 0);

signal DRP_addr_cnt    : std_logic_vector(5 downto 0);
signal DRP_addr_cnt_cl : std_logic;
signal DRP_addr_cnt_en : std_logic;
signal DRP_addr_cnt_tc : std_logic;

signal VAUXP           : std_logic_vector(15 downto 0);
signal VAUXN           : std_logic_vector(15 downto 0);

begin

process(clk_drp)
begin
   if rising_edge(clk_drp) then
      temp_alarm_o <= User_Temp_Alarm;
   end if;
end process;

sysm_gene : if (TRGT_DESC = 16#63#) generate

  clk_drp_buf : BUFR
  generic map (
    BUFR_DIVIDE => "2",
    SIM_DEVICE  => "VIRTEX6"
  )
  port map (
    CLR => rst,
    CE  => '1',
    I   => clk,
    O   => clk_drp
  );

  sysm : sysmon_v2_0
  generic map(
    ADC_LOW             => ADC_LOW,
    ADC_HIGH            => ADC_HIGH
  )
  port map (
    DADDR_IN            => DRP_address,
    DCLK_IN             => clk_drp,
    DEN_IN              => DRP_enable,
    DI_IN               => DRP_Data_in,
    DWE_IN              => DRP_write,
    RESET_IN            => rst,
    VAUXP0              => VAUXP(0),
    VAUXN0              => VAUXN(0),
    VAUXP1              => VAUXP(1),
    VAUXN1              => VAUXN(1),
    VAUXP2              => VAUXP(2),
    VAUXN2              => VAUXN(2),
    VAUXP3              => VAUXP(3),
    VAUXN3              => VAUXN(3),
    VAUXP4              => VAUXP(4),
    VAUXN4              => VAUXN(4),
    VAUXP5              => VAUXP(5),
    VAUXN5              => VAUXN(5),
    VAUXP6              => VAUXP(6),
    VAUXN6              => VAUXN(6),
    VAUXP7              => VAUXP(7),
    VAUXN7              => VAUXN(7),
    VAUXP8              => VAUXP(8),
    VAUXN8              => VAUXN(8),
    VAUXP9              => VAUXP(9),
    VAUXN9              => VAUXN(9),
    VAUXP10             => VAUXP(10),
    VAUXN10             => VAUXN(10),
    VAUXP11             => VAUXP(11),
    VAUXN11             => VAUXN(11),
    VAUXP12             => VAUXP(12),
    VAUXN12             => VAUXN(12),
    VAUXP13             => VAUXP(13),
    VAUXN13             => VAUXN(13),
    VAUXP14             => VAUXP(14),
    VAUXN14             => VAUXN(14),
    VAUXP15             => VAUXP(15),
    VAUXN15             => VAUXN(15),
    BUSY_OUT            => Busy_status,
    CHANNEL_OUT         => ADC_channel,
    DO_OUT              => DRP_data_out,
    DRDY_OUT            => DRP_Data_rdy,
    EOC_OUT             => End_Of_Conv,
    EOS_OUT             => End_Of_Sequ,
    OT_OUT              => Over_Temp_Alarm,
    VCCAUX_ALARM_OUT    => VCCAUX_Alarm,
    VCCINT_ALARM_OUT    => VCCINT_Alarm,
    USER_TEMP_ALARM_OUT => User_Temp_Alarm,
    VP_IN               => VP_in,
    VN_IN               => VN_in
	);
	
end generate sysm_gene;

xadc_gene : if (TRGT_DESC = 16#71#) or (TRGT_DESC = 16#72#) or (TRGT_DESC = 16#73#) generate

  clk_drp <= clk;

  xadc : xadc_v3_3
  generic map(
    ADC_LOW             => ADC_LOW,
    ADC_HIGH            => ADC_HIGH
  )
	port map (
    DADDR_IN            => DRP_address,
    DCLK_IN             => clk_drp,
    DEN_IN              => DRP_enable,
    DI_IN               => DRP_Data_in,
    DWE_IN              => DRP_write,
    RESET_IN            => rst,
    VAUXP0              => VAUXP(0),
    VAUXN0              => VAUXN(0),
    VAUXP1              => VAUXP(1),
    VAUXN1              => VAUXN(1),
    BUSY_OUT            => Busy_status,
    CHANNEL_OUT         => ADC_channel,
    DO_OUT              => DRP_data_out,
    DRDY_OUT            => DRP_Data_rdy,
    EOC_OUT             => End_Of_Conv,
    EOS_OUT             => End_Of_Sequ,
    OT_OUT              => Over_Temp_Alarm,
    VCCAUX_ALARM_OUT    => VCCAUX_Alarm,
    VCCINT_ALARM_OUT    => VCCINT_Alarm,
    USER_TEMP_ALARM_OUT => User_Temp_Alarm,
    ALARM_OUT           => open,
    VP_IN               => VP_in,
    VN_IN               => VN_in
	);

end generate xadc_gene;

DRP_write       <= '0';
DRP_Data_in     <= std16_null;
DRP_address     <= '0' & DRP_addr_cnt(5 downto 0);
DRP_addr_cnt_tc <= '1' when (DRP_addr_cnt = "111111") else '0';

VAUX0_inst : if (EBS_IN_AUX0 > 0) generate
  VAUXP(EBS_IN_AUX0 - 1) <= VAUXP0_in;
  VAUXN(EBS_IN_AUX0 - 1) <= VAUXN0_in;
end generate VAUX0_inst;

VAUX1_inst : if (EBS_IN_AUX1 > 0) generate
  VAUXP(EBS_IN_AUX1 - 1) <= VAUXP1_in;
  VAUXN(EBS_IN_AUX1 - 1) <= VAUXN1_in;
end generate VAUX1_inst;


process(clk, rst)                                             -- System monitor output data synchronisation
begin

  if (rst = '1') then

    for i in 0 to 15 loop
      sysmon_regs_o(i) <= (others => '0');
		end loop;

		SYNC_state    <= SYNC_INIT;

  elsif (clk'event and clk = '1') then

    case (SYNC_state) is

      when SYNC_INIT =>

        if (Out_Inhibit = '1') then
				  SYNC_state <= SYNC_WR;
				end if;

			when SYNC_WR =>

        if (Out_Inhibit = '0') then
				  SYNC_state <= SYNC_END;
				end if;

			when SYNC_END =>

        sysmon_regs_o <= sysmon_regs;
				SYNC_state    <= SYNC_INIT;

			when others =>

        SYNC_state <= SYNC_INIT;

		end case;

	end if;

end process;


process(clk_drp, rst)                                         -- System monitor DRP read State Machine
begin

  if (rst = '1') then

    for i in 0 to 63 loop
      tamp_regs(i) <= (others => '0');
		end loop;

    Out_Inhibit     <= '0';
    DRP_addr_cnt_cl <= '0';
		DRP_addr_cnt_en <= '0';
    DRP_enable      <= '0';
		DRP_state       <= DRP_INIT;

	elsif (clk_drp'event and clk_drp = '1') then

    case (DRP_state) is

			when DRP_INIT =>

        if (End_Of_Sequ = '1') and (Busy_status = '0') then
				  DRP_addr_cnt_cl <= '1';
          DRP_state       <= DRP_RD;
				end if;

      when DRP_RD =>

        DRP_addr_cnt_cl <= '0';
        DRP_enable      <= '1';
        DRP_state       <= DRP_RDY_WAIT;

      when DRP_RDY_WAIT =>

        DRP_enable <= '0';

        if (DRP_Data_rdy = '1') then
          tamp_regs(conv_integer(DRP_addr_cnt(5 downto 0))) <= "000000" & DRP_data_out(15 downto 6);
					DRP_addr_cnt_en <= '1';
				  DRP_state       <= DRP_INCR_WAIT;
				end if;

      when DRP_INCR_WAIT =>

        DRP_addr_cnt_en <= '0';
        DRP_state       <= DRP_TC;

			when DRP_TC =>

        if (DRP_addr_cnt_tc = '1') then
				  Out_Inhibit <= '1';
					DRP_state   <= DRP_END;
				else
				  DRP_state <= DRP_RD;
				end if;

      when DRP_END =>

        Out_Inhibit <= '0';
        DRP_state   <= DRP_INIT;

			when others =>

        DRP_state   <= DRP_INIT;

		end case;

	end if;

end process;


process(clk_drp, rst)                                         -- effective status registers
begin

  if (rst = '1') then

    for i in 0 to 15 loop
      sysmon_regs(i) <= (others => '0');
		end loop;

	elsif (clk_drp'event and clk_drp = '1') then

    if (DRP_addr_cnt_tc = '1') then
      sysmon_regs(0)  <= std16_null & tamp_regs(0);           -- Temperature
		  sysmon_regs(1)  <= std16_null & tamp_regs(32);          -- Temperature Max
		  sysmon_regs(2)  <= std16_null & tamp_regs(36);          -- Temperature Min

		  sysmon_regs(3)  <= std16_null & tamp_regs(1);           -- VCCINT
		  sysmon_regs(4)  <= std16_null & tamp_regs(33);          -- VCCINT Max
		  sysmon_regs(5)  <= std16_null & tamp_regs(37);          -- VCCINT Min

		  sysmon_regs(6)  <= std16_null & tamp_regs(2);           -- VCCAUX
		  sysmon_regs(7)  <= std16_null & tamp_regs(34);          -- VCCAUX Max
		  sysmon_regs(8)  <= std16_null & tamp_regs(38);          -- VCCAUX Min

		  sysmon_regs(9)  <= std16_null & tamp_regs(3);           -- VP/VN
		  sysmon_regs(10) <= std16_null & tamp_regs(4);           -- VREFP
		  sysmon_regs(11) <= std16_null & tamp_regs(5);           -- VREFN

      sysmon_regs(12) <= std16_null & tamp_regs(16);          -- VAUXP/VAUXP0
		  sysmon_regs(13) <= std16_null & tamp_regs(17);          -- VAUXP/VAUXP1

      sysmon_regs(14) <= std16_null & tamp_regs(63);          -- Flag register
		  sysmon_regs(15) <= std_logic_vector(to_unsigned(EBS_VERSION, 8)) & "0000" &
                                  std16_null &
			                            Over_Temp_Alarm &
																	VCCAUX_Alarm &
																	VCCINT_Alarm &
																	User_Temp_Alarm;            -- Alarm register
		end if;

	end if;

end process;


process(clk_drp, rst, DRP_addr_cnt_cl)                        -- DRP address counter
begin

  if ((rst = '1') or (DRP_addr_cnt_cl = '1')) then

    DRP_addr_cnt <= (others => '0');

  elsif (clk_drp'event and clk_drp = '1') then

		if (DRP_addr_cnt_en = '1') then
      DRP_addr_cnt <= DRP_addr_cnt + 1;
		end if;

  end if;

end process;

end; -- sysm_regs

