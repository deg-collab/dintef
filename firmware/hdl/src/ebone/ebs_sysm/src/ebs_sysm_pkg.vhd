--------------------------------------------------------------------------
--
-- E-bone - System Monitor Register slave
--
--------------------------------------------------------------------------
--
-- Version  Date      Author    Comment
-- 2.1      26/03/13  le caer
-- 2.2      30/09/13    herve   Record i/o, pipelined inputs
-- 2.3      18/12/13  Le Caer   Version (two nibbles) status offset 15
-- 2.4      26/02/15    herve   Added temperature alarm output
-- 2.5      05/09/16  Le Caer   XADC Version 3.3 update, VAUXP0..1 readout
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- Declare the component 'ebs_sysm'
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.ebs_pkg.all;

package ebs_sysm_pkg is

component ebs_sysm

generic (
  EBS_AD_RNGE  : NATURAL := 12; -- short adressing range
  EBS_AD_BASE  : NATURAL := 1;  -- usual IO segment
  EBS_AD_SIZE  : NATURAL := 16; -- size in segment
  EBS_AD_OFST  : NATURAL := 0;  -- offset in segment
  EBS_IN_AUX0  : NATURAL := 0;  -- 1st Auxiliary Analog Input number
  EBS_IN_AUX1  : NATURAL := 0;  -- 2nd Auxiliary Analog Input number
  TYPE_DESC    : NATURAL := 16#00000000#; -- Type description target 
  TEMP_LOW     : NATURAL := 50; -- temperature lower threshold dC
  TEMP_HIGH    : NATURAL := 60  -- temperature upper threshold dC
);

port (

-- E-bone interface
  eb_clk_i     : in std_logic;                           -- system clock
  ebs_i        : in ebs32_i_typ;
  ebs_o        : out ebs32_o_typ;

-- System monitor analog inputs
  VP_in        : in std_logic;                           -- Dedicated Analog Input Pair
  VN_in        : in std_logic;
  VAUXP0_in    : in std_logic;                           -- 1st Auxiliary Analog Input Pair
  VAUXN0_in    : in std_logic;
  VAUXP1_in    : in std_logic;                           -- 2nd Auxiliary Analog Input Pair
  VAUXN1_in    : in std_logic;

-- Temperature alarm
  temp_alarm_o : out std_logic
);

end component;

end package ebs_sysm_pkg;
