--------------------------------------------------------------------------
--
-- E-bone - System Monitor Register slave
--
--------------------------------------------------------------------------
--
-- Version  Date      Author   Comment
-- 1.0      02/12/10  Le Caer  1st release
-- 1.1      11/05/11  Le Caer  Updated to E-bone 1.2
-- 2.0      05/03/12  Le Caer  Updated from ebs_sysm_vx6
-- 2.1      26/03/13  Le Caer  Updated for TYPE_DESC parameter
-- 2.2      30/09/13    herve  Record i/o, pipelined inputs
-- 2.3      18/12/13  Le Caer  Version (two nibbles) status offset 15
-- 2.4      26/02/15    herve  Added temperature alarm output
-- 2.5      05/09/16  Le Caer  XADC Version 3.3 update, VAUXP0..1 readout
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- temp dC = (ADC * 504)/4096 - 273
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

entity ebs_sysm is

generic (
  EBS_AD_RNGE  : NATURAL := 12; -- short adressing range
  EBS_AD_BASE  : NATURAL := 1;  -- usual IO segment
  EBS_AD_SIZE  : NATURAL := 16; -- size in segment
  EBS_AD_OFST  : NATURAL := 0;  -- offset in segment
  EBS_IN_AUX0  : NATURAL := 0;  -- 1st Auxiliary Analog Input number
  EBS_IN_AUX1  : NATURAL := 0;  -- 2nd Auxiliary Analog Input number
  TYPE_DESC    : NATURAL := 16#00000000#; -- Type description target 
  TEMP_LOW     : NATURAL := 70; -- temperature lower threshold dC
  TEMP_HIGH    : NATURAL := 80  -- temperature upper threshold dC
);
port (

-- E-bone interface
  eb_clk_i     : in std_logic;                           -- system clock
  ebs_i        : in ebs32_i_typ;
  ebs_o        : out ebs32_o_typ;

-- System monitor analog inputs
  VP_in        : in std_logic;                           -- Dedicated Analog Input Pair
  VN_in        : in std_logic;
  VAUXP0_in    : in std_logic;                           -- 1st Auxiliary Analog Input Pair
  VAUXN0_in    : in std_logic;
  VAUXP1_in    : in std_logic;                           -- 2nd Auxiliary Analog Input Pair
  VAUXN1_in    : in std_logic;

-- Temperature alarm
  temp_alarm_o : out std_logic
);
end ebs_sysm;

--------------------------------------
architecture rtl of ebs_sysm is
--------------------------------------
component sysm_regs is

generic (
  EBS_IN_AUX0   : natural := 0;                          -- 1st Auxiliary Analog Input number
  EBS_IN_AUX1   : natural := 0;                          -- 2nd Auxiliary Analog Input number
  TRGT_DESC     : NATURAL := 16#00#;                     -- Type target description
  ADC_LOW       : bit_vector(15 downto 0);
  ADC_HIGH      : bit_vector(15 downto 0)
);

port (
  clk           : in  std_logic;
  rst           : in  std_logic;

  VP_in         : in  std_logic;
  VN_in         : in  std_logic;
  VAUXP0_in     : in  std_logic;
  VAUXN0_in     : in  std_logic;
  VAUXP1_in     : in  std_logic;
  VAUXN1_in     : in  std_logic;

  sysmon_regs_o : out std32_a(15 downto 0);
  temp_alarm_o  : out std_logic
);
end component;

function get_trgt_desc (
  full_type_desc : NATURAL
)
return NATURAL is
  variable full_type_desc_high : std_logic_vector(7 downto 0);
  variable full_type_desc_stdl : std_logic_vector(31 downto 0);
begin
  full_type_desc_stdl := std_logic_vector(to_unsigned(full_type_desc, 32));
	full_type_desc_high := full_type_desc_stdl(31 downto 24);
	case full_type_desc_high is
	  when X"53"  => return 16#53#;
		when X"60"  => return 16#60#;
		when X"63"  => return 16#63#;
		when X"71"  => return 16#71#;
		when X"72"  => return 16#72#;
		when X"73"  => return 16#73#;
		when others => return 16#00#;
	end case;
end get_trgt_desc;

type alog is array(16 downto 0) of natural;
subtype rd_typ is std32_a(EBS_AD_SIZE - 1 downto 0);
type state_typ is (iddle, adrs, rd1, bcc1, abort, error);

constant GND16        : std16 := (others => '0');
constant UPRANGE      : NATURAL := NLog2(EBS_AD_SIZE) - 1;
constant TRGT_DESC    : NATURAL := get_trgt_desc(TYPE_DESC);

constant ADC_LOW  : bit_vector(15 downto 0) 
         := to_bitvector(std_logic_vector(to_unsigned((((TEMP_LOW  + 273)*4096)/504)*16, 16)));
constant ADC_HIGH : bit_vector(15 downto 0) 
         := to_bitvector(std_logic_vector(to_unsigned((((TEMP_HIGH + 273)*4096)/504)*16, 16)));


signal state, nextate : state_typ;
signal dk             : std_logic := '0';                -- slave addrs & data acknowledge
signal s_dk           : std_logic := '0';                -- acknowledge registered
signal berr           : std_logic := '0';                -- addressing error
signal ovf_err        : std_logic := '0';                -- offset overflow error
signal myself         : std_logic := '0';                -- slave selected
signal eof            : std_logic := '0';                -- slave end of frame
signal ofld           : std_logic := '0';                -- offset counter load
signal ofen           : std_logic := '0';                -- offset counter enable
signal ofst           : unsigned(UPRANGE + 1 downto 0);  -- offset counter in burst, MSbit is overflow
signal rmux           : std32;
signal sysmon_regs    : std32_a(EBS_AD_SIZE -1 downto 0);

signal eb_rst_i  : std_logic;
signal eb_bmx_i  : std_logic;
signal eb_as_i   : std_logic;
signal eb_eof_i  : std_logic;
signal eb_dat_i  : std32;

signal sys_rst  : std_logic;
signal eb_bmx   : std_logic; -- busy some master, P&R help
signal eb_bcc   : std_logic; -- E-bone broad call starts
signal eb_mine  : std_logic; -- E-bone burst addressing me starts
signal eb_ofst  : std_logic_vector(EBS_AD_RNGE-1 downto 0);

alias eb_bar    : std_logic_vector(1 downto 0) is eb_dat_i(29 downto 28); 
alias eb_amsb   : std_logic_vector(EBS_AD_RNGE - 1 downto UPRANGE + 1) is eb_dat_i(EBS_AD_RNGE - 1 downto UPRANGE + 1);

------------------------------------------------------------------------
begin
------------------------------------------------------------------------

assert EBS_AD_SIZE = 16 report "EBS_AD_SIZE invalid (must be 16)!" severity failure;
assert EBS_AD_OFST mod 2**(UPRANGE + 1) = 0 report "EBS_AD_OFST invalid boundary!" severity failure;
assert EBS_IN_AUX0 < 17 report "EBS_IN_AUX0 invalid (must be 0..16)!" severity failure;
assert EBS_IN_AUX1 < 17 report "EBS_IN_AUX1 invalid (must be 0..16)!" severity failure;
assert (EBS_IN_AUX1 > EBS_IN_AUX0) or (EBS_IN_AUX1 = 0) report "EBS_IN_AUX1 must be > EBS_IN_AUX0!" severity failure;

process(eb_clk_i, eb_rst_i)
begin
  if eb_rst_i = '1' then
    sys_rst <= '1';
  elsif rising_edge(eb_clk_i) then
    sys_rst <= eb_rst_i;
  end if;
end process;

sysm : sysm_regs

generic map (
  EBS_IN_AUX0   => EBS_IN_AUX0,
  EBS_IN_AUX1   => EBS_IN_AUX1,
  TRGT_DESC     => TRGT_DESC,
  ADC_LOW       => ADC_LOW,
  ADC_HIGH      => ADC_HIGH
)

port map (
  clk           => eb_clk_i,
  rst           => sys_rst,

  VP_in         => VP_in,
  VN_in         => VN_in,
  VAUXP0_in     => VAUXP0_in,
  VAUXN0_in     => VAUXN0_in,
  VAUXP1_in     => VAUXP1_in,
  VAUXN1_in     => VAUXN1_in,

  sysmon_regs_o => sysmon_regs,
  temp_alarm_o  => temp_alarm_o
);

eb_rst_i <= ebs_i.eb_rst;
eb_bmx_i <= ebs_i.eb_bmx;
eb_as_i  <= ebs_i.eb_as;
eb_eof_i <= ebs_i.eb_eof;
eb_dat_i <= ebs_i.eb_dat;

-- Address decode input pipeline
--------------------------------
eb_ofst <= std_logic_vector(to_unsigned(EBS_AD_OFST, EBS_AD_RNGE));
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then 
      eb_bmx  <= eb_bmx_i;          -- P&R help
      eb_bcc  <= '0';
      eb_mine <= '0';
      if    eb_bmx_i   = '1'        -- burst start
        AND eb_as_i = '1' then      -- adrs
         if eb_eof_i = '1' then     -- broad-cast/call
            eb_bcc <= '1';
         elsif (   eb_bar  = std_logic_vector(to_unsigned(EBS_AD_BASE, 2))
               AND eb_amsb = eb_ofst(EBS_AD_RNGE-1 downto UPRANGE+1)) then
            eb_mine <= '1';         -- that's me
         end if;
      end if;
   end if;
end process;

-- E-bone slave FSM
-------------------
process	(eb_clk_i)
begin

  if rising_edge(eb_clk_i) then
    if (eb_rst_i = '1') then
      state  <= iddle;
      myself <= '0';
    else
      myself <= (ofld          -- set 
                 OR myself)    -- lock
								 AND NOT eof;  -- clear
      state  <= nextate;
    end if;
  end if;

end process;

process(state, eb_as_i, eb_eof_i, eb_dat_i, 
               eb_bmx, eb_bcc, eb_mine, ofst)

begin

  nextate <= state;                                      -- stay in same state
  ofld    <= '0';
  ofen    <= '0';
  dk      <= '0';
  eof     <= '0';
  berr    <= '0';

  case state is

    when iddle =>                                        -- sleeping
      if eb_bcc = '1' then
         nextate <= bcc1;
      elsif eb_mine = '1' then
         ofld    <= '1';                                 -- store offset
         if (eb_dat_i(31) = '0')  then
             berr    <= '1';
             nextate <= error;                           -- read only device
         else 
             dk      <= '1';                             -- slave addrs ack
             nextate <= adrs;
         end if;
      end if;

    when adrs =>                                         -- pipelined slave addrs ack (routing help)
      dk <= '1';                                         -- early ack burst
      ofen    <= '1';                                    -- pipeline early init
      nextate <= rd1;

    when rd1 =>                                          -- burst read
      if (eb_bmx = '0') then                             -- abort
        nextate <= abort;
      else   
        dk   <= NOT eb_eof_i;                            -- ack continues till end of burst
        ofen <= '1';
        if eb_eof_i = '1' then
          eof     <= '1';
          nextate <= iddle;
        end if;
      end if;

    when bcc1 =>                                        -- bcall/bcast
      if eb_bmx = '0' OR eb_eof_i = '1' then
         eof <= '1';
         nextate <= iddle;
      end if;

    when error =>                                     -- wait until bus released
      if eb_bmx = '0' OR eb_eof_i = '1' then
         eof <= '1';
         nextate <= iddle;
      else
         berr <= '1';
      end if;

    when abort =>                                     -- quit on abort
      eof     <= '1';
      nextate <= iddle;

  end case;

end process;

-- Offset counter
-----------------
process	(eb_clk_i)
begin

  if rising_edge(eb_clk_i) then

    ovf_err <= '0';
    if (ofld = '1') then                                 -- offset load
      ofst <= unsigned('0' & eb_dat_i(UPRANGE downto 0));
    elsif (ofen = '1') then
      if (ofst(UPRANGE+1) = '1') then                    -- count overflow
        ovf_err <= '1';
      else
        ofst <= ofst + 1;                                -- increment
      end if;
    end if;

  end if;

end process;

-- Register read back
-- Registered output multiplexor
--------------------------------
process(eb_clk_i)
begin

  if rising_edge(eb_clk_i) then
    for i in rd_typ'RANGE loop
      if to_integer(ofst) = i then
        rmux <= sysmon_regs(i);
      end if;
    end loop;
  end if;

end process;


-- E-bone drivers
-----------------
process(eb_clk_i)
begin

  if rising_edge(eb_clk_i) then
    s_dk  <= dk AND NOT ovf_err;
  end if;

end process;

ebs_o.eb_err <= berr  when myself = '1' else '0';
ebs_o.eb_dk  <= s_dk  when myself = '1' else '0';
ebs_o.eb_dat <= rmux  when myself = '1' else (others =>'0');

end rtl;
