################################################################################
#
# E-bone - System Monitor Register slave
# User constraints declarations.
# This file is an example valid with the Xilinx FPGA device defined below :
# FPGA family      : 7
# FPGA device      : xc7k325t
# Speedgrade       : -2
# Package          : fbg676c
#
################################################################################
#
# Version  Date      Author
# 2.5      08/09/16  le caer
#
################################################################################

set_property PACKAGE_PIN N12     [get_ports {VP_in}]
set_property PACKAGE_PIN P11     [get_ports {VN_in}]
set_property PACKAGE_PIN C16     [get_ports {VAUXP0}]
set_property IOSTANDARD LVCMOS25 [get_ports {VAUXP0}]
set_property PACKAGE_PIN B16     [get_ports {VAUXN0}]
set_property IOSTANDARD LVCMOS25 [get_ports {VAUXN0}]
set_property PACKAGE_PIN B17     [get_ports {VAUXP1}]
set_property IOSTANDARD LVCMOS25 [get_ports {VAUXP1}]
set_property PACKAGE_PIN A17     [get_ports {VAUXN1}]
set_property IOSTANDARD LVCMOS25 [get_ports {VAUXN1}]

################################################################################
