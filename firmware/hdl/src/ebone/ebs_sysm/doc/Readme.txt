--------------------------------------------------------------------------------
--
-- EBS_SYSM V2.5 05/09/2016
-- ========================
-- XADC IP update version 3.3
-- Auxiliary Analog Input Pair VAUXP/N_0 and VAUXP/N_1
   instead of VAUXP/N_12 and VAUXP/N_13
