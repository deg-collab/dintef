------------------------------------------------------------------------
--
-- Synchronous FIFO, XILINX SRL16 based 
--
------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     0.1  17/01/11   herve   Preliminary
--     1.0  07/11/11   herve   Improved reset
--     1.1  18/06/13   herve   Added asymetrical input
--                             Bug rd_empty early de-asserted corrected
--     1.2  14/05/17   herve   Fixed init simulation
--
---------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ebs_pkg.Nlog2;

entity fifo_srl is
generic(
   DWIDTH : natural;         -- data width
   DWINP  : natural;         -- port data in width
   ADEPTH : natural := 16    -- FIFO depth
);
port (
   arst     : in  std_logic; -- asyn reset
   clk      : in  std_logic; -- clock
   wr_en    : in  std_logic; -- write enable
   wr_dat   : in  std_logic_vector(DWINP-1 downto 0);
   wr_cnt   : out std_logic_vector(Nlog2(ADEPTH) downto 0);
   wr_afull : out std_logic; -- almost full flag
   wr_full  : out std_logic; -- full flag

   rd_en    : in  std_logic; -- read enable
   rd_dat   : out std_logic_vector(DWIDTH-1 downto 0);
   rd_aempty: out std_logic; -- almost empty flag
   rd_empty : out std_logic  -- empty flag
);
end fifo_srl;

------------------------------------------------------------------------
architecture rtl of fifo_srl is
------------------------------------------------------------------------

type srl_typ is array (ADEPTH-1 downto 0) of std_logic_vector(DWIDTH-1 downto 0);
signal srlf  : srl_typ; -- SRL16 primitives

constant AWIDTH : natural := Nlog2(ADEPTH)-1; 
constant C_AEMPTY : unsigned(AWIDTH downto 0):= (others => '0');
signal C_AFULL    : unsigned(AWIDTH downto 0);
signal rd_ptr     : unsigned(AWIDTH downto 0); -- read/write index pointer

-- Controls
signal rd_en_s  : std_logic; -- read  valid (from synchronized write status)
signal wr_en_s  : std_logic; -- write valid (from synchronized read status)

signal rd_aempti: std_logic; -- almost empty
signal wr_afulli: std_logic; -- almost full

signal rd_empti : std_logic; -- internal
signal wr_fulli : std_logic; -- internal

signal rst1, rst2, rst : std_logic := '1'; -- synchronous reset

-- asymetrical input management

constant ASYM : natural := DWIDTH/DWINP;
type   iregs_typ is array(0 to ASYM-1) of std_logic_vector(DWINP-1 downto 0);

-- Note: register iregs(ASYM-1) is not actually used but simplifies description
signal iregs  : iregs_typ;                        -- input buffer registers
signal icount : unsigned(Nlog2(ASYM)-1 downto 0); -- regs counter select
signal iterm  : std_logic;                        -- terminal count
signal idat   : std_logic_vector(DWIDTH-1 downto 0);

------------------------------------------------------------------------
begin
------------------------------------------------------------------------
assert ADEPTH = 8 OR ADEPTH = 16 OR ADEPTH = 32
       report "ADEPTH must be 8, 16 or 32!" severity failure;

C_AFULL(AWIDTH downto 1) <= (others => '1');
C_AFULL(0)               <=  '0';

-----------------------
-- SRL16/32 (hopefully)
-----------------------
process (clk)
begin
   if rising_edge(clk) then

      if wr_en_s = '1' then
         srlf(0) <= idat;
         for i in 0 to ADEPTH-2 loop
            srlf(i+1) <= srlf(i);
         end loop;
      end if;

   end if;
end process;

rd_dat <= srlf(to_integer(rd_ptr));

------------------------------
-- Synchronous reset generator
------------------------------
process(clk, arst)
begin
   if arst = '1' then
      rst1 <= '1';
   elsif rising_edge(clk) then
      rst1 <= arst;
   end if;
end process;

process(clk)
begin
   if rising_edge(clk) then
      rst2 <= rst1;
      rst  <= rst2 AND rst1; -- glitch filter
   end if;
end process;

-----------------------------------------------
-- Input register stack (asymetrical case only)
-----------------------------------------------
as1: if ASYM > 1 generate
process (clk)
begin
   if rising_edge(clk) then
      if rst = '1' then
         icount <= (others => '0');
      elsif wr_en = '1' then
         icount <= icount + 1; -- roll over counter
      end if;
   end if;
end process;

process (clk)
begin
   if rising_edge(clk) then
      if wr_en = '1' then
         iregs(to_integer(icount)) <= wr_dat;
      end if;
   end if;
end process;

iterm <= '1' when icount = to_unsigned(ASYM-1, Nlog2(ASYM)) else '0';
   as2: for i in 0 to ASYM-2 generate
      idat((i+1)*DWINP-1 downto i*DWINP) <= iregs(i);
   end generate;
end generate;

sym: if ASYM = 1 generate
iterm <= '1'; -- symetrical ports: write any
end generate;

idat(ASYM*DWINP-1 downto (ASYM-1)*DWINP) <= wr_dat;

---------------------------
-- Read/write index counter
---------------------------
-- Simultaneous read and write, then  pointer does not change
process(clk)
begin
   if rising_edge(clk) then
      if rst ='1' then
          rd_ptr <= (others => '1');
      elsif wr_en_s = '1' AND rd_en_s = '0' then -- write
          rd_ptr <= rd_ptr + 1;
      elsif rd_en_s = '1' AND wr_en_s = '0' then -- read
          rd_ptr <= rd_ptr - 1;
      end if;
   end if;
end process;

--------------
-- Write flags
--------------

-- Almost full flag

wr_afulli <= '1' when rd_ptr = C_AFULL
              else '0';

wr_afull <= wr_afulli;

-- Full flag

process(clk)
begin
   if rising_edge(clk) then
      if rst = '1' OR rd_en = '1' then          -- clear
         wr_fulli  <= '0';
      elsif (wr_afulli = '1' AND wr_en_s = '1') -- set
            OR wr_fulli  = '1' then             -- memorized
         wr_fulli  <= '1';
      end if;
   end if;
end process;

wr_full <= wr_fulli;

-- request validity
wr_en_s <= wr_en AND iterm AND NOT (wr_fulli OR rst);

----------------
-- Read flags
----------------

-- Almost empty flag

rd_aempti <= '1' when rd_ptr = C_AEMPTY
              else '0';

rd_aempty <= rd_aempti;

-- Empty flag

process(clk)
begin
   if rising_edge(clk) then
      if wr_en_s = '1' then                    -- clear
         rd_empti  <= '0';
      elsif (rd_aempti = '1' AND rd_en = '1' ) -- set
            OR rst = '1' 
            OR rd_empti  = '1' then            -- memorized
         rd_empti  <= '1';
      end if;
   end if;
end process;

rd_empty <= rd_empti OR rst;

-- request validity
rd_en_s <= rd_en AND NOT rd_empti;

-----------------
-- FIFO occupancy
-----------------
process(clk)
begin
   if rising_edge(clk) then
      wr_cnt <= wr_fulli & std_logic_vector(rd_ptr + 1);
   end if;
end process;

end rtl;
