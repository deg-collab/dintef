------------------------------------------------------------------------
--
-- Tiny double register synchronous fifo 
--
------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     1.0  20/01/15   herve   1st release
--     1.1  14/05/17   herve   Fixed init simulation
--
---------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity fifo_2regs is
generic(
   DWIDTH : natural          -- data width
);
port (
   clk      : in  std_logic; -- clock
   srst     : in  std_logic; -- synhronous reset
   wr_en    : in  std_logic; -- write enable
   wr_dat   : in  std_logic_vector(DWIDTH-1 downto 0);
   wr_afull : out std_logic; -- almost full flag
   wr_full  : out std_logic; -- full flag

   rd_en    : in  std_logic; -- read enable
   rd_dat   : out std_logic_vector(DWIDTH-1 downto 0);
   rd_aempty: out std_logic; -- almost empty flag
   rd_empty : out std_logic  -- empty flag
);
end fifo_2regs;

------------------------------------------------------------------------
architecture rtl of fifo_2regs is
------------------------------------------------------------------------

constant C_AEMPTY : std_logic := '0';
constant C_AFULL  : std_logic := '0';
signal rd_ptr     : std_logic; -- read/write pointer

-- Controls
signal rd_en_s  : std_logic; -- read  valid 
signal wr_en_s  : std_logic; -- write valid 

signal rd_aempti: std_logic; -- almost empty
signal wr_afulli: std_logic; -- almost full

signal rd_empti : std_logic; -- internal
signal wr_fulli : std_logic; -- internal

type regs_typ is array (1 downto 0) of std_logic_vector(DWIDTH-1 downto 0);
signal regs  : regs_typ; -- two register stack

------------------------------------------------------------------------
begin
------------------------------------------------------------------------

-----------------------
-- Register stack
-----------------------
process (clk)
begin
   if rising_edge(clk) then
      if wr_en_s = '1' then
         regs(0) <= wr_dat;
         regs(1) <= regs(0);
      end if;
   end if;
end process;

rd_dat <=      regs(0) when rd_ptr = '0'
          else regs(1);


---------------------------
-- Read/write pointer
---------------------------
-- Simultaneous read and write, then  pointer does not change
process(clk)
begin
   if rising_edge(clk) then
      if srst ='1' then
          rd_ptr <= '1';
      elsif wr_en_s = '1' AND rd_en_s = '0' then -- write
          rd_ptr <= NOT rd_ptr;
      elsif rd_en_s = '1' AND wr_en_s = '0' then -- read
          rd_ptr <= NOT rd_ptr;
      end if;
   end if;
end process;

--------------
-- Write flags
--------------

-- Almost full flag

wr_afulli <= '1' when rd_ptr = C_AFULL
              else '0';

wr_afull <= wr_afulli;

-- Full flag

process(clk)
begin
   if rising_edge(clk) then
      if srst = '1' OR rd_en = '1' then          -- clear
         wr_fulli  <= '0';
      elsif (wr_afulli = '1' AND wr_en_s = '1') -- set
            OR wr_fulli  = '1' then             -- memorized
         wr_fulli  <= '1';
      end if;
   end if;
end process;

wr_full <= wr_fulli;

-- request validity
wr_en_s <= wr_en AND NOT (wr_fulli OR srst);

----------------
-- Read flags
----------------

-- Almost empty flag

rd_aempti <= '1' when rd_ptr = C_AEMPTY
              else '0';

rd_aempty <= rd_aempti;

-- Empty flag

process(clk)
begin
   if rising_edge(clk) then
      if wr_en_s = '1' then                    -- clear
         rd_empti  <= '0';
      elsif (rd_aempti = '1' AND rd_en = '1' ) -- set
            OR srst = '1' 
            OR rd_empti  = '1' then            -- memorized
         rd_empti  <= '1';
      end if;
   end if;
end process;

rd_empty <= rd_empti OR srst;

-- request validity
rd_en_s <= rd_en AND NOT rd_empti;

end rtl;
