--------------------------------------------------------------------------
--
-- E-bone - utilities package
--
--------------------------------------------------------------------------
-- fifo_s
-- fifo_sft
-- fifo_srl
-- fifo_a
-- fifo_aft
-- gray_counter
-- gray2bin
-- bin2gray
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     0.2  17/01/11    herve  Preliminary release
--     1.0  07/11/11    herve  Improved reset
--     1.1  22/03/12    herve  Add fall-through fifos
--     1.2  07/05/13    herve  Fall-through bug single data fixed
--     1.3  03/06/13    herve  Added asymetrical input to fifos
--     1.4  18/11/13    herve  As. FIFO streched reset
--     1.5  12/07/14    herve  fifo_s non-registered output option
--                             Added tiny fifo: fifo_2regs
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ebs_pkg.all;

package eb_common_pkg is

-- Asynchronous FIFO 
--------------------
component fifo_a is
generic(
   DWIDTH : natural;         -- data width
   DWINP  : natural;         -- port data in width
   ADEPTH : natural;         -- FIFO depth
   RAMTYP : string := "auto" -- "auto", "block", "distributed"
);
port (
   arst     : in  std_logic; -- asyn reset
   wr_clk   : in  std_logic; -- write clock
   wr_en    : in  std_logic; -- write enable
   wr_dat   : in  std_logic_vector(DWINP-1 downto 0);
   wr_cnt   : out std_logic_vector(Nlog2(ADEPTH)-1 downto 0);
   wr_afull : out std_logic; -- almost full flag
   wr_full  : out std_logic; -- full flag

   rd_clk   : in  std_logic; -- read clock
   rd_en    : in  std_logic; -- read enable
   rd_dat   : out std_logic_vector(DWIDTH-1 downto 0);
   rd_cnt   : out std_logic_vector(Nlog2(ADEPTH)-1 downto 0);
   rd_aempty: out std_logic; -- almost empty flag
   rd_empty : out std_logic  -- empty flag
);
end component;


-- Asynchronous FIFO fall through
---------------------------------
component fifo_aft is
generic(
   DWIDTH : natural;         -- data width
   DWINP  : natural;         -- port data in width
   ADEPTH : natural;         -- FIFO depth
   RAMTYP : string := "auto" -- "auto", "block", "distributed"
);
port (
   arst     : in  std_logic; -- asyn reset
   wr_clk   : in  std_logic; -- write clock
   wr_en    : in  std_logic; -- write enable
   wr_dat   : in  std_logic_vector(DWINP-1 downto 0);
   wr_cnt   : out std_logic_vector(Nlog2(ADEPTH)-1 downto 0);
   wr_afull : out std_logic; -- almost full flag
   wr_full  : out std_logic; -- full flag

   rd_clk   : in  std_logic; -- read clock
   rd_en    : in  std_logic; -- read enable
   rd_dat   : out std_logic_vector(DWIDTH-1 downto 0);
   rd_cnt   : out std_logic_vector(Nlog2(ADEPTH) downto 0);
   rd_aempty: out std_logic; -- almost empty flag
   rd_empty : out std_logic  -- empty flag
);
end component;

-- Synchronous FIFO 
--------------------
component fifo_s is
generic(
   DWIDTH : natural;         -- fifo data width
   DWINP  : natural;         -- port data in width
   ADEPTH : natural;         -- FIFO depth
   REGOUT : boolean:= true;  -- Registered output
   RAMTYP : string := "auto" -- "auto", "block", "distributed"
);
port (
   arst     : in  std_logic;  -- asyn reset
   clk      : in  std_logic;  -- clock
   wr_en    : in  std_logic;  -- write enable
   wr_dat   : in  std_logic_vector(DWINP-1 downto 0);
   wr_cnt   : out std_logic_vector(Nlog2(ADEPTH) downto 0);
   wr_afull : out std_logic; -- almost full flag
   wr_full  : out std_logic; -- full flag

   rd_en    : in  std_logic;  -- read enable
   rd_dat   : out std_logic_vector(DWIDTH-1 downto 0);
   rd_aempty: out std_logic; -- almost empty flag
   rd_empty : out std_logic  -- empty flag
);
end component;

-- Synchronous FIFO fall through
--------------------------------
component fifo_sft is
generic(
   DWIDTH : natural;         -- data width
   DWINP  : natural;         -- port data in width
   ADEPTH : natural;         -- FIFO depth
   RAMTYP : string := "auto" -- "auto", "block", "distributed"
);
port (
   arst     : in  std_logic; -- asyn reset
   clk      : in  std_logic; -- clock
   wr_en    : in  std_logic; -- write enable
   wr_dat   : in  std_logic_vector(DWINP-1 downto 0);
   wr_cnt   : out std_logic_vector(Nlog2(ADEPTH) downto 0);
   wr_afull : out std_logic; -- almost full flag
   wr_full  : out std_logic; -- full flag

   rd_en    : in  std_logic; -- read enable
   rd_dat   : out std_logic_vector(DWIDTH-1 downto 0);
   rd_aempty: out std_logic; -- almost empty flag
   rd_empty : out std_logic  -- empty flag
);
end component;

-- Synchronous FIFO, XILINX SRL16/32 based 
--------------------------------------------------
component fifo_srl is
generic(
   DWIDTH : natural;         -- data width
   DWINP  : natural;         -- port data in width
   ADEPTH : natural := 16    -- FIFO depth
);
port (
   arst     : in  std_logic; -- asyn reset
   clk      : in  std_logic; -- clock
   wr_en    : in  std_logic; -- write enable
   wr_dat   : in  std_logic_vector(DWINP-1 downto 0);
   wr_cnt   : out std_logic_vector(Nlog2(ADEPTH) downto 0);
   wr_afull : out std_logic; -- almost full flag
   wr_full  : out std_logic; -- full flag

   rd_en    : in  std_logic; -- read enable
   rd_dat   : out std_logic_vector(DWIDTH-1 downto 0);
   rd_aempty: out std_logic; -- almost empty flag
   rd_empty : out std_logic  -- empty flag
);
end component;

-- Tiny double register synchronous fifo 
----------------------------------------
component fifo_2regs is
generic(
   DWIDTH : natural          -- data width
);
port (
   clk      : in  std_logic; -- clock
   srst     : in  std_logic; -- synhronous reset
   wr_en    : in  std_logic; -- write enable
   wr_dat   : in  std_logic_vector(DWIDTH-1 downto 0);
   wr_afull : out std_logic; -- almost full flag
   wr_full  : out std_logic; -- full flag

   rd_en    : in  std_logic; -- read enable
   rd_dat   : out std_logic_vector(DWIDTH-1 downto 0);
   rd_aempty: out std_logic; -- almost empty flag
   rd_empty : out std_logic  -- empty flag
);
end component;

-- Gray counter
---------------
component gray_counter is
generic(
   GWIDTH  : natural -- counter width
);
port ( 
   srst : in std_logic; -- synchronous reset
   clk	: in std_logic; -- clock
   en   : in std_logic; -- enable
   qrst	: in  std_logic_vector(GWIDTH-1 downto 0); -- reset loading value
   cnt	: out std_logic_vector(GWIDTH-1 downto 0)  -- gray count
);
end component;

end package eb_common_pkg;
