-------------------------------------------------------------------------------
--
-- Project     : Series 7 Integrated Block for PCI Express
-- File        : ep_reg.vhd
-- Description : Endpoint Memory: BADR0 4KB BlockRAM banks.
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use work.ep_comp_pkg.all;
use work.ebm0_pcie_b_pkg.all;

library unisim;
use unisim.vcomponents.BUFR;
use unisim.vcomponents.BUFGMUX;
use unisim.vcomponents.ICAPE2;

entity ep_reg is

generic (
  BADR0_MEM                      : INTEGER := 1;
  WATCH_DOG                      : INTEGER := 1;
  AXI_DWIDTH                     : NATURAL := 32;
	TRGT_DESC                      : NATURAL := 16#00#;
	TYPE_DESC                      : NATURAL := 16#00000000#;
	HARD_VERS                      : NATURAL := 16#00000000#;
	DEVICE_ID                      : BIT_VECTOR := X"EB01"
);

port (
  clk                            : in  std_logic;
  trn_rst                        : in  std_logic;
  trn_lnk_up_n                   : in  std_logic;

  rd_addr_i                      : in  std_logic_vector(9 downto 0);
  rd_en_i                        : in  std_logic;
  rd_data_o                      : out std_logic_vector(31 downto 0);

  wr_addr_i                      : in  std_logic_vector(9 downto 0);
  wr_data_i                      : in  std_logic_vector(31 downto 0);
  wr_en_i                        : in  std_logic;

  cfg_bus_number                 : in std_logic_vector(7 downto 0);    -- Provides the assigned bus number for the device, used in TLPs
  cfg_device_number              : in std_logic_vector(4 downto 0);    -- Provides the assigned device number for the device, used in TLPs
  cfg_function_number            : in std_logic_vector(2 downto 0);    -- Provides the assigned function number for the device, used in TLPs
  cfg_status                     : in std_logic_vector(15 downto 0);   -- Status register from the Configuration Space Header
  cfg_command                    : in std_logic_vector(15 downto 0);   -- Command register from the Configuration Space Header
  cfg_dstatus                    : in std_logic_vector(15 downto 0);   -- Device Status register from the Configuration Space Header
  cfg_dcommand                   : in std_logic_vector(15 downto 0);   -- Device Command register from the Configuration Space Header
	cfg_dcommand2                  : in std_logic_vector(15 downto 0);   -- Device Command register 2 from the Configuration Space Header
  cfg_lstatus                    : in std_logic_vector(15 downto 0);   -- Link Status register from the Configuration Space Header
  cfg_lcommand                   : in std_logic_vector(15 downto 0);   -- Link Command register from the Configuration Space Header

  cfg_interrupt_do               : in std_logic_vector(7 downto 0);    -- Interrupt data out, LSB message data field in the MSI structure
  cfg_interrupt_mmenable         : in std_logic_vector(2 downto 0);    -- Interrupt multiple Message enable in the MSI structure
  cfg_interrupt_msienable        : in std_logic;                       -- Interrupt MSI (Message Signaling Interrupt) enable
  cfg_interrupt_msixenable       : in std_logic;                       -- Interrupt MSI-X (Message Signaling Interrupt-X) enable
  cfg_interrupt_msixfm           : in std_logic;                       -- Configuration interrupt MSI-X mask

  pl_initial_link_width          : in std_logic_vector(2 downto 0);    -- PL link width after successful link training
  pl_lane_reversal_mode          : in std_logic_vector(1 downto 0);    -- PL lane reversal mode
  pl_link_gen2_capable           : in std_logic;                       -- PL link Gen2 (5.0 Gb/s) capable if 1
  pl_link_partner_gen2_supported : in std_logic;                       -- PL link partner Gen2 (5.0 Gb/s) capable if 1
  pl_link_upcfg_capable          : in std_logic;                       -- PL link Upconfigure capable if 1
  pl_ltssm_state                 : in std_logic_vector(5 downto 0);    -- PL Link Training and Status State machine report
  pl_received_hot_rst            : in std_logic;                       -- PL Hot reset received
  pl_sel_link_rate               : in std_logic;                       -- PL current link rate, 0: 2.5 Gb/s, 1: 5.0 Gb/s
  pl_sel_link_width              : in std_logic_vector(1 downto 0);    -- PL current link width
	pl_phy_lnk_up                  : in std_logic;                       -- PL Indicates Physical Layer Link Up Status
  pl_tx_pm_state                 : in std_logic_vector(2 downto 0);    -- PL Indicates TX Power Management State
  pl_rx_pm_state                 : in std_logic_vector(1 downto 0);    -- PL Indicates RX Power Management State
  pl_directed_change_done        : in std_logic;                       -- PL Indicates the Directed change is done
  pl_directed_link_auton         : out std_logic;                      -- PL direct autonomous link change
  pl_directed_link_change        : out std_logic_vector(1 downto 0);   -- PL direct link width and/or speed change control
  pl_directed_link_speed         : out std_logic;                      -- PL Target link speed for a directed link change operation
  pl_directed_link_width         : out std_logic_vector(1 downto 0);   -- PL Target link width for a directed link change operation
  pl_upstream_prefer_deemph      : out std_logic;                      -- PL De-emphasis control according to the link speed

  cfg_read_req_o                 : out std_logic;                      -- Configuration Space Header read request
	cfgs_regs_i                    : in std32_x32;                       -- Configuration Space Header registers array

  INtr_service_o                 : out std_logic;                      -- Interrupt acknowledge (unused => direct clear on interrupt status register read)
	INtr_disable_o                 : out std_logic;                      -- cfg_command(10) from the Configuration Space Header Command register
	INtr_enable_o                  : out std_logic;                      -- Global interrupt enable from BADR0 control register 0
	INtr_TLP_end_o                 : out std_logic;                      -- Interrupt on TLP end of transfer enable
	INtr_B_calls_o                 : out std_logic;                      -- Interrupt on Broad-call enable
	INtr_Rqst_nmb_o                : out std_logic_vector(7 downto 0);   -- Legacy interrupt value

  msg_signaling_i                : in std_logic;                       -- TLP messages received
	msg_code_i                     : in std_logic_vector(7 downto 0);    -- Message Code
	msg_data_lsb_i                 : in std_logic_vector(31 downto 0);   -- First and
	msg_data_msb_i                 : in std_logic_vector(31 downto 0);   -- Second dwords of message write TLP

  requester_fmt_o                : out std_logic;                      -- 64-bit address routing Transmit TLP when 1 otherwise 32-bit
  requester_leng_o               : out std_logic_vector(9 downto 0);   -- DW (4-byte) length field encoding corresponding to one TLP transmit operation

  TLP_end_Rqst_i                 : in std_logic;                       -- TLP end of transfer => interrupt module
  bcall_reg_i                    : in std_logic_vector(31 downto 0);   -- Broad-call report register
	B_calls_Rqst_o                 : out std_logic;                      -- Broad-call => interrupt module
	tlp_tx_addr                    : in std_logic_vector(AXI_DWIDTH - 1 downto 0);  -- Address sent for debugging purposes

  FT_on_i                        : in std_logic;                       -- FT has started when 1
  soft_wd_o                      : out std_logic;                      -- External watch dog reset signal
  soft_reset_o                   : out std_logic;                      -- External reset on ctrl_regs(2) write operation with Device_ID & Vendor_ID pattern
  status_led_o                   : out std_logic_vector(1 downto 0);   -- Status led for software test: 00=off, 01=red, 10=green
  ebone_reset_o                  : out std_logic;                      -- E-Bone part reset when high
	bcall_run_i                    : in std_logic;

  err_drop_count                 : in std_logic_vector(14 downto 0);
	err_drop_ovfl                  : in std_logic;
	tdst_rdy_count                 : in std_logic_vector(14 downto 0);
	tdst_rdy_ovfl                  : in std_logic;
  req_len_sup_count              : in std_logic_vector(15 downto 0);
	req_rx_valid_count             : in std_logic_vector(31 downto 0);
	req_tx_valid_count             : in std_logic_vector(31 downto 0);
  ext_ctrl_src_i                 : in std_logic;                       -- External configuration control when 1

  rg_probe_o                     : out std_logic_vector(3 downto 0);   -- Probe output signals linked to status/registers
  prob_mux_o                     : out std_logic_vector(19 downto 0);  -- Output test probes selection

  my_payload_size_o              : out std_logic_vector(2 downto 0);
  user_cnst_i                    : in std_logic_vector(15 downto 0)
);
end ep_reg;

architecture rtl of ep_reg is

signal rst_soft           : std_logic;
signal bit_or             : std_logic;
signal rd_en              : std_logic;
signal wr_en              : std_logic;
signal ofst_regs          : std_logic_vector(3 downto 0);
signal ctrl_wr_en         : std_logic_vector(3 downto 0);
signal sclr_wr_en         : std_logic_vector(3 downto 0);
signal sclr_regs_clr      : std_logic_vector(3 downto 0);
signal low_data_r         : std_logic_vector(31 downto 0);
signal high_data_r        : std_logic_vector(31 downto 0);
signal out_data_m         : std_logic_vector(31 downto 0);
signal addr_lsb           : std_logic_vector(2 downto 0);
signal addr_msb           : std_logic_vector(3 downto 0);
signal mesg_cnt           : std_logic_vector(4 downto 0);
signal tlp_tx_addr_s      : std_logic_vector(63 downto 0);
signal INtr_mask          : std_logic_vector(15 downto 0);
signal INtr_mask_or       : std_logic;
signal bcall_or           : std_logic;
signal bcall_runs         : std_logic;
signal bcall_full         : std_logic_vector(15 downto 0);
signal bcall_load         : std_logic;
signal stat_copy          : std_logic_vector(15 downto 0);
signal stat_regs_new      : std_logic_vector(31 downto 0);

signal ctrl_regs          : std32_x4;
signal stat_regs          : std32_x4;
signal errs_regs          : std32_x4;
signal dbug_regs          : std32_x4;
signal sclr_regs          : std32_x4;
signal mesg_regs          : std32_x32;
signal cfgs_regs          : std32_x32;

signal cfg_read_cmp       : std_logic;

signal soft_wd            : std_logic;
signal soft_reset         : std_logic;
signal boot_clk           : std_logic;
signal boot_cmp           : std_logic;
signal boot_acnt          : std_logic_vector(3 downto 0);
signal boot_acnt_cl       : std_logic;
signal boot_acnt_en       : std_logic;
signal boot_bcnt          : std_logic_vector(21 downto 0);
signal boot_bcnt_tc       : std_logic;

signal ebone_reset_cnt    : std_logic_vector(3 downto 0);
signal ebone_reset_cnt_cl : std_logic;
signal ebone_reset_cnt_en : std_logic;
signal ebone_reset_cnt_tc : std_logic;

signal icap_clk           : std_logic;
signal icap_cs            : std_logic;
signal icap_rw            : std_logic;
signal icap_din           : std_logic_vector(31 downto 0);
signal icap_dout          : std_logic_vector(31 downto 0);
signal icap_reg           : std_logic_vector(31 downto 0);
signal icap_trig          : std_logic;
signal ic_wr_en           : std_logic;

signal Uns_Pattern        : unsigned(31 downto 0);
signal Uns_Ctrlreg        : unsigned(31 downto 0);

type ebone_state_type     is (EBONE_RESET_INIT, EBONE_RESET_TEST, EBONE_RESET_WAIT, EBONE_RESET_END);
signal ebone_reset_state  : ebone_state_type;

type soft_state_type      is (BOOT_INIT, BOOT_TEST, BOOT_END);
signal boot_state         : soft_state_type;

type gate_state_type      is (GATE_INIT, GATE_ON, GATE_END);
signal gate_state         : gate_state_type;

type icap_state_type      is (ICAP_00, ICAP_01, ICAP_02, ICAP_03, ICAP_04, ICAP_05,
                              ICAP_06, ICAP_07, ICAP_08, ICAP_09, ICAP_10, ICAP_11);
signal icap_state         : icap_state_type;

type cfg_state_type       is (CFG_READ_INIT, CFG_READ_TEST, CFG_READ_END);
signal cfg_read_state     : cfg_state_type;

type bcall_imask_type     is (STAT_INIT, 
                              STAT_FTDMA_WR0, STAT_BCALL_WR0, STAT_BCALL_WR1, STAT_BCALL_WR2,
                              STAT_IMASK_WR0, STAT_IMASK_WR1, STAT_IMASK_WR2, STAT_IMASK_WR3);
signal bcall_imask_state  : bcall_imask_type;

begin

rg_probe_o(0) <= trn_rst;     -- 0
rg_probe_o(1) <= soft_wd;     -- 1
rg_probe_o(2) <= soft_reset;  -- 2
rg_probe_o(3) <= bit_or;      -- 3

badr0_mem_inst : if BADR0_MEM = 1 generate

  ep_badr_m0 : ep_dp_ram

  generic map (
    ADDR_WR_SIZE => 9,
    WORD_WR_SIZE => 32,
    ADDR_RD_SIZE => 9,
    WORD_RD_SIZE => 32
  )

  port map (
    clka         => clk,
    wea          => wr_en,
    addra        => wr_addr_i(8 downto 0),
    dina         => wr_data_i,
    clkb         => clk,
    enb          => rd_en,
    addrb        => rd_addr_i(8 downto 0),
    doutb        => out_data_m
  );

end generate badr0_mem_inst;

wd_inst : if WATCH_DOG = 1 generate

  wd_inst : ep_wd

  generic map (
    TRGT_DESC => TRGT_DESC
  )

  port map (
    clk       => clk,
    trn_rst   => trn_rst,
    rst_soft  => rst_soft,
    soft_wd   => soft_wd,
		FT_on_i   => FT_on_i
	);

end generate wd_inst;

bufr_kx7_gene : if (TRGT_DESC = 16#71#) or (TRGT_DESC = 16#72#) or (TRGT_DESC = 16#73#) generate
------------------------------------------------------------------------------------------------

  bufr_boot_i : BUFR
  generic map (
    SIM_DEVICE  => "7SERIES",
    BUFR_DIVIDE => "8"
  )
  port map (
    O           => boot_clk,
    CE          => '1',
    CLR         => trn_rst,
    I           => boot_acnt(1)
  );

  bufr_icap_i : BUFR
  generic map (
    SIM_DEVICE  => "7SERIES",
    BUFR_DIVIDE => "4"
  )
  port map (
    O           => icap_clk,
    CE          => '1',
    CLR         => trn_rst,
    I           => clk
  );

  icap_i : ICAPE2
  generic map (
    DEVICE_ID         => DEVICE_ID,
    ICAP_WIDTH        => "X32",
    SIM_CFG_FILE_NAME => "NONE"
  )
  port map (
    O                 => icap_dout,
    CLK               => icap_clk,
    CSIB              => icap_cs,
    I                 => icap_din,
    RDWRB             => icap_rw
  );

  Uns_Pattern <= unsigned(cfgs_regs(0));

end generate bufr_kx7_gene;
---------------------------

Uns_Ctrlreg <= unsigned(ctrl_regs(2));

tlp_tx_addr32_gene : if (AXI_DWIDTH = 32) generate

  tlp_tx_addr_s(31 downto 0)  <= tlp_tx_addr(31 downto 0);
  tlp_tx_addr_s(63 downto 32) <= (others => '0');

end generate tlp_tx_addr32_gene;

tlp_tx_addr64_gene : if (AXI_DWIDTH = 64) generate

  tlp_tx_addr_s(31 downto 0)  <= tlp_tx_addr(31 downto 0);
  tlp_tx_addr_s(63 downto 32) <= tlp_tx_addr(63 downto 32);

end generate tlp_tx_addr64_gene;

tlp_tx_addr128_gene : if (AXI_DWIDTH = 128) generate

  tlp_tx_addr_s(31 downto 0)  <= tlp_tx_addr(31 downto 0);
  tlp_tx_addr_s(63 downto 32) <= tlp_tx_addr(63 downto 32);

end generate tlp_tx_addr128_gene;

cfg_read_cmp       <= '1' when (ctrl_regs(3)(31) = '1') else '0';
boot_cmp           <= '1' when (Uns_Ctrlreg = Uns_Pattern) else '0';
boot_bcnt_tc       <= '1' when (boot_bcnt = "1111111111111111111111") else '0';
ebone_reset_cnt_tc <= '1' when (ebone_reset_cnt = "1011") else '0';
rd_en              <= '1' when ((rd_en_i = '1') and (rd_addr_i(9) = '1')) else '0';
wr_en              <= '1' when ((wr_en_i = '1') and (wr_addr_i(9) = '1')) else '0';

bcall_full         <= bcall_reg_i(15 downto 1) & TLP_end_Rqst_i;
bit_or             <= '0' when ((stat_copy = "0000000000000000") and (TLP_end_Rqst_i = '0')) else '1';
bcall_or           <= '0' when (bcall_full = "0000000000000000") else '1';
bcall_load         <= '1' when ((bcall_or = '1') and (bcall_runs = '1') and (bcall_run_i = '0')) else '0';
INtr_mask_or       <= '0' when (INtr_mask = "0000000000000000") else '1';

gene_0 : for i in 3 downto 0 generate
	ofst_regs(i)  <= '1' when (conv_integer(wr_addr_i(1 downto 0)) = i) else '0';
	ctrl_wr_en(i) <= '1' when ((wr_en_i = '1') and (wr_addr_i(9 downto 2) = "00000000") and (ofst_regs(i) = '1')) else '0';
	sclr_wr_en(i) <= '1' when ((wr_en_i = '1') and (wr_addr_i(9 downto 2) = "00010100") and (ofst_regs(i) = '1')) else '0';
end generate;

gene_1 : for i in 31 downto 0 generate
  cfgs_regs(i) <= cfgs_regs_i(i);
end generate;

my_payload_size_o <= ctrl_regs(0)(18 downto 16);
INtr_TLP_end_o    <= ctrl_regs(0)(8);
INtr_B_calls_o    <= ctrl_regs(0)(9);
INtr_Rqst_nmb_o   <= ctrl_regs(0)(7 downto 0);
INtr_enable_o     <= ctrl_regs(0)(31) or ext_ctrl_src_i;
INtr_disable_o    <= cfg_command(10);
INtr_service_o    <= sclr_regs(1)(0);
prob_mux_o        <= ctrl_regs(1)(19 downto 0);

pl_directed_link_auton    <= ctrl_regs(3)(0) when (ctrl_regs(1)(31) = '1') else '0';
pl_directed_link_change   <= ctrl_regs(3)(2 downto 1) when (ctrl_regs(1)(31) = '1') else "00";
pl_directed_link_speed    <= ctrl_regs(3)(3) when (ctrl_regs(1)(31) = '1') else '0';
pl_directed_link_width    <= ctrl_regs(3)(5 downto 4) when (ctrl_regs(1)(31) = '1') else "00";
pl_upstream_prefer_deemph <= ctrl_regs(3)(6) when (ctrl_regs(1)(31) = '1') else '0';

soft_wd_o      <= soft_wd;
soft_reset     <= boot_bcnt_tc;
soft_reset_o   <= soft_reset;
B_calls_Rqst_o <= bit_or;

stat_regs(0)(0)            <= not trn_lnk_up_n;
stat_regs(0)(5 downto 1)   <= cfg_lstatus(8 downto 4);
stat_regs(0)(6)            <= '1' when ((AXI_DWIDTH = 64) or (AXI_DWIDTH > 128)) else '0';
stat_regs(0)(7)            <= '1' when ((AXI_DWIDTH = 128) or (AXI_DWIDTH > 128)) else '0';
stat_regs(0)(15 downto 8)  <= cfg_bus_number;
stat_regs(0)(20 downto 16) <= cfg_device_number;
stat_regs(0)(23 downto 21) <= cfg_function_number;
stat_regs(0)(26 downto 24) <= cfg_dcommand(7 downto 5);
stat_regs(0)(27)           <= cfg_command(10);
stat_regs(0)(30 downto 28) <= cfg_interrupt_mmenable;
stat_regs(0)(31)           <= cfg_interrupt_msienable;

stat_regs(2) <= std_logic_vector(to_unsigned(HARD_VERS, 32));
stat_regs(3) <= std_logic_vector(to_unsigned(TYPE_DESC, 32));

addr_lsb <= rd_en_i & rd_addr_i(3 downto 2);
addr_msb <= rd_en_i & rd_addr_i(6 downto 4);

with addr_lsb select
  low_data_r <= ctrl_regs(conv_integer(rd_addr_i(1 downto 0))) when "100",
                stat_regs(conv_integer(rd_addr_i(1 downto 0))) when "101",
                errs_regs(conv_integer(rd_addr_i(1 downto 0))) when "110",
                dbug_regs(conv_integer(rd_addr_i(1 downto 0))) when "111",
                (others => '0')                                when others;

with addr_msb select
  high_data_r <= low_data_r                                          when "1000",
                 mesg_regs(conv_integer(rd_addr_i(3 downto 0)) + 0)  when "1001",
                 mesg_regs(conv_integer(rd_addr_i(3 downto 0)) + 16) when "1010",
                 cfgs_regs(conv_integer(rd_addr_i(3 downto 0)) + 0)  when "1011",
                 cfgs_regs(conv_integer(rd_addr_i(3 downto 0)) + 16) when "1100",
                 (others => '0')                                     when others;

rd_data_o <= out_data_m when (rd_addr_i(9) = '1') else high_data_r;

process(clk, trn_rst)
begin

  if (trn_rst = '1') then

    bcall_runs       <= '0';
    requester_fmt_o  <= '0';
		requester_leng_o <= (others => '0');
		status_led_o     <= (others => '0');

    for i in 0 to 3 loop
      ctrl_regs(i) <= (others => '0');
			errs_regs(i) <= (others => '0');
			dbug_regs(i) <= (others => '0');
		end loop;

	elsif (clk'event and clk = '1') then

    bcall_runs       <= bcall_run_i;
    requester_fmt_o  <= ctrl_regs(0)(19) or ext_ctrl_src_i;
    requester_leng_o <= ctrl_regs(0)(29 downto 20);
    status_led_o     <= ctrl_regs(0)(13 downto 12);

    dbug_regs(0) <= tlp_tx_addr_s(31 downto 0);
		dbug_regs(1) <= tlp_tx_addr_s(63 downto 32);
		dbug_regs(2) <= std_logic_vector(to_unsigned(TRGT_DESC, 8)) & std8_null & user_cnst_i;
		dbug_regs(3)(2 downto 0)   <= pl_initial_link_width;
		dbug_regs(3)(4 downto 3)   <= pl_lane_reversal_mode;
		dbug_regs(3)(5)            <= pl_link_gen2_capable;
		dbug_regs(3)(6)            <= pl_link_partner_gen2_supported;
		dbug_regs(3)(7)            <= pl_link_upcfg_capable;
		dbug_regs(3)(13 downto 8)  <= pl_ltssm_state;
		dbug_regs(3)(14)           <= pl_received_hot_rst;
		dbug_regs(3)(15)           <= pl_sel_link_rate;
		dbug_regs(3)(17 downto 16) <= pl_sel_link_width;
		dbug_regs(3)(18)           <= pl_phy_lnk_up;
		dbug_regs(3)(21 downto 19) <= pl_tx_pm_state;
		dbug_regs(3)(23 downto 22) <= pl_rx_pm_state;
		dbug_regs(3)(24)           <= pl_directed_change_done;

    errs_regs(0) <= err_drop_ovfl & err_drop_count & tdst_rdy_ovfl & tdst_rdy_count;
    errs_regs(1) <= std16_null & req_len_sup_count;
    errs_regs(2) <= req_tx_valid_count;
    errs_regs(3) <= req_rx_valid_count;

    for i in 0 to 3 loop
			if ctrl_wr_en(i) = '1' then
				ctrl_regs(i) <= wr_data_i;
			end if;
		end loop;

	end if;

end process;

process(clk, trn_rst)
begin

  if (trn_rst = '1') then

    for i in 0 to 3 loop
      sclr_regs(i)     <= (others => '0');
			sclr_regs_clr(i) <= '0';
		end loop;

	elsif (clk'event and clk = '1') then

    for i in 0 to 3 loop
			if (sclr_wr_en(i) = '1') then
				sclr_regs(i)     <= wr_data_i;
				sclr_regs_clr(i) <= sclr_wr_en(i);
			elsif (sclr_regs_clr(i) = '1') then
			  sclr_regs(i)     <= (others => '0');
				sclr_regs_clr(i) <= '0';
			end if;
		end loop;

	end if;

end process;

process(clk, trn_rst)
begin

  if (trn_rst = '1') then

    mesg_cnt <= (others => '0');

    for i in 0 to 31 loop
      mesg_regs(i) <= (others => '0');
		end loop;

	elsif (clk'event and clk = '1') then

    if (msg_signaling_i = '1') then
		  mesg_regs(conv_integer(mesg_cnt) + 0) <= std24_null & msg_code_i;
			mesg_regs(conv_integer(mesg_cnt) + 1) <= msg_data_lsb_i;
			mesg_regs(conv_integer(mesg_cnt) + 2) <= msg_data_msb_i;
			mesg_regs(conv_integer(mesg_cnt) + 3) <= (others => '0');
      mesg_cnt(4 downto 2)                  <= mesg_cnt(4 downto 2) + 1;
		end if;

	end if;

end process;

process(clk, trn_rst, boot_acnt_cl)
begin

  if ((trn_rst = '1') or (boot_acnt_cl = '1')) then

    boot_acnt <= (others => '0');

	elsif (clk'event and clk = '1') then

    if (boot_acnt_en = '1') then
		  boot_acnt <= boot_acnt + 1;
		end if;

	end if;

end process;

process(boot_clk, trn_rst, boot_acnt_cl)
begin

  if ((trn_rst = '1') or (boot_acnt_cl = '1')) then

    boot_bcnt <= (others => '0');

	elsif (boot_clk'event and boot_clk = '1') then

    if (boot_bcnt_tc = '0') then
      boot_bcnt <= boot_bcnt + 1;
		end if;

	end if;

end process;

process(clk, trn_rst, ebone_reset_cnt_cl)
begin

  if ((trn_rst = '1') or (ebone_reset_cnt_cl = '1')) then

    ebone_reset_cnt <= (others => '0');

	elsif (clk'event and clk = '1') then

    if (ebone_reset_cnt_en = '1') then
		  ebone_reset_cnt <= ebone_reset_cnt + 1;
		end if;

	end if;

end process;

-- ==================================================
-- Bcall and Interrupt mask register modified process
-- ==================================================
process begin

  wait until rising_edge(clk);

  if (trn_rst = '1') then

    stat_regs(1)      <= "00000000" & cfg_interrupt_do & "0000000000000000";
    stat_regs_new     <= (others => '0');
    stat_copy         <= (others => '0');
		INtr_mask         <= (others => '0');
    bcall_imask_state <= STAT_INIT;

	else

    case (bcall_imask_state) is

		  when STAT_INIT =>

        if (sclr_wr_en(0) = '1') then
				  bcall_imask_state <= STAT_IMASK_WR0;
        elsif (TLP_end_Rqst_i = '1') then
				  bcall_imask_state <= STAT_FTDMA_WR0;
				elsif (bcall_load = '1') then
				  bcall_imask_state <= STAT_BCALL_WR0;
				end if;

-----------------------------------------------------

      when STAT_IMASK_WR0 =>

        INtr_mask         <= stat_copy and (not sclr_regs(0)(15 downto 0));
        bcall_imask_state <= STAT_IMASK_WR1;

      when STAT_IMASK_WR1 =>

        stat_regs(1)      <= INtr_mask_or & "0000000" & cfg_interrupt_do & INtr_mask(15 downto 1) & TLP_end_Rqst_i;
        bcall_imask_state <= STAT_IMASK_WR2;

      when STAT_IMASK_WR2 =>

        stat_copy         <= stat_regs(1)(15 downto 0);
        bcall_imask_state <= STAT_IMASK_WR3;

      when STAT_IMASK_WR3 =>

        if (sclr_wr_en(0) = '0') then
          bcall_imask_state <= STAT_INIT;
				end if;

-----------------------------------------------------

      when STAT_FTDMA_WR0 =>

        stat_regs_new     <= "10000000" & cfg_interrupt_do & "000000000000000" & '1';
        bcall_imask_state <= STAT_BCALL_WR1;

-----------------------------------------------------

      when STAT_BCALL_WR0 =>

        stat_regs_new     <= "10000000" & cfg_interrupt_do & bcall_full(15 downto 1) & TLP_end_Rqst_i;
        bcall_imask_state <= STAT_BCALL_WR1;

      when STAT_BCALL_WR1 =>

        stat_regs(1)      <= stat_regs(1) or stat_regs_new;
        bcall_imask_state <= STAT_BCALL_WR2;

      when STAT_BCALL_WR2 =>

        stat_copy         <= stat_regs(1)(15 downto 0);
        bcall_imask_state <= STAT_INIT;

-----------------------------------------------------

			when others =>

        bcall_imask_state <= STAT_INIT;

		end case;

	end if;

end process;

process begin

  wait until rising_edge(clk);

  if (trn_rst = '1') then

    cfg_read_req_o <= '0';
		cfg_read_state <= CFG_READ_INIT;

  else

    case (cfg_read_state) is

      when CFG_READ_INIT =>

        if (ctrl_wr_en(3) = '1') then
				  cfg_read_state <= CFG_READ_TEST;
				end if;

			when CFG_READ_TEST =>

        if (cfg_read_cmp = '1') then
				  cfg_read_req_o <= '1';
				end if;

        cfg_read_state <= CFG_READ_END;

			when CFG_READ_END =>

        if (ctrl_wr_en(3) = '0') then
          cfg_read_state <= CFG_READ_INIT;
				end if;

        cfg_read_req_o <= '0';

			when others =>

        cfg_read_state <= CFG_READ_INIT;

		end case;

  end if;

end process;

process begin

  wait until rising_edge(clk);

  if (trn_rst = '1') then

    ebone_reset_o      <= '0';
		ebone_reset_cnt_en <= '0';
    ebone_reset_cnt_cl <= '1';
		ebone_reset_state  <= EBONE_RESET_INIT;

  else

    case (ebone_reset_state) is

      when EBONE_RESET_INIT =>

        if (sclr_wr_en(3) = '1') then                                  -- ebone reset self-clear register 3 bit 31 -> next implementation
				  ebone_reset_state <= EBONE_RESET_TEST;
				end if;

			when EBONE_RESET_TEST =>

        if (sclr_regs(3)(31) = '1') then                               -- ebone reset self-clear register 3 bit 31 -> next implementation
				  ebone_reset_o      <= '1';
					ebone_reset_cnt_en <= '1';
          ebone_reset_cnt_cl <= '0';
					ebone_reset_state  <= EBONE_RESET_WAIT;
				else
				  ebone_reset_state <= EBONE_RESET_END;
				end if;

			when EBONE_RESET_WAIT =>

        if (ebone_reset_cnt_tc = '1') then
				  ebone_reset_o      <= '0';
					ebone_reset_cnt_en <= '0';
					ebone_reset_cnt_cl <= '1';
					ebone_reset_state  <= EBONE_RESET_END;
				end if;

			when EBONE_RESET_END =>

        if (ctrl_wr_en(1) = '0') then                                  -- Test on ctrl_wr_en(1) to be removed on next implementation
				  ebone_reset_state <= EBONE_RESET_INIT;
				end if;

			when others =>

        ebone_reset_o      <= '0';
				ebone_reset_cnt_en <= '0';
        ebone_reset_cnt_cl <= '1';
        ebone_reset_state  <= EBONE_RESET_INIT;

		end case;

	end if;

end process;

process begin

  wait until rising_edge(clk);

  if (trn_rst = '1') then

    rst_soft     <= '0';
    boot_acnt_en <= '0';
    boot_acnt_cl <= '1';
    boot_state   <= BOOT_INIT;

  else

    case (boot_state) is

      when BOOT_INIT =>

        if (ctrl_wr_en(2) = '1') then
				  boot_state <= BOOT_TEST;
				end if;

			when BOOT_TEST =>

        if (boot_cmp = '1') then
					boot_acnt_en <= '1';
					boot_acnt_cl <= '0';
					boot_state   <= BOOT_END;
				else
          rst_soft    <= '1';
          boot_state  <= BOOT_END;
				end if;

      when BOOT_END =>

        if (ctrl_wr_en(2) = '0') then
				  rst_soft   <= '0';
				  boot_state <= BOOT_INIT;
				end if;

			when others =>

        rst_soft     <= '0';
        boot_acnt_en <= '0';
        boot_acnt_cl <= '1';
        boot_state   <= BOOT_INIT;

		end case;

	end if;

end process;

-- IPROG reconfiguration through ICAPE2 process --
--------------------------------------------------

icap_trig <= '1' when (icap_reg = x"5599AA66") else '0';

process begin
  wait until rising_edge(clk);
    if (trn_rst = '1') then
	    ic_wr_en  <= '0';
    else
	    ic_wr_en  <= sclr_wr_en(3);
	  end if;
end process;

process begin
  wait until rising_edge(clk);
    if (trn_rst = '1') then
	    icap_reg <= (others => '0');
    else
		  if (ic_wr_en = '1') then
			  icap_reg <= sclr_regs(3);
			end if;
	  end if;
end process;

process begin

  wait until falling_edge(icap_clk);

    if (trn_rst = '1') then

      icap_cs    <= '1';
      icap_rw    <= '1';
			icap_din   <= x"FFFFFFFF";
	    icap_state <= ICAP_00;

    else

      if (icap_trig = '1') then

        case (icap_state) is

          when ICAP_00 =>

            icap_cs    <= '1';
            icap_rw    <= '0';
			      icap_din   <= x"FFFFFFFF";
	          icap_state <= ICAP_01;

          when ICAP_01 =>

            icap_cs    <= '0';
            icap_rw    <= '0';
			      icap_din   <= x"FFFFFFFF";
	          icap_state <= ICAP_02;

          when ICAP_02 =>

            icap_cs    <= '0';
            icap_rw    <= '0';
			      icap_din   <= x"FFFFFFFF";  -- Dummy word
	          icap_state <= ICAP_03;

          when ICAP_03 =>

            icap_cs    <= '0';
            icap_rw    <= '0';
						icap_din   <= x"5599AA66";  -- Sync word (x"AA995566") swapped
	          icap_state <= ICAP_04;

					when ICAP_04 =>

            icap_cs    <= '0';
            icap_rw    <= '0';
						icap_din   <= x"04000000";  -- No OP (x"20000000") swapped
	          icap_state <= ICAP_05;

					when ICAP_05 =>

            icap_cs    <= '0';
            icap_rw    <= '0';
						icap_din   <= x"0C400080";  -- Write 1 word to WBSTAR (x"30020001") swapped
	          icap_state <= ICAP_06;

					when ICAP_06 =>

            icap_cs    <= '0';
            icap_rw    <= '0';
						icap_din   <= x"00000000";  -- Warm boot start address
	          icap_state <= ICAP_07;

					when ICAP_07 =>

            icap_cs    <= '0';
            icap_rw    <= '0';
						icap_din   <= x"0C000180";  -- Write 1 word to CMD (x"30008001") swapped
	          icap_state <= ICAP_08;

					when ICAP_08 =>

            icap_cs    <= '0';
            icap_rw    <= '0';
						icap_din   <= x"000000F0";  -- IPROG command (x"0000000F") swapped
	          icap_state <= ICAP_09;

          when ICAP_09 =>

            icap_cs    <= '0';
            icap_rw    <= '0';
						icap_din   <= x"04000000";  -- No OP (x"20000000") swapped
	          icap_state <= ICAP_10;

					when ICAP_10 =>

            icap_cs    <= '1';
            icap_rw    <= '0';
						icap_din   <= x"04000000";  -- No OP (x"20000000") swapped
	          icap_state <= ICAP_11;

          when ICAP_11 =>

            icap_cs    <= '1';
            icap_rw    <= '1';
						icap_din   <= x"04000000";  -- No OP (x"20000000") swapped
	          icap_state <= ICAP_11;

		      when others =>

            icap_cs    <= '1';
            icap_rw    <= '1';
						icap_din   <= x"AAAAAAAA";
            icap_state <= ICAP_00;

	      end case;

      else

        icap_cs    <= '1';
        icap_rw    <= '1';
			  icap_din   <= x"FFFFFFFF";
	      icap_state <= ICAP_00;

			end if;

    end if;

end process;

end; -- ep_reg

