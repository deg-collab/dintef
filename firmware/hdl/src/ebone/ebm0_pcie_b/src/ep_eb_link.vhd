----------------------------------------------------------------------------------
--
-- Project    : Series 7 Integrated Block for PCI Express
-- File       : ep_eb_link.vhd
-- Description: PCI Express Endpoint <--> E_Bone link.
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.ep_comp_pkg.all;
use work.ebm0_pcie_b_pkg.all;
use work.ebs_pkg.Nlog2;

entity ep_eb_link is

generic (
  EB_CLK             : REAL := 8.0;                                     -- E-Bone clock period in ns
	EBFT_DWIDTH        : NATURAL := 64;                                   -- FT data width
	BC_PERIOD          : BIT_VECTOR := X"0"                               -- Broad-Call period (0:1us, 1:10us, 2:100us, 3:1ms)
);

port (

  trn_clk            : in std_logic;                                    -- User clock from the endpoint module
  trn_rst            : in std_logic;                                    -- Active high reset signal from the endpoint
	s_axis_tx_tready   : in std_logic;                                    -- Transmit destination ready: the core is ready to accept data when low

--
-- LocalLink Rx/Tx interfacing
--

	ext_brd_data_o     : out std32_x32;                                   -- Memory Read Data array
	ext_brd_done_o     : out std_logic;                                   -- External Memory Read Burst done (E_Bone)
	brd_strt_i         : in std_logic;                                    -- Memory Read Burst start
  bwr_strt_i         : in std_logic;                                    -- Memory Write Burst start
  bwr_be_i           : in std_logic_vector(7 downto 0);                 -- Memory Write Byte Enable
  bwr_data_i         : in std32_x32;                                    -- Memory Write Data
  breq_len_i         : in std_logic_vector(9 downto 0);                 -- Memory Write payload length

  npck_prsnt_i       : in std_logic;                                    -- Indicates the start of a new packet header in m_axis_rx_tdata
	req_compl_i        : in std_logic;                                    -- Completion request running
	compl_done_i       : in std_logic;                                    -- Completion with data done

  Type_Offset_desc_i : in std_logic_vector(31 downto 0);                -- Type/Offset descriptor word for E-Bone interfacing
  E_Bone_trn_i       : in std_logic;                                    -- E_Bone external transaction request
  eb_ft_data_o       : out std_logic_vector(EBFT_DWIDTH - 1 downto 0);  -- TLP transmit data from fast transmitter (fifo output)
  eb_ft_addr_o       : out std_logic_vector(EBFT_DWIDTH - 1 downto 0);  -- TLP transmit address from fast transmitter
	eb_data_size_o     : out std_logic_vector(7 downto 0);                -- TLP data size valid
	eb_last_size_o     : out std_logic_vector(7 downto 0);                -- TLP last size valid
	eb_byte_size_o     : out std_logic_vector(15 downto 0);               -- TLP byte count
	TLP_end_Rqst_o     : out std_logic;                                   -- TLP end of transfer => interrupt module
  tx_ft_ready_o      : out std_logic;                                   -- External TLP dma transmit request start signal
	tx_ft_ready_i      : in std_logic;                                    -- Internal TLP transmit request for missing bytes transfer
	tx_ft_delay_i      : in std_logic;                                    -- Next FT delayed
	tx_tvalid_flag_i   : in std_logic;                                    -- Transmit data valid flag
  tlp_delayed_i      : in std_logic;                                    -- TLP running
  m_axis_rx_tvalid_i : in std_logic;                                    -- Receive Source Ready: Indicates that the core is presenting valid data on m_axis_rx_tdata
  m_axis_rx_tready_i : in std_logic;                                    -- Receive Destination Ready: Indicates that the User Application is ready to accept data on
  INtr_B_calls_i     : in std_logic;                                    -- Interrupt on Broad-call enable
  INtr_gate_i        : in std_logic;                                    -- Interrupt acknowledge running
	bcall_run_o        : out std_logic;                                   -- Broad-call process running
	bcall_reg_o        : out std_logic_vector(31 downto 0);               -- Broad-call report register
  RD_on_i            : in std_logic;                                    -- MRd TLP running
  WR_on_i            : in std_logic;                                    -- MWr TLP running
  FT_on_o            : out std_logic;                                   -- FT has started when 1
  FT_clr_i           : in std_logic;                                    -- FT completed from Tx engine
  wr_busy_eb_o       : out std_logic;                                   -- E_Bone write cycle end when low
  one_us_heartbeat_o : out std_logic;                                   -- One us pulse out trn_clk synchronous
  ebone_reset_i      : in std_logic;                                    -- E-Bone part reset when high
	cfg_dcommand_i     : in std_logic_vector(15 downto 0);                -- Device Command register from the Configuration Space Header

--
-- Master #0 dedicated
--

  eb_m0_clk_o        : out std_logic;                                   -- system clock
  eb_m0_rst_o        : out std_logic;                                   -- synchronous system reset
  eb_m0_brq_o        : out std_logic;                                   -- bus request
  eb_m0_bg_i         : in std_logic;                                    -- bus grant
  eb_m0_as_o         : out std_logic;                                   -- adrs strobe
  eb_m0_eof_o        : out std_logic;                                   -- end of frame
  eb_m0_aef_o        : out std_logic;                                   -- almost end of frame
  eb_m0_dat_o        : out std_logic_vector(31 downto 0);               -- master data write
  eb_dk_i            : in std_logic;                                    -- data acknowledge
  eb_err_i           : in std_logic;                                    -- bus error
  eb_dat_i           : in std_logic_vector(31 downto 0);                -- master data in

--
-- Shared bus
--

  eb_ft_dxt_i        : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);   -- FT data in
	eb_ft_blen_i       : in std_logic_vector(15 downto 0);                -- FT payload size in bytes
	eb_ft_dsz_i        : in std_logic_vector(7 downto 0);                 -- FT data size qualifier
	eb_ft_ss_i         : in std_logic;                                    -- FT size strobe
  eb_bmx_i           : in std_logic;                                    -- busy some master (but FT)
  eb_bft_i           : in std_logic;                                    -- busy FT
  eb_as_i            : in std_logic;                                    -- FT slave adrs strobe
  eb_eof_i           : in std_logic;                                    -- FT slave end of frame
  eb_dk_o            : out std_logic;                                   -- FT slave data acknowledge
  eb_err_o           : out std_logic;                                   -- FT slave bus error

--
-- Debug part
--

  eb_probe_o         : out std_logic_vector(3 downto 0)                 -- Debug output probes

);

end ep_eb_link;
    
architecture ep_eb_rtl of ep_eb_link is

type pload_typ          is array(0 to 7) of unsigned(15 downto 0);
constant pload_tab      : pload_typ := (x"0080", x"0100", x"0200", x"0400",
                                        x"0800", x"1000", x"2000", x"4000");
constant bound_val      : unsigned := x"1000";

type Retry_type         is (RETRY_INIT, RETRY_ON, RETRY_END);
signal Retry_state      : Retry_type;

type broad_cast_type    is (BROAD_CAST_INIT,
                            BROAD_CAST_RQST0, BROAD_CAST_RQST1, BROAD_CAST_RQST2,
                            BROAD_CAST_RQST3, BROAD_CAST_RQST4, BROAD_CAST_RQST,
                            BROAD_CAST_END);
signal broad_cast_state : broad_cast_type;
signal broad_cast_data  : std_logic_vector(31 downto 0);

signal eb_m0_brq        : std_logic;
signal eb_m0_as         : std_logic;
signal eb_m0_eof        : std_logic;
signal eb_m0_aef        : std_logic;
signal eb_dk_c          : std_logic;
signal eb_dk_r          : std_logic;
signal eb_err_r         : std_logic;
signal eb_bmx_s         : std_logic;
signal eb_retry         : std_logic;
signal eb_data_size     : std_logic_vector(7 downto 0);
signal eb_last_size     : std_logic_vector(7 downto 0);
signal eb_byte_size     : std_logic_vector(15 downto 0);
signal bytes_to_keep    : std_logic_vector(7 downto 0);

signal rw_cycle         : std_logic;
signal rd_cycle_wait    : std_logic;
signal wr_busy_eb       : std_logic;
signal end_of_cycle     : std_logic;
signal breq_len         : std_logic_vector(9 downto 0);
signal requester_rcv    : std_logic;
signal seg_desc_fmt     : std_logic_vector(1 downto 0);
signal Type_Offset_desc : std_logic_vector(31 downto 0);
signal cmp_pload_s      : std_logic_vector(3 downto 0);
signal cmp_pload        : std_logic;
signal sup_pload        : std_logic;
signal bcall_run        : std_logic;
signal my_m0_brq        : std_logic;

signal bound_ovfl       : std_logic;
signal bytes_ovfl       : std_logic_vector(15 downto 0);
signal bytes_calc       : std_logic_vector(15 downto 0);

signal one_us_cnt       : std_logic_vector(7 downto 0);
signal one_us_cnt_cl    : std_logic;
signal one_us_cnt_ld    : std_logic;
signal one_us_cnt_en    : std_logic;
signal one_us_cnt_tc    : std_logic;

signal bc_prd_cnt       : std_logic_vector(9 downto 0);
signal bc_prd_cnt_cl    : std_logic;
signal bc_prd_cnt_ld    : std_logic;
signal bc_prd_cnt_en    : std_logic;
signal bc_prd_cnt_tc    : std_logic;
signal bc_prd_cnt_sh    : std_logic_vector(1 downto 0);

signal b_data_cnt       : std_logic_vector(9 downto 0);
signal b_data_cnt_cl    : std_logic;
signal b_data_cnt_en    : std_logic;
signal b_data_cnt_m1    : std_logic;
signal b_data_cnt_m2    : std_logic;

signal wd_counter       : std_logic_vector(9 downto 0);
signal wd_counter_cl    : std_logic;
signal wd_counter_en    : std_logic;

signal rt_ovf_cnt       : std_logic_vector(9 downto 0);
signal rt_ovf_cnt_tc    : std_logic;
signal rt_ovf_rst       : std_logic;
signal ft_ovf_cnt       : std_logic_vector(10 downto 0);
signal ft_ovf_cnt_tc    : std_logic;
signal ft_ovf_rst       : std_logic;
signal ft_err_rst       : std_logic;

signal eb_ff_rst        : std_logic;
signal eb_ff_wr         : std_logic;
signal eb_ff_rd         : std_logic;
signal eb_ff_rd_s       : std_logic;
signal eb_ff_full       : std_logic;
signal eb_ff_empty      : std_logic;
signal eb_ff_cnt        : std_logic_vector(Nlog2(DEPTH_FIFO_S) downto 0);
signal eb_ff_afull      : std_logic;
signal eb_ff_aempty     : std_logic;
signal eb_ff_dout       : std_logic_vector(EBFT_DWIDTH - 1 downto 0);

signal EB_on            : std_logic;
signal FT_on            : std_logic;

begin

-- common implementation
------------------------

fifo_s_i : ep_fifo_s
generic map (
  DWIDTH    => EBFT_DWIDTH,
  DWINP     => EBFT_DWIDTH,
  ADEPTH    => DEPTH_FIFO_S,
  REGOUT    => FALSE,
  RAMTYP    => "auto"
)
port map (
  arst      => eb_ff_rst,
  clk       => trn_clk,
  wr_en     => eb_ff_wr,
  wr_dat    => eb_ft_dxt_i,
  wr_cnt    => eb_ff_cnt,
  wr_afull  => eb_ff_afull,
  wr_full   => eb_ff_full,
  rd_en     => eb_ff_rd,
  rd_dat    => eb_ff_dout,
  rd_aempty => eb_ff_aempty,
  rd_empty  => eb_ff_empty
);

eb_probe_o(0) <= eb_bft_i;   -- 12
eb_probe_o(1) <= eb_as_i;    -- 13
eb_probe_o(2) <= eb_m0_brq;  -- 14
eb_probe_o(3) <= eb_retry;   -- 15

eb_m0_clk_o        <= trn_clk;
eb_m0_brq_o        <= eb_m0_brq;
eb_m0_as_o         <= eb_m0_as;
eb_m0_eof_o        <= eb_m0_eof;
eb_m0_aef_o        <= eb_m0_aef;
eb_dk_o            <= (eb_dk_c or eb_dk_r);
eb_err_o           <= ((eb_err_r and (not eb_dk_c)) or (tlp_delayed_i and eb_dk_c));
FT_on_o            <= FT_on;
wr_busy_eb_o       <= wr_busy_eb;
bcall_run_o        <= bcall_run;
eb_data_size_o     <= eb_data_size;
eb_last_size_o     <= eb_last_size;
one_us_heartbeat_o <= one_us_cnt_tc;
seg_desc_fmt       <= Type_Offset_desc_i(29 downto 28);
cmp_pload          <= '1' when (unsigned(eb_ft_blen_i) >= pload_tab(to_integer(unsigned(cfg_dcommand_i(7 downto 5))))) else '0';
sup_pload          <= '1' when ((cmp_pload = '1') or (cmp_pload_s(0) = '1') or (cmp_pload_s(1) = '1') or (cmp_pload_s(2) = '1') or (cmp_pload_s(3) = '1')) else '0';
b_data_cnt_cl      <= '1' when ((bwr_strt_i = '1') or (brd_strt_i = '1') or (trn_rst = '1')) else '0';
b_data_cnt_m1      <= '1' when (unsigned(b_data_cnt) = (unsigned(breq_len) - 1)) else '0';
b_data_cnt_m2      <= '1' when (unsigned(b_data_cnt) = (unsigned(breq_len) - 2)) else '0';
bc_prd_cnt_en      <= '1' when ((INtr_B_calls_i = '1') and (m_axis_rx_tvalid_i = '0') and (m_axis_rx_tready_i = '1')) else '0';
bc_prd_cnt_cl      <= '1' when ((INtr_B_calls_i = '0') or (trn_rst = '1') or (bc_prd_cnt_ld = '1')) else '0';
one_us_cnt_en      <= '1' when ((INtr_B_calls_i = '1') and (m_axis_rx_tvalid_i = '0') and (m_axis_rx_tready_i = '1')) else '0';
one_us_cnt_cl      <= '1' when ((INtr_B_calls_i = '0') or (trn_rst = '1') or (one_us_cnt_ld = '1')) else '0';
bc_prd_cnt_tc      <= '1' when ((((bc_prd_cnt = X"001") and (BC_PERIOD = X"0")) or
	                               ((bc_prd_cnt = X"00A") and (BC_PERIOD = X"1")) or
													       ((bc_prd_cnt = X"064") and (BC_PERIOD = X"2")) or
													       ((bc_prd_cnt = X"3E8") and (BC_PERIOD = X"3"))) and ((m_axis_rx_tvalid_i = '0') and (m_axis_rx_tready_i = '1'))) else '0';

process (seg_desc_fmt, Type_Offset_desc_i)
begin
  case (seg_desc_fmt) is
    when "00" =>
      Type_Offset_desc <= (others => '0');
		when "01" =>
      Type_Offset_desc <= Type_Offset_desc_i(31 downto 28) & std14_null & Type_Offset_desc_i(13 downto 0);
		when "10" =>
      Type_Offset_desc <= Type_Offset_desc_i(31 downto 28) & std14_null & Type_Offset_desc_i(13 downto 0);
		when "11" =>
      Type_Offset_desc <= Type_Offset_desc_i(31 downto 28) & std12_null & Type_Offset_desc_i(15 downto 0);
    when others =>
      Type_Offset_desc <= (others => '0');
  end case;
end process;

-- =======================
-- E-Bone -> Reset process
-- =======================
process
begin
  wait until rising_edge(trn_clk);
	  if (trn_rst = '1') then
		  eb_m0_rst_o <= '1';
			rt_ovf_rst  <= '0';
      ft_ovf_rst  <= '0';
      ft_err_rst  <= '0';
		else
		  eb_m0_rst_o <= trn_rst or ebone_reset_i or ft_ovf_cnt_tc or rt_ovf_cnt_tc;
			rt_ovf_rst  <= rt_ovf_cnt_tc;
      ft_ovf_rst  <= ft_ovf_cnt_tc;
      ft_err_rst  <= (tlp_delayed_i and eb_dk_c);
		end if;
end process;

-- =====================================
-- E-Bone -> DL/FT/RT time-out processes
-- =====================================
process
begin
  wait until rising_edge(trn_clk);
    if ((trn_rst = '1') or (FT_clr_i = '1') or (ft_ovf_rst = '1')) then
		  ft_ovf_cnt <= (others => '0');
    else
		  if (FT_on = '1') then
			  ft_ovf_cnt <= ft_ovf_cnt + 1;
			end if;
    end if;
end process;

ft_ovf_cnt_tc <= '1' when (ft_ovf_cnt = X"7FF") else '0';

process
begin
  wait until rising_edge(trn_clk);
    if ((trn_rst = '1') or (FT_on = '1') or (rt_ovf_rst = '1')) then
		  rt_ovf_cnt <= (others => '0');
    else
		  if (eb_retry = '1') then
			  rt_ovf_cnt <= rt_ovf_cnt + 1;
			end if;
    end if;
end process;

rt_ovf_cnt_tc <= '1' when (rt_ovf_cnt = X"3FF") else '0';

-- =======================================
-- E-Bone -> Broad Call counters processes
-- =======================================
process(trn_clk, one_us_cnt_cl)
begin
  if (one_us_cnt_cl = '1') then
    one_us_cnt <= (others => '0');
  elsif (trn_clk'event and trn_clk = '1') then
    if (one_us_cnt_en = '1') then
      one_us_cnt <= one_us_cnt + 1;
	  end if;
  end if;
end process;

process(trn_clk, bc_prd_cnt_cl)
begin
  if (bc_prd_cnt_cl = '1') then
    bc_prd_cnt <= (others => '0');
  elsif (trn_clk'event and trn_clk = '1') then
    if ((bc_prd_cnt_en = '1') and (one_us_cnt_tc = '1')) then
      bc_prd_cnt <= bc_prd_cnt + 1;
	  end if;
  end if;
end process;

process
begin
  wait until rising_edge(trn_clk);
	  if (trn_rst = '1') then
		  eb_bmx_s      <= '0';
		  one_us_cnt_ld <= '0';
		  bc_prd_cnt_ld <= '0';
	    bc_prd_cnt_sh <= (others => '0');
		else
		  eb_bmx_s         <= eb_bmx_i;
		  one_us_cnt_ld    <= one_us_cnt_tc;
		  bc_prd_cnt_ld    <= bc_prd_cnt_tc;
	    bc_prd_cnt_sh(0) <= bc_prd_cnt_tc;
	    bc_prd_cnt_sh(1) <= bc_prd_cnt_sh(0);
		end if;
end process;

-- ==============================
-- E-Bone -> Broad Cast processes
-- ==============================
process
begin
  wait until rising_edge(trn_clk);
    if (trn_rst = '1') then
		  TLP_end_Rqst_o   <= '0';
			broad_cast_data  <= (others => '0');
			broad_cast_state <= BROAD_CAST_INIT;
		else
		  case (broad_cast_state) is

			  when BROAD_CAST_INIT =>
          if ((eb_bft_i = '1') and (eb_as_i = '1') and (eb_eof_i = '1')) then
            broad_cast_data <= eb_ft_dxt_i(31 downto 0);
            if (FT_on = '1') then
              broad_cast_state <= BROAD_CAST_RQST4;
            else
              broad_cast_state <= BROAD_CAST_RQST2;
            end if;
          else
            broad_cast_state <= BROAD_CAST_INIT;
          end if;

        when BROAD_CAST_RQST4 =>
          broad_cast_state <= BROAD_CAST_RQST3;

        when BROAD_CAST_RQST3 =>
          broad_cast_state <= BROAD_CAST_RQST2;

        when BROAD_CAST_RQST2 =>
          broad_cast_state <= BROAD_CAST_RQST1;

        when BROAD_CAST_RQST1 =>
          broad_cast_state <= BROAD_CAST_RQST0;

				when BROAD_CAST_RQST0 =>
					broad_cast_state <= BROAD_CAST_RQST;

				when BROAD_CAST_RQST =>
				  if ((eb_bft_i = '0') and (eb_as_i = '0') and (eb_eof_i = '0')) then
					  TLP_end_Rqst_o   <= '1';
						broad_cast_state <= BROAD_CAST_END;
					else
					  broad_cast_state <= BROAD_CAST_INIT;
					end if;

				when BROAD_CAST_END =>
				  TLP_end_Rqst_o   <= '0';
					broad_cast_state <= BROAD_CAST_INIT;

				when others =>
				  TLP_end_Rqst_o   <= '0';
			    broad_cast_data  <= (others => '0');
			    broad_cast_state <= BROAD_CAST_INIT;

			end case;
		end if;
end process;

-- =================================
-- E-Bone -> Data counters processes
-- =================================
process
begin
  wait until rising_edge(trn_clk);
    if ((trn_rst = '1') or (b_data_cnt_cl = '1') or (ft_err_rst = '1')) then
      b_data_cnt <= (others => '0');
	  else
      if (b_data_cnt_en = '1') then
        b_data_cnt <= b_data_cnt + 1;
		  end if;
	  end if;
end process;

process
begin
  wait until rising_edge(trn_clk);
    if ((trn_rst = '1') or (wd_counter_cl = '1') or (ft_err_rst = '1')) then
      wd_counter <= (others => '0');
    else
      if (wd_counter_en = '1') then
        wd_counter <= wd_counter + 1;
      end if;
    end if;
end process;

process
begin
  wait until rising_edge(trn_clk);
	  if (trn_rst = '1') then
		  cmp_pload_s <= (others => '0');
		else
		  cmp_pload_s(0) <= cmp_pload;
		  cmp_pload_s(1) <= cmp_pload_s(0);
		  cmp_pload_s(2) <= cmp_pload_s(1);
		  cmp_pload_s(3) <= cmp_pload_s(2);
		end if;
end process;

-- =======================
-- E-Bone -> Retry process
-- =======================
process
  variable ft_retry_cond : std_logic;
begin
	ft_retry_cond := (eb_bft_i and (not EB_on) and ((not s_axis_tx_tready) or end_of_cycle or
                                                                            RD_on_i or
                                                                            WR_on_i or
                                                                            tlp_delayed_i or
                                                                            tx_ft_delay_i or
                                                                            tx_tvalid_flag_i or
                                                                            tx_ft_ready_i or
                                                                            npck_prsnt_i));
  wait until rising_edge(trn_clk);
	  if (trn_rst = '1') then
		  eb_dk_r     <= '0';
      eb_err_r    <= '0';
		  eb_retry    <= '0';
      Retry_state <= RETRY_INIT;
		else
      case (Retry_state) is
        when RETRY_INIT =>
          if (ft_retry_cond = '1') then
            eb_dk_r      <= '1';
            eb_err_r     <= '1';
            eb_retry     <= '1';
					  Retry_state <= RETRY_ON;
          else
            Retry_state <= RETRY_INIT;
          end if;
        when RETRY_ON =>
          if (ft_retry_cond = '0') then
            Retry_state <= RETRY_END;
          else
            Retry_state <= RETRY_ON;
          end if;
        when RETRY_END =>
          eb_dk_r      <= '0';
          eb_err_r     <= '0';
          eb_retry     <= '0';
					Retry_state <= RETRY_INIT;
			  when others =>
          eb_dk_r     <= '0';
          eb_err_r    <= '0';
		      eb_retry    <= '0';
          Retry_state <= RETRY_INIT;
      end case;
		end if;
end process;

-----------------------------------------------------------------------------------------------------------
EP_EB_64B : if (EBFT_DWIDTH = 64) generate                                 -- ep_eb_64b_link implementation
-----------------------------------------------------------------------------------------------------------

  type ep_eb_type    is (BONE_RST,
                         BURST_BONE_BRQ, BURST_BONE_BG, BURST_BONE_DATA,
												 BURST_BONE_BCALL_0, BURST_BONE_BCALL_1, BURST_BONE_BCALL_2,
												 BURST_FT_ADDR, BURST_FT_SIZE, BURST_FT_DATA,
												 BURST_BONE_WAIT, BURST_BONE_END);
  signal ep_eb_state : ep_eb_type;
	signal tx_ft_ready : std_logic;
	signal eb_ft_addr  : std_logic_vector(EBFT_DWIDTH - 1 downto 0);

  begin

    US_CNT_4 : if (EB_CLK = 4.0) generate
    -------------------------------------
      one_us_cnt_tc <= '1' when (one_us_cnt = x"F8") else '0';
    end generate US_CNT_4;

    US_CNT_8 : if (EB_CLK = 8.0) generate
    -------------------------------------
      one_us_cnt_tc <= '1' when (one_us_cnt = x"7C") else '0';
    end generate US_CNT_8;

    US_CNT_16 : if (EB_CLK = 16.0) generate
    ---------------------------------------
      one_us_cnt_tc <= '1' when (one_us_cnt = x"3E") else '0';
    end generate US_CNT_16;

    tx_ft_ready_o  <= tx_ft_ready;
		eb_ft_addr_o   <= eb_ft_addr;
		eb_byte_size_o <= eb_byte_size;

    bytes_calc <= std_logic_vector((unsigned(eb_ft_addr(15 downto 0) and x"0FFF") + unsigned(eb_byte_size)));
    bound_ovfl <= '1' when (unsigned(bytes_calc) > bound_val) else '0';

    process
		begin
		  wait until rising_edge(trn_clk);
			  if (trn_rst = '1') then
				  bytes_ovfl <= (others => '0');
				else
				  if (bound_ovfl = '1') then
					  bytes_ovfl <= std_logic_vector(unsigned(bytes_calc) - bound_val);
					else
					  bytes_ovfl <= (others => '0');
					end if;
				end if;
    end process;

    process
		begin
      wait until rising_edge(trn_clk);
        eb_ft_data_o <= eb_ft_dxt_i;
    end process;

    -- =======================================================================
    -- Master Requesting E-Bone mastership for Burst or DMA operations process
    -- =======================================================================
    process

    variable ebone_wr_cond : std_logic;
    variable intrn_wr_cond : std_logic;
    variable ebone_rd_cond : std_logic;
    variable intrn_rd_cond : std_logic;
    variable brd_call_cond : std_logic;
    variable ft_begin_cond : std_logic;

    begin

      ebone_wr_cond := (bwr_strt_i and E_Bone_trn_i and (not eb_bft_i) and (not eb_bmx_i) and (not bcall_run) and (not FT_on));
      intrn_wr_cond := (bwr_strt_i and (not E_Bone_trn_i) and (not eb_bft_i) and (not eb_bmx_i));
      ebone_rd_cond := (brd_strt_i and E_Bone_trn_i and (not eb_bft_i) and (not eb_bmx_i) and (not bcall_run) and (not FT_on));
      intrn_rd_cond := (brd_strt_i and (not E_Bone_trn_i) and (not eb_bft_i) and (not eb_bmx_i));
	    brd_call_cond := ((not bwr_strt_i) and (not brd_strt_i) and (not eb_bft_i) and (not eb_bmx_s) and (not npck_prsnt_i) and
                        (not RD_on_i) and (not WR_on_i) and (not FT_on) and (not INtr_gate_i) and (bc_prd_cnt_sh(0)) and (not bc_prd_cnt_sh(1)));
      ft_begin_cond := (eb_bft_i and (not eb_eof_i) and (not bwr_strt_i) and (not brd_strt_i) and
                        (not req_compl_i) and s_axis_tx_tready and (not RD_on_i) and (not WR_on_i) and (not FT_on) and (not tlp_delayed_i) and (not npck_prsnt_i));

      wait until rising_edge(trn_clk);

      if ((trn_rst = '1') or (ft_ovf_rst = '1') or (rt_ovf_rst = '1') or (ft_err_rst = '1')) then

        my_m0_brq      <= '0';
        eb_m0_brq      <= '0';
        eb_m0_as       <= '0';
        eb_m0_eof      <= '0';
		    eb_m0_aef      <= '0';
        eb_m0_dat_o    <= (others => '0');
		    eb_dk_c        <= '0';
        rw_cycle       <= '0';
		    rd_cycle_wait  <= '0';
		    end_of_cycle   <= '0';
		    b_data_cnt_en  <= '0';
				bcall_run      <= '0';
		    bcall_reg_o    <= (others => '0');
		    EB_on          <= '0';
        FT_on          <= '0';
        wr_busy_eb     <= '1';
		    ext_brd_done_o <= '0';
			  wd_counter_en  <= '0';
			  wd_counter_cl  <= '1';
				tx_ft_ready    <= '0';
			  eb_ft_addr     <= (others => '0');
			  eb_data_size   <= (others => '0');
			  eb_last_size   <= (others => '0');
				eb_byte_size   <= (others => '0');
				bytes_to_keep  <= (others => '0');

        for i in 0 to 31 loop
          ext_brd_data_o(i) <= (others => '0');
		    end loop;

        ep_eb_state <= BONE_RST;

	    else

        if ((FT_on = '1') and (FT_clr_i = '1')) then
          FT_on <= '0';
        end if;

        tx_ft_ready <= '0';
        wr_busy_eb  <= '1';

        case (ep_eb_state) is

          when BONE_RST =>                                       -- Waiting for rd/wr burst or dma request
                                                                 -----------------------------------------
            if (ft_begin_cond = '1') then                        -- Fast transmitter burst
						  EB_on         <= '1';
              FT_on         <= '1';
					    eb_dk_c       <= '1';
						  wd_counter_en <= '0';
              wd_counter_cl <= '1';
					    ep_eb_state   <= BURST_FT_ADDR;
            elsif (ebone_wr_cond = '1') then                     -- E_bone Write request
              rw_cycle    <= '0';
					    breq_len    <= breq_len_i;
              ep_eb_state <= BURST_BONE_BRQ;
            elsif (ebone_rd_cond = '1') then                     -- E_bone Read request
              rw_cycle    <= '1';
					    breq_len    <= breq_len_i;
              ep_eb_state <= BURST_BONE_BRQ;
            elsif (intrn_wr_cond = '1') then                     -- Internal Write request
              end_of_cycle <= '1';
					    breq_len     <= breq_len_i;
              ep_eb_state  <= BURST_BONE_WAIT;
            elsif (intrn_rd_cond = '1') then                     -- Internal Read request
              end_of_cycle <= '1';
					    breq_len     <= breq_len_i;
              ep_eb_state  <= BURST_BONE_END;
            elsif (brd_call_cond = '1') then                     -- broad call
              eb_m0_brq   <= (not eb_bft_i);
              eb_m0_as    <= (not eb_bft_i);
              eb_m0_eof   <= (not eb_bft_i);
              bcall_run   <= (not eb_bft_i);
              my_m0_brq   <= '1';
              eb_m0_dat_o <= (31 => '1', others => '0');
					    ep_eb_state <= BURST_BONE_BG;
				    end if;

			    when BURST_BONE_BRQ =>                                 -- E_Bone burst request
                                                                 -----------------------
            eb_m0_brq   <= (not eb_bft_i);
            eb_m0_as    <= (not eb_bft_i);
            my_m0_brq   <= '1';
				    eb_m0_dat_o <= Type_Offset_desc;
            ep_eb_state <= BURST_BONE_BG;

			    when BURST_BONE_BG =>                                  -- Burst grant/err check
                                                                 ------------------------
            if ((eb_m0_bg_i = '1') and (eb_m0_eof = '0') and (eb_err_i = '0')) then
              if (b_data_cnt_m1 = '1') then
					      eb_m0_aef <= '1';
					    else
					      b_data_cnt_en <= '1';
					    end if;
              ep_eb_state <= BURST_BONE_DATA;
            elsif ((eb_m0_bg_i = '1') and (eb_m0_eof = '1') and (eb_err_i = '0')) then
              ep_eb_state <= BURST_BONE_BCALL_0;
            elsif ((eb_m0_bg_i = '0') and (my_m0_brq = '1')) then
              my_m0_brq   <= '0';
              eb_m0_brq   <= '0';
              eb_m0_as    <= '0';
              ep_eb_state <= BONE_RST;
            elsif (eb_err_i = '1') then
              my_m0_brq    <= '0';
					    eb_m0_brq    <= '0';
					    eb_m0_aef    <= '0';
					    eb_m0_eof    <= '0';
              eb_m0_as     <= '0';
					    end_of_cycle <= '0';
              if (rw_cycle = '1') then
					      rw_cycle       <= '0';
						    ext_brd_done_o <= '1';
						    ep_eb_state    <= BURST_BONE_END;
					    else
					      ext_brd_done_o <= '0';
					      ep_eb_state    <= BONE_RST;
					    end if;
            else
              ep_eb_state <= BURST_BONE_BG;
				    end if;

          when BURST_BONE_DATA =>                                -- Data Rx/Tx phase
                                                                 -------------------
            if ((eb_dk_i = '1') and (rw_cycle = '0')) then
              if ((b_data_cnt_m2 = '1') and (eb_m0_aef = '0')) then
					      eb_m0_aef   <= '1';
						    ep_eb_state <= BURST_BONE_DATA;
              elsif ((b_data_cnt_m1 = '1') and (eb_m0_eof = '0')) then
					      eb_m0_aef    <= '0';
					      eb_m0_eof    <= '1';
                wr_busy_eb   <= '0';
						    end_of_cycle <= '1';
                ep_eb_state  <= BURST_BONE_END;
					    end if;
              eb_m0_as     <= '0';
              eb_m0_dat_o  <= bwr_data_i(conv_integer(b_data_cnt))(7 downto 0) &
							                bwr_data_i(conv_integer(b_data_cnt))(15 downto 8) &
													    bwr_data_i(conv_integer(b_data_cnt))(23 downto 16) &
													    bwr_data_i(conv_integer(b_data_cnt))(31 downto 24);
            elsif ((eb_dk_i = '1') and (rw_cycle = '1') and (rd_cycle_wait = '0')) then
              eb_m0_as      <= '0';
					    eb_m0_aef     <= '0';
					    eb_m0_eof     <= '1';
              rd_cycle_wait <= '1';
              ep_eb_state   <= BURST_BONE_DATA;
				    elsif ((rw_cycle = '1') and (rd_cycle_wait = '1')) then
              eb_m0_eof         <= '0';
              ext_brd_data_o(0) <= eb_dat_i(7 downto 0) &
                                   eb_dat_i(15 downto 8) &
                                   eb_dat_i(23 downto 16) &
                                   eb_dat_i(31 downto 24);
					    ext_brd_done_o    <= '1';
					    end_of_cycle      <= '1';
              ep_eb_state       <= BURST_BONE_END;
            elsif (eb_err_i = '1') then
              my_m0_brq    <= '0';
              eb_m0_brq    <= '0';
					    eb_m0_aef    <= '0';
					    eb_m0_eof    <= '0';
              eb_m0_as     <= '0';
              wr_busy_eb   <= '0';
					    end_of_cycle <= '0';
              if (rw_cycle = '1') then
					      rw_cycle       <= '0';
						    ext_brd_done_o <= '1';
						    ep_eb_state    <= BURST_BONE_END;
					    else
					      ext_brd_done_o <= '0';
					      ep_eb_state    <= BONE_RST;
					    end if;
            else
              ep_eb_state <= BURST_BONE_DATA;
            end if;

          when BURST_BONE_BCALL_0 =>

            eb_m0_as    <= '0';
            eb_m0_eof   <= '0';
				    ep_eb_state <= BURST_BONE_BCALL_1;

          when BURST_BONE_BCALL_1 =>

            eb_m0_eof   <= '1';
				    ep_eb_state <= BURST_BONE_BCALL_2;

          when BURST_BONE_BCALL_2 =>

            my_m0_brq   <= '0';
            eb_m0_brq   <= '0';
            eb_m0_eof   <= '0';
				    bcall_reg_o <= eb_dat_i;
						bcall_run   <= '0';
				    ep_eb_state <= BONE_RST;

          when BURST_FT_ADDR =>

            if ((eb_as_i = '1') and (eb_ft_ss_i = '0')) then
					    wd_counter_cl <= '0';
							bytes_to_keep <= eb_ft_dsz_i;
					    eb_ft_addr    <= eb_ft_dxt_i;
						  ep_eb_state   <= BURST_FT_SIZE;
					  elsif ((eb_as_i = '1') and (eb_ft_ss_i = '1')) then
					    eb_ft_addr    <= eb_ft_dxt_i;
						  eb_data_size  <= eb_ft_dsz_i;
							eb_byte_size  <= eb_ft_blen_i;
							wd_counter_cl <= '0';
						  wd_counter_en <= '1';
							tx_ft_ready   <= '1';
						  ep_eb_state   <= BURST_FT_DATA;
				    else
				      ep_eb_state <= BURST_FT_ADDR;
				    end if;

          when BURST_FT_SIZE =>

				    if (eb_ft_ss_i = '1') then
					    eb_data_size  <= eb_ft_dsz_i;
							eb_byte_size  <= eb_ft_blen_i;
						  wd_counter_en <= '1';
							tx_ft_ready   <= '1';
						  ep_eb_state   <= BURST_FT_DATA;
						elsif ((eb_bft_i = '1') and (eb_eof_i = '1')) then
              eb_dk_c       <= '0';
              EB_on         <= '0';
							wd_counter_cl <= '1';
						  wd_counter_en <= '0';
						  ep_eb_state   <= BONE_RST;
						else
				      ep_eb_state <= BURST_FT_SIZE;
				    end if;

          when BURST_FT_DATA =>

				    if (eb_eof_i = '1') then
						  eb_dk_c       <= '0';
              EB_on         <= '0';
							wd_counter_en <= '0';
						  eb_last_size  <= eb_ft_dsz_i;
						  ep_eb_state   <= BONE_RST;
						else
				      ep_eb_state <= BURST_FT_DATA;
					  end if;

          when BURST_BONE_WAIT =>

            if (WR_on_i = '0') then
						  ep_eb_state <= BONE_RST;
				    end if;

          when BURST_BONE_END =>

            my_m0_brq     <= '0';
            eb_m0_brq     <= '0';
				    eb_m0_aef     <= '0';
            eb_m0_eof     <= '0';
            rw_cycle      <= '0';
				    rd_cycle_wait <= '0';
				    b_data_cnt_en <= '0';
            if ((bwr_strt_i = '0') and (brd_strt_i = '0') and (eb_bft_i = '0')) then
				      end_of_cycle   <= '0';
					    ext_brd_done_o <= '0';
              ep_eb_state    <= BONE_RST;
				    end if;

          when others =>

            ep_eb_state <= BONE_RST;

		    end case;

	    end if;
    end process;

-----------------------------------------------------------------------------------------------------------
end generate EP_EB_64B;
-----------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
EP_EB_128B : if (EBFT_DWIDTH = 128) generate                               -- ep_eb_128b_link implementation
------------------------------------------------------------------------------------------------------------

  type ep_eb_type     is (BONE_RST,
                          BURST_BONE_BRQ, BURST_BONE_BG, BURST_BONE_DATA,
												  BURST_BONE_BCALL_0, BURST_BONE_BCALL_1, BURST_BONE_BCALL_2,
												  BURST_FT_ADDR, BURST_FT_SIZE, BURST_FT_DATA,
												  BURST_BONE_WAIT, BURST_BONE_END);
  signal ep_eb_state  : ep_eb_type;
	signal tx_ft_ready  : std_logic;
	signal tx_ft_rdy_s  : std_logic;
	signal eb_ft_data_t : std_logic_vector(127 downto 0);
	signal eb_ft_data_c : std_logic_vector(127 downto 0);
	signal eb_ft_addr   : std_logic_vector(EBFT_DWIDTH - 1 downto 0);

  begin

    US_CNT_4 : if (EB_CLK = 4.0) generate
    -------------------------------------
      one_us_cnt_tc <= '1' when (one_us_cnt = x"F8") else '0';
    end generate US_CNT_4;

    US_CNT_8 : if (EB_CLK = 8.0) generate
    -------------------------------------
      one_us_cnt_tc <= '1' when (one_us_cnt = x"7C") else '0';
    end generate US_CNT_8;

    US_CNT_16 : if (EB_CLK = 16.0) generate
    ---------------------------------------
      one_us_cnt_tc <= '1' when (one_us_cnt = x"3E") else '0';
    end generate US_CNT_16;

    eb_ft_addr_o   <= eb_ft_addr;
    eb_byte_size_o <= eb_byte_size;

    with bytes_to_keep(3 downto 0) select
		  eb_ft_data_c <= eb_ft_data_t when "0000",
			                eb_ft_dxt_i(119 downto 0) & eb_ft_data_t(127 downto 120) when "0001",
											eb_ft_dxt_i(111 downto 0) & eb_ft_data_t(127 downto 112) when "0010",
											eb_ft_dxt_i(103 downto 0) & eb_ft_data_t(127 downto 104) when "0011",
											eb_ft_dxt_i(95 downto 0) & eb_ft_data_t(127 downto 96) when "0100",
											eb_ft_dxt_i(87 downto 0) & eb_ft_data_t(127 downto 88) when "0101",
											eb_ft_dxt_i(79 downto 0) & eb_ft_data_t(127 downto 80) when "0110",
											eb_ft_dxt_i(71 downto 0) & eb_ft_data_t(127 downto 72) when "0111",
											eb_ft_dxt_i(63 downto 0) & eb_ft_data_t(127 downto 64) when "1000",
											eb_ft_dxt_i(55 downto 0) & eb_ft_data_t(127 downto 56) when "1001",
											eb_ft_dxt_i(47 downto 0) & eb_ft_data_t(127 downto 48) when "1010",
											eb_ft_dxt_i(39 downto 0) & eb_ft_data_t(127 downto 40) when "1011",
											eb_ft_dxt_i(31 downto 0) & eb_ft_data_t(127 downto 32) when "1100",
											eb_ft_dxt_i(23 downto 0) & eb_ft_data_t(127 downto 24) when "1101",
											eb_ft_dxt_i(15 downto 0) & eb_ft_data_t(127 downto 16) when "1110",
											eb_ft_dxt_i(7 downto 0) & eb_ft_data_t(127 downto 8) when "1111",
											eb_ft_data_t when others;

    bytes_calc <= std_logic_vector((unsigned(eb_ft_addr(15 downto 0) and x"0FFF") + unsigned(eb_byte_size)));
    bound_ovfl <= '1' when (unsigned(bytes_calc) > bound_val) else '0';

    process
		begin
		  wait until rising_edge(trn_clk);
			  if (trn_rst = '1') then
				  bytes_ovfl <= (others => '0');
				else
				  if (bound_ovfl = '1') then
					  bytes_ovfl <= std_logic_vector(unsigned(bytes_calc) - bound_val);
					else
					  bytes_ovfl <= (others => '0');
					end if;
				end if;
    end process;

    process
		begin
      wait until rising_edge(trn_clk);
			  if (trn_rst = '1') then
				  eb_ft_data_t  <= (others => '0');
					eb_ft_data_o  <= (others => '0');
					tx_ft_rdy_s   <= '0';
					tx_ft_ready_o <= '0';
			  else
				  eb_ft_data_t  <= eb_ft_dxt_i;
					eb_ft_data_o  <= eb_ft_data_c;
			    tx_ft_rdy_s   <= tx_ft_ready;
					tx_ft_ready_o <= tx_ft_rdy_s;
			  end if;
    end process;

    -- =======================================================================
    -- Master Requesting E-Bone mastership for Burst or DMA operations process
    -- =======================================================================
    process

    variable ebone_wr_cond : std_logic;
    variable intrn_wr_cond : std_logic;
    variable ebone_rd_cond : std_logic;
    variable intrn_rd_cond : std_logic;
    variable brd_call_cond : std_logic;
    variable ft_begin_cond : std_logic;

    begin

      ebone_wr_cond := (bwr_strt_i and E_Bone_trn_i and (not eb_bft_i) and (not tx_ft_ready_i) and (not eb_bmx_i) and (not bcall_run) and (not FT_on));
      intrn_wr_cond := (bwr_strt_i and (not E_Bone_trn_i) and (not eb_bft_i) and (not tx_ft_ready_i) and (not eb_bmx_i));
      ebone_rd_cond := (brd_strt_i and E_Bone_trn_i and (not eb_bft_i) and (not tx_ft_ready_i) and (not eb_bmx_i) and (not bcall_run) and (not FT_on) and
                        s_axis_tx_tready and (not tx_ft_delay_i));
      intrn_rd_cond := (brd_strt_i and (not E_Bone_trn_i) and (not eb_bft_i) and (not tx_ft_ready_i) and (not eb_bmx_i) and
                        s_axis_tx_tready and (not tx_ft_delay_i));
	    brd_call_cond := ((not bwr_strt_i) and (not brd_strt_i) and (not eb_bft_i) and (not tx_ft_ready_i) and (not eb_bmx_s) and (not npck_prsnt_i) and
                        (not RD_on_i) and (not WR_on_i) and (not FT_on) and (not INtr_gate_i) and (bc_prd_cnt_sh(0)) and (not bc_prd_cnt_sh(1)));
			ft_begin_cond := (eb_bft_i and (not eb_eof_i) and (not bwr_strt_i) and (not brd_strt_i) and (not req_compl_i) and s_axis_tx_tready and
                        (not RD_on_i) and (not WR_on_i) and (not FT_on) and (not tx_ft_delay_i) and (not tx_ft_ready_i) and (not tlp_delayed_i) and (not npck_prsnt_i));

      wait until rising_edge(trn_clk);

      if ((trn_rst = '1') or (ft_ovf_rst = '1') or (rt_ovf_rst = '1') or (ft_err_rst = '1')) then

        my_m0_brq      <= '0';
        eb_m0_brq      <= '0';
        eb_m0_as       <= '0';
        eb_m0_eof      <= '0';
		    eb_m0_aef      <= '0';
        eb_m0_dat_o    <= (others => '0');
		    eb_dk_c        <= '0';
        rw_cycle       <= '0';
		    rd_cycle_wait  <= '0';
		    end_of_cycle   <= '0';
		    b_data_cnt_en  <= '0';
				bcall_run      <= '0';
		    bcall_reg_o    <= (others => '0');
		    EB_on          <= '0';
        FT_on          <= '0';
        wr_busy_eb     <= '1';
		    ext_brd_done_o <= '0';
			  wd_counter_en  <= '0';
			  wd_counter_cl  <= '1';
				tx_ft_ready    <= '0';
			  eb_ft_addr     <= (others => '0');
			  eb_data_size   <= (others => '0');
			  eb_last_size   <= (others => '0');
				eb_byte_size   <= (others => '0');
				bytes_to_keep  <= (others => '0');

        for i in 0 to 31 loop
          ext_brd_data_o(i) <= (others => '0');
		    end loop;

        ep_eb_state <= BONE_RST;

	    else

        if ((FT_on = '1') and (FT_clr_i = '1')) then
          FT_on <= '0';
        end if;

        tx_ft_ready <= '0';
        wr_busy_eb  <= '1';

        case (ep_eb_state) is

          when BONE_RST =>                                       -- Waiting for rd/wr burst or dma request
                                                                 -----------------------------------------
            if (ft_begin_cond = '1') then                        -- Fast transmitter burst
						  EB_on         <= '1';
              FT_on         <= '1';
					    eb_dk_c       <= '1';
						  wd_counter_en <= '0';
              wd_counter_cl <= '1';
					    ep_eb_state   <= BURST_FT_ADDR;
            elsif (ebone_wr_cond = '1') then                     -- E_bone Write request
              rw_cycle    <= '0';
					    breq_len    <= breq_len_i;
              ep_eb_state <= BURST_BONE_BRQ;
            elsif (ebone_rd_cond = '1') then                     -- E_bone Read request
              rw_cycle    <= '1';
					    breq_len    <= breq_len_i;
              ep_eb_state <= BURST_BONE_BRQ;
            elsif (intrn_wr_cond = '1') then                     -- Internal Write request
              end_of_cycle <= '1';
					    breq_len     <= breq_len_i;
              ep_eb_state  <= BURST_BONE_WAIT;
            elsif (intrn_rd_cond = '1') then                     -- Internal Read request
              end_of_cycle <= '1';
					    breq_len     <= breq_len_i;
              ep_eb_state  <= BURST_BONE_END;
            elsif (brd_call_cond = '1') then                     -- broad call
              eb_m0_brq   <= (not eb_bft_i);
              eb_m0_as    <= (not eb_bft_i);
              eb_m0_eof   <= (not eb_bft_i);
              bcall_run   <= (not eb_bft_i);
              my_m0_brq   <= '1';
              eb_m0_dat_o <= (31 => '1', others => '0');
					    ep_eb_state <= BURST_BONE_BG;
				    end if;

			    when BURST_BONE_BRQ =>                                 -- E_Bone burst request
                                                                 -----------------------
            eb_m0_brq   <= (not eb_bft_i);
            eb_m0_as    <= (not eb_bft_i);
            my_m0_brq   <= '1';
				    eb_m0_dat_o <= Type_Offset_desc;
            ep_eb_state <= BURST_BONE_BG;

			    when BURST_BONE_BG =>                                  -- Burst grant/err check
                                                                 ------------------------
            if ((eb_m0_bg_i = '1') and (eb_m0_eof = '0') and (bcall_run = '0') and (eb_err_i = '0')) then
              if (b_data_cnt_m1 = '1') then
					      eb_m0_aef <= '1';
					    else
					      b_data_cnt_en <= '1';
					    end if;
              ep_eb_state <= BURST_BONE_DATA;
            elsif ((eb_m0_bg_i = '1') and (eb_m0_eof = '1') and (bcall_run = '1') and (eb_err_i = '0')) then
              ep_eb_state <= BURST_BONE_BCALL_0;
            elsif ((eb_m0_bg_i = '0') and (my_m0_brq = '1')) then
              my_m0_brq   <= '0';
              eb_m0_brq   <= '0';
              eb_m0_as    <= '0';
              ep_eb_state <= BONE_RST;
            elsif (eb_err_i = '1') then
              my_m0_brq    <= '0';
					    eb_m0_brq    <= '0';
					    eb_m0_aef    <= '0';
					    eb_m0_eof    <= '0';
              eb_m0_as     <= '0';
					    end_of_cycle <= '0';
              if (rw_cycle = '1') then
					      rw_cycle       <= '0';
						    ext_brd_done_o <= '1';
						    ep_eb_state    <= BURST_BONE_END;
					    else
					      ext_brd_done_o <= '0';
					      ep_eb_state    <= BONE_RST;
					    end if;
            else
              ep_eb_state <= BURST_BONE_BG;
				    end if;

          when BURST_BONE_DATA =>                                -- Data Rx/Tx phase
                                                                 -------------------
            if ((eb_dk_i = '1') and (rw_cycle = '0')) then
              if ((b_data_cnt_m2 = '1') and (eb_m0_aef = '0')) then
					      eb_m0_aef   <= '1';
						    ep_eb_state <= BURST_BONE_DATA;
              elsif ((b_data_cnt_m1 = '1') and (eb_m0_eof = '0')) then
					      eb_m0_aef    <= '0';
					      eb_m0_eof    <= '1';
                wr_busy_eb   <= '0';
						    end_of_cycle <= '1';
                ep_eb_state  <= BURST_BONE_END;
					    end if;
              eb_m0_as     <= '0';
              eb_m0_dat_o  <= bwr_data_i(conv_integer(b_data_cnt))(7 downto 0) &
							                bwr_data_i(conv_integer(b_data_cnt))(15 downto 8) &
													    bwr_data_i(conv_integer(b_data_cnt))(23 downto 16) &
													    bwr_data_i(conv_integer(b_data_cnt))(31 downto 24);
            elsif ((eb_dk_i = '1') and (rw_cycle = '1') and (rd_cycle_wait = '0')) then
              eb_m0_as      <= '0';
					    eb_m0_aef     <= '0';
					    eb_m0_eof     <= '1';
              rd_cycle_wait <= '1';
              ep_eb_state   <= BURST_BONE_DATA;
				    elsif ((rw_cycle = '1') and (rd_cycle_wait = '1')) then
              eb_m0_eof         <= '0';
              ext_brd_data_o(0) <= eb_dat_i(7 downto 0) &
                                   eb_dat_i(15 downto 8) &
                                   eb_dat_i(23 downto 16) &
                                   eb_dat_i(31 downto 24);
					    ext_brd_done_o    <= '1';
					    end_of_cycle      <= '1';
              ep_eb_state       <= BURST_BONE_END;
            elsif (eb_err_i = '1') then
              my_m0_brq    <= '0';
              eb_m0_brq    <= '0';
					    eb_m0_aef    <= '0';
					    eb_m0_eof    <= '0';
              eb_m0_as     <= '0';
              wr_busy_eb   <= '0';
					    end_of_cycle <= '0';
              if (rw_cycle = '1') then
					      rw_cycle       <= '0';
						    ext_brd_done_o <= '1';
						    ep_eb_state    <= BURST_BONE_END;
					    else
					      ext_brd_done_o <= '0';
					      ep_eb_state    <= BONE_RST;
					    end if;
            else
              ep_eb_state <= BURST_BONE_DATA;
            end if;

          when BURST_BONE_BCALL_0 =>

            eb_m0_as    <= '0';
            eb_m0_eof   <= '0';
				    ep_eb_state <= BURST_BONE_BCALL_1;

          when BURST_BONE_BCALL_1 =>

            eb_m0_eof   <= '1';
				    ep_eb_state <= BURST_BONE_BCALL_2;

          when BURST_BONE_BCALL_2 =>

            my_m0_brq   <= '0';
            eb_m0_brq   <= '0';
            eb_m0_eof   <= '0';
				    bcall_reg_o <= eb_dat_i;
						bcall_run   <= '0';
				    ep_eb_state <= BONE_RST;

          when BURST_FT_ADDR =>

            if ((eb_as_i = '1') and (eb_ft_ss_i = '0')) then
					    wd_counter_cl <= '0';
							bytes_to_keep <= eb_ft_dsz_i;
					    eb_ft_addr    <= eb_ft_dxt_i;
						  ep_eb_state   <= BURST_FT_SIZE;
					  elsif ((eb_as_i = '1') and (eb_ft_ss_i = '1')) then
					    eb_ft_addr    <= eb_ft_dxt_i;
						  eb_data_size  <= eb_ft_dsz_i;
							eb_byte_size  <= eb_ft_blen_i;
							wd_counter_cl <= '0';
						  wd_counter_en <= '1';
							tx_ft_ready   <= '1';
						  ep_eb_state   <= BURST_FT_DATA;
				    else
				      ep_eb_state <= BURST_FT_ADDR;
				    end if;

          when BURST_FT_SIZE =>

				    if (eb_ft_ss_i = '1') then
					    eb_data_size  <= eb_ft_dsz_i;
							eb_byte_size  <= eb_ft_blen_i;
						  wd_counter_en <= '1';
							tx_ft_ready   <= '1';
						  ep_eb_state   <= BURST_FT_DATA;
            elsif ((eb_bft_i = '1') and (eb_eof_i = '1')) then
						  eb_dk_c       <= '0';
              EB_on         <= '0';
							wd_counter_cl <= '1';
						  wd_counter_en <= '0';
						  ep_eb_state   <= BONE_RST;
						else
				      ep_eb_state <= BURST_FT_SIZE;
				    end if;

          when BURST_FT_DATA =>

				    if (eb_eof_i = '1') then
						  eb_dk_c       <= '0';
              EB_on         <= '0';
							wd_counter_en <= '0';
						  eb_last_size  <= eb_ft_dsz_i;
						  ep_eb_state   <= BONE_RST;
						else
				      ep_eb_state <= BURST_FT_DATA;
					  end if;

          when BURST_BONE_WAIT =>

            if (WR_on_i = '0') then
						  ep_eb_state <= BONE_RST;
				    end if;

          when BURST_BONE_END =>

            my_m0_brq     <= '0';
            eb_m0_brq     <= '0';
				    eb_m0_aef     <= '0';
            eb_m0_eof     <= '0';
            rw_cycle      <= '0';
				    rd_cycle_wait <= '0';
				    b_data_cnt_en <= '0';
            if ((bwr_strt_i = '0') and (brd_strt_i = '0') and (eb_bft_i = '0')) then
				      end_of_cycle   <= '0';
					    ext_brd_done_o <= '0';
              ep_eb_state    <= BONE_RST;
				    end if;

          when others =>

            ep_eb_state <= BONE_RST;

		    end case;

	    end if;
    end process;

------------------------------------------------------------------------------------------------------------
end generate EP_EB_128B;
------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------
EP_EB_256B : if (EBFT_DWIDTH = 256) generate                               -- ep_eb_256b_link implementation
------------------------------------------------------------------------------------------------------------

  type ep_eb_type    is (BONE_RST,
                         BURST_BONE_BRQ, BURST_BONE_BG, BURST_BONE_DATA,
												 BURST_BONE_BCALL_0, BURST_BONE_BCALL_1, BURST_BONE_BCALL_2,
												 BURST_FT_ADDR, BURST_FT_SIZE, BURST_FT_DATA,
												 BURST_BONE_WAIT, BURST_BONE_END);
  signal ep_eb_state : ep_eb_type;

  begin

    tx_ft_ready_o  <= '0';
    my_m0_brq      <= '0';
    eb_m0_brq      <= '0';
    eb_m0_as       <= '0';
    eb_m0_eof      <= '0';
	  eb_m0_aef      <= '0';
    eb_m0_dat_o    <= (others => '0');
	  eb_dk_c        <= '0';
    rw_cycle       <= '0';
	  rd_cycle_wait  <= '0';
	  end_of_cycle   <= '0';
	  b_data_cnt_en  <= '0';
		bcall_run      <= '0';
	  bcall_reg_o    <= (others => '0');
	  EB_on          <= '0';
    FT_on          <= '0';
	  ext_brd_done_o <= '0';
	  wd_counter_en  <= '0';
	  wd_counter_cl  <= '1';
	  eb_ft_addr_o   <= (others => '0');
	  eb_data_size   <= (others => '0');
	  eb_last_size   <= (others => '0');
		eb_byte_size_o <= (others => '0');
		bytes_to_keep  <= (others => '0');
    ep_eb_state    <= BONE_RST;

------------------------------------------------------------------------------------------------------------
end generate EP_EB_256B;
------------------------------------------------------------------------------------------------------------

end ep_eb_rtl;
