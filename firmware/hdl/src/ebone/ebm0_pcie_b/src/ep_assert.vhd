----------------------------------------------------------------------------------
--
-- Project    : Series 7 Integrated Block for PCI Express
-- File       : ep_assert.vhd
-- Description: Irrelevant generic parameters cases control.
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

entity ep_assert is

generic (
  EB_CLK      : REAL := 8.0;              -- E-Bone clock period in ns
	EBFT_DWIDTH : NATURAL := 32;            -- FT data width
  TYPE_DESC   : NATURAL := 16#00000000#;  -- Type description application and/or target dependant
	TRGT_DESC   : NATURAL := 16#00#;        -- Target dependant
	LINK_SPEED  : INTEGER := 1;             -- PCI-Express EndPoint Link Speed (1=2.5G, 2=5G)
	LINK_WIDTH  : INTEGER := 1              -- PCI-Express EndPoint Lane numbers (1,4,8)
);

end ep_assert;

architecture assert_rtl of ep_assert is

signal full_type_desc_stdl : unsigned(31 downto 0);

begin

full_type_desc_stdl <= to_unsigned(TYPE_DESC, 32);

assert ((TRGT_DESC = 16#71#) or (TRGT_DESC = 16#72#) or (TRGT_DESC = 16#73#))
report "Bad Target FPGA !!!" severity failure;

assert (((EB_CLK = 16.0) and (EBFT_DWIDTH = 64) and (LINK_SPEED = 1) and (LINK_WIDTH = 1)) or
        ((EB_CLK = 8.0) and (EBFT_DWIDTH = 64) and (LINK_SPEED = 1) and (LINK_WIDTH = 1)) or
        ((EB_CLK = 8.0) and (EBFT_DWIDTH = 64) and (LINK_SPEED = 1) and (LINK_WIDTH = 4)) or
        ((EB_CLK = 8.0) and (EBFT_DWIDTH = 128) and (LINK_SPEED = 1) and (LINK_WIDTH = 8)) or
        ((EB_CLK = 8.0) and (EBFT_DWIDTH = 64) and (LINK_SPEED = 2) and (LINK_WIDTH = 1)) or
        ((EB_CLK = 8.0) and (EBFT_DWIDTH = 128) and (LINK_SPEED = 2) and (LINK_WIDTH = 4)) or
			  ((EB_CLK = 4.0) and (EBFT_DWIDTH = 128) and (LINK_SPEED = 2) and (LINK_WIDTH = 8)))
report "Bad relationship between EB_CLK, EBFT_DWIDTH, LINK_SPEED, LINK_WIDTH parameters !!!" severity failure;

assert (full_type_desc_stdl /= X"00000000") and (full_type_desc_stdl /= X"FFFFFFFF")
report "Bad TYPE_DESC parameter !!!" severity failure;

end assert_rtl;
