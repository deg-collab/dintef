--------------------------------------------------------------------------------
--
-- Description: Turn-off Control Unit 
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.ep_comp_pkg.all;
use work.ebm0_pcie_b_pkg.all;

entity ep_to_ctrl is
port (
  clk            : in std_logic;
  trn_rst        : in std_logic;

  req_compl_i    : in std_logic;
  compl_done_i   : in std_logic;

  cfg_to_turnoff : in std_logic;
  cfg_turnoff_ok : out std_logic
);
end ep_to_ctrl;

architecture rtl of ep_to_ctrl is

signal trn_pending : std_logic;

begin

  --
  -- Check if completion is pending
  --
  process begin
    wait until rising_edge(clk);
    if (trn_rst = '1') then
      trn_pending    <= '0';
    else
      if ((trn_pending = '0') and (req_compl_i = '1')) then
        trn_pending  <= '1';
      elsif (compl_done_i = '1') then
        trn_pending  <= '0';
      end if;
    end if;
  end process;

  --
  --  Turn-off OK if requested and no transaction is pending
  --
  process begin
    wait until rising_edge(clk);
    if (trn_rst = '1') then
      cfg_turnoff_ok <= '0';
    else
      if ((cfg_to_turnoff = '1') and (trn_pending = '0')) then
        cfg_turnoff_ok <= '1';
      else
        cfg_turnoff_ok <= '0';
      end if;
    end if;
  end process;

end rtl;

