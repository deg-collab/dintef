----------------------------------------------------------------------------------
--
-- Project    : Series 7 Integrated Block for PCI Express
-- File       : ep_tx_engine.vhd
-- Description: Local-Link Transmit Unit.
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.ep_comp_pkg.all;
use work.ebm0_pcie_b_pkg.all;
use work.ebs_pkg.std32_a;

entity ep_tx_engine is

generic (
	EBFT_DWIDTH           : NATURAL := 64                                    -- FT data width
);
port (
  clk                   : in std_logic;                                    -- User clock from the endpoint module
  trn_rst               : in std_logic;                                    -- Global active high reset signal

  tx_buf_av             : in std_logic_vector(5 downto 0);                 -- Transmit Buffers Available
  tx_cfg_req            : in std_logic;                                    -- Transmit Configuration Request: Asserted when the core is ready to transmit Conf. Completion or internally generated TLP.
  tx_err_drop           : in std_logic;                                    -- Transmit Error Drop: Indicates that the core discarded a packet because of length violation
  s_axis_tx_tready      : in std_logic;                                    -- Transmit Destination Ready: Indicates that the core is ready to accept data on s_axis_tx_tdata
  s_axis_tx_tdata       : out std_logic_vector((EBFT_DWIDTH - 1) downto 0);      -- Transmit Data: Packet data to be transmitted
  s_axis_tx_tkeep       : out std_logic_vector((EBFT_DWIDTH / 8 - 1) downto 0);  -- Transmit Data Strobe: Determines which data bytes are valid on s_axis_tx_tdata
  s_axis_tx_tlast       : out std_logic;                                   -- Transmit End-of-Frame (EOF): Signals the end of a packet
  s_axis_tx_tvalid      : out std_logic;                                   -- Transmit Source Ready: Indicates that the User Application is presenting valid data on s_axis_tx_tdata
  s_axis_tx_tuser       : out std_logic_vector(3 downto 0);                -- Transmit Source Discontinue: Can be asserted any time starting on the first cycle after SOF

  req_compl_i           : in std_logic;                                    -- Indicates completion required
  req_compl_wd_i        : in std_logic;                                    -- asserted indicates to generate a completion WITH data
  compl_done_o          : out std_logic;                                   -- Completion acknowledge

  req_tc_i              : in std_logic_vector(2 downto 0);                 -- Header field TC (Traffic Class)
  req_td_i              : in std_logic;                                    -- Header field TD (TLP Digest present)
  req_ep_i              : in std_logic;                                    -- Header field EP (TLP poisoned)
  req_attr_i            : in std_logic_vector(1 downto 0);                 -- Header field Attributes
  req_len_i             : in std_logic_vector(9 downto 0);                 -- Header field Data Length (Data Payload in DW)
  req_rid_i             : in std_logic_vector(15 downto 0);                -- Header field Requestor ID
  req_tag_i             : in std_logic_vector(7 downto 0);                 -- Header field Tag
  req_be_i              : in std_logic_vector(7 downto 0);                 -- Header field Byte Enables
  req_addr_i            : in std_logic_vector(31 downto 0);                -- Header field Address
	req_tx_valid_o        : out std_logic;                                   -- High for one system clock period when TX_MWR_FMT_TYPE or TX_MWR64_FMT_TYPE

  brd_addr_o            : out std_logic_vector(29 downto 0);               -- Header field req_addr_i(31 downto 2)
  brd_be_o              : out std_logic_vector(3 downto 0);                -- Header field Byte Enable req_be_i(3 downto 0)

  int_brd_data_i        : in std32_x32;                                    -- Internal Memory Read Data array
	ext_brd_data_i        : in std32_x32;                                    -- External Memory Read Data array (E_Bone)
  ext_brd_done_i        : in std_logic;                                    -- External Memory Read Burst done (E_Bone)

  completer_id_i        : in std_logic_vector(15 downto 0);                -- cfg_bus_number & cfg_device_number & cfg_function_number;
  cfg_bus_mstr_enable_i : in std_logic;                                    -- cfg_command(2) from the Configuration Space Header Command register
	cfg_dcommand_i        : in std_logic_vector(15 downto 0);                -- Device Command register from the Configuration Space Header

	E_Bone_trn_i          : in std_logic;                                    -- E_Bone external transaction request
	tx_ft_ready_i         : in std_logic;                                    -- External TLP transmit request
	tx_ft_ready_o         : out std_logic;                                   -- Internal TLP transmit request for missing bytes transfer
  requester_fmt_i       : in std_logic;                                    -- 64-bit address routing Transmit TLP when 1 otherwise 32-bit
	requester_leng_i      : in std_logic_vector(9 downto 0);                 -- DW (4-byte) length field encoding corresponding to one TLP transmit operation
  eb_eof_i              : in std_logic;                                    -- FT slave end of frame
  eb_ft_data_i          : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);   -- TLP transmit data from fast transmitter
  eb_ft_addr_i          : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);   -- TLP transmit address from fast transmitter
	eb_data_size_i        : in std_logic_vector(7 downto 0);                 -- TLP data size valid
	eb_last_size_i        : in std_logic_vector(7 downto 0);                 -- TLP last size valid
	eb_byte_size_i        : in std_logic_vector(15 downto 0);                -- TLP byte count
	TLP_end_Rqst_i        : in std_logic;                                    -- TLP end of transfer => interrupt module
  FT_on_i               : in std_logic;                                    -- FT has started when 1
  FT_clr_o              : out std_logic;                                   -- FT completed from Tx engine
	tlp_tx_addr           : out std_logic_vector(EBFT_DWIDTH - 1 downto 0);  -- TLP transmit address in DMA mode
	tx_ft_delay_o         : out std_logic;                                   -- Next FT delayed
  tlp_delayed_o         : out std_logic;                                   -- TLP delayed

  ext_addr_src_i        : in std_logic_vector(63 downto 0);                -- External addr source input
  ext_data_src_i        : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);   -- External data source input
	ext_data_flg_i        : in std_logic;                                    -- External data source input ready for DMA transfer when 1
	ext_data_ack_o        : out std_logic;                                   -- External data source acknowledge output

  tx_probe_o            : out std_logic_vector(3 downto 0)                 -- Probe output signals linked to Tx engine
);

end ep_tx_engine;

architecture ep_tx_rtl of ep_tx_engine is

-- common declarations
----------------------

constant TX_CPLD_FMT_TYPE     : std_logic_vector(6 downto 0) := "1001010";  -- Completion with data (CplD)
constant TX_CPL_FMT_TYPE      : std_logic_vector(6 downto 0) := "0001010";  -- Completion without data (Cpl)
constant TX_MWR_FMT_TYPE      : std_logic_vector(6 downto 0) := "1000000";  -- Memory write request (MWr32 3DW) -> Route to Root Complex
constant TX_MWR64_FMT_TYPE    : std_logic_vector(6 downto 0) := "1100000";  -- Memory write request 64-bit (MWr64 4DW) -> Route to Root Complex
constant TX_MWR_FMT_TYPE_AR   : std_logic_vector(6 downto 0) := "1000001";  -- Memory write request (MWr32 3DW) -> Use Address Routing
constant TX_MWR64_FMT_TYPE_AR : std_logic_vector(6 downto 0) := "1100001";  -- Memory write request 64-bit (MWr64 4DW) -> Use Address Routing

type wd_count_array           is array(0 to 16) of INTEGER;
constant wd_count_tab         : wd_count_array := (0, 1, 1, 1, 1, 2, 2, 2, 2, 3 , 3, 3, 3, 4, 4, 4, 4);

signal s_axis_tx_tlast_int    : std_logic;
signal s_axis_tx_tvalid_int   : std_logic;
signal compl_done             : std_logic;
signal tlp_valid              : std_logic;
signal tlp_delayed            : std_logic;
signal byte_count             : std_logic_vector(11 downto 0);
signal lower_addr             : std_logic_vector(6 downto 0);
signal rd_be_o_int            : std_logic_vector(3 downto 0);
signal last_frst_be           : std_logic_vector(7 downto 0);
signal dw_count               : std_logic_vector(9 downto 0);
signal qw_count               : std_logic_vector(9 downto 0);

signal mwr_data_cnt           : std_logic_vector(9 downto 0);
signal mwr_data_cnt_cl        : std_logic;
signal mwr_data_cnt_en        : std_logic;
signal mwr_data_cnt_pr        : std_logic;
signal mwr_data_cnt_tc        : std_logic;

signal wr_en                  : std_logic;
signal wr_dat                 : std_logic_vector(EBFT_DWIDTH - 1 downto 0);
signal wr_full                : std_logic;
signal rd_en                  : std_logic;
signal rd_dat                 : std_logic_vector(EBFT_DWIDTH - 1 downto 0);
signal rd_empty               : std_logic;

begin

-- common implementation
------------------------

fifo_s_i : ep_fifo_s
--------------------
generic map (
  DWIDTH    => EBFT_DWIDTH,
  DWINP     => EBFT_DWIDTH,
  ADEPTH    => 64,
  REGOUT    => FALSE,
  RAMTYP    => "auto"
)
port map (
  arst      => trn_rst,
  clk       => clk,
  wr_en     => wr_en,
  wr_dat    => wr_dat,
  wr_cnt    => open,
  wr_afull  => open,
  wr_full   => wr_full,

  rd_en     => rd_en,
  rd_dat    => rd_dat,
  rd_aempty => open,
  rd_empty  => rd_empty
);

rd_en <= '1' when ((rd_empty = '0') and (s_axis_tx_tready = '1')) else '0';

tx_probe_o(0) <= s_axis_tx_tvalid_int;  -- 8
tx_probe_o(1) <= s_axis_tx_tready;      -- 9
tx_probe_o(2) <= s_axis_tx_tlast_int;   -- 10
tx_probe_o(3) <= FT_on_i;               -- 11

s_axis_tx_tlast  <= s_axis_tx_tlast_int;
s_axis_tx_tvalid <= s_axis_tx_tvalid_int;

compl_done_o <= compl_done;
tlp_delayed_o <= tlp_delayed;
brd_be_o <= rd_be_o_int;
brd_addr_o <= req_addr_i(31 downto 2);
rd_be_o_int <= req_be_i(3 downto 0);

-- Calculate byte count based on byte enable
--------------------------------------------
process(rd_be_o_int)
begin
  case rd_be_o_int(3 downto 0) is
    when X"9" =>   byte_count <= X"004";
    when X"B" =>   byte_count <= X"004";
    when X"D" =>   byte_count <= X"004";
    when X"F" =>   byte_count <= X"004";
    when X"5" =>   byte_count <= X"003";
    when X"7" =>   byte_count <= X"003";
    when X"A" =>   byte_count <= X"003";
    when X"E" =>   byte_count <= X"003";
    when X"3" =>   byte_count <= X"002";
    when X"6" =>   byte_count <= X"002";
    when X"C" =>   byte_count <= X"002";
    when X"1" =>   byte_count <= X"001";
    when X"2" =>   byte_count <= X"001";
    when X"4" =>   byte_count <= X"001";
    when X"8" =>   byte_count <= X"001";
    when X"0" =>   byte_count <= X"001";
    when others => byte_count <= X"001";
  end case;
end process;

process (clk, trn_rst, mwr_data_cnt_cl)
begin
	if ((trn_rst = '1') or (mwr_data_cnt_cl = '1')) then
		mwr_data_cnt <= (others => '0');
	elsif clk'event and clk = '1' then
		if ((s_axis_tx_tready = '1') and ((mwr_data_cnt_en = '1') or (mwr_data_cnt_pr = '1'))) then
			mwr_data_cnt <= mwr_data_cnt + 1;
		end if;
	end if;
end process;

-------------------------------------------------------------------------------------------------------------
EP_TX_32B : if (EBFT_DWIDTH = 32) generate                                 -- ep_32b_tx_engine implementation
-------------------------------------------------------------------------------------------------------------

  type tx_state_type      is (TX_RST_STATE,
                              TX_CPL_CPLD_DW1,
														  TX_CPL_CPLD_DW2,
														  TX_CPLD_DW3,
														  TX_MWR_HDR,
														  TX_MWR_ADR,
														  TX_MWR_DAT,
														  TX_WAIT_STATE);
  signal tx_state         : tx_state_type;
  signal cpl_w_data       : std_logic;
  signal req_compl_q      : std_logic;
  signal req_compl_wd_q   : std_logic;

  begin

    tx_ft_delay_o <= '0';
    tx_ft_ready_o <= '0';

    -- Calculate First/Last DW Bytes Enable
		---------------------------------------
    process (dw_count, eb_byte_size_i)
    begin
      if (dw_count = "0000000001") then
        case eb_byte_size_i(1 downto 0) is
		      when "01"   => last_frst_be <= "00000001";
			    when "10"   => last_frst_be <= "00000011";
			    when "11"   => last_frst_be <= "00000111";
			    when others => last_frst_be <= "00001111";
		    end case;
	    else
        case eb_byte_size_i(1 downto 0) is
		      when "01"   => last_frst_be <= "00011111";
			    when "10"   => last_frst_be <= "00111111";
			    when "11"   => last_frst_be <= "01111111";
			    when others => last_frst_be <= "11111111";
		    end case;
	    end if;
    end process;

    -- Calculate 32-bit word count based on byte count
		--------------------------------------------------
    process (eb_byte_size_i)
    begin
      if (unsigned(eb_byte_size_i) > (EBFT_DWIDTH / 8)) then
	      if (unsigned(eb_byte_size_i(1 downto 0)) = 0) then
		      dw_count        <= ('0' & '0' & eb_byte_size_i(9 downto 2));
			    mwr_data_cnt_pr <= '0';
		    else
		      dw_count        <= (('0' & '0' & eb_byte_size_i(9 downto 2)) + 1);
			    mwr_data_cnt_pr <= '0';
		    end if;
	    else
        dw_count        <= std_logic_vector(to_unsigned(wd_count_tab(to_integer(unsigned(eb_byte_size_i))), 10));
		    mwr_data_cnt_pr <= '0';
	    end if;
    end process;

    mwr_data_cnt_tc <= '1' when (unsigned(mwr_data_cnt) = unsigned(dw_count)) else '0';

    -- Calculate lower address based on  byte enable
		------------------------------------------------
    process(rd_be_o_int, req_addr_i) begin
      case (rd_be_o_int(3 downto 0)) is
        when "0000" => lower_addr <= req_addr_i(6 downto 2) & "00";
        when "0001" => lower_addr <= req_addr_i(6 downto 2) & "00";
        when "0011" => lower_addr <= req_addr_i(6 downto 2) & "00";
        when "0101" => lower_addr <= req_addr_i(6 downto 2) & "00";
        when "0111" => lower_addr <= req_addr_i(6 downto 2) & "00";
        when "1001" => lower_addr <= req_addr_i(6 downto 2) & "00";
        when "1011" => lower_addr <= req_addr_i(6 downto 2) & "00";
        when "1101" => lower_addr <= req_addr_i(6 downto 2) & "00";
        when "1111" => lower_addr <= req_addr_i(6 downto 2) & "00";
        when "0010" => lower_addr <= req_addr_i(6 downto 2) & "01";
        when "0110" => lower_addr <= req_addr_i(6 downto 2) & "01";
        when "1010" => lower_addr <= req_addr_i(6 downto 2) & "01";
        when "1110" => lower_addr <= req_addr_i(6 downto 2) & "01";
        when "0100" => lower_addr <= req_addr_i(6 downto 2) & "10";
        when "1100" => lower_addr <= req_addr_i(6 downto 2) & "10";
        when others => lower_addr <= req_addr_i(6 downto 2) & "11"; -- "1000"
      end case;
    end process;

    process
    begin
      wait until rising_edge(clk);
      if (trn_rst = '1') then
        req_compl_q    <= '0';
        req_compl_wd_q <= '1';
      else
        req_compl_q    <= req_compl_i;
        req_compl_wd_q <= req_compl_wd_i;
      end if;
    end process;

    -- TX engine process
	  --------------------
    process
	  begin

      wait until rising_edge(clk);

      if (trn_rst = '1') then

        s_axis_tx_tdata      <= (others => '0');
        s_axis_tx_tkeep      <= (others => '1');
        s_axis_tx_tlast_int  <= '0';
        s_axis_tx_tvalid_int <= '0';
        s_axis_tx_tuser      <= (others => '0');
        compl_done           <= '0';
		    mwr_data_cnt_cl      <= '1';
        mwr_data_cnt_en      <= '0';
		    FT_clr_o             <= '0';
				req_tx_valid_o       <= '0';
        tlp_delayed          <= '0';
        tx_state             <= TX_RST_STATE;

      else

        compl_done     <= '0';
				req_tx_valid_o <= '0';

        case (tx_state) is

          when TX_RST_STATE =>

            if ((s_axis_tx_tready = '1') and (req_compl_q = '1') and (req_compl_wd_q = '1') and (FT_on_i = '0')) then

              s_axis_tx_tlast_int  <= '0';                                 -- Begin a CplD TLP
              s_axis_tx_tvalid_int <= '1';
              s_axis_tx_tdata      <= '0' &
                                      TX_CPLD_FMT_TYPE &
                                      '0' &
                                      req_tc_i &
                                      "0000" &
                                      '0' &
                                      req_ep_i &
                                      req_attr_i &
                                      "00" &
                                      req_len_i;
              cpl_w_data           <= req_compl_wd_i;
              tx_state             <= TX_CPL_CPLD_DW1;

            elsif ((s_axis_tx_tready = '1') and (req_compl_q = '1') and (req_compl_wd_q = '0') and (FT_on_i = '0')) then

              s_axis_tx_tlast_int  <= '0';                                 -- Begin a Cpl TLP
              s_axis_tx_tvalid_int <= '1';
              s_axis_tx_tdata      <= '0' &
                                      TX_CPL_FMT_TYPE &
                                      '0' &
                                      req_tc_i &
                                      "0000" &
                                      '0' &
                                      req_ep_i &
                                      req_attr_i &
                                      "00" &
                                      req_len_i;
              cpl_w_data           <= req_compl_wd_i;
              tx_state             <= TX_CPL_CPLD_DW1;

            elsif ((s_axis_tx_tready = '1') and (cfg_bus_mstr_enable_i = '1') and (requester_fmt_i = '0') and (tx_ft_ready_i = '1')) then

              s_axis_tx_tvalid_int <= '1';                                 -- Begin a MWr DMA 32-bit addressing TLP
							s_axis_tx_tkeep      <= x"F";
              s_axis_tx_tdata      <= ('0' &
						                           TX_MWR_FMT_TYPE &
                                       '0' &
									                     req_tc_i &
										                   "000000" &
										                   req_attr_i &
											                 "00" &
										                   dw_count);
							req_tx_valid_o       <= '1';
						  tx_state             <= TX_MWR_HDR;

            elsif ((s_axis_tx_tready = '0') and (cfg_bus_mstr_enable_i = '1') and (tx_ft_ready_i = '1')) then

              tlp_delayed <= '1';
              tx_state    <= TX_RST_STATE;

            else

              s_axis_tx_tlast_int  <= '0';
              s_axis_tx_tvalid_int <= '0';
              s_axis_tx_tdata      <= (others => '0');
              s_axis_tx_tkeep      <= x"F";
					    mwr_data_cnt_cl      <= '0';
              mwr_data_cnt_en      <= '0';
					    FT_clr_o             <= '0';
              tlp_delayed          <= '0';
              tx_state             <= TX_RST_STATE;

            end if;

          when TX_CPL_CPLD_DW1 =>                                          -- Completion with/without Data => header

            if (s_axis_tx_tready = '1') then

              s_axis_tx_tlast_int  <= '0';
              s_axis_tx_tvalid_int <= '1';
              s_axis_tx_tdata      <= completer_id_i &
                                      "000" &
                                      '0' &
                                      byte_count;
              tx_state             <= TX_CPL_CPLD_DW2;

            else

              tx_state <= TX_CPL_CPLD_DW1;

            end if;

          when TX_CPL_CPLD_DW2 =>                                          -- Completion with/without Data => addr

            if (s_axis_tx_tready = '1') then

              s_axis_tx_tlast_int  <= '0';
              s_axis_tx_tvalid_int <= '1';
              s_axis_tx_tdata      <= req_rid_i &
                                      req_tag_i &
                                      '0' &
                                      lower_addr;

              if (cpl_w_data = '1') then
                s_axis_tx_tlast_int <= '0';
                tx_state            <= TX_CPLD_DW3;
              else
                s_axis_tx_tlast_int <= '1';
								compl_done          <= '1';
                tx_state            <= TX_WAIT_STATE;
              end if;

            else

              tx_state <= TX_CPL_CPLD_DW2;

            end if;

          when TX_CPLD_DW3 =>                                              -- Completion with Data => data

            if (s_axis_tx_tready = '1') then

              s_axis_tx_tlast_int  <= '1';
              s_axis_tx_tvalid_int <= '1';
							if (E_Bone_trn_i = '1') then
                s_axis_tx_tdata <= ext_brd_data_i(0);
			        else
                s_axis_tx_tdata <= int_brd_data_i(0);
			        end if;
              compl_done <= '1';
              tx_state   <= TX_WAIT_STATE;

            else

              tx_state <= TX_CPLD_DW3;

            end if;

          when TX_MWR_HDR =>

            if (s_axis_tx_tready = '1') then

							mwr_data_cnt_cl <= '0';
              mwr_data_cnt_en <= '1';
              s_axis_tx_tdata <= (completer_id_i &
										              "00000000" &
											            last_frst_be);
							tx_state        <= TX_MWR_ADR;

						else
						  tx_state <= TX_MWR_HDR;
						end if;

					when TX_MWR_ADR =>

            if (s_axis_tx_tready = '1') then

						  if (mwr_data_cnt_tc = '1') then
							  s_axis_tx_tlast_int <= '1';
							  s_axis_tx_tkeep     <= x"F";
								mwr_data_cnt_cl     <= '1';
							  FT_clr_o            <= '1';
                tlp_delayed         <= '0';
							  tx_state            <= TX_RST_STATE;
							else
							  tx_state <= TX_MWR_DAT;
							end if;

              s_axis_tx_tdata <= eb_ft_addr_i(31 downto 2) & "00";
							tlp_tx_addr     <= eb_ft_addr_i(31 downto 2) & "00";

						else
              tlp_delayed <= '1';
						  tx_state    <= TX_MWR_ADR;
						end if;

					when TX_MWR_DAT =>

            if (s_axis_tx_tready = '1') then

						  if (mwr_data_cnt_tc = '1') then
							  s_axis_tx_tlast_int <= '1';
							  s_axis_tx_tkeep     <= x"F";
								mwr_data_cnt_cl     <= '1';
							  FT_clr_o            <= '1';
                tlp_delayed         <= '0';
							  tx_state            <= TX_RST_STATE;
							else
							  tx_state <= TX_MWR_DAT;
							end if;

							s_axis_tx_tdata <= eb_ft_data_i(7 downto 0) &
									               eb_ft_data_i(15 downto 8) &
									               eb_ft_data_i(23 downto 16) &
									               eb_ft_data_i(31 downto 24);

						else
              tlp_delayed <= '1';
						  tx_state    <= TX_MWR_DAT;
						end if;

          when TX_WAIT_STATE =>

            if (s_axis_tx_tready = '1') then

              s_axis_tx_tlast_int  <= '0';
              s_axis_tx_tvalid_int <= '0';
              s_axis_tx_tdata      <= (others => '0');
              tx_state             <= TX_RST_STATE;

            else

              tx_state <= TX_WAIT_STATE;

            end if;

          when others =>

            tx_state <= TX_RST_STATE;

        end case;

      end if;
    end process;

-------------------------------------------------------------------------------------------------------------
end generate EP_TX_32B;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
EP_TX_64B : if (EBFT_DWIDTH = 64) generate                                 -- ep_64b_tx_engine implementation
-------------------------------------------------------------------------------------------------------------

  type keep_typ           is array(0 to 15) of std_logic_vector(7 downto 0);
  constant keep_tab_32    : keep_typ := (x"0F", x"FF", x"FF", x"FF",
                                         x"FF", x"0F", x"0F", x"0F",
                                         x"0F", x"FF", x"FF", x"FF",
                                         x"FF", x"0F", x"0F", x"0F");
  constant keep_tab_64    : keep_typ := (x"FF", x"0F", x"0F", x"0F",
                                         x"0F", x"FF", x"FF", x"FF",
                                         x"FF", x"0F", x"0F", x"0F",
                                         x"0F", x"FF", x"FF", x"FF");
  type tx_state_type      is (TX_RST_STATE, TX_CPLD_QW1, TX_CPL_QW1,
                              TX_MWR_QWAD, TX_MWR64_ADDR,
                              TX_MWR_FF_HTLP, TX_MWR_FF_DTLP,
															TX_WAIT_STATE);

  signal tx_state         : tx_state_type;
  type ff_state_type      is (FF_RST_STATE,
                              FF_MWR_QWAD, FF_MWR_QWDD, FF_MWR_QWDD2, FF_MWR_QWDD1, FF_MWR_QWDD0,
                              FF_MWR64_ADDR, FF_MWR64_DATA, FF_MWR64_DATA2, FF_MWR64_DATA1, FF_MWR64_DATA0);
  signal ff_state         : ff_state_type;
  signal ext_brd_data_tmp : std32_x32;
  signal eb_ft_data_0     : std_logic_vector(EBFT_DWIDTH - 1 downto 0);
  signal eb_ft_data_1     : std_logic_vector(EBFT_DWIDTH - 1 downto 0);

  begin

    tx_ft_delay_o <= '0';
    tx_ft_ready_o <= '0';

    -- Calculate First/Last DW Bytes Enable
		---------------------------------------
    process (dw_count, eb_byte_size_i)
    begin
      if (dw_count = "0000000001") then
        case eb_byte_size_i(1 downto 0) is
		      when "01"   => last_frst_be <= "00000001";
			    when "10"   => last_frst_be <= "00000011";
			    when "11"   => last_frst_be <= "00000111";
			    when others => last_frst_be <= "00001111";
		    end case;
	    else
        case eb_byte_size_i(1 downto 0) is
		      when "01"   => last_frst_be <= "00011111";
			    when "10"   => last_frst_be <= "00111111";
			    when "11"   => last_frst_be <= "01111111";
			    when others => last_frst_be <= "11111111";
		    end case;
	    end if;
    end process;

    -- Calculate 32-bit word count based on byte count
		--------------------------------------------------
    process (eb_byte_size_i)
    begin
      if (unsigned(eb_byte_size_i) > (EBFT_DWIDTH / 8)) then
	      if (unsigned(eb_byte_size_i(1 downto 0)) = 0) then
		      dw_count        <= ('0' & '0' & eb_byte_size_i(9 downto 2));
			    mwr_data_cnt_pr <= '0';
		    else
		      dw_count        <= (('0' & '0' & eb_byte_size_i(9 downto 2)) + 1);
			    mwr_data_cnt_pr <= '0';
		    end if;
	    else
        dw_count        <= std_logic_vector(to_unsigned(wd_count_tab(to_integer(unsigned(eb_byte_size_i))), 10));
		    mwr_data_cnt_pr <= '0';
	    end if;
    end process;

    eb_ft_data_0    <= eb_ft_data_i;
		qw_count        <= '0' & dw_count(9 downto 1);
    mwr_data_cnt_tc <= '1' when (unsigned(mwr_data_cnt) = unsigned(qw_count)) else '0';
    tlp_valid       <= '1' when (((ext_brd_done_i = '0') and (E_Bone_trn_i = '0')) or ((ext_brd_done_i = '1') and (E_Bone_trn_i = '1'))) else '0';

    process(rd_be_o_int, req_addr_i)                                       -- Calculate lower address based on byte enable
    begin
      if (rd_be_o_int(0) = '1') then                                       -- when "---1"
        lower_addr <= req_addr_i(6 downto 2) & "00";
      elsif (rd_be_o_int(1) = '1') then                                    -- when "--10"
        lower_addr <= req_addr_i(6 downto 2) & "01";
      elsif (rd_be_o_int(2) = '1') then                                    -- when "-100"
        lower_addr <= req_addr_i(6 downto 2) & "10";
      elsif (rd_be_o_int(3) = '1') then                                    -- when "1000"
        lower_addr <= req_addr_i(6 downto 2) & "11";
      else                                                                 -- when "0000"
        lower_addr <= req_addr_i(6 downto 2) & "00";
      end if;
    end process;

    process (clk, trn_rst)
    begin
      if (trn_rst = '1') then
        for i in 0 to 31 loop
          ext_brd_data_tmp(i) <= (others => '0');
		    end loop;
	    elsif (clk'event and clk = '1') then
        for i in 0 to 31 loop
          ext_brd_data_tmp(i) <= ext_brd_data_i(i);
		    end loop;
	    end if;
    end process;

    process
    begin
      wait until rising_edge(clk);
        eb_ft_data_1 <= eb_ft_data_0;
    end process;

    -- FIFO engine process
	  ----------------------
    process
    begin

      wait until rising_edge(clk);

        if (trn_rst = '1') then

          wr_en       <= '0';
          wr_dat      <= (others => '0');
          tlp_tx_addr <= (others => '0');
          ff_state    <= FF_RST_STATE;

        else

          case (ff_state) is

            when FF_RST_STATE =>

              if ((requester_fmt_i = '0') and (tx_ft_ready_i = '1')) then

                wr_en  <= '1';                                             -- Begin a MWr DMA 32-bit addressing TLP
                wr_dat <= (completer_id_i &
										       "00000000" &
											     last_frst_be &
												   '0' &
						               TX_MWR_FMT_TYPE &
                           '0' &
									         req_tc_i &
										       "000000" &
										       req_attr_i &
											     "00" &
										       dw_count);
                ff_state <= FF_MWR_QWAD;

              elsif ((requester_fmt_i = '1') and (tx_ft_ready_i = '1')) then

                wr_en  <= '1';                                             -- Begin a MWr DMA 64-bit addressing TLP
                wr_dat <= (completer_id_i &
										       "00000000" &
											     last_frst_be &
												   '0' &
						               TX_MWR64_FMT_TYPE &
                           '0' &
									         req_tc_i &
										       "000000" &
										       req_attr_i &
											     "00" &
										       dw_count);
                ff_state <= FF_MWR64_ADDR;

              else

                wr_en    <= '0';
                wr_dat   <= (others => '0');
                ff_state <= FF_RST_STATE;

              end if;

            when FF_MWR_QWAD =>

              wr_dat      <= eb_ft_data_0(7 downto 0) &
											       eb_ft_data_0(15 downto 8) &
												     eb_ft_data_0(23 downto 16) &
					                   eb_ft_data_0(31 downto 24) &
										         eb_ft_addr_i(31 downto 2) & "00";
              tlp_tx_addr <= eb_ft_addr_i(31 downto 2) & "00" & eb_ft_data_0(31 downto 0);
              ff_state    <= FF_MWR_QWDD;

            when FF_MWR_QWDD =>

              if (eb_eof_i = '1') then
                ff_state <= FF_MWR_QWDD2;
              else
                ff_state <= FF_MWR_QWDD;
              end if;

              wr_dat <= eb_ft_data_0(7 downto 0) &
									      eb_ft_data_0(15 downto 8) &
									      eb_ft_data_0(23 downto 16) &
									      eb_ft_data_0(31 downto 24) &
							          eb_ft_data_1(39 downto 32) &
									      eb_ft_data_1(47 downto 40) &
									      eb_ft_data_1(55 downto 48) &
									      eb_ft_data_1(63 downto 56);

            when FF_MWR_QWDD2 =>

              wr_dat <= eb_ft_data_0(7 downto 0) &
									      eb_ft_data_0(15 downto 8) &
									      eb_ft_data_0(23 downto 16) &
									      eb_ft_data_0(31 downto 24) &
							          eb_ft_data_1(39 downto 32) &
									      eb_ft_data_1(47 downto 40) &
									      eb_ft_data_1(55 downto 48) &
									      eb_ft_data_1(63 downto 56);
              ff_state <= FF_MWR_QWDD1;

            when FF_MWR_QWDD1 =>

              wr_dat <= eb_ft_data_0(7 downto 0) &
									      eb_ft_data_0(15 downto 8) &
									      eb_ft_data_0(23 downto 16) &
									      eb_ft_data_0(31 downto 24) &
							          eb_ft_data_1(39 downto 32) &
									      eb_ft_data_1(47 downto 40) &
									      eb_ft_data_1(55 downto 48) &
									      eb_ft_data_1(63 downto 56);
              ff_state <= FF_MWR_QWDD0;

            when FF_MWR_QWDD0 =>

              wr_en  <= '0';
              wr_dat <= eb_ft_data_0(7 downto 0) &
									      eb_ft_data_0(15 downto 8) &
									      eb_ft_data_0(23 downto 16) &
									      eb_ft_data_0(31 downto 24) &
							          eb_ft_data_1(39 downto 32) &
									      eb_ft_data_1(47 downto 40) &
									      eb_ft_data_1(55 downto 48) &
									      eb_ft_data_1(63 downto 56);
              ff_state <= FF_RST_STATE;

            when FF_MWR64_ADDR =>

              wr_dat      <= eb_ft_addr_i(31 downto 2) & "00" & eb_ft_addr_i(63 downto 32);
              tlp_tx_addr <= eb_ft_addr_i(63 downto 2) & "00";
							ff_state    <= FF_MWR64_DATA;

            when FF_MWR64_DATA =>

              if (eb_eof_i = '1') then
                ff_state <= FF_MWR64_DATA2;
              else
                ff_state <= FF_MWR64_DATA;
              end if;

              wr_dat <= eb_ft_data_1(7 downto 0) &
									      eb_ft_data_1(15 downto 8) &
									      eb_ft_data_1(23 downto 16) &
									      eb_ft_data_1(31 downto 24) &
                        eb_ft_data_1(39 downto 32) &
									      eb_ft_data_1(47 downto 40) &
									      eb_ft_data_1(55 downto 48) &
									      eb_ft_data_1(63 downto 56);

            when FF_MWR64_DATA2 =>

              wr_dat <= eb_ft_data_1(7 downto 0) &
									      eb_ft_data_1(15 downto 8) &
									      eb_ft_data_1(23 downto 16) &
									      eb_ft_data_1(31 downto 24) &
                        eb_ft_data_1(39 downto 32) &
									      eb_ft_data_1(47 downto 40) &
									      eb_ft_data_1(55 downto 48) &
									      eb_ft_data_1(63 downto 56);
              ff_state <= FF_MWR64_DATA1;

            when FF_MWR64_DATA1 =>

              wr_dat <= eb_ft_data_1(7 downto 0) &
									      eb_ft_data_1(15 downto 8) &
									      eb_ft_data_1(23 downto 16) &
									      eb_ft_data_1(31 downto 24) &
                        eb_ft_data_1(39 downto 32) &
									      eb_ft_data_1(47 downto 40) &
									      eb_ft_data_1(55 downto 48) &
									      eb_ft_data_1(63 downto 56);
              ff_state <= FF_MWR64_DATA0;

            when FF_MWR64_DATA0 =>

              wr_en  <= '0';
              wr_dat <= eb_ft_data_1(7 downto 0) &
									      eb_ft_data_1(15 downto 8) &
									      eb_ft_data_1(23 downto 16) &
									      eb_ft_data_1(31 downto 24) &
                        eb_ft_data_1(39 downto 32) &
									      eb_ft_data_1(47 downto 40) &
									      eb_ft_data_1(55 downto 48) &
									      eb_ft_data_1(63 downto 56);
              ff_state <= FF_RST_STATE;

            when others =>

              wr_en    <= '0';
              wr_dat   <= (others => '0');
              ff_state <= FF_RST_STATE;

          end case;

        end if;

    end process;

    -- TX engine process
	  --------------------
    process
    begin

      wait until rising_edge(clk);

      if (trn_rst = '1') then

        s_axis_tx_tdata      <= (others => '0');
        s_axis_tx_tkeep      <= (others => '1');
        s_axis_tx_tlast_int  <= '0';
        s_axis_tx_tvalid_int <= '0';
        s_axis_tx_tuser      <= (others => '0');
        compl_done           <= '0';
        ext_data_ack_o       <= '0';
		    mwr_data_cnt_cl      <= '1';
        mwr_data_cnt_en      <= '0';
		    FT_clr_o             <= '0';
				req_tx_valid_o       <= '0';
        tlp_delayed          <= '0';
        tx_state             <= TX_RST_STATE;

      else

        compl_done     <= '0';
				req_tx_valid_o <= '0';

        case (tx_state) is

          when TX_RST_STATE =>

            if ((s_axis_tx_tready = '1') and (req_compl_i = '1') and (req_compl_wd_i = '1') and (tlp_valid = '1') and (FT_on_i = '0')) then

              s_axis_tx_tlast_int  <= '0';                                 -- Begin a CplD TLP
              s_axis_tx_tvalid_int <= '1';
              s_axis_tx_tkeep      <= x"FF";
              s_axis_tx_tdata      <= completer_id_i &
                                      "000" &
                                      '0' &
                                      byte_count &
                                      '0' &
                                      TX_CPLD_FMT_TYPE &
                                      '0' &
                                      req_tc_i &
                                      "0000" &
                                      '0' &
                                      req_ep_i &
                                      req_attr_i &
                                      "00" &
                                      req_len_i;
						  tx_state             <= TX_CPLD_QW1;

            elsif ((s_axis_tx_tready = '1') and (req_compl_i = '1') and (req_compl_wd_i = '0') and (tlp_valid = '1') and (FT_on_i = '0')) then

              s_axis_tx_tlast_int  <= '0';                                 -- Begin a Cpl TLP
              s_axis_tx_tvalid_int <= '1';
              s_axis_tx_tkeep      <= x"FF";
              s_axis_tx_tdata      <= completer_id_i &
                                      "000" &
                                      '0' &
                                      byte_count &
                                      '0' &
                                      TX_CPL_FMT_TYPE &
                                      '0' &
                                      req_tc_i &
                                      "0000" &
                                      '0' &
                                      req_ep_i &
                                      req_attr_i &
                                      "00" &
                                      req_len_i;
							tx_state             <= TX_CPL_QW1;

            elsif ((s_axis_tx_tready = '1') and (cfg_bus_mstr_enable_i = '1') and (rd_empty = '0')) then

              tx_state <= TX_MWR_FF_HTLP;                                  -- Begin a MWr DMA TLP

            elsif ((s_axis_tx_tready = '0') and (cfg_bus_mstr_enable_i = '1') and (tx_ft_ready_i = '1')) then

              tlp_delayed <= '1';
              tx_state    <= TX_RST_STATE;

            else

              s_axis_tx_tlast_int  <= '0';
              s_axis_tx_tvalid_int <= '0';
              s_axis_tx_tdata      <= (others => '0');
              s_axis_tx_tkeep      <= x"FF";
					    mwr_data_cnt_cl      <= '0';
              mwr_data_cnt_en      <= '0';
					    FT_clr_o             <= '0';
              tlp_delayed          <= '0';
              tx_state             <= TX_RST_STATE;

            end if;

          when TX_CPLD_QW1 =>                                              -- Completion with Data => Data

            if (s_axis_tx_tready = '1') then

              s_axis_tx_tlast_int  <= '1';
              s_axis_tx_tvalid_int <= '1';
							s_axis_tx_tkeep      <= x"FF";
							compl_done           <= '1';
							if (E_Bone_trn_i = '1') then
							  s_axis_tx_tdata <= ext_brd_data_tmp(0) & req_rid_i & req_tag_i & '0' & lower_addr;
							else
							  s_axis_tx_tdata <= int_brd_data_i(0) & req_rid_i & req_tag_i & '0' & lower_addr;
							end if;
							tx_state     <= TX_WAIT_STATE;

            else

              tx_state <= TX_CPLD_QW1;

            end if;

          when TX_CPL_QW1 =>                                               -- Completion without Data

            if (s_axis_tx_tready = '1') then

              s_axis_tx_tlast_int  <= '1';
              s_axis_tx_tvalid_int <= '1';
							s_axis_tx_tkeep      <= x"0F";
							s_axis_tx_tdata      <= X"00000000" & req_rid_i & req_tag_i & '0' & lower_addr;
							compl_done           <= '1';
							tx_state             <= TX_WAIT_STATE;

						else

              tx_state <= TX_CPL_QW1;

						end if;

          when TX_MWR_FF_HTLP =>

            s_axis_tx_tvalid_int <= '1';
						s_axis_tx_tkeep      <= x"FF";
            s_axis_tx_tdata      <= rd_dat;
						mwr_data_cnt_cl      <= '0';
            mwr_data_cnt_en      <= '1';
						req_tx_valid_o       <= '1';
            tx_state             <= TX_MWR_FF_DTLP;

          when TX_MWR_FF_DTLP =>

            if (s_axis_tx_tready = '1') then

              if (mwr_data_cnt_tc = '1') then
                s_axis_tx_tlast_int <= '1';
                if (requester_fmt_i = '1') then
                  s_axis_tx_tkeep <= keep_tab_64(to_integer(unsigned(eb_byte_size_i(3 downto 0))));
                else
                  s_axis_tx_tkeep <= keep_tab_32(to_integer(unsigned(eb_byte_size_i(3 downto 0))));
                end if;
                mwr_data_cnt_cl     <= '1';
                FT_clr_o            <= '1';
                tlp_delayed         <= '0';
                tx_state            <= TX_RST_STATE;
              else
                tx_state <= TX_MWR_FF_DTLP;
              end if;

              s_axis_tx_tdata <= rd_dat;

            else
              tlp_delayed <= '1';
              tx_state    <= TX_MWR_FF_DTLP;
            end if;

          when TX_WAIT_STATE =>

            tx_state <= TX_RST_STATE;

          when others =>

            tx_state <= TX_RST_STATE;

        end case;

      end if;
    end process;

--------------------------------------------------------------------------------------------------------------
end generate EP_TX_64B;
--------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
EP_TX_128B : if (EBFT_DWIDTH = 128) generate                               -- ep_128b_tx_engine implementation
--------------------------------------------------------------------------------------------------------------

  type array_x8            is array(0 to 7) of NATURAL;
  constant payload_val     : array_x8 := (128, 256, 512, 1024, 2048, 4096, 4096, 4096);

  type keep_typ            is array(0 to 15) of std_logic_vector(15 downto 0);
  constant keep_tab        : keep_typ := (x"FFFF", x"000F", x"000F", x"000F",
                                          x"000F", x"00FF", x"00FF", x"00FF",
                                          x"00FF", x"0FFF", x"0FFF", x"0FFF",
                                          x"0FFF", x"FFFF", x"FFFF", x"FFFF");

  type tx_state_type       is (TX_RST_STATE,
                               TX_MWR_QHDR,
															 TX_MWR_QHDM,
                               TX_MWR_QWDD,
													     TX_MWR_INT_QHDR,
                               TX_MWR_INT_QWDD,
												       TX_MWR64_DATA,
													     TX_MWR64_INT_DATA,
															 TX_MORE_STATE,
													     TX_WAIT_STATE);
  signal tx_state          : tx_state_type;

	type delay_type          is (DLY_INIT, DLY_WAIT, DLY_END);
	signal delay_state       : delay_type;

	signal tx_ft_ready       : std_logic;
	signal tx_ft_pause       : std_logic;
	signal tx_ft_delay_a     : std_logic;
	signal tx_ft_delay_b     : std_logic;
	signal eb_byte_size      : std_logic_vector(15 downto 0);
	signal eb_byte_more      : std_logic_vector(1 downto 0);
	signal byte_size_diff    : std_logic_vector(15 downto 0);
	signal last_frst_be_more : std_logic_vector(7 downto 0);
	signal CR                : std_logic_vector(23 downto 0);
	signal QW_carry          : std_logic_vector(23 downto 0);
	signal QWord             : std_logic_vector(EBFT_DWIDTH - 1 downto 0);
	signal QWord_s           : std_logic_vector(EBFT_DWIDTH - 1 downto 0);
	signal eb_ft_addr        : std_logic_vector(EBFT_DWIDTH - 1 downto 0);

  begin

    mwr_data_cnt_tc <= '1' when (unsigned(mwr_data_cnt) = unsigned(qw_count)) else '0';
    tlp_valid       <= '1' when (((ext_brd_done_i = '0') and (E_Bone_trn_i = '0')) or ((ext_brd_done_i = '1') and (E_Bone_trn_i = '1'))) else '0';
		tx_ft_ready_o   <= tx_ft_ready;

    -- TLP delayed when tx_buf_av low
    ---------------------------------
    process
    begin

      wait until rising_edge(clk);

        if (trn_rst = '1') then

				  tx_ft_delay_b <= '0';
				  delay_state   <= DLY_INIT;

		    else

				  case (delay_state) is
					  when DLY_INIT =>
						  if (tx_ft_delay_a = '1') then
							  tx_ft_delay_b <= '1';
                delay_state   <= DLY_WAIT;
              end if;
					  when DLY_WAIT =>
						  if (unsigned(tx_buf_av) > "000111") then
							  delay_state <= DLY_END;
							end if;
						when DLY_END =>
						  if (tx_ft_delay_a = '0') then
							  tx_ft_delay_b <= '0';
                delay_state   <= DLY_INIT;
							end if;
						when others =>
						  tx_ft_delay_b <= '0';
              delay_state   <= DLY_INIT;
					end case;

		    end if;

    end process;

    tx_ft_delay_a <= '1' when (((tx_ft_pause = '1') or (mwr_data_cnt_tc = '1')) and (unsigned(eb_byte_more) > "00") and (unsigned(tx_buf_av) < "001000")) else '0';
		tx_ft_delay_o <= '1' when ((tx_ft_delay_a = '1') or (tx_ft_delay_b = '1')) else '0';

    -- Calculate lower address based on byte enable
    -----------------------------------------------
    process(rd_be_o_int, req_addr_i)
    begin
      if (rd_be_o_int(0) = '1') then
        lower_addr <= req_addr_i(6 downto 2) & "00";
      elsif (rd_be_o_int(1) = '1') then
        lower_addr <= req_addr_i(6 downto 2) & "01";
      elsif (rd_be_o_int(2) = '1') then
        lower_addr <= req_addr_i(6 downto 2) & "10";
      elsif (rd_be_o_int(3) = '1') then
        lower_addr <= req_addr_i(6 downto 2) & "11";
      else
        lower_addr <= req_addr_i(6 downto 2) & "00";
      end if;
    end process;

    -- Calculate real byte count (based on a 128-byte payload !!!)
		--------------------------------------------------------------
    process (eb_ft_addr_i, eb_byte_size_i)
    begin
      case eb_ft_addr_i(1 downto 0) is
			  when "01"   =>
				  case eb_byte_size_i(7 downto 0) is
					  when x"00"  =>
						  eb_byte_size <= eb_byte_size_i;
							eb_byte_more <= "01";
					  when x"80"  =>
						  eb_byte_size <= eb_byte_size_i;
							eb_byte_more <= "01";
						when others =>
						  eb_byte_size <= std_logic_vector(unsigned(eb_byte_size_i) + unsigned(eb_ft_addr_i(1 downto 0)));
							eb_byte_more <= (others => '0');
					end case;
        when "10"   =>
				  case eb_byte_size_i(7 downto 0) is
					  when x"00"  =>
						  eb_byte_size <= eb_byte_size_i;
							eb_byte_more <= "10";
					  when x"7F"  =>
						  eb_byte_size <= eb_byte_size_i;
							eb_byte_more <= "01";
				    when x"80"  =>
						  eb_byte_size <= eb_byte_size_i;
							eb_byte_more <= "10";
						when x"FF"  =>
						  eb_byte_size <= eb_byte_size_i;
							eb_byte_more <= "01";
						when others =>
						  eb_byte_size <= std_logic_vector(unsigned(eb_byte_size_i) + unsigned(eb_ft_addr_i(1 downto 0)));
							eb_byte_more <= (others => '0');
					end case;
        when "11"   =>
				  case eb_byte_size_i(7 downto 0) is
					  when x"00"  =>
						  eb_byte_size <= eb_byte_size_i;
							eb_byte_more <= "11";
						when x"7E"  =>
						  eb_byte_size <= eb_byte_size_i;
							eb_byte_more <= "01";
						when x"7F"  =>
						  eb_byte_size <= eb_byte_size_i;
							eb_byte_more <= "10";
						when x"80"  =>
						  eb_byte_size <= eb_byte_size_i;
							eb_byte_more <= "11";
						when x"FE"  =>
						  eb_byte_size <= eb_byte_size_i;
							eb_byte_more <= "01";
						when x"FF"  =>
						  eb_byte_size <= eb_byte_size_i;
							eb_byte_more <= "10";
					  when others =>
						  eb_byte_size <= std_logic_vector(unsigned(eb_byte_size_i) + unsigned(eb_ft_addr_i(1 downto 0)));
							eb_byte_more <= (others => '0');
					end case;
        when others =>
				  eb_byte_size <= eb_byte_size_i;
					eb_byte_more <= (others => '0');
		  end case;
		end process;

    -- Calculate 32/128-bit word count based on byte count
		------------------------------------------------------
    process (eb_byte_size, tx_ft_ready_i, dw_count)
    begin

      if (unsigned(eb_byte_size) > (EBFT_DWIDTH / 8)) then
	      if (unsigned(eb_byte_size(1 downto 0)) = 0) then
		      dw_count <= ('0' & '0' & eb_byte_size(9 downto 2));
					qw_count <= ('0' & '0' & dw_count(9 downto 2));
					if (unsigned(dw_count(1 downto 0)) = 0) then
					  mwr_data_cnt_pr <= tx_ft_ready_i;
					else
					  mwr_data_cnt_pr <= '0';
					end if;
		    else
		      dw_count <= (('0' & '0' & eb_byte_size(9 downto 2)) + 1);
					qw_count <= ('0' & '0' & dw_count(9 downto 2));
					if (unsigned(dw_count(1 downto 0)) = 0) then
					  mwr_data_cnt_pr <= tx_ft_ready_i;
					else
					  mwr_data_cnt_pr <= '0';
					end if;
		    end if;
	    else
        dw_count        <= std_logic_vector(to_unsigned(wd_count_tab(to_integer(unsigned(eb_byte_size))), 10));
				qw_count        <= (others => '0');
		    mwr_data_cnt_pr <= '0';
	    end if;

    end process;

    -- Calculate diff. between (DW x 4) transfered and eb_byte_size_i requested
    ---------------------------------------------------------------------------
		process (eb_byte_size_i, dw_count)
    begin
			byte_size_diff <= std_logic_vector(to_unsigned((to_integer(unsigned(dw_count)) * 4), 16) - unsigned(eb_byte_size_i));
		end process;

    -- First/Last DW Bytes Enable processing
    ----------------------------------------
    process (dw_count, eb_ft_addr_i, eb_byte_size_i, cfg_dcommand_i)
    begin

      if (unsigned(dw_count) = "0000000001") then

        case eb_ft_addr_i(1 downto 0) is
          when "00" =>
	          case eb_byte_size_i(1 downto 0) is
              when "01"   => last_frst_be <= "00000001";  -- 1 byte trandfered
              when "10"   => last_frst_be <= "00000011";  -- 2 bytes trandfered
              when "11"   => last_frst_be <= "00000111";  -- 3 bytes trandfered
              when others => last_frst_be <= "00001111";  -- 4 bytes trandfered
            end case;
	        when "01" =>
	          case eb_byte_size_i(1 downto 0) is
              when "01"   => last_frst_be <= "00000010";  -- 2 bytes trandfered
              when "10"   => last_frst_be <= "00000110";  -- 3 bytes trandfered
              when "11"   => last_frst_be <= "00001110";  -- 4 bytes trandfered
              when others => last_frst_be <= "00001110";  -- unused
            end case;
	        when "10" =>
	          case eb_byte_size_i(1 downto 0) is
              when "01"   => last_frst_be <= "00000100";  -- 3 bytes trandfered
              when "10"   => last_frst_be <= "00001100";  -- 4 bytes trandfered
              when "11"   => last_frst_be <= "00001100";  -- unused
              when others => last_frst_be <= "00001100";  -- unused
            end case;
	        when "11" =>
	          case eb_byte_size_i(1 downto 0) is
              when "01"   => last_frst_be <= "00001000";  -- 4 bytes trandfered
              when "10"   => last_frst_be <= "00001000";  -- unused
              when "11"   => last_frst_be <= "00001000";  -- unused
              when others => last_frst_be <= "00001000";  -- unused
            end case;
	        when others =>
	          case eb_byte_size_i(1 downto 0) is
              when "01"   => last_frst_be <= "00000001";
              when "10"   => last_frst_be <= "00000011";
              when "11"   => last_frst_be <= "00000111";
              when others => last_frst_be <= "00001111";
            end case;
        end case;

      else

        case eb_ft_addr_i(1 downto 0) is
          when "00" =>
	          case eb_byte_size_i(1 downto 0) is
              when "01"   => last_frst_be <= "00011111";
              when "10"   => last_frst_be <= "00111111";
              when "11"   => last_frst_be <= "01111111";
              when others => last_frst_be <= "11111111";
            end case;
	        when "01" =>
						if (unsigned(eb_byte_size_i) = to_unsigned(payload_val(to_integer(unsigned(cfg_dcommand_i(7 downto 5)))), 16)) then
						  last_frst_be <= "11111110";
						-- elsif ((unsigned(eb_byte_size_i) + 1) = to_unsigned(payload_val(to_integer(unsigned(cfg_dcommand_i(7 downto 5)))), 16)) then
						--   last_frst_be <= "11111110";
						else
	            case eb_byte_size_i(1 downto 0) is
                when "01"   => last_frst_be <= "00111110";
                when "10"   => last_frst_be <= "01111110";
                when "11"   => last_frst_be <= "11111110";
                when others => last_frst_be <= "00011110";
              end case;
						end if;
	        when "10" =>
						if (unsigned(eb_byte_size_i) = to_unsigned(payload_val(to_integer(unsigned(cfg_dcommand_i(7 downto 5)))), 16)) then
						  last_frst_be <= "11111100";
						elsif ((unsigned(eb_byte_size_i) + 1) = to_unsigned(payload_val(to_integer(unsigned(cfg_dcommand_i(7 downto 5)))), 16)) then
							last_frst_be <= "11111100";
						else
					    case eb_byte_size_i(1 downto 0) is
                when "01"   => last_frst_be <= "01111100";
                when "10"   => last_frst_be <= "11111100";
                when "11"   => last_frst_be <= "00011100";
                when others => last_frst_be <= "00111100";
              end case;
						end if;
	        when "11" =>
						if (unsigned(eb_byte_size_i) = to_unsigned(payload_val(to_integer(unsigned(cfg_dcommand_i(7 downto 5)))), 16)) then
						  last_frst_be <= "11111000";
						elsif ((unsigned(eb_byte_size_i) + 1) = to_unsigned(payload_val(to_integer(unsigned(cfg_dcommand_i(7 downto 5)))), 16)) then
							last_frst_be <= "11111000";
						elsif ((unsigned(eb_byte_size_i) + 2) = to_unsigned(payload_val(to_integer(unsigned(cfg_dcommand_i(7 downto 5)))), 16)) then
							last_frst_be <= "11111000";
						else
					    case eb_byte_size_i(1 downto 0) is
                when "01"   => last_frst_be <= "11111000";
                when "10"   => last_frst_be <= "00011000";
                when "11"   => last_frst_be <= "00111000";
                when others => last_frst_be <= "01111000";
              end case;
						end if;
	        when others =>
	          case eb_byte_size_i(1 downto 0) is
              when "01"   => last_frst_be <= "00011111";
              when "10"   => last_frst_be <= "00111111";
              when "11"   => last_frst_be <= "01111111";
              when others => last_frst_be <= "11111111";
            end case;
        end case;

      end if;

    end process;

    -- TLP QWord and Carry processing
    ---------------------------------
    process
    begin

      wait until rising_edge(clk);

        if ((trn_rst = '1') or ((TLP_end_Rqst_i = '1') and (tx_ft_ready = '0'))) then
		      QWord <= (others => '0');
			    CR    <= (others => '0');
		    else
				  if (tx_ft_ready = '1') then
					  case (eb_byte_more) is
						  when "01" =>
							  QWord <= std64_null & std32_null & std24_null & QW_carry(7 downto 0);
							when "10" =>
							  QWord <= std64_null & std32_null & std16_null & QW_carry(15 downto 0);
							when "11" =>
							  QWord <= std64_null & std32_null & std8_null & QW_carry(23 downto 0);
							when others =>
							  QWord <= (others => '0');
						end case;
					else
		        case (eb_ft_addr_i(1 downto 0)) is
			        when "00" =>
				        QWord <= eb_ft_data_i;
				      when "01" =>
				        QWord <= eb_ft_data_i(119 downto 0) & CR(7 downto 0);
					      CR    <= x"0000" & eb_ft_data_i(127 downto 120);
				      when "10" =>
				        QWord <= eb_ft_data_i(111 downto 0) & CR(15 downto 0);
					      CR    <= x"00" & eb_ft_data_i(127 downto 112);
				      when "11" =>
				        QWord <= eb_ft_data_i(103 downto 0) & CR(23 downto 0);
					      CR    <= eb_ft_data_i(127 downto 104);
				      when others =>
				        QWord <= eb_ft_data_i;
			      end case;
					end if;
		    end if;

    end process;

    -- eb_ft_data register for 32-bit addressing TLP
    ------------------------------------------------
    process
    begin
      wait until rising_edge(clk);
			  if (trn_rst = '1') then
				  QWord_s <= (others => '0');
				else
				  QWord_s <= QWord;
				end if;
    end process;

    -- TX engine process
	  --------------------
    process
    begin

      wait until rising_edge(clk);

      if (trn_rst = '1') then

        wr_en                <= '0';
        wr_dat               <= (others => '0');
        s_axis_tx_tdata      <= (others => '0');
        s_axis_tx_tkeep      <= (others => '1');
        s_axis_tx_tlast_int  <= '0';
        s_axis_tx_tvalid_int <= '0';
        s_axis_tx_tuser      <= (others => '0');
				eb_ft_addr           <= (others => '0');
				last_frst_be_more    <= (others => '0');
				QW_carry             <= (others => '0');
        compl_done           <= '0';
				ext_data_ack_o       <= '0';
		    mwr_data_cnt_cl      <= '1';
        mwr_data_cnt_en      <= '0';
		    FT_clr_o             <= '0';
				req_tx_valid_o       <= '0';
        tlp_delayed          <= '0';
				tx_ft_ready          <= '0';
				tx_ft_pause          <= '0';
        tx_state             <= TX_RST_STATE;

      else

        compl_done     <= '0';
				ext_data_ack_o <= '0';
				req_tx_valid_o <= '0';

        case (tx_state) is

          when TX_RST_STATE =>

            if ((s_axis_tx_tready = '1') and (req_compl_i = '1') and (req_compl_wd_i = '1') and (tlp_valid = '1') and (FT_on_i = '0')) then

              s_axis_tx_tvalid_int <= '1';                                 -- Begin a CplD TLP
							s_axis_tx_tlast_int  <= '1';
						  if (E_Bone_trn_i = '1') then
                s_axis_tx_tdata(127 downto 96) <= ext_brd_data_i(0);
              else
                s_axis_tx_tdata(127 downto 96) <= int_brd_data_i(0);
              end if;
						  s_axis_tx_tdata(95 downto 0) <= (req_rid_i &
                                               req_tag_i &
                                               '0' &
                                               lower_addr &
                                               completer_id_i &
                                               "000" &
                                               '0' &
                                               byte_count &
                                               '0' &
                                               TX_CPLD_FMT_TYPE &
                                               '0' &
                                               req_tc_i &
                                               "0000" &
                                               '0' &
                                               req_ep_i &
                                               req_attr_i &
                                               "00" &
                                               req_len_i);
              s_axis_tx_tkeep <= x"FFFF";
						  compl_done      <= '1';
						  tx_state        <= TX_WAIT_STATE;

					  elsif ((s_axis_tx_tready = '1') and (req_compl_i = '1') and (req_compl_wd_i = '0') and (tlp_valid = '1') and (FT_on_i = '0')) then

              s_axis_tx_tvalid_int <= '1';                                 -- Begin a Cpl TLP
							s_axis_tx_tlast_int  <= '1';
						  s_axis_tx_tdata      <= (X"00000000" &
                                       req_rid_i &
                                       req_tag_i &
                                       '0' &
                                       lower_addr &
                                       completer_id_i &
                                       "000" &
                                       '0' &
                                       byte_count &
                                       '0' &
                                       TX_CPL_FMT_TYPE &
                                       '0' &
                                       req_tc_i &
                                       "0000" &
                                       '0' &
                                       req_ep_i &
                                       req_attr_i &
                                       "00" &
                                       req_len_i);
              s_axis_tx_tkeep      <= x"0FFF";
						  compl_done           <= '1';
						  tx_state             <= TX_WAIT_STATE;

            elsif ((s_axis_tx_tready = '1') and (cfg_bus_mstr_enable_i = '1') and (requester_fmt_i = '0') and (tx_ft_ready_i = '1')) then

              req_tx_valid_o <= '1';
              tx_state       <= TX_MWR_QHDR;                               -- Begin a MWr DMA 32-bit addressing TLP

            elsif ((s_axis_tx_tready = '1') and (cfg_bus_mstr_enable_i = '1') and (requester_fmt_i = '0') and (tx_ft_ready = '1')) then

              req_tx_valid_o <= '1';
							tx_ft_pause    <= '0';
              tx_state       <= TX_MWR_QHDM;                               -- Begin a MWr DMA 32-bit addressing TLP for missing bytes transfer

            elsif ((s_axis_tx_tready = '1') and (cfg_bus_mstr_enable_i = '1') and (requester_fmt_i = '1') and (tx_ft_ready_i = '1')) then

              s_axis_tx_tvalid_int <= '1';                                 -- Begin a MWr DMA 64-bit addressing TLP
							s_axis_tx_tkeep      <= x"FFFF";
							mwr_data_cnt_cl      <= '0';
              mwr_data_cnt_en      <= '1';
							req_tx_valid_o       <= '1';
              tx_state             <= TX_MWR64_DATA;

              s_axis_tx_tdata <= (eb_ft_addr_i(31 downto 2) &              -- 98..127
																	"00" &                                   -- 96..97
																	eb_ft_addr_i(63 downto 32) &             -- 64..95
																	completer_id_i &                         -- 48..63
										              "00000000" &                             -- 40..47
											            last_frst_be &                           -- 32..39
																	'0' &                                    -- 31
						                      TX_MWR64_FMT_TYPE &                      -- 24..30
                                  '0' &                                    -- 23
									                req_tc_i &                               -- 20..22
										              "000000" &                               -- 14..19
										              req_attr_i &                             -- 12..13
											            "00" &                                   -- 10..11
										              dw_count);                               -- 0..9

              tlp_tx_addr <= std64_null & eb_ft_addr_i(63 downto 2) & "00";

              -- Next address for missing bytes transfer
							eb_ft_addr(63 downto 0)     <= std_logic_vector((unsigned(eb_ft_addr_i(63 downto 2)) & "00") + unsigned(eb_byte_size_i) + unsigned(byte_size_diff));

            elsif ((s_axis_tx_tready = '1') and (cfg_bus_mstr_enable_i = '1') and (requester_fmt_i = '1') and (tx_ft_ready = '1')) then

              s_axis_tx_tvalid_int <= '1';                                 -- Begin a MWr DMA 64-bit addressing TLP for missing bytes transfer
							s_axis_tx_tkeep      <= x"FFFF";
							req_tx_valid_o       <= '1';
							tx_ft_pause          <= '0';
              tx_state             <= TX_MWR64_DATA;

              s_axis_tx_tdata <= (eb_ft_addr(31 downto 2) &                -- 98..127
																	"00" &                                   -- 96..97
																	eb_ft_addr(63 downto 32) &               -- 64..95
																	completer_id_i &                         -- 48..63
										              "00000000" &                             -- 40..47
											            last_frst_be_more &                      -- 32..39
																	'0' &                                    -- 31
						                      TX_MWR64_FMT_TYPE &                      -- 24..30
                                  '0' &                                    -- 23
									                req_tc_i &                               -- 20..22
										              "000000" &                               -- 14..19
										              req_attr_i &                             -- 12..13
											            "00" &                                   -- 10..11
										              "0000000001");                           -- 0..9

              tlp_tx_addr <= std64_null & eb_ft_addr(63 downto 2) & "00";

            elsif ((s_axis_tx_tready = '1') and (cfg_bus_mstr_enable_i = '1') and (requester_fmt_i = '0') and (ext_data_flg_i = '1')) then

              req_tx_valid_o <= '1';
              tx_state       <= TX_MWR_INT_QHDR;                           -- Begin a MWr DMA 32-bit addressing TLP internal data transfer

            elsif ((s_axis_tx_tready = '1') and (cfg_bus_mstr_enable_i = '1') and (requester_fmt_i = '1') and (ext_data_flg_i = '1')) then

              s_axis_tx_tvalid_int <= '1';                                 -- Begin a MWr DMA 64-bit addressing TLP internal data transfer
							s_axis_tx_tkeep      <= x"FFFF";
							tx_state             <= TX_MWR64_INT_DATA;

              s_axis_tx_tdata <= (ext_addr_src_i(31 downto 2) &            -- 98..127
																	"00" &                                   -- 96..97
																	std32_null &                             -- 64..95
																	completer_id_i &                         -- 48..63
										              "00000000" &                             -- 40..47
											            "11111111" &                             -- 32..39
																	'0' &                                    -- 31
						                      TX_MWR64_FMT_TYPE &                      -- 24..30
                                  '0' &                                    -- 23
									                req_tc_i &                               -- 20..22
										              "000000" &                               -- 14..19
										              req_attr_i &                             -- 12..13
											            "00" &                                   -- 10..11
										              "0000000100");                           -- 0..9

              tlp_tx_addr <= std64_null & ext_addr_src_i(63 downto 2) & "00";

            elsif ((s_axis_tx_tready = '0') and (cfg_bus_mstr_enable_i = '1') and (tx_ft_ready_i = '1')) then

              tlp_delayed <= '1';
              tx_state    <= TX_RST_STATE;

            elsif ((s_axis_tx_tready = '0') and (cfg_bus_mstr_enable_i = '1') and (tx_ft_ready = '1')) then

              tlp_delayed <= '1';
              tx_state    <= TX_RST_STATE;

					  else

              s_axis_tx_tlast_int  <= '0';
              s_axis_tx_tvalid_int <= '0';
              s_axis_tx_tdata      <= (others => '0');
              s_axis_tx_tkeep      <= (others => '1');
							mwr_data_cnt_cl      <= '0';
              mwr_data_cnt_en      <= '0';
						  FT_clr_o             <= '0';
              tlp_delayed          <= '0';
              tx_state             <= TX_RST_STATE;

            end if;

          when TX_MWR_QHDR =>

            if (s_axis_tx_tready = '1') then

						  s_axis_tx_tvalid_int <= '1';                                 -- MWr DMA 32-bit addressing TLP delayed
							s_axis_tx_tkeep      <= x"FFFF";
              s_axis_tx_tdata      <= (QWord(7 downto 0) &
															         QWord(15 downto 8) &
															         QWord(23 downto 16) &
															         QWord(31 downto 24) &
							                         eb_ft_addr_i(31 downto 2) &
																	     "00" &
																	     completer_id_i &
										                   "00000000" &
											                 last_frst_be &
																	     '0' &
						                           TX_MWR_FMT_TYPE &
                                       '0' &
									                     req_tc_i &
										                   "000000" &
										                   req_attr_i &
											                 "00" &
										                   dw_count);

					    tlp_tx_addr <= std64_null & std32_null & eb_ft_addr_i(31 downto 2) & "00";

              -- Next address for missing bytes transfer
							eb_ft_addr(63 downto 0) <= std_logic_vector((unsigned(eb_ft_addr_i(63 downto 2)) & "00") + unsigned(eb_byte_size_i) + unsigned(byte_size_diff));

              mwr_data_cnt_cl <= '0';
              mwr_data_cnt_en <= '1';
						  tx_state        <= TX_MWR_QWDD;

						else
						  tx_state <= TX_MWR_QHDR;
						end if;

          when TX_MWR_QHDM =>

            if (s_axis_tx_tready = '1') then

						  s_axis_tx_tvalid_int <= '1';                                 -- MWr DMA 32-bit addressing TLP delayed
							s_axis_tx_tkeep      <= x"FFFF";
              s_axis_tx_tdata      <= (QWord(7 downto 0) &
															         QWord(15 downto 8) &
															         QWord(23 downto 16) &
															         QWord(31 downto 24) &
							                         eb_ft_addr(31 downto 2) &
																	     "00" &
																	     completer_id_i &
										                   "00000000" &
											                 last_frst_be_more &
																	     '0' &
						                           TX_MWR_FMT_TYPE &
                                       '0' &
									                     req_tc_i &
										                   "000000" &
										                   req_attr_i &
											                 "00" &
										                   "0000000001");

					    tlp_tx_addr <= std64_null & std32_null & eb_ft_addr(31 downto 2) & "00";
						  tx_state    <= TX_MWR_QWDD;

						else
						  tx_state <= TX_MWR_QHDR;
						end if;

          when TX_MWR_QWDD =>

            if (s_axis_tx_tready = '1') then

						  if (mwr_data_cnt_tc = '1') then
							  s_axis_tx_tlast_int <= '1';
							  s_axis_tx_tkeep     <= keep_tab(to_integer(unsigned(eb_byte_size(3 downto 0)))) and x"0FFF";
								mwr_data_cnt_cl     <= '1';
							  FT_clr_o            <= '1';
                tlp_delayed         <= '0';
								if (unsigned(eb_byte_more) > "00") then
								  tx_ft_ready <= '1';
									tx_ft_pause <= '1';
									case (eb_byte_more) is
									  when "01" =>
										  case (eb_ft_addr(1 downto 0)) is
											  when "01" => last_frst_be_more <= "00000010";
												when "10" => last_frst_be_more <= "00000100";
												when "11" => last_frst_be_more <= "00001000";
												when others => last_frst_be_more <= "00000001";
											end case;
										when "10" =>
										  case (eb_ft_addr(1 downto 0)) is
											  when "01" => last_frst_be_more <= "00000110";
												when "10" => last_frst_be_more <= "00001100";
												when "11" => last_frst_be_more <= "00001000";
												when others => last_frst_be_more <= "00000011";
											end case;
										when "11" =>
										  case (eb_ft_addr(1 downto 0)) is
											  when "01" => last_frst_be_more <= "00001110";
												when "10" => last_frst_be_more <= "00001100";
												when "11" => last_frst_be_more <= "00001000";
												when others => last_frst_be_more <= "00000111";
											end case;
										when others =>
											last_frst_be_more <= "00000001";
									end case;
									tx_state <= TX_MORE_STATE;
								else
								  tx_ft_ready <= '0';
								  tx_state    <= TX_RST_STATE;
								end if;
							elsif (tx_ft_ready = '1') then
							  s_axis_tx_tlast_int <= '1';
								s_axis_tx_tkeep     <= keep_tab(to_integer("00" & unsigned(eb_byte_more))) and x"0FFF";
							  tx_ft_ready         <= '0';
								mwr_data_cnt_cl     <= '1';
								tx_state            <= TX_RST_STATE;
							else
							  tx_state <= TX_MWR_QWDD;
							end if;

							s_axis_tx_tdata <= QWord(7 downto 0) &
															   QWord(15 downto 8) &
															   QWord(23 downto 16) &
															   QWord(31 downto 24) &
							                   QWord_s(103 downto 96) &
                                 QWord_s(111 downto 104) &
															   QWord_s(119 downto 112) &
															   QWord_s(127 downto 120) &
															   QWord_s(71 downto 64) &
															   QWord_s(79 downto 72) &
															   QWord_s(87 downto 80) &
															   QWord_s(95 downto 88) &
															   QWord_s(39 downto 32) &
															   QWord_s(47 downto 40) &
															   QWord_s(55 downto 48) &
															   QWord_s(63 downto 56);

              QW_carry <= CR;

						else
              tlp_delayed <= '1';
						  tx_state    <= TX_MWR_QWDD;
						end if;

          when TX_MWR_INT_QHDR =>

            if (s_axis_tx_tready = '1') then

						  s_axis_tx_tvalid_int <= '1';                                 -- MWr DMA 32-bit addressing TLP delayed
							s_axis_tx_tkeep      <= x"FFFF";
              s_axis_tx_tdata      <= (ext_data_src_i(7 downto 0) &
															         ext_data_src_i(15 downto 8) &
															         ext_data_src_i(23 downto 16) &
															         ext_data_src_i(31 downto 24) &
							                         ext_addr_src_i(31 downto 2) &
																	     "00" &
																	     completer_id_i &
										                   "00000000" &
											                 "11111111" &
																	     '0' &
						                           TX_MWR_FMT_TYPE &
                                       '0' &
									                     req_tc_i &
										                   "000000" &
										                   req_attr_i &
											                 "00" &
										                   "0000000100");

					    tlp_tx_addr <= std64_null & std32_null & ext_addr_src_i(31 downto 2) & "00";
						  tx_state    <= TX_MWR_INT_QWDD;

						else
						  tx_state <= TX_MWR_INT_QHDR;
						end if;

          when TX_MWR_INT_QWDD =>

            if (s_axis_tx_tready = '1') then

              s_axis_tx_tlast_int <= '1';
							s_axis_tx_tkeep     <= x"0FFF";
						  ext_data_ack_o      <= '1';
              tx_state            <= TX_RST_STATE;
							s_axis_tx_tdata     <= ext_data_src_i(7 downto 0) &
															       ext_data_src_i(15 downto 8) &
															       ext_data_src_i(23 downto 16) &
															       ext_data_src_i(31 downto 24) &
							                       ext_data_src_i(103 downto 96) &
                                     ext_data_src_i(111 downto 104) &
															       ext_data_src_i(119 downto 112) &
															       ext_data_src_i(127 downto 120) &
															       ext_data_src_i(71 downto 64) &
															       ext_data_src_i(79 downto 72) &
															       ext_data_src_i(87 downto 80) &
															       ext_data_src_i(95 downto 88) &
															       ext_data_src_i(39 downto 32) &
															       ext_data_src_i(47 downto 40) &
															       ext_data_src_i(55 downto 48) &
															       ext_data_src_i(63 downto 56);

            else
						  tx_state <= TX_MWR_INT_QWDD;
						end if;

          when TX_MWR64_DATA =>

            if (s_axis_tx_tready = '1') then

						  if (mwr_data_cnt_tc = '1') then
							  s_axis_tx_tlast_int <= '1';
								s_axis_tx_tkeep     <= keep_tab(to_integer(unsigned(eb_byte_size(3 downto 0))));
								mwr_data_cnt_cl     <= '1';
							  FT_clr_o            <= '1';
                tlp_delayed         <= '0';
								if (unsigned(eb_byte_more) > "00") then
								  tx_ft_ready <= '1';
									tx_ft_pause <= '1';
									case (eb_byte_more) is
									  when "01" =>
										  case (eb_ft_addr(1 downto 0)) is
											  when "01" => last_frst_be_more <= "00000010";
												when "10" => last_frst_be_more <= "00000100";
												when "11" => last_frst_be_more <= "00001000";
												when others => last_frst_be_more <= "00000001";
											end case;
										when "10" =>
										  case (eb_ft_addr(1 downto 0)) is
											  when "01" => last_frst_be_more <= "00000110";
												when "10" => last_frst_be_more <= "00001100";
												when "11" => last_frst_be_more <= "00001000";
												when others => last_frst_be_more <= "00000011";
											end case;
										when "11" =>
										  case (eb_ft_addr(1 downto 0)) is
											  when "01" => last_frst_be_more <= "00001110";
												when "10" => last_frst_be_more <= "00001100";
												when "11" => last_frst_be_more <= "00001000";
												when others => last_frst_be_more <= "00000111";
											end case;
										when others =>
											last_frst_be_more <= "00000001";
									end case;
									tx_state <= TX_MORE_STATE;
								else
								  tx_ft_ready <= '0';
								  tx_state    <= TX_RST_STATE;
								end if;
						  elsif (tx_ft_ready = '1') then
							  s_axis_tx_tlast_int <= '1';
								s_axis_tx_tkeep     <= keep_tab(to_integer("00" & unsigned(eb_byte_more)));
							  tx_ft_ready         <= '0';
								mwr_data_cnt_cl     <= '1';
								tx_state            <= TX_RST_STATE;
							else
							  tx_state <= TX_MWR64_DATA;
							end if;

              s_axis_tx_tdata <= QWord(103 downto 96) &
                                 QWord(111 downto 104) &
															   QWord(119 downto 112) &
															   QWord(127 downto 120) &
															   QWord(71 downto 64) &
															   QWord(79 downto 72) &
															   QWord(87 downto 80) &
															   QWord(95 downto 88) &
															   QWord(39 downto 32) &
															   QWord(47 downto 40) &
															   QWord(55 downto 48) &
															   QWord(63 downto 56) &
															   QWord(7 downto 0) &
															   QWord(15 downto 8) &
															   QWord(23 downto 16) &
															   QWord(31 downto 24);

              QW_carry <= CR;

						else
              tlp_delayed <= '1';
						  tx_state    <= TX_MWR64_DATA;
						end if;

          when TX_MWR64_INT_DATA =>

            if (s_axis_tx_tready = '1') then

              s_axis_tx_tlast_int <= '1';
						  ext_data_ack_o      <= '1';
              tx_state            <= TX_RST_STATE;
              s_axis_tx_tdata     <= ext_data_src_i(103 downto 96) &
                                     ext_data_src_i(111 downto 104) &
															       ext_data_src_i(119 downto 112) &
															       ext_data_src_i(127 downto 120) &
															       ext_data_src_i(71 downto 64) &
															       ext_data_src_i(79 downto 72) &
															       ext_data_src_i(87 downto 80) &
															       ext_data_src_i(95 downto 88) &
															       ext_data_src_i(39 downto 32) &
															       ext_data_src_i(47 downto 40) &
															       ext_data_src_i(55 downto 48) &
															       ext_data_src_i(63 downto 56) &
															       ext_data_src_i(7 downto 0) &
															       ext_data_src_i(15 downto 8) &
															       ext_data_src_i(23 downto 16) &
															       ext_data_src_i(31 downto 24);

            else
						  tx_state <= TX_MWR64_INT_DATA;
						end if;

          when TX_MORE_STATE =>

            s_axis_tx_tlast_int  <= '0';
            s_axis_tx_tvalid_int <= '0';
            s_axis_tx_tdata      <= (others => '0');
            s_axis_tx_tkeep      <= (others => '1');
						FT_clr_o             <= '0';
						tx_state             <= TX_RST_STATE;

          when TX_WAIT_STATE =>

            s_axis_tx_tlast_int  <= '0';
            s_axis_tx_tvalid_int <= '0';
            s_axis_tx_tdata      <= (others => '0');
            s_axis_tx_tkeep      <= (others => '1');

            if ((req_compl_i = '0') and (req_compl_wd_i = '0')) then
              tx_state <= TX_RST_STATE;
            end if;

          when others =>

            tx_state <= TX_RST_STATE;

        end case;

      end if;
    end process;

---------------------------------------------------------------------------------------------------------------
end generate EP_TX_128B;
---------------------------------------------------------------------------------------------------------------

end ep_tx_rtl;
