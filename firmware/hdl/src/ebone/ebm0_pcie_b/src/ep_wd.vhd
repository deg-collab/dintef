-------------------------------------------------------------------------------
--
-- Project     : Series 7 Integrated Block for PCI Express
-- File        : ep_wd.vhd
-- Description : Endpoint watch dog for software monitoring
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.ep_comp_pkg.all;

library unisim;
use unisim.vcomponents.BUFR;

entity ep_wd is

generic (
  TRGT_DESC : NATURAL := 16#00#
);

port (
  clk       : in  std_logic;
  trn_rst   : in  std_logic;
	rst_soft  : in  std_logic;
  soft_wd   : out std_logic;
	FT_on_i   : in std_logic
);
end ep_wd;

architecture rtl of ep_wd is

signal clk_4m : std_logic;
signal wd_clr : std_logic;
signal dv_tgl : std_logic;
signal dv_cnt : std_logic_vector(3 downto 0);
signal wd_cnt : std_logic_vector(21 downto 0);

begin

bufr_kx7_gene : if (TRGT_DESC = 16#71#) or (TRGT_DESC = 16#72#) or (TRGT_DESC = 16#73#) generate

  bufr_inst : BUFR
  generic map (
    SIM_DEVICE  => "7SERIES",
    BUFR_DIVIDE => "8"
  )
  port map (
    O           => clk_4m,
    CE          => '1',
    CLR         => trn_rst,
    I           => dv_tgl
  );

end generate bufr_kx7_gene;

soft_wd <= '1' when (wd_cnt = x"3D0901") else '0';
wd_clr  <= '1' when ((rst_soft = '1') or (FT_on_i = '1')) else '0';

process(clk, trn_rst)
begin

  if (trn_rst = '1') then
    dv_tgl <= '0';
	elsif (clk'event and clk = '1') then
    dv_tgl <= not dv_tgl;
	end if;

end process;

process(clk, trn_rst)
begin

  if (trn_rst = '1') then
    dv_cnt <= (others => '0');
	elsif (clk'event and clk = '1') then
    dv_cnt <= dv_cnt + 1;
	end if;

end process;

process(clk_4m, trn_rst, wd_clr)
begin

  if ((trn_rst = '1') or (wd_clr = '1')) then
    wd_cnt <= (others => '0');
	elsif (clk_4m'event and clk_4m = '1') then
    wd_cnt <= wd_cnt + 1;
	end if;

end process;

end; -- ep_wd

