--------------------------------------------------------------------------------
--
-- E-Bone Master#0/Slave#0 to PCI_Express Endpoint interface
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--
--------------------------------------------------------------------------------
--
-- Description:
-- ------------
-- Entity module:  Performs connection between the FPGA PCI-Express I/O links
--                 and the Master#0/Slave#0 E-bone bus signals
--
-- Version  Date        Author
-- 3.2      07/03/2017  Le Caer
--
-- http://www.esrf.fr
--
--------------------------------------------------------------------------------
--
-- Dependencies:
-- -------------
-- ebs_pkg
-- ebm0_pcie_b_pkg
-- ep_eb_link
-- ep_app
-- ep_assert
-- s7_pcie_ep_v3_1_core_top
-- s7_pcie_ep_v3_1_pipe_clock
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.ep_comp_pkg.all;
use work.ep_function_pkg.all;
use work.ebm0_pcie_b_pkg.all;
use work.ebs_pkg.all;

library unisim;
use unisim.vcomponents.IBUFDS_GTE2;

entity ebm0_pcie_b is

generic (
  EB_CLK             : REAL := 8.0;                                       -- E-Bone clock period in ns
	EBFT_DWIDTH        : NATURAL := 64;                                     -- FT data width
	BADR0_MEM          : INTEGER := 1;                                      -- 2Kbyte BADR0 Block Ram Memory implementation when 1
  WATCH_DOG          : INTEGER := 1;                                      -- Watch dog process implementation when 1
  TYPE_DESC          : NATURAL := 16#00000000#;                           -- Type description application and/or target dependant
	HARD_VERS          : NATURAL := 16#00000000#;                           -- Hardware version implemented
  DEVICE_ID          : BIT_VECTOR := X"EB01";                             -- Device ID = E-Bone 01
	BC_PERIOD          : BIT_VECTOR := X"11";                               -- Broad-Call period (0:1us, 1:10us, 2:100us, 3:1ms)
	LINK_SPEED         : INTEGER := 1;                                      -- PCI-Express EndPoint Link Speed (1=2.5G, 2=5G)
	LINK_WIDTH         : INTEGER := 1;                                      -- PCI-Express EndPoint Lane numbers (1,4,8)
  SIM_FILE           : STRING;                                            -- Simulation script
	STAT_COUNTING      : INTEGER := 1;                                      -- ep_stat module implementation when 1
  EBM_PCIE_PRB       : STRING := "OFF"                                    -- ebm0_pcie_b dedicated probes unused when OFF. ep_probe = ap_probe when OFF.
);

port (

--
-- PCIe Endpoint dedicated
--

  sys_clk_p          : in std_logic;                                      -- 100MHz positive differential input clock signal (from PCIe slot)
  sys_clk_n          : in std_logic;                                      -- 100MHz negative differential input clock signal (from PCIe slot)
  sys_reset_n        : in std_logic;                                      -- Asynchronous active low PCIe module reset input signal (from PCIe slot)

  pci_exp_rxn        : in std_logic_vector(LINK_WIDTH - 1 downto 0);      -- PCIe receive negative serial differential input signal (from PCIe slot)
  pci_exp_rxp        : in std_logic_vector(LINK_WIDTH - 1 downto 0);      -- PCIe receive positive serial differential input signal (from PCIe slot)
  pci_exp_txn        : out std_logic_vector(LINK_WIDTH - 1 downto 0);     -- PCIe transmit negative serial differential input signal (from PCIe slot)
  pci_exp_txp        : out std_logic_vector(LINK_WIDTH - 1 downto 0);     -- PCIe transmit positive serial differential input signal (from PCIe slot)

  ep_link_up         : out std_logic;				                              -- High when Link is Up
	ep_1_lane          : out std_logic;				                              -- High when 1 lane detected
	ep_4_lane          : out std_logic;				                              -- High when 4 lanes detected
	ep_8_lane          : out std_logic;				                              -- High when 8 lanes detected

  soft_wd            : out std_logic;                                     -- Active high external watch dog reset signal, witdh=250ns, period=1s
  soft_reset         : out std_logic;                                     -- Active high external reset on ctrl_regs(2) wr. operation (Device_ID & Vendor_ID), witdh=250ns
  status_led         : out std_logic_vector(1 downto 0);                  -- Status led for software test: 00=off, 01=red, 10=green
  ep_probe           : out std_logic_vector(3 downto 0);                  -- Output signals for debbuging purposes
	ap_probe           : in std_logic_vector(3 downto 0);                   -- Input test signals from the application

--
-- Master #0 
--

  eb_m0_clk_o        : out std_logic;                                     -- system clock
  eb_m0_rst_o        : out std_logic;                                     -- synchronous system reset
  eb_m0_brq_o        : out std_logic;                                     -- bus request
  eb_m0_bg_i         : in std_logic;                                      -- bus grant
  eb_bmx_i           : in std_logic;                                      -- busy some master (but FT)

  eb_m0_o            : out ebm32_typ;                                     -- master out
  eb_m_i             : in ebs32_o_typ;                                    -- master in shared

--
-- Fast Transmitter
--

  eb_ft_bg_i         : in std_logic;                                      -- busy FT
  eb_ft_dxt_i        : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);     -- FT data in
  eb_ft_i            : in ebft_typ;                                       -- FT control
  eb_ft_dk_o         : out std_logic;                                     -- FT slave data acknowledge
  eb_ft_err_o        : out std_logic;                                     -- FT slave bus error

--
-- Application extension signals
--

  ext_ctrl_i         : in ext_typ;                                        -- Application extension control
	ext_data_ack_o     : out std_logic;                                     -- External data source acknowledge output
  payload_size_o     : out std_logic_vector(3 downto 0)                   -- Payload size encoding output

);

end ebm0_pcie_b;


architecture pcie_rtl of ebm0_pcie_b is

-----------------------------
-- Constants declaration area
-----------------------------

constant AXI_DWIDTH                    : NATURAL := EBFT_DWIDTH;
constant TRGT_DESC                     : NATURAL := get_trgt_desc(TYPE_DESC);
constant LINK_WIDTH_CONV               : BIT_VECTOR := get_link_width_conv(LINK_WIDTH);
constant TRGT_LINK_SPEED               : BIT_VECTOR := get_trgt_link_speed(LINK_SPEED);
constant PCIE_GT_DEVICE                : STRING := get_gt_device(TYPE_DESC);
constant PCIE_USE_MODE                 : STRING := get_use_mode(TYPE_DESC);
constant PCIE_EXT_CLK                  : STRING := "TRUE";
constant PL_FAST_TRAIN                 : STRING := "FALSE";
constant PIPELINE_STAGES               : INTEGER := get_pipeline_stages(LINK_WIDTH, LINK_SPEED);
constant TRN_DW                        : STRING := get_data_width(AXI_DWIDTH);
constant ALLOW_X8_GEN2                 : STRING := "FALSE";
constant USER_CLK_FREQ                 : INTEGER := get_user_clk_freq(EB_CLK, LINK_WIDTH, LINK_SPEED, AXI_DWIDTH);
constant USER_CLK2_DIV2                : STRING := get_user_clk2_div2(EB_CLK, LINK_WIDTH, AXI_DWIDTH);
constant USERCLK2_FREQ                 : INTEGER := get_user_clk2(USER_CLK2_DIV2, USER_CLK_FREQ);
constant DEV_CAP_MAX_PAYLOAD_SUPPORTED : INTEGER := get_max_pay_sup(LINK_WIDTH, LINK_SPEED);
constant VC0_RX_RAM_LIMIT              : BIT_VECTOR := get_ram_lim(EB_CLK);
constant PIPE_QPLL_LOCK_SIZE           : INTEGER := get_qpll_lock_size(LINK_WIDTH);
constant VC0_T                         : vc0_t_typ := get_vc0_t(EB_CLK);

---------------------------
-- Signals declaration area
---------------------------

-- Common
signal trn_clk                        : std_logic;
signal trn_rst                        : std_logic;
signal trn_reset_n                    : std_logic;
signal trn_lnk_up_n                   : std_logic;

-- Flow Control
signal trn_fc_cpld                    : std_logic_vector(11 downto 0);
signal trn_fc_cplh                    : std_logic_vector(7 downto 0);
signal trn_fc_npd                     : std_logic_vector(11 downto 0);
signal trn_fc_nph                     : std_logic_vector(7 downto 0);
signal trn_fc_pd                      : std_logic_vector(11 downto 0);
signal trn_fc_ph                      : std_logic_vector(7 downto 0);
signal trn_fc_sel                     : std_logic_vector(2 downto 0);

-- Interrupt signaling
signal cfg_interrupt                  : std_logic;
signal cfg_interrupt_rdy              : std_logic;
signal cfg_interrupt_assert           : std_logic;
signal cfg_interrupt_di               : std_logic_vector(7 downto 0);
signal cfg_interrupt_do               : std_logic_vector(7 downto 0);
signal cfg_interrupt_mmenable         : std_logic_vector(2 downto 0);
signal cfg_interrupt_msienable        : std_logic;
signal cfg_interrupt_msixenable       : std_logic;
signal cfg_interrupt_msixfm           : std_logic;

-- System configuration and status
signal cfg_bus_number                 : std_logic_vector(7 downto 0);
signal cfg_device_number              : std_logic_vector(4 downto 0);
signal cfg_function_number            : std_logic_vector(2 downto 0);
signal cfg_status                     : std_logic_vector(15 downto 0);
signal cfg_command                    : std_logic_vector(15 downto 0);
signal cfg_dstatus                    : std_logic_vector(15 downto 0);
signal cfg_dcommand                   : std_logic_vector(15 downto 0);
signal cfg_lstatus                    : std_logic_vector(15 downto 0);
signal cfg_lcommand                   : std_logic_vector(15 downto 0);
signal cfg_dcommand2                  : std_logic_vector(15 downto 0);
signal cfg_dsn                        : std_logic_vector(63 downto 0);
signal cfg_pmcsr_pme_en               : std_logic;
signal cfg_pmcsr_pme_status           : std_logic;
signal cfg_pmcsr_powerstate           : std_logic_vector(1 downto 0);
signal cfg_completer_id               : std_logic_vector(15 downto 0);
signal cfg_bus_mstr_enable            : std_logic;

-- Physical Layer Control and Status (PL) Interface
signal pl_initial_link_width          : std_logic_vector(2 downto 0);
signal pl_lane_reversal_mode          : std_logic_vector(1 downto 0);
signal pl_link_gen2_capable           : std_logic;
signal pl_link_partner_gen2_supported : std_logic;
signal pl_link_upcfg_capable          : std_logic;
signal pl_ltssm_state                 : std_logic_vector(5 downto 0);
signal pl_received_hot_rst            : std_logic;
signal pl_sel_link_rate               : std_logic;
signal pl_sel_link_width              : std_logic_vector(1 downto 0);
signal pl_directed_link_change        : std_logic_vector(1 downto 0);
signal pl_directed_link_width         : std_logic_vector(1 downto 0);
signal pl_directed_link_speed         : std_logic;
signal pl_directed_link_auton         : std_logic;
signal pl_upstream_prefer_deemph      : std_logic;

-- System (SYS) Interface
signal sys_clk                        : std_logic;
signal sys_reset                      : std_logic;
signal ep_sys_reset                   : std_logic;
signal user_clk_out                   : std_logic;
signal user_reset_out                 : std_logic;
signal user_lnk_up                    : std_logic;

-- Transaction (TRN) AXI4-Stream Interface signals
signal tx_buf_av                      : std_logic_vector(5 downto 0);
signal tx_cfg_req                     : std_logic;
signal tx_err_drop                    : std_logic;
signal tx_cfg_gnt                     : std_logic;

signal s_axis_tx_tready               : std_logic;
signal s_axis_tx_tdata                : std_logic_vector(AXI_DWIDTH - 1 downto 0);
signal s_axis_tx_tkeep                : std_logic_vector((AXI_DWIDTH / 8 - 1) downto 0);
signal s_axis_tx_tuser                : std_logic_vector(3 downto 0);
signal s_axis_tx_tlast                : std_logic;
signal s_axis_tx_tvalid               : std_logic;

signal m_axis_rx_tdata                : std_logic_vector(AXI_DWIDTH - 1 downto 0);
signal m_axis_rx_tkeep                : std_logic_vector((AXI_DWIDTH / 8 - 1) downto 0);
signal m_axis_rx_tlast                : std_logic;
signal m_axis_rx_tvalid               : std_logic;
signal m_axis_rx_tuser                : std_logic_vector(21 downto 0);
signal m_axis_rx_tready               : std_logic;
signal rx_np_ok                       : std_logic;
signal rx_np_req                      : std_logic;

signal fc_cpld                        : std_logic_vector(11 downto 0);
signal fc_cplh                        : std_logic_vector(7 downto 0);
signal fc_npd                         : std_logic_vector(11 downto 0);
signal fc_nph                         : std_logic_vector(7 downto 0);
signal fc_pd                          : std_logic_vector(11 downto 0);
signal fc_ph                          : std_logic_vector(7 downto 0);
signal fc_sel                         : std_logic_vector(2 downto 0);

signal cfg_do                         : std_logic_vector(31 downto 0);
signal cfg_di                         : std_logic_vector(31 downto 0);
signal cfg_dwaddr                     : std_logic_vector(9 downto 0);
signal cfg_rd_wr_done                 : std_logic;
signal cfg_byte_en                    : std_logic_vector(3 downto 0);
signal cfg_wr_en                      : std_logic;
signal cfg_rd_en                      : std_logic;
signal cfg_err_cor                    : std_logic;
signal cfg_err_ur                     : std_logic;
signal cfg_err_ecrc                   : std_logic;
signal cfg_err_cpl_timeout            : std_logic;
signal cfg_err_cpl_abort              : std_logic;
signal cfg_err_cpl_unexpect           : std_logic;
signal cfg_err_posted                 : std_logic;
signal cfg_err_locked                 : std_logic;
signal cfg_err_cpl_rdy                : std_logic;
signal cfg_err_tlp_cpl_header         : std_logic_vector(47 downto 0);
signal cfg_turnoff_ok                 : std_logic;
signal cfg_to_turnoff                 : std_logic;
signal cfg_trn_pending                : std_logic;
signal cfg_pm_wake                    : std_logic;
signal cfg_pcie_link_state            : std_logic_vector(2 downto 0);

-- New Transaction (TRN) AXI4-Stream signals linked to Series-7
---------------------------------------------------------------
signal cfg_mgmt_wr_readonly           : std_logic;
signal cfg_received_func_lvl_rst      : std_logic;
signal cfg_err_atomic_egress_blocked  : std_logic;
signal cfg_err_internal_cor           : std_logic;
signal cfg_err_malformed              : std_logic;
signal cfg_err_mc_blocked             : std_logic;
signal cfg_err_poisoned               : std_logic;
signal cfg_err_norecovery             : std_logic;
signal cfg_err_acs                    : std_logic;
signal cfg_err_internal_uncor         : std_logic;
signal cfg_pm_halt_aspm_l0s           : std_logic;
signal cfg_pm_halt_aspm_l1            : std_logic;
signal cfg_pm_force_state_en          : std_logic;
signal cfg_pm_force_state             : std_logic_vector(1 downto 0);

signal cfg_interrupt_stat             : std_logic;
signal cfg_pciecap_interrupt_msgnum   : std_logic_vector(4 downto 0);

signal pl_phy_lnk_up                  : std_logic;
signal pl_tx_pm_state                 : std_logic_vector(2 downto 0);
signal pl_rx_pm_state                 : std_logic_vector(1 downto 0);
signal pl_directed_change_done        : std_logic;

signal cfg_err_aer_headerlog          : std_logic_vector(127 downto 0);
signal cfg_aer_interrupt_msgnum       : std_logic_vector(4 downto 0);
signal cfg_err_aer_headerlog_set      : std_logic;
signal cfg_aer_ecrc_check_en          : std_logic;
signal cfg_aer_ecrc_gen_en            : std_logic;
---------------------------------------------------------------

-- Wires used with Series-7 Integrated Block for PCI Express for external clocking connectivity
-----------------------------------------------------------------------------------------------
signal pipe_pclk_in                   : std_logic;
signal pipe_rxusrclk_in               : std_logic;
signal pipe_rxoutclk_in               : std_logic_vector(LINK_WIDTH - 1 downto 0);
signal pipe_dclk_in                   : std_logic;
signal pipe_userclk1_in               : std_logic;
signal pipe_userclk2_in               : std_logic;
signal pipe_oobclk_in                 : std_logic;
signal pipe_mmcm_lock_in              : std_logic;

signal pipe_txoutclk_out              : std_logic;
signal pipe_rxoutclk_out              : std_logic_vector(LINK_WIDTH - 1 downto 0);
signal pipe_pclk_sel_out              : std_logic_vector(LINK_WIDTH - 1 downto 0);
signal pipe_gen3_out                  : std_logic;
-----------------------------------------------------------------------------------------------

-- E-Bone link related
signal int_brd_data                   : std32_x32;
signal ext_brd_data                   : std32_x32;
signal ext_brd_done                   : std_logic;
signal brd_strt                       : std_logic;
signal bwr_strt                       : std_logic;
signal bwr_be                         : std_logic_vector(7 downto 0);
signal bwr_data                       : std32_x32;
signal breq_len                       : std_logic_vector(9 downto 0);
signal req_compl                      : std_logic;
signal compl_done                     : std_logic;
signal npck_prsnt                     : std_logic;

signal Type_Offset_desc               : std_logic_vector(31 downto 0);
signal E_Bone_trn                     : std_logic;
signal tx_ft_ready                    : std_logic;
signal tx_ft_ready_mbt                : std_logic;
signal tx_ft_delay                    : std_logic;
signal tx_tvalid_flag                 : std_logic;
signal tlp_delayed                    : std_logic;
signal eb_ft_dk                       : std_logic;
signal eb_ft_data                     : std_logic_vector(EBFT_DWIDTH - 1 downto 0);
signal eb_ft_addr                     : std_logic_vector(EBFT_DWIDTH - 1 downto 0);
signal eb_data_size                   : std_logic_vector(7 downto 0);
signal eb_last_size                   : std_logic_vector(7 downto 0);
signal eb_byte_size                   : std_logic_vector(15 downto 0);
signal my_payload_size                : std_logic_vector(2 downto 0);
signal TLP_end_Rqst                   : std_logic;
signal INtr_B_calls                   : std_logic;
signal INtr_gate                      : std_logic;
signal bcall_run                      : std_logic;
signal bcall_reg                      : std_logic_vector(31 downto 0);
signal RD_on                          : std_logic;
signal WR_on                          : std_logic;
signal FT_on                          : std_logic;
signal FT_clr                         : std_logic;
signal wr_busy_eb                     : std_logic;
signal ebone_reset                    : std_logic;
signal one_us_heartbeat               : std_logic;

signal err_drop_count                 : std_logic_vector(14 downto 0);
signal err_drop_ovfl                  : std_logic;
signal tdst_rdy_count                 : std_logic_vector(14 downto 0);
signal tdst_rdy_ovfl                  : std_logic;
signal req_len_sup                    : std_logic;
signal req_len_sup_count              : std_logic_vector(15 downto 0);
signal req_rx_valid                   : std_logic;
signal req_rx_valid_count             : std_logic_vector(31 downto 0);
signal req_tx_valid                   : std_logic;
signal req_tx_valid_count             : std_logic_vector(31 downto 0);

signal mm_probe                       : std_logic_vector(3 downto 0);
signal rx_probe                       : std_logic_vector(3 downto 0);
signal tx_probe                       : std_logic_vector(3 downto 0);
signal it_probe                       : std_logic_vector(3 downto 0);
signal eb_probe                       : std_logic_vector(3 downto 0);
signal prob_mux                       : std_logic_vector(19 downto 0);

signal user_cnst_pft                  : std_logic;
signal user_cnst_pec                  : std_logic;
signal user_cnst_trn                  : std_logic;
signal user_cnst_ucd                  : std_logic;
signal user_constant                  : std_logic_vector(15 downto 0);

-- Debug related
signal ext_data_ack                   : std_logic;

begin

---------------------------------------------------------------------------------------------------------------------------------------
assert_i : ep_assert                                                      -- Irrelevant generic parameters cases control implementation
---------------------------------------------------------------------------------------------------------------------------------------
generic map (
  EB_CLK              => EB_CLK,
	EBFT_DWIDTH         => EBFT_DWIDTH,
  TYPE_DESC           => TYPE_DESC,
	TRGT_DESC           => TRGT_DESC,
	LINK_SPEED          => LINK_SPEED,
	LINK_WIDTH          => LINK_WIDTH
);

---------------------------------------------------------------------------------------------------------------------------------
link_i : ep_eb_link                                                       -- PCI Express Endpoint <--> E_Bone link implementation
---------------------------------------------------------------------------------------------------------------------------------
generic map (
  EB_CLK             => EB_CLK,
  EBFT_DWIDTH        => EBFT_DWIDTH,
	BC_PERIOD          => BC_PERIOD
)

port map (
  trn_clk            => trn_clk,
  trn_rst            => trn_rst,
	s_axis_tx_tready   => s_axis_tx_tready,

--
-- LocalLink Rx interfacing
--

	ext_brd_data_o     => ext_brd_data,
	ext_brd_done_o     => ext_brd_done,
	brd_strt_i         => brd_strt,
  bwr_strt_i         => bwr_strt,
  bwr_be_i           => bwr_be,
  bwr_data_i         => bwr_data,
  breq_len_i         => breq_len,

  npck_prsnt_i       => npck_prsnt,
	req_compl_i        => req_compl,
	compl_done_i       => compl_done,

  Type_Offset_desc_i => Type_Offset_desc,
	E_Bone_trn_i       => E_Bone_trn,
  eb_ft_data_o       => eb_ft_data,
  eb_ft_addr_o       => eb_ft_addr,
	eb_data_size_o     => eb_data_size,
	eb_last_size_o     => eb_last_size,
	eb_byte_size_o     => eb_byte_size,
  TLP_end_Rqst_o     => TLP_end_Rqst,
	tx_ft_ready_o      => tx_ft_ready,
	tx_ft_ready_i      => tx_ft_ready_mbt,
	tx_ft_delay_i      => tx_ft_delay,
	tx_tvalid_flag_i   => tx_tvalid_flag,
  tlp_delayed_i      => tlp_delayed,
  m_axis_rx_tvalid_i => m_axis_rx_tvalid,
  m_axis_rx_tready_i => m_axis_rx_tready,
	INtr_B_calls_i     => INtr_B_calls,
  INtr_gate_i        => INtr_gate,
	bcall_run_o        => bcall_run,
	bcall_reg_o        => bcall_reg,
  RD_on_i            => RD_on,
  WR_on_i            => WR_on,
  FT_on_o            => FT_on,
  FT_clr_i           => FT_clr,
  wr_busy_eb_o       => wr_busy_eb,
	one_us_heartbeat_o => one_us_heartbeat,
  ebone_reset_i      => ebone_reset,
  cfg_dcommand_i     => cfg_dcommand,

--
-- Master #0 dedicated
--

  eb_m0_clk_o        => eb_m0_clk_o,
  eb_m0_rst_o        => eb_m0_rst_o,
  eb_m0_brq_o        => eb_m0_brq_o,
  eb_m0_bg_i         => eb_m0_bg_i,
  eb_m0_as_o         => eb_m0_o.eb_as,
  eb_m0_eof_o        => eb_m0_o.eb_eof,
  eb_m0_aef_o        => eb_m0_o.eb_aef,
  eb_m0_dat_o        => eb_m0_o.eb_dat,
  eb_dk_i            => eb_m_i.eb_dk,
  eb_err_i           => eb_m_i.eb_err,
  eb_dat_i           => eb_m_i.eb_dat,

--
-- Fast Transmitter
--

  eb_ft_dxt_i        => eb_ft_dxt_i,
	eb_ft_blen_i       => eb_ft_i.eb_blen,
	eb_ft_dsz_i        => eb_ft_i.eb_dsz,
	eb_ft_ss_i         => eb_ft_i.eb_ss,
  eb_bmx_i           => eb_bmx_i,
  eb_bft_i           => eb_ft_bg_i,
  eb_as_i            => eb_ft_i.eb_as,
  eb_eof_i           => eb_ft_i.eb_eof,
  eb_dk_o            => eb_ft_dk_o,
  eb_err_o           => eb_ft_err_o,

  eb_probe_o         => eb_probe
);

---------------------------------------------------------------------------------------------------------------------------------------
app : ep_app                                                              -- PCI Express endpoint top module implementation application
---------------------------------------------------------------------------------------------------------------------------------------
generic map (
  EBFT_DWIDTH                    => EBFT_DWIDTH,
  BADR0_MEM                      => BADR0_MEM,
	WATCH_DOG                      => WATCH_DOG,
	AXI_DWIDTH                     => AXI_DWIDTH,
	BC_PERIOD                      => BC_PERIOD,
	TRGT_DESC                      => TRGT_DESC,
	TYPE_DESC                      => TYPE_DESC,
	HARD_VERS                      => HARD_VERS,
	DEVICE_ID                      => DEVICE_ID
)

port map (
  trn_clk                        => trn_clk,
  trn_rst                        => trn_rst,
  trn_lnk_up_n                   => trn_lnk_up_n,

-- Tx AXI4_Stream interface
  tx_buf_av                      => tx_buf_av,
  tx_cfg_req                     => tx_cfg_req,
  tx_err_drop                    => tx_err_drop,
  s_axis_tx_tready               => s_axis_tx_tready,
  s_axis_tx_tdata                => s_axis_tx_tdata,
  s_axis_tx_tkeep                => s_axis_tx_tkeep,
  s_axis_tx_tlast                => s_axis_tx_tlast,
  s_axis_tx_tvalid               => s_axis_tx_tvalid,
  s_axis_tx_tuser                => s_axis_tx_tuser,
  tx_cfg_gnt                     => tx_cfg_gnt,

-- Rx AXI4_Stream interface
  m_axis_rx_tdata                => m_axis_rx_tdata,
  m_axis_rx_tkeep                => m_axis_rx_tkeep,
  m_axis_rx_tlast                => m_axis_rx_tlast,
  m_axis_rx_tvalid               => m_axis_rx_tvalid,
  m_axis_rx_tready               => m_axis_rx_tready,
  m_axis_rx_tuser                => m_axis_rx_tuser,
  rx_np_ok                       => rx_np_ok,
  rx_np_req                      => rx_np_req,

-- Flow Control
  fc_cpld                        => fc_cpld,
  fc_cplh                        => fc_cplh,
  fc_npd                         => fc_npd,
  fc_nph                         => fc_nph,
  fc_pd                          => fc_pd,
  fc_ph                          => fc_ph,
  fc_sel                         => fc_sel,

-- Configuration (CFG) Interface
  cfg_do                         => cfg_do,
  cfg_rd_wr_done                 => cfg_rd_wr_done,
  cfg_di                         => cfg_di,
  cfg_byte_en                    => cfg_byte_en,
  cfg_dwaddr                     => cfg_dwaddr,
  cfg_wr_en                      => cfg_wr_en,
  cfg_rd_en                      => cfg_rd_en,

  cfg_err_cor                    => cfg_err_cor,
  cfg_err_ur                     => cfg_err_ur,
  cfg_err_ecrc                   => cfg_err_ecrc,
  cfg_err_cpl_timeout            => cfg_err_cpl_timeout,
  cfg_err_cpl_abort              => cfg_err_cpl_abort,
	cfg_err_cpl_unexpect           => cfg_err_cpl_unexpect,
  cfg_err_posted                 => cfg_err_posted,
  cfg_err_locked                 => cfg_err_locked,
	cfg_pm_wake                    => cfg_pm_wake,
	cfg_trn_pending                => cfg_trn_pending,
  cfg_err_tlp_cpl_header         => cfg_err_tlp_cpl_header,
  cfg_err_cpl_rdy                => cfg_err_cpl_rdy,

  cfg_turnoff_ok                 => cfg_turnoff_ok,
  cfg_to_turnoff                 => cfg_to_turnoff,
  cfg_bus_number                 => cfg_bus_number,
  cfg_device_number              => cfg_device_number,
  cfg_function_number            => cfg_function_number,
  cfg_status                     => cfg_status,
  cfg_command                    => cfg_command,
  cfg_dstatus                    => cfg_dstatus,
  cfg_dcommand                   => cfg_dcommand,
	cfg_dcommand2                  => cfg_dcommand2,
  cfg_lstatus                    => cfg_lstatus,
  cfg_lcommand                   => cfg_lcommand,
  cfg_pcie_link_state            => cfg_pcie_link_state,
  cfg_dsn                        => cfg_dsn,
  cfg_pmcsr_pme_en               => cfg_pmcsr_pme_en,
  cfg_pmcsr_pme_status           => cfg_pmcsr_pme_status,
  cfg_pmcsr_powerstate           => cfg_pmcsr_powerstate,
  cfg_received_func_lvl_rst      => cfg_received_func_lvl_rst,

-- Interrupt (CFG) Interface
  cfg_interrupt                  => cfg_interrupt,
  cfg_interrupt_rdy              => cfg_interrupt_rdy,
  cfg_interrupt_assert           => cfg_interrupt_assert,
  cfg_interrupt_di               => cfg_interrupt_di,
  cfg_interrupt_do               => cfg_interrupt_do,
  cfg_interrupt_mmenable         => cfg_interrupt_mmenable,
  cfg_interrupt_msienable        => cfg_interrupt_msienable,
  cfg_interrupt_msixenable       => cfg_interrupt_msixenable,
  cfg_interrupt_msixfm           => cfg_interrupt_msixfm,

  cfg_completer_id               => cfg_completer_id,
  cfg_bus_mstr_enable            => cfg_bus_mstr_enable,

-- Physical Layer Control and Status (PL) Interface
  pl_initial_link_width          => pl_initial_link_width,
  pl_lane_reversal_mode          => pl_lane_reversal_mode,
  pl_link_gen2_capable           => pl_link_gen2_capable,
  pl_link_partner_gen2_supported => pl_link_partner_gen2_supported,
  pl_link_upcfg_capable          => pl_link_upcfg_capable,
  pl_ltssm_state                 => pl_ltssm_state,
  pl_received_hot_rst            => pl_received_hot_rst,
  pl_sel_link_rate               => pl_sel_link_rate,
  pl_sel_link_width              => pl_sel_link_width,
	pl_phy_lnk_up                  => pl_phy_lnk_up,
  pl_tx_pm_state                 => pl_tx_pm_state,
  pl_rx_pm_state                 => pl_rx_pm_state,
  pl_directed_change_done        => pl_directed_change_done,
  pl_directed_link_change        => pl_directed_link_change,
  pl_directed_link_width         => pl_directed_link_width,
	pl_directed_link_speed         => pl_directed_link_speed,
	pl_directed_link_auton         => pl_directed_link_auton,
  pl_upstream_prefer_deemph      => pl_upstream_prefer_deemph,

-- E-Bone link related
	ext_brd_data_i                 => ext_brd_data,
  ext_brd_done_i                 => ext_brd_done,
	brd_strt_o                     => brd_strt,
  bwr_strt_o                     => bwr_strt,
  bwr_be_o                       => bwr_be,
  bwr_data_o                     => bwr_data,
  breq_len_o                     => breq_len,

  npck_prsnt_o                   => npck_prsnt,
	req_compl_o                    => req_compl,
	compl_done_o                   => compl_done,

  Type_Offset_desc_o             => Type_Offset_desc,
  E_Bone_trn_o                   => E_Bone_trn,
  tx_ft_ready_i                  => tx_ft_ready,
	tx_ft_ready_o                  => tx_ft_ready_mbt,
	tx_tvalid_flag_o               => tx_tvalid_flag,
  eb_ft_data_i                   => eb_ft_data,
  eb_ft_addr_i                   => eb_ft_addr,
	eb_data_size_i                 => eb_data_size,
	eb_last_size_i                 => eb_last_size,
	eb_byte_size_i                 => eb_byte_size,
	eb_bft_i                       => eb_ft_bg_i,
  eb_eof_i                       => eb_ft_i.eb_eof,
	TLP_end_Rqst_i                 => TLP_end_Rqst,
	INtr_B_calls_o                 => INtr_B_calls,
  INtr_gate_o                    => INtr_gate,
	bcall_run_i                    => bcall_run,
	bcall_reg_i                    => bcall_reg,
  RD_on_o                        => RD_on,
  WR_on_o                        => WR_on,
  FT_on_i                        => FT_on,
  FT_clr_o                       => FT_clr,
	wr_busy_eb_i                   => wr_busy_eb,
	one_us_heartbeat_i             => one_us_heartbeat,

  soft_wd_o                      => soft_wd,
  soft_reset_o                   => soft_reset,
  status_led_o                   => status_led,
  ebone_reset_o                  => ebone_reset,

  err_drop_count                 => err_drop_count,
	err_drop_ovfl                  => err_drop_ovfl,
	tdst_rdy_count                 => tdst_rdy_count,
	tdst_rdy_ovfl                  => tdst_rdy_ovfl,
	req_len_sup_o                  => req_len_sup,
	req_len_sup_count              => req_len_sup_count,
	req_rx_valid_o                 => req_rx_valid,
	req_rx_valid_count             => req_rx_valid_count,
	req_tx_valid_o                 => req_tx_valid,
	req_tx_valid_count             => req_tx_valid_count,

  ext_ctrl_src_i                 => ext_ctrl_i.ext_ctrl_src,
  ext_intr_src_i                 => ext_ctrl_i.ext_intr_src,
	ext_addr_src_i                 => ext_ctrl_i.ext_addr_src,
	ext_data_flg_i                 => ext_ctrl_i.ext_data_flg,
	ext_data_src_i                 => ext_ctrl_i.ext_data_src(EBFT_DWIDTH - 1 downto 0),
	ext_data_ack_o                 => ext_data_ack,

  mm_probe_o                     => mm_probe,
  rx_probe_o                     => rx_probe,
  tx_probe_o                     => tx_probe,
  it_probe_o                     => it_probe,
  prob_mux_o                     => prob_mux,

  my_payload_size_o              => my_payload_size,
  user_cnst_i                    => user_constant,
  tx_ft_delay_o                  => tx_ft_delay,
  tlp_delayed_o                  => tlp_delayed
);

--------------------------------------------------------------------------------------------------------------------------------------------------------
s7_type : if (TRGT_DESC = 16#71#) or (TRGT_DESC = 16#72#) or (TRGT_DESC = 16#73#) generate         -- Series-7 Integrated Block Endpoint for PCI Express
--------------------------------------------------------------------------------------------------------------------------------------------------------

  refclk_ibuf : IBUFDS_GTE2
  -------------------------
  port map (
    O     => sys_clk,
    ODIV2 => open,
    I     => sys_clk_p,
    IB    => sys_clk_n,
    CEB   => '0'
  );

  s7_ep : s7_pcie_ep_v3_1_core_top
  --------------------------------
  generic map (
    PL_FAST_TRAIN                               => PL_FAST_TRAIN,
    CFG_DEV_ID                                  => TO_STDLOGICVECTOR(DEVICE_ID),
    C_DATA_WIDTH                                => AXI_DWIDTH,
    LINK_CAP_MAX_LINK_SPEED                     => LINK_SPEED,
    LINK_CAP_MAX_LINK_WIDTH                     => LINK_WIDTH,
    LINK_CTRL2_TARGET_LINK_SPEED                => TRGT_LINK_SPEED,
    LTSSM_MAX_LINK_WIDTH                        => LINK_WIDTH_CONV,
    TRN_DW                                      => TRN_DW,
    USER_CLK_FREQ                               => USER_CLK_FREQ,
    USER_CLK2_DIV2                              => USER_CLK2_DIV2,
    ALLOW_X8_GEN2                               => ALLOW_X8_GEN2,
    PCIE_EXT_CLK                                => PCIE_EXT_CLK,
    PCIE_USE_MODE                               => PCIE_USE_MODE,
    PCIE_GT_DEVICE                              => PCIE_GT_DEVICE,
    PIPE_PIPELINE_STAGES                        => PIPELINE_STAGES,
    DEV_CAP_MAX_PAYLOAD_SUPPORTED               => DEV_CAP_MAX_PAYLOAD_SUPPORTED,
    VC0_RX_RAM_LIMIT                            => VC0_RX_RAM_LIMIT,
    VC0_TOTAL_CREDITS_CD                        => VC0_T.VC0_TOTAL_CREDITS_CD,
    VC0_TOTAL_CREDITS_CH                        => VC0_T.VC0_TOTAL_CREDITS_CH,
    VC0_TOTAL_CREDITS_NPH                       => VC0_T.VC0_TOTAL_CREDITS_NPH,
    VC0_TOTAL_CREDITS_NPD                       => VC0_T.VC0_TOTAL_CREDITS_NPD,
    VC0_TOTAL_CREDITS_PD                        => VC0_T.VC0_TOTAL_CREDITS_PD,
    VC0_TOTAL_CREDITS_PH                        => VC0_T.VC0_TOTAL_CREDITS_PH,
    VC0_TX_LASTPACKET                           => VC0_T.VC0_TX_LASTPACKET,
    PIPE_QPLL_LOCK_SIZE                         => PIPE_QPLL_LOCK_SIZE
  )

  port map (
	-------------------------------------------------------------------------------------------------------------------
  -- 1. PCI Express (pci_exp) Interface                                                                            --
  -------------------------------------------------------------------------------------------------------------------
    pci_exp_txp                                 => pci_exp_txp,
    pci_exp_txn                                 => pci_exp_txn,
    pci_exp_rxp                                 => pci_exp_rxp,
    pci_exp_rxn                                 => pci_exp_rxn,

  -------------------------------------------------------------------------------------------------------------------
  -- 2. Clocking Interface                                                                                         --
  -------------------------------------------------------------------------------------------------------------------
    int_pclk_out_slave                          => open,
    int_pipe_rxusrclk_out                       => open,
    int_rxoutclk_out                            => open,
    int_dclk_out                                => open,
    int_userclk1_out                            => open,
    int_userclk2_out                            => open,
    int_oobclk_out                              => open,
    int_mmcm_lock_out                           => open,
    int_qplllock_out                            => open,
    int_qplloutclk_out                          => open,
    int_qplloutrefclk_out                       => open,
    int_pclk_sel_slave                          => (others => '0'),

    pipe_pclk_in                                => pipe_pclk_in,
    pipe_rxusrclk_in                            => pipe_rxusrclk_in,
    pipe_rxoutclk_in                            => pipe_rxoutclk_in,
    pipe_dclk_in                                => pipe_dclk_in,
    pipe_userclk1_in                            => pipe_userclk1_in,
    pipe_userclk2_in                            => pipe_userclk2_in,
    pipe_oobclk_in                              => pipe_oobclk_in,
    pipe_mmcm_lock_in                           => pipe_mmcm_lock_in,

    pipe_txoutclk_out                           => pipe_txoutclk_out,
    pipe_rxoutclk_out                           => pipe_rxoutclk_out,
    pipe_pclk_sel_out                           => pipe_pclk_sel_out,
    pipe_gen3_out                               => pipe_gen3_out,

  -------------------------------------------------------------------------------------------------------------------
  -- 3. AXI-S Interface                                                                                            --
  -------------------------------------------------------------------------------------------------------------------
  -- Common
    user_clk_out                                => user_clk_out,
    user_reset_out                              => user_reset_out,
    user_lnk_up                                 => user_lnk_up,
    user_app_rdy                                => open,

  -- TX
    tx_buf_av                                   => tx_buf_av,
    tx_cfg_req                                  => tx_cfg_req,
    tx_err_drop                                 => tx_err_drop,
    s_axis_tx_tready                            => s_axis_tx_tready,
    s_axis_tx_tdata                             => s_axis_tx_tdata,
    s_axis_tx_tkeep                             => s_axis_tx_tkeep,
    s_axis_tx_tlast                             => s_axis_tx_tlast,
    s_axis_tx_tvalid                            => s_axis_tx_tvalid,
    s_axis_tx_tuser                             => s_axis_tx_tuser,
    tx_cfg_gnt                                  => tx_cfg_gnt,

  -- RX
    m_axis_rx_tdata                             => m_axis_rx_tdata,
    m_axis_rx_tkeep                             => m_axis_rx_tkeep,
    m_axis_rx_tlast                             => m_axis_rx_tlast,
    m_axis_rx_tvalid                            => m_axis_rx_tvalid,
    m_axis_rx_tready                            => m_axis_rx_tready,
    m_axis_rx_tuser                             => m_axis_rx_tuser,
    rx_np_ok                                    => rx_np_ok,
    rx_np_req                                   => rx_np_req,

  -- Flow Control
    fc_cpld                                     => fc_cpld,
    fc_cplh                                     => fc_cplh,
    fc_npd                                      => fc_npd,
    fc_nph                                      => fc_nph,
    fc_pd                                       => fc_pd,
    fc_ph                                       => fc_ph,
    fc_sel                                      => fc_sel,

  -------------------------------------------------------------------------------------------------------------------
  -- 4. Configuration (CFG) Interface                                                                              --
  -------------------------------------------------------------------------------------------------------------------
  ---------------------------------------------------------------------
  -- EP and RP                                                       --
  ---------------------------------------------------------------------
  -- Management Interface
    cfg_mgmt_do                                 => cfg_do,
    cfg_mgmt_rd_wr_done                         => cfg_rd_wr_done,
    cfg_mgmt_di                                 => cfg_di,
    cfg_mgmt_byte_en                            => cfg_byte_en,
    cfg_mgmt_dwaddr                             => cfg_dwaddr,
    cfg_mgmt_wr_en                              => cfg_wr_en,
    cfg_mgmt_rd_en                              => cfg_rd_en,
    cfg_mgmt_wr_readonly                        => cfg_mgmt_wr_readonly,

    cfg_status                                  => cfg_status,
    cfg_command                                 => cfg_command,
    cfg_dstatus                                 => cfg_dstatus,
    cfg_dcommand                                => cfg_dcommand,
    cfg_lstatus                                 => cfg_lstatus,
    cfg_lcommand                                => cfg_lcommand,
    cfg_dcommand2                               => cfg_dcommand2,
    cfg_pcie_link_state                         => cfg_pcie_link_state,

    cfg_pmcsr_pme_en                            => cfg_pmcsr_pme_en,
    cfg_pmcsr_powerstate                        => cfg_pmcsr_powerstate,
    cfg_pmcsr_pme_status                        => cfg_pmcsr_pme_status,
    cfg_received_func_lvl_rst                   => cfg_received_func_lvl_rst,

  -- Error Reporting Interface
    cfg_err_ur                                  => cfg_err_ur,
		cfg_err_cor                                 => cfg_err_cor,
		cfg_err_ecrc                                => cfg_err_ecrc,
    cfg_err_cpl_timeout                         => cfg_err_cpl_timeout,
    cfg_err_cpl_unexpect                        => cfg_err_cpl_unexpect,
    cfg_err_cpl_abort                           => cfg_err_cpl_abort,
    cfg_err_posted                              => cfg_err_posted,
		cfg_err_locked                              => cfg_err_locked,
		cfg_err_tlp_cpl_header                      => cfg_err_tlp_cpl_header,
    cfg_err_cpl_rdy                             => cfg_err_cpl_rdy,
		cfg_trn_pending                             => cfg_trn_pending,
		cfg_dsn                                     => cfg_dsn,
    cfg_err_atomic_egress_blocked               => cfg_err_atomic_egress_blocked,
    cfg_err_internal_cor                        => cfg_err_internal_cor,
    cfg_err_malformed                           => cfg_err_malformed,
    cfg_err_mc_blocked                          => cfg_err_mc_blocked,
    cfg_err_poisoned                            => cfg_err_poisoned,
    cfg_err_norecovery                          => cfg_err_norecovery,
    cfg_err_acs                                 => cfg_err_acs,
    cfg_err_internal_uncor                      => cfg_err_internal_uncor,
    cfg_pm_halt_aspm_l0s                        => cfg_pm_halt_aspm_l0s,
    cfg_pm_halt_aspm_l1                         => cfg_pm_halt_aspm_l1,
    cfg_pm_force_state_en                       => cfg_pm_force_state_en,
    cfg_pm_force_state                          => cfg_pm_force_state,

  ---------------------------------------------------------------------
  -- EP Only                                                         --
  ---------------------------------------------------------------------
    cfg_interrupt                               => cfg_interrupt,
    cfg_interrupt_rdy                           => cfg_interrupt_rdy,
    cfg_interrupt_assert                        => cfg_interrupt_assert,
    cfg_interrupt_di                            => cfg_interrupt_di,
    cfg_interrupt_do                            => cfg_interrupt_do,
    cfg_interrupt_mmenable                      => cfg_interrupt_mmenable,
    cfg_interrupt_msienable                     => cfg_interrupt_msienable,
    cfg_interrupt_msixenable                    => cfg_interrupt_msixenable,
    cfg_interrupt_msixfm                        => cfg_interrupt_msixfm,
    cfg_interrupt_stat                          => cfg_interrupt_stat,
    cfg_pciecap_interrupt_msgnum                => cfg_pciecap_interrupt_msgnum,
    cfg_to_turnoff                              => cfg_to_turnoff,
    cfg_turnoff_ok                              => cfg_turnoff_ok,
		cfg_pm_wake                                 => cfg_pm_wake,
    cfg_bus_number                              => cfg_bus_number,
    cfg_device_number                           => cfg_device_number,
    cfg_function_number                         => cfg_function_number,

  ---------------------------------------------------------------------
  -- RP Only                                                         --
  ---------------------------------------------------------------------
    cfg_pm_send_pme_to                          => '0',
    cfg_ds_bus_number                           => (others => '0'),
    cfg_ds_device_number                        => "00000",
    cfg_ds_function_number                      => "000",
    cfg_mgmt_wr_rw1c_as_rw                      => '0',
    cfg_msg_received                            => open,
    cfg_msg_data                                => open,

    cfg_bridge_serr_en                          => open,
    cfg_slot_control_electromech_il_ctl_pulse   => open,
    cfg_root_control_syserr_corr_err_en         => open,
    cfg_root_control_syserr_non_fatal_err_en    => open,
    cfg_root_control_syserr_fatal_err_en        => open,
    cfg_root_control_pme_int_en                 => open,
    cfg_aer_rooterr_corr_err_reporting_en       => open,
    cfg_aer_rooterr_non_fatal_err_reporting_en  => open,
    cfg_aer_rooterr_fatal_err_reporting_en      => open,
    cfg_aer_rooterr_corr_err_received           => open,
    cfg_aer_rooterr_non_fatal_err_received      => open,
    cfg_aer_rooterr_fatal_err_received          => open,

    cfg_msg_received_err_cor                    => open,
    cfg_msg_received_err_non_fatal              => open,
    cfg_msg_received_err_fatal                  => open,
    cfg_msg_received_pm_as_nak                  => open,
    cfg_msg_received_pm_pme                     => open,
    cfg_msg_received_pme_to_ack                 => open,
    cfg_msg_received_assert_int_a               => open,
    cfg_msg_received_assert_int_b               => open,
    cfg_msg_received_assert_int_c               => open,
    cfg_msg_received_assert_int_d               => open,
    cfg_msg_received_deassert_int_a             => open,
    cfg_msg_received_deassert_int_b             => open,
    cfg_msg_received_deassert_int_c             => open,
    cfg_msg_received_deassert_int_d             => open,
    cfg_msg_received_setslotpowerlimit          => open,

  -------------------------------------------------------------------------------------------------------------------
  -- 5. Physical Layer Control and Status (PL) Interface                                                           --
  -------------------------------------------------------------------------------------------------------------------
    pl_directed_link_change                     => pl_directed_link_change,
    pl_directed_link_width                      => pl_directed_link_width,
    pl_directed_link_speed                      => pl_directed_link_speed,
    pl_directed_link_auton                      => pl_directed_link_auton,
    pl_upstream_prefer_deemph                   => pl_upstream_prefer_deemph,

    pl_sel_lnk_rate                             => pl_sel_link_rate,
    pl_sel_lnk_width                            => pl_sel_link_width,
    pl_ltssm_state                              => pl_ltssm_state,
    pl_lane_reversal_mode                       => pl_lane_reversal_mode,

    pl_phy_lnk_up                               => pl_phy_lnk_up,
    pl_tx_pm_state                              => pl_tx_pm_state,
    pl_rx_pm_state                              => pl_rx_pm_state,

    pl_link_upcfg_cap                           => pl_link_upcfg_capable,
    pl_link_gen2_cap                            => pl_link_gen2_capable,
    pl_link_partner_gen2_supported              => pl_link_partner_gen2_supported,
    pl_initial_link_width                       => pl_initial_link_width,

    pl_directed_change_done                     => pl_directed_change_done,

  ---------------------------------------------------------------------
  -- EP Only                                                         --
  ---------------------------------------------------------------------
    pl_received_hot_rst                         => pl_received_hot_rst,
  ---------------------------------------------------------------------
  -- RP Only                                                         --
  ---------------------------------------------------------------------
    pl_transmit_hot_rst                         => '0',
    pl_downstream_deemph_source                 => '0',
  -------------------------------------------------------------------------------------------------------------------
  -- 6. AER interface                                                                                              --
  -------------------------------------------------------------------------------------------------------------------
    cfg_err_aer_headerlog                       => cfg_err_aer_headerlog,
    cfg_aer_interrupt_msgnum                    => cfg_aer_interrupt_msgnum,
    cfg_err_aer_headerlog_set                   => cfg_err_aer_headerlog_set,
    cfg_aer_ecrc_check_en                       => cfg_aer_ecrc_check_en,
    cfg_aer_ecrc_gen_en                         => cfg_aer_ecrc_gen_en,
  -------------------------------------------------------------------------------------------------------------------
  -- 7. VC interface                                                                                               --
  -------------------------------------------------------------------------------------------------------------------
    cfg_vc_tcvc_map                             => open,

  -------------------------------------------------------------------------------------------------------------------
  -- PCIe Fast Config: STARTUP primitive Interface - only used in Tandem configurations !!!                        --
  -------------------------------------------------------------------------------------------------------------------
    startup_eos_in                              => '0',
    startup_cfgclk                              => open,
    startup_cfgmclk                             => open,
    startup_eos                                 => open,
    startup_preq                                => open,

    startup_clk                                 => '0',
    startup_gsr                                 => '0',
    startup_gts                                 => '0',
    startup_keyclearb                           => '0',
    startup_pack                                => '0',
    startup_usrcclko                            => '0',
    startup_usrcclkts                           => '0',
    startup_usrdoneo                            => '0',
    startup_usrdonets                           => '0',

  -------------------------------------------------------------------------------------------------------------------
  -- PCIe Fast Config => ICAP primitive Interface - only used in Tandem configurations !!!                           --
  -------------------------------------------------------------------------------------------------------------------
    icap_clk                                    => '0',
    icap_csib                                   => '0',
    icap_rdwrb                                  => '0',
    icap_i                                      => (others => '0'),
    icap_o                                      => open,

    qpll_drp_crscode                            => (others => '0'),
    qpll_drp_fsm                                => (others => '0'),
    qpll_drp_done                               => (others => '0'),
    qpll_drp_reset                              => (others => '0'),
    qpll_qplllock                               => (others => '0'),
    qpll_qplloutclk                             => (others => '0'),
    qpll_qplloutrefclk                          => (others => '0'),
    qpll_qplld                                  => open,
    qpll_qpllreset                              => open,
    qpll_drp_clk                                => open,
    qpll_drp_rst_n                              => open,
    qpll_drp_ovrd                               => open,
    qpll_drp_gen3                               => open,
    qpll_drp_start                              => open,
    pipe_txprbssel                              => (others => '0'),
    pipe_rxprbssel                              => (others => '0'),
    pipe_txprbsforceerr                         => '0',
    pipe_rxprbscntreset                         => '0',
    pipe_loopback                               => (others => '0'),
    pipe_rxprbserr                              => open,
    pipe_txinhibit                              => (others => '0'),
    pipe_rst_fsm                                => open,
    pipe_qrst_fsm                               => open,
    pipe_rate_fsm                               => open,
    pipe_sync_fsm_tx                            => open,
    pipe_sync_fsm_rx                            => open,
    pipe_drp_fsm                                => open,
    pipe_rst_idle                               => open,
    pipe_qrst_idle                              => open,
    pipe_rate_idle                              => open,
    pipe_eyescandataerror                       => open,
    pipe_rxstatus                               => open,
    pipe_dmonitorout                            => open,

    pipe_cpll_lock                              => open,
    pipe_qpll_lock                              => open,
    pipe_rxpmaresetdone                         => open,
    pipe_rxbufstatus                            => open,
    pipe_txphaligndone                          => open,
    pipe_txphinitdone                           => open,
    pipe_txdlysresetdone                        => open,
    pipe_rxphaligndone                          => open,
    pipe_rxdlysresetdone                        => open,
    pipe_rxsyncdone                             => open,
    pipe_rxdisperr                              => open,
    pipe_rxnotintable                           => open,
    pipe_rxcommadet                             => open,

    gt_ch_drp_rdy                               => open,
    pipe_debug_0                                => open,
    pipe_debug_1                                => open,
    pipe_debug_2                                => open,
    pipe_debug_3                                => open,
    pipe_debug_4                                => open,
    pipe_debug_5                                => open,
    pipe_debug_6                                => open,
    pipe_debug_7                                => open,
    pipe_debug_8                                => open,
    pipe_debug_9                                => open,
    pipe_debug                                  => open,

    ext_ch_gt_drpclk                            => open,
    ext_ch_gt_drpaddr                           => (others => '0'),
    ext_ch_gt_drpen                             => (others => '0'),
    ext_ch_gt_drpdi                             => (others => '0'),
    ext_ch_gt_drpwe                             => (others => '0'),
    ext_ch_gt_drpdo                             => open,
    ext_ch_gt_drprdy                            => open,

  -------------------------------------------------------------------------------------------------------------------
  -- PCIe DRP Interface                                                                                            --
  -------------------------------------------------------------------------------------------------------------------
    pcie_drp_clk                                => '0',
    pcie_drp_en                                 => '0',
    pcie_drp_we                                 => '0',
    pcie_drp_addr                               => (others => '0'),
    pcie_drp_di                                 => (others => '0'),
    pcie_drp_do                                 => open,
    pcie_drp_rdy                                => open,

  -------------------------------------------------------------------------------------------------------------------
  -- PIPE PORTS to TOP Level For PIPE SIMULATION with 3rd Party IP/BFM/Xilinx BFM                                  --
  -------------------------------------------------------------------------------------------------------------------
    common_commands_in                          => (others => '0'),
    pipe_rx_0_sigs                              => (others => '0'),
    pipe_rx_1_sigs                              => (others => '0'),
    pipe_rx_2_sigs                              => (others => '0'),
    pipe_rx_3_sigs                              => (others => '0'),
    pipe_rx_4_sigs                              => (others => '0'),
    pipe_rx_5_sigs                              => (others => '0'),
    pipe_rx_6_sigs                              => (others => '0'),
    pipe_rx_7_sigs                              => (others => '0'),
                                                                
    common_commands_out                         => open,
    pipe_tx_0_sigs                              => open,
    pipe_tx_1_sigs                              => open,
    pipe_tx_2_sigs                              => open,
    pipe_tx_3_sigs                              => open,
    pipe_tx_4_sigs                              => open,
    pipe_tx_5_sigs                              => open,
    pipe_tx_6_sigs                              => open,
    pipe_tx_7_sigs                              => open,

  -------------------------------------------------------------------------------------------------------------------
  -- 8. System(SYS) Interface                                                                                      --
  -------------------------------------------------------------------------------------------------------------------
	  pipe_mmcm_rst_n                             => '1',
    sys_clk                                     => sys_clk,
    sys_rst_n                                   => ep_sys_reset
	);

  ep_sys_reset <= '0' when ((sys_reset = '1') or (pl_received_hot_rst = '1')) else '1';

  ext_clk_i : if (PCIE_EXT_CLK = "TRUE") generate
	-----------------------------------------------

    pipe_clock_i : s7_pcie_ep_v3_1_pipe_clock
    -----------------------------------------
    generic map (
      PCIE_ASYNC_EN       => "FALSE",                                     -- PCIe async enable
      PCIE_TXBUF_EN       => "FALSE",                                     -- PCIe TX buffer enable for Gen1/Gen2 only
      PCIE_CLK_SHARING_EN => "FALSE",                                     -- Enable Clock Sharing
      PCIE_LANE           => LINK_WIDTH,                                  -- PCIe number of lanes
      PCIE_LINK_SPEED     => 3,                                           -- PCIe link speed
      PCIE_REFCLK_FREQ    => 0,                                           -- PCIe reference clock frequency
      PCIE_USERCLK1_FREQ  => (USER_CLK_FREQ + 1),                         -- PCIe user clock 1 frequency
      PCIE_USERCLK2_FREQ  => (USERCLK2_FREQ + 1),                         -- PCIe user clock 2 frequency
      PCIE_OOBCLK_MODE    => 1,                                           -- PCIe oob clock mode
      PCIE_DEBUG_MODE     => 0
    )
    port map (
    ---------- Input -------------------------------------
      CLK_CLK             => sys_clk ,
      CLK_TXOUTCLK        => pipe_txoutclk_out,                           -- Reference clock from lane 0
      CLK_RXOUTCLK_IN     => pipe_rxoutclk_out,
      CLK_RST_N           => '1',
      CLK_PCLK_SEL        => pipe_pclk_sel_out,
      CLK_PCLK_SEL_SLAVE  => (others => '0'),
      CLK_GEN3            => pipe_gen3_out,

    --------- Output ------------------------------------
      CLK_PCLK            => pipe_pclk_in,
      CLK_PCLK_SLAVE      => open,
      CLK_RXUSRCLK        => pipe_rxusrclk_in,
      CLK_RXOUTCLK_OUT    => pipe_rxoutclk_in,
      CLK_DCLK            => pipe_dclk_in,
      CLK_OOBCLK          => pipe_oobclk_in,
      CLK_USERCLK1        => pipe_userclk1_in,
      CLK_USERCLK2        => pipe_userclk2_in,
      CLK_MMCM_LOCK       => pipe_mmcm_lock_in
    );

  end generate ext_clk_i;

  int_clk_i : if (not(PCIE_EXT_CLK = "TRUE")) generate
	----------------------------------------------------

    pipe_pclk_in      <= '0';
    pipe_rxusrclk_in  <= '0';
    pipe_rxoutclk_in  <= (others => '0');
    pipe_dclk_in      <= '0';
    pipe_userclk1_in  <= '0';
    pipe_userclk2_in  <= '0';
    pipe_oobclk_in    <= '0';
    pipe_mmcm_lock_in <= '0';

  end generate int_clk_i;

end generate s7_type;

ep_state : if STAT_COUNTING = 1 generate

  ep_stat_i : ep_stat
  -------------------
  port map (
    trn_clk            => trn_clk,
    trn_rst            => trn_rst,

    ebone_reset        => ebone_reset,
    tx_ft_ready        => tx_ft_ready,
    TLP_end_Rqst       => TLP_end_Rqst,
		tx_err_drop        => tx_err_drop,
		s_axis_tx_tready   => s_axis_tx_tready,
		req_len_sup        => req_len_sup,
		req_rx_valid       => req_rx_valid,
		req_tx_valid       => req_tx_valid,

    err_drop_count     => err_drop_count,
		err_drop_ovfl      => err_drop_ovfl,
    tdst_rdy_count     => tdst_rdy_count,
		tdst_rdy_ovfl      => tdst_rdy_ovfl,
		req_len_sup_count  => req_len_sup_count,
		req_rx_valid_count => req_rx_valid_count,
		req_tx_valid_count => req_tx_valid_count
	);

end generate ep_state;

-- New Tieoffs signals linked to Series-7
-----------------------------------------
cfg_err_atomic_egress_blocked <= '0';                                                              -- Never report Atomic TLP blocked
cfg_err_internal_cor          <= '0';                                                              -- Never report internal error occurred
cfg_err_malformed             <= '0';                                                              -- Never report malformed error
cfg_err_mc_blocked            <= '0';                                                              -- Never report multi-cast TLP blocked
cfg_err_poisoned              <= '0';                                                              -- Never report poisoned TLP received
cfg_err_norecovery            <= '0';                                                              -- Never qualify cfg_err_poisoned or cfg_err_cpl_timeout
cfg_err_acs                   <= '0';                                                              -- Never report an ACS violation
cfg_err_internal_uncor        <= '0';                                                              -- Never report internal uncorrectable error
cfg_pm_halt_aspm_l0s          <= '0';                                                              -- Allow entry into L0s
cfg_pm_halt_aspm_l1           <= '0';                                                              -- Allow entry into L1
cfg_pm_force_state_en         <= '0';                                                              -- Do not qualify cfg_pm_force_state
cfg_pm_force_state            <= "00";                                                             -- Do not move force core into specific PM state
cfg_mgmt_wr_readonly          <= '0';                                                              -- Never treat RO bit as RW

cfg_interrupt_stat            <= '0';                                                              -- Never set the Interrupt Status bit
cfg_pciecap_interrupt_msgnum  <= "00000";                                                          -- Zero out Interrupt Message Number

cfg_err_aer_headerlog         <= (others => '0');                                                  -- Zero out the AER Header Log
cfg_aer_interrupt_msgnum      <= "00000";                                                          -- Zero out the AER Root Error Status Register
-----------------------------------------

user_cnst_pft <= '0' when (PL_FAST_TRAIN = "FALSE") else '1';
user_cnst_pec <= '1' when (PCIE_EXT_CLK = "TRUE") else '0';
user_cnst_trn <= '1' when (TRN_DW = "TRUE") else '0';
user_cnst_ucd <= '1' when (USER_CLK2_DIV2 = "TRUE") else '0';
user_constant <= user_cnst_pft & user_cnst_pec & user_cnst_trn & user_cnst_ucd &
                 (std_logic_vector(to_unsigned(USER_CLK_FREQ, 4))) &
                 (std_logic_vector(to_unsigned(USERCLK2_FREQ, 4))) &
                 (std_logic_vector(to_unsigned(PIPELINE_STAGES, 4)));

trn_rst             <= not trn_reset_n;
cfg_completer_id    <= cfg_bus_number & cfg_device_number & cfg_function_number;
cfg_bus_mstr_enable <= cfg_command(2);

sys_reset    <= not sys_reset_n;
trn_clk      <= user_clk_out;
trn_reset_n  <= not user_reset_out;
trn_lnk_up_n <= not user_lnk_up;

ep_link_up <= user_lnk_up;
ep_1_lane  <= cfg_lstatus(4);
ep_4_lane  <= cfg_lstatus(6);
ep_8_lane  <= cfg_lstatus(7);

payload_size_o <= '0' & cfg_dcommand(7 downto 5);
ext_data_ack_o <= ext_data_ack;

EP_PROBES_ON : if (EBM_PCIE_PRB = "ON") generate                                                   -- ebm0_pcie_b dedicated probes implementation
-- ----------------------------------------------------------------------------------------------------------------------------------------------

  with prob_mux(4 downto 0) select
    ep_probe(0) <= mm_probe(0) when "00000", mm_probe(1) when "00001",
                   mm_probe(2) when "00010", mm_probe(3) when "00011",
                   rx_probe(0) when "00100", rx_probe(1) when "00101",
                   rx_probe(2) when "00110", rx_probe(3) when "00111",
                   tx_probe(0) when "01000", tx_probe(1) when "01001",
                   tx_probe(2) when "01010", tx_probe(3) when "01011",
                   eb_probe(0) when "01100", eb_probe(1) when "01101",
                   eb_probe(2) when "01110", eb_probe(3) when "01111",
                   it_probe(0) when "10000", it_probe(1) when "10001",
                   it_probe(2) when "10010", it_probe(3) when "10011",
                   ap_probe(0) when "10100", ap_probe(1) when "10101",
                   ap_probe(2) when "10110", ap_probe(3) when "10111",
                   '0'         when others;

  with prob_mux(9 downto 5) select
    ep_probe(1) <= mm_probe(0) when "00000", mm_probe(1) when "00001",
                   mm_probe(2) when "00010", mm_probe(3) when "00011",
                   rx_probe(0) when "00100", rx_probe(1) when "00101",
                   rx_probe(2) when "00110", rx_probe(3) when "00111",
                   tx_probe(0) when "01000", tx_probe(1) when "01001",
                   tx_probe(2) when "01010", tx_probe(3) when "01011",
                   eb_probe(0) when "01100", eb_probe(1) when "01101",
                   eb_probe(2) when "01110", eb_probe(3) when "01111",
                   it_probe(0) when "10000", it_probe(1) when "10001",
                   it_probe(2) when "10010", it_probe(3) when "10011",
                   ap_probe(0) when "10100", ap_probe(1) when "10101",
                   ap_probe(2) when "10110", ap_probe(3) when "10111",
                   '0'         when others;

  with prob_mux(14 downto 10) select
    ep_probe(2) <= mm_probe(0) when "00000", mm_probe(1) when "00001",
                   mm_probe(2) when "00010", mm_probe(3) when "00011",
                   rx_probe(0) when "00100", rx_probe(1) when "00101",
                   rx_probe(2) when "00110", rx_probe(3) when "00111",
                   tx_probe(0) when "01000", tx_probe(1) when "01001",
                   tx_probe(2) when "01010", tx_probe(3) when "01011",
                   eb_probe(0) when "01100", eb_probe(1) when "01101",
                   eb_probe(2) when "01110", eb_probe(3) when "01111",
                   it_probe(0) when "10000", it_probe(1) when "10001",
                   it_probe(2) when "10010", it_probe(3) when "10011",
                   ap_probe(0) when "10100", ap_probe(1) when "10101",
                   ap_probe(2) when "10110", ap_probe(3) when "10111",
                   '0'         when others;

  with prob_mux(19 downto 15) select
    ep_probe(3) <= mm_probe(0) when "00000", mm_probe(1) when "00001",
                   mm_probe(2) when "00010", mm_probe(3) when "00011",
                   rx_probe(0) when "00100", rx_probe(1) when "00101",
                   rx_probe(2) when "00110", rx_probe(3) when "00111",
                   tx_probe(0) when "01000", tx_probe(1) when "01001",
                   tx_probe(2) when "01010", tx_probe(3) when "01011",
                   eb_probe(0) when "01100", eb_probe(1) when "01101",
                   eb_probe(2) when "01110", eb_probe(3) when "01111",
                   it_probe(0) when "10000", it_probe(1) when "10001",
                   it_probe(2) when "10010", it_probe(3) when "10011",
                   ap_probe(0) when "10100", ap_probe(1) when "10101",
                   ap_probe(2) when "10110", ap_probe(3) when "10111",
                   '0'         when others;

end generate EP_PROBES_ON;

EP_PROBES_OFF : if (EBM_PCIE_PRB = "OFF") generate                                                 -- ebm0_pcie_b dedicated probes unused
-- --------------------------------------------------------------------------------------------------------------------------------------
  ep_probe <= ap_probe;
end generate EP_PROBES_OFF;

end pcie_rtl; -- ebm0_pcie_b
