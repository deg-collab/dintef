--------------------------------------------------------------------------------
--
-- Description: Interrupt processing module
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.ep_comp_pkg.all;
use work.ebm0_pcie_b_pkg.all;

entity ep_interrupt is

generic (
  BC_PERIOD                : BIT_VECTOR := X"1"                           -- Broad-Call period (0:1us, 1:10us, 2:100us, 3:1ms)
);

port (
  clk                      : in std_logic;                                -- 125MHz user clock from the endpoint module
	trn_rst                  : in std_logic;                                -- Global active high reset signal

  cfg_interrupt            : out std_logic;                               -- Active high interrupt request signal
  cfg_interrupt_rdy        : in std_logic;                                -- Active high interrupt grant signal
  cfg_interrupt_assert     : out std_logic;                               -- Legacy interrupt asserted when high
  cfg_interrupt_di         : out std_logic_vector(7 downto 0);            -- Interrupt data in for MSI (Message Signaling Interrupt)
  cfg_interrupt_do         : in std_logic_vector(7 downto 0);             -- Interrupt data out, LSB message data field in the MSI structure
  cfg_interrupt_mmenable   : in std_logic_vector(2 downto 0);             -- Interrupt multiple Message enable in the MSI structure
  cfg_interrupt_msienable  : in std_logic;                                -- Interrupt MSI (Message Signaling Interrupt) enable
  cfg_interrupt_msixenable : in std_logic;                                -- Interrupt MSI-X (Message Signaling Interrupt-X) enable
  cfg_interrupt_msixfm     : in std_logic;                                -- Configuration interrupt MSI-X mask

	INtr_service_i           : in std_logic;                                -- Interrupt acknowledge (direct clear on interrupt status register read)
	INtr_disable_i           : in std_logic;                                -- cfg_command(10) from the Configuration Space Header Command register
	INtr_enable_i            : in std_logic;                                -- Global interrupt enable from BADR0 control register 0
	INtr_TLP_end_i           : in std_logic;                                -- Interrupt on TLP end of transfer enable
	INtr_B_calls_i           : in std_logic;                                -- Interrupt on Broad-call enable
	INtr_Rqst_nmb_i          : in std_logic_vector(7 downto 0);             -- Legacy interrupt value
	ext_intr_src_i           : in std_logic;                                -- External interrrupt source input
  INtr_gate_o              : out std_logic;                               -- Interrupt acknowledge running

  TLP_end_Rqst_i           : in std_logic;                                -- Interrupt on TLP end of transfer
  B_calls_Rqst_i           : in std_logic;                                -- Interrupt on Broad-call

  one_us_heartbeat_i       : in std_logic;                                -- One us pulse out clk synchronous
  it_probe_o               : out std_logic_vector(3 downto 0)             -- Test probes from this module
);
end ep_interrupt;

architecture rtl of ep_interrupt is

signal cfg_msi_mode           : std_logic;
signal cfg_interrupt_s        : std_logic;
signal cfg_interrupt_assert_s : std_logic;
signal cfg_interrupt_legacy   : std_logic;
signal INtr_Rqst_s            : std_logic;

signal latency_cnt            : std_logic_vector(9 downto 0);
signal latency_cnt_cl         : std_logic;
signal latency_cnt_en         : std_logic;
signal latency_cnt_tc         : std_logic;

type cfg_int_state_type       is (int_rst_state, int_start_msi, int_start_li, int_wait_rdy, int_test_source, int_wait_latency, int_wait_service);
signal cfg_int_state          : cfg_int_state_type;

begin

it_probe_o(0) <= INtr_Rqst_s;              -- 16
it_probe_o(1) <= cfg_interrupt_s;          -- 17
it_probe_o(2) <= cfg_interrupt_rdy;        -- 18
it_probe_o(3) <= INtr_service_i;           -- 19

cfg_interrupt <= cfg_interrupt_s;
cfg_interrupt_assert <= cfg_interrupt_assert_s;

INtr_Rqst_s <= '1' when (((TLP_end_Rqst_i = '1') and (INtr_TLP_end_i = '1')) or
                         ((B_calls_Rqst_i = '1') and (INtr_B_calls_i = '1')) or
												 (ext_intr_src_i = '1')) else '0';

latency_cnt_tc <= '1' when (((latency_cnt = X"001") and (BC_PERIOD = X"0")) or
	                          ((latency_cnt = X"00A") and (BC_PERIOD = X"1")) or
													  ((latency_cnt = X"064") and (BC_PERIOD = X"2")) or
													  ((latency_cnt = X"3E8") and (BC_PERIOD = X"3"))) else '0';

process(clk, latency_cnt_cl)
begin

  if (latency_cnt_cl = '1') then

    latency_cnt <= (others => '0');

  elsif (clk'event and clk = '1') then

    if ((latency_cnt_en = '1') and (one_us_heartbeat_i = '1')) then
      latency_cnt <= latency_cnt + 1;
	  end if;

  end if;

end process;

process(clk, trn_rst)
begin

  if (trn_rst = '1') then

    cfg_interrupt_di <= (others => '0');

	elsif clk'event and clk = '1' then

    if (cfg_interrupt_msienable = '1') then
		  if (cfg_interrupt_mmenable > 0) then
			  cfg_interrupt_di <= "000" & INtr_Rqst_nmb_i(4 downto 0);
			else
			  cfg_interrupt_di <= (others => '0');
			end if;
		else
		  cfg_interrupt_di <= INtr_Rqst_nmb_i;
		end if;

	end if;

end process;

process(clk, trn_rst)
begin

  if (trn_rst = '1') then

    latency_cnt_cl         <= '0';
		latency_cnt_en         <= '0';
		cfg_interrupt_s        <= '0';
		cfg_interrupt_assert_s <= '0';
		cfg_interrupt_legacy   <= '0';
		cfg_msi_mode           <= '0';
    INtr_gate_o            <= '0';
		cfg_int_state          <= int_rst_state;

	elsif clk'event and clk = '1' then

    case cfg_int_state is

      when int_rst_state =>

        if ((INtr_Rqst_s = '1') and (cfg_interrupt_msienable = '1') and (INtr_enable_i = '1')) then  -- Message Signaled Interrupt mode
          cfg_msi_mode  <= '1';
          INtr_gate_o   <= '1';
          cfg_int_state <= int_start_msi;
        elsif ((INtr_Rqst_s = '1') and (INtr_disable_i = '0') and (INtr_enable_i = '1')) then        -- Legacy Interrupt mode
          cfg_msi_mode  <= '0';
          INtr_gate_o   <= '1';
          cfg_int_state <= int_start_li;
        else
          cfg_interrupt_s        <= '0';
          cfg_interrupt_assert_s <= '0';
					cfg_interrupt_legacy   <= '0';
          cfg_msi_mode           <= '0';
          INtr_gate_o            <= '0';
          cfg_int_state          <= int_rst_state;
        end if;

      when int_start_msi =>

        latency_cnt_cl  <= '1';
        cfg_interrupt_s <= '1';
				cfg_int_state   <= int_wait_rdy;

      when int_start_li =>

        latency_cnt_cl         <= '1';
        cfg_interrupt_s        <= '1';
        cfg_interrupt_assert_s <= '1';
				cfg_interrupt_legacy   <= '0';
				cfg_int_state          <= int_wait_rdy;

      when int_wait_rdy =>

        if (cfg_interrupt_rdy = '1') then
          cfg_interrupt_s <= '0';
          if (cfg_msi_mode = '1') then
            cfg_int_state <= int_test_source;
					elsif ((cfg_msi_mode = '0') and (cfg_interrupt_legacy = '1')) then
					  cfg_interrupt_legacy <= '0';
            INtr_gate_o          <= '0';
            cfg_int_state        <= int_rst_state;
					else
					  cfg_interrupt_legacy <= '1';
            cfg_int_state        <= int_wait_service;
					end if;
				end if;

      when int_test_source =>

        if (INtr_Rqst_s = '1') then
				  latency_cnt_cl <= '0';
          latency_cnt_en <= '1';
					cfg_int_state  <= int_wait_latency;
				else
          INtr_gate_o   <= '0';
				  cfg_int_state <= int_rst_state;
				end if;

      when int_wait_latency =>

        if (latency_cnt_tc = '1') then
          latency_cnt_en <= '0';
          INtr_gate_o    <= '0';
					cfg_int_state  <= int_rst_state;
				else
				  cfg_int_state <= int_wait_latency;
				end if;

			when int_wait_service =>

        if (INtr_service_i = '1') then
          cfg_interrupt_s        <= '1';
          cfg_interrupt_assert_s <= '0';
					cfg_int_state          <= int_wait_rdy;
				end if;

      when others	=>

        latency_cnt_cl         <= '0';
        latency_cnt_en         <= '0';
        cfg_interrupt_s        <= '0';
        cfg_interrupt_assert_s <= '0';
				cfg_interrupt_legacy   <= '0';
        cfg_msi_mode           <= '0';
        INtr_gate_o            <= '0';
        cfg_int_state          <= int_rst_state;

		end case;

	end if;

end process;

end; -- ep_interrupt

