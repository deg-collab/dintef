--------------------------------------------------------------------------------
--
-- E-Bone Master#0/Slave#0 to PCI_Express Endpoint interface
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--
--------------------------------------------------------------------------------
--
-- Description:
-- ------------
-- HDL Functions package file
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

package ep_function_pkg is

type vc0_t_typ is record
  VC0_TOTAL_CREDITS_CD  : INTEGER;
  VC0_TOTAL_CREDITS_CH  : INTEGER;
  VC0_TOTAL_CREDITS_NPH : INTEGER;
  VC0_TOTAL_CREDITS_NPD : INTEGER;
  VC0_TOTAL_CREDITS_PD  : INTEGER;
  VC0_TOTAL_CREDITS_PH  : INTEGER;
  VC0_TX_LASTPACKET     : INTEGER;
end record;

constant vc0_t_a : vc0_t_typ := (461, 36, 12, 24, 437, 32, 29);
constant vc0_t_b : vc0_t_typ := (205, 36, 12, 24, 181, 32, 28);

function get_trgt_desc(full_type_desc : NATURAL) return NATURAL;
function get_gt_device(full_type_desc : NATURAL) return STRING;
function get_use_mode(full_type_desc : NATURAL) return STRING;
function get_link_width_conv(link_width_size : INTEGER) return BIT_VECTOR;
function get_trgt_link_speed(link_speed_size : INTEGER) return BIT_VECTOR;
function get_pipeline_stages(link_width_size : INTEGER; link_speed_size : INTEGER) return INTEGER;
function get_data_width(data_width_size : INTEGER) return STRING;
function get_user_clk_freq(clk_period : REAL; link_width_size : INTEGER; link_speed_size : INTEGER; data_width_size : INTEGER) return INTEGER;
function get_user_clk2_div2(clk_period : REAL; link_width_size : INTEGER; data_width_size : INTEGER) return STRING;
function get_user_clk2(DIV2 : STRING; UC_FREQ : INTEGER) return INTEGER;
function get_max_pay_sup(link_width_size : INTEGER; link_speed_size : INTEGER) return INTEGER;
function get_ram_lim(clk_period : REAL) return BIT_VECTOR;
function get_qpll_lock_size(link_width_size : INTEGER) return INTEGER;
function get_vc0_t(clk_period : REAL) return vc0_t_typ;

end package ep_function_pkg;

package body ep_function_pkg is

function get_trgt_desc (
  full_type_desc : NATURAL
)
return NATURAL is
  variable full_type_desc_high : std_logic_vector(7 downto 0);
  variable full_type_desc_stdl : std_logic_vector(31 downto 0);
begin
  full_type_desc_stdl := std_logic_vector(to_unsigned(full_type_desc, 32));
	full_type_desc_high := full_type_desc_stdl(31 downto 24);
	case full_type_desc_high is
		when X"71"  => return 16#71#;
		when X"72"  => return 16#72#;
		when X"73"  => return 16#73#;
		when others => return 16#00#;
	end case;
end get_trgt_desc;

function get_gt_device (
  full_type_desc : NATURAL
)
return STRING is
  variable full_type_desc_high : std_logic_vector(7 downto 0);
  variable full_type_desc_stdl : std_logic_vector(31 downto 0);
begin
  full_type_desc_stdl := std_logic_vector(to_unsigned(full_type_desc, 32));
	full_type_desc_high := full_type_desc_stdl(31 downto 24);
	case full_type_desc_high is
		when X"71"  => return "GTP";
		when X"72"  => return "GTX";
		when X"73"  => return "GTX";
		when others => return "GTX";
	end case;
end get_gt_device;

function get_use_mode (
  full_type_desc : NATURAL
)
return STRING is
  variable full_type_desc_high : std_logic_vector(7 downto 0);
  variable full_type_desc_stdl : std_logic_vector(31 downto 0);
begin
  full_type_desc_stdl := std_logic_vector(to_unsigned(full_type_desc, 32));
	full_type_desc_high := full_type_desc_stdl(31 downto 24);
	case full_type_desc_high is
		when X"71"  => return "1.0";
		when X"72"  => return "3.0";
		when X"73"  => return "3.0";
		when others => return "1.0";
	end case;
end get_use_mode;

function get_link_width_conv (
  link_width_size : INTEGER
)
return BIT_VECTOR is
begin
  if (link_width_size = 1) then
    return X"01";
	elsif (link_width_size = 4) then
	  return X"04";
	elsif (link_width_size = 8) then
	  return X"08";
  else
    return X"01";
  end if;
end get_link_width_conv;

function get_trgt_link_speed (
  link_speed_size : INTEGER
)
return BIT_VECTOR is
begin
  if (link_speed_size = 1) then
    return X"0";
	elsif (link_speed_size = 2) then
	  return X"2";
  else
    return X"0";
  end if;
end get_trgt_link_speed;

function get_pipeline_stages (
  link_width_size : INTEGER;
	link_speed_size : INTEGER
)
return INTEGER is
begin
  if ((link_width_size > 4) or (link_speed_size = 2)) then
	  return 1;
	else
	  return 0;
	end if;
end get_pipeline_stages;

function get_data_width (
  data_width_size : INTEGER
)
return STRING is
begin
  if (data_width_size = 128) then
	  return "TRUE";
	else
	  return "FALSE";
	end if;
end get_data_width;

function get_user_clk_freq (
	clk_period      : REAL;
  link_width_size : INTEGER;
	link_speed_size : INTEGER;
  data_width_size : INTEGER
)
return INTEGER is
begin
  if (clk_period = 16.0) then
  ---------------------------
    if (link_width_size = 1) then
      if (link_speed_size = 1) then
        return 1;
      elsif (link_speed_size = 2) then
        return 1;
      else
        return 1;
      end if;
    elsif (link_width_size = 4) then
      if (link_speed_size = 1) then
        return 1;
      elsif (link_speed_size = 2) then
        return 2;
      else
        return 1;
      end if;
    elsif (link_width_size = 8) then
      return 2;
    else
      return 1;
    end if;
  elsif (clk_period = 8.0) then
  -----------------------------
    if (link_width_size = 1) then
      if (link_speed_size = 1) then
        if (data_width_size = 64) then
          return 2;
        elsif (data_width_size = 128) then
          return 2;
        else
          return 2;
        end if;
      elsif (link_speed_size = 2) then
        if (data_width_size = 64) then
          return 2;
        elsif (data_width_size = 128) then
          return 2;
        else
          return 2;
        end if;
      else
        return 2;
      end if;
    elsif (link_width_size = 4) then
      if (link_speed_size = 1) then
        if (data_width_size = 64) then
          return 2;
        elsif (data_width_size = 128) then
          return 2;
        else
          return 2;
        end if;
      elsif (link_speed_size = 2) then
        if (data_width_size = 64) then
          return 3;
        elsif (data_width_size = 128) then
          return 3;
        else
          return 3;
        end if;
      else
        return 2;
      end if;
    elsif (link_width_size = 8) then
      if (link_speed_size = 1) then
        if (data_width_size = 64) then
          return 3;
        elsif (data_width_size = 128) then
          return 3;
        else
          return 3;
        end if;
      elsif (link_speed_size = 2) then
        if (data_width_size = 64) then
          return 4;
        elsif (data_width_size = 128) then
          return 4;
        else
          return 4;
        end if;
      else
        return 3;
      end if;
    else
      return 3;
    end if;
  elsif (clk_period = 4.0) then
  -----------------------------
    return 4;
  elsif (clk_period = 2.0) then
  -----------------------------
    return 4;
	else
    return 1;
  end if;
end get_user_clk_freq;

function get_user_clk2_div2 (
  clk_period      : REAL;
  link_width_size : INTEGER;
	data_width_size : INTEGER
)
return STRING is
begin
  if (clk_period = 16.0) then
    return "FALSE";
  elsif (clk_period = 8.0) then
    if (link_width_size > 1) then
      if (data_width_size > 64) then
        return "TRUE";
      else
        return "FALSE";
      end if;
    else
      return "FALSE";
    end if;
  elsif (clk_period = 4.0) then
    return "TRUE";
  elsif (clk_period = 2.0) then
    return "TRUE";
	else
	  return "FALSE";
  end if;
end get_user_clk2_div2;

function get_user_clk2 (
  DIV2    : STRING;
  UC_FREQ : INTEGER
)
return INTEGER is
begin
  if (DIV2 = "TRUE") then
    if (UC_FREQ = 4) then
      return 3;
    elsif (UC_FREQ = 3) then
      return 2;
    else
      return UC_FREQ;
    end if;
  else
    return UC_FREQ;
  end if;
end get_user_clk2;

function get_max_pay_sup (
  link_width_size : INTEGER;
	link_speed_size : INTEGER
)
return INTEGER is
begin
  if ((link_width_size > 4) and (link_speed_size > 1)) then
    return 1;
  else
    return 2;
  end if;
end get_max_pay_sup;

function get_ram_lim (
  clk_period : REAL
)
return BIT_VECTOR is
begin
  if (clk_period < 8.0) then
    return X"3FF";
  else
    return X"7FF";
  end if;
end get_ram_lim;

function get_vc0_t (
  clk_period : REAL
)
return vc0_t_typ is
begin
  if (clk_period < 8.0) then
    return vc0_t_b;
  else
    return vc0_t_a;
  end if;
end get_vc0_t;

function get_qpll_lock_size (
  link_width_size : INTEGER
)
return INTEGER is
begin
  if (link_width_size > 4) then
    return 2;
  else
    return 1;
  end if;
end get_qpll_lock_size;

end package body ep_function_pkg;
