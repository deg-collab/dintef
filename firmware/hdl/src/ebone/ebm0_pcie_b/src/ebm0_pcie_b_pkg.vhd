-------------------------------------------------------------------------------
--
-- E-Bone Master#0/Slave#0 to PCI_Express Endpoint interface
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--
--------------------------------------------------------------------------------
--
-- Description:
-- ------------
-- E-Bone Master#0/Slave#0 Package file
-- PCI_Express Endpoint interface for 7 series and Ultra-Scale FPGAs
-- Component declaration
--
-- Version  Date        Author
-- 3.2      07/03/2017  Le Caer
--
-- http://www.esrf.fr
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.ep_comp_pkg.all;
use work.ebs_pkg.all;

package ebm0_pcie_b_pkg is

type ext_typ is record                                                    -- Application extension control
  ext_ctrl_src : std_logic;                                               -- Configuration control when 1
  ext_intr_src : std_logic;                                               -- Interrrupt source input
  ext_addr_src : std_logic_vector(63 downto 0);                           -- Addr source input
	ext_data_src : std_logic_vector(MAX_WIDTH - 1 downto 0);                -- Data source input
  ext_data_flg : std_logic;                                               -- Data source input ready for DMA transfer when 1
end record;

constant NO_EXT : ext_typ := ('0', '0', GND64, GND256, '0');

--------------------------------------------------------------------------------------------------------------------------
component ebm0_pcie_b                                                     -- E-Bone Master#0 component (PCIe EP interface)
--------------------------------------------------------------------------------------------------------------------------
generic (
  EB_CLK             : REAL := 8.0;                                       -- E-Bone clock period in ns
	EBFT_DWIDTH        : NATURAL := 64;                                     -- FT data width
	BADR0_MEM          : INTEGER := 1;                                      -- 2Kbyte BADR0 Block Ram Memory implementation when 1
  WATCH_DOG          : INTEGER := 1;                                      -- Watch dog process implementation when 1
  TYPE_DESC          : NATURAL := 16#00000000#;                           -- Type description application and/or target dependant
	HARD_VERS          : NATURAL := 16#00000000#;                           -- Hardware version implemented
  DEVICE_ID          : BIT_VECTOR := X"EB01";                             -- Device ID = E-Bone 01
	BC_PERIOD          : BIT_VECTOR := X"11";                               -- Broad-Call period (0:1us, 1:10us, 2:100us, 3:1ms)
	LINK_SPEED         : INTEGER := 1;                                      -- PCI-Express EndPoint Link Speed (1=2.5G, 2=5G)
	LINK_WIDTH         : INTEGER := 1;                                      -- PCI-Express EndPoint Lane numbers (1,4,8)
  SIM_FILE           : STRING;                                            -- Simulation script
	STAT_COUNTING      : INTEGER := 1;                                      -- ep_stat module implementation when 1
  EBM_PCIE_PRB       : STRING := "OFF"                                    -- ebm0_pcie_b dedicated probes unused when OFF. ep_probe = ap_probe when OFF.
);

port (

--
-- PCIe Endpoint dedicated
--

  sys_clk_p          : in std_logic;                                      -- 100MHz positive differential input clock signal (from PCIe slot)
  sys_clk_n          : in std_logic;                                      -- 100MHz negative differential input clock signal (from PCIe slot)
  sys_reset_n        : in std_logic;                                      -- Asynchronous active low PCIe module reset input signal (from PCIe slot)

  pci_exp_rxn        : in std_logic_vector(LINK_WIDTH - 1 downto 0);      -- PCIe receive negative serial differential input signal (from PCIe slot)
  pci_exp_rxp        : in std_logic_vector(LINK_WIDTH - 1 downto 0);      -- PCIe receive positive serial differential input signal (from PCIe slot)
  pci_exp_txn        : out std_logic_vector(LINK_WIDTH - 1 downto 0);     -- PCIe transmit negative serial differential input signal (from PCIe slot)
  pci_exp_txp        : out std_logic_vector(LINK_WIDTH - 1 downto 0);     -- PCIe transmit positive serial differential input signal (from PCIe slot)

  ep_link_up         : out std_logic;                                     -- High when PCIe Link is Up
  ep_1_lane          : out std_logic;                                     -- High when 1 lane detected
  ep_4_lane          : out std_logic;                                     -- High when 4 lanes detected
  ep_8_lane          : out std_logic;                                     -- High when 8 lanes detected

  soft_wd            : out std_logic;                                     -- Active high external watch dog reset signal, witdh=250ns, period=1s
  soft_reset         : out std_logic;                                     -- Active high external reset on ctrl_regs(2) wr. operation (Device_ID & Vendor_ID), witdh=250ns
  status_led         : out std_logic_vector(1 downto 0);                  -- Status led for software test: 00=off, 01=red, 10=green
  ep_probe           : out std_logic_vector(3 downto 0);                  -- Output probe pads for debbuging purposes
	ap_probe           : in std_logic_vector(3 downto 0);                   -- Input test signals from the application

--
-- Master #0 
--

  eb_m0_clk_o        : out std_logic;                                     -- system clock
  eb_m0_rst_o        : out std_logic;                                     -- synchronous system reset
  eb_m0_brq_o        : out std_logic;                                     -- bus request
  eb_m0_bg_i         : in std_logic;                                      -- bus grant
  eb_bmx_i           : in std_logic;                                      -- busy some master (but FT)

  eb_m0_o            : out ebm32_typ;                                     -- master out
  eb_m_i             : in ebs32_o_typ;                                    -- master in shared

--
-- Fast Transmitter
--

  eb_ft_bg_i         : in std_logic;                                      -- busy FT
  eb_ft_dxt_i        : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);     -- FT data in
  eb_ft_i            : in ebft_typ;                                       -- FT control
  eb_ft_dk_o         : out std_logic;                                     -- FT slave data acknowledge
  eb_ft_err_o        : out std_logic;                                     -- FT slave bus error

--
-- Application extension signals
--

  ext_ctrl_i         : in ext_typ;                                        -- Application extension control
	ext_data_ack_o     : out std_logic;                                     -- External data source acknowledge output
  payload_size_o     : out std_logic_vector(3 downto 0)                   -- Payload size encoding output

);

end component;

end package ebm0_pcie_b_pkg;
