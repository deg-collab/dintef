--------------------------------------------------------------------------------
--
-- Description:  PCI Express endpoint top module
--
--------------------------------------------------------------------------------
-- Dependencies
---------------
-- ep_to_fnct
-- ep_to_ctrl
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.ep_comp_pkg.all;
use work.ebm0_pcie_b_pkg.all;
use work.ebs_pkg.std32_a;

entity ep_app is

generic (
	EBFT_DWIDTH                    : NATURAL := 32;                         -- FT data width
	BADR0_MEM                      : INTEGER := 1;                          -- 2Kbyte BADR0 Block Ram Memory implementation when 1
  WATCH_DOG                      : INTEGER := 1;                          -- Watch dog process implementation when 1
  AXI_DWIDTH                     : NATURAL := 32;                         -- PCI-Express EndPoint TRN/AXI interface data width
	BC_PERIOD                      : BIT_VECTOR := X"1";                    -- Broad-Call period (0:1us, 1:10us, 2:100us, 3:1ms)
  TRGT_DESC                      : NATURAL := 16#00#;                     -- Target dependant
	TYPE_DESC                      : NATURAL := 16#00000000#;               -- Type description application and/or target dependant
	HARD_VERS                      : NATURAL := 16#00000000#;               -- Hardware version implemented
	DEVICE_ID                      : BIT_VECTOR := X"EB01"                  -- Device ID = E-Bone 01
);

port (
  trn_clk                        : in std_logic;                          -- User clock from the endpoint module
  trn_rst                        : in std_logic;                          -- Active high reset signal from the endpoint
  trn_lnk_up_n                   : in std_logic;                          -- Low when PCIe Link is Up

-- Tx AXI4_Stream interface
  tx_buf_av                      : in std_logic_vector(5 downto 0);       -- Transmit Buffers Available
  tx_cfg_req                     : in std_logic;                          -- Transmit Configuration Request: Asserted when the core is ready to transmit Conf. Completion or internally generated TLP.
  tx_err_drop                    : in std_logic;                          -- Transmit Error Drop: Indicates that the core discarded a packet because of length violation
  s_axis_tx_tready               : in std_logic;                          -- Transmit Destination Ready: Indicates that the core is ready to accept data on s_axis_tx_tdata
  s_axis_tx_tdata                : out std_logic_vector((EBFT_DWIDTH - 1) downto 0);      -- Transmit Data: Packet data to be transmitted
  s_axis_tx_tkeep                : out std_logic_vector((EBFT_DWIDTH / 8 - 1) downto 0);  -- Transmit Data Strobe: Determines which data bytes are valid on s_axis_tx_tdata
  s_axis_tx_tlast                : out std_logic;                         -- Transmit End-of-Frame (EOF): Signals the end of a packet
  s_axis_tx_tvalid               : out std_logic;                         -- Transmit Source Ready: Indicates that the User Application is presenting valid data on s_axis_tx_tdata
  s_axis_tx_tuser                : out std_logic_vector(3 downto 0);      -- Transmit Source Discontinue: Can be asserted any time starting on the first cycle after SOF
  tx_cfg_gnt                     : out std_logic;                         -- Transmit Configuration Grant: Asserted by the User Application in response to tx_cfg_req

-- Rx AXI4_Stream interface
  m_axis_rx_tdata                : in std_logic_vector((EBFT_DWIDTH - 1) downto 0);       -- Receive Data: Packet data being received
  m_axis_rx_tkeep                : in std_logic_vector((EBFT_DWIDTH / 8 - 1) downto 0);   -- Receive Data Strobe: Determines which data bytes are valid on m_axis_rx_tdata
  m_axis_rx_tlast                : in std_logic;                          -- Receive End-of-Frame (EOF): Signals the end of a packet
  m_axis_rx_tvalid               : in std_logic;                          -- Receive Source Ready: Indicates that the core is presenting valid data on m_axis_rx_tdata
  m_axis_rx_tready               : out std_logic;                         -- Receive Destination Ready: Indicates that the User Application is ready to accept data on m_axis_rx_tdata
  m_axis_rx_tuser                : in std_logic_vector(21 downto 0);      -- Receive Packet Status
  rx_np_ok                       : out std_logic;                         -- Receive Non-Posted OK
  rx_np_req                      : out std_logic;                         -- Receive Non-Posted Request

-- Flow Control
  fc_cpld                        : in std_logic_vector(11 downto 0);      -- Completion data flow control credits
  fc_cplh                        : in std_logic_vector(7 downto 0);       -- Completion header flow control credits
  fc_npd                         : in std_logic_vector(11 downto 0);      -- Non posted data flow control credits
  fc_nph                         : in std_logic_vector(7 downto 0);       -- Non posted header flow control credits
  fc_pd                          : in std_logic_vector(11 downto 0);      -- Posted data flow control credits
  fc_ph                          : in std_logic_vector(7 downto 0);       -- Posted header flow control credits
  fc_sel                         : out std_logic_vector(2 downto 0);      -- Flow control informational select

-- Configuration (CFG) Interface
  cfg_do                         : in std_logic_vector(31 downto 0);      -- CFG interface data from the core
  cfg_rd_wr_done                 : in std_logic;                          -- CFG interface successful register access operation
  cfg_di                         : out std_logic_vector(31 downto 0);     -- CFG interface data to the core
  cfg_byte_en                    : out std_logic_vector(3 downto 0);      -- CFG byte enable when low
  cfg_dwaddr                     : out std_logic_vector(9 downto 0);      -- CFG interface DWORD address
  cfg_wr_en                      : out std_logic;                         -- CFG interface register write enable
  cfg_rd_en                      : out std_logic;                         -- CFG interface register read enable

  cfg_err_cor                    : out std_logic;                         -- CFG interface correctable error
  cfg_err_ur                     : out std_logic;                         -- CFG interface unsupported request
  cfg_err_ecrc                   : out std_logic;                         -- CFG interface ECRC report error
  cfg_err_cpl_timeout            : out std_logic;                         -- CFG interface error completion timeout
  cfg_err_cpl_abort              : out std_logic;                         -- CFG interface completion aborted
	cfg_err_cpl_unexpect           : out std_logic;                         -- CFG configuration error completion unexpected
  cfg_err_posted                 : out std_logic;                         -- CFG interface error posted
  cfg_err_locked                 : out std_logic;                         -- CFG error locked when low
	cfg_pm_wake                    : out std_logic;                         -- Configuration power management event when low
	cfg_trn_pending                : out std_logic;                         -- User transaction pending when low
  cfg_err_tlp_cpl_header         : out std_logic_vector(47 downto 0);     -- CFG error TLP completion header
  cfg_err_cpl_rdy                : in std_logic;                          -- CFG interface error completion ready

  cfg_turnoff_ok                 : out std_logic;                         -- Configuration turnoff ok
  cfg_to_turnoff                 : in std_logic;                          -- Configuration to turnoff
  cfg_bus_number                 : in std_logic_vector(7 downto 0);       -- Provides the assigned bus number for the device, used in TLPs
  cfg_device_number              : in std_logic_vector(4 downto 0);       -- Provides the assigned device number for the device, used in TLPs
  cfg_function_number            : in std_logic_vector(2 downto 0);       -- Provides the assigned function number for the device, used in TLPs
  cfg_status                     : in std_logic_vector(15 downto 0);      -- Status register from the Configuration Space Header
  cfg_command                    : in std_logic_vector(15 downto 0);      -- Command register from the Configuration Space Header
  cfg_dstatus                    : in std_logic_vector(15 downto 0);      -- Device Status register from the Configuration Space Header
  cfg_dcommand                   : in std_logic_vector(15 downto 0);      -- Device Command register from the Configuration Space Header
	cfg_dcommand2                  : in std_logic_vector(15 downto 0);      -- Device Command register 2 from the Configuration Space Header
  cfg_lstatus                    : in std_logic_vector(15 downto 0);      -- Link Status register from the Configuration Space Header
  cfg_lcommand                   : in std_logic_vector(15 downto 0);      -- Link Command register from the Configuration Space Header
  cfg_pcie_link_state            : in std_logic_vector(2 downto 0);       -- PCI Express link state report
  cfg_dsn                        : out std_logic_vector(63 downto 0);     -- Configuration device serial number
  cfg_pmcsr_pme_en               : in std_logic;                          -- Power management control/status register enable bit[8]
  cfg_pmcsr_pme_status           : in std_logic;                          -- Power management control/status register status bit[15]
  cfg_pmcsr_powerstate           : in std_logic_vector(1 downto 0);       -- Power management control/status register state bits[1:0]
  cfg_received_func_lvl_rst      : in std_logic;                          -- Received Function Level Reset: Indicates when the Function Level Reset has been received

-- Interrupt (CFG) Interface
  cfg_interrupt                  : out std_logic;                         -- Active high interrupt request signal
  cfg_interrupt_rdy              : in std_logic;                          -- Active high interrupt request signal
  cfg_interrupt_assert           : out std_logic;                         -- Legacy interrupt asserted when high
  cfg_interrupt_di               : out std_logic_vector(7 downto 0);      -- Interrupt data in for MSI (Message Signaling Interrupt)
  cfg_interrupt_do               : in std_logic_vector(7 downto 0);       -- Interrupt data out, LSB message data field in the MSI structure
  cfg_interrupt_mmenable         : in std_logic_vector(2 downto 0);       -- Interrupt multiple Message enable in the MSI structure
  cfg_interrupt_msienable        : in std_logic;                          -- Interrupt MSI (Message Signaling Interrupt) enable
  cfg_interrupt_msixenable       : in std_logic;                          -- Interrupt MSI-X (Message Signaling Interrupt-X) enable
  cfg_interrupt_msixfm           : in std_logic;                          -- Configuration interrupt MSI-X mask

  cfg_completer_id               : in std_logic_vector(15 downto 0);      -- cfg_bus_number & cfg_device_number & cfg_function_number
  cfg_bus_mstr_enable            : in std_logic;                          -- cfg_command(2) from the Configuration Space Header Command register

-- Physical Layer Control and Status (PL) Interface
  pl_initial_link_width          : in std_logic_vector(2 downto 0);       -- PL link width after successful link training
  pl_lane_reversal_mode          : in std_logic_vector(1 downto 0);       -- PL lane reversal mode
  pl_link_gen2_capable           : in std_logic;                          -- PL link Gen2 (5.0 Gb/s) capable if 1
  pl_link_partner_gen2_supported : in std_logic;                          -- PL link partner Gen2 (5.0 Gb/s) capable if 1
  pl_link_upcfg_capable          : in std_logic;                          -- PL link Upconfigure capable if 1
  pl_ltssm_state                 : in std_logic_vector(5 downto 0);       -- PL Link Training and Status State machine report
  pl_received_hot_rst            : in std_logic;                          -- PL Hot reset received
  pl_sel_link_rate               : in std_logic;                          -- PL current link rate, 0: 2.5 Gb/s, 1: 5.0 Gb/s
  pl_sel_link_width              : in std_logic_vector(1 downto 0);       -- PL current link width
	pl_phy_lnk_up                  : in std_logic;                          -- PL Indicates Physical Layer Link Up Status
  pl_tx_pm_state                 : in std_logic_vector(2 downto 0);       -- PL Indicates TX Power Management State
  pl_rx_pm_state                 : in std_logic_vector(1 downto 0);       -- PL Indicates RX Power Management State
  pl_directed_change_done        : in std_logic;                          -- PL Indicates the Directed change is done
  pl_directed_link_change        : out std_logic_vector(1 downto 0);      -- PL direct link width and/or speed change control
	pl_directed_link_width         : out std_logic_vector(1 downto 0);      -- PL Target link width for a directed link change operation
  pl_directed_link_speed         : out std_logic;                         -- PL Target link speed for a directed link change operation
	pl_directed_link_auton         : out std_logic;                         -- PL direct autonomous link change
  pl_upstream_prefer_deemph      : out std_logic;                         -- PL De-emphasis control according to the link speed

-- E-Bone link related
	ext_brd_data_i                 : in std32_x32;                          -- Memory Read Data array
  ext_brd_done_i                 : in std_logic;                          -- External Memory Read Burst done (E_Bone)
	brd_strt_o                     : out std_logic;                         -- Memory Read Burst start
  bwr_strt_o                     : out std_logic;                         -- Memory Write Burst start
  bwr_be_o                       : out std_logic_vector(7 downto 0);      -- Memory Write Byte Enable
  bwr_data_o                     : out std32_x32;                         -- Memory Write Data
  breq_len_o                     : out std_logic_vector(9 downto 0);      -- Memory Write payload length

  npck_prsnt_o                   : out std_logic;                         -- Indicates the start of a new packet header in m_axis_rx_tdata
	req_compl_o                    : out std_logic;                         -- Indicates completion required
	compl_done_o                   : out std_logic;                         -- Completion acknowledge

  Type_Offset_desc_o             : out std_logic_vector(31 downto 0);     -- Type/Offset descriptor word for E-Bone interfacing
  E_Bone_trn_o                   : out std_logic;                         -- E_Bone external transaction request
  tx_ft_ready_i                  : in std_logic;                          -- External TLP transmit request
	tx_ft_ready_o                  : out std_logic;                         -- Internal TLP transmit request for missing bytes transfer
	tx_tvalid_flag_o               : out std_logic;                         -- Transmit data valid flag
  eb_ft_data_i                   : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);  -- TLP transmit data from fast transmitter
  eb_ft_addr_i                   : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);  -- TLP transmit address from fast transmitter
	eb_data_size_i                 : in std_logic_vector(7 downto 0);       -- TLP data size valid
	eb_last_size_i                 : in std_logic_vector(7 downto 0);       -- TLP last size valid
	eb_byte_size_i                 : in std_logic_vector(15 downto 0);      -- TLP byte count
	eb_bft_i                       : in std_logic;                          -- busy FT
  eb_eof_i                       : in std_logic;                          -- FT slave end of frame
	TLP_end_Rqst_i                 : in std_logic;                          -- TLP end of transfer => interrupt module
	INtr_B_calls_o                 : out std_logic;                         -- Interrupt on Broad-call enable
  INtr_gate_o                    : out std_logic;                         -- Interrupt acknowledge running
	bcall_run_i                    : in std_logic;                          -- Broad-call process running
	bcall_reg_i                    : in std_logic_vector(31 downto 0);      -- Broad-call report register
  RD_on_o                        : out std_logic;                         -- MRd TLP running
  WR_on_o                        : out std_logic;                         -- MWr TLP running
  FT_on_i                        : in std_logic;                          -- FT has started when 1
  FT_clr_o                       : out std_logic;                         -- FT completed from Tx engine
  wr_busy_eb_i                   : in std_logic;                          -- E_Bone write cycle end when low
	one_us_heartbeat_i             : in std_logic;                          -- One us pulse out trn_clk synchronous

  soft_wd_o                      : out std_logic;                         -- External watch dog reset signal
  soft_reset_o                   : out std_logic;                         -- External reset on ctrl_regs(2) write operation with Device_ID & Vendor_ID pattern
  status_led_o                   : out std_logic_vector(1 downto 0);      -- Status led for software test: 00=off, 01=red, 10=green
  ebone_reset_o                  : out std_logic;                         -- E-Bone part reset when high

  err_drop_count                 : in std_logic_vector(14 downto 0);
	err_drop_ovfl                  : in std_logic;
	tdst_rdy_count                 : in std_logic_vector(14 downto 0);
	tdst_rdy_ovfl                  : in std_logic;
	req_len_sup_o                  : out std_logic;                         -- High for one system clock period when req_len > 1
	req_len_sup_count              : in std_logic_vector(15 downto 0);
	req_rx_valid_o                 : out std_logic;                         -- High for one system clock period when RX_MEM_RD32_FMT_TYPE
	req_rx_valid_count             : in std_logic_vector(31 downto 0);
	req_tx_valid_o                 : out std_logic;                         -- High for one system clock period when TX_MWR_FMT_TYPE or TX_MWR64_FMT_TYPE
	req_tx_valid_count             : in std_logic_vector(31 downto 0);

-- Application extension signals
  ext_ctrl_src_i                 : in std_logic;                                   -- External configuration control when 1
  ext_intr_src_i                 : in std_logic;                                   -- External interrrupt source input
	ext_addr_src_i                 : in std_logic_vector(63 downto 0);               -- External addr source input
	ext_data_src_i                 : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);  -- External data source input
	ext_data_flg_i                 : in std_logic;                                   -- External data source input ready for DMA transfer when 1
	ext_data_ack_o                 : out std_logic;                                  -- External data source acknowledge output

  mm_probe_o                     : out std_logic_vector(3 downto 0);
  rx_probe_o                     : out std_logic_vector(3 downto 0);
  tx_probe_o                     : out std_logic_vector(3 downto 0);
  it_probe_o                     : out std_logic_vector(3 downto 0);
  prob_mux_o                     : out std_logic_vector(19 downto 0);

  my_payload_size_o              : out std_logic_vector(2 downto 0);
  user_cnst_i                    : in std_logic_vector(15 downto 0);
  tx_ft_delay_o                  : out std_logic;                                  -- Next FT delayed
  tlp_delayed_o                  : out std_logic                                   -- TLP running
);
end ep_app;

architecture rtl of ep_app is

signal req_compl           : std_logic;
signal compl_done          : std_logic;

begin

-- PCIe Block EP Tieoffs; doesn't support the following inputs
--------------------------------------------------------------
fc_sel                    <= "000";
rx_np_ok                  <= '1';                                      -- Allow Reception of Non-posted Traffic
rx_np_req                 <= '1';                                      -- Always request Non-posted Traffic if available
tx_cfg_gnt                <= '1';                                      -- Always allow transmission of Config traffic within block
cfg_err_cor               <= '0';                                      -- Never report Correctable Error
cfg_err_ur                <= '0';                                      -- Never report UR
cfg_err_ecrc              <= '0';                                      -- Never report ECRC Error
cfg_err_cpl_timeout       <= '0';                                      -- Never report Completion Timeout
cfg_err_cpl_abort         <= '0';                                      -- Never report Completion Abort
cfg_err_cpl_unexpect      <= '0';                                      -- Never report unexpected completion
cfg_err_posted            <= '0';                                      -- Never qualify cfg_err_* inputs
cfg_err_locked            <= '0';                                      -- Never qualify cfg_err_ur or cfg_err_cpl_abort
cfg_pm_wake               <= '0';                                      -- Never direct the core to send a PM_PME Message
cfg_trn_pending           <= '0';                                      -- Never set the transaction pending bit in the Device Status Register

cfg_err_tlp_cpl_header    <= x"000000000000";                          -- Zero out the header information
cfg_dsn                   <= X"0000000101000A35";                      -- Assign the input DSN
--------------------------------------------------------------

req_compl_o  <= req_compl;
compl_done_o <= compl_done;

----------------------------------------------------------------------------------------------------------------------
ep_to_fnct_i : ep_to_fnct                                              -- Endpoint functions top module implementation
----------------------------------------------------------------------------------------------------------------------
generic map (
  EBFT_DWIDTH                    => EBFT_DWIDTH,
  BADR0_MEM                      => BADR0_MEM,
	WATCH_DOG                      => WATCH_DOG,
	AXI_DWIDTH                     => AXI_DWIDTH,
	BC_PERIOD                      => BC_PERIOD,
  TRGT_DESC                      => TRGT_DESC,
	TYPE_DESC                      => TYPE_DESC,
	HARD_VERS                      => HARD_VERS,
	DEVICE_ID                      => DEVICE_ID
)

port map (
  clk                            => trn_clk,                           -- I
  trn_rst                        => trn_rst,                           -- I
	trn_lnk_up_n                   => trn_lnk_up_n,                      -- I

-- Tx AXI4_Stream interface
  tx_buf_av                      => tx_buf_av,                         -- I [5:0]
  tx_cfg_req                     => tx_cfg_req,                        -- I
  tx_err_drop                    => tx_err_drop,                       -- I
  s_axis_tx_tready               => s_axis_tx_tready,                  -- I
  s_axis_tx_tdata                => s_axis_tx_tdata,                   -- O [EBFT_DWIDTH - 1:0]
  s_axis_tx_tkeep                => s_axis_tx_tkeep,                   -- O [EBFT_DWIDTH / 8 - 1:0]
  s_axis_tx_tlast                => s_axis_tx_tlast,                   -- O
  s_axis_tx_tvalid               => s_axis_tx_tvalid,                  -- O
  s_axis_tx_tuser                => s_axis_tx_tuser,                   -- O [3:0]

-- Rx AXI4_Stream interface
  m_axis_rx_tdata                => m_axis_rx_tdata,                   -- I [EBFT_DWIDTH - 1:0]
  m_axis_rx_tkeep                => m_axis_rx_tkeep,                   -- I [EBFT_DWIDTH / 8 - 1:0]
  m_axis_rx_tlast                => m_axis_rx_tlast,                   -- I
  m_axis_rx_tvalid               => m_axis_rx_tvalid,                  -- I
  m_axis_rx_tready               => m_axis_rx_tready,                  -- O
  m_axis_rx_tuser                => m_axis_rx_tuser,                   -- I [21:0]

-- Configuration (CFG) Interface
  cfg_completer_id               => cfg_completer_id,                  -- I [15:0]
  cfg_bus_mstr_enable            => cfg_bus_mstr_enable,               -- I

  cfg_do                         => cfg_do,                            -- I [31:0]
  cfg_rd_wr_done                 => cfg_rd_wr_done,                    -- I
  cfg_di                         => cfg_di,                            -- O [31:0]
  cfg_byte_en                    => cfg_byte_en,                       -- O [3:0]
  cfg_dwaddr                     => cfg_dwaddr,                        -- O [9:0]
  cfg_wr_en                      => cfg_wr_en,                         -- O
  cfg_rd_en                      => cfg_rd_en,                         -- O

  cfg_bus_number                 => cfg_bus_number,                    -- I [7:0]
  cfg_device_number              => cfg_device_number,                 -- I [4:0]
  cfg_function_number            => cfg_function_number,               -- I [2:0]
  cfg_status                     => cfg_status,                        -- I [15:0]
  cfg_command                    => cfg_command,                       -- I [15:0]
  cfg_dstatus                    => cfg_dstatus,                       -- I [15:0]
  cfg_dcommand                   => cfg_dcommand,                      -- I [15:0]
	cfg_dcommand2                  => cfg_dcommand2,                     -- I [15:0]
  cfg_lstatus                    => cfg_lstatus,                       -- I [15:0]
  cfg_lcommand                   => cfg_lcommand,                      -- I [15:0]

-- Interrupt (CFG) Interface
  cfg_interrupt                  => cfg_interrupt,                     -- O
  cfg_interrupt_rdy              => cfg_interrupt_rdy,                 -- I
  cfg_interrupt_assert           => cfg_interrupt_assert,              -- O
  cfg_interrupt_di               => cfg_interrupt_di,                  -- O [7:0]
  cfg_interrupt_do               => cfg_interrupt_do,                  -- I [7:0]
  cfg_interrupt_mmenable         => cfg_interrupt_mmenable,            -- I [2:0]
  cfg_interrupt_msienable        => cfg_interrupt_msienable,           -- I
  cfg_interrupt_msixenable       => cfg_interrupt_msixenable,          -- I
  cfg_interrupt_msixfm           => cfg_interrupt_msixfm,              -- I
  TLP_end_Rqst_i                 => TLP_end_Rqst_i,                    -- I
  INtr_gate_o                    => INtr_gate_o,                       -- O

-- Physical Layer Control and Status (PL) Interface
  pl_initial_link_width          => pl_initial_link_width,             -- I [2:0]
  pl_lane_reversal_mode          => pl_lane_reversal_mode,             -- I [1:0]
  pl_link_gen2_capable           => pl_link_gen2_capable,              -- I
  pl_link_partner_gen2_supported => pl_link_partner_gen2_supported,    -- I
  pl_link_upcfg_capable          => pl_link_upcfg_capable,             -- I
  pl_ltssm_state                 => pl_ltssm_state,                    -- I
  pl_received_hot_rst            => pl_received_hot_rst,               -- I
  pl_sel_link_rate               => pl_sel_link_rate,                  -- I
  pl_sel_link_width              => pl_sel_link_width,                 -- I [1:0]
	pl_phy_lnk_up                  => pl_phy_lnk_up,                     -- I
  pl_tx_pm_state                 => pl_tx_pm_state,                    -- I [2:0]
  pl_rx_pm_state                 => pl_rx_pm_state,                    -- I [1:0]
  pl_directed_change_done        => pl_directed_change_done,           -- I
  pl_directed_link_auton         => pl_directed_link_auton,            -- O
  pl_directed_link_change        => pl_directed_link_change,           -- O [1:0]
  pl_directed_link_speed         => pl_directed_link_speed,            -- O
  pl_directed_link_width         => pl_directed_link_width,            -- O [1:0]
  pl_upstream_prefer_deemph      => pl_upstream_prefer_deemph,         -- O

  npck_prsnt_o                   => npck_prsnt_o,                      -- O
  req_compl_o                    => req_compl,                         -- O
  compl_done_o                   => compl_done,                        -- O

-- E-Bone related
	ext_brd_data_i                 => ext_brd_data_i,                    -- I [32][31:0]
  ext_brd_done_i                 => ext_brd_done_i,                    -- I
	brd_strt_o                     => brd_strt_o,                        -- O
  bwr_strt_o                     => bwr_strt_o,                        -- O
  bwr_be_o                       => bwr_be_o,                          -- O [7:0]
  bwr_data_o                     => bwr_data_o,                        -- O [32][31:0]
  breq_len_o                     => breq_len_o,                        -- O [9:0]

  Type_Offset_desc_o             => Type_Offset_desc_o,                -- O [31:0]
  E_Bone_trn_o                   => E_Bone_trn_o,                      -- O
  eb_ft_data_i                   => eb_ft_data_i,                      -- I [EBFT_DWIDTH - 1:0]
  eb_ft_addr_i                   => eb_ft_addr_i,                      -- I [63:0]
	eb_data_size_i                 => eb_data_size_i,                    -- I [7:0]
	eb_last_size_i                 => eb_last_size_i,                    -- I [7:0]
	eb_byte_size_i                 => eb_byte_size_i,                    -- I [15:0]
	eb_bft_i                       => eb_bft_i,                          -- I
  eb_eof_i                       => eb_eof_i,                          -- I
	tx_ft_ready_i                  => tx_ft_ready_i,                     -- I
	tx_ft_ready_o                  => tx_ft_ready_o,                     -- O
	tx_tvalid_flag_o               => tx_tvalid_flag_o,                  -- O
	INtr_B_calls_o                 => INtr_B_calls_o,                    -- O
	bcall_run_i                    => bcall_run_i,                       -- I
	bcall_reg_i                    => bcall_reg_i,                       -- I [31:0]
  RD_on_o                        => RD_on_o,                           -- O
  WR_on_o                        => WR_on_o,                           -- O
  FT_on_i                        => FT_on_i,                           -- I
  FT_clr_o                       => FT_clr_o,                          -- O
	wr_busy_eb_i                   => wr_busy_eb_i,                      -- I
	one_us_heartbeat_i             => one_us_heartbeat_i,                -- I

  soft_wd_o                      => soft_wd_o,                         -- O
  soft_reset_o                   => soft_reset_o,                      -- O
  status_led_o                   => status_led_o,                      -- O [1:0]
  ebone_reset_o                  => ebone_reset_o,                     -- O

  err_drop_count                 => err_drop_count,                    -- I [14:0]
	err_drop_ovfl                  => err_drop_ovfl,                     -- I
	tdst_rdy_count                 => tdst_rdy_count,                    -- I [14:0]
	tdst_rdy_ovfl                  => tdst_rdy_ovfl,                     -- I
	req_len_sup_o                  => req_len_sup_o,                     -- O
	req_len_sup_count              => req_len_sup_count,                 -- I [15:0]
	req_rx_valid_o                 => req_rx_valid_o,                    -- O
	req_rx_valid_count             => req_rx_valid_count,                -- I [31:0]
	req_tx_valid_o                 => req_tx_valid_o,                    -- O
	req_tx_valid_count             => req_tx_valid_count,                -- I [31:0]

-- Application extension signals
  ext_ctrl_src_i                 => ext_ctrl_src_i,                    -- I
  ext_intr_src_i                 => ext_intr_src_i,                    -- I
	ext_addr_src_i                 => ext_addr_src_i,                    -- I [63:0]
	ext_data_src_i                 => ext_data_src_i,                    -- I [EBFT_DWIDTH - 1:0]
	ext_data_flg_i                 => ext_data_flg_i,                    -- I
	ext_data_ack_o                 => ext_data_ack_o,                    -- O

  mm_probe_o                     => mm_probe_o,                        -- O [3:0]
  rx_probe_o                     => rx_probe_o,                        -- O [3:0]
  tx_probe_o                     => tx_probe_o,                        -- O [3:0]
  it_probe_o                     => it_probe_o,                        -- O [3:0]
  prob_mux_o                     => prob_mux_o,                        -- O [19:0]

  my_payload_size_o              => my_payload_size_o,                 -- O [2:0]
  user_cnst_i                    => user_cnst_i,                       -- I [15:0]
  tx_ft_delay_o                  => tx_ft_delay_o,                     -- O
  tlp_delayed_o                  => tlp_delayed_o                      -- O
);

--------------------------------------------------------------------------------------------------------------
ep_to_ctrl_i : ep_to_ctrl                                              -- Turn-off Control Unit implementation
--------------------------------------------------------------------------------------------------------------
port map (
  clk                            => trn_clk,                           -- I
  trn_rst                        => trn_rst,                           -- I

  req_compl_i                    => req_compl,                         -- I
  compl_done_i                   => compl_done,                        -- I

  cfg_to_turnoff                 => cfg_to_turnoff,                    -- I
  cfg_turnoff_ok                 => cfg_turnoff_ok                     -- O
);

end rtl;

