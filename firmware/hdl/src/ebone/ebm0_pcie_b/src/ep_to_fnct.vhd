--------------------------------------------------------------------------------
--
-- Description: Endpoint functions top module
--
--------------------------------------------------------------------------------
-- Dependencies
---------------
-- ep_cfg_rd
-- ep_interrupt
-- ep_rx_engine
-- ep_tx_engine
-- ep_mem_access
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.ep_comp_pkg.all;
use work.ebm0_pcie_b_pkg.all;
use work.ebs_pkg.std32_a;

entity ep_to_fnct is

generic (
  EBFT_DWIDTH                    : NATURAL := 32;                         -- FT data width
	BADR0_MEM                      : INTEGER := 1;                          -- 2Kbyte BADR0 Block Ram Memory implementation when 1
  WATCH_DOG                      : INTEGER := 1;                          -- Watch dog process implementation when 1
  AXI_DWIDTH                     : NATURAL := 32;                         -- PCI-Express EndPoint TRN/AXI interface data width
	BC_PERIOD                      : BIT_VECTOR := X"1";                    -- Broad-Call period (0:1us, 1:10us, 2:100us, 3:1ms)
  TRGT_DESC                      : NATURAL := 16#00#;                     -- Target dependant
	TYPE_DESC                      : NATURAL := 16#00000000#;               -- Type description application and/or target dependant
	HARD_VERS                      : NATURAL := 16#00000000#;               -- Hardware version implemented
	DEVICE_ID                      : BIT_VECTOR := X"EB01"                  -- Device ID = E-Bone 01
);

port (
  clk                            : in std_logic;
  trn_rst                        : in std_logic;
  trn_lnk_up_n                   : in std_logic;

-- Tx AXI4_Stream interface
  tx_buf_av                      : in std_logic_vector(5 downto 0);       -- Transmit Buffers Available
  tx_cfg_req                     : in std_logic;                          -- Transmit Configuration Request: Asserted when the core is ready to transmit Conf. Completion or internally generated TLP.
  tx_err_drop                    : in std_logic;                          -- Transmit Error Drop: Indicates that the core discarded a packet because of length violation
  s_axis_tx_tready               : in std_logic;                          -- Transmit Destination Ready: Indicates that the core is ready to accept data on s_axis_tx_tdata
  s_axis_tx_tdata                : out std_logic_vector((EBFT_DWIDTH - 1) downto 0);      -- Transmit Data: Packet data to be transmitted
  s_axis_tx_tkeep                : out std_logic_vector((EBFT_DWIDTH / 8 - 1) downto 0);  -- Transmit Data Strobe: Determines which data bytes are valid on s_axis_tx_tdata
  s_axis_tx_tlast                : out std_logic;                         -- Transmit End-of-Frame (EOF): Signals the end of a packet
  s_axis_tx_tvalid               : out std_logic;                         -- Transmit Source Ready: Indicates that the User Application is presenting valid data on s_axis_tx_tdata
  s_axis_tx_tuser                : out std_logic_vector(3 downto 0);      -- Transmit Source Discontinue: Can be asserted any time starting on the first cycle after SOF

-- Rx AXI4_Stream interface
  m_axis_rx_tdata                : in std_logic_vector((EBFT_DWIDTH - 1) downto 0);       -- Receive Data: Packet data being received
  m_axis_rx_tkeep                : in std_logic_vector((EBFT_DWIDTH / 8 - 1) downto 0);   -- Receive Data Strobe: Determines which data bytes are valid on m_axis_rx_tdata
  m_axis_rx_tlast                : in std_logic;                          -- Receive End-of-Frame (EOF): Signals the end of a packet
  m_axis_rx_tvalid               : in std_logic;                          -- Receive Source Ready: Indicates that the core is presenting valid data on m_axis_rx_tdata
  m_axis_rx_tready               : out std_logic;                         -- Receive Destination Ready: Indicates that the User Application is ready to accept data on m_axis_rx_tdata
  m_axis_rx_tuser                : in std_logic_vector(21 downto 0);      -- Receive Packet Status

-- Configuration (CFG) Interface
  cfg_completer_id               : in std_logic_vector(15 downto 0);      -- cfg_bus_number & cfg_device_number & cfg_function_number
  cfg_bus_mstr_enable            : in std_logic;                          -- cfg_command(2) from the Configuration Space Header Command register

  cfg_do                         : in std_logic_vector(31 downto 0);      -- CFG interface data from the core
  cfg_rd_wr_done                 : in std_logic;                          -- CFG interface successful register access operation
  cfg_di                         : out std_logic_vector(31 downto 0);     -- CFG interface data to the core
  cfg_byte_en                    : out std_logic_vector(3 downto 0);      -- CFG byte enable when low
  cfg_dwaddr                     : out std_logic_vector(9 downto 0);      -- CFG interface DWORD address
  cfg_wr_en                      : out std_logic;                         -- CFG interface register write enable
  cfg_rd_en                      : out std_logic;                         -- CFG interface register read enable

  cfg_bus_number                 : in std_logic_vector(7 downto 0);       -- Provides the assigned bus number for the device, used in TLPs
  cfg_device_number              : in std_logic_vector(4 downto 0);       -- Provides the assigned device number for the device, used in TLPs
  cfg_function_number            : in std_logic_vector(2 downto 0);       -- Provides the assigned function number for the device, used in TLPs
  cfg_status                     : in std_logic_vector(15 downto 0);      -- Status register from the Configuration Space Header
  cfg_command                    : in std_logic_vector(15 downto 0);      -- Command register from the Configuration Space Header
  cfg_dstatus                    : in std_logic_vector(15 downto 0);      -- Device Status register from the Configuration Space Header
  cfg_dcommand                   : in std_logic_vector(15 downto 0);      -- Device Command register from the Configuration Space Header
	cfg_dcommand2                  : in std_logic_vector(15 downto 0);      -- Device Command register 2 from the Configuration Space Header
  cfg_lstatus                    : in std_logic_vector(15 downto 0);      -- Link Status register from the Configuration Space Header
  cfg_lcommand                   : in std_logic_vector(15 downto 0);      -- Link Command register from the Configuration Space Header

-- Interrupt (CFG) Interface
  cfg_interrupt                  : out std_logic;                         -- Active high interrupt request signal
  cfg_interrupt_rdy              : in std_logic;                          -- Active high interrupt request signal
  cfg_interrupt_assert           : out std_logic;                         -- Legacy interrupt asserted when high
  cfg_interrupt_di               : out std_logic_vector(7 downto 0);      -- Interrupt data in for MSI (Message Signaling Interrupt)
  cfg_interrupt_do               : in std_logic_vector(7 downto 0);       -- Interrupt data out, LSB message data field in the MSI structure
  cfg_interrupt_mmenable         : in std_logic_vector(2 downto 0);       -- Interrupt multiple Message enable in the MSI structure
  cfg_interrupt_msienable        : in std_logic;                          -- Interrupt MSI (Message Signaling Interrupt) enable
  cfg_interrupt_msixenable       : in std_logic;                          -- Interrupt MSI-X (Message Signaling Interrupt-X) enable
  cfg_interrupt_msixfm           : in std_logic;                          -- Configuration interrupt MSI-X mask
  TLP_end_Rqst_i                 : in std_logic;                          -- TLP end of transfer => interrupt module
  INtr_gate_o                    : out std_logic;                         -- Interrupt acknowledge running

-- Physical Layer Control and Status (PL) Interface
  pl_initial_link_width          : in std_logic_vector(2 downto 0);       -- PL link width after successful link training
  pl_lane_reversal_mode          : in std_logic_vector(1 downto 0);       -- PL lane reversal mode
  pl_link_gen2_capable           : in std_logic;                          -- PL link Gen2 (5.0 Gb/s) capable if 1
  pl_link_partner_gen2_supported : in std_logic;                          -- PL link partner Gen2 (5.0 Gb/s) capable if 1
  pl_link_upcfg_capable          : in std_logic;                          -- PL link Upconfigure capable if 1
  pl_ltssm_state                 : in std_logic_vector(5 downto 0);       -- PL Link Training and Status State machine report
  pl_received_hot_rst            : in std_logic;                          -- PL Hot reset received
  pl_sel_link_rate               : in std_logic;                          -- PL current link rate, 0: 2.5 Gb/s, 1: 5.0 Gb/s
  pl_sel_link_width              : in std_logic_vector(1 downto 0);       -- PL current link width
	pl_phy_lnk_up                  : in std_logic;                          -- PL Indicates Physical Layer Link Up Status
  pl_tx_pm_state                 : in std_logic_vector(2 downto 0);       -- PL Indicates TX Power Management State
  pl_rx_pm_state                 : in std_logic_vector(1 downto 0);       -- PL Indicates RX Power Management State
  pl_directed_change_done        : in std_logic;                          -- PL Indicates the Directed change is done
  pl_directed_link_auton         : out std_logic;                         -- PL direct autonomous link change
  pl_directed_link_change        : out std_logic_vector(1 downto 0);      -- PL direct link width and/or speed change control
  pl_directed_link_speed         : out std_logic;                         -- PL Target link speed for a directed link change operation
  pl_directed_link_width         : out std_logic_vector(1 downto 0);      -- PL Target link width for a directed link change operation
  pl_upstream_prefer_deemph      : out std_logic;                         -- PL De-emphasis control according to the link speed

  npck_prsnt_o                   : out std_logic;                         -- Indicates the start of a new packet header in m_axis_rx_tdata
  req_compl_o                    : out std_logic;
  compl_done_o                   : out std_logic;

	ext_brd_data_i                 : in std32_x32;
	ext_brd_done_i                 : in std_logic;
	brd_strt_o                     : out std_logic;
  bwr_strt_o                     : out std_logic;
  bwr_be_o                       : out std_logic_vector(7 downto 0);
  bwr_data_o                     : out std32_x32;
  breq_len_o                     : out std_logic_vector(9 downto 0);

  Type_Offset_desc_o             : out std_logic_vector(31 downto 0);
  E_Bone_trn_o                   : out std_logic;
  eb_ft_data_i                   : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);
  eb_ft_addr_i                   : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);
	eb_data_size_i                 : in std_logic_vector(7 downto 0);
	eb_last_size_i                 : in std_logic_vector(7 downto 0);
	eb_byte_size_i                 : in std_logic_vector(15 downto 0);
	eb_bft_i                       : in std_logic;
  eb_eof_i                       : in std_logic;
	tx_ft_ready_i                  : in std_logic;
	tx_ft_ready_o                  : out std_logic;
	tx_tvalid_flag_o               : out std_logic;
	INtr_B_calls_o                 : out std_logic;
	bcall_run_i                    : in std_logic;
	bcall_reg_i                    : in std_logic_vector(31 downto 0);
  RD_on_o                        : out std_logic;
  WR_on_o                        : out std_logic;
  FT_on_i                        : in std_logic;
  FT_clr_o                       : out std_logic;
	wr_busy_eb_i                   : in std_logic;
	one_us_heartbeat_i             : in std_logic;

  soft_wd_o                      : out std_logic;
  soft_reset_o                   : out std_logic;
  status_led_o                   : out std_logic_vector(1 downto 0);
  ebone_reset_o                  : out std_logic;

  err_drop_count                 : in std_logic_vector(14 downto 0);
	err_drop_ovfl                  : in std_logic;
	tdst_rdy_count                 : in std_logic_vector(14 downto 0);
	tdst_rdy_ovfl                  : in std_logic;
	req_len_sup_o                  : out std_logic;
	req_len_sup_count              : in std_logic_vector(15 downto 0);
	req_rx_valid_o                 : out std_logic;
	req_rx_valid_count             : in std_logic_vector(31 downto 0);
	req_tx_valid_o                 : out std_logic;
	req_tx_valid_count             : in std_logic_vector(31 downto 0);

-- Application extension signals
  ext_ctrl_src_i                 : in std_logic;
  ext_intr_src_i                 : in std_logic;
	ext_addr_src_i                 : in std_logic_vector(63 downto 0);
	ext_data_src_i                 : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);
	ext_data_flg_i                 : in std_logic;
	ext_data_ack_o                 : out std_logic;

  mm_probe_o                     : out std_logic_vector(3 downto 0);
  rx_probe_o                     : out std_logic_vector(3 downto 0);
  tx_probe_o                     : out std_logic_vector(3 downto 0);
  it_probe_o                     : out std_logic_vector(3 downto 0);
  prob_mux_o                     : out std_logic_vector(19 downto 0);

  my_payload_size_o              : out std_logic_vector(2 downto 0);
  user_cnst_i                    : in std_logic_vector(15 downto 0);
  tx_ft_delay_o                  : out std_logic;
  tlp_delayed_o                  : out std_logic
);
end ep_to_fnct;

architecture rtl of ep_to_fnct is

-- Local signals
signal brd_addr             : std_logic_vector(29 downto 0);
signal brd_be               : std_logic_vector(3 downto 0);
signal int_brd_data         : std32_x32;

signal bwr_addr             : std_logic_vector(29 downto 0);
signal bwr_be               : std_logic_vector(7 downto 0);
signal bwr_data             : std32_x32;
signal wr_en                : std_logic;
signal wr_busy_ep           : std_logic;
signal breq_len             : std_logic_vector(9 downto 0);

signal req_compl            : std_logic;
signal req_compl_wd         : std_logic;
signal compl_done           : std_logic;

signal E_Bone_trn           : std_logic;
signal Type_Offset_desc     : std_logic_vector(31 downto 0);
signal requester_leng       : std_logic_vector(9 downto 0);
signal requester_fmt        : std_logic;
signal RD_on                : std_logic;
signal WR_on                : std_logic;
signal s_axis_tx_tvalid_int : std_logic;

signal req_tc               : std_logic_vector(2 downto 0);
signal req_td               : std_logic;
signal req_ep               : std_logic;
signal req_attr             : std_logic_vector(1 downto 0);
signal req_len              : std_logic_vector(9 downto 0);
signal req_rid              : std_logic_vector(15 downto 0);
signal req_tag              : std_logic_vector(7 downto 0);
signal req_be               : std_logic_vector(7 downto 0);
signal req_addr             : std_logic_vector(31 downto 0);

signal cfg_read_req         : std_logic;
signal cfgs_regs            : std32_x32;

signal INtr_service         : std_logic;
signal INtr_disable         : std_logic;
signal INtr_enable          : std_logic;
signal INtr_TLP_end         : std_logic;
signal INtr_B_calls         : std_logic;
signal INtr_Rqst_nmb        : std_logic_vector(7 downto 0);
signal Source_Rqst          : std_logic;
signal B_calls_Rqst         : std_logic;

signal tlp_tx_addr          : std_logic_vector(AXI_DWIDTH - 1 downto 0);

signal msg_signaling_s      : std_logic;
signal msg_code_s           : std_logic_vector(7 downto 0);
signal msg_data_lsb_s       : std_logic_vector(31 downto 0);
signal msg_data_msb_s       : std_logic_vector(31 downto 0);

begin

bwr_be_o           <= bwr_be;
bwr_data_o         <= bwr_data;
breq_len_o         <= breq_len;
req_compl_o        <= req_compl;
compl_done_o       <= compl_done;
INtr_B_calls_o     <= INtr_B_calls;
E_Bone_trn_o       <= E_Bone_trn;
Type_Offset_desc_o <= Type_Offset_desc;
RD_on_o            <= RD_on;
WR_on_o            <= WR_on;
s_axis_tx_tvalid   <= s_axis_tx_tvalid_int;

--------------------------------------------------------------------------------------------------------------
EP_CFG : ep_cfg_rd                                                   -- CFG Header read process implementation
--------------------------------------------------------------------------------------------------------------
port map (
  clk                      => clk,                                   -- I
	trn_rst                  => trn_rst,                               -- I

	cfg_do                   => cfg_do,                                -- I [31:0]
	cfg_rd_wr_done           => cfg_rd_wr_done,                        -- I
  cfg_di                   => cfg_di,                                -- O [31:0]
  cfg_byte_en              => cfg_byte_en,                           -- O [3:0]
  cfg_dwaddr               => cfg_dwaddr,                            -- O [9:0]
  cfg_wr_en                => cfg_wr_en,                             -- O
  cfg_rd_en                => cfg_rd_en,                             -- O

	cfg_read_req_i           => cfg_read_req,                          -- I
	cfgs_regs_o              => cfgs_regs                              -- O [32][31:0]
);

-----------------------------------------------------------------------------------------------------------
EP_INT : ep_interrupt                                                -- interrupt processing implementation
-----------------------------------------------------------------------------------------------------------
generic map (
  BC_PERIOD                => BC_PERIOD                              -- Broad-Call period (0:1us, 1:10us, 2:100us, 3:1ms)
)

port map (
  clk                      => clk,                                   -- I
	trn_rst                  => trn_rst,                               -- I

  cfg_interrupt            => cfg_interrupt,                         -- O
  cfg_interrupt_rdy        => cfg_interrupt_rdy,                     -- I
  cfg_interrupt_assert     => cfg_interrupt_assert,                  -- O
  cfg_interrupt_di         => cfg_interrupt_di,                      -- O [7:0]
  cfg_interrupt_do         => cfg_interrupt_do,                      -- I [7:0]
  cfg_interrupt_mmenable   => cfg_interrupt_mmenable,                -- I [2:0]
  cfg_interrupt_msienable  => cfg_interrupt_msienable,               -- I
  cfg_interrupt_msixenable => cfg_interrupt_msixenable,              -- I
  cfg_interrupt_msixfm     => cfg_interrupt_msixfm,                  -- I

  INtr_service_i           => INtr_service,                          -- I
	INtr_disable_i           => INtr_disable,                          -- I
	INtr_enable_i            => INtr_enable,                           -- I
	INtr_TLP_end_i           => INtr_TLP_end,                          -- I
	INtr_B_calls_i           => INtr_B_calls,                          -- I
	INtr_Rqst_nmb_i          => INtr_Rqst_nmb,                         -- I [7:0]
	ext_intr_src_i           => ext_intr_src_i,                        -- I
  INtr_gate_o              => INtr_gate_o,                           -- O

  TLP_end_Rqst_i           => TLP_end_Rqst_i,                        -- I
  B_calls_Rqst_i           => B_calls_Rqst,                          -- I

  one_us_heartbeat_i       => one_us_heartbeat_i,                    -- I
  it_probe_o               => it_probe_o                             -- O [3:0]
);

--------------------------------------------------------------------------------------------------------------------
EP_MEM : ep_mem_access                                               -- memory aperture implemented in FPGA BlockRAM
--------------------------------------------------------------------------------------------------------------------
generic map (
  BADR0_MEM                      => BADR0_MEM,
	WATCH_DOG                      => WATCH_DOG,
	AXI_DWIDTH                     => AXI_DWIDTH,
	TRGT_DESC                      => TRGT_DESC,
	TYPE_DESC                      => TYPE_DESC,
	HARD_VERS                      => HARD_VERS,
	DEVICE_ID                      => DEVICE_ID
)

port map (
  clk                            => clk,                             -- I
  trn_rst                        => trn_rst,                         -- I
  trn_lnk_up_n                   => trn_lnk_up_n,                    -- I

-- Read Port
  brd_addr_i                     => brd_addr,                        -- I [29:0]
  brd_be_i                       => brd_be,                          -- I [3:0]
  int_brd_data_o                 => int_brd_data,                    -- O [32][31:0]

-- Write Port
  bwr_addr_i                     => bwr_addr,                        -- I [29:0]
  bwr_be_i                       => bwr_be,                          -- I [7:0]
  bwr_data_i                     => bwr_data,                        -- I [32][31:0]
  wr_en_i                        => wr_en,                           -- I
	breq_len_i                     => breq_len,                        -- I [9:0]
  wr_busy_ep_o                   => wr_busy_ep,                      -- O

  cfg_bus_number                 => cfg_bus_number,                  -- I [7:0]
  cfg_device_number              => cfg_device_number,               -- I [4:0]
  cfg_function_number            => cfg_function_number,             -- I [2:0]
  cfg_status                     => cfg_status,                      -- I [15:0]
  cfg_command                    => cfg_command,                     -- I [15:0]
  cfg_dstatus                    => cfg_dstatus,                     -- I [15:0]
  cfg_dcommand                   => cfg_dcommand,                    -- I [15:0]
	cfg_dcommand2                  => cfg_dcommand2,                   -- I [15:0]
  cfg_lstatus                    => cfg_lstatus,                     -- I [15:0]
  cfg_lcommand                   => cfg_lcommand,                    -- I [15:0]

  cfg_interrupt_do               => cfg_interrupt_do,                -- I [7:0]
  cfg_interrupt_mmenable         => cfg_interrupt_mmenable,          -- I [2:0]
  cfg_interrupt_msienable        => cfg_interrupt_msienable,         -- I
  cfg_interrupt_msixenable       => cfg_interrupt_msixenable,        -- I
  cfg_interrupt_msixfm           => cfg_interrupt_msixfm,            -- I

  pl_initial_link_width          => pl_initial_link_width,           -- I [2:0]
  pl_lane_reversal_mode          => pl_lane_reversal_mode,           -- I [1:0]
  pl_link_gen2_capable           => pl_link_gen2_capable,            -- I
  pl_link_partner_gen2_supported => pl_link_partner_gen2_supported,  -- I
  pl_link_upcfg_capable          => pl_link_upcfg_capable,           -- I
  pl_ltssm_state                 => pl_ltssm_state,                  -- I [5:0]
  pl_received_hot_rst            => pl_received_hot_rst,             -- I
  pl_sel_link_rate               => pl_sel_link_rate,                -- I
  pl_sel_link_width              => pl_sel_link_width,               -- I [1:0]
	pl_phy_lnk_up                  => pl_phy_lnk_up,                   -- I
  pl_tx_pm_state                 => pl_tx_pm_state,                  -- I [2:0]
  pl_rx_pm_state                 => pl_rx_pm_state,                  -- I [1:0]
  pl_directed_change_done        => pl_directed_change_done,         -- I
  pl_directed_link_auton         => pl_directed_link_auton,          -- O
  pl_directed_link_change        => pl_directed_link_change,         -- O [1:0]
  pl_directed_link_speed         => pl_directed_link_speed,          -- O
  pl_directed_link_width         => pl_directed_link_width,          -- O [1:0]
  pl_upstream_prefer_deemph      => pl_upstream_prefer_deemph,       -- O

  cfg_read_req_o                 => cfg_read_req,                    -- O
	cfgs_regs_i                    => cfgs_regs,                       -- I

  INtr_service_o                 => INtr_service,                    -- O
	INtr_disable_o                 => INtr_disable,                    -- O
	INtr_enable_o                  => INtr_enable,                     -- O
	INtr_TLP_end_o                 => INtr_TLP_end,                    -- O
	INtr_B_calls_o                 => INtr_B_calls,                    -- O
	INtr_Rqst_nmb_o                => INtr_Rqst_nmb,                   -- O [7:0]

  msg_signaling_i                => msg_signaling_s,                 -- I
	msg_code_i                     => msg_code_s,                      -- I [7:0]
	msg_data_lsb_i                 => msg_data_lsb_s,                  -- I [31:0]
	msg_data_msb_i                 => msg_data_msb_s,                  -- I [31:0]

  requester_fmt_o                => requester_fmt,                   -- O
	requester_leng_o               => requester_leng,                  -- O [9:0]

  TLP_end_Rqst_i                 => TLP_end_Rqst_i,                  -- I
	bcall_reg_i                    => bcall_reg_i,                     -- I [31:0]
	B_calls_Rqst_o                 => B_calls_Rqst,                    -- O
	tlp_tx_addr                    => tlp_tx_addr,                     -- I [AXI_DWIDTH - 1:0]

  FT_on_i                        => FT_on_i,                         -- I
  soft_wd_o                      => soft_wd_o,                       -- O
  soft_reset_o                   => soft_reset_o,                    -- O
  status_led_o                   => status_led_o,                    -- O [1:0]
  ebone_reset_o                  => ebone_reset_o,                   -- O
	bcall_run_i                    => bcall_run_i,                     -- I

  err_drop_count                 => err_drop_count,                  -- I [14:0]
	err_drop_ovfl                  => err_drop_ovfl,                   -- I
	tdst_rdy_count                 => tdst_rdy_count,                  -- I [14:0]
	tdst_rdy_ovfl                  => tdst_rdy_ovfl,                   -- I
	req_len_sup_count              => req_len_sup_count,               -- I [15:0]
	req_rx_valid_count             => req_rx_valid_count,              -- I [31:0]
	req_tx_valid_count             => req_tx_valid_count,              -- I [31:0]
  ext_ctrl_src_i                 => ext_ctrl_src_i,                  -- I

  mm_probe_o                     => mm_probe_o,                      -- O [3:0]
  prob_mux_o                     => prob_mux_o,                      -- O [19:0]

  my_payload_size_o              => my_payload_size_o,               -- O [2:0]
  user_cnst_i                    => user_cnst_i                      -- I [15:0]
);

--------------------------------------------------------------------------------------------------------------------
EP_RX : ep_rx_engine                                                 -- Local-Link Receive controller implementation
--------------------------------------------------------------------------------------------------------------------
generic map (
  EBFT_DWIDTH              => EBFT_DWIDTH
)

port map (
	clk                      => clk,                                   -- I
	trn_rst                  => trn_rst,                               -- I

-- LocalLink Rx
  m_axis_rx_tdata          => m_axis_rx_tdata,                       -- I [EBFT_DWIDTH - 1:0]
  m_axis_rx_tkeep          => m_axis_rx_tkeep,                       -- I [EBFT_DWIDTH / 8 - 1:0]
  m_axis_rx_tlast          => m_axis_rx_tlast,                       -- I
  m_axis_rx_tvalid         => m_axis_rx_tvalid,                      -- I
  m_axis_rx_tready         => m_axis_rx_tready,                      -- O
  m_axis_rx_tuser          => m_axis_rx_tuser,                       -- I [21:0]
	s_axis_tx_tvalid         => s_axis_tx_tvalid_int,                  -- I

  npck_prsnt_o             => npck_prsnt_o,                          -- O

-- Handshake with Tx engine
	req_compl_o              => req_compl,                             -- O
	req_compl_wd_o           => req_compl_wd,                          -- O
	compl_done_i             => compl_done,                            -- I
	tx_tvalid_flag_o         => tx_tvalid_flag_o,                      -- O

	req_tc_o                 => req_tc,                                -- O [2:0]
	req_td_o                 => req_td,                                -- O
	req_ep_o                 => req_ep,                                -- O
	req_attr_o               => req_attr,                              -- O [1:0]
	req_len_o                => req_len,                               -- O [9:0]
	req_rid_o                => req_rid,                               -- O [15:0]
	req_tag_o                => req_tag,                               -- O [7:0]
	req_be_o                 => req_be,                                -- O [7:0]
	req_addr_o               => req_addr,                              -- O [31:0]
  req_len_sup_o            => req_len_sup_o,                         -- O
	req_rx_valid_o           => req_rx_valid_o,                        -- O

-- Memory Write Port
	brd_strt_o               => brd_strt_o,                            -- O
	bwr_strt_o               => bwr_strt_o,                            -- O
	bwr_addr_o               => bwr_addr,                              -- O [29:0]
	bwr_be_o                 => bwr_be,                                -- O [7:0]
	bwr_data_o               => bwr_data,                              -- O [32][31:0]
	breq_len_o               => breq_len,                              -- O [9:0]

	wr_en_o                  => wr_en,                                 -- O
	wr_busy_ep_i             => wr_busy_ep,                            -- I
  wr_busy_eb_i             => wr_busy_eb_i,                          -- I
	bcall_run_i              => bcall_run_i,                           -- I
	FT_on_i                  => FT_on_i,                               -- I
	RD_on_o                  => RD_on,                                 -- O
	WR_on_o                  => WR_on,                                 -- O
  eb_bft_i                 => eb_bft_i,                              -- I
  eb_eof_i                 => eb_eof_i,                              -- I

	E_Bone_trn_o             => E_Bone_trn,                            -- O
	Type_Offset_desc_o       => Type_Offset_desc,                      -- O [31:0]

	msg_signaling_o          => msg_signaling_s,                       -- O
	msg_code_o               => msg_code_s,                            -- O [7:0]
	msg_data_lsb_o           => msg_data_lsb_s,                        -- O [31:0]
	msg_data_msb_o           => msg_data_msb_s,                        -- O [31:0]

  rx_probe_o               => rx_probe_o                             -- O [3:0]
);

---------------------------------------------------------------------------------------------------------------------
EP_TX : ep_tx_engine                                                 -- Local-Link Transmit controller implementation
---------------------------------------------------------------------------------------------------------------------
generic map (
  EBFT_DWIDTH              => EBFT_DWIDTH
)

port map (
  clk                      => clk,                                   -- I
  trn_rst                  => trn_rst,                               -- I

-- LocalLink Tx
  tx_buf_av                => tx_buf_av,                             -- I [5:0]
  tx_cfg_req               => tx_cfg_req,                            -- I
  tx_err_drop              => tx_err_drop,                           -- I
  s_axis_tx_tready         => s_axis_tx_tready,                      -- I
  s_axis_tx_tdata          => s_axis_tx_tdata,                       -- O [EBFT_DWIDTH - 1:0]
  s_axis_tx_tkeep          => s_axis_tx_tkeep,                       -- O [EBFT_DWIDTH / 8 - 1:0]
  s_axis_tx_tlast          => s_axis_tx_tlast,                       -- O
  s_axis_tx_tvalid         => s_axis_tx_tvalid_int,                  -- O
  s_axis_tx_tuser          => s_axis_tx_tuser,                       -- O [3:0]

-- Handshake with Rx engine
  req_compl_i              => req_compl,                             -- I
  req_compl_wd_i           => req_compl_wd,                          -- I
  compl_done_o             => compl_done,                            -- O

  req_tc_i                 => req_tc,                                -- I [2:0]
  req_td_i                 => req_td,                                -- I
  req_ep_i                 => req_ep,                                -- I
  req_attr_i               => req_attr,                              -- I [1:0]
  req_len_i                => req_len,                               -- I [9:0]
  req_rid_i                => req_rid,                               -- I [15:0]
  req_tag_i                => req_tag,                               -- I [7:0]
  req_be_i                 => req_be,                                -- I [7:0]
  req_addr_i               => req_addr,                              -- I [31:0]
	req_tx_valid_o           => req_tx_valid_o,                        -- O

-- Read Port
  brd_addr_o               => brd_addr,                              -- O [29:0]
  brd_be_o                 => brd_be,                                -- O [3:0]

  int_brd_data_i           => int_brd_data,                          -- I [32][31:0]
  ext_brd_data_i           => ext_brd_data_i,                        -- I [32][31:0]
  ext_brd_done_i           => ext_brd_done_i,                        -- I

  completer_id_i           => cfg_completer_id,                      -- I [15:0]
  cfg_bus_mstr_enable_i    => cfg_bus_mstr_enable,                   -- I
	cfg_dcommand_i           => cfg_dcommand,                          -- I [15:0]

  E_Bone_trn_i             => E_Bone_trn,                            -- I
  tx_ft_ready_i            => tx_ft_ready_i,                         -- I
	tx_ft_ready_o            => tx_ft_ready_o,                         -- O
  requester_fmt_i          => requester_fmt,                         -- I
  requester_leng_i         => requester_leng,                        -- I [9:0]
  eb_eof_i                 => eb_eof_i,                              -- I
  eb_ft_data_i             => eb_ft_data_i,                          -- I [EBFT_DWIDTH - 1:0]
  eb_ft_addr_i             => eb_ft_addr_i,                          -- I [63:0]
	eb_data_size_i           => eb_data_size_i,                        -- I [7:0]
	eb_last_size_i           => eb_last_size_i,                        -- I [7:0]
  eb_byte_size_i           => eb_byte_size_i,                        -- I [15:0]
	TLP_end_Rqst_i           => TLP_end_Rqst_i,                        -- I
  FT_on_i                  => FT_on_i,                               -- I
  FT_clr_o                 => FT_clr_o,                              -- O
	tlp_tx_addr              => tlp_tx_addr,                           -- I [AXI_DWIDTH - 1:0]
	tx_ft_delay_o            => tx_ft_delay_o,                         -- O
  tlp_delayed_o            => tlp_delayed_o,                         -- O

  ext_addr_src_i           => ext_addr_src_i,                        -- I [63:0]
  ext_data_src_i           => ext_data_src_i,                        -- I [EBFT_DWIDTH - 1:0);
	ext_data_flg_i           => ext_data_flg_i,                        -- I
	ext_data_ack_o           => ext_data_ack_o,                        -- O

  tx_probe_o               => tx_probe_o                             -- O [3:0]
);

end rtl;
