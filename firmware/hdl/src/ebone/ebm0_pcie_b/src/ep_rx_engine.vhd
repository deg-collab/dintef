----------------------------------------------------------------------------------
--
-- Project    : Series 7 Integrated Block for PCI Express
-- File       : ep_rx_engine.vhd
-- Description: Local-Link Receive Unit.
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ep_comp_pkg.all;
use work.ebm0_pcie_b_pkg.all;

entity ep_rx_engine is

generic (
	EBFT_DWIDTH           : NATURAL := 32                                    -- FT data width
);
port (
  clk                   : in std_logic;                                    -- User clock from the endpoint module
  trn_rst               : in std_logic;                                    -- Global active high reset signal

  m_axis_rx_tdata       : in std_logic_vector((EBFT_DWIDTH - 1) downto 0);       -- Receive Data: Packet data being received
  m_axis_rx_tkeep       : in std_logic_vector((EBFT_DWIDTH / 8 - 1) downto 0);   -- Receive Data Strobe: Determines which data bytes are valid on m_axis_rx_tdata
  m_axis_rx_tlast       : in std_logic;                                    -- Receive End-of-Frame (EOF): Signals the end of a packet
  m_axis_rx_tvalid      : in std_logic;                                    -- Receive Source Ready: Indicates that the core is presenting valid data on m_axis_rx_tdata
  m_axis_rx_tready      : out std_logic;                                   -- Receive Destination Ready: Indicates that the User Application is ready to accept data on m_axis_rx_tdata
  m_axis_rx_tuser       : in std_logic_vector(21 downto 0);                -- Receive Packet Status
	s_axis_tx_tvalid      : in std_logic;                                    -- Transmit Source Ready: Indicates that the User Application is presenting valid data on s_axis_tx_tdata

  npck_prsnt_o          : out std_logic;                                   -- Indicates the start of a new packet header in m_axis_rx_tdata
  req_compl_o           : out std_logic;                                   -- Indicates completion required
  req_compl_wd_o        : out std_logic;                                   -- asserted indicates to generate a completion WITH data
  compl_done_i          : in  std_logic;                                   -- Completion acknowledge
	tx_tvalid_flag_o      : out std_logic;                                   -- Transmit data valid flag

  req_tc_o              : out std_logic_vector(2 downto 0);                -- Header field TC (Traffic Class)
  req_td_o              : out std_logic;                                   -- Header field TD (TLP Digest present)
  req_ep_o              : out std_logic;                                   -- Header field EP (TLP poisoned)
  req_attr_o            : out std_logic_vector(1 downto 0);                -- Header field Attributes
  req_len_o             : out std_logic_vector(9 downto 0);                -- Header field Data Length (Data Payload in DW)
  req_rid_o             : out std_logic_vector(15 downto 0);               -- Header field Requestor ID
  req_tag_o             : out std_logic_vector(7 downto 0);                -- Header field Tag
  req_be_o              : out std_logic_vector(7 downto 0);                -- Header field Byte Enables
  req_addr_o            : out std_logic_vector(31 downto 0);               -- Header field Address
	req_len_sup_o         : out std_logic;                                   -- High for one system clock period when req_len > 1
	req_rx_valid_o        : out std_logic;                                   -- High for one system clock period when RX_MEM_RD32_FMT_TYPE

  brd_strt_o            : out std_logic;                                   -- Memory Read Burst start
  bwr_strt_o            : out std_logic;                                   -- Memory Write Burst start
  bwr_addr_o            : out std_logic_vector(29 downto 0);               -- Memory Write Address
  bwr_be_o              : out std_logic_vector(7 downto 0);                -- Memory Write Byte Enable
  bwr_data_o            : out std32_x32;                                   -- Memory Write Data
  breq_len_o            : out std_logic_vector(9 downto 0);                -- Memory Write payload data length in DW

  wr_en_o               : out std_logic;                                   -- Memory Write Enable
  wr_busy_ep_i          : in std_logic;                                    -- Memory Write Busy
	wr_busy_eb_i          : in std_logic;                                    -- E_Bone write cycle end when low
	bcall_run_i           : in std_logic;                                    -- Broad-call process running
	FT_on_i               : in std_logic;                                    -- FT has started when 1
	RD_on_o               : out std_logic;                                   -- MRd TLP running
	WR_on_o               : out std_logic;                                   -- MWr TLP running
	eb_bft_i              : in std_logic;                                    -- busy FT
  eb_eof_i              : in std_logic;                                    -- FT slave end of frame

  E_Bone_trn_o          : out std_logic;                                   -- E_Bone external transaction request
  Type_Offset_desc_o    : out std_logic_vector(31 downto 0);               -- Type/Offset descriptor word for E-Bone interfacing

  msg_signaling_o       : out std_logic;                                   -- TLP messages received
	msg_code_o            : out std_logic_vector(7 downto 0);                -- Message Code
	msg_data_lsb_o        : out std_logic_vector(31 downto 0);               -- First and
	msg_data_msb_o        : out std_logic_vector(31 downto 0);               -- Second dwords of message write TLP

  rx_probe_o            : out std_logic_vector(3 downto 0)                 -- Probe output signals linked to Rx engine
);

end ep_rx_engine;

architecture ep_rx_rtl of ep_rx_engine is

-- common declarations
----------------------

alias trn_rbar_hit            : std_logic_vector(6 downto 0) is m_axis_rx_tuser(8 downto 2);

constant RX_MEM_RD32_FMT_TYPE : std_logic_vector(6 downto 0) := "0000000"; -- MRd: 32-bit Address Memory Read Request
constant RX_MEM_WR32_FMT_TYPE : std_logic_vector(6 downto 0) := "1000000"; -- MWr: 32-bit Address Memory Write Request
constant RX_MEM_RD64_FMT_TYPE : std_logic_vector(6 downto 0) := "0100000"; -- MRd: 64-bit Address Memory Read Request
constant RX_MEM_WR64_FMT_TYPE : std_logic_vector(6 downto 0) := "1100000"; -- MWr: 64-bit Address Memory Write Request
constant RX_IO_RD32_FMT_TYPE  : std_logic_vector(6 downto 0) := "0000010"; -- IORd: IO Read Request
constant RX_IO_WR32_FMT_TYPE  : std_logic_vector(6 downto 0) := "1000010"; -- IOWr: IO Write Request
constant RX_MSGD_ID_FMT_TYPE  : std_logic_vector(6 downto 0) := "1110010"; -- MsgD: Message Request with data, Routed by ID
constant RX_MSGD_RC_FMT_TYPE  : std_logic_vector(6 downto 0) := "1110011"; -- MsgD: Message Request with data, from Root Cplx
constant RX_MSGD_LC_FMT_TYPE  : std_logic_vector(6 downto 0) := "1110100"; -- MsgD: Message Request with data, local
constant RX_MSG_ID_FMT_TYPE   : std_logic_vector(6 downto 0) := "0110010"; -- MsgD: Message Request without data, Routed by ID
constant RX_MSG_RC_FMT_TYPE   : std_logic_vector(6 downto 0) := "0110011"; -- MsgD: Message Request without data, from Root Cplx
constant RX_MSG_LC_FMT_TYPE   : std_logic_vector(6 downto 0) := "0110100"; -- MsgD: Message Request without data, local
constant RX_CPL_NODT_FMT_TYPE : std_logic_vector(6 downto 0) := "0001010"; -- Cpl:  Completion without data
constant RX_CPL_RD32_FMT_TYPE : std_logic_vector(6 downto 0) := "1001010"; -- CplD: Completion with data

signal DW_wr_Cnt              : std_logic_vector(9 downto 0);              -- DW (Addr/Data) valid and DW counter
signal DW_wr_Cnt_cl           : std_logic;                                 -- DW (Addr/Data) valid and DW count clear
signal DW_wr_Cnt_en           : std_logic;                                 -- DW (Addr/Data) valid and DW count enable
signal DW_wr_Cnt_tc           : std_logic;                                 -- DW (Addr/Data) valid and DW terminal count

signal RD_on                  : std_logic;
signal WR_on                  : std_logic;

signal tlp_type               : std_logic_vector(6 downto 0);
signal m_axis_rx_tready_int   : std_logic;
signal tx_tvalid_flag         : std_logic;
signal region_select          : std_logic_vector(1 downto 0);
signal seg_desc_fmt           : std_logic_vector(1 downto 0);              -- Encoded 2-bit segment base address
signal E_Bone_trn             : std_logic;
signal E_Bone_trn_l           : std_logic;
signal req_compl              : std_logic;
signal req_compl_wd           : std_logic;
signal req_len                : std_logic_vector(9 downto 0);

begin

-- common implementation
------------------------

m_axis_rx_tready <= m_axis_rx_tready_int;
req_len_o        <= req_len;
RD_on_o          <= RD_on;
WR_on_o          <= WR_on;
E_Bone_trn_o     <= E_Bone_trn_l;
tx_tvalid_flag_o <= tx_tvalid_flag;

process (trn_rbar_hit)
begin
  case (trn_rbar_hit) is

    when "0000001" =>                                                      -- BADR0 4Kbytes MWr/MRd TLPs access
      region_select <= "00";
      seg_desc_fmt  <= "00";
      E_Bone_trn    <= '0';

    when "0000010" =>                                                      -- BADR1 64Kbytes MWr/MRd TLPs access
      region_select <= "01";
      seg_desc_fmt  <= "01";
      E_Bone_trn    <= '1';

    when "0000100" =>                                                      -- BADR2 64Kbytes MWr/MRd TLPs access
      region_select <= "10";
      seg_desc_fmt  <= "10";
      E_Bone_trn    <= '1';

    when "0001000" =>                                                      -- BADR3 256Kbytes MWr/MRd TLPs access
      region_select <= "11";
      seg_desc_fmt  <= "11";
      E_Bone_trn    <= '1';

    when others =>
      region_select <= "00";
      seg_desc_fmt  <= "00";
      E_Bone_trn    <= '0';

  end  case;
end process;

process (clk, DW_wr_Cnt_cl)
begin
  if (DW_wr_Cnt_cl = '1') then
    DW_wr_Cnt <= (others => '0');
	elsif (clk'event and clk = '1') then
    if ((DW_wr_Cnt_en = '1') and (m_axis_rx_tvalid = '1')) then
      DW_wr_Cnt <= DW_wr_Cnt + 1;
    end if;
	end if;
end process;

-------------------------------------------------------------------------------------------------------------
EP_RX_32B : if (EBFT_DWIDTH = 32) generate                                 -- ep_32b_rx_engine implementation
-------------------------------------------------------------------------------------------------------------

  type rx_state_type  is (RX_RST_STATE,
                          RX_MEM_RD32_DW1,
												  RX_MEM_RD32_DW2,
												  RX_IO_MEM_WR32_DW1,
												  RX_IO_MEM_WR32_DW2,
												  RX_IO_MEM_WR32_DW3,
												  RX_IO_MEM_WR32_DW4,
													RX_IO_MEM_WR_WAIT_STATE,
												  RX_MEM_RD64_DW1,
												  RX_MEM_RD64_DW2,
												  RX_MEM_RD64_DW3,
												  RX_MEM_WR64_DW1,
												  RX_MEM_WR64_DW2,
												  RX_MEM_WR64_DW3,
												  RX_MEM_WR64_DW4,
												  RX_MSGD_DW0,
												  RX_MSGD_DW1,
												  RX_MSGD_DW2,
												  RX_MSGD_DW3,
												  RX_MSG_SIGNALING,
												  RX_WAIT_STATE);
  signal rx_state     : rx_state_type;
  signal rx_in_packet : std_logic;
  signal rx_sop       : std_logic;

  begin

    rx_probe_o(0) <= rx_sop;            -- 4
    rx_probe_o(1) <= m_axis_rx_tvalid;  -- 5
    rx_probe_o(2) <= WR_on;             -- 6
    rx_probe_o(3) <= RD_on;             -- 7

    DW_wr_Cnt_cl <= '1' when ((trn_rst = '1') or (rx_sop = '1')) else '0';
    DW_wr_Cnt_tc <= '0';

    rx_sop <= m_axis_rx_tvalid and (not rx_in_packet);
    npck_prsnt_o <= m_axis_rx_tvalid and (not rx_in_packet);

    process begin
      wait until rising_edge(clk);
        if (trn_rst = '1') then
          rx_in_packet <= '0';
        elsif ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1') and (m_axis_rx_tlast = '1')) then
          rx_in_packet <= '0';
        elsif ((rx_sop = '1') and (m_axis_rx_tready_int = '1')) then
          rx_in_packet <= '1';
        end if;
    end process;

    process begin
      wait until rising_edge(clk);
        req_compl_o    <= req_compl;
        req_compl_wd_o <= req_compl_wd;
    end process;

    process
    begin

      wait until rising_edge(clk);

      if (trn_rst = '1') then

        m_axis_rx_tready_int <= '0';

        req_compl            <= '0';
        req_compl_wd         <= '1';

        req_tc_o             <= (others => '0');
        req_td_o             <= '0';
        req_ep_o             <= '0';
        req_attr_o           <= (others => '0');
        req_rid_o            <= (others => '0');
        req_tag_o            <= (others => '0');
        req_be_o             <= (others => '0');
        req_addr_o           <= (others => '0');
		    req_len              <= (others => '0');
				req_len_sup_o        <= '0';

        bwr_be_o             <= (others => '0');
        bwr_addr_o           <= (others => '0');
		    breq_len_o           <= (others => '0');
		    brd_strt_o           <= '0';
		    bwr_strt_o           <= '0';
        wr_en_o              <= '0';
			  tlp_type             <= (others => '0');
		    DW_wr_Cnt_en         <= '0';
				tx_tvalid_flag       <= '0';
				req_rx_valid_o       <= '0';

		    RD_on                <= '0';
        WR_on                <= '0';

        for i in 0 to 31 loop
          bwr_data_o(i) <= (others => '0');
        end loop;

        E_Bone_trn_l         <= '0';
        Type_Offset_desc_o   <= (others => '0');

        msg_signaling_o      <= '0';
        msg_code_o           <= (others => '0');
        msg_data_lsb_o       <= (others => '0');
        msg_data_msb_o       <= (others => '0');

        rx_state             <= RX_RST_STATE;

      else

        wr_en_o        <= '0';
        req_compl_wd   <= '1';
				req_len_sup_o  <= '0';
				req_rx_valid_o <= '0';

        case (rx_state) is

          when RX_RST_STATE =>

            m_axis_rx_tready_int <= '1';
					  msg_signaling_o      <= '0';

            if ((rx_sop = '1') and (m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then  -- RX begin process

              tlp_type   <= m_axis_rx_tdata(30 downto 24);
              req_tc_o   <= m_axis_rx_tdata(22 downto 20);
              req_td_o   <= m_axis_rx_tdata(15);
              req_ep_o   <= m_axis_rx_tdata(14);
              req_attr_o <= m_axis_rx_tdata(13 downto 12);
              req_len    <= m_axis_rx_tdata(9 downto 0);
							breq_len_o <= m_axis_rx_tdata(9 downto 0);

              case (m_axis_rx_tdata(30 downto 24)) is

                when RX_MEM_RD32_FMT_TYPE =>                               -- MRd: 32-bit Address Memory Read Request

                  req_rx_valid_o <= '1';

                  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
									  RD_on        <= '1';
								    E_Bone_trn_l <= E_Bone_trn;
                    rx_state     <= RX_MEM_RD32_DW1;
                  else
									  req_len_sup_o <= '1';
                    rx_state      <= RX_RST_STATE;
                  end if;

                when RX_MEM_WR32_FMT_TYPE =>                               -- MWr: 32-bit Address Memory Write Request

                  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
									  WR_on        <= '1';
							      E_Bone_trn_l <= E_Bone_trn;
                    rx_state     <= RX_IO_MEM_WR32_DW1;
                  else
                    req_len_sup_o <= '1';
                    rx_state      <= RX_RST_STATE;
                  end if;

                when RX_MEM_RD64_FMT_TYPE =>                               -- MRd: 64-bit Address Memory Read Request

                  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
									  RD_on        <= '1';
								    E_Bone_trn_l <= E_Bone_trn;
                    rx_state     <= RX_MEM_RD64_DW1;
                  else
                    req_len_sup_o <= '1';
                    rx_state      <= RX_RST_STATE;
                  end if;

                when RX_MEM_WR64_FMT_TYPE =>                               -- MWr: 64-bit Address Memory Write Request

                  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
									  WR_on        <= '1';
							      E_Bone_trn_l <= E_Bone_trn;
                    rx_state     <= RX_MEM_WR64_DW1;
                  else
                    req_len_sup_o <= '1';
                    rx_state      <= RX_RST_STATE;
                  end if;

                when RX_IO_RD32_FMT_TYPE =>                                -- IORd: IO Read Request

                  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
									  RD_on        <= '1';
								    E_Bone_trn_l <= E_Bone_trn;
                    rx_state     <= RX_MEM_RD32_DW1;
                  else
                    req_len_sup_o <= '1';
                    rx_state      <= RX_RST_STATE;
                  end if;

                when RX_IO_WR32_FMT_TYPE =>                                -- IOWr: IO Write Request

                  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
									  WR_on        <= '1';
								    E_Bone_trn_l <= E_Bone_trn;
                    rx_state     <= RX_IO_MEM_WR32_DW1;
                  else
                    req_len_sup_o <= '1';
                    rx_state      <= RX_RST_STATE;
                  end if;

                when RX_MSGD_ID_FMT_TYPE =>                                -- MsgD: Message Request with data, Routed by ID

                  rx_state <= RX_MSGD_DW1;

                when RX_MSGD_RC_FMT_TYPE =>                                -- MsgD: Message Request with data, from Root Cplx

                  rx_state <= RX_MSGD_DW1;

                when RX_MSGD_LC_FMT_TYPE =>                                -- MsgD: Message Request with data, local

                  rx_state <= RX_MSGD_DW1;

                when RX_MSG_ID_FMT_TYPE =>                                 -- MsgD: Message Request without data, Routed by ID

                  rx_state <= RX_MSGD_DW0;

                when RX_MSG_RC_FMT_TYPE =>                                 -- MsgD: Message Request without data, from Root Cplx

                  rx_state <= RX_MSGD_DW0;

                when RX_MSG_LC_FMT_TYPE =>                                 -- MsgD: Message Request without data, local

                  rx_state <= RX_MSGD_DW0;

                when others =>                                             -- other TLPs

                  rx_state <= RX_RST_STATE;

              end case;

            else                                                           -- RX end process
              rx_state <= RX_RST_STATE;
            end if;

          when RX_MEM_RD32_DW1 =>                                          -- first dword of mem32 read TLP

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int ='1')) then
              req_rid_o  <= m_axis_rx_tdata(31 downto 16);
              req_tag_o  <= m_axis_rx_tdata(15 downto 8);
              req_be_o   <= m_axis_rx_tdata(7 downto 0);
							brd_strt_o <= '1';
              rx_state   <= RX_MEM_RD32_DW2;
            else
              rx_state <= RX_MEM_RD32_DW1;
            end if;

          when RX_MEM_RD32_DW2 =>                                           -- second dword of mem32 read TLP

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
              req_addr_o           <= region_select(1 downto 0) & m_axis_rx_tdata(29 downto 2) & "00";
              req_compl            <= '1';
              m_axis_rx_tready_int <= '0';
							Type_Offset_desc_o   <= '1' & '0' & seg_desc_fmt & m_axis_rx_tdata(29 downto 2);
              rx_state             <= RX_WAIT_STATE;
            else
              rx_state <= RX_MEM_RD32_DW2;
            end if;

          when RX_IO_MEM_WR32_DW1 =>                                        -- first dword of mem32 write TLP

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
              req_rid_o <= m_axis_rx_tdata(31 downto 16);
              req_tag_o <= m_axis_rx_tdata(15 downto 8);
							bwr_be_o  <= m_axis_rx_tdata(7 downto 0);
              rx_state  <= RX_IO_MEM_WR32_DW2;
            else
              rx_state <= RX_IO_MEM_WR32_DW1;
            end if;

          when RX_IO_MEM_WR32_DW2 =>                                        -- second dword of mem32 write TLP

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
						  bwr_addr_o         <= region_select(1 downto 0) & m_axis_rx_tdata(29 downto 2);
					    bwr_strt_o         <= '1';
              Type_Offset_desc_o <= '0' & '0' & seg_desc_fmt & m_axis_rx_tdata(29 downto 2);
              rx_state           <= RX_IO_MEM_WR32_DW3;
            else
              rx_state <= RX_IO_MEM_WR32_DW2;
            end if;

          when RX_IO_MEM_WR32_DW3 =>                                        -- third dword of mem32 write TLP

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					    if (tlp_type = RX_IO_WR32_FMT_TYPE) then
                req_compl <= '1';
              else
                req_compl <= '0';
              end if;
              req_compl_wd                                    <= '0';
						  m_axis_rx_tready_int                            <= '0';
              bwr_data_o(conv_integer(DW_wr_Cnt(4 downto 0))) <= m_axis_rx_tdata(31 downto 0);
							if (bcall_run_i = '0') then
                wr_en_o  <= '1';
							  rx_state <= RX_WAIT_STATE;
							else
							  rx_state <= RX_IO_MEM_WR_WAIT_STATE;
							end if;
            else
              rx_state <= RX_IO_MEM_WR32_DW3;
            end if;

          when RX_MEM_RD64_DW1 =>                                          -- first dword of mem64 read TLP

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int ='1')) then
              req_rid_o <= m_axis_rx_tdata(31 downto 16);
              req_tag_o <= m_axis_rx_tdata(15 downto 8);
              req_be_o  <= m_axis_rx_tdata(7 downto 0);
              rx_state  <= RX_MEM_RD64_DW2;
            else
              rx_state <= RX_MEM_RD64_DW1;
            end if;

          when RX_MEM_RD64_DW2 =>                                           -- second dword of mem64 read TLP

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
						  brd_strt_o <= '1';
              rx_state   <= RX_MEM_RD64_DW3;
            else
              rx_state <= RX_MEM_RD64_DW2;
            end if;

          when RX_MEM_RD64_DW3 =>                                           -- third dword of mem64 read TLP

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
              req_addr_o           <= region_select(1 downto 0) & m_axis_rx_tdata(29 downto 2) & "00";
              req_compl            <= '1';
              m_axis_rx_tready_int <= '0';
              Type_Offset_desc_o   <= '1' & '0' & seg_desc_fmt & m_axis_rx_tdata(29 downto 2);
              rx_state             <= RX_WAIT_STATE;
            else
              rx_state <= RX_MEM_RD64_DW3;
            end if;

          when RX_MEM_WR64_DW1 =>                                           -- first dword of mem64 write TLP

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
              bwr_be_o <= m_axis_rx_tdata(7 downto 0);
              rx_state <= RX_MEM_WR64_DW2;
            else
              rx_state <= RX_MEM_WR64_DW1;
            end if;

          when RX_MEM_WR64_DW2 =>                                           -- second dword of mem64 write TLP

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
              rx_state <= RX_MEM_WR64_DW3;
            else
              rx_state <= RX_MEM_WR64_DW2;
            end if;

          when RX_MEM_WR64_DW3 =>                                           -- third dword of mem64 write TLP

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
						  bwr_addr_o         <= region_select(1 downto 0) & m_axis_rx_tdata(29 downto 2);
					    bwr_strt_o         <= '1';
              Type_Offset_desc_o <= '0' & '0' & seg_desc_fmt & m_axis_rx_tdata(29 downto 2);
              rx_state           <= RX_MEM_WR64_DW4;
            else
              rx_state <= RX_MEM_WR64_DW3;
            end if;

          when RX_MEM_WR64_DW4 =>                                           -- fourth dword of mem64 write TLP

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
              m_axis_rx_tready_int                            <= '0';
              bwr_data_o(conv_integer(DW_wr_Cnt(4 downto 0))) <= m_axis_rx_tdata(31 downto 0);
							if (bcall_run_i = '0') then
                wr_en_o  <= '1';
							  rx_state <= RX_WAIT_STATE;
							else
							  rx_state <= RX_IO_MEM_WR_WAIT_STATE;
							end if;
            else
              rx_state <= RX_MEM_WR64_DW4;
            end if;

          when RX_MSGD_DW0 =>                                               -- first dword of message write tlp without data

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
              msg_code_o <= m_axis_rx_tdata(7 downto 0);
			        rx_state   <= RX_MSG_SIGNALING;
            else
              rx_state <= RX_MSGD_DW0;
            end if;

          when RX_MSGD_DW1 =>                                               -- first dword of message write tlp with data

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
              msg_code_o <= m_axis_rx_tdata(7 downto 0);
			        rx_state   <= RX_MSGD_DW2;
            else
              rx_state <= RX_MSGD_DW1;
            end if;

          when RX_MSGD_DW2 =>                                               -- second dword of message write tlp

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
              msg_data_msb_o <= m_axis_rx_tdata(31 downto 0);
			        rx_state       <= RX_MSGD_DW3;
            else
              rx_state <= RX_MSGD_DW2;
            end if;

          when RX_MSGD_DW3 =>                                               -- second dword of message write tlp

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
              msg_data_lsb_o <= m_axis_rx_tdata(31 downto 0);
			        rx_state       <= RX_MSG_SIGNALING;
            else
              rx_state <= RX_MSGD_DW3;
            end if;

          when RX_MSG_SIGNALING =>                                          -- start message processing

            msg_signaling_o <= '1';
            rx_state        <= RX_RST_STATE;

          when RX_IO_MEM_WR_WAIT_STATE =>

            if (bcall_run_i = '0') then
              wr_en_o  <= '1';
              rx_state <= RX_WAIT_STATE;
						else
						  rx_state <= RX_IO_MEM_WR_WAIT_STATE;
						end if;

          when RX_WAIT_STATE =>

            wr_en_o <= '0';

            if (FT_on_i = '0') then
              req_compl <= '0';
				    end if;

            if ((tlp_type = RX_MEM_WR32_FMT_TYPE) and
						    (((wr_busy_ep_i = '0') and (E_Bone_trn_l = '0')) or ((wr_busy_eb_i = '0') and (E_Bone_trn_l = '1')))) then

				      WR_on                <= '0';
              bwr_strt_o           <= '0';
              m_axis_rx_tready_int <= '1';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_IO_WR32_FMT_TYPE) and
						       (((wr_busy_ep_i = '0') and (E_Bone_trn_l = '0')) or ((wr_busy_eb_i = '0') and (E_Bone_trn_l = '1')))) then

				      WR_on                <= '0';
              bwr_strt_o           <= '0';
              m_axis_rx_tready_int <= '1';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_MEM_WR64_FMT_TYPE) and
						       (((wr_busy_ep_i = '0') and (E_Bone_trn_l = '0')) or ((wr_busy_eb_i = '0') and (E_Bone_trn_l = '1')))) then

				      WR_on                <= '0';
              bwr_strt_o           <= '0';
              m_axis_rx_tready_int <= '1';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_MEM_RD32_FMT_TYPE) and (compl_done_i = '1')) then

              req_addr_o           <= (others => '0');
					    brd_strt_o           <= '0';
				      RD_on                <= '0';
              m_axis_rx_tready_int <= '1';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_IO_RD32_FMT_TYPE) and (compl_done_i = '1')) then

				      req_addr_o           <= (others => '0');
					    brd_strt_o           <= '0';
				      RD_on                <= '0';
              m_axis_rx_tready_int <= '1';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_MEM_RD64_FMT_TYPE) and (compl_done_i = '1')) then

				      req_addr_o           <= (others => '0');
					    brd_strt_o           <= '0';
				      RD_on                <= '0';
              m_axis_rx_tready_int <= '1';
              rx_state             <= RX_RST_STATE;

            else
              rx_state <= RX_WAIT_STATE;
            end if;

          when others =>

            rx_state <= RX_RST_STATE;

        end case;

      end if;

    end process;

-------------------------------------------------------------------------------------------------------------
end generate EP_RX_32B;
-------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------
EP_RX_64B : if (EBFT_DWIDTH = 64) generate                                 -- ep_64b_rx_engine implementation
-------------------------------------------------------------------------------------------------------------

  type rx_state_type  is (RX_RST_STATE,
                          RX_MEM_RD32_DW1DW2,
                          RX_MEM_WR32_DW1DW2,
                          RX_IO_WR_DW1DW2,
                          RX_MEM_RD64_DW1DW2,
                          RX_MEM_WR64_DW1DW2,
                          RX_MEM_WR64_DW3,
                          RX_IO_MEM_WR_WAIT_STATE,
													RX_MSGD_DW1DW2,
                          RX_MSG_SIGNALING,
                          RX_WAIT_STATE);
  signal rx_state     : rx_state_type;
  signal rx_in_packet : std_logic;
  signal rx_sop       : std_logic;

  begin

    rx_probe_o(0) <= rx_sop;            -- 4
    rx_probe_o(1) <= m_axis_rx_tvalid;  -- 5
    rx_probe_o(2) <= WR_on;             -- 6
    rx_probe_o(3) <= RD_on;             -- 7

    DW_wr_Cnt_cl <= '1' when ((trn_rst = '1') or (rx_sop = '1')) else '0';
    DW_wr_Cnt_tc <= '0';

    rx_sop <= m_axis_rx_tvalid and (not rx_in_packet);
    npck_prsnt_o <= m_axis_rx_tvalid and (not rx_in_packet);

    process begin
      wait until rising_edge(clk);
        if (trn_rst = '1') then
          rx_in_packet <= '0';
        elsif ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1') and (m_axis_rx_tlast = '1')) then
          rx_in_packet <= '0';
        elsif ((rx_sop = '1') and (m_axis_rx_tready_int = '1')) then
          rx_in_packet <= '1';
        end if;
    end process;

    req_compl_o    <= req_compl;
    req_compl_wd_o <= req_compl_wd;

    process
    begin

      wait until rising_edge(clk);

      if (trn_rst = '1') then

        m_axis_rx_tready_int <= '0';
        req_compl            <= '0';
        req_compl_wd         <= '1';

        req_tc_o             <= (others => '0');
        req_td_o             <= '0';
        req_ep_o             <= '0';
        req_attr_o           <= (others => '0');
        req_rid_o            <= (others => '0');
        req_tag_o            <= (others => '0');
        req_be_o             <= (others => '0');
        req_addr_o           <= (others => '0');
				req_len              <= (others => '0');
				req_len_sup_o        <= '0';
				wr_en_o              <= '0';
        tlp_type             <= (others => '0');

        brd_strt_o           <= '0';
        bwr_strt_o           <= '0';
        bwr_be_o             <= (others => '0');
        bwr_addr_o           <= (others => '0');
        breq_len_o           <= (others => '0');

        for i in 0 to 31 loop
          bwr_data_o(i) <= (others => '0');
        end loop;

        DW_wr_Cnt_en         <= '0';
        RD_on                <= '0';
        WR_on                <= '0';
        E_Bone_trn_l         <= '0';
				tx_tvalid_flag       <= '0';
				req_rx_valid_o       <= '0';
        Type_Offset_desc_o   <= (others => '0');

        msg_signaling_o      <= '0';
        msg_code_o           <= (others => '0');
        msg_data_lsb_o       <= (others => '0');
        msg_data_msb_o       <= (others => '0');

        rx_state             <= RX_RST_STATE;

      else

        wr_en_o        <= '0';
				req_len_sup_o  <= '0';
				req_rx_valid_o <= '0';

        case (rx_state) is

          when RX_RST_STATE =>

					  m_axis_rx_tready_int <= '1';
						msg_signaling_o      <= '0';

            if ((rx_sop = '1') and (m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then  -- RX begin process

              case (m_axis_rx_tdata(30 downto 24)) is

                when RX_MEM_RD32_FMT_TYPE =>                               -- MRd: 32-bit Address Memory Read Request

                  req_rx_valid_o       <= '1';
                  tlp_type             <= m_axis_rx_tdata(30 downto 24);
									breq_len_o           <= m_axis_rx_tdata(9 downto 0);
                  m_axis_rx_tready_int <= '0';
								  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
										RD_on      <= '1';
										req_tc_o   <= m_axis_rx_tdata(22 downto 20);
                    req_td_o   <= m_axis_rx_tdata(15);
                    req_ep_o   <= m_axis_rx_tdata(14);
                    req_attr_o <= m_axis_rx_tdata(13 downto 12);
										req_rid_o  <= m_axis_rx_tdata(63 downto 48);
                    req_tag_o  <= m_axis_rx_tdata(47 downto 40);
                    req_be_o   <= m_axis_rx_tdata(39 downto 32);
										req_len    <= m_axis_rx_tdata(9 downto 0);
                    rx_state   <= RX_MEM_RD32_DW1DW2;
                  else
                    req_len_sup_o <= '1';
                    rx_state      <= RX_RST_STATE;
                  end if;

                when RX_MEM_WR32_FMT_TYPE =>                               -- MWr: 32-bit Address Memory Write Request

                  tlp_type             <= m_axis_rx_tdata(30 downto 24);
									breq_len_o           <= m_axis_rx_tdata(9 downto 0);
									bwr_be_o             <= m_axis_rx_tdata(39 downto 32);
                  m_axis_rx_tready_int <= '0';
                  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
									  WR_on      <= '1';
										req_tc_o   <= m_axis_rx_tdata(22 downto 20);
                    req_td_o   <= m_axis_rx_tdata(15);
                    req_ep_o   <= m_axis_rx_tdata(14);
                    req_attr_o <= m_axis_rx_tdata(13 downto 12);
										req_rid_o  <= m_axis_rx_tdata(63 downto 48);
                    req_tag_o  <= m_axis_rx_tdata(47 downto 40);
                    req_be_o   <= m_axis_rx_tdata(39 downto 32);
										req_len    <= m_axis_rx_tdata(9 downto 0);
                    rx_state   <= RX_MEM_WR32_DW1DW2;
                  else
                    req_len_sup_o <= '1';
                    rx_state      <= RX_RST_STATE;
                  end if;

                when RX_MEM_RD64_FMT_TYPE =>                               -- MRd: 64-bit Address Memory Read Request

                  tlp_type             <= m_axis_rx_tdata(30 downto 24);
									breq_len_o           <= m_axis_rx_tdata(9 downto 0);
                  m_axis_rx_tready_int <= '0';
                  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
									  RD_on      <= '1';
										req_tc_o   <= m_axis_rx_tdata(22 downto 20);
                    req_td_o   <= m_axis_rx_tdata(15);
                    req_ep_o   <= m_axis_rx_tdata(14);
                    req_attr_o <= m_axis_rx_tdata(13 downto 12);
										req_rid_o  <= m_axis_rx_tdata(63 downto 48);
                    req_tag_o  <= m_axis_rx_tdata(47 downto 40);
                    req_be_o   <= m_axis_rx_tdata(39 downto 32);
										req_len    <= m_axis_rx_tdata(9 downto 0);
                    rx_state   <= RX_MEM_RD64_DW1DW2;
                  else
                    req_len_sup_o <= '1';
                    rx_state      <= RX_RST_STATE;
                  end if;

                when RX_MEM_WR64_FMT_TYPE =>                               -- MWr: 64-bit Address Memory Write Request

                  tlp_type             <= m_axis_rx_tdata(30 downto 24);
									breq_len_o           <= m_axis_rx_tdata(9 downto 0);
									bwr_be_o             <= m_axis_rx_tdata(39 downto 32);
                  m_axis_rx_tready_int <= '0';
                  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
									  WR_on      <= '1';
										req_tc_o   <= m_axis_rx_tdata(22 downto 20);
                    req_td_o   <= m_axis_rx_tdata(15);
                    req_ep_o   <= m_axis_rx_tdata(14);
                    req_attr_o <= m_axis_rx_tdata(13 downto 12);
										req_rid_o  <= m_axis_rx_tdata(63 downto 48);
                    req_tag_o  <= m_axis_rx_tdata(47 downto 40);
                    req_be_o   <= m_axis_rx_tdata(39 downto 32);
										req_len    <= m_axis_rx_tdata(9 downto 0);
                    rx_state   <= RX_MEM_WR64_DW1DW2;
                  else
                    req_len_sup_o <= '1';
                    rx_state      <= RX_RST_STATE;
                  end if;

                when RX_IO_RD32_FMT_TYPE =>                                -- IORd: IO Read Request

                  tlp_type             <= m_axis_rx_tdata(30 downto 24);
									breq_len_o           <= m_axis_rx_tdata(9 downto 0);
                  m_axis_rx_tready_int <= '0';
                  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
									  RD_on      <= '1';
										req_tc_o   <= m_axis_rx_tdata(22 downto 20);
                    req_td_o   <= m_axis_rx_tdata(15);
                    req_ep_o   <= m_axis_rx_tdata(14);
                    req_attr_o <= m_axis_rx_tdata(13 downto 12);
										req_rid_o  <= m_axis_rx_tdata(63 downto 48);
                    req_tag_o  <= m_axis_rx_tdata(47 downto 40);
                    req_be_o   <= m_axis_rx_tdata(39 downto 32);
										req_len    <= m_axis_rx_tdata(9 downto 0);
                    rx_state   <= RX_MEM_RD32_DW1DW2;
                  else
                    req_len_sup_o <= '1';
                    rx_state      <= RX_RST_STATE;
                  end if;

                when RX_IO_WR32_FMT_TYPE =>                                -- IOWr: IO Write Request

                  tlp_type             <= m_axis_rx_tdata(30 downto 24);
									breq_len_o           <= m_axis_rx_tdata(9 downto 0);
									bwr_be_o             <= m_axis_rx_tdata(39 downto 32);
                  m_axis_rx_tready_int <= '0';
                  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
									  WR_on      <= '1';
										req_tc_o   <= m_axis_rx_tdata(22 downto 20);
                    req_td_o   <= m_axis_rx_tdata(15);
                    req_ep_o   <= m_axis_rx_tdata(14);
                    req_attr_o <= m_axis_rx_tdata(13 downto 12);
										req_rid_o  <= m_axis_rx_tdata(63 downto 48);
                    req_tag_o  <= m_axis_rx_tdata(47 downto 40);
                    req_be_o   <= m_axis_rx_tdata(39 downto 32);
										req_len    <= m_axis_rx_tdata(9 downto 0);
                    rx_state   <= RX_IO_WR_DW1DW2;
                  else
                    req_len_sup_o <= '1';
                    rx_state      <= RX_RST_STATE;
                  end if;

                when RX_CPL_NODT_FMT_TYPE =>                               -- Cpl:  Completion without data

                  rx_state <= RX_RST_STATE;

                when RX_CPL_RD32_FMT_TYPE =>                               -- CplD: Completion with data

                  rx_state <= RX_RST_STATE;

                when RX_MSGD_ID_FMT_TYPE =>                                -- MsgD: Message Request with data, Routed by ID

                  tlp_type             <= m_axis_rx_tdata(30 downto 24);
									req_len              <= m_axis_rx_tdata(9 downto 0);
							    msg_code_o           <= m_axis_rx_tdata(39 downto 32);
									m_axis_rx_tready_int <= '0';
                  rx_state             <= RX_MSGD_DW1DW2;

                when RX_MSGD_RC_FMT_TYPE =>                                -- MsgD: Message Request with data, from Root Cplx

                  tlp_type             <= m_axis_rx_tdata(30 downto 24);
									req_len              <= m_axis_rx_tdata(9 downto 0);
							    msg_code_o           <= m_axis_rx_tdata(39 downto 32);
									m_axis_rx_tready_int <= '0';
                  rx_state             <= RX_MSGD_DW1DW2;

                when RX_MSGD_LC_FMT_TYPE =>                                -- MsgD: Message Request with data, local

                  tlp_type             <= m_axis_rx_tdata(30 downto 24);
									req_len              <= m_axis_rx_tdata(9 downto 0);
							    msg_code_o           <= m_axis_rx_tdata(39 downto 32);
									m_axis_rx_tready_int <= '0';
                  rx_state             <= RX_MSGD_DW1DW2;

                when RX_MSG_ID_FMT_TYPE =>                                 -- MsgD: Message Request without data, Routed by ID

                  tlp_type             <= m_axis_rx_tdata(30 downto 24);
									req_len              <= m_axis_rx_tdata(9 downto 0);
							    msg_code_o           <= m_axis_rx_tdata(39 downto 32);
									m_axis_rx_tready_int <= '0';
                  rx_state             <= RX_MSG_SIGNALING;

                when RX_MSG_RC_FMT_TYPE =>                                 -- MsgD: Message Request without data, from Root Cplx

                  tlp_type             <= m_axis_rx_tdata(30 downto 24);
									req_len              <= m_axis_rx_tdata(9 downto 0);
							    msg_code_o           <= m_axis_rx_tdata(39 downto 32);
									m_axis_rx_tready_int <= '0';
                  rx_state             <= RX_MSG_SIGNALING;

                when RX_MSG_LC_FMT_TYPE =>                                 -- MsgD: Message Request without data, local

                  tlp_type             <= m_axis_rx_tdata(30 downto 24);
									req_len              <= m_axis_rx_tdata(9 downto 0);
							    msg_code_o           <= m_axis_rx_tdata(39 downto 32);
									m_axis_rx_tready_int <= '0';
                  rx_state             <= RX_MSG_SIGNALING;

                when others =>                                             -- other TLPs

                  rx_state <= RX_RST_STATE;

              end case;

            else                                                           -- RX end process
              rx_state <= RX_RST_STATE;
            end if;

          when RX_MEM_RD32_DW1DW2 =>                                       -- first and second dwords of mem32 read tlp

            if (m_axis_rx_tvalid = '1') then
							req_addr_o          <= region_select(1 downto 0) & m_axis_rx_tdata(29 downto 2) & "00";
              req_compl           <= '1';
              req_compl_wd        <= '1';
						  Type_Offset_desc_o  <= '1' & '0' & seg_desc_fmt & m_axis_rx_tdata(29 downto 2);
						  brd_strt_o          <= '1';
						  E_Bone_trn_l        <= E_Bone_trn;
              rx_state            <= RX_WAIT_STATE;
            else
              rx_state <= RX_MEM_RD32_DW1DW2;
            end if;

          when RX_MEM_WR32_DW1DW2 =>                                       -- first and second dwords of mem32 write tlp

            if (m_axis_rx_tvalid = '1') then
              bwr_data_o(conv_integer(DW_wr_Cnt(4 downto 0))) <= m_axis_rx_tdata(63 downto 32);
              bwr_addr_o                                      <= region_select(1 downto 0) & m_axis_rx_tdata(29 downto 2);
						  Type_Offset_desc_o                              <= '0' & '0' & seg_desc_fmt & m_axis_rx_tdata(29 downto 2);
						  bwr_strt_o                                      <= '1';
						  E_Bone_trn_l                                    <= E_Bone_trn;
              rx_state                                        <= RX_IO_MEM_WR_WAIT_STATE;
            else
              rx_state <= RX_MEM_WR32_DW1DW2;
            end if;

          when RX_IO_MEM_WR_WAIT_STATE =>

            if (bcall_run_i = '0') then
              wr_en_o  <= '1';
              rx_state <= RX_WAIT_STATE;
						else
						  rx_state <= RX_IO_MEM_WR_WAIT_STATE;
						end if;

          when RX_IO_WR_DW1DW2 =>                                          -- first and second dwords of io write tlp

            if (m_axis_rx_tvalid = '1') then
              bwr_data_o(conv_integer(DW_wr_Cnt(4 downto 0))) <= m_axis_rx_tdata(63 downto 32);
              bwr_addr_o                                      <= region_select(1 downto 0) & m_axis_rx_tdata(29 downto 2);
              req_compl                                       <= '1';
						  req_compl_wd                                    <= '0';
              rx_state                                        <= RX_IO_MEM_WR_WAIT_STATE;
            else
              rx_state <= RX_IO_WR_DW1DW2;
            end if;

          when RX_MEM_RD64_DW1DW2 =>                                       -- first and second dwords of mem64 read tlp

            if (m_axis_rx_tvalid = '1') then
              req_addr_o          <= region_select(1 downto 0)  & m_axis_rx_tdata(61 downto 34) & "00";
              req_compl           <= '1';
              req_compl_wd        <= '1';
						  Type_Offset_desc_o  <= '1' & '0' & seg_desc_fmt & m_axis_rx_tdata(61 downto 34);
						  brd_strt_o          <= '1';
						  E_Bone_trn_l        <= E_Bone_trn;
              rx_state            <= RX_WAIT_STATE;
            else
              rx_state <= RX_MEM_RD64_DW1DW2;
            end if;

          when RX_MEM_WR64_DW1DW2 =>                                       -- first and second dwords of mem64 write tlp

            if (m_axis_rx_tvalid = '1') then
              bwr_addr_o         <= region_select(1 downto 0) & m_axis_rx_tdata(61 downto 34);
						  Type_Offset_desc_o <= '0' & '0' & seg_desc_fmt & m_axis_rx_tdata(61 downto 34);
						  bwr_strt_o         <= '1';
						  E_Bone_trn_l       <= E_Bone_trn;
              rx_state           <= RX_MEM_WR64_DW3;
            else
              rx_state <= RX_MEM_WR64_DW1DW2;
            end if;

          when RX_MEM_WR64_DW3 =>                                          -- third dword of mem64 write tlp contains 1 DW write data

            if (m_axis_rx_tvalid = '1') then
              bwr_data_o(conv_integer(DW_wr_Cnt(4 downto 0))) <= m_axis_rx_tdata(31 downto 0);
              if (bcall_run_i = '0') then
                wr_en_o  <= '1';
							  rx_state <= RX_WAIT_STATE;
							else
							  rx_state <= RX_IO_MEM_WR_WAIT_STATE;
							end if;
            else
              rx_state <= RX_MEM_WR64_DW3;
            end if;

          when RX_MSGD_DW1DW2 =>                                           -- first and second dwords of message write tlp

            if (m_axis_rx_tvalid = '1') then
              msg_data_msb_o <= m_axis_rx_tdata(31 downto 0);
              msg_data_lsb_o <= m_axis_rx_tdata(63 downto 32);
				      rx_state       <= RX_MSG_SIGNALING;
            else
              rx_state <= RX_MSGD_DW1DW2;
            end if;

          when RX_MSG_SIGNALING =>                                         -- start message processing

            msg_signaling_o <= '1';
            rx_state        <= RX_RST_STATE;

          -- Stay in wait state for
          --  1. Target writes until the write has been completed and
          --     written into BRAM.
          --  2. Target reads until the completion has been generated
          --     and has been successfully transmitted via the PIO's
          --     TX interface.
          --  3. IO Write and Extended CFG write until the completion
          --     has been generated and has been successfully transmitted
          --     via the PIOs TX interface

          when RX_WAIT_STATE =>

            wr_en_o <= '0';

            if ((tlp_type = RX_MEM_WR32_FMT_TYPE) and
						    (((wr_busy_ep_i = '0') and (E_Bone_trn_l = '0')) or ((wr_busy_eb_i = '0') and (E_Bone_trn_l = '1')))) then

              WR_on                <= '0';
              bwr_strt_o           <= '0';
              m_axis_rx_tready_int <= '1';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_IO_WR32_FMT_TYPE) and
						       (((wr_busy_ep_i = '0') and (E_Bone_trn_l = '0')) or ((wr_busy_eb_i = '0') and (E_Bone_trn_l = '1')))) then

              WR_on                <= '0';
              bwr_strt_o           <= '0';
              m_axis_rx_tready_int <= '1';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_MEM_WR64_FMT_TYPE) and
						       (((wr_busy_ep_i = '0') and (E_Bone_trn_l = '0')) or ((wr_busy_eb_i = '0') and (E_Bone_trn_l = '1')))) then

              WR_on                <= '0';
              bwr_strt_o           <= '0';
              m_axis_rx_tready_int <= '1';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_MEM_RD32_FMT_TYPE) and (compl_done_i = '1')) then

              m_axis_rx_tready_int <= '1';
							req_compl            <= '0';
							req_compl_wd         <= '0';
							RD_on                <= '0';
							brd_strt_o           <= '0';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_IO_RD32_FMT_TYPE) and (compl_done_i = '1')) then

              m_axis_rx_tready_int <= '1';
							req_compl            <= '0';
							req_compl_wd         <= '0';
							RD_on                <= '0';
							brd_strt_o           <= '0';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_MEM_RD64_FMT_TYPE) and (compl_done_i = '1')) then

              m_axis_rx_tready_int <= '1';
							req_compl            <= '0';
							req_compl_wd         <= '0';
							RD_on                <= '0';
							brd_strt_o           <= '0';
              rx_state             <= RX_RST_STATE;

            else

              rx_state <= RX_WAIT_STATE;

            end if;

          when others =>

            rx_state <= RX_RST_STATE;

				end case;

      end if;

    end process;

--------------------------------------------------------------------------------------------------------------
end generate EP_RX_64B;
--------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
EP_RX_128B : if (EBFT_DWIDTH = 128) generate                               -- ep_128b_rx_engine implementation
--------------------------------------------------------------------------------------------------------------

  alias rx_sof       : std_logic is m_axis_rx_tuser(14);
	alias rx_eof       : std_logic is m_axis_rx_tuser(21);

  type rx_state_type is (RX_RST_STATE,
                         RX_MEM_RD32_DW1DW2,
                         RX_MEM_WR32_DW1DW2,
                         RX_MEM_RD64_DW1DW2,
                         RX_MEM_WR64_DW1DW2,
                         RX_MEM_WR64_DW3,
                         RX_MSG_SIGNALING,
												 RX_IO_MEM_WR_WAIT_STATE,
												 RX_DLY_STATE,
                         RX_WAIT_STATE);
  signal rx_state    : rx_state_type;
  signal rx_sof_dw0  : std_logic;
	signal rx_sof_dw2  : std_logic;
	signal rx_eof_dw0  : std_logic;
	signal rx_eof_dw1  : std_logic;
	signal rx_eof_dw2  : std_logic;
	signal rx_eof_dw3  : std_logic;

  begin

    rx_probe_o(0) <= rx_sof_dw0 or rx_sof_dw2;                              -- 4
    rx_probe_o(1) <= m_axis_rx_tvalid;                                      -- 5
    rx_probe_o(2) <= WR_on;                                                 -- 6
    rx_probe_o(3) <= RD_on;                                                 -- 7

    DW_wr_Cnt_cl <= '1' when ((trn_rst = '1') or (rx_sof = '1')) else '0';
    DW_wr_Cnt_tc <= '0';

    npck_prsnt_o <= rx_sof_dw0 or rx_sof_dw2;

	  rx_sof_dw0 <= '1' when ((rx_sof = '1') and (m_axis_rx_tuser(13) = '0')) else '0';
	  rx_sof_dw2 <= '1' when ((rx_sof = '1') and (m_axis_rx_tuser(13) = '1')) else '0';
		rx_eof_dw0 <= '1' when ((rx_eof = '1') and (m_axis_rx_tuser(20 downto 17) = "0011")) else '0';
		rx_eof_dw1 <= '1' when ((rx_eof = '1') and (m_axis_rx_tuser(20 downto 17) = "0111")) else '0';
		rx_eof_dw2 <= '1' when ((rx_eof = '1') and (m_axis_rx_tuser(20 downto 17) = "1011")) else '0';
		rx_eof_dw3 <= '1' when ((rx_eof = '1') and (m_axis_rx_tuser(20 downto 17) = "1111")) else '0';

    process begin
      wait until rising_edge(clk);
        req_compl_o    <= req_compl;
        req_compl_wd_o <= req_compl_wd;
    end process;

    process
    begin

      wait until rising_edge(clk);

      if (trn_rst = '1') then

        m_axis_rx_tready_int <= '0';
        req_compl            <= '0';
        req_compl_wd         <= '0';

        req_tc_o             <= (others => '0');
        req_td_o             <= '0';
        req_ep_o             <= '0';
        req_attr_o           <= (others => '0');
        req_rid_o            <= (others => '0');
        req_tag_o            <= (others => '0');
        req_be_o             <= (others => '0');
        req_addr_o           <= (others => '0');
				req_len              <= (others => '0');
				req_len_sup_o        <= '0';
				wr_en_o              <= '0';
        tlp_type             <= (others => '0');

        brd_strt_o           <= '0';
        bwr_strt_o           <= '0';
        bwr_be_o             <= (others => '0');
        bwr_addr_o           <= (others => '0');
        breq_len_o           <= (others => '0');

        for i in 0 to 31 loop
          bwr_data_o(i) <= (others => '0');
        end loop;

        DW_wr_Cnt_en         <= '0';
        RD_on                <= '0';
        WR_on                <= '0';
        E_Bone_trn_l         <= '0';
				tx_tvalid_flag       <= '0';
				req_rx_valid_o       <= '0';
        Type_Offset_desc_o   <= (others => '0');

        msg_signaling_o      <= '0';
        msg_code_o           <= (others => '0');
        msg_data_lsb_o       <= (others => '0');
        msg_data_msb_o       <= (others => '0');

        rx_state             <= RX_RST_STATE;

      else

        wr_en_o        <= '0';
				req_len_sup_o  <= '0';
				req_rx_valid_o <= '0';

        case rx_state is

          when RX_RST_STATE  =>

            m_axis_rx_tready_int <= '1';
						msg_signaling_o      <= '0';

            if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then -- RX begin process

              if (rx_sof_dw2 = '1') then                                        -- tlp_type [94..88]
                                                                                --------------------
                tlp_type             <= m_axis_rx_tdata(94 downto 88);
								req_len              <= m_axis_rx_tdata(73 downto 64);
                m_axis_rx_tready_int <= '0';

                case m_axis_rx_tdata(94 downto 88) is

                  when RX_MEM_RD32_FMT_TYPE =>                                  -- MRd: 32-bit Address Memory Read Request

                    req_rx_valid_o <= '1';

                    if (m_axis_rx_tdata(73 downto 64) = "0000000001") then
										  req_tc_o           <= m_axis_rx_tdata(86 downto 84);
                      req_td_o           <= m_axis_rx_tdata(79);
                      req_ep_o           <= m_axis_rx_tdata(78);
                      req_attr_o         <= m_axis_rx_tdata(77 downto 76);
                      req_rid_o          <= m_axis_rx_tdata(127 downto 112);
                      req_tag_o          <= m_axis_rx_tdata(111 downto 104);
                      req_be_o           <= m_axis_rx_tdata(103 downto 96);
											breq_len_o         <= m_axis_rx_tdata(73 downto 64);
											req_compl          <= '1';
                      req_compl_wd       <= '1';
											Type_Offset_desc_o <= '1' & '0' & seg_desc_fmt & m_axis_rx_tdata(61 downto 34);
                      E_Bone_trn_l       <= E_Bone_trn;
											if (s_axis_tx_tvalid = '1') then
											  tx_tvalid_flag <= '1';
											else
											  RD_on      <= '1';
											  brd_strt_o <= '1';
											end if;
                      rx_state <= RX_MEM_RD32_DW1DW2;
                    else
                      req_len_sup_o <= '1';
                      rx_state      <= RX_RST_STATE;
                    end if;

                  when RX_MEM_WR32_FMT_TYPE =>                                  -- MWr: 32-bit Address Memory Write Request

                    if (m_axis_rx_tdata(73 downto 64) = "0000000001") then
											WR_on              <= '1';
						          Type_Offset_desc_o <= '0' & '0' & seg_desc_fmt & m_axis_rx_tdata(61 downto 34);
						          bwr_strt_o         <= '1';
						          E_Bone_trn_l       <= E_Bone_trn;
											breq_len_o         <= m_axis_rx_tdata(73 downto 64);
                      bwr_be_o           <= m_axis_rx_tdata(103 downto 96);
                      rx_state           <= RX_MEM_WR32_DW1DW2;
                    else
                      req_len_sup_o <= '1';
                      rx_state      <= RX_RST_STATE;
                    end if;

                  when RX_MEM_RD64_FMT_TYPE =>                                  -- MRd: 64-bit Address Memory Read Request

                    if (m_axis_rx_tdata(73 downto 64) = "0000000001") then
										  req_tc_o           <= m_axis_rx_tdata(86 downto 84);
                      req_td_o           <= m_axis_rx_tdata(79);
                      req_ep_o           <= m_axis_rx_tdata(78);
                      req_attr_o         <= m_axis_rx_tdata(77 downto 76);
                      req_rid_o          <= m_axis_rx_tdata(127 downto 112);
                      req_tag_o          <= m_axis_rx_tdata(111 downto 104);
                      req_be_o           <= m_axis_rx_tdata(103 downto 96);
											breq_len_o         <= m_axis_rx_tdata(73 downto 64);
											req_compl          <= '1';
                      req_compl_wd       <= '1';
											Type_Offset_desc_o <= '1' & '0' & seg_desc_fmt & m_axis_rx_tdata(61 downto 34);
                      E_Bone_trn_l       <= E_Bone_trn;
											if (s_axis_tx_tvalid = '1') then
											  tx_tvalid_flag <= '1';
											else
											  RD_on      <= '1';
											  brd_strt_o <= '1';
											end if;
                      rx_state <= RX_MEM_RD64_DW1DW2;
                    else
                      req_len_sup_o <= '1';
                      rx_state      <= RX_RST_STATE;
                    end if;

                  when RX_MEM_WR64_FMT_TYPE =>                                  -- MWr: 64-bit Address Memory Write Request

                    if (m_axis_rx_tdata(73 downto 64) = "0000000001") then
										  WR_on              <= '1';
						          Type_Offset_desc_o <= '0' & '0' & seg_desc_fmt & m_axis_rx_tdata(61 downto 34);
						          bwr_strt_o         <= '1';
						          E_Bone_trn_l       <= E_Bone_trn;
											breq_len_o         <= m_axis_rx_tdata(73 downto 64);
                      bwr_be_o           <= m_axis_rx_tdata(103 downto 96);
                      rx_state           <= RX_MEM_WR64_DW1DW2;
                    else
                      req_len_sup_o <= '1';
                      rx_state      <= RX_RST_STATE;
                    end if;

                  when RX_IO_RD32_FMT_TYPE =>                                   -- IORd: IO Read Request

                    if (m_axis_rx_tdata(73 downto 64) = "0000000001") then
										  req_tc_o           <= m_axis_rx_tdata(86 downto 84);
                      req_td_o           <= m_axis_rx_tdata(79);
                      req_ep_o           <= m_axis_rx_tdata(78);
                      req_attr_o         <= m_axis_rx_tdata(77 downto 76);
                      req_rid_o          <= m_axis_rx_tdata(127 downto 112);
                      req_tag_o          <= m_axis_rx_tdata(111 downto 104);
                      req_be_o           <= m_axis_rx_tdata(103 downto 96);
											breq_len_o         <= m_axis_rx_tdata(73 downto 64);
                      req_compl          <= '1';
                      req_compl_wd       <= '1';
											Type_Offset_desc_o <= '1' & '0' & seg_desc_fmt & m_axis_rx_tdata(61 downto 34);
                      E_Bone_trn_l       <= E_Bone_trn;
											if (s_axis_tx_tvalid = '1') then
											  tx_tvalid_flag <= '1';
											else
											  RD_on      <= '1';
											  brd_strt_o <= '1';
											end if;
                      rx_state <= RX_MEM_RD32_DW1DW2;
                    else
                      req_len_sup_o <= '1';
                      rx_state      <= RX_RST_STATE;
                    end if;

                  when RX_IO_WR32_FMT_TYPE =>                                   -- IOWr: IO Write Request

                    if (m_axis_rx_tdata(73 downto 64) = "0000000001") then
										  req_tc_o           <= m_axis_rx_tdata(86 downto 84);
                      req_td_o           <= m_axis_rx_tdata(79);
                      req_ep_o           <= m_axis_rx_tdata(78);
                      req_attr_o         <= m_axis_rx_tdata(77 downto 76);
                      req_rid_o          <= m_axis_rx_tdata(127 downto 112);
                      req_tag_o          <= m_axis_rx_tdata(111 downto 104);
                      req_be_o           <= m_axis_rx_tdata(103 downto 96);
											breq_len_o         <= m_axis_rx_tdata(73 downto 64);
										  bwr_be_o           <= m_axis_rx_tdata(103 downto 96);
										  WR_on              <= '1';
						          Type_Offset_desc_o <= '0' & '0' & seg_desc_fmt & m_axis_rx_tdata(61 downto 34);
						          bwr_strt_o         <= '1';
						          E_Bone_trn_l       <= E_Bone_trn;
                      rx_state           <= RX_MEM_WR32_DW1DW2;
                    else
                      req_len_sup_o <= '1';
                      rx_state      <= RX_RST_STATE;
                    end if;

                  when RX_CPL_NODT_FMT_TYPE =>                                  -- Cpl:  Completion without data

                    rx_state <= RX_RST_STATE;

                  when RX_CPL_RD32_FMT_TYPE =>                                  -- CplD: Completion with data

                    rx_state <= RX_RST_STATE;

                  when RX_MSGD_ID_FMT_TYPE =>                                   -- MsgD: Message Request with data, Routed by ID

							      msg_code_o     <= m_axis_rx_tdata(103 downto 96);
								    msg_data_lsb_o <= m_axis_rx_tdata(31 downto 0);
                    msg_data_msb_o <= m_axis_rx_tdata(63 downto 32);
				            rx_state       <= RX_MSG_SIGNALING;

                  when RX_MSGD_RC_FMT_TYPE =>                                   -- MsgD: Message Request with data, from Root Cplx

							      msg_code_o     <= m_axis_rx_tdata(103 downto 96);
                    msg_data_lsb_o <= m_axis_rx_tdata(31 downto 0);
                    msg_data_msb_o <= m_axis_rx_tdata(63 downto 32);
				            rx_state       <= RX_MSG_SIGNALING;

                  when RX_MSGD_LC_FMT_TYPE =>                                   -- MsgD: Message Request with data, local

							      msg_code_o     <= m_axis_rx_tdata(103 downto 96);
                    msg_data_lsb_o <= m_axis_rx_tdata(31 downto 0);
                    msg_data_msb_o <= m_axis_rx_tdata(63 downto 32);
				            rx_state       <= RX_MSG_SIGNALING;

                  when RX_MSG_ID_FMT_TYPE =>                                    -- MsgD: Message Request without data, Routed by ID

							      msg_code_o <= m_axis_rx_tdata(103 downto 96);
                    rx_state   <= RX_MSG_SIGNALING;

                  when RX_MSG_RC_FMT_TYPE =>                                    -- MsgD: Message Request without data, from Root Cplx

							      msg_code_o <= m_axis_rx_tdata(103 downto 96);
                    rx_state   <= RX_MSG_SIGNALING;

                  when RX_MSG_LC_FMT_TYPE =>                                    -- MsgD: Message Request without data, local

							      msg_code_o <= m_axis_rx_tdata(103 downto 96);
                    rx_state   <= RX_MSG_SIGNALING;

                  when others =>                                                -- other TLPs

                    rx_state <= RX_RST_STATE;

                end case;

              else                                                              -- tlp_type [30..24]
                                                                                --------------------
                tlp_type             <= m_axis_rx_tdata(30 downto 24);
								req_len              <= m_axis_rx_tdata(9 downto 0);
                m_axis_rx_tready_int <= '0';

                case m_axis_rx_tdata(30 downto 24) is

                  when RX_MEM_RD32_FMT_TYPE =>                                  -- MRd: 32-bit Address Memory Read Request

                    req_rx_valid_o <= '1';

                    if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
										  req_tc_o           <= m_axis_rx_tdata(22 downto 20);
                      req_td_o           <= m_axis_rx_tdata(15);
                      req_ep_o           <= m_axis_rx_tdata(14);
                      req_attr_o         <= m_axis_rx_tdata(13 downto 12);
                      req_rid_o          <= m_axis_rx_tdata(63 downto 48);
                      req_tag_o          <= m_axis_rx_tdata(47 downto 40);
                      req_be_o           <= m_axis_rx_tdata(39 downto 32);
											breq_len_o         <= m_axis_rx_tdata(9 downto 0);
											req_addr_o         <= region_select(1 downto 0) & m_axis_rx_tdata(93 downto 66) & "00";
                      req_compl          <= '1';
                      req_compl_wd       <= '1';
											Type_Offset_desc_o <= '1' & '0' & seg_desc_fmt & m_axis_rx_tdata(93 downto 66);
                      E_Bone_trn_l       <= E_Bone_trn;
											if (s_axis_tx_tvalid = '1') then
											  tx_tvalid_flag <= '1';
												rx_state       <= RX_DLY_STATE;
											else
											  RD_on      <= '1';
											  brd_strt_o <= '1';
												rx_state   <= RX_WAIT_STATE;
											end if;
                    else
                      req_len_sup_o <= '1';
                      rx_state      <= RX_RST_STATE;
                    end if;

                  when RX_MEM_WR32_FMT_TYPE =>                                  -- MWr: 32-bit Address Memory Write Request

                    if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
                      bwr_data_o(conv_integer(DW_wr_Cnt(4 downto 0))) <= m_axis_rx_tdata(127 downto 96);
                      bwr_addr_o                                      <= (region_select(1 downto 0) & m_axis_rx_tdata(93 downto 66));
											bwr_be_o                                        <= m_axis_rx_tdata(39 downto 32);
											breq_len_o                                      <= m_axis_rx_tdata(9 downto 0);
											WR_on                                           <= '1';
						          Type_Offset_desc_o                              <= '0' & '0' & seg_desc_fmt & m_axis_rx_tdata(93 downto 66);
						          bwr_strt_o                                      <= '1';
						          E_Bone_trn_l                                    <= E_Bone_trn;
                      rx_state                                        <= RX_IO_MEM_WR_WAIT_STATE;
                    else
                      req_len_sup_o <= '1';
                      rx_state      <= RX_RST_STATE;
                    end if;

                  when RX_MEM_RD64_FMT_TYPE =>                                  -- MRd: 64-bit Address Memory Read Request

                    if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
										  req_tc_o           <= m_axis_rx_tdata(22 downto 20);
                      req_td_o           <= m_axis_rx_tdata(15);
                      req_ep_o           <= m_axis_rx_tdata(14);
                      req_attr_o         <= m_axis_rx_tdata(13 downto 12);
                      req_rid_o          <= m_axis_rx_tdata(63 downto 48);
                      req_tag_o          <= m_axis_rx_tdata(47 downto 40);
                      req_be_o           <= m_axis_rx_tdata(39 downto 32);
											breq_len_o         <= m_axis_rx_tdata(9 downto 0);
                      req_addr_o         <= region_select(1 downto 0) & m_axis_rx_tdata(93 downto 66) & "00";
                      req_compl          <= '1';
                      req_compl_wd       <= '1';
											Type_Offset_desc_o <= '1' & '0' & seg_desc_fmt & m_axis_rx_tdata(93 downto 66);
                      E_Bone_trn_l       <= E_Bone_trn;
											if (s_axis_tx_tvalid = '1') then
											  tx_tvalid_flag <= '1';
												rx_state       <= RX_DLY_STATE;
											else
											  RD_on      <= '1';
											  brd_strt_o <= '1';
												rx_state   <= RX_WAIT_STATE;
											end if;
                    else
                      req_len_sup_o <= '1';
                      rx_state      <= RX_RST_STATE;
                    end if;

                  when RX_MEM_WR64_FMT_TYPE =>                                  -- MWr: 64-bit Address Memory Write Request

                    if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
                      bwr_addr_o         <= (region_select(1 downto 0) & m_axis_rx_tdata(93 downto 66));
											bwr_be_o           <= m_axis_rx_tdata(39 downto 32);
											WR_on              <= '1';
											Type_Offset_desc_o <= '0' & '0' & seg_desc_fmt & m_axis_rx_tdata(93 downto 66);
						          bwr_strt_o         <= '1';
						          E_Bone_trn_l       <= E_Bone_trn;
                      rx_state           <= RX_MEM_WR64_DW3;
                    else
                      req_len_sup_o <= '1';
                      rx_state      <= RX_RST_STATE;
                    end if;

                  when RX_IO_RD32_FMT_TYPE =>                                   -- IORd: IO Read Request

                    if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
                      req_tc_o           <= m_axis_rx_tdata(22 downto 20);
                      req_td_o           <= m_axis_rx_tdata(15);
                      req_ep_o           <= m_axis_rx_tdata(14);
                      req_attr_o         <= m_axis_rx_tdata(13 downto 12);
                      req_rid_o          <= m_axis_rx_tdata(63 downto 48);
                      req_tag_o          <= m_axis_rx_tdata(47 downto 40);
                      req_be_o           <= m_axis_rx_tdata(39 downto 32);
                      req_addr_o         <= region_select(1 downto 0) & m_axis_rx_tdata(93 downto 66) & "00";
                      req_compl          <= '1';
                      req_compl_wd       <= '1';
											Type_Offset_desc_o <= '1' & '0' & seg_desc_fmt & m_axis_rx_tdata(93 downto 66);
                      E_Bone_trn_l       <= E_Bone_trn;
											if (s_axis_tx_tvalid = '1') then
											  tx_tvalid_flag <= '1';
												rx_state       <= RX_DLY_STATE;
											else
											  RD_on      <= '1';
											  brd_strt_o <= '1';
												rx_state   <= RX_WAIT_STATE;
											end if;
                    else
                      req_len_sup_o <= '1';
                      rx_state      <= RX_RST_STATE;
                    end if;

                  when RX_IO_WR32_FMT_TYPE =>                                   -- IOWr: IO Write Request

                    if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
                     req_tc_o                                        <= m_axis_rx_tdata(22 downto 20);
                     req_td_o                                        <= m_axis_rx_tdata(15);
                     req_ep_o                                        <= m_axis_rx_tdata(14);
                     req_attr_o                                      <= m_axis_rx_tdata(13 downto 12);
                     req_rid_o                                       <= m_axis_rx_tdata(63 downto 48);
                     req_tag_o                                       <= m_axis_rx_tdata(47 downto 40);
                     bwr_data_o(conv_integer(DW_wr_Cnt(4 downto 0))) <= m_axis_rx_tdata(127 downto 96);
                     bwr_addr_o                                      <= (region_select(1 downto 0) & m_axis_rx_tdata(93 downto 66));
                     wr_en_o                                         <= '1';
										 WR_on                                           <= '1';
                     req_compl                                       <= '1';
                     req_compl_wd                                    <= '0';
                     rx_state                                        <= RX_WAIT_STATE;
                    else
                      req_len_sup_o <= '1';
                      rx_state      <= RX_RST_STATE;
                    end if;

                  when RX_CPL_NODT_FMT_TYPE =>                                  -- Cpl:  Completion without data

                    rx_state <= RX_RST_STATE;

                  when RX_CPL_RD32_FMT_TYPE =>                                  -- CplD: Completion with data

                    rx_state <= RX_RST_STATE;

                  when RX_MSGD_ID_FMT_TYPE =>                                   -- MsgD: Message Request with data, Routed by ID

							      msg_code_o     <= m_axis_rx_tdata(39 downto 32);
								    msg_data_lsb_o <= m_axis_rx_tdata(31 downto 0);
                    msg_data_msb_o <= m_axis_rx_tdata(63 downto 32);
				            rx_state       <= RX_MSG_SIGNALING;

                  when RX_MSGD_RC_FMT_TYPE =>                                   -- MsgD: Message Request with data, from Root Cplx

							      msg_code_o     <= m_axis_rx_tdata(39 downto 32);
                    msg_data_lsb_o <= m_axis_rx_tdata(31 downto 0);
                    msg_data_msb_o <= m_axis_rx_tdata(63 downto 32);
				            rx_state       <= RX_MSG_SIGNALING;

                  when RX_MSGD_LC_FMT_TYPE =>                                   -- MsgD: Message Request with data, local

							      msg_code_o     <= m_axis_rx_tdata(39 downto 32);
                    msg_data_lsb_o <= m_axis_rx_tdata(31 downto 0);
                    msg_data_msb_o <= m_axis_rx_tdata(63 downto 32);
				            rx_state       <= RX_MSG_SIGNALING;

                  when RX_MSG_ID_FMT_TYPE =>                                    -- MsgD: Message Request without data, Routed by ID

							      msg_code_o <= m_axis_rx_tdata(39 downto 32);
                    rx_state   <= RX_MSG_SIGNALING;

                  when RX_MSG_RC_FMT_TYPE =>                                    -- MsgD: Message Request without data, from Root Cplx

							      msg_code_o <= m_axis_rx_tdata(39 downto 32);
                    rx_state   <= RX_MSG_SIGNALING;

                  when RX_MSG_LC_FMT_TYPE =>                                    -- MsgD: Message Request without data, local

							      msg_code_o <= m_axis_rx_tdata(39 downto 32);
                    rx_state   <= RX_MSG_SIGNALING;

                  when others =>                                                -- other TLPs

                    rx_state <= RX_RST_STATE;

                end case;

              end if;

            else
              rx_state <= RX_RST_STATE;
            end if;  -------------------------------------------------------------------------- RX end process

          when RX_IO_MEM_WR_WAIT_STATE =>

            if (bcall_run_i = '0') then
              wr_en_o  <= '1';
              rx_state <= RX_WAIT_STATE;
						else
						  rx_state <= RX_IO_MEM_WR_WAIT_STATE;
						end if;

          when RX_MEM_WR64_DW3 =>

            if (m_axis_rx_tvalid = '1') then
              bwr_data_o(conv_integer(DW_wr_Cnt(4 downto 0))) <= m_axis_rx_tdata(31 downto 0);
              if (bcall_run_i = '0') then
                wr_en_o  <= '1';
							  rx_state <= RX_WAIT_STATE;
							else
							  rx_state <= RX_IO_MEM_WR_WAIT_STATE;
							end if;
            else
              rx_state <= RX_MEM_WR64_DW3;
            end if;

          when RX_MEM_RD32_DW1DW2 =>

            if (m_axis_rx_tvalid = '1') then
              m_axis_rx_tready_int <= '0';
              req_addr_o           <= (region_select(1 downto 0) & m_axis_rx_tdata(29 downto 2) & "00");
              req_compl            <= '1';
              req_compl_wd         <= '1';
							if (tx_tvalid_flag = '1') then
							  rx_state <= RX_DLY_STATE;
							else
                rx_state <= RX_WAIT_STATE;
							end if;
            else
              rx_state <= RX_MEM_RD32_DW1DW2;
            end if;

          when RX_MEM_WR32_DW1DW2 =>

            if (m_axis_rx_tvalid = '1') then
              bwr_data_o(conv_integer(DW_wr_Cnt(4 downto 0))) <= m_axis_rx_tdata(63 downto 32);
              m_axis_rx_tready_int                            <= '0';
              bwr_addr_o                                      <= (region_select(1 downto 0) & m_axis_rx_tdata(29 downto 2));
							if (bcall_run_i = '0') then
                wr_en_o  <= '1';
							  rx_state <= RX_WAIT_STATE;
							else
							  rx_state <= RX_IO_MEM_WR_WAIT_STATE;
							end if;
            else
              rx_state <= RX_MEM_WR32_DW1DW2;
            end if;

          when RX_MEM_RD64_DW1DW2 =>

            if (m_axis_rx_tvalid = '1') then  
              req_addr_o           <= (region_select(1 downto 0) & m_axis_rx_tdata(29 downto 2) & "00");
              req_compl            <= '1';
              req_compl_wd         <= '1';
              m_axis_rx_tready_int <= '0';
              if (tx_tvalid_flag = '1') then
							  rx_state <= RX_DLY_STATE;
							else
                rx_state <= RX_WAIT_STATE;
							end if;
            else
              rx_state <= RX_MEM_RD64_DW1DW2;
            end if;

          when RX_MEM_WR64_DW1DW2 =>

            if (m_axis_rx_tvalid = '1') then
              bwr_addr_o                                      <= (region_select(1 downto 0) & m_axis_rx_tdata(29 downto 2));
              bwr_data_o(conv_integer(DW_wr_Cnt(4 downto 0))) <= m_axis_rx_tdata(95 downto 64);
              m_axis_rx_tready_int                            <= '0';
							if (bcall_run_i = '0') then
                wr_en_o  <= '1';
							  rx_state <= RX_WAIT_STATE;
							else
							  rx_state <= RX_IO_MEM_WR_WAIT_STATE;
							end if;
            else
              rx_state <= RX_MEM_WR64_DW1DW2;
            end if;

          when RX_MSG_SIGNALING =>                                              -- start message processing

            msg_signaling_o <= '1';
            rx_state        <= RX_RST_STATE;

          when RX_DLY_STATE =>

						RD_on      <= '1';
						brd_strt_o <= '1';
            rx_state   <= RX_WAIT_STATE;

          when RX_WAIT_STATE =>

            wr_en_o <= '0';

            if ((tlp_type = RX_MEM_WR32_FMT_TYPE) and
						    (((wr_busy_ep_i = '0') and (E_Bone_trn_l = '0')) or ((wr_busy_eb_i = '0') and (E_Bone_trn_l = '1')))) then

              WR_on                <= '0';
              bwr_strt_o           <= '0';
              m_axis_rx_tready_int <= '1';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_IO_WR32_FMT_TYPE) and
						       (((wr_busy_ep_i = '0') and (E_Bone_trn_l = '0')) or ((wr_busy_eb_i = '0') and (E_Bone_trn_l = '1')))) then

              WR_on                <= '0';
              bwr_strt_o           <= '0';
              m_axis_rx_tready_int <= '1';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_MEM_WR64_FMT_TYPE) and
						       (((wr_busy_ep_i = '0') and (E_Bone_trn_l = '0')) or ((wr_busy_eb_i = '0') and (E_Bone_trn_l = '1')))) then

              WR_on                <= '0';
              bwr_strt_o           <= '0';
              m_axis_rx_tready_int <= '1';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_MEM_RD32_FMT_TYPE) and (compl_done_i = '1')) then

              m_axis_rx_tready_int <= '1';
							req_compl            <= '0';
							req_compl_wd         <= '0';
							RD_on                <= '0';
							brd_strt_o           <= '0';
							tx_tvalid_flag       <= '0';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_IO_RD32_FMT_TYPE) and (compl_done_i = '1')) then

              m_axis_rx_tready_int <= '1';
							req_compl            <= '0';
							req_compl_wd         <= '0';
							RD_on                <= '0';
							brd_strt_o           <= '0';
							tx_tvalid_flag       <= '0';
              rx_state             <= RX_RST_STATE;

            elsif ((tlp_type = RX_MEM_RD64_FMT_TYPE) and (compl_done_i = '1')) then

              m_axis_rx_tready_int <= '1';
							req_compl            <= '0';
							req_compl_wd         <= '0';
							RD_on                <= '0';
							brd_strt_o           <= '0';
							tx_tvalid_flag       <= '0';
              rx_state             <= RX_RST_STATE;

            else

              rx_state <= RX_WAIT_STATE;

            end if;

          when others =>

					  rx_state <= RX_WAIT_STATE;

        end case;

      end if;

    end process;

---------------------------------------------------------------------------------------------------------------
end generate EP_RX_128B;
---------------------------------------------------------------------------------------------------------------

end ep_rx_rtl;
