-------------------------------------------------------------------------------
--
-- E-Bone Master#0/Slave#0 to PCI_Express Endpoint interface
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--
--------------------------------------------------------------------------------
--
-- Description:
-- ------------
-- E-Bone Master#0/Slave#0 Package file
-- PCI_Express Endpoint interface for 7 series and Ultra-Scale FPGAs
-- Types and components declarations
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.ebs_pkg.all;

package ep_comp_pkg is

subtype std1          is std_logic_vector(0 downto 0);
subtype std10         is std_logic_vector(9 downto 0);
subtype std12         is std_logic_vector(11 downto 0);
subtype std14         is std_logic_vector(13 downto 0);
subtype std24         is std_logic_vector(23 downto 0);

type std16_x32        is array(0 to 31) of std16;
type std32_x2         is array(0 to 1) of std32;
type std32_x4         is array(0 to 3) of std32;
type std32_x8         is array(0 to 7) of std32;
type std32_x16        is array(0 to 15) of std32;
type std32_x32        is array(0 to 31) of std32;
type std64_x16        is array(0 to 15) of std64;

constant std1_null    : std1    := (others => '0');
constant std8_null    : std8    := (others => '0');
constant std10_null   : std10   := (others => '0');
constant std12_null   : std12   := (others => '0');
constant std14_null   : std14   := (others => '0');
constant std16_null   : std16   := (others => '0');
constant std24_null   : std24   := (others => '0');
constant std32_null   : std32   := (others => '0');
constant std64_null   : std64   := (others => '0');
constant DEPTH_FIFO_S : NATURAL := 64;
constant MAX_WIDTH    : NATURAL := 256;

------------------------------------------------------------------------------------------------------------------------
component ep_assert                                                       -- Irrelevant generic parameters cases control
------------------------------------------------------------------------------------------------------------------------
generic (
  EB_CLK             : REAL := 8.0;                                       -- E-Bone clock period in ns
	EBFT_DWIDTH        : NATURAL := 32;                                     -- FT data width
  TYPE_DESC          : NATURAL := 16#00000000#;                           -- Type description application and/or target dependant
	TRGT_DESC          : NATURAL := 16#00#;                                 -- Target dependant
	LINK_SPEED         : INTEGER := 1;                                      -- PCI-Express EndPoint Link Speed (1=2.5G, 2=5G)
	LINK_WIDTH         : INTEGER := 1                                       -- PCI-Express EndPoint Lane numbers (1,4,8)
);

end component;

-----------------------------------------------------------------------------------------------------------------------------------------
component ep_reg                                                          -- Endpoint BADR0 Memory and Control/Status registers component
-----------------------------------------------------------------------------------------------------------------------------------------
generic (
  BADR0_MEM                      : INTEGER := 1;
	WATCH_DOG                      : INTEGER := 1;
  AXI_DWIDTH                     : NATURAL := 32;
	TRGT_DESC                      : NATURAL := 16#00#;
	TYPE_DESC                      : NATURAL := 16#00000000#;
	HARD_VERS                      : NATURAL := 16#00000000#;
	DEVICE_ID                      : BIT_VECTOR := X"EB01"
);

port (
  clk                            : in std_logic;
  trn_rst                        : in std_logic;
  trn_lnk_up_n                   : in std_logic;

  rd_addr_i                      : in std_logic_vector(9 downto 0);
  rd_en_i                        : in std_logic;
  rd_data_o                      : out std_logic_vector(31 downto 0);

  wr_addr_i                      : in std_logic_vector(9 downto 0);
  wr_data_i                      : in std_logic_vector(31 downto 0);
  wr_en_i                        : in std_logic;

  cfg_bus_number                 : in std_logic_vector(7 downto 0);
  cfg_device_number              : in std_logic_vector(4 downto 0);
  cfg_function_number            : in std_logic_vector(2 downto 0);
  cfg_status                     : in std_logic_vector(15 downto 0);
  cfg_command                    : in std_logic_vector(15 downto 0);
  cfg_dstatus                    : in std_logic_vector(15 downto 0);
  cfg_dcommand                   : in std_logic_vector(15 downto 0);
	cfg_dcommand2                  : in std_logic_vector(15 downto 0);
  cfg_lstatus                    : in std_logic_vector(15 downto 0);
  cfg_lcommand                   : in std_logic_vector(15 downto 0);

  cfg_interrupt_do               : in std_logic_vector(7 downto 0);
  cfg_interrupt_mmenable         : in std_logic_vector(2 downto 0);
  cfg_interrupt_msienable        : in std_logic;
  cfg_interrupt_msixenable       : in std_logic;
  cfg_interrupt_msixfm           : in std_logic;

  pl_initial_link_width          : in std_logic_vector(2 downto 0);
  pl_lane_reversal_mode          : in std_logic_vector(1 downto 0);
  pl_link_gen2_capable           : in std_logic;
  pl_link_partner_gen2_supported : in std_logic;
  pl_link_upcfg_capable          : in std_logic;
  pl_ltssm_state                 : in std_logic_vector(5 downto 0);
  pl_received_hot_rst            : in std_logic;
  pl_sel_link_rate               : in std_logic;
  pl_sel_link_width              : in std_logic_vector(1 downto 0);
	pl_phy_lnk_up                  : in std_logic;
  pl_tx_pm_state                 : in std_logic_vector(2 downto 0);
  pl_rx_pm_state                 : in std_logic_vector(1 downto 0);
  pl_directed_change_done        : in std_logic;
  pl_directed_link_auton         : out std_logic;
  pl_directed_link_change        : out std_logic_vector(1 downto 0);
  pl_directed_link_speed         : out std_logic;
  pl_directed_link_width         : out std_logic_vector(1 downto 0);
  pl_upstream_prefer_deemph      : out std_logic;

  cfg_read_req_o                 : out std_logic;
	cfgs_regs_i                    : in std32_x32;

  INtr_service_o                 : out std_logic;
	INtr_disable_o                 : out std_logic;
	INtr_enable_o                  : out std_logic;
	INtr_TLP_end_o                 : out std_logic;
	INtr_B_calls_o                 : out std_logic;
	INtr_Rqst_nmb_o                : out std_logic_vector(7 downto 0);

  msg_signaling_i                : in std_logic;
	msg_code_i                     : in std_logic_vector(7 downto 0);
	msg_data_lsb_i                 : in std_logic_vector(31 downto 0);
	msg_data_msb_i                 : in std_logic_vector(31 downto 0);

  requester_fmt_o                : out std_logic;
  requester_leng_o               : out std_logic_vector(9 downto 0);

  TLP_end_Rqst_i                 : in std_logic;
	bcall_reg_i                    : in std_logic_vector(31 downto 0);
  B_calls_Rqst_o                 : out std_logic;
  tlp_tx_addr                    : in std_logic_vector(AXI_DWIDTH - 1 downto 0);

  FT_on_i                        : in std_logic;
  soft_wd_o                      : out std_logic;
  soft_reset_o                   : out std_logic;
  status_led_o                   : out std_logic_vector(1 downto 0);
  ebone_reset_o                  : out std_logic;
	bcall_run_i                    : in std_logic;

  err_drop_count                 : in std_logic_vector(14 downto 0);
	err_drop_ovfl                  : in std_logic;
	tdst_rdy_count                 : in std_logic_vector(14 downto 0);
	tdst_rdy_ovfl                  : in std_logic;
	req_len_sup_count              : in std_logic_vector(15 downto 0);
	req_rx_valid_count             : in std_logic_vector(31 downto 0);
	req_tx_valid_count             : in std_logic_vector(31 downto 0);
  ext_ctrl_src_i                 : in std_logic;

  rg_probe_o                     : out std_logic_vector(3 downto 0);
  prob_mux_o                     : out std_logic_vector(19 downto 0);

  my_payload_size_o              : out std_logic_vector(2 downto 0);
  user_cnst_i                    : in std_logic_vector(15 downto 0)
);
end component;

--------------------------------------------------------------------------------------------------------------------------------------
component ep_mem_access                                                   -- Endpoint local registers and memory access Unit component
--------------------------------------------------------------------------------------------------------------------------------------
generic (
  BADR0_MEM                      : INTEGER := 1;
	WATCH_DOG                      : INTEGER := 1;
	AXI_DWIDTH                     : NATURAL := 32;
	TRGT_DESC                      : NATURAL := 16#00#;
	TYPE_DESC                      : NATURAL := 16#00000000#;
	HARD_VERS                      : NATURAL := 16#00000000#;
	DEVICE_ID                      : BIT_VECTOR := X"EB01"
);

port (
  clk                            : in std_logic;
  trn_rst                        : in std_logic;
  trn_lnk_up_n                   : in std_logic;

  brd_addr_i                     : in std_logic_vector(29 downto 0);
  brd_be_i                       : in std_logic_vector(3 downto 0);
  int_brd_data_o                 : out std32_x32;

  bwr_addr_i                     : in std_logic_vector(29 downto 0);
  bwr_be_i                       : in std_logic_vector(7 downto 0);
  bwr_data_i                     : in std32_x32;
  wr_en_i                        : in std_logic;
	breq_len_i                     : in std_logic_vector(9 downto 0);
  wr_busy_ep_o                   : out std_logic;

  cfg_bus_number                 : in std_logic_vector(7 downto 0);
  cfg_device_number              : in std_logic_vector(4 downto 0);
  cfg_function_number            : in std_logic_vector(2 downto 0);
  cfg_status                     : in std_logic_vector(15 downto 0);
  cfg_command                    : in std_logic_vector(15 downto 0);
  cfg_dstatus                    : in std_logic_vector(15 downto 0);
  cfg_dcommand                   : in std_logic_vector(15 downto 0);
	cfg_dcommand2                  : in std_logic_vector(15 downto 0);
  cfg_lstatus                    : in std_logic_vector(15 downto 0);
  cfg_lcommand                   : in std_logic_vector(15 downto 0);

  cfg_interrupt_do               : in std_logic_vector(7 downto 0);
  cfg_interrupt_mmenable         : in std_logic_vector(2 downto 0);
  cfg_interrupt_msienable        : in std_logic;
  cfg_interrupt_msixenable       : in std_logic;
  cfg_interrupt_msixfm           : in std_logic;

  pl_initial_link_width          : in std_logic_vector(2 downto 0);
  pl_lane_reversal_mode          : in std_logic_vector(1 downto 0);
  pl_link_gen2_capable           : in std_logic;
  pl_link_partner_gen2_supported : in std_logic;
  pl_link_upcfg_capable          : in std_logic;
  pl_ltssm_state                 : in std_logic_vector(5 downto 0);
  pl_received_hot_rst            : in std_logic;
  pl_sel_link_rate               : in std_logic;
  pl_sel_link_width              : in std_logic_vector(1 downto 0);
	pl_phy_lnk_up                  : in std_logic;
  pl_tx_pm_state                 : in std_logic_vector(2 downto 0);
  pl_rx_pm_state                 : in std_logic_vector(1 downto 0);
  pl_directed_change_done        : in std_logic;
  pl_directed_link_auton         : out std_logic;
  pl_directed_link_change        : out std_logic_vector(1 downto 0);
  pl_directed_link_speed         : out std_logic;
  pl_directed_link_width         : out std_logic_vector(1 downto 0);
  pl_upstream_prefer_deemph      : out std_logic;

  cfg_read_req_o                 : out std_logic;
	cfgs_regs_i                    : in std32_x32;

  INtr_service_o                 : out std_logic;
	INtr_disable_o                 : out std_logic;
	INtr_enable_o                  : out std_logic;
	INtr_TLP_end_o                 : out std_logic;
	INtr_B_calls_o                 : out std_logic;
	INtr_Rqst_nmb_o                : out std_logic_vector(7 downto 0);

  msg_signaling_i                : in std_logic;
	msg_code_i                     : in std_logic_vector(7 downto 0);
	msg_data_lsb_i                 : in std_logic_vector(31 downto 0);
	msg_data_msb_i                 : in std_logic_vector(31 downto 0);

  requester_fmt_o                : out std_logic;
  requester_leng_o               : out std_logic_vector(9 downto 0);

  TLP_end_Rqst_i                 : in std_logic;
	bcall_reg_i                    : in std_logic_vector(31 downto 0);
	B_calls_Rqst_o                 : out std_logic;
	tlp_tx_addr                    : in std_logic_vector(AXI_DWIDTH - 1 downto 0);

  FT_on_i                        : in std_logic;
  soft_wd_o                      : out std_logic;
  soft_reset_o                   : out std_logic;
  status_led_o                   : out std_logic_vector(1 downto 0);
  ebone_reset_o                  : out std_logic;
	bcall_run_i                    : in std_logic;

  err_drop_count                 : in std_logic_vector(14 downto 0);
	err_drop_ovfl                  : in std_logic;
	tdst_rdy_count                 : in std_logic_vector(14 downto 0);
	tdst_rdy_ovfl                  : in std_logic;
	req_len_sup_count              : in std_logic_vector(15 downto 0);
	req_rx_valid_count             : in std_logic_vector(31 downto 0);
	req_tx_valid_count             : in std_logic_vector(31 downto 0);
	ext_ctrl_src_i                 : in std_logic;

  mm_probe_o                     : out std_logic_vector(3 downto 0);
  prob_mux_o                     : out std_logic_vector(19 downto 0);

  my_payload_size_o              : out std_logic_vector(2 downto 0);
  user_cnst_i                    : in std_logic_vector(15 downto 0)
);
end component;

----------------------------------------------------------------------------------------------------------------
component ep_tx_engine                                                     -- Local-Link Transmit Unit component
----------------------------------------------------------------------------------------------------------------
generic (
	EBFT_DWIDTH           : NATURAL := 32                                    -- FT data width
);

port (
  clk                   : in std_logic;                                    -- User clock from the endpoint module
  trn_rst               : in std_logic;                                    -- Global active high reset signal

  tx_buf_av             : in std_logic_vector(5 downto 0);                 -- Transmit Buffers Available
  tx_cfg_req            : in std_logic;                                    -- Transmit Configuration Request: Asserted when the core is ready to transmit Conf. Completion or internally generated TLP.
  tx_err_drop           : in std_logic;                                    -- Transmit Error Drop: Indicates that the core discarded a packet because of length violation
  s_axis_tx_tready      : in std_logic;                                    -- Transmit Destination Ready: Indicates that the core is ready to accept data on s_axis_tx_tdata
  s_axis_tx_tdata       : out std_logic_vector((EBFT_DWIDTH - 1) downto 0);      -- Transmit Data: Packet data to be transmitted
  s_axis_tx_tkeep       : out std_logic_vector((EBFT_DWIDTH / 8 - 1) downto 0);  -- Transmit Data Strobe: Determines which data bytes are valid on s_axis_tx_tdata
  s_axis_tx_tlast       : out std_logic;                                   -- Transmit End-of-Frame (EOF): Signals the end of a packet
  s_axis_tx_tvalid      : out std_logic;                                   -- Transmit Source Ready: Indicates that the User Application is presenting valid data on s_axis_tx_tdata
  s_axis_tx_tuser       : out std_logic_vector(3 downto 0);                -- Transmit Source Discontinue: Can be asserted any time starting on the first cycle after SOF

  req_compl_i           : in std_logic;                                    -- Indicates completion required
  req_compl_wd_i        : in std_logic;                                    -- asserted indicates to generate a completion WITH data
  compl_done_o          : out std_logic;                                   -- Completion acknowledge

  req_tc_i              : in std_logic_vector(2 downto 0);                 -- Header field TC (Traffic Class)
  req_td_i              : in std_logic;                                    -- Header field TD (TLP Digest present)
  req_ep_i              : in std_logic;                                    -- Header field EP (TLP poisoned)
  req_attr_i            : in std_logic_vector(1 downto 0);                 -- Header field Attributes
  req_len_i             : in std_logic_vector(9 downto 0);                 -- Header field Data Length (Data Payload in DW)
  req_rid_i             : in std_logic_vector(15 downto 0);                -- Header field Requestor ID
  req_tag_i             : in std_logic_vector(7 downto 0);                 -- Header field Tag
  req_be_i              : in std_logic_vector(7 downto 0);                 -- Header field Byte Enables
  req_addr_i            : in std_logic_vector(31 downto 0);                -- Header field Address
	req_tx_valid_o        : out std_logic;                                   -- High for one system clock period when TX_MWR_FMT_TYPE or TX_MWR64_FMT_TYPE

  brd_addr_o            : out std_logic_vector(29 downto 0);               -- Header field req_addr_i(31 downto 2)
  brd_be_o              : out std_logic_vector(3 downto 0);                -- Header field Byte Enable req_be_i(3 downto 0)

  int_brd_data_i        : in std32_x32;                                    -- Internal Memory Read Data array
	ext_brd_data_i        : in std32_x32;                                    -- External Memory Read Data array (E_Bone)
  ext_brd_done_i        : in std_logic;                                    -- External Memory Read Burst done (E_Bone)

  completer_id_i        : in std_logic_vector(15 downto 0);                -- cfg_bus_number & cfg_device_number & cfg_function_number;
  cfg_bus_mstr_enable_i : in std_logic;                                    -- cfg_command(2) from the Configuration Space Header Command register
	cfg_dcommand_i        : in std_logic_vector(15 downto 0);                -- Device Command register from the Configuration Space Header

	E_Bone_trn_i          : in std_logic;                                    -- E_Bone external transaction request
	tx_ft_ready_i         : in std_logic;                                    -- External TLP transmit request
	tx_ft_ready_o         : out std_logic;                                   -- Internal TLP transmit request for missing bytes transfer
  requester_fmt_i       : in std_logic;                                    -- 64-bit address routing Transmit TLP when 1 otherwise 32-bit
	requester_leng_i      : in std_logic_vector(9 downto 0);                 -- DW (4-byte) length field encoding corresponding to one TLP transmit operation
  eb_eof_i              : in std_logic;                                    -- FT slave end of frame
  eb_ft_data_i          : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);   -- TLP transmit data from fast transmitter
  eb_ft_addr_i          : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);   -- TLP transmit address from fast transmitter
	eb_data_size_i        : in std_logic_vector(7 downto 0);                 -- TLP data size valid
	eb_last_size_i        : in std_logic_vector(7 downto 0);                 -- TLP last size valid
	eb_byte_size_i        : in std_logic_vector(15 downto 0);                -- TLP byte count
	TLP_end_Rqst_i        : in std_logic;                                    -- TLP end of transfer => interrupt module
  FT_on_i               : in std_logic;                                    -- FT has started when 1
  FT_clr_o              : out std_logic;                                   -- FT completed from Tx engine
	tlp_tx_addr           : out std_logic_vector(EBFT_DWIDTH - 1 downto 0);  -- TLP transmit address in DMA mode
	tx_ft_delay_o         : out std_logic;                                   -- Next FT delayed
  tlp_delayed_o         : out std_logic;                                   -- TLP running

  ext_addr_src_i        : in std_logic_vector(63 downto 0);                -- External addr source input
  ext_data_src_i        : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);   -- External data source input
	ext_data_flg_i        : in std_logic;                                    -- External data source input ready for DMA transfer when 1
	ext_data_ack_o        : out std_logic;                                   -- External data source acknowledge output

  tx_probe_o            : out std_logic_vector(3 downto 0)                 -- Probe output signals linked to Tx engine
);
end component;

---------------------------------------------------------------------------------------------------------------
component ep_rx_engine                                                     -- Local-Link Receive Unit component
---------------------------------------------------------------------------------------------------------------
generic (
	EBFT_DWIDTH           : NATURAL := 32                                    -- FT data width
);

port (
  clk                   : in std_logic;                                    -- User clock from the endpoint module
  trn_rst               : in std_logic;                                    -- Global active high reset signal

  m_axis_rx_tdata       : in std_logic_vector((EBFT_DWIDTH - 1) downto 0);       -- Receive Data: Packet data being received
  m_axis_rx_tkeep       : in std_logic_vector((EBFT_DWIDTH / 8 - 1) downto 0);   -- Receive Data Strobe: Determines which data bytes are valid on m_axis_rx_tdata
  m_axis_rx_tlast       : in std_logic;                                    -- Receive End-of-Frame (EOF): Signals the end of a packet
  m_axis_rx_tvalid      : in std_logic;                                    -- Receive Source Ready: Indicates that the core is presenting valid data on m_axis_rx_tdata
  m_axis_rx_tready      : out std_logic;                                   -- Receive Destination Ready: Indicates that the User Application is ready to accept data on m_axis_rx_tdata
  m_axis_rx_tuser       : in std_logic_vector(21 downto 0);                -- Receive Packet Status
  s_axis_tx_tvalid      : in std_logic;                                    -- Transmit Source Ready: Indicates that the User Application is presenting valid data on s_axis_tx_tdata

  npck_prsnt_o          : out std_logic;                                   -- Indicates the start of a new packet header in m_axis_rx_tdata
  req_compl_o           : out std_logic;                                   -- Indicates completion required
  req_compl_wd_o        : out std_logic;                                   -- asserted indicates to generate a completion WITH data
  compl_done_i          : in  std_logic;                                   -- Completion acknowledge
  tx_tvalid_flag_o      : out std_logic;                                   -- Transmit data valid flag

  req_tc_o              : out std_logic_vector(2 downto 0);                -- Header field TC (Traffic Class)
  req_td_o              : out std_logic;                                   -- Header field TD (TLP Digest present)
  req_ep_o              : out std_logic;                                   -- Header field EP (TLP poisoned)
  req_attr_o            : out std_logic_vector(1 downto 0);                -- Header field Attributes
  req_len_o             : out std_logic_vector(9 downto 0);                -- Header field Data Length (Data Payload in DW)
  req_rid_o             : out std_logic_vector(15 downto 0);               -- Header field Requestor ID
  req_tag_o             : out std_logic_vector(7 downto 0);                -- Header field Tag
  req_be_o              : out std_logic_vector(7 downto 0);                -- Header field Byte Enables
  req_addr_o            : out std_logic_vector(31 downto 0);               -- Header field Address
  req_len_sup_o         : out std_logic;                                   -- High for one system clock period when req_len > 1
  req_rx_valid_o        : out std_logic;                                   -- High for one system clock period when RX_MEM_RD32_FMT_TYPE

  brd_strt_o            : out std_logic;                                   -- Memory Read Burst start
  bwr_strt_o            : out std_logic;                                   -- Memory Write Burst start
  bwr_addr_o            : out std_logic_vector(29 downto 0);               -- Memory Write Address
  bwr_be_o              : out std_logic_vector(7 downto 0);                -- Memory Write Byte Enable
  bwr_data_o            : out std32_x32;                                   -- Memory Write Data
  breq_len_o            : out std_logic_vector(9 downto 0);                -- Memory Write payload data length in DW

  wr_en_o               : out std_logic;                                   -- Memory Write Enable
  wr_busy_ep_i          : in std_logic;                                    -- Memory Write Busy
	wr_busy_eb_i          : in std_logic;                                    -- E_Bone write cycle end when low
	bcall_run_i           : in std_logic;                                    -- Broad-call process running
	FT_on_i               : in std_logic;                                    -- FT has started when 1
	RD_on_o               : out std_logic;                                   -- MRd TLP running
	WR_on_o               : out std_logic;                                   -- MWr TLP running
	eb_bft_i              : in std_logic;                                    -- busy FT
  eb_eof_i              : in std_logic;                                    -- FT slave end of frame

  E_Bone_trn_o          : out std_logic;                                   -- E_Bone external transaction request
  Type_Offset_desc_o    : out std_logic_vector(31 downto 0);               -- Type/Offset descriptor word for E-Bone interfacing

  msg_signaling_o       : out std_logic;                                   -- TLP messages received
	msg_code_o            : out std_logic_vector(7 downto 0);                -- Message Code
	msg_data_lsb_o        : out std_logic_vector(31 downto 0);               -- First and
	msg_data_msb_o        : out std_logic_vector(31 downto 0);               -- Second dwords of message write TLP

  rx_probe_o            : out std_logic_vector(3 downto 0)                 -- Probe output signals linked to Rx engine
);
end component;

------------------------------------------------------------------------------------------------------------------
component ep_interrupt                                                    -- Interrupt processing module component
------------------------------------------------------------------------------------------------------------------
generic (
	BC_PERIOD                : BIT_VECTOR := X"1"                           -- Broad-Call period (0:1us, 1:10us, 2:100us, 3:1ms)
);

port (
  clk                      : in std_logic;                                -- 125MHz user clock from the endpoint module
	trn_rst                  : in std_logic;                                -- Global active high reset signal

  cfg_interrupt            : out std_logic;                               -- Active high interrupt request signal
  cfg_interrupt_rdy        : in std_logic;                                -- Active high interrupt request signal
  cfg_interrupt_assert     : out std_logic;                               -- Legacy interrupt asserted when high
  cfg_interrupt_di         : out std_logic_vector(7 downto 0);            -- Interrupt data in for MSI (Message Signaling Interrupt)
  cfg_interrupt_do         : in std_logic_vector(7 downto 0);             -- Interrupt data out, LSB message data field in the MSI structure
  cfg_interrupt_mmenable   : in std_logic_vector(2 downto 0);             -- Interrupt multiple Message enable in the MSI structure
  cfg_interrupt_msienable  : in std_logic;                                -- Interrupt MSI (Message Signaling Interrupt) enable
  cfg_interrupt_msixenable : in std_logic;                                -- Interrupt MSI-X (Message Signaling Interrupt-X) enable
  cfg_interrupt_msixfm     : in std_logic;                                -- Configuration interrupt MSI-X mask

	INtr_service_i           : in std_logic;                                -- Interrupt acknowledge (direct clear on interrupt status register read)
	INtr_disable_i           : in std_logic;                                -- cfg_command(10) from the Configuration Space Header Command register
	INtr_enable_i            : in std_logic;                                -- Global interrupt enable from BADR0 control register 0
	INtr_TLP_end_i           : in std_logic;                                -- Interrupt on TLP end of transfer enable
	INtr_B_calls_i           : in std_logic;                                -- Interrupt on Broad-call enable
	INtr_Rqst_nmb_i          : in std_logic_vector(7 downto 0);             -- Legacy interrupt value
	ext_intr_src_i           : in std_logic;                                -- External interrrupt source input
  INtr_gate_o              : out std_logic;                               -- Interrupt acknowledge running

  TLP_end_Rqst_i           : in std_logic;                                -- Interrupt on TLP end of transfer
  B_calls_Rqst_i           : in std_logic;                                -- Interrupt on Broad-call

  one_us_heartbeat_i       : in std_logic;                                -- One us pulse out trn_clk synchronous
  it_probe_o               : out std_logic_vector(3 downto 0)             -- Test probes from this module
);
end component;

----------------------------------------------------------------------------------------------------------------
component ep_cfg_rd                                                       -- configuration read module component
----------------------------------------------------------------------------------------------------------------
port (
  clk                     : in std_logic;                                 -- 125MHz user clock from the endpoint module
	trn_rst                 : in std_logic;                                 -- Global active high reset signal

  cfg_do                  : in std_logic_vector(31 downto 0);             -- CFG interface data from the core
  cfg_rd_wr_done          : in std_logic;                                 -- CFG interface successful register access operation
  cfg_di                  : out std_logic_vector(31 downto 0);            -- CFG interface data to the core
  cfg_byte_en             : out std_logic_vector(3 downto 0);             -- CFG byte enable when low
  cfg_dwaddr              : out std_logic_vector(9 downto 0);             -- CFG interface DWORD address
  cfg_wr_en               : out std_logic;                                -- CFG interface register write enable
  cfg_rd_en               : out std_logic;                                -- CFG interface register read enable

	cfg_read_req_i          : in std_logic;                                 -- Configuration Space Header read request
	cfgs_regs_o             : out std32_x32                                 -- Configuration Space Header registers array
);
end component;

--------------------------------------------------------------------------------------------------------------------
component ep_to_fnct                                                      -- Endpoint functions top module component
--------------------------------------------------------------------------------------------------------------------
generic (
  EBFT_DWIDTH                    : NATURAL := 32;                         -- FT data width
	BADR0_MEM                      : INTEGER := 1;                          -- 2Kbyte BADR0 Block Ram Memory implementation when 1
  WATCH_DOG                      : INTEGER := 1;                          -- Watch dog process implementation when 1
  AXI_DWIDTH                     : NATURAL := 32;                         -- PCI-Express EndPoint TRN/AXI interface data width
	BC_PERIOD                      : BIT_VECTOR := X"1";                    -- Broad-Call period (0:1us, 1:10us, 2:100us, 3:1ms)
  TRGT_DESC                      : NATURAL := 16#00#;                     -- Target dependant
	TYPE_DESC                      : NATURAL := 16#00000000#;               -- Type description application and/or target dependant
	HARD_VERS                      : NATURAL := 16#00000000#;               -- Hardware version implemented
	DEVICE_ID                      : BIT_VECTOR := X"EB01"                  -- Device ID = E-Bone 01
);

port (
  clk                            : in std_logic;
  trn_rst                        : in std_logic;
  trn_lnk_up_n                   : in std_logic;

-- Tx AXI4_Stream interface
  tx_buf_av                      : in std_logic_vector(5 downto 0);       -- Transmit Buffers Available
  tx_cfg_req                     : in std_logic;                          -- Transmit Configuration Request: Asserted when the core is ready to transmit Conf. Completion or internally generated TLP.
  tx_err_drop                    : in std_logic;                          -- Transmit Error Drop: Indicates that the core discarded a packet because of length violation
  s_axis_tx_tready               : in std_logic;                          -- Transmit Destination Ready: Indicates that the core is ready to accept data on s_axis_tx_tdata
  s_axis_tx_tdata                : out std_logic_vector((EBFT_DWIDTH - 1) downto 0);      -- Transmit Data: Packet data to be transmitted
  s_axis_tx_tkeep                : out std_logic_vector((EBFT_DWIDTH / 8 - 1) downto 0);  -- Transmit Data Strobe: Determines which data bytes are valid on s_axis_tx_tdata
  s_axis_tx_tlast                : out std_logic;                         -- Transmit End-of-Frame (EOF): Signals the end of a packet
  s_axis_tx_tvalid               : out std_logic;                         -- Transmit Source Ready: Indicates that the User Application is presenting valid data on s_axis_tx_tdata
  s_axis_tx_tuser                : out std_logic_vector(3 downto 0);      -- Transmit Source Discontinue: Can be asserted any time starting on the first cycle after SOF

-- Rx AXI4_Stream interface
  m_axis_rx_tdata                : in std_logic_vector((EBFT_DWIDTH - 1) downto 0);       -- Receive Data: Packet data being received
  m_axis_rx_tkeep                : in std_logic_vector((EBFT_DWIDTH / 8 - 1) downto 0);   -- Receive Data Strobe: Determines which data bytes are valid on m_axis_rx_tdata
  m_axis_rx_tlast                : in std_logic;                          -- Receive End-of-Frame (EOF): Signals the end of a packet
  m_axis_rx_tvalid               : in std_logic;                          -- Receive Source Ready: Indicates that the core is presenting valid data on m_axis_rx_tdata
  m_axis_rx_tready               : out std_logic;                         -- Receive Destination Ready: Indicates that the User Application is ready to accept data on m_axis_rx_tdata
  m_axis_rx_tuser                : in std_logic_vector(21 downto 0);      -- Receive Packet Status

-- Configuration (CFG) Interface
  cfg_completer_id               : in std_logic_vector(15 downto 0);      -- cfg_bus_number & cfg_device_number & cfg_function_number
  cfg_bus_mstr_enable            : in std_logic;                          -- cfg_command(2) from the Configuration Space Header Command register

  cfg_do                         : in std_logic_vector(31 downto 0);      -- CFG interface data from the core
  cfg_rd_wr_done                 : in std_logic;                          -- CFG interface successful register access operation
  cfg_di                         : out std_logic_vector(31 downto 0);     -- CFG interface data to the core
  cfg_byte_en                    : out std_logic_vector(3 downto 0);      -- CFG byte enable when low
  cfg_dwaddr                     : out std_logic_vector(9 downto 0);      -- CFG interface DWORD address
  cfg_wr_en                      : out std_logic;                         -- CFG interface register write enable
  cfg_rd_en                      : out std_logic;                         -- CFG interface register read enable

  cfg_bus_number                 : in std_logic_vector(7 downto 0);       -- Provides the assigned bus number for the device, used in TLPs
  cfg_device_number              : in std_logic_vector(4 downto 0);       -- Provides the assigned device number for the device, used in TLPs
  cfg_function_number            : in std_logic_vector(2 downto 0);       -- Provides the assigned function number for the device, used in TLPs
  cfg_status                     : in std_logic_vector(15 downto 0);      -- Status register from the Configuration Space Header
  cfg_command                    : in std_logic_vector(15 downto 0);      -- Command register from the Configuration Space Header
  cfg_dstatus                    : in std_logic_vector(15 downto 0);      -- Device Status register from the Configuration Space Header
  cfg_dcommand                   : in std_logic_vector(15 downto 0);      -- Device Command register from the Configuration Space Header
	cfg_dcommand2                  : in std_logic_vector(15 downto 0);      -- Device Command register 2 from the Configuration Space Header
  cfg_lstatus                    : in std_logic_vector(15 downto 0);      -- Link Status register from the Configuration Space Header
  cfg_lcommand                   : in std_logic_vector(15 downto 0);      -- Link Command register from the Configuration Space Header

-- Interrupt (CFG) Interface
  cfg_interrupt                  : out std_logic;                         -- Active high interrupt request signal
  cfg_interrupt_rdy              : in std_logic;                          -- Active high interrupt request signal
  cfg_interrupt_assert           : out std_logic;                         -- Legacy interrupt asserted when high
  cfg_interrupt_di               : out std_logic_vector(7 downto 0);      -- Interrupt data in for MSI (Message Signaling Interrupt)
  cfg_interrupt_do               : in std_logic_vector(7 downto 0);       -- Interrupt data out, LSB message data field in the MSI structure
  cfg_interrupt_mmenable         : in std_logic_vector(2 downto 0);       -- Interrupt multiple Message enable in the MSI structure
  cfg_interrupt_msienable        : in std_logic;                          -- Interrupt MSI (Message Signaling Interrupt) enable
  cfg_interrupt_msixenable       : in std_logic;                          -- Interrupt MSI-X (Message Signaling Interrupt-X) enable
  cfg_interrupt_msixfm           : in std_logic;                          -- Configuration interrupt MSI-X mask
  TLP_end_Rqst_i                 : in std_logic;                          -- TLP end of transfer => interrupt module
  INtr_gate_o                    : out std_logic;                         -- Interrupt acknowledge running

-- Physical Layer Control and Status (PL) Interface
  pl_initial_link_width          : in std_logic_vector(2 downto 0);       -- PL link width after successful link training
  pl_lane_reversal_mode          : in std_logic_vector(1 downto 0);       -- PL lane reversal mode
  pl_link_gen2_capable           : in std_logic;                          -- PL link Gen2 (5.0 Gb/s) capable if 1
  pl_link_partner_gen2_supported : in std_logic;                          -- PL link partner Gen2 (5.0 Gb/s) capable if 1
  pl_link_upcfg_capable          : in std_logic;                          -- PL link Upconfigure capable if 1
  pl_ltssm_state                 : in std_logic_vector(5 downto 0);       -- PL Link Training and Status State machine report
  pl_received_hot_rst            : in std_logic;                          -- PL Hot reset received
  pl_sel_link_rate               : in std_logic;                          -- PL current link rate, 0: 2.5 Gb/s, 1: 5.0 Gb/s
  pl_sel_link_width              : in std_logic_vector(1 downto 0);       -- PL current link width
	pl_phy_lnk_up                  : in std_logic;                          -- PL Indicates Physical Layer Link Up Status
  pl_tx_pm_state                 : in std_logic_vector(2 downto 0);       -- PL Indicates TX Power Management State
  pl_rx_pm_state                 : in std_logic_vector(1 downto 0);       -- PL Indicates RX Power Management State
  pl_directed_change_done        : in std_logic;                          -- PL Indicates the Directed change is done
  pl_directed_link_auton         : out std_logic;                         -- PL direct autonomous link change
  pl_directed_link_change        : out std_logic_vector(1 downto 0);      -- PL direct link width and/or speed change control
  pl_directed_link_speed         : out std_logic;                         -- PL Target link speed for a directed link change operation
  pl_directed_link_width         : out std_logic_vector(1 downto 0);      -- PL Target link width for a directed link change operation
  pl_upstream_prefer_deemph      : out std_logic;                         -- PL De-emphasis control according to the link speed

  npck_prsnt_o                   : out std_logic;                         -- Indicates the start of a new packet header in m_axis_rx_tdata
  req_compl_o                    : out std_logic;
  compl_done_o                   : out std_logic;

	ext_brd_data_i                 : in std32_x32;
	ext_brd_done_i                 : in std_logic;
	brd_strt_o                     : out std_logic;
  bwr_strt_o                     : out std_logic;
  bwr_be_o                       : out std_logic_vector(7 downto 0);
  bwr_data_o                     : out std32_x32;
  breq_len_o                     : out std_logic_vector(9 downto 0);

  Type_Offset_desc_o             : out std_logic_vector(31 downto 0);
  E_Bone_trn_o                   : out std_logic;
  eb_ft_data_i                   : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);
  eb_ft_addr_i                   : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);
	eb_data_size_i                 : in std_logic_vector(7 downto 0);
	eb_last_size_i                 : in std_logic_vector(7 downto 0);
	eb_byte_size_i                 : in std_logic_vector(15 downto 0);
	eb_bft_i                       : in std_logic;
  eb_eof_i                       : in std_logic;
	tx_ft_ready_i                  : in std_logic;
	tx_ft_ready_o                  : out std_logic;
	tx_tvalid_flag_o               : out std_logic;
	INtr_B_calls_o                 : out std_logic;
	bcall_run_i                    : in std_logic;
	bcall_reg_i                    : in std_logic_vector(31 downto 0);
  RD_on_o                        : out std_logic;
  WR_on_o                        : out std_logic;
  FT_on_i                        : in std_logic;
  FT_clr_o                       : out std_logic;
  wr_busy_eb_i                   : in std_logic;
	one_us_heartbeat_i             : in std_logic;

  soft_wd_o                      : out std_logic;
  soft_reset_o                   : out std_logic;
  status_led_o                   : out std_logic_vector(1 downto 0);
  ebone_reset_o                  : out std_logic;

  err_drop_count                 : in std_logic_vector(14 downto 0);
	err_drop_ovfl                  : in std_logic;
	tdst_rdy_count                 : in std_logic_vector(14 downto 0);
	tdst_rdy_ovfl                  : in std_logic;
	req_len_sup_o                  : out std_logic;
	req_len_sup_count              : in std_logic_vector(15 downto 0);
	req_rx_valid_o                 : out std_logic;
	req_rx_valid_count             : in std_logic_vector(31 downto 0);
  req_tx_valid_o                 : out std_logic;
	req_tx_valid_count             : in std_logic_vector(31 downto 0);

-- Application extension signals
  ext_ctrl_src_i                 : in std_logic;
  ext_intr_src_i                 : in std_logic;
	ext_addr_src_i                 : in std_logic_vector(63 downto 0);
	ext_data_src_i                 : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);
	ext_data_flg_i                 : in std_logic;
	ext_data_ack_o                 : out std_logic;

  mm_probe_o                     : out std_logic_vector(3 downto 0);
  rx_probe_o                     : out std_logic_vector(3 downto 0);
  tx_probe_o                     : out std_logic_vector(3 downto 0);
  it_probe_o                     : out std_logic_vector(3 downto 0);
  prob_mux_o                     : out std_logic_vector(19 downto 0);

  my_payload_size_o              : out std_logic_vector(2 downto 0);
  user_cnst_i                    : in std_logic_vector(15 downto 0);
  tx_ft_delay_o                  : out std_logic;
  tlp_delayed_o                  : out std_logic
);
end component;

------------------------------------------------------------------------------------------------------------
component ep_to_ctrl                                                      -- Turn-off Control Unit component
------------------------------------------------------------------------------------------------------------
port (
  clk            : in std_logic;
  trn_rst        : in std_logic;

  req_compl_i    : in std_logic;
  compl_done_i   : in std_logic;

  cfg_to_turnoff : in std_logic;
  cfg_turnoff_ok : out std_logic

);
end component;

----------------------------------------------------------------------------------------------------------------------------------
component ep_app                                                          -- PCI Express endpoint top module component application
----------------------------------------------------------------------------------------------------------------------------------
generic (
  EBFT_DWIDTH                    : NATURAL := 32;                         -- FT data width
	BADR0_MEM                      : INTEGER := 1;                          -- 2Kbyte BADR0 Block Ram Memory implementation when 1
  WATCH_DOG                      : INTEGER := 1;                          -- Watch dog process implementation when 1
  AXI_DWIDTH                     : NATURAL := 32;                         -- PCI-Express EndPoint TRN/AXI interface data width
	BC_PERIOD                      : BIT_VECTOR := X"1";                    -- Broad-Call period (0:1us, 1:10us, 2:100us, 3:1ms)
  TRGT_DESC                      : NATURAL := 16#00#;                     -- Target dependant
	TYPE_DESC                      : NATURAL := 16#00000000#;               -- Type description application and/or target dependant
	HARD_VERS                      : NATURAL := 16#00000000#;               -- Hardware version implemented
	DEVICE_ID                      : BIT_VECTOR := X"EB01"                  -- Device ID = E-Bone 01
);

port (
  trn_clk                        : in std_logic;                          -- 125MHz user clock from the endpoint module
  trn_rst                        : in std_logic;                          -- Active high reset signal from the endpoint
  trn_lnk_up_n                   : in std_logic;                          -- Low when PCIe Link is Up

-- Tx AXI4_Stream interface
  tx_buf_av                      : in std_logic_vector(5 downto 0);       -- Transmit Buffers Available
  tx_cfg_req                     : in std_logic;                          -- Transmit Configuration Request: Asserted when the core is ready to transmit Conf. Completion or internally generated TLP.
  tx_err_drop                    : in std_logic;                          -- Transmit Error Drop: Indicates that the core discarded a packet because of length violation
  s_axis_tx_tready               : in std_logic;                          -- Transmit Destination Ready: Indicates that the core is ready to accept data on s_axis_tx_tdata
  s_axis_tx_tdata                : out std_logic_vector((EBFT_DWIDTH - 1) downto 0);      -- Transmit Data: Packet data to be transmitted
  s_axis_tx_tkeep                : out std_logic_vector((EBFT_DWIDTH / 8 - 1) downto 0);  -- Transmit Data Strobe: Determines which data bytes are valid on s_axis_tx_tdata
  s_axis_tx_tlast                : out std_logic;                         -- Transmit End-of-Frame (EOF): Signals the end of a packet
  s_axis_tx_tvalid               : out std_logic;                         -- Transmit Source Ready: Indicates that the User Application is presenting valid data on s_axis_tx_tdata
  s_axis_tx_tuser                : out std_logic_vector(3 downto 0);      -- Transmit Source Discontinue: Can be asserted any time starting on the first cycle after SOF
  tx_cfg_gnt                     : out std_logic;                         -- Transmit Configuration Grant: Asserted by the User Application in response to tx_cfg_req

-- Rx AXI4_Stream interface
  m_axis_rx_tdata                : in std_logic_vector((EBFT_DWIDTH - 1) downto 0);       -- Receive Data: Packet data being received
  m_axis_rx_tkeep                : in std_logic_vector((EBFT_DWIDTH / 8 - 1) downto 0);   -- Receive Data Strobe: Determines which data bytes are valid on m_axis_rx_tdata
  m_axis_rx_tlast                : in std_logic;                          -- Receive End-of-Frame (EOF): Signals the end of a packet
  m_axis_rx_tvalid               : in std_logic;                          -- Receive Source Ready: Indicates that the core is presenting valid data on m_axis_rx_tdata
  m_axis_rx_tready               : out std_logic;                         -- Receive Destination Ready: Indicates that the User Application is ready to accept data on m_axis_rx_tdata
  m_axis_rx_tuser                : in std_logic_vector(21 downto 0);      -- Receive Packet Status
  rx_np_ok                       : out std_logic;                         -- Receive Non-Posted OK
  rx_np_req                      : out std_logic;                         -- Receive Non-Posted Request

-- Flow Control
  fc_cpld                        : in std_logic_vector(11 downto 0);      -- Completion data flow control credits
  fc_cplh                        : in std_logic_vector(7 downto 0);       -- Completion header flow control credits
  fc_npd                         : in std_logic_vector(11 downto 0);      -- Non posted data flow control credits
  fc_nph                         : in std_logic_vector(7 downto 0);       -- Non posted header flow control credits
  fc_pd                          : in std_logic_vector(11 downto 0);      -- Posted data flow control credits
  fc_ph                          : in std_logic_vector(7 downto 0);       -- Posted header flow control credits
  fc_sel                         : out std_logic_vector(2 downto 0);      -- Flow control informational select

-- Configuration (CFG) Interface
  cfg_do                         : in std_logic_vector(31 downto 0);      -- CFG interface data from the core
  cfg_rd_wr_done                 : in std_logic;                          -- CFG interface successful register access operation
  cfg_di                         : out std_logic_vector(31 downto 0);     -- CFG interface data to the core
  cfg_byte_en                    : out std_logic_vector(3 downto 0);      -- CFG byte enable when low
  cfg_dwaddr                     : out std_logic_vector(9 downto 0);      -- CFG interface DWORD address
  cfg_wr_en                      : out std_logic;                         -- CFG interface register write enable
  cfg_rd_en                      : out std_logic;                         -- CFG interface register read enable

  cfg_err_cor                    : out std_logic;                         -- CFG interface correctable error
  cfg_err_ur                     : out std_logic;                         -- CFG interface unsupported request
  cfg_err_ecrc                   : out std_logic;                         -- CFG interface ECRC report error
  cfg_err_cpl_timeout            : out std_logic;                         -- CFG interface error completion timeout
  cfg_err_cpl_abort              : out std_logic;                         -- CFG interface completion aborted
	cfg_err_cpl_unexpect           : out std_logic;                         -- CFG configuration error completion unexpected
  cfg_err_posted                 : out std_logic;                         -- CFG interface error posted
  cfg_err_locked                 : out std_logic;                         -- CFG error locked when low
	cfg_pm_wake                    : out std_logic;                         -- Configuration power management event when low
	cfg_trn_pending                : out std_logic;                         -- User transaction pending when low
  cfg_err_tlp_cpl_header         : out std_logic_vector(47 downto 0);     -- CFG error TLP completion header
  cfg_err_cpl_rdy                : in std_logic;                          -- CFG interface error completion ready

  cfg_turnoff_ok                 : out std_logic;                         -- Configuration turnoff ok
  cfg_to_turnoff                 : in std_logic;                          -- Configuration to turnoff
  cfg_bus_number                 : in std_logic_vector(7 downto 0);       -- Provides the assigned bus number for the device, used in TLPs
  cfg_device_number              : in std_logic_vector(4 downto 0);       -- Provides the assigned device number for the device, used in TLPs
  cfg_function_number            : in std_logic_vector(2 downto 0);       -- Provides the assigned function number for the device, used in TLPs
  cfg_status                     : in std_logic_vector(15 downto 0);      -- Status register from the Configuration Space Header
  cfg_command                    : in std_logic_vector(15 downto 0);      -- Command register from the Configuration Space Header
  cfg_dstatus                    : in std_logic_vector(15 downto 0);      -- Device Status register from the Configuration Space Header
  cfg_dcommand                   : in std_logic_vector(15 downto 0);      -- Device Command register from the Configuration Space Header
	cfg_dcommand2                  : in std_logic_vector(15 downto 0);      -- Device Command register 2 from the Configuration Space Header
  cfg_lstatus                    : in std_logic_vector(15 downto 0);      -- Link Status register from the Configuration Space Header
  cfg_lcommand                   : in std_logic_vector(15 downto 0);      -- Link Command register from the Configuration Space Header
  cfg_pcie_link_state            : in std_logic_vector(2 downto 0);       -- PCI Express link state report
  cfg_dsn                        : out std_logic_vector(63 downto 0);     -- Configuration device serial number
  cfg_pmcsr_pme_en               : in std_logic;                          -- Power management control/status register enable bit[8]
  cfg_pmcsr_pme_status           : in std_logic;                          -- Power management control/status register status bit[15]
  cfg_pmcsr_powerstate           : in std_logic_vector(1 downto 0);       -- Power management control/status register state bits[1:0]
  cfg_received_func_lvl_rst      : in std_logic;                          -- Received Function Level Reset: Indicates when the Function Level Reset has been received

-- Interrupt (CFG) Interface
  cfg_interrupt                  : out std_logic;                         -- Active high interrupt request signal
  cfg_interrupt_rdy              : in std_logic;                          -- Active high interrupt request signal
  cfg_interrupt_assert           : out std_logic;                         -- Legacy interrupt asserted when high
  cfg_interrupt_di               : out std_logic_vector(7 downto 0);      -- Interrupt data in for MSI (Message Signaling Interrupt)
  cfg_interrupt_do               : in std_logic_vector(7 downto 0);       -- Interrupt data out, LSB message data field in the MSI structure
  cfg_interrupt_mmenable         : in std_logic_vector(2 downto 0);       -- Interrupt multiple Message enable in the MSI structure
  cfg_interrupt_msienable        : in std_logic;                          -- Interrupt MSI (Message Signaling Interrupt) enable
  cfg_interrupt_msixenable       : in std_logic;                          -- Interrupt MSI-X (Message Signaling Interrupt-X) enable
  cfg_interrupt_msixfm           : in std_logic;                          -- Configuration interrupt MSI-X mask

  cfg_completer_id               : in std_logic_vector(15 downto 0);      -- cfg_bus_number & cfg_device_number & cfg_function_number
  cfg_bus_mstr_enable            : in std_logic;                          -- cfg_command(2) from the Configuration Space Header Command register

-- Physical Layer Control and Status (PL) Interface
  pl_initial_link_width          : in std_logic_vector(2 downto 0);       -- PL link width after successful link training
  pl_lane_reversal_mode          : in std_logic_vector(1 downto 0);       -- PL lane reversal mode
  pl_link_gen2_capable           : in std_logic;                          -- PL link Gen2 (5.0 Gb/s) capable if 1
  pl_link_partner_gen2_supported : in std_logic;                          -- PL link partner Gen2 (5.0 Gb/s) capable if 1
  pl_link_upcfg_capable          : in std_logic;                          -- PL link Upconfigure capable if 1
  pl_ltssm_state                 : in std_logic_vector(5 downto 0);       -- PL Link Training and Status State machine report
  pl_received_hot_rst            : in std_logic;                          -- PL Hot reset received
  pl_sel_link_rate               : in std_logic;                          -- PL current link rate, 0: 2.5 Gb/s, 1: 5.0 Gb/s
  pl_sel_link_width              : in std_logic_vector(1 downto 0);       -- PL current link width
	pl_phy_lnk_up                  : in std_logic;                          -- PL Indicates Physical Layer Link Up Status
  pl_tx_pm_state                 : in std_logic_vector(2 downto 0);       -- PL Indicates TX Power Management State
  pl_rx_pm_state                 : in std_logic_vector(1 downto 0);       -- PL Indicates RX Power Management State
  pl_directed_change_done        : in std_logic;                          -- PL Indicates the Directed change is done
  pl_directed_link_change        : out std_logic_vector(1 downto 0);      -- PL direct link width and/or speed change control
	pl_directed_link_width         : out std_logic_vector(1 downto 0);      -- PL Target link width for a directed link change operation
  pl_directed_link_speed         : out std_logic;                         -- PL Target link speed for a directed link change operation
	pl_directed_link_auton         : out std_logic;                         -- PL direct autonomous link change
  pl_upstream_prefer_deemph      : out std_logic;                         -- PL De-emphasis control according to the link speed

-- E-Bone link related
	ext_brd_data_i                 : in std32_x32;                          -- Memory Read Data array
  ext_brd_done_i                 : in std_logic;                          -- External Memory Read Burst done (E_Bone)
	brd_strt_o                     : out std_logic;                         -- Memory Read Burst start
  bwr_strt_o                     : out std_logic;                         -- Memory Write Burst start
  bwr_be_o                       : out std_logic_vector(7 downto 0);      -- Memory Write Byte Enable
  bwr_data_o                     : out std32_x32;                         -- Memory Write Data
  breq_len_o                     : out std_logic_vector(9 downto 0);      -- Memory Write payload length

  npck_prsnt_o                   : out std_logic;                         -- Indicates the start of a new packet header in m_axis_rx_tdata
	req_compl_o                    : out std_logic;                         -- Indicates completion required
	compl_done_o                   : out std_logic;                         -- Completion acknowledge

  Type_Offset_desc_o             : out std_logic_vector(31 downto 0);     -- Type/Offset descriptor word for E-Bone interfacing
  E_Bone_trn_o                   : out std_logic;                         -- E_Bone external transaction request
  tx_ft_ready_i                  : in std_logic;                          -- External TLP transmit request
	tx_ft_ready_o                  : out std_logic;                         -- Internal TLP transmit request for missing bytes transfer
	tx_tvalid_flag_o               : out std_logic;                         -- Transmit data valid flag
  eb_ft_data_i                   : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);  -- TLP transmit data from fast transmitter
  eb_ft_addr_i                   : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);  -- TLP transmit address from fast transmitter
	eb_data_size_i                 : in std_logic_vector(7 downto 0);       -- TLP data size valid
	eb_last_size_i                 : in std_logic_vector(7 downto 0);       -- TLP last size valid
	eb_byte_size_i                 : in std_logic_vector(15 downto 0);      -- TLP byte count
	eb_bft_i                       : in std_logic;                          -- busy FT
  eb_eof_i                       : in std_logic;                          -- FT slave end of frame
	TLP_end_Rqst_i                 : in std_logic;                          -- TLP end of transfer => interrupt module
	INtr_B_calls_o                 : out std_logic;                         -- Interrupt on Broad-call enable
  INtr_gate_o                    : out std_logic;                         -- Interrupt acknowledge running
	bcall_run_i                    : in std_logic;                          -- Broad-call process running
	bcall_reg_i                    : in std_logic_vector(31 downto 0);      -- Broad-call report register
  RD_on_o                        : out std_logic;                         -- MRd TLP running
  WR_on_o                        : out std_logic;                         -- MWr TLP running
  FT_on_i                        : in std_logic;                          -- FT has started when 1
  FT_clr_o                       : out std_logic;                         -- FT completed from Tx engine
	wr_busy_eb_i                   : in std_logic;                          -- E_Bone write cycle end when low
	one_us_heartbeat_i             : in std_logic;                          -- One us pulse out trn_clk synchronous

  soft_wd_o                      : out std_logic;                         -- External watch dog reset signal
  soft_reset_o                   : out std_logic;                         -- External reset on ctrl_regs(2) write operation with Device_ID & Vendor_ID pattern
  status_led_o                   : out std_logic_vector(1 downto 0);      -- Status led for software test: 00=off, 01=red, 10=green
  ebone_reset_o                  : out std_logic;                         -- E-Bone part reset when high

  err_drop_count                 : in std_logic_vector(14 downto 0);
	err_drop_ovfl                  : in std_logic;
	tdst_rdy_count                 : in std_logic_vector(14 downto 0);
	tdst_rdy_ovfl                  : in std_logic;
	req_len_sup_o                  : out std_logic;
	req_len_sup_count              : in std_logic_vector(15 downto 0);
	req_rx_valid_o                 : out std_logic;
	req_rx_valid_count             : in std_logic_vector(31 downto 0);
  req_tx_valid_o                 : out std_logic;
	req_tx_valid_count             : in std_logic_vector(31 downto 0);

-- Application extension signals
  ext_ctrl_src_i                 : in std_logic;                                   -- External configuration control when 1
  ext_intr_src_i                 : in std_logic;                                   -- External interrrupt source input
	ext_addr_src_i                 : in std_logic_vector(63 downto 0);               -- External addr source input
	ext_data_flg_i                 : in std_logic;                                   -- External data source input ready for DMA transfer when 1
	ext_data_src_i                 : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);  -- External data source input
	ext_data_ack_o                 : out std_logic;                                  -- External data source acknowledge output

  mm_probe_o                     : out std_logic_vector(3 downto 0);
  rx_probe_o                     : out std_logic_vector(3 downto 0);
  tx_probe_o                     : out std_logic_vector(3 downto 0);
  it_probe_o                     : out std_logic_vector(3 downto 0);
  prob_mux_o                     : out std_logic_vector(19 downto 0);

  my_payload_size_o              : out std_logic_vector(2 downto 0);
  user_cnst_i                    : in std_logic_vector(15 downto 0);
  tx_ft_delay_o                  : out std_logic;                                  -- Next FT delayed
  tlp_delayed_o                  : out std_logic                                   -- TLP running
);
end component;

----------------------------------------------------------------------------------------------------------------------------
component ep_eb_link                                                      -- PCI Express Endpoint <--> E_Bone link component
----------------------------------------------------------------------------------------------------------------------------
generic (
  EB_CLK             : REAL := 8.0;                                       -- E-Bone clock period in ns
	EBFT_DWIDTH        : natural := 32;                                     -- FT data width
	BC_PERIOD          : BIT_VECTOR := X"0"                                 -- Broad-Call period (0:1us, 1:10us, 2:100us, 3:1ms)
);

port (
  trn_clk            : in std_logic;                                      -- User clock from the endpoint module
  trn_rst            : in std_logic;                                      -- Active high reset signal from the endpoint
	s_axis_tx_tready   : in std_logic;                                      -- Transmit destination ready: the core is ready to accept data when low

	ext_brd_data_o     : out std32_x32;                                     -- Memory Read Data array
	ext_brd_done_o     : out std_logic;                                     -- External Memory Read Burst done (E_Bone)
	brd_strt_i         : in std_logic;                                      -- Memory Read Burst start
  bwr_strt_i         : in std_logic;                                      -- Memory Write Burst start
  bwr_be_i           : in std_logic_vector(7 downto 0);                   -- Memory Write Byte Enable
  bwr_data_i         : in std32_x32;                                      -- Memory Write Data
  breq_len_i         : in std_logic_vector(9 downto 0);                   -- Memory Write payload length

  npck_prsnt_i       : in std_logic;                                      -- Indicates the start of a new packet header in m_axis_rx_tdata
	req_compl_i        : in std_logic;                                      -- Completion request running
	compl_done_i       : in std_logic;                                      -- Completion with data done

  Type_Offset_desc_i : in std_logic_vector(31 downto 0);                  -- Type/Offset descriptor word for E-Bone interfacing
  E_Bone_trn_i       : in std_logic;                                      -- E_Bone external transaction request
  eb_ft_data_o       : out std_logic_vector(EBFT_DWIDTH - 1 downto 0);    -- TLP transmit data from fast transmitter (fifo output)
  eb_ft_addr_o       : out std_logic_vector(EBFT_DWIDTH - 1 downto 0);    -- TLP transmit address from fast transmitter
	eb_data_size_o     : out std_logic_vector(7 downto 0);                  -- TLP data size valid
	eb_last_size_o     : out std_logic_vector(7 downto 0);                  -- TLP last size valid
	eb_byte_size_o     : out std_logic_vector(15 downto 0);                 -- TLP byte count
	TLP_end_Rqst_o     : out std_logic;                                     -- TLP end of transfer => interrupt module
  tx_ft_ready_o      : out std_logic;                                     -- External TLP dma transmit request start signal
	tx_ft_ready_i      : in std_logic;                                      -- Internal TLP transmit request for missing bytes transfer
	tx_ft_delay_i      : in std_logic;                                      -- Next FT delayed
	tx_tvalid_flag_i   : in std_logic;                                      -- Transmit data valid flag
  tlp_delayed_i      : in std_logic;                                      -- TLP running
  m_axis_rx_tvalid_i : in std_logic;                                      -- Receive Source Ready: Indicates that the core is presenting valid data on m_axis_rx_tdata
  m_axis_rx_tready_i : in std_logic;                                      -- Receive Destination Ready: Indicates that the User Application is ready to accept data on
  INtr_B_calls_i     : in std_logic;                                      -- Interrupt on Broad-call enable
  INtr_gate_i        : in std_logic;                                      -- Interrupt acknowledge running
	bcall_run_o        : out std_logic;                                     -- Broad-call process running
	bcall_reg_o        : out std_logic_vector(31 downto 0);                 -- Broad-call report register
  RD_on_i            : in std_logic;                                      -- MRd TLP running
  WR_on_i            : in std_logic;                                      -- MWr TLP running
  FT_on_o            : out std_logic;                                     -- FT has started when 1
  FT_clr_i           : in std_logic;                                      -- FT completed from Tx engine
  wr_busy_eb_o       : out std_logic;                                     -- E_Bone write cycle end when low
	one_us_heartbeat_o : out std_logic;                                     -- One us pulse out trn_clk synchronous
  ebone_reset_i      : in std_logic;                                      -- E-Bone part reset when high
  cfg_dcommand_i     : in std_logic_vector(15 downto 0);                  -- Device Command register from the Configuration Space Header

  eb_m0_clk_o        : out std_logic;                                     -- system clock
  eb_m0_rst_o        : out std_logic;                                     -- synchronous system reset
  eb_m0_brq_o        : out std_logic;                                     -- bus request
  eb_m0_bg_i         : in std_logic;                                      -- bus grant
  eb_m0_as_o         : out std_logic;                                     -- adrs strobe
  eb_m0_eof_o        : out std_logic;                                     -- end of frame
  eb_m0_aef_o        : out std_logic;                                     -- almost end of frame
  eb_m0_dat_o        : out std_logic_vector(31 downto 0);                 -- master data write
  eb_dk_i            : in std_logic;                                      -- data acknowledge
  eb_err_i           : in std_logic;                                      -- bus error
  eb_dat_i           : in std_logic_vector(31 downto 0);                  -- master data in

  eb_ft_dxt_i        : in std_logic_vector(EBFT_DWIDTH - 1 downto 0);     -- FT data in
	eb_ft_blen_i       : in std_logic_vector(15 downto 0);                  -- FT payload size in bytes
	eb_ft_dsz_i        : in std_logic_vector(7 downto 0);                   -- FT data size qualifier
	eb_ft_ss_i         : in std_logic;                                      -- FT size strobe
  eb_bmx_i           : in std_logic;                                      -- busy some master (but FT)
  eb_bft_i           : in std_logic;                                      -- busy FT
  eb_as_i            : in std_logic;                                      -- FT slave adrs strobe
  eb_eof_i           : in std_logic;                                      -- FT slave end of frame
  eb_dk_o            : out std_logic;                                     -- FT slave data acknowledge
  eb_err_o           : out std_logic;                                     -- FT slave bus error

  eb_probe_o         : out std_logic_vector(3 downto 0)                   -- Debug output probes
);
end component;

-----------------------------------------------------------------------------------------------------------------------
component ep_wd                                                           -- Endpoint watch dog for software monitoring
-----------------------------------------------------------------------------------------------------------------------
generic (
  TRGT_DESC : NATURAL := 16#00#                                           -- Target dependant
);

port (
  clk       : in  std_logic;
  trn_rst   : in  std_logic;
	rst_soft  : in  std_logic;
  soft_wd   : out std_logic;
	FT_on_i   : in std_logic
);
end component;

-----------------------------------------------------------------------------------------------------------------------
component ep_stat                                                         -- Endpoint error/retry statistical counters
-----------------------------------------------------------------------------------------------------------------------
port (
  trn_clk            : in std_logic;
  trn_rst            : in std_logic;

  ebone_reset        : in std_logic;
  tx_ft_ready        : in std_logic;
  TLP_end_Rqst       : in std_logic;
	tx_err_drop        : in std_logic;
	s_axis_tx_tready   : in std_logic;
  req_len_sup        : in std_logic;
	req_rx_valid       : in std_logic;
	req_tx_valid       : in std_logic;

  err_drop_count     : out std_logic_vector(14 downto 0);
	err_drop_ovfl      : out std_logic;
	tdst_rdy_count     : out std_logic_vector(14 downto 0);
	tdst_rdy_ovfl      : out std_logic;
	req_len_sup_count  : out std_logic_vector(15 downto 0);
	req_rx_valid_count : out std_logic_vector(31 downto 0);
	req_tx_valid_count : out std_logic_vector(31 downto 0)
);
end component;

---------------------------------------------------------------------------------------------------------------------------
component ep_dp_ram                                                       -- Hardware Description Language dual-port memory
---------------------------------------------------------------------------------------------------------------------------
generic (
  ADDR_WR_SIZE : NATURAL := 9;
	WORD_WR_SIZE : NATURAL := 32;
  ADDR_RD_SIZE : NATURAL := 9;
	WORD_RD_SIZE : NATURAL := 32
);

port (
  clka         : in std_logic;
	wea          : in std_logic;
	addra        : in std_logic_vector(ADDR_WR_SIZE - 1 downto 0);
	dina         : in std_logic_vector(WORD_WR_SIZE - 1 downto 0);
	clkb         : in std_logic;
	enb          : in std_logic;
	addrb        : in std_logic_vector(ADDR_RD_SIZE - 1 downto 0);
	doutb        : out std_logic_vector(WORD_RD_SIZE - 1 downto 0)
);
end component;

---------------------------------------------------------------------------------------------------------------------------
component ep_fifo_s                                                       -- Hardware Description Language synchronous fifo
---------------------------------------------------------------------------------------------------------------------------
generic (
  DWIDTH    : natural;                                                    -- fifo data width
  DWINP     : natural;                                                    -- port data in width
  ADEPTH    : natural;                                                    -- FIFO depth
  REGOUT    : boolean:= true;                                             -- Registered output
  RAMTYP    : string := "auto"                                            -- "auto", "block", "distributed"
);

port (
  arst      : in std_logic;
  clk       : in std_logic;
  wr_en     : in std_logic;
  wr_dat    : in std_logic_vector(DWINP - 1 downto 0);
  wr_cnt    : out std_logic_vector(Nlog2(ADEPTH) downto 0);
  wr_afull  : out std_logic;
  wr_full   : out std_logic;
  rd_en     : in std_logic;
  rd_dat    : out std_logic_vector(DWIDTH - 1 downto 0);
  rd_aempty : out std_logic;
  rd_empty  : out std_logic
);
end component;

-----------------------------------------------------------------------------------------------------------------------
component s7_pcie_ep_v3_1_core_top                                         -- Series-7 Integrated Block for PCI Express
-----------------------------------------------------------------------------------------------------------------------
generic (
  CFG_VEND_ID                                : std_logic_vector := X"10EE";
  CFG_DEV_ID                                 : std_logic_vector := X"7028";
  CFG_REV_ID                                 : std_logic_vector := X"00";
  CFG_SUBSYS_VEND_ID                         : std_logic_vector := X"10EE";
  CFG_SUBSYS_ID                              : std_logic_vector := X"EB01";
  INIT_PATTERN_WIDTH                         : integer                      := 8;
  INIT_PATTERN1                              : std_logic_vector(7 downto 0) := X"12";
  INIT_PATTERN2                              : std_logic_vector(7 downto 0) := X"9a";
  ALLOW_X8_GEN2                              : string     := "TRUE";
  PIPE_PIPELINE_STAGES                       : integer    := 1;
  AER_BASE_PTR                               : bit_vector := X"000";
  AER_CAP_ECRC_CHECK_CAPABLE                 : string     := "FALSE";
  AER_CAP_ECRC_GEN_CAPABLE                   : string     := "FALSE";
  AER_CAP_MULTIHEADER                        : string     := "FALSE";
  AER_CAP_NEXTPTR                            : bit_vector := X"000";
  AER_CAP_OPTIONAL_ERR_SUPPORT               : bit_vector := X"000000";
  AER_CAP_ON                                 : string     := "FALSE";
  AER_CAP_PERMIT_ROOTERR_UPDATE              : string     := "FALSE";

  BAR0                                       : bit_vector := X"FFFFF000";
  BAR1                                       : bit_vector := X"FFFF0000";
  BAR2                                       : bit_vector := X"FFFF0000";
  BAR3                                       : bit_vector := X"FFFC0000";
  BAR4                                       : bit_vector := X"00000000";
  BAR5                                       : bit_vector := X"00000000";

  C_DATA_WIDTH                               : integer    := 128;
  CARDBUS_CIS_POINTER                        : bit_vector := X"00000000";
  CLASS_CODE                                 : bit_vector := X"118000";
  CMD_INTX_IMPLEMENTED                       : string     := "TRUE";
  CPL_TIMEOUT_DISABLE_SUPPORTED              : string     := "FALSE";
  CPL_TIMEOUT_RANGES_SUPPORTED               : bit_vector := X"2";

  DEV_CAP_ENDPOINT_L0S_LATENCY               : integer    := 0;
  DEV_CAP_ENDPOINT_L1_LATENCY                : integer    := 7;
  DEV_CAP_EXT_TAG_SUPPORTED                  : string     := "FALSE";
  DEV_CAP_MAX_PAYLOAD_SUPPORTED              : integer    := 1;
  DEV_CAP_PHANTOM_FUNCTIONS_SUPPORT          : integer    := 0;

  DEV_CAP2_ARI_FORWARDING_SUPPORTED          : string     := "FALSE";
  DEV_CAP2_ATOMICOP32_COMPLETER_SUPPORTED    : string     := "FALSE";
  DEV_CAP2_ATOMICOP64_COMPLETER_SUPPORTED    : string     := "FALSE";
  DEV_CAP2_ATOMICOP_ROUTING_SUPPORTED        : string     := "FALSE";
  DEV_CAP2_CAS128_COMPLETER_SUPPORTED        : string     := "FALSE";
  DEV_CAP2_TPH_COMPLETER_SUPPORTED           : bit_vector := X"00";
  DEV_CONTROL_EXT_TAG_DEFAULT                : string     := "FALSE";

  DISABLE_LANE_REVERSAL                      : string     := "TRUE";
  DISABLE_RX_POISONED_RESP                   : string     := "FALSE";
  DISABLE_SCRAMBLING                         : string     := "FALSE";
  DSN_BASE_PTR                               : bit_vector := X"100";
  DSN_CAP_NEXTPTR                            : bit_vector := X"000";
  DSN_CAP_ON                                 : string     := "TRUE";

  ENABLE_MSG_ROUTE                           : bit_vector := "00000000000";
  ENABLE_RX_TD_ECRC_TRIM                     : string     := "FALSE";
  EXPANSION_ROM                              : bit_vector := X"00000000";
  EXT_CFG_CAP_PTR                            : bit_vector := X"3F";
  EXT_CFG_XP_CAP_PTR                         : bit_vector := X"3FF";
  HEADER_TYPE                                : bit_vector := X"00";
  INTERRUPT_PIN                              : bit_vector := X"1";

  LAST_CONFIG_DWORD                          : bit_vector := X"3FF";
  LINK_CAP_ASPM_OPTIONALITY                  : string     := "FALSE";
  LINK_CAP_DLL_LINK_ACTIVE_REPORTING_CAP     : string     := "FALSE";
  LINK_CAP_LINK_BANDWIDTH_NOTIFICATION_CAP   : string     := "FALSE";
  LINK_CAP_MAX_LINK_SPEED                    : integer    := 2;
  LINK_CAP_MAX_LINK_WIDTH                    : integer    := 8;

  LINK_CTRL2_DEEMPHASIS                      : string     := "FALSE";
  LINK_CTRL2_HW_AUTONOMOUS_SPEED_DISABLE     : string     := "FALSE";
  LINK_CTRL2_TARGET_LINK_SPEED               : bit_vector := X"2";
  LINK_STATUS_SLOT_CLOCK_CONFIG              : string     := "TRUE";

  LL_ACK_TIMEOUT                             : bit_vector := X"0000";
  LL_ACK_TIMEOUT_EN                          : string     := "FALSE";
  LL_ACK_TIMEOUT_FUNC                        : integer    := 0;
  LL_REPLAY_TIMEOUT                          : bit_vector := X"0000";
  LL_REPLAY_TIMEOUT_EN                       : string     := "FALSE";
  LL_REPLAY_TIMEOUT_FUNC                     : integer    := 1;

  LTSSM_MAX_LINK_WIDTH                       : bit_vector := X"08";
  MSI_CAP_MULTIMSGCAP                        : integer    := 0;
  MSI_CAP_MULTIMSG_EXTENSION                 : integer    := 0;
  MSI_CAP_ON                                 : string     := "TRUE";
  MSI_CAP_PER_VECTOR_MASKING_CAPABLE         : string     := "FALSE";
  MSI_CAP_64_BIT_ADDR_CAPABLE                : string     := "TRUE";

  MSIX_CAP_ON                                : string     := "FALSE";
  MSIX_CAP_PBA_BIR                           : integer    := 0;
  MSIX_CAP_PBA_OFFSET                        : bit_vector := X"0";
  MSIX_CAP_TABLE_BIR                         : integer    := 0;
  MSIX_CAP_TABLE_OFFSET                      : bit_vector := X"0";
  MSIX_CAP_TABLE_SIZE                        : bit_vector := X"000";

  PCIE_CAP_DEVICE_PORT_TYPE                  : bit_vector := X"0";
  PCIE_CAP_NEXTPTR                           : bit_vector := X"00";

  PM_CAP_DSI                                 : string     := "FALSE";
  PM_CAP_D1SUPPORT                           : string     := "FALSE";
  PM_CAP_D2SUPPORT                           : string     := "FALSE";
  PM_CAP_NEXTPTR                             : bit_vector := X"48";
  PM_CAP_PMESUPPORT                          : bit_vector := X"0F";
  PM_CSR_NOSOFTRST                           : string     := "TRUE";

  PM_DATA_SCALE0                             : bit_vector := X"0";
  PM_DATA_SCALE1                             : bit_vector := X"0";
  PM_DATA_SCALE2                             : bit_vector := X"0";
  PM_DATA_SCALE3                             : bit_vector := X"0";
  PM_DATA_SCALE4                             : bit_vector := X"0";
  PM_DATA_SCALE5                             : bit_vector := X"0";
  PM_DATA_SCALE6                             : bit_vector := X"0";
  PM_DATA_SCALE7                             : bit_vector := X"0";

  PM_DATA0                                   : bit_vector := X"00";
  PM_DATA1                                   : bit_vector := X"00";
  PM_DATA2                                   : bit_vector := X"00";
  PM_DATA3                                   : bit_vector := X"00";
  PM_DATA4                                   : bit_vector := X"00";
  PM_DATA5                                   : bit_vector := X"00";
  PM_DATA6                                   : bit_vector := X"00";
  PM_DATA7                                   : bit_vector := X"00";

  RBAR_BASE_PTR                              : bit_vector := X"000";
  RBAR_CAP_CONTROL_ENCODEDBAR0               : bit_vector := X"00";
  RBAR_CAP_CONTROL_ENCODEDBAR1               : bit_vector := X"00";
  RBAR_CAP_CONTROL_ENCODEDBAR2               : bit_vector := X"00";
  RBAR_CAP_CONTROL_ENCODEDBAR3               : bit_vector := X"00";
  RBAR_CAP_CONTROL_ENCODEDBAR4               : bit_vector := X"00";
  RBAR_CAP_CONTROL_ENCODEDBAR5               : bit_vector := X"00";
  RBAR_CAP_INDEX0                            : bit_vector := X"0";
  RBAR_CAP_INDEX1                            : bit_vector := X"0";
  RBAR_CAP_INDEX2                            : bit_vector := X"0";
  RBAR_CAP_INDEX3                            : bit_vector := X"0";
  RBAR_CAP_INDEX4                            : bit_vector := X"0";
  RBAR_CAP_INDEX5                            : bit_vector := X"0";
  RBAR_CAP_ON                                : string     := "FALSE";
  RBAR_CAP_SUP0                              : bit_vector := X"00001";
  RBAR_CAP_SUP1                              : bit_vector := X"00001";
  RBAR_CAP_SUP2                              : bit_vector := X"00001";
  RBAR_CAP_SUP3                              : bit_vector := X"00001";
  RBAR_CAP_SUP4                              : bit_vector := X"00001";
  RBAR_CAP_SUP5                              : bit_vector := X"00001";
  RBAR_NUM                                   : bit_vector := X"0";

  RECRC_CHK                                  : integer    := 0;
  RECRC_CHK_TRIM                             : string     := "FALSE";
  REF_CLK_FREQ                               : integer    := 0;    -- 0 - 100 MHz; 1 - 125 MHz; 2 - 250 MHz

  --KEEP_WIDTH                               : integer    := C_DATA_WIDTH / 8;

  TL_RX_RAM_RADDR_LATENCY                    : integer    := 0; 
  TL_RX_RAM_WRITE_LATENCY                    : integer    := 0;
  TL_TX_RAM_RADDR_LATENCY                    : integer    := 0;
  TL_TX_RAM_WRITE_LATENCY                    : integer    := 0;
  TL_RX_RAM_RDATA_LATENCY                    : integer    := 2;
  TL_TX_RAM_RDATA_LATENCY                    : integer    := 2;
  TRN_NP_FC                                  : string     := "TRUE";
  TRN_DW                                     : string     := "TRUE";

  UPCONFIG_CAPABLE                           : string     := "TRUE";
  UPSTREAM_FACING                            : string     := "TRUE";
  UR_ATOMIC                                  : string     := "FALSE";
  UR_INV_REQ                                 : string     := "TRUE";
  UR_PRS_RESPONSE                            : string     := "TRUE";
  USER_CLK_FREQ                              : integer    := 4;
  USER_CLK2_DIV2                             : string     := "TRUE";

  VC_BASE_PTR                                : bit_vector := X"000";
  VC_CAP_NEXTPTR                             : bit_vector := X"000";
  VC_CAP_ON                                  : string     := "FALSE";
  VC_CAP_REJECT_SNOOP_TRANSACTIONS           : string     := "FALSE";

  VC0_CPL_INFINITE                           : string     := "TRUE";
  VC0_RX_RAM_LIMIT                           : bit_vector := X"3FF";
  VC0_TOTAL_CREDITS_CD                       : integer    := 205;
  VC0_TOTAL_CREDITS_CH                       : integer    := 36;
  VC0_TOTAL_CREDITS_NPH                      : integer    := 12;
  VC0_TOTAL_CREDITS_NPD                      : integer    := 24;
  VC0_TOTAL_CREDITS_PD                       : integer    := 181;
  VC0_TOTAL_CREDITS_PH                       : integer    := 32;
  VC0_TX_LASTPACKET                          : integer    := 28;

  VSEC_BASE_PTR                              : bit_vector := X"000";
  VSEC_CAP_NEXTPTR                           : bit_vector := X"000";
  VSEC_CAP_ON                                : string     := "FALSE";

  DISABLE_ASPM_L1_TIMER                      : string     := "FALSE";
  DISABLE_BAR_FILTERING                      : string     := "FALSE";
  DISABLE_ID_CHECK                           : string     := "FALSE";
  DISABLE_RX_TC_FILTER                       : string     := "FALSE";
  DNSTREAM_LINK_NUM                          : bit_vector := X"00";

  DSN_CAP_ID                                 : bit_vector := X"0003";
  DSN_CAP_VERSION                            : bit_vector := X"1";
  ENTER_RVRY_EI_L0                           : string     := "TRUE";
  INFER_EI                                   : bit_vector := X"00";
  IS_SWITCH                                  : string     := "FALSE";

  LINK_CAP_ASPM_SUPPORT                      : integer    := 1;
  LINK_CAP_CLOCK_POWER_MANAGEMENT            : string     := "FALSE";
  LINK_CAP_L0S_EXIT_LATENCY_COMCLK_GEN1      : integer    := 7;
  LINK_CAP_L0S_EXIT_LATENCY_COMCLK_GEN2      : integer    := 7;
  LINK_CAP_L0S_EXIT_LATENCY_GEN1             : integer    := 7;
  LINK_CAP_L0S_EXIT_LATENCY_GEN2             : integer    := 7;
  LINK_CAP_L1_EXIT_LATENCY_COMCLK_GEN1       : integer    := 7;
  LINK_CAP_L1_EXIT_LATENCY_COMCLK_GEN2       : integer    := 7;
  LINK_CAP_L1_EXIT_LATENCY_GEN1              : integer    := 7;
  LINK_CAP_L1_EXIT_LATENCY_GEN2              : integer    := 7;
  LINK_CAP_RSVD_23                           : integer    := 0;
  LINK_CONTROL_RCB                           : integer    := 0;

  MSI_BASE_PTR                               : bit_vector := X"48";
  MSI_CAP_ID                                 : bit_vector := X"05";
  MSI_CAP_NEXTPTR                            : bit_vector := X"60";
  MSIX_BASE_PTR                              : bit_vector := X"9c";
  MSIX_CAP_ID                                : bit_vector := X"11";
  MSIX_CAP_NEXTPTR                           : bit_vector := X"00";

  N_FTS_COMCLK_GEN1                          : integer    := 255;
  N_FTS_COMCLK_GEN2                          : integer    := 255;
  N_FTS_GEN1                                 : integer    := 255;
  N_FTS_GEN2                                 : integer    := 255;

  PCIE_BASE_PTR                              : bit_vector := X"60";
  PCIE_CAP_CAPABILITY_ID                     : bit_vector := X"10";
  PCIE_CAP_CAPABILITY_VERSION                : bit_vector := X"2";
  PCIE_CAP_ON                                : string     := "TRUE";
  PCIE_CAP_RSVD_15_14                        : integer    := 0;
  PCIE_CAP_SLOT_IMPLEMENTED                  : string     := "FALSE";
  PCIE_REVISION                              : integer    := 2;

  PL_AUTO_CONFIG                             : integer    := 0;
  PL_FAST_TRAIN                              : string     := "FALSE";

  PCIE_EXT_CLK                               : string     := "TRUE";
  EXT_PIPE_SIM                               : string     := "FALSE";

  PCIE_EXT_GT_COMMON                         : string     := "FALSE";
  EXT_CH_GT_DRP                              : string     := "FALSE";
  TRANSCEIVER_CTRL_STATUS_PORTS              : string     := "FALSE";
  PL_INTERFACE                               : string     := "TRUE";
  CFG_MGMT_IF                                : string     := "TRUE";
  CFG_CTL_IF                                 : string     := "TRUE";
  CFG_STATUS_IF                              : string     := "TRUE";
  RCV_MSG_IF                                 : string     := "TRUE";
  CFG_FC_IF                                  : string     := "TRUE";
  SHARED_LOGIC_IN_CORE		                   : string     := "FALSE";
  EXT_PIPE_INTERFACE                         : string     := "FALSE";
  PM_BASE_PTR                                : bit_vector := X"40";
  PM_CAP_AUXCURRENT                          : integer    := 0;
  PM_CAP_ID                                  : bit_vector := X"01";
  PM_CAP_ON                                  : string     := "TRUE";
  PM_CAP_PME_CLOCK                           : string     := "FALSE";
  PM_CAP_RSVD_04                             : integer    := 0;
  PM_CAP_VERSION                             : integer    := 3;
  PM_CSR_BPCCEN                              : string     := "FALSE";
  PM_CSR_B2B3                                : string     := "FALSE";

  ROOT_CAP_CRS_SW_VISIBILITY                 : string     := "FALSE";
  SELECT_DLL_IF                              : string     := "FALSE";
  SLOT_CAP_ATT_BUTTON_PRESENT                : string     := "FALSE";
  SLOT_CAP_ATT_INDICATOR_PRESENT             : string     := "FALSE";
  SLOT_CAP_ELEC_INTERLOCK_PRESENT            : string     := "FALSE";
  SLOT_CAP_HOTPLUG_CAPABLE                   : string     := "FALSE";
  SLOT_CAP_HOTPLUG_SURPRISE                  : string     := "FALSE";
  SLOT_CAP_MRL_SENSOR_PRESENT                : string     := "FALSE";
  SLOT_CAP_NO_CMD_COMPLETED_SUPPORT          : string     := "FALSE";
  SLOT_CAP_PHYSICAL_SLOT_NUM                 : bit_vector := X"0000";
  SLOT_CAP_POWER_CONTROLLER_PRESENT          : string     := "FALSE";
  SLOT_CAP_POWER_INDICATOR_PRESENT           : string     := "FALSE";
  SLOT_CAP_SLOT_POWER_LIMIT_SCALE            : integer    := 0;
  SLOT_CAP_SLOT_POWER_LIMIT_VALUE            : bit_vector := X"00";

  SPARE_BIT0                                 : integer    := 0;

  SPARE_BIT1                                 : integer    := 0;
  SPARE_BIT2                                 : integer    := 0;
  SPARE_BIT3                                 : integer    := 0;
  SPARE_BIT4                                 : integer    := 0;
  SPARE_BIT5                                 : integer    := 0;
  SPARE_BIT6                                 : integer    := 0;
  SPARE_BIT7                                 : integer    := 0;
  SPARE_BIT8                                 : integer    := 0;
  SPARE_BYTE0                                : bit_vector := X"00";
  SPARE_BYTE1                                : bit_vector := X"00";
  SPARE_BYTE2                                : bit_vector := X"00";
  SPARE_BYTE3                                : bit_vector := X"00";
  SPARE_WORD0                                : bit_vector := X"00000000";
  SPARE_WORD1                                : bit_vector := X"00000000";
  SPARE_WORD2                                : bit_vector := X"00000000";
  SPARE_WORD3                                : bit_vector := X"00000000";

  TL_RBYPASS                                 : string     := "FALSE";
  TL_TFC_DISABLE                             : string     := "FALSE";
  TL_TX_CHECKS_DISABLE                       : string     := "FALSE";
  EXIT_LOOPBACK_ON_EI                        : string     := "TRUE";

  CFG_ECRC_ERR_CPLSTAT                       : integer    := 0;
  CAPABILITIES_PTR                           : bit_vector := X"40";
  CRM_MODULE_RSTS                            : bit_vector := X"00";
  DEV_CAP_ENABLE_SLOT_PWR_LIMIT_SCALE        : string     := "TRUE";
  DEV_CAP_ENABLE_SLOT_PWR_LIMIT_VALUE        : string     := "TRUE";
  DEV_CAP_FUNCTION_LEVEL_RESET_CAPABLE       : string     := "FALSE";
  DEV_CAP_ROLE_BASED_ERROR                   : string     := "TRUE";
  DEV_CAP_RSVD_14_12                         : integer    := 0;
  DEV_CAP_RSVD_17_16                         : integer    := 0;
  DEV_CAP_RSVD_31_29                         : integer    := 0;
  DEV_CONTROL_AUX_POWER_SUPPORTED            : string     := "FALSE";

  VC_CAP_ID                                  : bit_vector := X"0002";
  VC_CAP_VERSION                             : bit_vector := X"1";
  VSEC_CAP_HDR_ID                            : bit_vector := X"1234";
  VSEC_CAP_HDR_LENGTH                        : bit_vector := X"018";
  VSEC_CAP_HDR_REVISION                      : bit_vector := X"1";
  VSEC_CAP_ID                                : bit_vector := X"000b";
  VSEC_CAP_IS_LINK_VISIBLE                   : string     := "TRUE";
  VSEC_CAP_VERSION                           : bit_vector := X"1";

  DISABLE_ERR_MSG                            : string     := "FALSE";
  DISABLE_LOCKED_FILTER                      : string     := "FALSE";
  DISABLE_PPM_FILTER                         : string     := "FALSE";
  ENDEND_TLP_PREFIX_FORWARDING_SUPPORTED     : string     := "FALSE";
  INTERRUPT_STAT_AUTO                        : string     := "TRUE";
  MPS_FORCE                                  : string     := "FALSE";
  PM_ASPML0S_TIMEOUT                         : bit_vector := X"0000";
  PM_ASPML0S_TIMEOUT_EN                      : string     := "FALSE";
  PM_ASPML0S_TIMEOUT_FUNC                    : integer    := 0;
  PM_ASPM_FASTEXIT                           : string     := "FALSE";
  PM_MF                                      : string     := "FALSE";

  RP_AUTO_SPD                                : bit_vector := X"1";
  RP_AUTO_SPD_LOOPCNT                        : bit_vector := X"1f";
  SIM_VERSION                                : string     := "1.0";
  SSL_MESSAGE_AUTO                           : string     := "FALSE";
  TECRC_EP_INV                               : string     := "FALSE";
  UR_CFG1                                    : string     := "TRUE";
  USE_RID_PINS                               : string     := "FALSE";

-- New Parameters
  DEV_CAP2_ENDEND_TLP_PREFIX_SUPPORTED       : string     := "FALSE";
  DEV_CAP2_EXTENDED_FMT_FIELD_SUPPORTED      : string     := "FALSE";
  DEV_CAP2_LTR_MECHANISM_SUPPORTED           : string     := "FALSE";
  DEV_CAP2_MAX_ENDEND_TLP_PREFIXES           : bit_vector := X"0";
  DEV_CAP2_NO_RO_ENABLED_PRPR_PASSING        : string     := "FALSE";

  LINK_CAP_SURPRISE_DOWN_ERROR_CAPABLE       : string     := "FALSE";

  AER_CAP_ID                                 : bit_vector := X"0001";
  AER_CAP_VERSION                            : bit_vector := X"1";

  RBAR_CAP_ID                                : bit_vector := X"0015";
  RBAR_CAP_NEXTPTR                           : bit_vector := X"000";
  RBAR_CAP_VERSION                           : bit_vector := X"1";
  PCIE_USE_MODE                              : string     := "3.0";
  PCIE_GT_DEVICE                             : string     := "GTX";
  PCIE_CHAN_BOND                             : integer    := 0;
  PCIE_PLL_SEL                               : string     := "CPLL";
  PCIE_ASYNC_EN                              : string     := "FALSE";
  PCIE_TXBUF_EN                              : string     := "FALSE";

  PIPE_QPLL_LOCK_SIZE                        : integer    := 1
);

port (
-------------------------------------------------------------------------------------------------------------------
-- PCI Express (pci_exp) Interface                                                                               --
-------------------------------------------------------------------------------------------------------------------
  pci_exp_txp                                : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pci_exp_txn                                : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pci_exp_rxp                                : in std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pci_exp_rxn                                : in std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);

-------------------------------------------------------------------------------------------------------------------
-- Clock & GT COMMON Sharing Interface                                                                           --
-------------------------------------------------------------------------------------------------------------------
-- Shared Logic Internal
  int_pclk_out_slave                         : out std_logic;  
  int_pipe_rxusrclk_out                      : out std_logic;    
  int_rxoutclk_out                           : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  int_dclk_out                               : out std_logic;   
  int_userclk1_out                           : out std_logic;    
  int_userclk2_out                           : out std_logic;     
  int_oobclk_out                             : out std_logic;      
  int_mmcm_lock_out                          : out std_logic;       
  int_qplllock_out                           : out std_logic_vector(1 downto 0);
  int_qplloutclk_out                         : out std_logic_vector(1 downto 0);
  int_qplloutrefclk_out                      : out std_logic_vector(1 downto 0);
  int_pclk_sel_slave                         : in std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);

  pipe_pclk_in                               : in std_logic;
  pipe_rxusrclk_in                           : in std_logic;
  pipe_rxoutclk_in                           : in std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_dclk_in                               : in std_logic;
  pipe_userclk1_in                           : in std_logic;
  pipe_userclk2_in                           : in std_logic;
  pipe_oobclk_in                             : in std_logic;
  pipe_mmcm_lock_in                          : in std_logic;

  pipe_txoutclk_out                          : out std_logic;
  pipe_rxoutclk_out                          : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_pclk_sel_out                          : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_gen3_out                              : out std_logic;

-------------------------------------------------------------------------------------------------------------------
-- AXI-S Interface                                                                                               --
-------------------------------------------------------------------------------------------------------------------
-- Common
  user_clk_out                               : out std_logic;
  user_reset_out                             : out std_logic;
  user_lnk_up                                : out std_logic;
  user_app_rdy                               : out std_logic;

-- TX
  tx_buf_av                                  : out std_logic_vector(5 downto 0);
  tx_cfg_req                                 : out std_logic;
  tx_err_drop                                : out std_logic;
  s_axis_tx_tready                           : out std_logic;
  s_axis_tx_tdata                            : in std_logic_vector((C_DATA_WIDTH - 1) downto 0);
  s_axis_tx_tkeep                            : in std_logic_vector((C_DATA_WIDTH / 8 - 1) downto 0);
  s_axis_tx_tlast                            : in std_logic;
  s_axis_tx_tvalid                           : in std_logic;
  s_axis_tx_tuser                            : in std_logic_vector(3 downto 0);
  tx_cfg_gnt                                 : in std_logic;

-- RX
  m_axis_rx_tdata                            : out std_logic_vector((C_DATA_WIDTH - 1) downto 0);
  m_axis_rx_tkeep                            : out std_logic_vector((C_DATA_WIDTH / 8 - 1) downto 0);
  m_axis_rx_tlast                            : out std_logic;
  m_axis_rx_tvalid                           : out std_logic;
  m_axis_rx_tready                           : in std_logic;
  m_axis_rx_tuser                            : out std_logic_vector(21 downto 0);
  rx_np_ok                                   : in std_logic;
  rx_np_req                                  : in std_logic;

-- Flow Control
  fc_cpld                                    : out std_logic_vector(11 downto 0);
  fc_cplh                                    : out std_logic_vector(7 downto 0);
  fc_npd                                     : out std_logic_vector(11 downto 0);
  fc_nph                                     : out std_logic_vector(7 downto 0);
  fc_pd                                      : out std_logic_vector(11 downto 0);
  fc_ph                                      : out std_logic_vector(7 downto 0);
  fc_sel                                     : in std_logic_vector(2 downto 0);

-------------------------------------------------------------------------------------------------------------------
-- Configuration (CFG) Interface                                                                                 --
-------------------------------------------------------------------------------------------------------------------
-- EP and RP
------------
  cfg_mgmt_do                                : out std_logic_vector (31 downto 0);
  cfg_mgmt_rd_wr_done                        : out std_logic;

  cfg_status                                 : out std_logic_vector(15 downto 0);
  cfg_command                                : out std_logic_vector(15 downto 0);
  cfg_dstatus                                : out std_logic_vector(15 downto 0);
  cfg_dcommand                               : out std_logic_vector(15 downto 0);
  cfg_lstatus                                : out std_logic_vector(15 downto 0);
  cfg_lcommand                               : out std_logic_vector(15 downto 0);
  cfg_dcommand2                              : out std_logic_vector(15 downto 0);
  cfg_pcie_link_state                        : out std_logic_vector(2 downto 0);

  cfg_pmcsr_pme_en                           : out std_logic;
  cfg_pmcsr_powerstate                       : out std_logic_vector(1 downto 0);
  cfg_pmcsr_pme_status                       : out std_logic;
  cfg_received_func_lvl_rst                  : out std_logic;

-- Management Interface
  cfg_mgmt_di                                : in std_logic_vector (31 downto 0);
  cfg_mgmt_byte_en                           : in std_logic_vector (3 downto 0);
  cfg_mgmt_dwaddr                            : in std_logic_vector (9 downto 0);
  cfg_mgmt_wr_en                             : in std_logic;
  cfg_mgmt_rd_en                             : in std_logic;
  cfg_mgmt_wr_readonly                       : in std_logic;

-- Error Reporting Interface
  cfg_err_ecrc                               : in std_logic;
  cfg_err_ur                                 : in std_logic;
  cfg_err_cpl_timeout                        : in std_logic;
  cfg_err_cpl_unexpect                       : in std_logic;
  cfg_err_cpl_abort                          : in std_logic;
  cfg_err_posted                             : in std_logic;
  cfg_err_cor                                : in std_logic;
  cfg_err_atomic_egress_blocked              : in std_logic;
  cfg_err_internal_cor                       : in std_logic;
  cfg_err_malformed                          : in std_logic;
  cfg_err_mc_blocked                         : in std_logic;
  cfg_err_poisoned                           : in std_logic;
  cfg_err_norecovery                         : in std_logic;
  cfg_err_tlp_cpl_header                     : in std_logic_vector(47 downto 0);
  cfg_err_cpl_rdy                            : out std_logic;
  cfg_err_locked                             : in std_logic;
  cfg_err_acs                                : in std_logic;
  cfg_err_internal_uncor                     : in std_logic;
  cfg_trn_pending                            : in std_logic;
  cfg_pm_halt_aspm_l0s                       : in std_logic;
  cfg_pm_halt_aspm_l1                        : in std_logic;
  cfg_pm_force_state_en                      : in std_logic;
  cfg_pm_force_state                         : in std_logic_vector(1 downto 0);
  cfg_dsn                                    : in std_logic_vector(63 downto 0);

-- EP Only
----------
  cfg_interrupt                              : in std_logic;
  cfg_interrupt_rdy                          : out std_logic;
  cfg_interrupt_assert                       : in std_logic;
  cfg_interrupt_di                           : in std_logic_vector(7 downto 0);
  cfg_interrupt_do                           : out std_logic_vector(7 downto 0);
  cfg_interrupt_mmenable                     : out std_logic_vector(2 downto 0);
  cfg_interrupt_msienable                    : out std_logic;
  cfg_interrupt_msixenable                   : out std_logic;
  cfg_interrupt_msixfm                       : out std_logic;
  cfg_interrupt_stat                         : in std_logic;
  cfg_pciecap_interrupt_msgnum               : in std_logic_vector(4 downto 0);
  cfg_to_turnoff                             : out std_logic;
  cfg_turnoff_ok                             : in std_logic;
  cfg_bus_number                             : out std_logic_vector(7 downto 0);
  cfg_device_number                          : out std_logic_vector(4 downto 0);
  cfg_function_number                        : out std_logic_vector(2 downto 0);
  cfg_pm_wake                                : in std_logic;

-- RP Only
----------
  cfg_pm_send_pme_to                         : in std_logic;
  cfg_ds_bus_number                          : in std_logic_vector(7 downto 0);
  cfg_ds_device_number                       : in std_logic_vector(4 downto 0);
  cfg_ds_function_number                     : in std_logic_vector(2 downto 0);

  cfg_mgmt_wr_rw1c_as_rw                     : in std_logic;
  cfg_msg_received                           : out std_logic;
  cfg_msg_data                               : out std_logic_vector(15 downto 0);

  cfg_bridge_serr_en                         : out std_logic;
  cfg_slot_control_electromech_il_ctl_pulse  : out std_logic;
  cfg_root_control_syserr_corr_err_en        : out std_logic;
  cfg_root_control_syserr_non_fatal_err_en   : out std_logic;
  cfg_root_control_syserr_fatal_err_en       : out std_logic;
  cfg_root_control_pme_int_en                : out std_logic;
  cfg_aer_rooterr_corr_err_reporting_en      : out std_logic;
  cfg_aer_rooterr_non_fatal_err_reporting_en : out std_logic;
  cfg_aer_rooterr_fatal_err_reporting_en     : out std_logic;
  cfg_aer_rooterr_corr_err_received          : out std_logic;
  cfg_aer_rooterr_non_fatal_err_received     : out std_logic;
  cfg_aer_rooterr_fatal_err_received         : out std_logic;

  cfg_msg_received_err_cor                   : out std_logic;
  cfg_msg_received_err_non_fatal             : out std_logic;
  cfg_msg_received_err_fatal                 : out std_logic;
  cfg_msg_received_pm_as_nak                 : out std_logic;
  cfg_msg_received_pm_pme                    : out std_logic;
  cfg_msg_received_pme_to_ack                : out std_logic;
  cfg_msg_received_assert_int_a              : out std_logic;
  cfg_msg_received_assert_int_b              : out std_logic;
  cfg_msg_received_assert_int_c              : out std_logic;
  cfg_msg_received_assert_int_d              : out std_logic;
  cfg_msg_received_deassert_int_a            : out std_logic;
  cfg_msg_received_deassert_int_b            : out std_logic;
  cfg_msg_received_deassert_int_c            : out std_logic;
  cfg_msg_received_deassert_int_d            : out std_logic;
  cfg_msg_received_setslotpowerlimit         : out std_logic;

-------------------------------------------------------------------------------------------------------------------
-- Physical Layer Control and Status (PL) Interface                                                              --
-------------------------------------------------------------------------------------------------------------------
  pl_directed_link_change                    : in std_logic_vector(1 downto 0);
  pl_directed_link_width                     : in std_logic_vector(1 downto 0);
  pl_directed_link_speed                     : in std_logic;
  pl_directed_link_auton                     : in std_logic;
  pl_upstream_prefer_deemph                  : in std_logic;

  pl_sel_lnk_rate                            : out std_logic;
  pl_sel_lnk_width                           : out std_logic_vector(1 downto 0);
  pl_ltssm_state                             : out std_logic_vector(5 downto 0);
  pl_lane_reversal_mode                      : out std_logic_vector(1 downto 0);

  pl_phy_lnk_up                              : out std_logic;
  pl_tx_pm_state                             : out std_logic_vector(2 downto 0);
  pl_rx_pm_state                             : out std_logic_vector(1 downto 0);

  pl_link_upcfg_cap                          : out std_logic;
  pl_link_gen2_cap                           : out std_logic;
  pl_link_partner_gen2_supported             : out std_logic;
  pl_initial_link_width                      : out std_logic_vector(2 downto 0);

  pl_directed_change_done                    : out std_logic;

-- EP Only
----------
  pl_received_hot_rst                        : out std_logic;

-- RP Only
----------
  pl_transmit_hot_rst                        : in std_logic;
  pl_downstream_deemph_source                : in std_logic;

-------------------------------------------------------------------------------------------------------------------
-- AER interface                                                                                                 --
-------------------------------------------------------------------------------------------------------------------
  cfg_err_aer_headerlog                      : in std_logic_vector(127 downto 0);
  cfg_aer_interrupt_msgnum                   : in std_logic_vector(4 downto 0);
  cfg_err_aer_headerlog_set                  : out std_logic;
  cfg_aer_ecrc_check_en                      : out std_logic;
  cfg_aer_ecrc_gen_en                        : out std_logic;

-------------------------------------------------------------------------------------------------------------------
-- VC interface                                                                                                  --
-------------------------------------------------------------------------------------------------------------------
  cfg_vc_tcvc_map                            : out std_logic_vector(6 downto 0);

-------------------------------------------------------------------------------------------------------------------
-- PCIe Fast Config: STARTUP primitive Interface - only used in Tandem configurations                            --
-------------------------------------------------------------------------------------------------------------------
  startup_eos_in                             : in std_logic;              -- 1-bit input: This signal should be driven by the EOS output of the STARTUP primitive.
  startup_cfgclk                             : out std_logic := '0';      -- 1-bit output: Configuration main clock output
  startup_cfgmclk                            : out std_logic := '0';      -- 1-bit output: Configuration internal oscillator clock output
  startup_eos                                : out std_logic := '0';      -- 1-bit output: Active high output signal indicating the End Of Startup
  startup_preq                               : out std_logic := '0';      -- 1-bit output: PROGRAM request to fabric output

  startup_clk                                : in std_logic;              -- 1-bit input: User start-up clock input
  startup_gsr                                : in std_logic;              -- 1-bit input: Global Set/Reset input (GSR cannot be used for the port name)
  startup_gts                                : in std_logic;              -- 1-bit input: Global 3-state input (GTS cannot be used for the port name)
  startup_keyclearb                          : in std_logic;              -- 1-bit input: Clear AES Decrypter Key input from Battery-Backed RAM (BBRAM)
  startup_pack                               : in std_logic;              -- 1-bit input: PROGRAM acknowledge input
  startup_usrcclko                           : in std_logic;              -- 1-bit input: User CCLK input
  startup_usrcclkts                          : in std_logic;              -- 1-bit input: User CCLK 3-state enable input
  startup_usrdoneo                           : in std_logic;              -- 1-bit input: User DONE pin output control
  startup_usrdonets                          : in std_logic;              -- 1-bit input: User DONE 3-state enable output

-------------------------------------------------------------------------------------------------------------------
-- PCIe Fast Config: ICAP primitive Interface - only used in Tandem configurations                               --
-------------------------------------------------------------------------------------------------------------------
  icap_clk                                   : in std_logic;
  icap_csib                                  : in std_logic;
  icap_rdwrb                                 : in std_logic;
  icap_i                                     : in std_logic_vector(31 downto 0);
  icap_o                                     : out std_logic_vector(31 downto 0):= (others => '0');

  qpll_drp_crscode                           : in std_logic_vector(11 downto 0);
  qpll_drp_fsm                               : in std_logic_vector(17 downto 0);
  qpll_drp_done                              : in std_logic_vector(1 downto 0);
  qpll_drp_reset                             : in std_logic_vector(1 downto 0);
  qpll_qplllock                              : in std_logic_vector(1 downto 0);
  qpll_qplloutclk                            : in std_logic_vector(1 downto 0);
  qpll_qplloutrefclk                         : in std_logic_vector(1 downto 0);
  qpll_qplld                                 : out std_logic;
  qpll_qpllreset                             : out std_logic_vector(1 downto 0);
  qpll_drp_clk                               : out std_logic;
  qpll_drp_rst_n                             : out std_logic;
  qpll_drp_ovrd                              : out std_logic;
  qpll_drp_gen3                              : out std_logic;
  qpll_drp_start                             : out std_logic;
  pipe_txprbssel                             : in std_logic_vector(2 downto 0);
  pipe_rxprbssel                             : in std_logic_vector(2 downto 0);
  pipe_txprbsforceerr                        : in std_logic;
  pipe_rxprbscntreset                        : in std_logic;
  pipe_loopback                              : in std_logic_vector(2 downto 0);
  pipe_rxprbserr                             : out std_logic_vector((LINK_CAP_MAX_LINK_WIDTH-1) downto 0);
  pipe_txinhibit                             : in std_logic_vector((LINK_CAP_MAX_LINK_WIDTH-1) downto 0);
  pipe_rst_fsm                               : out std_logic_vector(4 downto 0);
  pipe_qrst_fsm                              : out std_logic_vector(11 downto 0);
  pipe_rate_fsm                              : out std_logic_vector((5*LINK_CAP_MAX_LINK_WIDTH-1) downto 0);
  pipe_sync_fsm_tx                           : out std_logic_vector((6*LINK_CAP_MAX_LINK_WIDTH-1) downto 0);
  pipe_sync_fsm_rx                           : out std_logic_vector((7*LINK_CAP_MAX_LINK_WIDTH-1) downto 0);
  pipe_drp_fsm                               : out std_logic_vector((7*LINK_CAP_MAX_LINK_WIDTH-1) downto 0);
  pipe_rst_idle                              : out std_logic;
  pipe_qrst_idle                             : out std_logic;
  pipe_rate_idle                             : out std_logic;
  pipe_eyescandataerror                      : out std_logic_vector((LINK_CAP_MAX_LINK_WIDTH-1)  downto 0);
  pipe_rxstatus                              : out std_logic_vector((3*LINK_CAP_MAX_LINK_WIDTH-1) downto 0);
  pipe_dmonitorout                           : out std_logic_vector((15*LINK_CAP_MAX_LINK_WIDTH-1) downto 0);

  pipe_cpll_lock                             : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_qpll_lock                             : out std_logic_vector(PIPE_QPLL_LOCK_SIZE - 1 downto 0);
  pipe_rxpmaresetdone                        : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);  
  pipe_rxbufstatus                           : out std_logic_vector((LINK_CAP_MAX_LINK_WIDTH * 3) - 1 downto 0);  
  pipe_txphaligndone                         : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);   
  pipe_txphinitdone                          : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);      
  pipe_txdlysresetdone                       : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);    
  pipe_rxphaligndone                         : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);     
  pipe_rxdlysresetdone                       : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);      
  pipe_rxsyncdone                            : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);      
  pipe_rxdisperr                             : out std_logic_vector((LINK_CAP_MAX_LINK_WIDTH * 8) - 1 downto 0);     
  pipe_rxnotintable                          : out std_logic_vector((LINK_CAP_MAX_LINK_WIDTH * 8) - 1 downto 0);     
  pipe_rxcommadet                            : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);

  gt_ch_drp_rdy                              : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_debug_0                               : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_debug_1                               : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_debug_2                               : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_debug_3                               : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_debug_4                               : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_debug_5                               : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_debug_6                               : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_debug_7                               : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_debug_8                               : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_debug_9                               : out std_logic_vector(LINK_CAP_MAX_LINK_WIDTH - 1 downto 0);
  pipe_debug                                 : out std_logic_vector(31 downto 0);

  ext_ch_gt_drpclk                           : out std_logic;
  ext_ch_gt_drpaddr                          : in std_logic_vector((LINK_CAP_MAX_LINK_WIDTH*9)-1 downto 0);
  ext_ch_gt_drpen                            : in std_logic_vector((LINK_CAP_MAX_LINK_WIDTH)-1 downto 0);
  ext_ch_gt_drpdi                            : in std_logic_vector((LINK_CAP_MAX_LINK_WIDTH*16)-1 downto 0);
  ext_ch_gt_drpwe                            : in std_logic_vector((LINK_CAP_MAX_LINK_WIDTH)-1 downto 0);
  ext_ch_gt_drpdo                            : out std_logic_vector((LINK_CAP_MAX_LINK_WIDTH*16)-1 downto 0);
  ext_ch_gt_drprdy                           : out std_logic_vector((LINK_CAP_MAX_LINK_WIDTH)-1 downto 0);

-------------------------------------------------------------------------------------------------------------------
-- PCIe DRP Interface                                                                                            --
-------------------------------------------------------------------------------------------------------------------
  pcie_drp_clk                               : in std_logic;
  pcie_drp_en                                : in std_logic;
  pcie_drp_we                                : in std_logic;
  pcie_drp_addr                              : in std_logic_vector(8 downto 0);
  pcie_drp_di                                : in std_logic_vector(15 downto 0);
  pcie_drp_do                                : out std_logic_vector(15 downto 0):= (others => '0');
  pcie_drp_rdy                               : out std_logic:='0';

-------------------------------------------------------------------------------------------------------------------
-- PIPE PORTS to TOP Level For PIPE SIMULATION with 3rd Party IP/BFM/Xilinx BFM                                  --
-------------------------------------------------------------------------------------------------------------------
  common_commands_in                         : in std_logic_vector( 3 downto 0);
  pipe_rx_0_sigs                             : in std_logic_vector(24 downto 0);
  pipe_rx_1_sigs                             : in std_logic_vector(24 downto 0);
  pipe_rx_2_sigs                             : in std_logic_vector(24 downto 0);
  pipe_rx_3_sigs                             : in std_logic_vector(24 downto 0);
  pipe_rx_4_sigs                             : in std_logic_vector(24 downto 0);
  pipe_rx_5_sigs                             : in std_logic_vector(24 downto 0);
  pipe_rx_6_sigs                             : in std_logic_vector(24 downto 0);
  pipe_rx_7_sigs                             : in std_logic_vector(24 downto 0);

  common_commands_out                        : out std_logic_vector(11 downto 0);
  pipe_tx_0_sigs                             : out std_logic_vector(22 downto 0);
  pipe_tx_1_sigs                             : out std_logic_vector(22 downto 0);
  pipe_tx_2_sigs                             : out std_logic_vector(22 downto 0);
  pipe_tx_3_sigs                             : out std_logic_vector(22 downto 0);
  pipe_tx_4_sigs                             : out std_logic_vector(22 downto 0);
  pipe_tx_5_sigs                             : out std_logic_vector(22 downto 0);
  pipe_tx_6_sigs                             : out std_logic_vector(22 downto 0);
  pipe_tx_7_sigs                             : out std_logic_vector(22 downto 0);

-------------------------------------------------------------------------------------------------------------------
--  System(SYS) Interface                                                                                        --
-------------------------------------------------------------------------------------------------------------------
  pipe_mmcm_rst_n                            : in std_logic;
  sys_clk                                    : in std_logic;
  sys_rst_n                                  : in std_logic
);
end component;

------------------------------------------------------------------------------------------------------------------------
component s7_pcie_ep_v3_1_pipe_clock                                       -- PIPE Clock Module for 7 Series Transceiver
------------------------------------------------------------------------------------------------------------------------
generic (
  PCIE_ASYNC_EN                              : string := "FALSE";          -- PCIe async enable
  PCIE_TXBUF_EN                              : string := "FALSE";          -- PCIe TX buffer enable for Gen1/Gen2 only
  PCIE_CLK_SHARING_EN                        : string := "FALSE";          -- Enable Clock Sharing
  PCIE_LANE                                  : integer := 8;               -- PCIe number of lanes
  PCIE_LINK_SPEED                            : integer := 2;               -- PCIe link speed
  PCIE_REFCLK_FREQ                           : integer := 0;               -- PCIe reference clock frequency
  PCIE_USERCLK1_FREQ                         : integer := 3;               -- PCIe user clock 1 frequency
  PCIE_USERCLK2_FREQ                         : integer := 3;               -- PCIe user clock 2 frequency
  PCIE_OOBCLK_MODE                           : integer := 1;               -- PCIe oob clock mode
  PCIE_DEBUG_MODE                            : integer := 0                -- PCIe Debug Mode
);
port (
------------ Input -------------------------------------
  CLK_CLK                                    : in std_logic;
  CLK_TXOUTCLK                               : in std_logic;
  CLK_RXOUTCLK_IN                            : in std_logic_vector(PCIE_LANE - 1 downto 0);
  CLK_RST_N                                  : in std_logic;
  CLK_PCLK_SEL                               : in std_logic_vector(PCIE_LANE - 1 downto 0);
  CLK_PCLK_SEL_SLAVE                         : in std_logic_vector(PCIE_LANE - 1 downto 0);
  CLK_GEN3                                   : in std_logic;

------------ Output ------------------------------------
  CLK_PCLK                                   : out std_logic;
  CLK_PCLK_SLAVE                             : out std_logic;
  CLK_RXUSRCLK                               : out std_logic;
  CLK_RXOUTCLK_OUT                           : out std_logic_vector(PCIE_LANE - 1 downto 0);
  CLK_DCLK                                   : out std_logic;
  CLK_USERCLK1                               : out std_logic;
  CLK_USERCLK2                               : out std_logic;
  CLK_OOBCLK                                 : out std_logic;
  CLK_MMCM_LOCK                              : out std_logic
);
end component;

end package ep_comp_pkg;
