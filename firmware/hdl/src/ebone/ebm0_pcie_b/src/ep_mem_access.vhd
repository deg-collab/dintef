-------------------------------------------------------------------------------
--
-- Project    : Series 7 Integrated Block for PCI Express
-- File       : ep_mem_access.vhd
-- Description: Endpoint Memory Access Unit. This module provides access
--              functions to the Endpoint memory aperture.
--
--              Read Access: Module returns data for the specifed address and
--              byte enables selected.
--
--              Write Access: Module accepts data, byte enables and updates
--              data when write enable is asserted. Modules signals write busy
--              when data write is in progress.
--
-------------------------------------------------------------------------------
-- Dependencies
---------------
-- ep_reg
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.ep_comp_pkg.all;
use work.ebm0_pcie_b_pkg.all;

entity ep_mem_access is

generic (
  BADR0_MEM                      : INTEGER := 1;
  WATCH_DOG                      : INTEGER := 1;
	AXI_DWIDTH                     : NATURAL := 32;
	TRGT_DESC                      : NATURAL := 16#00#;
	TYPE_DESC                      : NATURAL := 16#00000000#;
	HARD_VERS                      : NATURAL := 16#00000000#;
	DEVICE_ID                      : BIT_VECTOR := X"EB01"
);

port (
  clk                            : in std_logic;
  trn_rst                        : in std_logic;
  trn_lnk_up_n                   : in std_logic;

-- Read Port
  brd_addr_i                     : in std_logic_vector(29 downto 0);
  brd_be_i                       : in std_logic_vector(3 downto 0);
  int_brd_data_o                 : out std32_x32;

-- Write Port
  bwr_addr_i                     : in std_logic_vector(29 downto 0);
  bwr_be_i                       : in std_logic_vector(7 downto 0);
  bwr_data_i                     : in std32_x32;
  wr_en_i                        : in std_logic;
	breq_len_i                     : in std_logic_vector(9 downto 0);
  wr_busy_ep_o                   : out std_logic;

-- Configuration (CFG) Interface
  cfg_bus_number                 : in std_logic_vector(7 downto 0);
  cfg_device_number              : in std_logic_vector(4 downto 0);
  cfg_function_number            : in std_logic_vector(2 downto 0);
  cfg_status                     : in std_logic_vector(15 downto 0);
  cfg_command                    : in std_logic_vector(15 downto 0);
  cfg_dstatus                    : in std_logic_vector(15 downto 0);
  cfg_dcommand                   : in std_logic_vector(15 downto 0);
	cfg_dcommand2                  : in std_logic_vector(15 downto 0);
  cfg_lstatus                    : in std_logic_vector(15 downto 0);
  cfg_lcommand                   : in std_logic_vector(15 downto 0);

  cfg_interrupt_do               : in std_logic_vector(7 downto 0);
  cfg_interrupt_mmenable         : in std_logic_vector(2 downto 0);
  cfg_interrupt_msienable        : in std_logic;
  cfg_interrupt_msixenable       : in std_logic;
  cfg_interrupt_msixfm           : in std_logic;

-- Physical Layer Control and Status (PL) Interface
  pl_initial_link_width          : in std_logic_vector(2 downto 0);
  pl_lane_reversal_mode          : in std_logic_vector(1 downto 0);
  pl_link_gen2_capable           : in std_logic;
  pl_link_partner_gen2_supported : in std_logic;
  pl_link_upcfg_capable          : in std_logic;
  pl_ltssm_state                 : in std_logic_vector(5 downto 0);
  pl_received_hot_rst            : in std_logic;
  pl_sel_link_rate               : in std_logic;
  pl_sel_link_width              : in std_logic_vector(1 downto 0);
	pl_phy_lnk_up                  : in std_logic;
  pl_tx_pm_state                 : in std_logic_vector(2 downto 0);
  pl_rx_pm_state                 : in std_logic_vector(1 downto 0);
  pl_directed_change_done        : in std_logic;
  pl_directed_link_auton         : out std_logic;
  pl_directed_link_change        : out std_logic_vector(1 downto 0);
  pl_directed_link_speed         : out std_logic;
  pl_directed_link_width         : out std_logic_vector(1 downto 0);
  pl_upstream_prefer_deemph      : out std_logic;

  cfg_read_req_o                 : out std_logic;
	cfgs_regs_i                    : in std32_x32;

  INtr_service_o                 : out std_logic;
	INtr_disable_o                 : out std_logic;
	INtr_enable_o                  : out std_logic;
	INtr_TLP_end_o                 : out std_logic;
	INtr_B_calls_o                 : out std_logic;
	INtr_Rqst_nmb_o                : out std_logic_vector(7 downto 0);

  msg_signaling_i                : in std_logic;
	msg_code_i                     : in std_logic_vector(7 downto 0);
	msg_data_lsb_i                 : in std_logic_vector(31 downto 0);
	msg_data_msb_i                 : in std_logic_vector(31 downto 0);

  requester_fmt_o                : out std_logic;
  requester_leng_o               : out std_logic_vector(9 downto 0);

  TLP_end_Rqst_i                 : in std_logic;
	bcall_reg_i                    : in std_logic_vector(31 downto 0);
	B_calls_Rqst_o                 : out std_logic;
	tlp_tx_addr                    : in std_logic_vector(AXI_DWIDTH - 1 downto 0);

  FT_on_i                        : in std_logic;
  soft_wd_o                      : out std_logic;
  soft_reset_o                   : out std_logic;
  status_led_o                   : out std_logic_vector(1 downto 0);
  ebone_reset_o                  : out std_logic;
	bcall_run_i                    : in std_logic;

  err_drop_count                 : in std_logic_vector(14 downto 0);
	err_drop_ovfl                  : in std_logic;
	tdst_rdy_count                 : in std_logic_vector(14 downto 0);
	tdst_rdy_ovfl                  : in std_logic;
  req_len_sup_count              : in std_logic_vector(15 downto 0);
	req_rx_valid_count             : in std_logic_vector(31 downto 0);
	req_tx_valid_count             : in std_logic_vector(31 downto 0);
  ext_ctrl_src_i                 : in std_logic;

  mm_probe_o                     : out std_logic_vector(3 downto 0);
  prob_mux_o                     : out std_logic_vector(19 downto 0);

  my_payload_size_o              : out std_logic_vector(2 downto 0);
  user_cnst_i                    : in std_logic_vector(15 downto 0)
);
end ep_mem_access;

architecture rtl of ep_mem_access is

type state_type       is (MEM_ACCESS_WR_RST, MEM_ACCESS_WR_WRITE);
signal wr_mem_state   : state_type;

signal rd_data_raw_o  : std_logic_vector(31 downto 0);

signal rd_data0_o     : std_logic_vector(31 downto 0);
signal rd_data1_o     : std_logic_vector(31 downto 0);
signal rd_data2_o     : std_logic_vector(31 downto 0);

signal write_en       : std_logic;
signal post_wr_data   : std_logic_vector(31 downto 0);
signal w_pre_wr_data  : std_logic_vector(31 downto 0);

signal pre_wr_data    : std_logic_vector(31 downto 0);
signal w_pre_wr_data0 : std_logic_vector(31 downto 0);
signal w_pre_wr_data1 : std_logic_vector(31 downto 0);
signal w_pre_wr_data2 : std_logic_vector(31 downto 0);
signal w_pre_wr_data3 : std_logic_vector(31 downto 0);

signal bwr_cnt        : std_logic_vector(9 downto 0);
signal bwr_cnt_cl     : std_logic;
signal bwr_cnt_en     : std_logic;
signal bwr_cnt_tc     : std_logic;

signal rg_probe       : std_logic_vector(3 downto 0);
signal breq_len       : std_logic_vector(9 downto 0);

signal Uns_len        : unsigned(9 downto 0);
signal Uns_cwr        : unsigned(9 downto 0);

--
-- Memory Write Process
--

--
--  Extract current data bytes. These need to be swizzled
--  BRAM storage format :
--    data[31:0] = { byte[3], byte[2], byte[1], byte[0] (lowest addr) }
--
signal w_pre_wr_data_b3 : std_logic_vector(7 downto 0);
signal w_pre_wr_data_b2 : std_logic_vector(7 downto 0);
signal w_pre_wr_data_b1 : std_logic_vector(7 downto 0);
signal w_pre_wr_data_b0 : std_logic_vector(7 downto 0);

--
--  Extract new data bytes from payload
--  TLP Payload format :
--    data[31:0] = { byte[0] (lowest addr), byte[2], byte[1], byte[3] }
--
signal w_wr_data_b3 : std_logic_vector(7 downto 0);
signal w_wr_data_b2 : std_logic_vector(7 downto 0);
signal w_wr_data_b1 : std_logic_vector(7 downto 0);
signal w_wr_data_b0 : std_logic_vector(7 downto 0);

signal w_wr_data0_int : std_logic_vector(7 downto 0);
signal w_wr_data1_int : std_logic_vector(7 downto 0);
signal w_wr_data2_int : std_logic_vector(7 downto 0);
signal w_wr_data3_int : std_logic_vector(7 downto 0);

signal rd_data0_en : std_logic;
signal rd_data1_en : std_logic;
signal rd_data2_en : std_logic;

signal rd_data_raw_int0 : std_logic_vector(7 downto 0);
signal rd_data_raw_int1 : std_logic_vector(7 downto 0);
signal rd_data_raw_int2 : std_logic_vector(7 downto 0);
signal rd_data_raw_int3 : std_logic_vector(7 downto 0);

signal b_wr_en_0 : std_logic;
signal b_wr_en_1 : std_logic;
signal b_wr_en_2 : std_logic;

signal wr_addr_0 : std_logic;
signal wr_addr_1 : std_logic;
signal wr_addr_2 : std_logic;

begin

mm_probe_o(0) <= rg_probe(0);  -- 0
mm_probe_o(1) <= rg_probe(1);  -- 1
mm_probe_o(2) <= rg_probe(2);  -- 2
mm_probe_o(3) <= rg_probe(3);  -- 3

Uns_len <= unsigned(breq_len);
Uns_cwr <= unsigned(bwr_cnt);

bwr_cnt_tc <= '1' when (Uns_cwr = Uns_len - 1) else '0';

--
--  Extract current data bytes. These need to be swizzled
--  BRAM storage format :
--    data[31:0] = { byte[3], byte[2], byte[1], byte[0] (lowest addr) }
--
w_pre_wr_data_b3 <= pre_wr_data(31 downto 24);
w_pre_wr_data_b2 <= pre_wr_data(23 downto 16);
w_pre_wr_data_b1 <= pre_wr_data(15 downto 08);
w_pre_wr_data_b0 <= pre_wr_data(07 downto 00);

--
--  Extract new data bytes from payload
--  TLP Payload format :
--    data[31:0] = { byte[0] (lowest addr), byte[2], byte[1], byte[3] }
--
w_wr_data_b3 <= bwr_data_i(0)(7 downto 0);
w_wr_data_b2 <= bwr_data_i(0)(15 downto 8);
w_wr_data_b1 <= bwr_data_i(0)(23 downto 16);
w_wr_data_b0 <= bwr_data_i(0)(31 downto 24);

w_wr_data3_int <= w_wr_data_b3 when (bwr_be_i(3) = '1') else w_pre_wr_data_b3;
w_wr_data2_int <= w_wr_data_b2 when (bwr_be_i(2) = '1') else w_pre_wr_data_b2;
w_wr_data1_int <= w_wr_data_b1 when (bwr_be_i(1) = '1') else w_pre_wr_data_b1;
w_wr_data0_int <= w_wr_data_b0 when (bwr_be_i(0) = '1') else w_pre_wr_data_b0;

process
begin

  wait until rising_edge(clk);

  if (trn_rst = '1') then

    write_en     <= '0';
		breq_len     <= (others => '0');
		bwr_cnt_cl   <= '1';
		bwr_cnt_en   <= '0';
    post_wr_data <= (others => '0');
    wr_mem_state <= MEM_ACCESS_WR_RST;

  else

    case (wr_mem_state) is

      when MEM_ACCESS_WR_RST =>

        if (wr_en_i = '1') then
				  breq_len     <= breq_len_i;
					bwr_cnt_cl   <= '0';
          bwr_cnt_en   <= '1';
          wr_mem_state <= MEM_ACCESS_WR_WRITE;
        else
          write_en     <= '0';
					bwr_cnt_cl   <= '1';
          bwr_cnt_en   <= '0';
          wr_mem_state <= MEM_ACCESS_WR_RST;
        end if;

      when MEM_ACCESS_WR_WRITE =>

        if (bwr_cnt_tc = '1') then
				  wr_mem_state <= MEM_ACCESS_WR_RST;
				else
				  wr_mem_state <= MEM_ACCESS_WR_WRITE;
				end if;

        post_wr_data <= w_wr_data3_int &
                        w_wr_data2_int &
                        w_wr_data1_int &
                        w_wr_data0_int;
        write_en     <= '1';

      when others =>

        wr_mem_state <= MEM_ACCESS_WR_RST;

    end case;

  end if;

end process;

process(clk, bwr_cnt_cl)
begin

  if (bwr_cnt_cl = '1') then

    bwr_cnt <= (others => '0');

	elsif (clk'event and clk = '1') then

    if (bwr_cnt_en = '1') then
      bwr_cnt <= bwr_cnt + 1;
		end if;

	end if;

end process;

--
-- Write controller busy
--
wr_busy_ep_o <= '1' when (wr_en_i = '1') else '0';

--
-- Select BlockRAM output based on higher 2 address bits
--
process (bwr_addr_i, w_pre_wr_data0, w_pre_wr_data1, w_pre_wr_data2, w_pre_wr_data3)
begin
  case (bwr_addr_i(29 downto 28)) is
    when "00"   => w_pre_wr_data <= w_pre_wr_data0;
    when "01"   => w_pre_wr_data <= w_pre_wr_data1;
    when "10"   => w_pre_wr_data <= w_pre_wr_data2;
    when "11"   => w_pre_wr_data <= w_pre_wr_data3;
		when others => w_pre_wr_data <= w_pre_wr_data0;
  end case;
end process;

--
-- Memory Read Controller
--
rd_data0_en <= '1' when (brd_addr_i(29 downto 28) = "00") else '0';
rd_data1_en <= '1' when (brd_addr_i(29 downto 28) = "01") else '0';
rd_data2_en <= '1' when (brd_addr_i(29 downto 28) = "10") else '0';

process(brd_addr_i, rd_data0_o, rd_data1_o, rd_data2_o)
begin
  case (brd_addr_i(29 downto 28)) is
    when "00"   => rd_data_raw_o <= rd_data0_o;
    when "01"   => rd_data_raw_o <= rd_data1_o;
    when "10"   => rd_data_raw_o <= rd_data2_o;
    when "11"   => rd_data_raw_o <= rd_data0_o;
		when others => rd_data_raw_o <= rd_data0_o;
  end case;
end process;

-- Handle Read byte enables  --
rd_data_raw_int0 <= rd_data_raw_o(7 downto 0)    when (brd_be_i(0) = '1')
                    else (others => '0');
rd_data_raw_int1 <= rd_data_raw_o(15 downto 8)   when (brd_be_i(1) = '1')
                    else (others => '0');
rd_data_raw_int2 <= rd_data_raw_o(23 downto 16)  when (brd_be_i(2) = '1')
                    else (others => '0');
rd_data_raw_int3 <= rd_data_raw_o (31 downto 24) when (brd_be_i(3) = '1')
                    else (others => '0');

int_brd_data_o(0) <= rd_data_raw_int0 &
                     rd_data_raw_int1 &
                     rd_data_raw_int2 &
                     rd_data_raw_int3;

b_wr_en_0 <= write_en when (bwr_addr_i(29 downto 28) = "00") else '0';
b_wr_en_1 <= write_en when (bwr_addr_i(29 downto 28) = "01") else '0';
b_wr_en_2 <= write_en when (bwr_addr_i(29 downto 28) = "10") else '0';

wr_addr_0 <= '1' when (bwr_addr_i(29 downto 28) = "00") else '0';
wr_addr_1 <= '1' when (bwr_addr_i(29 downto 28) = "01") else '0';
wr_addr_2 <= '1' when (bwr_addr_i(29 downto 28) = "10") else '0';

-- BADR0 4Kbytes MWr/MRd TLPs access
-- =================================

ep_badr0_inst : ep_reg

generic map (
  BADR0_MEM                      => BADR0_MEM,
  WATCH_DOG                      => WATCH_DOG,
	AXI_DWIDTH                     => AXI_DWIDTH,
  TRGT_DESC                      => TRGT_DESC,
	TYPE_DESC                      => TYPE_DESC,
	HARD_VERS                      => HARD_VERS,
	DEVICE_ID                      => DEVICE_ID
)

port map (
  clk                            => clk,                             -- I
  trn_rst                        => trn_rst,                         -- I
  trn_lnk_up_n                   => trn_lnk_up_n,                    -- I

  rd_addr_i                      => brd_addr_i(9 downto 0),          -- I [9:0]
  rd_en_i                        => rd_data0_en,                     -- I
  rd_data_o                      => rd_data0_o,                      -- O [31:0]

  wr_addr_i                      => bwr_addr_i(9 downto 0),          -- I [9:0]
  wr_data_i                      => post_wr_data,                    -- I [31:0]
  wr_en_i                        => b_wr_en_0,                       -- I

  cfg_bus_number                 => cfg_bus_number,                  -- I [7:0]
  cfg_device_number              => cfg_device_number,               -- I [4:0]
  cfg_function_number            => cfg_function_number,             -- I [2:0]
  cfg_status                     => cfg_status,                      -- I [15:0]
  cfg_command                    => cfg_command,                     -- I [15:0]
  cfg_dstatus                    => cfg_dstatus,                     -- I [15:0]
  cfg_dcommand                   => cfg_dcommand,                    -- I [15:0]
	cfg_dcommand2                  => cfg_dcommand2,                   -- I [15:0]
  cfg_lstatus                    => cfg_lstatus,                     -- I [15:0]
  cfg_lcommand                   => cfg_lcommand,                    -- I [15:0]

  cfg_interrupt_do               => cfg_interrupt_do,                -- I [7:0]
  cfg_interrupt_mmenable         => cfg_interrupt_mmenable,          -- I [2:0]
  cfg_interrupt_msienable        => cfg_interrupt_msienable,         -- I
  cfg_interrupt_msixenable       => cfg_interrupt_msixenable,        -- I
  cfg_interrupt_msixfm           => cfg_interrupt_msixfm,            -- I

  pl_initial_link_width          => pl_initial_link_width,           -- I [2:0]
  pl_lane_reversal_mode          => pl_lane_reversal_mode,           -- I [1:0]
  pl_link_gen2_capable           => pl_link_gen2_capable,            -- I
  pl_link_partner_gen2_supported => pl_link_partner_gen2_supported,  -- I
  pl_link_upcfg_capable          => pl_link_upcfg_capable,           -- I
  pl_ltssm_state                 => pl_ltssm_state,                  -- I [5:0]
  pl_received_hot_rst            => pl_received_hot_rst,             -- I
  pl_sel_link_rate               => pl_sel_link_rate,                -- I
  pl_sel_link_width              => pl_sel_link_width,               -- I [1:0]
	pl_phy_lnk_up                  => pl_phy_lnk_up,                   -- I
  pl_tx_pm_state                 => pl_tx_pm_state,                  -- I [2:0]
  pl_rx_pm_state                 => pl_rx_pm_state,                  -- I [1:0]
  pl_directed_change_done        => pl_directed_change_done,         -- I
  pl_directed_link_auton         => pl_directed_link_auton,          -- O
  pl_directed_link_change        => pl_directed_link_change,         -- O [1:0]
  pl_directed_link_speed         => pl_directed_link_speed,          -- O
  pl_directed_link_width         => pl_directed_link_width,          -- O [1:0]
  pl_upstream_prefer_deemph      => pl_upstream_prefer_deemph,       -- O

  cfg_read_req_o                 => cfg_read_req_o,                  -- O
	cfgs_regs_i                    => cfgs_regs_i,                     -- I [32][31:0]

  INtr_service_o                 => INtr_service_o,                  -- O
	INtr_disable_o                 => INtr_disable_o,                  -- O
	INtr_enable_o                  => INtr_enable_o,                   -- O
	INtr_TLP_end_o                 => INtr_TLP_end_o,                  -- O
	INtr_B_calls_o                 => INtr_B_calls_o,                  -- O
	INtr_Rqst_nmb_o                => INtr_Rqst_nmb_o,                 -- O [7:0]

  msg_signaling_i                => msg_signaling_i,                 -- I
	msg_code_i                     => msg_code_i,                      -- I
	msg_data_lsb_i                 => msg_data_lsb_i,                  -- I
	msg_data_msb_i                 => msg_data_msb_i,                  -- I

  requester_fmt_o                => requester_fmt_o,                 -- O
  requester_leng_o               => requester_leng_o,                -- O [9:0]

  TLP_end_Rqst_i                 => TLP_end_Rqst_i,                  -- I
	bcall_reg_i                    => bcall_reg_i,                     -- I [31:0]
	B_calls_Rqst_o                 => B_calls_Rqst_o,                  -- O
  tlp_tx_addr                    => tlp_tx_addr,                     -- I [AXI_DWIDTH - 1:0]

  FT_on_i                        => FT_on_i,                         -- I
  soft_wd_o                      => soft_wd_o,                       -- O
  soft_reset_o                   => soft_reset_o,                    -- O
  status_led_o                   => status_led_o,                    -- O [1:0]
  ebone_reset_o                  => ebone_reset_o,                   -- O
	bcall_run_i                    => bcall_run_i,                     -- I

  err_drop_count                 => err_drop_count,                  -- I [14:0]
	err_drop_ovfl                  => err_drop_ovfl,                   -- I
	tdst_rdy_count                 => tdst_rdy_count,                  -- I [14:0]
	tdst_rdy_ovfl                  => tdst_rdy_ovfl,                   -- I
  req_len_sup_count              => req_len_sup_count,               -- I [15:0]
	req_rx_valid_count             => req_rx_valid_count,              -- I [31:0]
	req_tx_valid_count             => req_tx_valid_count,              -- I [31:0]
	ext_ctrl_src_i                 => ext_ctrl_src_i,                  -- I

  rg_probe_o                     => rg_probe,                        -- O [3:0]
  prob_mux_o                     => prob_mux_o,                      -- O [19:0]

  my_payload_size_o              => my_payload_size_o,               -- O [2:0]
  user_cnst_i                    => user_cnst_i                      -- I [15:0]
);

end rtl;

