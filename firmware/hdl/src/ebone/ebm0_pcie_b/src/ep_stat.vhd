-------------------------------------------------------------------------------
--
-- Project     : Series 7 Integrated Block for PCI Express
-- File        : ep_stat.vhd
-- Description : Endpoint statistics/counters
--
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.ep_comp_pkg.all;

entity ep_stat is

port (
  trn_clk            : in  std_logic;
  trn_rst            : in  std_logic;

  ebone_reset        : in std_logic;
  tx_ft_ready        : in std_logic;
  TLP_end_Rqst       : in std_logic;
	tx_err_drop        : in std_logic;
	s_axis_tx_tready   : in std_logic;
  req_len_sup        : in std_logic;
	req_rx_valid       : in std_logic;
	req_tx_valid       : in std_logic;

  err_drop_count     : out std_logic_vector(14 downto 0);
	err_drop_ovfl      : out std_logic;
	tdst_rdy_count     : out std_logic_vector(14 downto 0);
	tdst_rdy_ovfl      : out std_logic;
  req_len_sup_count  : out std_logic_vector(15 downto 0);
	req_rx_valid_count : out std_logic_vector(31 downto 0);
	req_tx_valid_count : out std_logic_vector(31 downto 0)
);
end ep_stat;

architecture rtl of ep_stat is

type tx_state_type            is (TX_INIT, TX_WAIT);
signal tx_state               : tx_state_type;
signal tx_frame               : std_logic;
signal ct_reset               : std_logic;
signal err_drop_count_int     : std_logic_vector(15 downto 0);
signal tdst_rdy_count_int     : std_logic_vector(15 downto 0);
signal req_len_sup_count_int  : std_logic_vector(15 downto 0);
signal req_rx_valid_count_int : std_logic_vector(31 downto 0);
signal req_tx_valid_count_int : std_logic_vector(31 downto 0);

begin

process (trn_rst, trn_clk, ebone_reset)
begin

  if ((trn_rst = '1') or (ebone_reset = '1')) then

    ct_reset <= '0';
    tx_frame <= '0';
		tx_state <= TX_INIT;

	elsif (trn_clk'event and trn_clk = '1') then

    ct_reset <= '0';

	  case (tx_state) is

		  when TX_INIT =>

        if ((s_axis_tx_tready = '1') and (tx_ft_ready = '1')) then
				  ct_reset <= '1';
				  tx_frame <= '1';
          tx_state <= TX_WAIT;
				end if;

			when TX_WAIT =>

        if (TLP_end_Rqst = '1') then
				  tx_frame <= '0';
          tx_state <= TX_INIT;
				end if;

			when others =>

        tx_frame <= '0';
        tx_state <= TX_INIT;

		end case;

	end if;

end process;

process (trn_rst, trn_clk, ct_reset)
begin

  if ((trn_rst = '1') or (ct_reset = '1')) then

    err_drop_count_int <= (others => '0');
	  err_drop_ovfl      <= '0';
	  tdst_rdy_count_int <= (others => '0');
	  tdst_rdy_ovfl      <= '0';

	elsif (trn_clk'event and trn_clk = '1') then

    if ((tx_frame = '1') and (tx_err_drop = '1')) then
		  err_drop_count_int <= std_logic_vector(unsigned(err_drop_count_int) + 1);
		end if;

    if (err_drop_count_int(15) = '1') then
		  err_drop_ovfl <= '1';
		end if;

    if ((tx_frame = '1') and (s_axis_tx_tready = '0')) then
		  tdst_rdy_count_int <= std_logic_vector(unsigned(tdst_rdy_count_int) + 1);
		end if;

    if (tdst_rdy_count_int(15) = '1') then
		  tdst_rdy_ovfl <= '1';
		end if;

	end if;

end process;

process (trn_rst, trn_clk, ebone_reset)
begin

  if ((trn_rst = '1') or (ebone_reset = '1')) then

    req_len_sup_count_int <= (others => '0');

	elsif (trn_clk'event and trn_clk = '1') then

    if (req_len_sup = '1') then
		  req_len_sup_count_int <= std_logic_vector(unsigned(req_len_sup_count_int) + 1);
		end if;

	end if;

end process;

process (trn_rst, trn_clk, ebone_reset)
begin

  if ((trn_rst = '1') or (ebone_reset = '1')) then

    req_rx_valid_count_int <= (others => '0');

	elsif (trn_clk'event and trn_clk = '1') then

    if (req_rx_valid = '1') then
		  req_rx_valid_count_int <= std_logic_vector(unsigned(req_rx_valid_count_int) + 1);
		end if;

	end if;

end process;

process (trn_rst, trn_clk, ebone_reset)
begin

  if ((trn_rst = '1') or (ebone_reset = '1')) then

    req_tx_valid_count_int <= (others => '0');

	elsif (trn_clk'event and trn_clk = '1') then

    if (req_tx_valid = '1') then
		  req_tx_valid_count_int <= std_logic_vector(unsigned(req_tx_valid_count_int) + 1);
		end if;

	end if;

end process;

err_drop_count     <= err_drop_count_int(14 downto 0);
tdst_rdy_count     <= tdst_rdy_count_int(14 downto 0);
req_len_sup_count  <= req_len_sup_count_int;
req_rx_valid_count <= req_rx_valid_count_int;
req_tx_valid_count <= req_tx_valid_count_int;

end; -- ep_stat

