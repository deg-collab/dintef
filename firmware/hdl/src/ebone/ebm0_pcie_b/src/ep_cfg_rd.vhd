--------------------------------------------------------------------------------
--
-- Description: configuration read module
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.ep_comp_pkg.all;
use work.ebm0_pcie_b_pkg.all;

entity ep_cfg_rd is

port (
  clk            : in std_logic;                                 -- 125MHz user clock from the endpoint module
	trn_rst        : in std_logic;                                 -- Global active high reset signal

  cfg_do         : in std_logic_vector(31 downto 0);             -- CFG interface data from the core
  cfg_rd_wr_done : in std_logic;                                 -- CFG interface successful register access operation
  cfg_di         : out std_logic_vector(31 downto 0);            -- CFG interface data to the core
  cfg_byte_en    : out std_logic_vector(3 downto 0);             -- CFG byte enable when low
  cfg_dwaddr     : out std_logic_vector(9 downto 0);             -- CFG interface DWORD address
  cfg_wr_en      : out std_logic;                                -- CFG interface register write enable
  cfg_rd_en      : out std_logic;                                -- CFG interface register read enable

	cfg_read_req_i : in std_logic;                                 -- Configuration Space Header read request
	cfgs_regs_o    : out std32_x32                                 -- Configuration Space Header registers array
);
end ep_cfg_rd;

architecture rtl of ep_cfg_rd is

signal cfg_reg_cnt        : std_logic_vector(4 downto 0);
signal cfg_reg_cnt_cl     : std_logic;
signal cfg_reg_cnt_en     : std_logic;
signal cfg_reg_cnt_tc     : std_logic;

type cfg_reg_state_type   is (i0, i1, i2, i3, i4, i5);
signal cfg_reg_state      : cfg_reg_state_type;

begin

cfg_di         <= (others => '0');
cfg_byte_en    <= "0000";
cfg_wr_en      <= '0';
cfg_reg_cnt_tc <= '1' when (cfg_reg_cnt = "100000") else '0';
cfg_dwaddr     <= "00000" & cfg_reg_cnt;

process(clk, trn_rst)
begin

	if (trn_rst = '1') then

		cfg_reg_cnt_cl <= '0';
		cfg_reg_cnt_en <= '0';
		cfg_rd_en      <= '0';
		cfg_reg_state  <= i0;

		for i in 0 to 31 loop
			cfgs_regs_o(i) <= (others => '0');
    end loop;

	elsif clk'event and clk = '1' then

		case cfg_reg_state is
			when i0			=>	if (cfg_read_req_i = '1') then
												cfg_reg_cnt_cl <= '1';
												cfg_reg_state  <= i1;
											end if;

			when i1			=>	cfg_reg_cnt_cl <= '0';
											cfg_reg_state  <= i2;

			when i2			=>	cfg_rd_en     <= '1';
											cfg_reg_state <= i3;

			when i3			=>	if (cfg_rd_wr_done = '1') then
												cfgs_regs_o(conv_integer(cfg_reg_cnt)) <= cfg_do;
												cfg_rd_en      <= '0';
												cfg_reg_cnt_en <= '1';
												cfg_reg_state  <= i4;
											end if;

			when i4			=>	cfg_reg_cnt_en <= '0';
											cfg_reg_state  <= i5;

			when i5			=>	if (cfg_reg_cnt_tc = '1') then
												cfg_reg_state <= i0;
											else
												cfg_reg_state <= i2;
											end if;

			when others	=>	cfg_reg_cnt_cl <= '0';
											cfg_reg_cnt_en <= '0';
											cfg_rd_en      <= '0';
											cfg_reg_state  <= i0;
		end case;

	end if;

end process;

process(clk, cfg_reg_cnt_cl)
begin

	if cfg_reg_cnt_cl = '1' then

		cfg_reg_cnt <= (others => '0');

	elsif clk'event and clk = '1' then

		if cfg_reg_cnt_en = '1' then
			cfg_reg_cnt <= cfg_reg_cnt + 1;
		end if;

	end if;

end process;

end; -- ep_cfg_rd

