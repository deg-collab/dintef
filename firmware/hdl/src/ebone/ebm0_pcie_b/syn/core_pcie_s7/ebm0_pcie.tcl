# THIS FILE IS AN EXAMPLE AND SHOULD BE FITTED TO THE APPLICATION
# ---------------------------------------------------------------

read_verilog [glob ../src/core_pcie_s7/*.v]
read_vhdl    [glob ../src/core_pcie_s7/*.vhd]
read_vhdl    [glob ../src/*.vhd]

set_property part xc7k325tffg900-2 [current_project];

# Setting constraints
read_xdc ebm0_pcie_a.xdc

# Synthesis
synth_design -top ebm0_pcie_a -keep_equivalent_registers
write_checkpoint -force ebm0_pcie_a_syn.dcp

# Place
opt_design -propconst -sweep -remap 
place_design -directive Explore
report_clock_utilization -file rpt_clocks.txt
report_clocks            -file rpt_clocks.txt -append
report_utilization       -file rpt_place.txt

# Route
route_design
write_checkpoint      -force par.dcp
check_timing          -file rpt_timing_check.txt
report_route_status   -file rpt_route.txt
report_timing_summary -file rpt_timing.txt
report_drc            -file rpt_drc.txt
report_io             -file rpt_io.txt
report_clock_networks -file rpt_clock_network.txt

open_checkpoint par.dcp

# bitstream
set_property BITSTREAM.CONFIG.CONFIGRATE 16 [current_design] 
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design] 
write_bitstream -force ebm0_pcie_a.bit
