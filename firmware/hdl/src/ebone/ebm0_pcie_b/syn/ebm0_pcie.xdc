##################################################################################################################################
#
# Constraints file name: ebm0_pcie.xdc
#
# FPGA family: 7
# FPGA:        XC7K325T-FFG900
# Speedgrade:  -2
#
# User constraints declarations
# -----------------------------
# THIS FILE IS AN EXAMPLE AND IS ONLY VALID WITH THE XILINX KC705 KIT BUILT AROUND THE DEVICE LISTED ABOVE
#
##################################################################################################################################
#
# Version    Date        Author
# Reference  07/03/2017  Le Caer
#
# http://www.esrf.fr
#
##################################################################################################################################

################################################################################
# PCIe Lanes 7..0
################################################################################

set_false_path -through          [get_nets pcie_sys_rst_n]
set_property PACKAGE_PIN G25     [get_ports {pcie_sys_rst_n}]
set_property IOSTANDARD LVCMOS25 [get_ports {pcie_sys_rst_n}]
set_property PULLUP true         [get_ports {pcie_sys_rst_n}]

set_property LOC GTXE2_CHANNEL_X0Y7 [get_cells {ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[0].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
set_property LOC GTXE2_CHANNEL_X0Y6 [get_cells {ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[1].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
set_property LOC GTXE2_CHANNEL_X0Y5 [get_cells {ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[2].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
set_property LOC GTXE2_CHANNEL_X0Y4 [get_cells {ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[3].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
set_property LOC GTXE2_CHANNEL_X0Y3 [get_cells {ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[4].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
set_property LOC GTXE2_CHANNEL_X0Y2 [get_cells {ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[5].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
set_property LOC GTXE2_CHANNEL_X0Y1 [get_cells {ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[6].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
set_property LOC GTXE2_CHANNEL_X0Y0 [get_cells {ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[7].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]

set_property LOC PCIE_X0Y0 [get_cells {pepu_device[0].dev_i/ebm0/s7_type.s7_ep/pcie_top_i/pcie_7x_i/pcie_block_i}]
set_property LOC IBUFDS_GTE2_X0Y1 [get_cells {pepu_device[0].dev_i/ebm0/s7_type.refclk_ibuf}]

set_property LOC RAMB36_X4Y34 [get_cells {pepu_device[0].dev_i/ebm0/s7_type.s7_ep/pcie_top_i/pcie_7x_i/pcie_bram_top/pcie_brams_rx/brams[0].ram/use_tdp.ramb36/ramb_bl.ramb36_dp_bl.ram36_bl}]
set_property LOC RAMB36_X4Y33 [get_cells {pepu_device[0].dev_i/ebm0/s7_type.s7_ep/pcie_top_i/pcie_7x_i/pcie_bram_top/pcie_brams_rx/brams[1].ram/use_tdp.ramb36/ramb_bl.ramb36_dp_bl.ram36_bl}]
set_property LOC RAMB36_X4Y31 [get_cells {pepu_device[0].dev_i/ebm0/s7_type.s7_ep/pcie_top_i/pcie_7x_i/pcie_bram_top/pcie_brams_tx/brams[0].ram/use_tdp.ramb36/ramb_bl.ramb36_dp_bl.ram36_bl}]
set_property LOC RAMB36_X4Y30 [get_cells {pepu_device[0].dev_i/ebm0/s7_type.s7_ep/pcie_top_i/pcie_7x_i/pcie_bram_top/pcie_brams_tx/brams[1].ram/use_tdp.ramb36/ramb_bl.ramb36_dp_bl.ram36_bl}]

create_clock -name ep_sys_clk -period 10.00 [get_pins {ebm0/s7_type.refclk_ibuf/O}]
create_clock -name ep_txout_clk -period 10.00 [get_pins ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[0].gt_wrapper_i/gtx_channel.gtxe2_channel_i/TXOUTCLK]

set_false_path -through [get_pins {pepu_device[0].dev_i/ebm0/s7_type.s7_ep/pcie_top_i/pcie_7x_i/pcie_block_i/PLPHYLNKUPN*}]
set_false_path -through [get_pins {pepu_device[0].dev_i/ebm0/s7_type.s7_ep/pcie_top_i/pcie_7x_i/pcie_block_i/PLRECEIVEDHOTRST*}]

set_property PACKAGE_PIN AF20    [get_ports {soft_wd_p}]
set_property IOSTANDARD LVCMOS25 [get_ports {soft_wd_p}]
set_property PACKAGE_PIN AG20    [get_ports {soft_reset_n}]
set_property IOSTANDARD LVCMOS25 [get_ports {soft_reset_n}]
set_property PACKAGE_PIN AB25    [get_ports {user_prb[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {user_prb[0]}]
set_property PACKAGE_PIN AA25    [get_ports {user_prb[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {user_prb[1]}]
set_property PACKAGE_PIN AB28    [get_ports {user_prb[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {user_prb[2]}]
set_property PACKAGE_PIN AA27    [get_ports {user_prb[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {user_prb[3]}]
set_property PACKAGE_PIN AH21    [get_ports {status_led_p[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {status_led_p[0]}]
set_property PACKAGE_PIN AG22    [get_ports {status_led_p[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {status_led_p[1]}]

################################################################################
# End
################################################################################

