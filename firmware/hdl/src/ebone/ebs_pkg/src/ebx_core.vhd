--------------------------------------------------------------------------
--
-- E-bone - extension core interconnect 
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     1.4  21/12/12    herve  Added ebx_core (extensions) 
--     1.5  04/05/13    herve  Separated ebs_core_XX distribution 
--     2.0  03/02/14    herve  Record i/o
--                             Message data extended to 16 bits
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

entity ebx_core is
port (
-- Masters
   ebx_msg_m_set_i : in  std8_a;  -- message management 
   ebx_msg_m_dat_o : out std16_a; -- message routed to receiver 
   ebx_m_dsz_o     : out std4;    -- device size, shared

-- Slaves
   ebx_msg_set_o   : out std8;    -- message management, shared
   ebx_s_i         : in ebx_oa_typ
);
end ebx_core;


--------------------------------------------------------------------------
architecture rtl of ebx_core is
--------------------------------------------------------------------------

begin

process(ebx_msg_m_set_i)
variable tmp_msg_set : std8 := "00000000";

begin
   tmp_msg_set := "00000000";
   for master in ebx_msg_m_set_i'RANGE loop
      tmp_msg_set := tmp_msg_set OR ebx_msg_m_set_i(master);
   end loop;
   ebx_msg_set_o <= tmp_msg_set;
end process; 

process(ebx_s_i)
begin

   for dest in ebx_msg_m_dat_o'RANGE loop       -- on destination (masters)
      ebx_msg_m_dat_o(dest) <= (others => '0'); -- no latch    

      for source in ebx_s_i'RANGE loop  -- on source (slaves)
         if ebx_s_i(source).ebx_msg_dst = std_logic_vector(to_unsigned(dest, 4)) then
            -- feed destination from this source
            ebx_msg_m_dat_o(dest) <= ebx_s_i(source).ebx_msg_dat; 
            -- exit the loop
            exit;
         end if;      
      end loop;

   end loop;
end process; 

process(ebx_s_i) 
variable tmp_dsz : std4 := "0000";

begin
   tmp_dsz  := "0000";
   for slave in ebx_s_i'RANGE loop
      tmp_dsz  := tmp_dsz OR ebx_s_i(slave).ebx_dsz;
   end loop;
   ebx_m_dsz_o  <= tmp_dsz; 
end process; 

end rtl;

--------------------------------------------------------------------------
--
-- E-bone - extension core interconnect, single master 
--
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

entity ebx_core1 is
port (
-- Master
   ebx_msg_m_dat_o : out std16;  -- message to (single) receiver 
   ebx_m_dsz_o     : out std4;   -- device size, shared 

-- Slaves
   ebx_s_i         : in ebx_oa_typ
);
end ebx_core1;

--------------------------------
architecture rtl of ebx_core1 is
--------------------------------

begin

process(ebx_s_i)
begin

      ebx_msg_m_dat_o <= (others => '0'); -- no latch    

      for source in ebx_s_i'RANGE loop    -- on source (slaves)
         if ebx_s_i(source).ebx_msg_dst /= "0000" then
            -- feed destination from this source
            ebx_msg_m_dat_o <= ebx_s_i(source).ebx_msg_dat; 
            -- exit the loop
            exit;
         end if;      
      end loop;

end process; 

process(ebx_s_i) 
variable tmp_dsz : std4 := "0000";

begin
   tmp_dsz  := "0000";
   for slave in ebx_s_i'RANGE loop
      tmp_dsz  := tmp_dsz OR ebx_s_i(slave).ebx_dsz;
   end loop;
   ebx_m_dsz_o  <= tmp_dsz; 
end process; 

end rtl;
