--------------------------------------------------------------------------
--
-- E-bone - Required package
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     1.0  08/12/09    herve  1st release
--     1.1  19/10/10    herve  Added eb_aef signal
--                             Nlog2() revisited
--                             type s revisited
--     1.2  12/04/12    herve  Minor edit
--     1.3  04/10/12    herve  Bug Inconsistent BRQ fixed
--     1.4  11/02/13    herve  Added ebx_core, ebx_core1 (extensions) 
--     1.5  04/05/13    herve  Separated ebs_coreXX distribution
--     2.0  03/02/14    herve  Record i/o
--                             Message data extended to 16 bits
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- Package required for any E-bone system
-- Defines common types and functions
-- Declare the core interconnect components
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;

package ebs_pkg is

subtype  std4     is std_logic_vector(3 downto 0);
subtype  std8     is std_logic_vector(7 downto 0);
subtype  std16    is std_logic_vector(15 downto 0);
subtype  std32    is std_logic_vector(31 downto 0);
subtype  std64    is std_logic_vector(63 downto 0);
subtype  std128   is std_logic_vector(127 downto 0);
subtype  std256   is std_logic_vector(255 downto 0);
type     std4_a   is array(natural RANGE <>) of std4;
type     std8_a   is array(natural RANGE <>) of std8;
type     std16_a  is array(natural RANGE <>) of std16;
type     std32_a  is array(natural RANGE <>) of std32;
type     std64_a  is array(natural RANGE <>) of std64;
type     std128_a is array(natural RANGE <>) of std128;
type     std256_a is array(natural RANGE <>) of std256;

-- FT output type (data size NOT dependent) 

type ebft_typ is record     -- FT outputs (but un-sized data write)
   eb_as      : std_logic;  -- adrs strobe
   eb_eof     : std_logic;  -- end of frame
   eb_ss      : std_logic;  -- FT size strobe
   eb_dsz     : std8;       -- FT data size qualifier
   eb_blen    : std16;      -- FT burst length in bytes
end record;

-- 32 bit interconnect types

type ebm32_typ is record    -- master outputs
   eb_as      : std_logic;  -- adrs strobe
   eb_eof     : std_logic;  -- end of frame
   eb_aef     : std_logic;  -- almost end of frame
   eb_dat     : std32;      -- data write
end record;

type ebs32_i_typ is record  -- slave inputs
   eb_rst     : std_logic;  -- synchronous system reset
   eb_bmx     : std_logic;  -- busy others
   eb_as      : std_logic;  -- adrs strobe
   eb_eof     : std_logic;  -- end of frame
   eb_aef     : std_logic;  -- almost end of frame
   eb_dat     : std32;      -- data write
end record;

type ebs32_o_typ is record  -- slave outputs
   eb_dk      : std_logic;  -- data acknowledge
   eb_err     : std_logic;  -- bus error
   eb_dat     : std32;      -- data read
end record;

type ebs32_oa_typ is array(natural RANGE <>) of ebs32_o_typ;

-- 64 bit interconnect types

type ebm64_typ is record    -- master outputs
   eb_as      : std_logic;  -- adrs strobe
   eb_eof     : std_logic;  -- end of frame
   eb_aef     : std_logic;  -- almost end of frame
   eb_dat     : std64;      -- data write
end record;

type ebs64_i_typ is record  -- slave inputs
   eb_rst     : std_logic;  -- synchronous system reset
   eb_bmx     : std_logic;  -- busy others
   eb_as      : std_logic;  -- adrs strobe
   eb_eof     : std_logic;  -- end of frame
   eb_aef     : std_logic;  -- almost end of frame
   eb_dat     : std64;      -- data write
end record;

type ebs64_o_typ is record  -- slave outputs
   eb_dk      : std_logic;  -- data acknowledge
   eb_err     : std_logic;  -- bus error
   eb_dat     : std64;      -- data read
end record;

type ebs64_oa_typ is array(natural RANGE <>) of ebs64_o_typ;

-- 128 bit interconnect types

type ebm128_typ is record   -- master outputs
   eb_as      : std_logic;  -- adrs strobe
   eb_eof     : std_logic;  -- end of frame
   eb_aef     : std_logic;  -- almost end of frame
   eb_dat     : std128;     -- data write
end record;

type ebs128_i_typ is record -- slave inputs
   eb_rst     : std_logic;  -- synchronous system reset
   eb_bmx     : std_logic;  -- busy others
   eb_as      : std_logic;  -- adrs strobe
   eb_eof     : std_logic;  -- end of frame
   eb_aef     : std_logic;  -- almost end of frame
   eb_dat     : std128;     -- data write
end record;

type ebs128_o_typ is record -- slave outputs
   eb_dk      : std_logic;  -- data acknowledge
   eb_err     : std_logic;  -- bus error
   eb_dat     : std128;     -- data read
end record;

type ebs128_oa_typ is array(natural RANGE <>) of ebs128_o_typ;

-- 256 bit interconnect types

type ebm256_typ is record   -- master outputs
   eb_as      : std_logic;  -- adrs strobe
   eb_eof     : std_logic;  -- end of frame
   eb_aef     : std_logic;  -- almost end of frame
   eb_dat     : std256;     -- data write
end record;

type ebs256_i_typ is record -- slave inputs
   eb_rst     : std_logic;  -- synchronous system reset
   eb_bmx     : std_logic;  -- busy others
   eb_as      : std_logic;  -- adrs strobe
   eb_eof     : std_logic;  -- end of frame
   eb_aef     : std_logic;  -- almost end of frame
   eb_dat     : std256;     -- data write
end record;

type ebs256_o_typ is record -- slave outputs
   eb_dk      : std_logic;  -- data acknowledge
   eb_err     : std_logic;  -- bus error
   eb_dat     : std256;     -- data read
end record;

type ebs256_oa_typ is array(natural RANGE <>) of ebs256_o_typ;

-- E-bone extension types

type ebx_o_typ is record    -- slave extension outputs
   ebx_dsz     : std4;      -- device data size
   ebx_msg_dst : std4;      -- message destination
   ebx_msg_dat : std16;     -- message data
end record;

type ebx_oa_typ is array(natural RANGE <>) of ebx_o_typ;

-- Constants
--------------------------------------------------------------------------
constant GND4    : std4     := (others => '0');  
constant GND8    : std8     := (others => '0');  
constant GND16   : std16    := (others => '0');  
constant GND32   : std32    := (others => '0');  
constant GND64   : std64    := (others => '0');  
constant GND128  : std128   := (others => '0');  
constant GND256  : std256   := (others => '0');  
constant NO_FT   : ebft_typ := ('0', '0', '0', GND8, GND16);

-- Functions
--------------------------------------------------------------------------

function Nlog2(x : natural) return integer;

-- Components
--------------------------------------------------------------------------

-- E-bone - core interconnect 
-- EBS_DWIDTH := 32

component ebs_core_32 is
generic (
   EBS_TMO_DK   : natural := 4  
);
port (
   eb_clk_i     : in  std_logic;  -- system clock
   eb_rst_i     : in  std_logic;  -- synchronous system reset

-- Fast Transmitter
   eb_ft_bg_o   : out std_logic;  -- bus grant
   eb_ft_brq_i  : in  std_logic;  -- bus request

-- Master #0 dedicated 
   eb_m0_bg_o   : out std_logic;  -- bus grant
   eb_m0_brq_i  : in  std_logic;  -- bus request
   eb_m0_i      : in  ebm32_typ;  -- master out

-- Masters shared bus
   eb_m_o       : out ebs32_o_typ;

-- From Slaves (array) 
   eb_s_i       : in  ebs32_oa_typ;

-- To Slave shared bus
   eb_s_o       : out ebs32_i_typ
);
end component;

-- E-bone - core interconnect 
-- EBS_DWIDTH := 64

component ebs_core_64 is
generic (
   EBS_TMO_DK   : natural := 4  
);
port (
   eb_clk_i     : in  std_logic;  -- system clock
   eb_rst_i     : in  std_logic;  -- synchronous system reset

-- Fast Transmitter
   eb_ft_bg_o   : out std_logic;  -- bus grant
   eb_ft_brq_i  : in  std_logic;  -- bus request

-- Master #0 dedicated 
   eb_m0_bg_o   : out std_logic;  -- bus grant
   eb_m0_brq_i  : in  std_logic;  -- bus request
   eb_m0_i      : in  ebm64_typ;  -- master out

-- Masters shared bus
   eb_m_o       : out ebs64_o_typ;

-- From Slaves (array) 
   eb_s_i       : in  ebs64_oa_typ;

-- To Slave shared bus
   eb_s_o       : out ebs64_i_typ
);
end component;

-- E-bone - core interconnect 
-- EBS_DWIDTH := 128

component ebs_core_128 is
generic (
   EBS_TMO_DK   : natural := 4 
);
port (
   eb_clk_i     : in  std_logic;  -- system clock
   eb_rst_i     : in  std_logic;  -- synchronous system reset

-- Fast Transmitter
   eb_ft_bg_o   : out std_logic;  -- bus grant
   eb_ft_brq_i  : in  std_logic;  -- bus request

-- Master #0 dedicated 
   eb_m0_bg_o   : out std_logic;  -- bus grant
   eb_m0_brq_i  : in  std_logic;  -- bus request
   eb_m0_i      : in  ebm128_typ; -- master out

-- Masters shared bus
   eb_m_o       : out ebs128_o_typ;

-- From Slaves (array) 
   eb_s_i       : in  ebs128_oa_typ;

-- To Slave shared bus
   eb_s_o       : out ebs128_i_typ
);
end component;

-- E-bone - core interconnect 
-- EBS_DWIDTH := 256

component ebs_core_256 is
generic (
   EBS_TMO_DK   : natural := 4 
);
port (
   eb_clk_i     : in  std_logic;  -- system clock
   eb_rst_i     : in  std_logic;  -- synchronous system reset

-- Fast Transmitter
   eb_ft_bg_o   : out std_logic;  -- bus grant
   eb_ft_brq_i  : in  std_logic;  -- bus request

-- Master #0 dedicated 
   eb_m0_bg_o   : out std_logic;  -- bus grant
   eb_m0_brq_i  : in  std_logic;  -- bus request
   eb_m0_i      : in  ebm256_typ; -- master out

-- Masters shared bus
   eb_m_o       : out ebs256_o_typ;

-- From Slaves (array) 
   eb_s_i       : in  ebs256_oa_typ;

-- To Slave shared bus
   eb_s_o       : out ebs256_i_typ
);
end component;

-- E-bone extension core interconnect

component ebx_core is
port (
-- Masters
   ebx_msg_m_set_i : in  std8_a;  -- message management 
   ebx_msg_m_dat_o : out std16_a; -- message routed to receiver 
   ebx_m_dsz_o     : out std4;    -- device size, shared

-- Slaves (array) 
   ebx_msg_set_o   : out std8;    -- message management, shared
   ebx_s_i         : in ebx_oa_typ
);
end component;

-- E-bone extension core interconnect
-- Single master

component ebx_core1 is
port (
-- Master 
   ebx_msg_m_dat_o : out std16;  -- message to (single) receiver 
   ebx_m_dsz_o     : out std4;   -- device size, shared

-- Slaves (array) 
   ebx_s_i         : in ebx_oa_typ
);
end component;

end package ebs_pkg;

--------------------------------------------------------------------------
package body ebs_pkg is
--------------------------------------------------------------------------

-- log2(x) function for value > 2
---------------------------------
function Nlog2(x : natural) return integer is 
   variable j  : integer := 0; 
begin 
   for i in 1 to 31 loop
      if(2**i >= x) then
         j := i;
         exit;
      end if;
   end loop;
   return j;
end Nlog2; 

end package body ebs_pkg; 
