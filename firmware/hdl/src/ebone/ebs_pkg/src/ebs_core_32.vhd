--------------------------------------------------------------------------
--
-- E-bone - core interconnect 
-- EBS_DWIDTH := 32
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     1.0  08/12/09    herve  1st release
--     1.1  19/10/10    herve  Added eb_aef signal
--                             Nlog2() revisited
--                             Types revisited
--     1.2  12/04/12    herve  Minor edit
--     1.3  04/10/12    herve  Bug Inconsistent BRQ fixed
--     1.4  11/02/13    herve  Added ebx_core, ebx_core1 (extensions) 
--     1.5  04/05/13    herve  Separated ebs_core_XX distribution 
--     2.0  30/09/13    herve  Record i/o
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- Generic constants:
-- EBS_TMO_DK =  DS to DK time out (2**EBS_TMO_DK clocks)
--
-- Arbiter features: 
-- Asynchronous
-- Self monitors inconsistent BRQ/BG
-- Time out on slave not responding
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

entity ebs_core_32 is
generic (
   EBS_TMO_DK   : natural := 4 
);
port (
   eb_clk_i     : in  std_logic;  -- system clock
   eb_rst_i     : in  std_logic;  -- synchronous system reset

-- Fast Transmitter
   eb_ft_bg_o   : out std_logic;  -- bus grant
   eb_ft_brq_i  : in  std_logic;  -- bus request

-- Master #0 dedicated 
   eb_m0_bg_o   : out std_logic;  -- bus grant
   eb_m0_brq_i  : in  std_logic;  -- bus request
   eb_m0_i      : in  ebm32_typ; 

-- Masters shared bus
   eb_m_o       : out ebs32_o_typ;

-- From Slaves (array) 
   eb_s_i       : in  ebs32_oa_typ;

-- To Slave shared bus
   eb_s_o       : out ebs32_i_typ
);
end ebs_core_32;

------------------------------------------------------------------------
architecture rtl of ebs_core_32 is
------------------------------------------------------------------------

type bustate_typ is (iddle, ft, m0);       -- asynchronous
signal bustate     : bustate_typ;

type bustats_typ is (iddle_s, ft_s, m0_s); -- synchronous
signal bustats     : bustats_typ;

signal eb_bmx      : std_logic; -- busy others
signal eb_busy     : std_logic; 
signal eb_busy2    : std_logic; 
signal eb_m_dk     : std_logic; -- DK internal
signal core_err    : std_logic; 
signal brq_err     : std_logic; 
signal cnt_dk      : unsigned(EBS_TMO_DK downto 0);

------------------------------------------------------------------------
begin
------------------------------------------------------------------------
assert EBS_TMO_DK > 1
       report "EBS_TMO_DK must be greater than 1!" severity failure;

-- Bus arbiter
--------------
process(eb_m0_brq_i, eb_ft_brq_i)
begin
   if eb_m0_brq_i = '1' then
      bustate <= m0;
   elsif eb_ft_brq_i = '1' then
      bustate <= ft;
   else
      bustate <= iddle;
   end if;
end process; 

process(bustate)
begin
       case bustate is
         when iddle => eb_ft_bg_o <= '0';
                       eb_bmx     <= '0';
                       eb_m0_bg_o <= '0'; 

         when ft    => eb_ft_bg_o <= '1';
                       eb_bmx     <= '0';
                       eb_m0_bg_o <= '0'; 

         when m0    => eb_ft_bg_o <= '0';
                       eb_bmx     <= '1';
                       eb_m0_bg_o <= '1'; 
       end case;
end process;

eb_s_o.eb_bmx <= eb_bmx;   -- internal to port
eb_s_o.eb_rst <= eb_rst_i; -- forward reset

-- Bus interconnect
-- Master to Slaves
-------------------
process(bustate, 
        eb_m0_i)
begin
       case bustate is
         when m0    => 
                       eb_s_o.eb_as   <= eb_m0_i.eb_as;
                       eb_s_o.eb_eof  <= eb_m0_i.eb_eof;
                       eb_s_o.eb_aef  <= eb_m0_i.eb_aef;
                       eb_s_o.eb_dat  <= eb_m0_i.eb_dat;

         when others => 
                       eb_s_o.eb_as   <= '0';
                       eb_s_o.eb_eof  <= '0';
                       eb_s_o.eb_aef  <= '0';
                       eb_s_o.eb_dat  <= (others => '0');
       end case;

end process; 

-- Bus interconnect
-- Slaves to Masters
--------------------------------------------------
-- BG asserted to DK asserted watch dog timer
-- Note on broadcall :
-- EBS_TMO_DK must be >= 2 to prevent false ERR
-- since slaves may not generate DK
--------------------------------------------------
process(eb_s_i,                  -- slaves
        core_err)                 -- bus monitor

variable tmp_dk : std_logic := '0';
variable tmp_err: std_logic := '0';
variable tmp_dat: std32 := (others => '0');

begin
   tmp_dk  := '0';
   tmp_err := '0';
   tmp_dat := (others => '0');
   for slave in eb_s_i'RANGE loop
      tmp_dk  := tmp_dk  OR eb_s_i(slave).eb_dk;
      tmp_err := tmp_err OR eb_s_i(slave).eb_err;
      tmp_dat := tmp_dat OR eb_s_i(slave).eb_dat;
   end loop;
   eb_m_dk   <= tmp_dk;
   eb_m_o.eb_err <= tmp_err OR core_err;
   eb_m_o.eb_dat <= tmp_dat ; 
end process; 
eb_m_o.eb_dk <= eb_m_dk; -- internal to port

process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      eb_busy  <= eb_bmx;
      eb_busy2 <= eb_busy; -- delay for preventing broadcall error
      if eb_m_dk = '1' OR eb_busy = '0' then
         cnt_dk <= (others => '0');
      elsif eb_busy2 = '1' then
         cnt_dk <= cnt_dk + 1;
      end if;
      core_err <=    std_logic(cnt_dk(EBS_TMO_DK)) 
                  OR brq_err; 
   end if;
end process; 

-- Inconsistent BRQ detector
----------------------------
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then

       brq_err <= '0';

       case bustate is
         when iddle => bustats <= iddle_s;

         when ft    => bustats <= ft_s;
               
         when m0    => bustats <= m0_s;             
       end case;

       case bustats is
         when iddle_s => brq_err <= '0';

         when ft_s    => 
              if eb_m0_brq_i = '1'  then
                 brq_err <= '1';
              end if;
                              
         when m0_s    =>               
              if eb_ft_brq_i = '1' then
                 brq_err <= '1';
              end if;
       end case;
   end if;
end process; 

end rtl;

