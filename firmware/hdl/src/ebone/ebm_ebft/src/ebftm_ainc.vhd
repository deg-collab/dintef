--------------------------------------------------------------------------
--
-- DMA controller - 64 bit destination address incrementer 
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     2.2  13/01/14    herve  Creation
--     3.0  11/05/15    herve  Added FIFO scan mode
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- Conditionnally implement the 64-bit destination address incrementer.
-- When EBFT_DA64=false the incrementer is not instantiated,
-- and the 32 most significant bits are forced to zero.
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

entity ebftm_ainc is
generic (
   EBFT_DA64    : boolean := true -- Dest. Adrs. 32/64 bits
);
port (
   eb_ft_clk_i  : in  std_logic;  -- FT system clock
   dma_damsb_i  : in  std32;      -- DMA desc. dest. address MSbits
   dx_init_i    : in  std_logic;  -- DMA desc. initialization
   fsc_damsb_i  : in  std32;      -- FIFO desc. dest. address MSbits
   fs_ald_i     : in  std_logic;  -- FIFO desc. address reload
   daburst_i    : in  std32;      -- dest. address current burst
   dupdate_i    : in  std_logic;  -- dest. address update valid
   dx_damsb_o   : out std32       -- packet dest. address MSbits
);
end ebftm_ainc;

--------------------------------------------------------------------------
architecture rtl of ebftm_ainc is
--------------------------------------------------------------------------
signal damsb_cnt : unsigned(31 downto 0);
signal damsb_en  : std_logic; 
signal dx_init2  : std_logic; 
signal fs_ald2   : std_logic; 

------------------------------------------------------------------------
begin
------------------------------------------------------------------------

-- No incrementer: zeroed MSBits
---------------------------------------------
no_inc: if NOT EBFT_DA64 generate
dx_damsb_o  <= GND32;
end generate no_inc;

-- Incrementer for destination address MSbits
---------------------------------------------
do_inc: if EBFT_DA64 generate
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if     dupdate_i = '1' 
         AND daburst_i = GND32 then
         damsb_en <= '1';
       else
         damsb_en <= '0';
       end if;
   end if;
end process;

process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      dx_init2 <= dx_init_i; -- P&R
      fs_ald2  <= fs_ald_i;  -- P&R

      if dx_init2 = '1' then
         damsb_cnt <= unsigned(dma_damsb_i);
      elsif fs_ald2 = '1' then
         damsb_cnt <= unsigned(fsc_damsb_i);
      elsif damsb_en = '1' then
         damsb_cnt <= damsb_cnt + 1;
      end if;
   end if;
end process;

dx_damsb_o <= std_logic_vector(damsb_cnt);

end generate do_inc;

end rtl;
