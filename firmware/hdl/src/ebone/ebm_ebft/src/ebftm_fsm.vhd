--------------------------------------------------------------------------
--
-- DMA controller - Destination Fast Transmitter (FT) master
-- Finite State Machine
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     0.1  18/10/10    herve  Preliminary release
--     1.0  09/02/11    herve  1st release, fifo bug corrected
--     1.1  19/07/11    herve  FIFO(MIF) retry bug fixed
--     1.2  26/06/12    herve  Added overlapping error bit and optimized
--     1.3  24/07/12    herve  Added 128 and 256 bits support
--                             Changed GCSR FT size reporting
--     1.4  04/10/12    herve  FT master bus request bug fixed
--                             Exact count on small burst (BRAM only)
--     1.5  14/11/12    herve  Exact count on small burst (all cases)
--     1.6  21/12/12    herve  Added messages, E-bone V1.3
--                             Added reset MIF command
--     1.7  11/02/13    herve  unaligned, down-sized data,
--                             error reporting
--     2.0  30/05/13    herve  Split in two clock domains
--                             control clock - Fast Transmitter clock
--                             Accurate burst size (in bytes) output
--     2.1  23/11/13    herve  Unaligned management to the last byte
--                             Source now byte addressing
--                             New regs. arrangement (NOT upwards compatible)
--                             Added ft_psize_i input port
--                             New FT signalling
--     2.2  03/02/14    herve  added software MIF reset
--                             Revisited for boundary crossing mngmt
--                             MIF FIFO mngmt moved to DMAC
--     3.0  11/05/15    herve  Added FIFO scan mode
--                             Added memory fifo virtualization
--                             Bug retry fixed
--     3.1  30/06/16    herve  Added FIFO scan flush
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- E-bone Fast Transmitter master i/f
-- Broad-cast at end of transfer (empty message)
--------------------------------------------------------------------------
-- ebx2_dsz_i : 00=32, 01=64, 10=128, 11=256
-- Ready for future dynamic size management.
-- 
-- dsize_o is so far fixed size (early) known from EBFT_DWIDTH.
--
-- In future version (with true down-sizing management)
-- the first elementary burst addressing phase should be extended
-- until dsize_o (merged with wzise_o) is known from the slave.
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

entity ebftm_fsm is
generic (
   EBFT_DWIDTH  : natural := 64;  -- FT data width 
   EBFT_FSCAN   : natural := 0;   -- max. nb. of fifo scan descriptors
   EBS_VERSION  : std8            -- IP version
);
port (
-- E-bone Fast Transmitter
   eb_ft_clk_i  : in  std_logic;  -- system clock
   eb_ft_rst_i  : in  std_logic;  -- synchronous system reset

   eb_bmx_i     : in  std_logic;  -- busy some master (but FT)
   eb_bft_i     : in  std_logic;  -- FT master bus grant
   eb_ft_brq_o  : out std_logic;  -- FT master bus request
   eb_ft_dxt_o  : out std_logic_vector(EBFT_DWIDTH-1 downto 0);
   eb_ft_as_o   : out std_logic;  -- FT adrs strobe
   eb_ft_ss_o   : out std_logic;  -- FT size strobe
   eb_ft_eof_o  : out std_logic;  -- FT end of frame
   eb_ft_dsz_o  : out std8;       -- FT data size qualifier
   eb_ft_dk_i   : in  std_logic;  -- master data acknowledge
   eb_ft_err_i  : in  std_logic;  -- master bus error
   ft_psize_i   : in  std4;       -- FT encoded payload size
   ebrq_o       : out std_logic;  -- E-bone request

-- Fscan dedicated packet processor and FSM i/f 
   typ_fscan_i  : in  std_logic;  -- fifo scan
   typ_fvmem_i  : in  std_logic;  -- fifo virtual memory
   fpp_init_i   : in  std_logic;  -- new (pseudo) descriptor start
   fpp_open_i   : in  std_logic;  -- message open command
   fpp_bcast_i  : in  std_logic;  -- do broad-cast
   fpp_rst_i    : in  std_logic;  -- fifo reset request
   fpp_done_o   : out std_logic;  -- fifo scan descriptor execution done
   fpp_skip_o   : out std_logic;  -- fifo scan descriptor skipped
   fs_open_o    : out std_logic;  -- get occupancy for flushing out

-- Other packet processor and FSM i/f 
   dma_abrt_i   : in  std_logic;  -- soft abort request
   typ_mif_i    : in  std_logic;  -- MIF type selector
   typ_fifo_i   : in  std_logic;  -- FIFO memory type selector
   dx_damsb_i   : in  std32;      -- dest. address MSbits

   pp_init_i    : in  std_logic;  -- new (pseudo) descriptor start
   pp_bcast_i   : in  std_logic;  -- do broad-cast
   pp_abrt_i    : in  std_logic;  -- abort request
   pp_flush_i   : in  std_logic;  -- flush request
   pp_clr_i     : in  std_logic;  -- clear request
   pp_rst_i     : in  std_logic;  -- reset request
   pp_done_o    : out std_logic;  -- (pseudo) descriptor execution done
   pp_err_o     : out std_logic;  -- error abort flag

   ftaerr_o     : out std_logic;  -- FT adrs. error flag
   ftderr_o     : out std_logic;  -- FT data  error flag
   adrerr_o     : out std_logic;  -- source adrs. error flag
   daterr_o     : out std_logic;  -- data error flag
   ovlerr_o     : out std_logic;  -- overlapping readout error flag
   timerr_o     : out std_logic;  -- timeout error flag
   irq_stat_i   : in  std32;      -- descriptor status for broad-casting

   dma_stat_o   : out std32;      -- DMA status
   dma_eot_o    : out std_logic;  -- end of transfer
   dma_err_o    : out std_logic;  -- transfer aborted on error

-- Burst management i/f
   mx_brq_o     : out std_logic;  -- burst request command
   mx_bwr_o     : out std_logic;  -- burst write command
   mx_clr_o     : out std_logic;  -- clear message command
   mx_open_o    : out std_logic;  -- open message command
   mx_rst_o     : out std_logic;  -- MIF reset command
   mx_read_o    : out std_logic;  -- burst read data command
   mx_aend_o    : out std_logic;  -- burst almost end command
   mx_end_o     : out std_logic;  -- burst end command
   mx_abrt_o    : out std_logic;  -- burst abort command
   mx_bg_i      : in  std_logic;  -- burst request granted
   mx_ss_i      : in  std_logic;  -- data size strobe
   mx_ardy_i    : in  std_logic;  -- burst data almost ready
   mx_rdy_i     : in  std_logic;  -- burst data ready
   mx_err_i     : in  std_logic;  -- burst error
   mx_dout_i    : in  std_logic_vector(EBFT_DWIDTH-1 downto 0);

-- MIF fifo management
   fif_rq_o     : out std_logic;  -- store requested occupancy
   fif_rdy_i    : in  std_logic;  -- FIFO occupancy is OK to transmit
   fif_emty_i   : in  std_logic;  -- FIFO empty

-- FSM related counters
   firstword_o  : out std_logic;  -- flag 1st word (in 1st elementary burst)
   dsize_o      : out std4;       -- early size for count processing (CHCH)
   wsize_o      : out std4;       -- slave size memorized for burst
   b1st_i       : in  std8;       -- 1st word MSbytes count (0=all) 

   bcld_o       : out std_logic;  -- ebone burst counter load
   bcen_o       : out std_logic;  -- ebone burst counter enable
   bcazero_i    : in  std_logic;  -- burst counter almost zero flag
   bczero_i     : in  std_logic;  -- burst counter zero flag

   fcld_o       : out std_logic;  -- fifo counter load
   fcen_o       : out std_logic;  -- fifo counter enable
   fczero_i     : in  std_logic;  -- fifo counter zero flag

   mald_o       : out std_logic;  -- mem. address counter load
   maen_o       : out std_logic;  -- mem. address counter enable
   daclr_o      : out std_logic;  -- dest. counter clear
   daen_o       : out std_logic;  -- dest. counter enable
   daendall_i   : in  std_logic;  -- dest. end flag
   daendm1_i    : in  std_logic;  -- dest. end flag minus 1
   daendm2_i    : in  std_logic;  -- dest. end flag minus 2
   daendm3_i    : in  std_logic;  -- dest. end flag minus 3
   daburst_i    : in  std32       -- dest. address current burst
);
end ebftm_fsm;

--------------------------------------------------------------------------
architecture rtl of ebftm_fsm is
--------------------------------------------------------------------------

constant EBFT_DW32  : natural := EBFT_DWIDTH/32;

type state_typ is (iddle, 
                   wait2, wait3, wait31, wait32, wait4, wait5,
                   brq1, brq2, brq3, brq9,
                   adr1, adr2, adr3, dat01, dat1, dat11, dat2,
                   end1, end2, bc0, bc1, bc2, bc3, bc4,  
                   abrt1, abrt2, abrt3, abrt4, rst1, rst2, rst3);
signal state, nextate : state_typ;

signal dat_lsw   : std32;
signal dat_msw   : std32;
signal ftw_stat  : std4;

signal ftd32   : std_logic;   -- E-bone Fast Transmitter 32 bit data 
signal alsw    : std_logic;   -- dest. address low word
signal ebadrs  : std_logic;   -- E-bone addressing phase start
signal ebrq    : std_logic;   -- E-bone request
signal irqend  : std_logic;   -- E-bone interrupt request end
signal ebft    : std_logic;   -- E-bone FT busy
signal ebbon   : std_logic;   -- E-bone FT broad-cast start
signal ebbc    : std_logic;   -- E-bone FT broad-cast burst
signal ebdat   : std_logic;   -- E-bone switch to data phase
signal ebeof   : std_logic;   -- E-bone end of frame
signal errdone : std_logic;   -- error abort flag

signal pausen  : std_logic;             -- pause counter enable
signal pauscnt : unsigned(5 downto 0);  -- pause counter 

signal mx_bwr  : std_logic; 
signal timoen  : std_logic;             -- timeout counter enable
signal timocnt : unsigned(15 downto 0); -- timeout counter 

signal bcld    : std_logic;   -- ebone burst counter load
signal bcen    : std_logic;   -- ebone burst counter enable

signal fcld    : std_logic;   -- fifo counter load
signal fcen    : std_logic;   -- fifo counter enable

signal dsize   : std4;        -- slave size info
signal firstword: std_logic;  -- flag 1st word (in 1st elementary burst)
signal fs_open : std_logic;   -- flag fifo scan open message

signal daclr    : std_logic;   -- dest. counter clear
signal daen     : std_logic;   -- dest. counter enable

signal dbld     : std_logic;   -- dest. address cur. burst load
signal mald     : std_logic;   -- mem. address counter load
signal maen     : std_logic;   -- mem. address counter enable

alias pausok    : std_logic is pauscnt(pauscnt'HIGH); -- pause counter overflow
alias timout    : std_logic is timocnt(timocnt'HIGH); -- timeout counter overflow

------------------------------------------------------------------------
begin
------------------------------------------------------------------------

-- E-bone FT data drivers
-------------------------
ft32: if EBFT_DW32=1 generate -- 32 bit width
   ftw_stat    <= "0000";
   ftd32       <= '1';
   dsize       <= ftw_stat;
   dat_lsw     <=      daburst_i  when alsw = '1' 
                  else mx_dout_i;
   eb_ft_dxt_o <=      irq_stat_i when ebbc = '1'
                  else dx_damsb_i when ebadrs = '1' 
                  else dat_lsw;
end generate;

ft64: if EBFT_DW32=2 generate -- 64 bit width
   ftw_stat    <= "0001";
   ftd32       <= '0';
   dsize       <= ftw_stat;
   dat_lsw     <=      irq_stat_i when ebbc = '1'
                  else daburst_i  when ebadrs = '1' 
                  else mx_dout_i(31 downto 0);
   dat_msw     <=      dx_damsb_i when ebadrs = '1' 
                  else mx_dout_i(63 downto 32);
   eb_ft_dxt_o <= dat_msw & dat_lsw;
end generate;

ftx: if EBFT_DW32>3 generate -- 128 bit (and more) width
   ftw_stat    <= std_logic_vector(to_unsigned(Nlog2(EBFT_DW32), 4));
   ftd32       <= '0';
   dsize       <= ftw_stat;
   dat_lsw     <=      irq_stat_i when ebbc = '1'
                  else daburst_i  when ebadrs = '1' 
                  else mx_dout_i(31 downto 0);
   dat_msw     <=      dx_damsb_i when ebadrs = '1' 
                  else mx_dout_i(63 downto 32);
   eb_ft_dxt_o <= mx_dout_i(EBFT_DWIDTH-1 downto 64) & dat_msw & dat_lsw;
end generate;

ebrq_o      <= ebrq;
dsize_o     <= dsize;
eb_ft_brq_o <= ebft;

-- Forward data size and
-- Map message to internal signals
----------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if dbld = '1' then
         wsize_o <= dsize;               -- store word size qualifier
         if firstword = '1' then
            eb_ft_dsz_o <= b1st_i;       -- 1st word bytes to keep 
         else
            eb_ft_dsz_o <= "00000000";   -- keep all of them
         end if;
      elsif mx_ardy_i = '1' then
         eb_ft_dsz_o <= "0000" & dsize;  -- forward source size 
      end if;
   end if;
end process;

-- Flag 1st word (in 1st elementary burst)
------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if eb_ft_rst_i = '1' then
         firstword <= '0';
      else          
         firstword <= (pp_init_i OR fpp_init_i OR fpp_open_i 
                       OR firstword)                                  -- set
                       AND NOT (daen OR (ebeof AND NOT eb_ft_err_i)); -- clear
      end if;
  end if;
end process;

-- Pause generator (delay before retrying)
------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if ebeof = '1' then
         pauscnt <= (others => '0');
      elsif pausen = '1' then
         pauscnt <= pauscnt + 1;
      end if;
   end if;
end process;

-- Timeout generator 
--------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if mx_bwr = '1' then
         timocnt <= (others => '0');
      elsif timoen = '1' then
         timocnt <= timocnt + 1;
      end if;
   end if;
end process;

-- E-bone FT FSM
-----------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if eb_ft_rst_i = '1' then
         ebft    <= '0';       
         ebbc    <= '0';       
         ebadrs  <= '0';       
         fs_open <= '0';       
         state   <= iddle;
      else
         ebft    <= (ebrq OR ebft)     -- elementary burst on 
                    AND NOT ebeof;   
         ebbc    <= (ebbon OR ebbc)    -- broad-cast phase  
                    AND NOT irqend;     
         ebadrs  <= (bcld OR ebadrs)   -- burst addressing phase     
                    AND NOT ebdat;   
         fs_open <= (fpp_open_i OR fs_open) -- memorize open request     
                    AND NOT (mx_err_i OR mx_rdy_i);   
         state    <= nextate;            
      end if;
   end if;
end process;

fs_open_o <= fs_open;

process(state, ftd32, 
               typ_fscan_i, typ_fvmem_i, fpp_init_i, fpp_open_i, fpp_bcast_i, fpp_rst_i,
               pp_init_i, pp_abrt_i, pp_flush_i, pp_clr_i, pp_rst_i, pp_bcast_i,
               dma_abrt_i, typ_fifo_i, typ_mif_i, fif_rdy_i, fif_emty_i, pausok, timout, fs_open,
               bcazero_i, bczero_i, fczero_i, daendm2_i, daendm3_i, daendm1_i, daendall_i, 
               eb_bmx_i, eb_bft_i, eb_ft_dk_i, eb_ft_err_i,
               mx_bg_i, mx_ardy_i, mx_rdy_i, mx_err_i)
begin
   nextate <= state; -- stay in same state
   alsw    <= '0';
   ebrq    <= '0';
   ebdat   <= '0';
   ebeof   <= '0';
   ebbon   <= '0';
   bcld    <= '0';
   bcen    <= '0';
   fcld    <= '0';
   fcen    <= '0';
   dbld    <= '0';
   daen    <= '0';
   mald    <= '0';
   maen    <= '0';
   irqend  <= '0';
   errdone <= '0';
   pausen  <= '0';
   timoen  <= '0';

   fif_rq_o  <= '0';
   ftaerr_o  <= '0';
   ftderr_o  <= '0';
   adrerr_o  <= '0';
   daterr_o  <= '0';
   ovlerr_o  <= '0';
   timerr_o  <= '0';
   pp_done_o <= '0';
   fpp_done_o <= '0';
   fpp_skip_o <= '0';

   mx_brq_o  <= '0';
   mx_bwr    <= '0';
   mx_open_o <= '0';
   mx_clr_o  <= '0';
   mx_rst_o  <= '0';
   mx_read_o <= '0';
   mx_aend_o <= '0';
   mx_end_o  <= '0';
   mx_abrt_o <= '0';

   eb_ft_eof_o <= '0';
   eb_ft_as_o  <= '0';
   eb_ft_ss_o  <= '0';

   case state is 
      when iddle =>                -- sleeping
         if pp_rst_i = '1' then 
            nextate <= rst2;
         elsif fpp_rst_i = '1' then 
            mald    <= '1';        -- store source address
            nextate <= rst2;
         elsif pp_clr_i = '1' then 
            nextate <= rst1;
         elsif pp_init_i = '1' 
            OR fpp_init_i = '1'  
            OR fpp_open_i = '1' then 
            nextate <= wait2;
         elsif pp_bcast_i = '1' then 
            nextate <= bc0;        -- broad-cast also closing message channel
         elsif fpp_bcast_i = '1' then 
            nextate <= bc1;        -- broad-cast
         end if;

      when wait2 =>                -- start new packet 
         fcld    <= '1';           -- reload FIFO burst size
         mald    <= '1';           -- store source address
         if    typ_mif_i = '1' then
            nextate <= wait3;
         elsif typ_fscan_i = '1' then
            if typ_fvmem_i = '1' then
               nextate <= wait3;
            else
               nextate <= wait31;
            end if;
         elsif typ_fifo_i = '1' then
            nextate <= wait5;
         else
            nextate <= brq1;      -- NO fifo (must be BRAM)
         end if;

-- Begin loop on MIF request

      when wait3 =>                -- MIF address depends on macnt, remsmall
         nextate <= wait31;        -- delay until OK

      when wait31 =>               -- fifo scan delay until remcnt valid
        nextate <= wait32;

      when wait32 =>               -- MIF address depends on macnt, remsmall
         fcld     <= '1';          -- reload it (now macnt, remsmall is ok)
         fif_rq_o <= '1';          -- for MIF fifo threshold processing
         nextate  <= wait4;

      when wait4 =>                -- MIF request with adrs and count
         if fs_open = '1' then     -- ... but fifo VM when 256 word rq. already done
            mx_open_o <= '1';      -- ... just open message to get fifo occupancy
         end if;
         mx_bwr  <= '1';  

         if mx_err_i = '1' then
            adrerr_o  <= '1';
            nextate <= abrt1;
         elsif mx_rdy_i = '1' then
            nextate <= wait5; 
         end if;

      when wait5 =>                -- waiting for FIFO ready
         timoen  <= '1';
         if    pp_abrt_i = '1' 
            OR mx_err_i  = '1'
            OR (fczero_i  = '1' AND typ_fscan_i = '0') then
            nextate <= abrt1;      -- something went wrong
         elsif timout = '1' AND typ_fifo_i = '0' then
            timerr_o  <= '1';
            nextate <= abrt1;      -- expected data count did not come in time
         elsif fif_rdy_i = '1'
            OR pp_flush_i = '1' then
            nextate <= brq1;       -- data ready, get them out
         elsif  typ_fscan_i = '1'  -- true fifo scan mode
            AND typ_fvmem_i = '0' then 
            fpp_skip_o <= '1';     -- fifo empty
            nextate   <= rst1;     -- nothing done
         end if;

      when brq1 =>                 -- start 1st payload burst 
         bcld    <= '1';
         nextate <= brq2;

-- Begin loop on payload bursts

      when brq2 =>                 -- begin loop on new payload burst
         dbld    <= '1';           -- load dest. adrs for this burst
         if eb_bmx_i = '0' then    -- wait for dest. E-bone NOT busy
            ebrq <= '1';           -- dest. E-bone request
            mx_brq_o  <= '1';      -- source burst request starts
            nextate <= brq3;
         end if;

      when brq3 =>                 -- check for both E-bones granted
         mx_brq_o   <= '1';        -- source burst request continues
         if eb_bft_i = '1' AND     -- granted 
            mx_bg_i  = '1' then
            nextate <= adr1; 
         else
            ebeof   <= '1';
            nextate <= brq9;       -- not granted, give up for now
         end if;

      when brq9 =>                 -- delay a while
         mx_abrt_o <= '1';
         pausen  <= '1';
         if pausok = '1' then
            nextate  <= brq2;
         end if;

      when adr1 =>                 -- check for FT slave ready
         eb_ft_as_o <= '1';        -- FT addressing phase 
         if eb_ft_err_i = '1' then 
            if eb_ft_dk_i = '1' then 
               ebeof   <= '1';
               nextate <= brq9;    -- retry
            else
               ftaerr_o <= '1';
               nextate <= abrt1;   -- fatal error
            end if;

         elsif eb_ft_dk_i = '1' then -- dest. ready
            mx_read_o <= '1';        -- ask for data
            nextate <= adr2; 
         end if;

      when adr2 =>                 -- wait for source ready
         eb_ft_as_o <= '1';        -- extend addressing phase
         mx_read_o  <= '1';        -- ask for data

         if mx_ardy_i = '1' AND    -- source soon OK
            daendm1_i = '1' then   -- remaining single data
            mx_aend_o    <= '1';
         end if;

         if eb_ft_err_i = '1' then 
            ftaerr_o   <= '1';
            nextate <= abrt1;      -- fatal error

         elsif mx_err_i = '1' then 
            adrerr_o  <= '1';
            nextate <= abrt1;      -- fatal error

         elsif ftd32 = '1' then
            if mx_ardy_i = '1' then-- source soon OK 
               eb_ft_ss_o  <= '1';
               ebdat   <= '1';     -- switch to data phase
               nextate <= adr3; 
            end if;

         elsif mx_rdy_i = '1' then -- source OK
            eb_ft_ss_o  <= '1';
            ebdat   <= '1';
            bcen    <= '1';
            fcen    <= '1'; 
            daen    <= '1';
            maen    <= '1';  
            if daendm1_i = '1' then    -- remaining single data
               mx_end_o   <= '1';
               nextate <= dat01;
            elsif daendm2_i = '1' then -- remaining 2 data        
               mx_aend_o   <= '1';
               nextate <= dat1;
            else 
               nextate <= dat1;        -- more data follow
            end if;
         end if;

      when adr3 =>  
         alsw    <= '1'; -- 32bit: put dest addrs. LSW as 1st data
         ebdat   <= '1';
         bcen    <= '1';
         fcen    <= '1'; 
         daen    <= '1';
         maen    <= '1';            
         if daendm1_i = '1' then     -- remaining single data        
            mx_end_o  <= '1';
            nextate <= dat01;
         elsif daendm2_i = '1' then  -- remaining 2 data        
            mx_aend_o   <= '1';
            nextate <= dat1; 
         else
            nextate <= dat1; 
         end if;

      when dat01 =>                -- single data 
         eb_ft_eof_o <= '1';       -- FT end of frame
         ebeof   <= '1';           -- FT bus release
         if typ_fscan_i = '1' then
            fpp_done_o <= '1';     -- fifo scan readout done
            nextate <= rst1;  
         else
            nextate <= end2; 
         end if;

      when dat1 =>                 -- data phase
         bcen    <= '1';
         fcen    <= '1'; 
         daen    <= '1';
         maen    <= '1';            
         if eb_ft_err_i = '1' OR   -- abort on FT dest. error
            eb_ft_dk_i  = '0' then 
            ftderr_o  <= '1';
            nextate <= abrt1;
         elsif mx_err_i = '1' then -- abort on source error
            daterr_o  <= '1';
            nextate <= abrt1;
         elsif bczero_i  = '1' OR    -- payload burst done
               daendm2_i = '1' then  -- short burst done
            if typ_fscan_i = '1' then
               fpp_done_o <= '1';    -- fifo scan readout done
            end if; 
            mx_end_o  <= '1';
            nextate <= dat2; 
         elsif (typ_fifo_i = '1' AND fif_emty_i = '1') then 
            if pp_flush_i = '1' then -- flushing out 
               mx_end_o  <= '1';
               nextate <= dat2; 
            else                     -- overlapping error?
               mx_end_o  <= '1';
               nextate <= dat11;
            end if;
         elsif bcazero_i = '1' OR 
               daendm3_i = '1' then  -- payload burst almost done
            mx_aend_o  <= '1';  
         end if;

      when dat11 =>                -- overlapping error pipeline flushing out
         eb_ft_eof_o <= '1';       -- FT end of frame
         ovlerr_o  <= '1';
         nextate <= abrt1;   

      when dat2 =>                 -- pipeline flushing out
         eb_ft_eof_o <= '1';       -- FT end of frame
         fif_rq_o    <= '1';       -- process next FT burst word length
         ebeof   <= '1';           -- FT bus release
         if typ_fscan_i = '1' then
            nextate <= rst1;       -- close message channel
         else
            nextate <= end1;       -- note: daendall not yet valid 
         end if; 

      when end1 =>                 -- E-bone burst end
         if daendall_i = '1' then  -- packet done
            nextate <= end2;      
         elsif typ_fifo_i = '1' AND fczero_i = '1' then -- FIFO composite burst done
            fcld    <= '1';        -- relaoad FIFO burst size
            if typ_mif_i = '1' then
               nextate <= wait3;   -- loop back to MIF request
            else
               nextate <= wait5;   -- loop back waiting for FIFO ready
            end if;
         elsif typ_fifo_i = '1' AND pp_flush_i = '1' then -- flushing out 
            nextate <= end2;      
         else   
            bcld    <= '1';
            if typ_mif_i = '1' then -- check MIF fifo occupancy
               if fif_rdy_i = '1' then 
                  nextate <= brq2;  -- loop back to next payload burst
               end if;
            else
               nextate <= brq2;     -- loop back to next payload burst
            end if;
         end if;

-- End loop on payload bursts
-- End loop on MIF/FIFO request

      when end2 =>                 -- packet done
         pp_done_o <= '1';
         nextate <= iddle; 

-- Abort on execution error

      when abrt1 =>                -- quit on error
         ebeof   <= '1';
         mx_abrt_o <= '1';         -- make sure 2nd E-bone gives up
         nextate <= abrt2;

      when abrt2 =>  
         mx_abrt_o <= '1';         -- make sure 2nd E-bone gives up
         nextate <= abrt3;

      when abrt3 =>               
         if typ_mif_i = '1' then   -- close message channel
            mx_bwr <= '1';  
            mx_clr_o <= '1';   
            if    mx_err_i = '1'   -- ignore error on closing message
               OR mx_rdy_i = '1' then
               nextate <= abrt4; 
            end if;
         else
            nextate  <= abrt4;      
         end if;

      when abrt4 =>  
         errdone <= '1'; 
         if dma_abrt_i = '1' then 
            nextate <= rst3;       -- software abort, do NOT broad-cast
         else
            nextate <= bc1;
         end if;

-- Close MIF message channel

      when rst1 =>                 -- MIF clear command
         mx_bwr <= '1';            -- for writing
         mx_clr_o <= '1';          -- reset request
         if mx_err_i = '1' OR mx_rdy_i = '1' then
            nextate <= rst3;
         end if;

      when rst2 =>                 -- MIF reset command
         mx_bwr <= '1';            -- for writing
         mx_rst_o <= '1';          -- reset request
         if mx_err_i = '1' OR mx_rdy_i = '1' then
            nextate <= rst3;
         end if;

      when rst3 =>  
         pp_done_o <= '1';
         nextate <= iddle; 

-- Broad-cast (and close message channel)

      when bc0 =>                  -- close message channel
         if typ_mif_i = '1' then
            mx_bwr <= '1';  
            mx_clr_o <= '1'; 
            if    mx_err_i = '1'   -- ignore error on closing message
               OR mx_rdy_i = '1' then
               nextate <= bc1;
            end if;
         else
            nextate  <= bc1;      
         end if;

      when bc1 =>                  -- Broad-cast
         ebbon   <= '1';
         if eb_bmx_i = '0' then    -- wait for E-bone NOT busy
            ebrq <= '1';           -- E-bone request
            nextate <= bc2;
         end if;

      when bc2 =>                  -- wait for E-bone granted
         if eb_bft_i = '1' then   
            eb_ft_as_o  <= '1';    -- broad-cast 1st clock
            eb_ft_eof_o <= '1';
            nextate <= bc4;        -- granted 
         else
            ebeof   <= '1';
            nextate <= bc3;        -- not granted
         end if;

      when bc3 =>                  -- give up for now
         nextate <= bc1;

      when bc4 =>                  -- broad-cast 2nd clock
         pp_done_o   <= '1';       -- packet processor hanshake
         eb_ft_as_o  <= '1'; 
         eb_ft_eof_o <= '1';
         irqend  <= '1';
         ebeof   <= '1';
         nextate <= iddle;   

   end case;

end process;

-- initial clear (P&R help)
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      daclr <= mald; 
   end if;
end process;

-- Controls to FT counters manager
----------------------------------
bcld_o      <= bcld;
bcen_o      <= bcen;
fcld_o      <= fcld;
fcen_o      <= fcen;
mald_o      <= mald;
maen_o      <= maen;
daclr_o     <= daclr;
daen_o      <= daen;
firstword_o <= firstword; 
mx_bwr_o    <= mx_bwr;

-- Status outputs
-----------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      dma_eot_o <= irqend;
      dma_err_o <= errdone;
   end if;
end process;
pp_err_o  <= errdone;

dma_stat_o <=   ftw_stat & fif_emty_i & fif_rdy_i & "00" & ft_psize_i 
              & std_logic_vector(to_unsigned(EBFT_FSCAN, 4))
              & "00000000"
              & EBS_VERSION;

end rtl;
