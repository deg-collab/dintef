--------------------------------------------------------------------------
--
-- DMA controller - Fifo scan internal timer
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     3.1  30/06/16    herve  Creation
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

entity ebftm_fif_timer is
generic (
   EBFT_FSCAN   : natural := 8;   -- nb. of descriptors
   EBFT_FCLK    : natural := 125  -- Clock frequency in MHz
);
port (
-- External control port @eb_clk_i
   c_fsc_cmd_i  : in  std32;     -- fifo scan command register

-- FSM and PP i/f
   eb_ft_clk_i  : in  std_logic; -- system clock
   fs_tclr_i    : in  std_logic; -- internal timer clear
   fs_irqf_i    : in  std_logic_vector(EBFT_FSCAN-1 downto 0); -- IRQ full 
   fs_itack_i   : in  std_logic; -- internal time out acknowledge
   fs_itim_o    : out std_logic  -- internal time out
);
end ebftm_fif_timer;

--------------------------------------------------------------------------
architecture rtl of ebftm_fif_timer is
--------------------------------------------------------------------------
-- For simulation purpose only
constant SIM_FAST: integer := 1
--synthesis translate_off
 * 100
--synthesis translate_on
;

-- 1 ms prescaler
constant PS_DIV  : integer := EBFT_FCLK * 1000/SIM_FAST;
signal pcnt      : unsigned(20 downto 0);
signal pcnt_tc   : std_logic; 

-- timer counter
signal tcnt_clr  : std_logic; 
signal tcnt_en   : std_logic; 
signal tcnt      : unsigned(11 downto 0); 
signal timout    : unsigned(11 downto 0); 
signal tim_en    : std_logic;
signal softim1   : std_logic; -- software force timer time out
signal softim2   : std_logic; 
signal fs_itim   : std_logic; -- internal time out

-- IRQ full change of state detector
signal irqf1     : std_logic_vector(EBFT_FSCAN-1 downto 0); 
signal irqf2     : std_logic_vector(EBFT_FSCAN-1 downto 0); 
signal irq_ok    : std_logic; 

--------------------------------------------------------------------------
begin
--------------------------------------------------------------------------

-- Change of IRQ status detector
--------------------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      irqf1 <= fs_irqf_i;
      irqf2 <= irqf1;

      irq_ok <= '0';
      if NOT (irqf2 = irqf1) then
         irq_ok <= '1';
      end if;
   end if;
end process;

-- 100 us prescaler
-------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if tcnt_clr = '1' then
         pcnt <= (others => '0');
         pcnt_tc <= '0';
      elsif pcnt = to_unsigned(PS_DIV-1, pcnt'LENGTH) then
         pcnt <= (others => '0');
         pcnt_tc <= '1';
      else
         pcnt <= pcnt + 1;
         pcnt_tc <= '0';
      end if;
   end if;
end process;

-- Timer counter
----------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      tcnt_clr <= NOT c_fsc_cmd_i(30) -- control timer enable
                  OR fs_tclr_i OR irq_ok OR fs_itim;
      tcnt_en  <= c_fsc_cmd_i(31)     -- control go
                  AND pcnt_tc; 

      if tcnt_clr = '1' then
         tcnt <= (others => '0');
      elsif tcnt_en = '1' then
         tcnt <= tcnt + 1;
      end if;
   end if;
end process;

-- Time out detector
--------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      timout  <= unsigned(c_fsc_cmd_i(15 downto 4));
      tim_en  <= c_fsc_cmd_i(30);
      softim1 <= c_fsc_cmd_i(29);
      softim2 <= softim1; -- for rising edge detection

      if fs_itack_i = '1' OR fs_tclr_i = '1' then
         fs_itim <= '0';
      elsif    (tcnt = timout AND tim_en = '1')
            OR (softim1 = '1' AND softim2 = '0') then
         fs_itim <= '1';
      end if;
   end if;
end process;

fs_itim_o <= fs_itim;

end rtl;
