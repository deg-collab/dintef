--------------------------------------------------------------------------
--
-- DMA controller - Fifo scan 
-- Packet pre-processing for boundary crossing 
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     3.0  11/05/15    herve  Creation for FIFO scan mode
--     3.1  30/06/16    herve  Added FIFO scan flush
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- E-bone Fast Transmitter master i/f
--------------------------------------------------------------------------
-- Management of boundary crossing limit is imposed by the PCIe protocol.
-- This may not be required in other cases
-- (like Ethernet or serial E-bone).
-- Possible source boundary crossing limitation is NOT managed,
-- as this might be required by some AXI4 devices.
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;
use work.ebm_ebft_pkg.all;

entity ebftm_fif_pp is
generic (
   EBFT_DWIDTH  : natural := 64;   -- FT data width 
   EBFT_DBOUND  : natural := 4096; -- Dest. boundady crossing 2**N (0=none)
   EBFT_FSCAN   : natural := 8     -- nb. of fifos
);
port (
-- Descriptor @eb_clk_i
   c_fsc_srce_i   : in  std32;   -- source
   c_fsc_sadrs_i  : in  std32;   -- source addrs. (VM)
   c_fsc_dalsb_i  : in  std32;   -- dest. addrs. LSbits
   c_fsc_damsb_i  : in  std32;   -- dest. addrs. MSbits
   c_fsc_ctrl_i   : in  std32;   -- fifo readout control

-- IRQs acknowledge @eb_clk_i
   c_fsc_irqofs_i : in  std4;  
   c_fsc_irqwr0_i : in  std_logic;  
   c_fsc_irqwr1_i : in  std_logic;  

-- Descriptor pre-processing control
   eb_ft_clk_i  : in  std_logic; -- FT system clock
   eb_ft_rst_i  : in  std_logic; -- synchronous system reset
   ft_wsize_i   : in  unsigned(7 downto 0); -- FT payload size in words
   fs_sel_i     : in  std4;      -- fifo selector
   fs_first_i   : in  std_logic; -- new scan starts
   fs_init_i    : in  std_logic; -- descriptor counter initialization
   fs_firq_i    : in  std_logic; -- force IRQ status
   fs_upcnt_i   : in  std_logic; -- descriptor counter update
   fs_tclr_i    : in  std_logic; -- internal timer clear
   fs_itack_i   : in  std_logic; -- internal time out acknowledge
   fs_ald_i     : in  std_logic; -- descriptor address reload
   fs_balign_o  : out std_logic; -- buffer filling is payload aligned
   fs_zero_o    : out std_logic; -- VM 256 word aligned
   fs_b256_o    : out std_logic; -- VM 256 word still to read
   fs_bcast_o   : out std_logic; -- request to send IRQ
   fs_hmode_o   : out std_logic; -- half full IRQ mode
   fs_finit_o   : out std_logic; -- force initialization
   fs_valid_o   : out std_logic; -- descriptor valid
   fs_etrig_o   : out std_logic; -- external trigger control
   fs_rst_o     : out std_logic; -- fifo reset request
   fs_irqtim_o  : out std_logic; -- internal timer IRQ pending
   fs_full0_o   : out std_logic; -- readout buffer went half-full
   fs_irq0_o    : out std_logic; -- IRQ#0 (half-full) pending
   fs_full1_o   : out std_logic; -- readout buffer went full
   fs_irq1_o    : out std_logic; -- IRQ#1 (full) pending
   fs_stat_o    : out std32_a(EBFT_FSCAN-1 downto 0); -- status registers

-- Address incrementer i/f
   fsc_damsb_o  : out std32;     -- fifo desc. dest. address MSbits

-- FT FSM i/f
   fif_rd_cnt_i : in  std_logic_vector(11 downto 0); -- fifo occupancy
   fs_open2_i   : in  std_logic; -- FIFO occupancy is valid
   fpp_flush_i  : in  std_logic; -- flush command
   fpp_bcast_i  : in  std_logic; -- do broad-cast
   typ_fvmem_o  : out std_logic; -- fifo virtualized memory
   typ_fvram_o  : out std_logic; -- fifo virtualized BRAM
   fs_srce_o    : out std32;     -- source location
   fs_sadrs_o   : out std32;     -- source address (VM)
   fs_dalsb_o   : out std32;     -- dest. address LSbits
   fs_bcnt_o    : out std_logic_vector(23 downto 0) -- byte count
);
end ebftm_fif_pp;

--------------------------------------------------------------------------
architecture rtl of ebftm_fif_pp is
--------------------------------------------------------------------------
constant EBFT_DWLOG : natural := Nlog2(EBFT_DWIDTH/16);
constant UPFSCAN    : natural := Nlog2(EBFT_FSCAN)-1;

subtype uns32 is unsigned(31 downto 0);
subtype uns24 is unsigned(23 downto 0);
subtype uns20 is unsigned(19 downto 0);
subtype uns8  is unsigned(7 downto 0);
subtype uns4  is unsigned(3 downto 0);
type uns8_a   is array(EBFT_FSCAN-1 downto 0) of uns8;
type uns20_a  is array(EBFT_FSCAN-1 downto 0) of uns20;

signal fs_sel    : uns4;      -- fifo selector

signal fs_iclr   : std_logic; -- IRQ pending clear all
signal fs_init1  : std_logic; 
signal fs_init2  : std_logic; 
signal fs_zero   : std_logic; -- VM 256 word aligned
signal dx_ok     : std_logic; -- packet destination is payload aligned
signal p1st_nok  : std_logic; -- flag first burst not payload aligned
signal plast_ok  : std_logic; -- flag remaining count larger than 256
signal p256_ok   : std_logic; -- flag 256 words read out
signal pcnt      : uns8_a;    -- dedicated payload count

signal wcnt      : uns20_a;   -- dedicated buffer word count
signal remcnt0   : uns20;     -- remaining word count until buffer half-full
signal remcnt1   : uns20;     -- remaining word count until buffer full
signal wmask     : uns8;      -- payload size word mask
signal wsmall    : uns8;      -- 1st small packet word size
signal fs_last0  : std_logic; -- flag last packet until buffer half-full
signal fs_last1  : std_logic; -- flag last packet until buffer full
signal fs_ald2   : std_logic; -- packet descriptor update, delayed
signal fs_hmode  : std_logic; -- half full IRQ moded

signal old_valid : std_logic_vector(EBFT_FSCAN-1 downto 0);   -- memorize not valid
signal stat0_set : std_logic_vector(EBFT_FSCAN-1 downto 0);   -- set buffer half-full
signal stat0_clr : std_logic_vector(EBFT_FSCAN-1 downto 0);   -- half-full flag clear
signal stat0_iclr: std_logic_vector(EBFT_FSCAN-1 downto 0);   -- IRQ#0 clear
signal stat1_set : std_logic_vector(EBFT_FSCAN-1 downto 0);   -- set buffer full
signal stat1_clr : std_logic_vector(EBFT_FSCAN-1 downto 0);   -- full flag clear
signal stat1_iclr: std_logic_vector(EBFT_FSCAN-1 downto 0);   -- IRQ#1 clear
signal stat_irqp : std_logic_vector(2*EBFT_FSCAN-1 downto 0); -- IRQ pending
signal stat_full : std_logic_vector(2*EBFT_FSCAN-1 downto 0); -- full/hfull status

signal tim_iclr  : std_logic; -- timer IRQ host clear
signal fsc_irqofs: std4;  
signal fsc_irqwr0: std_logic;  
signal fsc_irqwr1: std_logic;  
signal fsc_srce  : std32;
signal fsc_sadrs : std32;
signal fsc_dalsb : std32; 
signal fsc_ctrl  : std32; 

signal fs_sadrs  : uns32;   -- source address (VM)
signal fs_dalsb  : uns32;   -- packet dest. address LSbits
signal fs_bcnt   : uns24;   -- packet byte count 

------------------------------------------------------------------------
begin
------------------------------------------------------------------------

-- Control clock to FT clock domain crossing
-- or P&R help
--------------------------------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      fsc_irqwr0  <= c_fsc_irqwr0_i;
      fsc_irqwr1  <= c_fsc_irqwr1_i;
      fsc_irqofs  <= c_fsc_irqofs_i;
      fsc_srce    <= c_fsc_srce_i;
      fsc_sadrs   <= c_fsc_sadrs_i;
      fsc_ctrl    <= c_fsc_ctrl_i;
      fsc_dalsb   <= c_fsc_dalsb_i;
      fsc_damsb_o <= c_fsc_damsb_i;
   end if;
end process;

-- Trigger mask; needs to know asap to generate fpp_flush
fs_etrig_o  <= c_fsc_ctrl_i(24);

-- Virtual fifo; needs to know asap to mask off trig/tim
typ_fvmem_o <= c_fsc_ctrl_i(27); 

-- Do NOT process boundary crossing
-----------------------------------
no_bnd: if EBFT_DBOUND=0 generate
dx_ok <= '1';
end generate no_bnd;

-- DO process boundary crossing
-----------------------------------
do_bnd: if EBFT_DBOUND>0 generate

-- Check for full payload packet OK,
-- that is when destination is payload aligned
-- Note: source is word aligned by definition
--------------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      wmask <= ft_wsize_i - 1 ; -- convert 2**N to 0..01..1
   end if;
end process;

-- Small packet word offset 
wsmall <=     fs_dalsb(EBFT_DWLOG+1+7 downto EBFT_DWLOG+1) 
          AND wmask;

dx_ok  <= '1' when wsmall = "00000000"
          else '0';

end generate do_bnd;

fs_balign_o <=  dx_ok; 

-- Buffer occupancy word count processing
-----------------------------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then

      fs_sel    <= unsigned(fs_sel_i);

      if fs_init_i = '1' then
         wcnt(to_integer(fs_sel)) <= (others => '0');
      elsif fs_upcnt_i = '1' then 
         wcnt(to_integer(fs_sel)) <=  wcnt(to_integer(fs_sel)) 
                                    + fs_bcnt(EBFT_DWLOG+1+19 downto EBFT_DWLOG+1); 
      end if;
   end if;
end process;

-- Process remaining count
-- and check for last packet until buffer is (almost) half-full
-- "almost" happens when the buffer is NOT payload aligned
---------------------------------------------------------------
remcnt0  <= unsigned(fsc_ctrl(19 downto 0))/2 - wcnt(to_integer(fs_sel));

fs_last0 <= '1' when     remcnt0 <  resize(ft_wsize_i, 20)*2
                     AND remcnt0 >= resize(ft_wsize_i, 20) 
                     AND remcnt0(remcnt0'HIGH) = '0' -- positive 
            else '0';

-- Process remaining count
-- and check for last packet until buffer is full
-------------------------------------------------
remcnt1  <= unsigned(fsc_ctrl(19 downto 0)) - wcnt(to_integer(fs_sel));

fs_last1 <= '1' when  remcnt1 <= resize(ft_wsize_i, 20) 
            else '0';

plast_ok <= '1' when  remcnt1 >= to_unsigned(256, 20) 
            else '0';

-- Current byte count processing
--------------------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      fs_ald2 <= fs_ald_i;        -- delay until wsmall valid

      if    fs_open2_i  = '1'  then -- does not impact virtual fifo
          fs_bcnt <= SHIFT_LEFT(resize(unsigned(fif_rd_cnt_i), 24), EBFT_DWLOG+1);
      elsif fs_ald2 = '1' then 
         if fs_last1 = '1' then   -- last packet is always aligned
            fs_bcnt <= SHIFT_LEFT(resize(remcnt1, 24), EBFT_DWLOG+1); 
         elsif dx_ok = '1' then   -- destination is payload aligned
            fs_bcnt <= SHIFT_LEFT(resize(ft_wsize_i, 24), EBFT_DWLOG+1); 
         else                     -- 1st packet to get dest. aligned
            fs_bcnt <= SHIFT_LEFT(resize((ft_wsize_i - wsmall), 24), EBFT_DWLOG+1); 
         end if;
      end if;
   end if;
end process;

-- Destination address processing
---------------------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if fs_ald_i = '1' then
         fs_dalsb <=  unsigned(fsc_dalsb)
                    + SHIFT_LEFT(resize(wcnt(to_integer(fs_sel)), 32), EBFT_DWLOG+1);
      end if;
   end if;
end process;

-- Fifo virtualized memory source address processing
----------------------------------------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if fs_ald_i = '1' then
         fs_sadrs <=  unsigned(fsc_sadrs)
                    + resize(wcnt(to_integer(fs_sel)), 32);
      end if;
   end if;
end process;

-- fifo virtualized memory is managed by super-bursts of 256 words
-- 256 words boundary counters management
------------------------------------------------------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      fs_init1 <= fs_init_i;
      fs_init2 <= fs_init1;

      if fs_init2 = '1' AND dx_ok = '0' then
         p1st_nok <= '1'; -- flag first small burst to start with
      elsif fs_upcnt_i = '1' OR fs_init_i = '1' OR eb_ft_rst_i = '1' then 
         p1st_nok <= '0'; -- from now aligned
      end if;
   end if;
end process;

-- Manage to re-start from 256 word aligned buffer boundary

p256_ok <=    (fs_init2  AND dx_ok)      -- buffer is aligned from start
           OR (fs_upcnt_i AND p1st_nok); -- got aligment after 1st small burst

process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if    p256_ok = '1' then 
         pcnt(to_integer(fs_sel)) <= (others => '0');
      elsif fs_upcnt_i = '1' AND p1st_nok = '0' then 
         pcnt(to_integer(fs_sel)) <=  pcnt(to_integer(fs_sel)) 
                                    + fs_bcnt(EBFT_DWLOG+1+7 downto EBFT_DWLOG+1); 
      end if;
   end if;
end process;

fs_zero   <= '1' when pcnt(to_integer(fs_sel)) = "00000000"
              else '0';

fs_b256_o <= '1' when p1st_nok = '0' AND plast_ok = '1' AND fs_zero = '1'
             else '0';

fs_zero_o <= fs_zero;

-- Manage to set the half-full and IRQ#0 flags
----------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      stat0_set <= (others => '0');
      if     fs_upcnt_i = '1'    -- sampling time at the end of actual FT burst
         AND fs_last0 = '1'      -- old wcnt not yet updated
         AND fs_hmode = '1' then -- half-full mode enabled
         stat0_set(to_integer(fs_sel(UPFSCAN downto 0))) <= '1';
      end if;
   end if;
end process;

-- Manage to set the full and IRQ#1 flags
-----------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      stat1_set <= (others => '0');
      if    (fs_upcnt_i = '1'    -- sampling time at the end of actual FT burst
             OR fs_firq_i = '1')
        AND (fs_last1 = '1'      -- old wcnt not yet updated
             OR fpp_flush_i = '1')  then 
         stat1_set(to_integer(fs_sel(UPFSCAN downto 0))) <= '1';
      end if;
   end if;
end process;

-- Tell FSM when to broad-cast (on IRQ rising edge)
-- In case of flush command, this is managed internal to FSM
------------------------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if eb_ft_rst_i = '1' OR fpp_bcast_i = '1' then
         fs_bcast_o <= '0';
      elsif fs_upcnt_i = '1' then  -- sampling time at the end of actual FT burst
         if    (fs_last0 = '1' AND fs_hmode = '1'
                AND stat_irqp(to_integer(fs_sel(UPFSCAN downto 0))*2) = '0')
            OR (fs_last1 = '1' 
                AND stat_irqp(to_integer(fs_sel(UPFSCAN downto 0))*2 + 1) = '0')
         then 
            fs_bcast_o <= '1';
         end if;
      end if;
   end if;
end process;

-- FSM manages to clear the half-full and full flags
----------------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      stat0_clr <= (others => '0');
      if fs_init_i = '1' then
         stat0_clr(to_integer(unsigned(fs_sel(UPFSCAN downto 0)))) <= '1';
      end if;
   end if;
end process;

process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      stat1_clr <= (others => '0');
      if fs_init_i = '1' then
         stat1_clr(to_integer(unsigned(fs_sel(UPFSCAN downto 0)))) <= '1';
      end if;
   end if;
end process;

-- fifo dedicated buffer status management
------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if eb_ft_rst_i = '1' OR fs_iclr = '1' then
         stat_full <= (others => '0');
      else
         for ii in EBFT_FSCAN-1 downto 0 loop

            if stat0_clr(ii) = '1' then
               stat_full(2*ii) <= '0';
            elsif stat0_set(ii) = '1' then
               stat_full(2*ii) <= '1';
            end if;

            if    stat1_clr(ii) = '1' then 
               stat_full(2*ii+1) <= '0';
            elsif stat1_set(ii) = '1' then
               stat_full(2*ii+1) <= '1';
            end if;
         end loop; 
      end if;
   end if;
end process;

-- Host manages to clear the half-full and full IRQs
----------------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      stat0_iclr <= (others => '0');
      if fsc_irqwr0 = '1' then
         stat0_iclr(to_integer(unsigned(fsc_irqofs(UPFSCAN downto 0)))) <= '1';
      end if;
   end if;
end process;

process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      stat1_iclr <= (others => '0');
      if fsc_irqwr1 = '1' then
         stat1_iclr(to_integer(unsigned(fsc_irqofs(UPFSCAN downto 0)))) <= '1';
      end if;
   end if;
end process;

process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      tim_iclr <= '0';
      if fsc_irqwr1 = '1' AND fsc_irqofs = "1111" then
         tim_iclr <= '1';
      end if;
   end if;
end process;

-- Internal timer IRQ pending status
------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if    tim_iclr = '1' 
         OR fs_tclr_i = '1' then
         fs_irqtim_o <= '0';
      elsif fs_itack_i = '1' then
         fs_irqtim_o <= '1';
      end if;
   end if;
end process;

-- fifo dedicated IRQ status bits management
--------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if eb_ft_rst_i = '1' OR fs_iclr = '1' then
         stat_irqp <= (others => '0');
      else
         for ii in EBFT_FSCAN-1 downto 0 loop

            if stat0_iclr(ii) = '1' then
               stat_irqp(2*ii) <= '0';
            elsif stat0_set(ii) = '1' then
               stat_irqp(2*ii) <= '1';
            end if;

            if stat1_iclr(ii) = '1' 
               OR tim_iclr = '1' then
               stat_irqp(2*ii+1) <= '0';
            elsif stat1_set(ii) = '1' then
               stat_irqp(2*ii+1) <= '1';
            end if;

         end loop; 
      end if;
   end if;
end process;

-- FIFO scan status registers
-----------------------------
fstat: for ii in EBFT_FSCAN-1 downto 0 generate
   fs_stat_o(ii) <= stat_irqp(2*ii+1 downto 2*ii) & 
                    std_logic_vector(resize(wcnt(ii), 30));
end generate fstat; 

-- Descriptor valid change of state detector
--------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if fs_first_i = '1' then
         old_valid <= (others => '0');
      elsif fs_ald_i = '1' then
         old_valid(to_integer(fs_sel(UPFSCAN downto 0))) <= fsc_ctrl(31);
      end if;

      fs_iclr <= fs_first_i; -- P&R
   end if;
end process;

fs_finit_o <=         fsc_ctrl(31) 
              AND NOT old_valid(to_integer(fs_sel(UPFSCAN downto 0)));

-- Some flags for driving the FSM
---------------------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      fs_full0_o <= stat_full(2*to_integer(fs_sel(UPFSCAN downto 0)));
      fs_irq0_o  <= stat_irqp(2*to_integer(fs_sel(UPFSCAN downto 0)));
      fs_full1_o <= stat_full(2*to_integer(fs_sel(UPFSCAN downto 0))+1);
      fs_irq1_o  <= stat_irqp(2*to_integer(fs_sel(UPFSCAN downto 0))+1);
   end if;
end process;

typ_fvram_o <= fsc_ctrl(26);
fs_rst_o    <= fsc_ctrl(29);
fs_valid_o  <= fsc_ctrl(31);
fs_hmode    <= fsc_ctrl(30);
fs_hmode_o  <= fs_hmode;

-- Build the packet descriptor
------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if fs_ald_i = '1' then
         fs_srce_o  <= fsc_srce;
      end if;
   end if;
end process;

-- Internal exported for execution
fs_sadrs_o <= std_logic_vector(fs_sadrs);
fs_dalsb_o <= std_logic_vector(fs_dalsb);
fs_bcnt_o  <= std_logic_vector(fs_bcnt); 

end rtl;
