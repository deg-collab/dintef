--------------------------------------------------------------------------
--
-- DMA controller - fifo scan descriptors 
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     3.0  11/05/15    herve  Creation for FIFO scan mode
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

entity ebftm_fif_desc is
generic (
   EBFT_FSCAN   : natural := 8    -- nb. of descriptors
);
port (
-- Fifo scan status array @ft_clk
   fs_stat_i    : in  std32_a(EBFT_FSCAN-1 downto 0);
   fs_sel_i     : in  std4;       -- fifo selector
   fs_irqtim_i  : in  std_logic;  -- internal timer IRQ pending
   fs_irqf_o    : out std_logic_vector(EBFT_FSCAN-1 downto 0); -- IRQ full

-- Control i/f
   eb_clk_i     : in  std_logic;  -- system clock
   fsc_mwr_i    : in  std_logic;  -- mem. desc. write enable
   fsc_madrs_i  : in  std8;       -- mem. desc. address
   fsc_mdati_i  : in  std32;      -- mem. desc. data in
   fsc_mdato_o  : out std32;      -- mem. desc. data out

   fsc_sreg_o   : out std32_a(EBFT_FSCAN-1 downto 0);
   fsc_irqs_o   : out std32;      -- IRQ status

-- Selected descriptor output
   fsc_srce_o   : out std32;      -- source
   fsc_sadrs_o  : out std32;      -- source addrs. (VM)
   fsc_dalsb_o  : out std32;      -- dest. addrs. LSbits
   fsc_damsb_o  : out std32;      -- dest. addrs. MSbits
   fsc_ctrl_o   : out std32       -- fifo readout control
);
end ebftm_fif_desc;

--------------------------------------------------------------------------
architecture rtl of ebftm_fif_desc is
--------------------------------------------------------------------------
constant UPFSCAN2 : natural := Nlog2(EBFT_FSCAN)-1+2;

type ram_typ is array (EBFT_FSCAN-1 downto 0) of std32; 
signal desc_srce  : ram_typ;
signal desc_dalsb : ram_typ;
signal desc_damsb : ram_typ;
signal desc_ctrl  : ram_typ;
signal desc_sadrs : ram_typ;

signal fsc_sel  : std4;      -- fifo selector
signal selm     : std_logic_vector(2 downto 0); 
signal fvmem    : std_logic; -- fifo virtualized memory
signal wr_srce  : std_logic;
signal wr_dalsb : std_logic;
signal wr_damsb : std_logic;
signal wr_ctrl  : std_logic;
signal wr_sadrs : std_logic;

signal rd_srce  : std32;
signal rd_dalsb : std32;
signal rd_damsb : std32;
signal rd_ctrl  : std32;
signal rd_sadrs : std32;

signal fsc_irqtim : std_logic;  -- internal timer IRQ pending
signal irqs       : std_logic_vector(2*EBFT_FSCAN-1 downto 0); 
signal irqfill    : std_logic_vector(30 downto 2*EBFT_FSCAN) 
                    := (others => '0'); 

------------------------------------------------------------------------
begin
------------------------------------------------------------------------
-- Generate the IRQ full status
-------------------------------
irqf: for ii in EBFT_FSCAN-1 downto 0 generate
   fs_irqf_o(ii) <= fs_stat_i(ii)(31);
end generate;

-- Clock domain crossing
-------------------------
process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      fsc_sreg_o <= fs_stat_i;
      fsc_sel    <= fs_sel_i;
   end if;
end process;

-- Build up the composite IRQ status
------------------------------------
process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      fsc_irqtim <= fs_irqtim_i;
      for ii in EBFT_FSCAN-1 downto 0 loop
         irqs(2*ii+1 downto 2*ii) <= fs_stat_i(ii)(31 downto 30);
      end loop;
   end if;
end process;

fsc_irqs_o <= fsc_irqtim & irqfill & irqs;

-- Descriptor memories write enable
-----------------------------------
fvmem    <= fsc_madrs_i(5) AND fsc_madrs_i(4);
wr_srce  <= fsc_mwr_i AND NOT fvmem AND NOT fsc_madrs_i(1) AND NOT fsc_madrs_i(0);
wr_dalsb <= fsc_mwr_i AND NOT fvmem AND NOT fsc_madrs_i(1) AND     fsc_madrs_i(0);
wr_damsb <= fsc_mwr_i AND NOT fvmem AND     fsc_madrs_i(1) AND NOT fsc_madrs_i(0);
wr_ctrl  <= fsc_mwr_i AND NOT fvmem AND     fsc_madrs_i(1) AND     fsc_madrs_i(0);
wr_sadrs <= fsc_mwr_i AND     fvmem;

-- Descriptor memories 
----------------------
process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      if wr_srce = '1' then
         desc_srce(to_integer(unsigned(fsc_madrs_i(UPFSCAN2 downto 2)))) <= fsc_mdati_i;
      end if;
   end if;
end process;
rd_srce    <= desc_srce(to_integer(unsigned(fsc_madrs_i(UPFSCAN2 downto 2))));
fsc_srce_o <= desc_srce(to_integer(unsigned(fsc_sel)));

process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      if wr_dalsb = '1' then
         desc_dalsb(to_integer(unsigned(fsc_madrs_i(UPFSCAN2 downto 2)))) <= fsc_mdati_i;
      end if;
   end if;
end process;
rd_dalsb    <= desc_dalsb(to_integer(unsigned(fsc_madrs_i(UPFSCAN2 downto 2))));
fsc_dalsb_o <= desc_dalsb(to_integer(unsigned(fsc_sel)));

process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      if wr_damsb = '1' then
         desc_damsb(to_integer(unsigned(fsc_madrs_i(UPFSCAN2 downto 2)))) <= fsc_mdati_i;
      end if;
   end if;
end process;
rd_damsb    <= desc_damsb(to_integer(unsigned(fsc_madrs_i(UPFSCAN2 downto 2))));
fsc_damsb_o <= desc_damsb(to_integer(unsigned(fsc_sel)));

process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      if wr_ctrl = '1' then
         desc_ctrl(to_integer(unsigned(fsc_madrs_i(UPFSCAN2 downto 2)))) <= fsc_mdati_i;
      end if;
   end if;
end process;
rd_ctrl    <= desc_ctrl(to_integer(unsigned(fsc_madrs_i(UPFSCAN2 downto 2))));
fsc_ctrl_o <= desc_ctrl(to_integer(unsigned(fsc_sel)));

process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      if wr_sadrs = '1' then
         desc_sadrs(to_integer(unsigned(fsc_madrs_i(UPFSCAN2-2 downto 0)))) <= fsc_mdati_i;
      end if;
   end if;
end process;
rd_sadrs    <= desc_sadrs(to_integer(unsigned(fsc_madrs_i(UPFSCAN2-2 downto 0))));
fsc_sadrs_o <= desc_sadrs(to_integer(unsigned(fsc_sel)));

-- memory read output pipeline
------------------------------
selm <= fvmem & fsc_madrs_i(1 downto 0);
process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      case selm is
         when "000"    => fsc_mdato_o <= rd_srce;
         when "001"    => fsc_mdato_o <= rd_dalsb;
         when "010"    => fsc_mdato_o <= rd_damsb;
         when "011"    => fsc_mdato_o <= rd_ctrl;
         when others   => fsc_mdato_o <= rd_sadrs;
      end case;
   end if;
end process;

end rtl;
