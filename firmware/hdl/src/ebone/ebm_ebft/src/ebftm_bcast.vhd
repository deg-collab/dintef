--------------------------------------------------------------------------
--
-- DMA controller - Broad-cast data management 
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     3.0  11/05/15    herve  Creation
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.ebs_pkg.all;

entity ebftm_bcast is
port (
   eb_ft_clk_i  : in  std_logic; -- FT system clock
   pp_err_i     : in  std_logic; -- error abort flag
   run0_i       : in  std_logic; -- descriptor #0 running
   run1_i       : in  std_logic; -- descriptor #1 running
   pp_bcast_i   : in  std_logic; -- do broad-cast
   dma0_stat_i  : in  std32;     -- descriptor #0 status 
   dma1_stat_i  : in  std32;     -- descriptor #1 status 

   fsc_run_i    : in  std_logic; -- fifo scan running
   fpp_bcast_i  : in  std_logic; -- do broad-cast
   fsc_stat_i   : in  std32;     -- fifo scan status for broad-casting

   irq_stat_o   : out std32      -- descriptor status for broad-casting
);
end ebftm_bcast;

--------------------------------------------------------------------------
architecture rtl of ebftm_bcast is
--------------------------------------------------------------------------
signal ebbc0   : std_logic; -- descriptor #0 broad-cast enable
signal ebbc1   : std_logic; -- descriptor #1 broad-cast enable
signal ebbcf   : std_logic; -- fifo scan broad-cast enable
signal fscrun  : std_logic; -- fifo scan running

--------------------------------------------------------------------------
begin
--------------------------------------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      fscrun <= fsc_run_i;
      ebbc0  <= run0_i  AND (pp_bcast_i  OR pp_err_i);
      ebbc1  <= run1_i  AND (pp_bcast_i  OR pp_err_i);
      ebbcf  <= fscrun  AND (fpp_bcast_i OR pp_err_i);

      if    ebbc0 = '1'  then 
            irq_stat_o <= '0' & dma0_stat_i(30 downto 0); 
      elsif ebbc1 = '1'  then
            irq_stat_o <= '0' & dma1_stat_i(30 downto 0); 
      elsif ebbcf = '1'  then
            irq_stat_o <= '1' & fsc_stat_i(30 downto 0); 
      end if;
   end if;
end process;

end rtl;
