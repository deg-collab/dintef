--------------------------------------------------------------------------
--
-- DMA controller - Packet pre-processing 
-- for 4K boundary crossing 
-- in standard mode management
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     2.2  13/01/14    herve  Creation
--     3.0  11/05/15    herve  Split up
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- E-bone Fast Transmitter master i/f
--------------------------------------------------------------------------
-- Management of boundary crossing limit is imposed by the PCIe protocol.
-- This may not be required in other cases
-- (like Ethernet or serial E-bone).
-- Possible source boundary crossing limitation is NOT managed,
-- as this might be required by some AXI4 devices.
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;
use work.ebm_ebft_pkg.all;

entity ebftm_mem_pp is
generic (
   EBFT_DWIDTH  : natural := 64;   -- FT data width 
   EBFT_DBOUND  : natural := 4096; -- Dest. boundady crossing 2**N (0=none)
   EBFT_ASYNCH  : boolean := false -- true when different clock domains
);
port (
-- DMAC registers @eb_clk_i
   c_dma_regs_i : in  std32_a(11 downto 0);

-- DMAC registers control bits
   eb_ft_clk_i  : in  std_logic; -- FT system clock
   dma0_go_o    : out std_logic;
   dma0_link_o  : out std_logic;
   dma0_abrt_o  : out std_logic;
   dma0_flush_o : out std_logic;
   dma0_fifo_o  : out std_logic;
   dma0_mif_o   : out std_logic;
   dma1_go_o    : out std_logic;
   dma1_link_o  : out std_logic;
   dma1_abrt_o  : out std_logic;
   dma1_flush_o : out std_logic;
   dma1_fifo_o  : out std_logic;
   dma1_mif_o   : out std_logic;

-- Descriptor pre-processing control
   dx_init_i    : in  std_logic; -- packet descriptor initialization
   dx_load_i    : in  std_logic; -- packet descriptor count re-load
   run0_i       : in  std_logic; -- descriptor #0 running
   run1_i       : in  std_logic; -- descriptor #1 running
   pp_zero_o    : out std_logic; -- remaining zero byte count

-- Address incrementer i/f
   dma_damsb_o  : out std32;     -- DMA desc. dest. address MSbits

-- FT FSM i/f
   dx_srce_o    : out std32;     -- source location
   dx_sadrs_o   : out std32;     -- source address
   dx_dalsb_o   : out std32;     -- dest. address LSbits
   dx_bcnt_o    : out std_logic_vector(23 downto 0) -- byte count
);
end ebftm_mem_pp;

--------------------------------------------------------------------------
architecture rtl of ebftm_mem_pp is
--------------------------------------------------------------------------
constant EBFT_DWLOG : natural := Nlog2(EBFT_DWIDTH/16);
constant UPBOUND    : natural := Nlog2(EBFT_DBOUND)-1;
constant DZERO      : unsigned(UPBOUND downto 0) := (others => '0');

subtype uns32 is unsigned(31 downto 0);
subtype uns24 is unsigned(23 downto 0);

signal dma_regs  : std32_a(11 downto 0); -- memory descriptors

signal dx_init2  : std_logic; 
signal dx_init3  : std_logic; 
signal dx_load2  : std_logic; 
signal dx_load3  : std_logic; 

signal dx_srce   : std32;   -- packet source location
signal dx_sadrs  : uns32;   -- packet source address
signal dx_dalsb  : uns32;   -- packet dest. address LSbits
signal dx_bcnt   : uns24;   -- packet byte count 

signal dx_4kok   : std_logic; -- packet destination is 4K aligned
signal align_sce : std_logic; -- source is word aligned
signal rem_ok    : std_logic; -- remaining small count
signal rem_bcnt  : uns24; -- remaining byte count
signal cnt4k     : unsigned(23 downto 0)
                 := to_unsigned(EBFT_DBOUND, 24);

------------------------------------------------------------------------
begin
------------------------------------------------------------------------

-- Control clock to FT clock domain crossing
--------------------------------------------
fta: if EBFT_ASYNCH generate
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      dma_regs <= c_dma_regs_i;
   end if;
end process;
end generate fta;

fts: if NOT EBFT_ASYNCH generate -- does nothing
   dma_regs  <= c_dma_regs_i; 
end generate fts;

dma0_go_o    <= dma_regs(5)(31);
dma0_link_o  <= dma_regs(5)(30);
dma0_abrt_o  <= dma_regs(5)(29);
dma0_flush_o <= dma_regs(5)(28);
dma0_fifo_o  <= dma_regs(5)(27);
dma0_mif_o   <= dma_regs(5)(26);
dma1_go_o    <= dma_regs(5+6)(31);
dma1_link_o  <= dma_regs(5+6)(30);
dma1_abrt_o  <= dma_regs(5+6)(29);
dma1_flush_o <= dma_regs(5+6)(28);
dma1_fifo_o  <= dma_regs(5+6)(27);
dma1_mif_o   <= dma_regs(5+6)(26);

-- Do NOT process boundary crossing
-----------------------------------
no_bnd: if EBFT_DBOUND=0 generate
dx_4kok <= '1';
rem_ok  <= '1';
end generate no_bnd;

-- DO process boundary crossing
-----------------------------------
do_bnd: if EBFT_DBOUND>0 generate

-- Check for single packet OK:
--    Destination is 4K aligned AND source is word aligned
-- OR small packet that does not cross 4K boundary
-----------------------------------------------------------

-- Is source word aligned?
align_sce <= '1' when unsigned(dx_sadrs(EBFT_DWLOG downto 0)) = 0
             else '0';

-- Is single packet OK?
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if   (    dx_dalsb(UPBOUND downto 0) = DZERO
            AND align_sce = '1' )     
         OR resize(dx_dalsb(UPBOUND downto 0), 24) + rem_bcnt < cnt4k then
         dx_4kok <= '1';
      else
         dx_4kok <= '0';
      end if;
   end if;
end process;

-- Ckeck remaining small count
------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if rem_bcnt <= cnt4k then 
         rem_ok <= '1';
      else
         rem_ok <= '0';
      end if;
   end if;
end process;

end generate do_bnd;

-- Current byte count processing pipeline delay
----------------------------------------------- 
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      dx_init2 <= dx_init_i;
      dx_init3 <= dx_init2;
      dx_load2 <= dx_load_i; 
      dx_load3 <= dx_load2; 
   end if;
end process;


-- Remaining byte count processing
----------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if dx_init_i = '1' then    -- 1st pass
         if    run0_i = '1' then
            rem_bcnt <= unsigned(dma_regs(5)(23 downto 0));
         elsif run1_i = '1' then
            rem_bcnt <= unsigned(dma_regs(5+6)(23 downto 0));
         end if;
      elsif dx_load_i = '1' then -- next passes
         rem_bcnt <= rem_bcnt - dx_bcnt;
      end if;
   end if;
end process;
pp_zero_o <= '1' when rem_bcnt = 0 else '0';

-- Current byte count processing
--------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if dx_init3 = '1' then 
         if dx_4kok = '1' then   -- 4K aligned, single packet does all
            dx_bcnt <= rem_bcnt; 
         else                    -- 1st packet to get dest. aligned
            dx_bcnt <= cnt4k - resize(dx_dalsb(UPBOUND downto 0), 24); 
         end if;

      elsif dx_load3 = '1' then 
         if   rem_ok  = '1'      -- last packet is smaller or eq. 4K
           OR dx_4kok = '1' then -- 4K aligned, single packet does all
            dx_bcnt <= rem_bcnt; 
          else
            dx_bcnt <= cnt4k;    -- loop on 4K packets
         end if;
      end if;
   end if;
end process;

-- Build the packet descriptor
------------------------------
dma_damsb_o <=      dma_regs(4) when run0_i = '1'
               else dma_regs(4+6);

process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if dx_init_i = '1' then
         if    run0_i = '1' then
            dx_srce  <= dma_regs(0);
         elsif run1_i = '1' then
            dx_srce  <= dma_regs(0+6);
         end if;
      end if;
   end if;
end process;

process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if dx_init_i = '1' then
         if    run0_i = '1' then
            dx_sadrs <= unsigned(dma_regs(1));
            dx_dalsb <= unsigned(dma_regs(3));
         elsif run1_i = '1' then
            dx_sadrs <= unsigned(dma_regs(1+6));
            dx_dalsb <= unsigned(dma_regs(3+6));
         end if;

      elsif dx_load_i = '1' then
         dx_sadrs <= dx_sadrs + resize(dx_bcnt, 32);
         dx_dalsb <= dx_dalsb + resize(dx_bcnt, 32);       
      end if;
   end if;
end process;

-- Internal exported for execution
dx_srce_o  <= dx_srce;
dx_sadrs_o <= std_logic_vector(dx_sadrs);
dx_dalsb_o <= std_logic_vector(dx_dalsb);
dx_bcnt_o  <= std_logic_vector(dx_bcnt); 

end rtl;
