--------------------------------------------------------------------------
--
-- DMA controller - MIF fifo occupancy management
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     2.2  03/02/14    herve  Creation
--     3.0  11/05/15    herve  bug max size fixed
--                             fifo virtualized memory support
--     3.1  30/06/16    herve  Added FIFO scan flush
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

entity ebftm_mif is
port (
   eb_ft_clk_i    : in  std_logic;  -- system clock
   ebx2_msg_dat_i : in  std16;      -- message data 
   ft_wsize_i     : in  unsigned(7 downto 0);  -- FT payload size in words
   remcnt_i       : in  unsigned(23 downto 0); -- remaining count in words
   fs_open_i      : in  std_logic;  -- get occupancy for flushing out
   fs_open2_o     : out std_logic;  -- FIFO occupancy is valid
   typ_fvram_i    : in  std_logic;  -- fifo virtualized BRAM
   fif_rq_i       : in  std_logic;  -- store requested occupancy
   fif_rd_cnt_o   : out std_logic_vector(11 downto 0); -- fifo occupancy
   fif_rdy_o      : out std_logic;  -- FIFO occupancy is OK to transmit
   fif_emty_o     : out std_logic   -- FIFO empty
);
end ebftm_mif;

--------------------------------------------------------------------------
architecture rtl of ebftm_mif is
--------------------------------------------------------------------------
signal rd_cnt    : std_logic_vector(11 downto 0); -- fifo occupancy
signal fif_thld  : std_logic_vector(11 downto 0); 
signal fif_emty  : std_logic; 
signal fs_open1  : std_logic; 

--------------------------------------------------------------------------
begin
--------------------------------------------------------------------------

-- Determine the minimum fifo occupancy threshold

process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if fif_rq_i = '1' then
         if remcnt_i >= resize(ft_wsize_i, 24) then 
            fif_thld <= GND4 & std_logic_vector(ft_wsize_i);
         else  
            fif_thld <= std_logic_vector(remcnt_i(11 downto 0));
         end if;
      end if;

      fs_open1   <= fs_open_i;
      fs_open2_o <= fs_open1; 
   end if;
end process;


-- Generate the green light to start FT burst

fif_emty  <= ebx2_msg_dat_i(14);
rd_cnt    <= ebx2_msg_dat_i(11 downto 0); -- message data is fifo occupancy

process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if    fs_open_i = '1'
         OR typ_fvram_i = '1' then 
         fif_rdy_o <= NOT fif_emty;
      else
         if rd_cnt >= fif_thld then
            fif_rdy_o <= '1';
         else
            fif_rdy_o <= '0';
         end if;
      end if;
   end if;
end process;

-- FIFO occupancy status
-- Truncate at payload size

fif_emty_o   <= fif_emty;

process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
     if fs_open1 = '1' then -- rd_cnt may be late when fif_emty de-asserted
         if rd_cnt >= std_logic_vector(resize(ft_wsize_i, rd_cnt'LENGTH)) then
          fif_rd_cnt_o <= std_logic_vector(resize(ft_wsize_i, rd_cnt'LENGTH));
         else
          fif_rd_cnt_o <= rd_cnt; 
         end if;
      end if;
   end if;
end process;

end rtl;
