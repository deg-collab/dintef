--------------------------------------------------------------------------
--
-- DMA controller - Packet pre-processing state machine
-- for 4K boundary crossing 
-- in standart mode management
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     2.2  13/01/14    herve  Creation
--     3.0  11/05/15    herve  Split up, added FIFO scan mode
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- E-bone Fast Transmitter master i/f
-- Broad-cast at end of transfer (empty message)
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;
use work.ebm_ebft_pkg.all;

entity ebftm_mem_fsm is
generic (
   EBFT_ASYNCH  : boolean := false -- true when different clock domains
);
port (
-- External control ports @eb_clk_i
   c_cmd_go_i     : in  std_logic; -- go command
   c_cmd_flush_i  : in  std_logic; -- flush command
   c_cmd_abort_i  : in  std_logic; -- abort command
   c_cmd_reset_i  : in  std_logic; -- MIF reset command

-- DMAC registers control bits
   eb_ft_clk_i  : in  std_logic;  -- FT system clock
   eb_ft_rst_i  : in  std_logic;  -- synchronous system reset
   dma0_go_i    : in  std_logic;
   dma0_link_i  : in  std_logic;
   dma0_abrt_i  : in  std_logic;
   dma0_flush_i : in  std_logic;
   dma0_fifo_i  : in  std_logic;
   dma0_mif_i   : in  std_logic;
   dma1_go_i    : in  std_logic;
   dma1_link_i  : in  std_logic;
   dma1_abrt_i  : in  std_logic;
   dma1_flush_i : in  std_logic;
   dma1_fifo_i  : in  std_logic;
   dma1_mif_i   : in  std_logic;

-- Fifo scan i/f
   fsc_run_i    : in  std_logic; 

-- Descriptor pre-processing control
   dx_init_o    : out std_logic; -- packet descriptor initialization
   dx_load_o    : out std_logic; -- packet descriptor count re-load
   run0_o       : out std_logic; -- descriptor #0 running
   run1_o       : out std_logic; -- descriptor #1 running
   pp_zero_i    : in  std_logic; -- remaining zero byte count

-- FT FSM i/f
   typ_mif_o    : out std_logic;  -- MIF type selector
   typ_fifo_o   : out std_logic;  -- fifo memory type selector
   dma_abrt_o   : out std_logic;  -- soft abort request

   pp_init_o    : out std_logic;  -- new (pseudo) descriptor start
   pp_bcast_o   : out std_logic;  -- do broad-cast
   pp_abrt_o    : out std_logic;  -- abort request
   pp_flush_o   : out std_logic;  -- flush request
   pp_clr_o     : out std_logic;  -- clear request
   pp_rst_o     : out std_logic;  -- reset request
   pp_done_i    : in  std_logic;  -- (pseudo) descriptor execution done
   pp_err_i     : in  std_logic;  -- error abort flag

   daen_i       : in  std_logic;  -- dest. counter enable
   ftaerr_i     : in  std_logic;  -- FT adrs. error flag
   ftderr_i     : in  std_logic;  -- FT data  error flag
   adrerr_i     : in  std_logic;  -- source adrs. error flag
   daterr_i     : in  std_logic;  -- data error flag
   ovlerr_i     : in  std_logic;  -- overlapping readout error flag
   timerr_i     : in  std_logic;  -- timeout error flag

-- Status 
   dma0_stat_o  : out std32;      -- descriptor #0 status 
   dma1_stat_o  : out std32       -- descriptor #1 status 
);
end ebftm_mem_fsm;

--------------------------------------------------------------------------
architecture rtl of ebftm_mem_fsm is
--------------------------------------------------------------------------

signal cmd_go1    : std_logic;  -- go command
signal cmd_flush1 : std_logic;  -- flush command
signal cmd_abort1 : std_logic;  -- abort command
signal cmd_reset1 : std_logic;  -- MIF reset command
signal cmd_go2    : std_logic;
signal cmd_flush2 : std_logic; 
signal cmd_abort2 : std_logic;
signal cmd_reset2 : std_logic;

type state_typ is (iddle, wait0, wait1, 
                   wait90, wait91, wait92, wait93, wait94,
                   exec1, exec2, exec3, exec4, exec5, exec6, exec7, exec9,
                   abrt1, clr1, rst1, rst2, rst3);
signal state, nextate : state_typ;

signal typ_mif : std_logic; -- MIF type selector
signal typ_fifo: std_logic; -- fifo memory type selector

signal init0   : std_logic; -- DMAC desc. #0 go flag
signal init1   : std_logic; -- DMAC desc. #1 go flag
signal run0    : std_logic; -- DMAC desc. #0 running status
signal run1    : std_logic; -- DMAC desc. #1 running status
signal pp_bcast: std_logic; -- broad-cast go flag
signal dmadone : std_logic; -- descriptor done flag

signal dma_cnt : unsigned(23 downto 0); -- dest. counter
signal daen    : std_logic; -- dest. counter enable
signal ftaerr0 : std_logic; -- DMAC desc. #0 FT adrs. error status
signal ftaerr1 : std_logic; -- DMAC desc. #1 FT adrs. error status
signal ftderr0 : std_logic; -- DMAC desc. #0 FT data  error status
signal ftderr1 : std_logic; -- DMAC desc. #1 FT data  error status
signal adrerr0 : std_logic; -- DMAC desc. #0 source adrs. error status
signal adrerr1 : std_logic; -- DMAC desc. #1 source adrs. error status
signal daterr0 : std_logic; -- DMAC desc. #0 data error status
signal daterr1 : std_logic; -- DMAC desc. #1 data error status
signal ovlerr0 : std_logic; -- DMAC desc. #0 overlapping readout error status
signal ovlerr1 : std_logic; -- DMAC desc. #1 overlapping readout error status
signal timerr0 : std_logic; -- DMAC desc. #0 timeout error flag
signal timerr1 : std_logic; -- DMAC desc. #0 timeout error flag
signal err0_or : std_logic; -- DMAC desc. #0 error status
signal err1_or : std_logic; -- DMAC desc. #1 error status
signal dx_init1  : std_logic; 
signal dx_init2  : std_logic; 

signal dma0_cnt  : std_logic_vector(23 downto 0); -- count status
signal dma1_cnt  : std_logic_vector(23 downto 0); 

------------------------------------------------------------------------
begin
------------------------------------------------------------------------

-- Control clock to FT clock domain crossing
--------------------------------------------
fta: if EBFT_ASYNCH generate
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      cmd_go1  <= c_cmd_go_i;
      cmd_go2  <= cmd_go1;
   end if;
end process;
end generate fta;

fts: if NOT EBFT_ASYNCH generate -- does nothing
   cmd_go2   <= c_cmd_go_i;
end generate fts;

-- Resample anyway; P&R help; delay doesn't care
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      cmd_flush1  <= c_cmd_flush_i;
      cmd_flush2  <= cmd_flush1;
      cmd_abort1  <= c_cmd_abort_i;
      cmd_abort2  <= cmd_abort1;
      cmd_reset1  <= c_cmd_reset_i;
      cmd_reset2  <= cmd_reset1;
  end if;
end process;

process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      dx_init2 <= dx_init1; -- P&R
   end if;
end process;
dx_init_o <= dx_init1;
run0_o    <= run0;
run1_o    <= run1;

-- Packet pre-processor FSM
---------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if eb_ft_rst_i = '1' then
         run0    <= '0';       
         run1    <= '0';       
         ftaerr0 <= '0';       
         ftaerr1 <= '0';       
         ftderr0 <= '0';       
         ftderr1 <= '0';       
         adrerr0 <= '0';       
         adrerr1 <= '0';       
         daterr0 <= '0';       
         daterr1 <= '0';       
         err0_or <= '0';       
         err1_or <= '0';       
         ovlerr0 <= '0';       
         ovlerr1 <= '0';       
         timerr0 <= '0';       
         timerr1 <= '0';       
         typ_fifo<= '0';       
         typ_mif <= '0';       
         state   <= iddle;
      else
         run0    <= (init0 OR run0)    -- composite burst running  
                    AND NOT dmadone;   
         run1    <= (init1 OR run1)    -- composite burst running  
                    AND NOT dmadone;
         ftaerr0 <= ((ftaerr_i AND run0) OR ftaerr0)
                    AND NOT dx_init2;
         ftaerr1 <= ((ftaerr_i AND run1) OR ftaerr1)
                    AND NOT dx_init2;
         ftderr0 <= ((ftderr_i AND run0) OR ftderr0)
                    AND NOT dx_init2;
         ftderr1 <= ((ftderr_i AND run1) OR ftderr1)
                    AND NOT dx_init2;
         adrerr0 <= ((adrerr_i AND run0) OR adrerr0)
                    AND NOT dx_init2;
         adrerr1 <= ((adrerr_i AND run1) OR adrerr1)
                    AND NOT dx_init2;
         daterr0 <= ((daterr_i AND run0) OR daterr0)
                    AND NOT dx_init2;
         daterr1 <= ((daterr_i AND run1) OR daterr1)
                    AND NOT dx_init2;
         err0_or <= ((pp_err_i AND run0) OR err0_or)
                    AND NOT dx_init2;
         err1_or <= ((pp_err_i AND run1) OR err1_or)
                    AND NOT dx_init2;
         ovlerr0  <= ((ovlerr_i AND run0) OR ovlerr0)
                    AND NOT dx_init2;
         ovlerr1  <= ((ovlerr_i AND run1) OR ovlerr1)
                    AND NOT dx_init2;
         timerr0  <= ((timerr_i AND run0) OR timerr0)
                    AND NOT dx_init2;
         timerr1  <= ((timerr_i AND run1) OR timerr1)
                    AND NOT dx_init2;
         typ_fifo <= (((init0 AND dma0_fifo_i) OR (init1 AND dma1_fifo_i)) OR typ_fifo)  
                    AND NOT dmadone;   
         typ_mif  <= (((init0 AND dma0_mif_i) OR (init1 AND dma1_mif_i)) OR typ_mif)  
                    AND NOT dmadone;   
         state    <= nextate;            
      end if;
   end if;
end process;

process(state, fsc_run_i,
        dma0_go_i, dma1_go_i, dma0_link_i, dma1_link_i, dma0_abrt_i, dma0_flush_i, dma0_mif_i, dma1_abrt_i,
        cmd_go2, cmd_abort2, cmd_flush2, cmd_reset2, run0, run1,
        pp_err_i, pp_done_i, pp_zero_i)
begin
   nextate   <= state; -- stay in same state
   init0     <= '0';
   init1     <= '0';
   dx_init1  <= '0';
   dx_load_o <= '0';
   dmadone   <= '0';
   pp_init_o <= '0';
   pp_rst_o  <= '0';
   pp_clr_o  <= '0';
   pp_abrt_o <= '0';
   pp_bcast  <= '0';

   case state is 
      when iddle =>                 -- sleeping
         if     cmd_reset2 = '1'
            OR (dma0_abrt_i = '1' AND dma0_flush_i = '1') then 
            nextate <= rst1;
         elsif fsc_run_i = '0' then -- wait until fifo scan done
            if    cmd_flush2 = '1'
               OR dma0_flush_i = '1' then 
               nextate <= clr1;
            elsif dma0_go_i = '1'  
               OR cmd_go2 = '1' then
               init0   <= '1';
              nextate <= exec1;
            end if;
         end if;

      when wait0 =>                -- waiting for descriptor #0
         if dma0_abrt_i  = '1' OR
            cmd_abort2 = '1' then
            nextate <= abrt1;
         elsif dma0_go_i = '1' OR 
               cmd_go2 = '1' then
            init0   <= '1';
            nextate <= exec1;
         end if;

      when wait1 =>                -- waiting for descriptor #1
         if dma1_abrt_i = '1' then
            nextate <= abrt1;
         elsif dma1_go_i  = '1' then
            init1   <= '1';
            nextate <= exec1;
         end if;

      when exec1 =>                -- start new DMA descriptor 
         dx_init1 <= '1';
         nextate  <= exec2;

      when exec2 =>                -- packet parameter processing
         if pp_zero_i = '1' then
            nextate  <= exec9;     -- DMA desc. count is zero; terminate.
         else
            nextate  <= exec3;
         end if;

      when exec3 =>                -- packet parameter processing finishes
         pp_init_o <= '1';         -- can execute
         nextate <= exec4;

      when exec4 =>                 -- packet execution
         if    dma0_abrt_i  = '1' 
            OR dma1_abrt_i  = '1' 
            OR cmd_abort2 = '1' then
            nextate <= abrt1;
         
         elsif pp_err_i = '1' then  -- packet cancelled on error
            nextate <= wait90; 
         
         elsif pp_done_i = '1' then -- packet done OK
            dx_load_o <= '1';        
            nextate   <= exec5;   
         end if;

      when exec5 =>                 -- check for next packet  
         if pp_zero_i = '1' then
            nextate <= exec6; 
         else           
            nextate <= exec3; 
         end if;
        
      when exec6 =>                -- (if MIF) close channel 
         pp_bcast <= '1';          -- and broad-cast to FT
         nextate <= exec7;

      when exec7 =>                -- wait for broad-cast done  
         if pp_done_i = '1' then
            nextate <= exec9; 
         elsif cmd_reset2 = '1'
            OR (dma0_abrt_i = '1' AND dma0_flush_i = '1') then 
            nextate <= wait90;
         end if;
        
      when exec9 =>                -- check for next DMA descriptor  
         dmadone <= '1';           -- clear the GO bit (on next clock)  
         if    run0 = '1' AND
               dma0_link_i = '1'then
            nextate <= wait1;      -- switch to DMA descriptor #1    
         elsif run1 = '1' AND
               dma1_link_i = '1'then
            nextate <= wait0;      -- switch to DMA descriptor #0    
         else  
            nextate <= wait91;     -- all done     
         end if;

      when abrt1 =>                -- quit on abort request
         pp_abrt_o <= '1';
         if pp_done_i = '1' then
            nextate <= wait90; 
         end if;

      when clr1 =>                 -- MIF clear command
         if dma0_mif_i = '1' then 
            init0    <= '1';       -- select descriptor #0
            pp_clr_o <= '1';
            nextate  <= rst2;
         else
            nextate  <= rst3;      -- NOT MIF, so ignore it
         end if;

      when rst1 =>                 -- MIF reset command
         if dma0_mif_i = '1' then 
            init0    <= '1';       -- select descriptor #0
            pp_rst_o <= '1';
            nextate  <= rst2;
         else
            nextate  <= rst3;      -- NOT MIF, so ignore it
         end if;

      when rst2 =>                 -- wait until MIF reset done
         if pp_done_i <= '1' then
            nextate <= rst3;
         end if;

      when rst3 =>                 -- wait for soft request released
         if dma0_flush_i = '0' then
            nextate <= wait90; 
         end if;

      when wait90 =>               -- clear the GO bit 
         dmadone <= '1';
         nextate <= wait91;

      when wait91 =>               -- delay until GO bit cleared
         nextate <= wait92;

      when wait92 =>               -- delay until GO bit cleared
         nextate <= wait93;

      when wait93 =>               -- delay until GO bit (asynch.) cleared
         nextate <= wait94;

      when wait94 =>               -- delay until GO bit (asynch.) cleared
         nextate <= iddle;

   end case;

end process;

-- FSM controls
---------------
typ_fifo_o  <= typ_fifo;
typ_mif_o   <= typ_mif;
pp_flush_o  <= cmd_flush2 OR dma0_flush_i OR dma1_flush_i;
pp_bcast_o  <= pp_bcast;
dma_abrt_o  <= dma0_abrt_i OR dma1_abrt_i;

-- Status outputs and store broad-cast data
-------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      daen <= daen_i;                 -- P&R help
      if dx_init2 = '1' then
         dma_cnt  <= (others => '0'); -- zero transmitted for now
      elsif daen = '1' then
         dma_cnt <= dma_cnt + 1;      -- running
      end if;
   end if;
end process;

process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if run0 = '1' then
         dma0_cnt <= std_logic_vector(dma_cnt); 
      end if;
      if run1 = '1' then
         dma1_cnt <= std_logic_vector(dma_cnt); 
      end if;
   end if;
end process;

dma0_stat_o <= run0 & err0_or & ovlerr0 & daterr0 & adrerr0 & ftaerr0 & ftderr0 & timerr0 & dma0_cnt; 
dma1_stat_o <= run1 & err1_or & ovlerr1 & daterr1 & adrerr1 & ftaerr1 & ftderr1 & timerr1 & dma1_cnt;

end rtl;
