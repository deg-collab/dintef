--------------------------------------------------------------------------
--
-- E-bone Fast Transmitter to (2nd) E-bone master
-- 256 bit wrapper for ebm_ebft
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     2.2  12/01/14    herve  Record i/o
--                             Revisited for boundary crossing mngmt
--     3.0  11/05/15    herve  Added FIFO scan mode
--                             Added memory fifo virtualization
--     3.1  30/06/16    herve  Added FIFO scan triggers and timer
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.ebs_pkg.all;
use work.ebm_ebft_pkg.all;

entity ebm_ebft_256 is
generic (
   EBS_AD_RNGE  : natural := 12;   -- short adressing range
   EBS_AD_BASE  : natural := 1;    -- usual IO segment
   EBS_AD_SIZE  : natural := 32;   -- size 32 (no fifo scan) or 128
   EBS_AD_OFST  : natural := 0;    -- offset in segment
   EBX_MSG_MID  : natural := 1;    -- message master identifier
   EBFT_DBOUND  : natural := 4096; -- Dest. boundady crossing 2**N (0=none)
   EBFT_DA64    : boolean := true; -- Dest. Adrs. 32/64 bits
   EBFT_MEMS    : boolean := true; -- memory support (or not)
   EBFT_FCLK    : natural := 125;  -- Clock frequency in MHz (for timer)
   EBFT_FSCAN   : natural := 0;    -- FIFO scan desc. number max. 12
   EBFT_ASYNCH  : boolean := false;-- true when different clock domains
   EBFT_ABRQ    : boolean := false -- true for asynchronous arbitration
);
port (
-- E-bone slave interface
   eb_clk_i     : in  std_logic;  -- system control clock
   ebs_i        : in  ebs32_i_typ; 
   ebs_o        : out ebs32_o_typ; 

-- External control ports @eb_clk
   cmd_go_i     : in  std_logic;  -- go command
   cmd_flush_i  : in  std_logic;  -- flush command
   cmd_abort_i  : in  std_logic;  -- abort command
   cmd_reset_i  : in  std_logic;  -- MIF reset command

-- External status ports @eb_clk
   d0_stat_o    : out std32;      -- Descriptor #0 status register
   d1_stat_o    : out std32;      -- Descriptor #1 status register
   fsc_irqs_o   : out std32;      -- fifo scan IRQ status register
   fsc_stat_o   : out std32;      -- fifo scan status register
   dma_stat_o   : out std32;      -- Global status register

-- E-bone Fast Transmitter
   ft_psize_i   : in  std4;       -- FT encoded payload size
   eb_ft_clk_i  : in  std_logic;  -- FT and 2nd E-bone clock
   eb_ft_brq_o  : out std_logic;  -- FT master bus request
   eb_ft_bg_i   : in  std_logic;  -- FT master bus grant
   eb_ft_dxt_o  : out std256;     -- FT data 
   eb_ft_o      : out ebft_typ;   -- FT controls 
   eb_ft_dk_i   : in  std_logic;  -- FT data acknowledge
   eb_ft_err_i  : in  std_logic;  -- FT bus error

-- (2nd) E-bone master interface
   eb2_ft_bg_i  : in std_logic;   -- busy FT
   eb2_mx_brq_o : out std_logic;  -- bus request
   eb2_mx_bg_i  : in  std_logic;  -- bus grant
   eb2_bmx_i    : in  std_logic;  -- busy some master (but FT)
   eb2_mx_o     : out ebm256_typ; -- controls
   eb2_m_i      : in  ebs256_o_typ;-- master shared

-- (2nd) E-bone extension master 
   ebx2_msg_set_o : out std8;     -- message management
   ebx2_msg_dat_i : in  std16;    -- message data
   ebx2_dsz_i     : in  std4;     -- device data size

-- External triggers
   fs_trig_i    : in  std_logic_vector(11 downto 0);
   fs_tack_o    : out std_logic_vector(11 downto 0);

-- External status ports @eb_ft_clk
   dma_count_o  : out std16;      -- FIFO requested word count
   dma_eot_o    : out std_logic;  -- end of transfer
   dma_err_o    : out std_logic   -- transfer aborted on error
);
end ebm_ebft_256;

------------------------------------------------------------------------
architecture rtl of ebm_ebft_256 is
------------------------------------------------------------------------
constant   EBFT_DWIDTH  : natural := 256;   -- FT data width

------------------------------------------------------------------------
begin
------------------------------------------------------------------------
ebmft: ebm_ebft
generic map(
   EBS_AD_RNGE  => EBS_AD_RNGE,
   EBS_AD_BASE  => EBS_AD_BASE, 
   EBS_AD_SIZE  => EBS_AD_SIZE, 
   EBS_AD_OFST  => EBS_AD_OFST, 
   EBX_MSG_MID  => EBX_MSG_MID,
   EBFT_DWIDTH  => EBFT_DWIDTH,
   EBFT_DBOUND  => EBFT_DBOUND,
   EBFT_DA64    => EBFT_DA64,  
   EBFT_MEMS    => EBFT_MEMS,
   EBFT_FCLK    => EBFT_FCLK,
   EBFT_FSCAN   => EBFT_FSCAN,
   EBFT_ASYNCH  => EBFT_ASYNCH,
   EBFT_ABRQ    => EBFT_ABRQ
)
port  map(
-- E-bone slave interface
   eb_clk_i     => eb_clk_i,  -- system control clock
   ebs_i        => ebs_i, 
   ebs_o        => ebs_o, 

-- External control ports @eb_clk
   cmd_go_i     => cmd_go_i,
   cmd_flush_i  => cmd_flush_i,
   cmd_abort_i  => cmd_abort_i,
   cmd_reset_i  => cmd_reset_i,

-- External status ports @eb_clk
   d0_stat_o    => d0_stat_o,
   d1_stat_o    => d1_stat_o,
   fsc_irqs_o   => fsc_irqs_o,
   fsc_stat_o   => fsc_stat_o,
   dma_stat_o   => dma_stat_o,

-- E-bone Fast Transmitter
   ft_psize_i   => ft_psize_i, 
   eb_ft_clk_i  => eb_ft_clk_i, -- FT and 2nd E-bone clock
   eb_ft_brq_o  => eb_ft_brq_o,
   eb_ft_bg_i   => eb_ft_bg_i,
   eb_ft_dxt_o  => eb_ft_dxt_o, 
   eb_ft_o      => eb_ft_o,
   eb_ft_dk_i   => eb_ft_dk_i,
   eb_ft_err_i  => eb_ft_err_i,

-- (2nd) E-bone master interface
   eb2_mx_brq_o => eb2_mx_brq_o,
   eb2_mx_bg_i  => eb2_mx_bg_i,
   eb2_mx_as_o  => eb2_mx_o.eb_as,
   eb2_mx_eof_o => eb2_mx_o.eb_eof, 
   eb2_mx_aef_o => eb2_mx_o.eb_aef,
   eb2_mx_dat_o => eb2_mx_o.eb_dat, 

-- (2nd) E-bone master shared bus
   eb2_dk_i     => eb2_m_i.eb_dk,
   eb2_err_i    => eb2_m_i.eb_err,
   eb2_dat_i    => eb2_m_i.eb_dat,
   eb2_bmx_i    => eb2_bmx_i,
   eb2_bft_i    => eb2_ft_bg_i,

-- (2nd) E-bone extension master 
   ebx2_msg_set_o => ebx2_msg_set_o,
   ebx2_msg_dat_i => ebx2_msg_dat_i,  
   ebx2_dsz_i     => ebx2_dsz_i, 

-- External triggers
   fs_trig_i    => fs_trig_i,
   fs_tack_o    => fs_tack_o, 

-- External status ports @eb_ft_clk
   dma_count_o  => dma_count_o, 
   dma_eot_o    => dma_eot_o, 
   dma_err_o    => dma_err_o
);

end rtl;
