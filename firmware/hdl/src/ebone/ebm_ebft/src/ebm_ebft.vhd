--------------------------------------------------------------------------
--
-- E-bone Fast Transmitter to (2nd) E-bone master
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     0.1  18/10/10    herve  Preliminary release
--     0.2  09/02/11    herve  fifo bug corrected
--     1.0  11/04/11    herve  Updated to E-bone rev. 1.2
--     1.1  19/07/11    herve  FIFO(MIF) retry bug fixed
--     1.2  26/06/12    herve  Added status ports and FSM optimized
--     1.3  24/07/12    herve  Added 128 and 256 bits support
--                             Changed GCSR FT data width reporting
--     1.4  04/10/12    herve  FT master bus request bug fixed
--                             Exact count on small burst (BRAM only)
--     1.5  14/11/12    herve  Exact count on small burst (all cases)
--     1.6  21/12/12    herve  Added messages, E-bone V1.3 preliminary
--                             Added reset MIF command
--     1.7  11/02/13    herve  unaligned, down-sized data,
--                             error reporting, E-bone V1.3 revisited
--     2.0  30/05/13    herve  Split in two clock domains
--                             control clock - Fast Transmitter clock
--                             Accurate burst size (in bytes) output
--     2.1  24/10/13    herve  Byte unaligned management
--                             Source now byte addressing
--                             New regs. arrangement (NOT upwards compatible)
--                             Added ft_psize_i input port
--                             New FT signalling
--     2.2  03/02/14    herve  Record i/o, pipelined inputs
--                             Revisited for boundary crossing mngmt
--                             MIF FIFO mngmt moved to DMAC
--                             Added software MIF reset
--     3.0  11/05/15    herve  Added FIFO scan mode
--                             Added memory fifo virtualization
--                             Bug MIF/FIFO max size fixed
--                             Bug some offset wrong decoding fixed
--                             Bug retry fixed
--     3.1  30/06/16    herve  Added FIFO scan related
--                              - external trigger
--                              - internal timer
--                             
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- E-bone Master Fast Transmitter (1st E-bone)
-- Master #0 on (2nd) E-bone 
--
-- Both E-bone interconnects share the same clock
-- Fast Transmitter and 2nd E-bone same data width, 64 bit fixed.
-- Broad-cast at end of transfer (empty message)
--
-- E-bone slave supported are of 3 types:
-- BRAM
-- FIFO
-- MIF interface (for external memory with MIG interfacing)
--
-- DMAC operates in two modes: Block mover or FIFO scan.
--
-- + Memory block mover
--
-- DMAC manages to move the total size requested (count#1)
-- in a number of (smaller) FT payload blocks (count#2).
--
-- FIFO readout 
-- DMAC waits for enough data in FIFO.
-- Then it moves this FIFO block size (count#3)
-- in a number of (smaller) FT payload blocks (count#2).
-- Repeat reading the FIFO 
-- until the the total size requested is exhausted (count#1).
--
-- + FIFO scan
--
-- DMAC scans a number of FIFOs, checking for their occupancy.
-- When at least payload size data is available, the FIFO is read out.
-- DMAC endlessly loops.
--
-- MIF readout
-- DMAC place a request by writing @FIFO offset
-- Then managed like FIFO
--
-- MIF request format is as follows
-- offset data
-- 0      MIF address
-- 1      MIF count (fifo threshold / requested count)
-- 2      reserved
-- 3      MIF command
--        0000 = Close message channel
--        1111 = Reset
--
-- It is recommended to set EBS_TMO_DK(PCIe/FT) > EBS_TMO_DK(2nd E-bone)
--------------------------------------------------------------------------
-- Source (byte) address is limited to 30 bits
--------------------------------------------------------------------------
-- E-bone slave for control
-- Register map

-- Descriptors
--------------
-- Ofst  Usage
--    0  D0_SRCE; desc. 0; E-bone source
 
--       Plain memory
--        27-0  E-bone offset
--       29-28  E-bone segment
--       31-30  "00"

--       FIFO (or MIF) memory
--        15-0  E-bone offset
--       27-16  FIFO readout count (max 4096)
--       31-28  Reserved

--    1  D0_SADRS; desc. 0; MIF byte address / BRAM byte displacement

--    2  desc. 0; reserved

--    3  D0_DLSW; desc. 0; Dest. address LSW
--    4  D0_DMSW; desc. 0; Dest. address MSW

--    5  D0_CTRL; desc. 0; Control
--        23-0  count (bytes)
--       25-24  reserved
--          26  i/f type 0=ebs_bram, 1=MIF
--          27  memory type 0=plain memory, 1=FIFO
--          28  command flush
--          29  command abort
--          30  descriptor linked
--          31  descriptor valid (self clear when done)

--    6  desc. 1; E-bone source 
--    7  desc. 1; Source address 
--    8  desc. 1; reserved
--    9  desc. 1; Dest. address LSW
--   10  desc. 1; Dest. address MSW
--   11  desc. 1; Control

-- Config. & status
-------------------
-- Ofst  Usage
--   12  FS_CTRL; fifo scan control
--       03-00  Actual number of fifo descriptors
--       15-04  Timer theshold (ms unit)
--       27-16  Reserved
--          28  1=reset dest. buffer pointers and pending IRQs on restart
--          29  Force flush all
--          30  Timer enable
--          31  fifo scan go (1=run, 0=pause); self clears on error.

--   13  GSR; Global status register, read only
--       07-00   Version (two nibbles)
--       15-08   Reserved 
--       19-16   max. nb. of fifo scan descriptors
--       23-20   Encoded payload size
--               0000=128, 0001=256, 0010=512, 0011=1024, ...
--        25-24  "00"
--           26  MIF fifo ready
--           27  MIF fifo empty
--        31-28  FT size

--   14  D0_STAT; Desc. 0; Status, read only
--       23-00  count actually moved
--       24     Time-out error status
--       25     FT data  phase error status
--       26     FT adrs. phase error status
--       27     source adrs. phase error status
--       28     source data phase error status
--       29     underrun (overlapping readout) error status
--       30     error status
--       31     running status

--   15  D1_STAT; Desc. 1; Status, read only
--
-- FIFO scan registers
----------------------
-- Ofst  Usage
--   16 to 16+Nfifo  
--       FSX_CSR; fifo scan dedicated control and status
--       19-00  R    Dest. buffer occupancy (words)
--          30  R/W  DMAC set   = IRQ#0 (1/2 full) pending 
--                   Host clear = IRQ#0 acknowledged
--          31  R/W  DMAC set   = IRQ#1 (full) pending
--                   Host clear = IRQ#1 acknowledged
--
--   30  FS_STAT; Global status
--       03-00  current active fifo descriptor
--       07-04  max. nb. of descriptors
--          08  scan loop toggle indicator
--       23-09  0
--          24  Time-out error status
--          25  FT data  phase error status
--          26  FT adrs. phase error status
--          27  source adrs. phase error status
--          28  source data phase error status
--          29  0
--          30  error status
--          31  scan running status
--
--   31  FS_IRQ; Global IRQ status
--       23-00  fif#n-irq#1 fif#n-irq#0, ... , fif#0-irq#1 fif#0-irq#0
--          31  timer IRQ (write to clear)
-- 
-- FIFO scan descriptors
------------------------
-- Ofst  
-- 64 to EBS_AD_SIZE-1 
--
-- Descriptor format
-- +ofst 
--    0    Source
--       15-00  E-bone offset
--       31-16  Reserved
--
--    1    Dest. address LSW
--    2    Dest. address MSW
--
--    3    Control
--       19-00  Dest. buffer size (words, multiple of payld)
--       25-20  Reserved
--          24  External trigger enable
--          25  Reserved
--          26  1 = BRAM (NOT MIF) type
--          27  1 = virtual fifo (BRAM or MIF)
--          28  Reserved
--          29  1 = fifo reset
--          30  1 = half full IRQ mode
--          31  1 = valid, 0 = skip
--
-- Notes: 
-- If MIF bit is set, the FIFO bit must be set as well
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;
use work.ebm_ebft_pkg.all;

entity ebm_ebft is
generic (
   EBS_AD_RNGE  : natural := 12;   -- short adressing range
   EBS_AD_BASE  : natural := 1;    -- usual IO segment
   EBS_AD_SIZE  : natural := 32;   -- size 32 (no fifo scan) or 128
   EBS_AD_OFST  : natural := 0;    -- offset in segment
   EBX_MSG_MID  : natural := 1;    -- message master identifier
   EBFT_DWIDTH  : natural := 64;   -- FT data width
   EBFT_DBOUND  : natural := 4096; -- Dest. boundady crossing 2**N (0=none)
   EBFT_DA64    : boolean := true; -- Dest. Adrs. 32/64 bits
   EBFT_MEMS    : boolean := true; -- memory support (or not)
   EBFT_FCLK    : natural := 125;  -- Clock frequency in MHz (for timer)
   EBFT_FSCAN   : natural := 0;    -- FIFO scan desc. number max. 12
   EBFT_ASYNCH  : boolean := false;-- true when different clock domains
   EBFT_ABRQ    : boolean := false -- true for asynchronous arbitration
);
port (
-- E-bone slave interface
   eb_clk_i     : in  std_logic;  -- system control clock
   ebs_i        : in  ebs32_i_typ; 
   ebs_o        : out ebs32_o_typ; 

-- External control ports @eb_clk
   cmd_go_i     : in  std_logic;  -- go command
   cmd_flush_i  : in  std_logic;  -- flush command
   cmd_abort_i  : in  std_logic;  -- abort command
   cmd_reset_i  : in  std_logic;  -- MIF reset command

-- External status ports @eb_clk
   d0_stat_o    : out std32;      -- Descriptor #0 status register
   d1_stat_o    : out std32;      -- Descriptor #1 status register
   fsc_irqs_o   : out std32;      -- fifo scan IRQ status register
   fsc_stat_o   : out std32;      -- fifo scan status register
   dma_stat_o   : out std32;      -- Global status register

-- E-bone Fast Transmitter
   ft_psize_i   : in  std4;       -- FT encoded payload size
   eb_ft_clk_i  : in  std_logic;  -- FT and 2nd E-bone clock
   eb_ft_brq_o  : out std_logic;  -- FT master bus request
   eb_ft_bg_i   : in  std_logic;  -- FT master bus grant
   eb_ft_dxt_o  : out std_logic_vector(EBFT_DWIDTH-1 downto 0); -- FT data 
   eb_ft_o      : out ebft_typ;   -- FT controls 
   eb_ft_dk_i   : in  std_logic;  -- FT data acknowledge
   eb_ft_err_i  : in  std_logic;  -- FT bus error

-- (2nd) E-bone master interface
   eb2_mx_brq_o : out std_logic;  -- bus request
   eb2_mx_bg_i  : in  std_logic;  -- bus grant
   eb2_mx_as_o  : out std_logic;  -- adrs strobe
   eb2_mx_eof_o : out std_logic;  -- end of frame
   eb2_mx_aef_o : out std_logic;  -- almost end of frame
   eb2_mx_dat_o : out std_logic_vector(EBFT_DWIDTH-1 downto 0); -- master data write

-- (2nd) E-bone master shared bus
   eb2_dk_i     : in std_logic;   -- data acknowledge
   eb2_err_i    : in std_logic;   -- bus error
   eb2_dat_i    : in std_logic_vector(EBFT_DWIDTH-1 downto 0); -- master data in
   eb2_bmx_i    : in std_logic;   -- busy some master (but FT)
   eb2_bft_i    : in std_logic;   -- busy FT

-- (2nd) E-bone extension master 
   ebx2_msg_set_o : out std8;     -- message management
   ebx2_msg_dat_i : in  std16;    -- message data
   ebx2_dsz_i     : in  std4;     -- device data size

-- External triggers
   fs_trig_i    : in  std_logic_vector(11 downto 0);
   fs_tack_o    : out std_logic_vector(11 downto 0);

-- External status ports @eb_ft_clk
   dma_count_o  : out std16;      -- MIF FIFO requested word count
   dma_eot_o    : out std_logic;  -- end of transfer
   dma_err_o    : out std_logic   -- transfer aborted on error
);
end ebm_ebft;

------------------------------------------------------------------------
architecture rtl of ebm_ebft is
------------------------------------------------------------------------
--
-- Control and Status registers
--
component ebftm_regs is
generic (
   EBS_AD_RNGE  : natural := 12;  -- short adressing range
   EBS_AD_BASE  : natural := 1;   -- usual IO segment
   EBS_AD_SIZE  : natural := 32;  -- size 32 (no fifo scan) or 128
   EBS_AD_OFST  : natural := 0;   -- offset in segment
   EBFT_MEMS    : boolean := true;-- memory support (or not)
   EBFT_FSCAN   : natural := 0    -- nb. of fifos
);
port (
-- E-bone slave interface
   eb_clk_i     : in  std_logic;  -- system clock
   ebs_i        : in  ebs32_i_typ; 
   ebs_o        : out ebs32_o_typ; 

-- DMAC registers and control
   dma_regs_o   : out std32_a;    -- register outputs
   dma0_stat_i  : in  std32;      -- DMA #0 status 
   dma1_stat_i  : in  std32;      -- DMA #1 status
   dma_stat_i   : in  std32;      -- DMA status

-- FSCAN registers and control
   fsc_itack_i  : in  std_logic;  -- internal time out acknowledge
   fsc_cmd_o    : out std32;      -- fifo scan command register
   fsc_irqofs_o : out std4;       -- IACK register offset
   fsc_irqwr0_o : out std_logic;  -- IACK#0 write
   fsc_irqwr1_o : out std_logic;  -- IACK#1 write
   fsc_mwr_o    : out std_logic;  -- mem. desc. write enable
   fsc_madrs_o  : out std8;       -- mem. desc. address
   fsc_mdati_o  : out std32;      -- mem. desc. data in
   fsc_mdato_i  : in  std32;      -- mem. desc. data out
   fsc_sreg_i   : in  std32_a(EBFT_FSCAN-1 downto 0);
   fsc_irqs_i   : in  std32;      -- IRQ status
   -- @ft_clk
   fsc_stat_i   : in  std32;      -- fifo scan status
   fsc_abrt_i   : in  std_logic   -- some error 
);
end component;

--
-- Packet preprocessing state machine
--
component ebftm_mem_fsm is
generic (
   EBFT_ASYNCH  : boolean := false -- true when different clock domains
);
port (
-- External control ports @eb_clk_i
   c_cmd_go_i     : in  std_logic; -- go command
   c_cmd_flush_i  : in  std_logic; -- flush command
   c_cmd_abort_i  : in  std_logic; -- abort command
   c_cmd_reset_i  : in  std_logic; -- MIF reset command

-- DMAC registers control bits
   eb_ft_clk_i  : in  std_logic;  -- FT system clock
   eb_ft_rst_i  : in  std_logic;  -- synchronous system reset
   dma0_go_i    : in  std_logic;
   dma0_link_i  : in  std_logic;
   dma0_abrt_i  : in  std_logic;
   dma0_flush_i : in  std_logic;
   dma0_fifo_i  : in  std_logic;
   dma0_mif_i   : in  std_logic;
   dma1_go_i    : in  std_logic;
   dma1_link_i  : in  std_logic;
   dma1_abrt_i  : in  std_logic;
   dma1_flush_i : in  std_logic;
   dma1_fifo_i  : in  std_logic;
   dma1_mif_i   : in  std_logic;

-- Fifo scan i/f
   fsc_run_i    : in  std_logic; 

-- Descriptor pre-processing control
   dx_init_o    : out std_logic; -- packet descriptor initialization
   dx_load_o    : out std_logic; -- packet descriptor count re-load
   run0_o       : out std_logic; -- descriptor #0 running
   run1_o       : out std_logic; -- descriptor #1 running
   pp_zero_i    : in  std_logic; -- zero byte count from DMA descriptor

-- FT FSM i/f
   typ_mif_o    : out std_logic;  -- MIF type selector
   typ_fifo_o   : out std_logic;  -- fifo memory type selector
   dma_abrt_o   : out std_logic;  -- soft abort request

   pp_init_o    : out std_logic;  -- new (pseudo) descriptor start
   pp_bcast_o   : out std_logic;  -- do broad-cast
   pp_abrt_o    : out std_logic;  -- abort request
   pp_flush_o   : out std_logic;  -- flush request
   pp_clr_o     : out std_logic;  -- clear request
   pp_rst_o     : out std_logic;  -- reset request
   pp_done_i    : in  std_logic;  -- (pseudo) descriptor execution done
   pp_err_i     : in  std_logic;  -- error abort flag

   daen_i       : in  std_logic;  -- dest. counter enable
   ftaerr_i     : in  std_logic;  -- FT adrs. error flag
   ftderr_i     : in  std_logic;  -- FT data  error flag
   adrerr_i     : in  std_logic;  -- source adrs. error flag
   daterr_i     : in  std_logic;  -- data error flag
   ovlerr_i     : in  std_logic;  -- overlapping readout error flag
   timerr_i     : in  std_logic;  -- timeout error flag

-- Status 
   dma0_stat_o  : out std32;      -- descriptor #0 status 
   dma1_stat_o  : out std32       -- descriptor #1 status 
);
end component;

--
-- Packet preprocessing 
-- for 4K boundary crossing management
--
component ebftm_mem_pp is
generic (
   EBFT_DWIDTH  : natural := 64;   -- FT data width 
   EBFT_DBOUND  : natural := 4096; -- Dest. boundady crossing 2**N (0=none)
   EBFT_ASYNCH  : boolean := false -- true when different clock domains
);
port (
-- DMAC registers @eb_clk_i
   c_dma_regs_i : in  std32_a(11 downto 0);

-- DMAC registers control bits
   eb_ft_clk_i  : in  std_logic; -- FT system clock
   dma0_go_o    : out std_logic;
   dma0_link_o  : out std_logic;
   dma0_abrt_o  : out std_logic;
   dma0_flush_o : out std_logic;
   dma0_fifo_o  : out std_logic;
   dma0_mif_o   : out std_logic;
   dma1_go_o    : out std_logic;
   dma1_link_o  : out std_logic;
   dma1_abrt_o  : out std_logic;
   dma1_flush_o : out std_logic;
   dma1_fifo_o  : out std_logic;
   dma1_mif_o   : out std_logic;

-- Descriptor pre-processing control
   dx_init_i    : in  std_logic; -- packet descriptor initialization
   dx_load_i    : in  std_logic; -- packet descriptor count re-load
   run0_i       : in  std_logic; -- descriptor #0 running
   run1_i       : in  std_logic; -- descriptor #1 running
   pp_zero_o    : out std_logic; -- zero byte count from DMA descriptor

-- Address incrementer i/f
   dma_damsb_o  : out std32;     -- DMA desc. dest. address MSbits

-- FT FSM i/f
   dx_srce_o    : out std32;     -- source location
   dx_sadrs_o   : out std32;     -- source address
   dx_dalsb_o   : out std32;     -- dest. address LSbits
   dx_bcnt_o    : out std_logic_vector(23 downto 0) -- byte count
);
end component;

--
-- 64 bit destination address incrementer 
--
component ebftm_ainc is
generic (
   EBFT_DA64    : boolean := true -- Dest. Adrs. 32/64 bits
);
port (
   eb_ft_clk_i  : in  std_logic;  -- FT system clock
   dma_damsb_i  : in  std32;      -- DMA desc. dest. address MSbits
   dx_init_i    : in  std_logic;  -- DMA desc. initialization
   fsc_damsb_i  : in  std32;      -- FIFO desc. dest. address MSbits
   fs_ald_i     : in  std_logic;  -- FIFO desc. address reload
   daburst_i    : in  std32;      -- dest. address current burst
   dupdate_i    : in  std_logic;  -- dest. address update valid
   dx_damsb_o   : out std32       -- packet dest. address MSbits
);
end component;

--
-- Broad-cast data management 
--
component ebftm_bcast is
port (
   eb_ft_clk_i  : in  std_logic; -- FT system clock
   pp_err_i     : in  std_logic; -- error abort flag
   run0_i       : in  std_logic; -- descriptor #0 running
   run1_i       : in  std_logic; -- descriptor #1 running
   pp_bcast_i   : in  std_logic; -- do broad-cast
   dma0_stat_i  : in  std32;     -- descriptor #0 status 
   dma1_stat_i  : in  std32;     -- descriptor #1 status 

   fsc_run_i    : in  std_logic; -- fifo scan running
   fpp_bcast_i  : in  std_logic; -- do broad-cast
   fsc_stat_i   : in  std32;     -- fifo scan status for broad-casting

   irq_stat_o   : out std32      -- descriptor status for broad-casting
);
end component;

--
-- Fast Transmitter FSM
--
component ebftm_fsm is
generic (
   EBFT_DWIDTH  : natural := 64;  -- FT data width
   EBFT_FSCAN   : natural := 0;   -- max. nb. of fifo scan
   EBS_VERSION  : std8            -- IP version
);
port (
-- E-bone Fast Transmitter
   eb_ft_clk_i  : in  std_logic;  -- system clock
   eb_ft_rst_i  : in  std_logic;  -- synchronous system reset

   eb_bmx_i     : in  std_logic;  -- busy some master (but FT)
   eb_bft_i     : in  std_logic;  -- FT master bus grant
   eb_ft_brq_o  : out std_logic;  -- FT master bus request
   eb_ft_dxt_o  : out std_logic_vector(EBFT_DWIDTH-1 downto 0);
   eb_ft_as_o   : out std_logic;  -- FT adrs strobe
   eb_ft_ss_o   : out std_logic;  -- FT size strobe
   eb_ft_eof_o  : out std_logic;  -- FT end of frame
   eb_ft_dsz_o  : out std8;       -- FT data size qualifier
   eb_ft_dk_i   : in  std_logic;  -- master data acknowledge
   eb_ft_err_i  : in  std_logic;  -- master bus error
   ft_psize_i   : in  std4;       -- FT encoded payload size
   ebrq_o       : out std_logic;  -- E-bone request

-- Fscan dedicated packet processor and FSM i/f 
   typ_fscan_i  : in  std_logic;  -- fifo scan
   typ_fvmem_i  : in  std_logic;  -- fifo virtual memory
   fpp_init_i   : in  std_logic;  -- new (pseudo) descriptor start
   fpp_open_i   : in  std_logic;  -- message open command
   fpp_bcast_i  : in  std_logic;  -- do broad-cast
   fpp_rst_i    : in  std_logic;  -- fifo reset request
   fpp_done_o   : out std_logic;  -- fifo scan descriptor execution done
   fpp_skip_o   : out std_logic;  -- fifo scan descriptor skipped
   fs_open_o    : out std_logic;  -- get occupancy for flushing out

-- Other packet processor i/f 
   dx_damsb_i   : in  std32;      -- dest. address MSbits
   dma_abrt_i   : in  std_logic;  -- soft abort request
   typ_mif_i    : in  std_logic;  -- MIF type selector
   typ_fifo_i   : in  std_logic;  -- FIFO memory type selector

   pp_init_i    : in  std_logic;  -- new (pseudo) descriptor start
   pp_bcast_i   : in  std_logic;  -- do broad-cast
   pp_abrt_i    : in  std_logic;  -- abort request
   pp_flush_i   : in  std_logic;  -- flush request
   pp_clr_i     : in  std_logic;  -- clear request
   pp_rst_i     : in  std_logic;  -- reset request
   pp_done_o    : out std_logic;  -- (pseudo) descriptor execution done
   pp_err_o     : out std_logic;  -- error abort flag

   ftaerr_o     : out std_logic;  -- FT adrs. error flag
   ftderr_o     : out std_logic;  -- FT data  error flag
   adrerr_o     : out std_logic;  -- source adrs. error flag
   daterr_o     : out std_logic;  -- data error flag
   ovlerr_o     : out std_logic;  -- overlapping readout error flag
   timerr_o     : out std_logic;  -- timeout readout error flag
   irq_stat_i   : in  std32;      -- descriptor status for broad-casting

   dma_stat_o   : out std32;      -- DMA status
   dma_eot_o    : out std_logic;  -- end of transfer
   dma_err_o    : out std_logic;  -- transfer aborted on error

-- Burst management i/f
   mx_brq_o     : out std_logic;  -- burst request command
   mx_bwr_o     : out std_logic;  -- burst write command
   mx_clr_o     : out std_logic;  -- clear message command
   mx_open_o    : out std_logic;  -- open message command
   mx_rst_o     : out std_logic;  -- MIF reset command
   mx_read_o    : out std_logic;  -- burst read data command
   mx_aend_o    : out std_logic;  -- burst almost end command
   mx_end_o     : out std_logic;  -- burst end command
   mx_abrt_o    : out std_logic;  -- burst abort command
   mx_bg_i      : in  std_logic;  -- burst request granted
   mx_ss_i      : in  std_logic;  -- data size strobe
   mx_ardy_i    : in  std_logic;  -- burst data almost ready
   mx_rdy_i     : in  std_logic;  -- burst data ready
   mx_err_i     : in  std_logic;  -- burst error
   mx_dout_i    : in  std_logic_vector(EBFT_DWIDTH-1 downto 0);

-- MIF fifo management
   fif_rq_o     : out std_logic;  -- store requested occupancy
   fif_rdy_i    : in  std_logic;  -- FIFO occupancy is OK to transmit
   fif_emty_i   : in  std_logic;  -- FIFO empty

-- FSM related counters
   firstword_o  : out std_logic;  -- flag 1st word (in 1st elementary burst)
   dsize_o      : out std4;       -- early size for count processing (CHCH)
   wsize_o      : out std4;       -- slave size memorized for burst
   b1st_i       : in  std8;       -- 1st word MSbytes count (0=all) 

   bcld_o       : out std_logic;  -- ebone burst counter load
   bcen_o       : out std_logic;  -- ebone burst counter enable
   bcazero_i    : in  std_logic;  -- burst counter almost zero flag
   bczero_i     : in  std_logic;  -- burst counter zero flag

   fcld_o       : out std_logic;  -- fifo counter load
   fcen_o       : out std_logic;  -- fifo counter enable
   fczero_i     : in  std_logic;  -- fifo counter zero flag

   mald_o       : out std_logic;  -- mem. address counter load
   maen_o       : out std_logic;  -- mem. address counter enable
   daclr_o      : out std_logic;  -- dest. counter clear
   daen_o       : out std_logic;  -- dest. counter enable
   daendall_i   : in  std_logic;  -- dest. end flag
   daendm1_i    : in  std_logic;  -- dest. end flag minus 1
   daendm2_i    : in  std_logic;  -- dest. end flag minus 2
   daendm3_i    : in  std_logic;  -- dest. end flag minus 3
   daburst_i    : in  std32       -- dest. address current burst
);
end component;

--
-- Fast Transmitter counters management
--
component ebftm_ft is
generic (
   EBFT_DWIDTH  : natural := 64   -- FT data width 
);
port (
-- E-bone Fast Transmitter
   eb_ft_clk_i  : in  std_logic;  -- system clock
   eb_ft_eof_i  : in  std_logic;  -- FT end of frame
   ft_psize_i   : in  std4;       -- FT encoded payload size
   ebrq_i       : in  std_logic;  -- E-bone request
   eb_ft_blen_o : out std16;      -- FT burst length in bytes

-- Burst management i/f
   mif_adrs_o   : out std32;      -- MIF starting address
   mif_count_o  : out std16;      -- MIF word count
   mx_adrs_o    : out std32;      -- Source E-bone starting address

-- Fscan packet processor i/f 
   typ_fscan_i  : in  std_logic;  -- fifo scan type selector
   fs_open2_i   : in  std_logic;  -- FIFO occupancy is valid
   fs_b256_i    : in  std_logic;  -- force MIF count to 256 words
   fs_srce_i    : in  std32;      -- source location
   fs_sadrs_i   : in  std32;      -- source address (VM)
   fs_dalsb_i   : in  std32;      -- dest. address LSbits
   fs_bcnt_i    : in  std_logic_vector(23 downto 0); -- byte count

-- Mem. acket processor i/f 
   typ_mif_i    : in  std_logic;  -- MIF type selector
   typ_fifo_i   : in  std_logic;  -- FIFO memory type selector
   dx_srce_i    : in  std32;      -- source location
   dx_sadrs_i   : in  std32;      -- source address
   dx_dalsb_i   : in  std32;      -- dest. address LSbits
   dx_bcnt_i    : in  std_logic_vector(23 downto 0); -- byte count

-- MIF fifo management
   remcnt_o     : out unsigned(23 downto 0); -- remaining count in words
   ft_wsize_o   : out unsigned(7 downto 0);  -- FT payload size in words

-- FSM i/f
   firstword_i  : in  std_logic;  -- flag 1st word (in 1st elementary burst)
   dsize_i      : in  std4;       -- early size for data (CHCH)
   wsize_i      : in  std4;       -- slave size memorized for burst
   b1st_o       : out std8;       -- 1st word MSbytes count (0=all) 

   bcld_i       : in  std_logic;  -- ebone burst counter load
   bcen_i       : in  std_logic;  -- ebone burst counter enable
   bcazero_o    : out std_logic;  -- burst counter almost zero flag
   bczero_o     : out std_logic;  -- burst counter zero flag

   fcld_i       : in  std_logic;  -- fifo counter load
   fcen_i       : in  std_logic;  -- fifo counter enable
   fczero_o     : out std_logic;  -- fifo counter zero flag

   mald_i       : in  std_logic;  -- mem. address counter load
   maen_i       : in  std_logic;  -- mem. address counter enable
   daclr_i      : in  std_logic;  -- dest. counter clear
   daen_i       : in  std_logic;  -- dest. counter enable
   daendall_o   : out std_logic;  -- dest. end flag
   daendm1_o    : out std_logic;  -- dest. end flag minus 1
   daendm2_o    : out std_logic;  -- dest. end flag minus 2
   daendm3_o    : out std_logic;  -- dest. end flag minus 3
   dupdate_o    : out std_logic;  -- dest. address update valid
   daburst_o    : out std32       -- dest. address current burst
);
end component;

--
-- MIF fifo occupancy management
--
component ebftm_mif is
port (
   eb_ft_clk_i    : in  std_logic;  -- system clock
   ebx2_msg_dat_i : in  std16;      -- message data 
   ft_wsize_i     : in  unsigned(7 downto 0);  -- FT payload size in words
   remcnt_i       : in  unsigned(23 downto 0); -- remaining count in words
   fs_open_i      : in  std_logic;  -- get occupancy for flushing out
   fs_open2_o     : out std_logic;  -- FIFO occupancy is valid
   typ_fvram_i    : in  std_logic;  -- fifo virtualized BRAM
   fif_rq_i       : in  std_logic;  -- store requested occupancy
   fif_rd_cnt_o   : out std_logic_vector(11 downto 0); -- fifo occupancy
   fif_rdy_o      : out std_logic;  -- FIFO occupancy is OK to transmit
   fif_emty_o     : out std_logic   -- FIFO empty
);
end component;

--
-- 2nd E-bone master
--
component ebftm_mx
generic (
   EBX_MSG_MID  : natural := 1;   -- message master identifier
   EBS_DWIDTH   : natural := 64   -- E-bone data width 
);
port (
-- 2nd E-bone master interface
   eb_ft_clk_i  : in  std_logic;  -- system clock
   eb_ft_rst_i  : in  std_logic;  -- synchronous system reset

   eb2_mx_brq_o : out std_logic;  -- bus request
   eb2_mx_bg_i  : in std_logic;   -- bus grant
   eb2_mx_as_o  : out std_logic;  -- adrs strobe
   eb2_mx_eof_o : out std_logic;  -- end of frame
   eb2_mx_aef_o : out std_logic;  -- almost end of frame
   eb2_mx_dat_o : out std_logic_vector(EBS_DWIDTH-1 downto 0); 
   ebx2_msg_set_o : out std8;     -- message management

-- 2nd E-bone master shared bus
   eb2_dk_i     : in std_logic;   -- data acknowledge
   eb2_err_i    : in std_logic;   -- bus error
   eb2_dat_i    : in std_logic_vector(EBS_DWIDTH-1 downto 0);
   eb2_bmx_i    : in std_logic;   -- busy some master (but FT)
   eb2_bft_i    : in std_logic;   -- busy FT

-- Burst management i/f
   mif_adrs_i   : in  std32;      -- MIF starting address
   mif_count_i  : in  std16;      -- MIF word count
   mx_adrs_i    : in  std32;      -- burst starting address
   mx_brq_i     : in  std_logic;  -- burst request command
   mx_bwr_i     : in  std_logic;  -- burst write command
   mx_clr_i     : in  std_logic;  -- clear message command
   mx_open_i    : in  std_logic;  -- open message command
   mx_rst_i     : in  std_logic;  -- MIF reset command
   mx_read_i    : in  std_logic;  -- burst read command
   mx_aend_i    : in  std_logic;  -- burst almost end command
   mx_end_i     : in  std_logic;  -- burst end command
   mx_abrt_i    : in  std_logic;  -- burst abort command
   mx_bg_o      : out std_logic;  -- burst request acknowledge
   mx_ss_o      : out std_logic;  -- data size strobe
   mx_ardy_o    : out std_logic;  -- burst data almost ready
   mx_rdy_o     : out std_logic;  -- burst data ready
   mx_err_o     : out std_logic;  -- burst error
   mx_dout_o    : out std_logic_vector(EBS_DWIDTH-1 downto 0)
);
end component;

--
-- Fifo scan descriptors 
--
component ebftm_fif_desc is
generic (
   EBFT_FSCAN   : natural := 8    -- nb. of descriptors
);
port (
-- Status @ft_clk
   fs_stat_i    : in  std32_a(EBFT_FSCAN-1 downto 0);
   fs_sel_i     : in  std4;       -- fifo selector
   fs_irqtim_i  : in  std_logic;  -- internal timer IRQ pending
   fs_irqf_o    : out std_logic_vector(EBFT_FSCAN-1 downto 0); -- IRQ full

-- Control i/f
   eb_clk_i     : in  std_logic;  -- system clock
   fsc_mwr_i    : in  std_logic;  -- mem. desc. write enable
   fsc_madrs_i  : in  std8;       -- mem. desc. address
   fsc_mdati_i  : in  std32;      -- mem. desc. data in
   fsc_mdato_o  : out std32;      -- mem. desc. data out

   fsc_sreg_o   : out std32_a(EBFT_FSCAN-1 downto 0);
   fsc_irqs_o   : out std32;      -- IRQ status

-- Selected descriptor output
   fsc_srce_o   : out std32;      -- source
   fsc_sadrs_o  : out std32;      -- source addrs. (VM)
   fsc_dalsb_o  : out std32;      -- dest. addrs. LSbits
   fsc_damsb_o  : out std32;      -- dest. addrs. MSbits
   fsc_ctrl_o   : out std32       -- fifo readout control
);
end component;

--
-- Fifo scan
-- Packet pre-processing state machine
--
component ebftm_fif_fsm is
generic (
   EBFT_FSCAN   : natural := 8   -- nb. of fifos
);
port (
-- External control port @eb_clk_i
   c_fsc_cmd_i  : in  std32;     -- fifo scan command register

-- Descriptor pre-processing control
   eb_ft_clk_i  : in  std_logic; -- FT system clock
   eb_ft_rst_i  : in  std_logic; -- synchronous system reset
   typ_fvmem_i  : in  std_logic; -- fifo virtualized memory
   fs_sel_o     : out std4;      -- fifo selector
   fs_first_o   : out std_logic; -- new scan starts
   fs_init_o    : out std_logic; -- descriptor counter initialization
   fs_firq_o    : out std_logic; -- force IRQ status
   fs_upcnt_o   : out std_logic; -- descriptor counter update
   fs_tclr_o    : out std_logic; -- internal timer clear
   fs_ald_o     : out std_logic; -- descriptor address reload
   fs_itack_o   : out std_logic; -- internal time out acknowledge
   fs_balign_i  : in  std_logic; -- buffer is payload aligned
   fs_zero_i    : in  std_logic; -- VM 256 word aligned
   fs_b256_i    : in  std_logic; -- VM destination is 256 word aligned
   fs_bcast_i   : in  std_logic; -- last packet to fill buffer
   fs_finit_i   : in  std_logic; -- force initialization
   fs_valid_i   : in  std_logic; -- descriptor valid
   fs_etrig_i   : in  std_logic; -- external trigger control
   fs_itim_i    : in  std_logic; -- internal timer control
   fs_irqtim_i  : in  std_logic; -- internal timer IRQ pending
   fs_rst_i     : in  std_logic; -- fifo reset request
   fs_hmode_i   : in  std_logic; -- half full IRQ mode
   fs_full0_i   : in  std_logic; -- readout buffer half-full
   fs_irq0_i    : in  std_logic; -- IRQ#0 (half-full) pending
   fs_full1_i   : in  std_logic; -- readout buffer full
   fs_irq1_i    : in  std_logic; -- IRQ pending

-- External triggers
   fs_trig_i    : in  std_logic_vector(11 downto 0);
   fs_tack_o    : out std_logic_vector(11 downto 0);

-- memory mnagement i/f
   run0_i       : in  std_logic; -- descriptor #0 running
   run1_i       : in  std_logic; -- descriptor #1 running
   fsc_run_o    : out std_logic; -- fifo scan running

-- FT FSM i/f
   typ_fscan_o  : out std_logic; -- fifo scan type selector
   fpp_init_o   : out std_logic; -- new (pseudo) descriptor start
   fpp_flush_o  : out std_logic; -- flush command
   fpp_open_o   : out std_logic; -- message open command
   fpp_bcast_o  : out std_logic; -- do broad-cast
   fpp_rst_o    : out std_logic; -- fifo reset request
   fpp_done_i   : in  std_logic; -- fifo scan descriptor execution done
   fpp_skip_i   : in  std_logic; -- fifo scan descriptor skipped
   pp_done_i    : in  std_logic; -- (pseudo) descriptor execution done
   pp_err_i     : in  std_logic; -- error abort flag

   ftaerr_i     : in  std_logic; -- FT adrs. error flag
   ftderr_i     : in  std_logic; -- FT data  error flag
   adrerr_i     : in  std_logic; -- source adrs. error flag
   daterr_i     : in  std_logic; -- data error flag
   timerr_i     : in  std_logic; -- timeout error flag
   fsc_abrt_o   : out std_logic; -- some error 
   fsc_stat_o   : out std32      -- fifo scan status for broad-casting
);
end component;

--
-- Fifo scan 
-- Packet pre-processing for boundary crossing 
--
component ebftm_fif_pp is
generic (
   EBFT_DWIDTH  : natural := 64;   -- FT data width 
   EBFT_DBOUND  : natural := 4096; -- Dest. boundady crossing 2**N (0=none)
   EBFT_FSCAN   : natural := 8     -- nb. of fifos
);
port (
-- Descriptor @eb_clk_i
   c_fsc_srce_i   : in  std32;   -- source
   c_fsc_sadrs_i  : in  std32;   -- source addrs. (VM)
   c_fsc_dalsb_i  : in  std32;   -- dest. addrs. LSbits
   c_fsc_damsb_i  : in  std32;   -- dest. addrs. MSbits
   c_fsc_ctrl_i   : in  std32;   -- fifo readout control

-- IRQs acknowledge @eb_clk_i
   c_fsc_irqofs_i : in  std4;  
   c_fsc_irqwr0_i : in  std_logic;  
   c_fsc_irqwr1_i : in  std_logic;  

-- Descriptor pre-processing control
   eb_ft_clk_i  : in  std_logic; -- FT system clock
   eb_ft_rst_i  : in  std_logic; -- synchronous system reset
   ft_wsize_i   : in  unsigned(7 downto 0); -- FT payload size in words
   fs_sel_i     : in  std4;      -- fifo selector
   fs_first_i   : in  std_logic; -- new scan starts
   fs_init_i    : in  std_logic; -- descriptor counter initialization
   fs_firq_i    : in  std_logic; -- force IRQ status
   fs_upcnt_i   : in  std_logic; -- descriptor counter update
   fs_tclr_i    : in  std_logic; -- internal timer clear
   fs_itack_i   : in  std_logic; -- internal time out acknowledge
   fs_ald_i     : in  std_logic; -- descriptor address reload
   fs_balign_o  : out std_logic; -- buffer is payload aligned
   fs_zero_o    : out std_logic; -- VM 256 word aligned
   fs_b256_o    : out std_logic; -- VM destination is 256 word aligned
   fs_bcast_o   : out std_logic; -- request to send IRQ
   fs_hmode_o   : out std_logic; -- half full IRQ mode
   fs_finit_o   : out std_logic; -- force initialization
   fs_valid_o   : out std_logic; -- descriptor valid
   fs_etrig_o   : out std_logic; -- external trigger control
   fs_rst_o     : out std_logic; -- fifo reset request
   fs_irqtim_o  : out std_logic; -- internal timer IRQ pending
   fs_full0_o   : out std_logic; -- readout buffer went half-full
   fs_irq0_o    : out std_logic; -- IRQ#0 (half-full) pending
   fs_full1_o   : out std_logic; -- readout buffer went full
   fs_irq1_o    : out std_logic; -- IRQ#1 (full) pending
   fs_stat_o    : out std32_a(EBFT_FSCAN-1 downto 0); -- status registers

-- Address incrementer i/f
   fsc_damsb_o  : out std32;     -- fifo desc. dest. address MSbits

-- FT FSM i/f
   fif_rd_cnt_i : std_logic_vector(11 downto 0); -- fifo occupancy
   fs_open2_i   : in  std_logic; -- FIFO occupancy is valid
   fpp_flush_i  : in  std_logic; -- flush command
   fpp_bcast_i  : in  std_logic; -- do broad-cast
   typ_fvmem_o  : out std_logic; -- fifo virtualized memory
   typ_fvram_o  : out std_logic; -- fifo virtualized BRAM
   fs_srce_o    : out std32;     -- source location
   fs_sadrs_o   : out std32;     -- source address (VM)
   fs_dalsb_o   : out std32;     -- dest. address LSbits
   fs_bcnt_o    : out std_logic_vector(23 downto 0) -- byte count
);
end component;

--
-- Fifo scan internal timer
--
component ebftm_fif_timer is
generic (
   EBFT_FSCAN   : natural := 8;   -- nb. of descriptors
   EBFT_FCLK    : natural := 125  -- Clock frequency in MHz (for timer)
);
port (
-- External control port @eb_clk_i
   c_fsc_cmd_i   : in  std32;     -- fifo scan command register

-- FSM and PP i/f
   eb_ft_clk_i   : in  std_logic; -- system clock
   fs_tclr_i     : in  std_logic; -- internal timer clear
   fs_irqf_i     : in  std_logic_vector(EBFT_FSCAN-1 downto 0); -- IRQ full
   fs_itack_i    : in  std_logic; -- internal time out acknowledge
   fs_itim_o     : out std_logic  -- internal time out
);
end component;

function set_fscan return integer is 
variable tmp : integer;
begin 
   if EBFT_FSCAN = 0 then
      tmp := 1; -- prevents null vector, although unused
   else
      tmp := EBFT_FSCAN;
   end if;
   return(tmp);
end set_fscan; 

constant FSCAN_MAX : natural := set_fscan;
constant MRW_SIZE  : integer := set_mrw(EBFT_MEMS);

-- Signals
-- @eb_clk_i
-- Main control registers and descriptors
signal c_dma_regs  : std32_a(MRW_SIZE-1 downto 0);
signal c_dma0_stat : std32;
signal c_dma1_stat : std32;
signal c_dma_stat  : std32;

-- fifo scan dedicated registers and descriptors
signal c_fsc_irqofs : std4; 
signal c_fsc_irqwr0 : std_logic; 
signal c_fsc_irqwr1 : std_logic; 
signal c_fsc_mwr   : std_logic;
signal c_fsc_madrs : std8; 
signal c_fsc_mdati : std32; 
signal c_fsc_mdato : std32; 
signal c_fsc_sreg  : std32_a(FSCAN_MAX-1 downto 0);
signal c_fsc_irqs  : std32; 
signal c_fsc_stat  : std32;
signal c_fsc_cmd   : std32;
signal c_fsc_itack : std_logic; 

-- fifo scan selected descriptor contents
signal c_fsc_srce  : std32; 
signal c_fsc_sadrs : std32; 
signal c_fsc_dalsb : std32;
signal c_fsc_damsb : std32; 
signal c_fsc_ctrl  : std32;

-- @eb_ft_clk_i
signal eb_ft_rst  : std_logic;  -- synchronous system reset
signal dma0_go    : std_logic;
signal dma0_link  : std_logic;
signal dma0_abrt  : std_logic;
signal dma0_flush : std_logic;
signal dma0_fifo  : std_logic;
signal dma0_mif   : std_logic;
signal dma1_go    : std_logic;
signal dma1_link  : std_logic;
signal dma1_abrt  : std_logic;
signal dma1_flush : std_logic;
signal dma1_fifo  : std_logic;
signal dma1_mif   : std_logic;

signal dx_init    : std_logic;  -- packet descriptor initialization
signal dx_load    : std_logic;  -- packet descriptor count re-load
signal run0       : std_logic;  -- descriptor #0 running
signal run1       : std_logic;  -- descriptor #1 running
signal pp_zero    : std_logic;  -- zero byte count from DMA descriptor

signal dx_srce    : std32;      -- source location
signal dx_sadrs   : std32;      -- source address
signal dx_dalsb   : std32;      -- dest. address LSbits
signal dx_damsb   : std32;      -- dest. address MSbits
signal dx_bcnt    : std_logic_vector(23 downto 0); -- byte count

signal pp_init    : std_logic;  -- new (pseudo) descriptor start
signal pp_bcast   : std_logic;  -- do broad-cast
signal pp_abrt    : std_logic;  -- abort request
signal pp_flush   : std_logic;  -- flush request
signal pp_clr     : std_logic;  -- clear request
signal pp_rst     : std_logic;  -- reset request
signal pp_done    : std_logic;  -- (pseudo) descriptor execution done
signal pp_err     : std_logic;  -- error abort flag

signal fpp_done   : std_logic;  -- fifo scan descriptor execution done
signal fpp_skip   : std_logic;  -- fifo scan descriptor skipped

signal typ_mif    : std_logic;  -- MIF type selector
signal typ_fifo   : std_logic;  -- FIFO memory type selector
signal typ_fscan  : std_logic;  -- fifo scan type selector
signal typ_fvmem  : std_logic;  -- fifo virtual memory
signal typ_fvram  : std_logic;  -- fifo virtualized BRAM

signal dma_abrt   : std_logic;  -- soft abort request
signal dma0_stat  : std32;      -- DMA desc. #0 status 
signal dma1_stat  : std32;      -- DMA desc. #1 status 
signal dma_stat   : std32;      -- DMA status 
signal mx_adrs    : std32;      -- burst starting address
signal mx_brq     : std_logic;  -- burst request command
signal mx_bwr     : std_logic;  -- burst write command
signal mx_clr     : std_logic;  -- clear message command
signal mx_open    : std_logic;  -- open message command
signal mx_rst     : std_logic;  -- MIF reset command
signal mx_read    : std_logic;  -- burst read data command
signal mx_aend    : std_logic;  -- burst almost end command
signal mx_end     : std_logic;  -- burst end command
signal mx_abrt    : std_logic;  -- burst abort command
signal mx_bg      : std_logic;  -- burst request acknowledge
signal mx_ss      : std_logic;  -- data size strobe
signal mx_ardy    : std_logic;  -- burst data almost ready
signal mx_rdy     : std_logic;  -- burst data ready
signal mx_err     : std_logic;  -- burst error
signal mx_dout    : std_logic_vector(EBFT_DWIDTH-1 downto 0);
signal mif_adrs   : std32;      -- MIF starting address
signal mif_count  : std16;      -- MIF word count
signal eb2_mx_as  : std_logic; 
signal eb_ft_bmx  : std_logic; 
signal eb_ft_eof  : std_logic;  -- FT end of frame
signal firstword  : std_logic;  -- flag 1st word (in 1st elementary burst)
signal ebrq       : std_logic;  -- E-bone request
signal dsize    : std4;       -- early size for data (CHCH)
signal wsize    : std4;       -- slave size memorized for burst
signal b1st     : std8;       -- 1st word MSbytes count (0=all) 
signal bcld     : std_logic;  -- ebone burst counter load
signal bcen     : std_logic;  -- ebone burst counter enable
signal bcazero  : std_logic;  -- burst counter almost zero flag
signal bczero   : std_logic;  -- burst counter zero flag
signal fcld     : std_logic;  -- fifo counter load
signal fcen     : std_logic;  -- fifo counter enable
signal fczero   : std_logic;  -- fifo counter zero flag
signal mald     : std_logic;  -- mem. address counter load
signal maen     : std_logic;  -- mem. address counter enable
signal daclr    : std_logic;  -- dest. counter clear
signal daen     : std_logic;  -- dest. counter enable
signal daendall : std_logic;  -- dest. end flag
signal daendm1  : std_logic;  -- dest. end flag minus 1
signal daendm2  : std_logic;  -- dest. end flag minus 2
signal daendm3  : std_logic;  -- dest. end flag minus 3
signal dupdate  : std_logic;  -- dest. address update valid
signal daburst  : std32;      -- dest. address current burst
signal ftaerr   : std_logic;  -- FT adrs. error flag
signal ftderr   : std_logic;  -- FT data  error flag
signal adrerr   : std_logic;  -- source adrs. error flag
signal daterr   : std_logic;  -- data error flag
signal ovlerr   : std_logic;  -- overlapping readout error flag
signal timerr   : std_logic;  -- timeout error flag
signal irq_stat : std32;      -- descriptor status for broad-casting
signal dma_damsb: std32;      -- DMA desc. dest. address MSbits

signal eb_ft_as_o   : std_logic;  -- FT adrs strobe
signal eb_ft_ss_o   : std_logic;  -- FT size strobe
signal eb_ft_dsz_o  : std8;       -- FT data size qualifier
signal eb_ft_blen_o : std16;      -- FT burst length in bytes

signal fif_rq    : std_logic;  -- store requested occupancy
signal fif_rdy   : std_logic;  -- FIFO occupancy is OK to transmit
signal fif_emty  : std_logic;  -- FIFO empty
signal fif_rd_cnt: std_logic_vector(11 downto 0); -- fifo occupancy
signal remcnt    : unsigned(23 downto 0); -- remaining count in words
signal ft_wsize  : unsigned(7 downto 0);  -- FT payload size in words

signal fs_sel    : std4; 
signal fs_stat   : std32_a(FSCAN_MAX-1 downto 0);
signal fs_irqf   : std_logic_vector(FSCAN_MAX-1 downto 0); -- IRQ full status
signal fs_srce   : std32;     -- source location
signal fs_sadrs  : std32;     -- source address (VM)
signal fs_dalsb  : std32;     -- dest. address LSbits
signal fs_bcnt   : std_logic_vector(23 downto 0); -- byte count
signal fsc_stat  : std32;     -- fifo scan status
signal fsc_damsb : std32;     -- fifo desc. dest. address MSbits
signal fsc_abrt  : std_logic; -- some error 
signal fsc_run   : std_logic; 
signal fs_first  : std_logic; -- new scan starts
signal fs_finit  : std_logic; -- force initialization
signal fs_init   : std_logic; -- descriptor counter initialization
signal fs_firq   : std_logic; -- force IRQ status
signal fs_upcnt  : std_logic; -- descriptor counter update
signal fs_tclr   : std_logic; -- internal timer clear
signal fs_itim   : std_logic; -- internal timer time out
signal fs_itack  : std_logic; -- internal time out acknowledge
signal fs_itack2 : std_logic; -- internal time out acknowledge
signal fs_ald    : std_logic; -- descriptor address reload
signal fs_balign : std_logic; -- buffer is payload aligned
signal fs_bcast  : std_logic; -- request to send IRQ
signal fs_hmode  : std_logic; -- half full IRQ moded
signal fs_valid  : std_logic; -- descriptor valid
signal fs_etrig  : std_logic; -- external trigger control
signal fs_rst    : std_logic; -- fifo reset request
signal fs_irqtim : std_logic; -- internal timer IRQ pending
signal fs_full0  : std_logic; -- readout buffer half-full
signal fs_irq0   : std_logic; -- IRQ#0 (half-full) pending
signal fs_full1  : std_logic; -- readout buffer full
signal fs_irq1   : std_logic; -- IRQ#1 (full) pending
signal fs_zero   : std_logic; -- VM 256 word aligned
signal fs_b256   : std_logic; -- force MIF count to 256 words
signal fs_open   : std_logic; -- get occupancy for flushing out
signal fs_open2  : std_logic; -- FIFO occupancy is valid

signal fpp_init  : std_logic; -- fifo scan new (pseudo) descriptor start
signal fpp_flush : std_logic; -- flush command
signal fpp_open  : std_logic; -- message open command
signal fpp_bcast : std_logic; -- fifo scan do broad-cast
signal fpp_rst   : std_logic; -- fifo reset request

------------------------------------------------------------------------
begin
------------------------------------------------------------------------

assert    (EBFT_FSCAN <= 12)
       report "EBFT_FSCAN must be 0, or max. 12!" severity failure;
assert    ((EBFT_FSCAN = 0) AND (EBS_AD_SIZE = 32)) 
       OR ((EBFT_FSCAN > 0) AND (EBS_AD_SIZE = 128))
       report "EBS_AD_SIZE invalid; must be 32 or 128 depending on EBFT_FSCAN!" 
       severity failure;
assert EBS_AD_OFST mod EBS_AD_SIZE = 0 
       report "EBS_AD_OFST invalid boundary!" severity failure;
assert (EBFT_DWIDTH mod 32 = 0) OR (EBFT_DWIDTH/32 mod 2 = 0)
       report "EBFT_DWIDTH invalid!" severity failure;
assert (EBFT_DBOUND = 2**(NLog2(EBFT_DBOUND)) OR EBFT_DBOUND=0)
       report "EBFT_DBOUND must be power of 2!" severity failure;

eb_ft_o.eb_as   <= eb_ft_as_o;
eb_ft_o.eb_ss   <= eb_ft_ss_o;
eb_ft_o.eb_eof  <= eb_ft_eof;
eb_ft_o.eb_dsz  <= eb_ft_dsz_o;
eb_ft_o.eb_blen <= eb_ft_blen_o;

-- Nullify useless signals when NO fifo scan

no_fscan: if EBFT_FSCAN = 0 generate
   c_fsc_mdato <= (others => '0');
   c_fsc_sreg  <= (others => (others => '0'));
   c_fsc_irqs  <= (others => '0');
   fsc_stat    <= (others => '0');
   fsc_abrt    <= '0';
   fsc_run     <= '0';
   typ_fscan   <= '0';
   typ_fvmem   <= '0';
   typ_fvram   <= '0';
   fpp_init    <= '0';
   fpp_open    <= '0';
   fpp_rst     <= '0';
   fpp_bcast   <= '0'; 
   fpp_flush   <= '0';
   fs_ald      <= '0'; 
   fs_b256     <= '0'; 
   fs_srce     <= (others => '0');
   fs_sadrs    <= (others => '0');
   fs_bcnt     <= (others => '0');
   fs_dalsb    <= (others => '0');
   fsc_damsb   <= (others => '0');
end generate no_fscan;

-- Nullify useless signals when NO memory support

no_mems: if NOT EBFT_MEMS generate
   dma0_stat   <= (others => '0');
   dma1_stat   <= (others => '0');
   typ_mif     <= '0';
   typ_fifo    <= '0';
   dma_abrt    <= '0';
   dx_init     <= '0';
   run0        <= '0';
   run1        <= '0';
   pp_bcast    <= '0';
   pp_init     <= '0';
   pp_abrt     <= '0';
   pp_flush    <= '0';
   pp_clr      <= '0';
   pp_rst      <= '0';
   dx_srce     <= (others => '0');
   dx_bcnt     <= (others => '0');
   dx_sadrs    <= (others => '0');
   dx_dalsb    <= (others => '0');
   dma_damsb   <= (others => '0');
end generate no_mems;

-- Control clock to FT clock domain crossing
--------------------------------------------
fta: if EBFT_ASYNCH generate
eb_ft_bmx <= '0'; -- FT is single master
end generate fta;

fts: if NOT EBFT_ASYNCH generate -- does nothing
   eb_ft_bmx <= ebs_i.eb_bmx;
end generate fts;

process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      eb_ft_rst   <= ebs_i.eb_rst;
      fs_itack2   <= fs_itack;
  end if;
end process;

-- FT to control clock domain crossing
-- (or P&R help when synchronous)
--------------------------------------
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      c_dma0_stat <= dma0_stat;
      c_dma1_stat <= dma1_stat;
      c_dma_stat  <= dma_stat;
      c_fsc_stat  <= fsc_stat;
      c_fsc_itack <= fs_itack OR fs_itack2;
  end if;
end process;

-- E-bone registers slave i/f
-----------------------------
regs: ebftm_regs 
generic map(
   EBS_AD_RNGE  => EBS_AD_RNGE,  -- short adressing range
   EBS_AD_BASE  => EBS_AD_BASE,  -- usual IO segment
   EBS_AD_SIZE  => EBS_AD_SIZE,  -- size in segment
   EBS_AD_OFST  => EBS_AD_OFST,  -- offset in segment
   EBFT_MEMS    => EBFT_MEMS,    -- memory support (or not)
   EBFT_FSCAN   => FSCAN_MAX     -- nb. of fifo scan
)
port map(
-- E-bone slave interface
   eb_clk_i     => eb_clk_i,     -- system control clock
   ebs_i        => ebs_i, 
   ebs_o        => ebs_o, 

-- DMAC registers and control
   dma_regs_o   => c_dma_regs,   -- register outputs
   dma0_stat_i  => c_dma0_stat,  -- DMA #0 status 
   dma1_stat_i  => c_dma1_stat,  -- DMA #1 status
   dma_stat_i   => c_dma_stat,   -- DMA status

-- FSCAN registers and control
   fsc_itack_i  => c_fsc_itack, -- internal time out acknowledge
   fsc_cmd_o    => c_fsc_cmd,   -- fifo scan command register
   fsc_irqofs_o => c_fsc_irqofs,-- IACK register offset
   fsc_irqwr0_o => c_fsc_irqwr0,-- IACK#0 write
   fsc_irqwr1_o => c_fsc_irqwr1,-- IACK#1 write
   fsc_mwr_o    => c_fsc_mwr,   -- mem. desc. write enable
   fsc_madrs_o  => c_fsc_madrs, -- mem. desc. address
   fsc_mdati_o  => c_fsc_mdati, -- mem. desc. data in
   fsc_mdato_i  => c_fsc_mdato, -- mem. desc. data out
   fsc_sreg_i   => c_fsc_sreg,  -- fifo scan status array
   fsc_irqs_i   => c_fsc_irqs,  -- IRQ status
   -- @ft_clk
   fsc_stat_i   => fsc_stat,    -- fifo scan status
   fsc_abrt_i   => fsc_abrt     -- some error 
);

-- Fifo scan dedicated descriptors 
----------------------------------
dfscan: if EBFT_FSCAN > 0 generate
fif_desc: ebftm_fif_desc 
generic map(
   EBFT_FSCAN   => FSCAN_MAX    -- nb. of descriptors
)
port map(
-- Fifo scan status array @ft_clk
   fs_stat_i    => fs_stat,
   fs_sel_i     => fs_sel,      -- fifo selector
   fs_irqtim_i  => fs_irqtim,   -- internal timer IRQ pending
   fs_irqf_o    => fs_irqf,     -- IRQ full aggregated

-- Control i/f
   eb_clk_i     => eb_clk_i,    -- system clock
   fsc_mwr_i    => c_fsc_mwr,   -- mem. desc. write enable
   fsc_madrs_i  => c_fsc_madrs, -- mem. desc. address
   fsc_mdati_i  => c_fsc_mdati, -- mem. desc. data in
   fsc_mdato_o  => c_fsc_mdato, -- mem. desc. data out
   fsc_sreg_o   => c_fsc_sreg,  -- fifo scan status array
   fsc_irqs_o   => c_fsc_irqs,  -- IRQ status

-- Selected descriptor output
   fsc_srce_o   => c_fsc_srce,  -- source
   fsc_sadrs_o  => c_fsc_sadrs, -- source addrs. (VM)
   fsc_dalsb_o  => c_fsc_dalsb, -- dest. addrs. LSbits
   fsc_damsb_o  => c_fsc_damsb, -- dest. addrs. MSbits
   fsc_ctrl_o   => c_fsc_ctrl   -- fifo readout control
);
end generate dfscan;

------------------------------------------------------------------------
-- Below is the ft_clk clock domain
------------------------------------------------------------------------

-- 64 bit destination address incrementer 
-----------------------------------------
ainc: ebftm_ainc
generic map(
   EBFT_DA64   => EBFT_DA64
)
port map(
   eb_ft_clk_i  => eb_ft_clk_i, -- FT system clock
   dma_damsb_i  => dma_damsb,   -- DMA desc. dest. address MSbits
   dx_init_i    => dx_init,     -- DMA desc. initialization
   fsc_damsb_i  => fsc_damsb,   -- FIFO desc. dest. address MSbits
   fs_ald_i     => fs_ald,      -- FIFO desc. address reload
   daburst_i    => daburst,     -- dest. address current burst
   dupdate_i    => dupdate,     -- dest. address update valid
   dx_damsb_o   => dx_damsb     -- packet dest. address MSbits
);

-- Broad-cast data management 
-----------------------------
bcast: ebftm_bcast 
port map(
   eb_ft_clk_i  => eb_ft_clk_i, -- FT system clock
   pp_err_i     => pp_err,      -- error abort flag
   run0_i       => run0,        -- descriptor #0 running
   run1_i       => run1,        -- descriptor #1 running
   pp_bcast_i   => pp_bcast,    -- do broad-cast
   dma0_stat_i  => dma0_stat,   -- descriptor #0 status 
   dma1_stat_i  => dma1_stat,   -- descriptor #1 status 

   fsc_run_i    => fsc_run,     -- fifo scan running
   fpp_bcast_i  => fpp_bcast,   -- do broad-cast
   fsc_stat_i   => fsc_stat,    -- fifo scan status for broad-casting

   irq_stat_o   => irq_stat     -- descriptor status for broad-casting
);

-- E-bone master Fast Transmitter FSM
--------------------------------------
fsm: ebftm_fsm 
generic map(
   EBFT_DWIDTH  => EBFT_DWIDTH, -- FT data width 
   EBFT_FSCAN   => EBFT_FSCAN,  -- max. nb. of fifo scan
   EBS_VERSION  => EBS_VERSION  -- IP version
)
port map(
   eb_ft_clk_i  => eb_ft_clk_i,  -- FT clock
   eb_ft_rst_i  => eb_ft_rst,    -- synchronous system reset

-- E-bone Fast Transmitter
   eb_bmx_i     => eb_ft_bmx,    -- busy some master (but FT)
   eb_bft_i     => eb_ft_bg_i,   -- FT master bus grant
   eb_ft_brq_o  => eb_ft_brq_o,  -- FT master bus request
   eb_ft_dxt_o  => eb_ft_dxt_o,  -- FT data write
   eb_ft_as_o   => eb_ft_as_o,   -- FT data strobe
   eb_ft_ss_o   => eb_ft_ss_o,   -- FT size strobe
   eb_ft_eof_o  => eb_ft_eof,    -- FT end of frame
   eb_ft_dsz_o  => eb_ft_dsz_o,  -- FT data size qualifier
   eb_ft_dk_i   => eb_ft_dk_i,   -- master data acknowledge
   eb_ft_err_i  => eb_ft_err_i,  -- master bus error
   ft_psize_i   => ft_psize_i,   -- FT encoded payload size
   ebrq_o       => ebrq,         -- E-bone request

-- Fscan dedicated packet processor and FSM i/f 
   typ_fscan_i  => typ_fscan,    -- fifo scan
   typ_fvmem_i  => typ_fvmem,    -- fifo virtual BRAM
   fpp_init_i   => fpp_init,     -- new (pseudo) descriptor start
   fpp_open_i   => fpp_open,     -- message open command
   fpp_bcast_i  => fpp_bcast,    -- do broad-cast
   fpp_rst_i    => fpp_rst,      -- fifo reset request
   fpp_done_o   => fpp_done,     -- fifo scan descriptor execution done
   fpp_skip_o   => fpp_skip,     -- fifo scan descriptor skipped
   fs_open_o    => fs_open,      -- get occupancy for flushing out

-- Other packet processor and FSM i/f 
   dma_abrt_i   => dma_abrt,     -- soft abort request
   typ_mif_i    => typ_mif,      -- MIF type selector
   typ_fifo_i   => typ_fifo,     -- FIFO memory type selector
   dx_damsb_i   => dx_damsb,     -- dest. address MSbits

   pp_init_i    => pp_init,      -- new (pseudo) descriptor start
   pp_bcast_i   => pp_bcast,     -- do broad-cast
   pp_abrt_i    => pp_abrt,      -- abort request
   pp_flush_i   => pp_flush,     -- flush request
   pp_clr_i     => pp_clr,       -- clear request
   pp_rst_i     => pp_rst,       -- reset request
   pp_done_o    => pp_done,      -- (pseudo) descriptor execution done
   pp_err_o     => pp_err,       -- error abort flag

   ftaerr_o     => ftaerr,       -- FT adrs. error flag
   ftderr_o     => ftderr,       -- FT data  error flag
   adrerr_o     => adrerr,       -- source adrs. error flag
   daterr_o     => daterr,       -- data error flag
   ovlerr_o     => ovlerr,       -- overlapping readout error flag
   timerr_o     => timerr,       -- timeout readout error flag
   irq_stat_i   => irq_stat,     -- descriptor status for broad-casting

   dma_stat_o   => dma_stat,     -- DMA status
   dma_eot_o    => dma_eot_o,    -- end of transfer
   dma_err_o    => dma_err_o,    -- transfer aborted on error

-- Burst management i/f
   mx_brq_o     => mx_brq,       -- burst request command
   mx_bwr_o     => mx_bwr,       -- burst write command
   mx_clr_o     => mx_clr,       -- clear message command
   mx_open_o    => mx_open,      -- open message command
   mx_rst_o     => mx_rst,       -- MIF reset command
   mx_read_o    => mx_read,      -- burst read data command
   mx_end_o     => mx_end,       -- burst end command
   mx_aend_o    => mx_aend,      -- burst almost end command
   mx_abrt_o    => mx_abrt,      -- burst abort command
   mx_bg_i      => mx_bg,        -- burst request granted
   mx_ss_i      => mx_ss,        -- data size strobe
   mx_ardy_i    => mx_ardy,      -- burst almost ready
   mx_rdy_i     => mx_rdy,       -- burst ready
   mx_err_i     => mx_err,       -- burst error
   mx_dout_i    => mx_dout,      -- burst data

-- MIF fifo management
   fif_rq_o     => fif_rq,    -- store requested occupancy
   fif_rdy_i    => fif_rdy,   -- FIFO occupancy is OK to transmit
   fif_emty_i   => fif_emty,  -- FIFO empty

-- FSM related counters
   firstword_o  => firstword, -- flag 1st word (in 1st elementary burst)
   dsize_o      => dsize,     -- early size for data (CHCH)
   wsize_o      => wsize,     -- slave size memorized for burst
   b1st_i       => b1st,      -- 1st word MSbytes count (0=all) 

   bcld_o       => bcld,     -- ebone burst counter load
   bcen_o       => bcen,     -- ebone burst counter enable
   bcazero_i    => bcazero,  -- burst counter almost zero flag
   bczero_i     => bczero,   -- burst counter zero flag

   fcld_o       => fcld,     -- fifo counter load
   fcen_o       => fcen,     -- fifo counter enable
   fczero_i     => fczero,   -- fifo counter zero flag

   mald_o       => mald,     -- mem. address counter load
   maen_o       => maen,     -- mem. address counter enable
   daclr_o      => daclr,    -- dest. counter clear
   daen_o       => daen,     -- dest. counter enable
   daendall_i   => daendall, -- dest. end flag
   daendm1_i    => daendm1,  -- dest. end flag minus 1
   daendm2_i    => daendm2,  -- dest. end flag minus 2
   daendm3_i    => daendm3,  -- dest. end flag minus 3
   daburst_i    => daburst   -- dest. address current burst
);

-- E-bone master Fast Transmitter counters
------------------------------------------
ft: ebftm_ft 
generic map(
   EBFT_DWIDTH  => EBFT_DWIDTH  -- FT data width 
)
port map(
-- E-bone Fast Transmitter
   eb_ft_clk_i  => eb_ft_clk_i,  -- FT clock
   eb_ft_eof_i  => eb_ft_eof,    -- FT end of frame
   ft_psize_i   => ft_psize_i,   -- FT encoded payload size
   ebrq_i       => ebrq,         -- E-bone request
   eb_ft_blen_o => eb_ft_blen_o, -- FT burst length in bytes

-- Burst management i/f
   mif_adrs_o   => mif_adrs,     -- MIF starting address
   mif_count_o  => mif_count,    -- MIF word count
   mx_adrs_o    => mx_adrs,      -- burst starting address

-- Fscan packet processor i/f 
   typ_fscan_i  => typ_fscan,    -- fifo scan type selector
   fs_open2_i   => fs_open2,     -- FIFO occupancy is valid
   fs_b256_i    => fs_b256,      -- force MIF count to 256 words
   fs_srce_i    => fs_srce,      -- source location
   fs_sadrs_i   => fs_sadrs,     -- source address (VM)
   fs_dalsb_i   => fs_dalsb,     -- dest. address LSbits
   fs_bcnt_i    => fs_bcnt,      -- byte count

-- Mem. packet processor i/f 
   typ_mif_i    => typ_mif,      -- MIF type selector
   typ_fifo_i   => typ_fifo,     -- FIFO memory type selector
   dx_srce_i    => dx_srce,      -- source location
   dx_sadrs_i   => dx_sadrs,     -- source address
   dx_dalsb_i   => dx_dalsb,     -- dest. address LSbits
   dx_bcnt_i    => dx_bcnt,      -- byte count

-- MIF fifo management
   remcnt_o     => remcnt,   -- remaining count in words
   ft_wsize_o   => ft_wsize, -- FT payload size in words

-- FSM i/f
   firstword_i  => firstword, -- flag 1st word (in 1st elementary burst)
   dsize_i      => dsize,    -- early size for data (CHCH)
   wsize_i      => wsize,    -- slave size memorized for burst
   b1st_o       => b1st,     -- 1st word MSbytes count (0=all) 

   bcld_i       => bcld,     -- ebone burst counter load
   bcen_i       => bcen,     -- ebone burst counter enable
   bcazero_o    => bcazero,  -- burst counter almost zero flag
   bczero_o     => bczero,   -- burst counter zero flag

   fcld_i       => fcld,     -- fifo counter load
   fcen_i       => fcen,     -- fifo counter enable
   fczero_o     => fczero,   -- fifo counter zero flag

   mald_i       => mald,     -- mem. address counter load
   maen_i       => maen,     -- mem. address counter enable
   daclr_i      => daclr,    -- dest. counter clear
   daen_i       => daen,     -- dest. counter enable
   daendall_o   => daendall, -- dest. end flag
   daendm1_o    => daendm1,  -- dest. end flag minus 1
   daendm2_o    => daendm2,  -- dest. end flag minus 2
   daendm3_o    => daendm3,  -- dest. end flag minus 3
   dupdate_o    => dupdate,  -- dest. address update valid
   daburst_o    => daburst   -- dest. address current burst
);

-- MIF fifo occupancy management
--------------------------------
mif: ebftm_mif
port map (
   eb_ft_clk_i    => eb_ft_clk_i,    -- FT clock
   ebx2_msg_dat_i => ebx2_msg_dat_i, -- message data 
   ft_wsize_i     => ft_wsize,  -- FT payload size in words
   remcnt_i       => remcnt,    -- remaining count in words
   fs_open_i      => fs_open,   -- get occupancy for flushing out
   fs_open2_o     => fs_open2,  -- FIFO occupancy is valid
   typ_fvram_i    => typ_fvram, -- fifo virtualized BRAM
   fif_rq_i       => fif_rq,    -- store requested occupancy
   fif_rd_cnt_o   => fif_rd_cnt,  -- fifo occupancy
   fif_rdy_o      => fif_rdy,   -- FIFO occupancy is OK to transmit
   fif_emty_o     => fif_emty   -- FIFO empty
);

-- 2nd E-bone master
--------------------
mx: ebftm_mx
generic map (
   EBX_MSG_MID => EBX_MSG_MID,   -- message master identifier
   EBS_DWIDTH  => EBFT_DWIDTH    -- E-bone data width 
)
port map(
-- 2nd E-bone master interface
   eb_ft_clk_i  => eb_ft_clk_i,  -- FT clock
   eb_ft_rst_i  => eb_ft_rst,    -- synchronous system reset

   eb2_mx_brq_o => eb2_mx_brq_o, -- bus request
   eb2_mx_bg_i  => eb2_mx_bg_i,  -- bus grant
   eb2_mx_as_o  => eb2_mx_as,    -- adrs strobe
   eb2_mx_eof_o => eb2_mx_eof_o, -- end of frame
   eb2_mx_aef_o => eb2_mx_aef_o, -- almost end of frame
   eb2_mx_dat_o => eb2_mx_dat_o, -- master data write
   ebx2_msg_set_o => ebx2_msg_set_o, -- message management

-- 2nd E-bone master shared bus
   eb2_dk_i     => eb2_dk_i,     -- data acknowledge
   eb2_err_i    => eb2_err_i,    -- bus error
   eb2_dat_i    => eb2_dat_i,    -- master data in
   eb2_bmx_i    => eb2_bmx_i,    -- busy some master (but FT)
   eb2_bft_i    => eb2_bft_i,    -- busy FT

-- Burst management i/f
   mif_adrs_i   => mif_adrs,     -- MIF starting address
   mif_count_i  => mif_count,    -- MIF word count
   mx_adrs_i    => mx_adrs,      -- burst starting address
   mx_brq_i     => mx_brq,       -- burst request command
   mx_bwr_i     => mx_bwr,       -- burst write command
   mx_clr_i     => mx_clr,       -- clear message command
   mx_open_i    => mx_open,      -- open message command
   mx_rst_i     => mx_rst,       -- MIF reset command
   mx_read_i    => mx_read,      -- burst read data command
   mx_aend_i    => mx_aend,      -- burst almost end command
   mx_end_i     => mx_end,       -- burst end command
   mx_abrt_i    => mx_abrt,      -- burst abort command
   mx_bg_o      => mx_bg,        -- burst request acknowledge
   mx_ss_o      => mx_ss,        -- data size strobe
   mx_ardy_o    => mx_ardy,      -- burst almost ready
   mx_rdy_o     => mx_rdy,       -- burst ready
   mx_err_o     => mx_err,       -- burst error
   mx_dout_o    => mx_dout       -- burst data
);

eb2_mx_as_o <= eb2_mx_as;    -- internal to port

------------------------------------------------------------------------
-- Memory (or standard fifo) management follows
------------------------------------------------------------------------
do_mems: if EBFT_MEMS generate

-- Packet preprocessing state machine
-------------------------------------
mem_fsm: ebftm_mem_fsm
generic map(
   EBFT_ASYNCH  => EBFT_ASYNCH 
)
port map(
-- External control ports @eb_clk_i
   c_cmd_go_i     => cmd_go_i,    -- go command
   c_cmd_flush_i  => cmd_flush_i, -- flush command
   c_cmd_abort_i  => cmd_abort_i, -- abort command
   c_cmd_reset_i  => cmd_reset_i, -- MIF reset command

-- DMAC registers control bits
   eb_ft_clk_i  => eb_ft_clk_i,  -- FT system clock
   eb_ft_rst_i  => eb_ft_rst,    -- synchronous system reset
   dma0_go_i    => dma0_go,
   dma0_link_i  => dma0_link,
   dma0_abrt_i  => dma0_abrt,
   dma0_flush_i => dma0_flush,
   dma0_fifo_i  => dma0_fifo,
   dma0_mif_i   => dma0_mif,
   dma1_go_i    => dma1_go,
   dma1_link_i  => dma1_link,
   dma1_abrt_i  => dma1_abrt,
   dma1_flush_i => dma1_flush,
   dma1_fifo_i  => dma1_fifo,
   dma1_mif_i   => dma1_mif,

-- Fifo scan i/f
   fsc_run_i    => fsc_run,

-- Descriptor pre-processing control
   dx_init_o    => dx_init,      -- packet descriptor initialization
   dx_load_o    => dx_load,      -- packet descriptor count re-load
   run0_o       => run0,         -- descriptor #0 running
   run1_o       => run1,         -- descriptor #1 running
   pp_zero_i    => pp_zero,      -- zero byte count from DMA descriptor

-- FT FSM i/f
   typ_mif_o    => typ_mif,      -- MIF type selector
   typ_fifo_o   => typ_fifo,     -- fifo memory type selector
   dma_abrt_o   => dma_abrt,     -- soft abort request

   pp_init_o    => pp_init,      -- new (pseudo) descriptor start
   pp_bcast_o   => pp_bcast,     -- do broad-cast
   pp_abrt_o    => pp_abrt,      -- abort request
   pp_flush_o   => pp_flush,     -- flush request
   pp_clr_o     => pp_clr,       -- clear request
   pp_rst_o     => pp_rst,       -- reset request
   pp_done_i    => pp_done,      -- (pseudo) descriptor execution done
   pp_err_i     => pp_err,       -- error abort flag

   daen_i       => daen,       -- dest. counter enable
   ftaerr_i     => ftaerr,     -- FT adrs. error flag
   ftderr_i     => ftderr,     -- FT data  error flag
   adrerr_i     => adrerr,     -- source adrs. error flag
   daterr_i     => daterr,     -- data error flag
   ovlerr_i     => ovlerr,     -- overlapping readout error flag
   timerr_i     => timerr,     -- timeout error flag

-- Status 
   dma0_stat_o  => dma0_stat,  -- descriptor #0 status 
   dma1_stat_o  => dma1_stat   -- descriptor #1 status 
);

-- Packet preprocessing 
-----------------------
mem_pp: ebftm_mem_pp
generic map(
   EBFT_DWIDTH  => EBFT_DWIDTH,  -- FT data width 
   EBFT_DBOUND  => EBFT_DBOUND,  -- Dest. boundady crossing 2**N (0=none)
   EBFT_ASYNCH  => EBFT_ASYNCH 
)
port map(
-- DMAC registers and external control ports @eb_clk_i
   c_dma_regs_i => c_dma_regs,   -- registers

-- DMAC registers control bits
   eb_ft_clk_i  => eb_ft_clk_i,  -- FT system clock
   dma0_go_o    => dma0_go,
   dma0_link_o  => dma0_link,
   dma0_abrt_o  => dma0_abrt,
   dma0_flush_o => dma0_flush,
   dma0_fifo_o  => dma0_fifo,
   dma0_mif_o   => dma0_mif,
   dma1_go_o    => dma1_go,
   dma1_link_o  => dma1_link,
   dma1_abrt_o  => dma1_abrt,
   dma1_flush_o => dma1_flush,
   dma1_fifo_o  => dma1_fifo,
   dma1_mif_o   => dma1_mif,

-- Descriptor pre-processing control
   dx_init_i    => dx_init,  -- packet descriptor initialization
   dx_load_i    => dx_load, -- packet descriptor count re-load
   run0_i       => run0,     -- descriptor #0 running
   run1_i       => run1,     -- descriptor #1 running
   pp_zero_o    => pp_zero,  -- zero byte count from DMA descriptor

-- Address incrementer i/f
   dma_damsb_o  => dma_damsb,    -- DMA desc. dest. address MSbits

-- FT FSM i/f
   dx_srce_o    => dx_srce,      -- source location
   dx_sadrs_o   => dx_sadrs,     -- source address
   dx_dalsb_o   => dx_dalsb,     -- dest. address LSbits
   dx_bcnt_o    => dx_bcnt       -- byte count
);

end generate do_mems;

------------------------------------------------------------------------
-- Fifo scan management follows
------------------------------------------------------------------------
do_fscan: if EBFT_FSCAN > 0 generate

-- Fifo scan packet pre-processing for boundary crossing 
--------------------------------------------------------
fif_pp: ebftm_fif_pp 
generic map(
   EBFT_DWIDTH  => EBFT_DWIDTH, -- FT data width 
   EBFT_DBOUND  => EBFT_DBOUND, -- Dest. boundady crossing 2**N (0=none)
   EBFT_FSCAN   => EBFT_FSCAN   -- nb. of fifos
)
port map(
-- Descriptor @eb_clk_i
   c_fsc_srce_i   => c_fsc_srce,  -- source
   c_fsc_sadrs_i  => c_fsc_sadrs, -- source addrs. (VM)
   c_fsc_dalsb_i  => c_fsc_dalsb, -- dest. addrs. LSbits
   c_fsc_damsb_i  => c_fsc_damsb, -- dest. addrs. MSbits
   c_fsc_ctrl_i   => c_fsc_ctrl,  -- fifo readout control

-- IRQs acknowledge @eb_clk_i
   c_fsc_irqofs_i => c_fsc_irqofs,  
   c_fsc_irqwr0_i => c_fsc_irqwr0,   
   c_fsc_irqwr1_i => c_fsc_irqwr1,  

-- Descriptor pre-processing control
   eb_ft_clk_i  => eb_ft_clk_i, -- FT system clock
   eb_ft_rst_i  => eb_ft_rst,   -- synchronous system reset
   ft_wsize_i   => ft_wsize,    -- FT payload size in words
   fs_sel_i     => fs_sel,      -- fifo selector
   fs_first_i   => fs_first,    -- new scan starts
   fs_init_i    => fs_init,     -- descriptor counter initialization
   fs_firq_i    => fs_firq,     -- force IRQ status
   fs_upcnt_i   => fs_upcnt,    -- descriptor counter update
   fs_tclr_i    => fs_tclr,     -- internal timer clear
   fs_itack_i   => fs_itack,    -- internal time out acknowledge
   fs_ald_i     => fs_ald,      -- descriptor address reload
   fs_balign_o  => fs_balign,   -- buffer is payload aligned
   fs_zero_o    => fs_zero,     -- VM 256 word aligned
   fs_b256_o    => fs_b256,     -- VM still 256 word to read
   fs_bcast_o   => fs_bcast,    -- request to send IRQ
   fs_hmode_o   => fs_hmode,    -- half full IRQ mode
   fs_finit_o   => fs_finit,    -- force initialization
   fs_valid_o   => fs_valid,    -- descriptor valid
   fs_etrig_o   => fs_etrig,    -- external trigger control
   fs_rst_o     => fs_rst,      -- fifo reset request
   fs_irqtim_o  => fs_irqtim,   -- internal timer IRQ pending
   fs_full0_o   => fs_full0,    -- readout buffer went half-full
   fs_irq0_o    => fs_irq0,     -- IRQ#0 (half-full) pending
   fs_full1_o   => fs_full1,    -- readout buffer went full
   fs_irq1_o    => fs_irq1,     -- IRQ#1 (full) pending
   fs_stat_o    => fs_stat,     -- status registers

-- Address incrementer i/f
   fsc_damsb_o  => fsc_damsb,   -- fifo desc. dest. address MSbits

-- FT FSM i/f
   fif_rd_cnt_i => fif_rd_cnt,  -- fifo occupancy
   fs_open2_i   => fs_open2,    -- FIFO occupancy is valid
   fpp_flush_i  => fpp_flush,   -- flush command
   fpp_bcast_i  => fpp_bcast,   -- do broad-cast
   typ_fvmem_o  => typ_fvmem,   -- fifo virtual BRAM
   typ_fvram_o  => typ_fvram,   -- fifo virtualized memory
   fs_srce_o    => fs_srce,     -- source location
   fs_sadrs_o   => fs_sadrs,    -- source address (VM)
   fs_dalsb_o   => fs_dalsb,    -- dest. address LSbits
   fs_bcnt_o    => fs_bcnt      -- byte count
);

-- Fifo scan internal timer
---------------------------
fif_timer: ebftm_fif_timer 
generic map(
   EBFT_FSCAN   => EBFT_FSCAN,  -- nb. of descriptors
   EBFT_FCLK    => EBFT_FCLK    -- Clock frequency in MHz
)
port map(
-- External control port @eb_clk_i
   c_fsc_cmd_i  => c_fsc_cmd,   -- fifo scan command register

-- FSM and PP i/f
   eb_ft_clk_i  => eb_ft_clk_i, -- system clock
   fs_tclr_i    => fs_tclr,     -- internal timer clear
   fs_irqf_i    => fs_irqf,     -- IRQ full status
   fs_itack_i   => fs_itack,    -- internal time out acknowledge
   fs_itim_o    => fs_itim      -- internal time out
);

-- Fifo scan packet pre-processing state machine
------------------------------------------------
fif_fsm: ebftm_fif_fsm
generic map(
   EBFT_FSCAN   => EBFT_FSCAN   -- max nb. of descriptors
)
port map(
-- External control port @eb_clk_i
   c_fsc_cmd_i  => c_fsc_cmd,   -- fifo scan command register

-- Descriptor pre-processing control
   eb_ft_clk_i  => eb_ft_clk_i, -- FT system clock
   eb_ft_rst_i  => eb_ft_rst,   -- synchronous system reset
   typ_fvmem_i  => typ_fvmem,   -- fifo virtualized memory
   fs_sel_o     => fs_sel,      -- fifo selector
   fs_first_o   => fs_first,    -- new scan starts
   fs_init_o    => fs_init,     -- descriptor counter initialization
   fs_firq_o    => fs_firq,     -- force IRQ status
   fs_upcnt_o   => fs_upcnt,    -- descriptor counter update
   fs_tclr_o    => fs_tclr,     -- internal timer clear
   fs_ald_o     => fs_ald,      -- descriptor address reload
   fs_itack_o   => fs_itack,    -- internal time out acknowledge
   fs_balign_i  => fs_balign,   -- buffer is payload aligned
   fs_zero_i    => fs_zero,     -- VM 256 word aligned
   fs_b256_i    => fs_b256,     -- VM still 256 word to read
   fs_bcast_i   => fs_bcast,    -- request to send IRQ
   fs_finit_i   => fs_finit,    -- force initialization
   fs_valid_i   => fs_valid,    -- descriptor valid
   fs_etrig_i   => fs_etrig,    -- external trigger control
   fs_itim_i    => fs_itim,     -- internal timer control
   fs_irqtim_i  => fs_irqtim,   -- internal timer IRQ pending
   fs_rst_i     => fs_rst,      -- fifo reset request
   fs_hmode_i   => fs_hmode,    -- half full IRQ mode
   fs_full0_i   => fs_full0,    -- readout buffer went half-full
   fs_irq0_i    => fs_irq0,     -- IRQ#0 (half-full) pending
   fs_full1_i   => fs_full1,    -- readout buffer went full
   fs_irq1_i    => fs_irq1,     -- IRQ pending

-- External triggers
   fs_trig_i    => fs_trig_i,
   fs_tack_o    => fs_tack_o,

-- memory mnagement i/f
   run0_i       => run0,       -- descriptor #0 running
   run1_i       => run1,       -- descriptor #1 running
   fsc_run_o    => fsc_run,    -- fifo scan running

-- FT FSM i/f
   typ_fscan_o  => typ_fscan,  -- fifo scan type selector
   fpp_init_o   => fpp_init,   -- new (pseudo) descriptor start
   fpp_flush_o  => fpp_flush,  -- flush command
   fpp_open_o   => fpp_open,   -- message open command
   fpp_bcast_o  => fpp_bcast,  -- do broad-cast
   fpp_rst_o    => fpp_rst,    -- reset request
   fpp_done_i   => fpp_done,   -- fifo scan descriptor execution done
   fpp_skip_i   => fpp_skip,   -- fifo scan descriptor skipped
   pp_done_i    => pp_done,    -- (pseudo) descriptor execution done
   pp_err_i     => pp_err,     -- error abort flag

   ftaerr_i     => ftaerr,     -- FT adrs. error flag
   ftderr_i     => ftderr,     -- FT data  error flag
   adrerr_i     => adrerr,     -- source adrs. error flag
   daterr_i     => daterr,     -- data error flag
   timerr_i     => timerr,     -- timeout error flag
   fsc_abrt_o   => fsc_abrt,   -- some error 
   fsc_stat_o   => fsc_stat    -- fifo scan status for broad-casting
);

end generate do_fscan;

-- External status ports
------------------------
d0_stat_o   <= c_dma0_stat;  -- Descriptor #0 status register
d1_stat_o   <= c_dma1_stat;  -- Descriptor #1 status register
dma_stat_o  <= c_dma_stat;   -- Global status register
fsc_stat_o  <= c_fsc_stat;   -- fifo scan status register
fsc_irqs_o  <= c_fsc_irqs;   -- fifo scan IRQ status register
dma_count_o <= "0000" & mif_count(11 downto 0);

end rtl;
