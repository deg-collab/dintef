--------------------------------------------------------------------------
--
-- E-bone Fast Transmitter to (2nd) E-bone master package
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     0.1  18/10/10    herve  Preliminary release
--     1.0  09/02/11    herve  1st release, fifo bug corrected
--     1.1  19/07/11    herve  FIFO(MIF) retry bug fixed
--     1.2  26/06/12    herve  Added status ports and FSM optimized
--     1.3  24/07/12    herve  Added 128 and 256 bits support
--                             Changed GCSR FT size reporting
--     1.4  04/10/12    herve  FT master bus request bug fixed
--                             Exact count on small burst (BRAM only)
--     1.5  14/11/12    herve  Exact count on small burst (all cases)
--     1.6  21/12/12    herve  Added messages, E-bone V1.3 preliminary
--                             Added reset MIF command
--     1.7  11/02/13    herve  unaligned, down-sized data, 
--                             error reporting, E-bone V1.3 revisited
--     2.0  30/05/13    herve  Split in two clock domains
--                             control clock - Fast Transmitter clock
--                             Accurate burst size (in bytes) output
--     2.1  24/10/13    herve  Byte unaligned management
--                             Source now byte addressing
--                             New regs. arrangement (NOT upwards compatible)
--                             Added ft_psize_i input port
--                             New FT signalling
--     2.2  03/02/14    herve  Record i/o, pipelined inputs
--                             Bug fix
--                             Revisited for boundary crossing mngmt
--                             MIF FIFO mngmt moved to DMAC
--                             Added software MIF reset
--     3.0  11/05/15    herve  Added FIFO scan mode
--                             Added memory fifo virtualization
--                             Bug std. FIFO max size fixed
--                             Bug some offset wrong decoding fixed
--                             Bug retry fixed
--     3.1  30/06/16    herve  Added FIFO scan related
--                              - external trigger
--                              - internal timer
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- Declare the components 'ebm_ebft_XX' and 'ebm_ebft'
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.ebs_pkg.all;

package ebm_ebft_pkg is

constant EBS_VERSION : std8 := "00110001"; -- version 3.1

component ebm_ebft_32 is
generic (
   EBS_AD_RNGE  : natural := 12;   -- short adressing range
   EBS_AD_BASE  : natural := 1;    -- usual IO segment
   EBS_AD_SIZE  : natural := 32;   -- size 32 (no fifo scan) or 128
   EBS_AD_OFST  : natural := 0;    -- offset in segment
   EBX_MSG_MID  : natural := 1;    -- message master identifier
   EBFT_DBOUND  : natural := 4096; -- Dest. boundady crossing 2**N (0=none)
   EBFT_DA64    : boolean := true; -- Dest. Adrs. 32/64 bits
   EBFT_MEMS    : boolean := true; -- memory support (or not)
   EBFT_FCLK    : natural := 125;  -- Clock frequency in MHz (for timer)
   EBFT_FSCAN   : natural := 0;    -- FIFO scan desc. number max. 12
   EBFT_ASYNCH  : boolean := false;-- true when different clock domains
   EBFT_ABRQ    : boolean := false -- true for asynchronous arbitration
);
port (
-- E-bone slave interface
   eb_clk_i     : in  std_logic;  -- system control clock
   ebs_i        : in  ebs32_i_typ; 
   ebs_o        : out ebs32_o_typ; 

-- External control ports @eb_clk
   cmd_go_i     : in  std_logic;  -- go command
   cmd_flush_i  : in  std_logic;  -- flush command
   cmd_abort_i  : in  std_logic;  -- abort command
   cmd_reset_i  : in  std_logic;  -- MIF reset command

-- External status ports @eb_clk
   d0_stat_o    : out std32;      -- Descriptor #0 status register
   d1_stat_o    : out std32;      -- Descriptor #1 status register
   fsc_irqs_o   : out std32;      -- fifo scan IRQ status register
   fsc_stat_o   : out std32;      -- fifo scan status register
   dma_stat_o   : out std32;      -- Global status register

-- E-bone Fast Transmitter
   ft_psize_i   : in  std4;       -- FT encoded payload size
   eb_ft_clk_i  : in  std_logic;  -- FT and 2nd E-bone clock
   eb_ft_brq_o  : out std_logic;  -- FT master bus request
   eb_ft_bg_i   : in  std_logic;  -- FT master bus grant
   eb_ft_dxt_o  : out std32;      -- FT data 
   eb_ft_o      : out ebft_typ;   -- FT controls 
   eb_ft_dk_i   : in  std_logic;  -- FT data acknowledge
   eb_ft_err_i  : in  std_logic;  -- FT bus error

-- (2nd) E-bone master interface
   eb2_ft_bg_i  : in  std_logic;  -- busy FT
   eb2_mx_brq_o : out std_logic;  -- bus request
   eb2_mx_bg_i  : in  std_logic;  -- bus grant
   eb2_bmx_i    : in  std_logic;  -- busy some master (but FT)
   eb2_mx_o     : out ebm32_typ;  -- controls
   eb2_m_i      : in ebs32_o_typ; -- master shared

-- (2nd) E-bone extension master 
   ebx2_msg_set_o : out std8;     -- message management
   ebx2_msg_dat_i : in  std16;    -- message data
   ebx2_dsz_i     : in  std4;     -- device data size

-- External triggers
   fs_trig_i    : in  std_logic_vector(11 downto 0);
   fs_tack_o    : out std_logic_vector(11 downto 0);

-- External status ports @eb_ft_clk
   dma_count_o  : out std16;      -- FIFO requested word count
   dma_eot_o    : out std_logic;  -- end of transfer
   dma_err_o    : out std_logic   -- transfer aborted on error
);
end component;

component ebm_ebft_64 is
generic (
   EBS_AD_RNGE  : natural := 12;   -- short adressing range
   EBS_AD_BASE  : natural := 1;    -- usual IO segment
   EBS_AD_SIZE  : natural := 32;   -- size 32 (no fifo scan) or 128
   EBS_AD_OFST  : natural := 0;    -- offset in segment
   EBX_MSG_MID  : natural := 1;    -- message master identifier
   EBFT_DBOUND  : natural := 4096; -- Dest. boundady crossing 2**N (0=none)
   EBFT_DA64    : boolean := true; -- Dest. Adrs. 32/64 bits
   EBFT_MEMS    : boolean := true; -- memory support (or not)
   EBFT_FCLK    : natural := 125;  -- Clock frequency in MHz (for timer)
   EBFT_FSCAN   : natural := 0;    -- FIFO scan desc. number max. 12
   EBFT_ASYNCH  : boolean := false;-- true when different clock domains
   EBFT_ABRQ    : boolean := false -- true for asynchronous arbitration
);
port (
-- E-bone slave interface
   eb_clk_i     : in  std_logic;  -- system control clock
   ebs_i        : in  ebs32_i_typ; 
   ebs_o        : out ebs32_o_typ; 

-- External control ports @eb_clk
   cmd_go_i     : in  std_logic;  -- go command
   cmd_flush_i  : in  std_logic;  -- flush command
   cmd_abort_i  : in  std_logic;  -- abort command
   cmd_reset_i  : in  std_logic;  -- MIF reset command

-- External status ports @eb_clk
   d0_stat_o    : out std32;      -- Descriptor #0 status register
   d1_stat_o    : out std32;      -- Descriptor #1 status register
   fsc_irqs_o   : out std32;      -- fifo scan IRQ status register
   fsc_stat_o   : out std32;      -- fifo scan status register
   dma_stat_o   : out std32;      -- Global status register

-- E-bone Fast Transmitter
   ft_psize_i   : in  std4;       -- FT encoded payload size
   eb_ft_clk_i  : in  std_logic;  -- FT and 2nd E-bone clock
   eb_ft_brq_o  : out std_logic;  -- FT master bus request
   eb_ft_bg_i   : in  std_logic;  -- FT master bus grant
   eb_ft_dxt_o  : out std64;      -- FT data 
   eb_ft_o      : out ebft_typ;   -- FT controls 
   eb_ft_dk_i   : in  std_logic;  -- FT data acknowledge
   eb_ft_err_i  : in  std_logic;  -- FT bus error

-- (2nd) E-bone master interface
   eb2_ft_bg_i  : in  std_logic;  -- busy FT
   eb2_mx_brq_o : out std_logic;  -- bus request
   eb2_mx_bg_i  : in  std_logic;  -- bus grant
   eb2_bmx_i    : in  std_logic;  -- busy some master (but FT)
   eb2_mx_o     : out ebm64_typ;  -- controls
   eb2_m_i      : in ebs64_o_typ; -- master shared

-- (2nd) E-bone extension master 
   ebx2_msg_set_o : out std8;     -- message management
   ebx2_msg_dat_i : in  std16;    -- message data
   ebx2_dsz_i     : in  std4;     -- device data size

-- External triggers
   fs_trig_i    : in  std_logic_vector(11 downto 0);
   fs_tack_o    : out std_logic_vector(11 downto 0);

-- External status ports @eb_ft_clk
   dma_count_o  : out std16;      -- FIFO requested word count
   dma_eot_o    : out std_logic;  -- end of transfer
   dma_err_o    : out std_logic   -- transfer aborted on error
);
end component;

component ebm_ebft_128 is
generic (
   EBS_AD_RNGE  : natural := 12;   -- short adressing range
   EBS_AD_BASE  : natural := 1;    -- usual IO segment
   EBS_AD_SIZE  : natural := 32;   -- size 32 (no fifo scan) or 128
   EBS_AD_OFST  : natural := 0;    -- offset in segment
   EBX_MSG_MID  : natural := 1;    -- message master identifier
   EBFT_DBOUND  : natural := 4096; -- Dest. boundady crossing 2**N (0=none)
   EBFT_DA64    : boolean := true; -- Dest. Adrs. 32/64 bits
   EBFT_MEMS    : boolean := true; -- memory support (or not)
   EBFT_FCLK    : natural := 125;  -- Clock frequency in MHz (for timer)
   EBFT_FSCAN   : natural := 0;    -- FIFO scan desc. number max. 12
   EBFT_ASYNCH  : boolean := false;-- true when different clock domains
   EBFT_ABRQ    : boolean := false -- true for asynchronous arbitration
);
port (
-- E-bone slave interface
   eb_clk_i     : in  std_logic;  -- system control clock
   ebs_i        : in  ebs32_i_typ; 
   ebs_o        : out ebs32_o_typ; 

-- External control ports @eb_clk
   cmd_go_i     : in  std_logic;  -- go command
   cmd_flush_i  : in  std_logic;  -- flush command
   cmd_abort_i  : in  std_logic;  -- abort command
   cmd_reset_i  : in  std_logic;  -- MIF reset command

-- External status ports @eb_clk
   d0_stat_o    : out std32;      -- Descriptor #0 status register
   d1_stat_o    : out std32;      -- Descriptor #1 status register
   fsc_irqs_o   : out std32;      -- fifo scan IRQ status register
   fsc_stat_o   : out std32;      -- fifo scan status register
   dma_stat_o   : out std32;      -- Global status register

-- E-bone Fast Transmitter
   ft_psize_i   : in  std4;       -- FT encoded payload size
   eb_ft_clk_i  : in  std_logic;  -- FT and 2nd E-bone clock
   eb_ft_brq_o  : out std_logic;  -- FT master bus request
   eb_ft_bg_i   : in  std_logic;  -- FT master bus grant
   eb_ft_dxt_o  : out std128;     -- FT data 
   eb_ft_o      : out ebft_typ;   -- FT controls 
   eb_ft_dk_i   : in  std_logic;  -- FT data acknowledge
   eb_ft_err_i  : in  std_logic;  -- FT bus error

-- (2nd) E-bone master interface
   eb2_ft_bg_i  : in  std_logic;  -- busy FT
   eb2_mx_brq_o : out std_logic;  -- bus request
   eb2_mx_bg_i  : in  std_logic;  -- bus grant
   eb2_bmx_i    : in  std_logic;  -- busy some master (but FT)
   eb2_mx_o     : out ebm128_typ; -- controls
   eb2_m_i      : in  ebs128_o_typ;-- master shared

-- (2nd) E-bone extension master 
   ebx2_msg_set_o : out std8;     -- message management
   ebx2_msg_dat_i : in  std16;    -- message data
   ebx2_dsz_i     : in  std4;     -- device data size

-- External triggers
   fs_trig_i    : in  std_logic_vector(11 downto 0);
   fs_tack_o    : out std_logic_vector(11 downto 0);

-- External status ports @eb_ft_clk
   dma_count_o  : out std16;      -- FIFO requested word count
   dma_eot_o    : out std_logic;  -- end of transfer
   dma_err_o    : out std_logic   -- transfer aborted on error
);
end component;

component ebm_ebft_256 is
generic (
   EBS_AD_RNGE  : natural := 12;   -- short adressing range
   EBS_AD_BASE  : natural := 1;    -- usual IO segment
   EBS_AD_SIZE  : natural := 32;   -- size 32 (no fifo scan) or 128
   EBS_AD_OFST  : natural := 0;    -- offset in segment
   EBX_MSG_MID  : natural := 1;    -- message master identifier
   EBFT_DBOUND  : natural := 4096; -- Dest. boundady crossing 2**N (0=none)
   EBFT_DA64    : boolean := true; -- Dest. Adrs. 32/64 bits
   EBFT_MEMS    : boolean := true; -- memory support (or not)
   EBFT_FCLK    : natural := 125;  -- Clock frequency in MHz (for timer)
   EBFT_FSCAN   : natural := 0;    -- FIFO scan desc. number max. 12
   EBFT_ASYNCH  : boolean := false;-- true when different clock domains
   EBFT_ABRQ    : boolean := false -- true for asynchronous arbitration
);
port (
-- E-bone slave interface
   eb_clk_i     : in  std_logic;  -- system control clock
   ebs_i        : in  ebs32_i_typ; 
   ebs_o        : out ebs32_o_typ; 

-- External control ports @eb_clk
   cmd_go_i     : in  std_logic;  -- go command
   cmd_flush_i  : in  std_logic;  -- flush command
   cmd_abort_i  : in  std_logic;  -- abort command
   cmd_reset_i  : in  std_logic;  -- MIF reset command

-- External status ports @eb_clk
   d0_stat_o    : out std32;      -- Descriptor #0 status register
   d1_stat_o    : out std32;      -- Descriptor #1 status register
   fsc_irqs_o   : out std32;      -- fifo scan IRQ status register
   fsc_stat_o   : out std32;      -- fifo scan status register
   dma_stat_o   : out std32;      -- Global status register

-- E-bone Fast Transmitter
   ft_psize_i   : in  std4;       -- FT encoded payload size
   eb_ft_clk_i  : in  std_logic;  -- FT and 2nd E-bone clock
   eb_ft_brq_o  : out std_logic;  -- FT master bus request
   eb_ft_bg_i   : in  std_logic;  -- FT master bus grant
   eb_ft_dxt_o  : out std256;     -- FT data 
   eb_ft_o      : out ebft_typ;   -- FT controls 
   eb_ft_dk_i   : in  std_logic;  -- FT data acknowledge
   eb_ft_err_i  : in  std_logic;  -- FT bus error

-- (2nd) E-bone master interface
   eb2_ft_bg_i  : in  std_logic;  -- busy FT
   eb2_mx_brq_o : out std_logic;  -- bus request
   eb2_mx_bg_i  : in  std_logic;  -- bus grant
   eb2_bmx_i    : in  std_logic;  -- busy some master (but FT)
   eb2_mx_o     : out ebm256_typ; -- controls
   eb2_m_i      : in  ebs256_o_typ;-- master shared

-- (2nd) E-bone extension master 
   ebx2_msg_set_o : out std8;     -- message management
   ebx2_msg_dat_i : in  std16;    -- message data
   ebx2_dsz_i     : in  std4;     -- device data size

-- External triggers
   fs_trig_i    : in  std_logic_vector(11 downto 0);
   fs_tack_o    : out std_logic_vector(11 downto 0);

-- External status ports @eb_ft_clk
   dma_count_o  : out std16;      -- FIFO requested word count
   dma_eot_o    : out std_logic;  -- end of transfer
   dma_err_o    : out std_logic   -- transfer aborted on error
);
end component;

component ebm_ebft 
generic (
   EBS_AD_RNGE  : natural := 12;   -- short adressing range
   EBS_AD_BASE  : natural := 1;    -- usual IO segment
   EBS_AD_SIZE  : natural := 32;   -- size 32 (no fifo scan) or 128
   EBS_AD_OFST  : natural := 0;    -- offset in segment
   EBX_MSG_MID  : natural := 1;    -- message master identifier
   EBFT_DWIDTH  : natural := 64;   -- FT data width
   EBFT_DBOUND  : natural := 4096; -- Dest. boundady crossing 2**N (0=none)
   EBFT_DA64    : boolean := true; -- Dest. Adrs. 32/64 bits
   EBFT_MEMS    : boolean := true; -- memory support (or not)
   EBFT_FCLK    : natural := 125;  -- Clock frequency in MHz (for timer)
   EBFT_FSCAN   : natural := 0;    -- FIFO scan desc. number max. 12
   EBFT_ASYNCH  : boolean := false;-- true when different clock domains
   EBFT_ABRQ    : boolean := false -- true for asynchronous arbitration
);
port (
-- E-bone slave interface
   eb_clk_i     : in  std_logic;  -- system control clock
   ebs_i        : in  ebs32_i_typ; 
   ebs_o        : out ebs32_o_typ; 

-- External control ports @eb_clk
   cmd_go_i     : in  std_logic;  -- go command
   cmd_flush_i  : in  std_logic;  -- flush command
   cmd_abort_i  : in  std_logic;  -- abort command
   cmd_reset_i  : in  std_logic;  -- MIF reset command

-- External status ports @eb_clk
   d0_stat_o    : out std32;      -- Descriptor #0 status register
   d1_stat_o    : out std32;      -- Descriptor #1 status register
   fsc_irqs_o   : out std32;      -- fifo scan IRQ status register
   fsc_stat_o   : out std32;      -- fifo scan status register
   dma_stat_o   : out std32;      -- Global status register

-- E-bone Fast Transmitter
   ft_psize_i   : in  std4;       -- FT encoded payload size
   eb_ft_clk_i  : in  std_logic;  -- FT and 2nd E-bone clock
   eb_ft_brq_o  : out std_logic;  -- FT master bus request
   eb_ft_bg_i   : in  std_logic;  -- FT master bus grant
   eb_ft_dxt_o  : out std_logic_vector(EBFT_DWIDTH-1 downto 0); -- FT data
   eb_ft_o      : out ebft_typ;   -- FT controls 
   eb_ft_dk_i   : in  std_logic;  -- FT data acknowledge
   eb_ft_err_i  : in  std_logic;  -- FT bus error

-- (2nd) E-bone master interface
   eb2_mx_brq_o : out std_logic;  -- bus request
   eb2_mx_bg_i  : in  std_logic;  -- bus grant
   eb2_mx_as_o  : out std_logic;  -- adrs strobe
   eb2_mx_eof_o : out std_logic;  -- end of frame
   eb2_mx_aef_o : out std_logic;  -- almost end of frame
   eb2_mx_dat_o : out std_logic_vector(EBFT_DWIDTH-1 downto 0); -- master data write

-- (2nd) E-bone master shared bus
   eb2_dk_i     : in  std_logic;  -- data acknowledge
   eb2_err_i    : in  std_logic;  -- bus error
   eb2_dat_i    : in  std_logic_vector(EBFT_DWIDTH-1 downto 0); -- master data in
   eb2_bmx_i    : in  std_logic;  -- busy some master (but FT)
   eb2_bft_i    : in  std_logic;  -- busy FT

-- (2nd) E-bone extension master
   ebx2_msg_set_o : out std8;     -- message management
   ebx2_msg_dat_i : in  std16;    -- message data
   ebx2_dsz_i     : in  std4;     -- device data size

-- External triggers
   fs_trig_i    : in  std_logic_vector(11 downto 0);
   fs_tack_o    : out std_logic_vector(11 downto 0);

-- External status ports @eb_ft_clk
   dma_count_o  : out std16;      -- FIFO requested word count
   dma_eot_o    : out std_logic;  -- end of transfer
   dma_err_o    : out std_logic   -- transfer aborted on error
);
end component;

function set_mrw(mems: boolean) return integer;

end package ebm_ebft_pkg;

package body ebm_ebft_pkg is

function set_mrw(mems: boolean) return integer is 
variable tmp : integer;
begin 
   if mems then
      tmp := 12; 
   else
      tmp := 0;
   end if;
   return(tmp);
end set_mrw; 

end package body ebm_ebft_pkg; 
