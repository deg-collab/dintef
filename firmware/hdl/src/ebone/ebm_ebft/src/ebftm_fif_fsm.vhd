--------------------------------------------------------------------------
--
-- DMA controller - Fifo scan
-- Packet pre-processing state machine
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     3.0  11/05/15    herve  Creation for FIFO scan mode
--     3.1  30/06/16    herve  Added FIFO scan flush
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- E-bone Fast Transmitter master i/f
-- Fifo scan management
-- fs_full0/1 : flags half-full/full status has beeen reached
--
-- Single buffer mode states
-- fs_full0  fs_full1    do
--        X         0    increment ptr
--        X         1    wait until irq1=0, reset ptr
--
-- Double buffer (half-full) mode states
-- fs_full0  fs_full1    do
--        0         0    increment ptr
--        1         0    wait until irq1=0, increment ptr 
--        1         1    wait until irq0=0, reset ptr
--        
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;
use work.ebm_ebft_pkg.all;

entity ebftm_fif_fsm is
generic (
   EBFT_FSCAN   : natural := 8   -- nb. of descriptors
);
port (
-- External control port @eb_clk_i
   c_fsc_cmd_i  : in  std32;     -- fifo scan command register

-- Descriptor pre-processing control
   eb_ft_clk_i  : in  std_logic; -- FT system clock
   eb_ft_rst_i  : in  std_logic; -- synchronous system reset
   typ_fvmem_i  : in  std_logic; -- fifo virtualized memory
   fs_sel_o     : out std4;      -- fifo selector
   fs_first_o   : out std_logic; -- new scan starts
   fs_init_o    : out std_logic; -- descriptor counter initialization
   fs_firq_o    : out std_logic; -- force IRQ status
   fs_upcnt_o   : out std_logic; -- descriptor counter update
   fs_tclr_o    : out std_logic; -- internal timer clear
   fs_ald_o     : out std_logic; -- descriptor address reload
   fs_itack_o   : out std_logic; -- internal time out acknowledge
   fs_balign_i  : in  std_logic; -- buffer fillingis payload aligned
   fs_zero_i    : in  std_logic; -- VM 256 word aligned
   fs_b256_i    : in  std_logic; -- VM 256 word still to read
   fs_bcast_i   : in  std_logic; -- request to send IRQ
   fs_finit_i   : in  std_logic; -- force initialization
   fs_valid_i   : in  std_logic; -- descriptor valid
   fs_etrig_i   : in  std_logic; -- external trigger control
   fs_itim_i    : in  std_logic; -- internal time out
   fs_irqtim_i  : in  std_logic; -- internal timer IRQ pending
   fs_rst_i     : in  std_logic; -- fifo reset request
   fs_hmode_i   : in  std_logic; -- half full IRQ mode
   fs_full0_i   : in  std_logic; -- readout buffer went half-full
   fs_irq0_i    : in  std_logic; -- IRQ#0 (half-full) pending
   fs_full1_i   : in  std_logic; -- readout buffer went full
   fs_irq1_i    : in  std_logic; -- IRQ pending

-- External triggers
   fs_trig_i    : in  std_logic_vector(11 downto 0);
   fs_tack_o    : out std_logic_vector(11 downto 0);

-- memory mnagement i/f
   run0_i       : in  std_logic; -- descriptor #0 running
   run1_i       : in  std_logic; -- descriptor #1 running
   fsc_run_o    : out std_logic; -- fifo scan running

-- FT FSM i/f
   typ_fscan_o  : out std_logic; -- fifo scan type selector
   fpp_init_o   : out std_logic; -- new (pseudo) descriptor start
   fpp_flush_o  : out std_logic; -- flush command
   fpp_open_o   : out std_logic; -- message open command
   fpp_bcast_o  : out std_logic; -- do broad-cast
   fpp_rst_o    : out std_logic; -- fifo reset request
   fpp_done_i   : in  std_logic; -- fifo scan descriptor execution done
   fpp_skip_i   : in  std_logic; -- fifo scan descriptor skipped
   pp_done_i    : in  std_logic; -- (pseudo) descriptor execution done
   pp_err_i     : in  std_logic; -- error abort flag

   ftaerr_i     : in  std_logic; -- FT adrs. error flag
   ftderr_i     : in  std_logic; -- FT data  error flag
   adrerr_i     : in  std_logic; -- source adrs. error flag
   daterr_i     : in  std_logic; -- data error flag
   timerr_i     : in  std_logic; -- timeout error flag
   fsc_abrt_o   : out std_logic; -- some error 
   fsc_stat_o   : out std32      -- fifo scan status for broad-casting
);
end ebftm_fif_fsm;

--------------------------------------------------------------------------
architecture rtl of ebftm_fif_fsm is
--------------------------------------------------------------------------
constant UPFSCAN  : natural := Nlog2(EBFT_FSCAN)-1;

signal fs_sel    : std4;      -- fifo selector
signal fsc_cmd   : std32;     -- fifo scan command register, resampled
signal fsc_go    : std_logic; -- go command
signal fsc_go2   : std_logic; 

type state_typ is (iddle,  
                   wait90, wait91, wait92, wait93, wait94,
                   exec0, exec1, exec2, exec2a, exec3a, exec3b, 
                   exec4, exec5, exec6, exec7,
                   exec10, exec11, exec12, exec15, exec16, exec17, exec18,
                   exec20, exec21, exec22, exec23, exec25);
signal state, nextate : state_typ;

signal typ_fscan : std_logic; -- Fifo scan type selector

signal vmcnt    : unsigned(UPFSCAN downto 0); -- "open" virtual fifo counter
signal vmzero   : std_logic; -- virtual fifo transfers all done

signal ffscnt   : unsigned(UPFSCAN downto 0); -- fifo scan counter
signal ffsctop  : unsigned(UPFSCAN downto 0); -- actual nb. of descriptors
signal ffscinc  : std_logic; -- fifo counter increment
signal ffsok    : std_logic; -- fifo counter just updated
signal ffsctc   : std_logic; -- fifo counter terminal count
signal ffsclr   : std_logic; -- fifo counter clear

signal dma_run  : std_logic; -- dma memory running status
signal fsc_run  : std_logic; -- scan running status
signal fsc_lap  : std_logic; -- scan loop toggle indicator

signal run1st   : std_logic; -- force first pass like flag
signal fs_first : std_logic; -- initial go 
signal ffscon   : std_logic; -- start of scan
signal ffscdo   : std_logic; -- restart scan loop
signal ffscoff  : std_logic; -- end of scan
signal domifrq  : std_logic; -- Do MIF read request (else just open message)
signal fpp_flush: std_logic; -- flush pending
signal flsh_tim : std_logic; -- internal timer flush pending

signal fpp_rst  : std_logic; -- fifo reset request
signal fpp_rst1 : std_logic;
signal fpp_rst2 : std_logic;

signal ftaerr   : std_logic; -- FT adrs. error status
signal ftderr   : std_logic; -- FT data  error status
signal adrerr   : std_logic; -- source adrs. error status
signal daterr   : std_logic; -- data error status
signal timerr   : std_logic; -- DMAC desc. #0 timeout error flag
signal err_or   : std_logic; -- ored error status 

signal fpp_init : std_logic; -- new (pseudo) descriptor start
signal fpp_bcast: std_logic; -- broad-cast go flag
signal fs_init  : std_logic; -- packet descriptor initialization
signal fs_upcnt : std_logic; -- descriptor counter update
signal fs_next  : std_logic; -- descriptor next addrs update
signal fs_ald   : std_logic; -- descriptor address reload


------------------------------------------------------------------------
begin
------------------------------------------------------------------------
-- Control clock to FT clock domain crossing
-- or P&R help
--------------------------------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      fsc_cmd   <= c_fsc_cmd_i;
      ffsctop   <= unsigned(fsc_cmd(UPFSCAN downto 0)) - 1;
   end if;
end process;

-- Memorize internal timer flush request
-- common to all descriptors
----------------------------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if eb_ft_rst_i = '1' OR ffscoff = '1' then
         flsh_tim <= '0';
      elsif ffscdo = '1' AND fs_itim_i = '1' then
         flsh_tim <= '1';
      end if;
   end if;
end process;

-- Memorize external trigger (or timer) flush request 
-- for the current descriptor
-- IF destination buffer is payload aligned
-- force flushing out any size;
-- ELSE 
-- flushing still requires a 1st packet of wsmall size to get aligned 
-- and the external trigger is masked off.
--------------------------------------------------------------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if fs_first = '1' then
         fpp_flush <= '0';
      elsif ffscon = '1' OR ffsok = '1' then
         if     fs_balign_i = '1'      -- ignore until aligned in buffer
            AND typ_fvmem_i = '0' then -- trig/timer do NOT impact virtual fifo

            if (fs_etrig_i = '1' AND fs_trig_i(to_integer(ffscnt)) = '1')
               OR flsh_tim = '1' then
               fpp_flush <=  '1';
            else
               fpp_flush <= '0';
            end if;
         end if;
      end if;
   end if;
end process;

fpp_flush_o <= fpp_flush; 

-- Acknowledge flush pending request
------------------------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      fs_tack_o <= (others => '0'); -- single clock pulse
      if fs_first = '1' then
         fs_tack_o <= (others => '1');
      elsif fpp_bcast = '1' then
         fs_tack_o(to_integer(ffscnt)) <= '1';
      end if;
   end if;
end process;

-- Go for fifo scanning
-----------------------
process(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if fsc_cmd(31) = '1' 
         AND NOT (unsigned(fsc_cmd(UPFSCAN downto 0)) = 0) then
         fsc_go <= '1';
      else
         fsc_go <= '0';
      end if;

      fsc_go2   <= fsc_go; -- rising edge detector
      fs_tclr_o <= eb_ft_rst_i OR (fsc_go AND NOT fsc_go2);
   end if;
end process;

-- fifo scan lap toggling flag
------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if eb_ft_rst_i = '1' then
         fsc_lap <= '0';
      elsif ffsclr = '1' then
         fsc_lap <= NOT fsc_lap;
      end if;
   end if;
end process;

-- fifo scan counter
--------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if ffsclr = '1' OR ffscon = '1' OR eb_ft_rst_i = '1' then
         ffscnt <= (others => '0');
      elsif ffscinc = '1' then
         ffscnt <= ffscnt + 1;
      end if;
   end if;
end process;

ffsctc <= '1' when ffscnt >= ffsctop  -- terminal count
           else '0';

fs_sel   <= std_logic_vector(resize(ffscnt, 4));
fs_sel_o <= fs_sel;

-- Current byte count processing pipeline delay
----------------------------------------------- 
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      fs_ald    <= fs_init OR fs_next OR fpp_rst;
      fpp_rst1  <= fpp_rst;
      fpp_rst2  <= fpp_rst1;
      fpp_rst_o <= fpp_rst2;
   end if;
end process;

fs_ald_o    <= fs_ald;
fpp_init_o  <= fpp_init;
fs_init_o   <= fs_init;
fs_upcnt_o  <= fs_upcnt;
fsc_run_o   <= fsc_run;
typ_fscan_o <= typ_fscan;

-- Tell open command for virtualized memories
---------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if fs_b256_i = '1' AND fs_upcnt = '1' then
         domifrq <= '0';
      elsif   eb_ft_rst_i = '1' 
           OR (fs_zero_i = '1' AND fs_ald = '1')
           OR fs_init = '1' then
         domifrq <= '1';
      end if;
   end if;
end process;

-- Count on-going "open" virtualized memories
-- This is used for delaying the timer broad-cast
-------------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if fs_first = '1' then
         vmcnt <= (others => '0') ;
      elsif typ_fvmem_i = '1' AND fs_init = '1' then   -- open
         vmcnt <= vmcnt + 1 ;
      elsif typ_fvmem_i = '1' AND fpp_bcast = '1' then -- close
         vmcnt <= vmcnt - 1 ;
      end if;

      if vmcnt = to_unsigned(0, vmcnt'LENGTH) then
         vmzero <= '1';
      else
         vmzero <= '0';
      end if;
   end if;
end process;

-- Packet pre-processor FSM
---------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if eb_ft_rst_i = '1' then
         fsc_run <= '0';       
         run1st  <= '0';       
         ftaerr  <= '0';       
         ftderr  <= '0';       
         adrerr  <= '0';       
         daterr  <= '0';       
         timerr  <= '0';       
         err_or  <= '0';       
         typ_fscan <= '0';       
         state     <= iddle;
      else
         fsc_run   <= (ffscon OR fsc_run)
                    AND NOT ffscoff;   
         run1st    <= (fs_first OR run1st)  
                    AND NOT ffscoff; 
         ftaerr    <= ((ftaerr_i AND fsc_run) OR ftaerr)
                    AND NOT ffscon;
         ftderr    <= ((ftderr_i AND fsc_run) OR ftderr)
                    AND NOT ffscon;
         adrerr    <= ((adrerr_i AND fsc_run) OR adrerr)
                    AND NOT ffscon;
         daterr    <= ((daterr_i AND fsc_run) OR daterr)
                    AND NOT ffscon;
         timerr    <= ((timerr_i AND fsc_run) OR timerr)
                    AND NOT ffscon;
         err_or    <= ((pp_err_i AND fsc_run) OR err_or)
                    AND NOT ffscon;
         typ_fscan <= (ffscon OR typ_fscan)
                    AND NOT ffscoff;   
         state     <= nextate;            
      end if;
   end if;
end process;

dma_run   <= run0_i OR run1_i;

process(state, dma_run, 
        fsc_go, run1st, ffsctc, domifrq, fpp_flush, flsh_tim, vmzero, fs_irqtim_i,
        fs_finit_i, fs_valid_i, fs_rst_i, fs_bcast_i, typ_fvmem_i, fs_b256_i, 
        fs_hmode_i, fs_full0_i, fs_irq0_i, fs_full1_i, fs_irq1_i,
        pp_err_i, pp_done_i, fpp_done_i, fpp_skip_i)
begin
   nextate   <= state; -- stay in same state
   fs_first  <= '0';
   ffsclr    <= '0';
   ffscon    <= '0';
   ffscdo    <= '0';
   ffscoff   <= '0';
   ffscinc   <= '0';
   ffsok     <= '0';
   fs_init   <= '0';
   fs_upcnt  <= '0';
   fs_next   <= '0';
   fpp_init  <= '0';
   fpp_open_o <= '0';
   fsc_abrt_o <= '0';
   fs_firq_o  <= '0';
   fpp_rst    <= '0';
   fpp_bcast  <= '0';

   case state is 
      when iddle =>                  -- sleeping
         if dma_run = '0' then       -- wait until memory done
            if fsc_go = '1' then
               fs_first <= '1';
               nextate  <= exec0;
            end if;
         end if;

      when exec0 =>                  -- start new scan list 
         ffscon   <= '1';
         nextate  <= exec1;

      when exec1 =>                  -- packet parameter processing
         if fs_valid_i = '0' then    -- descriptor NOT validated
            nextate <= exec15;   
    
         elsif fs_rst_i = '1' then
            fpp_rst  <= '1';         -- reset fifo
            nextate  <= exec5;

         elsif     run1st = '1'            -- force first like pass init
                OR fs_finit_i = '1'        -- not valid to valid transition

            OR (   fs_hmode_i = '0'        -- single buffer mode
               AND fs_full1_i = '1'        -- buffer was full
               AND fs_irq1_i  = '0')       -- and emptied

            OR (   fs_hmode_i = '1'        -- double buffer mode
               AND fs_full0_i = '1'        -- buffer was half-full
               AND fs_full1_i = '1'        -- buffer was full
               AND fs_irq0_i  = '0')       -- 1st halve read out done
                                    then  
            fs_init   <= '1';              -- restart from buffer origin
            nextate   <= exec2;

         elsif (   fs_hmode_i = '0'        -- single buffer mode
               AND fs_full1_i = '0'        -- buffer NOT full
               AND fs_irq1_i  = '0')       -- read out done

            OR (   fs_hmode_i = '1'        -- double buffer mode
               AND fs_full0_i = '0'        -- buffer free
               AND fs_full1_i = '0')

            OR (   fs_hmode_i = '1'        -- double buffer mode
               AND fs_full0_i = '1'        -- buffer was half-full
               AND fs_full1_i = '0'        -- but not yet full
               AND fs_irq1_i  = '0')       -- 2nd halve read out done
                                    then  
            fs_next  <= '1';               -- fill the buffer
            nextate  <= exec2;

         else                        -- buffer is full, IRQ still pending
            nextate <= exec15; 
         end if;
 
      when exec2 =>  
         if    typ_fvmem_i = '1' then  -- virtualized memory
            nextate <= exec3a;
         elsif fpp_flush = '1' then    -- flushing out
            nextate <= exec2a;
         else
            fpp_init <= '1';         -- packet parameter processing done: can execute
            nextate  <= exec4;
         end if;

      when exec2a =>                 -- flushing out
         fpp_open_o <= '1';          -- get fifo occupancy
         nextate <= exec4;
 
      when exec3a =>                 -- delay until fs_b256 processing is OK
         nextate <= exec3b;

      when exec3b =>                 -- 256 word boundary processing done: can execute
         if domifrq = '1' then 
            fpp_init <= '1';         -- actual MIF request 
         else
            fpp_open_o <= '1';       -- just read out a payload from virtualization fifo
         end if;
         nextate <= exec4;

      when exec4 =>                  -- packet execution
         if pp_err_i = '1' then      -- packet cancelled on error
            nextate <= wait90;          
         elsif fpp_skip_i = '1' then -- done, no data actually moved

            if fpp_flush = '1' then
               fs_firq_o <= '1';     -- force irq status, anyway
            end if;

            if fpp_flush = '1' AND flsh_tim = '0' then
               nextate  <= exec7;    -- always broad-cast on trigger flushing
            else
               nextate  <= exec6;   
            end if;
         elsif fpp_done_i = '1' then -- packet done OK
            fs_upcnt <= '1';         -- update buffer occupancy
            nextate  <= exec5; 
         end if;

      when exec5 =>                  -- check if broad-cast required
         if    (fpp_flush = '1' AND flsh_tim = '0')
            OR fs_bcast_i = '1' then  
            nextate  <= exec7; 
         else
            nextate  <= exec6; 
         end if;

      when exec6 => 
         if pp_done_i = '1' then     -- wait for MIF message closing
            nextate <= exec15; 
         end if;
                 
      when exec7 =>                  -- buffer full or flushing out
         if pp_done_i = '1' then     -- wait for MIF message closing
            nextate <= exec10; 
         end if;
                 
      when exec10 =>                 -- leave time for FT to go iddle
         nextate   <= exec11; 
        
      when exec11 =>                 -- FT broad-cast IRQ 
         fpp_bcast <= '1'; 
         nextate   <= exec12; 
        
      when exec12 =>                 -- wait for broad-cast done  
         if pp_done_i = '1' then
            nextate <= exec15; 
         end if;

      when exec15 =>                 -- check for scan end 
         if ffsctc = '1' then
            ffscoff <= '1';
            if flsh_tim = '1' then   -- when flushing on timer
               if vmzero = '1' then  
                  nextate <= exec16; -- bcast is delayed until the end of scan loop
               else
                  nextate <= exec22; -- bcast is delayed until all virtual fifo done
               end if;
            else
               nextate <= exec22; 
            end if;
         else                     
            ffscinc <= '1';          -- switch to next fifo in the list
            nextate <= exec20; 
         end if;
        
      when exec16 =>                 -- FT broad-cast timout IRQ 
         fpp_bcast <= '1'; 
         nextate   <= exec17; 
        
      when exec17 =>                 -- wait for broad-cast done  
         if pp_done_i = '1' then
            nextate <= exec18; 
         end if;

      when exec18 =>                 -- scan paused until timer IRQ acknowledged  
         ffsok   <= '1';             -- to clear fpp_flush
         if fs_irqtim_i = '0' then
            nextate <= exec22;       -- knows at end of scan
         end if;

      when exec20 =>                 -- processing time 
         nextate <= exec21; 
        
      when exec21 =>                 -- processing time 
         ffsok   <= '1';
         nextate <= exec1;           -- loop back to next fifo
        
      when exec22 =>                 -- end of scan 
         ffsclr  <= '1';
         nextate <= exec23; 
        
      when exec23 =>                 -- leave enough time 
         nextate <= exec25;          -- for DMAC memory to start
                
      when exec25 =>                 -- scan pause
         if dma_run = '0' then       -- wait until memory done
            if fsc_go = '1' then     -- for restarting new scan loop
               ffscdo  <= '1';       -- for timer sampling
               nextate <= exec0;
            else
               nextate <= iddle;
            end if;
         end if;
        
      when wait90 =>               -- clear the GO bit 
         fsc_abrt_o <= '1';
         nextate <= wait91;

      when wait91 =>               -- delay until GO bit cleared
         fsc_abrt_o <= '1';
         nextate <= wait92;

      when wait92 =>               -- delay until GO bit cleared
         fsc_abrt_o <= '1';
         nextate <= wait93;

      when wait93 =>               -- delay until GO bit (asynch.) cleared
         nextate <= wait94;

      when wait94 =>               -- delay until GO bit (asynch.) cleared
         nextate <= iddle;

   end case;

end process;

fs_first_o  <= fs_first;
fs_itack_o  <= flsh_tim AND ffscoff;
fpp_bcast_o <= fpp_bcast;

-- Status outputs and store broad-cast data
-------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      fsc_stat_o <= fsc_run & err_or & '0' & daterr & adrerr & ftaerr & ftderr & timerr & 
                    GND8 & GND4 & 
                    fpp_flush & "00" & fsc_lap & 
                    std_logic_vector(to_unsigned(EBFT_FSCAN, 4)) &
                    fs_sel; 
   end if;
end process;

end rtl;
