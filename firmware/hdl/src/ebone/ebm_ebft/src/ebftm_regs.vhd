--------------------------------------------------------------------------
--
-- DMA controller - slave registers i/f
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     0.1  18/10/10    herve  Preliminary release
--     1.0  11/04/11    herve  Updated to E-bone rev. 1.2
--     2.1  24/10/13    herve  New regs. arrangement
--     2.2  30/10/13    herve  Record i/o, pipelined inputs
--     3.0  11/05/15    herve  Added FIFO scan mode
--                             Removed memory related regs. when useless
--                             Bug some offset wrong decoding fixed
--     3.1  30/06/16    herve  Added FIFO scan flush
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- E-bone slave registers stack for E-bone bridge control
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;
use work.ebm_ebft_pkg.all;

entity ebftm_regs is
generic (
   EBS_AD_RNGE  : natural := 12;  -- short adressing range
   EBS_AD_BASE  : natural := 1;   -- usual IO segment
   EBS_AD_SIZE  : natural := 32;  -- size 32 (no fifo scan) or 128
   EBS_AD_OFST  : natural := 0;   -- offset in segment
   EBFT_MEMS    : boolean := true;-- memory support (or not)
   EBFT_FSCAN   : natural := 0    -- FIFO scan desc. number
);
port (
-- E-bone slave interface
   eb_clk_i     : in  std_logic;  -- system clock
   ebs_i        : in  ebs32_i_typ; 
   ebs_o        : out ebs32_o_typ; 

-- DMAC registers and control
   dma_regs_o   : out std32_a;    -- memory descriptors
   dma0_stat_i  : in  std32;      -- DMA #0 status 
   dma1_stat_i  : in  std32;      -- DMA #1 status
   dma_stat_i   : in  std32;      -- DMA status

-- FSCAN registers and control
   fsc_itack_i  : in  std_logic;  -- internal time out acknowledge
   fsc_cmd_o    : out std32;      -- fifo scan command register
   fsc_irqofs_o : out std4;       -- IACK register offset
   fsc_irqwr0_o : out std_logic;  -- IACK#0 write
   fsc_irqwr1_o : out std_logic;  -- IACK#1 write
   fsc_mwr_o    : out std_logic;  -- mem. desc. write enable
   fsc_madrs_o  : out std8;       -- mem. desc. address
   fsc_mdati_o  : out std32;      -- mem. desc. data in
   fsc_mdato_i  : in  std32;      -- mem. desc. data out
   fsc_sreg_i   : in  std32_a(EBFT_FSCAN-1 downto 0);
   fsc_irqs_i   : in  std32;      -- IRQ status
   -- @ft_clk
   fsc_stat_i   : in  std32;      -- fifo scan status
   fsc_abrt_i   : in  std_logic   -- some error 
);
end ebftm_regs;

--------------------------------------------------------------------------
architecture rtl of ebftm_regs is
--------------------------------------------------------------------------
constant REGS_RG  : natural := Nlog2(EBS_AD_SIZE); 
constant UPRANGE  : natural := REGS_RG-1; -- offset up range (MSbit is overflow)

constant MRW_SIZE : integer := set_mrw(EBFT_MEMS);
subtype dma_regs_typ is std32_a(MRW_SIZE-1 downto 0); -- memory descriptors
signal rw_regs   : dma_regs_typ := (others => (others => '0'));

type state_typ is (iddle, adrs, wr1, rd0, rd1, bcc1, abort);
signal state, nextate : state_typ;

signal dk        : std_logic; -- slave addrs & data acknowledge
signal s_dk      : std_logic; -- acknowledge registered
signal oerr      : std_logic; -- offset error
signal myself    : std_logic; -- slave selected
signal eof       : std_logic; -- slave end of frame
signal load      : std_logic; -- register stack load
signal ofld      : std_logic; -- offset counter load
signal ofen      : std_logic; -- offset counter enable
signal ofst      : unsigned(REGS_RG downto 0); -- offset counter in burst
signal rmux      : std32;
signal dma0_done : std_logic; -- DMA done flag
signal dma0_rund : std_logic; -- DMA running delayed
signal dma1_done : std_logic; -- DMA done flag
signal dma1_rund : std_logic; -- DMA running delayed

signal fsc_en    : std_logic; -- fifo scan desc. memory enable
signal fsc_mwr   : std_logic; -- mem. desc. write enable
signal fsc_done  : std_logic; -- fifo scan stops
signal fsc_irqwr : std_logic; -- IACK write
signal fsc_wadrs : std8;      -- write mem. desc. address
signal fsc_stat  : std32;     -- fifo scan status
signal fsc_cmd   : std32;     -- fifo scan command register

signal eb_rst_i  : std_logic;
signal eb_bmx_i  : std_logic;
signal eb_as_i   : std_logic;
signal eb_eof_i  : std_logic;
signal eb_dat_i  : std32;

signal eb_bmx   : std_logic; -- busy some master, P&R help
signal eb_bcc   : std_logic; -- E-bone broad call starts
signal eb_mine  : std_logic; -- E-bone burst addressing me starts
signal eb_ofst  : std_logic_vector(EBS_AD_RNGE-1 downto 0);

alias eb_bar  : std_logic_vector(1 downto 0) 
                is eb_dat_i(29 downto 28); 
alias eb_amsb : std_logic_vector(EBS_AD_RNGE-1 downto UPRANGE+1) 
                is eb_dat_i(EBS_AD_RNGE-1 downto UPRANGE+1);
alias dma0_run: std_logic is dma0_stat_i(31);
alias dma1_run: std_logic is dma1_stat_i(31);

------------------------------------------------------------------------
begin
------------------------------------------------------------------------
-- Clock domain crossing
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then 
      fsc_stat <= fsc_stat_i;
   end if;
end process;

eb_rst_i <= ebs_i.eb_rst;
eb_bmx_i <= ebs_i.eb_bmx;
eb_as_i  <= ebs_i.eb_as;
eb_eof_i <= ebs_i.eb_eof;
eb_dat_i <= ebs_i.eb_dat;

-- Address decode input pipeline
--------------------------------
eb_ofst <= std_logic_vector(to_unsigned(EBS_AD_OFST, EBS_AD_RNGE));
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then 
      eb_bmx  <= eb_bmx_i;          -- P&R help
      eb_bcc  <= '0';
      eb_mine <= '0';
      if    eb_bmx_i   = '1'        -- burst start
        AND eb_as_i = '1' then      -- adrs
         if eb_eof_i = '1' then     -- broad-cast/call
            eb_bcc <= '1';
         elsif (   eb_bar  = std_logic_vector(to_unsigned(EBS_AD_BASE, 2))
               AND eb_amsb = eb_ofst(EBS_AD_RNGE-1 downto UPRANGE+1)) then
            eb_mine <= '1';         -- that's me
         end if;
      end if;
   end if;
end process;

-- E-bone slave FSM
-------------------
process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      if eb_rst_i = '1' then
         state  <= iddle;
         myself <= '0';  
      else
         myself <= (ofld OR myself)       -- set and lock
                   AND NOT (eof OR oerr); -- clear
         state  <= nextate;
      end if;
   end if;
end process;

process(state, eb_as_i, eb_eof_i, eb_dat_i, 
               eb_bmx, eb_bcc, eb_mine)
begin
   nextate <= state; -- stay in same state
   load    <= '0';
   ofld    <= '0';
   ofen    <= '0';
   dk      <= '0';
   eof     <= '0';

   case state is 
      when iddle =>                    -- sleeping
         if eb_bcc = '1' then
            nextate <= bcc1;
         elsif eb_mine = '1' then
            ofld    <= '1';            -- store offset
            if NOT (eb_dat_i(31) = '1' 
                    AND eb_dat_i(UPRANGE) = '1') then -- early ACK
               dk      <= '1';         -- BUT fifo desc. read is pipelined!
            end if;
            nextate <= adrs;
         end if;

      when adrs =>                     -- pipelined slave addrs ack (routing help)
            if eb_dat_i(31) = '1' then -- read or write?
               if eb_dat_i(UPRANGE) = '1' then
                  nextate <= rd0;      -- fifo descriptor pipeline delay
               else
                  dk      <= '1';      -- early ack burst
                  ofen    <= '1';      -- pipeline early init
                  nextate <= rd1;
               end if;
            else
               dk      <= '1';         -- early ack burst
               nextate <= wr1;
            end if;

      when wr1 =>                     -- burst write
         if eb_bmx = '0' then         -- abort
            nextate <= abort;
         else                         -- wait until addressing done
            dk   <= NOT eb_eof_i;     -- ack continues till end of burst

            if  eb_as_i = '0' then    -- store data
               load  <= '1';
               ofen  <= '1';
               if eb_eof_i = '1' then
                  eof <= '1';
                  nextate <= iddle;
               end if;
            end if;
         end if;

      when rd0 =>                     -- fifo descriptor burst read
         dk      <= '1';
         ofen    <= '1';
         nextate <= rd1;

      when rd1 =>                     -- burst read
         if eb_bmx = '0' then         -- abort
            nextate <= abort;
         else   
            dk   <= NOT eb_eof_i;     -- ack continues till end of burst
            if eb_eof_i = '1' then
               eof <= '1';
               nextate <= iddle;
            else
               ofen  <= '1';
            end if;
         end if;

      when bcc1 =>                    -- wait until bus released
         if eb_bmx = '0' OR eb_eof_i = '1' then
            eof <= '1';
            nextate <= iddle;
         end if;

      when abort =>                   -- quit on abort
         eof <= '1';
         nextate <= iddle;

   end case;

end process;

-- Offset counter
-----------------
process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      if ofld = '1' then      -- offset load
         ofst <= unsigned('0' & eb_dat_i(REGS_RG-1 downto 0));
      elsif ofen = '1' then
         ofst <= ofst + 1; -- increment
      end if;
   end if;
end process;

oerr   <= ofst(REGS_RG)   AND ofen; -- count overflow
fsc_en <= ofst(REGS_RG-1) AND ofen; -- upper half addressing is FSCAN memory

-- End of run detectors
-----------------------
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      dma0_rund <= dma0_run;
      dma0_done <= eb_rst_i OR
                   (dma0_rund AND NOT dma0_run); -- running falling edge
      dma1_rund <= dma1_run;
      dma1_done <= eb_rst_i OR
                   (dma1_rund AND NOT dma1_run); -- running falling edge
      fsc_done  <= eb_rst_i OR fsc_abrt_i;       -- fifo scan error/abort
   end if;
end process;

-- Core register stack writing in
---------------------------------
-- memory descriptors
mwr: if EBFT_MEMS generate 
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      if load = '1' then
         for i in rw_regs'RANGE loop 
            if to_integer(ofst) = i then
               rw_regs(i) <= eb_dat_i;
            end if;
         end loop;
      end if;

      if dma0_done = '1' then -- self clear the go bit
         rw_regs(5)(31) <= '0';
      end if;
      if dma1_done = '1' then -- self clear the go bit
         rw_regs(5+6)(31) <= '0';
      end if;
   end if;
end process;
dma_regs_o <= rw_regs; -- internal to port
end generate mwr;

-- fifo scan command register
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      if eb_rst_i = '1' then
         fsc_cmd <= (others => '0');
      elsif load = '1' then
         if to_integer(ofst) = 12 then
            fsc_cmd <= eb_dat_i;
         end if;
      end if;

      if fsc_itack_i = '1' then  -- self clear flush bit
         fsc_cmd(29) <= '0';
      end if;
      if fsc_done = '1' then     -- self clear the go bit
         fsc_cmd(31) <= '0';
      end if;
   end if;
end process;
fsc_cmd_o <= fsc_cmd; -- internal to port

-- Registered output multiplexor
--------------------------------
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      for i in rw_regs'RANGE loop       -- mem. regs.
         if to_integer(ofst) = i then
            rmux <= rw_regs(i);
         end if;
      end loop;

      if to_integer(ofst) = 12 then     -- fscan command reg.
         rmux <= fsc_cmd;
      end if;

      if to_integer(ofst) = 13 then     -- GSR
         rmux <= dma_stat_i;
      end if;

      if to_integer(ofst) = 14 then     -- #0 Status read only
         rmux <= dma0_stat_i;
      end if;

      if to_integer(ofst) = 15 then     -- #1 Status read only
         rmux <= dma1_stat_i;
      end if;

      for i in EBFT_FSCAN-1+16 downto 16 loop -- FSCAN status regs.
         if to_integer(ofst) = i then
            rmux <= fsc_sreg_i(i-16);
         end if;
      end loop;

      if to_integer(ofst) = 30 then           -- FSCAN error status
         rmux <= fsc_stat;
      end if;

      if to_integer(ofst) = 31 then           -- FSCAN global IRQ status
         rmux <= fsc_irqs_i;
      end if;

      if fsc_en = '1' then                    -- FSCAN desc. memory read
         rmux <= fsc_mdato_i;
      end if;

   end if;
end process;

-- FSCAN descriptor memory write pipeline (P&R help)
----------------------------------------------------
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      fsc_mwr     <= fsc_en AND load;
      fsc_wadrs   <= std_logic_vector(resize(ofst(REGS_RG-2 downto 0), 8));
      fsc_mdati_o <= eb_dat_i;
   end if;
end process;

fsc_mwr_o   <= fsc_mwr;
fsc_madrs_o <= fsc_wadrs when load = '1' OR fsc_mwr = '1' else
               std_logic_vector(resize(ofst(REGS_RG-2 downto 0), 8));

fsc_irqwr    <= load AND ofst(4) AND NOT ofst(5);
fsc_irqwr0_o <= fsc_irqwr AND eb_dat_i(30);
fsc_irqwr1_o <= fsc_irqwr AND eb_dat_i(31);
fsc_irqofs_o <= std_logic_vector(ofst(3 downto 0));

-- E-bone slave drivers
-----------------------
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      s_dk  <= dk AND NOT oerr;
   end if;
end process;

ebs_o.eb_err <= '0';
ebs_o.eb_dk  <= s_dk  when myself = '1' else '0';
ebs_o.eb_dat <= rmux  when myself = '1' else (others =>'0');

end rtl;

