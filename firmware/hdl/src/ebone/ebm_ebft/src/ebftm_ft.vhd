--------------------------------------------------------------------------
--
-- DMA controller - Destination Fast Transmitter (FT) master
-- Burst length and address counters
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     2.1  10/09/13    herve  Extracted from FSM
--                             Unaligned management to the last byte
--                             Source now byte addressing
--                             New regs. arrangement (NOT upwards compatible)
--                             Added ft_psize_i input port
--                             New FT signalling
--     2.2  03/02/14    herve  Bug fixes (from latest 2.1)
--                             Revisited for boundary crossing mngmt
--                             MIF FIFO mngmt moved to DMAC
--     3.0  30/06/15    herve  Added FIFO scan mode
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone.
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

entity ebftm_ft is
generic (
   EBFT_DWIDTH  : natural := 64   -- FT data width 
);
port (
-- E-bone Fast Transmitter
   eb_ft_clk_i  : in  std_logic;  -- system clock
   eb_ft_eof_i  : in  std_logic;  -- FT end of frame
   ft_psize_i   : in  std4;       -- FT encoded payload size
   ebrq_i       : in  std_logic;  -- E-bone request
   eb_ft_blen_o : out std16;      -- FT burst length in bytes

-- Burst management i/f
   mif_adrs_o   : out std32;      -- MIF starting address
   mif_count_o  : out std16;      -- MIF word count
   mx_adrs_o    : out std32;      -- Source E-bone starting address

-- Fscan packet processor i/f 
   typ_fscan_i  : in  std_logic;  -- fifo scan type selector
   fs_open2_i   : in  std_logic;  -- FIFO occupancy is valid
   fs_b256_i    : in  std_logic;  -- force MIF count to 256 words
   fs_srce_i    : in  std32;      -- source location
   fs_sadrs_i   : in  std32;      -- source address (VM)
   fs_dalsb_i   : in  std32;      -- dest. address LSbits
   fs_bcnt_i    : in  std_logic_vector(23 downto 0); -- byte count

-- Mem. packet processor i/f 
   typ_mif_i    : in  std_logic;  -- MIF type selector
   typ_fifo_i   : in  std_logic;  -- FIFO memory type selector
   dx_srce_i    : in  std32;      -- source location
   dx_sadrs_i   : in  std32;      -- source address
   dx_dalsb_i   : in  std32;      -- dest. address LSbits
   dx_bcnt_i    : in  std_logic_vector(23 downto 0); -- byte count

-- MIF fifo management
   remcnt_o     : out unsigned(23 downto 0); -- remaining count in words
   ft_wsize_o   : out unsigned(7 downto 0);  -- FT payload size in words

-- FSM i/f
   firstword_i  : in  std_logic;  -- flag 1st word (in 1st elementary burst)
   dsize_i      : in  std4;       -- early size for count processing (CHCH)
   wsize_i      : in  std4;       -- slave size memorized for burst
   b1st_o       : out std8;       -- 1st word MSbytes count (0=all) 

   bcld_i       : in  std_logic;  -- ebone burst counter load
   bcen_i       : in  std_logic;  -- ebone burst counter enable
   bcazero_o    : out std_logic;  -- burst counter almost zero flag
   bczero_o     : out std_logic;  -- burst counter zero flag

   fcld_i       : in  std_logic;  -- fifo counter load
   fcen_i       : in  std_logic;  -- fifo counter enable
   fczero_o     : out std_logic;  -- fifo counter zero flag

   mald_i       : in  std_logic;  -- mem. address counter load
   maen_i       : in  std_logic;  -- mem. address counter enable
   daclr_i      : in  std_logic;  -- dest. counter clear
   daen_i       : in  std_logic;  -- dest. counter enable
   daendall_o   : out std_logic;  -- dest. end flag
   daendm1_o    : out std_logic;  -- dest. end flag minus 1
   daendm2_o    : out std_logic;  -- dest. end flag minus 2
   daendm3_o    : out std_logic;  -- dest. end flag minus 3
   dupdate_o    : out std_logic;  -- dest. address update valid
   daburst_o    : out std32       -- dest. address current burst
);
end ebftm_ft;

------------------------------------------------------------------------
architecture rtl of ebftm_ft is
------------------------------------------------------------------------
constant EBFT_DW8   : natural := EBFT_DWIDTH/8;
constant EBFT_DWLOG : natural := Nlog2(EBFT_DWIDTH/16);

signal psize    : std16;                 -- payload size in bytes
signal ft_wsize : unsigned(7 downto 0);  -- FT payload size in words
signal xx_wcnt  : unsigned(11 downto 0); -- fifo word size
signal xx_bcnt  : unsigned(23 downto 0); -- actual byte count
signal fs_open3 : std_logic;  -- FIFO occupancy is valid, delayed
signal ebrq2    : std_logic;  -- E-bone request, delayed

signal bcnt     : unsigned(7 downto 0);  -- ebone burst size counter 

signal fcnt     : unsigned(11 downto 0); -- fifo size counter 
signal mafif    : std_logic_vector(27 downto 0); -- fifo E-bone offset
signal typ_afif : std_logic;  -- any fifo type

signal dabase   : unsigned(31 downto 0); -- dest. address origin
signal daburst  : unsigned(31 downto 0); -- dest. address current burst
signal dacnt    : unsigned(23 downto 0); -- dest. counter
signal dactop   : unsigned(dacnt'RANGE); -- count max. in full word 
signal remcnt   : unsigned(dacnt'RANGE); -- remaining count to move
signal remsmall : std_logic;             -- remaining small burst
signal rnlbcnt  : unsigned(EBFT_DWLOG+1 downto 0); -- remaining (NOT alg) last byte count
signal ralbcnt  : unsigned(EBFT_DWLOG downto 0);   -- remaining last byte count
signal eb_ft_end: std_logic;  -- store elementary burst info
signal still_cnt: unsigned(dacnt'RANGE); -- current burst byte counter 
signal min_cnt  : unsigned(15 downto 0); -- adjusted first burst count 

signal b1st     : unsigned(EBFT_DWLOG downto 0); -- 1st word MSbytes count (0=all) 
signal bofst    : unsigned(EBFT_DWLOG downto 0); -- 1st word offset (0=aligned) 
signal macnt    : unsigned(27 downto 0);         -- mem. address counter 
signal maseg    : std_logic_vector(3 downto 0);  -- mem. addrs segment 
signal eb_ft_blen : std16;     -- FT burst length in bytes, internal

------------------------------------------------------------------------
begin
------------------------------------------------------------------------
-- E-bone FT burst counter
-- Payload size in bytes is translated to FT words
--------------------------------------------------
process(ft_psize_i)
begin
   case ft_psize_i is
      when "0000" => psize <= "0000000010000000"; -- 128
                     ft_wsize <= to_unsigned((128/EBFT_DW8), 8);

      when "0001" => psize <= "0000000100000000"; -- 256
                     ft_wsize <= to_unsigned((256/EBFT_DW8), 8);

      when "0010" => psize <= "0000001000000000"; -- 512
                     ft_wsize <= to_unsigned((512/EBFT_DW8), 8);

      when "0011" => psize <= "0000010000000000"; -- 1024
                     ft_wsize <= to_unsigned((1024/EBFT_DW8), 8);

      when "0100" => psize <= "0000100000000000"; -- 2048
                     ft_wsize <= to_unsigned((2048/EBFT_DW8), 8);

      when "0101" => psize <= "0001000000000000"; -- 4096
                     ft_wsize <= to_unsigned((4096/EBFT_DW8), 8);

      when others => psize <= "0001000000000000"; -- 4096
                     ft_wsize <= to_unsigned((4096/EBFT_DW8), 8);
   end case;
end process;
ft_wsize_o <= ft_wsize;

process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if bcld_i = '1' then
         bcnt <= unsigned(psize(EBFT_DWLOG+1+7 downto EBFT_DWLOG+1)); 
      elsif bcen_i = '1' then
         bcnt <= bcnt - 1; 
      end if;
   end if;
end process;

bcazero_o <= '1' when bcnt = "00000010" else '0'; -- almost end flag
bczero_o  <= '1' when bcnt = "00000001" else '0'; -- end flag


-- FIFO burst counter
-- Note: mif_count_o(15 downto 12) could be used for additional MIF control
---------------------------------------------------------------------------
xx_wcnt <=      resize(ft_wsize, 12) when typ_fscan_i = '1'
           else unsigned(dx_srce_i(27 downto 16));

xx_bcnt <=      unsigned(fs_bcnt_i) when typ_fscan_i = '1'
           else unsigned(dx_bcnt_i);

process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if remcnt < resize(xx_wcnt, remcnt'HIGH+1) then 
         remsmall <= '1';    -- last burst is smaller than FIFO size
      else
         remsmall <= '0'; 
      end if;

      if fcld_i = '1' then      -- load fifo readout depth 
         if remsmall = '1' then -- last burst is smaller than FIFO size
            fcnt        <= remcnt(11 downto 0); 
            mif_count_o <= GND4 & std_logic_vector(remcnt(11 downto 0));
         elsif fs_b256_i = '1' then               -- fifo virtualized memory
            fcnt        <= xx_wcnt;               -- still payload burst
            mif_count_o <= GND4 & "000100000000"; -- fill fifo ahead with 256 words
         else
            fcnt        <= xx_wcnt;
            mif_count_o <= GND4 & std_logic_vector(xx_wcnt); 
         end if;

      elsif fcen_i = '1' then
         fcnt <= fcnt - 1; -- running
      end if;

      if fcnt = "000000000000" then
         fczero_o <= '1';
      else
         fczero_o <= '0';
      end if;

   end if;
end process;

---------------------------------------------------
-- Initialize parameters from new descriptor
---------------------------------------------------
-- E-bone source segment
------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      typ_afif <= typ_fscan_i OR typ_fifo_i;
      if mald_i = '1' then
         if    typ_afif = '1' then  -- get source FIFO E-bone offset
            maseg <= "0001";        -- force segment #1
         else                       -- Dumb BRAM
            maseg <= "00" & dx_srce_i(29 downto 28); -- memory segment 
         end if;
      end if;
   end if;
end process;

-- E-bone FIFO offset
------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if mald_i = '1' then
         if typ_fscan_i = '1' then
            mafif <= "000000000000" & fs_srce_i(15 downto 0);
         else
            mafif <= "000000000000" & dx_srce_i(15 downto 0);
         end if;
      end if;
   end if;
end process;

-- Source address counter
-- Byte address is tranlated into FT word address
-------------------------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if mald_i = '1' then   -- load source offset
         if typ_fscan_i = '1' then
            bofst <= (others => '0'); -- fifo scan is word aligned
         else
            bofst <= unsigned(dx_sadrs_i(EBFT_DWLOG downto 0));
         end if;

         if typ_fscan_i = '1' then  -- fifo virtualization, WORD offset
            macnt <= unsigned(fs_sadrs_i(27 downto 0));
         elsif typ_mif_i = '1' then -- standard mode, byte offset
            macnt <= resize(unsigned(dx_sadrs_i(31 downto EBFT_DWLOG+1)), 28);
         else
            macnt <=   unsigned(dx_srce_i(27 downto 0))  -- E-bone word base adrs.
                       -- add byte displacement tranlated into word
                     + resize(unsigned(dx_sadrs_i(27 downto EBFT_DWLOG+1)), 28);
         end if;

      elsif maen_i = '1' then
         macnt <= macnt + 1; -- running
      end if;

      if fcld_i = '1' then   -- load MIF offset
         mif_adrs_o <= "0000" & std_logic_vector(macnt); 
      end if;

   end if;
end process;

-- Number of MSbytes to keep in first word when un-aligned
----------------------------------------------------------
b1st   <= EBFT_DW8 - bofst; 
b1st_o <= std_logic_vector(resize(b1st, 8));

---------------------------------------------------
-- Update word count (actually moved) counter
-- and current burst destination address
---------------------------------------------------

mx_adrs_o <= maseg & mafif when typ_afif = '1'  else 
             maseg & std_logic_vector(macnt);

process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      eb_ft_end <= eb_ft_eof_i ; -- FT end pulse
      dupdate_o <= eb_ft_end;
   end if;
end process;

process	(eb_ft_clk_i) 
begin
   if rising_edge(eb_ft_clk_i) then
      if    daclr_i = '1' 
         OR fs_open2_i = '1' then 
         dacnt  <= (others => '0'); -- zero transmitted for now
      elsif daen_i = '1' then
         dacnt <= dacnt + 1;        -- running
      end if;
         
      if daclr_i = '1' then         -- dest. adrs. 
         if typ_fscan_i = '1' then
            daburst <= unsigned(fs_dalsb_i); 
         else
            daburst <= unsigned(dx_dalsb_i); 
         end if;
      elsif eb_ft_end = '1' then    -- next burst dest. adrs. 
         daburst <= daburst + resize(unsigned(eb_ft_blen), 32); 
      end if;
   end if;
end process;

daburst_o <= std_logic_vector(daburst);

------------------------
-- Byte count management
------------------------

-- Remaining byte count when aligned
-- Note: Fscan is always aligned!
------------------------------------
ralbcnt  <= xx_bcnt(EBFT_DWLOG downto 0); 

-- Remaining byte count when NOT aligned
-- Note: Fscan is never un-aligned!
----------------------------------------
rnlbcnt  <= resize(bofst, EBFT_DWLOG+2) + resize(unsigned(dx_bcnt_i(EBFT_DWLOG downto 0)), EBFT_DWLOG+2); 

-- Determine count to be moved in full word unit
------------------------------------------------
process	(eb_ft_clk_i, dsize_i, xx_bcnt) 
variable shft_cnt: integer range 5 downto 2;
variable wcnt    : unsigned(dacnt'RANGE);

begin
   -- use only 2 LSbits to limit the shifter (hw implementation) size
   -- resize shift count from byte unit to word unit
   shft_cnt := to_integer(unsigned(dsize_i(1 downto 0)))+2;
   wcnt     := SHIFT_RIGHT(xx_bcnt, shft_cnt);

   if rising_edge(eb_ft_clk_i) then

      fs_open3 <= fs_open2_i;

      if    daclr_i = '1' 
         OR fs_open3 = '1' then 

       if b1st = to_unsigned(0, b1st'HIGH+1) then -- source is aligned
          if ralbcnt = to_unsigned(0, ralbcnt'HIGH+1) then -- exact full word count
             dactop <= wcnt;       -- so rounded count is ok
          else                     -- some more bytes
             dactop <= wcnt + 1;   -- move one more word
          end if;

       else                        -- source is NOT aligned
         if rnlbcnt > EBFT_DW8 then 
            dactop <= wcnt + 2;    -- compensate for start unaligned plus end overflow
         else  
            dactop <= wcnt + 1;    -- compensate for start unaligned
         end if;
       end if;

      end if;
   end if;
end process;

-- FSCAN note: 
-- Aligned; dacnt = 0 ; therefore remcnt = word(xx_bcnt) = word(fs_bcnt_i)

remcnt   <= dactop - dacnt; -- remaining word count still to be moved
remcnt_o <= remcnt;

-- End of full block move detector
----------------------------------
process	(eb_ft_clk_i)
begin
   if rising_edge(eb_ft_clk_i) then
      if dacnt < (dactop - 3)  then
         daendm3_o <= '0';
      else
         daendm3_o <= '1';
      end if;

      if dacnt < (dactop - 2)  then
         daendm2_o <= '0';
      else
         daendm2_o <= '1';
      end if;

      if dacnt < (dactop - 1)  then
         daendm1_o <= '0';
      else
         daendm1_o <= '1';
      end if;

      if dacnt < dactop  then
         daendall_o <= '0';
      else
         daendall_o <= '1';
      end if;
  end if;
end process;

-- Manage current burst byte count
----------------------------------
process(eb_ft_clk_i, wsize_i, psize, bofst) 
variable sub_cnt: unsigned(dacnt'RANGE);
variable pld_cnt: unsigned(dacnt'RANGE);
variable sft_cnt: integer range 3 downto 0;

begin
      -- Asynchronous variables
      -- determine nb. of bytes per word
      case wsize_i(1 downto 0) is
         when   "00" => sub_cnt := to_unsigned( 4, 24);
                        case EBFT_DWIDTH is
                           when  32 => sft_cnt := 0;
                           when  64 => sft_cnt := 1;
                           when 128 => sft_cnt := 2;
                           when 256 => sft_cnt := 3;
                           when others => sft_cnt := 0;
                        end case;
         when   "01" => sub_cnt := to_unsigned( 8, 24);
                        case EBFT_DWIDTH is
                           when  64 => sft_cnt := 0;
                           when 128 => sft_cnt := 1;
                           when 256 => sft_cnt := 2;
                           when others => sft_cnt := 0;
                        end case;
         when   "10" => sub_cnt := to_unsigned(16, 24);
                        case EBFT_DWIDTH is
                           when 128 => sft_cnt := 0;
                           when 256 => sft_cnt := 1;
                           when others => sft_cnt := 0;
                        end case;
         when   "11" => sub_cnt := to_unsigned(32, 24);
                        sft_cnt := 0;
         when others => sub_cnt := to_unsigned( 4, 24);
                        sft_cnt := 0;
      end case;

      -- Determine the payload size as a function of actual data
      -- When full size sft_cnt=0
      pld_cnt := resize(SHIFT_RIGHT(unsigned(psize), sft_cnt), dacnt'HIGH+1);
      min_cnt <= pld_cnt(15 downto 0) - resize(bofst, 16); -- compensate missing bytes in 1st word of 1st burst

   if rising_edge(eb_ft_clk_i) then

      ebrq2 <= ebrq_i;

      -- update byte count still to be transmitted
      if    daclr_i = '1' 
         OR fs_open3 = '1' then 
         still_cnt <= xx_bcnt; -- total byte count to transmit
      elsif daen_i = '1' then  -- running
         if firstword_i = '1' AND NOT (b1st = 0) then
            still_cnt <= still_cnt - resize(b1st, still_cnt'HIGH+1) ; 
         else
            still_cnt <= still_cnt - sub_cnt; 
         end if;
      end if;

      -- process byte count per burst
      if ebrq2 = '1' then 
         if still_cnt >=  pld_cnt then -------------------------------- remaining is larger than payload 
            if firstword_i = '1' then
               eb_ft_blen <= std_logic_vector(min_cnt);              -- missing bytes in 1st word
            else
               eb_ft_blen <= std_logic_vector(pld_cnt(15 downto 0)); -- burst size is payload size
            end if;
         else --------------------------------------------------------- remaining is smaller than payload 
            if firstword_i = '1' then  
               if b1st = to_unsigned(0, b1st'HIGH+1) then            -- source is aligned
                  eb_ft_blen <= std_logic_vector(still_cnt(15 downto 0));  
               else                                                  -- source NOT aligned
                  if (still_cnt(15 downto 0) + resize(bofst, 16)) > pld_cnt(15 downto 0) then
                     eb_ft_blen <= std_logic_vector(min_cnt);        -- does not fit in single payload, 2nd burst will follow 
                  else
                     eb_ft_blen <= std_logic_vector(still_cnt(15 downto 0)); -- fits in this single burst
                  end if;
               end if;
            else
               eb_ft_blen <= std_logic_vector(still_cnt(15 downto 0)); -- short burst at end
            end if;
         end if;
      end if;

   end if;
end process;

eb_ft_blen_o <= eb_ft_blen; -- internal to port 

end rtl;
