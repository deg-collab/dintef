--------------------------------------------------------------------------
--
-- E-bone - Flash SPI interface
-- SER/DES top module
--
--------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.ebs_pkg.all;

library unisim;
use unisim.vcomponents.BUFG;
use unisim.vcomponents.BUFR;

entity fspi_ser_des is
generic (
  BUFR_DIVIDE     : STRING := "4";
  TRGT_DESC       : NATURAL := 16#00#;
	DWIDTH          : NATURAL := 32;
  ADEPTH          : NATURAL := 128;
	sys_w           : INTEGER := 1;
  dev_w           : INTEGER := 8
);
port (
  sys_rst         : in std_logic;
  sys_clk         : in std_logic;

	wr_en_ack       : in std_logic;
	wr_dat_reg      : in std_logic_vector(DWIDTH - 1 downto 0);
	wd_cnt          : in std_logic_vector(DWIDTH - 1 downto 0);
	wd_size         : in std_logic_vector(DWIDTH - 1 downto 0);
	edge_nb         : out std_logic_vector(DWIDTH - 1 downto 0);
	ser_stat        : out std_logic_vector(DWIDTH - 1 downto 0);
  par_stat        : out std_logic_vector(DWIDTH - 1 downto 0);

  fspi_cs         : out std_logic;
	fspi_sck        : out std_logic;
	fspi_mosi       : out std_logic;
  fspi_miso       : in std_logic
);
end fspi_ser_des;

------------------------------------------------------------------------
architecture rtl of fspi_ser_des is
------------------------------------------------------------------------

component fspi_fifo_a                                                     -- Hardware Description Language asynchronous fifo
generic(
  DWIDTH    : natural;                                                    -- data width
  ADEPTH    : natural;                                                    -- FIFO depth
  RAMTYP    : string := "auto"                                            -- "auto", "block", "distributed"
);
port (
  arst      : in std_logic;                                               -- asyn reset
  wr_clk    : in std_logic;                                               -- write clock
  wr_en     : in std_logic;                                               -- write enable
  wr_dat    : in std_logic_vector(DWIDTH - 1 downto 0);
  wr_cnt    : out std_logic_vector(Nlog2(ADEPTH) - 1 downto 0);
  wr_afull  : out std_logic;                                              -- almost full flag
  wr_full   : out std_logic;                                              -- full flag

  rd_clk    : in std_logic;                                               -- read clock
  rd_en     : in std_logic;                                               -- read enable
  rd_dat    : out std_logic_vector(DWIDTH - 1 downto 0);
  rd_cnt    : out std_logic_vector(Nlog2(ADEPTH) - 1 downto 0);
  rd_aempty : out std_logic;                                              -- almost empty flag
  rd_empty  : out std_logic                                               -- empty flag
);
end component;

component fspi_ser
generic (
  sys_w                 : integer := 1;
  dev_w                 : integer := 8
);
port (
-- From the device out to the system
  DATA_OUT_FROM_DEVICE  : in std_logic_vector(dev_w - 1 downto 0);
  DATA_OUT_TO_PINS      : out std_logic_vector(sys_w - 1 downto 0);
  CLK_TO_PINS           : out std_logic;

-- Clock and reset signals
  CLK_IN                : in std_logic;
  CLK_DIV_OUT           : out std_logic;
  CLK_RESET             : in std_logic;
  IO_RESET              : in std_logic
);
end component;

component fspi_des
generic (
  DWIDTH                : natural := 32;
  sys_w                 : integer := 1;
  dev_w                 : integer := 8
);
port (
-- From the system into the device
  DATA_IN_FROM_PINS     : in std_logic_vector(sys_w - 1 downto 0);
  DATA_IN_TO_DEVICE     : out std_logic_vector(DWIDTH - 1 downto 0);
  BITSLIP               : in std_logic;
 
-- Clock and reset signals
  CLK_IN                : in std_logic;
  CLK_DIV_IN            : in std_logic;
  CLK_RESET             : in std_logic;
  IO_RESET              : in std_logic
);
end component;

type page_state_type    is (PAGE_BEGIN, PAGE_RD_ON, PAGE_RD_OFF, PAGE_CS, PAGE_WAIT, PAGE_END);
signal page_state       : page_state_type;

type wr_en_gate_type    is (CSG0, CSG1, CSG2);
signal wr_en_gate_state : wr_en_gate_type;

type cs_wait_type       is (CSW0, CSW1, CSW2, CSW3, CSW4, CSW5, CSW6, CSW7, CSW8, CSW9);
signal cs_wait_state    : cs_wait_type;

signal wr_full       : std_logic;
signal wr_cnt        : std_logic_vector(Nlog2(ADEPTH) - 1 downto 0);
signal wr_dat        : std_logic_vector(DWIDTH - 1 downto 0);
signal wr_en         : std_logic;
signal rd_en         : std_logic;
signal rd_en_a       : std_logic;
signal rd_dat        : std_logic_vector(DWIDTH - 1 downto 0);
signal rd_empty      : std_logic;
signal rd_empty_s    : std_logic;
signal my_wr_full    : std_logic;

signal ser_cs        : std_logic;
signal ser_cs_s      : std_logic;
signal ser_clk       : std_logic;
signal spi_clk       : std_logic;
signal sys_clk_div   : std_logic_vector(1 downto 0);
signal byte_miso     : std_logic_vector(dev_w - 1 downto 0);
signal byte_wr_en    : std_logic_vector(dev_w - 1 downto 0);
signal word_rd_en    : std_logic_vector(DWIDTH - 1 downto 0);
signal ser_dat       : std_logic_vector(sys_w - 1 downto 0);
signal cs_wait       : std_logic;
signal wr_en_gate    : std_logic;

signal int_cnt       : std_logic_vector(4 downto 0);
signal int_cnt_cl    : std_logic;
signal int_cnt_ce    : std_logic;
signal int_cnt_tc    : std_logic;
signal int_cnt_ac    : std_logic;

signal edge_cnt      : std_logic_vector(11 downto 0);

signal correct_cnt   : std_logic;
signal edge_cnt_err  : std_logic;
signal wd_cnt_null   : std_logic;
signal wd_cnt_page   : std_logic;
signal wd_double     : std_logic;
signal first_loop    : std_logic;

signal fspi_miso_int : std_logic_vector(sys_w - 1 downto 0);
signal fspi_cs_int   : std_logic;
signal fspi_cs_int_v : std_logic_vector(1 downto 0);
signal fspi_mosi_int : std_logic;

signal Uns_wr_cnt    : unsigned(Nlog2(ADEPTH) - 1 downto 0);
signal Uns_edge_cnt  : unsigned(11 downto 0);

-----
begin
-----

gene_wr_dat : for i in wr_dat'range generate
  wr_dat(i) <= wr_dat_reg(31 - i);
end generate;

Uns_wr_cnt   <= unsigned(wr_cnt);
Uns_edge_cnt <= unsigned(edge_cnt);

rd_en       <= '1' when ((rd_en_a = '1') or (int_cnt_ac = '1')) else '0';
wd_double   <= '1' when ((unsigned(wd_size) = (DWIDTH * 2)) and (first_loop = '0')) else '0';
wd_cnt_null <= '1' when (unsigned(wd_cnt) = 0) else '0';
wd_cnt_page <= '1' when (unsigned(wd_cnt) > 1) else '0';
my_wr_full  <= '1' when ((Uns_wr_cnt >= unsigned(wd_cnt)) and (wd_cnt_null = '0')) else '0';
correct_cnt <= '1' when ((Uns_edge_cnt = (to_unsigned((DWIDTH / 4), 12))) or
                         (Uns_edge_cnt = (to_unsigned((DWIDTH / 2), 12))) or
												 (Uns_edge_cnt = (to_unsigned(DWIDTH, 12))) or
												 (Uns_edge_cnt = (to_unsigned((DWIDTH * 2), 12))) or
												 (Uns_edge_cnt = (to_unsigned((DWIDTH * 65), 12)))) else '0';

fspi_sck         <= ser_clk;
fspi_cs          <= fspi_cs_int;
fspi_miso_int(0) <= fspi_miso;
fspi_mosi        <= fspi_mosi_int;

int_cnt_ac <= '1' when ((int_cnt = "11110") and (wd_cnt_page = '1')) else '0';
int_cnt_tc <= '1' when (((int_cnt = "11111") and ((unsigned(wd_size)) = to_unsigned((DWIDTH * 2), 32))) or
                        ((int_cnt = "11111") and ((unsigned(wd_size)) = to_unsigned(DWIDTH, 32))) or
                        ((int_cnt = "01111") and ((unsigned(wd_size)) = to_unsigned((DWIDTH / 2), 32))) or
                        ((int_cnt = "00111") and ((unsigned(wd_size)) = to_unsigned((DWIDTH / 4), 32)))) else '0';

with int_cnt(4 downto 3) select
  byte_wr_en <= rd_dat(dev_w - 1 downto 0)                   when "00",
                rd_dat((dev_w + 8) - 1 downto dev_w)         when "01",
                rd_dat((dev_w + 16) - 1 downto (dev_w + 8))  when "10",
						    rd_dat((dev_w + 24) - 1 downto (dev_w + 16)) when "11",
                (others => '0')                              when others;

bufg_sp6_gene : if (TRGT_DESC = 16#60#) generate

  bufg_inst : BUFG
  port map (
    O => spi_clk,
    I => sys_clk_div(1)
  );

end generate bufg_sp6_gene;

bufr_vx6_gene : if (TRGT_DESC = 16#63#) generate

  bufr_inst : BUFR
  generic map (
    SIM_DEVICE  => "VIRTEX6",
    BUFR_DIVIDE => BUFR_DIVIDE
  )
  port map (
    O           => spi_clk,
    CE          => '1',
    CLR         => '0',
    I           => sys_clk
  );

end generate bufr_vx6_gene;

bufr_kx7_gene : if (TRGT_DESC = 16#71#) or (TRGT_DESC = 16#72#) or (TRGT_DESC = 16#73#) generate

  bufr_inst : BUFR
  generic map (
    SIM_DEVICE  => "7SERIES",
    BUFR_DIVIDE => BUFR_DIVIDE
  )
  port map (
    O           => spi_clk,
    CE          => '1',
    CLR         => '0',
    I           => sys_clk
  );

end generate bufr_kx7_gene;

cor_ser : fspi_ser

generic map (
  sys_w                 => sys_w,
  dev_w                 => dev_w
)

port map (
  DATA_OUT_FROM_DEVICE  => byte_wr_en,
  DATA_OUT_TO_PINS      => ser_dat,
  CLK_TO_PINS           => ser_clk,

  CLK_IN                => spi_clk,
  CLK_DIV_OUT           => open,
  CLK_RESET             => sys_rst,
  IO_RESET              => ser_cs
);

cor_des : fspi_des

generic map (
  DWIDTH                => DWIDTH,
  sys_w                 => sys_w,
  dev_w                 => dev_w
)

port map (
  DATA_IN_FROM_PINS     => fspi_miso_int,
  DATA_IN_TO_DEVICE     => word_rd_en,
  BITSLIP               => '0',

  CLK_IN                => spi_clk,
  CLK_DIV_IN            => '0',
  CLK_RESET             => sys_rst,
  IO_RESET              => wr_en_ack
);

fifo_i : fspi_fifo_a

generic map (
  DWIDTH    => DWIDTH,
  ADEPTH    => ADEPTH,
  RAMTYP    => "block"
)

port map (
  arst      => sys_rst,
  wr_clk    => sys_clk,
  wr_en     => wr_en,
  wr_dat    => wr_dat,
  wr_cnt    => wr_cnt,
  wr_afull  => open,
  wr_full   => wr_full,

  rd_clk    => spi_clk,
  rd_en     => rd_en,
  rd_dat    => rd_dat,
  rd_cnt    => open,
  rd_aempty => open,
  rd_empty  => rd_empty
);

process(sys_rst, spi_clk)
begin
  if (sys_rst = '1') then
    fspi_cs_int_v <= (others => '0');
  elsif (spi_clk'event and spi_clk = '1') then
    fspi_cs_int_v(0) <= fspi_cs_int;
    fspi_cs_int_v(1) <= fspi_cs_int_v(0);
  end if;
end process;

process(sys_rst, spi_clk)
begin
	if (sys_rst = '1') then

    rd_en_a      <= '0';
		ser_cs       <= '1';
		int_cnt_ce   <= '0';
		int_cnt_cl   <= '1';
		first_loop   <= '0';
		edge_cnt_err <= '0';
    page_state   <= PAGE_BEGIN;

  elsif (spi_clk'event and spi_clk = '1') then
    case (page_state) is

      when PAGE_BEGIN =>
			  if ((my_wr_full = '1') and (rd_empty = '0')) then
				  page_state <= PAGE_RD_ON;
				end if;

      when PAGE_RD_ON =>
			  rd_en_a    <= '1';
			  page_state <= PAGE_RD_OFF;

      when PAGE_RD_OFF =>
			  rd_en_a    <= '0';
				page_state <= PAGE_CS;

      when PAGE_CS =>
			  ser_cs     <= '0';
				int_cnt_ce <= '1';
				int_cnt_cl <= '0';
        page_state <= PAGE_WAIT;

      when PAGE_WAIT =>
			  if ((int_cnt_tc = '1') and (rd_empty_s = '1') and (wd_cnt_page = '0') and (wd_double = '0')) then
				  ser_cs     <= '1';
					int_cnt_ce <= '0';
          page_state <= PAGE_END;
			  elsif ((int_cnt_tc = '1') and (rd_empty_s = '1') and (wd_cnt_page = '0') and (wd_double = '1')) then
				  first_loop <= '1';
				  page_state <= PAGE_WAIT;
				elsif ((int_cnt_tc = '1') and (rd_empty_s = '0') and (wd_cnt_page = '1')) then
				  page_state <= PAGE_WAIT;
				elsif ((int_cnt_tc = '1') and (rd_empty_s = '1') and (wd_cnt_page = '1')) then
				  ser_cs     <= '1';
				  int_cnt_ce <= '0';
				  page_state <= PAGE_END;
				else
				  page_state <= PAGE_WAIT;
				end if;

      when PAGE_END =>
				if (correct_cnt = '1') then
					edge_cnt_err <= '0';
				else
					edge_cnt_err <= '1';
			  end if;
				first_loop <= '0';
				int_cnt_cl <= '1';
				page_state <= PAGE_BEGIN;

			when others =>
			  rd_en_a      <= '0';
				ser_cs       <= '1';
				int_cnt_ce   <= '0';
				int_cnt_cl   <= '1';
				first_loop   <= '0';
				edge_cnt_err <= '0';
        page_state   <= PAGE_BEGIN;

		end case;
	end if;
end process;

process(sys_rst, spi_clk)
begin
	if (sys_rst = '1') then
	  rd_empty_s    <= '1';
	  fspi_cs_int   <= '1';
		fspi_mosi_int <= '0';
  elsif (spi_clk'event and spi_clk = '1') then
	  rd_empty_s    <= rd_empty;
    fspi_cs_int   <= ser_cs;
    fspi_mosi_int <= ser_dat(0);
	end if;
end process;

process(sys_rst, spi_clk, int_cnt_cl)
begin
	if ((sys_rst = '1') or (int_cnt_cl = '1')) then
		int_cnt     <= (others => '0');
		edge_cnt    <= (others => '0');
  elsif (spi_clk'event and spi_clk = '1') then
	  if (int_cnt_ce = '1') then
		  int_cnt  <= int_cnt + 1;
			edge_cnt <= edge_cnt + 1;
		end if;
	end if;
end process;

process(sys_rst, sys_clk)
begin
  if (sys_rst = '1') then
	  wr_en    <= '0';
		ser_cs_s <= '1';
	elsif (sys_clk'event and sys_clk = '1') then
	  wr_en    <= (wr_en_ack and ser_cs_s);
		ser_cs_s <= ser_cs;
	end if;
end process;

process(sys_rst, sys_clk)
begin
  if (sys_rst = '1') then
		edge_nb     <= (others => '0');
		ser_stat    <= (others => '0');
		par_stat    <= (others => '0');
	elsif (sys_clk'event and sys_clk = '1') then
	  if (ser_cs_s = '0') then
      edge_nb(11 downto 0) <= edge_cnt;
		end if;
		if ((fspi_cs_int_v(0) = '1') and (fspi_cs_int_v(1) = '0')) then
		  par_stat <= word_rd_en;
		end if;
		ser_stat(30) <= wr_en_gate and cs_wait and fspi_cs_int and fspi_cs_int_v(0) and fspi_cs_int_v(1);
		ser_stat(31) <= edge_cnt_err;
	end if;
end process;

process(sys_clk)
begin
  if rising_edge(sys_clk) then
    sys_clk_div <= sys_clk_div + 1;
  end if;
end process;

process(sys_clk)
begin
  if rising_edge(sys_clk) then
	  if (sys_rst = '1') then
		  wr_en_gate       <= '1';
			wr_en_gate_state <= CSG0;
		else
		  case (wr_en_gate_state) is
			  when CSG0 =>
				  if (wr_en_ack = '1') then
					  wr_en_gate       <= '0';
            wr_en_gate_state <= CSG1;
					end if;
				when CSG1 =>
				  if (cs_wait = '0') then
				    wr_en_gate_state <= CSG2;
					else
					  wr_en_gate_state <= CSG1;
					end if;
				when CSG2 =>
				  wr_en_gate       <= '1';
          wr_en_gate_state <= CSG0;
				when others =>
				  wr_en_gate       <= '1';
          wr_en_gate_state <= CSG0;
			end case;
		end if;
	end if;
end process;

process(sys_rst, spi_clk)
begin
	if (sys_rst = '1') then
		cs_wait       <= '1';
    cs_wait_state <= CSW0;
  elsif (spi_clk'event and spi_clk = '1') then
		case (cs_wait_state) is
			when CSW0 =>
				if (wr_en_gate = '0') then
					cs_wait       <= '0';
          cs_wait_state <= CSW1;
				end if;
		  when CSW1 =>
				cs_wait_state <= CSW2;
			when CSW2 =>
				cs_wait_state <= CSW3;
			when CSW3 =>
				cs_wait_state <= CSW4;
			when CSW4 =>
				cs_wait_state <= CSW5;
			when CSW5 =>
				cs_wait_state <= CSW6;
			when CSW6 =>
				cs_wait_state <= CSW7;
			when CSW7 =>
				cs_wait_state <= CSW8;
			when CSW8 =>
				cs_wait_state <= CSW9;
			when CSW9 =>
				cs_wait       <= '1';
				cs_wait_state <= CSW0;
			when others =>
				cs_wait       <= '1';
        cs_wait_state <= CSW0;
    end case;
  end if;
end process;

end rtl;
