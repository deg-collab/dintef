--------------------------------------------------------------------------
--
-- E-bone - Flash SPI interface top file
--
--------------------------------------------------------------------------
--
-- Version  Date       Author    Comment
-- 1.1      22/10/12   Le Caer
-- 1.2      26/03/13   Le Caer   Updated for TYPE_DESC parameter
-- 2.0      30/09/13   herve     Record i/o, pipelined inputs
-- 2.1      18/12/13   Le Caer   Version (two nibbles) status offset 7
-- 2.2      26/03/14   Le Caer   spi clock edge and period parameters
-- 2.3      12/08/14   Le Caer   Version new parameter BUFR_DIVIDE
-- 2.4      17/09/15   Le Caer   End of Startup, active High output signal, from STARTUP module
-- 2.5      05/09/16   Le Caer   Status offset7 bit-31 = End of Startup
-- 2.6      15/02/18   herve     Removed useless interrupt logic
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- Register (32 bits) stack  
-- EBS_AD_SIZE   = Total register number, range 4
--
-- Mapping from bottom (zero offset) to the top
-- Read & write registers
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

library unisim;
use unisim.vcomponents.ODDR2;
use unisim.vcomponents.STARTUP_VIRTEX6;
use unisim.vcomponents.STARTUPE2;

entity ebs_fspi is
generic (
  USE_RS_EDGE  : INTEGER := 0;                            -- User CCLK falling edge when 0 (default)
  BUFR_DIVIDE  : STRING  := "4";                          -- EB_CLK frequency divisor for SPI flash memory clock
  TYPE_DESC    : NATURAL := 16#00000000#;                 -- Type description target and/or application dependant
  EBS_AD_RNGE  : NATURAL := 12;                           -- short adressing range
  EBS_AD_BASE  : NATURAL := 1;                            -- usual IO segment
  EBS_AD_SIZE  : NATURAL := 8;                            -- size in segment
  EBS_AD_OFST  : NATURAL := 0;                            -- offset in segment
  EBS_MIRQ     : std16   := (others => '0')               -- Message IRQ (not used)
);
port (
-- E-bone interface
  eb_clk_i     : in std_logic;                            -- system clock
  ebs_i        : in ebs32_i_typ;
  ebs_o        : out ebs32_o_typ;

-- SPI interface 
  fspi_cs_o    : out std_logic;                           -- SPI slave select (active Low)
  fspi_sck_o   : out std_logic;                           -- SPI clock
  fspi_mosi_o  : out std_logic;                           -- SPI master out slave in
  fspi_miso_i  : in std_logic;                            -- SPI master in slave out

  fpga_eos_o   : out std_logic                            -- End of Startup. Active High output
);
end ebs_fspi;

--------------------------------------
architecture rtl of ebs_fspi is
--------------------------------------

component fspi_ser_des
generic (
  BUFR_DIVIDE     : STRING := "4";
  TRGT_DESC       : NATURAL := 16#00#;
	DWIDTH          : NATURAL := 32;
  ADEPTH          : NATURAL := 128;
	sys_w           : INTEGER := 1;
  dev_w           : INTEGER := 8
);
port (
  sys_rst         : in std_logic;
  sys_clk         : in std_logic;

	wr_en_ack       : in std_logic;
	wr_dat_reg      : in std_logic_vector(DWIDTH - 1 downto 0);
	wd_cnt          : in std_logic_vector(DWIDTH - 1 downto 0);
	wd_size         : in std_logic_vector(DWIDTH - 1 downto 0);
  edge_nb         : out std_logic_vector(DWIDTH - 1 downto 0);
	ser_stat        : out std_logic_vector(DWIDTH - 1 downto 0);
	par_stat        : out std_logic_vector(DWIDTH - 1 downto 0);

  fspi_cs         : out std_logic;
	fspi_sck        : out std_logic;
	fspi_mosi       : out std_logic;
  fspi_miso       : in std_logic
);
end component;

function get_trgt_desc (
  full_type_desc : NATURAL
)
return NATURAL is
  variable full_type_desc_high : std_logic_vector(7 downto 0);
  variable full_type_desc_stdl : std_logic_vector(31 downto 0);
begin
  full_type_desc_stdl := std_logic_vector(to_unsigned(full_type_desc, 32));
	full_type_desc_high := full_type_desc_stdl(31 downto 24);
	case full_type_desc_high is
	  when X"53"  => return 16#53#;
		when X"60"  => return 16#60#;
		when X"63"  => return 16#63#;
		when X"71"  => return 16#71#;
		when X"72"  => return 16#72#;
		when X"73"  => return 16#73#;
		when others => return 16#00#;
	end case;
end get_trgt_desc;

subtype rw_typ is std32_a(EBS_AD_SIZE - 5 downto 0);                        -- first reg. are R/W (3..0)
subtype rd_typ is std32_a(EBS_AD_SIZE - 1 downto 4);                        -- last reg. are read only (7..4)
type state_typ is (iddle, adrs, wr1, rd1, bcc1, bcc2, error, abort);
type ack_err_type     is (ACK_ERR_INIT,
                          WAIT_CS_LOW_ACK, WAIT_CS_LOW_ERR, WAIT_CS_LOW_ACK_ERR,
													WAIT_CS_HIGH_ACK, WAIT_CS_HIGH_ERR, WAIT_CS_HIGH_ACK_ERR,
                          ACK_DLY, ERR_DLY, ACK_ERR_DLY, ACK_ERR_END);
signal ack_err_state  : ack_err_type;
type dat_rd_type      is (DAT_RD_INIT, DAT_RD_ON, DAT_RD_END);
signal dat_rd_state   : dat_rd_type;

constant GND16        : std16   := (others => '0');
constant UPRANGE      : NATURAL := NLog2(EBS_AD_SIZE) - 1;
constant TRGT_DESC    : NATURAL := get_trgt_desc(TYPE_DESC);
constant EBS_VERSION  : NATURAL := 16#00000025#;                            -- version 2.5

signal state, nextate : state_typ;
signal dk             : std_logic := '0';                                   -- slave addrs & data acknowledge
signal s_dk           : std_logic := '0';                                   -- acknowledge registered
signal berr           : std_logic := '0';                                   -- addressing error
signal myself         : std_logic := '0';                                   -- slave selected
signal bwrite         : std_logic := '0';                                   -- burst write
signal eof            : std_logic := '0';                                   -- slave end of frame
signal load           : std_logic := '0';                                   -- register stack load
signal ofld           : std_logic := '0';                                   -- offset counter load
signal ofen           : std_logic := '0';                                   -- offset counter enable
signal ovf_wr         : std_logic := '0';                                   -- offset write overflow
signal ovf_err        : std_logic := '0';                                   -- offset overflow error
signal ofst           : unsigned(UPRANGE + 1 downto 0);                     -- offset counter in burst, MSbit is overflow
signal ofstwr         : unsigned(UPRANGE downto 0);                         -- max. offset when writing in
signal rmux           : std32;
signal rw_regs        : rw_typ;
signal rd_regs        : rd_typ;
signal regs_ofswr     : std_logic;                                          -- write burst offset enable
signal regs_ofs       : std_logic_vector(Nlog2(EBS_AD_SIZE) - 1 downto 0);  -- offset
signal wr_en_ack      : std_logic;
signal fpga_eos       : std_logic;
signal sys_rst        : std_logic;
signal fspi_sck_p     : std_logic;
signal fspi_sck_n     : std_logic;
signal fspi_sck_usr   : std_logic;
signal fspi_miso_p    : std_logic;
signal fspi_miso_s    : std_logic;
signal fspi_cs        : std_logic;
signal fspi_mosi      : std_logic;

signal eb_rst_i       : std_logic;
signal eb_bmx_i       : std_logic;
signal eb_as_i        : std_logic;
signal eb_eof_i       : std_logic;
signal eb_dat_i       : std32;
signal ebs_o_dat      : std32;
signal valid_data     : std32;

signal eb_bmx         : std_logic;                                          -- busy some master, P&R help
signal eb_bcc         : std_logic;                                          -- E-bone broad call starts
signal eb_mine        : std_logic;                                          -- E-bone burst addressing me starts
signal eb_ofst        : std_logic_vector(EBS_AD_RNGE-1 downto 0);

alias eb_bar          : std_logic_vector(1 downto 0) is eb_dat_i(29 downto 28);
alias eb_amsb         : std_logic_vector(EBS_AD_RNGE - 1 downto UPRANGE + 1) is eb_dat_i(EBS_AD_RNGE - 1 downto UPRANGE + 1);
------------------------------------------------------------------------
begin
------------------------------------------------------------------------

assert EBS_AD_SIZE = 8 report "EBS_AD_SIZE must be set to 8!" severity failure;
assert EBS_AD_OFST mod 2**(UPRANGE+1) = 0 report "EBS_AD_OFST invalid boundary!" severity failure;

process(fspi_sck_p, eb_rst_i)
begin
  if eb_rst_i = '1' then
    fspi_cs_o   <= '1';
	  fspi_mosi_o <= '0';
  elsif rising_edge(fspi_sck_p) then
    fspi_cs_o   <= fspi_cs;
	  fspi_mosi_o <= fspi_mosi;
  end if;
end process;

process(eb_clk_i, eb_rst_i)
begin
  if eb_rst_i = '1' then
    sys_rst <= '1';
  elsif rising_edge(eb_clk_i) then
    sys_rst <= eb_rst_i;
  end if;
end process;

ser_i : fspi_ser_des

generic map (
  BUFR_DIVIDE     => BUFR_DIVIDE,
  TRGT_DESC       => TRGT_DESC,
  DWIDTH          => 32,
  ADEPTH          => 128,
	sys_w           => 1,
  dev_w           => 8
)

port map (
  sys_rst         => sys_rst,
  sys_clk         => eb_clk_i,

	wr_en_ack       => wr_en_ack,
	wr_dat_reg      => rw_regs(0),
	wd_cnt          => rw_regs(1),
	wd_size         => rw_regs(2),
  edge_nb         => rd_regs(4),
  ser_stat        => rd_regs(5),
	par_stat        => rd_regs(6),

  fspi_cs         => fspi_cs,
	fspi_sck        => fspi_sck_p,
	fspi_mosi       => fspi_mosi,
  fspi_miso       => fspi_miso_s
);

oddr_sp6_gene : if (TRGT_DESC = 16#60#) generate

  fspi_miso_s <= fspi_miso_i;
  fspi_sck_n  <= not fspi_sck_p;
  fpga_eos    <= '0';

  oddr_inst : ODDR2
	-----------------
  generic map (
    DDR_ALIGNMENT => "NONE",
    INIT          => '0',
    SRTYPE        => "SYNC"
  )
  port map (
    Q  => fspi_sck_o,
    C0 => fspi_sck_p,
    C1 => fspi_sck_n,
    CE => '1',
    D0 => '1',
    D1 => '0',
    R  => '0',
    S  => '0'
  );

end generate oddr_sp6_gene;

oddr_vx6_gene : if (TRGT_DESC = 16#63#) generate

  fspi_miso_s <= fspi_miso_p;
	fspi_sck_n  <= not fspi_sck_p;

  use_rs_edge_gene : if (USE_RS_EDGE = 1) generate
	  fspi_sck_usr <= fspi_sck_p;
		fspi_sck_o   <= fspi_sck_p;
	end generate use_rs_edge_gene;

  use_fl_edge_gene : if (USE_RS_EDGE = 0) generate
	  fspi_sck_usr <= fspi_sck_n;
		fspi_sck_o   <= fspi_sck_n;
	end generate use_fl_edge_gene;

  startup6 : STARTUP_VIRTEX6
	--------------------------
  generic map (
    PROG_USR => FALSE                                          -- Activate program event security feature
  )
  port map (
    CFGCLK    => open,                                         -- Configuration main clock output
    CFGMCLK   => open,                                         -- Configuration internal oscillator clock output
    DINSPI    => fspi_miso_p,                                  -- DIN SPI PROM access output
    EOS       => fpga_eos,                                     -- Active high output signal indicating the End Of Configuration.
    PREQ      => open,                                         -- PROGRAM request to fabric output
    TCKSPI    => open,                                         -- TCK configuration pin access output
    CLK       => '0',                                          -- User start-up clock input
    GSR       => '0',                                          -- Global Set/Reset input 
    GTS       => '0',                                          -- Global 3-state input (GTS cannot be used for the port name)
    KEYCLEARB => '0',                                          -- Clear AES Decrypter Key input from Battery-Backed RAM (BBRAM)
    PACK      => '0',                                          -- PROGRAM acknowledge input
    USRCCLKO  => fspi_sck_usr,                                 -- User CCLK input
    USRCCLKTS => '0',                                          -- User CCLK 3-state enable input
    USRDONEO  => '1',                                          -- User DONE pin output control
    USRDONETS => '1'                                           -- User DONE 3-state enable output
  );

end generate oddr_vx6_gene;

oddr_kx7_gene : if (TRGT_DESC = 16#71#) or (TRGT_DESC = 16#72#) or (TRGT_DESC = 16#73#) generate

  fspi_miso_s <= fspi_miso_i;
	fspi_sck_n  <= not fspi_sck_p;

  use_rs_edge_gene : if (USE_RS_EDGE = 1) generate
	  fspi_sck_usr <= fspi_sck_p;
		fspi_sck_o   <= fspi_sck_p;
	end generate use_rs_edge_gene;

  use_fl_edge_gene : if (USE_RS_EDGE = 0) generate
	  fspi_sck_usr <= fspi_sck_n;
		fspi_sck_o   <= fspi_sck_n;
	end generate use_fl_edge_gene;

  startup7 : STARTUPE2
  --------------------
  generic map (
    PROG_USR => "FALSE"                                        -- Activate program event security feature
  )
  port map (
    CFGCLK    => open,                                         -- Configuration main clock output
    CFGMCLK   => open,                                         -- Configuration internal oscillator clock output
    EOS       => fpga_eos,                                     -- Active high output signal indicating the End Of Configuration.
    PREQ      => open,                                         -- PROGRAM request to fabric output
    CLK       => '0',                                          -- User start-up clock input
    GSR       => '0',                                          -- Global Set/Reset input 
    GTS       => '0',                                          -- Global 3-state input (GTS cannot be used for the port name)
    KEYCLEARB => '0',                                          -- Clear AES Decrypter Key input from Battery-Backed RAM (BBRAM)
    PACK      => '0',                                          -- PROGRAM acknowledge input
    USRCCLKO  => fspi_sck_usr,                                 -- User CCLK input
    USRCCLKTS => '0',                                          -- User CCLK 3-state enable input
    USRDONEO  => '1',                                          -- User DONE pin output control
    USRDONETS => '1'                                           -- User DONE 3-state enable output
  );

end generate oddr_kx7_gene;

eb_rst_i <= ebs_i.eb_rst;
eb_bmx_i <= ebs_i.eb_bmx;
eb_as_i  <= ebs_i.eb_as;
eb_eof_i <= ebs_i.eb_eof;
eb_dat_i <= ebs_i.eb_dat;

-- Address decode input pipeline
--------------------------------
eb_ofst <= std_logic_vector(to_unsigned(EBS_AD_OFST, EBS_AD_RNGE));
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then 
      eb_bmx  <= eb_bmx_i;          -- P&R help
      eb_bcc  <= '0';
      eb_mine <= '0';
      if    eb_bmx_i   = '1'        -- burst start
        AND eb_as_i = '1' then      -- adrs
         if eb_eof_i = '1' then     -- broad-cast/call
            eb_bcc <= '1';
         elsif (   eb_bar  = std_logic_vector(to_unsigned(EBS_AD_BASE, 2))
               AND eb_amsb = eb_ofst(EBS_AD_RNGE-1 downto UPRANGE+1)) then
            eb_mine <= '1';         -- that's me
         end if;
      end if;
   end if;
end process;

-- E-bone slave FSM
-------------------
process	(eb_clk_i)
begin
  if rising_edge(eb_clk_i) then
    if eb_rst_i = '1' then
      state     <= iddle;
      myself    <= '0';  
    else
      myself    <= (ofld OR myself) and NOT (eof OR ovf_err);  -- set and lock clear
      state     <= nextate;
    end if;
  end if;
end process;

process(state, eb_as_i, eb_eof_i, eb_dat_i,
               eb_bmx, eb_bcc, eb_mine, ovf_wr)
begin
  nextate <= state;                  -- stay in same state
  load    <= '0';
  ofld    <= '0';
  ofen    <= '0';
  dk      <= '0';
  eof     <= '0';
  bwrite  <= '0';
  berr    <= '0';

  case state is 
    when iddle =>                  -- sleeping
      if eb_bcc = '1' then
         nextate <= bcc1;
      elsif eb_mine = '1' then
         ofld <= '1';              -- store offset
         if ovf_wr = '1' then
            berr    <= '1';
            nextate <= error;      -- write to read only location
         else 
            dk      <= '1';        -- slave addrs ack
            nextate <= adrs;
         end if;
      end if;

    when adrs =>                     -- pipelined slave addrs ack (routing help)
      dk  <= '1';                    -- early ack burst
      if eb_dat_i(31) = '1' then     -- read or write?
        ofen    <= '1';              -- pipeline early init
        nextate <= rd1;
      else
        nextate <= wr1;
      end if;

    when wr1 =>                     -- burst write
      bwrite  <= '1';
      if eb_bmx = '0' then          -- abort
        nextate <= abort;
      else                          -- wait until addressing done
        dk <= NOT eb_eof_i;         -- ack continues till end of burst

        if  eb_as_i = '0' then      -- store data
          load <= '1';
          ofen <= '1';
          if eb_eof_i = '1' then
            eof     <= '1';
            nextate <= iddle;
          end if;
        end if;
      end if;

    when rd1 =>                     -- burst read
      if eb_bmx = '0' then          -- abort
        nextate <= abort;
      else   
        dk   <= NOT eb_eof_i;       -- ack continues till end of burst
        ofen <= '1';
        if eb_eof_i = '1' then
          eof     <= '1';
          nextate <= iddle;
        end if;
      end if;

    when bcc1 =>                    -- broad-cast/call
      nextate <= bcc2;

    when bcc2 =>                    -- wait until bus released
      if eb_bmx = '0' OR eb_eof_i = '1' then
         eof <= '1';
         nextate <= iddle;
      end if;

    when error =>                   -- wait until bus released
      if eb_bmx = '0' OR eb_eof_i = '1' then
         eof <= '1';
         nextate <= iddle;
      else
         berr <= '1';
      end if;

    when abort =>                   -- quit on abort
      eof     <= '1';
      nextate <= iddle;

  end case;

end process;

-- Offset counter
-----------------
process	(eb_clk_i)
begin
  if rising_edge(eb_clk_i) then
    ovf_err <= '0';
    if ofld = '1' then              -- offset load
      ofst <= unsigned('0' & eb_dat_i(UPRANGE downto 0));
    elsif ofen = '1' then
      if ofst(UPRANGE+1) = '1' then -- count overflow
        ovf_err <= '1';
      else
        ofst <= ofst + 1;           -- increment
      end if;
    end if;
  end if;
end process;

ofstwr <= to_unsigned(EBS_AD_SIZE - 1, UPRANGE + 1);
ovf_wr <= '1' when eb_dat_i(31) = '0' AND unsigned(eb_dat_i(UPRANGE downto 0)) >= ofstwr else '0'; -- write to read only regs.

regs_ofs   <= std_logic_vector(ofst(UPRANGE downto 0)); 
regs_ofswr <= ofen when (bwrite = '1') else '0';

-- Register stack writing in
----------------------------
process(eb_clk_i)
begin
  if rising_edge(eb_clk_i) then
    if load = '1' then
      for i in rw_typ'RANGE loop
        if to_integer(ofst) = i then
          rw_regs(i) <= eb_dat_i;
        end if;
      end loop;
    end if;
  end if;
end process;

-- Register read back
-- Registered output multiplexor
--------------------------------
process(eb_clk_i)
begin
  if rising_edge(eb_clk_i) then
    for i in rw_typ'RANGE loop
      if to_integer(ofst) = i then
        rmux <= rw_regs(i);
      end if;
    end loop;
    for i in rd_typ'RANGE loop
      if to_integer(ofst) = i then
        rmux <= rd_regs(i);
      end if;
    end loop;
  end if;
end process;

rd_regs(7) <= fpga_eos & std_logic_vector(to_unsigned(EBS_VERSION, 31));
wr_en_ack  <= '1' when ((regs_ofswr = '1') and (regs_ofs = "000")) else '0';

-- E-bone drivers
-----------------
process(eb_clk_i)
begin
  if rising_edge(eb_clk_i) then
    s_dk     <= dk AND NOT ovf_err;
  end if;
end process;

-- E-bone data read process
---------------------------
process(eb_clk_i)
begin
  if rising_edge(eb_clk_i) then
	  if eb_rst_i = '1' then
		  ebs_o_dat    <= (others => '0');
			dat_rd_state <= DAT_RD_INIT;
		else
		  case (dat_rd_state) is

			  when DAT_RD_INIT =>

          if ((myself = '1') and (s_dk = '1') and (berr = '0')) then
					  dat_rd_state <= DAT_RD_ON;
					else
					  dat_rd_state <= DAT_RD_INIT;
          end if;

				when DAT_RD_ON =>

          ebs_o_dat    <= valid_data;
					dat_rd_state <= DAT_RD_END;

        when DAT_RD_END =>

          if (myself = '0') then
            dat_rd_state <= DAT_RD_INIT;
					else
					  dat_rd_state <= DAT_RD_END;
					end if;

				when others =>

          ebs_o_dat    <= (others => '0');
          dat_rd_state <= DAT_RD_INIT;

			end case;
		end if;
  end if;
end process;

-- E-bone ack/err machine process
---------------------------------
process(eb_clk_i)
begin
  if rising_edge(eb_clk_i) then
	  if eb_rst_i = '1' then
		  ebs_o.eb_dk   <= '0';
			ebs_o.eb_err  <= '0';
		  ack_err_state <= ACK_ERR_INIT;
		else
		  case (ack_err_state) is

			  when ACK_ERR_INIT =>

          if ((myself = '1') and (fspi_cs = '1') and (s_dk = '1') and (berr = '0')) then
			      ebs_o.eb_dk   <= '1';
			      ebs_o.eb_err  <= '0';
						ack_err_state <= ACK_ERR_END;
					elsif ((myself = '1') and (fspi_cs = '1') and (s_dk = '0') and (berr = '1')) then
					  ebs_o.eb_dk   <= '0';
			      ebs_o.eb_err  <= '1';
						ack_err_state <= ACK_ERR_END;
					elsif ((myself = '1') and (fspi_cs = '1') and (s_dk = '1') and (berr = '1')) then
					  ebs_o.eb_dk   <= '1';
			      ebs_o.eb_err  <= '1';
						ack_err_state <= ACK_ERR_END;
					elsif ((myself = '1') and (fspi_cs = '0') and (s_dk = '1') and (berr = '0')) then
					  ack_err_state <= WAIT_CS_HIGH_ACK;
					elsif ((myself = '1') and (fspi_cs = '0') and (s_dk = '0') and (berr = '1')) then
					  ack_err_state <= WAIT_CS_HIGH_ERR;
					elsif ((myself = '1') and (fspi_cs = '0') and (s_dk = '1') and (berr = '1')) then
					  ack_err_state <= WAIT_CS_HIGH_ACK_ERR;
					else
					  ack_err_state <= ACK_ERR_INIT;
			    end if;

        when WAIT_CS_HIGH_ACK =>

          if (fspi_cs = '1') then
					  ack_err_state <= ACK_DLY;
					else
					  ack_err_state <= WAIT_CS_HIGH_ACK;
					end if;

        when WAIT_CS_HIGH_ERR =>

          if (fspi_cs = '1') then
					  ack_err_state <= ERR_DLY;
					else
					  ack_err_state <= WAIT_CS_HIGH_ERR;
					end if;

        when WAIT_CS_HIGH_ACK_ERR =>

          if (fspi_cs = '1') then
					  ack_err_state <= ACK_ERR_DLY;
					else
					  ack_err_state <= WAIT_CS_HIGH_ACK_ERR;
					end if;

        when ACK_DLY =>

          ebs_o.eb_dk   <= '1';
			    ebs_o.eb_err  <= '0';
				  ack_err_state <= ACK_ERR_END;

				when ERR_DLY =>

          ebs_o.eb_dk   <= '0';
			    ebs_o.eb_err  <= '1';
				  ack_err_state <= ACK_ERR_END;

				when ACK_ERR_DLY =>

          ebs_o.eb_dk   <= '1';
			    ebs_o.eb_err  <= '1';
				  ack_err_state <= ACK_ERR_END;

        when ACK_ERR_END =>

          if (myself = '0') then
            ebs_o.eb_dk   <= '0';
			      ebs_o.eb_err  <= '0';
            ack_err_state <= ACK_ERR_INIT;
					else
            ebs_o.eb_dk   <= dk;
			      ebs_o.eb_err  <= '0';
					  ack_err_state <= ACK_ERR_END;
					end if;

				when others =>

          ebs_o.eb_dk   <= '0';
			    ebs_o.eb_err  <= '0';
		      ack_err_state <= ACK_ERR_INIT;

			end case;
		end if;
  end if;
end process;

valid_data   <= rmux when (myself = '1') else (others =>'0');
ebs_o.eb_dat <= ebs_o_dat when (myself = '1')  else (others =>'0');
fpga_eos_o   <= fpga_eos;

end rtl;
