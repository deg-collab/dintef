--------------------------------------------------------------------------
--
-- E-bone - Flash SPI interface
-- SER module
--
--------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.ebs_pkg.all;

library unisim;
use unisim.vcomponents.BUFR;

entity fspi_ser is

generic (
  sys_w                : integer := 1;
  dev_w                : integer := 8
);

port (
  DATA_OUT_FROM_DEVICE : in std_logic_vector(dev_w - 1 downto 0);
  DATA_OUT_TO_PINS     : out std_logic_vector(sys_w - 1 downto 0);
  CLK_TO_PINS          : out std_logic;

  CLK_IN               : in std_logic;
  CLK_DIV_OUT          : out std_logic;
  CLK_RESET            : in std_logic;
  IO_RESET             : in std_logic
);

end fspi_ser;

------------------------------------------------------------------------
architecture rtl of fspi_ser is
------------------------------------------------------------------------

signal lp_cnt : std_logic_vector(2 downto 0);

-----
begin
-----

CLK_DIV_OUT         <= '0';
CLK_TO_PINS         <= CLK_IN;
DATA_OUT_TO_PINS(0) <= DATA_OUT_FROM_DEVICE(conv_integer(lp_cnt));

process(IO_RESET, CLK_RESET, CLK_IN)
begin

  if ((IO_RESET = '1') or (CLK_RESET = '1')) then
	  lp_cnt <= (others => '0');
	elsif (CLK_IN'event and CLK_IN = '1') then
	  lp_cnt <= lp_cnt + 1;
	end if;

end process;

end rtl;



