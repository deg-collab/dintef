--------------------------------------------------------------------------
--
-- E-bone - Flash SPI interface
-- DES module
--
--------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.ebs_pkg.all;

library unisim;
use unisim.vcomponents.BUFR;

entity fspi_des is

generic (
  DWIDTH            : natural := 32;
  sys_w             : integer := 1;
  dev_w             : integer := 8
);

port (
  DATA_IN_FROM_PINS : in std_logic_vector(sys_w - 1 downto 0);
  DATA_IN_TO_DEVICE : out std_logic_vector(DWIDTH - 1 downto 0);
  BITSLIP           : in std_logic;

  CLK_IN            : in std_logic;
  CLK_DIV_IN        : in std_logic;
  CLK_RESET         : in std_logic;
  IO_RESET          : in std_logic
);

end fspi_des;

------------------------------------------------------------------------
architecture rtl of fspi_des is
------------------------------------------------------------------------

signal data_s : std_logic_vector(DWIDTH - 1 downto 0);

-----
begin
-----

DATA_IN_TO_DEVICE <= data_s;

process(IO_RESET, CLK_RESET, CLK_IN)
begin
  if ((IO_RESET = '1') or (CLK_RESET = '1')) then
    data_s <= (others => '0');
  elsif (CLK_IN'event and CLK_IN = '1') then
    data_s(0) <= DATA_IN_FROM_PINS(0);
    for i in 1 to (DWIDTH - 1) loop
		  data_s(i) <= data_s(i - 1);
	  end loop;
  end if;
end process;

end rtl;



