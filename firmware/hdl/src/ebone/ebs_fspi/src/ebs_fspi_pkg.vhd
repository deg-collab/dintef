--------------------------------------------------------------------------
--
-- E-bone - Flash SPI interface package file
--
--------------------------------------------------------------------------
--
-- Version  Date       Author    Comment
-- 1.1      22/10/12   Le Caer
-- 1.2      26/03/13   Le Caer
-- 2.0      30/09/13   herve     Record i/o, pipelined inputs
-- 2.1      18/12/13   Le Caer   Version (two nibbles) status offset 7
-- 2.2      26/03/14   Le Caer   spi clock edge and period parameters
-- 2.3      12/08/14   Le Caer   Version new parameter BUFR_DIVIDE
-- 2.4      17/09/15   Le Caer   End of Startup, active High output signal, from STARTUP module
-- 2.5      05/09/16   Le Caer   Status offset7 bit-31 = End of Startup
-- 2.6      15/02/18   herve     Removed useless interrupt logic
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- Declare the component 'ebs_fspi'
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.ebs_pkg.all;

package ebs_fspi_pkg is

component ebs_fspi 
generic (
  USE_RS_EDGE  : INTEGER := 0;                         -- User CCLK falling edge when 0 (default)
  BUFR_DIVIDE  : STRING  := "4";                       -- EB_CLK frequency divisor for SPI flash memory clock
  TYPE_DESC    : NATURAL := 16#00000000#;              -- Type description target and/or application dependant
  EBS_AD_RNGE  : NATURAL := 12;                        -- short adressing range
  EBS_AD_BASE  : NATURAL := 1;                         -- usual IO segment
  EBS_AD_SIZE  : NATURAL := 8;                         -- size in segment
  EBS_AD_OFST  : NATURAL := 0;                         -- offset in segment
  EBS_MIRQ     : std16   := (others => '0')            -- Message IRQ (not used)
);
port (
-- E-bone interface
  eb_clk_i     : in std_logic;                         -- system clock
  ebs_i        : in ebs32_i_typ;
  ebs_o        : out ebs32_o_typ;

-- SPI interface 
  fspi_cs_o    : out std_logic;                        -- SPI slave select (active Low)
  fspi_sck_o   : out std_logic;                        -- SPI clock
  fspi_mosi_o  : out std_logic;                        -- SPI master out slave in
  fspi_miso_i  : in std_logic;                         -- SPI master in slave out

  fpga_eos_o   : out std_logic                         --  End of Startup. Active High output
);
end component;

end package ebs_fspi_pkg;
