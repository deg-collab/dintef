################################################################################
#
# E-bone - Fast SPI interface slave
# User constraints declarations.
# This file is an example valid with the Xilinx FPGA device defined below :
# FPGA family      : 7
# FPGA device      : xc7k325t
# Speedgrade       : -2
# Package          : fbg676c
#
################################################################################
#
# Version  Date      Author
# 2.5      08/09/16  le caer
#
################################################################################

# SPI Flash Bitstream, BANKs 14, VCCO = 2.5V
# ------------------------------------------
# SPI_CS
# ------
set_property PACKAGE_PIN C23     [get_ports {FLASH_CS}]
set_property IOSTANDARD LVCMOS25 [get_ports {FLASH_CS}]
set_property IOB TRUE            [get_ports {FLASH_CS}]
# SPI_MOSI
# --------
set_property PACKAGE_PIN B24     [get_ports {FLASH_MOSI}]
set_property IOSTANDARD LVCMOS25 [get_ports {FLASH_MOSI}]
set_property IOB TRUE            [get_ports {FLASH_MOSI}]
# SPI_MISO
# --------
set_property PACKAGE_PIN A25     [get_ports {FLASH_MISO}]
set_property IOSTANDARD LVCMOS25 [get_ports {FLASH_MISO}]
set_property IOB TRUE            [get_ports {FLASH_MISO}]
# SPI_WP
# ------
set_property PACKAGE_PIN B22     [get_ports {FLASH_WP}]
set_property IOSTANDARD LVCMOS25 [get_ports {FLASH_WP}]
# SPI_HOLD
# --------
set_property PACKAGE_PIN A22     [get_ports {FLASH_HOLD}]
set_property IOSTANDARD LVCMOS25 [get_ports {FLASH_HOLD}]

set_property LOC BUFR_X0Y10 [get_cells {S1/ser_i/bufr_kx7_gene.bufr_inst}]
create_clock -name spi_clk -period 32.00 [get_pins {S1/ser_i/bufr_kx7_gene.bufr_inst/O}]

################################################################################

