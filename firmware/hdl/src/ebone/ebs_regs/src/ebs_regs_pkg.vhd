--------------------------------------------------------------------------
--
-- E-bone - Register stack slave package
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     1.0  12/10/09    herve  1st release
--     1.1  20/10/10    herve  Updated to E-bone 1.1
--                             Fixed bug overflow error 
--     1.2  11/04/11    herve  Removed ACKs outputs, added offset
--                             All read only regs now supported
--                             Updated to E-bone 1.2
--     1.3  12/01/12    herve  Check size 2**N
--     1.4  14/08/13    herve  regs_irq_i glitch filter removed
--
--     2.0  30/10/13    herve  Record i/o, pipelined inputs
--                             reg0_sclr_i input removed
--     2.1  14/01/15    herve  Minor (P&R) improvement
--     2.2  25/02/15    herve  Bug when all R/W fixed
--     2.3  14/02/18    herve  Bug interrupt request fixed
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- Declare the component 'ebs_regs'
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.ebs_pkg.all;

package ebs_regs_pkg is

component ebs_regs 
generic (
   EBS_AD_RNGE  : natural := 12;  -- short adressing range
   EBS_AD_BASE  : natural := 1;   -- usual IO segment
   EBS_AD_SIZE  : natural := 16;  -- size in segment
   EBS_AD_OFST  : natural := 0;   -- offset in segment
   EBS_MIRQ     : std16   := (0=> '1', others => '0'); -- Message IRQ
   REG_RO_SIZE  : natural := 1;   -- read only reg. number
   REG_WO_BITS  : natural := 0    -- register #0 self clear bit number
);
port (

-- E-bone interface
   eb_clk_i     : in  std_logic;  -- system clock
   ebs_i        : in  ebs32_i_typ; 
   ebs_o        : out ebs32_o_typ; 

-- Register interface
   regs_o       : out std32_a;    -- R/W registers external outputs
   regs_i       : in  std32_a;    -- read only register external inputs
   regs_irq_i   : in  std_logic;  -- interrupt request
   regs_iak_o   : out std_logic;  -- interrupt handshake
   regs_ofsrd_o : out std_logic;  -- read burst offset enable
   regs_ofswr_o : out std_logic;  -- write burst offset enable
   regs_ofs_o   : out std_logic_vector(Nlog2(EBS_AD_SIZE)-1 downto 0) -- offset
);
end component;
end package ebs_regs_pkg;
