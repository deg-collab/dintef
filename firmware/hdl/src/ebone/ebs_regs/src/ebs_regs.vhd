--------------------------------------------------------------------------
--
-- E-bone - Register stack slave
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     1.0  12/10/09    herve  1st release
--     1.1  20/10/10    herve  Updated to E-bone 1.1
--                             Fixed bug overflow error 
--     1.2  11/04/11    herve  Removed ACKs outputs, added offset
--                             All read only regs now supported
--                             Updated to E-bone 1.2
--     1.3  12/01/12    herve  Check size 2**N
--     1.4  14/08/13    herve  regs_irq_i glitch filter removed
--
--     2.0  30/10/13    herve  Record i/o, pipelined inputs
--                             reg0_sclr_i input removed
--     2.1  14/01/15    herve  Minor (P&R) improvement
--     2.2  25/02/15    herve  Bug when all R/W fixed
--     2.3  14/02/18    herve  Bug interrupt request fixed
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- Register (32 bits) stack  
-- EBS_AD_SIZE   = Total register number, range 4 to 128
-- REG_RO_SIZE   = read only register number
-- REG_WO_BITS   = register #0 self clear bit number
--
-- Mapping from bottom (zero offset) to the top:
-- Read & write registers
-- Read only registers
--
-- Example: EBS_AD_SIZE=16 , REG_RO_SIZE=2
-- regs_o type is array(13 downto  0) of std32; -- R/W, 14 of them
-- regs_i  type is array(15 downto 14) of std32; -- Read only, 2 of them
--
-- Self clear feature
-- if REG_WO_BITS > 0
-- then register #0 least REG_WO_BITS bits self clear (after writing in)
--
-- regs_i external input are NOT resampled
-- regs_irq_i external input (re-sampled) trigs message IRQ
-- regs_iak_o acknowledge request and asserted until IRQ has been served
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

entity ebs_regs is
generic (
   EBS_AD_RNGE  : natural := 12;  -- short adressing range
   EBS_AD_BASE  : natural := 1;   -- usual IO segment
   EBS_AD_SIZE  : natural := 16;  -- size in segment
   EBS_AD_OFST  : natural := 0;   -- offset in segment
   EBS_MIRQ     : std16   := (1 => '1', others => '0'); -- Message IRQ
   REG_RO_SIZE  : natural := 1;   -- read only reg. number
   REG_WO_BITS  : natural := 0    -- register #0 self clear bit number
);
port (

-- E-bone interface
   eb_clk_i     : in  std_logic;  -- system clock
   ebs_i        : in  ebs32_i_typ; 
   ebs_o        : out ebs32_o_typ; 

-- Register interface
   regs_o       : out std32_a;    -- R/W registers external outputs
   regs_i       : in  std32_a;    -- read only register external inputs
   regs_irq_i   : in  std_logic;  -- interrupt request
   regs_iak_o   : out std_logic;  -- interrupt handshake
   regs_ofsrd_o : out std_logic;  -- read burst offset enable
   regs_ofswr_o : out std_logic;  -- write burst offset enable
   regs_ofs_o   : out std_logic_vector(Nlog2(EBS_AD_SIZE)-1 downto 0) -- offset
);
end ebs_regs;

--------------------------------------------------------------------------
architecture rtl of ebs_regs is
--------------------------------------------------------------------------
subtype rw_typ is std32_a(EBS_AD_SIZE-REG_RO_SIZE-1 downto 0); -- first reg. are R/W
subtype rd_typ is std32_a(EBS_AD_SIZE-1 downto EBS_AD_SIZE-REG_RO_SIZE); -- last reg. are read only
type state_typ is (iddle, adrs, wr1, rd1, bcc1, bcc2, error, abort);

constant UPRANGE : natural := NLog2(EBS_AD_SIZE)-1;
constant OK_SIZE : natural := 2**(UPRANGE+1);

signal state, nextate : state_typ;
signal dk        : std_logic; -- slave addrs & data acknowledge
signal s_dk      : std_logic; -- acknowledge registered
signal berr      : std_logic; -- addressing error
signal myself    : std_logic; -- slave selected
signal bwrite    : std_logic; -- burst write
signal irq1, irq2 : std_logic; -- IRQ filter
signal irq       : std_logic; -- pulsed IRQ
signal irq_pendg : std_logic; -- IRQ pending
signal bcall2    : std_logic; -- broad-call phi2
signal eof       : std_logic; -- slave end of frame
signal load      : std_logic; -- register stack load
signal ofld      : std_logic; -- offset counter load
signal ofen      : std_logic; -- offset counter enable
signal ovf_wr    : std_logic; -- offset write overflow
signal ovf_err   : std_logic; -- offset overflow error
signal ofst      : unsigned(UPRANGE+1 downto 0); -- offset counter in burst, MSbit is overflow
signal ofstwr    : unsigned(UPRANGE+1 downto 0); -- max. offset when writing in
signal rmux      : std32;
signal rw_regs   : rw_typ := (others => (others => '0'));
signal reg0_sclr : std_logic; -- register zero self clear flag

signal eb_rst_i  : std_logic;
signal eb_bmx_i  : std_logic;
signal eb_as_i   : std_logic;
signal eb_eof_i  : std_logic;
signal eb_dat_i  : std32;

signal eb_bmx   : std_logic; -- busy some master, P&R help
signal eb_bcc   : std_logic; -- E-bone broad call starts
signal eb_mine  : std_logic; -- E-bone burst addressing me starts
signal eb_ofst  : std_logic_vector(EBS_AD_RNGE-1 downto 0);

alias eb_bar  : std_logic_vector(1 downto 0) 
                is eb_dat_i(29 downto 28); 
alias eb_amsb : std_logic_vector(EBS_AD_RNGE-1 downto UPRANGE+1) 
                is eb_dat_i(EBS_AD_RNGE-1 downto UPRANGE+1);

------------------------------------------------------------------------
begin
------------------------------------------------------------------------

assert EBS_AD_SIZE mod 4 = 0 
       report "EBS_AD_SIZE must be multiple of 4!" severity failure;
assert EBS_AD_SIZE = OK_SIZE
       report "EBS_AD_SIZE must be power of 2" severity failure;
assert EBS_AD_OFST mod OK_SIZE = 0 
       report "EBS_AD_OFST invalid boundary!" severity failure;

eb_rst_i <= ebs_i.eb_rst;
eb_bmx_i <= ebs_i.eb_bmx;
eb_as_i  <= ebs_i.eb_as;
eb_eof_i <= ebs_i.eb_eof;
eb_dat_i <= ebs_i.eb_dat;

-- Address decode input pipeline
--------------------------------
eb_ofst <= std_logic_vector(to_unsigned(EBS_AD_OFST, EBS_AD_RNGE));
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then 
      eb_bmx  <= eb_bmx_i;          -- P&R help
      eb_bcc  <= '0';
      eb_mine <= '0';
      if    eb_bmx_i   = '1'        -- burst start
        AND eb_as_i = '1' then      -- adrs
         if eb_eof_i = '1' then     -- broad-cast/call
            eb_bcc <= '1';
         elsif (   eb_bar  = std_logic_vector(to_unsigned(EBS_AD_BASE, 2))
               AND eb_amsb = eb_ofst(EBS_AD_RNGE-1 downto UPRANGE+1)) then
            eb_mine <= '1';         -- that's me
         end if;
      end if;
   end if;
end process;

-- E-bone slave FSM
-------------------
process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      if eb_rst_i = '1' then
         state  <= iddle;
         myself     <= '0';  
      else
         myself <= (ofld OR myself)           -- set and lock
                   AND NOT (eof OR ovf_err);  -- clear

         state  <= nextate;
      end if;
   end if;
end process;

process(state, eb_as_i, eb_eof_i, eb_dat_i, 
               eb_bmx, eb_bcc, eb_mine, 
               ovf_wr)
begin
   nextate <= state; -- stay in same state
   load    <= '0';
   ofld    <= '0';
   ofen    <= '0';
   bcall2  <= '0';   
   dk      <= '0';
   eof     <= '0';
   bwrite  <= '0';
   berr    <= '0';

   case state is 
      when iddle =>                 -- sleeping
         if eb_bcc = '1' then
            nextate <= bcc1;
         elsif eb_mine = '1' then
            ofld    <= '1';         -- store offset
            if ovf_wr = '1' then
               berr    <= '1';
               nextate <= error;    -- write to read only location
            else 
               dk      <= '1';      -- slave addrs ack
               nextate <= adrs;
            end if;
         end if;

      when adrs =>                     -- pipelined slave addrs ack (routing help)
            dk  <= '1';                -- early ack burst
            if eb_dat_i(31) = '1' then -- read or write?
               ofen  <= '1';           -- pipeline early init
               nextate <= rd1;
            else
               nextate <= wr1;
            end if;

      when wr1 =>                  -- burst write
         bwrite  <= '1';
         if eb_bmx = '0' then      -- abort
            nextate <= abort;
         else                      -- wait until addressing done
            dk   <= NOT eb_eof_i;  -- ack continues till end of burst

            if eb_as_i = '0' then  -- store data
               load  <= '1';
               ofen  <= '1';
               if eb_eof_i = '1' then
                  eof <= '1';
                  nextate <= iddle;
               end if;
            end if;
         end if;

      when rd1 =>                  -- burst read
         if eb_bmx = '0' then      -- abort
            nextate <= abort;
         else   
            dk   <= NOT eb_eof_i;  -- ack continues till end of burst
            ofen <= '1';
            if eb_eof_i = '1' then
               eof <= '1';
               nextate <= iddle;
            end if;
         end if;

      when bcc1 =>                  -- broad-cast/call
         nextate <= bcc2;

      when bcc2 =>                  -- wait until bus released
         bcall2 <= '1';             -- broad-call phi2
         if eb_bmx = '0' OR eb_eof_i = '1' then
            eof <= '1';
            nextate <= iddle;
         end if;

      when error =>                 -- wait until bus released
         if eb_bmx = '0' OR eb_eof_i = '1' then
            eof <= '1';
            nextate <= iddle;
         else
            berr <= '1';
         end if;

      when abort =>                 -- quit on abort
         eof <= '1';
         nextate <= iddle;

   end case;

end process;

-- Offset counter
-----------------
process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then

      ovf_err <= '0';
      if ofld = '1'  then   -- offset load
         ofst <= unsigned('0' & eb_dat_i(UPRANGE downto 0));
      elsif ofen = '1' then
         if ofst(UPRANGE+1) = '1' then   -- count overflow
            ovf_err <= '1';
         else
            ofst <= ofst + 1; -- increment
         end if;
      end if;

   end if;
end process;

ofstwr <=  to_unsigned(EBS_AD_SIZE-REG_RO_SIZE, UPRANGE+2); 
ovf_wr <= '1' when eb_dat_i(31) = '0' AND unsigned('0' & eb_dat_i(UPRANGE downto 0)) >= ofstwr 
           else '0'; -- write to read only regs.

regs_ofs_o   <= std_logic_vector(ofst(UPRANGE downto 0)); 
regs_ofswr_o <= ofen when bwrite = '1' else '0';
regs_ofsrd_o <= ofen AND NOT eof when bwrite = '0' else '0';

-- Self-clear bit management
----------------------------
sclr: if REG_WO_BITS > 0 generate -- do self clear bits
   reg0_sclr <= '1';
end generate sclr;

noclr: if REG_WO_BITS = 0 generate -- no self clear bits
   reg0_sclr <= '0';
end generate noclr;


-- Register stack writing in
----------------------------
swr: if REG_RO_SIZE < EBS_AD_SIZE generate -- some R/W
begin
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then

      if reg0_sclr = '1' then
         for i in REG_WO_BITS-1 downto 0 loop
            rw_regs(0)(i) <= '0'; -- self clear
         end loop;
      end if;

      if load = '1' then
         for i in rw_typ'RANGE loop
            if to_integer(ofst) = i then
               rw_regs(i) <= eb_dat_i;
            end if;
         end loop;
      end if;
   end if;
end process;
regs_o <= rw_regs; -- internal to port
end generate swr;


-- Register read back
-- Registered output multiplexor
--------------------------------
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then

      for i in rw_typ'RANGE loop
         if to_integer(ofst) = i then
            rmux <= rw_regs(i);
         end if;
      end loop;

      for i in rd_typ'RANGE loop
         if to_integer(ofst) = i then
            rmux <= regs_i(i);
         end if;
      end loop;

   end if;
end process;

-- Interrupt request management
-------------------------------
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      irq1 <= regs_irq_i 
              AND NOT eb_bcc;    -- postpone IRQ when broad-call
      irq2 <= irq1; 
      irq  <= irq1 AND NOT irq2; -- rising edge detector

      if irq = '1' then
         irq_pendg <= '1';
      elsif bcall2 = '1' 
         OR ebs_i.eb_rst = '1' then 
         irq_pendg <= '0';
      end if;

   end if;
end process;

-- E-bone drivers
-----------------
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      s_dk       <= dk AND NOT ovf_err;
      regs_iak_o <= irq_pendg; -- IRQ handshake

      ebs_o.eb_err <= berr AND myself;
   end if;
end process;

ebs_o.eb_dk  <= s_dk  when myself = '1' else '0';
ebs_o.eb_dat <= rmux  when myself = '1' else
                GND16 & EBS_MIRQ when bcall2 = '1' AND irq_pendg = '1' 
                else (others =>'0');

end rtl;
