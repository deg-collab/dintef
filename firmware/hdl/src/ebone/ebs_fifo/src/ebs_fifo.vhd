--------------------------------------------------------------------------
--
-- E-bone - FIFO interface
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     1.0  21/12/12    herve  Created from ebs_ram
--                             Message support (E-bone 1.3)
--     1.1  11/02/13    herve  data width reporting
--     2.0  03/02/14    herve  Record i/o, pipelined inputs
--                             Message data extended to 16 bits
--     2.1  21/06/15    herve  Added MIF open command
--     2.2  15/06/16    herve  Message glitch fixed
--
--------------------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- FIFO COREGEN option: registered output
-- IRQ : none
--
-- MIF protocol (see ebm_ebft documentation) compliant
-- MIF request format is as follows
-- offset data
-- 0      Unused
-- 1      MIF count
-- 2      reserved
-- 3      MIF command
--        0000 = Close message channel
--        0001 = Open message channel
--        1111 = Reset
--
----------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.ebs_pkg.all;

entity ebs_fifo is
generic (
   EBS_AD_RNGE  : natural := 16;  -- adressing range
   EBS_AD_BASE  : natural := 2;   -- usual memory segment
   EBS_AD_SIZE  : natural := 4;   -- size in segment
   EBS_AD_OFST  : natural := 0;   -- offset in segment
   EBS_DWIDTH   : natural := 32   -- fifo (E-bone) data width 
);
port (
-- E-bone interface
   eb_clk_i    : in  std_logic;  -- system clock
   eb_rst_i    : in  std_logic;  -- synchronous system reset

   eb_bmx_i    : in  std_logic;  -- busy others
   eb_as_i     : in  std_logic;  -- adrs strobe
   eb_eof_i    : in  std_logic;  -- end of frame
   eb_aef_i    : in  std_logic;  -- almost end of frame
   eb_dat_i    : in  std_logic_vector(EBS_DWIDTH-1 downto 0); -- data write
   eb_dk_o     : out std_logic;  -- data acknowledge
   eb_err_o    : out std_logic;  -- bus error
   eb_dat_o    : out std_logic_vector(EBS_DWIDTH-1 downto 0); -- data read

-- E-bone extension
   ebx_msg_set_i : in  std8;     -- message management
   ebx_dsz_o     : out std4;     -- device data size
   ebx_msg_dst_o : out std4;     -- message destination
   ebx_msg_dat_o : out std16;    -- message data

-- FIFO memory interface
   mem_count_o : out std16;      -- MIF requested count
   mem_din_o   : out std_logic_vector(EBS_DWIDTH-1 downto 0); -- FIFO data in
   mem_dout_i  : in  std_logic_vector(EBS_DWIDTH-1 downto 0); -- FIFO data out
   mem_wr_o    : out std_logic;  -- FIFO write enable
   mem_rd_o    : out std_logic;  -- FIFO read enable
   mem_rst_o   : out std_logic;  -- FIFO reset
   mem_empty_i : in  std_logic;  -- FIFO empty status
   mem_full_i  : in  std_logic;  -- FIFO full status
   mem_rd_cnt_i: in  std16       -- FIFO occupancy (may ground, but message in use)
);
end ebs_fifo;

--------------------------------------
architecture rtl of ebs_fifo is
--------------------------------------
constant EBS_DWLOG : natural := Nlog2(EBS_DWIDTH/16);
constant UPRANGE   : natural := NLog2(EBS_AD_SIZE)-1;
constant OK_SIZE   : natural := 2**(UPRANGE+1);

type state_typ is (iddle, wr1, rd1, bcc1, 
                   mif1, mif2, mif3, mif4, abort, error);
signal state, nextate : state_typ;
signal adk       : std_logic := '0'; -- slave addrs & data acknowledge
signal s_dk      : std_logic := '0'; -- slave data acknowledge
signal rw_err    : std_logic := '0'; -- read (empty) or write(full) error
signal fif_err   : std_logic := '0'; -- early fifo error 
signal myself    : std_logic := '0'; -- slave selected
signal eof       : std_logic := '0'; -- slave end of frame
signal ofld      : std_logic := '0'; -- offset counter load
signal emti1     : std_logic ; -- empty status delayed
signal emti2     : std_logic ; -- empty status delayed
signal mem_rd_j  : std_logic ; -- internal
signal count_ld  : std_logic ; -- MIF count capture
signal rst_on    : std_logic ; -- MIF reset request
signal rst_off   : std_logic ; 
signal msg_clr   : std_logic ; -- message clear
signal msg_err   : std_logic ; -- message set error
signal msg_run   : std4;       -- running message initiator
signal mem_rdy   : std_logic;  -- FIFO ready to send data

signal eb_bmx   : std_logic; -- busy some master, P&R help
signal eb_bcc   : std_logic; -- E-bone broad call starts
signal eb_mine  : std_logic; -- E-bone burst addressing me starts
signal eb_ofst  : std_logic_vector(EBS_AD_RNGE-1 downto 0);

alias eb_bar  : std_logic_vector(1 downto 0) 
                is eb_dat_i(29 downto 28); 
alias eb_amsb : std_logic_vector(EBS_AD_RNGE-1 downto UPRANGE+1) 
                is eb_dat_i(EBS_AD_RNGE-1 downto UPRANGE+1);
alias msg_lsb : std4 is ebx_msg_set_i(3 downto 0);

------------------------------------------------------------------------
begin
------------------------------------------------------------------------

assert EBS_AD_SIZE mod 4 = 0 
       report "EBS_AD_SIZE must be multiple of 4!" severity failure;
assert EBS_AD_SIZE = OK_SIZE
       report "EBS_AD_SIZE must be power of 2" severity failure;
assert EBS_AD_OFST mod OK_SIZE = 0 
       report "EBS_AD_OFST invalid boundary!" severity failure;

-- Address decode input pipeline
--------------------------------
eb_ofst <= std_logic_vector(to_unsigned(EBS_AD_OFST, EBS_AD_RNGE));
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then 
      eb_bmx  <= eb_bmx_i;          -- P&R help
      eb_bcc  <= '0';
      eb_mine <= '0';
      if    eb_bmx_i   = '1'        -- burst start
        AND eb_as_i = '1' then      -- adrs
         if eb_eof_i = '1' then     -- broad-cast/call
            eb_bcc <= '1';
         elsif (   eb_bar  = std_logic_vector(to_unsigned(EBS_AD_BASE, 2))
               AND eb_amsb = eb_ofst(EBS_AD_RNGE-1 downto UPRANGE+1)) then
            eb_mine <= '1';         -- that's me
         end if;
      end if;
   end if;
end process;

-- E-bone slave FSM
-------------------
process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      if eb_rst_i = '1' then
         state  <= iddle;
         myself <= '0';
      else
         myself <= (ofld OR myself)         -- set and lock
                   AND NOT (eof OR rw_err); -- clear
         state  <= nextate;
      end if;
   end if;
end process;

process(state, eb_bmx_i, eb_as_i, eb_aef_i, eb_eof_i, eb_dat_i,
               eb_bmx, eb_bcc, eb_mine, myself,
               emti2, mem_full_i)
begin
   nextate <= state; -- stay in same state
   ofld    <= '0';
   adk     <= '0';
   eof     <= '0';
   mem_wr_o <= '0';   
   mem_rd_j <= '0';
   rw_err   <= '0'; 
   fif_err  <= '0'; 
   msg_clr  <= '0'; 
   rst_on   <= '0';
   rst_off  <= '0';
   count_ld <= '0';

   case state is 
      when iddle =>                    -- sleeping
         rst_off  <= '1';
         if eb_bcc = '1' then
            nextate <= bcc1;
         elsif eb_mine = '1' then
            ofld    <= '1';            -- store offset
            if eb_dat_i(31) = '1' then -- burst read 
               if emti2 = '1' then
                  fif_err <= '1';      -- early error, don't ack
                  nextate <= error;
               else
                  adk   <= '1';        -- addrs ack
                  mem_rd_j <= '1';
                  nextate  <= rd1;
               end if;

            else                       -- burst write
               if eb_dat_i(1 downto 0) = "11" then 
                  nextate <= mif1;
               elsif mem_full_i = '1' then
                  fif_err <= '1';    -- early error, don't ack
                  nextate <= error;
               else
                  adk     <= '1';    -- write early addrs ack
                  nextate <= wr1;
               end if;
            end if;
         end if;

      when wr1 =>                    -- burst write
         if eb_bmx = '0' then        -- abort
            nextate <= abort;

         else                        -- wait until addressing done
            adk  <= NOT eb_eof_i;    -- ack continues till end of burst

            if  eb_as_i = '0' then   -- store data
               mem_wr_o <= '1';
               if eb_eof_i = '1' then
                  count_ld <= '1';   -- MIF count capture
                  eof <= '1';
                  nextate <= iddle;
               elsif mem_full_i = '1' then -- full FIFO
                  rw_err <= '1';     -- stop acknowledge
               end if;
            end if;
         end if;

      when rd1 =>                    -- burst read 
         if eb_bmx = '0' then        -- abort
            nextate <= abort;
         else                        -- read data
            if eb_eof_i = '1' then
               eof <= '1';
               nextate <= iddle;
            elsif emti2 = '1' then   -- empty FIFO
               rw_err <= '1';        -- stop acknowledge
            else
               adk <= '1';           -- data ack
               if eb_aef_i = '0' then-- check when 2-stage pipeline stops
                  mem_rd_j <= '1';
               end if;
            end if;
         end if;

     when bcc1 =>                    -- broad-cast/call
         if eb_bmx = '0' OR eb_eof_i = '1' then
            eof <= '1';
            nextate <= iddle;
         end if;

      when mif1 =>                   -- MIF command
         if NOT (eb_dat_i(3 downto 0) = "0001") 
            AND eb_eof_i = '1' then 
            msg_clr <= '1';          -- close message channel, but OPEN command
         end if;

        if eb_dat_i(3 downto 0) = "1111"  
            AND eb_eof_i = '1' then 
            rst_on <= '1';           -- RESET command
            nextate <= mif2;
         elsif eb_bmx_i = '0' then   -- wait for end of burst       
            eof <= '1';
            nextate <= iddle;
         else
            adk  <= NOT eb_eof_i;    -- ack continues till end of burst
         end if;

      when mif2 =>                   -- MIF reset
         if eb_bmx_i = '0' then      -- wait for end of burst       
            eof <= '1';
            nextate <= mif3;
         else
            adk  <= NOT eb_eof_i;    -- ack continues till end of burst
         end if;
      
      when mif3 =>                   -- MIF reset
         nextate <= mif4;
      
      when mif4 =>                   -- MIF reset
         nextate <= iddle;
      
      when error =>                  -- early FIFO error in addressing phase
         if eb_bmx = '1' then        -- wait for end of burst 
            fif_err <= '1';          -- asking for retry
         else
            eof <= '1';
            nextate <= iddle;
         end if;

      when abort =>                   -- quit on abort
         eof <= '1';
         nextate <= iddle;

   end case;

end process;

process	(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then
      emti1 <= mem_empty_i;   -- empty status delayed
      emti2 <= emti1;         -- empty status delayed
   end if;
end process;

-- Message management
---------------------
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then 
      if eb_rst_i = '1' then
         msg_run <= (others => '0');
      elsif ofld = '1' then 
         if msg_run = "0000" then
            msg_run <= msg_lsb;
         end if;
      elsif msg_clr = '1' then
         msg_run <= (others => '0');
      end if;
   end if;
end process;

process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then 
      if eb_rst_i = '1' OR eb_eof_i = '1' then
         msg_err <= '0';
      elsif ofld = '1' then 
         if    msg_lsb /= "0000"       -- clear is OK anyway
           AND msg_run /= "0000"       -- already locked by an other MID
           AND msg_lsb /= msg_run then -- reject new MID
            msg_err <= '1';
         end if;
      end if;
   end if;
end process;

ebx_msg_dst_o <= msg_run;

process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then 
      ebx_msg_dat_o <= mem_full_i & mem_empty_i & -- MSbits are flags
                       mem_rd_cnt_i(13 downto 0); -- message data is fifo occupancy
   end if;
end process;

-- FIFO interface
-----------------
mem_din_o <= eb_dat_i;

mem_rd_o  <=     mem_rd_j when eb_as_i = '1' -- single data burst
            else mem_rd_j AND NOT eb_aef_i;  -- multiple data burst

process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then 
      if count_ld = '1' then
         mem_count_o <= eb_dat_i(15 downto 0);
      end if;

      if rst_off = '1' then
         mem_rst_o <= '0';
      elsif rst_on = '1' then
         mem_rst_o <= '1';
      end if;
   end if;
end process;

-- Tell word size
-----------------
ebx_dsz_o <= std_logic_vector(to_unsigned(EBS_DWLOG-1, 4)) 
             when myself = '1' AND eb_as_i = '1'
             else (others => '0');

-- E-bone drivers
-----------------
process(eb_clk_i)
begin
   if rising_edge(eb_clk_i) then 
      s_dk  <= adk AND NOT  rw_err; -- routing help
   end if;
end process;

eb_err_o  <= fif_err OR msg_err when myself = '1' else '0';
eb_dk_o   <= s_dk OR fif_err OR msg_err when myself = '1' else '0';
eb_dat_o  <= mem_dout_i when myself = '1' else (others => '0'); 

end rtl;
