--------------------------------------------------------------------------
--
-- E-bone - FIFO interface
-- 256 bit wrapper
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     2.0  30/09/13    herve  Record i/o
--     2.1  21/06/15    herve  Added MIF open command
--     2.2  15/06/16    herve  Message glitch fixed
--
---------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.ebs_pkg.all;
use work.ebs_fifo_pkg.all;

entity ebs_fifo_256 is
generic (
   EBS_AD_RNGE  : natural := 16; -- adressing range
   EBS_AD_BASE  : natural := 2;  -- usual memory segment
   EBS_AD_SIZE  : natural := 4;  -- size in segment
   EBS_AD_OFST  : natural := 0   -- offset in segment
);
port (
-- E-bone interface
   eb_clk_i    : in  std_logic;  -- system clock
   ebs_i       : in  ebs256_i_typ; 
   ebs_o       : out ebs256_o_typ; 
   ebx_o       : out ebx_o_typ; 
   ebx_msg_set_i : in  std8;     -- message management

-- FIFO memory interface
   mem_count_o : out std16;      -- MIF requested count
   mem_din_o   : out std256;     -- FIFO data in
   mem_dout_i  : in  std256;     -- FIFO data out
   mem_wr_o    : out std_logic;  -- FIFO write enable
   mem_rd_o    : out std_logic;  -- FIFO read enable
   mem_rst_o   : out std_logic;  -- FIFO reset 
   mem_empty_i : in std_logic;   -- FIFO empty status
   mem_full_i  : in std_logic;   -- FIFO full status
   mem_rd_cnt_i: in  std16       -- FIFO occupancy (may ground, but message in use)
);
end ebs_fifo_256;

--------------------------------------------------------------------------
architecture rtl of ebs_fifo_256 is
--------------------------------------------------------------------------
constant EBS_DWIDTH: natural := 256; 

begin

eb_fifo: ebs_fifo
generic map (
   EBS_AD_RNGE  => EBS_AD_RNGE, 
   EBS_AD_BASE  => EBS_AD_BASE,
   EBS_AD_SIZE  => EBS_AD_SIZE, 
   EBS_AD_OFST  => EBS_AD_OFST, 
   EBS_DWIDTH   => EBS_DWIDTH  
)
port map (
-- E-bone interface
   eb_clk_i    => eb_clk_i,  
   eb_rst_i    => ebs_i.eb_rst, 
   eb_bmx_i    => ebs_i.eb_bmx, 
   eb_as_i     => ebs_i.eb_as, 
   eb_eof_i    => ebs_i.eb_eof,
   eb_aef_i    => ebs_i.eb_aef,
   eb_dat_i    => ebs_i.eb_dat, 
   eb_dk_o     => ebs_o.eb_dk, 
   eb_err_o    => ebs_o.eb_err, 
   eb_dat_o    => ebs_o.eb_dat,

-- E-bone extension
   ebx_msg_set_i => ebx_msg_set_i,
   ebx_dsz_o     => ebx_o.ebx_dsz, 
   ebx_msg_dst_o => ebx_o.ebx_msg_dst, 
   ebx_msg_dat_o => ebx_o.ebx_msg_dat, 

-- FIFO memory interface
   mem_count_o  => mem_count_o,
   mem_din_o    => mem_din_o,
   mem_dout_i   => mem_dout_i,
   mem_wr_o     => mem_wr_o,
   mem_rd_o     => mem_rd_o,
   mem_rst_o    => mem_rst_o,
   mem_empty_i  => mem_empty_i,
   mem_full_i   => mem_full_i,
   mem_rd_cnt_i => mem_rd_cnt_i
);

end rtl;

