--------------------------------------------------------------------------
--
-- E-bone - BRAM memory interface package
--
--------------------------------------------------------------------------
--
-- Version  Date       Author  Comment
--     1.0  21/12/12    herve  Created from ebs_ram
--                             Message support (E-bone 1.3)
--     1.1  11/02/13    herve  data width reporting
--     2.0  03/02/14    herve  Record i/o, pipelined inputs
--                             Message data extended to 16 bits
--     2.1  21/06/15    herve  Added MIF open command
--     2.2  15/06/16    herve  Message glitch fixed
--
---------------------------------------------------------------
-- (C) http://www.esrf.fr
-- This file and related is part of E-bone
-- E-bone is distributed under the terms of the GPL License.
-- See <http://www.gnu.org/licenses/>
--------------------------------------------------------------------------
-- Declare the components
-- 'ebs_fifo' and its wrappers
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.ebs_pkg.all;

package ebs_fifo_pkg is

component ebs_fifo_32 is
generic (
   EBS_AD_RNGE  : natural := 16; -- adressing range
   EBS_AD_BASE  : natural := 2;  -- usual memory segment
   EBS_AD_SIZE  : natural := 4;  -- size in segment
   EBS_AD_OFST  : natural := 0   -- offset in segment
);
port (
-- E-bone interface
   eb_clk_i    : in  std_logic;  -- system clock
   ebs_i       : in  ebs32_i_typ; 
   ebs_o       : out ebs32_o_typ; 
   ebx_o       : out ebx_o_typ; 
   ebx_msg_set_i : in  std8;     -- message management

-- FIFO memory interface
   mem_count_o : out std16;      -- MIF requested count
   mem_din_o   : out std32;      -- FIFO data in
   mem_dout_i  : in  std32;      -- FIFO data out
   mem_wr_o    : out std_logic;  -- FIFO write enable
   mem_rd_o    : out std_logic;  -- FIFO read enable
   mem_rst_o   : out std_logic;  -- FIFO reset 
   mem_empty_i : in  std_logic;  -- FIFO empty status
   mem_full_i  : in  std_logic;  -- FIFO full status
   mem_rd_cnt_i: in  std16       -- FIFO occupancy (may ground, but message in use)
);
end component;

component ebs_fifo_64 is
generic (
   EBS_AD_RNGE  : natural := 16; -- adressing range
   EBS_AD_BASE  : natural := 2;  -- usual memory segment
   EBS_AD_SIZE  : natural := 4;  -- size in segment
   EBS_AD_OFST  : natural := 0   -- offset in segment
);
port (
-- E-bone interface
   eb_clk_i    : in  std_logic;  -- system clock
   ebs_i       : in  ebs64_i_typ; 
   ebs_o       : out ebs64_o_typ; 
   ebx_o       : out ebx_o_typ; 
   ebx_msg_set_i : in  std8;     -- message management

-- FIFO memory interface
   mem_count_o : out std16;      -- MIF requested count
   mem_din_o   : out std64;      -- FIFO data in
   mem_dout_i  : in  std64;      -- FIFO data out
   mem_wr_o    : out std_logic;  -- FIFO write enable
   mem_rd_o    : out std_logic;  -- FIFO read enable
   mem_rst_o   : out std_logic;  -- FIFO reset 
   mem_empty_i : in  std_logic;  -- FIFO empty status
   mem_full_i  : in  std_logic;  -- FIFO full status
   mem_rd_cnt_i: in  std16       -- FIFO occupancy
);
end component;

component ebs_fifo_128 is
generic (
   EBS_AD_RNGE  : natural := 16; -- adressing range
   EBS_AD_BASE  : natural := 2;  -- usual memory segment
   EBS_AD_SIZE  : natural := 4;  -- size in segment
   EBS_AD_OFST  : natural := 0   -- offset in segment
);
port (
-- E-bone interface
   eb_clk_i    : in  std_logic;  -- system clock
   ebs_i       : in  ebs128_i_typ; 
   ebs_o       : out ebs128_o_typ; 
   ebx_o       : out ebx_o_typ; 
   ebx_msg_set_i : in  std8;     -- message management

-- FIFO memory interface
   mem_count_o : out std16;      -- MIF requested count
   mem_din_o   : out std128;     -- FIFO data in
   mem_dout_i  : in  std128;     -- FIFO data out
   mem_wr_o    : out std_logic;  -- FIFO write enable
   mem_rd_o    : out std_logic;  -- FIFO read enable
   mem_rst_o   : out std_logic;  -- FIFO reset 
   mem_empty_i : in  std_logic;  -- FIFO empty status
   mem_full_i  : in  std_logic;  -- FIFO full status
   mem_rd_cnt_i: in  std16       -- FIFO occupancy
);
end component;

component ebs_fifo_256 is
generic (
   EBS_AD_RNGE  : natural := 16; -- adressing range
   EBS_AD_BASE  : natural := 2;  -- usual memory segment
   EBS_AD_SIZE  : natural := 4;  -- size in segment
   EBS_AD_OFST  : natural := 0   -- offset in segment
);
port (
-- E-bone interface
   eb_clk_i    : in  std_logic;  -- system clock
   ebs_i       : in  ebs256_i_typ; 
   ebs_o       : out ebs256_o_typ; 
   ebx_o       : out ebx_o_typ; 
   ebx_msg_set_i : in  std8;     -- message management

-- FIFO memory interface
   mem_count_o : out std16;      -- MIF requested count
   mem_din_o   : out std256;     -- FIFO data in
   mem_dout_i  : in  std256;     -- FIFO data out
   mem_wr_o    : out std_logic;  -- FIFO write enable
   mem_rd_o    : out std_logic;  -- FIFO read enable
   mem_rst_o   : out std_logic;  -- FIFO reset 
   mem_empty_i : in  std_logic;  -- FIFO empty status
   mem_full_i  : in  std_logic;  -- FIFO full status
   mem_rd_cnt_i: in  std16       -- FIFO occupancy
);
end component;

component ebs_fifo 
generic (
   EBS_AD_RNGE  : natural := 16;  -- adressing range
   EBS_AD_BASE  : natural := 2;   -- usual memory segment
   EBS_AD_SIZE  : natural := 4;   -- size in segment
   EBS_AD_OFST  : natural := 0;   -- offset in segment
   EBS_DWIDTH   : natural := 32   -- fifo (E-bone) data width 
);
port (
-- E-bone interface
   eb_clk_i    : in  std_logic;  -- system clock
   eb_rst_i    : in  std_logic;  -- synchronous system reset

   eb_bmx_i    : in  std_logic;  -- busy others
   eb_as_i     : in  std_logic;  -- adrs strobe
   eb_eof_i    : in  std_logic;  -- end of frame
   eb_aef_i    : in  std_logic;  -- almost end of frame
   eb_dat_i    : in  std_logic_vector(EBS_DWIDTH-1 downto 0); -- data write
   eb_dk_o     : out std_logic;  -- data acknowledge
   eb_err_o    : out std_logic;  -- bus error
   eb_dat_o    : out std_logic_vector(EBS_DWIDTH-1 downto 0); -- data read

-- E-bone extension
   ebx_msg_set_i : in  std8;     -- message management
   ebx_dsz_o     : out std4;     -- device data size
   ebx_msg_dst_o : out std4;     -- message destination
   ebx_msg_dat_o : out std16;    -- message data

-- FIFO memory interface
   mem_count_o : out std16;      -- MIF requested count
   mem_din_o   : out std_logic_vector(EBS_DWIDTH-1 downto 0); -- FIFO data in
   mem_dout_i  : in  std_logic_vector(EBS_DWIDTH-1 downto 0); -- FIFO data out
   mem_wr_o    : out std_logic;  -- FIFO write enable
   mem_rd_o    : out std_logic;  -- FIFO read enable
   mem_rst_o   : out std_logic;  -- FIFO reset 
   mem_empty_i : in  std_logic;  -- FIFO empty status
   mem_full_i  : in  std_logic;  -- FIFO full status
   mem_rd_cnt_i: in  std16       -- FIFO occupancy
);
end component;
end package ebs_fifo_pkg;
