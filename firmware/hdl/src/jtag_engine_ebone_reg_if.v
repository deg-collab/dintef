module jtag_engine_ebone_reg_if(
  input        clk,
  input        res_n,
  input        data_out_read,
  output       data_out_valid,
  output [7:0] data_out,            // data to PC
  output       data_in_busy,        // full of internal FIFO
  input        data_in_write,
  input  [7:0] data_in,             // data from PC

  output       tck,
  output       tms,
  output       td_out_to_asic,
  input        td_in_from_asic,

  input        ext_make_idle,
  output       is_idle
);

wire empty;
wire almost_empty;
wire almost_full;



//AsyncDeviceFifo_65536 fifo_I(
//  .rst(!res_n),
//  .wr_clk(clk),
//  .rd_clk(clk),
//  .din(data_in),
//  .wr_en(fifo_write),
//  .rd_en(fifo_read),
//  .dout(data_out),
//  .full(data_in_busy),
//  .almost_full(almost_full),
//  .empty(empty),
//  .almost_empty(almost_empty)
//);

//assign tck = 0'b0;
//assign tms = 0'b0;
//assign td_out_to_asic = 0'b0;
//
//assign data_out_valid = !empty;


// Reading from the PC:
// - connect data_out / data_out_valid to ebone read only register
// - connect data_out_read to read write register, module reads data from
// - read data from PC when data_out_valid
//
// Writing from the PC:
// - check if data_in_busy=1
// - apply data_in and data_in_valid=1
// - toggle data_in_valid to 0
// - data is written on the rising edge of data_in_valid
reg data_out_read_regged, data_in_write_regged;

always @(posedge clk or negedge res_n) begin           // only write on rising edge of write signal
  if (!res_n) begin
    data_in_write_regged = 1'b0;
    data_out_read_regged = 1'b0;
  end else begin
    data_in_write_regged <= data_in_write;
    data_out_read_regged <= data_out_read;
  end
end

wire fifo_write = !data_in_write_regged && data_in_write && !data_in_busy;
wire fifo_read  = !data_out_read_regged && data_out_read && data_out_valid;
 
jtag_engine jtag_engine_I(
  .clk(clk),
  .res_n(res_n),
  .busy(),
  .enable(1'b1),
  .ext_make_idle(ext_make_idle),
  .idle(is_idle),
  .o_current_state(),

  .if_clk( clk ),
  .if_res_n( res_n ),
  .iom_write( fifo_write ),
  .iom_data_to_dev( data_in ),
  .iom_busy( data_in_busy ),
  .iom_almost_busy(  ),
  .iom_read( fifo_read ),
  .iom_data_from_dev( data_out ),
  .iom_data_valid( data_out_valid ),

  .tck( tck ),
  .tms( tms ),
  .td_out_to_asic( td_out_to_asic ),
  .td_in_from_asic( td_in_from_asic )
);

endmodule
