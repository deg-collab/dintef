module packer_16_to_64_bit(
    input wire clk,
    input wire rst_n,
    input wire [15:0] data_in_16,
    input wire data_in_valid,
    
    output reg [63:0] data_out_64,
    output reg data_out_valid
);


reg [2:0] data_out_valid_cnt;


always @(posedge clk or negedge rst_n) begin
    if(rst_n == 1'b0) begin
        data_out_64 <= 64'b0;
    end
    else if(clk) begin
        if(data_in_valid == 1'b1) begin
            data_out_64 <= data_in_16 & data_out_64[63:16]; 
        end;
    end;
end;

always @(posedge clk or negedge rst_n) begin
    if(rst_n == 1'b0) begin 
        data_out_valid_cnt <= 1'b0;
        data_out_valid <= 1'b0;
    end
    else if(clk) begin
        if(data_out_valid_cnt == 3'd4) begin  
            data_out_valid_cnt <= 1'b0;
            data_out_valid <= 1'b1;
        end
        else begin
            data_out_valid <= 1'b0;
            if(data_in_valid == 1'b1) begin
                data_out_valid_cnt <= data_out_valid_cnt + 1;
            end;
        end;
    end;
end;

endmodule

