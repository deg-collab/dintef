library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use IEEE.numeric_std.all;

use work.ebs_pkg.all;
use work.eb_common_pkg.all;
use work.ebs_regs_pkg.all;
use work.dblock_dmak_pkg.all;


package sus_top_pkg is


------------------------------
-- Components declaration area
------------------------------

------------------------------------------------------
component sus_top  -- User data flow emulator
------------------------------------------------------
generic (
  EBS_AD_RNGE          : NATURAL := 16;                                -- Number of address bits to decode. Must be the same for all slaves mapped in the same segment
  EBFT_DWIDTH          : NATURAL := 64                                 -- FT (DMA) data width
);

port (
-- EBONE INTERFACE ----------------------------------------------------
  eb_clk_i                          : in std_logic;
  ebs_i                             : in ebs32_i_typ;
  ebs_o                             : out ebs32_oa_typ(EBS_SLV_NB downto 1);


-- JTAG ---------------------------------------------------------------
  sus_jtag_wrapper_clk              : in std_logic;  

  sus_jtag_wrapper_rst_n            : out std_logic;

  sus_jtag_wrapper_tck              : out std_logic;
  sus_jtag_wrapper_tms              : out std_logic;
  sus_jtag_wrapper_td_out_to_asic   : out std_logic;
  sus_jtag_wrapper_td_in_from_asic  : in std_logic;


-- Dynamic ASIC Control ------------------------------------------------

  sus_dyn_data_out_valid          : out std_logic;
  sus_dyn_data_out_64             : out std_logic_vector(EBFT_DWIDTH-1 downto 0);

  sus_dyn_asic_run                : out std_logic;
  sus_dyn_asic_ser0_dout          : in std_logic;
  sus_dyn_asic_ser1_dout          : in std_logic;

  sus_dyn_asic_res_n              : out std_logic;
  
  sus_dyn_asic_mon                : in std_logic;

  sus_dyn_pll_clk                 : in std_logic;
  sus_dyn_pll_clk_by5             : out std_logic;

  sus_dyn_cycle_done              : out std_logic;
  sus_dyn_cycle_done_delayed      : out std_logic;

  daq_fifo_wr_full                : in std_logic;



-- T3 Controller & Telegram ------------------------------------------------

  sus_T3_telegram_cmd_to_asic     : out std_logic;
  sus_T3_data_from_asic           : in std_logic;

  sus_T3_tele_fifo_wclk           : out std_logic


);


end component;


end package sus_top_pkg;
