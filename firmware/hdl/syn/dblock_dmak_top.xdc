##################################################################################################################################
#
# Constraints file necst_top.xdc
#
# FPGA family       : 7
# FPGA device       : xc7k160t
# Speedgrade        : -2
# Package           : fbg676c
# DBRIDGE Connector : BANK12, VCCO = +VAPP
#                     BANK13, VCCO = +2.5V
#                     BANK14, VCCO = +2.5V
#                     BANK15, VCCO = +VADJ
#                     BANK16, VCCO = +VADJ
#
# This file (example) is only valid with the DBLOCK Carrier Card built around the device listed above
# All P/N are differential IO standards
#
##################################################################################################################################

create_clock -name db_sys_clk -period 5.000 [get_pins {IBUFG_CLK2_i/O}]

set_property PACKAGE_PIN J11    [get_ports {LA2_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA2_P}]
set_property DIFF_TERM TRUE     [get_ports {LA2_P}]
set_property PACKAGE_PIN J10    [get_ports {LA2_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA2_N}]
set_property DIFF_TERM TRUE     [get_ports {LA2_N}]

set_property PACKAGE_PIN J13    [get_ports {LA4_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA4_P}]
set_property DIFF_TERM TRUE     [get_ports {LA4_P}]
set_property PACKAGE_PIN H13    [get_ports {LA4_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA4_N}]
set_property DIFF_TERM TRUE     [get_ports {LA4_N}]

set_property PACKAGE_PIN J15    [get_ports {LA7_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA7_P}]
set_property DIFF_TERM TRUE     [get_ports {LA7_P}]
set_property PACKAGE_PIN J16    [get_ports {LA7_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA7_N}]
set_property DIFF_TERM TRUE     [get_ports {LA7_N}]

set_property PACKAGE_PIN H16    [get_ports {LA8_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA8_P}]
set_property DIFF_TERM TRUE     [get_ports {LA8_P}]
set_property PACKAGE_PIN G16    [get_ports {LA8_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA8_N}]
set_property DIFF_TERM TRUE     [get_ports {LA8_N}]


set_property PACKAGE_PIN B15    [get_ports {LA31_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA31_P}]
set_property DIFF_TERM TRUE     [get_ports {LA31_P}]
set_property PACKAGE_PIN A15    [get_ports {LA31_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA31_N}]
set_property DIFF_TERM TRUE     [get_ports {LA31_N}]

set_property PACKAGE_PIN A18    [get_ports {LA33_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA33_P}]
set_property DIFF_TERM TRUE     [get_ports {LA33_P}]
set_property PACKAGE_PIN A19    [get_ports {LA33_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA33_N}]
set_property DIFF_TERM TRUE     [get_ports {LA33_N}]


set_property PACKAGE_PIN G15    [get_ports {LA5_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA5_P}]
set_property PACKAGE_PIN F15    [get_ports {LA5_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA5_N}]

set_property PACKAGE_PIN L17    [get_ports {LA9_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA9_P}]
set_property PACKAGE_PIN K18    [get_ports {LA9_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA9_N}]

set_property PACKAGE_PIN E13    [get_ports {LA11_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA11_P}]
set_property PACKAGE_PIN E12    [get_ports {LA11_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA11_N}]

#set_property PACKAGE_PIN J18    [get_ports {LA13_P}]
#set_property IOSTANDARD LVDS_25 [get_ports {LA13_P}]
#set_property PACKAGE_PIN J19    [get_ports {LA13_N}]
#set_property IOSTANDARD LVDS_25 [get_ports {LA13_N}]


set_property PACKAGE_PIN L19    [get_ports {LA14_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA14_P}]
set_property PACKAGE_PIN L20    [get_ports {LA14_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA14_N}]

set_property PACKAGE_PIN G10    [get_ports {LA15_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA15_P}]
set_property PACKAGE_PIN G9     [get_ports {LA15_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA15_N}]

set_property PACKAGE_PIN C9     [get_ports {LA19_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA19_P}]
set_property PACKAGE_PIN B9     [get_ports {LA19_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA19_N}]

# set_property PACKAGE_PIN A9     [get_ports {LA20_P}]
# set_property IOSTANDARD LVDS_25 [get_ports {LA20_P}]
# set_property PACKAGE_PIN A8     [get_ports {LA20_N}]
# set_property IOSTANDARD LVDS_25 [get_ports {LA20_N}]
#
# set_property PACKAGE_PIN B10    [get_ports {LA22_P}]
# set_property IOSTANDARD LVDS_25 [get_ports {LA22_P}]
# set_property PACKAGE_PIN A10    [get_ports {LA22_N}]
# set_property IOSTANDARD LVDS_25 [get_ports {LA22_N}]
#
# set_property PACKAGE_PIN A13    [get_ports {LA25_P}]
# set_property IOSTANDARD LVDS_25 [get_ports {LA25_P}]
# set_property PACKAGE_PIN A12    [get_ports {LA25_N}]
# set_property IOSTANDARD LVDS_25 [get_ports {LA25_N}]
#
# set_property PACKAGE_PIN B14    [get_ports {LA29_P}]
# set_property IOSTANDARD LVDS_25 [get_ports {LA29_P}]
# set_property PACKAGE_PIN A14    [get_ports {LA29_N}]
# set_property IOSTANDARD LVDS_25 [get_ports {LA29_N}]


set_property PACKAGE_PIN C19    [get_ports {LA30_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA30_P}]
set_property PACKAGE_PIN B19    [get_ports {LA30_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA30_N}]

set_property PACKAGE_PIN D19    [get_ports {LA32_P}]
set_property IOSTANDARD LVDS_25 [get_ports {LA32_P}]
set_property PACKAGE_PIN D20    [get_ports {LA32_N}]
set_property IOSTANDARD LVDS_25 [get_ports {LA32_N}]




set_property PACKAGE_PIN E25     [get_ports {DB_IO_2V5[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5[0]}]
set_property PACKAGE_PIN D26     [get_ports {DB_IO_2V5[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5[1]}]
set_property PACKAGE_PIN A23     [get_ports {DB_IO_2V5[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5[2]}]
set_property PACKAGE_PIN A24     [get_ports {DB_IO_2V5[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5[3]}]
set_property PACKAGE_PIN K21     [get_ports {DB_IO_2V5[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5[4]}]
set_property PACKAGE_PIN F23     [get_ports {DB_IO_2V5[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5[5]}]
set_property PACKAGE_PIN D21     [get_ports {DB_IO_2V5[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5[6]}]
set_property PACKAGE_PIN C24     [get_ports {DB_IO_2V5[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5[7]}]
set_property PACKAGE_PIN B21     [get_ports {DB_IO_2V5[8]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5[8]}]
set_property PACKAGE_PIN C21     [get_ports {DB_IO_2V5[9]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5[9]}]

set_property PACKAGE_PIN U21     [get_ports {DB_IO_VAPP[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_VAPP[0]}]

set_property PACKAGE_PIN T20    [get_ports {DB_IO_2V5_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {DB_IO_2V5_P[0]}]
set_property DIFF_TERM TRUE     [get_ports {DB_IO_2V5_P[0]}]
set_property PACKAGE_PIN R20    [get_ports {DB_IO_2V5_N[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {DB_IO_2V5_N[0]}]
set_property DIFF_TERM TRUE     [get_ports {DB_IO_2V5_N[0]}]

set_property PACKAGE_PIN T18     [get_ports {DB_IO_2V5_P[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5_P[1]}]
set_property PACKAGE_PIN T19     [get_ports {DB_IO_2V5_N[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5_N[1]}]

set_property PACKAGE_PIN N18     [get_ports {DB_IO_2V5_P[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5_P[2]}]
set_property PACKAGE_PIN M19     [get_ports {DB_IO_2V5_N[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5_N[2]}]

set_property PACKAGE_PIN T22    [get_ports {DB_IO_2V5_P[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {DB_IO_2V5_P[3]}]
set_property PACKAGE_PIN T23    [get_ports {DB_IO_2V5_N[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {DB_IO_2V5_N[3]}]

set_property PACKAGE_PIN N19     [get_ports {DB_IO_2V5_P[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5_P[4]}]
set_property PACKAGE_PIN M20     [get_ports {DB_IO_2V5_N[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_IO_2V5_N[4]}]

set_property PACKAGE_PIN F25    [get_ports {DB_IO_2V5_P[19]}]
set_property IOSTANDARD LVDS_25 [get_ports {DB_IO_2V5_P[19]}]
set_property DIFF_TERM TRUE     [get_ports {DB_IO_2V5_P[19]}]
set_property PACKAGE_PIN E26    [get_ports {DB_IO_2V5_N[19]}]
set_property IOSTANDARD LVDS_25 [get_ports {DB_IO_2V5_N[19]}]
set_property DIFF_TERM TRUE     [get_ports {DB_IO_2V5_N[19]}]

set_property PACKAGE_PIN E21    [get_ports {DB_IO_2V5_P[21]}]
set_property IOSTANDARD LVDS_25 [get_ports {DB_IO_2V5_P[21]}]
set_property PACKAGE_PIN E22    [get_ports {DB_IO_2V5_N[21]}]
set_property IOSTANDARD LVDS_25 [get_ports {DB_IO_2V5_N[21]}]

set_property PACKAGE_PIN F19     [get_ports {GA[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GA[0]}]
set_property PACKAGE_PIN E20     [get_ports {GA[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GA[1]}]

set_property PACKAGE_PIN H12     [get_ports {PRSNT_M2C_L}]
set_property IOSTANDARD LVCMOS25 [get_ports {PRSNT_M2C_L}]

set_property PACKAGE_PIN Y20     [get_ports {TRST_L}]
set_property IOSTANDARD LVCMOS25 [get_ports {TRST_L}]

set_property PACKAGE_PIN Y22    [get_ports {DB_IO_MRCC_VAPP_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {DB_IO_MRCC_VAPP_P[0]}]
set_property DIFF_TERM TRUE     [get_ports {DB_IO_MRCC_VAPP_P[0]}]
set_property PACKAGE_PIN AA22   [get_ports {DB_IO_MRCC_VAPP_N[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {DB_IO_MRCC_VAPP_N[0]}]
set_property DIFF_TERM TRUE     [get_ports {DB_IO_MRCC_VAPP_N[0]}]

# DB_DLINK A
# ----------
set_property PACKAGE_PIN H22     [get_ports {DB_DLINK_ACT}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_DLINK_ACT}]
set_property PACKAGE_PIN J25     [get_ports {DB_DLINK_TXEN}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_DLINK_TXEN}]
set_property PACKAGE_PIN J21     [get_ports {DB_DLINK_TX[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_DLINK_TX[0]}]
set_property PACKAGE_PIN J24     [get_ports {DB_DLINK_TX[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_DLINK_TX[1]}]
set_property PACKAGE_PIN K22     [get_ports {DB_DLINK_RXEN}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_DLINK_RXEN}]
set_property PACKAGE_PIN L22     [get_ports {DB_DLINK_RX[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_DLINK_RX[0]}]
set_property PACKAGE_PIN K23     [get_ports {DB_DLINK_RX[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_DLINK_RX[1]}]

# set_clock_groups -physically_exclusive \
# -group [get_clocks -include_generated_clock ep_txout_clk] \
# -group [get_clocks -include_generated_clock db_sys_clk] \
# -group {spi_clk}

##################################################################################################################################
