# usage:
# $ viva dblock_dmak_top

# Reading ebone and top sources
# -----------------------------
read_verilog [glob ../src/ebone/ebm0_pcie_b/src/core_pcie_s7/*.v]
read_vhdl    [glob ../src/ebone/ebm0_pcie_b/src/core_pcie_s7/*.vhd]
read_vhdl    [glob ../src/ebone/ebm0_pcie_b/src/*.vhd]
read_vhdl    [glob ../src/ebone/ebs_pkg/src/*.vhd]
read_vhdl    [glob ../src/ebone/ebs_fspi/src/*.vhd]
read_vhdl    [glob ../src/ebone/ebs_sysm/src/*.vhd]
read_vhdl    [glob ../src/ebone/ebs_regs/src/*.vhd]
read_vhdl    [glob ../src/ebone/ebs_fifo/src/*.vhd]
read_vhdl    [glob ../src/ebone/ebm_ebft/src/*.vhd]
read_vhdl    [glob ../src/ebone/eb_common/src/*.vhd]
read_verilog [glob ../ip/mmcm_user_v6_0/mmcm_user_v6_0/mmcm_user_v6_0_clk_wiz.v]
read_verilog [glob ../ip/mmcm_user_v6_0/mmcm_user_v6_0/mmcm_user_v6_0.v]

read_vhdl    [glob ../ip/AsyncDeviceFifo_65536/hdl/*.vhd]
read_verilog [glob ../ip/AsyncDeviceFifo_65536/hdl/*.v]
read_vhdl    [glob ../src/common/*.vhd]
read_verilog [glob ../src/sus/*.v]
read_vhdl    [glob ../src/sus/sus_top.vhd]

# Setting target
# --------------
set_property part xc7k160tfbg676-2 [current_project]

# Setting constraints
# -------------------
read_xdc dblock_dmak_top.xdc
read_xdc dblock_I2C.xdc
read_xdc dblock_top.xdc

# Synthesis
synth_design -top dblock_dmak_top -keep_equivalent_registers

write_checkpoint -force dblock_dmak_top_syn.dcp

# Place
opt_design -propconst -sweep -remap
place_design -directive Explore
report_clock_utilization -file rpt_clocks.txt
report_clocks -file rpt_clocks.txt -append
report_utilization -file rpt_place.txt

# Route
route_design -directive Explore
write_checkpoint -force par.dcp
report_route_status -file rpt_route.txt
report_timing_summary -file rpt_timing.txt -report_unconstrained
report_io -file rpt_io.txt

open_checkpoint par.dcp

# bitstream
set_property BITSTREAM.CONFIG.CONFIGRATE 16 [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
write_bitstream -force dblock_dmak_top.bit

# configuration memory file
write_cfgmem -format mcs -interface spix4 -size 32 -loadbit "up 0 dblock_dmak_top.bit" -file dblock_dmak_top.mcs -force
