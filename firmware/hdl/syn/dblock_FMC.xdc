##################################################################################################################################
#
# Constraints file dblock_FMC.xdc
#
# FPGA family       : 7
# FPGA device       : xc7k160t/xc7k325t
# Speedgrade        : -2
# Package           : fbg676c
# DBRIDGE Connector : BANK12, VCCO = +VCCAPP
#                     BANK13, VCCO = +2.5V
#                     BANK14, VCCO = +2.5V
#                     BANK15, VCCO = +VADJ
#                     BANK16, VCCO = +VADJ
#
# This file (example) is only valid with the DBLOCK Carrier Card built around the device listed above
# All P/N are differential IO standards
#
##################################################################################################################################

# ================================================================================================================================
# DBRIDGE FMC/LPC MEZZANINE PART, BANKs 15/16 VCCO = +VADJ
# ================================================================================================================================

# MGT, Physical layer
# -------------------
set_property PACKAGE_PIN H6 [get_ports {GBTCLK0_M2C_P}]
set_property PACKAGE_PIN H5 [get_ports {GBTCLK0_M2C_N}]

set_property PACKAGE_PIN R4 [get_ports {DP_M2C_P[0]}]
set_property PACKAGE_PIN R3 [get_ports {DP_M2C_N[0]}]
set_property PACKAGE_PIN P2 [get_ports {DP_C2M_P[0]}]
set_property PACKAGE_PIN P1 [get_ports {DP_C2M_N[0]}]

# MRCC Clocks (Multi Region Clock capable)
# ----------------------------------------
set_property PACKAGE_PIN E10    [get_ports {CLK_M2C_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {CLK_M2C_P[0]}]
set_property DIFF_TERM TRUE     [get_ports {CLK_M2C_P[0]}]
set_property PACKAGE_PIN D10    [get_ports {CLK_M2C_N[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {CLK_M2C_N[0]}]
set_property DIFF_TERM TRUE     [get_ports {CLK_M2C_N[0]}]

set_property PACKAGE_PIN C12    [get_ports {CLK_M2C_P[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {CLK_M2C_P[1]}]
set_property DIFF_TERM TRUE     [get_ports {CLK_M2C_P[1]}]
set_property PACKAGE_PIN C11    [get_ports {CLK_M2C_N[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {CLK_M2C_N[1]}]
set_property DIFF_TERM TRUE     [get_ports {CLK_M2C_N[1]}]

# Differential GP/IO
# ------------------
set_property PACKAGE_PIN G11    [get_ports {LA_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[0]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[0]}]
set_property PACKAGE_PIN F10    [get_ports {LA_N[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[0]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[0]}]

set_property PACKAGE_PIN G12    [get_ports {LA_P[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[1]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[1]}]
set_property PACKAGE_PIN F12    [get_ports {LA_N[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[1]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[1]}]

set_property PACKAGE_PIN J11    [get_ports {LA_P[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[2]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[2]}]
set_property PACKAGE_PIN J10    [get_ports {LA_N[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[2]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[2]}]

set_property PACKAGE_PIN F17    [get_ports {LA_P[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[3]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[3]}]
set_property PACKAGE_PIN E17    [get_ports {LA_N[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[3]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[3]}]

set_property PACKAGE_PIN J13    [get_ports {LA_P[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[4]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[4]}]
set_property PACKAGE_PIN H13    [get_ports {LA_N[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[4]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[4]}]

set_property PACKAGE_PIN G15    [get_ports {LA_P[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[5]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[5]}]
set_property PACKAGE_PIN F15    [get_ports {LA_N[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[5]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[5]}]

set_property PACKAGE_PIN H14    [get_ports {LA_P[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[6]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[6]}]
set_property PACKAGE_PIN G14    [get_ports {LA_N[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[6]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[6]}]

set_property PACKAGE_PIN J15    [get_ports {LA_P[7]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[7]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[7]}]
set_property PACKAGE_PIN J16    [get_ports {LA_N[7]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[7]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[7]}]

set_property PACKAGE_PIN H16    [get_ports {LA_P[8]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[8]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[8]}]
set_property PACKAGE_PIN G16    [get_ports {LA_N[8]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[8]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[8]}]

set_property PACKAGE_PIN L17    [get_ports {LA_P[9]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[9]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[9]}]
set_property PACKAGE_PIN K18    [get_ports {LA_N[9]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[9]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[9]}]

set_property PACKAGE_PIN K16    [get_ports {LA_P[10]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[10]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[10]}]
set_property PACKAGE_PIN K17    [get_ports {LA_N[10]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[10]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[10]}]

set_property PACKAGE_PIN E13    [get_ports {LA_P[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[11]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[11]}]
set_property PACKAGE_PIN E12    [get_ports {LA_N[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[11]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[11]}]

set_property PACKAGE_PIN F14    [get_ports {LA_P[12]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[12]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[12]}]
set_property PACKAGE_PIN F13    [get_ports {LA_N[12]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[12]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[12]}]

set_property PACKAGE_PIN J18    [get_ports {LA_P[13]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[13]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[13]}]
set_property PACKAGE_PIN J19    [get_ports {LA_N[13]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[13]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[13]}]

set_property PACKAGE_PIN L19    [get_ports {LA_P[14]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[14]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[14]}]
set_property PACKAGE_PIN L20    [get_ports {LA_N[14]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[14]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[14]}]

set_property PACKAGE_PIN G10    [get_ports {LA_P[15]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[15]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[15]}]
set_property PACKAGE_PIN G9     [get_ports {LA_N[15]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[15]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[15]}]

set_property PACKAGE_PIN H9     [get_ports {LA_P[16]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[16]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[16]}]
set_property PACKAGE_PIN H8     [get_ports {LA_N[16]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[16]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[16]}]

set_property PACKAGE_PIN F9     [get_ports {LA_P[17]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[17]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[17]}]
set_property PACKAGE_PIN F8     [get_ports {LA_N[17]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[17]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[17]}]

set_property PACKAGE_PIN D9     [get_ports {LA_P[18]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[18]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[18]}]
set_property PACKAGE_PIN D8     [get_ports {LA_N[18]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[18]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[18]}]

set_property PACKAGE_PIN C9     [get_ports {LA_P[19]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[19]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[19]}]
set_property PACKAGE_PIN B9     [get_ports {LA_N[19]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[19]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[19]}]

set_property PACKAGE_PIN A9     [get_ports {LA_P[20]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[20]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[20]}]
set_property PACKAGE_PIN A8     [get_ports {LA_N[20]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[20]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[20]}]

set_property PACKAGE_PIN G19    [get_ports {LA_P[21]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[21]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[21]}]
set_property PACKAGE_PIN F20    [get_ports {LA_N[21]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[21]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[21]}]

set_property PACKAGE_PIN B10    [get_ports {LA_P[22]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[22]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[22]}]
set_property PACKAGE_PIN A10    [get_ports {LA_N[22]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[22]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[22]}]

set_property PACKAGE_PIN E11    [get_ports {LA_P[23]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[23]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[23]}]
set_property PACKAGE_PIN D11    [get_ports {LA_N[23]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[23]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[23]}]

set_property PACKAGE_PIN E15    [get_ports {LA_P[24]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[24]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[24]}]
set_property PACKAGE_PIN E16    [get_ports {LA_N[24]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[24]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[24]}]

set_property PACKAGE_PIN A13    [get_ports {LA_P[25]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[25]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[25]}]
set_property PACKAGE_PIN A12    [get_ports {LA_N[25]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[25]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[25]}]

set_property PACKAGE_PIN B12    [get_ports {LA_P[26]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[26]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[26]}]
set_property PACKAGE_PIN B11    [get_ports {LA_N[26]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[26]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[26]}]

set_property PACKAGE_PIN D14    [get_ports {LA_P[27]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[27]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[27]}]
set_property PACKAGE_PIN D13    [get_ports {LA_N[27]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[27]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[27]}]

set_property PACKAGE_PIN C17    [get_ports {LA_P[28]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[28]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[28]}]
set_property PACKAGE_PIN C18    [get_ports {LA_N[28]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[28]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[28]}]

set_property PACKAGE_PIN B14    [get_ports {LA_P[29]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[29]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[29]}]
set_property PACKAGE_PIN A14    [get_ports {LA_N[29]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[29]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[29]}]

set_property PACKAGE_PIN C19    [get_ports {LA_P[30]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[30]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[30]}]
set_property PACKAGE_PIN B19    [get_ports {LA_N[30]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[30]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[30]}]

set_property PACKAGE_PIN B15    [get_ports {LA_P[31]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[31]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[31]}]
set_property PACKAGE_PIN A15    [get_ports {LA_N[31]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[31]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[31]}]

set_property PACKAGE_PIN D19    [get_ports {LA_P[32]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[32]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[32]}]
set_property PACKAGE_PIN D20    [get_ports {LA_N[32]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[32]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[32]}]

set_property PACKAGE_PIN A18    [get_ports {LA_P[33]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_P[33]}]
set_property DIFF_TERM TRUE     [get_ports {LA_P[33]}]
set_property PACKAGE_PIN A19    [get_ports {LA_N[33]}]
set_property IOSTANDARD LVDS_25 [get_ports {LA_N[33]}]
set_property DIFF_TERM TRUE     [get_ports {LA_N[33]}]

set_property PACKAGE_PIN F19     [get_ports {GA[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GA[0]}]
set_property PACKAGE_PIN E20     [get_ports {GA[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GA[1]}]

set_property PACKAGE_PIN H12     [get_ports {PRSNT_M2C_L}]
set_property IOSTANDARD LVCMOS25 [get_ports {PRSNT_M2C_L}]

set_property PACKAGE_PIN B20     [get_ports {PG_C2M}]
set_property IOSTANDARD LVCMOS25 [get_ports {PG_C2M}]

##################################################################################################################################

