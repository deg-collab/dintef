##################################################################################################################################
#
# Constraints file dblock_top.xdc
#
# FPGA family      : 7
# FPGA device      : xc7k325t
# Speedgrade       : -2
# Package          : fbg676c
# Top requirements : FPGA I2C
#                    BANK 13, VCCO = +2.5V
#
# This file is only valid with the DBLOCK Carrier Card built around the device listed above
# All P/N are differential IO standards
#
##################################################################################################################################

# I2C, BANK 13, VCCO = 2.5V
# -------------------------
set_property PACKAGE_PIN P18     [get_ports {FPGA_I2C_EN}]
set_property IOSTANDARD LVCMOS25 [get_ports {FPGA_I2C_EN}]
set_property PACKAGE_PIN P20     [get_ports {FPGA_I2C_CLK}]
set_property IOSTANDARD LVCMOS25 [get_ports {FPGA_I2C_CLK}]
set_property PACKAGE_PIN M22     [get_ports {FPGA_I2C_DAT}]
set_property IOSTANDARD LVCMOS25 [get_ports {FPGA_I2C_DAT}]

##################################################################################################################################

