##################################################################################################################################
#
# Constraints file dblock_top.xdc
#
# FPGA family      : 7
# FPGA device      : xc7k325t
# Speedgrade       : -2
# Package          : fbg676c
# Top requirements : QSEVEN/PCIe, SPI/Flash memory, Misc
#                    BANK 13, VCCO = +2.5V
#                    BANK 14, VCCO = +2.5V
#
# This file is only valid with the DBLOCK Carrier Card built around the device listed above
# All P/N are differential IO standards
#
##################################################################################################################################

set_property BITSTREAM.CONFIG.CONFIGRATE 16 [current_design]
# set_property C_USER_SCAN_CHAIN 2 [get_debug_cores dbg_hub]

# PCIe 4 x Lanes
# --------------
set_false_path -through          [get_nets pcie_sys_rst_n]
set_property PACKAGE_PIN R18     [get_ports {pcie_sys_rst_n}]
set_property IOSTANDARD LVCMOS25 [get_ports {pcie_sys_rst_n}]
set_property PULLUP true         [get_ports {pcie_sys_rst_n}]

set_property PACKAGE_PIN K25     [get_ports {pcie_wke_n}]
set_property IOSTANDARD LVCMOS25 [get_ports {pcie_wke_n}]

# set_property PROHIBIT true [get_sites GTXE2_CHANNEL_X0Y4]
set_property PROHIBIT true [get_sites GTXE2_CHANNEL_X0Y5]
set_property PROHIBIT true [get_sites GTXE2_CHANNEL_X0Y6]
set_property PROHIBIT true [get_sites GTXE2_CHANNEL_X0Y7]

set_property LOC IBUFDS_GTE2_X0Y2 [get_cells {ebm0/s7_type.refclk_ibuf}]
set_property LOC GTXE2_CHANNEL_X0Y4 [get_cells {ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[0].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
# set_property LOC GTXE2_CHANNEL_X0Y5 [get_cells {ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[1].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
# set_property LOC GTXE2_CHANNEL_X0Y6 [get_cells {ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[2].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]
# set_property LOC GTXE2_CHANNEL_X0Y7 [get_cells {ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[3].gt_wrapper_i/gtx_channel.gtxe2_channel_i}]

create_clock -name ep_sys_clk -period 10.00 [get_pins {ebm0/s7_type.refclk_ibuf/O}]
create_clock -name ep_txout_clk -period 10.00 [get_pins ebm0/s7_type.s7_ep/gt_top_i/pipe_wrapper_i/pipe_lane[0].gt_wrapper_i/gtx_channel.gtxe2_channel_i/TXOUTCLK]

# PCIe BADR0, BANK 14, VCCO = 2.5V
# --------------------------------
set_property PACKAGE_PIN C26     [get_ports {soft_reset_n}]
set_property IOSTANDARD LVCMOS25 [get_ports {soft_reset_n}]
set_property PACKAGE_PIN D25     [get_ports {soft_wd_p}]
set_property IOSTANDARD LVCMOS25 [get_ports {soft_wd_p}]
set_property PACKAGE_PIN J23     [get_ports {status_led_p[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {status_led_p[0]}]
set_property PACKAGE_PIN L23     [get_ports {status_led_p[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {status_led_p[1]}]

# Miscelleaneous QSV Signals, BANK 13, VCCO = 2.5V
# ------------------------------------------------
set_property PACKAGE_PIN P19     [get_ports {WD_TRIG_N}]
set_property IOSTANDARD LVCMOS25 [get_ports {WD_TRIG_N}]
set_property PACKAGE_PIN M21     [get_ports {THRM_TRIP_N}]
set_property IOSTANDARD LVCMOS25 [get_ports {THRM_TRIP_N}]
set_property PACKAGE_PIN K26     [get_ports {THRM_N}]
set_property IOSTANDARD LVCMOS25 [get_ports {THRM_N}]

# QSV Reset, BANK 14, VCCO = 2.5V
# -------------------------------
set_property PACKAGE_PIN C22     [get_ports {QSV_RST_N}]
set_property IOSTANDARD LVCMOS25 [get_ports {QSV_RST_N}]

# ----------------
# SPI Flash Memory
# ----------------

# SPI Flash Bitstream, BANKs 14, VCCO = 2.5V
# ------------------------------------------
# SPI_CS
# ------
set_property PACKAGE_PIN C23     [get_ports {FLASH_CS}]
set_property IOSTANDARD LVCMOS25 [get_ports {FLASH_CS}]
set_property IOB TRUE            [get_ports {FLASH_CS}]
# SPI_MOSI
# --------
set_property PACKAGE_PIN B24     [get_ports {FLASH_MOSI}]
set_property IOSTANDARD LVCMOS25 [get_ports {FLASH_MOSI}]
set_property IOB TRUE            [get_ports {FLASH_MOSI}]
# SPI_MISO
# --------
set_property PACKAGE_PIN A25     [get_ports {FLASH_MISO}]
set_property IOSTANDARD LVCMOS25 [get_ports {FLASH_MISO}]
set_property IOB TRUE            [get_ports {FLASH_MISO}]
# SPI_WP
# ------
set_property PACKAGE_PIN B22     [get_ports {FLASH_WP}]
set_property IOSTANDARD LVCMOS25 [get_ports {FLASH_WP}]
# SPI_HOLD
# --------
set_property PACKAGE_PIN A22     [get_ports {FLASH_HOLD}]
set_property IOSTANDARD LVCMOS25 [get_ports {FLASH_HOLD}]

set_property LOC BUFR_X0Y10 [get_cells {fspi_i/ser_i/bufr_kx7_gene.bufr_inst}]
create_clock -name spi_clk -period 32.00 [get_pins {fspi_i/ser_i/bufr_kx7_gene.bufr_inst/O}]

#create_clock -period 2 [get_ports CLK_M2C_P]
#create_clock -period 2 [get_ports CLK_M2C_N]
create_clock -period 2 [get_ports LA4_P]
create_clock -period 2 [get_ports LA4_N]



# ----------
# Power Good
# ----------

# Carrier to FMC/LPC Mezzanine, BANK 14, VCCO = 2.5V
# --------------------------------------------------
set_property PACKAGE_PIN B20     [get_ports {PG_C2M}]
set_property IOSTANDARD LVCMOS25 [get_ports {PG_C2M}]

# Carrier to Application, BANK 14, VCCO = 2.5V
# --------------------------------------------
set_property PACKAGE_PIN A20     [get_ports {DB_PG_VAPP}]
set_property IOSTANDARD LVCMOS25 [get_ports {DB_PG_VAPP}]

# -------
# MEASURE
# -------

set_property PACKAGE_PIN N12     [get_ports {VP_in}]
set_property PACKAGE_PIN P11     [get_ports {VN_in}]
set_property PACKAGE_PIN C16     [get_ports {VAUXP0}]
set_property IOSTANDARD LVCMOS25 [get_ports {VAUXP0}]
set_property PACKAGE_PIN B16     [get_ports {VAUXN0}]
set_property IOSTANDARD LVCMOS25 [get_ports {VAUXN0}]
set_property PACKAGE_PIN B17     [get_ports {VAUXP1}]
set_property IOSTANDARD LVCMOS25 [get_ports {VAUXP1}]
set_property PACKAGE_PIN A17     [get_ports {VAUXN1}]
set_property IOSTANDARD LVCMOS25 [get_ports {VAUXN1}]

##################################################################################################################################

