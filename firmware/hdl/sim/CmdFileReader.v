module CmdFileReader(
  input clk,
  input en,
  output reg [7:0] dout,
  output reg dout_valid
);

parameter FILENAME = "cmdsFromSoftware";

reg [8:0]  SRread_input;
integer    SRin; 
integer    SRstatusI;

reg clk_half;

always @(posedge clk)
  clk_half <= ~clk_half; 


initial begin 
  clk_half <= 1'b0;
  dout <= 8'b0;
  dout_valid <= 1'b0;
  SRin  = $fopen(FILENAME,"r");
  repeat(100) @(posedge clk_half);
  while ( ! $feof(SRin)) begin
    @ (negedge clk_half);
    if (en) begin
      SRstatusI = $fscanf(SRin,"%h\n",SRread_input);
      $display("Read %h from file\n",SRread_input);
      if (SRread_input[8]) begin
        dout_valid = 1'b0;
        $display("Simulation time: %d ns, Waiting for 10000 clock cycles.\n", $time);
        repeat (10000) @(negedge clk_half);
      end else begin
        add_to_read_fifo(SRread_input[7:0]);
      end
    end
  end
  repeat (100) @ (posedge clk_half);
  $fclose(SRin);
end

task add_to_read_fifo;
  input [7:0] din;
  begin
    dout <= din;
    dout_valid <= 1'b1;
    @(posedge clk_half);
    dout_valid <= 1'b0;
    //@(posedge clk);
  end
endtask

endmodule
