`default_nettype none
import Readout::clock_t;

module ClockGen (
    input wire CLK_125,
    input wire REBOOT,
    output clock_t CLOCK,
    output bit ALL_LOCKED,
    output bit CONFIG_DONE
);

  timeunit 1ns; timeprecision 10ps;

  bit delay_locked, pll_locked;
  bit clk_300;  // 300 MHz for IDELAYCTRL.

  bit clockfb;  // TODO: buffer?
  //STARTUPE3 #(
//  STARTUPE2 #(
//      .PROG_USR("FALSE"), // Activate program event security feature. Requires encrypted bitstreams.
//      .SIM_CCLK_FREQ(0.0)  // Set the Configuration Clock Frequency(ns) for simulation.
//  ) STARTUPE2_inst (
//      .CFGCLK(),  // 1-bit output: Configuration main clock output
//      .CFGMCLK(),  // 1-bit output: Configuration internal oscillator clock output
//      .EOS(CONFIG_DONE),  // 1-bit output: Active high output signal indicating the End Of Startup.
//      .PREQ(),  // 1-bit output: PROGRAM request to fabric output
//      .CLK(1'b0),  // 1-bit input: User start-up clock input
//      .GSR(REBOOT),  // 1-bit input: Global Set/Reset input (GSR cannot be used for the port name)
//      .GTS(1'b0),  // 1-bit input: Global 3-state input (GTS cannot be used for the port name)
//      .KEYCLEARB( 1'b0 ), // 1-bit input: Clear AES Decrypter Key input from Battery-Backed RAM (BBRAM)
//      .PACK(1'b0),  // 1-bit input: PROGRAM acknowledge input
//      .USRCCLKO(1'b0),  // 1-bit input: User CCLK input
//      .USRCCLKTS(1'b0),  // 1-bit input: User CCLK 3-state enable input
//      .USRDONEO(1'b1),  // 1-bit input: User DONE pin output control
//      .USRDONETS(1'b0)  // 1-bit input: User DONE 3-state enable output
//  );

  MMCME2_BASE #(
      .BANDWIDTH("OPTIMIZED"),  // Jitter programming (OPTIMIZED, HIGH, LOW)
      //.CLKFBOUT_MULT_F(2.0),  // Multiply value for all CLKOUT (2.000-64.000).
      .CLKFBOUT_MULT_F(5.0),  // Multiply value for all CLKOUT (2.000-64.000).
      .CLKFBOUT_PHASE(0.0),  // Phase offset in degrees of CLKFB (-360.000-360.000).
     //.CLKIN1_PERIOD(3.2),  // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
      .CLKIN1_PERIOD(8),  // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
      // CLKOUT0_DIVIDE - CLKOUT6_DIVIDE: Divide amount for each CLKOUT (1-128)
//      .CLKOUT0_DIVIDE_F(2.083),  // Divide amount for CLKOUT0 (1.000-128.000).
//      .CLKOUT1_DIVIDE(2),
//      .CLKOUT2_DIVIDE(10),
//      .CLKOUT3_DIVIDE(1),
//      .CLKOUT4_DIVIDE(20),
//      .CLKOUT5_DIVIDE(4),
//      .CLKOUT6_DIVIDE(1),
      .CLKOUT0_DIVIDE_F(2.083),  // Divide amount for CLKOUT0 (1.000-128.000).
      .CLKOUT1_DIVIDE(2),
      .CLKOUT2_DIVIDE(10),
      .CLKOUT3_DIVIDE(1),
      .CLKOUT4_DIVIDE(20),
      .CLKOUT5_DIVIDE(4),
      .CLKOUT6_DIVIDE(1),




      // CLKOUT0_DUTY_CYCLE - CLKOUT6_DUTY_CYCLE: Duty cycle for each CLKOUT (0.01-0.99).
      .CLKOUT0_DUTY_CYCLE(0.5),
      .CLKOUT1_DUTY_CYCLE(0.5),
      .CLKOUT2_DUTY_CYCLE(0.5),
      .CLKOUT3_DUTY_CYCLE(0.5),
      .CLKOUT4_DUTY_CYCLE(0.5),
      .CLKOUT5_DUTY_CYCLE(0.5),
      .CLKOUT6_DUTY_CYCLE(0.5),
      // CLKOUT0_PHASE - CLKOUT6_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
      .CLKOUT0_PHASE(0.0),
      .CLKOUT1_PHASE(0.0),
      .CLKOUT2_PHASE(0.0),
      .CLKOUT3_PHASE(0.0),
      .CLKOUT4_PHASE(0.0),
      .CLKOUT5_PHASE(0.0),
      .CLKOUT6_PHASE(0.0),
      .DIVCLK_DIVIDE(1),  // Master division value (1-106)
      .REF_JITTER1(0.0)  // Reference input jitter in UI (0.000-0.999).
  ) MMCME2_BASE_inst (
      // Clock Outputs: 1-bit (each) output: User configurable clock outputs
      .CLKOUT0(clk_300),  // 1-bit output: CLKOUT2
      .CLKOUT0B(),  // 1-bit output: Inverted CLKOUT2
      .CLKOUT1(CLOCK.CLK_125),  // 1-bit output: CLKOUT0
      .CLKOUT1B(CLOCK.CLK_125_B),  // 1-bit output: Inverted CLKOUT0
      .CLKOUT2(CLOCK.CLK_62),  // 1-bit output: CLKOUT1
      .CLKOUT2B(),  // 1-bit output: Inverted CLKOUT1
      .CLKOUT3(CLOCK.CLK_625),  // 1-bit output: CLKOUT3
      .CLKOUT3B(),  // 1-bit output: Inverted CLKOUT3
      .CLKOUT4(CLOCK.CLK_31),  // 1-bit output: CLKOUT4
      .CLKOUT5(CLOCK.CLK_156),  // 1-bit output: CLKOUT5
      .CLKOUT6(),  // 1-bit output: CLKOUT6
      // Feedback Clocks: 1-bit (each) output: Clock feedback ports
      .CLKFBOUT(clockfb),  // 1-bit output: Feedback clock
      .CLKFBOUTB(),  // 1-bit output: Inverted CLKFBOUT
      // Status Ports: 1-bit (each) output: MMCM status ports
      .LOCKED(pll_locked),  // 1-bit output: LOCK
      // Clock Inputs: 1-bit (each) input: Clock input
      .CLKIN1(CLK_125),  // 1-bit input: Clock
      // Control Ports: 1-bit (each) input: MMCM control ports
      .PWRDWN(1'b0),  // 1-bit input: Power-down
      .RST(1'b0),  // 1-bit input: Reset
      // Feedback Clocks: 1-bit (each) input: Clock feedback ports
      .CLKFBIN(clockfb)  // 1-bit input: Feedback clock
  );
  /*
(* IODELAY_GROUP = "PETAIO" *) IDELAYCTRL delctrl_I (
        .REFCLK( clk_300 ),
        .RST( 1'b0 ),
        .RDY( delay_locked )
    );
*/
  always_comb begin
    ALL_LOCKED = pll_locked;  // && delay_locked;
  end

  bit [3:0] ten_counter;
  always_ff @(posedge CLOCK.CLK_125) begin
    if (ten_counter == 4'd9) ten_counter <= 4'd0;
    else ten_counter <= ten_counter + 1'd1;
  end

  always_comb CLOCK.WORD_START = ten_counter == 4'd0;

endmodule : ClockGen
`default_nettype wire
