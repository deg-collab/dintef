library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;



library unisim;
use unisim.vcomponents.IBUFGDS;
use unisim.vcomponents.IBUFDS_GTE2;



entity data_out_handler is

   
port (

  jtag_wrapper_clk          : in std_logic;
  jtag_wrapper_rst_n		: in std_logic;


  dyn_data_in               : in std_logic_vector(7 downto 0);
  dyn_data_in_write         : in std_logic;

  dyn_pll_clk_by5       : in std_logic;

  dyn_asic_run              : out std_logic;
  dyn_asic_ser0_dout        : in std_logic;
  dyn_asic_res              : out std_logic;

  dyn_init_jtag             : out std_logic;
  dyn_jtag_idle             : in std_logic;

  dyn_o_current_state       : out std_logic_vector(5 downto 0);
  dyn_o_next_state          : out std_logic_vector(5 downto 0);
  dyn_o_dev_in_fifo_empty   : out std_logic;
  dyn_cycle_done            : out std_logic; 


  dyn_data_out_64_o         : out std_logic_vector(63 downto 0);

  dyn_data_in_busy          : out std_logic;
  
  sus_dyn_data_out_valid    : out std_logic 


);

end data_out_handler;
------------------------------------------------------------------
architecture data_out_handler_rtl of data_out_handler is
------------------------------------------------------------------
    
signal dyn_data_out_valid_cnt      : integer range 0 to 5;
signal dyn_data_out_valid          : std_logic;
signal dyn_data_out                : std_logic_vector(15 downto 0);
signal dyn_data_out_64             : std_logic_vector(63 downto 0);

signal sus_dyn_data_out_valid_int  : std_logic;

signal jtag_wrapper_rst_del_n      : std_logic;
signal jtag_wrapper_rst_sync_n      : std_logic;

component DynamicAsicControl port(
  res_n                       : in std_logic;
  clk                         : in std_logic; -- this must be the same as slow clock for the chip
  data_in_busy                : out std_logic;        -- full of internal FIFO
  data_in_write               : in std_logic;
  data_in                     : in std_logic_vector(7 downto 0);                    -- data from PC

  data_out_valid : out std_logic;
  data_out       : out std_logic_vector(15 downto 0);

  asic_run   : out std_logic;
  asic_res   : out std_logic;
  init_jtag  : out std_logic;
  jtag_idle  : in std_logic;
  o_current_state : out std_logic_vector(5 downto 0);
  o_next_state    : out std_logic_vector(5 downto 0);
  o_dev_in_fifo_empty   : out std_logic;
  cycle_done            : out std_logic;


  conf_cycle_length        : in std_logic_vector(11 downto 0);
  conf_num_words_to_readout : in std_logic_vector(11 downto 0);
  asic_ser_dout                   : in std_logic;      
  conf_send_test_data             : in std_logic
);
end component;


-------------------------------------------------------------------
begin

dyn_data_out_64_o           <= dyn_data_out_64;
sus_dyn_data_out_valid      <= sus_dyn_data_out_valid_int;

process (dyn_pll_clk_by5, jtag_wrapper_rst_n)
begin
    if(jtag_wrapper_rst_n = '0') then
        jtag_wrapper_rst_del_n <= '0';
        jtag_wrapper_rst_sync_n <= '0';
    elsif rising_edge(dyn_pll_clk_by5) then
        jtag_wrapper_rst_del_n <= '1';
        jtag_wrapper_rst_sync_n <= jtag_wrapper_rst_del_n;
    end if;
end process;



process(jtag_wrapper_clk)
begin
    if rising_edge(jtag_wrapper_clk) then
        if (jtag_wrapper_rst_sync_n = '0') then
            dyn_data_out_64 <= (others => '0');
        else
            if (dyn_data_out_valid = '1') then
              --dyn_data_out_64 <= dyn_data_out_64(47 downto 0) & dyn_data_out; 
                dyn_data_out_64 <=  dyn_data_out & dyn_data_out_64(63 downto 16); 
--                dyn_data_out_words_left <= dyn_data_out_words_left - 1;
            end if;
        end if;
    end if;
end process;

process(jtag_wrapper_clk)
begin 
    if rising_edge(jtag_wrapper_clk) then
        if(jtag_wrapper_rst_sync_n = '0') then
            dyn_data_out_valid_cnt <= 0;
            sus_dyn_data_out_valid_int <= '0';
        else
            if( dyn_data_out_valid_cnt = 4 ) then  
                dyn_data_out_valid_cnt <= 0;
                sus_dyn_data_out_valid_int <= '1';
            else
                sus_dyn_data_out_valid_int <= '0';
                if(dyn_data_out_valid = '1') then
                    dyn_data_out_valid_cnt <= dyn_data_out_valid_cnt+1;
                end if;
            end if;
        end if;
    end if;

end process;

-- Dynamic Asic Control ---------------------------------------------------------------------------------------------------------

dyn_asic_cntrl_i : DynamicAsicControl                            -- Dynamic ASIC control engine for data acquisition
---------------------------------------------------------------------------------------------------------------------------------
port map (
  res_n				=> jtag_wrapper_rst_sync_n,
  clk				=> dyn_pll_clk_by5,   
  data_in_busy		=> dyn_data_in_busy,        
  data_in_write		=> dyn_data_in_write,
  data_in			=> dyn_data_in,             

  data_out_valid	=> dyn_data_out_valid,
  data_out			=> dyn_data_out,

  asic_run			=> dyn_asic_run,
  asic_res			=> dyn_asic_res,
  init_jtag			=> dyn_init_jtag,
  jtag_idle			=> dyn_jtag_idle,
  o_current_state		=> dyn_o_current_state,
  o_next_state			=> dyn_o_next_state,
  o_dev_in_fifo_empty	=> dyn_o_dev_in_fifo_empty,
  cycle_done            => dyn_cycle_done, 


  conf_cycle_length		=> std_logic_vector(to_unsigned(300, 12)),
  conf_num_words_to_readout	=> std_logic_vector(to_unsigned(800, 12)),
  asic_ser_dout			=> dyn_asic_ser0_dout,
  conf_send_test_data		=> '0'		
);

end data_out_handler_rtl;

