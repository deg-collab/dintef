`timescale 1ns/10ps

import Readout::clock_t;

module ClockGen_tb;
    
reg  clk_125, all_locked;

clock_t clk_out;


parameter T = 8;
parameter T3 = 10000*T;

parameter halfT = T/2;


always #halfT clk_125 = ~clk_125;


ClockGen DUT(
    .CLK_125(clk_125), 
    .REBOOT(),
    .CLOCK(clk_out),
    .ALL_LOCKED(all_locked),
    .CONFIG_DONE()

);


initial begin
    clk_125                 <= 1'b0;
    #T3;
    //$finish;

end


endmodule



