module cycle_done_delay(
    input wire clk,
    input wire rst_n,
    input wire cycle_done,
    
    input wire [10:0] cycle_done_delay,
    input wire [10:0] cycle_done_delay_skips,
    input wire single,
    input wire enable,

    output wire cycle_done_delayed
    
);

reg [2000:0] cycle_cnt;
reg [2000:0] cycle_done_delay_reg;
wire en_cycle_done_delayed; 
reg stop;



assign en_cycle_done_delayed = (cycle_cnt == cycle_done_delay_skips) && !stop;
assign cycle_done_delayed = cycle_done_delay_reg[cycle_done_delay] & en_cycle_done_delayed; 



always @(posedge clk or negedge rst_n) begin
    if((rst_n == 1'b0) | (enable == 1'b0)) begin
        stop <= 0;
    end
    else begin
        if(!single) begin
            stop <= 0;
        end
        else if(cycle_done_delayed == 1'b1) begin
            stop <= 1;
        end
    end 
end




always @(posedge clk or negedge rst_n) begin
    if((rst_n == 1'b0) | (enable == 1'b0)) begin 
        cycle_done_delay_reg <= 2000'd0;
    end
    else if (clk == 1'b1) begin 
        cycle_done_delay_reg <= {cycle_done_delay_reg[1998:0], cycle_done};
    end
    else begin
        cycle_done_delay_reg <= cycle_done_delay_reg;
    end
end


always @(posedge clk or negedge rst_n) begin
    if((rst_n == 1'b0) | (enable == 1'b0)) begin
        cycle_cnt <= 0;
    end
    else if (clk == 1'b1) begin
        if(cycle_done == 1'b1) begin
            if(cycle_cnt == cycle_done_delay_skips) begin
                cycle_cnt <= 0;
            end
            else begin
                cycle_cnt <= cycle_cnt + 1;
            end 
        end
        else begin
            cycle_cnt <= cycle_cnt;
        end
    end
end



endmodule
