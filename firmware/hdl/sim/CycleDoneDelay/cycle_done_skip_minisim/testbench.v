`timescale 1ns/10ps


module cycle_done_delay_tb;
    
reg rst_n, clk, cycle_done;
wire cycle_done_delayed;

wire [10:0] cycle_done_delay, cycle_done_delay_skips;

reg single;
reg enable;

reg [10:0] cycle_done_cnt;

always #1 clk = ~clk;

cycle_done_delay DUT(
    .clk(clk),
    .rst_n(rst_n),
    .cycle_done(cycle_done),

    .cycle_done_delay(cycle_done_delay),
    .cycle_done_delay_skips(cycle_done_delay_skips),
    .single(single),
    .enable(enable),


    .cycle_done_delayed(cycle_done_delayed)
);


assign cycle_done_delay = 11'd5;
assign cycle_done_delay_skips = 11'd3;

initial begin
    enable <= 1'b1;
    single <= 1'b1;


    clk <= 0;
    cycle_done <= 0;
    cycle_done_cnt <= 11'd0;
    rst_n <= 1'b1;
    #2 rst_n <= 1'b0;
    #10 rst_n <= 1'b1;
    #100 enable <= 1'b0;
    #50 enable <= 1'b1;



    #100 single <= 1'b0;
    enable <= 1'b0;
    #10;
    enable <= 1'b1;
    #1000 $finish;
end





always @(posedge clk) begin
    if(cycle_done_cnt == 11'd10) begin
        cycle_done_cnt <= 0;
    end
    else begin
        cycle_done_cnt <= cycle_done_cnt + 1;
    end
end 

always @(posedge clk) begin
    if(cycle_done_cnt == 11'd10) begin
        cycle_done <= 1;
    end
    else begin 
        cycle_done <= 0;
    end
end
endmodule



