`timescale 1ns/10ps


module data_out_handler_tb;
    
reg  clk, rst_n, dyn_data_in_write, dyn_jtag_idle;
reg [7:0] dyn_data_in; 
reg asic_ser0_dout;


wire dyn_asic_run, dyn_asic_res, dyn_init_jtag, dyn_cycle_done, dyn_data_in_busy, sus_dyn_data_out_valid, dyn_o_dev_in_fifo_empty;
wire [5:0] dyn_o_current_state, dyn_o_next_state;
wire [63:0] dyn_data_out_64;

reg en_asic_ser0_dout;

parameter T = 5;
parameter T2 = 2*T;
parameter T3 = 28*T;

always #T clk = ~clk;



data_out_handler DUT(
    .jtag_wrapper_clk(clk),       
    .jtag_wrapper_rst_n(rst_n),
  
  
    .dyn_data_in(dyn_data_in),
    .dyn_data_in_write(dyn_data_in_write),     
  
    .dyn_pll_clk_by5(clk),   
    //.dyn_pll_clk_by5(dyn_pll_clk_by5),   
  
    .dyn_asic_run(dyn_asic_run),          
    .dyn_asic_ser0_dout(asic_ser0_dout),
    .dyn_asic_res(dyn_asic_res),        
  
    .dyn_init_jtag(dyn_init_jtag), 
    .dyn_jtag_idle(dyn_jtag_idle),
  
    .dyn_o_current_state(dyn_o_current_state), 
    .dyn_o_next_state(dyn_o_next_state),
    .dyn_o_dev_in_fifo_empty(dyn_o_dev_in_fifo_empty),
    .dyn_cycle_done(dyn_cycle_done),
  
    .dyn_data_out_64_o(dyn_data_out_64),        
  
    .dyn_data_in_busy(dyn_data_in_busy),
    
    .sus_dyn_data_out_valid(sus_dyn_data_out_valid)
);


initial begin
    
    en_asic_ser0_dout   <= 0'b1;
    #3350
    en_asic_ser0_dout   <= 0'b0;

end



initial begin
    clk                 <= 1'b0;
    rst_n               <= 1'b1;
    dyn_data_in         <= 8'd3;        //RUN RO 
    dyn_data_in_write   <= 1'b0;
    dyn_jtag_idle           <= 1'b0;
    asic_ser0_dout      <= 0'b0;

    #2 rst_n <= 1'b0;
    #2 rst_n <= 1'b1;
    #400 dyn_data_in_write <= 1'b1;
    #10 dyn_data_in_write <= 1'b0;
//    #6000;
//    #400 dyn_data_in_write <= 1'b1;
//    #10 dyn_data_in_write <= 1'b0;


    #491;
    forever begin
         
        #T2 asic_ser0_dout <= en_asic_ser0_dout && 1'b1;  
        #T2 asic_ser0_dout <= 1'b0;
        #T3;

    end



end


endmodule



