#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function

import sys
import struct
import time
import numpy as np
import h5py
import ctypes
import types
import re
import logging as log

import inspect

#sys.path.append('..../xxxx/yyyy')

from libdeep import DeepError
from pydancelib.cmd import DanceCmd
from dintefdevice import DintefDevice
import libdeep
import sus.gui.QXiderConfGui as guilib
import sus.gui.CmdWriter as cmdwriter

import sus.ASICcontrol.jtag as jtaglib
import sus.ASICcontrol.dyn as dynlib
import sus.ASICcontrol.t3cont as t3contlib
import sus.pll.pll as plllib
import sus.daq.daq as daqlib
import sus.measurement.measurement as measlib
import sus.hv.hv as hvlib
#import sus

from collections import OrderedDict


class DintefCmd(DanceCmd):

    def __init__(self, device, *args, **kwargs):

        self.log = log.getLogger("dintefcmd")
        DanceCmd.__init__(self, device, *args, **kwargs)
        self.qXiderConfGui = guilib.QXiderConfGui()
#        self.myCmdWriter = cmdwriter.CmdWriter(self)
#        self.susJtag = sus.jtag(self)


        # Initialization of FPGA bus voltage and PLL control. This needs to be done before creating object instances of the FPGA and PLL controller!
        # Otherwise, objects can not initialize their ebone wrapper registers
        self.device.command('REG VADJR 0x8000005C')                     # Vadj to 2.5V
        self.device.command('REG REGRW2 12')                            # PLL reset sequence. Bit (#3) is active low. Start with pulling it high, keep ASIC reset (#2) high through the process 
        self.device.command('REG REGRW2 4')                             # PLL reset low (active) ; keep ASIC reset off (4 on its own)
        self.device.command('REG REGRW2 12')                            # PLL reset high(off) (8 on its own); keep ASIC reset off as well (4 on its own)


        self.susJtag = jtaglib.jtag(self)
        self.susDyn = dynlib.dyn(self)
        self.t3cont = t3contlib.t3cont(self)
        self.susPll = plllib.pll(self)
        self.susDAQ = daqlib.daq(self)
        self.susMeasurement = measlib.measurement(self, self.susJtag, self.susDyn)
        self.susHV = hvlib.hv(self)
        guilib.dintefcmd = self

        self.do_sus_system_init()
        self.do_pll_init()
        self.do_pll_bypass()
        self.do_sus_t3cont_init_link()


    def buildprompt(self):
        """
        Overrides the buildprompt method of DanceCmd
        """
        dev = self.device
        devname = dev.name().split(':')[0]
        self.prompt = "\n%s [%s] >> " % (devname, dev.get_state())

    def do_sus_system_init_nogui(self, *args):
        dev = self.device
        print('Set Vadj to 2.5V, reset ASIC and FPGA JTAG, reset PLL, start GUI')


        #Use raw register commands here instead of JTAG and PLL classes. This function needs to work, even when JTAG and PLL classes are not instantiated
        dev.command('REG VADJR 0x8000005C')                     # Vadj to 2.5V
        
        self.do_sus_system_reset()                              # FPGA JTAG engine reset
        
        dev.command('REG REGRW2 12')                            # PLL reset sequence. Bit (#3) is active low. Start with pulling it high, keep ASIC reset (#2) high through the process 
        dev.command('REG REGRW2 4')                             # PLL reset low (active) ; keep ASIC reset off (4 on its own)
        dev.command('REG REGRW2 12')                            # PLL reset high(off) (8 on its own); keep ASIC reset off as well (4 on its own)

    def do_sus_system_init(self, *args):
        self.do_sus_system_init_nogui()
        self.qXiderConfGui.showGui()


    def do_show_gui(self, *args):
        self.qXiderConfGui.showGui()

    def do_sus_jtag_set_single_reg(self, moduleName, regName, value):
        self.qXiderConfGui.setRegisterSignal( moduleName, regName, value )

    def do_static_reset_retoggle(self, *args):
        try:   
            while(True):
                   self.qXiderConfGui.setRegisterSignal( "Sequencer", "stats_val_idle", 0 )
                   self.do_sus_jtag_program()
                   time.sleep(0.000001) 
                   self.qXiderConfGui.setRegisterSignal("Sequencer", "stats_val_idle", 3 )      
                   self.do_sus_jtag_program()
                   time.sleep(0.000001)
        except KeyboardInterrupt:
                print('Keyboard interrupt')

#    def do_sus_jtag_init(self, *args):
#        self.do_fifo_reset()

    def do_sus_system_reset(self, *args):
        print('Reset JTAG state machines on FPGA and ASIC & DAQ FIFO on FPGA')
        dev = self.device
        dev.command('REG 1:0x0E00 0')
        time.sleep(0.001)
        dev.command('REG 1:0x0E00 1')



    def do_sus_reload(self, *args):
        reload(sus)
        self.susDyn = sus.dyn(self)
        self.susPll = sus.pll(self)
        self.susMeasurement = sus.measurement(self)

if __name__ == "__main__":
    # register_module(DintefCmd, sus)
    log.basicConfig(level=log.INFO, format='\033[00m[%(name)s][%(levelname)s]: %(message)s')

    DintefCmd.main(devclass=DintefDevice)
