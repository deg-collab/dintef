import numpy as np
import h5py

inputFile = input('Please specify the input file: ')
data = h5py.File(inputFile, 'r')

print(data.keys())
meas_top_key = data.keys()[0]
meas_top = data[meas_top_key]



#DAC0_hierarchy = meas_top.keys()[0]

seqConf = meas_top.attrs['Sequencer Configuration']
regConf = meas_top.attrs['Slow Control Register Configuration']


print(regConf)

seqConf_outFile = open("SequencerConfiguration.xml", 'w')
regConf_outFile = open("RegisterConfiguration.txt", 'w')


seqConf_outFile.write(seqConf)
regConf_outFile.write(regConf)

seqConf_outFile.close()
regConf_outFile.close()

