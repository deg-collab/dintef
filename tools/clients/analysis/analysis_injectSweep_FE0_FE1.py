import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches



def calcAvgsAndStdDevs(counterVals, stageRatio):
    
    counterValsAvg = np.zeros(np.shape(counterVals)[1])
    counterValsStdDev = np.zeros(np.shape(counterVals)[1])

    for i in np.arange(len(counterValsAvg)):
        cntVals = stageRatio*counterVals[1:-1, i, 0] - counterVals[1:-1, i, 1]
        counterValsAvg[i] = np.mean( cntVals ) 
        counterValsStdDev[i] = np.std( cntVals )
    return counterValsAvg, counterValsStdDev



inputFile = input('Please specify the input file: ../measurements/')
pathToMeasurements = '../measurements/'
hdf5File = h5py.File(pathToMeasurements + inputFile, 'r')

hdf5FileKeys = hdf5File.keys()


data = hdf5File[hdf5FileKeys[0]]
counterVals0 = data['cntVals FE0']
counterVals1 = data['cntVals FE1']
DACvalues = data['DAC Values']


counterValsAvg0, counterValsStdDev0 = calcAvgsAndStdDevs(counterVals0, 8)
counterValsAvg1, counterValsStdDev1 = calcAvgsAndStdDevs(counterVals1, 8)

plt.rc('axes', axisbelow = True)
plt.rcParams.update({'lines.markeredgewidth': 1})

plt.minorticks_on()
plt.grid(which='major', linestyle='-', linewidth=0.6, color='grey')
plt.grid(which='minor', linestyle=':', linewidth=0.4, color='grey')
plt.errorbar(DACvalues, counterValsAvg0, yerr=counterValsStdDev0, elinewidth = 0.7, label = "Average FE0", fmt = "o", barsabove = True, markersize = 0.8, capsize = 1)
plt.errorbar(DACvalues, counterValsAvg1, yerr=counterValsStdDev1, elinewidth = 0.7, label = "Average FE1", fmt = "o", barsabove = True, markersize = 0.8, capsize = 1)
plt.xlabel('Injected Charge (a.u.)')
plt.ylabel('Counted Photons (a.u.)')


outputFile = inputFile[0:-2] + 'pdf'

plt.savefig(outputFile)

hdf5File.close()



