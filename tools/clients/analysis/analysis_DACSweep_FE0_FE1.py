import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import re


from collections import OrderedDict

def calcAvgsAndStdDevs(counterVals, stageRatio):
    
    counterValsAvg = np.zeros(np.shape(counterVals)[1])
    counterValsStdDev = np.zeros(np.shape(counterVals)[1])

    for i in np.arange(len(counterValsAvg)):
        cntVals = stageRatio*counterVals[:, i, 0] + counterVals[:, i, 1]
        counterValsAvg[i] = np.mean( cntVals ) 
        counterValsStdDev[i] = np.std( cntVals )
    return counterValsAvg, counterValsStdDev


def plotFe0Fe1(hdf5InputFile, pathToDatasets, DACs, hdf5OutputFile):
    #print(pathToDatasets)
    data = hdf5InputFile[pathToDatasets]
    xaxis = DACs[DACs.keys()[-1]]

    FE0cntVals = data[data.keys()[0]]
    FE1cntVals = data[data.keys()[1]]
#    FE0avg = np.zeros(len(xaxis)) 
#    FE0stddev = np.zeros(len(xaxis)) 
#    FE1avg = np.zeros(len(xaxis)) 
#    FE1stddev = np.zeros(len(xaxis)) 
    FE0avg, FE0stddev = calcAvgsAndStdDevs(FE0cntVals[1:], 8)
#    print(FE0stddev)
    FE1avg, FE1stddev = calcAvgsAndStdDevs(FE1cntVals[1:], 8)
 #   print(FE0avg)
    print(FE0stddev)
    print( FE1stddev)

    fig, ax = plt.subplots(dpi=150)
    
    #plt.rc('axes', axisbelow = True)
    #plt.rcParams.update({'lines.markeredgewidth': 1})
#    print(xaxis) 
   
#    FE0avg[FE0avg > 1] = 1
#    FE1avg[FE1avg > 1] = 1

 
    plt.minorticks_on()
    plt.grid(which='major', linestyle='-', linewidth=0.6, color='grey')
    plt.grid(which='minor', linestyle=':', linewidth=0.4, color='grey')
    ax.errorbar(DACvalues, FE0avg, yerr=FE0stddev, elinewidth = 0.7, label = "Average FE0", fmt = "o", barsabove = True, markersize = 0.8, capsize = 1)
    ax.errorbar(DACvalues, FE1avg, yerr=FE1stddev, elinewidth = 0.7, label = "Average FE1", fmt = "o", barsabove = True, markersize = 0.8, capsize = 1)
#    ax.scatter(xaxis, FE0avg, label = "Average FE0", s = 4)
#    ax.scatter(xaxis, FE1avg, label = "Average FE1", s = 4)
    xlabel = DACs.keys()[-1]
#    print(xlabel)
    ax.set_xlabel(xlabel + ' (a.u.)')
#    ax.set_xlabel('COMP0_Vthresh_p (a.u.)')
    ax.set_ylabel('Counted Photons')
    ax.legend() 
    fig.canvas.draw()
    idata = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
    idata = idata.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    testset = hdf5OutputFile.create_dataset(pathToDatasets, data=idata, dtype='uint8',chunks=True)
    
    testset.attrs.create('CLASS', 'IMAGE')
    testset.attrs.create('IMAGE_VERSION', '1.2')
    testset.attrs.create('IMAGE_SUBCLASS', 'IMAGE_TRUECOLOR')
    testset.attrs.create('INTERLACE_MODE', 'INTERLACE_PIXEL')
    
    #testset.attrs['IMAGE_COLORMODEL'] = 'RGB'
    plt.close(fig) 
    

def extractDataAndPlot(hdf5InputFile, DACs, hdf5OutputFile, iteration = 0, pathToDatasets = '', currIndex = 0):
   # print(pathToDataset)
    last_iteration = len(DACs) 
    if(iteration < last_iteration):
        currDACname = DACs.keys()[iteration]
        currDACvalues = DACs[currDACname]
        for i in np.arange(len(DACs[currDACname])):
       #     print(DACs[currDACname][i])
    #        print(currDACname + ' ' + str(currDACvalues[i]))
           # if(not(re.search(currDACname + ' ' + str(currDACvalues[i]) , pathToDataset))):
            #    pathToDataset = pathToDataset + '/' + currDACname + ' ' + str(currDACvalues[i])
            if(iteration < last_iteration - 1):
                pathToDatasets = pathToDatasets + '/' + currDACname + ' ' + str(currDACvalues[i])
            extractDataAndPlot(hdf5InputFile, DACs, hdf5OutputFile, iteration = iteration + 1, pathToDatasets = pathToDatasets, currIndex = i)
            if(iteration < last_iteration - 1):
                pathToDatasets = pathToDatasets.replace('/' + currDACname + ' ' + str(currDACvalues[i]), '')

    else:
#        print(iteration)
        currDACname = DACs.keys()[iteration - 1]
        lastDACvalue = DACs[currDACname][-1]
#        print(lastDACvalue)
        if(DACs[currDACname][currIndex] == lastDACvalue):
            plotFe0Fe1(hdf5InputFile, pathToDatasets, DACs, hdf5OutputFile)





inputFile = input('Please specify the input file: ../measurements/')
#inputFile = 'DAQ.h5' 
pathToMeasurements = '../measurements/'
hdf5InputFile = h5py.File(pathToMeasurements + inputFile, 'r')
#hdf5InputFile = h5py.File(inputFile, 'r')
sweptDACs = hdf5InputFile.attrs['Swept DACs']

hdf5OutputFileName = inputFile[:-3] + '_ana.h5'
hdf5OutputFile = h5py.File(hdf5OutputFileName,'w')


hdf5InputFileKeys = hdf5InputFile.keys()

DACs_dict = OrderedDict()
for i in np.arange(len(sweptDACs)):
    DACname = sweptDACs[i]
    tmp = hdf5InputFileKeys.index('DAC Values ' + DACname)
    DACvalues = hdf5InputFile[hdf5InputFileKeys[tmp]] 
    hdf5OutputFile.create_dataset('DAC Values ' + DACname, data=DACvalues)
    DACs_dict[DACname] = DACvalues


pathToDatasets = "DAC Sweep"

extractDataAndPlot(hdf5InputFile, DACs_dict, hdf5OutputFile, pathToDatasets = pathToDatasets)
#print(hdf5InputFile.attrs.keys())
attributes = hdf5InputFile.attrs.keys()
for i in np.arange(len(attributes)):
    hdf5OutputFile.attrs[attributes[i]] = hdf5InputFile.attrs[attributes[i]]



hdf5InputFile.close()
hdf5OutputFile.close()

