import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import re
from scipy.optimize import curve_fit

def line(x, m, c):
    return m*x+c

def errfunc(x,A,mu,sig):
    return A*scipy.special.erfc((x-mu)/(sig*np.sqrt(2)))


def countPumpAmountsPerComp(counterVals, stageRatio):
    pumpAmounts = np.zeros(np.shape(counterVals)[1])

    for i in np.arange(np.shape(counterVals)[1]):
        cntVals = stageRatio*counterVals[1:, i, 0] + counterVals[1:, i, 1]
        pumpAmounts[i] = np.sum(cntVals)

    return pumpAmounts

#def calcAvgsAndStdDevs(counterVals):
#    
#    counterValsAvg = np.zeros(np.shape(counterVals)[1])
#    counterValsStdDev = np.zeros(np.shape(counterVals)[1])
#
#    for i in np.arange(len(counterValsAvg)):
#        cntVals = counterVals[:,i]
#        print(cntVals)
#        counterValsAvg[i] = np.mean( cntVals ) 
#        counterValsStdDev[i] = np.std( cntVals )
#    return counterValsAvg, counterValsStdDev


pathToMeasurement = '../' + input('Enter measurement file: ../')
#pathToDataset = 'DAC Sweep/COMP1_Vthresh_p 300/CP1_ibias 250'
#pathToDataset = 'DAC Sweep/CP1_ibias_p 600'
pathToDataset = 'DAC Sweep/FE8'
#pathToDataset = 'DAC Sweep'



hdf5InputFile = h5py.File(pathToMeasurement, 'r')
data = hdf5InputFile[pathToDataset]
sweptDACs = hdf5InputFile.attrs['Swept DACs']

xaxis = hdf5InputFile['DAC Values ' + sweptDACs[-1]]
print(xaxis)

#FE0cntVals = data[data.keys()[0]]
#FE1cntVals = data[data.keys()[1]]
#FE9cntVals = data[data.keys()[0]]
FE9cntVals = data
#    FE0avg = np.zeros(len(xaxis)) 
#    FE0stddev = np.zeros(len(xaxis)) 
#    FE1avg = np.zeros(len(xaxis)) 
#    FE1stddev = np.zeros(len(xaxis)) 
#FE0avg, FE0stddev = calcAvgsAndStdDevs(FE0cntVals[1:], 8)
FE9avg, FE9stddev = calcAvgsAndStdDevs(FE9cntVals[1:])
#    print(FE0stddev)
#FE1avg, FE1stddev = calcAvgsAndStdDevs(FE1cntVals[1:], 8)
 #   print(FE0avg)
print(FE9stddev)
#print( FE1stddev)

fig, ax = plt.subplots(dpi=600)
    
    #plt.rc('axes', axisbelow = True)
    #plt.rcParams.update({'lines.markeredgewidth': 1})
#    print(xaxis) 
   
#    FE0avg[FE0avg > 1] = 1
#    FE1avg[FE1avg > 1] = 1

 
plt.minorticks_on()
plt.grid(which='major', linestyle='-', linewidth=0.6, color='grey')
plt.grid(which='minor', linestyle=':', linewidth=0.4, color='grey')


print(len(xaxis))
print(len(FE9avg))
ax.errorbar(xaxis, FE9avg, yerr=FE9stddev, elinewidth = 0.7, label = "Average FE0", fmt = "o", barsabove = True, markersize = 0.8, capsize = 1)
#popt0,pcov0 = curve_fit(line, xaxis[0:50], FE0avg[0:50])
#plt.plot(xaxis, line(xaxis, *popt0), label='Linear Fit FE0', color = 'c')

#ax.errorbar(xaxis, FE1avg, yerr=FE1stddev, elinewidth = 0.7, label = "Average FE1", fmt = "o", barsabove = True, markersize = 0.8, capsize = 1)
#popt1,pcov1 = curve_fit(line, xaxis[0:50], FE1avg[0:50])
#plt.plot(xaxis, line(xaxis, *popt1), label='Linear Fit FE1', color = 'm')



#ax.scatter(xaxis, FE0avg, label = "Average FE0", s = 3)
#ax.scatter(xaxis, FE1avg, label = "Average FE1", s = 3)
xlabel = sweptDACs[-1]
#    print(xlabel)
ax.set_xlabel(xlabel + ' (a.u.)')
#    ax.set_xlabel('COMP0_Vthresh_p (a.u.)')
ax.set_ylabel('Counted Photons')
ax.legend() 
#ax.set_aspect(6)


plt.savefig('test.png')

plt.close(fig) 


#fig2, ax2 = plt.subplots(dpi=600)
#
#plt.scatter(xaxis, FE0avg-line(xaxis, *popt0), s=3, label='FE0')
#plt.scatter(xaxis, FE1avg-line(xaxis, *popt1), s=3, label='FE1')
#ax2.set_ylabel('Residual')
#ax2.set_xlabel(xlabel + ' (a.u)')
# 
#plt.minorticks_on()
#plt.grid(which='major', linestyle='-', linewidth=0.6, color='grey')
#plt.grid(which='minor', linestyle=':', linewidth=0.4, color='grey')
#
#
#ax2.legend() 
#ax2.set_aspect(25)
#
#plt.savefig('test2.png')
#plt.close(fig2)

#testset.attrs['IMAGE_COLORMODEL'] = 'RGB'
    

