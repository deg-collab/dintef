import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib as mpl
#mpl.use('Qt4Agg')
import re
from scipy.optimize import curve_fit
import scipy

import base

pathToFile = '../'
fileName = input('Enter measurement path: ../')

#base.plt.rc('text', usetex = True)

#pathToMeasurement = 'test.h5'
pathToMeasurement = pathToFile + fileName 

hdf5InputFile = h5py.File(pathToMeasurement, 'r')
FE_cnts = base.getFECntsFromHdf5(hdf5InputFile, 'DAC Sweep', 1)
print(FE_cnts)
FE_names = FE_cnts.keys()

FE_avgs, FE_stdevs = base.calcAvgsAndStdDevsSingleAll(FE_cnts)
DAC_key = list(hdf5InputFile.keys())[-1]
DAC_data = hdf5InputFile[DAC_key]

hdf5OutputFileName = 'basetest.h5'
hdf5OutputFile = h5py.File(hdf5OutputFileName, 'w')



base.plotAndFitSCurveAll(FE_avgs, DAC_data, hdf5OutputFile)


print(FE_avgs['FE1'])




