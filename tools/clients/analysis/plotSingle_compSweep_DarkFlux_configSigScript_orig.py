import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import re
from scipy.optimize import curve_fit
import scipy


plt.rc('text', usetex = True)

def line(x, m, c):
    return m*x+c


def errfunc(x,A,mu,sig):
    return A*scipy.special.erfc((x-mu)/(sig*np.sqrt(2)))

def calcAvgsAndStdDevs(counterVals):
    
    counterValsAvg = np.zeros(np.shape(counterVals)[1])
    counterValsStdDev = np.zeros(np.shape(counterVals)[1])

    for i in np.arange(len(counterValsAvg)):
        cntVals = counterVals[:,i]
        print(cntVals)
        counterValsAvg[i] = np.mean( cntVals ) 
        counterValsStdDev[i] = np.std( cntVals )
    return counterValsAvg, counterValsStdDev


#def countPumpAmountsPerComp(counterVals, stageRatio):
#    pumpAmounts = np.zeros(np.shape(counterVals)[1])
#
#    for i in np.arange(np.shape(counterVals)[1]):
#        cntVals = stageRatio*counterVals[1:, i, 0] + counterVals[1:, i, 1]
#        pumpAmounts[i] = np.sum(cntVals)
#
#    return pumpAmounts
#    
#def calcRates(pumpAmounts, cycles, slowClksPerCycle, PLLFreq = 125e6):                      # CAREFUL: ONLY GIVE THIS FUNCTION THE DATA OF A SINGLE FE; OTHERWISE THE RATE IS WRONG
#    PLLPeriod = 1./125e6
#    SlowPeriod = PLLPeriod*5.
#    cycleLength = slowClksPerCycle*SlowPeriod
#    measurementLength = cycleLength*cycles
#
#    rates = pumpAmounts/float(measurementLength)
#    return rates
#
#
#def calcEnergiesFromComps(comps, baseLineComp, capacitor = 50e-15):
#    CdTe_conversion = 4.43                  # in eV
#    elem_charge = 1.6e-19
#    dComps = comps - baseLineComp
#    dVs = 2*dComps*1000./1024*1e-3
#    charges = dVs*capacitor
#    electrons = charges/elem_charge
#    energies = electrons*CdTe_conversion
#    return energies



pathToMeasurement = '../'
fileName = input('Enter measurement file: ' + pathToMeasurement)
pathToFile = pathToMeasurement + fileName
pathToDataset = 'DAC Sweep/'

hdf5InputFile = h5py.File(pathToFile, 'r')
data = hdf5InputFile[pathToDataset]
sweptDACs = hdf5InputFile.attrs['Swept DACs']

xaxis = hdf5InputFile['DAC Values ' + sweptDACs[-1]]
FE8cntVals = data["FE12"]
FE8cntVals_cp = np.copy(FE8cntVals)


######################################################## PLOT S CURVE & FIT

FE8cntVals_tmp = np.copy(FE8cntVals_cp)
FE8cntVals_tmp[FE8cntVals_cp > 1] = 1
FE8avg, FE8stddev = calcAvgsAndStdDevs(FE8cntVals_tmp[1:])
edgeStart = 0
edgeEnd = 0


for i in np.arange(len(FE8avg)-1):
    if((FE8avg[i] > 0.9) and (FE8avg[i+1] <= 0.9)):
        edgeStart = xaxis[i]
    if((FE8avg[i] > 0.1) and (FE8avg[i+1] <= 0.1)):
        edgeEnd = xaxis[i]


fig, ax = plt.subplots(dpi=600)
    
    #plt.rc('axes', axisbelow = True)
    #plt.rcParams.update({'lines.markeredgewidth': 1})
#    print(xaxis) 
   
#    FE0avg[FE0avg > 1] = 1
#    FE8avg[FE8avg > 1] = 1

 
plt.minorticks_on()
plt.grid(which='major', linestyle='-', linewidth=0.6, color='grey')
plt.grid(which='minor', linestyle=':', linewidth=0.4, color='grey')


ax.errorbar(xaxis, FE8avg, yerr=FE8stddev, elinewidth = 0.7, label = "Average FE8", fmt = "o", barsabove = True, markersize = 0.8, capsize = 1)


popt1, pcov1 = curve_fit(errfunc, xaxis, FE8avg, p0 = [1/3., (edgeEnd+edgeStart)/2., 3])
stdev_e = 50e-15*popt1[2]*1e-3/1.6e-19
ax.plot(xaxis, errfunc(xaxis, *popt1), label = r'erfc Fit: $\mu$ = {:.2f}, $\sigma$ = {:.2f}mV $\sim$ {:.2f}e$^-$' .format(popt1[1], popt1[2], stdev_e)) 


xlabel = sweptDACs[-1]

xlabel = sweptDACs[-1] + ' (a.u.)'
xlabel = xlabel.replace("_","\_")
ax.set_xlabel(xlabel)
ax.set_ylabel('Comparator Fired')
ax.legend() 


plt.savefig(fileName[:-3] + '.pdf')

plt.close(fig) 


######################################################### PLOT RATES 
#
#fig2, ax2 = plt.subplots(dpi=600)
#
##plt.scatter(xaxis, FE0avg-line(xaxis, *popt0), s=3, label='FE0')
##plt.scatter(xaxis, FE8avg-line(xaxis, *popt1), s=3, label='FE8')
##ax2.set_ylabel('Residual')
##ax2.set_xlabel(xlabel + ' (a.u)')
#
#pumpAmounts = countPumpAmountsPerComp(FE8cntVals_cp[1:], 1)
#print(pumpAmounts) 
#rates = calcRates(pumpAmounts, len(FE8cntVals_cp), 70, PLLFreq = 125e6)                      # CAREFUL: ONLY GIVE THIS FUNCTION THE DATA OF A SINGLE FE; OTHERWISE THE RATE IS WRONG
##print(len(FE8cntVals_cp[0]))
#print(rates)
#
#
#
#for i in np.arange(len(FE8cntVals_cp[0])):
#    slc = FE8cntVals_cp[1:,i]
#    mask = FE8cntVals_cp[1:,i] > 1
#    slc_masked = slc[mask]
##    mask2 = mask & (FE8cntVals_cp[1:,i] < 100)
##    slc_masked2 = slc[mask2]
##    print(xaxis[i], np.sum(mask), slc_masked)
#
#
#energies = calcEnergiesFromComps(xaxis, popt1[1])
#energies_keV = energies*1e-3
#
#plt.scatter(energies_keV[xaxis > popt1[1]], rates[xaxis > popt1[1]], s=3)
#plt.minorticks_on()
#plt.grid(which='major', linestyle='-', linewidth=0.6, color='grey')
#plt.grid(which='minor', linestyle=':', linewidth=0.4, color='grey')
#plt.ylabel('Rate (Hz/pixel)')
#plt.ylim((1e-2,1e7))
#plt.yscale('log')
#xlabel = 'Photon Energy (keV)' 
#xlabel = xlabel.replace("_","\_")
#ax2.set_xlabel(xlabel)
#
#plt.savefig(fileName[:-3] + '_Rates.pdf')
#
## 
##
##
##ax2.legend() 
##ax2.set_aspect(25)
##
##plt.savefig('test2.png')
##plt.close(fig2)
#
##testset.attrs['IMAGE_COLORMODEL'] = 'RGB'
#    
#
