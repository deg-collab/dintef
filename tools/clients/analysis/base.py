import numpy as np
import h5py

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib as mpl

import re
from scipy.optimize import curve_fit
import scipy

import math


def line(x, m, c):
    return m*x+c



def errfunc(x,A,mu,sig):
    return A*scipy.special.erfc((x-mu)/(sig*np.sqrt(2)))

def findEdge(FEavgs, DAC):
    edgeStart = 0
    edgeEnd = 0
    for i in np.arange(len(FEavgs)-1):
        if((FEavgs[i] > 0.9) and (FEavgs[i+1] <= 0.9)):
            edgeStart = DAC[i]
        if((FEavgs[i] > 0.1) and (FEavgs[i+1] <= 0.1)):
            edgeEnd = DAC[i]
    edgePos = (edgeEnd+edgeStart)/2.
    return edgePos


def getFECntsFromHdf5(hdf5InputFile, pathToDataset, capTo1 = 0):
    inputData = hdf5InputFile[pathToDataset]
    keys = inputData.keys()
    FE_cnts = {}
    for i in np.arange(len(keys)):
        if(re.match(r"^FE\d\d?$", list(keys)[i])):
            FE_key = list(keys)[i]
            cnts = np.copy(inputData[FE_key])
            if(capTo1):
                cnts[cnts > 1] = 1
            FE_cnts[FE_key] = cnts 
    return FE_cnts 

def calcAvgsAndStdDevsSingle(counterVals, filterVals = 0):
    counterValsAvg = np.zeros(np.shape(counterVals)[1])
    counterValsStdDev = np.zeros(np.shape(counterVals)[1])

    #reject_candidate_indices = np.array([]) 
    reject_candidate_indices = {} 
 #   print(counterVals)
    FIFO_size = 2047
    for i in np.arange(np.shape(counterVals)[0]):
        cntVals = counterVals[i]
        for j in np.arange(np.shape(counterVals)[1]):
            if(j == 0):
                pass
            elif (j == len(cntVals)-1):
                pass
            else:
#                if(((cntVals[j+1]-cntVals[j]) == 1) or ((cntVals[j-1]-cntVals[j]) == 1)):
##                    print("Was here")
#                    if(not j in reject_candidate_indices):
#                        reject_candidate_indices[j] = np.array([i])
#                    else:
#                        reject_candidate_indices[j] = np.append(reject_candidate_indices[j], i) 
                if(((cntVals[j+1]-cntVals[j]) == 1) and ((cntVals[j-1]-cntVals[j]) == 1)):
#                    print("Was here2")
                    if(not j in reject_candidate_indices):
                        reject_candidate_indices[j] = np.array([i])
                    else:
                        reject_candidate_indices[j] = np.append(reject_candidate_indices[j], i) 
    print('keys', reject_candidate_indices.keys())
    possible_starts = np.arange(0, np.shape(counterVals)[0], FIFO_size) 
    print('possible starts', possible_starts)

    rejected_start_indices = {}

    for i in np.arange(len(reject_candidate_indices.keys())):
        key = list(reject_candidate_indices.keys())[i]
        rej_inds = reject_candidate_indices[key]
        #print('rej_inds', rej_inds)
        vals = counterVals[:,key]
        start_list = np.array([])
        start = 0
        count = 0
        possible_rejection_starts_for_this_key = np.array([])
        actual_rejection_starts_for_this_key = np.array([], dtype = int)
        for j in np.arange(len(possible_starts)):
            if(np.any(item in np.arange(possible_starts[j], possible_starts[j]+FIFO_size) for item in rej_inds)):
                possible_rejection_starts_for_this_key = np.append(possible_rejection_starts_for_this_key, possible_starts[j]) 
        #print('possible rejection start for key ' +  str(key) ,possible_rejection_starts_for_this_key)
        for j in np.arange(len(possible_rejection_starts_for_this_key)):
            scan_range = np.arange(possible_rejection_starts_for_this_key[j], possible_rejection_starts_for_this_key[j] + FIFO_size)
            start = possible_rejection_starts_for_this_key[j]
            for k in np.arange(len(scan_range)):
                current_index_in_cnt_vals = int(possible_rejection_starts_for_this_key[j] + k)
#                print(current_index_in_cnt_vals)
                if(k == 0):
                    count = 0
    #                if(vals[j+1]-vals[j] == 1):
                    if(vals[current_index_in_cnt_vals] == 0):
                        count = count+1
                    else:
                        count = 0
                elif(count == 2046):
                    actual_rejection_starts_for_this_key = np.append(actual_rejection_starts_for_this_key, int(start))   
                    count = 0
                else:
                    if(vals[current_index_in_cnt_vals] == 0):
                        count = count+1
                    else:
                        count = 0
        if(actual_rejection_starts_for_this_key.any()):
            rejected_start_indices[key] = actual_rejection_starts_for_this_key
    

    for i in np.arange(len(counterValsAvg)):
        cntVals = counterVals[:,i]
        cntVals_masked = np.ma.array(cntVals, mask=False)
        if(i in rejected_start_indices):
            print('i', i)
            rejected_starts = rejected_start_indices[i]
            rejected_stops = rejected_starts + FIFO_size
            for j in np.arange(len(rejected_starts)):
                print(rejected_starts[j], rejected_stops[j])
                cntVals_masked.mask[rejected_starts[j]:rejected_stops[j]] = True

#        print("cntVals", cntVals)
#        print("cntVals_masked",cntVals_masked)
        counterValsAvg[i] = np.mean( cntVals_masked ) 
        counterValsStdDev[i] = np.std( cntVals_masked )
    return counterValsAvg, counterValsStdDev

def calcAvgsAndStdDevsSingleAll(FEdict):
    FeAvgDict = {}
    FeStdDict = {}

    for i in np.arange(len(FEdict.keys())):
        key = list(FEdict.keys())[i]
        data = FEdict[key]
        print(key)
        cntValAvg, cntValStdDev = calcAvgsAndStdDevsSingle(data)
        FeAvgDict[key] = cntValAvg
        FeStdDict[key] = cntValStdDev

    return FeAvgDict, FeStdDict


def drawPlotIntoHdf5File(fig, path, hdf5OutputFile):
    fig.canvas.draw()
    idata = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
    idata = idata.reshape(fig.canvas.get_width_height()[::-1] + (3,))
    testset = hdf5OutputFile.create_dataset(path, data=idata, dtype='uint8',chunks=True)
    
    testset.attrs.create('CLASS', 'IMAGE')
    testset.attrs.create('IMAGE_VERSION', '1.2')
    testset.attrs.create('IMAGE_SUBCLASS', 'IMAGE_TRUECOLOR')
    testset.attrs.create('INTERLACE_MODE', 'INTERLACE_PIXEL')
    
    #testset.attrs['IMAGE_COLORMODEL'] = 'RGB'
    plt.close(fig) 
 

def plotDataVsDACSingle(FEname, FEdata, DAC, hdf5OutputFile):


    cycleLength = 4e-6
    fraction_IntTime_CycleLength = 90/100 
    integrationTime = cycleLength*fraction_IntTime_CycleLength 

#    xaxis = np.arange(len(FEdata)) 
#    xaxis = xaxis*cycleLength*1e3 
    xaxis = DAC
    
    fig, ax = plt.subplots(dpi=150)
    

#    charge = calcCharge(FEdata)
#    equivalentCurrent = calcEquivalentCurrent(charge, integrationTime)*1e9 # (in nA)

    plt.minorticks_on()
    plt.grid(which='major', linestyle='-', linewidth=0.6, color='grey')
    plt.grid(which='minor', linestyle=':', linewidth=0.4, color='grey')
    ax.scatter(xaxis, FEdata, label = "FE1 counts", s = 3)
    xlabel = "COMP0_Vthresh_p" 
    ax.set_xlabel(xlabel)
    ax.set_ylabel('Counted Photons')
    ax.legend() 

#    ax2 = ax.twinx()
#    ax2.scatter(xaxis, equivalentCurrent, s = 3)
#    ax2.set_ylabel("Equivalent current (nA)")
#
    
    return fig


def plotDataVsDACAll(FEAvgs, DAC, hdf5OutputFile):
    keys = FEAvgs.keys()
    for i in np.arange(len(keys)):
        FEname = keys[i]
        FEdata = FEAvgs[FEname]
        fig = plotDataVsDACSingle(FEname, FEdata, DAC, hdf5OutputFile)
        path = "DAC Sweep/" + FEname 
        drawPlotIntoHdf5File(fig, path, hdf5OutputFile)

def plotAndFitSCurveAll(FEAvgs, DAC, hdf5OutputFile):
    keys = FEAvgs.keys()
    stdevs = {}
    for i in np.arange(len(keys)):
        FEname = list(keys)[i]
        FEdata = FEAvgs[FEname]
       
        
        fig = plotDataVsDACSingle(FEname, FEdata, DAC, hdf5OutputFile)
        edge = findEdge(FEdata, DAC)

        FEdata_fit = np.copy(FEdata)
        rejected_indices = np.array([])
        for j in np.arange(len(FEdata_fit)):
            if(j == 0):
                if((FEdata_fit[j+1]-FEdata_fit[j]) > 0.5):
                    rejected_indices = np.append(rejected_indices, j)
            elif (j == len(FEdata_fit)-1):
                if((FEdata_fit[j]-FEdata_fit[j]) > 0.5):
                    rejected_indices = np.append(rejected_indices, j)
            else:
                if((FEdata_fit[j+1]-FEdata_fit[j] > 0.95) or (FEdata_fit[j-1]-FEdata_fit[j]) > 0.95):
                    rejected_indices = np.append(rejected_indices, j)
                    print("Hello3")
                elif(((FEdata_fit[j+1]-FEdata_fit[j]) > 0.2) and ((FEdata_fit[j-1]-FEdata_fit[j]) > 0.2)):
                    print("Hello2")
                    rejected_indices = np.append(rejected_indices, j)
#        print(FEname)
#        print(rejected_indices)

        DAC_fit = np.copy(DAC)
        FEdata_fit = np.delete(FEdata_fit, rejected_indices)
        DAC_fit = np.delete(DAC_fit, rejected_indices)
        popt1, pcov1 = curve_fit(errfunc, DAC_fit, FEdata_fit, p0 = [1/3., edge, 3])
        stdev_e = 50e-15*popt1[2]*1e-3/1.6e-19
        stdevs[FEname] = stdev_e
         
        ax = fig.axes[0]
        print(ax)
        ax.plot(DAC, errfunc(DAC, *popt1), label = r'erfc Fit: $\mu$ = {:.0f}, $\sigma$ = {:.1f}mV $\sim$ {:.0f}e$^-$' .format(popt1[1], popt1[2], stdev_e)) 
        ax.legend()
        path = "DAC Sweep/" + FEname 
        drawPlotIntoHdf5File(fig, path, hdf5OutputFile)
    x = np.array(stdevs.items())[:,0]
    y = np.array(stdevs.items())[:,1]
    print(x)
    print(y)
    sort_array = np.zeros(len(x))
    for i in np.arange(len(x)):
        m = re.search(r'(?<=FE)\d\d?', x[i])
        sort_array[i] = m.group(0)
    print('sort_array', sort_array)
    sorted_x = np.array([i for _, i in sorted(zip(sort_array,x))])
    sorted_x = np.flip(sorted_x)
    sorted_y = np.array([i for _, i in sorted(zip(sort_array,y))])
    sorted_y = np.flip(np.float_(sorted_y))

    print(sorted_x)
    print(sorted_y)
    plt.plot(sorted_x, sorted_y)
    plt.show()
    path = "DAC Sweep/NoiseVsFE"


#pathToMeasurement = 'test.h5'
#
#hdf5InputFile = h5py.File(pathToMeasurement, 'r')
#FE_cnts = getFECntsFromHdf5(hdf5InputFile, 'DAC Sweep', 1)
#FE_names = FE_cnts.keys()
#
#FE_avgs, FE_stdevs = calcAvgsAndStdDevsSingleAll(FE_cnts)
#DAC_key = hdf5InputFile.keys()[-1]
#DAC_data = hdf5InputFile[DAC_key]
#
#hdf5OutputFileName = 'basetest.h5'
#hdf5OutputFile = h5py.File(hdf5OutputFileName, 'w')
#
#
#
#plotAndFitSCurveAll(FE_avgs, DAC_data, hdf5OutputFile)
#
#
#print(FE_avgs['FE1'])

