# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function
import logging as log


from libdeep import DanceDevice  # , DeepArray

# This has been added by SuS


class BusyError(EnvironmentError):
    pass


class DintefDevice(DanceDevice):
    def __init__(self, *args, **kwargs):

        self.log = log.getLogger("DintefDevice")
        DanceDevice.__init__(self, *args, **kwargs)

        if not self.command('?APPNAME').startswith('DINTEF'):
            print(
                'Warning!!! {} is not a dintef device!!!'.format(self.name())
            )

    def get_state(self):
        pass
        # return 'IDLE'+str(self.modshape)
