# -*- coding: utf-8 -*-
import numpy

from .widgets.pixelview import (PixelTableDialog, ImageModel)
from .widgets.enable_pixel_dialog import SetMaskWidget
from .toolbase import ToolBase, ToolEvent

from PyMca5.PyMca import MaskImageWidget, PlotWidget

class PixelDisplayTool(ToolBase):
    show_event = ToolEvent('show', object)

    def init_tool(self):
        self.mask_widget = MaskImageWidget.MaskImageWidget()
        self.mask_widget.graphWidget.toolBar.hide()
        return self.mask_widget

    def init_display(self):
        self.mask_widget.graphWidget.graph.setGraphTitle(self.name)

    def show(self, data):
        self.bin_numpy_show(data)

    def bin_deeparray_show(self, deep_array):
        data = map(int, deep_array.tolist())
        img = numpy.array(data)
        img.shape = self.device.detsize()
        self.mask_widget.setImageData(img)
        self.mask_widget.show()

    def bin_numpy_show(self, numpy_array):
        img = numpy.array(numpy_array.tolist(), dtype='int')
        img.shape = self.device.detsize()
        self.mask_widget.setImageData(img)
        self.mask_widget.show()

    def main_widget(self):
        return self.mask_widget

    def scan_data_acquired_handler(self, scan_name, param, data):
        names_to_plot = self.get_config('VALUES')

        if names_to_plot:
            if '*' in names_to_plot:
                for name, data in data.iteritems():
                    if isinstance(data, numpy.ndarray):
                        self.bin_numpy_show(data)
                        break
            else:
                try:
                    self.bin_numpy_show(data[names_to_plot[0]])
                except KeyError:
                    pass

        #self.bin_numpy_show(data['Counters'])


class PlotTool(ToolBase):
    show_event = ToolEvent('show', object)

    def init_tool(self):
        self.plot_widget = PlotWidget.PlotWidget()
        self.curve_data = {}
        return self.plot_widget

    def init_display(self):
        self.plot_widget.setGraphTitle(self.name)

    def reset(self):
        self.plot_widget.show()
        self.plot_widget.clear()
        self.curve_data = {}
        self.plot_widget.setGraphTitle(self.name)

    def add_item(self, name, x, y):
        if not name in self.curve_data:
            self.curve_data[name] = {}
            self.curve_data[name]['x'] = []
            self.curve_data[name]['y'] = []

        self.curve_data[name]['x'].append(x)
        self.curve_data[name]['y'].append(y)

        for name, data in self.curve_data.iteritems():
            self.plot_widget.addCurve(data['x'], data['y'], legend=name)

    def show(self, data):
        pass

    def main_widget(self):
        return self.plot_widget

    def scan_data_acquired_handler(self, scan_name, param,  data):
        names_to_plot = self.names_to_plot()

        if '*' in names_to_plot:
            for name, value in data.iteritems():
                if isinstance(value, float) or isinstance(value, int):
                    self.add_item(name, data[param], float(data[name]))

        elif names_to_plot:
            for name in names_to_plot:
                if (name in data) and (param in data):
                    self.add_item(name, data[param], float(data[name]))

    def scan_init_handler(self, scan_name):
        self.reset()

    def names_to_plot(self):
        return self.get_config('VALUES')


class PixelTableTool(ToolBase):
    def show(self, data):
        try:
            values = data.values()

            if len(values) == 1024:
                model = ImageModel()
                model.set_image(values, 32, 32)

                tw = PixelTableDialog(self.console_widget)
                tw.setModel(model)
                tw.show()

        except Exception as ex:
            print(str(ex))
            self.console_widget._insert_prompt("\nCan't display data ! \n")


class EnpixTool(ToolBase):
    show_event = ToolEvent('show', object)

    def init_tool(self):
        self._enpix_widget = SetMaskWidget()
        self._enpix_widget.enable_pixels_button.clicked.connect(self.set_mask)
        self._enpix_widget.disable_pixels_button.clicked.connect(self.clear_mask)
        self.set_mask_cb = None

        return self._enpix_widget

    def handle_command(self, cmd_str, value):
        pass

    def init_display(self, *args, **kwargs):
        pass
        #self.cmd.do_edit_mask(' ')

    def show(self, data):
        self._enpix_widget.set_data(data)
        self._enpix_widget.show()

    def set_mask(self):
        prev_mask = self._enpix_widget.mask_widget.getImageData()
        new_mask = self._enpix_widget.mask_widget.getSelectionMask()
        self._enpix_widget.mask_widget._resetSelection()
        mask = prev_mask | new_mask
        self._enpix_widget.set_data(mask)

        if callable(self.set_mask_cb):
            dev_mask = prev_mask | new_mask
            dev_mask.shape = 1, 1024
            dev_mask = dev_mask[0]
            self.set_mask_cb(dev_mask)

    def clear_mask(self):
        prev_mask = self._enpix_widget.mask_widget.getImageData()
        new_mask = self._enpix_widget.mask_widget.getSelectionMask()
        self._enpix_widget.mask_widget._resetSelection()
        new_mask = ~new_mask + 2
        mask = prev_mask & new_mask
        self._enpix_widget.set_data(mask)

        if callable(self.set_mask_cb):
            dev_mask = prev_mask & new_mask
            dev_mask.shape = 1, 1024
            dev_mask = dev_mask[0]
            self.set_mask_cb(dev_mask)
