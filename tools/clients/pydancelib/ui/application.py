#/usr/bin/python
# -*- coding: utf-8 -*-
import os
import bisect
import threading
import ctypes

from .ext.qt import QtCore
from .ext.qt.QtWidgets import (QApplication, QMainWindow, QMdiArea, QDockWidget,
                               QHBoxLayout, QMenu, QWidget, QAction,
                               QVBoxLayout, QTabWidget)

from .widgets.pyqtconsole.console import PythonConsole
from .widgets.dconsole import DanceConsole
from .utils.text import camel_to_underscore

from ..app.baseapp import ApplicationBase, AppBase
from ..cmd.command import (ShowCommand, ImageCommand, PlotCommand)

class UIScanDispatcher(QtCore.QObject):
    scan_init_signal = QtCore.Signal(object, object)
    scan_end_signal = QtCore.Signal(object, object)
    scan_data_acquired_signal = QtCore.Signal(object, object, object)

    def __init__(self):
        super(UIScanDispatcher, self).__init__()

    def connect(self, tool):
        if hasattr(tool, 'scan_data_acquired_handler'):
            self.scan_data_acquired_signal.\
              connect(tool.scan_data_acquired_handler)

        if hasattr(tool, 'scan_init_handler'):
            self.scan_init_signal.connect(tool.scan_init_handler)

        if hasattr(tool, 'scan_end_handler'):
            self.scan_end_signal.connect(tool.scan_end_handler)

    def disconnect(self, tool):
        if hasattr(tool, 'scan_data_acquired_handler'):
            self.scan_data_acquired_signal.\
              disconnect(tool.scan_data_acquired_handler)

        if hasattr(tool, 'scan_init_handler'):
            self.scan_init_signal.disconnect(tool.scan_init_handler)

        if hasattr(tool, 'scan_end_handler'):
            self.scan_end_signal.disconnect(tool.scan_end_handler)


class MainWindow(QMainWindow):
    def __init__(self, parent = None):
        super(MainWindow, self).__init__(parent)
        self.resize(1280, 800)
        self.setContentsMargins(7,7,7,0)
        self.central_widget = QWidget(self)
        self.central_widget.setObjectName("central_widget")
        self.central_widget.setContentsMargins(0,0,0,0)
        self.setCentralWidget(self.central_widget)

        self.hlayout = QVBoxLayout(self.central_widget)
        self.hlayout.setObjectName("hlayout")
        self.hlayout.setContentsMargins(0,0,0,0)

        self.tool_mdi_area = QMdiArea(self.central_widget)
        self.tool_mdi_area.setContentsMargins(0,0,0,0)
        self.hlayout.addWidget(self.tool_mdi_area)

        self.tool_container_widget = ToolContainerWidget(self.tool_mdi_area)
        flags = QtCore.Qt.CustomizeWindowHint
        flags |= QtCore.Qt.WindowMinimizeButtonHint
        flags |= QtCore.Qt.WindowMaximizeButtonHint
        flags |= QtCore.Qt.WindowTitleHint
        self.tool_mdi_area.addSubWindow(self.tool_container_widget, flags)
        self.tool_container_widget.setWindowTitle('Figure-1')
        #self.tool_container_widget.showMaximized()

        self.console_dock_widget = QDockWidget(self.central_widget)
        self.console_dock_widget.setObjectName("dock_widget")
        self.console_dock_widget.setFeatures(QDockWidget.NoDockWidgetFeatures)        
        self.console_dock_widget.setTitleBarWidget(QWidget())
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.console_dock_widget)

        self.console_tab_widget = QTabWidget(self.console_dock_widget)
        self.console_dock_widget.setWidget(self.console_tab_widget)

        self.console = PythonConsole(self.console_tab_widget)
        self.console.ctrl_d_exits_console(False)
        self.console.setWindowTitle("Python")
        self.console_tab_widget.addTab(self.console, 'Python')
        self.console.eval_in_thread()
        console_size = self.console.size()
        self.console.setMinimumWidth(console_size.width())
        
        self.file_menu = QMenu(self.menuBar())
        self.file_menu.setObjectName("file_menu")
        self.file_menu.setTitle("File")
        self.file_menu.addAction(QAction('Quit', self, shortcut='Ctrl+Q',
                                         triggered=self.close))

        self.statusBar().showMessage("Ready")
        self.statusBar().setContentsMargins(0,0,0,0)

        self.menuBar().addAction(self.file_menu.menuAction())
        QtCore.QMetaObject.connectSlotsByName(self)


class ToolContainerWidget(QWidget):
    def __init__(self, parent = None):
        super(ToolContainerWidget, self).__init__(parent)
        self._row_layouts = {}
        self._locations = {}
        self._num_rows = 0
        self._num_cols = 0
        self._vlayout = QVBoxLayout(self)
        self._add_row(0)

    def _add_row(self, index):
        row = QHBoxLayout()
        self._locations[index] = []
        self._row_layouts[index] = row
        self._vlayout.addLayout(row)

    def _insert_widget(self, widget, row, col):
        self._row_layouts[row].insertWidget(col, widget)
        _hi = len(self._locations[row])
        bisect.insort_right(self._locations[row], col, lo=0, hi=_hi)

    def add_widget(self, widget, location):
        row, col = location

        # Make sure that the location is not already taken
        if not self.widget_at_location(row, col):
            if row in self._locations:
                self._insert_widget(widget, row, col)
            else:
                self._add_row(row)
                self._insert_widget(widget, row, col)
        else:
            w_cls_name = type(widget).__name__
            msg = 'Widget location %s for %s taken' % (str(location), w_cls_name)
            raise AssertionError(msg)

    def remove_widget(self, location):
        row, col = location

        if self.widget_at_location(row, col):
            row_layout = self._row_layouts[row]
            widget = row_layout.itemAt(col).widget()
            widget.setParent(None)
            row_layout.removeWidget(widget)
            self._locations[row].remove(col)

    def widget_at_location(self, row, col):
        if row in self._locations and col in self._locations[row]:
            return True
        else:
            return False

    def num_rows(self):
        return max(self._locations.keys())

    def num_cols(self):
        return max(self._locations.values())


class UIApp(AppBase, QtCore.QObject):
    def __init__(self):
        AppBase.__init__(self)
        QtCore.QObject.__init__(self)

        self._qapp = QApplication([])
        self.main_win = MainWindow()
        self.scan_dispatcher = UIScanDispatcher()

        self._ci_console, self._ci_thread =  self._init_uiconsole(self._ci)
        self._ci_thread.start()

    def _init_uiconsole(self, ci):
        dance_console = DanceConsole(self.main_win.console_tab_widget, ci)
        dance_console.restore_history(self.app_settings.cmd_history)
        dance_console.ctrl_c_signal.connect(self._console_ctrl_c_handler)
        dance_console.ctrl_d_exits_console(False)
        wtitle = ci.prompt()[:-5] + ' Console'
        dance_console.setWindowTitle(wtitle)

        self.main_win.console_tab_widget.insertTab(0, dance_console, wtitle)
        self.main_win.console_tab_widget.setCurrentIndex(0)
        self.main_win.setWindowTitle(ci.prompt()[:-5])

        ci_thread = self._init_ci_thread(dance_console)
        dance_console.showMaximized()

        return dance_console, ci_thread

    def _init_ci_thread(self, console):
        cmd_thread = threading.Thread(target = console.eval_in_thread)
        cmd_thread.daemon = True
        return cmd_thread

    def _console_ctrl_c_handler(self):
        self._ci_console.stdout.write('\n\nKeyboardInterrupt !\n\n')
        self.scan_engine.cleanup_last_scan()
        self.reset_command_interpreter()

    def reset_command_interpreter(self):
        print('Received SIGINT !')
        print('Active threads %s' % threading.active_count())

        while self._ci_thread.isAlive():
            print('Terminating thread %s'% self._ci_thread.ident)
            _id = self._ci_thread.ident
            _id, exobj = ctypes.c_long(_id), ctypes.py_object(KeyboardInterrupt)
            ctypes.pythonapi.PyThreadState_SetAsyncExc(_id, exobj)
            self._ci_console.stdin.write('EOF' + os.linesep)
            self._ci_thread.join(1)

        print('Active threads %s' % threading.active_count())

        self._ci_console.reset_io(self._ci)
        self._ci_thread = self._init_ci_thread(self._ci_console)
        self._ci_thread.start()

    def execute(self):
        AppBase.execute(self)
        exit_code = -1
        self.main_win.show()
        exit_code = self._qapp.exec_()
        self.main_win.console.close()
        self.app_settings.cmd_history = self._ci_console.get_history_content()
        AppBase.post_execute(self)
        return exit_code

    def scan_init(self, scan_name, actuator_list=[]):
        self.scan_dispatcher.scan_init_signal.emit(scan_name, actuator_list)
        AppBase.scan_init(self, scan_name, actuator_list)

    def scan_data_acquired(self, scan_name, param, data):
        self.scan_dispatcher.scan_data_acquired_signal.emit(scan_name, param, data)
        AppBase.scan_data_acquired(self, scan_name, param, data)

    def scan_end(self, scan_name, actuator_list=[]):
        self.scan_dispatcher.scan_end_signal.emit(scan_name, actuator_list)
        AppBase.scan_end(self, scan_name, actuator_list)

class UIApplication(ApplicationBase, QtCore.QObject):
    add_tool_signal = QtCore.Signal(object, object)

    def __init__(self, netloc_list):
        ApplicationBase.__init__(self, netloc_list)
        QtCore.QObject.__init__(self)

        self._app = QApplication([])
        self._init_done = True

        self._ui_console_dict = {}

        self.ui_tools = {}
        self.ui_tool_loc_to_name = {}
        self.active_tools = {}

        self.main_win = MainWindow()
        self.scan_dispatcher = UIScanDispatcher()

        self.add_tool_signal.connect(self._add_tool)
        self.add_tool_cond = threading.Condition()

        for dev in self._device_list:
            self.add_device_uiconsole(dev.hostname())

        try:
            for dev in self._device_list:
                # Create ui_tools
                for ui_tool_cls in self.TOOLS:
                    self._add_tool(ui_tool_cls, dev)
        except:
            import traceback
            traceback.print_exc()
            self._init_done = False

    def add_device_uiconsole(self, hostname):
        cmd, dev = self.get_device_cmd(hostname)

#        cmd.register_command('show', ShowCommand)
#        cmd.register_command('image', ImageCommand)
#        cmd.register_command('plot', PlotCommand)
        
        dance_console = DanceConsole(self.main_win.console_tab_widget, cmd)
        dance_console.restore_history(self.app_settings.cmd_history)
        dance_console.ctrl_c_signal.connect(self._console_ctrl_c_handler)
        dance_console.ctrl_d_exits_console(False)
        wtitle = "DANCE-Console (%s)" % hostname
        dance_console.setWindowTitle(wtitle)

        self.main_win.console_tab_widget.insertTab(0, dance_console, wtitle)
        self.main_win.console_tab_widget.setCurrentIndex(0)
        #self.main_win.cedit.register_console('DANCE Console', dance_console)
        self.main_win.setWindowTitle(hostname)

        cmd_thread = self.run_cmd_in_thread(dance_console)
        self._ui_console_dict[hostname] = [dance_console, cmd_thread]
        dance_console.showMaximized()

        return dance_console

    def run_cmd_in_thread(self, device_console):
        cmd_thread = threading.Thread(target = device_console.eval_in_thread)
        cmd_thread.daemon = True
        cmd_thread.start()
        return cmd_thread

    def reset_device_ui_console(self, hostname):
        console, cmd_thread = self.get_device_uiconsole(hostname)
        print('Received SIGINT !')
        print('Active threads %s' % threading.active_count())

        #console.stdin.write('EOF' + os.linesep)

        while cmd_thread.isAlive():
            print('Terminating thread %s'% cmd_thread.ident)
            _id = cmd_thread.ident
            _id, exobj = ctypes.c_long(_id), ctypes.py_object(KeyboardInterrupt)
            ctypes.pythonapi.PyThreadState_SetAsyncExc(_id, exobj)
            console.stdin.write('EOF' + os.linesep)
            cmd_thread.join(1)

        print('Active threads %s' % threading.active_count())

        self.reinit_device_cmd(hostname)        
        cmd, device = self.get_device_cmd(hostname)
        console.reset_io(cmd)
        cmd_thread = self.run_cmd_in_thread(console)
        self._ui_console_dict[hostname] = [console, cmd_thread]

#        cmd.register_command('show', ShowCommand)
#        cmd.register_command('image', ImageCommand)
#        cmd.register_command('plot', PlotCommand)

    def get_device_uiconsole(self, hostname):
        return self._ui_console_dict.get(hostname, None)

    def _console_ctrl_c_handler(self, hostname):
        console, cmd_thread = self.get_device_uiconsole(hostname)
        console.stdout.write('\n\nKeyboardInterrupt !\n\n')
        self.scan_engine.cleanup_last_scan()
        self.reset_device_ui_console(hostname)

    def scan_init(self, scan_name, actuator_list=[]):
        self.scan_dispatcher.scan_init_signal.emit(scan_name, actuator_list)
        ApplicationBase.scan_init(self, scan_name, actuator_list)

    def scan_data_acquired(self, scan_name, param, data):
        self.scan_dispatcher.scan_data_acquired_signal.emit(scan_name, param, data)
        ApplicationBase.scan_data_acquired(self, scan_name, param, data)

    def scan_end(self, scan_name, actuator_list=[]):
        self.scan_dispatcher.scan_end_signal.emit(scan_name, actuator_list)
        ApplicationBase.scan_end(self, scan_name, actuator_list)

    def _add_tool(self, ui_tool_spec, dev):
        self.add_tool_cond.acquire()
        ui_tool_cls = ui_tool_spec[0]
        tool = ui_tool_cls(self, dev)
        tool_name = camel_to_underscore(tool.__class__.__name__)
        location, config_dict = None, None

        if len(ui_tool_spec) == 2:
            ui_tool_cls, location = ui_tool_spec
        elif len(ui_tool_spec) == 3:
            ui_tool_cls, location, tool_name = ui_tool_spec
        elif len(ui_tool_spec) == 4:
            ui_tool_cls, location, tool_name, config_dict = ui_tool_spec

        # Add the tool to the container widget if we have a 'grid' like
        # location
        if isinstance(location, tuple) and len(location) == 2:
            row, col = location
            twidget = tool.main_widget()
            self.main_win.tool_container_widget.add_widget(twidget, location)

        self.ui_tools[tool_name] = (tool, location)
        self.ui_tool_loc_to_name[location] = tool_name
        self.scan_dispatcher.connect(tool)

        tool.name = tool_name
        tool.configure(config_dict)
        tool.init_display()

        self.add_tool_cond.notify()
        self.add_tool_cond.release()

    def get_tool(self, tool_name, loc = None):
        return self.ui_tools.get(tool_name, (None, None))[0]

    def add_tool(self, ui_tool_spec, dev):
        self.add_tool_cond.acquire()

        self.add_tool_signal.emit(ui_tool_spec, dev)
        self.add_tool_cond.wait()

        self.add_tool_cond.release()

    def remove_tool(self, name):
        tool, location = self.ui_tools[name]

        if location:
            self.main_win.tool_container_widget.remove_widget(location)
            del self.ui_tool_loc_to_name[location]

        del self.ui_tools[name]
        del self.session.settings[name.upper()]

    def set_active_tool(self, name):
        tool, loc = self.ui_tools.get(name, (None, None))

        # No tool with name, check if name is a location
        if not tool:
            try:
                loc = tuple(map(int, name.split(',')))
            except (ValueError, IndexError):
                raise AssertionError('No tool with name or location %s' % name)
            else:
                name = self.ui_tools_loc_to_name.get(loc, None)
                tool, loc = self.ui_tools.get(name, (None, None))

        # Still no tool, the tool does not exist:
        if not tool:
            raise AssertionError('No tool with name or location %s' % name)

        ptool_name = self.active_tools.get(tool.__class__, '')
        ptool = self.get_tool(ptool_name)

        if ptool:
            self.scan_dispatcher.disconnect(ptool)

        self.active_tools[tool.__class__] = name
        self.scan_dispatcher.connect(tool)

    def get_active_tool(self, tool_cls):
        return '' #self.active_tools[tool_cls]

    def execute(self):
        exit_code = -1

        self.main_win.show()
        exit_code = self._app.exec_()
        self.main_win.console.close()

        for tool, location in self.ui_tools.values():
            tool.close()

        for dev in self._devices.values():
            ui_console = self._ui_console_dict[dev.hostname()][0]
            self.app_settings.cmd_history = ui_console.get_history_content()

        #TaskManager.wait_for_tasks()

        ApplicationBase.post_execute(self)
        return exit_code

def _command(self, str_cmd, *args):
    res = self.__unaug_command(str_cmd, *args)
    print(str_cmd)
    return res

def augment_command(cls_):
    cls_.__unaug_command = cls_.command
    cls_.command = _command
    return cls_
