# -*- coding: utf-8 -*-
from .ext.qt import QtCore

class ToolEvent(object):
    def __init__(self, name, *args):
        super(ToolEvent, self).__init__()
        self.__handled_objects = {}
        self.name = name
        self.args = args

    def __get__(self, obj, objtype):
        signal = getattr(obj, self.name + '_qt_signal')
        
        if not id(obj) in self.__handled_objects:
            handler = getattr(obj, self.name)
            signal.connect(handler)
            self.__handled_objects[id(obj)] = obj

        return signal.emit
            
    def __set__(self, obj, value):
        pass

    def __delete__(self, obj):
        pass


class ToolMeta(QtCore.pyqtWrapperType):
    def __new__(meta_cls, name, bases, cls_dict):
        cls = QtCore.pyqtWrapperType.__new__(meta_cls, name, bases, cls_dict)
  
        for mro in reversed(cls.__mro__):
            for aname, atype in mro.__dict__.items():
                if isinstance(atype, ToolEvent):
                    qt_signal = QtCore.Signal(*atype.args)
                    setattr(cls, atype.name + '_qt_signal', qt_signal)

        return cls


class ToolBase(QtCore.QObject):
    __metaclass__ = ToolMeta
    # (object) data to display
    show_event = ToolEvent('show', object)

    def __init__(self, app, dev):
        super(ToolBase, self).__init__()
        self.name = None
        self.app = app
        self.device = dev
        self.cmd = app.get_device_cmd(dev.hostname())[1]
        self.console_widget = app.get_device_uiconsole(dev.hostname())   
        self._main_widget = self.init_tool()

    def _default_config(self):
        return {'VALUES' : ['*']}

    def configure(self, conf_dict=None):
        if not self.name in self.app.session_mgr.session().settings:
            name = self.name.upper()
            
            if conf_dict:
                self.app.session_mgr.session().settings[name] = conf_dict
            else:
                self.app.session_mgr.session().settings[name] = self._default_config()

    def set_current_device(self, cmd):
        self.cmd = cmd
        self.device = cmd.device

    # Abstract
    def init_tool(self):
        return None

    # Abstract
    def init_display(self):
        pass
    
    #Abstract
    def show(self, data):
        pass

    #Abstract
    def close(self):
        pass

    #Abstract
    def main_widget(self):
        return self._main_widget

    def set_config(self, name, value = None):
        self.app.session_mgr.session().settings[self.name][name] = value

    def get_config(self, name = None):
        if name:
            return self.app.session_mgr.session().settings[self.name.upper()][name]
        else:
            return self.app.session_mgr.session().settings[self.name.upper()]
