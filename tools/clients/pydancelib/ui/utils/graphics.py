# -*- coding: utf-8 -*-
import colorsys

def pseudocolor(val, minval, maxval):
    # convert val in range minval..maxval to the range 0..120 degrees which
    # correspond to the colors red..green in the HSV colorspace
    h = (float(val-minval) / (maxval-minval)) * 240
    # convert hsv color (h,1,1) to its rgb equivalent
    # note: the hsv_to_rgb() function expects h to be in the range 0..1 not
    # 0..360
    r, g, b = colorsys.hsv_to_rgb(h/360, 0.5, 1.)
    return int(r*255), int(g*255), int(b*255)
