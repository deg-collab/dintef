# -*- coding: utf-8 -*-

from ..ext.qt.QtWidgets import (QTextEdit, QMenu)
from ..ext.qt.QtGui import QFontMetrics
from ..ext.pyqtconsole.syntaxhighlighter import PythonHighlighter

class CodeEdit(QTextEdit):
    def __init__(self, parent = None):
        super(CodeEdit, self).__init__(parent)
        self._console_list = []
        self.highlighter = PythonHighlighter(self.document())

        font = self.document().defaultFont()
        font.setFamily("Courier New")
        font_width = QFontMetrics(font).width('M')

        self.document().setDefaultFont(font)
        geometry = self.geometry()
        geometry.setWidth(font_width*80+20)
        geometry.setHeight(font_width*40)
        self.setGeometry(geometry)        
        self.resize(font_width*80+20, font_width*40)

    def contextMenuEvent(self, e):
        popup = self.createStandardContextMenu()
        popup.addSeparator()
 
        exec_menu = self.create_evalute_context_menu()
        popup.addMenu(exec_menu)

        popup.exec_(e.globalPos())

    def register_console(self, name, console):
        self._console_list.append((name, console._evaluate_buffer))

    def create_evalute_context_menu(self):
        exec_menu = QMenu('Evaluate buffer', self)
        
        for console in self._console_list:
            exec_menu.addAction(console[0], console[1])

        return exec_menu
