# -*- coding: utf-8 -*-
from ..ext.qt.QtWidgets import (QVBoxLayout, QHBoxLayout, QWidget, QPushButton)
from PyMca5.PyMca.MaskImageWidget import MaskImageWidget

class SetMaskWidget(QWidget):
    def __init__(self, parent = None):
        super(SetMaskWidget, self).__init__(parent)

        self.mask_widget = MaskImageWidget(self, aspect=True, maxNRois=2)
        self.mask_widget.graphWidget.toolBar.hide()
        self.enable_pixels_button = QPushButton('Enable Selected Pixels')
        self.disable_pixels_button = QPushButton('Disable Selected Pixels')

        edit_layout = QHBoxLayout()
        edit_layout.addStretch(-1)
        edit_layout.addWidget(self.disable_pixels_button)
        edit_layout.addWidget(self.enable_pixels_button)

        layout = QVBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)
        layout.addWidget(self.mask_widget)
        layout.addLayout(edit_layout)

    def set_data(self, data):
        self.mask_widget.setImageData(data)
