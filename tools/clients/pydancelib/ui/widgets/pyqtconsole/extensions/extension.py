# -*- coding: utf-8 -*-
class ExtensionManager(object):
    def __init__(self, owner):
        self._owner = owner
        self._extension_dict = {}

    def install(self, ext_cls, name = None):
        ext = ext_cls()
        ext._ext_mngr = self
        ext.install()
        _id = name if name else id(ext)
        self._extension_dict[_id] = ext

    def get(self, name):
        return self._extension_dict.get(name, None)

class Extension(object):
    #Abstract
    def install(self):
        pass

    def owner(self):
        return self._ext_mngr._owner
