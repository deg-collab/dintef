# -*- coding: utf-8 -*-
from ....ext.qt import QtCore
from .extension import Extension


class CommandHistory(Extension, QtCore.QObject):
    def __init__(self):
        Extension.__init__(self)
        QtCore.QObject.__init__(self)
        self._cmd_history = []
        self._idx = 0

    def _insert_in_editor(self, str_):
        self.owner().textCursor().clearSelection()
        self.owner()._clear_buffer()
        self.owner()._keep_cursor_in_buffer()
        self.owner()._insert_in_buffer(str_)

    def _is_last_entry(self, str_):
        return len(self._cmd_history) and str_ == self._cmd_history[-1]

    def install(self):
        self.owner().installEventFilter(self)

    def add(self, str_):
        if str_.strip() and not self._is_last_entry(str_):
            self._cmd_history.append(str_)

        self._idx = len(self._cmd_history)

    def inc(self):
        # index starts at 0 so + 1 to make sure that we are within the
        # limits of the list
        if len(self._cmd_history) and (self._idx + 1) < len(self._cmd_history):
            self._idx += 1
            self._insert_in_editor(self.current())
        elif self._idx == len(self._cmd_history) - 1:
            self._idx += 1
            self._insert_in_editor('')

    def dec(self):
        if len(self._cmd_history):
            if self._idx > 0:
                self._idx -= 1

            self._insert_in_editor(self.current())

    def current(self):
        if len(self._cmd_history):
            return self._cmd_history[self._idx]

    def eventFilter(self, source, event):
        if event.type() == QtCore.QEvent.KeyPress:
            key = event.key()

            if key in (QtCore.Qt.Key_Return, QtCore.Qt.Key_Enter):
                self.add(self.owner()._get_buffer())
            elif key == QtCore.Qt.Key_Up:
                self.dec()
            elif key == QtCore.Qt.Key_Down:
                self.inc()

        return False

    def restore_history(self, content):
        for entry in content:
            self.add(entry)
