#/usr/bin/python
# -*- coding: utf-8 -*-
"""
"""
from ..ext.qt import QtCore
from ..ext.qt.QtWidgets import (
    QGraphicsView, QGraphicsScene, QVBoxLayout, QFrame,QDialog, QGraphicsItem,
    QSpinBox, QGraphicsRectItem, QStyle, QTreeWidgetItem, QTableView,
    QTreeWidget, QAbstractItemView, QPushButton, QHBoxLayout)

from ..ext.qt.QtGui import (QBrush, QColor, qRgb, QPen)
from ..utils.graphics import pseudocolor

class ImageModel(QtCore.QAbstractTableModel):
    def __init__(self, parent = None):
        super(ImageModel, self).__init__(parent)
        self.__pixel_values = None
        self.__rows = None
        self.__cols = None

    def set_image(self, pixel_values, rows, cols):
        self.beginResetModel()
        self.__pixel_values = pixel_values
        self.__rows = rows
        self.__cols = cols
        self.endResetModel()

    def rowCount(self, parent_model_index):
        return self.__rows

    def columnCount(self, parent_model_index):
        return self.__cols

    def data(self, model_index, role = QtCore.Qt.DisplayRole):
        result = None

        if role in (QtCore.Qt.DisplayRole, QtCore.Qt.EditRole):
            row, col = model_index.row(), model_index.column()
            index = row * self.__rows + col
            result = self.__pixel_values[index]

        return result

    def setData(self, model_index, value, role = QtCore.Qt.EditRole):
        if role == QtCore.Qt.EditRole:
            row, col = model_index.row(), model_index.column()
            index = row * self.__rows + col
            self.__pixel_values[index] = value
            self.dataChanged.emit(model_index, model_index, [role])

            return True

        return False

    def flags(self, model_index):
        return QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled \
          | QtCore.Qt.ItemIsSelectable

class PixelRectItem(QGraphicsRectItem):
    def __init__(self, *args, **kwargs):
        super(PixelRectItem, self).__init__(*args, **kwargs)

    def paint(self, painter, option, widget):
        super(PixelRectItem, self).paint(painter, option, widget)

        if option.state & QStyle.State_Selected:
            painter.fillRect(self.boundingRect(), QBrush(QColor(0,0,0,50)))

    def set_value(self, value):
        color = QColor(qRgb(*pseudocolor(value, 0, 65535)))
        pen = QPen(QtCore.Qt.black, 1, QtCore.Qt.NoPen)
        self.setBrush(QBrush(color))
        self.setPen(pen)


class PanableGraphicsView(QGraphicsView):
    def __init__(self, parent):
        super(QGraphicsView, self).__init__(parent)
        self._pan = False
        self._prev_posx = 0
        self._prev_posy = 0

    def mousePressEvent(self, mouseEvent):
        if (mouseEvent.button() == QtCore.Qt.RightButton) and \
           (mouseEvent.modifiers() & QtCore.Qt.ShiftModifier):
            self._pan = True
            self._prev_posx = mouseEvent.pos().x()
            self._prev_posy = mouseEvent.pos().y()
            self.setCursor(QtCore.Qt.ClosedHandCursor)
            mouseEvent.accept()
        else:
            super(PanableGraphicsView, self).mousePressEvent(mouseEvent)

    def mouseMoveEvent(self, mouseEvent):
        if self._pan:
            dx = self._prev_posx - mouseEvent.pos().x()
            dy = self._prev_posy - mouseEvent.pos().y()
            rect = self.sceneRect()
            rect.translate(dx, dy)
            self.setSceneRect(rect)
            self._prev_posx = mouseEvent.pos().x()
            self._prev_posy = mouseEvent.pos().y() 
        else:
            super(PanableGraphicsView, self).mouseMoveEvent(mouseEvent)

    def mouseReleaseEvent(self, mouseEvent):
        if (mouseEvent.button() == QtCore.Qt.RightButton):
            if self._pan:
                self._pan = False
                self.setCursor(QtCore.Qt.ArrowCursor)
                mouseEvent.accept()
        else:
            item = self.itemAt(mouseEvent.pos())

            if item:
                item.setSelected(True)

        super(PanableGraphicsView, self).mouseReleaseEvent(mouseEvent)


class PixelScene(QGraphicsScene):
    # object is a list of pairs (pixel_id, value)
    pixel_selected_signal = QtCore.Signal(object)

    def __init__(self, *args, **kwargs):
        if kwargs.has_key('pixels_hor'):
            num_pixels_hor = kwargs.pop('pixels_hor')

        if kwargs.has_key('pixels_ver'):
            num_pixels_ver = kwargs.pop('pixels_ver')

        super(PixelScene, self).__init__(*args, **kwargs)

        self.pixel_width = (self.width() - num_pixels_hor)/num_pixels_hor
        self.pixel_height = (self.height() - num_pixels_ver)/num_pixels_ver
        self.item_to_data = {}
        self.index_to_item = {}

        self.setBackgroundBrush(QtCore.Qt.black)

        self.num_pixels_ver = num_pixels_ver
        self.num_pixels_hor = num_pixels_hor

    def __model_data_changed(self, top_left_index, bottom_right_index, roles):
        index = top_left_index.row(), top_left_index.column()
        value = top_left_index.data()
        item = self.index_to_item[index]
        item.set_value(value)

    def __create_pixel(self, row, col, value):
        args = (col*(self.pixel_width+1) + 1,row*(self.pixel_height+1) + 1,
                self.pixel_width, self.pixel_height)
        rect = PixelRectItem(*args)
        rect.setFlag(QGraphicsItem.ItemIsSelectable, True);
        rect.set_value(value)

        return rect

    def mouseReleaseEvent(self, mouseEvent):
        selected_pixels = self.selected_pixels()

        if selected_pixels:
            self.pixel_selected_signal.emit(selected_pixels)

    def selected_pixels(self):
        selected_items = self.selectedItems()
        data_list = []

        if selected_items:
            for item in selected_items:
                index = self.item_to_data[item]
                data = self.model.index(*index).data()
                data = (index, data)
                data_list.append(data)

            data_list = sorted(data_list, key=lambda data: data[0])

        return data_list

    def _set_pixel_item_values(self, value):
        selected_items = self.selectedItems()

        if selected_items:
            for item in selected_items:
                index = self.item_to_data[item]
                index = self.model.index(*index)
                self.model.setData(index, value)

    def add_rect(self, rect_item):
        rect, index, value = rect_item

        if not index in self.indices:
            self.addItem(rect)
            self.item_to_data[rect] = (index, value)

        self.indices.append(index)

    def setModel(self, model):
        self.model = model
        self.model.dataChanged.connect(self.__model_data_changed)
        rows, cols = (model.rowCount(None), model.columnCount(None))

        for row in range(rows):
            for col in range(cols):
                index = (row, col)
                value = model.data(model.index(*index))
                rect = self.__create_pixel(row, col, value)
                self.addItem(rect)
                self.index_to_item[index] = rect
                self.item_to_data[rect] = (row, col)

    def set_data(self, pixel_values):
        for row in range(self.num_pixels_hor):
            for col in range(self.num_pixels_ver):
                index = row * self.num_pixels_hor + col
                value = pixel_values[index]
                rect = self.__create_pixel(row, col, value)
                self.addItem(rect)

                self.item_to_data[rect] = (index, value)


class PixelValueTreeWidget(QTreeWidget):
    item_editing_finished_signal = QtCore.Signal(object, object)

    def __init__(self, parent = None):
        super(PixelValueTreeWidget, self).__init__(parent = None)
        self.setAlternatingRowColors(True)
        self.setColumnCount(2)
        self.setHeaderLabels(["Property", "Value"])
        self.setAnimated(False)
        self.itemClicked.connect(self.on_click)
        self.setIndentation(0)

    def on_click(self, item, index):
        if item.flags() & QtCore.Qt.ItemIsEditable:
            item.create_editor()

    def item_editing_finished(self, new_value):
        self.item_editing_finished_signal.emit(self, new_value)


class PixelValueEditTreeItem(QTreeWidgetItem):
    def __init__(self, parent = None, label = '', value = 0):
        super(PixelValueEditTreeItem, self).__init__(parent)
        self.setFlags(self.flags() | QtCore.Qt.ItemIsEditable)
        self.__min = -100001
        self.__max = 100000
        self.__value = value
        self.__label_text = label

        self.setText(0, self.__label_text)

        if self.__value == self.__min:
            self.setText(1, '-')
        else:
            self.setText(1, str(self.__value))

    def create_editor(self):
        tw = self.treeWidget()
        spinbox = QSpinBox(tw)
        self.setText(1,'')
        spinbox.setMinimum(self.__min)
        spinbox.setMaximum(self.__max)
        spinbox.setMaximumWidth(75)
        spinbox.setSpecialValueText('-')
        spinbox.setValue(self.__value)
        spinbox.editingFinished.connect(self.edit_finished)
        tw.setItemWidget(self, 1, spinbox)
        spinbox.setFocus()

    def edit_finished(self):
        spinbox = self.treeWidget().itemWidget(self, 1)
        self.setText(1, spinbox.text())

        if spinbox.value() != spinbox.minimum():
            self.__value = spinbox.value()
            self.treeWidget().item_editing_finished(self.__value)

        spinbox.hide()
        spinbox.close()


class PixelDialog(QDialog):
    def __init__(self, parent, pixels_hor, pixels_ver):
        super(PixelDialog, self).__init__(parent)
        self.pixels_ver = pixels_ver
        self.pixels_hor = pixels_hor

        self.__create_graphics_view(pixels_hor, pixels_ver)
        self.data_widget = self._create_pixel_value_tree()
        self.layout = QVBoxLayout(self)
        self.layout.addWidget(self.view)
        self.layout.addWidget(self.data_widget)

    def __create_graphics_view(self, pixels_hor, pixels_ver):
        self.view = PanableGraphicsView(self)
        self.view.setFrameShape(QFrame.NoFrame)
        self.view.setFrameShadow(QFrame.Plain)
        self.view.setDragMode(QGraphicsView.RubberBandDrag)
        self.view.setRubberBandSelectionMode(QtCore.Qt.IntersectsItemShape)

        width = pixels_hor * 15
        height = pixels_ver * 15

        self.scene = PixelScene(0, 0, width, height, pixels_hor=pixels_hor,
                                pixels_ver=pixels_ver)
        self.view.setScene(self.scene)
        self.view.setSceneRect(0, 0, width, height)

        self.scene.pixel_selected_signal.connect(self._pixel_selected)

    def _create_pixel_value_tree(self):
        vt = PixelValueTreeWidget(self)
        vt.item_editing_finished_signal.connect(self.item_editing_finished)
        return vt

    def _pixel_selected(self, data_list):
        self.data_widget.clear()

        for data in data_list:
            cols = ['Count %s:' % str(data[0]), '%s' % data[1]]
            QTreeWidgetItem(self.data_widget, cols)

        self.data_widget.resizeColumnToContents(0)

    def item_editing_finished(self, sender, new_value):
        self.scene._set_pixel_item_values(new_value)

    def set_data(self, pixel_values):
        self.scene.set_data(pixel_values)

    def setModel(self, model):
        self.scene.setModel(model)


class PixelEditDialog(PixelDialog):
    def __init__(self, *args):
        super(PixelEditDialog, self).__init__(*args)
        self.ok_button = QPushButton('OK', self)
        hlayout = QHBoxLayout()
        hlayout.addStretch(-1)
        hlayout.addWidget(self.ok_button)
        self.layout.addLayout(hlayout)

    def _pixel_selected(self, data_list):
        self.data_widget.clear()
        value = -100001

        if len(data_list) == 1:
            value = data_list[0][1]

        PixelValueEditTreeItem(self.data_widget, 'Count:', value)

    def selected_pixels(self):
        values = self.scene.selected_pixels()
        return map(lambda value: value[0], values)

    def selected_pixel_data(self):
        return self.scene.selected_pixels()

    def all_pixel_data(self):
        it = self.scene.item_to_data.itervalues()
        return sorted(it, key=lambda item: item[0])

    def pixel_edit_result(self):
        return PixelEditResult(self)


class PixelTableDialog(QDialog):
    def __init__(self, parent):
        super(PixelTableDialog, self).__init__(parent)
        self.table_view = QTableView(self)
        self.table_view.setSelectionBehavior(QAbstractItemView.SelectItems)
        self.layout = QVBoxLayout(self)
        self.layout.addWidget(self.table_view)
        
    def setModel(self, model):
        self.table_view.setModel(model)


class PixelEditResult(object):
    def __init__(self, pixel_edit_dialog):
        super(PixelEditResult, self).__init__()

        self.pixels = pixel_edit_dialog.selected_pixels
        self.values = pixel_edit_dialog.selected_pixel_data
        self.all = pixel_edit_dialog.all_pixel_data

if __name__ == '__main__':
    import sys
    from ..qt.QtWidgets import (QApplication)
    rows, cols = (100, 100)
    numpx = rows*cols

    img = [i for i in xrange(0, numpx, 1)]
    
    app = QApplication(sys.argv)

    model = ImageModel()
    model.set_image(img, rows, cols)

    table_view1 = QTableView()
    table_view1.setModel(model)
    table_view1.show()

    pw = PixelDialog(None, rows, cols)
    pw.setModel(model)
    pw.show()

    pwe = PixelEditDialog(None, rows, cols)
    pwe.setModel(model)
    pwe.show()
    sys.exit(app.exec_())
