# -*- coding: utf-8 -*-
import os

from ..ext.qt import QtCore
from pyqtconsole.console import BaseConsole
from pyqtconsole.console import Stream


class ConsoleInterrupt(BaseException):
    pass


class DanceConsole(BaseConsole):
    ctrl_c_signal = QtCore.Signal()

    def __init__(self, parent = None, cmd = None):
        super(DanceConsole, self).__init__(parent)
        self.set_tab(' ')
        self.reset_io(cmd)

    def reset_io(self, cmd):
        if self.stdout:
            self.stdout_write_signal.disconnect(self._stdout_data_handler)

        self.stdin = Stream()
        self.stdout = Stream(write_cb = self._stdout_write_cb_handler)
        self.stdout_write_signal.connect(self._stdout_data_handler)
        self._cmd = cmd
        self._cmd.stdin = self.stdin
        self._cmd.stdout = self.stdout
        self._cmd.use_rawinput = False

    def set_cmd(self, cmd):
        self._cmd = cmd

    def get_completions(self, line):
        return self._cmd.completenames(line)

    def _close(self):
        super(DanceConsole, self)._close()
        self._cmd.device.close()
        self.close()

    def closeEvent(self, event):
        self._close()
        event.accept()

    def evaluate_buffer(self, _buffer, echo_lines = True):
        lines = _buffer.split('\n')

        if len(lines) > 1:
            for line in lines:
                if (line != 'EOF'  and line != ''):

                    if echo_lines:
                        self._insert_prompt(line)

                    self._cmd.stdin.write(line + '\n')
        elif echo_lines:
            self._insert_in_buffer(lines[0])

    def _handle_ctrl_c(self):
        self.ctrl_c_signal.emit()

    def eval_in_thread(self):

        try:
            self._cmd.repl()
        except KeyboardInterrupt:
            print('KeyboardInterrupt in cmdloop, thread exit !')
            return

if __name__ == '__main__':
    import sys

    from ...device import DanceDevice
    from ...cmd import DanceCmd

    from optparse import OptionParser
    from ..qt.QtWidgets import (QApplication)

    exec_name = os.path.basename(__file__)
    exec_name  = os.path.splitext(exec_name)[0]

    usage = '{0} host:port \n'
    usage += 7*' ' + 'Default port is 5000'
    usage = usage.format(exec_name)
    parser = OptionParser(usage=usage)
    options, args = parser.parse_args()

    if len(args) == 1:
        device = DanceDevice(args[0])
        cmd = DanceCmd(device)

        app = QApplication([])
        console = DanceConsole(cmd = cmd)
        console.show()

        retcode = app.exec_()
    else:
        parser.print_help()
        retcode = 0

    sys.exit(retcode)
