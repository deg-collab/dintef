# -*- coding: utf-8 -*-
import copy
import json
import os


class FilePersistant(object):
    """
    Object that is capable of writing and loading it's data attributes from/to
    a file. The data is stored in JSON Format.
    """
    def __init__(self, fpath):
        self._fpath = fpath

    def _write(self, fpath):
        """
        Write the this object to fpath

        :param str fpath: Full path including file name
        :returns: None
        """
        with open(fpath, 'w') as f:
            json.dump(self.__dict__, f)

    def _load(self, fpath):
        """
        Load object from fpath

        :param str fpath: Full path including file name
        :returns: None
        """
        if os.path.isfile(fpath):
            with open(fpath, 'r') as f:
                sdict = json.load(f)
                self.__dict__.update(sdict)

            return True
        else:
            return False

    def write(self):
        """
        Write object to disk (To the path passed to init)
        """
        self._write(self._fpath)

    def load(self):
        """
        Load object from disk (From the file passed to init)
        """
        return self._load(self._fpath)


class AppSettings(FilePersistant):
    def __init__(self, fpath):
        super(AppSettings, self).__init__(fpath)
        self.cmd_history = []
        self.session_fpath = ''


class Session(FilePersistant):
    """
    Encapsulates data that are shared between various components and that can
    be easily accessed through various utility methods.
    """
    def __init__(self, fpath):
        super(Session, self).__init__(fpath)
        # Internal member, gives a session a unique id. So that they can
        # be referred to by number, if needed.
        self._id = 0

        # Contain configuration data, used by ScanEngine object and \
        # objects that derive Tool
        self.settings = {}

        # Name of last scan, required by the ScanEngine object
        self.last_scan_name = None
        # Flag that indicates if a scan is in progress, required by the
        # ScanEngine object
        self.scanning = False
        # Scan start time, the time the current or last scan was started,
        # required by the ScanEngine object
        self.scan_stime = 0
        # Last executed command line, in case a command line interpreter is used
        self.last_cli_line = None
        # Validated arguments of last command, arguments that were parsed
        # and validated
        self.last_cli_vargs = None
        # Ignored args of last command, arguments that were parsed but ignored
        # in the validation 
        self.last_cli_iargs = None

        self.settings['DATA'] = {
            'AUTO_SAVE': True,
            'DATA_DIR': os.path.expanduser("~/dance_data/"),
            'DATA_FILE': '{session_name}-session-data',
            'DATA_WARNING': True}

        self.settings['SCAN'] = {
            'SCAN_NAME': '[{num}]{cmd}',
            'ACQTIME': 1,
            'SCAN_WAIT': 0,
            'SCAN_RESET': True,
            'ACQ_METHODS': []}

    def _get_dict(self, path):
        parts = path.split('.')
        target_dict = self.settings

        for part in parts[:-1]:
            target_dict = target_dict.get(part, None)

            if not isinstance(target_dict, dict):
                return False

        return target_dict

    def _quote(self, value):
        if isinstance(value, str) or isinstance(value, unicode):
            return "'%s'" % value
        elif isinstance(value, list):
            s = '[%s' % self._quote(value[0])
            for v in value[1:]:
                s += ','.join(self._quote(value))

            return s + ']'
        else:
            return "%s" % value

    def _str_format_value(self, name, value):
        return "%s = %s\n" % (name, self._quote(value))

    def _str_format_values_rec(self, values, indent = 1):
        str_ = ''

        for key, value in values.iteritems():
            if isinstance(value, dict):
                str_ += key + ':\n'
                str_ += self._str_format_values_rec(value, indent + 1)
            else:
                str_ += indent*"  " + self._str_format_value(key, value)

        return str_

    def set_value(self, path, value):
        parts = path.split('.')
        self._get_dict(path)[parts[-1]] = value

    def get_value(self, path):
        parts = path.split('.')

        if parts[0] != '':
            return self._get_dict(path)[parts[-1]]
        else:
            return self._get_dict('')

    def str_format_values(self, path):
        parts = path.split('.')
        values = self.get_value(path)

        if isinstance(values, dict):
            if parts[-1]:
                s = parts[-1] + ":\n" + self._str_format_values_rec(values,1)
            else:
                s = self._str_format_values_rec(values, 1)
            return s
        else:
            return self._str_format_value(parts[-1], values)


class SessionManager(object):
    """
    Manages a directory with sessions, provides convinience methods for saving
    and loading sessions within the "managed" directory.
    """
    def __init__(self, session_root_dir, fname=None):
        self._session_root_path = session_root_dir
        self._fname = None
        self._session_po = None

        if fname != None:
            self.load(fname)

    def _get_free_session_id(self):
        """
        Gets the next free session id for a session located in <session_root>.
        Loads all sessions in <session_root> and looks for the next free id,
        so O(N) load operations.
        """
        names = os.listdir(self._session_root_path)
        _id, id_list = (0, [])

        for name in names:
            fpath = os.path.join(self._session_root_path, name)

            if os.path.isfile(fpath):
                session = Session(fpath)
                session.load()
                id_list.append(session._id)

        for i in range(1, 1000):
            if i not in id_list:
                _id = i
                break

        return _id

    def load(self, fname):
        """
        Load session with file name <fname> (located in root_path passed to
        init, that is the full path is session_root_dir/fname.

        :param str fname: Session file name
        :returns: None
        """
        if not fname:
            fname = 'default'

        fpath = os.path.join(self._session_root_path, fname)
        session = Session(fpath)

        # Load file at fpath, if it does not already exist assume that we
        # want to create it. Create a session named default if no fname is
        # given
        if os.path.isfile(fpath):
            session.load()
        elif fname:
            session.write()

        self._session_po = session
        self._fname = fname

    def save_as(self, fname):
        """
        Writes a copy of the current session to <fname>

        :param str fpath: File name within session_root to write to
        :returns: None
        """
        fpath =  os.path.join(self._session_root_path, fname)
        session_copy = copy.deepcopy(self._session_po)
        session_copy._id = self._get_free_session_id()
        session_copy._fpath = unicode(fpath)
        session_copy.write()

    def save(self):
        """
        Writes the current session to disk
        :returns: None
        """
        self._session_po.write()

    def delete(self, fname):
        """
        Deletes session file at <fpath>
        """
        fpath =  os.path.join(self._session_root_path, fname)

        if os.path.isfile(fpath) and fpath != self._session_po._fpath:
            os.unlink(fpath)

    def list(self):
        """
        Returns two dictionaries one with the names and the other with
        the ids as key. The values are either id or name depending on key used
        and a flag 'active' which indicates if its the currently used session.
        For example:

        {1: ('default', True), 2 : ('test', False)}
        {'default': (1, True), 'test' : (2, False)}

        :returns: A tuple with two dictionaries
        :rtype: Tuple
        """
        names = os.listdir(self._session_root_path)
        id_dict = {}
        name_dict = {}

        for name in names:
            fpath = os.path.join(self._session_root_path, name)
            if os.path.isfile(fpath):
                active = False

                if fpath == self._session_po._fpath:
                    active = True

                session = Session(fpath)
                session.load()

                name_dict[name] = session._id, active
                id_dict[session._id] = name, active

        return id_dict, name_dict

    def session(self):
        """
        :returns: current Session object
        :rtype: Session
        """
        return self._session_po

    def session_name(self):
        """
        :returns: The session name
        :rtype: str
        """
        return self._fname

    def set_value(self, path, value):
        self._session_po.set_value(path, value)

    def get_value(self, path):
        return self._session_po.get_value(path)

    def str_format_values(self, path):
        return self._session_po.str_format_values(path)
