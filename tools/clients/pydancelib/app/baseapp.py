# -*- coding: utf-8 -*-
import ctypes
import optparse
import os
import sys
import threading
import weakref
import errno
import time
import urlparse
import _app

from .session import SessionManager, AppSettings
from ..scan.engine import (ScanEngine, Sampler)
from ..io.datastore import ThreadedDataStore
from ..cmd.command import (ScanCommand, SessionCommand, MeasureCommand,
                           ConfigCommand, NewSessionCommand, SaveCommand,
                           LoopScanCommand, TimeScanCommand, NScanCommand,
                           MScanCommand, DScanCommand, DNScanCommand,
                           DMScanCommand, MoveCommand)

from ..cmd.interpreter import CommandInterpreter

class ApplicationBase(object):
    SESSION_ROOT = os.path.expanduser("~/.dance/sessions/")
    APP_HOME = os.path.expanduser("~/.dance/")

    def __init__(self, netloc_list):
        # It's safer to expose week references to the devices so that
        # any device and device command interpreter (cmd) lingering is
        # removed. The strong references are kept in _device_list
        self._command_interpreter = CommandInterpreter()
        self._command_interpreter.set_app(self)
        self._devices = weakref.WeakValueDictionary()
        self._device_list = []
        self._acq_methods = {}
        self._current_actuators = []
        self._ds = ThreadedDataStore()

        self.app_settings = self._load_app_settings()

        session_root_dir, fname = os.path.split(self.app_settings.session_fpath)

        if not session_root_dir:
            session_root_dir = ApplicationBase.SESSION_ROOT

        self.session_mgr = SessionManager(session_root_dir, fname)

        self.scan_engine = ScanEngine()
        self.scan_engine.register_app(self)
        self.scan_engine.set_data_store(self._ds)
        self.scan_engine.set_session_mgr(self.session_mgr)

        for netloc in netloc_list:
            self.add_device(netloc)

        self._set_scan_path()

    def _set_scan_path(self, fname=None):
        dname = self.session_mgr.get_value('DATA.DATA_DIR')
        sname = self.session_mgr.session_name()

        if not fname:
            fname = self.session_mgr.get_value('DATA.DATA_FILE')

        try:
            _date = time.strftime('%y%m%d', time.localtime())
            _time = time.strftime('%H%M%S', time.localtime())
            fname = fname.format(date=_date, time=_time, session_name=sname)
        except KeyError:
            fname = '%s-session-data' % sname

        scan_path = os.path.join(dname, fname)
        self._ds.set_file_path(scan_path)

    def _init_app_home(self):
        try:
            os.makedirs(ApplicationBase.SESSION_ROOT)
        except OSError, e:
            if e.errno != errno.EEXIST:
                raise

    def _load_app_settings(self):
        self._init_app_home()
        fpath = os.path.join(ApplicationBase.APP_HOME, 'settings.json')
        app_settings = AppSettings(fpath)

        if os.path.isfile(fpath):
            app_settings.load()

        return app_settings

    def _write_session(self, session):
        self.session_mgr.save()

    def _add_acq_method(self, method):
        self._acq_methods[method.name] = method

    def execute(self):
        """
        Executes the application, needs to be implemented by each subclass
        (Abstract)
        """
        raise NotImplemented

    def post_execute(self):
        """
        Cleanup and finalization, to be called last in execute
        :returns: The exit code, -1 for failure and 0 for success
        """
        self.app_settings.write()
        self.session_mgr.save()
        self._ds.stop()

        for dev in self._device_list:
            dev.close()

        return 0

    def resolve_remote_cmd(self, netloc):
        host, cmd = netloc.split('/')
        return self._devices[host].ackcommand, cmd

    def remove_device(self, netloc):
        del self._devices[netloc]

    def add_device(self, netloc):
        """
        Registers a device command interpreter and it's corresponding device to
        the application.

        :param DeepDevice device: A DeepDevice
        :param class cmd_cls: The Type of command interpreter to add
        :returns: None
        """
        if netloc in self._devices.keys():
            return

        device = self.DEVICE_CLS(netloc)
        # cmd = cmd_cls(device)

        # cmd.register_command('scan', ScanCommand)
        # cmd.register_command('session', SessionCommand)
        # cmd.register_command('meas', MeasureCommand)
        # cmd.register_command('config', ConfigCommand)
        # cmd.register_command('newsession', NewSessionCommand)
        # cmd.register_command('save', SaveCommand)
        # cmd.register_command('loopscan', LoopScanCommand)
        # cmd.register_command('timescan', TimeScanCommand)
        # cmd.register_command('nscan', NScanCommand)
        # cmd.register_command('mscan', MScanCommand)
        # cmd.register_command('dscan', DScanCommand)
        # cmd.register_command('dnscan', DNScanCommand)
        # cmd.register_command('dmscan', DMScanCommand)
        # cmd.register_command('mv', MoveCommand)

        self._device_list.append(device)
        self._devices[str(device.hostname())] = device

        method = Sampler(device.hostname() + '_acq', device)
        self._add_acq_method(method)

        if method.name not in self.session_mgr.get_value('SCAN.ACQ_METHODS'):
            self.session_mgr.get_value('SCAN.ACQ_METHODS').append(method.name)

    def reinit_device_cmd(self, hostname):
        """
        Reinitialize a device command interpreter and it's device

        :param hostname: The hostname (with port, separated by a :) of a device
        :returns: None
        """
        cmd, dev = self.get_device_cmd(hostname)
        dev.close()
        dev = dev.__class__(hostname)
        self.register_device_cmd(cmd.__class__, dev)

    def get_device_cmd(self, hostname=None):
        """
        Retrieves the device command interpreter and the device for a
        given hostname

        :param str hostname: The hostname of a device
        :returns: The device command interpreter and the device for
                  a given hostname
        """
        device = self._devices.values()

        if hostname:
            device = self._devices.get(hostname, None)

        return self._command_interpreter, device

    def user_msg(self, msg_str):
        cmd, dev = self.get_device_cmd()
        cmd.stdout.write(msg_str)

    # # Method called by scan procedure when scan is started
    def scan_init(self, scan_name, actuator_list=[]):
        pass

    # Method called by scan procedure after data acquisition
    def scan_data_acquired(self, scan_name, param, data):
        pass

    # Method called by scan procedure when scan ends
    def scan_end(self, scan_name, actuator_list):
        pass


class AppBase(object):
    SESSION_ROOT = os.path.expanduser("~/.dance/sessions/")
    APP_HOME = os.path.expanduser("~/.dance/")
    APP = None

    def __init__(self):
        self._device_dict = {}
        self._current_device = ''
        self._ds = ThreadedDataStore()
        self._init_app_home()

        app_settings = self._load_app_settings()
        self.session_mgr = self._init_session_mgr(app_settings)
        self.app_settings = app_settings

        self._ci = CommandInterpreter()
        self._ci.unknown_command_cb = self.remote_cmd_call_ext
        self._ci.command_executed_cb = self._ci_command_executed

        self.scan_engine = ScanEngine()
        self.scan_engine.register_app(self)
        self.scan_engine.set_data_store(self._ds)
        self.scan_engine.set_session_mgr(self.session_mgr)


    def _init_app_home(self):
        try:
            os.makedirs(ApplicationBase.SESSION_ROOT)
            os.makedirs(ApplicationBase.APP_HOME)
        except OSError, e:
            if e.errno != errno.EEXIST:
                raise

    def _load_app_settings(self):
        fpath = os.path.join(ApplicationBase.APP_HOME, 'settings.json')
        app_settings = AppSettings(fpath)

        if os.path.isfile(fpath):
            app_settings.load()

        return app_settings

    def _init_session_mgr(self, app_settings):
        session_root_dir, fname = os.path.split(app_settings.session_fpath)

        if not session_root_dir:
            session_root_dir = ApplicationBase.SESSION_ROOT

        return SessionManager(session_root_dir, fname)

    def _ci_command_executed(self, cmd, arg, line):
        self._session_mgr.session().last_cli_line = line

    def remote_cmd_call_ext(self, line, cmd, arg):
        func, cmd = self.resolve_remote_cmd_call(cmd)
        return func, [cmd] + arg

    def set_current_device(self, netloc):
        if not netloc:
            self._current_device = None    
        elif not self._device_dict.has_key(netloc):
            self.add_device(netloc)

        self._current_device = netloc

    def current_device(self):
        if self._current_device:
            return self._device_dict[self._current_device]
        else:
            return None

    def get_device(self, netloc, dev_cls=None):
        if netloc not in self._device_dict.keys():
            self.add_device(netloc, dev_cls)
        
        return self._device_dict[netloc]

    def devices(self):
        return self._device_dict.keys()

    def add_device(self, netloc, dev_cls=None):
        if not dev_cls:
            dev_cls = self.DEVICE_CLS

        self._device_dict[netloc] = dev_cls(netloc)

    def netloc_to_device(self, netloc):
        # If param end points is passed with a netloc and path but
        # not protocol add deep:// as default protocol
        if not '://' in netloc and '/' in netloc:
            netloc = 'deep://' + netloc

        if '/' in netloc:
            pres = urlparse.urlparse(netloc)
            netloc = pres.netloc
            # Ignore slash in beginning of path
            path = pres.path[1:]
            return netloc, path
        else:
            return self._current_device, netloc
        
    def remove_device(self, netloc):
        del self._device_dict[netloc]

    def resolve_remote_cmd_call(self, netloc):
        if '/' in netloc:
            host, cmd = netloc.split('/')
        elif self._current_device:
            host, cmd = self._current_device, netloc
        else:
            return None, None

        if self._device_dict.has_key(host):
            return self._device_dict[host].ackcommand, cmd
        else:
            raise AttributeError('Unable to connect to %s' % host)

    def reset_command_interpreter(self):
        pass

    def execute(self):
        """
        Executes the application, needs to be implemented by each subclass
        (Abstract)
        :returns: None
        """
        if _app._APP:
            raise RuntimeError('Application main loop is already running')

        _app._APP = self    
        
    def post_execute(self):
        """
        Cleanup and finalization, to be called last in any execute method

        :returns: The exit code, -1 for failure and 0 for success
        :rtype: int
        """
        self.app_settings.write()
        self.session_mgr.save()
        self._ds.stop()

        for host, dev in self._device_dict:
            dev.close()

        return 0

    def user_msg(self, msg_str):
        self._ci.stdout.write(msg_str)

    def scan_init(self, scan_name, actuator_list=[]):
        pass

    def scan_data_acquired(self, scan_name, param, data):
        pass

    def scan_end(self, scan_name, actuator_list):
        pass


class CLIApplication(ApplicationBase):
    """
    CLI Application object that can be used directly from the shell
    """
    def __init__(self, device_list):
        super(CLIApplication, self).__init__(device_list)

    def _run_cmd(self, hostname):
        # Run the command repl in a separate thread, "listening" for
        # KeyboardInterrupt.
        cmd, device = self.get_device_cmd(hostname)

        while True:
            try:
                self._run_in_thread(cmd)
            except KeyboardInterrupt:
                # If keyboard interrupt, re-initialize the device command
                # interpreter (and device)
                self.reinit_device_cmd(hostname)
                cmd, device = self.get_device_cmd(hostname)
            else:
                device.close()
                break

    def _run_in_thread(self, cmd):
        # Run cmd in new thread and try to kill it on KeyboardInterrupt
        thread = threading.Thread(target = cmd.cmdloop)
        thread.daemon = True

        try:
            thread.start()

            while thread.is_alive():
                thread.join(1)

        except KeyboardInterrupt:
            cmd.stdout.write('\nKeyboardInterrupt\n')
            cmd.stop()

            while thread.is_alive():
                _id = thread.ident
                _id, exobj = ctypes.c_long(_id), ctypes.py_object(KeyboardInterrupt)
                ctypes.pythonapi.PyThreadState_SetAsyncExc(_id, exobj)
                cmd.stop()
                thread.join(1)

            raise KeyboardInterrupt

    def execute(self):
        # We only support one device command interpreter at the moment
        dev = self._devices.values()[0]
        self._run_cmd(dev.hostname())
        ApplicationBase.post_execute(self)


def run_app(app_cls):
    #from pydancelib.device import DeviceError
    from pydancelib.ui.application import UIApp

    exec_name = os.path.basename(__file__)
    exec_name = os.path.splitext(exec_name)[0]

    usage = '{0} host:port \n'
    usage += 7*' ' + 'Default port is 5000'
    usage = usage.format(exec_name)
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("-n", "--noui", dest="noui", action="store_true",
                      help="Run without GUI toolkit, (just terminal)")
    options, args = parser.parse_args()
    noui = options.noui

    if len(args) == 1:
        #device = None
        app = None

        #try:
            #device = app_cls.DEVICE_CLS(args[0])

        #except DeviceError as ex:
        #    print(ex.msg)
        #    retcode = -1
        #else:
        if not noui:
            base_app_cls = UIApp
        else:
            base_app_cls = CLIApplication

        app_cls = type(app_cls.__name__, (base_app_cls, ),
                       app_cls.__dict__.copy())

        app = app_cls()

        if len(args) > 0:
            app.add_device(args[0])
        
        retcode = app.execute()
    else:
        parser.print_help()
        retcode = 0

    sys.exit(retcode)
