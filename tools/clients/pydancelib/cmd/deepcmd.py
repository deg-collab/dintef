# -*- coding: utf-8 -*-
"""
A Deep Device specific line-oriented command interpreters based on the Cmd
class of the cmd module. See the cmd module documentation for more information
"""

from __future__ import absolute_import, print_function

import sys
import os
from cmd import Cmd

# do not import Command to remove further dependencies
#from .command import Command

from libdeep import DeepDevice, DanceDevice, DeepError

class DeepCmd(Cmd):
    DEVCLS = DanceDevice
    cmd_globals = {'_': None, '__builtins__' : __builtins__}

    def __init__(self, device, completekey='tab', stdin=None, stdout=None):
        Cmd.__init__(self, completekey, stdin, stdout)
        self._cmd_exec = False
        self._stop = False

        DeepCmd.identchars += '*'

        if not isinstance(device, DeepDevice):
            raise TypeError
        else:
            self.device = device
            self.devcomm = device.getcommandlist()
            self.custom_cmd_names = self.extract_cmd_names()

        self.use_rawinput = True
        self._extension_commands = {}

        try:
            from pydancelib.io.stream import register_io, dprint, dinput
            self._register_io = register_io
            self._print = dprint
            self._input = dinput
        except ImportError:
            self._register_io = lambda stdin, stdout: None
            self._print = print
            # For python 2 and 3 compatibility
            try:
                self._input = raw_input
            except NameError:
                self._input = input


    def register_command(self, name, command_cls):
        self._extension_commands[name] = command_cls()

    def is_extcommand(self, name):
        return name in self._extension_commands

    """
    def get_extcommand(self, name):
        handler = self._extension_commands[name]
        return handler
    """

    def exec_extcommand(self, name, arg_str, line):
        cmd = self._extension_commands[name]

        # remove temporarily until a better fix
        #if isinstance(cmd, Command):
        self._app.session_mgr.session().last_cli_line = line
        cmd.execute(line, name, arg_str, app=self._app, device=self.device)

    def executing_cmd(self):
        return self._cmd_exec

    def in_repl(self):
        return (not self._stop)

    def stop(self):
        self._stop = True

    # Hook method declared by the CMD module
    def precmd(self, line):
        """
        Hook method executed just before the command line is
        interpreted, but after the input prompt is generated and issued.
        """
        self._cmd_exec = True
        if line == 'EOF':
            line = '.quit'

        return line

    # Hook method declared by the CMD module
    def postcmd(self, stop, line):
        """
        Hook method executed just after a command dispatch is finished.
        """
        if line != "":
            self.cmd_n += 1

        self.buildprompt()
        self._cmd_exec = False
        return stop

    # Hook method declared by the CMD module
    def preloop(self):
        """
        Hook method executed once when the cmdloop() method is called.
        """
        self.cmd_n = 1
        self.buildprompt()

        if self.use_rawinput and self.completekey:
            try:
                import readline
                readline.set_completer_delims(" ")
            except ImportError:
                pass

    # Hook method declared by the CMD module
    def postloop(self):
        """
        Hook method executed once when the cmdloop() method is about to
        return.
        """
        self.device = None

    # Overrides parseline in CMD module
    def parseline(self, line):
        """
        Parse the line into a command name and a string containing
        the arguments.  Returns a tuple containing (command, args, line).
        'command' and 'args' may be None if the line couldn't be parsed.
        """
        line = line.strip()

        if not line:
            return None, None, line

        elif line == '?' or line[0:2] == '? ':
            line = '.help ' + line[1:]

        elif line[0] == '!':
            if hasattr(self, 'do_shell'):
                line = 'shell ' + line[1:]
            else:
                return None, None, line

        i, n = (0, len(line))

        if line[0] == '.' or line[0] == '?':
             i = 1

        while i < n and line[i] in self.identchars:
            i = i + 1

        cmd, arg = line[:i], line[i:].strip()

        return cmd, arg, line


    def replacevars(self, strng):
        for var in self.cmd_globals:
            vname = '.' + var
            vlen = len(vname)
            pos = 0
            while True:
                pos = strng.find(vname, pos)
                if pos < 0:
                    break
                if strng[pos-1:pos].isalnum() or strng[pos+vlen:pos+vlen+1].isalnum():
                    pos += vlen
                    continue
                vval = str(self.cmd_globals[var])
                strng = strng[:pos] + vval + strng[pos+vlen:]
                pos += vlen
        return strng


    def onecmd(self, line):
        """
        Interpret the argument as though it had been typed in response
        to the prompt.

        This may be overridden, but should not normally need to be;
        see the precmd() and postcmd() methods for useful execution hooks.
        The return value is a flag indicating whether interpretation of
        commands by the interpreter should stop.
        """
        cmd, arg, line = self.parseline(line)

        if not line:
            return self.emptyline()

        if cmd is None:
            return self.default(line)

        self.lastcmd = line

        if cmd == '':
            return self.default(line)

        if cmd[0] == '.':
            arg = self.replacevars(arg)
            if hasattr(self, 'do_' + cmd[1:]):
                func = getattr(self, 'do_' + cmd[1:])

                try:
                    return func(arg)
                except KeyboardInterrupt:
                    pass

            elif self.is_extcommand(cmd[1:]):
                self.exec_extcommand(cmd[1:], arg, line)
            else:
                return self.default(line)

        elif self.device.isvalidcommand(cmd):
            if (cmd.startswith('*')):
                try:
                    lsplt = line.rsplit(None, 1)  # to work in Python2 and 3
                    var = lsplt[1]
                    if var[0] == '.':
                        var = var[1:]
                    elif var != '_':
                        raise
                    bindata = self.cmd_globals[var]
                    if isinstance(bindata, bytearray) or (bindata.size >= 0 and bindata.itemsize >= 0):
                        line = lsplt[0]
                    else:
                        bindata = None
                except Exception as e:
#                print(e.__class__.__name__, ':', e)
                    bindata = None
            else:
                bindata = None

            line = self.replacevars(line)
            self.processdevcmd(line, bindata)
        else:
            line = self.replacevars(line)
            self.executeusercode(line)

        return False


    def default(self, line):
        """Called on an input line when the command prefix is not recognized.

        If this method is not overridden, it prints an error message and
        returns.

        """
        self._print('*** Unknown syntax: {}\n'.format(line))


    def executeusercode(self, line):
        var = None
        line = line.replace('\\n', '\n')
        if not line.endswith('\n'): line += '\n'

        try:
            mode = 'eval'
            code = compile(line, '<userinput>', mode)
        except SyntaxError:
            varss = line.split('=')
            if len(varss) > 1: var = varss[0].strip()
            try:
                mode = 'single'
                code = compile(line, '<userinput>', mode)
            except SyntaxError:
                try:
                    mode = 'exec'
                    code = compile(line, '<userinput>', mode)
                except SyntaxError as e:
                    self._print("{}: {}".format(e.__class__.__name__, e))
                    return False
        try:
            if mode == 'eval':
                _ = eval(code, self.cmd_globals)
                self._print(_, '\n')
            else:
                exec(code, self.cmd_globals)
                if var in self.cmd_globals:
                    _ = self.cmd_globals[var]
                else:
                    _ = None
                self._print()

            if _ is not None:
                self.cmd_globals['_'] = _

        except Exception as e:
            self._print("{}: {}\n".format(e.__class__.__name__, e))
        return False


    def cmdloop(self, intro=None):
        """Repeatedly issue a prompt, accept input, parse an initial prefix
        off the received input, and dispatch to action methods, passing them
        the remainder of the line as argument.

        """
        self._register_io(self.stdin, self.stdout)
        self.preloop()
        if self.use_rawinput and self.completekey:
            try:
                import readline
                self.old_completer = readline.get_completer()
                readline.set_completer(self.complete)
                readline.parse_and_bind(self.completekey + ": complete")
            except ImportError:
                pass
        try:
            self._stop = False

            if intro is not None:
                self.intro = intro
                self._print(str(self.intro)+"\n")

            while not self._stop:
                if self.cmdqueue:
                    line = self.cmdqueue.pop(0)
                else:
                    if self.use_rawinput:
                        try:
                            line = self._input(self.prompt)
                        except EOFError:
                            line = 'EOF'
                    else:
                        self._print(self.prompt, end='')
                        line = self._input()
                        if not len(line):
                            line = 'EOF'
                        else:
                            line = line.rstrip('\r\n')

                line = self.precmd(line)
                self._stop = self.onecmd(line)
                self._stop = self.postcmd(self._stop, line)
        finally:
            self.postloop()
            if self.use_rawinput and self.completekey:
                try:
                    import readline
                    readline.set_completer(self.old_completer)
                except ImportError:
                    pass

    # Overrides method in CMD module
    def emptyline(self):
        """
        Called when an empty line is entered in response to the prompt.

        If this method is not overridden, it repeats the last nonempty
        command entered.
        """
        return None

    # Overrides method in CMD module
    def completenames(self, text, *ignored):
        names = []

        if text == "" or text[0] == '.':
            dotext = 'do_' + text[1:]
            names.extend(['.' + a[3:] + ' ' for a in self.get_names() if \
                          a.startswith(dotext)])

            names.extend(['.' + n + ' ' for n in self._extension_commands.keys() if \
                          n.startswith(text[1:])])

        if text == "" or text[0] != '.':
            text = text.upper()
            names.extend([a + ' ' for a in self.devcomm if a.startswith(text)])

        return names

    # Overrides method in CMD acmodule
    def get_names(self):
        return self.custom_cmd_names

    def buildprompt(self):
        """
        Builds the complete prompt
        """
        self.prompt = "\n{}.{}".format(self.cmd_n, self.newprompt())

    def newprompt(self):
        """
        Returns the current prompt string
        """
        return "{}> ".format(self.device.name().split(':')[0])

    def processdevcmd(self, cmd, bindata):
        """
        Execute device command
        """
        i = len(cmd) if not " " in cmd else str.index(cmd, " ")
        cmd = cmd[:i].upper() + cmd[i:]
        try:
            self._print("\n===> {} ".format(cmd))
            answer = self.device.ackcommand(cmd, bindata=bindata)
            if isinstance(answer, tuple):
                self._last_answer = answer[0]
                self._last_bindata = answer[1]
            else:
                self._last_answer = answer
                self._last_bindata = None

            self._print("{}\n".format(self._last_answer))

            if self._last_bindata is not None:
                nitems = self._last_bindata.nitems
                itembits = self._last_bindata.itemsize * 8
                if nitems == 0:
                    self._print("No binary data received")
                else:
                    self._print("Binary data received: {} {}-bit values".format(nitems, itembits))

        except DeepError as devexcept:
            self._print("Error from {}: {}\n".format(self.device.name(), devexcept))
        except:
            raise

    def extract_cmd_names(self):
        from inspect import getmembers, ismethod

        return [a[0] for a in getmembers(self, ismethod) \
                if (a[0].startswith("do_") and a[0][3] != "_")]

    def complete_help(self, *args):
        self._print("complete_help()")
        return self.completenames(*args)

    def do__nothing(self, arg):
        return False

    def do_quit(self, arg):
        self._print("\nExit ...\n")
        return True

    def testcmd(self, *args):
        self._print(*args)

    @classmethod
    def dev_cls_init(cls_, hostname):
        device = cls_.DEVCLS(hostname)
        return cls_(device)

    @classmethod
    def main(cls, devname=None, devclass=None):
        if not devname:
            if len(sys.argv) < 2:
                print("\nUsage: {} hostname\n".format(os.path.basename(sys.argv[0])))
                return(-1)
            else:
                devname = sys.argv[1]

        if not devclass:
            devclass = DanceDevice
        try:
            dev = devclass(devname)
            cmd = cls(dev)
        except (IOError, ImportError, DeepError) as ex:
            print("\nError: {}\n".format(ex))
            return(-1)
        else:
            cmd.cmdloop()
            return 0
