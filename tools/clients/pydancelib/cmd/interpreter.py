# -*- coding: utf-8 -*-
from __future__ import print_function

import string
import sys
import stdcommand
import inspect
import jedi

from ..ui.utils.text import columnize
from ..io.stream import register_io, dprint, dinput


class CommandInterpreter(object):
    """
    """
    IDENTCHARS = string.ascii_letters + string.digits + '_/?*'
    ruler = '='
    lastcmd = ''
    intro = None
    doc_leader = ""
    doc_header = "Documented commands (type help <topic>):"
    misc_header = "Miscellaneous help topics:"
    undoc_header = "Undocumented commands:"
    nohelp = "*** No help on %s"
    use_rawinput = True

    def __init__(self, completekey='tab', stdin=None, stdout=None):
        """
        The optional argument 'completekey' is the readline name of a
        completion key; it defaults to the Tab key. If completekey is
        not None and the readline module is available, command completion
        is done automatically. The optional arguments stdin and stdout
        specify alternate input and output file objects; if not specified,
        sys.stdin and sys.stdout are used.

        :param str completekey: The key to use to trigger gnu readline
                                completion
        :param FileObject stdin: File object to use for stdin
        :param FileObject stdout: File object to use for stdout
        """
        self._line_num = 0
        self._prompt = 'INCON '
        self._command_fun_dict = {}

        self.completekey = completekey
        self.unknown_command_cb = None
        self.command_executed_cb = None
        
        if stdin is not None:
            self.stdin = stdin
        else:
            self.stdin = sys.stdin

        if stdout is not None:
            self.stdout = stdout
        else:
            self.stdout = sys.stdout

        self.add_command_fun_from_module(stdcommand)

    def add_command_fun_from_module(self, module):
        """
        Adds functions from the module <module> as commands to the interpreter
        Functions starting with '_' or that are not defined directly in the
        module <module> are ignored. All other functions are considered as
        available commands.

        :param Module module: A python module
        :returns: None
        """
        for fun_name in dir(module):
            fun = getattr(module, fun_name)

            if inspect.isfunction(fun) and fun_name[0] != '_' and \
              module.__name__ == fun.__module__:
                
                self._command_fun_dict[fun_name] = fun   

    def prompt(self, prompt_str=None):
        """
        The optional argument <prompt_str> specifies the text to use before
        the line number.

        :param str prompt_str: The text to display before the line number of
                               the prompt.
        
        :returns: The current prompt
        """
        if prompt_str:
            self._prompt = prompt_str
        
        return self._prompt + "[%s]: " % self._line_num

    def repl(self, intro=None):
        """
        Runs the read eval print loop. Lines entered by the user are interpreted
        into a command and a set of arguments. The arguments are parsed to
        python data structures.

        The read eval print loop uses the following hook methods, documentation
        about what they do can be found in the respective method documentation
        (if the name of the method is not enough).

        - preloop()
        - precmd(line)
        - postcmd(stop, line)
        - postloop()

        In addition to these hooks there are the following call backs used.

        - unknown_command_cb
        - command_executed_cb

        :returns: None when the loop exits
        """
        register_io(self.stdin, self.stdout)
        self.preloop()

        if self.use_rawinput and self.completekey:
            try:
                import readline
                readline.set_completer_delims(" ")
                self.old_completer = readline.get_completer()
                readline.set_completer(self.complete)
                readline.parse_and_bind(self.completekey + ": complete")
            except ImportError:
                pass
        try:
            if intro is not None:
                self.intro = intro
            if self.intro:
                dprint(str(self.intro))

            stop = None
            while not stop:
                if self.use_rawinput:
                    try:
                        line = dinput(self.prompt())
                    except EOFError:
                        line = 'EOF'
                else:
                    dprint(self.prompt(), end='')
                    self.stdout.flush()
                    line = dinput()
                    if not len(line):
                        line = 'EOF'
                    else:
                        line = line.rstrip('\r\n')

                line = self.precmd(line)
                stop = self.parse_and_exec(line)
                stop = self.postcmd(stop, line)

            self.postloop()
        finally:
            if self.use_rawinput and self.completekey:
                try:
                    import readline
                    readline.set_completer(self.old_completer)
                except ImportError:
                    pass


    def precmd(self, line):
        """
        Hook method executed just before the command line is interpreted,
        but after the input prompt is generated and issued.

        :param str line: The line issued bu the user
        :rtype: str
        :returns: The line 
        """
        return line

    def postcmd(self, stop, line):
        """
        Hook method executed just after a command dispatch is finished.
        """
        if line:
            self._line_num += 1

        return stop

    def preloop(self):
        """
        Hook method executed once when the repl() method is called.
        """
        pass

    def postloop(self):
        """
        Hook method executed once when the repl() method is about to
        return.
        """
        pass

    def _parse_arg_str(self, arg_str):
        """
        Parses <arg_str> into python data structures, the following parsing is
        performed:

        Statements surrounded with quotes ' or "  are interpreted as strings:
        Example: "a string"

        Statements surrounded with "[ ]" and contains a comma separated sequence
        of values are interpreted as lists. (Nested lists are not supported)
        Example: [1, 2, 'a string']

        Statements surrounded with "$( )" are first evaluated as python
        expression, the expression have to return a valid python data structure.
        Example: $(1<<32)

        Numbers are parsed as Float if a decimal . is given other wise as
        Integer
        Example: 1.2

        A sequence of characters with out white space ' ' or considered as a
        symbolic string and are pares as a python str
        Example: a_str_symbol

        Strings True and False are parsed into Boolean values (True and False)
        Example: False

        Three numbers separated with ':' are parsed into a tuple (considered to
        be a range)
        Example: 1:10:2

        :param str arg_str: The string to parse
        :returns: A list of arguments (Python data structures)
        """
        arg_list = []

        while arg_str:
            # We have a string argument (starts with a ' or ") find string end
            # marker (' or ")
            if arg_str[0] in ['"', "'"]:
                idx = arg_str[1:].find(arg_str[0])

                if idx < 0:
                    msg = "Invalid syntax, can't find end of string "
                    msg += "starting with:\n"
                    msg += "%s" % arg_str[0:]
                    return None, msg

                arg_list.append(arg_str[1:idx + 1])

                # Ignore the ending ' or "
                arg_str = arg_str[idx + 2:]

            elif arg_str[0] == '[':
                idx = arg_str[1:].find(']')

                if arg_str[1:].find('[') != -1 and arg_str[1:].find('[') < idx:
                    return None, 'Nested lists not yet supported'

                list_parts = arg_str[1:idx + 1].split(',')
                list_arg = []

                for part in list_parts:
                    parsed_part, msg = self._parse_arg_str(part)

                    if parsed_part:
                        list_arg.extend(parsed_part)
                    else:
                        return parsed_part, msg

                arg_list.append(list_arg)

                #Skip ending ']'
                arg_str = arg_str[idx + 1:]

            elif arg_str[0:2] == '$(':
                opened, closed, unclosed = [], [], []
                pos = 0

                # Find the last un-balanced parenthesis so we can find the
                # end of the expression
                for c in arg_str[2:]:
                    if c == '(':
                        opened.append(pos)
                    elif c == ')':
                        if len(opened):
                            s = opened.pop(-1)
                            closed.append((s, pos))
                        else:
                            unclosed.append(pos)
                    pos += 1

                idx = unclosed[0] + 2

                if idx < 0:
                    msg = "Invalid syntax, can't find end of expression "
                    msg += "starting with:\n"
                    msg += "%s" % arg_str[0:]
                    return None, msg

                expr_arg = self.arg_expr_parse(arg_str[2:idx])

                if expr_arg[0]:
                    arg_list.append(expr_arg[0])
                else:
                    return None, expr_arg[1]

                arg_str = arg_str[idx + 1:]

            # We have either a number, range or symbolic name
            elif arg_str[0].isalnum() or arg_str[0] in ['*', '?', '-']:
                # Locate end of character sequence, looking for a trailing
                # white-space
                idx = arg_str[0:].find(' ')

                if idx > 0:
                    arg = arg_str[0:idx].strip()
                    arg_str = arg_str[idx:]
                else:
                    arg = arg_str[0:].strip()
                    arg_str = ''

                # Range or number
                if arg[0].isdigit():
                    if ':' in arg:
                        range_arg = self.arg_range_parse(arg)

                        if range_arg[0] != None:
                            arg_list.append(range_arg)
                        else:
                            return None, range_arg[1]

                    elif self.arg_is_number(arg):
                        arg_list.append(self.arg_number_parse(arg))
                    else:
                        return None, '%s is not valid number' % arg

                # We have a symbolic name (a string without white-spaces)
                # or a Boolean value
                elif arg[0].isalpha() or arg[0] in ['*', '?', '-']:
                    if self.arg_is_bool(arg):
                        arg_list.append(self.arg_bool_parse(arg))
                    else:
                        arg_list.append(str(arg))

            # Ingore unhandled characters
            else:
                arg_str = arg_str[1:]

        return arg_list, ''

    def arg_range_parse(self, range_arg):
        """
        :param str range_arg: String with numbers on the form start:stop:step
        :returns: Tuple with (start, stop, step)
        """
        steps = range_arg.split(':')
        start, end, npoints = 1,1,1

        try:
            if len(steps) == 2:
                start, end = map(float, steps)
            elif len(steps) == 3:
                start, end, npoints = map(float, steps)
            else:
                msg = "'%s' is not a valid range" % ':'.join(steps)
                return None, msg
        except ValueError:
            msg = "'%s' is not a  valid range" % ':'.join(steps)
            return None, msg

        return start, end, npoints

    def arg_expr_parse(self, expr):
        """
        Evaluates expr in python (using eval) and returns the result.
        """
        arg = None

        try:
            arg = eval(expr)
        except Exception as ex:
            msg = 'Error while evaluating expression %s \n' % expr
            msg += str(ex)
            return None, msg

        return arg, ''

    def arg_is_bool(self, arg):
        """
        :param str arg: The argument to check
        :returns: True if the argument is considered to as a boolean False
                  otherwise.
        """
        return arg.lower() == 'false' or arg.lower() == 'true'

    def arg_bool_parse(self, arg):
        """
        :param str arg: Argument to parse
        :returns: True of arg is 'True' and False if its 'False'
        """
        if arg.lower() == 'false':
            return False
        elif arg.lower() == 'true':
            return True

    def arg_is_number(self, s):
        """
        :param str s: The argument to check
        :returns: True if the argument is a number otherwise False
        """
        try:
            float(s)
            return True
        except ValueError:
            return False

    def arg_number_parse(self, arg):
        """
        :param str arg: The string to parse
        :returns: A Float if arg contains a decimal . otherwise an Int
        """
        if '.' in arg:
            return(float(arg))
        else:
            return(int(arg))

    def parseline(self, line):
        """
        Parse the line into a command name and a string containing
        the arguments.  Returns a tuple containing (command, args, line).
        'command' and 'args' may be None if the line couldn't be parsed.
        """
        line = line.strip()

        if not line:
            return None, None, line
        elif line[0] == '!':
            if hasattr(self, 'do_shell'):
                line = 'shell ' + line[1:]
            else:
                return None, None, line

        i, n = 0, len(line)
        while i < n and line[i] in self.identchars:
            i = i + 1

        cmd, arg = line[:i], line[i:].strip()
        arg_list, msg = self._parse_arg_str(arg)
        return cmd, arg_list, line

    def parse_and_exec(self, line):
        """
        Parse line into command and arguments, execute the function, if any,
        that corresponds to the command.

        :param line: The line to parse and execute
        :returns: None
        
        """
        cmd, arg, line = self.parseline(line)

        if not line:
            return self.emptyline()

        if line == 'EOF' :
            self.lastcmd = ''
        else:
            self.lastcmd = line

        try:
            if cmd == 'help':
                func = self.get_usage_fun
            elif cmd in self._command_fun_dict.keys():
                func = self._command_fun_dict[cmd]
            else:            
                if callable(self.unknown_command_cb):
                    func, arg = self.unknown_command_cb(line, cmd, arg)

            if func:
                res = func(*tuple(arg))
                self.handle_result(res)
            else:
                self.unknown_syntax(line)

        except TypeError as ex:
            ex_str = str(ex).replace(cmd+'()', cmd)
            dprint("\n" + ex_str + "\n")
        except Exception as ex:
            dprint("\nError while executing %s \n" % cmd)
            dprint(str(ex) + "\n")
            import traceback
            traceback.print_exc()
        else:
            if callable(self.command_executed_cb):
                self.command_executed_cb(cmd, arg, line)

    def get_usage_fun(self, cmd):
        """
        Executes the usage function, function with prefix _usage_, for the
        command <cmd>. The usage function for the command 'test' would be
        _usage_test.

        :param str cmd: The str corresponding to a command 
        :returns: None
        """
        try:
            func = getattr(stdcommand, '_usage_' + cmd)
            func(cmd)
        except AttributeError:
            dprint("%s\n"%str(self.nohelp % (cmd,)))

        return

    def emptyline(self):
        """
        Called when an empty line is entered.
        """
        pass

    def unknown_syntax(self, line):
        """
        Called on an input line when the command is not recognized.
        """
        dprint('\nUnknown syntax: %s' % line)

    def handle_result(self, res):
        """
        Handles the result returned by a command function. Prints the result
        as a str after the 'ans' prompt.
        """
        if res:
            ansp = "ANS [%s]:" % self._line_num
            resstr = "\n\n%s\n" % res
            alignws = (len(self.prompt()) - len(ansp) - 1) * ' '
            dprint(alignws + ansp + resstr)

    def completedefault(self, *ignored):
        """
        Method called to complete an input line when no command-specific
        complete_*() method is available.

        By default, it returns an empty list.
        """
        return []

    def completenames(self, line, *ignored):
        """
        Returns the completion alternatives for line
        
        :param str line: The line to complete
        :returns: A list of alternatives
        """
        cmd, arg, line = self.parseline(line)

        # A valid command was entered, get potential options / sub commands
        if cmd and cmd in self._command_fun_dict:
            ns_dict = {}
    
            for fun_name in dir(stdcommand):
                if '_' + cmd + '_' in fun_name:
                    prefix, sub_cmd_name = fun_name.split('_' + cmd + '_')
                    ns_dict[cmd + ' ' + sub_cmd_name] = getattr(stdcommand, fun_name)                    
            return self.get_complete_names(line, ns_dict)
        else:
            return self.get_complete_names(line, self._command_fun_dict)
                    

    def get_complete_names(self, line, ns_dict):
        """
        Get the completion alternatives in the namespace dict <ns_dict> for
        <line>.

        :param str line: The line to get completion alternatives for
        :param ns_dict: A namespace dict to look in
        :returns: A list of names
        """
        names = []
        script = jedi.Interpreter(line, [ns_dict])
        
        for completion in script.completions():            
            if completion.name in ns_dict.keys():
                names.append(completion.name)

        return names

    def complete(self, text, state):
        """
        Return the next possible completion for 'text'.

        If a command has not been entered, then complete against command list.
        Otherwise try to call complete_<command> to get list of completions.
        """
        if state == 0:
            import readline
            origline = readline.get_line_buffer()
            line = origline.lstrip()
            stripped = len(origline) - len(line)
            begidx = readline.get_begidx() - stripped
            endidx = readline.get_endidx() - stripped
            if begidx>0:
                cmd, args, foo = self.parseline(line)
                if cmd == '':
                    compfunc = self.completedefault
                else:
                    try:
                        compfunc = getattr(self, 'complete_' + cmd)
                    except AttributeError:
                        compfunc = self.completedefault
            else:
                compfunc = self.completenames
            self.completion_matches = compfunc(text, line, begidx, endidx)
        try:
            return self.completion_matches[state]
        except IndexError:
            return None

    def get_names(self):
        # This method used to pull in base class attributes
        # at a time dir() didn't do it yet.
        return dir(stdcommand)

    def complete_help(self, *args):
        commands = set(self.completenames(*args))
        topics = set(a[5:] for a in self.get_names()
                     if a.startswith('help_' + args[0]))
        return list(commands | topics)

    def print_topics(self, header, cmds, cmdlen, maxcol):
        if cmds:
            dprint("%s\n"%str(header))
            if self.ruler:
                dprint("%s\n"%str(self.ruler * len(header)))
            columnize(cmds)

if __name__ == "__main__":
    CommandInterpreter().repl()
