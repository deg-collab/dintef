#!/usr/bin/env python
import gdtools
from distutils.core import setup

setup(name='gdtools',
    version=gdtools.__version__,
    description=gdtools.__description__,
    author=gdtools.__author__,
    author_email=gdtools.__author_email__,
    url=gdtools.__url__,
    packages=['gdtools'])
