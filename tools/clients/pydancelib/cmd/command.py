# -*- coding: utf-8 -*-
import os

from ..io.stream import dprint, getvalue
from ..scan.procedure import (scan, measure, loopscan, timescan, nscan,
                              mscan, dscan, dnscan, dmscan, value_iterator)
from ..scan.engine import run_with_cleanup
from ..ui.utils.text import columnize
from ..ui.tools import (PixelDisplayTool, PlotTool)


class Command(object):
    def execute(self, line, name, arg_str, **kwargs):
        args, emsg = self.parse_command_args(arg_str)

        if emsg:
            dprint('\nError while parsing command arguments, make sure that ')
            dprint('arguments are correctly formatted.\n\nError:')
            dprint(emsg + '\n')
            return

        options = self.option_map().keys()
        option, args = Command.get_option(args, self.option_map())

        try:
            vargs, iargs, valid, emsg = \
              self.validate_args(line, args, option, options, **kwargs)
            kwargs['app'].session_mgr.session().last_cli_iargs = iargs
            kwargs['app'].session_mgr.session().last_cli_vargs = vargs
        except:
            vargs, iargs, valid = (), (), False,
            emsg = 'Invalid number or type of arguments'

        if not valid:
            dprint('\n' + emsg)
            dprint(self._usage_msg(name, options, **kwargs))
        elif option in self.option_map():
            self.option_map()[option](*vargs, **kwargs)
        else:
            dprint(self._usage_msg(name, options, **kwargs))

    def _usage_msg(self, cmd_name, options, **kwargs):
        usage_opts = [opt for opt in options if opt != '']
        usage_msg = '\n'

        if len(options) and options[0] != '':
            usage_msg ='usage: .{cmd_name} [%s] \n\n' % '|'.join(usage_opts)

        usage_msg += self.usage_msg(options, **kwargs)

        return usage_msg.format(cmd_name=cmd_name)

    def parse_command_args(self, arg_str):
        arg_list = []

        while arg_str:
            # We have a string argument (starts with a ' or ") find string end
            # marker (' or ")
            if arg_str[0] in ['"', "'"]:
                idx = arg_str[1:].find(arg_str[0])

                if idx < 0:
                    msg = "Invalid syntax, can't find end of string "
                    msg += "starting with:\n"
                    msg += "%s" % arg_str[0:]
                    return None, msg

                arg_list.append(arg_str[1:idx + 1])

                # Ignore the ending ' or "
                arg_str = arg_str[idx + 2:]

            elif arg_str[0] == '[':
                idx = arg_str[1:].find(']')

                if arg_str[1:].find('[') != -1 and arg_str[1:].find('[') < idx:
                    return None, 'Nested lists not yet supported'

                list_parts = arg_str[1:idx + 1].split(',')
                list_arg = []

                for part in list_parts:
                    parsed_part, msg = self.parse_command_args(part)

                    if parsed_part:
                        list_arg.extend(parsed_part)
                    else:
                        return parsed_part, msg

                arg_list.append(list_arg)

                #Skip ending ']'
                arg_str = arg_str[idx + 1:]

            elif arg_str[0:2] == '$(':
                opened, closed, unclosed = [], [], []
                pos = 0

                # Find the last un-balanced parenthesis so we can find the
                # end of the expression
                for c in arg_str[2:]:
                    if c == '(':
                        opened.append(pos)
                    elif c == ')':
                        if len(opened):
                            s = opened.pop(-1)
                            closed.append((s, pos))
                        else:
                            unclosed.append(pos)
                    pos += 1

                idx = unclosed[0] + 2

                if idx < 0:
                    msg = "Invalid syntax, can't find end of expression "
                    msg += "starting with:\n"
                    msg += "%s" % arg_str[0:]
                    return None, msg

                expr_arg = self.cmd_expr_parse(arg_str[2:idx])

                if expr_arg[0]:
                    arg_list.append(expr_arg[0])
                else:
                    return None, expr_arg[1]

                arg_str = arg_str[idx + 1:]

            # We have either a number, range or symbolic name
            elif arg_str[0].isalnum() or arg_str[0] in ['*', '?']:
                # Locate end of character sequence, looking for a trailing
                # white-space
                idx = arg_str[0:].find(' ')

                if idx > 0:
                    arg = arg_str[0:idx].strip()
                    arg_str = arg_str[idx:]
                else:
                    arg = arg_str[0:].strip()
                    arg_str = ''

                # Range or number
                if arg[0].isdigit():
                    if ':' in arg:
                        range_arg = self.cmd_arg_range_parse(arg)

                        if range_arg[0] != None:
                            arg_list.append(range_arg)
                        else:
                            return None, range_arg[1]

                    elif self.is_number(arg):
                        arg_list.append(self.number_parse(arg))
                    else:
                        return None, '%s is not valid number' % arg

                # We have a symbolic name (a string without white-spaces)
                # or a Boolean value
                elif arg[0].isalpha() or arg[0] in ['*', '?']:
                    if self.is_bool(arg):
                        arg_list.append(self.bool_parse(arg))
                    else:
                        arg_list.append(str(arg))

            # Ingore unhandled characters
            else:
                arg_str = arg_str[1:]

        return arg_list, ''

    def cmd_arg_range_parse(self, range_arg):
        steps = range_arg.split(':')
        start, end, npoints = 1,1,1

        try:
            if len(steps) == 2:
                start, end = map(float, steps)
            elif len(steps) == 3:
                start, end, npoints = map(float, steps)
            else:
                msg = "'%s' is not a valid range" % ':'.join(steps)
                return None, msg
        except ValueError:
            msg = "'%s' is not a  valid range" % ':'.join(steps)
            return None, msg

        return start, end, npoints

    def cmd_expr_parse(self, expr):
        arg = None

        try:
            arg = eval(expr)
        except Exception as ex:
            msg = 'Error while evaluating expression %s \n' % expr
            msg += str(ex)
            return None, msg

        return arg, ''

    def is_bool(self, arg):
        return arg.lower() == 'false' or arg.lower() == 'true'

    def bool_parse(self, arg):
        if arg.lower() == 'false':
            return False
        elif arg.lower() == 'true':
            return True

    def is_number(self, s):
        try:
            float(s)
            return True
        except ValueError:
            return False

    def number_parse(self, arg):
        if '.' in arg:
            return(float(arg))
        else:
            return(int(arg))

    @staticmethod
    def get_option(arg_list, option_map):
        if len(option_map):
            if len(arg_list):
                if arg_list[0] in option_map:
                    return arg_list[0], arg_list[1:]
                else:
                    return '', arg_list
            else:
                return '', arg_list

        return None, arg_list


class SessionCommand(Command):
    @staticmethod
    def option_map():
        return {'ls': SessionCommand.session_list_cmd,
                'save': SessionCommand.session_save_cmd,
                'load': SessionCommand.session_load_cmd,
                'rm': SessionCommand.session_rm_cmd}

    def usage_msg(self, options, app=None, device=None):
         usage_msg = "usage: .{cmd_name} ls \n"
         usage_msg += "usage: .{cmd_name} rm <name|num> \n"
         usage_msg += "usage: .{cmd_name} save <name> \n"
         usage_msg += "usage: .{cmd_name} load <name|num> \n"

         if app:
             usage_msg += '\nAvailable sessions:\n\n%s'
             usage_msg = usage_msg % SessionCommand.session_get_list(app)

         return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        session_names = [n for n, a in app.session_mgr.list()[0].values()]

        if (option == '' and len(arg_list)) and (arg_list[0] not in options):
            msg = '%s is not a valid option should be one of %s'
            msg = msg % (arg_list[0], ', '.join(options))

            if arg_list[0] == '':
                msg = 'No option given should be one of: %s'
                msg = msg % ', '.join(options)

            return (), (), False, msg

        if not len(arg_list) == 1 and option and option != 'ls':
            return (), (), False, '%s: no session name given' % option

        if option == 'new':
            if arg_list[0] in session_names:
                msg = "%s: a session named '%s' already exists"
                msg = msg % (option, arg_list[0])
                return (), (), False, msg

        elif option == 'load':
            valid, msg = SessionCommand.is_session_name(arg_list[0], app, option)

            if not valid:
                return (), (), valid, msg

        elif option == 'rm':
            valid, msg = SessionCommand.is_session_name(arg_list[0], app, option)

            if not valid:
                return (), (), valid, msg

            if SessionCommand.is_session_active(arg_list[0], app):
                msg = "Can't remove currently active session"
                return (), (), False, msg

        elif option == 'ls':
            if len(arg_list) != 0:
                msg = "To many arguments given to %s" % option
                return (), (), False, msg

        return tuple(arg_list), (), True, ''

    @staticmethod
    def is_session_name(sname, app, option):
        valid, msg = True, ''

        if isinstance(sname, int) and sname not in app.session_mgr.list()[0].keys():
            msg = '%s: no session with num %s' % (option, sname)
            valid = False
        elif isinstance(sname, str) and sname not in app.session_mgr.list()[1].keys():
            msg = '%s: no session with name %s' % (option, sname)
            valid = False

        return valid, msg

    @staticmethod
    def is_session_active(sname, app):
        id_dict, name_dict = app.session_mgr.list()
        name, active = id_dict.get(sname, (None, False))

        if not name:
            name, active = name_dict.get(sname, (None, False))

        return active

    @staticmethod
    def session_get_list(app):
        slist = []

        for idx, value in app.session_mgr.list()[0].iteritems():
            name, active = value
            sname = '[%s] ' % idx + name
            sname = sname = '* ' + sname if active else sname
            slist.append(sname)

        return columnize(slist, colsep = '  |  ')

    @staticmethod
    def session_list_cmd(app=None, device=None):
        dprint('\n' + SessionCommand.session_get_list(app))

    @staticmethod
    def session_load_cmd(sname, app=None, device=None):
        if isinstance(sname, int):
            sname = app.session_mgr.list()[0].get(int(sname), None)[0]

        app.session_mgr.load(sname)
        SessionCommand.session_list_cmd(app, device)

    @staticmethod
    def session_save_cmd(fname, app=None, device=None):
        app.session_mgr.save_as(fname)
        SessionCommand.session_list_cmd(app, device)

    @staticmethod
    def session_rm_cmd(sname, app=None, device=None):
        if isinstance(sname, int):
            sname = app.session_mgr.list()[0].get(int(sname), None)[0]

        app.session_mgr.delete(sname)
        SessionCommand.session_list_cmd(app, device)


class ScanCommand(Command):
    @staticmethod
    def option_map():
        return {'' :ScanCommand.scan_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name} param start end nint \n"
        usage_msg += "       .{cmd_name} [v_1, ... ,v_n-1, v_n] \n\n"
        usage_msg += "Any arguments following the value range or the " +\
                     "value list is \npassed to the acquisition method\n\n"
        usage_msg += "The param argument can either be a parameter name " +\
                     "of the \ncurrent instrument, or the full parameter " +\
                     "address;\n\n    <protocol>://<host>:<port>/<name> \n" +\
                     "    e.g.: deep://comex019/dacovl\n"
                     
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        args, ignored_args, valid = (), (), True

        if option == '':
            if len(arg_list) < 2:
                return (), (), False, 'Not enough arguments'
            elif isinstance(arg_list[1], list):
                args = arg_list[0].upper(), arg_list[1], line
                ignored_args = tuple(arg_list[2:])
            elif len(arg_list) >= 4:
                try:
                    param = arg_list[0].upper()
                    start, end, npoints = tuple(map(float, arg_list[1:4]))
                except ValueError:
                    msg = 'Invalid range %s:%s:%s ' % tuple(arg_list[1:4])
                    return (), (), False, msg

                args = param, (start, end, npoints), line
                ignored_args = tuple(arg_list[4:])
            else:
                return (), (), False, 'Invalid number of arguments'

        return args, ignored_args, valid, ''

    @staticmethod
    def scan_cmd(param, value_arg, line, app=None, device=None):
        actlst = [app.scan_engine.get_actuator(param)]
        value_iter, start, end, step = value_iterator(value_arg)
        valid, msg = actlst[0].check_range(start, end)

        if not valid:
            dprint('\n%s\n' % msg)
            return

        if step:
            msg = '\nScanning %s from %G to %G totally %G intervals '
            msg += '(with step %G)'
            msg = msg % (param, start, end, value_arg[2], step)
        else:
            msg = '\nScanning %s with: %s' % (param, value_arg)

        dprint(msg)
        run_with_cleanup(scan, app.scan_engine, actlst, [value_iter])


class DScanCommand(Command):
    @staticmethod
    def option_map():
        return {'' :DScanCommand.dscan_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name} param start end nint \n"
        usage_msg += "       .{cmd_name} [v_1, ... ,v_n-1, v_n] \n\n"
        usage_msg += "Any arguments following the value range or the " +\
                     "value list is \npassed to the acquisition method\n\n"
        usage_msg += "The param argument can either be a parameter name " +\
                     "of the \ncurrent instrument, or the full parameter " +\
                     "address;\n\n    <protocol>://<host>:<port>/<name> \n" +\
                     "    e.g.: deep://comex019/dacovl\n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        return ScanCommand.validate_args(line, arg_list, option, options, device, app)

    @staticmethod
    def dscan_cmd(param, value_arg, line, app=None, device=None):
        actlst = [app.scan_engine.get_actuator(param)]
        value_iter, start, end, step = value_iterator(value_arg)
        valid, msg = actlst[0].check_range(start, end)

        if not valid:
            dprint('\n%s\n' % msg)
            return

        if step:
            msg = '\nScanning %s from %G to %G totally %G intervals '
            msg += '(with step %G)'
            msg = msg % (param, start, end, value_arg[2], step)
        else:
            msg = '\nScanning %s with: %s' % (param, value_arg)

        dprint(msg)
        run_with_cleanup(dscan, app.scan_engine, actlst, [value_iter])


class MeasureCommand(Command):
    @staticmethod
    def option_map():
        return {'' :MeasureCommand.meas_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name}\n\n"
        usage_msg += "Any arguments given are passed to the acquisition "+\
                     "method \n\n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        return (line,), tuple(arg_list), True, ''

    @staticmethod
    def meas_cmd(line, device=None, app=None):
        run_with_cleanup(measure, app.scan_engine, [], [])


class LoopScanCommand(Command):
    @staticmethod
    def option_map():
        return {'' : LoopScanCommand.loopscan_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name} <n>\n\n"
        usage_msg += "Any arguments following the number of iterations n\n" +\
                     "are passed to the acquisition method\n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        valid = True

        if option == '':
            if len(arg_list) < 1:
                msg = 'Command takes exactly one argument <n>; '
                msg += 'number of iterations '
                return (), (), False, msg

            if not Command().is_number(arg_list[0]):
                msg = 'Number of iterations <n> have to be a number'
                return (), (), False, msg

        return tuple(arg_list[0:1] + [line]), tuple(arg_list[1:]), valid, ''

    @staticmethod
    def loopscan_cmd(ntimes, line, device=None, app=None):
        run_with_cleanup(loopscan, app.scan_engine, [], [range(0, ntimes)])


class TimeScanCommand(Command):
    @staticmethod
    def option_map():
        return {'' : TimeScanCommand.timescan_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name}\n\n"
        usage_msg += "Any arguments given are passed to the acquisition "+\
                     "method \n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        return (line,), tuple(arg_list), True, ''

    @staticmethod
    def timescan_cmd(line, device=None, app=None):
        run_with_cleanup(timescan, app.scan_engine, [], [])


class ConfigCommand(Command):
    @staticmethod
    def option_map():
        return {'ls' : ConfigCommand.config_ls_cmd,
                'set': ConfigCommand.config_set_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name} ls\n"
        usage_msg += "usage: .{cmd_name} ls <path>\n"
        usage_msg += "usage: .{cmd_name} set <path> <value>\n"
        usage_msg += "\nWhere <path> is the dot (.) separated path to the " +\
                     "variable to access. i.e. SCAN.ACQTIME\n\n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        path, value, valid = '', None, True

        if option in ['ls', 'set']:
            if len(arg_list) > 0:
                path = arg_list[0].upper()

            if option == 'ls':
                return (path,), (), True, ''

        if option == 'set':
            if len(arg_list) > 1:
                value = arg_list[1]

                try:
                    variable = app.session_mgr.get_value(path)

                    if isinstance(variable, dict):
                        msg = "The path %s contains a set of variables, and "
                        msg +="can't be set\n"
                        msg = msg % path
                        return (), (), False, msg

                except KeyError:
                    msg = 'No group of settings with name %s' % path
                    return (), (), False, msg
                else:
                    return (path, value), (), True, ''
            else:
                return (), (), False, 'Not enough arguments'

        return (), (), False, 'No option supplied use one of [%s]'%'|'.join(options)

    @staticmethod
    def config_ls_cmd(path, device=None, app=None):

        try:
            s = app.session_mgr.str_format_values(path)
            dprint('\n%s' % s)
        except KeyError:
             dprint('\nNot parameter at path: %s \n' % path)
             dprint('Available parameters:')
             s = app.session_mgr.str_format_values('')
             dprint('\n%s' % s)

    @staticmethod
    def config_set_cmd(path, value, device=None, app=None):
        app.session_mgr.session().set_value(path, value)
        dprint('\n' + app.session_mgr.session().str_format_values(path))


class NewSessionCommand(Command):
    @staticmethod
    def option_map():
        return {'' : NewSessionCommand.new_session_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name}\n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        if len(arg_list) > 0:
            return (), (), False, 'To many arguments supplied'
        else:
            return tuple(arg_list), (), True, ''

    @staticmethod
    def new_session_cmd(device=None, app=None):
        msg = 'Directory where to store data (DATA_DIR)'
        data_dir = getvalue(msg, default="~/dance_data/")
        data_dir = os.path.expanduser(data_dir)

        msg = 'File name (DATA_FILE)'
        data_file = getvalue(msg, default='session-{date}')

        app.session_mgr.set_value('DATA.DATA_DIR', data_dir)
        app.session_mgr.set_value('DATA.DATA_FILE', data_file)

        dprint('\nNew data configuration:')
        dprint(app.session_mgr.str_format_values('DATA'))


class SaveCommand(Command):
    @staticmethod
    def option_map():
        return {'' : SaveCommand.save_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name}\n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        if len(arg_list) > 0:
            return (), (), False, 'To many arguments supplied'
        else:
            return tuple(arg_list), (), True, ''

    @staticmethod
    def save_cmd(device=None, app=None):
        app.scan_engine.save_data(app.session_mgr.session().last_scan_name)
        dprint()


class ShowCommand(Command):
    @staticmethod
    def option_map():
        return {'' : ShowCommand.show_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name} <image-name> <command (binary)>\n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        if len(arg_list) != 2:
            return (), (), False, 'Invalid number of arguments'

        # Does the plot with the given name exist
        if (app.get_tool(arg_list[0]) == None):
            return (), (), False, '\nNo image with name: %s' % arg_list[0]

        if not '*' in arg_list[1]:
            return (), (), False, '\n%s does not return binary data' % arg_list[1]

        return tuple(arg_list), (), True, ''

    @staticmethod
    def show_cmd(plot_name, cmd_name, device=None, app=None):
        answ, image = device.ackcommand(cmd_name)
        app.get_tool(plot_name).show_event(image)


class ImageCommand(Command):
    @staticmethod
    def option_map():
        return {'add' : ImageCommand.add_cmd,
                'rm': ImageCommand.rm_cmd,
                'ls': ImageCommand.ls_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = 'usage:  .{cmd_name} add <name> <x-pos> <y-pos> \n'
        usage_msg += 'usage:  .{cmd_name} rm <name> \n'
        usage_msg += 'usage:  .{cmd_name} ls \n'

        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        valid = True

        if option == 'add':
            if len(arg_list) != 3:
                return (), (), False, 'Invalid number of arguments'

            if len(arg_list) == 3:
                msg = "\n'%s' is an invalid position" % str(arg_list[1])

            if not isinstance(arg_list[1], int) or \
               not isinstance(arg_list[2], int):
                return (), (), False, msg

        elif option in ['rm', 'enable']:
            if (not arg_list[0]):
                return (), (), False, ''

            # Does the plot with the given name exist
            if (app.get_tool(arg_list[0]) == None):
                return (), (), False, '\nNo plot with name: %s' % arg_list[0]

        elif option == 'ls':
            pass

        elif option == 'config':
            # Does the plot with the given name exist
            if (app.get_tool(arg_list[0]) == None):
                return (), (), False, '\nNo plot with name: %s' % arg_list[0]

            if len(arg_list) > 3:
                return (), (), False

        return tuple(arg_list), (), valid, ''

    @staticmethod
    def rm_cmd(name, device=None, app=None):
        app.remove_tool(name)

    @staticmethod
    def add_cmd(name, xpos, ypos, device=None, app=None):
        ui_tool_spec = (PixelDisplayTool, (xpos, ypos), name)
        app.add_tool(ui_tool_spec, device)

    @staticmethod
    def ls_cmd(device=None, app=None):
        tool_list = ImageCommand.get_plot_tool_list(PixelDisplayTool, device, app)
        tool_str_list = []

        for name, loc in tool_list:
            tool_str_list.append('%s %s' % (name, loc))

        dprint('\n' + columnize(tool_str_list, colsep = '  |  '))

    @staticmethod
    def get_plot_tool_list(tool_cls=PlotTool, device=None, app=None):
        name_list = []

        for name, tool in app.ui_tools.iteritems():
            if isinstance(tool[0], tool_cls):
                name_list.append((name, tool[1]))

        return name_list


class PlotCommand(Command):
    @staticmethod
    def option_map():
        return {'add' : PlotCommand.add_cmd,
                'rm': PlotCommand.rm_cmd,
                'ls': PlotCommand.ls_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = 'usage:  .{cmd_name} add <name> <x-pos> <y-pos> \n'
        usage_msg += 'usage:  .{cmd_name} rm <name> \n'
        usage_msg += 'usage:  .{cmd_name} ls \n'

        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        return ImageCommand.validate_args(line, arg_list, options, device, app)

    @staticmethod
    def rm_cmd(name, device=None, app=None):
        app.remove_tool(name)

    @staticmethod
    def add_cmd(name, xpos, ypos, device=None, app=None):
        ui_tool_spec = (PlotTool, (xpos, ypos), name)
        app.add_tool(ui_tool_spec, device)

    @staticmethod
    def ls_cmd(device=None, app=None):
        tool_list = ImageCommand.get_plot_tool_list(PlotTool, device, app)
        tool_str_list = []

        for name, loc in tool_list:
            tool_str_list.append('%s %s' % (name, loc))

        dprint('\n' + columnize(tool_str_list, colsep = '  |  '))


class NScanCommand(Command):
    @staticmethod
    def option_map():
        return {'' : NScanCommand.nscan_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name} param_a [<s_a1 e_a1> | [v_a1 ... v_ai]]"
        usage_msg += " ... \n              "
        usage_msg += "param_k [<s_k1 e_k1> | [v_k1 ... v_ki]] nint\n\n"
        usage_msg += "Any arguments passed after the value ranges or value "+\
                     "lists are \npassed to the acquisition method\n\n"
        usage_msg += "The param argument can either be a parameter name " +\
                     "of the \ncurrent instrument, or the full parameter " +\
                     "address;\n\n    <protocol>://<host>:<port>/<name> \n" +\
                     "    e.g.: deep://comex019/dacovl\n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        args, num_params, valid = [], 0, True
        param_list = arg_list

        if option == '':
            # Basic length check, the shortest command is
            # .nscan 1 <param name> <list of values>
            if len(arg_list) < 2:
                msg = 'Not enough arguments, command takes at-least 3 arguments'
                return (), (), False, msg

            # The first argument should be either;
            # A Number in case the number of parameters are either 1 or
            # more than 2 or A str (parameter name) in case of exactly two
            # parameters
            if isinstance(arg_list[0], str):
                num_params = 2
            elif isinstance(arg_list[0], int):
                num_params = arg_list[0]
                param_list = arg_list[1:]
            else:
                msg = 'First parameter have to be a number indicating the \n'
                msg += 'number of parameters. Or in the special case of two \n'
                msg += 'parameters it has to be a parameter name, '
                msg += 'argumetn was %s'
                msg = msg % arg_list[0]
                return (), (), False, msg

            n = 0
            offset = 0
            while n < num_params:
                arg = param_list[offset]

                if str(arg).isalpha():
                    args.append([arg.upper()])

                if isinstance(param_list[offset+1], list):
                    args[-1].append(param_list[offset+1])
                    offset += 2
                else:
                    range_args = param_list[offset+1: offset+3]
                    is_number = map(Command().is_number, range_args)

                    if False in is_number:
                        msg = 'Invalid argument: argument following '
                        msg += 'a parameter must be either a list of values '
                        msg += 'or <start> <end> of the interval to scan'
                        return (), (), False, msg
                    else:
                        args[-1].append(tuple(range_args))

                    offset += 3
                n+=1

            # If there are lists among the parameter values, make sure
            # that they have the same length
            list_len = None
            for param_group in args:
                if isinstance(param_group[1], list):
                    if not list_len:
                        list_len = len(param_group[1])
                    else:
                        if list_len != len(param_group[1]):
                            msg = 'The lists passed does not have the same '+\
                                  'length'
                            return (), (), False, msg

            # If lists are used set the number of intervals to the length of
            # the list - 1. If use the nint argument
            if not list_len:
                list_len = param_list[offset]
            else:
                offset -= 1

            for param_group in args:
                if isinstance(param_group[1], tuple):
                    param_group[1] = param_group[1] + (list_len,)

            args = [args, line]

        return tuple(args), tuple(param_list[offset+1:]), valid, ''

    @staticmethod
    def nscan_cmd(param_list, line, device=None, app=None):
        actlist = []
        valueiter_list = []

        for param in param_list:
            actuator = app.scan_engine.get_actuator(param[0])
            value_iter, start, end, step = value_iterator(param[1])
            valid, msg = actuator.check_range(start, end)

            if not valid:
                dprint('\n%s\n' % msg)
                return

            actlist.append(actuator)
            valueiter_list.append(value_iter)

        run_with_cleanup(nscan, app.scan_engine, actlist, valueiter_list)


class DNScanCommand(Command):
    @staticmethod
    def option_map():
        return {'' : DNScanCommand.dnscan_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name} param_a [<s_a1 e_a1> | [v_a1 ... v_ai]]"
        usage_msg += " ... \n              "
        usage_msg += "param_k [<s_k1 e_k1> | [v_k1 ... v_ki]] nint\n\n"
        usage_msg += "Any arguments passed after the value ranges or value "+\
                     "lists are \npassed to the acquisition method\n\n"
        usage_msg += "The param argument can either be a parameter name " +\
                     "of the \ncurrent instrument, or the full parameter " +\
                     "address;\n\n    <protocol>://<host>:<port>/<name> \n" +\
                     "    e.g.: deep://comex019/dacovl\n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        return NScanCommand.validate_args(line, arg_list, option, options, device=device, app=app)

    @staticmethod
    def dnscan_cmd(param_list, line, device=None, app=None):
        actlist = []
        valueiter_list = []

        for param in param_list:
            actuator = app.scan_engine.get_actuator(param[0])
            value_iter, start, end, step = value_iterator(param[1])
            valid, msg = actuator.check_range(start, end)

            if not valid:
                dprint('\n%s\n' % msg)
                return

            actlist.append(actuator)
            valueiter_list.append(value_iter)

        run_with_cleanup(dnscan, app.scan_engine, actlist, valueiter_list)


class MScanCommand(Command):
    @staticmethod
    def option_map():
        return {'' : MScanCommand.mscan_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name} param_a [<s_a1 e_a1> | [v_a1 ... v_ai]]"
        usage_msg += " ... \n              "
        usage_msg += "param_k [<s_k1 e_k1> | [v_k1 ... v_ki]] nint\n\n"
        usage_msg += "Any arguments passed after the value ranges or value "+\
                     "lists are \npassed to the acquisition method\n\n"
        usage_msg += "The param argument can either be a parameter name " +\
                     "of the \ncurrent instrument, or the full parameter " +\
                     "address;\n\n    <protocol>://<host>:<port>/<name> \n" +\
                     "    e.g.: deep://comex019/dacovl\n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        args, num_params, valid = [], 0, True
        param_list = arg_list

        if option == '':
            # Basic length check, the shortest command is
            # .nscan 1 <param name> <list of values>
            if len(arg_list) < 2:
                msg = 'Not enough arguments, command takes at-least 3 arguments'
                return (), (), False, msg

            # The first argument should be either;
            # A Number in case the number of parameters are either 1 or
            # more than 2 or A str (parameter name) in case of exactly two
            # parameters
            if isinstance(arg_list[0], str):
                num_params = 2
            elif isinstance(arg_list[0], int):
                num_params = arg_list[0]
                param_list = arg_list[1:]
            else:
                msg = 'First parameter have to be a number indicating the \n'
                msg += 'number of parameters. Or in the special case of two \n'
                msg += 'parameters it has to be a parameter name, '
                msg += 'argument was %s'
                msg = msg % arg_list[0]
                return (), (), False, msg

            n = 0
            offset = 0
            while n < num_params:
                arg = param_list[offset]

                if str(arg).isalpha():
                    args.append([arg.upper()])

                if isinstance(param_list[offset+1], list):
                    args[-1].append(param_list[offset+1])
                    offset += 2
                else:
                    range_args = param_list[offset+1: offset+4]
                    is_number = map(Command().is_number, range_args)

                    if False in is_number:
                        msg = 'Invalid argument: argument following '
                        msg += 'a parameter must be either a list of values '
                        msg += 'or <start> <end> of the interval to scan'
                        return (), (), False, msg
                    else:
                        args[-1].append(tuple(range_args))

                    offset += 4
                n+=1

            args = [args, line]

        return tuple(args), tuple(param_list[offset:]), valid, ''

    @staticmethod
    def mscan_cmd(param_list, line, device=None, app=None):
        actlist = []
        valueiter_list = []

        for param in param_list:
            actuator = app.scan_engine.get_actuator(param[0])
            value_iter, start, end, step = value_iterator(param[1])
            valid, msg = actuator.check_range(start, end)

            if not valid:
                dprint('\n%s\n' % msg)
                return

            actlist.append(actuator)
            valueiter_list.append(value_iter)

        run_with_cleanup(mscan, app.scan_engine, actlist, valueiter_list)


class DMScanCommand(Command):
    @staticmethod
    def option_map():
        return {'' : DMScanCommand.dmscan_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name} param_a [<s_a1 e_a1> | [v_a1 ... v_ai]]"
        usage_msg += " ... \n              "
        usage_msg += "param_k [<s_k1 e_k1> | [v_k1 ... v_ki]] nint\n\n"
        usage_msg += "Any arguments passed after the value ranges or value "+\
                     "lists are \npassed to the acquisition method\n\n"
        usage_msg += "The param argument can either be a parameter name " +\
                     "of the \ncurrent instrument, or the full parameter " +\
                     "address;\n\n    <protocol>://<host>:<port>/<name> \n" +\
                     "    e.g.: deep://comex019/dacovl\n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        return MScanCommand.validate_args(line, arg_list, option, options, device=device, app=app)

    @staticmethod
    def dmscan_cmd(param_list, line, device=None, app=None):
        actlist = []
        valueiter_list = []

        for param in param_list:
            actuator = app.scan_engine.get_actuator(param[0])
            value_iter, start, end, step = value_iterator(param[1])
            valid, msg = actuator.check_range(start, end)

            if not valid:
                dprint('\n%s\n' % msg)
                return

            actlist.append(actuator)
            valueiter_list.append(value_iter)

        run_with_cleanup(dmscan, app.scan_engine, actlist, valueiter_list)

class MoveCommand(Command):
    @staticmethod
    def option_map():
        return {'' :MoveCommand.mv_cmd}

    def usage_msg(self, options, device=None, app=None):
        usage_msg = "usage: .{cmd_name} param value\n\n"
        usage_msg += "The param argument can either be a parameter name " +\
                     "of the \ncurrent instrument, or the full parameter " +\
                     "address;\n\n    <protocol>://<host>:<port>/<name> \n" +\
                     "    e.g.: deep://comex019/dacovl\n"
        return usage_msg

    @staticmethod
    def validate_args(line, arg_list, option, options, device=None, app=None):
        args, ignored_args, valid = (), (), True

        if option == '':
            if len(arg_list) < 2:
                return (), (), False, 'Not enough arguments'

            arg_list[0] = arg_list[0].upper()

        return tuple(arg_list), ignored_args, valid, ''

    @staticmethod
    def mv_cmd(param, value, app=None, device=None):
        actlst = [app.scan_engine.get_actuator(param)]

        if actlst[0]:
            valid, msg = actlst[0].check_range(value, value)
        else:
            return

        if not valid:
            dprint('\n%s\n' % msg)
            return
        
        dprint('\nMoving %s to %G ... ' %(param, value), end='')
        value = actlst[0].set(value)
        dprint('Final value is: %G\n' % value)
