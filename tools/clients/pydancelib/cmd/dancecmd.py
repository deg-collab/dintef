# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function

import os

from .deepcmd import DeepCmd
from ..device import DConfig
from .utils import getvalue, yesno


class DanceCmd(DeepCmd):
    def __init__(self, dev, *args):
        DeepCmd.__init__(self, dev, *args)
        self.lastconfname = None
        self.defconfname = "mydanceconf"
        self._app = None

    def set_app(self, app):
        self._app = app

    def do_saveconf(self, confname):
        """
        Retrieves configuration information from the DAnCE device
        and saves it in a file (or directory)
        """
        devconf = self.device.dconfig()
        def_ext = devconf.extension()

        self._print("\nCurrent configuration: \n")
        self._show_configuration(devconf)
        self._print("\nCurrent directory: %s\n" % os.getcwd())

        if not len(confname):
            defname = self.lastconfname

            if not defname:
                defname = self.defconfname

            confname = getvalue("Enter configuration file ", default=defname,
                                stdin = self.stdin, stdout = self.stdout)

        if not confname.endswith(('.dcfz', '.dcfd')):
           newname = confname + def_ext

           if '.' in confname:
               if not yesno(False, "Use filename '{0}' ".format(newname),
                            stdin = self.stdin, stdout = self.stdout):
                   return

           confname = newname

        if os.path.isfile(confname):
            self._print("File '{0}' exists. ".format(confname))

            if not yesno(False, "Overwrite ", stdin = self.stdin,
                         stdout = self.stdout):
                return

        devconf.write(confname)

        self._print("Configuration saved in %s" % confname)
        self.lastconfname = confname

    def do_loadconf(self, confname):
        """
        Loads configuration information from a file and uploads into
        the DAnCE device
        """
        nerrors = 0
        self._print("\nCurrent directory:", os.getcwd())

        if not len(confname):
            defname = self.lastconfname

            if not defname:
                defname = self.defconfname

            confname = getvalue("Enter configuration file ", default = defname)

        try:
            devconf = DConfig(confname)
        except (ValueError, IOError) as ex:
            self._print("Configuration retrieval failed. {0}".format(ex))
            return

        self._show_configuration(devconf)

        if not yesno(False, "Upload configuration into device ",
                     stdin = self.stdin, stdout = self.stdout):
            return

        for cmd, sname in devconf.commands():
            try:
                if not sname:
                    self._print("{0:<35}".format(cmd))
                    self.device.ackcommand(cmd)
                else:
                    self._print("{0:<35}".format(cmd + '<' + sname + '>'))
                    bindata = devconf.binary(sname)
                    self.device.ackcommand(cmd, bindata)

                self._print("OK")

            except (IOError, SyntaxError) as ex:
                self._print(ex)
                nerrors += 1

        if nerrors:
            msg = "Configuration uploaded with {0} errors".format(nerrors)
            self._print(msg)
        else:
            self._print("Configuration uploaded successfully")

        self.lastconfname = confname

    def _show_configuration(self, devconf):
        for line in devconf.cfglines():
            self._print("   %s" % line)

    def get_config(self):
        return self.device.get_config()

    def set_config(self, sourcepath = 'cfg.zip'):
        self.device.send_config(sourcepath)

    def do_reboot(self, arg):
        """
        Reboots the DAnCE instrument and exits
        """
        cmd = 'REBOOT'

        if not cmd in self.device.commands:
            self._print('device does not implement the command %s ' % cmd)
        else:
            try:
                self.device.ackcommand(cmd, arg if arg else 1)
                self._print("\nRebooting ")
                return self.do_quit(None)
            except:
                self._print("\nRebooting failed")

    def get_active_plot(self):
        return None
