# -*- coding: utf-8 -*-
"""Utilities to test deep based DAnCE devices"""

from __future__ import absolute_import, print_function

from .dancecmd import DanceCmd

__version__ = "0.0.1"
__author__ = ""
__author_email__ = ""
__description__ = "Tools for testing deep based DAnCE devices"
__url__ = "https://deg-svn.esrf.fr/svn/pydancelib"

