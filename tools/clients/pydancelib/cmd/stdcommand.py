# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function

from ..scan.engine import run_with_cleanup
from ..scan.procedure import value_iterator
from ..scan.procedure import scan as scanp
from ..io.stream import dprint
from ..ui.utils.text import columnize

from ..app import APP


def use(hostname=''):
    if hostname in [' ', '-', '']:
        hostname = None

    if hostname:
        APP().set_current_device(hostname)
        dprint("\nConnected to %s !\n" % hostname)
        APP()._ci.prompt(hostname + ' ')
    elif APP().current_device():
        APP().set_current_device(hostname)
        dprint("\nDisconnected !\n")
        APP()._ci.prompt('INCON ')
    else:
        dprint("\nNot connected to any device !, issue use <host> to connect\n")


def _usage_use(cmd):
    msg =\
    """
    The use command allows you to work with a single device as if you
    were connected to it. It allows you to omit the host part of a
    parameter or command endpoint.

    usage: use <hostname>
    usage: use -
    """
    dprint(msg)


def devices():
    devlist = columnize(APP().devices(), colsep = ' | ')
    dprint("\nConnected devices: \n%s" % devlist)


def call(netloc, arg_str=''):
    func, cmd = APP().resolve_remote_cmd_call(netloc)

    if func:
        return(func(cmd + ' ' + arg_str))
    else:
        return None


def sampler(option, *args):
    options = ['add', 'rm', 'ls']

    if option in options:
        if option == 'add':
            _sampler_add(*args)
        elif option == 'rm':
            _sampler_rm(*args)
        elif option == 'ls':
            _sampler_list()
    else:
        raise TypeError('sampler have no option %s' % option)


def _sampler_usage():
    pass


def _sampler_add(cmd_frame, endpoint, name=None):
    APP().scan_engine.add_sample_method(endpoint)


def _sampler_rm(cmd_frame, endpoint):
    APP().scan_engine.remove_sample_method(endpoint)
    _sampler_list(cmd_frame)


def _sampler_list():
    acq_methods = APP().session_mgr.get_value('SCAN.ACQ_METHODS')
    dprint("\nSampling using:\n%s" % columnize(acq_methods, colsep = ' | '))


def scan(param, start, end, nint):
    actlst = [APP().scan_engine.get_actuator(param)]
    value_iter, start, end, step = value_iterator((start, end, nint))
    valid, msg = actlst[0].check_range(start, end)
    run_with_cleanup(scanp, APP().scan_engine, actlst, [value_iter])

