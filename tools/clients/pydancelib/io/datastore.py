# -*- coding: utf-8 -*-
import errno
#import md5
import os
#import struct
import threading
import time
import Queue as queue

import h5py
import numpy


class Measurement(object):
    def __init__(self, ds = None):
        self._ds = ds
        # Contains the data for each data set the key is the name of
        # the data set.
        self._data_set_dict = {}

        # Meta data
        self._meta_dict = {'date': [time.strftime('%y-%m-%d', time.localtime())],
                           'time': [time.strftime('%H:%M:%S', time.localtime())],
                           'num_datasets': [0]}

        self._add_sample_set_cb = None

    def add_sample_set(self, sample_set_dict):
        for name, value in sample_set_dict.iteritems():
            if name not in self._data_set_dict:
                self._data_set_dict[name] = []

            self._data_set_dict[name].append(value)

        self._meta_dict['num_datasets'][0] += 1
        self._ds._sample_set_added_cb(sample_set_dict)

    def iteritems(self):
        return self._data_set_dict.iteritems()

    def size(self):
        return len(self._data_set_dict)


class DataStore(object):
    def __init__(self, fpath = None, tempfile = True):
        self._file_handler = HDFFileHandler(self, fpath, tempfile)
        self._ms_dict = {}

    def __getitem__(self, name):
        return self._ms_dict[name]

    def create_measurement(self, name):
        self._ms_dict[name] = Measurement(ds = self)
        self._file_handler.add_write_index(name, 0)
        return self._ms_dict[name]

    def _sample_set_added_cb(self, sample_set_dict):
        self._file_handler.write_tmpfile()

    def write(self, name=None):
        self._file_handler.write(name)

    def filename(self):
        return self._file_handler.filename()

    def tmpfilename(self):
        return self._file_handler.tmpfilename()

    def set_file_path(self, fpath):
        self._file_handler.set_file_path(fpath)

    def num_data_sets(self):
        return self._file_handler.num_data_sets()


class QueuedMeasurement(Measurement):
    def __init__(self, ds):
        super(QueuedMeasurement, self).__init__(ds)
        self._queue = queue.Queue(100)

    def add_sample_set(self, sample_set_dict):
        self._ds._data_avail_cond.acquire()
        self._queue.put(sample_set_dict)
        self._ds._data_avail_cond.notify()
        self._ds._data_avail_cond.release()

    def process(self):
        try:
            sample_set_dict = self._queue.get(False)
        except queue.Empty:
            return

        if sample_set_dict:
            super(QueuedMeasurement, self).add_sample_set(sample_set_dict)


class ThreadedDataStore(DataStore, threading.Thread):
    def __init__(self, fpath = None, tempfile = True):
        DataStore.__init__(self, fpath, tempfile)
        threading.Thread.__init__(self)

        self._running = False
        self._data_avail_cond = threading.Condition()
        self.daemon = True
        self.start()

    def _stop(self):
        self._running = False
        self._data_avail_cond.acquire()
        self._data_avail_cond.notify()
        self._data_avail_cond.release()
        self.join()

    def create_measurement(self, name):
        self._ms_dict[name] = QueuedMeasurement(ds = self)
        self._file_handler.add_write_index(name, 0)
        return self._ms_dict[name]

    def run(self):
        self._running = True

        while self._running:
            self._data_avail_cond.acquire()
            self._data_avail_cond.wait()

            for name, queued_ms in self._ms_dict.iteritems():
                queued_ms.process()

            self._data_avail_cond.release()

    def write(self, name=None):
        super(ThreadedDataStore, self).write(name)

    def stop(self):
        self._stop()


class HDFFileHandler(object):
    def __init__(self, datastore, fpath=None, tempfile=True):
        self._ds = datastore
        self._ms_write_idx = {}
        self._num_ds = 0
        self.set_file_path(fpath, tempfile)

    def tmpfilename(self):
        return os.path.basename(self._tmpfpath)

    def filename(self):
        return os.path.basename(self._fpath)

    def set_file_path(self, fpath=None, tempfile=True):
        if not fpath:
            datestr = time.strftime("session-%y%m%d-%H%M%S", time.localtime())
            fpath = './' + datestr

        fpath = os.path.splitext(fpath)[0] + '.hdf'

        if fpath in self._ms_write_idx:
            return

        self._fpath = fpath
        self._tmpfpath = os.path.splitext(fpath)[0] + '.h5'

        self._use_tempfile = tempfile
        self._tmpfile = None

        self._ms_write_idx = {self._tmpfpath: {}, self._fpath: {}}

        try:
            os.makedirs(os.path.dirname(self._fpath))
        except OSError, e:
            if e.errno != errno.EEXIST:
                raise
            
        if os.path.isfile(fpath):
            with h5py.File(fpath, 'r+') as f:
                self._num_ds = len(f)

    def add_write_index(self, name, idx):
        self._ms_write_idx[self._fpath][name] = idx
        self._ms_write_idx[self._tmpfpath][name] = idx

    def write(self, name=None):
        self._write(self._fpath, name)

        if os.path.isfile(self._tmpfpath):
            self._tmpfile.close()
            os.remove(self._tmpfpath)

    def write_tmpfile(self):
        if self._use_tempfile:
            self._write(self._tmpfpath, keep_open = True)

    def num_data_sets(self):
        return self._num_ds

    def _write(self, fname, name=None, keep_open=False):
        if keep_open:
            self._write_keep_open(fname, name)
        else:
            self._write_close(fname, name)

    def _write_close(self, fname, name=None):
        if not os.path.isfile(fname):
            f = h5py.File(fname, 'w-')
            f.close()

        with h5py.File(fname, 'r+') as f:
            self._write_hdf(f, name)
            f.flush()

    def _write_keep_open(self, fname, name=None):
        if not os.path.isfile(fname):
            f = h5py.File(fname, 'w-')
            f.close()

        if not self._tmpfile:
            self._tmpfile = h5py.File(fname, 'r+')

        self._write_hdf(self._tmpfile, name)
        self._tmpfile.flush()

    def _write_hdf(self, fo = None, name=None):
        if name:
            self._write_ms(fo, name, self._ds._ms_dict[name])
        else:
            for ms_name, ms in self._ds._ms_dict.iteritems():
                self._write_ms(fo, ms_name, ms)


    def _write_ms(self, fo, ms_name, ms):
        nsamples_w = 0

        for sname, samples in ms.iteritems():
            ds_path = ms_name + '/' + sname
            idx = self._ms_write_idx[fo.filename][ms_name]
            nsamples_w = self._ds_append(fo, ds_path, samples[idx:])

        for key, value in ms._meta_dict.iteritems():
            meta_path = ms_name + '/__meta__/' + key
            self._ds_append(fo, meta_path, value)

        self._ms_write_idx[fo.filename][ms_name] += nsamples_w

    def _ds_append(self, f, path, data):
        item_list = []        

        if data:
            if path in f:
                dset = f[path]
                item_list = data
                dset_len = dset.len()
                dset.resize(dset_len + len(item_list), 0)
                dset[dset_len:] = item_list
            else:
                rank = self._get_rank(data)

                f.create_dataset(path, data=data, chunks = True,
                                 maxshape = rank*(None,))

                item_list = data

        if f.filename == self._fpath:
            self._num_ds = len(f)

        return len(item_list)

    def _get_rank(self, data):
        if isinstance(data[0], tuple) or isinstance(data[0], list):
            return 2
        elif isinstance(data[0], numpy.ndarray):
            return data[0].ndim + 1
        else:
            return 1
