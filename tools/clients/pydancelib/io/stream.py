# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import sys
import threading

# For python 2 and 3 compatibility
try:
    input = raw_input
except NameError:
    pass


__THREAD_IO__ = {}

class Stream(object):
    def __init__(self, write_cb = None, flush_cb = None):
        super(Stream, self).__init__()
        self._data_cond = threading.Condition()
        self._buffer = ''
        self.write_cb = write_cb
        self.flush_cb = flush_cb

    def _reset_buffer(self):
        data = self._buffer
        self._buffer = ''
        return data

    def _flush(self):
        with self._data_cond:
            data = self._reset_buffer()
            self._data_cond.notifyAll()

        return data

    def readline(self, timeout = None):
        data = ''

        try:
            with self._data_cond:
                first_linesep = self._buffer.find(os.linesep)

                # Is there already some lines in the buffer, write might have
                # been called before we read !
                while first_linesep == -1:
                    notfied = self._data_cond.wait(timeout)
                    first_linesep = self._buffer.find(os.linesep)

                    # We had a timeout, break !
                    if not notfied and timeout:
                        break

                # Check if there really is something in the buffer after waiting
                # for data_cond. There might have been a timeout, and there is
                # still no data available
                if first_linesep > -1:
                    data = self._buffer[0:first_linesep + 1]

                    if len(self._buffer) > len(data):
                        self._buffer = self._buffer[first_linesep + 1:]
                    else:
                        self._buffer = ''

        # Tricky RuntimeError !, wait releases the lock and waits for notify
        # and then acquire the lock again !. There might be an exception, i.e
        # KeyboardInterupt which interrupts the wait. The cleanup of the with
        # statement then tries to release the lock which is not acquired,
        # causing a RuntimeError. puh ! If its the case just try again !
        except RuntimeError:
            self._data_cond.notifyAll()

        # except (RuntimeError, KeyboardInterrupt) as ex:
        #     self._data_cond.notifyAll()
        #     data = self.readline(timeout)

        return data

    def write(self, data):    
        with self._data_cond:
            self._buffer += data
            self._data_cond.notify()

            if callable(self.write_cb):
                self.write_cb(data)
                
    def read(self, timeout = None):
        data = ''

        with self._data_cond:
            try:
                while self._buffer == '':
                    notified = self._data_cond.wait(timeout)

                    # We had a timeout, break !
                    if not notified and timeout:
                        break

                data = self._reset_buffer()
            except (RuntimeError, KeyboardInterrupt) as ex:
                print(type(ex))
                self._data_cond.notifyAll()
                data = self.readline(timeout)
            except RuntimeError:
                data = self.read(timeout)

        return data

    def flush(self):
        data = self._flush()

        if callable(self.flush_cb):
            self.flush_cb(data)
        
        return data

def _register_thread_io(_in = sys.stdin, _out = sys.stdout, thread = None):
    if not thread:
        _id = threading.current_thread().ident
    else:
        _id = thread.ident
    
    __THREAD_IO__[_id] = (_in, _out)

  
def get_thread_io(thread = None):
    if not thread:
        _id = threading.current_thread().ident
    else:
        _id = thread.ident

    if not _id:
        raise AssertionError('No Thread ID')

    if _id in __THREAD_IO__:
        _in, _out =  __THREAD_IO__[_id]
    else:
        _in, _out = sys.stdin, sys.stdout

    return (_in, _out)


def _thread_print(*args, **kwargs):
    thread = kwargs.get('thread', None)

    _in, _out = get_thread_io(thread)
    kwargs['file'] = _out

    print(*args, **kwargs)


def _thread_readline(prompt = '', timeout = None, thread = None):
    _in, _out = get_thread_io(thread)

    if _in == sys.stdin:
        return input(prompt)
    else:
        if prompt != '':
            _thread_print(prompt, end='')

        return _in.readline(timeout)


def dprint(*args, **kwargs):
    _thread_print(*args, **kwargs)


def dinput(*args, **kwargs):
    return _thread_readline(*args, **kwargs)

    
def register_io(*args, **kwargs):
    _register_thread_io(*args, **kwargs)


def getvalue(query, default=None):
    stdin, stdout = get_thread_io()
    
    if default != None and default != "":
        prompt = query + " [%s]: " % default
    else:
        prompt = query + ': '
        
    stdout.write(prompt)
    stdout.flush()
    answer = stdin.readline().strip('\n')
    
    if answer == "":
        answer = default

    return answer
