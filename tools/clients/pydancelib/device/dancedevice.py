# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function

from itertools import cycle
from libdeep import DeepDevice
from .dconfig import DConfig



class BusyError(EnvironmentError):
   pass

ERR_BUSY = BusyError('Instrument is busy')
ERR_BAD_VALUE = ValueError('Bad input value')
ERR_BAD_MODE = ValueError('Bad operation mode')
ERR_BAD_RANGE = ValueError('Out of range')


class DanceDevice(DeepDevice):
    def __init__(self, *args, **kwargs):
        super(DanceDevice, self).__init__(*args, **kwargs)
        """  TODO: Fix this properly: silent commands, ...
        try:
            self._params = self.command("?PARINFO").split()
        except:
            self._params = []
        """
        self._params = []
        if self.isvalidcommand("?PARINFO"):
            self._params = self.command("?PARINFO").split()               
        self._actuator_dict = {}
        self._sampler_dict = {}

        #self.add_actuators(self._params)
        #self.add_sampler('acq')

    """
    def add_actuators(self, param_list):
        from ..scan.engine import Actuator
        
        for param in param_list:
            self._actuator_dict[param] = Actuator(self, param)

    def get_actuator(self, name):
        return self._actuator_dict[name]

    def is_actuator(self, name):
        return name in self._actuator_dict.keys()

    def add_sampler(self, fun_name):
        from ..scan.engine import Sampler
        
        self._sampler_dict[fun_name] = Sampler(fun_name, self, fun_name)

    def is_sampler(self, fun_name):
        return fun_name in self._sampler_dict.keys()

    def get_sampler(self, fun_name):
        return self._sampler_dict[fun_name]
    """
    
    @staticmethod
    def _string_extract(type_fn, str_value):
        if type_fn in (int, float, str):
            if not str_value:
               if type_fn == str:
                   return None, None
               else:
                   msg = "empty string cannot be converted to " + repr(type_fn)
                   raise ValueError(msg)

            elif type_fn == str and str_value[0] in "\"\'":
               idx = str_value[1:].find(str_value[0])
               if idx >= 0:
                   return str_value[1:1+idx], str_value[2+idx:].strip()
               else:
                   return str_value[1:].strip(), None

            else:
               strsplit = str_value.split(None, 1)
               str_token = strsplit[0]
               str_remain = strsplit[1] if len(strsplit) == 2 else None

               if type_fn == int and str_token[0:2].lower() == '0x':
                   # convert C-type hexadecimal values
                   return int(str_token, base=16), str_remain
               else:
                   return type_fn(str_token), str_remain

        elif type_fn:
            # used define parsing function
            return type_fn(str_value)

        else:
            # type_fn == None
            try:
                return DanceDevice._string_extract(int, str_value)
            except ValueError:
                try:
                    return DanceDevice._string_extract(float, str_value)
                except ValueError:
                    return DanceDevice._string_extract(str, str_value)

    def _get_typed_list(self, _type_fn, devquery, *args):
        answ = self.command(devquery, *args)
        if not answ:
            raise ValueError('Not a query: "' + devquery + '"')
        try:
            vlist = list(map(_type_fn, answ.split()))
        except ValueError as e:
            raise ValueError(str(e) + ' from device answer: "' + answ + '"')

        return vlist

    def _type_convert(self, type_fn, str_value):
        if type_fn == int and str_value[0:2].lower() == '0x':
            # convert C-type hexadecimal values
            return int(str_value, base=16)
        elif type_fn:
            return type_fn(str_value)
        else:   # type_fn == None
            try:
                return self._type_convert(int, str_value)
            except ValueError:
                try:
                    return float(str_value)
                except ValueError:
                    return str_value

    def dconfig(self):
        """
        Fetches the DAnCE configuration from the device by issuing a ?DCONFIG
        query and creates a associated DConfig object.

        :returns: a DConfig instance containing the current device
                  configuration.
        :raises DeviceError: If the ?DCONFIG query is not properly implemented
                             in the DAnCE device.
        :raises ValueError: If the answer from the DAnCE device is badly
                            formatted.
        :raises IOError: If there is any communication error.
        """
        return DConfig(self)

    def upload_dconfig(self, dconfig):
        """
        Uploads the configuration contained in a DConfig object into the
        DAnCE device.

        :param  dconfig: The DConfig instance containing the DAnCE configuration
        :raises IOError: If there is any communication error.
        :raises DeviceError: If the DAnCE device does not accept the
                             configuration contained in dconfig.
        """
        dconfig.write(self)

    def get_answer(self, type_fns, devquery, *args):
        """
        Returns the answer from the device to a given query as a tuple of
        values, each element of the tuple having been converted to the type
        determined by the argument type_fns.

        type_fns is either a conversion function or a sequence of conversion
        functions that is used to convert the string tokens in the device
        answer to the elements of the returned tuple. The most frequent
        conversion functions are the built int types int, float and str, but
        others are possible. The built-in int conversion is extended to accept
        C-format hexadecimal strings.

        If the device answer contains more tokens than the number of conversion
        function in type_fns, the sequence of conversion functions is
        repeated cyclically.

        :param  type_fns: the conversion function or the list or tuple of
                          conversion functions that is used by the method.
        :param  devquery: The query string to be sent to the device followed by
                          any additional arguments
        :raises DeviceError: If the query is not properly executed in the
                             device.
        :raises IOError: If there is any communication error.
        :raises ValueError: If devquery is not a query or if the answer from
                            the device is not compatible with the conversion
                            types.
        """
        answ = self.command(devquery, *args)

        if not answ:
            raise ValueError('Not a query: "' + devquery + '"')
        try:
            answ_items_list = answ.split()

            if isinstance(type_fns, (list, tuple)):
                tfn_list = list(type_fns)
            else:
                tfn_list = [type_fns]

            if len(tfn_list) > len(answ_items_list):
                msg = 'Too many answer types for query: "' + devquery + '"'
                raise ValueError(msg)
            if len(tfn_list) < len(answ_items_list):
                tfn_list *= len(answ_items_list) // len(tfn_list)
                tfn_list += tfn_list[0:len(answ_items_list) - len(tfn_list)]

            vtuple = tuple(map(self._type_convert, tfn_list, answ_items_list))

        except ValueError as ex:
            raise ValueError(str(ex) + ' from device answer: "' + answ + '"')

        return vtuple

    def parse_answer(self, convfns, devquery, *args):
        """
        Returns the ASCII answer from the device to a given query converted
        into a tuple of values, each element of the tuple having been parsed
        accordingly to the argument convfns. If the query also returns a binary
        block, the method returns in addition the DeepArray containing the
        binary data. In general, convfns is either a function or a sequence of
        functions that is used to extract from the device ASCII answer the
        elements included in the returned tuple. If needed, the sequence of
        conversion or parsing functions in convfns is repeated cyclically until
        all the elements in the device answer are extracted. The most frequently
        used are the conversion functions for the built-in types int, float
        and str, but the method accepts also any user provided parsing function
        as well as certain objects that select special conversions as described
        below. In general the built-in conversions parse and extract a string
        token separated by whitespaces in the answer string and converts them
        to the corresponding type. However, for string conversion, the method
        also manages single or double quoted strings in the device answer that
        are treated and returned as full strings regardless of whether or not
        they include whitespaces. The built-in integer conversion is also
        extended to accept C-format hexadecimal strings.

        User provided parsing functions must parse an input string and extract
        a single element at each call. The functions must return a two-value
        tuple consisting of the extracted element, that can be an object of any
        arbitrary type, and the remaining string still to be parsed. If the
        extracted element returned by the user function is None, the element
        is ignored and not included in the final sequence. This feature may be
        used to manipulate the ASCII string at a given point of the parsing
        sequence without generation of output values.

        Optionally the conversion functions may be replaced with predefined
        strings that select special built-in parsing operations. Currenly the
        following special parsing operations are implemented:

        - 'auto' - the method tries and converts automatically the next token
                   into one of the built-in types int, float or str.
        - 'tail' - the method treats and extracts as a single string all the
                   remaining characters pending to parse. This is always the
                   last element returned by the method.
        - 'skip' - the method discards the next token in the string to parse.

        :param  convfns: the conversion/parsing function or the list or tuple
                         of conversion/parsing functions that is used by the
                         method. The strings 'auto', 'tail' and 'skip' may be
                         used instead of a function to indicate special parsing
                         operations.
        :param  devquery: The query string to be sent to the device followed by
                          any additional required arguments.
        :raises DeviceError: If the query is not properly executed in the
                             device.
        :raises IOError: If there is any communication error.
        :raises ValueError: If devquery is not a query or if the answer from
                            the device is not compatible with the conversion
                            scheme described by convfs.
        """
        retansw = self.command(devquery, *args)

        if not retansw:
            raise ValueError("not a query: '" + devquery + "'")
        else:
            if type(retansw) is tuple:
                answ, binary_answ = retansw
            else:
                answ, binary_answ = (retansw, None)

        vtuple = self.parse_string(convfns, answ)

        if binary_answ:
            return vtuple, binary_answ
        else:
            return vtuple

    @staticmethod
    def parse_string(convfns, answ):
        """
        Parses an input string and extracts values by the same method used in
        parse_answer(). Returns the input string converted into a tuple of
        values, each element of the tuple having been parsed accordingly to
        the argument convfns.

        :param  convfns: the conversion/parsing function or the list or tuple
                         of conversion/parsing functions that is used by the
                         method. The strings 'auto', 'tail' and 'skip' may be
                         used instead of a function to indicate special parsing
                         operations.
        :param  answ: The string to be parsed.
        :raises ValueError: If the input string is not compatible with the
                            conversion scheme described by convfs.
        """
        str_remain = answ.strip()
        vtuple = ()

        if isinstance(convfns, (list, tuple)):
            fn_iterator = cycle(list(convfns))
        else:
            fn_iterator = cycle([convfns])

        for type_fn in fn_iterator:
            if str_remain == None:
                break

            if (type(type_fn) is str):
                if type_fn == 'auto':
                    # None requests automatic parsing in _string_extract()
                    type_fn = None
                elif type_fn == 'tail':
                    vtuple += (str_remain,)
                    break
                elif type_fn == 'skip':
                    # No try block here: parsing strings should never
                    # raise exceptions...
                    value, str_remain = DanceDevice.\
                      _string_extract(str, str_remain)
                    continue
                else:
                    msg = "string '%s' is not a valid special " % type_fn
                    msg += "parsing selector"
                    raise ValueError(msg)
            try:
                value, str_remain = DanceDevice.\
                  _string_extract(type_fn, str_remain)
            except ValueError as ex:
                raise ValueError(str(ex) + " from string: '" + answ + "'")

            if value != None:
                vtuple += (value,)

        return vtuple

    def param_info(self, param):
        """
        Returns parameter information (data type, min, max) associated to
        the parameter <param> .

        :param str param: Prameter name
        :returns: The tuple (type, min, max)
        :rtype: Tuple
        """
        if not param in self._params:
            msg = '%s is not a valid parameter name for device %s'
            msg = msg % (param, self.hostname())
            raise ValueError(msg)

        answ = self.command("?PARINFO", param).split()

        answ[0] = int if answ[0] == 'INT' else float
        answ[1] = float(answ[1])
        answ[2] = float(answ[2])

        return tuple(answ)

    def param_set(self, param, value):
        """
        Sets parameter <param> to value <value>, the value is automatically
        converted to the type associated with <param> (see param_info())

        :param str param: The parameter name
        :param str value: str representation of the value
        :returns: The actual value after the set operation, type depends on
                  <param>
        """
        ptype, min_, max_ = self.param_info(param)
        value = float(value)

        if ptype is int:
            value = int(round(value))

        if (value < min_) or (value > max_):
            raise ERR_BAD_RANGE

        self.ackcommand(param, value)
        return self.param_query(param, ptype)

    def param_query(self, param, ptype=None):
        """
        :returns: The value of parameter <param>, applies the type function
                  ptype before returning. The return type is the one associated
                  with <param> if no ptype function is given. (see param_info()) 
        """
        if not ptype:
            ptype, min_, max_ = self.param_info(param)

        value = self.parse_answer(ptype, '?'+param)

        if isinstance(value, tuple):
           value = value[0]
        return value

    def params(self):
        return self._params

    def sequence_init(self):
        self.ackcommand("CONFIG STORE")

    def sequence_cleanup(self):
        self.ackcommand("CONFIG RESTORE")
