# -*- coding: utf-8 -*-
import time
import os
import urlparse

class Sampler(object):
    """
    Object that encapsulates the most basic sampling logic, to be used as
    Base Class for more complex sampling logic. The methods init() and
    cleanup() are called before and after the acquire() method. Acquire should
    contain the main acquisition logic.
    """
    def __init__(self, name, device, fun_name):
        self.device = device
        self.name = name
        self.fun_name = fun_name
        self.fun = getattr(self.device, self.fun_name)

    def sample(self, scan_frame):
        """
        Implementation of sampling logic

        :param ScanFrame scan_frame: ScanFrame object used during the sampling
                                     process
        :returns: The sample data in a dictionary, on the form
                  {name_1: value_1, ... name_n: value_n}
        :rtype: dict
        """
        return self.fun(*scan_frame.get_default_sample_args())

    def init(self):
        """
        Initialize the acquisition, called once before series of acquire()
        calls.
        """
        self.device.sequence_init()

    def cleanup(self):
        """
        Called once after a series of acquire() calls.
        """
        self.device.sequence_cleanup()


class Actuator(object):
    """
    Actuator BaseClass that implements the most basic Actuator behavior.
    """
    def __init__(self, device, param_name):
        self.device = device
        self.param_name = param_name
        self._value_on_init = None

    def set(self, value):
        """
        Sets a value or values, the BaseClass implementation only handles one
        value.

        :param Object value: The value to set
        :returns: The value returned by get() after the value have been set
        :rtype: Object
        """
        return self.device.param_set(self.param_name, value)

    def get(self):
        """
        :returns: The value associated with the actuator
        """
        return self.device.param_query(self.param_name)

    def init(self):
        """
        Initialize the actuator, called once before a series of a call to
        set() or get()

        :returns: None
        """
        self._value_on_init = self.get()

    def cleanup(self):
        """
        Cleans-up the actuator after a series of get() or set()
        """
        self.set(self._value_on_init)

    def check_range(self, start, end):
        """
        Check if the value range <start> <end> is valid for this actuator:

        :param float start: Start of value range
        :param float end: End of value range
        :returns: A Tuple containing ('state', message) where state is True
                  if the range is valid and Flase otherwise. The message
                  is used to give a message to the caller in case 'state' is
                  False.
        """
        try:
            ptype, min_, max_ = self.device.param_info(self.param_name)
        except ValueError as ex:
            return False, '%s ' % self.param_name +  str(ex)

        if (start < min_) or (start > max_) or (end < min_) or (end > max_):
            msg = "Given range %G:%G" % (start, end)
            msg += " for %s is out of limits %s"
            msg = msg % (self.param_name.upper(), str((min_, max_)))
            return False, msg

        return True, ''


class ScanFrame(object):
    """
    Object that creates a context/environment for the scan to run it. The
    ScanFrame object is passed as an argument to the ScanProcedure. The
    ScanFrame object is only valid for the runtime of the ScanProcedure, and
    is automatically deleted or invalidated after execution finishes.
    """
    def __init__(self, engine):
        self._valid = True
        self._engine = engine
        self._apps = engine._app_list
        self.session = engine._session_mgr.session()

    def is_valid(self):
        return self._valid

    def init(self, *args, **kwargs):
        self._engine.scan_init(*args, **kwargs)

    def data_acquired(self, *args, **kwargs):
        self._engine.scan_data_acquired(*args)

    def end(self, *args, **kwargs):
        self._valid = False
        self._engine.scan_end(*args, **kwargs)

    def scan_name(self, line):
        return self._engine.get_scan_name(line)

    def get_acq_methods(self):
        return self._engine.get_acq_methods()

    def get_default_sample_args(self):
        return self._engine.get_default_sample_args()

    def cleanup_actuators(self):
        return self._engine.cleanup_actuators()


class ScanEngine(object):
    def __init__(self):
        self._session_mgr = None
        self._data_store = None
        self._app_list = []
        self._current_actuators = []
        self._device_dict = {}
        self._default_device_netloc = None
        self._sample_methods = {}

    def register_app(self, app):
        """
        Adds a application object (external object that uses the ScanEngine).
        The registered object will receive all ScanEngine events such as;
        scan_init, scan_data_acquired, scan_end. The applications need to
        share the Session object.

        :returns: None
        """
        self._app_list.append(app)

    def set_data_store(self, data_store):
        """
        Sets the DataStore object to use for data storage

        :param data_store: DataStore object
        :returns: None
        """
        self._data_store = data_store

    def set_session_mgr(self, session_mgr):
        """
        Sets the session_mgr object to use. The session is used to manage
        variables and configuration parameters that are directly or indirectly
        used by the scan procedures. The session object is persistent and can be
        loaded from disk if necessary.

        :param session_mgr: SessionManager object
        :returns: None
        """
        self._session_mgr = session_mgr

        # Initialize the internal sample method data structure with potential
        # methods saved in the context.
        for method_name in self._session_mgr.get_value('SCAN.ACQ_METHODS'):
            self.add_sample_method(method_name)

    def current_session(self):
        """
        :returns: The currently used Session object
        """
        return self._session

    def new_scan_frame(self):
        """
        Creates and returns a new ScanFrame
        :returns: ScanFrame
        """
        return ScanFrame(self)

    def scan_init(self, actuator_list=[]):
        """
        Called before or at the very beginning of ScanProcedure execution.

        :param str scan_name: The name of the scan
        :param list actuator_list: List of actuators that will be used

        :returns: None
        """    
        scan_name = self.get_scan_name()
        self._session_mgr.session().last_scan_name = scan_name
        self._session_mgr.session().scanning = True
        self._session_mgr.session().scan_stime = time.time()

        self.init_actuators(actuator_list)
        self.init_acquistion_methods()

        self._data_store.create_measurement(scan_name)

        for app in self._app_list:
            app.scan_init(scan_name, actuator_list)

        msg = "Using temporary data file %s for recovery\n\n"
        msg = msg % self._data_store.tmpfilename()

        for app in self._app_list:
            app.user_msg(msg)

    def scan_data_acquired(self, param, data):
        """
        Method called by ScanProcedure has acquired data.

        :returns: None
        """
        scan_name = self.get_scan_name()
        self._data_store[scan_name].add_sample_set(data)

        for app in self._app_list:
            app.scan_data_acquired(scan_name, param, data)

    def scan_end(self, actuator_list):
        """
        Called at the very end or after ScanProcedure execution.

        :param str scan_name: The name of the scan
        :param list actuator_list: List of actuators that will be used

        :returns: None
        """
        self.cleanup_last_scan()

        for app in self._app_list:
            app.scan_end(self.get_scan_name(), actuator_list)

    def init_acquistion_methods(self):
        """
        Initializes acquisitions that are to be used by the current
        ScanProcedure. The method get_acq_methods should return the
        acquisition methods to initialize.

        :returns: None
        """
        for method in self.get_acq_methods():
            method.init()

    def cleanup_acquisition_methods(self):
        """
        Clean-up the acquisition methods used, at the end of the scan or if
        something unexpected occurred (Exception or other error state). The
        method get_acq_methods should return the acquisition methods to
        clean-up

        :returns: None
        """
        for method in self.get_acq_methods():
            method.cleanup()

    def init_actuators(self, actuator_list):
        """
        Initializes the actuators before the scan starts.

        :param list actuator_list: A list of Actuator objects to use
        :returns: None
        """
        self._current_actuators = actuator_list

        for actuator in self._current_actuators:
            actuator.init()

    def cleanup_actuators(self):
        """
        Clean-up the actuators used by the scan at the end of scan or when
        something unexpected occurs (Exception or other error state).

        :returns: None
        """

        # Only cleanup if there is something to cleanup
        if self._current_actuators:
            values = []

            msg = "\nResetting actuators to their original values(s) !\n"

            for app in self._app_list:
                app.user_msg(msg)

            for actuator in self._current_actuators:
                actuator.cleanup()
                values.append((actuator.param_name, actuator.get()))

            for app in self._app_list:
                msg = ', '.join(['%s=%s' % (n, v) for n, v in values])
                app.user_msg(msg)

            self._current_actuators = []

    def save_data(self, scan_name):
        """
        Write the data that belongs to <scan_name> to disk

        :param str scan_name: Scan name
        :returns: None
        """
        self._data_store.write(scan_name)

        for app in self._app_list:
            msg = "\n\nWrote data as %s to: '%s'\n"
            msg = msg % (scan_name, self._data_store.filename())
            app.user_msg(msg)

    def add_sample_method(self, netloc):
        for app in self._app_list:
            dev_netloc, path = app.netloc_to_device(netloc)
            device = app.get_device(dev_netloc)
            method = device.get_sampler(path)
            self._sample_methods[netloc] = method

            if netloc not in self._session_mgr.get_value('SCAN.ACQ_METHODS'):
                self._session_mgr.get_value('SCAN.ACQ_METHODS').append(netloc)

    def get_acq_methods(self):
        """
        :returns: A list of AcquisitionMethod objects to use to acquire data.
        """
        acq_methods = []

        for name in self._session_mgr.get_value('SCAN.ACQ_METHODS'):
            for app in self._app_list:
                acq_methods.append(self._sample_methods[name])

        return acq_methods

    def remove_sample_method(self, netloc):
        if netloc in self._sample_methods.keys():
            del self._sample_methods[netloc]
            self._session_mgr.get_value('SCAN.ACQ_METHODS').remove(netloc)

    def get_actuator(self, param_end_point):
        """
        Creates and returns a Actuator object corresponding to the endpoint
        <endpoint>. The endpoint point to any device through the protocol:

            protocol://host:port/<parameter name>

        Returns the first match (device) if only <parameter name> and not
        the full protocol://host:port/ is given.

        :returns: Actuator
        :rtype: Actuator
        """
        # If param end points is passed with a netloc and path but
        # not protocol add deep:// as default protocol
        if not '://' in param_end_point and '/' in param_end_point:
            param_end_point = 'deep://' + param_end_point

        if '/' in param_end_point:
            pres = urlparse.urlparse(param_end_point)
            netloc = pres.netloc
            # Ignore slash in beginning of path
            param_path = pres.path[1:]
        else:
            # TODO Add better management of devices
            if not self._default_device_netloc:
                cmd, device = self._app_list[0].get_device_cmd()
                self._device_dict[device.hostname()] = device
                self._default_device_netloc = device.hostname()

            netloc = self._default_device_netloc
            param_path = param_end_point

        try:
            device = self._device_dict[netloc]
        except KeyError:
            try:
                device = self._app_list[0].DEVICE_CLS(netloc)
                self._device_dict[netloc] = device
            except IOError as ex:
                self._app_list[0].user_msg('\n' + str(ex) + '\n\n')
                return None

        return device.get_actuator(param_path)

    def get_default_sample_args(self):
        """
        Gets the default arguments to pass to the Sampler object
        :returns: A tuple with arguments
        """
        if self._session_mgr.session().last_cli_iargs:
            return self._session_mgr.session().last_cli_iargs
        else:
            return (self._session_mgr.get_value('SCAN.ACQTIME'),)

    def get_scan_name(self):
        """
        :returns: The scan name
        """
        self._data_store.set_file_path(self.get_scan_path())
        line = self._session_mgr.session().last_cli_line
        ds = self._data_store

        try:
            sname = self._session_mgr.get_value('SCAN.SCAN_NAME')
            sname = sname.format(num=ds.num_data_sets(), cmd=line)
        except KeyError:
            sname = 'scan-%s' % self._session_mgr.session().scan_num

        return sname

    def get_scan_path(self, fname=None):
        """
        Gets the scan path to use, that is in which directory and which
        file to store data.

        :param str fname: The file name
        :returns: The path
        :rtype: str
        """
        dname = self._session_mgr.get_value('DATA.DATA_DIR')

        # Todo encapsulate session methods in a new Session obejct !?
        sname = self._app_list[0].session_mgr.session_name()

        if not fname:
            fname = self._session_mgr.get_value('DATA.DATA_FILE')

        try:
            _date = time.strftime('%y%m%d', time.localtime())
            _time = time.strftime('%H%M%S', time.localtime())
            fname = fname.format(date=_date, time=_time, session_name=sname)
        except KeyError:
            fname = '%s-session-data' % sname

        return os.path.join(dname, fname)

    def cleanup_last_scan(self):
        """
        Clean up the last scan (the one just performed). Calls cleanup() on
        all Actuators and AcquisitionMethods. Applies the data saving "scheme"
        defined in the SCAN/DATA/AUTO_SAVE parameter within the session.
        """
        if self._session_mgr.session().scanning:
            if self._session_mgr.session().get_value('SCAN.SCAN_RESET'):
                self.cleanup_actuators()
            elif self._current_actuators:
                msg = "\nWARNING: The SCAN_RESET flag is set to false, "
                msg += "NOT resetting any actuators\n"

                for app in self._app_list:
                    app.user_msg(msg)

            self.cleanup_acquisition_methods()

            if self._session_mgr.get_value('DATA.AUTO_SAVE') == True:
                 self.save_data(self._session_mgr.session().last_scan_name)
            elif self._session_mgr.get_value('DATA.DATA_WARNING') == True:
                 msg = "\nWARNING: Data storage is turned off, "
                 msg += "(AUTO_SAVE = False), no data is saved !. "
                 msg += "You can save the data with the .save "
                 msg += "command. \n\n"

                 for app in self._app_list:
                     app.user_msg(msg)

            msg = 'Scan took %.2fs\n\n'
            msg = msg % round(time.time() - self._session_mgr.session().scan_stime, 2)

            for app in self._app_list:
                app.user_msg(msg)

            self._session_mgr.session().scanning = False


def run_with_cleanup(procedure, scan_engine, act_list, viter_list):
    """
    Executes a procedure, initializing actuator and acquisition methods before
    and cleaning up after execution.
    """
    sframe = scan_engine.new_scan_frame()

    try:
        sframe.init(act_list)
        procedure(sframe, act_list, viter_list)
        sframe.end(act_list)
    except Exception as ex:
        #print('scanengine.py run_with_cleanup, exception at exec\n ')
        #traceback.print_exc()
        raise ex
