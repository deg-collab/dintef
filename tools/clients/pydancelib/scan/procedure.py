# -*- coding: utf-8 -*-
from .engine import Actuator
from ..io.stream import dprint

def frange(start, end, step = 1.0):
    """
    A float range generator that generates values between start and end
    with increment of <step>.

    :param float start:
    :param float end:
    :param float step:
    :returns: Generator that yields values between start and end
    """
    val = start

    if step == 0.0:
        raise ValueError("step must not be zero")

    while True:
        if (step > 0 and val >= end) or (step < 0 and val <= end):
            raise StopIteration

        yield val
        val += step


def fpoints(start, step, n):
    """
    A float range generator that generates exactly <n> values from start
    with increments of size <step>. That is vn = start + n*step.

    :param float start: Start value
    :param float step: Step, increment/step size
    :param int n: Number of values
    :returns: A generator that yields n values
    """
    val = start
    _n = 0

    while True:
        if (_n >= n):
            raise StopIteration

        yield val
        val += step
        _n += 1


def value_iterator(value_arg):
    """
    Creates and returns a iterator from an argument that describes that
    iterator.
    """
    if isinstance(value_arg, tuple):
        start, end, nint = value_arg
        npoints = nint + 1

        # If nint is 1 we only do one step from end to start, of size
        # end - start.
        if npoints - 1 != 0.0:
            step = (end - start) / (npoints - 1)
        else:
            step = (end - start)

        return fpoints(start, step, npoints), start, end, step
    elif isinstance(value_arg, list):
        return iter(value_arg), value_arg[0], value_arg[-1], None


def scan(sframe, actuator_list, value_iter_list):
    actuator = actuator_list[0]
    param_name = actuator.param_name

    for i in value_iter_list[0]:
        val = actuator.set(i)
        data = {}

        dprint('%s = %s' % (param_name, val))
        data[param_name] = val

        for method in sframe.get_acq_methods():
            data.update(method.sample(sframe))

        sframe.data_acquired(param_name, data)


def dscan(sframe, actuator_list, value_iter_list):
    actuator = actuator_list[0]
    param_name = actuator.param_name
    start_value = actuator.get()
    
    for i in value_iter_list[0]:
        val = actuator.set(i + start_value)
        data = {}

        dprint('%s = %s' % (param_name, val))
        data[param_name] = val

        for method in sframe.get_acq_methods():
            data.update(method.sample(sframe))

        sframe.data_acquired(param_name, data)

    sframe.cleanup_actuators()


def measure(sframe, actuator_list, value_iter_list):
    data_dict = {}

    for method in sframe.get_acq_methods():
        data = method.sample(sframe)
        data_dict.update(data)

    sframe.data_acquired(0, data_dict)


def loopscan(sframe, actuator_list, value_iter_list):
    value_iter = value_iter_list[0]

    for i in value_iter:
        data_dict = {}

        for method in sframe.get_acq_methods():
            data = method.sample(sframe)
            data_dict.update(data)

        sframe.data_acquired(i, data_dict)


def timescan(sframe, actuator_list, value_iter_list):
    i = 0
    while True:
        data_dict = {}

        for method in sframe.get_acq_methods():
            data = method.sample(sframe)
            data_dict.update(data)

        sframe.data_acquired(i, data_dict)
        i += 1


def nscan(sframe, actuator_list, value_iter_list):
    # Assume for the time being that all parameters are on the
    # same device.
    param = actuator_list[0].param_name

    while value_iter_list:
        try:
            values = tuple(map(next, value_iter_list))
            msg_parts = []
            data = {}

            for i in range(0, len(actuator_list)):
                value = actuator_list[i].set(values[i])
                msg_parts.append((actuator_list[i].param_name, value))
                data[actuator_list[i].param_name] = value

            msg = ', '.join(map('{0[0]}={0[1]}'.format, msg_parts))
            dprint(msg)

            for method in sframe.get_acq_methods():
                data.update(method.sample(sframe))

            sframe.data_acquired(param, data)

        except StopIteration:
            break

def dnscan(sframe, actuator_list, value_iter_list):
    # Assume for the time being that all parameters are on the
    # same device.
    param = actuator_list[0].param_name
    start_values = map(Actuator.get, actuator_list)
    
    while value_iter_list:
        try:
            values = tuple(map(next, value_iter_list))
            msg_parts = []
            data = {}

            for i in range(0, len(actuator_list)):
                value = actuator_list[i].set(start_values[i] + values[i])
                msg_parts.append((actuator_list[i].param_name, value))
                data[actuator_list[i].param_name] = value

            msg = ', '.join(map('{0[0]}={0[1]}'.format, msg_parts))
            dprint(msg)

            for method in sframe.get_acq_methods():
                data.update(method.sample(sframe))

            sframe.data_acquired(param, data)
        except StopIteration:
            break

    sframe.cleanup_actuators()


def mscan(sframe, actuator_list, value_iter_list):
    # Assume for the time being that all parameters are on the
    # same device.
    param = actuator_list[0].param_name

    try:
        data = {}
        param_values_list = map(tuple, value_iter_list)
        result = [[]]

        for value_list in param_values_list:
            result = [x+[y] for x in result for y in value_list]

        for pvalues in result:
            msg_parts = []

            for i in range(0, len(pvalues)):
                value = actuator_list[i].set(pvalues[i])
                data[actuator_list[i].param_name] = value
                msg_parts.append((actuator_list[i].param_name, value))

            msg = ', '.join(map('{0[0]}={0[1]}'.format, msg_parts))
            dprint(msg)

            for method in sframe.get_acq_methods():
                data.update(method.sample(sframe))

            sframe.data_acquired(param, data)
    except StopIteration:
        pass


def dmscan(sframe, actuator_list, value_iter_list):
    # Assume for the time being that all parameters are on the
    # same device.
    param = actuator_list[0].param_name
    start_values = map(Actuator.get, actuator_list)

    try:
        data = {}
        param_values_list = map(tuple, value_iter_list)
        result = [[]]

        for value_list in param_values_list:
            result = [x+[y] for x in result for y in value_list]

        for pvalues in result:
            msg_parts = []

            for i in range(0, len(pvalues)):
                value = actuator_list[i].set(start_values[i] + pvalues[i])
                data[actuator_list[i].param_name] = value
                msg_parts.append((actuator_list[i].param_name, value))

            msg = ', '.join(map('{0[0]}={0[1]}'.format, msg_parts))
            dprint(msg)

            for method in sframe.get_acq_methods():
                data.update(method.sample(sframe))

            sframe.data_acquired(param, data)
    except StopIteration:
        pass

    sframe.cleanup_actuators()
