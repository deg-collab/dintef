# -*- coding: utf-8 -*-
__version__ = '0.1-alpha'
__description__ = 'Command line utility for running tests on devices'
__author__ = 'Marcus Oskarsson'
__author_email__ = 'marcus.oscarsson@esrf.fr'
__url__ = 'https://deg-svn.esrf.fr/svn/pydancelib/dev/'
