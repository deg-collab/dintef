# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function


class TokenParser(object):

    def __init__(self, tag=None, silent=False, capitalise=False):
        self._tag = tag
        self._silent = silent
        self._capitalise = capitalise

    @property
    def silent(self):
        return self._silent

    def _extract_token(self, strarg):
        s = strarg.split(None, 1)
        try:
            token = s[0]
            try:
                tail = s[1]
            except:
                tail = None
        except:
            raise ValueError('missing token in parsed string')

        if self._capitalise:
            token = token.upper()
        return token, tail


    def _build_strlist(self, args):
        if type(args) is str:  # first try if string
            if self._capitalise: args = args.upper()
            return args.split()

        else:
            try:               # then try if iterable
                strlist = []
                for a in args:
                    self._build_strlist(a)
                    strlist += self._build_strlist(a)
                return strlist
            except TypeError:  # otherwise convert to str
                return self._build_strlist(str(args))


    def _parse_tag(self, strarg):
        token, tail = self._extract_token(strarg)
        if token == self._tag:
            return tail
        else:
            raise ValueError('tag "{}" not found in string'.format(self._tag))


    def __call__(self, strarg):
        try:
            if self._tag:
                strarg = self._parse_tag(strarg)
            return self.do_parse(strarg)

        except ValueError as ex:
            if self._silent:
                return None, strarg
            else:
                raise ex


    def do_parse(self, strarg):
        raise NotImplementedError('parser classes must implement do_parse()')



class StringParser(TokenParser):

    def do_parse(self, strarg):
        strarg = strarg.lstrip()
        if not strarg or strarg[0] not in '\'\"':
            string = strarg
            tail = None
        else:
            try:
                length = strarg[1:].index(strarg[0])
                string = strarg[1:1+length]
                tail = strarg[length+2:]
            except:
                string = strarg[1:]
                tail = None

        if self._capitalise:
            string = string.upper()
        return string, tail



class OptionParser(TokenParser):

    def __init__(self, *args, **kwargs):
        self.uselabels = kwargs.pop("uselabels", False)
        self.usebitmask = kwargs.pop("usebitmask", False)
        super(OptionParser, self).__init__(**kwargs)

        self.options = self._build_strlist(args)
        self.length = len(self.options)

        if not self.options:
            raise ValueError('missing valid option list')

        if len(set(self.options)) != self.length:
            raise ValueError('repeated options are invalid')


    def _extract_option(self, strarg):
        token, tail = self._extract_token(strarg)
        try:
            index = list.index(self.options, token)
        except:
            raise ValueError('token \'{}\' is not a valid option or flag'.format(token))
        return index, token, tail


    def do_parse(self, strarg):
        index, token, tail = self._extract_option(strarg)

        if self.uselabels:
            return token, tail
        if self.usebitmask:
            return 1 << index, tail
        else:
            return index, tail



class FlagListParser(OptionParser):

    def __init__(self, *args, **kwargs):
        self.allowempty = kwargs.pop("allowempty", True)
        super(FlagListParser, self).__init__(*args, **kwargs)

    def do_parse(self, strarg):
        indexset = set()
        tail = strarg
        while tail:
            try:
                index, token, tail = self._extract_option(tail)
                indexset.add(index)
            except ValueError:
                break

        if not indexset and not self.allowempty:
            raise ValueError('no valid flags found in parsed string')

        if self.uselabels:
            return [self.options[i] for i in indexset], tail
        if self.usebitmask:
            mask = 0
            for i in indexset:
                mask |= (1 << i)
            return mask, tail
        else:
            return [i for i in indexset], tail



class BoolParser(TokenParser):

    _tvals = ['YES','ON',  '1']
    _fvals = ['NO', 'OFF', '0']

    def __init__(self, truevals=[], falsevals=[], **kwargs):
        super(BoolParser, self).__init__(**kwargs)

        if truevals:
            self._tvals = self._build_strlist(truevals)
            if falsevals:
                self._fvals = self._build_strlist(falsevals)
            else:
                self._fvals = []

        elif falsevals:
            raise ValueError('cannot set false values in absence of true ones')

    def do_parse(self, strarg):
        token, tail = self._extract_token(strarg)

        if token in self._tvals:
            return True, tail
        elif not self._fvals or token in self._fvals:
            return False, tail
        else:
            raise ValueError('boolean value not found in "{}"'.format(strarg))



class NumberParser(TokenParser):

    def __init__(self, tointeger=False, tofloat=False, isinteger=False, ispositive=False, isnonzero=False, **kwargs):
        super(NumberParser, self).__init__(**kwargs)

        self.tointeger = tointeger
        self.tofloat = tofloat
        self.isinteger = isinteger
        self.ispositive = ispositive
        self.isnonzero = isnonzero

    def do_parse(self, strarg):
        token, tail = self._extract_token(strarg)

        try:
            if token[0:2].lower() == '0x':
                value = int(token, base=16)
            else:
                try:
                    value = int(token)
                except ValueError:
                    value = float(token)
        except ValueError:
            raise ValueError('non numeric value from "{}"'.format(strarg))

        if self.isinteger and type(value) is not int:
            raise ValueError('non integer numeric value "{}"'.format(token))
        if self.ispositive and value < 0:
            raise ValueError('negative numeric value in "{}"'.format(token))
        if self.isnonzero and value == 0:
            raise ValueError('null numeric value in "{}"'.format(token))

        if self.tointeger:
            value = int(value)
        elif self.tofloat:
            value = float(value)

        return value, tail
