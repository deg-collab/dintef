# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function

from itertools import cycle

class AnswerParser(object):
    def __init__(self, parsefns):
        self.parsefns = parsefns

    """
    This class creates parser objects that can convert input ASCII strings
    into tuples of values, each element of the tuple having been parsed
    accordingly to the argument parsefns.
    In general, parsefns is either a function or a sequence of
    functions that is used to extract the elements included in the returned tuple.
    If needed, the sequence of conversion or parsing functions in parsefns
    is repeated cyclically until all the tokens in the input string are parsed.
    The most frequently used conversion functions are the built-in types int, float
    and str, but the method accepts also any user provided parsing function
    as well as certain objects that select special conversions as described
    below. In general the built-in conversion method extract a token
    separated by whitespaces from the input string and converts it
    to the corresponding type. However, for string conversion, the method
    also manages single or double quoted strings in the device answer that
    are treated and returned as single strings regardless of whether or not
    they include whitespaces. The built-in integer conversion is also
    extended to accept C-format hexadecimal strings.

    User provided parsing functions must parse an input string and extract
    a single element at each call. The functions must return a two-value
    tuple consisting of the extracted element, that can be an object of any
    arbitrary type, and the remaining string still to be parsed. If the
    extracted element returned by the user function is None, the element
    is ignored and not included in the final sequence. This feature may be
    used to manipulate the ASCII string at a given point of the parsing
    sequence without generating additional output values.

    Optionally the conversion functions may be replaced with predefined
    strings that select special built-in parsing operations. Currently the
    following special parsing operations are implemented:

    - 'auto' - the method tries and converts automatically the next token
               into one of the built-in types int, float or str.
    - 'tail' - the method treats and extracts as a single string all the
               remaining characters pending to parse. This is always the
               last element returned by the parsing process.
    - 'skip' - the method discards the next token in the string to parse.

    :param  parsefns: the conversion/parsing function or the list or tuple
                     of conversion/parsing functions that is used by the
                     method. The strings 'auto', 'tail' and 'skip' may be
                     used instead of a function to indicate special parsing
                     operations.
    """

    def parse(self, answ):
        """
        Parses an input string and extracts values by the same method used in
        parse_answer(). Returns the input string converted into a tuple of
        values, each element of the tuple having been parsed accordingly to
        the attribute parsefns.

        :param  answ: The string to be parsed.
        :raises ValueError: If the input string is not compatible with the
                            conversion scheme described by parsefns.
        """
        parsefns = self.parsefns
        str_remain = answ.strip()
        vtuple = ()

        if isinstance(parsefns, (list, tuple)):
            single = False
            fn_iterator = cycle(list(parsefns))
        else:
            single = True
            fn_iterator = cycle([parsefns])

        for type_fn in fn_iterator:
            if str_remain == None:
                break

            if (type(type_fn) is str):
                if type_fn == 'auto':
                    # None requests automatic parsing in _string_extract()
                    type_fn = None
                elif type_fn == 'tail':
                    vtuple += (str_remain,)
                    break
                elif type_fn == 'skip':
                    # No try block here: parsing strings should never
                    # raise exceptions...
                    value, str_remain = self._string_extract(str, str_remain)
                    continue
                else:
                    msg = "string '%s' is not a valid special " % type_fn
                    msg += "parsing selector"
                    raise ValueError(msg)
            try:
                value, str_remain = self._string_extract(type_fn, str_remain)
            except ValueError as ex:
                raise ValueError(str(ex) + " from string: '" + answ + "'")

            if value != None:
                vtuple += (value,)

        return vtuple if len(vtuple) > 1 or not single else vtuple[0]


    @classmethod
    def _string_extract(cls, type_fn, str_value):
        if type_fn in (int, float, str):
            if not str_value:
               if type_fn == str:
                   return None, None
               else:
                   msg = "empty string cannot be converted to " + repr(type_fn)
                   raise ValueError(msg)

            elif type_fn == str and str_value[0] in "\"\'":
               idx = str_value[1:].find(str_value[0])
               if idx >= 0:
                   return str_value[1:1+idx], str_value[2+idx:].strip()
               else:
                   return str_value[1:].strip(), None

            else:
               strsplit = str_value.split(None, 1)
               str_token = strsplit[0]
               str_remain = strsplit[1] if len(strsplit) == 2 else None

               if type_fn == int and str_token[0:2].lower() == '0x':
                   # convert C-type hexadecimal values
                   return int(str_token, base=16), str_remain
               else:
                   return type_fn(str_token), str_remain

        elif type_fn:
            # used define parsing function
            return type_fn(str_value)

        else:
            # type_fn == None
            try:
                return cls._string_extract(int, str_value)
            except ValueError:
                try:
                    return cls._string_extract(float, str_value)
                except ValueError:
                    return cls._string_extract(str, str_value)
