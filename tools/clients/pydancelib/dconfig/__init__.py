# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function


from . dconfig import DConfig
from . import _version

__version__ = _version.VERSION
__author__ = _version.AUTHOR
__author_email__ =  _version.AUTHOR_EMAIL
__description__ = _version.DESCRIPTON
__url__ = _version.URL
