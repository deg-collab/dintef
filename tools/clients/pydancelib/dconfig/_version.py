# -*- coding: utf-8 -*-
VERSION = '0.0.1'
AUTHOR = 'Marcus Oscarsson'
AUTHOR_EMAIL =  'marcus.oscarsson@esrf.fr'
DESCRIPTON = "Library for reading and writing Dance device configuration data"
URL = "https://deg-svn.esrf.fr/svn/dconfig"
