#ModuleSet:DACs
modules   :0
numBits   :200
numSignals:20
reverse   :0
address   :18
#Signals:MON_DAC_IP:CP_ibias_buf_n:CP0_ibias_p:COMP0_Vthresh_p:AMP_VC_p:Buffer_FE_Out_ibias_n:INJECT0_p:I_InpMirr_n:VCO_ibias_BufBig_p:VCO_ibias_BufSm_p:AMP_ibias_legs_p:AMP_ibias_tail_n:AMP_ibias_comp_n:CP1_ibias_p:COMP1_Vthresh_p:AMP_Vref_p:INJECT1_p:VPump1_p:VPump0_p:VCO_ibias_p:
#SignalsAliases:::::::::::::::::::::
#ReadOnly:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:
#ActiveLow:0:1:0:0:0:1:0:1:0:0:0:1:1:0:0:0:0:0:0:0:
#Positions:0-9:10-19:20-29:30-39:40-49:50-59:60-69:70-79:80-89:90-99:100-109:110-119:120-129:130-139:140-149:150-159:160-169:170-179:180-189:190-199:
#AccessLevels:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:
#Outputs:0:
#Module:0:1023:250:260:650:800:512:0:0:0:0:386:290:300:210:250:350:200:600:600:0:
#ModuleSet:Global Control Register
modules   :0
numBits   :133
numSignals:26
reverse   :0
address   :17
#Signals:px_shunt:conf_test_pattern:conf_cycle_length:mon_sel:MUX_BigCSA:MUX_FE0:MUX_FE1:MUX_FE2:MUX_FE3:MUX_RAW:EnMuxes:SerIn:gl_InpCurrMirrEnBig_N:gl_InpCurrMirrEn0_N:gl_InpCurrMirrEn1_N:gl_InpCurrMirrEn2_N:gl_InpCurrMirrEn3_N:gl_InpCurrMirrEnRaw_N:gl_bigFE_gain:gl_bigFE_inj:gl_FE0_FE2_BigInj_En:gl_FE1_FE3_BigInj_En:gl_FE0_FE2_SmInj_En:gl_FE1_FE3_SmInj_En:gl_AmpInpCap:gl_nc:
#SignalsAliases:::::::::::::::::::::::::::
#ReadOnly:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:1:
#ActiveLow:0:0:0:0:0:0:0:0:0:0:0:0:1:1:1:1:1:1:0:0:0:0:0:0:0:0:
#Positions:57-30:98-113:114-126:127-130:0-4:20-24:15-19:10-14:5-9:25-29:131:132:75:71:72:73:74:70:76-78:79-82:58;60:62;64:59;61:63;65:66-69:83-97:
#AccessLevels:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:
#Outputs:0:
#Module:0:0:2:99:0:1:30:31:1:1:1:0:0:0:0:0:0:0:0:7:15:0:0:0:0:0:0:
#ModuleSet:Sequencer
modules   :0
numBits   :49
numSignals:6
reverse   :0
address   :16
#Signals:stats_val_iprog:stats_val_idle:invert_hold:sel_ext:sel_mon_ddyn:track_hold:
#SignalsAliases:::::::
#ReadOnly:0:0:0:0:0:0:
#ActiveLow:0:0:0:0:0:0:
#Positions:0-8:9-17:18-26:27-35:36-39:40-48:
#AccessLevels:0:0:0:0:0:0:
#Outputs:0:
#Module:0:0:1:0:0:13:0:
