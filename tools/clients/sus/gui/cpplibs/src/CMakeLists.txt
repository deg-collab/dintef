set (CMAKE_VERBOSE_MAKEFILE off)

project(SUS65T2)

cmake_minimum_required(VERSION 3.1.0)
find_package(Qt4 4.5.3 COMPONENTS QtCore QtGui QtXml REQUIRED)
add_definitions(${QT_DEFINITIONS})
include(${QT_USE_FILE})

include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
else()
  message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support. Please use a different C++ compiler.")
endif()

option(BUILD_SHARED_LIBS ON)

find_package(PkgConfig)               # to load pkg_check_modules

add_definitions( -g ) # enable gnu debugger output in logger
#add_definitions( -DDBUG ) # enable debugging messages

#automatically includes the current source and binary directories
SET(CMAKE_INCLUDE_CURRENT_DIR ON)
SET(CMAKE_CXX_FLAGS "-ggdb -O0 -fopenmp -Wall -std=c++11 -Xlinker --no-as-needed") # add "-g" for debugger
# -Xlinker --as-needed is Ubuntu default
# -Xlinker --no-as-needed is Debian default, must be set for Ubuntu, otherwise there will be undefined references
#SET(CMAKE_CXX_FLAGS "-Wall -std=c++11") # add "-g" for debugger
#SET(CMAKE_EXE_LINKER_FLAGS "-Xlinker --no-as-needed") # doesn't work
#CMAKE_MODULE_LINKER_FLAGS
#SET(CMAKE_SHARED_LINKER_FLAGS "-Xlinker --as-needed")
#message("COMPILER IS ${CMAKE_CXX_COMPILER}")

link_directories(
   ${QT_LIBRARY_DIR}
   ${SUS65T1_BINARY_DIR}/build/libs
   )

include_directories(
  ${QT_INCLUDE_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}/src/ConfigReg
  ${CMAKE_CURRENT_SOURCE_DIR}/src/ConfigReg/ConfigReg
  ${CMAKE_CURRENT_SOURCE_DIR}/src/Sequencer
  ${CMAKE_CURRENT_SOURCE_DIR}/src/QXiderConfGui
  )

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${SUS65T2_BINARY_DIR}/libs)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${SUS65T2_BINARY_DIR}/bin)

add_subdirectory(src/ConfigReg)
add_subdirectory(src/Sequencer)
add_subdirectory(src/QXiderConfGui)
