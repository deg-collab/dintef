ADD_DEFINITIONS(
  -Wall
  -Wpedantic
)

set(Utils_SRCS
  utils.cpp
  SimpleThreadSafeQueue.cpp
  VariableIterator.cpp
)

add_library(Utils SHARED ${Utils_SRCS})

#target_link_libraries(Utils
#)
