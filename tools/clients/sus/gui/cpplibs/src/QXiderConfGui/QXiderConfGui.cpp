#include <QXiderConfGui.h>
#include <QApplication>
#include <QTabWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QToolBar>
#include <thread>
#include "QConfigRegWidget.h"
#include "Sequencer_GUI.h"

using namespace SuS;

/* TODO 
 * - add moduleStr parameter for python. needed when there are multiple instances of one register,
 *   for instance a pixel register. not needed so far.
 */

QXiderConfGui::QXiderConfGui( QConfigReg* _qConfigReg, SequencerQt* _qSequencer, void (*_programJtagCallBack)(), void (*_runASICCallBack)(), void (*_stopASICCallBack)(), void (*_clearSimDumpFileCallBack)() )
  : QMainWindow(),
    qConfigRegWidget( new QConfigRegWidget( _qConfigReg ) ),
    sequencerGUI( new Sequencer_GUI( _qSequencer ) ),
    m_programJtagCallBack( _programJtagCallBack ),
    m_runASICCallBack( _runASICCallBack ),
    m_stopASICCallBack( _stopASICCallBack ),
    m_clearSimDumpFileCallBack( _clearSimDumpFileCallBack ),
    m_qSequencer( _qSequencer )
{
  QWidget *mainWidget = new QWidget;
  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainWidget->setLayout(mainLayout);
  QTabWidget* mainTabWidget = new QTabWidget;
  mainTabWidget->insertTab( 0, qConfigRegWidget, "Register Configuration" );
  mainTabWidget->insertTab( 1, sequencerGUI, "Sequencer Tracks" );
  mainTabWidget->setCurrentIndex( 0 );
  mainLayout->addWidget( mainTabWidget );
  QPushButton *progBtn = new QPushButton("Program JTAG");
  QPushButton *runBtn = new QPushButton("Run ASIC");
  QPushButton *stopBtn = new QPushButton("Stop ASIC");
  connect( progBtn, SIGNAL(clicked()), this, SLOT(programJtag()));
  connect( stopBtn, SIGNAL(clicked()), this, SLOT(stopASIC()));
  connect( runBtn, SIGNAL(clicked()), this, SLOT(runASIC()));
  mainLayout->addWidget( runBtn );
  mainLayout->addWidget( stopBtn );
  mainLayout->addWidget( progBtn );
  this->setCentralWidget( mainWidget );

  connect(sequencerGUI, SIGNAL(cycleLengthUpdated(int)), this, SLOT(setCycleLength(int)));
  initSimToolbar();

}


SuS::QXiderConfGui::~QXiderConfGui()
{
  delete qConfigRegWidget;
  delete sequencerGUI; 
}


void SuS::QXiderConfGui::setCycleLength(int _cycleLength)
{
  m_qSequencer->setCycleLength( _cycleLength );
//  qConfigRegWidget->setSignalValue( "Global Control Register", "0", "conf_cycle_length", _cycleLength-1);
  qConfigRegWidget->setSignalValue( "Global Control Register", "0", "conf_cycle_length", _cycleLength);
}


void SuS::QXiderConfGui::programJtag()
{
  m_programJtagCallBack();
}

void SuS::QXiderConfGui::runASIC()
{
  
  m_runASICCallBack();
}

void SuS::QXiderConfGui::stopASIC()
{
  
  m_stopASICCallBack();
}


void SuS::QXiderConfGui::setSimMode( bool val )
{
  this->m_simMode = val;
  
  if(this->m_simMode == 1){
    std::cout << "Simulation mode enabled. JTAG configuration will be dumped to file"<< std::endl; 
  }
  else{
    std::cout << "Simulation mode disabled. JTAG configuration will be sent to ASIC"<< std::endl; 
  }
}

bool SuS::QXiderConfGui::getSimMode( )
{
  return this->m_simMode;
//  std::cout << this->m_simMode << std::endl; 
}

void SuS::QXiderConfGui::clearSimDumpFile()
{
  m_clearSimDumpFileCallBack();
}




void SuS::QXiderConfGui::initSimToolbar()
{
  m_simToolBar = new QToolBar;
  m_simToolBar->hide();
  addToolBar(m_simToolBar);
  QPushButton* simModeBtn = new QPushButton("&Simulation Mode");
  simModeBtn->setCheckable(true);

  QWidget* simBtnsWidget = new QWidget;
  QHBoxLayout* simBtnsLayout = new QHBoxLayout;
  simBtnsWidget->setLayout(simBtnsLayout);

  m_simToolBar->addWidget(simModeBtn);
  m_simToolBar->addWidget(simBtnsWidget);

  QPushButton* clearSimDumpFileBtn = new QPushButton("&Clear Sim File");
//  QPushButton* addSimDelayBtn = new QPushButton("&Add Delay to Sim File");
//  QPushButton* initAndSingleCycleSimBtn = new QPushButton("&Init and Single Cycle to Sim");
//  QPushButton* setSimFileBtn = new QPushButton("Sim File&name");
  // simFilenameLabel = new QLabel(IomPackage::getDumpFilename());
//  initAndSingleCycleSimBtn-> setToolTip("Inits the asic, adds one delay, resets the chip\nand adds one burst-readout cycle to simulation file");
  simBtnsLayout->addWidget(clearSimDumpFileBtn);
//  simBtnsLayout->addWidget(addSimDelayBtn);
//  simBtnsLayout->addWidget(initAndSingleCycleSimBtn);
//  simBtnsLayout->addWidget(setSimFileBtn);
//  simBtnsLayout->addWidget(simFilenameLabel);

  connect(simModeBtn, SIGNAL(clicked(bool)), this, SLOT(setSimMode(bool)));
  connect(clearSimDumpFileBtn, SIGNAL(clicked()), this, SLOT(clearSimDumpFile()));
//  connect(addSimDelayBtn, SIGNAL(clicked()), this, SLOT(addDelayToSimFile()));
//  connect(initAndSingleCycleSimBtn, SIGNAL(clicked()), this, SLOT(sendInitAndSingleCycletoSim()));
//  connect(this, SIGNAL(signalSimModeEnabled(bool)), simBtnsWidget, SLOT(setEnabled(bool)));
//  connect(this, SIGNAL(signalSimModeEnabled(bool)), simModeBtn, SLOT(setChecked(bool)));
//  //connect(this, SIGNAL(signalSimModeEnabled(bool)), seqGUI, SLOT(slotEnableSimMode(bool)));
//  connect(setSimFileBtn, SIGNAL(clicked()), this, SLOT(setSimFilename()));

//  simModeBtn->setChecked(IomPackage::isDumpToFileMode());
//  simModeBtn->setChecked(true);
//  setSimMode(IomPackage::isDumpToFileMode());
//  simBtnsWidget->setEnabled(IomPackage::isDumpToFileMode());
  m_simToolBar->setVisible(true);
  QHBoxLayout *simFnLabelLayout = new QHBoxLayout();
  QLabel *simFilenameLabel = new QLabel("Sim File (used if Simulation Mode is enabled): ../../digital/sim/mixed/cmdsFromSoftware");
  simFnLabelLayout->addWidget(simFilenameLabel);
  simFnLabelLayout->addStretch();
//  simFnLabelLayout->setEnabled(true);
  simBtnsLayout->addLayout(simFnLabelLayout);
  
}

SuS::QSignalHelper::QSignalHelper( QXiderConfGui* _qXiderConfGui )
{
  if ( _qXiderConfGui ) {
    connect(this, SIGNAL(showGuiSignal()), _qXiderConfGui, SLOT(show()));
  } else {
    std::cout << "QSignalHelper: QXiderConfGui not initialized!" << std::endl;
  }
}



SuS::QSignalHelper::~QSignalHelper()
{
}

void SuS::QSignalHelper::showGui()
{
  emit showGuiSignal();
  std::cout << "Showing GUI." << std::endl;
}


// these funtions can be called from the python environment
// extern "C" is needed so that the compiler does not mangle the function names
// without extern "C", the compiler addes info about the arguments, which is necessary because
// overloading is possible (C++). no overloading is possible in C
extern "C" {

  // global variables for python environment
  QSignalHelper* g_qSignalHelper = nullptr;
  QXiderConfGui* g_qXiderConfGui = nullptr;
  QApplication* g_qapp = nullptr;
  QConfigReg* g_qConfigReg = nullptr;
  SuS::SequencerQt* g_qSequencer;
  std::vector<int>* g_bitString = nullptr;
  bool g_guiStarted = false;
  void (*g_programJtagCallBack)(void);
  void (*g_runASICCallBack)(void);
  void (*g_stopASICCallBack)(void);
  void (*g_clearSimDumpFileCallBack)(void);
  
  void QXiderConf_init( const char* _regFname, const char* _seqFname, void (*progJtag)(void)=nullptr, void (*runASIC)(void)=nullptr, void (*stopASIC)(void) = nullptr, void (*clearSimDumpFile)(void) = nullptr)
  {
    if ( g_qConfigReg ) {
      delete g_qConfigReg;
    }
    g_qConfigReg = new QConfigReg( _regFname );
    g_qSequencer = new SequencerQt( _seqFname );
    g_programJtagCallBack = progJtag;
    g_runASICCallBack = runASIC;
    g_stopASICCallBack = stopASIC;
    g_clearSimDumpFileCallBack = clearSimDumpFile;
  }

  void QXiderConfGui_show() 
  {
    if ( !g_qapp ) {
      int argc = 0;
      char argv[] = "";
      g_qapp = new QApplication( argc, (char**) &argv ); // QApplication needed before any windows can be shown
      QApplication::setQuitOnLastWindowClosed( false ); 
      // the prev line prevents exec to return. otherwise the thread for the gui would die and the
      // gui cannot be shown anymore
    }
    if ( !g_qXiderConfGui ) {
      g_qXiderConfGui = new QXiderConfGui( g_qConfigReg, g_qSequencer, g_programJtagCallBack, g_runASICCallBack, g_stopASICCallBack, g_clearSimDumpFileCallBack );
      g_qSignalHelper = new QSignalHelper( g_qXiderConfGui );
    }
    std::cout << "QXiderConfGui initialized." << std::endl;
    g_qXiderConfGui->show();
    g_qapp->exec();
  }

  void QXiderConfGui_show_thread() 
  {
    if ( !g_guiStarted ) {
      std::thread t(QXiderConfGui_show);
      t.detach(); // needed in order to keep python running
      g_guiStarted = true; 
    } else {
      g_qSignalHelper->showGui();
    }
  }

  // updates the g_bitString with the selected register bits and returns its length
  // attention, the def argument does not work with python
  // _seqTrackNum only needed when _regNmae = "SequencerTrack"
  int updateBitString(const char* _regName = "Global Control Register", int _seqTrackNum = 0) 
  {
    //std::cout << "getBitString() from register \"" << _regName << "\"" << std::endl;
    std::string tmpstr(_regName );
    if (!g_bitString) {
      g_bitString = new std::vector<int>;
    } else {
      g_bitString->clear();
    }
    if ( tmpstr.compare( 0, 14, "SequencerTrack" ) == 0 ) {
      std::vector<bool> seqBits;
      std::cout << "Updateing bitString with SequencerTrack[" << _seqTrackNum << "] bits." << std::endl;
      g_qSequencer->getTrackDdynProgBits( SuS::Sequencer::TrackNum(_seqTrackNum), seqBits);

      for (auto b : seqBits) g_bitString->push_back( (int)b );
    } else if ( tmpstr.compare( 0, 16, "SequencerHoldGen" ) == 0 ) {
      std::vector<bool> seqBits;
      std::cout << "Updateing bitString with SequencerHoldGen" << std::endl;
      g_qSequencer->getHoldProgBits( seqBits) ;
      for (auto b : seqBits) g_bitString->push_back( (int)b );
    }
    else if ( g_qConfigReg ) {
      QVector<bool> tmp = g_qConfigReg->printContent( _regName, 0);
      for (auto b : tmp) g_bitString->push_back( (int)b );
    } else {
      std::cout << "Error could not open qConfReg." << std::endl;
    }
    return g_bitString->size();
  }

  // returns the current g_bitString
  // bool* getCurrentBitString() { // does not work because std::vector<bool>::data() is not
  // implemented for bools -> using ints instread...
  int* getCurrentBitString() 
  {
    if ( !g_bitString ) {
      std::cout << "g_bitString not initialized. Initializing from Global Control Register.";
      updateBitString( "Global Control Register" );
    }
    return g_bitString->data();
  }

  void printCurrentBitString() 
  {
    std::cout << "g_bitString->size() = " << g_bitString->size() << std::endl;
    for (unsigned int i=0; i<g_bitString->size(); ++i) std::cout << g_bitString->at(i) << " ";
    std::cout << std::endl;
  }

  uint32_t getRegAddress( const char* _regName, int _seqTrackNum ) 
  {
    std::string tmpstr(_regName );
    if ( tmpstr.compare( 0, 14, "SequencerTrack" ) == 0 ) {
      return g_qSequencer->getTrackJtagSubAddress( Sequencer::TrackNum(_seqTrackNum) );
    } else if ( tmpstr.compare( 0, 16, "SequencerHoldGen" ) == 0 ) {
      return 18;
    } else {
      return g_qConfigReg->getRegAddress( _regName );
    }
  } 

  void setRegisterSignal( const char* _regName, const char* _signalName, uint32_t _value)
  {
    g_qConfigReg->setSignalValue( _regName, "0", _signalName, _value);
  }


  uint32_t getRegisterSignal( const char* _regName, const char* _signalName)
  {
    return g_qConfigReg->getSignalValue( _regName, "0", _signalName );
  }

  bool getSimMode( )
  {
    return g_qXiderConfGui->getSimMode( ); 
  }



  void loadRegisterConfigFile( const char* _fname )
  {
    if ( g_qConfigReg ) {
      g_qConfigReg->initFromFile( _fname );
    } else {
      std::cout << "ERROR: loadRegisterConfigFile(): qConfReg not initialized." << std::endl;
    }
  }

  void saveRegisterConfigFile( const char* _fname )
  {
    if ( g_qConfigReg ) {
      g_qConfigReg->saveToFile( _fname );
    } else {
      std::cout << "ERROR: saveRegisterConfigFile(): qConfReg not initialized." << std::endl;
    }
  }

  void loadSequencerFile( const char* _fname )
  {
    if ( g_qSequencer ) {
      g_qSequencer->loadFile( _fname );
    } else {
      std::cout << "ERROR: qSeqeucner not initialized." << std::endl;
    }
  }

  void saveSequencerFile( const char* _fname )
  {
    if ( g_qSequencer ) {
      g_qSequencer->saveToFile( _fname );
    } else {
      std::cout << "ERROR: qSeqeucner not initialized." << std::endl;
    }
  }
}
