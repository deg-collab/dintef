#ifndef QXIDERCONF_H
#define QXIDERCONF_H

#include <QMainWindow>

namespace SuS
{

  class QConfigReg;
  class QConfigRegWidget;
  class SequencerQt;
  class Sequencer_GUI;

  // gui class showing a register tab and a sequencer tab.
  class QXiderConfGui : public QMainWindow
  {
    Q_OBJECT

    public:
  
      QXiderConfGui( QConfigReg* _qConfigReg = nullptr, SequencerQt* _qSequencer = nullptr, void (*_programJtagCallBack)() = nullptr, void (*_runASICCallBack)() = nullptr , void (*_stopASICCallBack)() = nullptr , void (*_clearSimDumpFileCallBack)() = nullptr);
      ~QXiderConfGui();

    private:
      
      QConfigReg* qConfigReg;
      QConfigRegWidget* qConfigRegWidget;
      Sequencer_GUI* sequencerGUI;
      SequencerQt* m_qSequencer;
      QToolBar*    m_simToolBar;

      bool m_simMode;
      
      void (*m_programJtagCallBack)();
      void (*m_runASICCallBack)();
      void (*m_stopASICCallBack)();
      void (*m_clearSimDumpFileCallBack)();
      
      void initSimToolbar();


    public slots:

      void setSimMode( bool val );
      bool getSimMode( );
      void programJtag();      
      void runASIC();
      void stopASIC();
      void clearSimDumpFile();
      void setCycleLength(int _cycleLength);

  };

  // helper class to send singals to a running thread. one cannot just call methods of an object
  // created in another thread in qt. one has to use a signal.
  class QSignalHelper : public QObject
  {
    Q_OBJECT

    public:

      QSignalHelper( QXiderConfGui* _qXiderConfGui );
      ~QSignalHelper();

      void showGui();

    signals:
       
      void showGuiSignal();
  };
}

#endif
