#ifndef SEQDRAWLABEL_HPP
#define SEQDRAWLABEL_HPP

#include <QWidget>
#include <QLabel>
#include <QMouseEvent>
#include <QEvent>

namespace SuS
{
  class SeqDrawLabel : public QLabel
  {
    Q_OBJECT

    public:
      explicit SeqDrawLabel(QWidget *parent = 0);

      void mouseMoveEvent(QMouseEvent *ev);
      void mousePressEvent(QMouseEvent *ev);

      int x,y;

    signals:
      void mouse_pressed(int type, int x, int y);
      void mouse_pos(int type, int x, int y);
  };
}

#endif
