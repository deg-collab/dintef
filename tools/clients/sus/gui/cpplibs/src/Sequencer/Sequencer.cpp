#include "Sequencer.h"
#include "SequencerTrack.h"
#include "utils.h"

#include <assert.h>
#include <sstream>
#include <iostream>
#include <fstream>

using namespace SuS;
using namespace std;

//bool Sequencer::debugMode = true;
bool Sequencer::debugMode = false;

Sequencer::HoldCntsRegister Sequencer::holdCnts;
int Sequencer::m_cycleLength;
bool Sequencer::holdEnabled;

//namespace{ SuS::logfile::subsystem_registrator log_id( "Sequencer" ); }

#define log_id() "Sequencer"

#define SuS_LOG_STREAM(type,id,output)\
          std::cout << "++++"#type": Sequencer: " << output << std::endl;

std::map<std::string, Sequencer::SeqParam> Sequencer::paramNamesMap;

//int Sequencer::c_numOfTracks      = 9;
int Sequencer::c_numOfTracks      = 12;
int Sequencer::c_holdCntWidth     = 14;
int Sequencer::c_holdCntsRegDepth = 5;
int Sequencer::c_numOfParams      = 1;

Sequencer::Sequencer(std::string filename, bool initTracks) :
   tracks(c_numOfTracks),
   configGood(false),
   m_progMode( Direct )
{
  if (paramNamesMap.empty()) {
    for(int i=0; i<c_numOfParams; i++){
      paramNamesMap[getParameterName((SeqParam)i)] = (SeqParam)i;
      //SuS_LOG_STREAM(info, log_id(), getParameterName((SeqParam)i) << " = " << i);
    }
  }

  if (initTracks) {
    for (unsigned int i=0; i<tracks.size(); ++i) {
      tracks[i] = new SequencerTrack;
      tracks[i]->setTrackNum(i);
//       SuS_LOG_STREAM(info, log_id(), "TrackNum = " << tracks[i]->trackNum <<
//           ", TrackName = " << Sequencer::c_trackNames[tracks[i]->trackNum]);
    }
  }

  setJtagSubAddressAndName(Res0,            6, "Res0");
  setJtagSubAddressAndName(Res1,            7, "Res1");
  setJtagSubAddressAndName(EnCP0,           8, "EnCP0");
  setJtagSubAddressAndName(EnCP1,           9, "EnCP1");
  setJtagSubAddressAndName(Sw0to1,          10, "Sw0to1");
  setJtagSubAddressAndName(ClkCP,           11, "ClkCP");
  setJtagSubAddressAndName(InjB0,           12, "InjB0");
  setJtagSubAddressAndName(InjB1,           13, "InjB1");
  setJtagSubAddressAndName(InpShieldTest,   14, "InpShieldTest");
//  setJtagSubAddressAndName(ResBigCSA,   14, "ResBigCSA");
  setJtagSubAddressAndName(EnIN,            15, "EnIN");
  setJtagSubAddressAndName(Ch0Coarse,       16, "Ch0Coarse");
  setJtagSubAddressAndName(Ch0Fine,         17, "Ch0Fine");

  if ( initTracks ) {
    loadFile(filename);
  }
}


Sequencer::~Sequencer()
{
  for (unsigned int i=0; i<tracks.size(); ++i) {
    delete tracks[i];
  }
}


bool Sequencer::compareTrackContent(const std::vector<bool> &data_vec, TrackNum track)
{
  return tracks[track]->compareContent(data_vec,compareErrors);
}


bool Sequencer::compareHoldContent(const std::vector<bool> &data_vec)
{
  std::vector<bool> bits;
  getHoldProgBits(bits);

  if(bits.size() > data_vec.size()){
    compareErrors.push_back("Sequencer: Data Vector too short:" + std::to_string(data_vec.size())+ "/" + std::to_string(bits.size()));
    return false;
  }

  for(uint i=0; i<bits.size(); i++){
     if(bits[i]!=data_vec[i]){
       std::stringstream ss;
       ss << "Sequencer Hold bits error at " << i << std::endl;
       ss << "Sequencer Track rbda:" << utils::boolVecToStdStr(data_vec) << std::endl;
       ss << "Sequencer Track bits:" << utils::boolVecToStdStr(bits)     << std::endl;
       compareErrors.push_back(ss.str());
       return false;
     }
  }

  return true;
}


void Sequencer::getTrackDdynProgBits(TrackNum n, std::vector<bool>& bits) const
{
  tracks[n]->getTrackDdynProgBits(bits);
}


void Sequencer::getStatVals(std::vector<bool>& bits) const
{
  bits.clear();
  bits.resize(c_numOfTracks);
  for (int i=0; i<c_numOfTracks; ++i) {
    bits[i] = tracks[i]->getStatVal();
  }
}

void Sequencer::appendInvHolds(std::vector<bool>& bits)
{
  for (int i=0; i<c_numOfTracks; ++i) {
    bits.push_back(tracks[i]->getInvHold());
  }
}


void Sequencer::getHoldProgBits(std::vector<bool>& bits) const
{
  int totalBits = (c_holdCntWidth+1)*c_holdCntsRegDepth;
  bits.clear();
  bits.resize(totalBits);
  assert((int)holdCnts.size()==c_holdCntsRegDepth);

  if (holdEnabled) {
    for (int i=0; i<(int)holdCnts.size(); ++i) {
      uint16_t holdLength = holdCnts[holdCnts.size()-i-1].length - 1;
      /*
      if (holdLength<2) {
        SuS_LOG_STREAM(warning, log_id(), "holdLength = " << holdLength << " but must be > 1, set to 2.");
        holdLength = 2;
      }
      */
      for (int j=0; j<c_holdCntWidth; ++j) {
        if (holdLength & uint16_t(1 << j)) {
          bits[i*(c_holdCntWidth+1)+j] = true;
        }
      }
      // set the hold flag
      bits[c_holdCntWidth + i*(c_holdCntWidth+1)] = holdCnts[holdCnts.size()-i-1].hold;
    }
  }
}


bool Sequencer::compileAndCheckAllTracks()
{
  bool success = true;
  for (int i=0; i<c_numOfTracks; ++i) {
    success &= tracks[i]->compileAndCheck();
  }
  return success;
}

bool Sequencer::compileAllTracks()
{
  if (debugMode) {
    SuS_LOG_STREAM(debug, log_id(), "Compiling all sequencer tracks.");
  }
  bool success = compileAndCheckAllTracks();

  //if(success){
  //  integrationLength = getIntegrationTime();
  //  resetLength = getResetLength();
  //  rampLength = getRampLength();
  //}
  return success;
}


bool Sequencer::saveToFile()
{
  return saveToFile(filename);
}


bool Sequencer::saveToFile(const std::string & _filename)
{
  std::ofstream ofs (_filename, std::ofstream::out);

  ofs << "<!DOCTYPE Sequencer>" << std::endl;

  ofs << "<Sequencer ";
  if ( m_progMode == Compiler ) ofs << "mode=\"compiler\"";
  ofs << " cycleLength=\"" << m_cycleLength << "\"";
//  ofs << " holdGenEnabled=\"" << holdEnabled << "\"";
//  ofs << " singleSHCapMode=\"" << singleSHCapMode << "\"";
//  ofs << " progMode=\"" << (int)mode << "\"";
//  ofs << ">" << std::endl;

  writeXMLCycleParameters(ofs);
  for(int i=0; i<c_numOfTracks; i++){
    writeXMLSequencerTrack(ofs,(Sequencer::TrackNum)i);
  }

  writeXMLHoldCounts(ofs);

  ofs << "</Sequencer>" << std::endl;

  ofs.close();

  utils::CoutColorKeeper keeper(utils::STDGREEN);
  cout << "Save Sequencer to File: " << _filename << endl;
  return true;
}


void Sequencer::writeXMLCycleParameters(std::ofstream &ofs)
{
  ofs << "<cycleParameters ";
  //ofs << "integrationLength=\""     << integrationLength     << "\" ";
  ofs << std::endl;
}


void Sequencer::writeXMLSequencerTrack(std::ofstream &ofs, TrackNum trackNum)
{
  SequencerTrack& seqTrack = *getTrack(trackNum);
  std::string trackName = trackNumToName(trackNum);

  ofs << "<SequencerTrack ";
  ofs << "signalName=\"" << trackName             << "\" ";
  ofs << "invHold=\""    << seqTrack.getInvHold() << "\" ";
  ofs << "statVal=\""    << seqTrack.getStatVal() << "\">";
  ofs << "clockMode=\""  << seqTrack.isClockMode() << "\">";
  ofs << std::endl;

  for(uint i=0; i<seqTrack.getPhases().size(); i++){
    writeXMLPhase(ofs,seqTrack.getPhases().at(i));
  }

  ofs << "</SequencerTrack>" << std::endl;
}


void Sequencer::writeXMLPhase(std::ofstream &ofs, const SequencePhase& phase)
{
  ofs << "<Phase ";
  ofs << "length=\"" << phase.clockCycles << "\" ";
  ofs << "type=\""   << phase.high        << "\"/>";
  ofs << std::endl;
}


void Sequencer::writeXMLHoldCounts(std::ofstream &ofs)
{
  ofs << "<HoldGenerator enabled=\"" << holdEnabled << "\">" << std::endl;

  for(uint i=0; i<holdCnts.size(); i++){
    writeXMLHoldRegister(ofs,holdCnts.at(i));
  }

  ofs << "</HoldGenerator>" << std::endl;
}


void Sequencer::writeXMLHoldRegister(std::ofstream &ofs, const HoldCntsRegisterEntry & regEntry)
{
  ofs << "<HoldCntsRegEntry ";
  ofs << "length=\"" << regEntry.length << "\" ";
  ofs << "hold=\""   << regEntry.hold   << "\"/>";
  ofs << std::endl;
}


bool Sequencer::loadFile(std::string _filename)
{
  SuS_LOG_STREAM( info, log_id(), "Loading file " << _filename );
  holdCnts.clear();

  fieldNotFound = false;
  bool configOk = true;
  bool manualMode = false;
  int currHoldPhase = 0;

  std::ifstream infile( _filename,std::ifstream::in );
  if ( !infile.is_open() ) {
    filename = "";
    configGood = false;
    return false;
  }

  setFileName(_filename);

  TrackNum currTrack;
  SequencerTrack* t = nullptr;
  std::string line;
  while (std::getline(infile, line))
  {
    if(line.length()>0)
    {

      if(line.find("<Sequencer ") != std::string::npos)
      {
        int length = getValueFromXMLLine(line,"cycleLength");
        setCycleLength(length);
        bool enabled = (getValueFromXMLLine(line,"holdGenEnabled")!=0);
        setHoldEnabled(enabled);
      } else if ( m_progMode == Compiler && line.find("<cycleParameters") != std::string::npos ) {
        //resetLength           = getValueFromXMLLine(line,"resetLength");
      //} else if ( ( m_progMode == Compiler || manualMode) && line.find("<SequencerTrack") != std::string::npos ) {
      } else if ( ( m_progMode == Direct ) && line.find("<SequencerTrack") != std::string::npos ) {
        std::string trackName = getStringFromXMLLine(line,"signalName");
        bool invHold = getValueFromXMLLine(line,"invHold") != 0;
        bool statVal = getValueFromXMLLine(line,"statVal") != 0;
        int readClockMode = getValueFromXMLLine(line,"clockMode");
        bool clockMode = readClockMode == -1 ? false : (bool)readClockMode; // default (read -1) is false

        int x = trackNameToNum(trackName);

        if (x>-1) {
          currTrack = (TrackNum)x;
#ifdef DEBUG
          std::cout << "Track " << trackName << " found - number " << x << "!" << std::endl;
#endif
        } else {
          std::cout << "Track " << trackName << " unknown!" << std::endl;
          break;
        }

        t = tracks[currTrack];
        t->setStatVal(statVal);
        t->setInvHold(invHold);
        t->setClockMode(clockMode);
        t->clearPhases();

      } else if ( ( m_progMode == Direct ) && line.find("<Phase")!=std::string::npos ) {
      //} else if ( ( m_progMode != Compiler || manualMode) && line.find("<Phase")!=std::string::npos ) {
        int phaseLength = getValueFromXMLLine(line,"length");
        bool phaseType = getValueFromXMLLine(line,"type") != 0;
        t->addPhase(phaseType, phaseLength);
#ifdef DEBUG
        std::cout << "Track " << currTrack << " added Phase: length " << phaseLength << " type " << phaseType << std::endl;
#endif
      } else if ( line.find("<HoldGenerator")!=std::string::npos ) {
        holdEnabled = getValueFromXMLLine(line,"enabled") != 0;
      } else if ( line.find("<HoldCntsRegEntry")!=std::string::npos ) {
        int holdLength = getValueFromXMLLine(line,"length");
        bool enabled = getValueFromXMLLine(line,"hold") != 0;
        holdCnts.push_back( HoldCntsRegisterEntry(holdLength, enabled));
        currHoldPhase++;
      }
    }
  }

  infile.close();

  if ( m_progMode == Compiler ) {
    generateSignals();
  } else { // Direct mode
    compileAndCheckAllTracks();
  }

  if (!checkHoldCnts(holdCnts)) {
    configOk = false;
  }

  configGood = configOk;
  if(configGood) {
    utils::CoutColorKeeper keeper(utils::STDGREEN);
    std::cout << "++++ Sequencer initialized from file " << _filename <<  "!" << std::endl;
  }
  else  {
    utils::CoutColorKeeper keeper(utils::STDRED);
    std::cout << "++++ ERROR: Sequencer could not be read correctly!" << std::endl;
  }

  return configOk;
}


Sequencer::ProgMode Sequencer::getProgModeFromXMLLine(const std::string &line)
{
  SuS_LOG_STREAM(info, log_id(), "TODO: getProgMode()");
  return Direct;
//  int pos = line.find("progMode");
//  if(pos < 0){
//    std::cerr << "Sequencer Load File: ProgMode not found set to manual mode " << std::endl;
//    return MANUAL;
//  }
//
//  pos += 8;
//  std::string linePart = line.substr(pos);
//  pos = linePart.find("\"");
//  linePart = linePart.substr(0,pos);
//
//  //convert string to int;
//  std::istringstream iss(linePart);
//  int a;
//  iss >> a; // error
//
//#ifdef DEBUG
//  std::cout << "getProgModeFromXMLLine: " << line << " is " << a << std::endl;
//#endif
//
//  return (Sequencer::ProgMode)a;
}


int Sequencer::getValueFromXMLLine(const std::string &line, std::string valueName)
{
  int pos = line.find(valueName);
  if(pos < 0){
    utils::CoutColorKeeper keeper(utils::STDBROWN);
    std::cerr << "WARNING: getStringFromXMLFile: signalName " << valueName << " not found in " << line << std::endl;
    std::cerr << valueName << " not found." << std::endl;
    keeper.change(utils::STDGREEN);
    std::cerr << "Will store standard value to file" << std::endl;
    fieldNotFound = true;
    return -1;
  }

  pos += valueName.length() + 2;

  std::string linePart = line.substr(pos);

  pos = linePart.find("\"");

  linePart = linePart.substr(0,pos);

  //convert string to int;
  std::istringstream iss(linePart);
  int a;
  iss >> a; // error

#ifdef DEBUG
  std::cout << "getValueFromXMLFile: " << valueName << " is " << a << std::endl;
#endif

  return a;
}

std::string Sequencer::getStringFromXMLLine(const std::string &line, std::string valueName)
{
  int pos = line.find(valueName);
  if(pos < 0){
    std::cerr << "getStringFromXMLFile: signalName " << valueName << " not found in " << line << std::endl;
    return "0";
  }

  pos += valueName.length() + 2;

  std::string linePart = line.substr(pos);

  pos = linePart.find("\"");

  linePart = linePart.substr(0,pos);

#ifdef DEBUG
  std::cout << "getStringFromXMLFile: " << valueName << " is " << linePart << std::endl;
#endif

  return linePart;
}


/*

bool Sequencer::saveToFile(std::string _filename)
{
  SuS_LOG_STREAM(info, log_id(), "Saving to file " << _filename.toStdString());

  QDomDocument doc("Sequencer");
  QDomElement rootEl = doc.createElement("Sequencer");
  doc.appendChild(rootEl);
  rootEl.setAttribute("cycleLength", QString::number(Sequencer::cycleLength));
  rootEl.setAttribute("holdGenEnabled", QString::number(Sequencer::getHoldEnabled()));

  if (signalsCompilerMode) {
    rootEl.setAttribute("mode", "signalCompiler");
    QDomElement cycleParametersEl = doc.createElement("cycleParameters");
    cycleParametersEl.setAttribute("resetLength", QString::number(resetLength));
    cycleParametersEl.setAttribute("resetIntegOffset", QString::number(resetIntegOffset));
    cycleParametersEl.setAttribute("resetHoldLength", QString::number(resetHoldLength));
    cycleParametersEl.setAttribute("integrationLength", QString::number(integrationLength));
    cycleParametersEl.setAttribute("flattopLength", QString::number(flattopLength));
    cycleParametersEl.setAttribute("flattopHoldLength", QString::number(flattopHoldLength));
    cycleParametersEl.setAttribute("rampLength", QString::number(rampLength));
    rootEl.appendChild(cycleParametersEl);
  }
  for (unsigned int t=0; t<tracks.size(); ++t) {
    QDomElement trackEl = doc.createElement("SequencerTrack");
    SequencerTrack& track = *tracks[t];
    //trackEl.setAttribute("statVal", QString::number(track.getStatVal()));
    trackEl.setAttribute("signalName", c_trackNames[t]);
    trackEl.setAttribute("statVal", QString::number((int)track.getStatVal()));
    trackEl.setAttribute("invHold", QString::number((int)track.getInvHold()));
    for (unsigned int i=0; i<track.phases.size(); ++i) {
      QDomElement phaseEl = doc.createElement("Phase");
      SequencerTrack::SequencePhase& phase = track.phases[i];
      phaseEl.setAttribute("type", QString::number((int)phase.high));
      phaseEl.setAttribute("length", QString::number(phase.clockCycles));
      trackEl.appendChild(phaseEl);
    }
    rootEl.appendChild(trackEl);
  }

  QDomElement holdGenEl = doc.createElement("HoldGenerator");
  holdGenEl.setAttribute("enabled", QString::number(holdEnabled));
  rootEl.appendChild(holdGenEl);
  for (unsigned int i=0; i<holdCnts.size(); ++i) {
    QDomElement holdCntsEl = doc.createElement("HoldCntsRegEntry");
    HoldCntsRegisterEntry& h = holdCnts[i];
    holdCntsEl.setAttribute("hold", QString::number(h.hold));
    holdCntsEl.setAttribute("length", QString::number(h.length));
    holdGenEl.appendChild(holdCntsEl);
  }

  QFile file(_filename);
  if (!file.open( QIODevice::WriteOnly )) {
    SuS_LOG_STREAM(error, log_id(), "Saving to file failed");
    return false;
  }

  QTextStream out(&file);
  doc.save(out, 0);
  file.close();
  setFileName(_filename);
  return true;
}


bool Sequencer::loadFile(std::string _filename)
{
  QDomDocument doc;

  QFile file(_filename);
  if (!file.open( QIODevice::ReadOnly ) ) {
    SuS_LOG_STREAM(error, log_id(), "Error opening file.");
    return false;
  }
  if (!doc.setContent(&file)) {
    SuS_LOG_STREAM(error, log_id(), "Failed to parse XML file");
  }
  setFileName(_filename);

  QDomElement rootEl = doc.documentElement();
  setCycleLength(rootEl.attribute("cycleLength").toInt());
  setHoldEnabled(rootEl.attribute("holdGenEnabled").toInt());

  bool retVal = true;

  QDomElement holdGenEl = doc.nextSiblingElement("HoldGenerator");
  QDomNodeList holdCntsNodeList = rootEl.elementsByTagName("HoldCntsRegEntry");
  holdCnts.clear();
  for (int i=0; i<holdCntsNodeList.size(); ++i) {
    QDomElement holdCntEl = holdCntsNodeList.at(i).toElement();
    uint16_t holdLength =  holdCntEl.attribute("length").toInt();
    holdCnts.push_back(HoldCntsRegisterEntry(holdLength, holdCntEl.attribute("hold").toInt()));
  }
  if (!checkHoldCnts(holdCnts)) {
    retVal = false;
  }

  if (rootEl.hasAttribute("mode")) {          // USE SIGNAL COMPILER
    signalsCompilerMode = true;
    QDomElement cycleParametersEl = rootEl.firstChildElement("cycleParameters");
    resetLength       = cycleParametersEl.attribute("resetLength").toInt();
    resetIntegOffset  = cycleParametersEl.attribute("resetIntegOffset").toInt();
    resetHoldLength   = cycleParametersEl.attribute("resetHoldLength").toInt();
    integrationLength = cycleParametersEl.attribute("integrationLength").toInt();
    flattopLength     = cycleParametersEl.attribute("flattopLength").toInt();
    flattopHoldLength = cycleParametersEl.attribute("flattopHoldLength").toInt();
    rampLength        = cycleParametersEl.attribute("rampLength").toInt();
    //rampIntegOffset   = cycleParametersEl.attribute("rampIntegOffset").toInt();
    generateSignals();
  } else {                                    // USE TRACK COMPILER (PHASES)
    signalsCompilerMode = false;
    QDomNodeList trackNodeList = rootEl.elementsByTagName("SequencerTrack");
    for (int i=0; i<trackNodeList.size(); ++i) {
      QDomElement trackEl = trackNodeList.at(i).toElement();
      QDomNodeList phaseNodeList = trackEl.elementsByTagName("Phase");
      int x = trackNameToNum(trackEl.attribute("signalName"));
      TrackNum n;
      if (x>-1) {
        n = (TrackNum)x;
      }else{
        break;
      }
      SequencerTrack& t = *tracks[n];
      t.setStatVal(trackEl.attribute("statVal").toInt());
      t.setInvHold(trackEl.attribute("invHold").toInt());
      t.clearPhases();
      for (int j=0; j<phaseNodeList.size(); ++j) {
        QDomElement phaseEl = phaseNodeList.at(j).toElement();
        bool type = (bool)phaseEl.attribute("type").toInt();
        int length = phaseEl.attribute("length").toInt();
        t.addPhase(type, length);
      }
    }

    //SuS_LOG_STREAM(info, log_id(), "New holdCnts set.");

    // compilation of the tracks needs to be done after the holdCnts are set because
    // the hold positions need to be known for compilation because of a bug in the chip...
    //

//  for (int i=0; i<c_numOfTracks; ++i) {
//    SequencerTrack& t = *tracks[i];
//    if (!t.compileAndCheck()) {
//      retVal = false;
//      SuS_LOG_STREAM(error, log_id(), "Error while loading bits for track " << c_trackNames[i]);
//    }
//  }


    compileAllTracks(); // removed because it calls a virtual function
  }

  setProgMode(getProgMode());

  file.close();
  emit contentChanged();
  emit cycleLengthChanged(cycleLength);

  configGood = retVal;
  return retVal;
}
*/


void Sequencer::setCycleLength(int _cycleLength)
{
  {
    utils::CoutColorKeeper keeper(utils::STDGRAY);
    SuS_LOG_STREAM(info,log_id(),"Setting cycleLength to " << _cycleLength);
  }
  m_cycleLength = _cycleLength;
  generateSignals();
}


int Sequencer::trackNameToNum(std::string name)
{
  for (int i=0; i<c_numOfTracks; ++i) {
    if (name == getTrack( (TrackNum)i )->name) return i;
  }
  SuS_LOG_STREAM(error, log_id(), "Trackname unknown. Check Sequencer File!");
  return -1;
}


void Sequencer::setFileName(std::string _filename)
{
  filename = _filename;
}

int Sequencer::getHoldCnts()
{
  if (!checkHoldCnts(holdCnts)) {
    return -1;
  }

  int holdCntsLength = 0;
  for (unsigned int i=0; i<holdCnts.size(); ++i) {
    if (holdCnts[i].hold) {
      holdCntsLength += holdCnts[i].length;
    }
  }
  return holdCntsLength;
}


int Sequencer::getRealCycleLength()
{
  int realCycleLength = m_cycleLength;
  if (getHoldEnabled()){
    realCycleLength += getHoldCnts();
  }

#ifdef DEBUG
  SuS_LOG_STREAM(debug, log_id(), "Real Cycle Length = " << realCycleLength);
#endif

  return realCycleLength;
}


bool Sequencer::setHoldCnts(HoldCntsRegister _holdCnts)
{
  if(!checkHoldCnts(_holdCnts)) {
    return false;
  }
  //reorderPartSeqsForHold();
  holdCnts = _holdCnts;

  return true;
}


bool Sequencer::correctNumberOfHolds(HoldCntsRegister &_holdCnts)
{
  int smallerCnt = 0;
  while((int)_holdCnts.size() < c_holdCntsRegDepth && smallerCnt<=c_holdCntsRegDepth)
  {
    int idx = 0;
    for(auto & holdCnt : _holdCnts){
      if(holdCnt.length>4){
        holdCnt.length -= 2;
        _holdCnts.insert(_holdCnts.begin()+idx,HoldCntsRegisterEntry(2,holdCnt.hold));
        break;
      }
      idx++;
    }
    smallerCnt++;
  }

  uint32_t idx = 0;
  while((int)_holdCnts.size() > c_holdCntsRegDepth && idx < (_holdCnts.size()-1))
  {
    if(_holdCnts[idx].hold == _holdCnts[idx+1].hold){
      _holdCnts[idx].length += _holdCnts[idx+1].length;
      _holdCnts.erase(_holdCnts.begin()+idx+1);
    }
    idx++;
  }


  return (int)_holdCnts.size() == c_holdCntsRegDepth;
}


bool Sequencer::checkHoldCnts(HoldCntsRegister &_holdCnts)
{
  if ((int)_holdCnts.size() != c_holdCntsRegDepth) {
    if(!correctNumberOfHolds(_holdCnts)){
      return false;
    }
  }
  holdCntsInSyncWithCycleLength(_holdCnts); // just check...
  return true;
}


bool Sequencer::holdCntsInSyncWithCycleLength(const HoldCntsRegister & _holdCnts)
{
  int holdCntsCycleLength = 0;
  for (unsigned int i=0; i<_holdCnts.size(); ++i) {
    if (!_holdCnts[i].hold) {
      holdCntsCycleLength += _holdCnts[i].length;
    }
  }
  if ( (holdCntsCycleLength % m_cycleLength) != 0) {
    SuS_LOG_STREAM(warning, log_id(), "holdCnts not in sync with m_cycleLength");
    SuS_LOG_STREAM(warning, log_id(), "holdCntsCycleLength = " << holdCntsCycleLength);
    SuS_LOG_STREAM(warning, log_id(), "cycleLength = " << m_cycleLength);
    return false;
  }

  if(debugMode)
  {
    SuS_LOG_STREAM(info, log_id(), "holdCntsCyleLength / sequenceCycleLength is "
                                    << holdCntsCycleLength / m_cycleLength);
  }

  int lastNoHoldPhaseLength = 0;
  for (unsigned int i=_holdCnts.size(); i>0; i--) {
    if (!_holdCnts[i].hold) {
      lastNoHoldPhaseLength += _holdCnts[i-1].length;
    }else{
      break;
    }
  }

  if(lastNoHoldPhaseLength < 3){
    SuS_LOG_STREAM(error, log_id(),"Last Hold 0 Phase is too short. Must be larger than 2!");
  }

  return true;
}


void Sequencer::setProgMode(ProgMode _mode, bool generate)
{
  mode = _mode;
  if ( mode == Compiler )
    generateSignals();
}


void Sequencer::setZeroHolds()
{
  int remCycleLength = m_cycleLength;
  int meanCycleLength = remCycleLength/c_holdCntsRegDepth;

  holdCnts.clear();
  for (int i=0; i<(c_holdCntsRegDepth-1); ++i) {
    holdCnts.push_back(HoldCntsRegisterEntry(meanCycleLength, false));
    remCycleLength -= meanCycleLength;
  }
  holdCnts.push_back(HoldCntsRegisterEntry(remCycleLength, false));

  setTracksNotCompiled();

}


void Sequencer::findHoldPositions(int &firstPos, int& firstLength, int& secondPos, int& secondLength)
{
  int numPhases = holdCnts.size();
  int phase = 0;
  firstLength  = 0;
  secondLength = 0;
  firstPos     = 0;
  secondPos    = 0;

  if(holdEnabled == 0){
    firstPos     = m_cycleLength+1;
    firstLength  = 0;
    secondPos    = m_cycleLength+1;
    secondLength = 0;
    return;
  }

  while(phase < numPhases)
  {
    if(!holdCnts.at(phase).hold){
      firstPos += holdCnts.at(phase).length;
    }else{
      secondPos += firstPos;
      break;
    }
    phase++;
  }

  while(phase < numPhases)
  {
    if(holdCnts.at(phase).hold){
      firstLength += holdCnts.at(phase).length;
    }else
    {
      break;
    }
    secondPos++;
    phase++;
  }

  while(phase < numPhases)
  {
    if(!holdCnts.at(phase).hold){
      secondPos += holdCnts.at(phase).length;
    }else{
      break;
    }
    phase++;
  }

  while(phase < numPhases)
  {
    if(holdCnts.at(phase).hold){
      secondLength += holdCnts.at(phase).length;
    }else
    {
      break;
    }
    phase++;
  }

  if(secondPos==0){
    secondPos = m_cycleLength+1;
    secondLength = 0;
  }

}


std::string Sequencer::getProgModeStr(Sequencer::ProgMode _progMode)
{
  switch ( _progMode ) {
    case Direct   : return "Direct";
    case Compiler : return "Compiler";
  }
  return "undefined";
}


Sequencer::ProgMode Sequencer::getProgMode()
{
  return m_progMode;
}


int Sequencer::getTotalClockCycles(TrackNum trackNum)
{
  int totalClockCycles = 0;

  const SequencerTrack::SequencePhases& seqPhases = getTrack(trackNum)->getPhases();

  for(uint i=0; i< seqPhases.size(); i++){
    totalClockCycles += seqPhases.at(i).clockCycles;
  }

  return totalClockCycles;
}


void Sequencer::disableSecondHold()
{
  int firstPos=0,secondPos=0,firstLength=0,secondLength=0;
  findHoldPositions(firstPos,firstLength,secondPos,secondLength);
  setHoldPositions(firstPos,firstLength);
}


void Sequencer::setTracksNotCompiled()
{
  for (unsigned int i=0; i<tracks.size(); ++i) {
    tracks[i]->isCompiled = false;
  }
}


bool Sequencer::setHoldPositions(int firstPos, int firstLength, int secondPos, int secondLength)
{
  setTracksNotCompiled();

  bool singleHold = (secondPos==0);

  int remCycleLength      = m_cycleLength;
  int maxFirstHoldPos     = m_cycleLength-3;
  int maxHoldLengthPerReg = 1 << (c_holdCntWidth - 1);
  int totalMaxHoldLength  = (singleHold) ? 3*maxHoldLengthPerReg : maxHoldLengthPerReg;


  if(firstLength <= 1){
    SuS_LOG_STREAM(info, log_id(), "MM3 Error: First hold length must not be smaller than 2. Set to 2" );
    firstLength=2;
  }
  if(secondLength <= 1){
    SuS_LOG_STREAM(info, log_id(), "MM3 Error: Second hold length must not be smaller than 2. Set to 2" );
    secondLength=2;
  }

  if((firstLength+secondLength) > 1000 && (m_cycleLength%2)==1){
    SuS_LOG_STREAM(warning, log_id(), "Using long holds, set cycle length to an even number to not run out of sync");
  }


  if (firstPos>maxFirstHoldPos) {
    SuS_LOG_STREAM(error, log_id(), "setHoldPosition(): Hold position cannot be > " << maxFirstHoldPos);
    return false;
  }
  if (firstLength>totalMaxHoldLength) {
    SuS_LOG_STREAM(error, log_id(), "setHoldPosition(): Hold length cannot be > " << totalMaxHoldLength);
    return false;
  }

  if( debugMode ){
    SuS_LOG_STREAM(info, log_id(),"Setting hold @ " << firstPos << " with length " << firstLength);
    if (!singleHold)
    {
      SuS_LOG_STREAM(info, log_id(),"Setting second hold @ " << secondPos << " with length " << secondLength);
    }
  }

  holdCnts.clear();
  if (firstPos>0) {
    holdCnts.push_back(HoldCntsRegisterEntry(firstPos, false));
    remCycleLength -= firstPos;
  }

  for (int i=0; i<3; ++i) {
    if (firstLength <= maxHoldLengthPerReg) {
      holdCnts.push_back(HoldCntsRegisterEntry(firstLength, true));
      break;
    } else {
      holdCnts.push_back(HoldCntsRegisterEntry(maxHoldLengthPerReg, true));
      firstLength -= maxHoldLengthPerReg;
    }
  }

  if (singleHold) {
    int extraEntriesWithZeroLength = c_holdCntsRegDepth-holdCnts.size()-1;
    holdCnts.push_back(HoldCntsRegisterEntry(remCycleLength-extraEntriesWithZeroLength, false));
    for (int i=0; i<extraEntriesWithZeroLength; ++i) {
      holdCnts.push_back(HoldCntsRegisterEntry(1, false));
    }
  } else {
    holdCnts.push_back(HoldCntsRegisterEntry(secondPos-firstPos-1, false));

    for (int i=0; i<3; ++i) {
      if (secondLength <= maxHoldLengthPerReg) {
        holdCnts.push_back(HoldCntsRegisterEntry(secondLength, true));
        break;
      } else {
        holdCnts.push_back(HoldCntsRegisterEntry(maxHoldLengthPerReg, true));
        secondLength -= maxHoldLengthPerReg;
      }
    }

    holdCnts.push_back(HoldCntsRegisterEntry(m_cycleLength-secondPos+1, false));
  }
  return true;
}


void Sequencer::printHoldCounts()
{
  for(uint i=0; i< holdCnts.size(); i++){
    SuS_LOG_STREAM(info, log_id(),"holdphase " << i << " Phase " << holdCnts[i].hold << " length " << holdCnts[i].length );
  }
}


void Sequencer::printTracks()
{
  for(int i=0; i< c_numOfTracks; i++){
    SuS_LOG_STREAM(info, log_id(),"+++++");
    for(uint p=0; p< getTrack((TrackNum)i)->getPhases().size(); p++){
      SuS_LOG_STREAM(info, log_id(),"TrackPhase " << i << " Phase " << getTrack((TrackNum)i)->getPhases().at(p).high << " length " << getTrack((TrackNum)i)->getPhases().at(p).clockCycles );
    }
  }
}


void Sequencer::generateSimPhases(int t)
{
  ////Fix Track 1
  //SequencerTrack::SequencePhases injSeqPhases;
  //injSeqPhases.push_back(SequencerTrack::SequencePhase(0,1));
  //injSeqPhases.push_back(SequencerTrack::SequencePhase(1,21));
  //injSeqPhases.push_back(SequencerTrack::SequencePhase(0,((m_cycleLength*7)-21-1)));
  //if(!getTrack(Sequencer::FCF_Flip)->setPhasesAndCompile(injSeqPhases)) {
  //  SuS_LOG_STREAM(error, log_id(), "Error while updating INJECT track.");
  //}


  ////Fix Track 2
  //SequencerTrack::SequencePhases resSeqPhases;
  //resSeqPhases.push_back(SequencerTrack::SequencePhase(0,2));
  //resSeqPhases.push_back(SequencerTrack::SequencePhase(1,21));
  //resSeqPhases.push_back(SequencerTrack::SequencePhase(0,((m_cycleLength*7)-21-2)));
  //if(!getTrack(Sequencer::FCF_Res_B)->setPhasesAndCompile(resSeqPhases)) {
  //  SuS_LOG_STREAM(error, log_id(), "Error while updating RES track.");
  //}

  ////Fix Track 3
  //SequencerTrack::SequencePhases swinSeqPhases;
  //swinSeqPhases.push_back(SequencerTrack::SequencePhase(0,4+t*6));
  //swinSeqPhases.push_back(SequencerTrack::SequencePhase(1,21));
  //swinSeqPhases.push_back(SequencerTrack::SequencePhase(0,((m_cycleLength*7)-21-4)-t*6));
  //if(!getTrack(Sequencer::FCF_SwIn)->setPhasesAndCompile(swinSeqPhases)) {
  //  SuS_LOG_STREAM(error, log_id(), "Error while updating SwIn track.");
  //}

  ////Dynamic Track
  //SequencerTrack::SequencePhases rmpSeqPhases;
  //rmpSeqPhases.push_back(SequencerTrack::SequencePhase(0,t*7));
  //rmpSeqPhases.push_back(SequencerTrack::SequencePhase(1,7));
  //rmpSeqPhases.push_back(SequencerTrack::SequencePhase(0,(m_cycleLength-t-1)*7));
  //if(!getTrack(Sequencer::ADC_RMP)->setPhasesAndCompile(rmpSeqPhases)) {
  //  SuS_LOG_STREAM(error, log_id(), "Error while updating RMP track.");
  //}

  //getTrack(Sequencer::ADC_RMP)->setStatVal(0);
  //getTrack(Sequencer::FCF_SwIn)->setStatVal(0);
  //getTrack(Sequencer::FCF_Res_B)->setStatVal(0);
  //getTrack(Sequencer::FCF_Flip)->setStatVal(0);
  //getTrack(Sequencer::ISubPulse)->setStatVal(0);

  //getTrack(Sequencer::ADC_RMP)->setInvHold(0);
  //getTrack(Sequencer::FCF_SwIn)->setInvHold(0);
  //getTrack(Sequencer::FCF_Res_B)->setInvHold(0);
  //getTrack(Sequencer::FCF_Flip)->setInvHold(0);
  //getTrack(Sequencer::ISubPulse)->setInvHold(1);
}


bool Sequencer::generateSignals( int cycleLength )
{
  SuS_LOG_STREAM(info, log_id(), "TODO: generateSignals with parameters.");
  return generateSignals();
}


bool Sequencer::generateSignals()
{
  SuS_LOG_STREAM(debug, log_id(), "TODO: generateCycles.");
  SuS_LOG_STREAM(info, log_id(), "Updating current signals for new cycle length.");
  bool newCycleLengthValidForAllTracks = true;
  for ( auto t : tracks ) {
    bool b = t->checkCycleLength();
    if ( !b ) {
      SuS_LOG_STREAM(info, log_id(), "Track " << t->name << " is not vaild.");
    }
    newCycleLengthValidForAllTracks &= b;
  }
  if ( newCycleLengthValidForAllTracks ) {
    for ( auto t : tracks ) {
      t->setCycleLength(); // compiles the track also
    }
    return true;
  } else {
    SuS_LOG_STREAM( error, log_id(), "New cycle length not valid for all tracks." );
    return false;
  }
}

void Sequencer::shiftTrack(SequencerTrack * track, int shift)
{
  if(shift < 0){
    shift = getTotalClockCycles() + shift;
  }
  shiftTrackRight(track, shift);
}


void Sequencer::shiftTrackRight(SequencerTrack * track, uint32_t shift)
{
  //SuS_LOG_STREAM(debug, log_id(), "trackShift is " << shift);

  auto & phases = track->phases;
  if(phases.size() <= 1) return;

  while(shift > 0)
  {
    int currentShift = shift;
    if((uint32_t)phases.back().clockCycles < shift){
      currentShift = phases.back().clockCycles;
    }

    if(phases.front().high == phases.back().high)
    {
      phases.front().clockCycles += currentShift;
    }else{
      phases.insert(phases.begin(),SequencePhase(phases.back().high,currentShift));
    }
    phases.back().clockCycles -= currentShift;
    shift -= currentShift;
    if(phases.back().clockCycles <= 0){
      phases.erase(phases.end()-1);
    }
  }
}


void Sequencer::shiftAllTracksRight(uint32_t shift)
{
  for (auto i=0; i<c_numOfTracks; ++i) {
    shiftTrackRight( getTrack( (TrackNum)i ), shift );
  }
}


void Sequencer::shiftHolds(int shiftFastCycles)
{
  int shiftSlowCycles = floor(1.0/7.0*shiftFastCycles);
  if(shiftSlowCycles<0){
    shiftSlowCycles = m_cycleLength + shiftSlowCycles;
  }
  shiftHoldsRight(shiftSlowCycles);

  checkHoldCnts(holdCnts);
}


void Sequencer::shiftHoldsRight(int shiftSlowCycles)
{
  while(shiftSlowCycles > 0)
  {
    int currentShift = shiftSlowCycles;
    if(holdCnts.back().length <= currentShift){
      currentShift = holdCnts.back().length;
      holdCnts.insert(holdCnts.begin(),HoldCntsRegisterEntry(currentShift,holdCnts.back().hold));
      holdCnts.erase(holdCnts.end()-1);
      if(holdCnts.back().hold){
        holdCnts.insert(holdCnts.begin(),holdCnts.back());
        holdCnts.erase(holdCnts.end()-1);
      }
    }
    else
    {
      if(holdCnts.front().hold == holdCnts.back().hold)
      {
        holdCnts.front().length += currentShift;
      }else{
        holdCnts.insert(holdCnts.begin(),HoldCntsRegisterEntry(currentShift,holdCnts.back().hold));
      }
      holdCnts.back().length -= currentShift;
    }
    shiftSlowCycles -= currentShift;
  }
}


SequencerTrack* Sequencer::getTrack(Sequencer::TrackNum n)
{
  if (n > c_numOfTracks-1) {
    SuS_LOG_STREAM(error, log_id(), "TrackNum " << n << " not available.");
    return nullptr;
  }
  SequencerTrack* track = tracks[n];
  //SuS_LOG_STREAM(info, log_id(), "getTrack(): n = " << n << ", TrackNum = " << track->trackNum << ", trackNumAddr " << &(track->trackNum) <<
  //    ", TrackName = " << Sequencer::c_trackNames[track->trackNum] << ", trackAddr " << tracks[n]);
  return track;
}


void Sequencer::setHoldEnabled(bool _holdEnabled)
{
  holdEnabled = _holdEnabled;
}


void Sequencer::setSequencerParameter(std::string _paramName, int _setting, bool genSignals)
{
  SuS_LOG_STREAM(info, log_id(), "Setting sequence parameter " << _paramName << " = " << _setting << ".");
  setSequencerParameter(paramNameToSeqParam(_paramName), _setting, genSignals);
}


void Sequencer::setSequencerParameter(SeqParam _p, int _setting, bool genSignals)
{
  switch(_p) {
    case (int)CycleLength:
      m_cycleLength = _setting; break;
    default :
      SuS_LOG_STREAM(warning, log_id(), "SeqParam " << (int)_p << " not valid.");
  }

  if (genSignals) generateSignals();
}


int Sequencer::getSequencerParameter(std::string _paramName)
{
  return getSequencerParameter(paramNameToSeqParam(_paramName));
}


std::string Sequencer::getParameterName(SeqParam _p)
{
  switch(_p) {
    case (int)CycleLength:
      return "CycleLength";
    default :
      SuS_LOG_STREAM(warning, log_id(), "SeqParam " << (int)_p << " not valid.");
      return "";
  }
}


int Sequencer::getSequencerParameter(SeqParam _p) const
{
  switch(_p) {
    //case (int)RampLength :
    //  return rampLength;
    default :
      SuS_LOG_STREAM(warning, log_id(), "SeqParam " << (int)_p << " not valid.");
      return -1;
  }
}


std::vector<std::string> Sequencer::getParameterNames()
{
  std::vector<std::string> paramNames;
  for(int i=0; i<c_numOfParams; i++){
    paramNames.push_back(getParameterName((SeqParam)i));
  }
  return paramNames;
}


Sequencer::SeqParam Sequencer::paramNameToSeqParam(const std::string & paramName)
{
  auto it = paramNamesMap.find(paramName);
  if (it == paramNamesMap.end()) {
    SuS_LOG_STREAM(error, log_id(), paramName << " not found.");
    return Invalid;
  }
  return it->second;
}


std::map<std::string,uint32_t> Sequencer::getSequencerParameterMap() const
{
  std::map<std::string,uint32_t> paraMap;
  for(int i=0; i<c_numOfParams; i++){
    paraMap[getParameterName((SeqParam)i)] = getSequencerParameter((SeqParam)i);
  }
  return paraMap;
}


void Sequencer::setJtagSubAddressAndName(TrackNum n, int addr, std::string name)
{
  getTrack(n)->jtagSubAddress = addr;
  getTrack(n)->name = name;
}


Sequencer::TrackNum Sequencer::getSeqTrackIndex(int seqTrackAddr)
{
  for (auto i=0; i<c_numOfTracks; ++i) {
    if (getTrack( (TrackNum)i )->jtagSubAddress == seqTrackAddr)
      return (TrackNum)i;
  }
  cout << "ERROR: Sequencer - seqTrackAddr " << seqTrackAddr << " not found" << endl;
  return (TrackNum)0;
}
