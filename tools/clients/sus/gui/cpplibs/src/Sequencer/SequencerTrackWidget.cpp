#include "SequencerTrackWidget.h"
#include "Sequencer.h"

#include <QTableWidgetItem>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QPushButton>
#include <QLabel>
#include <QComboBox>
#include <QFormLayout>
#include <QFileDialog>
#include <QGridLayout>
#include <QCheckBox>
#include <QRadioButton>
#include <QButtonGroup>
#include <QLineEdit>

//#include "logger.h"
//#include "subsystem_registrator.h"
#include <sstream>
#include <iostream>
#include <cassert>

using namespace SuS;

#define SuS_LOG_STREAM(type,id,output)\
          std::cout << "++++"#type": Sequencer: " << output << std::endl;

//namespace{ SuS::logfile::subsystem_registrator log_id( "SeqTrack" ); }


HoldCntsTableEntry::HoldCntsTableEntry(int slowClockCycles, bool holdPhase)
{
  slowClockCyclesSB = new QSpinBox;
  slowClockCyclesSB->setRange(1,16383);
  slowClockCyclesSB->setValue(slowClockCycles);
  slowClockCyclesSB->setAlignment(Qt::AlignRight);

  holdPhaseSB = new QSpinBox;
  holdPhaseSB->setRange(0,1);
  holdPhaseSB->setValue(holdPhase ? 1 : 0);
  holdPhaseSB->setAlignment(Qt::AlignRight);
}


HoldCntsTableEntry::~HoldCntsTableEntry()
{
}


PhaseSpinBoxes::PhaseSpinBoxes(int fastClockCycles, bool high)
{
  fastClockCyclesSB = new QSpinBox;
  slowClockCyclesLabel = new QLabel();
  connect(fastClockCyclesSB, SIGNAL(valueChanged(int)), this, SLOT(updateSlowClockCycles(int)));

  fastClockCyclesSB->setRange(0,10000);
  fastClockCyclesSB->setValue(fastClockCycles);
  fastClockCyclesSB->setAlignment(Qt::AlignRight);
  slowClockCyclesLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);  

  phaseTypeSB = new QSpinBox;
  phaseTypeSB->setRange(0,1);
  phaseTypeSB->setValue(high ? 1 : 0);
  phaseTypeSB->setAlignment(Qt::AlignRight);
}


PhaseSpinBoxes::~PhaseSpinBoxes()
{
  // owner is the table in which they are displayed
  delete fastClockCyclesSB;
  delete phaseTypeSB;
}


void PhaseSpinBoxes::updateSlowClockCycles(int fastClockCycles)
{
  slowClockCyclesLabel->setText(
        QString::number(fastClockCycles/SequencerTrack::c_partSeqWidth)
      + ", "
      + QString::number(fastClockCycles%SequencerTrack::c_partSeqWidth)
      );
}


/*
 * Table to manually enter the values to be programmed to the chip registers.
 */
QPartSeqsTable::QPartSeqsTable(QWidget* _parent) :
  QTableWidget(_parent),
  partSeqs(SequencerTrack::c_partSeqDepth)
{
   setRowCount(0);
   setColumnCount(2);
   setSelectionMode(QAbstractItemView::SingleSelection);
   setSelectionBehavior(QAbstractItemView::SelectItems);
   setHorizontalHeaderItem(0, new QTableWidgetItem("Partial Sequence"));
   setHorizontalHeaderItem(1, new QTableWidgetItem("Repetition Count"));
   for(int i=0; i<partSeqs.size(); ++i) {
     QSpinBox* partSeqEdit = new QSpinBox;
     partSeqEdit->setAlignment(Qt::AlignRight | Qt::AlignVCenter); 
     partSeqEdit->setRange(0, (1<<SequencerTrack::c_partSeqWidth)-1); 
     //partSeqEdit->setDisplayIntegerBase(2);  // only from qt5
     QSpinBox* repCntSB = new QSpinBox;
     repCntSB->setAlignment(Qt::AlignRight | Qt::AlignVCenter); 
     repCntSB->setRange(0, SequencerTrack::c_maxRepCntRegVal);
     partSeqs[i] = QPair<QSpinBox*, QSpinBox*>(partSeqEdit,repCntSB);
     int newRowNum = rowCount();
     setRowCount(newRowNum+1);
     setCellWidget(newRowNum, 0, partSeqEdit);
     setCellWidget(newRowNum, 1, repCntSB);
   }
   horizontalHeader()->setResizeMode(QHeaderView::Stretch);
}


QPartSeqsTable::~QPartSeqsTable()
{
}


QPhasesTable::QPhasesTable(QWidget* _parent) :
  QTableWidget(_parent),
  phasesSBs(QVector<PhaseSpinBoxes*>())
{
  setRowCount(0);
  setColumnCount(3);
  setSelectionMode(QAbstractItemView::SingleSelection);
  setSelectionBehavior(QAbstractItemView::SelectItems);
  setHorizontalHeaderItem(0, new QTableWidgetItem("Fast Clock Cycles"));
  setHorizontalHeaderItem(1, new QTableWidgetItem("Slow Clock Cycles"));
  setHorizontalHeaderItem(2, new QTableWidgetItem("Phase Type (0 or 1)"));
  horizontalHeader()->setResizeMode(QHeaderView::Stretch);
}


void QPhasesTable::clearPhases()
{
  for (int i=0; i<phasesSBs.size(); ++i) {   
    removeCellWidget(i+1,0);
    removeCellWidget(i+1,1);
    delete phasesSBs.at(i);
  }
  phasesSBs.clear();
  setRowCount(0);
}


QPhasesTable::~QPhasesTable()
{
}


void QPhasesTable::getSequencePhases(SequencerTrack::SequencePhases& phases)
{
  phases.clear();
  for (int p=0; p<phasesSBs.size(); ++p) {
    PhaseSpinBoxes* sbs = phasesSBs[p];
    bool phaseType = (bool)(sbs->phaseTypeSB->value());
    int clockCycles = (int)(sbs->fastClockCyclesSB->value());
    phases.push_back(SequencerTrack::SequencePhase(phaseType, clockCycles));
  }
}


SequencerTrackWidget::SequencerTrackWidget(SequencerTrack* _seqTrack, QWidget* _parent) :
  QFrame(_parent),
  seqTrack( _seqTrack ),
  manualMode(false)
{
  setFrameStyle(QFrame::Panel | QFrame::Raised);
  QVBoxLayout* mainLayout = new QVBoxLayout;
  this->setLayout(mainLayout);
  phasesTable = new QPhasesTable;
  partSeqsTable = new QPartSeqsTable;
  QHBoxLayout* statValLayout = new QHBoxLayout;
  statValSB = new QSpinBox;
  statValSB->setRange(0,1);
  statValSB->setAlignment(Qt::AlignRight);
  statValSB->setEnabled(false);
  invHoldSB = new QSpinBox;
  invHoldSB->setRange(0,1);
  invHoldSB->setEnabled(false);
  clkCB = new QCheckBox("Clock Track");
  connect(clkCB, SIGNAL(toggled(bool)), this, SLOT(setClockMode(bool)));
  //statValLayout->addWidget(new QLabel("Static IProg Value"));
  //statValLayout->addWidget(statValSB);
  //statValLayout->addWidget(new QLabel("Invert Hold"),Qt::AlignRight);
  //statValLayout->addWidget(invHoldSB);

  //mainLayout->addLayout(statValLayout);
  mainLayout->addWidget(phasesTable);
  mainLayout->addWidget(partSeqsTable);
  mainLayout->addWidget(clkCB);
  
  this->setMaximumHeight(500);
}


SequencerTrackWidget::~SequencerTrackWidget()
{
}


void SequencerTrackWidget::setManualMode(bool _manualMode)
{
  manualMode = _manualMode;
  phasesTable->setVisible(!manualMode);
  partSeqsTable->setVisible(manualMode);
}


bool SequencerTrackWidget::getStatVal()
{
  return (bool)statValSB->value();
}

bool SequencerTrackWidget::getInvHold()
{
  return (bool)invHoldSB->value();
}

void SequencerTrackWidget::putPartSeqs(SequencerTrack::PartialSequences& _partSeqs)
{
  _partSeqs.clear();
  for (int i=0; i<partSeqsTable->partSeqs.size(); ++i) {
    _partSeqs.push_back(SequencerTrack::PartialSequence(
          partSeqsTable->partSeqs[i].first->value(),
          partSeqsTable->partSeqs[i].second->value()));
  }
}

void SequencerTrackWidget::setPartSeqTable(SequencerTrack::PartialSequences& _partSeqs)
{
  QVector<QPair<QSpinBox*, QSpinBox*> >& ps = partSeqsTable->partSeqs;
  if((uint)_partSeqs.size() == (uint)ps.size()) {
    //qDebug() << "_partSeqs.size() = " << _partSeqs.size();
    for (int i=0; i<ps.size(); ++i) {
      ps[i].first->setValue(_partSeqs[i].partSeq);
      ps[i].second->setValue(_partSeqs[i].repCnt);
    }
  } //else {
  //  SuS_LOG(error,log_id(),"");
  //}
}


void SequencerTrackWidget::setClockMode(bool _en)
{
  seqTrack->setClockMode( _en );
}


#include "SequencerTrackWidget.moc"
