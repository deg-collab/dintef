#ifndef Sequencer_GUI_H
#define Sequencer_GUI_H

#include <QWidget>
#include <QVector>
#include <QTableWidget>
#include "SequencerQt.h"
#include "SeqDrawLabel.h"
#include <QSpinBox>
#include <QGroupBox>

class QComboBox;
class QLabel;
class QEvent;
class QCheckBox;
class QRadioButton;
class QButtonGroup;
class QLineEdit;
class QGroupBox;
class QHBoxLayout;
class QPixmap;

namespace SuS
{

  class SequencerTrackWidget;
  class HoldCntsTableEntry;

  class LabeledSlider : public QWidget
  {
    public :
      LabeledSlider(QString label,  QWidget *parent = NULL);
      ~LabeledSlider();

      void setValue(int value){posSlider->setValue(value);}
      int  value(){return posSlider->value();}

      QLabel  *nameLabel;
      QLabel  *valueLabel;
      QSlider *posSlider;
  };

  class HoldGeneratorWidget : public QGroupBox
  {
    Q_OBJECT

    friend class Sequencer_GUI;

    public :

      HoldGeneratorWidget(SequencerQt *sequencer, QWidget *parent = NULL);
      ~HoldGeneratorWidget();

      QTableWidget *holdCntsTable;
      QRadioButton *compilerModeRB;
      QRadioButton *manModeRB;
      QRadioButton *intTimeExtRB;
      QRadioButton *depFetModeRB;
      QCheckBox    *enableSecondHoldPhaseCB;
      LabeledSlider *firstPosSlider;
      LabeledSlider *secondPosSlider;
      QSpinBox     *firstLengthSB;
      QSpinBox     *secondLengthSB;

    protected :

      QVector<HoldCntsTableEntry*> holdCnts;

    private :

      SequencerQt *sequencer;

    protected slots:

      void cycleLengthChangedSlot(int cycleLength);
      void adjustSecondHoldRangeSlot();
      bool setHoldGenerator();
  };

  class Sequencer_GUI : public QWidget
  {
    Q_OBJECT

    friend class CHIP_GUI;

    public:

      Sequencer_GUI(SequencerQt *_sequencer, QWidget* _parent = NULL);
      ~Sequencer_GUI();

      inline int getCycleLength(){return cycleLengthSB->value();}

    signals:

      void cycleLengthUpdated(int cycleLength);
      void programSequencer();
      void readBackSequencer();
      void newFileLoaded();
      void setMonitorOutput(int trackSel);
      void cycleLengthChanged();

    public slots:

      void emitCycleLengthChanged();

      void addPhaseToCurrentTrack(bool high=false, int clockCylces=0);
      void addPhase(SequencerQt::TrackNum n, bool high=false, int clockCylces=0);
      void setStatVal(SequencerQt::TrackNum n, bool val);
      void rmLastPhase();
      void saveImage();
      bool updateAndCompileSequencer();
      void emitReadBackSequencer();
      void compileAndProgram();
      void changeTrackTable(int index);
      void saveAsFile();
      void saveFile();
      void saveToFile(QString filename);
      void loadFile();
      void reLoadFile();
      void loadFile(QString filename);
      void updateGUI();
      void updateHoldGUI();
      void updateMissingCycles();
      void updatePartSeqsTables();
      void slotEnableSimMode(bool en);
      void enSignalCompilerMode(bool en);
      void setSimMode(bool simMode);
      void holdEnableToggled(bool enable);
      void disableGUIUpdate();
      void sendSetMonitorOutputSignal();
      void mouse_event_filter(int type, int x, int y);
      void setDefaultSequence();
      void setCycleLength( int _val );

    protected slots:
      void updateSequencerParameter(const QString& param);
      void setSequencerParameter();

    protected :

      QLabel *filenameLabel;
      QLabel *missingCyclesLabel;
      QGroupBox *simBtns;
      QGroupBox *signalsCompilerBox;
      QGroupBox *tracksCompilerBox;

      SequencerQt *sequencer;

    private:

      void disconnectSetOpMode(bool dis);
      void generateLayout();
      void initTables();
      void updateDrawArea();
      QPixmap* generateTracksMap(int scaleFactor=1);

      bool manualOpModeChange;

      QVector<SequencerTrackWidget*> trackWidgets;

      HoldGeneratorWidget *holdGenWidget;
      QWidget *trackWidget;
      QSpinBox *cycleLengthSB;
      QComboBox *trackSelect;
      QCheckBox *holdEnableCB;
      QCheckBox *enableMonitorOutputCB;

      QSpinBox* compRampLengthSB;
      QSpinBox* compResetLengthSB;
      QSpinBox* compResetIntegOffsetSB;
      QSpinBox* compResetHoldLengthSB;
      QSpinBox* compIntegrationTimeSB;
      QSpinBox* compFlattopLengthSB;
      QSpinBox* compFlattopHoldLengthSB;
      QSpinBox* compRampIntOffsetSB;
      QSpinBox* compBackFlipAtResetSB;
      QSpinBox* compBackFlipToResetOffsetSB;
      QSpinBox* compSingleCapLoadLengthSB;
      QLabel *compSingleCapLoadLabel;

      QRadioButton *singleIntegrationRB;
      QRadioButton *normalModeRB;
      QRadioButton *manualPhasesRB;
      QRadioButton *resetValueRB;
      QRadioButton *bufferModeRB;
      QRadioButton *extLatchModeRB;

      QRadioButton* manModeRB;
      QRadioButton* signalCompilerModeRB;
      QRadioButton* trackCompilerModeRB;

      QButtonGroup* selectOpModeGr;
      QCheckBox *singleCapModeCB;

      SeqDrawLabel* drawLabel;

      QComboBox* sequencerParameterCB;
      QSpinBox*  sequencerParameterSB;
      QCheckBox* autoCompileCB;
  };

}

#endif
