#ifndef SEQUENCER_H
#define SEQUENCER_H

#include <stdint.h>
#include <vector>
#include <string>
#include <fstream>
#include <map>

#include "SequencerTrack.h"

#include <iostream>


namespace SuS
{

  class Sequencer
  {
    friend class SequencerTrack;

    public :

      Sequencer(std::string filename = "", bool initTracks=true);
      virtual ~Sequencer();

      struct HoldCntsRegisterEntry {
        HoldCntsRegisterEntry(uint16_t _length, bool _hold) :
          length(_length), hold(_hold) {}
        uint16_t length;  // c_holdCntWidth bits
        bool hold;        // flags a hold phase
      };

      typedef SequencerTrack::SequencePhase SequencePhase;

      typedef std::vector<HoldCntsRegisterEntry> HoldCntsRegister;

      enum TrackNum{
        Res0            = 0,
        Res1            = 1,
        EnCP0           = 2,
        EnCP1           = 3,
        Sw0to1          = 4,
        ClkCP           = 5,
        InjB0           = 6,
        InjB1           = 7,
//        ResBigCSA   = 8,
        InpShieldTest   = 8,
        EnIN            = 9,
        Ch0Coarse       = 10,
        Ch0Fine         = 11,

      };

      TrackNum getSeqTrackIndex(int seqTrackAddr);
      inline int getTrackJtagSubAddress(TrackNum n) {return getTrack(n)->jtagSubAddress;}
      std::string getTrackName(TrackNum n) {return getTrack(n)->name;}
      void setTracksNotCompiled();

      enum ProgMode {
        Direct   = 0,  // enables to directly enter the register values
        Compiler = 1   // abstract compilation controlled by sequencer parameters 
      };

      Sequencer::ProgMode getProgMode();
      static std::string getProgModeStr(Sequencer::ProgMode progMode);

      enum SeqParam {
        CycleLength = 0,
        Res0Length = 1, 
        Res1Length = 2, 
        Invalid    = 999
      };

      // string name to parameter enum conversion
      SeqParam paramNameToSeqParam(const std::string &paramName);
      static std::string getParameterName(SeqParam _p);
      static std::vector<std::string> getParameterNames();

      // get number of bits to program
      inline int getNumHoldProgBits() const { return ((c_holdCntWidth+1)*c_holdCntsRegDepth); }
      inline int getNumTrackBits(TrackNum n) const{ return tracks[n]->getNumBits();}

      // getters and setters for the sequencer parameters
      int getSequencerParameter(SeqParam _p) const;
      int getSequencerParameter(std::string _paramName);
      void setSequencerParameter(SeqParam _p, int _setting, bool genSignals=true);
      void setSequencerParameter(std::string _paramName, int _setting, bool genSignals=true);

      void setJtagSubAddressAndName(TrackNum n, int addr, std::string name);

      int getTotalClockCycles(TrackNum trackNum = Res0);

      // methods to obtain bit streams for programming
      void getTrackDdynProgBits(TrackNum n, std::vector<bool>& bit) const;
      void getHoldProgBits(std::vector<bool>& bits) const;
      void getStatVals(std::vector<bool>& bits) const;
      void appendInvHolds(std::vector<bool>& bits);

      bool compareTrackContent(const std::vector<bool> &data_vec, TrackNum track);
      bool compareHoldContent(const std::vector<bool> &data_vec);

      std::map<std::string, uint32_t> getSequencerParameterMap() const;

      int trackNameToNum(std::string name);
      std::string trackNumToName(TrackNum n) {return getTrack(n)->name;}
      bool compileAllTracks();
      virtual bool compileAndCheckAllTracks();
      static bool setHoldCnts(HoldCntsRegister _holdCnts);
      static bool checkHoldCnts(SuS::Sequencer::HoldCntsRegister & _holdCnts);
      static bool correctNumberOfHolds(SuS::Sequencer::HoldCntsRegister & _holdCnts);

      static bool holdCntsInSyncWithCycleLength(const SuS::Sequencer::HoldCntsRegister &_holdCnts);
      static int getCycleLength() {return m_cycleLength;}
      int getHoldCnts();
      void setZeroHolds();
      int getRealCycleLength();

      bool getSignalCompilerMode() {return m_progMode == Compiler;}

      static int c_numOfTracks;
      static int c_numOfParams;
      static int c_holdCntWidth;
      static int c_holdCntsRegDepth;

      bool isGood(){return configGood;}
      void printHoldCounts();
      void printTracks();
      void findHoldPositions(int& firstPos, int& firstLength, int& secondPos, int& secondLength);
      void findIntegrationPositions(int& firstPos, int& firstLength, int& secondPos, int& secondLength);
      void setFilterInBufferMode();

      SequencerTrack* getTrack(TrackNum n); // {return tracks[(int)n];};

      typedef std::vector<SequencerTrack*> SequencerTracks;
      SequencerTracks tracks;
      static bool debugMode;
      std::string filename;
      // holds the measurement cycle length which must be written to the MasterFSM config reg
      static int m_cycleLength;
      static HoldCntsRegister holdCnts;
      static bool holdEnabled;
      bool configGood;

      // signal compiler variables
      // int cpHiLength;
      // int cpLowLength;

      ProgMode mode;

      bool saveToFile();
      bool saveToFile(const std::string &_filename);
      bool loadFile(std::string _filename);
      void setFileName(std::string _filename);
      const std::string& getFilename() {return filename;}

      virtual void setCycleLength(int _cycleLength);
      static void setHoldEnabled(bool _holdEnabled);
      static bool getHoldEnabled() {return holdEnabled;}
      static void setDebugMode(bool _debugMode) {debugMode = _debugMode;} 
      void disableSecondHold();
      bool setHoldPositions(int firstPos, int firstLength, int secondPos=0, int secondLength=0);

      void setProgMode(ProgMode _mode, bool generate = true);
      void setProgMode(const std::string & _modeStr, bool generate = true);

      bool generateSignals();
      bool generateSignals(int _cycleLength );

      void generateSimPhases(int t);
      //      void emitContentChanged();

      void shiftHolds(int shift);
      void shiftHoldsRight(int shift);
      void shiftTrack(SequencerTrack * track, int shift);
      void shiftTrackRight(SequencerTrack * track, uint32_t shift);
      void shiftAllTracksRight(uint32_t shift);

    private:
      int getValueFromXMLLine(const std::string &line, std::string valueName);
      std::string getStringFromXMLLine(const std::string &line, std::string valueName);
      ProgMode getProgModeFromXMLLine(const std::string &line);

      void writeXMLCycleParameters(std::ofstream &ofs);
      void writeXMLSequencerTrack(std::ofstream &ofs, TrackNum trackNum);
      void writeXMLPhase(std::ofstream &ofs, const SequencePhase& phase);
      void writeXMLHoldRegister(std::ofstream &ofs, const HoldCntsRegisterEntry& regEntry);
      void writeXMLHoldCounts(std::ofstream &ofs);

      static std::map<std::string, SeqParam> paramNamesMap;

      std::vector<std::string> compareErrors;
      bool fieldNotFound;

      ProgMode m_progMode;


  public:
      inline const std::vector<std::string> & getCompareErrors() const {return compareErrors;}
      inline void clearCompareErrors() { compareErrors.clear(); }
    
      class ConfigKeeper { // keepers can be constructed to store state, destructor restores old state

        public:
          ConfigKeeper(Sequencer * _seq,Sequencer::ProgMode newMode = Sequencer::Direct)
          : m_seq(_seq)
          {
            //mode = seq->getProgMode();
            //holdEnabled           = seq->getHoldEnabled();
            //cycleLength           = seq->getCycleLength();
            //seq->setProgMode(newMode);
            // maybe better: use copy constructor
          }

          ~ConfigKeeper() {
          }

        private:

          Sequencer *m_seq;
          Sequencer::ProgMode m_progMode;
      };

  }; // Sequencer

} // SuS


#endif
