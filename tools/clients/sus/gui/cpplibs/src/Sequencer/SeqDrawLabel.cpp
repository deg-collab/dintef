

#include "SeqDrawLabel.h"

using namespace SuS;

SeqDrawLabel::SeqDrawLabel(QWidget *parent)
  :QLabel(parent)
{

}

void SeqDrawLabel::mousePressEvent(QMouseEvent *ev)
{
  this->x = ev->x();
  this->y = ev->y();

  emit mouse_pressed(1,x,y);
}

void SeqDrawLabel::mouseMoveEvent(QMouseEvent *ev)
{
  this->x = ev->x();
  this->y = ev->y();

  emit mouse_pos(2,x,y);
}


#include "SeqDrawLabel.moc"
