#ifndef SEQUENCERQT_H
#define SEQUENCERQT_H

#include <QString>
#include <QStringList>
#include <QObject>
#include "Sequencer.h"

namespace SuS
{

  class SequencerQt : public QObject, public Sequencer
  {

    Q_OBJECT

    friend class Sequencer_GUI;
    friend class CHIP_GUI;
    friend class CNTRL1_GUI;
    friend class MM3_GUI;
    friend class F1_GUI;
    friend class F1ProbecardWidget;
    friend class CHIP;
    friend class SequencerTrack_ChipBugFixes;
    friend class MeasurementWidget;
    friend class HoldGeneratorWidget;

    public :

      SequencerQt(QString filename = "", bool initTracks=true);
      ~SequencerQt();

      void setFileName(QString _filename);
      void saveToFile(QString _filename){Sequencer::saveToFile(_filename.toStdString());}
      void saveToFile(){Sequencer::saveToFile();}
      bool loadFile(QString _filename){return Sequencer::loadFile(_filename.toStdString());}
      QString getFilename(){return QString::fromStdString(Sequencer::getFilename());}

      QString getParameterName(SeqParam _p){return QString::fromStdString(Sequencer::getParameterName(_p));}
      QStringList getSequencerTrackNames()
      {
        QStringList names;
        for(auto && t : tracks){
          names.push_back(QString::fromStdString(t->name));
        }
        return names;
      }

      QString trackNumToQName(TrackNum n) {return QString::fromStdString(getTrack(n)->name);}

  public slots:

      void setCycleLength(int _cycleLength) override;

    signals :

      void contentChanged();
      void filenameChanged(QString const& filename);
      void cycleLengthChanged(int cycleLength);
      void realCycleLengthChanged(int realCycleLength);

      void programSequencer();
      void doSingleBurst();

    private:
      void emitContentChanged();


  }; // SequencerQt

} // SuS


#endif
