#include <sstream>
#include <QString>
#include "SequencerQt.h"

using namespace SuS;

SequencerQt::SequencerQt(QString filename, bool initTracks) :
  Sequencer(filename.toStdString(),initTracks)
{
  emitContentChanged();
}


SequencerQt::~SequencerQt()
{

}

void SequencerQt::setFileName(QString _filename)
{
  Sequencer::setFileName(_filename.toStdString());
  emit filenameChanged(_filename);
}


void SequencerQt::setCycleLength(int _cycleLength)
{
  Sequencer::setCycleLength(_cycleLength);
  emit realCycleLengthChanged(getRealCycleLength());
  emit cycleLengthChanged(_cycleLength);
}

void SequencerQt::emitContentChanged()
{
  emit contentChanged();
}


#include "SequencerQt.moc"
