#include "Sequencer_GUI.h"
#include "SequencerQt.h"
#include "SequencerTrackWidget.h"
#include "QConfigReg.h"

#include <QTableWidgetItem>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QHeaderView>
#include <QPushButton>
#include <QLabel>
#include <QComboBox>
#include <QFormLayout>
#include <QFileDialog>
#include <QGridLayout>
#include <QCheckBox>
#include <QRadioButton>
#include <QButtonGroup>
#include <QSlider>
#include <QGroupBox>
#include <QDebug>
#include <QPainter>
#include <QPainterPath>
#include <QPixmap>
#include <QEvent>
#include <QMouseEvent>
#include <QMessageBox>

//#include "logger.h"
//#include "subsystem_registrator.h"
#include <sstream>
#include <iostream>

using namespace SuS;

#define SuS_LOG_STREAM(type,id,output)\
          std::cout << "++++"#type": Sequencer: " << output << std::endl;

#define SuS_LOG(type,id,output)\
          std::cout << "++++"#type": Sequencer: " << output << std::endl;

//namespace{ SuS::logfile::subsystem_registrator log_id( "SeqGUI" ); }

LabeledSlider::LabeledSlider(QString label, QWidget *parent) :
  QWidget(parent)
{
  QHBoxLayout *layout = new QHBoxLayout;
  this->setLayout(layout);
  nameLabel   = new QLabel(label);
  valueLabel  = new QLabel;
  posSlider   = new QSlider(Qt::Horizontal);
  posSlider->setTickPosition(QSlider::TicksBelow);
  layout->addWidget(nameLabel);
  layout->addWidget(valueLabel);
  layout->addWidget(posSlider);
  connect(posSlider,SIGNAL(valueChanged(int)),valueLabel,SLOT(setNum(int)));
}


LabeledSlider::~LabeledSlider()
{
}


HoldGeneratorWidget::HoldGeneratorWidget(SequencerQt* _sequencer, QWidget *parent) :
  QGroupBox("Hold Generator",parent),
  sequencer(_sequencer)
{
  QHBoxLayout *selectLayout = new QHBoxLayout;
  compilerModeRB = new QRadioButton("Compiler");
  intTimeExtRB   = new QRadioButton("Ext IntTime");
  intTimeExtRB -> setToolTip("Integration time can be extended by using holds if enabled.<br>"
                             "If integration time is larger than 34 holds will be inserted.");
  depFetModeRB    = new QRadioButton("DEPFET");
  depFetModeRB -> setToolTip("Inserts two hold phases:<br>"
                             "First phase is inserted during the RMP high phase, and before res_n low<br>"
                             "Second phase extends the flattop after first integration phase an before flip down.");
  manModeRB      = new QRadioButton("Manual");
  manModeRB->setChecked(true);

  selectLayout->addWidget(compilerModeRB);
  selectLayout->addWidget(depFetModeRB);
  selectLayout->addWidget(intTimeExtRB);
  selectLayout->addWidget(manModeRB);
  selectLayout->addStretch();

  QWidget      *compilerWidget           = new QWidget;
  QGridLayout  *compilerLayout           = new QGridLayout;
  QLabel       *secondPosLabel           = new QLabel;

  compilerWidget->setLayout(compilerLayout);

  enableSecondHoldPhaseCB  = new QCheckBox;

  firstLengthSB   = new QSpinBox;
  secondLengthSB  = new QSpinBox;
  firstPosSlider  = new LabeledSlider("First Position");
  secondPosSlider = new LabeledSlider("Second Position");

  firstLengthSB->setRange(1,(1<<Sequencer::c_holdCntWidth)-1);
  secondLengthSB->setRange(1,(1<<Sequencer::c_holdCntWidth)-1);

  connect(sequencer, SIGNAL(cycleLengthChanged(int)),this, SLOT(cycleLengthChangedSlot(int)));
  connect(firstPosSlider->posSlider, SIGNAL(valueChanged(int)),this,SLOT(adjustSecondHoldRangeSlot()));

  compilerLayout->addWidget(enableSecondHoldPhaseCB,0,2);
  compilerLayout->addWidget(new QLabel("Enable Second Hold"),0,1);
  compilerLayout->addWidget(firstPosSlider,1,0);
  compilerLayout->addWidget(new QLabel("First Length"),1,1);
  compilerLayout->addWidget(firstLengthSB,1,2);
  compilerLayout->addWidget(secondPosSlider,2,0);
  compilerLayout->addWidget(new QLabel("Second Length"),2,1);
  compilerLayout->addWidget(secondLengthSB,2,2);

  firstPosSlider->posSlider->setRange(1,Sequencer::getCycleLength()-1);
  firstPosSlider->posSlider->setSingleStep(1);
  firstPosSlider->posSlider->setSliderPosition(1);
  firstPosSlider->posSlider->setTickPosition(QSlider::TicksBelow);

  secondPosSlider->posSlider->setRange(1,Sequencer::getCycleLength());
  secondPosSlider->posSlider->setSingleStep(1);
  secondPosSlider->posSlider->setSliderPosition(1);
  secondPosSlider->posSlider->setTickPosition(QSlider::TicksBelow);

  connect(enableSecondHoldPhaseCB, SIGNAL(toggled(bool)),secondLengthSB,SLOT(setEnabled(bool)));
  connect(enableSecondHoldPhaseCB, SIGNAL(toggled(bool)),secondPosLabel,SLOT(setEnabled(bool)));
  connect(enableSecondHoldPhaseCB, SIGNAL(toggled(bool)),secondPosSlider,SLOT(setEnabled(bool)));
  enableSecondHoldPhaseCB->setChecked(false);

  QTableWidget *holdCntsTable = new QTableWidget;
  holdCntsTable = new QTableWidget;
  holdCntsTable->setRowCount(5);
  holdCntsTable->setColumnCount(2);
  holdCntsTable->setSelectionMode(QAbstractItemView::SingleSelection);
  holdCntsTable->setSelectionBehavior(QAbstractItemView::SelectItems);
  holdCntsTable->setHorizontalHeaderItem(0, new QTableWidgetItem("Hold Phase"));
  holdCntsTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Slow Clock Cycles"));
  holdCntsTable->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
  for (int i=0; i<Sequencer::c_holdCntsRegDepth; ++i) {
    HoldCntsTableEntry *entry = new HoldCntsTableEntry(0,0);
    holdCntsTable->setCellWidget(i,0,entry->holdPhaseSB);
    holdCntsTable->setCellWidget(i,1,entry->slowClockCyclesSB);
    holdCnts.push_back(entry);
  }

  QVBoxLayout  *topLayout = new QVBoxLayout;
  this->setLayout(topLayout);

  topLayout->addLayout(selectLayout);
  topLayout->addWidget(compilerWidget);
  topLayout->addWidget(holdCntsTable);
  topLayout->addStretch();

  compilerWidget->hide();
  holdCntsTable->show();

  connect(compilerModeRB, SIGNAL(toggled(bool)), compilerWidget, SLOT(setVisible(bool)));
  connect(manModeRB, SIGNAL(toggled(bool)), holdCntsTable, SLOT(setVisible(bool)));
  connect(intTimeExtRB, SIGNAL(toggled(bool)), holdCntsTable, SLOT(setVisible(bool)));
  connect(depFetModeRB, SIGNAL(toggled(bool)), holdCntsTable, SLOT(setVisible(bool)));

  connect(manModeRB, SIGNAL(toggled(bool)), sequencer, SLOT(setManualMode(bool)));

}


HoldGeneratorWidget::~HoldGeneratorWidget()
{

}


void HoldGeneratorWidget::cycleLengthChangedSlot(int cycleLength)
{
  firstPosSlider->posSlider->setMaximum(cycleLength);
  secondPosSlider->posSlider->setMaximum(cycleLength);
}


void HoldGeneratorWidget::adjustSecondHoldRangeSlot()
{
  secondPosSlider->posSlider->setMinimum(firstPosSlider->posSlider->value()+1);
}


bool HoldGeneratorWidget::setHoldGenerator()
{
  if (compilerModeRB->isChecked()) {
    return sequencer->setHoldPositions(
        firstPosSlider->posSlider->value(),
        firstLengthSB->value()
        );
  } else {
    Sequencer::HoldCntsRegister holdCntsToSet;
    for (int i=0; i<Sequencer::c_holdCntsRegDepth; ++i) {
      holdCntsToSet.push_back(Sequencer::HoldCntsRegisterEntry(
            holdCnts[i]->slowClockCyclesSB->value(),
            holdCnts[i]->holdPhaseSB->value()));
      if(Sequencer::debugMode){
        SuS_LOG_STREAM(info, log_id(), "Hold Phase " << i << " : " << holdCnts[i]->holdPhaseSB->value() << " /Length " << holdCnts[i]->slowClockCyclesSB->value());
      }
    }
    return sequencer->setHoldCnts(holdCntsToSet);
  }
}


Sequencer_GUI::Sequencer_GUI(SequencerQt *_sequencer, QWidget* _parent) :
  QWidget(_parent),
  sequencer(_sequencer),
  manualOpModeChange(false),
  trackWidgets(Sequencer::c_numOfTracks)
{
  if ( sequencer == nullptr ) {
    SuS_LOG(warning, log_id(), "Sequencer not initialized.");
    return;
  }

  generateLayout();

  if(!sequencer->isGood()){
    loadFile();
  }

  updateGUI();
}


Sequencer_GUI::~Sequencer_GUI()
{
  for (int i=0; i<Sequencer::c_numOfTracks; ++i) {
    delete trackWidgets[i];
  }
}


void Sequencer_GUI::enSignalCompilerMode(bool en)
{
  ////tracksCompilerBox->setVisible(!en);
  //trackWidget->setEnabled(!en);
  //signalsCompilerBox->setVisible(en);
  //sequencer->setSignalCompilerMode(en);
  //holdEnableCB->setChecked(sequencer->getHoldEnabled());
  //holdEnableCB->setEnabled(!en);
  //holdGenWidget->setEnabled(!en);
  //if (en && manualPhasesRB->isChecked()) {
  //  normalModeRB->setChecked(true);
  //}
}


void Sequencer_GUI::holdEnableToggled(bool enable)
{
}


void Sequencer_GUI::updatePartSeqsTables()
{
  for (int i=0; i<trackWidgets.size(); ++i) {
    trackWidgets[i]->setPartSeqTable(sequencer->getTrack(Sequencer::TrackNum(i))->compiledPartSeqs);
  }
}

void Sequencer_GUI::emitCycleLengthChanged()
{
  emit cycleLengthChanged();
}


void Sequencer_GUI::addPhase(Sequencer::TrackNum n, bool high, int clockCycles)
{
  QPhasesTable* t = trackWidgets[n]->phasesTable;
  if (t->phasesSBs.size()>=6 && !sequencer->getTrack(n)->isClockMode()) {
    SuS_LOG(error, log_id(), "More than 6 phases are not allowed.");
    return;
  }
  int newRowNum = t->rowCount();
  t->setRowCount(newRowNum+1);
  PhaseSpinBoxes *phaseSBs = new PhaseSpinBoxes(clockCycles, high);
  t->setCellWidget(newRowNum, 0, phaseSBs->fastClockCyclesSB);
  t->setCellWidget(newRowNum, 1, phaseSBs->slowClockCyclesLabel);
  t->setCellWidget(newRowNum, 2, phaseSBs->phaseTypeSB);
  t->phasesSBs.push_back(phaseSBs);
}


void Sequencer_GUI::addPhaseToCurrentTrack(bool high, int clockCycles)
{
  int n = trackSelect->currentIndex();
  addPhase(Sequencer::TrackNum(n), high, clockCycles);
  updateMissingCycles();
}


void Sequencer_GUI::rmLastPhase()
{
  QPhasesTable* t = trackWidgets[trackSelect->currentIndex()]->phasesTable;
  t->setRowCount(t->rowCount()-1);
  t->phasesSBs.pop_back();
}


bool Sequencer_GUI::updateAndCompileSequencer()
{
  bool success = true;

  if (Sequencer::getHoldEnabled()) {
    holdGenWidget->setHoldGenerator();
  }

  for (int t=0; t<Sequencer::c_numOfTracks; ++t) {

    SequencerTrack& track = *sequencer->getTrack((Sequencer::TrackNum)t);
    if (manModeRB->isChecked()) {
      trackWidgets[t]->putPartSeqs(track.compiledPartSeqs);
      //track.reverseCompile();
      //track.checkCompiledPartSeqs();
      track.setStatVal((bool)trackWidgets[t]->getStatVal());
      track.setInvHold((bool)trackWidgets[t]->getInvHold());
    }
    else if ( trackCompilerModeRB->isChecked() ) {
      QPhasesTable* table = trackWidgets[t]->phasesTable;
      SequencerTrack::SequencePhases phases;
      table->getSequencePhases(phases);
      track.setPhases(phases);
      track.setStatVal((bool)trackWidgets[t]->getStatVal());
      track.setInvHold((bool)trackWidgets[t]->getInvHold());
      //updateMissingCycles();
    }
  }
  if (!manModeRB->isChecked()) {
    sequencer->compileAllTracks();
  }


  if (!success) {
    SuS_LOG(error, log_id(), "Error during update of sequencer.");
  }

  //if(!manualPhasesRB->isChecked()) {
  //  updateGUI();
  //}

  // must be AFTER parameters in the sequencer instance are set because it
  // adjusts the parameters and writes them back into the GUI. a manual
  // change of a parameter is hence voided
  setCycleLength( cycleLengthSB->value() );

  manualOpModeChange = false;

  updateDrawArea();
  emit cycleLengthUpdated(Sequencer::getCycleLength());

  return success;
}


void Sequencer_GUI::compileAndProgram()
{
  updateAndCompileSequencer();
  emit programSequencer();
  //if(!manualPhasesRB->isChecked()) {
  //  updateGUI();
  //}
}


void Sequencer_GUI::emitReadBackSequencer()
{
  updateAndCompileSequencer();
  emit readBackSequencer();
}


void Sequencer_GUI::initTables()
{
  for (int i=0; i<Sequencer::c_numOfTracks; ++i) {
    trackWidgets[i] = new SequencerTrackWidget( sequencer->getTrack( Sequencer::TrackNum( i ) ) );
    connect(manModeRB, SIGNAL(toggled(bool)), trackWidgets[i], SLOT(setManualMode(bool)));
    trackWidgets[i]->setManualMode(false);
  }
}


void Sequencer_GUI::updateGUI()
{
  SuS_LOG(info, log_id(), "Updating sequencer GUI.");
  //signalCompilerModeRB->setChecked(sequencer->getSignalCompilerMode());

  const Sequencer::ProgMode currProgMode = sequencer->getProgMode();
  switch (currProgMode) {
//    case Sequencer::NORM      : normalModeRB->setChecked(true);
//                                SuS_LOG(info, log_id(), "Sequencer in normal (double integration) mode.");
//                                break;
//    case Sequencer::SINGLEINT : singleIntegrationRB->setChecked(true);
//                                SuS_LOG(info, log_id(), "Sequencer in single integration mode.");
//                                break;
//    case Sequencer::BUFFER    : bufferModeRB->setChecked(true);
//                                SuS_LOG(info, log_id(), "Sequencer in buffer mode.");
//                                break;
//    case Sequencer::RESET     : resetValueRB->setChecked(true);
//                                SuS_LOG(info, log_id(), "Sequencer in reset mode.");
//                                break;
//    case Sequencer::EXTLATCH  : extLatchModeRB->setChecked(true);
//                                SuS_LOG(info, log_id(), "Sequencer in ext latch mode.");
//                                break;
    default : //manualPhasesRB->setChecked(true);
              SuS_LOG(info, log_id(), "TODO: Sequencer in manual mode.");
  }


  if(sequencer->isGood())
  {
    //filenameLabel->setText(sequencer->getFilename());
    //int integrationTime = sequencer->getIntegrationTime();

    cycleLengthSB->setValue(sequencer->getCycleLength());

    //singleCapModeCB->setChecked(sequencer->singleSHCapMode);

    //compResetLengthSB->setValue(sequencer->resetLength);
    //compResetIntegOffsetSB->setValue(sequencer->resetIntegOffset);
    //compIntegrationTimeSB->setValue(sequencer->integrationLength);
    //compFlattopLengthSB->setValue(sequencer->flattopLength);
    //compFlattopHoldLengthSB->setValue(sequencer->flattopHoldLength);
    //compResetHoldLengthSB->setValue(sequencer->resetHoldLength);
    //compRampLengthSB->setValue(sequencer->rampLength);
    //compRampIntOffsetSB->setValue(sequencer->rampIntegOffset);
    //compBackFlipAtResetSB->setValue(sequencer->backFlipAtReset);
    //compBackFlipAtResetSB->setDisabled(sequencer->isSingleSHCapMode());
    //compBackFlipToResetOffsetSB->setValue(sequencer->backFlipToResetOffset);
    //compSingleCapLoadLengthSB->setValue(sequencer->singleCapLoadLength);

    //// this is the track compiler integration time

    //if(sequencer->backFlipAtReset == 2){
    //  compBackFlipToResetOffsetSB->setRange(0,sequencer->getMaxBackFlipToResetOffset());
    //}else{
    //  compBackFlipToResetOffsetSB->setRange(0,sequencer->getRmpPos()-1);
    //}

    //enSignalCompilerMode(sequencer->signalsCompilerMode);

    for (int i=0; i<Sequencer::c_numOfTracks; ++i) {
      SequencerTrack& track = *sequencer->getTrack(Sequencer::TrackNum(i));
      trackWidgets[i]->statValSB->setValue(track.getStatVal());
      trackWidgets[i]->invHoldSB->setValue(track.getInvHold());
      QPhasesTable* t = trackWidgets[i]->phasesTable;
      t->clearPhases();
      t->setRowCount(0);
      const SequencerTrack::SequencePhases& phases = track.getPhases();
      for (uint j=0; j<phases.size(); ++j) {
        addPhase((Sequencer::TrackNum)i, phases[j].high, phases[j].clockCycles);
      }
      trackWidgets[i]->setPartSeqTable(track.compiledPartSeqs);
      trackWidgets[i]->clkCB->setChecked(track.isClockMode());
    }

    //updateHoldGUI();
    //updateMissingCycles();
  }
  else {
    SuS_LOG(info, log_id(), "No sequencer loaded.");
  }

  updateDrawArea();
}

void Sequencer_GUI::updateHoldGUI()
{
  int firstHoldPos=0,firstHoldLength=0,secondHoldPos=0,secondHoldLength=0;
  sequencer->findHoldPositions(firstHoldPos,firstHoldLength,secondHoldPos,secondHoldLength);
  if(sequencer->holdCnts.size() != (uint)Sequencer::c_holdCntsRegDepth){
    SuS_LOG_STREAM(warning, log_id(), "Sequencer corrupted. Wrong hold register count detected: "
        << sequencer->holdCnts.size() << " Set to "<< Sequencer::c_holdCntsRegDepth);
    sequencer->setZeroHolds();
  }

  for (int i=0; i<Sequencer::c_holdCntsRegDepth; ++i) {
    holdGenWidget->holdCnts[i]->holdPhaseSB->setValue(sequencer->holdCnts[i].hold);
    holdGenWidget->holdCnts[i]->slowClockCyclesSB->setValue(sequencer->holdCnts[i].length);
  }
  holdGenWidget->secondPosSlider-> setValue(secondHoldPos);
  holdGenWidget->firstPosSlider -> setValue(firstHoldPos);
  holdGenWidget->secondLengthSB-> setValue(secondHoldLength);
  holdGenWidget->firstLengthSB -> setValue(firstHoldLength);

  holdEnableCB->setChecked(sequencer->getHoldEnabled());

}


void  Sequencer_GUI::updateMissingCycles()
{
  if(trackSelect->currentIndex() >= trackWidgets.size()){
    QMessageBox::warning(this,
                         tr("Sequencer GUI"),
                         tr("Error track select current Index out of bounds\n"),
                         QMessageBox::Ok,
                         QMessageBox::Ok);
    return;
  }

  QPhasesTable* table = trackWidgets.at(trackSelect->currentIndex())->phasesTable;
  int cycles = cycleLengthSB->value()*SequencerTrack::c_partSeqWidth;
  for(PhaseSpinBoxes * phases : table->phasesSBs){
     int clockCycles = (int)(phases->fastClockCyclesSB->value());
     cycles -= clockCycles;
  }

  // int maxIntTime = sequencer->getMaxIntegrationTime();
  // missingCyclesLabel->setText(QString::number(cycles) + " | " +QString::number(maxIntTime));
}


void Sequencer_GUI::changeTrackTable(int index)
{
  if(trackSelect->currentIndex()>=0){
    for (int i=0; i<Sequencer::c_numOfTracks; ++i){
      trackWidgets[i]->hide();
    }
    trackWidgets[trackSelect->currentIndex()]->show();
  }
}


void Sequencer_GUI::setStatVal(Sequencer::TrackNum n, bool val)
{
  trackWidgets[n]->statValSB->setValue(val ? 1 : 0);
}


void Sequencer_GUI::saveAsFile()
{
  if (!updateAndCompileSequencer()) {
    SuS_LOG(error, log_id(), "Error during compilation of sequences, nothing saved.");
    SuS_LOG(warning, log_id(), "Saving file nevertheless.");
  }

  QString filename = QFileDialog::getSaveFileName(
                      this,
                      "Save Sequencer",
                      QDir::currentPath(),
                      "XML file (*.xml)");

  if (filename.isNull()) {
      SuS_LOG(error, log_id(), "File not saved");
      return;
  }
  sequencer->saveToFile(filename);
  filenameLabel->setText(filename);
}


void Sequencer_GUI::saveFile()
{
  if (!updateAndCompileSequencer()) {
    SuS_LOG(error, log_id(), "Error during compilation of sequences.");
    SuS_LOG(warning, log_id(), "Saving file nevertheless.");
  }
  sequencer->saveToFile();
}


void Sequencer_GUI::saveToFile(QString filename)
{
  if (!updateAndCompileSequencer()) {
    SuS_LOG(error, log_id(), "Error during compilation of sequences.");
    SuS_LOG(warning, log_id(), "Saving file nevertheless.");
  }

  sequencer->saveToFile(filename);
  filenameLabel->setText(filename);

}


void Sequencer_GUI::loadFile()
{
  QString path = QDir::currentPath()+QString::fromStdString("/../ConfigFiles/");
  //SuS_LOG(debug, log_id(), path.toStdString());

  QString filename = QFileDialog::getOpenFileName(
                      this,
                      "Load Sequencer",
                      QDir::currentPath()+QString::fromStdString("/../ConfigFiles/"),
                      "XML file (*.xml)");

  if (filename.isNull()) {
      SuS_LOG(error, log_id(), "File not found!");
      return;
  }

  loadFile(filename);
}


void Sequencer_GUI::reLoadFile()
{
  SuS_LOG(info, log_id(), "Reload sequencer file.");
  loadFile(sequencer->getFilename());
}


void Sequencer_GUI::loadFile(QString filename)
{
  manualOpModeChange = false;
  if (!sequencer->loadFile(filename)) {
    SuS_LOG(error, log_id(), "Error while loading sequencer file.");
  } else {
    updateGUI();
  }
  emit newFileLoaded();
}


void Sequencer_GUI::slotEnableSimMode(bool en)
{
  //simBtns->setVisible(en);
}


void Sequencer_GUI::setDefaultSequence()
{
  SuS_LOG(info, log_id(), "Sequencer set to default sequence!");

  int cycleLength = cycleLengthSB->value();
  int fastClockCycles = cycleLength*SequencerTrack::c_partSeqWidth;

  int rampLength = ((fastClockCycles-10) > 250 )? 250 : fastClockCycles -10;
  if(singleCapModeCB->isChecked()){
    if(rampLength > 140) rampLength = 140;
  }

  compRampLengthSB       -> setValue(rampLength);
  compResetLengthSB      -> setValue(23);
  compResetIntegOffsetSB -> setValue(2);
  compIntegrationTimeSB  -> setValue(35);
  compFlattopLengthSB    -> setValue(30);
  compRampIntOffsetSB    -> setValue(5);

  //sequencer->generateSignals( compIntegrationTimeSB->value(),
  //                            compFlattopLengthSB->value(),
  //                            compFlattopHoldLengthSB->value(),
  //                            compResetLengthSB->value(),
  //                            compResetIntegOffsetSB->value(),
  //                            compResetHoldLengthSB->value(),
  //                            compRampLengthSB->value(),
  //                            compRampIntOffsetSB->value(),
  //                            0,
  //                            0,
  //                            11
  //                            );

  updateAndCompileSequencer();
}


void Sequencer_GUI::setSimMode(bool simMode)
{
  //simBtns->setVisible(simMode);
}


void Sequencer_GUI::disableGUIUpdate()
{
}



void Sequencer_GUI::mouse_event_filter(int type, int x, int y)
{
  /*
  static int lastXpos;

  static int valueReminder = 0;

  static int trackNr = 0;
  static int transNr = 0;

  if(type == 1){

    int cycleLength = sequencer->getCycleLength() * 7;

    if(x<150 || x> 150+cycleLength) return;

    trackNr = -1;
    transNr = -1;

    lastXpos = x;

//    qDebug() << "lastYPos = " << lastYpos << " WidgetPos = " << drawLabel->pos().y();
//    qDebug() << "lastYPos = " << lastXpos << " WidgetPos = " << drawLabel->pos().x();

    int sequencePos = x-150;

    if(y>26 && y < 53){
      trackNr = 0;

      int rmpPos = sequencer->getRmpPos();
      int rmpEnd = sequencer->getRmpEnd();

      int dist1 = sequencePos-rmpEnd;
      if(dist1<0) dist1 = -dist1;
      int dist2 = sequencePos-rmpPos;
      if(dist2<0) dist2 = -dist2;

      if(dist1<dist2){
        transNr=1;
        valueReminder = sequencer->getRampLength();
      }else{
        transNr=2;
        valueReminder = sequencer->getRampIntegOffset();
      }

    }else if(y < 83){
      trackNr = 1;

      int firstIntegOffset = sequencer->getFirstIntegOffset();
      int integrationLength = sequencer->getIntegrationTime();

      int dist1 = sequencePos-firstIntegOffset;
      if(dist1<0) dist1 = -dist1;
      int dist2 = sequencePos-integrationLength-firstIntegOffset;
      if(dist2<0) dist2 = -dist2;

      if(dist1<dist2){
        transNr = 1;
        valueReminder = integrationLength;
      }else{
        transNr = 2;
        valueReminder = sequencer->getFlattopLength();
      }
//      qDebug() << " dist1: " << dist1 << " dist2: " << dist2;
    }else if(y < 113){
      trackNr = 2;
      valueReminder = sequencer->getResetIntegOffset();
    }else if(y < 143){
      trackNr = 3;
    }else if(y < 173){
      trackNr = 4;
    }
//    qDebug() << "change Track nr " << trackNr ;
    trackSelect->setCurrentIndex(trackNr);

  }else if(type == 2){
    Sequencer::OpMode mode = sequencer->getOpMode();

    int diffX = x - lastXpos;
    if(trackNr == 0){
      if(sequencer->signalsCompilerMode){
        if(transNr==1){
          compRampLengthSB->setValue(valueReminder+diffX);
        }else if(transNr==2){
          compRampIntOffsetSB->setValue(valueReminder+diffX);
        }
      }
    }else if(trackNr == 1 && mode == Sequencer::NORM){
      if(sequencer->signalsCompilerMode){
        if(transNr==1){
          compIntegrationTimeSB->setValue(valueReminder-diffX);
        }else if(transNr == 2){
          compFlattopLengthSB->setValue(valueReminder-diffX);
        }
      }
    }else if(trackNr == 2 && (mode != Sequencer::BUFFER) ){
      if(sequencer->signalsCompilerMode){
        compResetIntegOffsetSB->setValue(valueReminder-diffX);
      }
    }else{
      return;
    }

    updateAndCompileSequencer();

//    qDebug() << "lastYPos = " << lastXpos << " diff = " << diffX;
  }
*/
}


void Sequencer_GUI::generateLayout()
{
  //============================ MODE SELECTION ===================================================
  //
  QHBoxLayout* selLayout   = new QHBoxLayout;
  signalCompilerModeRB     = new QRadioButton("Signals Compiler Mode");
  trackCompilerModeRB      = new QRadioButton("Track Compiler Mode");
  manModeRB                = new QRadioButton("Direct Mode");
  QButtonGroup* selectBG   = new QButtonGroup;
  auto updateGuiBtn        = new QPushButton("Update Gui");
  selectBG->addButton(signalCompilerModeRB);
  selectBG->addButton(trackCompilerModeRB);
  selectBG->addButton(manModeRB);
  selLayout->addWidget(signalCompilerModeRB);
  selLayout->addWidget(trackCompilerModeRB);
  selLayout->addWidget(manModeRB);
  selLayout->addWidget(updateGuiBtn);
  selLayout->addStretch();

  signalCompilerModeRB->setEnabled( false );

  connect(updateGuiBtn, SIGNAL(clicked()),this,SLOT(updateGUI()));


  //============================ FILE ACCESS ======================================================
  //
  QHBoxLayout *fileLayout = new QHBoxLayout;
  QPushButton *saveAsFileBtn = new QPushButton("Save &As");
  saveAsFileBtn->setMinimumWidth(150);
  QPushButton *saveFileBtn   = new QPushButton("&Save");
  saveFileBtn->setMinimumWidth(150);
  QPushButton *loadFileBtn   = new QPushButton("&Load");
  loadFileBtn->setMinimumWidth(150);
  QPushButton *reLoadFileBtn   = new QPushButton("&Reload");
  reLoadFileBtn->setMinimumWidth(150);
  reLoadFileBtn->setMinimumWidth(150);
  fileLayout->addWidget(saveAsFileBtn);
  fileLayout->addWidget(saveFileBtn);
  fileLayout->addWidget(loadFileBtn);
  fileLayout->addWidget(reLoadFileBtn);
  fileLayout->addStretch();

  filenameLabel = new QLabel;
  QHBoxLayout *fileNameLayout = new QHBoxLayout;
  fileNameLayout->addStretch();
  fileNameLayout->addWidget(filenameLabel);

  //============================ COMMON STUFF =====================================================
  //
  QHBoxLayout *commonLayout = new QHBoxLayout;
  cycleLengthSB = new QSpinBox;
  cycleLengthSB->setRange(1,4095);
  cycleLengthSB->setAlignment(Qt::AlignRight);
  cycleLengthSB->setToolTip("CycleLength between 1 and 4095 possible<br>"
                            "@700MHz: CycleFreq = 100 / CycleLength<br>"
                            "- 1.0 MHz CycleLength = 100<br>"
                            "- 2.0 MHz CycleLength = 50<br>"
                            "- 4.5 MHz CycleLength = 22<br>"
                            "- 5.0 MHz CycleLength = 20<br>");

  commonLayout->addWidget(new QLabel("Cycle Length"));

  commonLayout->addWidget(cycleLengthSB);
  commonLayout->addStretch();

  connect(cycleLengthSB,SIGNAL(valueChanged(int)),this,SLOT(emitCycleLengthChanged()));

  //============================ TRACK COMPILER WIDGET ============================================
  //
  tracksCompilerBox = new QGroupBox;
  tracksCompilerBox->setToolTip("To measure baseline current stability <b>enable 'Manual Phases'</b> and <b>remove second integration phase (SwIn)</b>.<br>The stability over a burst can be checked in the display widget");
  QVBoxLayout *trackCompLayout = new QVBoxLayout;
  tracksCompilerBox->setLayout(trackCompLayout);

  //missingCyclesLabel = new QLabel;
  //missingCyclesLabel->setText("Missing Cycles: not loaded");
  //missingCyclesLabel->setToolTip("Missing Cycles | Max IntegrationTime");
  //integTimeLayout-> addWidget(missingCyclesLabel);

  trackSelect = new QComboBox;
  trackSelect->addItems(sequencer->getSequencerTrackNames());

  trackSelect->setToolTip(tr("<b> Sequencer Track QuickManual:</b><br>"
                             " - <b>RMP</b>    : To measure VHOLD: set first rising edge before reset goeas low.<br>"
                             "                   Wait min 15ns after last integration phase before RMP->1. <b>Stat0</b><br>"
                             " - <b>SwIn</b>   : Two Integration Phases (sig high) of equal length. Between integration phases flip signal must change state. <b>Stat1</b><br>"
                             " - <b>RES_N</b>  : Low Active. Should not be smaller than 14. If set larger than 255, 9th bit of ADC is disabled. <b>Stat1</b><br>"
                             " - <b>FlipInj</b>: Must be one during reset. PxInjection is active if signal is low.<br>"
                             "                   Must not change state between last integration phase and RMP high. <b>Stat0</b><br>"
  ));

  QHBoxLayout * seqBoxLayout = new QHBoxLayout;

  QPushButton *enableMonitorOutputBtn = new QPushButton("Enable Monitor");
  enableMonitorOutputBtn->setToolTip("Activate monitor output for selected track");

  seqBoxLayout-> addWidget(trackSelect);
  seqBoxLayout-> addStretch();
  //seqBoxLayout-> addWidget(enableMonitorOutputBtn);
  connect(enableMonitorOutputBtn, SIGNAL(clicked()),this,SLOT(sendSetMonitorOutputSignal()));

  trackCompLayout->addLayout(seqBoxLayout);

  trackWidget = new QWidget;
  QVBoxLayout* trackWidgetLayout = new QVBoxLayout(trackWidget);
  trackCompLayout->addWidget(trackWidget);
  initTables();
  //trackWidgetLayout->addLayout(integTimeLayout);
  for (int i=0; i<Sequencer::c_numOfTracks; ++i) {
    trackWidgetLayout->addWidget(trackWidgets[i]);
  }
  changeTrackTable(trackSelect->currentIndex());
  QHBoxLayout *phaseBtnsL = new QHBoxLayout;
  QPushButton *addPhaseBtn = new QPushButton("&Add Phase");
  QPushButton *rmPhaseBtn = new QPushButton("&Remove Last Phase");
  phaseBtnsL->addWidget(addPhaseBtn);
  phaseBtnsL->addWidget(rmPhaseBtn);
  trackWidgetLayout->addLayout(phaseBtnsL);

  //============================ SIGNALS COMPILER WIDGET ==========================================
  //
  signalsCompilerBox = new QGroupBox("Signals Compiler");
  QVBoxLayout *signalsCompVLayoutForStretch = new QVBoxLayout;
  QHBoxLayout *signalsCompHLayoutForStretch = new QHBoxLayout;
  signalsCompVLayoutForStretch->addLayout(signalsCompHLayoutForStretch);
  signalsCompVLayoutForStretch->addStretch();
  QGridLayout* signalsCompLayout = new QGridLayout;
  signalsCompHLayoutForStretch->addLayout(signalsCompLayout);
  signalsCompHLayoutForStretch->addStretch();
  signalsCompilerBox->setLayout(signalsCompVLayoutForStretch);

  QPushButton *setDefaultSeqBtn   = new QPushButton("Default Sequence");
  setDefaultSeqBtn->setMinimumWidth(150);
  setDefaultSeqBtn->setToolTip("If sequence is broken use this button to reset the sequence to default values");

  compRampLengthSB             = new QSpinBox;
  compResetLengthSB            = new QSpinBox;
  compResetIntegOffsetSB       = new QSpinBox;
  compIntegrationTimeSB        = new QSpinBox;
  compFlattopLengthSB          = new QSpinBox;
  compFlattopHoldLengthSB      = new QSpinBox;
  compResetHoldLengthSB        = new QSpinBox;
  compRampIntOffsetSB          = new QSpinBox;
  compBackFlipAtResetSB        = new QSpinBox;
  compBackFlipToResetOffsetSB  = new QSpinBox;
  compSingleCapLoadLengthSB    = new QSpinBox;

  sequencerParameterCB         = new QComboBox;
  sequencerParameterCB->addItems(QConfigReg::toQList(sequencer->getParameterNames()));

  sequencerParameterSB         = new QSpinBox;
  sequencerParameterSB->setRange(0,1000);
  sequencerParameterSB->setValue(sequencer->getSequencerParameter(sequencerParameterCB->currentText().toStdString()));
  connect(sequencerParameterCB,SIGNAL(currentIndexChanged(const QString&)),this,SLOT(updateSequencerParameter(const QString&)));

  QPushButton* setSequencerParameterBtn = new QPushButton("Set");
  connect(setSequencerParameterBtn,SIGNAL(clicked()),this,SLOT(setSequencerParameter()));

  autoCompileCB = new QCheckBox("Auto Compile");

  compSingleCapLoadLabel = new QLabel("Single Cap Load Length");
  compSingleCapLoadLabel->setVisible(false);
  compSingleCapLoadLengthSB->setVisible(false);

  int row = 0;
  signalsCompLayout->addWidget(setDefaultSeqBtn,row++,2,1,2);
  signalsCompLayout->addWidget(new QLabel("ADC Ramp Length (*1.44ns)"),row,0);
  signalsCompLayout->addWidget(compRampLengthSB,row++,1);
  signalsCompLayout->addWidget(new QLabel("Reset Length (*1.44ns)"),row,0);
  signalsCompLayout->addWidget(compResetLengthSB,row,1);
  signalsCompLayout->addWidget(new QLabel("Reset Hold (*10ns)"),row,2);
  signalsCompLayout->addWidget(compResetHoldLengthSB,row++,3);
  signalsCompLayout->addWidget(new QLabel("Reset to Integration Offset (*1.44ns)"),row,0);
  signalsCompLayout->addWidget(compResetIntegOffsetSB,row++,1);
  signalsCompLayout->addWidget(new QLabel("Integration Time (*1.44ns)"),row,0);
  signalsCompLayout->addWidget(compIntegrationTimeSB,row++,1);
  signalsCompLayout->addWidget(new QLabel("Flattop Length (*1.44ns)"),row,0);
  signalsCompLayout->addWidget(compFlattopLengthSB,row,1);
  signalsCompLayout->addWidget(new QLabel("Flattop Hold (*10ns)"),row,2);
  signalsCompLayout->addWidget(compFlattopHoldLengthSB,row++,3);
  signalsCompLayout->addWidget(new QLabel("Ramp Offset"),row,0);
  signalsCompLayout->addWidget(compRampIntOffsetSB,row++,1);
  signalsCompLayout->addWidget(new QLabel("Back Flip At Reset"),row,0);
  signalsCompLayout->addWidget(compBackFlipAtResetSB,row++,1);
  signalsCompLayout->addWidget(new QLabel("Back Flip To Reset Offset"),row,0);
  signalsCompLayout->addWidget(compBackFlipToResetOffsetSB,row++,1);
  signalsCompLayout->addWidget(compSingleCapLoadLabel,row,0);
  signalsCompLayout->addWidget(compSingleCapLoadLengthSB,row++,1);
  signalsCompLayout->addWidget(sequencerParameterCB,row,0);
  signalsCompLayout->addWidget(sequencerParameterSB,row,1);
  signalsCompLayout->addWidget(setSequencerParameterBtn,row,2);
  signalsCompLayout->addWidget(autoCompileCB,row++,3);

  compBackFlipAtResetSB->setToolTip("0 -> flip occurs at beginning of period (should be best for VHOLD) <br>"
                                    "1 -> flip occurs with at negedge (start) of reset pulse. <br>"
                                    "2 -> makes the \"Back Flip To Reset Offset\" valid.");
  compBackFlipToResetOffsetSB->setToolTip("Sets the time between FCF back flip and reset. <br>"
                                          "Only valid if \"Back Flip At Reset\" = 1");

  holdGenWidget = new HoldGeneratorWidget(sequencer);
  holdGenWidget->setVisible(false);

  //============================ SEQUENCE DRAWING =================================================
  //
  QPixmap* pixmap = new QPixmap(200,200);
  pixmap->fill();
  drawLabel = new SeqDrawLabel();
  drawLabel->setPixmap(*pixmap);
  delete pixmap;
  QHBoxLayout *seqDrawLayout = new QHBoxLayout;
  seqDrawLayout->addWidget(drawLabel);

  //connect(drawLabel,SIGNAL(mouse_pos(int,int,int)),this,SLOT(mouse_event_filter(int,int,int)));
  //connect(drawLabel,SIGNAL(mouse_pressed(int,int,int)),this,SLOT(mouse_event_filter(int,int,int)));

  //============================ COMPILATION AND PROGRAMMING ======================================
  //
  QHBoxLayout *compileBtnsLayout = new QHBoxLayout;
  QPushButton *saveImageBtn = new QPushButton("Save High-Res Image");
  QPushButton *compileBtn = new QPushButton("&Compile");
  QPushButton *compileAndProgramBtn = new QPushButton("Compile And &Program");
  QPushButton *readBackBtn = new QPushButton("Read &Back");
  compileBtnsLayout->addWidget(saveImageBtn);
  compileBtnsLayout->addWidget(compileBtn);
  //compileBtnsLayout->addWidget(compileAndProgramBtn);  // not functional in python environmant
  compileBtnsLayout->addWidget(readBackBtn);
  //compileBtnsLayout->addStretch();

  //============================ SIMULATION BUTTONS ===============================================
  //
  simBtns = new QGroupBox("Simulation Button");
  QHBoxLayout *simBtnsLayout     = new QHBoxLayout;
  simBtns->setLayout(simBtnsLayout);
  QPushButton *sweepRampBtn = new QPushButton("Sweep &RMP");
  QPushButton *sweepFlatTopBtn = new QPushButton("Sweep &Flattop");
  QPushButton *sweepIntegTimeBtn = new QPushButton("Sweep &Integration Time");
  QPushButton *sweepHoldPosBtn   = new QPushButton("Sweep &Hold Position");
  simBtnsLayout->addWidget(sweepRampBtn);
  simBtnsLayout->addWidget(sweepFlatTopBtn);
  simBtnsLayout->addWidget(sweepIntegTimeBtn);
  simBtnsLayout->addWidget(sweepHoldPosBtn);

  //============================ MAIN WIDGET LAYOUT ===============================================
  //
  QHBoxLayout *compilerLayout = new QHBoxLayout;
  //compilerLayout->addWidget(signalsCompilerBox);
  compilerLayout->addWidget(tracksCompilerBox);
  compilerLayout->addWidget(holdGenWidget);
  compilerLayout->addLayout(seqDrawLayout);
  compilerLayout->addStretch();

  QVBoxLayout *mainLayout = new QVBoxLayout;
  this->setLayout(mainLayout);
  mainLayout->addLayout(fileLayout);
  mainLayout->addLayout(fileNameLayout);
  mainLayout->addLayout(selLayout);
  mainLayout->addLayout(commonLayout);
  mainLayout->addLayout(compilerLayout);
  //mainLayout->addLayout(splitLayout);
  //mainLayout->addWidget(holdGenWidget);
  //mainLayout->addLayout(seqDrawLayout);
  mainLayout->addLayout(compileBtnsLayout);
  //mainLayout->addWidget(simBtns);
  mainLayout->addStretch();

  //signalsCompilerBox->setVisible(sequencer->signalsCompilerMode);
  //tracksCompilerBox->setVisible(true);
  //signalCompilerModeRB->setChecked(sequencer->signalsCompilerMode);
  //============================ SIGNAL CONNECTIONS ===============================================
  //
  connect(addPhaseBtn, SIGNAL(clicked()), this, SLOT(addPhaseToCurrentTrack()));
  connect(rmPhaseBtn, SIGNAL(clicked()), this, SLOT(rmLastPhase()));
  connect(saveImageBtn, SIGNAL(clicked()), this, SLOT(saveImage()));
  connect(compileBtn, SIGNAL(clicked()), this, SLOT(updateAndCompileSequencer()));
  connect(compileAndProgramBtn, SIGNAL(clicked()), this, SLOT(compileAndProgram()));
  connect(readBackBtn, SIGNAL(clicked()), this, SLOT(emitReadBackSequencer()));
  connect(trackSelect, SIGNAL(currentIndexChanged(int)), this, SLOT(changeTrackTable(int)));
  connect(saveAsFileBtn, SIGNAL(clicked()), this, SLOT(saveAsFile()));
  connect(saveFileBtn, SIGNAL(clicked()), this, SLOT(saveFile()));
  connect(loadFileBtn, SIGNAL(clicked()), this, SLOT(loadFile()));
  connect(reLoadFileBtn, SIGNAL(clicked()), this, SLOT(reLoadFile()));
  connect(updateGuiBtn, SIGNAL(clicked()), this, SLOT(updateGUI()));
  connect(setDefaultSeqBtn, SIGNAL(clicked()), this, SLOT(setDefaultSequence()));
  connect(sequencer, SIGNAL(contentChanged()), this, SLOT(updateGUI()));
  connect(sequencer, SIGNAL(filenameChanged(QString)), filenameLabel, SLOT(setText(QString)));
  //connect(holdEnableCB, SIGNAL(toggled(bool)), this, SLOT(holdEnableToggled(bool)));
  //connect(cycleLengthSB, SIGNAL(valueChanged(int)), this, SLOT(setCycleLength(int)));
  connect(signalCompilerModeRB, SIGNAL(toggled(bool)),this,SLOT(enSignalCompilerMode(bool)));

  trackCompilerModeRB->setChecked(true);
}


QPixmap* Sequencer_GUI::generateTracksMap(int scaleFactor)
{
  //Origin of draw area is in upper left corner
  const int cycleLength = sequencer->getCycleLength();

  const int phasesOffsetX = scaleFactor*100;
  const int pixmapWidth = phasesOffsetX + scaleFactor*(cycleLength*SequencerTrack::c_partSeqWidth + 5);

  QPixmap* pixmap = new QPixmap(pixmapWidth,scaleFactor*400);
  pixmap->fill();
  QPainter* painter = new QPainter(pixmap);

  //painter->setPen(Qt::black);
  painter->setPen(QPen(Qt::black, scaleFactor, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin));

  QFont font = painter->font() ;

  font.setPointSize(10);
  /* twice the size than the current font size */
  font.setPointSize(font.pointSize() * scaleFactor);
  /* set the modified font to the painter */
  painter->setFont(font);

  auto clkDiv = SequencerTrack::c_partSeqWidth; 
  float clkPer = 2.5;
  if (sequencer->getHoldEnabled()){
    int firstPos, firstLength, secondPos, secondLength;
    sequencer->findHoldPositions(firstPos, firstLength, secondPos, secondLength);
    painter->drawText(scaleFactor*10,scaleFactor*15,"Sequence length: " 
        + QString::number(cycleLength*clkDiv*clkPer) + "ns, with hold: " 
        + QString::number((firstLength+secondLength+cycleLength)*clkDiv*clkPer)+"ns"
        + " @ FastClk = " + QString::number(1/clkPer*1000) + " MHz");
  }else{
    painter->drawText(scaleFactor*10,scaleFactor*15,"Sequence length: " 
        + QString::number(cycleLength*clkDiv*clkPer) + "ns"
        + " @ FastClk = " + QString::number(1/clkPer*1000) + " MHz");
  }


  for (int i=0; i<Sequencer::c_numOfTracks; ++i) {
    //qDebug() << "Track number: " << i;
    int trackOffsetY = (i+1)*30*scaleFactor;
    SequencerTrack& track = *sequencer->getTrack(Sequencer::TrackNum(i));
    const SequencerTrack::SequencePhases& phases = track.getPhases();

    QString trackName = sequencer->trackNumToQName(Sequencer::TrackNum(i));
    painter->setPen(Qt::black);
    painter->drawText(scaleFactor*10,trackOffsetY+scaleFactor*15,trackName);

    //painter->setPen(i%2 ? Qt::red : Qt::green);
    painter->setPen(QPen(i%2 ? Qt::red : Qt::green, scaleFactor, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin));
    int x = phasesOffsetX;
    for (unsigned int j=0; j<phases.size(); ++j) {
      //qDebug() << "j: " << j << " " << (phases[j].high ? "1 ":"0 ") << phases[j].clockCycles;
      if (phases[j].clockCycles < 0)
      {
        delete painter;
        delete pixmap;
        QPixmap* pixmap = new QPixmap(pixmapWidth,200);
        pixmap->fill();
        QPainter* painter = new QPainter(pixmap);
        painter->setPen(Qt::black);
        painter->drawText(50,50,"Negative clock cycles encountered. Check sequence.");
        return pixmap;
      }
      int phaseOffsetY = phases[j].high ? 0 : scaleFactor*20;

      int startX = x;
      int startY = trackOffsetY+phaseOffsetY;
      int endX = x+scaleFactor*(phases[j].clockCycles);
      int endY = trackOffsetY+phaseOffsetY;
      //qDebug() << startX << ";" << startY << ";" << endX << ";" << endY;
      //draw the horizontal line for the phase
      painter->drawLine(startX,startY,endX,endY);
      if (j<phases.size()-1){ //draw the vertical lines at signal transitions
        if(phases[j].high != phases[j+1].high){
          painter->drawLine(endX,trackOffsetY,endX,trackOffsetY+scaleFactor*20);
        }
      }
      x=endX;
    }
  }

  //draw vertical lines at hold positions
  if (sequencer->getHoldEnabled())
  {
    int firstPos, firstLength, secondPos, secondLength;
    sequencer->findHoldPositions(firstPos, firstLength, secondPos, secondLength);

    painter->setPen(QPen(Qt::black, scaleFactor, Qt::SolidLine, Qt::SquareCap, Qt::BevelJoin));
    painter->drawLine(phasesOffsetX+scaleFactor*firstPos*SequencerTrack::c_partSeqWidth,scaleFactor*30,phasesOffsetX+scaleFactor*firstPos*SequencerTrack::c_partSeqWidth,scaleFactor*170);
    if (secondPos < cycleLength +1) painter->drawLine(phasesOffsetX+scaleFactor*(secondPos-1)*SequencerTrack::c_partSeqWidth,scaleFactor*30,phasesOffsetX+scaleFactor*(secondPos-1)*SequencerTrack::c_partSeqWidth,scaleFactor*170);
    painter->drawText(phasesOffsetX+scaleFactor*(firstPos*SequencerTrack::c_partSeqWidth-14),scaleFactor*185,"Hold");
    if (secondPos < cycleLength +1) painter->drawText(phasesOffsetX+scaleFactor*((secondPos-1)*SequencerTrack::c_partSeqWidth-14),scaleFactor*185,"Hold");
    if(Sequencer::debugMode){
      SuS_LOG_STREAM(info, log_id(), "firstPos = " << firstPos);
      SuS_LOG_STREAM(info, log_id(), "secondPos = " << secondPos);
    }
  }

  delete painter;
  return pixmap;
}


void Sequencer_GUI::saveImage()
{
  QString fn = QFileDialog::getSaveFileName(
      this,
      "Choose filename",
      QDir::currentPath(),
      "Image Files (*.png *.jpg *.bmp)");

  if (fn.isNull())
    return;

  int scaleFactor = 5;

  QPixmap* pm = generateTracksMap(scaleFactor);

  pm->save(fn);
  delete pm;
}

void Sequencer_GUI::updateDrawArea()
{
  QPixmap* pm = generateTracksMap();
  drawLabel->setPixmap(*pm);
  delete pm;
}


void Sequencer_GUI::sendSetMonitorOutputSignal()
{
  emit setMonitorOutput(trackSelect->currentIndex());
}


void Sequencer_GUI::updateSequencerParameter(const QString& param)
{
  sequencerParameterSB->setValue(sequencer->getSequencerParameter(param.toStdString()));
}


void Sequencer_GUI::setSequencerParameter()
{
  sequencer->setSequencerParameter(sequencerParameterCB->currentText().toStdString(),sequencerParameterSB->value(),autoCompileCB->isChecked());
  updateGUI();
}


void Sequencer_GUI::setCycleLength( int _val )
{
  sequencer->setCycleLength( _val );
  updateGUI();
}


#include "Sequencer_GUI.moc"
