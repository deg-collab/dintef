#ifndef SEQUENCERTRACKWIDGET_H
#define SEQUENCERTRACKWIDGET_H

#include <QWidget>
#include <QVector>
#include <QTableWidget>
#include "Sequencer.h"
#include <QSpinBox>

class QComboBox;
class QLabel;
class QCheckBox;


namespace SuS
{

  class HoldCntsTableEntry 
  {
    public :

      HoldCntsTableEntry(int slowClockCycles, bool holdPhase);
      ~HoldCntsTableEntry();

      QSpinBox *holdPhaseSB;
      QSpinBox *slowClockCyclesSB;
  }; // HoldCntsTableEntry


  class PhaseSpinBoxes : public QObject
  {
    Q_OBJECT
    //friend class Sequencer_GUI;

    public :

      PhaseSpinBoxes(int fastClockCycles, bool high);
      ~PhaseSpinBoxes();

    //protected :

      QSpinBox *fastClockCyclesSB;
      QLabel *slowClockCyclesLabel;
      QSpinBox *phaseTypeSB;
    
    protected slots:

      void updateSlowClockCycles(int fastClockCycles);

  }; // PhaseSpinBoxes


  class QPhasesTable : public QTableWidget
  {
    public :

      QPhasesTable(QWidget *_parent=NULL);
      ~QPhasesTable();

      void clearPhases();
      void getSequencePhases(SequencerTrack::SequencePhases& phases);

      QVector<PhaseSpinBoxes*> phasesSBs;

  }; // QPhasesTable


  class QPartSeqsTable : public QTableWidget
  {
    public :

      QPartSeqsTable(QWidget *_parent=NULL);
      ~QPartSeqsTable();

      QVector<QPair<QSpinBox*, QSpinBox*> > partSeqs;

  }; // QSeqRegsTable


  class SequencerTrackWidget : public QFrame
  {
    Q_OBJECT

    friend class Sequencer_GUI;

    public :

      SequencerTrackWidget(SequencerTrack* _seqTrack=nullptr, QWidget* _parent=nullptr);
      ~SequencerTrackWidget();

      bool getStatVal();
      bool getInvHold();
      void setPartSeqTable(SequencerTrack::PartialSequences& _partSeqs);
      void putPartSeqs(SequencerTrack::PartialSequences& _partSeqs);

      QPhasesTable* phasesTable;
      QPartSeqsTable* partSeqsTable;

    public slots :

      void setManualMode(bool _manualMode);
      void setClockMode(bool _en);

    protected :

      QSpinBox *statValSB;
      QSpinBox *invHoldSB;
      QCheckBox *clkCB;

    private :

      bool manualMode;
      SequencerTrack* seqTrack;

  }; // SequencerTrackWidget

} // namespace SuS

#endif
