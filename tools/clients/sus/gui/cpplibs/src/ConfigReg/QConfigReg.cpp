

#include <bitset>
#include <cassert>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>

#include "QConfigReg.h"
//#define DEBUG


namespace SuS {
/**
    \brief Constructor.
*/
QConfigReg::QConfigReg()
: ConfigReg()
{
}

QConfigReg::QConfigReg(const QString &_filename)
: ConfigReg(_filename.toStdString())
{
}

/**
    \brief Destructor.
*/
QConfigReg::~QConfigReg()
{
}


QStringList QConfigReg::toQList(const std::vector<std::string> & vector)
{
  QStringList list;
  for(auto && item : vector){
    list << QString::fromStdString(item);
  }
  return list;
}

}


