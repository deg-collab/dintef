#include <QComboBox>
#include <QLineEdit>
#include <QString>
#include <QTableWidget>
#include <QCheckBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QDebug>
#include <QMessageBox>
#include <QHeaderView>
#include <QFileDialog>
#include <QInputDialog>
#include <QGroupBox>
#include <QStringList>
#include <QSpinBox>
#include <QCloseEvent>
#include <QSettings>
#include <QApplication>
#include <QMainWindow>
#include <thread>

#include <sstream>
#include <iostream>
#include "QConfigRegWidget.h"

#ifdef LOGFILE
//Logfile
 #include "logger.h"
 #include "subsystem_registrator.h"

namespace{ SuS::logfile::subsystem_registrator log_id( "ConfigRegGui" ); }

#else

#define log_id() "ConfigRegGui"
#define info "Info"
#define error "Error"
#define warning "Warning"

#define SuS_LOG(stat,id,message){                           \
        std::cout << stat << " " << id << " " << message << std::endl;    \
}

#define SuS_LOG_STREAM(stat,id,message){                       \
        std::cout << stat << " " << id << " " << message << std::endl;    \
}

#endif


using namespace SuS;
///////////////////////////OutputGuiQt class//////////////////////////////////////////////////////

OutputGuiQt::OutputGuiQt(QWidget* parent, Qt::WindowFlags fl)
 : QWidget(parent,fl),
   configRegWidget(nullptr)
{
  setWindowTitle("Output Editor");
  resize(600,600);
  generateLayout();
}

OutputGuiQt::~OutputGuiQt()
{
}

void OutputGuiQt::generateLayout()
{
  QPushButton *outClearSelBtn = new QPushButton("Clear selected");
  QPushButton *outClearAllBtn = new QPushButton("Clear all");
  QPushButton *addEntryBtn = new QPushButton("Add Entry");
  QPushButton *saveOutputsBtn = new QPushButton("Save Outputs");

  outputTable = new QTableWidget();
  outputTable -> setColumnCount(2);
  outputTable -> setRowCount(0);
  outputTable -> setEditTriggers(QAbstractItemView::NoEditTriggers);

  QTableWidgetItem *Header = new QTableWidgetItem("Module Set",0);
  outputTable->setHorizontalHeaderItem(0, Header);
  Header = new QTableWidgetItem("Modules",0);
  outputTable->setHorizontalHeaderItem(1, Header);

  //outputTable->horizontalHeader()->setResizeMode(QHeaderView::Stretch);

  outModuleSetCB = new QComboBox();
  outModuleEdit = new QLineEdit();

  QVBoxLayout *vlayout = new QVBoxLayout;
  this->setLayout(vlayout);

  QHBoxLayout *hlayout = new QHBoxLayout;

  hlayout = new QHBoxLayout;
  hlayout->addStretch();
  QLabel* orderLabel = new QLabel("Output order");
  hlayout -> addWidget(orderLabel);
  hlayout->addStretch();
  vlayout -> addLayout(hlayout);

  hlayout = new QHBoxLayout;
  QLabel *label = new QLabel("Order given by table entry order from top to bottom.");
  hlayout -> addWidget(label);
  vlayout -> addLayout(hlayout);
  hlayout = new QHBoxLayout;
  label = new QLabel("Module order: from left to right, ranges (x-y) and lists (x;y) supported.");
  hlayout -> addWidget(label);
  vlayout -> addLayout(hlayout);
  hlayout = new QHBoxLayout;
  label = new QLabel("For each module, the submodule list (ranges and lists supported) from left to right is written.");
  hlayout -> addWidget(label);
  vlayout -> addLayout(hlayout);
  hlayout = new QHBoxLayout;
  hlayout -> addWidget(new QLabel("Module Set"));
  hlayout -> addWidget(outModuleSetCB);
  hlayout -> addWidget(new QLabel("Modules"));
  hlayout -> addWidget(outModuleEdit);
  hlayout -> addWidget(addEntryBtn);
  hlayout -> addWidget(saveOutputsBtn);
  hlayout -> addStretch();
  vlayout -> addLayout(hlayout);

  hlayout = new QHBoxLayout;
  hlayout -> addWidget(outputTable);
  QVBoxLayout *btnvlayout = new QVBoxLayout;
  btnvlayout -> addWidget(outClearSelBtn);
  btnvlayout -> addWidget(outClearAllBtn);
  btnvlayout -> addStretch();
  hlayout -> addLayout(btnvlayout);
  vlayout -> addLayout(hlayout);

  connect(outModuleSetCB, SIGNAL(currentIndexChanged(int)), this, SLOT(updateModuleSets()));
  connect(outClearSelBtn, SIGNAL(pressed()), this, SLOT(outClearSel()));
  connect(outClearAllBtn, SIGNAL(pressed()), this, SLOT(outClearAll()));
  connect(addEntryBtn, SIGNAL(pressed()), this, SLOT(addEntry()));
  connect(saveOutputsBtn, SIGNAL(pressed()), this, SLOT(saveOutputorder()));
}

void OutputGuiQt::setConfigRegGui(QConfigRegWidget* _configRegWidget)
{
  configRegWidget=_configRegWidget;
}

void OutputGuiQt::updateModuleSets()
{
  QStringList names = configRegWidget->configReg->getQModuleSetNames();

  {
    SignalsBlocker outBlock(outModuleSetCB);

    outModuleSetCB -> clear();
    outModuleSetCB -> addItems(names);
    outModuleSetCB -> setCurrentIndex(0);
    outModuleSetCB -> setSizeAdjustPolicy(QComboBox::AdjustToContents);
  }
}

void OutputGuiQt::addEntries(int _modSetIndex, QString _newOutputs)
{
  outModuleSetCB->setCurrentIndex(_modSetIndex);
  outModuleEdit->setText(_newOutputs);
  addEntry();

  QMessageBox msgBox;
  msgBox.setText("Outputs Already Generated!");
  msgBox.exec();

}

void OutputGuiQt::addEntry()
{
  QString moduleSet = outModuleSetCB -> currentText();
  QString module = outModuleEdit->text();

  if (moduleSet.isEmpty())
    return;
  if (module.isEmpty())
    module="all";
  int tablePos = outputTable->rowCount();
  outputTable->setRowCount(tablePos + 1);

  QTableWidgetItem *newitem = new QTableWidgetItem(moduleSet,0);
  outputTable->setItem(tablePos, 0, newitem);

  newitem = new QTableWidgetItem(module,0);
  outputTable->setItem(tablePos, 1, newitem);

}

void OutputGuiQt::outClearSel()
{
  QList<QTableWidgetItem *> selectedItems = outputTable -> selectedItems();
  for (int i=0; i<selectedItems.size(); ++i)
  {
    int row = selectedItems.at(i)->row();
    outputTable -> removeRow(row);
  }

}

void OutputGuiQt::outClearAll()
{
  while (outputTable->rowCount() != 0)
    outputTable -> removeRow(0);
}

void OutputGuiQt::saveOutputorder()
{
  QString moduleSet = outModuleSetCB -> currentText();
  QString outputs;

  for(int i=0; i<outputTable->rowCount(); i++){
    outputs += outputTable->item(i,1)->text() + ";";
  }

  configRegWidget->configReg->setOutputList(moduleSet,outputs);

  SuS_LOG(info, log_id(), "New OutputList Stored!");
}

void OutputGuiQt::updateOutputTable()
{
  auto moduleSet = outModuleSetCB -> currentText();
  auto outputList = configRegWidget->configReg->getOutputList(moduleSet);

  outModuleEdit->setText(outputList);
  addEntry();
  outModuleEdit->setText("");
}


///////////////////////////QConfigRegWidget class//////////////////////////////////////////////////////

/**
  \brief Constructor.
*/
QConfigRegWidget::QConfigRegWidget(QWidget* parent, Qt::WindowFlags fl)
  : QWidget(parent,fl),
    initMode(false),
    configReg(new QConfigReg)
{
    generateLayout();
}

QConfigRegWidget::QConfigRegWidget(const QString & fileName, QWidget* parent, Qt::WindowFlags fl)
  : QWidget(parent,fl),
    configReg(new QConfigReg(fileName))
{
    initMode=false;
    generateLayout();
}

QConfigRegWidget::QConfigRegWidget(QConfigReg * _configReg, QWidget* parent, Qt::WindowFlags fl)
 : QWidget(parent,fl),
   configReg(_configReg)
{
    initMode=false;

    setWindowTitle("Memory Structure Editor");

    //resize(800, 400);
    generateLayout();
    updateModuleSets();
}


void QConfigRegWidget::setConfigReg(QConfigReg * _configReg)
{
  configReg = _configReg;
  updateModuleSets();
}


void QConfigRegWidget::generateLayout()
{
    setWindowTitle("Memory Structure Editor");

    resize(800, 400);

    QVBoxLayout *vLayout = new QVBoxLayout(this);
    QHBoxLayout *upperbuttonLayout = new QHBoxLayout();

    QPushButton *newButton = new QPushButton("&New");;
    upperbuttonLayout -> addWidget(newButton);
    QPushButton *openButton = new QPushButton("&Open");
    upperbuttonLayout -> addWidget(openButton);
    QPushButton *saveAsButton = new QPushButton("Save &as");
    upperbuttonLayout -> addWidget(saveAsButton);
    QPushButton *saveButton = new QPushButton("&Save");
    upperbuttonLayout ->addWidget(saveButton);
    upperbuttonLayout -> addStretch();

    reverseCB = new QCheckBox();
    reverseCB->setEnabled(false);

    upperbuttonLayout -> addWidget(new QLabel("Reverse Output"));
    upperbuttonLayout -> addWidget(reverseCB);

    QLabel* label = new QLabel("Module Set");
    upperbuttonLayout -> addWidget(label);
    moduleSetCB = new QComboBox;
    moduleSetCB -> setMinimumWidth(120);
    moduleSetCB -> setEditable(false);
    upperbuttonLayout -> addWidget(moduleSetCB);

    label = new QLabel("Module");
    upperbuttonLayout -> addWidget(label);
    moduleCB = new QComboBox;
    moduleCB -> setMinimumWidth(120);
    moduleCB -> setEditable(true);
    moduleCB -> setInsertPolicy(QComboBox::NoInsert);
    moduleCB -> setCompleter(0);
    moduleEdit = moduleCB->lineEdit();
    moduleCB -> setSizeAdjustPolicy(QComboBox::AdjustToContents);

    upperbuttonLayout -> addWidget(moduleCB);


    vLayout -> addLayout(upperbuttonLayout);

    QHBoxLayout *fnLabelLayout = new QHBoxLayout();
    filenameLabel = new QLabel(configReg->getQFileName());
    filenameLabel = new QLabel( "test" );
    fnLabelLayout->addWidget(filenameLabel);
    fnLabelLayout->addStretch();

    vLayout -> addLayout(fnLabelLayout);

    table = new QTableWidget;
    table -> setRowCount(0);
    table -> setColumnCount(6);
    table -> setSelectionMode(QAbstractItemView::SingleSelection);
    table -> setSelectionBehavior(QAbstractItemView::SelectItems);

    updateHeaders();

    QHBoxLayout *tableHLayout = new QHBoxLayout();
    tableHLayout -> addWidget(table);

    vLayout -> addLayout(tableHLayout);

    QHBoxLayout *lowerButtonLayout = new QHBoxLayout();

    addressLabel = new QLabel("0");
    QPushButton *printBtn = new QPushButton("Print Bitfile");

    vLayout -> addLayout(lowerButtonLayout);


    initModeCB = new QCheckBox("Admin Mode");
    initModeCB -> setChecked(false);

    lowerButtonLayout -> addWidget(initModeCB);
    lowerButtonLayout -> addStretch();
    lowerButtonLayout -> addWidget(new QLabel("ModuleSet address"));
    lowerButtonLayout -> addWidget(addressLabel);
    lowerButtonLayout -> addWidget(printBtn);

    //admin mode, first row
    int labelWidth = 150;

    adminBox = new QGroupBox("",this);
    QVBoxLayout *adminLayout = new QVBoxLayout();
    adminBox->setLayout(adminLayout);
    adminBox->setVisible(false);

    QHBoxLayout *initModSetLayout = new QHBoxLayout();

    QLabel *modulesetLabel = new QLabel("Module Set");
    modulesetLabel -> setFixedWidth(labelWidth);
    initModSetLayout -> addWidget(modulesetLabel);

    QPushButton *addSetBtn = new QPushButton("Add");
    initModSetLayout -> addWidget(addSetBtn);

    QPushButton *remSetBtn = new QPushButton("Delete");
    initModSetLayout -> addWidget(remSetBtn);

    QPushButton *renSetBtn = new QPushButton("Rename Current");
    initModSetLayout -> addWidget(renSetBtn);

    initModSetLayout -> addStretch();
    adminLayout -> addLayout(initModSetLayout);

    //admin mode, second row
    QHBoxLayout *initModulesLayout = new QHBoxLayout();

    QLabel *moduleLabel = new QLabel("Module");
    moduleLabel -> setFixedWidth(labelWidth);
    initModulesLayout -> addWidget(moduleLabel);

    initModuleCB = new QComboBox;
    initModuleCB -> setMinimumWidth(120);
    initModuleCB -> setEditable(true);
    initModuleCB -> setInsertPolicy(QComboBox::NoInsert);
    initModuleCB -> setCompleter(0);
    initModuleEdit = initModuleCB -> lineEdit();
    initModuleCB -> setSizeAdjustPolicy(QComboBox::AdjustToContents);

    initModulesLayout -> addWidget(initModuleCB);


    QPushButton *addModBtn = new QPushButton("Add");
    initModulesLayout -> addWidget(addModBtn);

    QPushButton *remModBtn = new QPushButton("Delete");
    initModulesLayout -> addWidget(remModBtn);

    initModulesLayout -> addStretch();
    adminLayout -> addLayout(initModulesLayout);

    QHBoxLayout *initSignalsLayout = new QHBoxLayout();
    QLabel * signalsLabel = new QLabel("Signals");
    signalsLabel->setFixedWidth(labelWidth);
    initSignalsLayout -> addWidget(signalsLabel);

    QPushButton *addSignalBtn = new QPushButton("Add Signal");
    QPushButton *removeSignalBtn = new QPushButton("Remove Signal");

    initSignalsLayout -> addWidget(addSignalBtn);
    initSignalsLayout -> addWidget(removeSignalBtn);

    initSignalsLayout -> addStretch();
    adminLayout -> addLayout(initSignalsLayout);


    //admin mode, third row
    QHBoxLayout *initAddressLayout = new QHBoxLayout();

    QLabel *initAddressLabel = new QLabel("Address");
    initAddressLabel -> setFixedWidth(labelWidth);
    initAddressLayout -> addWidget(initAddressLabel);

    addressEdit = new QLineEdit("0b00000000");
    addressEdit->setFixedWidth(120);
    initAddressLayout -> addWidget(addressEdit);

    QPushButton *saveAddrBtn = new QPushButton("Set");
    initAddressLayout -> addWidget(saveAddrBtn);
    initAddressLayout -> addStretch();

    adminLayout -> addLayout(initAddressLayout);

    //admin mode, forth row
    QHBoxLayout *initOutputsLayout = new QHBoxLayout();

    QLabel *configLabel = new QLabel("Configuration");
    configLabel -> setFixedWidth(labelWidth);
    initOutputsLayout -> addWidget(configLabel);

    QPushButton *showOutputGuiBtn = new QPushButton("Outputs");
    initOutputsLayout -> addWidget(showOutputGuiBtn);

    initOutputsLayout -> addStretch();
    adminLayout -> addLayout(initOutputsLayout);

    vLayout->addWidget(adminBox);

    connect(newButton, SIGNAL(clicked()), this, SLOT(newFile()));
    connect(openButton, SIGNAL(clicked()), this, SLOT(openFile()));
    connect(saveButton, SIGNAL(clicked()), this, SLOT(saveFile()));
    connect(saveAsButton, SIGNAL(clicked()), this, SLOT(saveFileAs()));
    connect(moduleSetCB, SIGNAL(currentIndexChanged(int)), this, SLOT(updateModules()));
    connect(moduleCB, SIGNAL(currentIndexChanged(int)), this, SLOT(updateTable()));
    connect(moduleCB, SIGNAL(editTextChanged(QString)), this, SLOT(checkModuleEdit()));
    connect(initModeCB, SIGNAL(stateChanged(int)), this, SLOT(setInitMode(int)));
    connect(addSignalBtn, SIGNAL(clicked()), this, SLOT(addSignal()));
    connect(removeSignalBtn, SIGNAL(clicked()), this, SLOT(removeSignal()));
    connect(printBtn, SIGNAL(clicked()), this, SLOT(printBitfile()));
    connect(addModBtn, SIGNAL(clicked()), this, SLOT(addModules()));
    connect(remModBtn, SIGNAL(clicked()), this, SLOT(removeModules()));
    connect(addSetBtn, SIGNAL(clicked()), this, SLOT(addModuleSet()));
    connect(remSetBtn, SIGNAL(clicked()), this, SLOT(removeModuleSet()));
    connect(showOutputGuiBtn, SIGNAL(clicked()), this, SLOT(showOutputGui()));
    connect(renSetBtn, SIGNAL(clicked()), this, SLOT(renameModuleSet()));
    connect(table, SIGNAL(cellChanged(int, int)), this, SLOT(cellChanged(int)));
    connect(saveAddrBtn, SIGNAL(clicked()), this, SLOT(storeModSetAddress()));

    outputgui = new OutputGuiQt(this,Qt::Window);
    outputgui->setConfigRegGui(this);
    if(configReg){
      if (!configReg->isLoaded()) {
        openFile();
      }else{
        updateModuleSets();
      }
    }
}

/**
  \brief Destructor.
*/
QConfigRegWidget::~QConfigRegWidget()
{
}


void QConfigRegWidget::cellChanged(int row)
{
  saveChanges(row);

  emit modified();
  changed=true;
}

void QConfigRegWidget::setSignalValue(const QString &moduleSet, const QString &modules, const QString &signalName, uint32_t value)
{
  if (!configReg -> isLoaded()) return;

  configReg -> setSignalValue(moduleSet, modules, signalName, value);

  updateTable();

  emit modified();
  changed=true;
}




void QConfigRegWidget::renameModuleSet()
{
  QString moduleSet = moduleSetCB -> currentText();
  if (moduleSet.isEmpty())
    return;

  QString newName = QInputDialog::getText (this,
                                           "Enter new name",
                                           "",
                                           QLineEdit::Normal,
                                           moduleSet);

  configReg -> renameModuleSet(moduleSet,newName);
  updateModuleSets();
}

void QConfigRegWidget::setInitMode(int enable)
{
  initMode = initModeCB->isChecked();
  if(enable==0){
    initMode = false;
  }

  {
    SignalsBlocker block(initModeCB);
    initModeCB->setChecked(initMode);
  }

  if (initMode){
    SignalsBlocker block(moduleEdit);
    moduleEdit->setText("init");
    connect(moduleSetCB, SIGNAL(activated(int)), this, SLOT(saveChanges()));
  }else{
    changeReverse();
    moduleCB->setCurrentIndex(0);
    disconnect(moduleSetCB, SIGNAL(activated(int)), this, SLOT(saveChanges()));
  }

  reverseCB -> setEnabled(initMode);
  moduleCB -> setEnabled(!initMode);

  adminBox -> setVisible(initMode);

  if(configReg->isLoaded())
    updateTable();
}

void QConfigRegWidget::showOutputGui()
{
  if (!outputgui -> isVisible())
  {
    outputgui -> outClearAll();
    outputgui -> updateOutputTable();
    outputgui -> setVisible(true);
  }
}

void QConfigRegWidget::saveChangesPressed()
{

}

void QConfigRegWidget::addModules()
{
  //add the module numbers from combobox input
  QString newModulesTxt = initModuleEdit->text();
  QString moduleSet = moduleSetCB -> currentText();
  int set = moduleSetCB->currentIndex();

  if (newModulesTxt == "" || newModulesTxt == "all" || newModulesTxt == "a")
    return;

  //add the nonexistent modules
  QString newModules = configReg->addModules(moduleSet,newModulesTxt);
  if(configReg->isLoaded()){
    outputgui->addEntries(set,newModules);
  }
  //update module combobox and select the new modules
  updateModules();
}

void QConfigRegWidget::removeModules()
{
  if (!configReg -> isLoaded()) return;
  QString moduleTxt = initModuleEdit->text();
  QString moduleSet = moduleSetCB -> currentText();

  if (moduleTxt == "")
    return;

  int ret=QMessageBox::Ok;
  if (configReg -> isLoaded())
  {
#undef warning
    ret = QMessageBox::warning(this, "Attention", "Are you sure?",
                                QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel);
#define warning "Warning"
  }

  if (ret == QMessageBox::Ok)
  {
    configReg -> removeModules(moduleSet, moduleTxt);
    updateModules();
  }
}

void QConfigRegWidget::addModuleSet()
{
  QString setName = QInputDialog::getText(this,
                                        "Module set name",
                                        "Enter name:",
                                        QLineEdit::Normal);

  if (setName.isEmpty())
    return;

  configReg -> addModuleSet(setName);
  updateModuleSets();
  {
    SignalsBlocker block(moduleSetCB);
    moduleSetCB -> setCurrentIndex(moduleSetCB->findText(setName));
  }

  bool ok;
  QString newModulesList = QInputDialog::getText(this, tr("Get Modules"),
                                     tr("Define new Modules: 0-9;15;32"), QLineEdit::Normal,
                                     tr(""), &ok);

  if(!ok) return;

  QString newAddress = QInputDialog::getText(this, tr("Get Address"),
                                     tr("Define moduleset addess (dec = 10 /bin = 0b1 / hex = 0xAA): \ncan also be defined later"), QLineEdit::Normal,
                                     tr(""), &ok);

  if(!ok) return;

  initModuleEdit->setText(newModulesList);
  addModules();

  addressEdit->setText(newAddress);
  storeModSetAddress();

  addSignal();
}


void QConfigRegWidget::removeModuleSet()
{
  if (!configReg -> isLoaded()) return;
  int ret=QMessageBox::Ok;
  if (configReg -> isLoaded())
  {

#undef warning

    ret = QMessageBox::warning(this, "Attention", "Are you sure?",
                                QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel);
#define warning "Warning"

  }
  if (ret == QMessageBox::Ok)
  {
    configReg -> removeModuleSet(moduleSetCB -> currentText());
    updateModuleSets();
  }
}

bool QConfigRegWidget::checkSignalBitPositions()
{

  return true;
}

bool QConfigRegWidget::saveInitChanges()
{
  QString moduleSet = moduleSetCB->currentText();
  int numSetSignals = configReg->getNumSignals(moduleSet);

  changeReverse();

  bool ok = checkSignalBitPositions();

  if(!ok){
    SuS_LOG(error, log_id(), "Check Bit Positions, not all positions assigned!");
    return false;
  }

  for(int row=0; row<table->rowCount(); ++row){
    QString signalName = getTSignalName(row);
    if(row<numSetSignals){
      configReg->renameSignal(moduleSet.toStdString(),row,signalName.toStdString());
      configReg->changeSignalReadOnly(moduleSet.toStdString(),row,getTSignalReadOnly(row));
      configReg->changeSignalActiveLow(moduleSet.toStdString(),row,getTSignalActiveLow(row));
      configReg->setSignalPositions(moduleSet.toStdString(),row,getTSignalPositions(row).toStdString());
      configReg->setSignalAliases(moduleSet.toStdString(),row,getTSignalAliases(row).toStdString());
    }else{
      if(allTValuesDefined(row)){
        try{
          configReg->addSignal(moduleSet.toStdString(),signalName.toStdString(),getTSignalPositions(row).toStdString(),getTSignalReadOnly(row),getTSignalActiveLow(row));
        }
        catch(...)
        {
          SuS_LOG(error, log_id(), "Not all values assigned");
        }
      }else{
        SuS_LOG(error, log_id(), "Not all values assigned");
      }
    }
  }

  emit modified();
  changed=true;
  if(initMode){
    setInitMode(0);
  }
  return true;
}

void QConfigRegWidget::storeModSetAddress()
{
  QString moduleSet = moduleSetCB->currentText();

  QString addrText = addressEdit->text();

  uint32_t addr=0;

  bool ok = true;
  if(addrText.contains ("0x",Qt::CaseSensitive )){
    addr = addrText.section("x",1,1).toUShort(&ok,16);
  }else if(addrText.contains ("0X",Qt::CaseSensitive )){
    addr = addrText.section("X",1,1).toUShort(&ok,16);
  }if(addrText.contains ("0b",Qt::CaseSensitive )){
    addr = addrText.section("b",1,1).toUShort(&ok,2);
  }else{
    addr = addressEdit->text().toInt();
  }

  if(!ok){
    SuS_LOG(error, log_id(), "storeModsetAddress: sth. went wrong check format!");
  }

  if(addr>255){
    SuS_LOG(warning, log_id(), "storeModSetAddress: address too large! 8 bit supported!");
  }else{
    configReg->setRegAddress(moduleSet,addr);
    addressLabel->setText(QString::number(configReg->getRegAddress(moduleSet)));
  }
}

void QConfigRegWidget::saveChanges(int tableRow)
{
  QString moduleSet = moduleSetCB->currentText();
  QString moduleText = moduleCB -> currentText();
  if (moduleText == "")
    return;

  if (moduleText == "all" || moduleText == "a"|| moduleText == "al" || moduleText == "init"){
    moduleText = configReg -> getModuleNumberList(moduleSet);
  }

  QStringList signalList = configReg->getSignalNames(moduleSet);

  int startRow = (tableRow == -1)? 0 : tableRow;
  int endRow   = (tableRow == -1)? table -> rowCount()-1 : tableRow;

  for (int row=startRow; row<=endRow; ++row){
    QString signalName = signalList.at(row);
    if(initMode){
      changeReverse();

      if(allTValuesDefined(row)){
        configReg->renameSignal(moduleSet.toStdString(),row,getTSignalName(row).toStdString());
        configReg->setSignalPositions(moduleSet.toStdString(),row,getTSignalPositions(row).toStdString());
        configReg->changeSignalReadOnly(moduleSet.toStdString(),row,getTSignalReadOnly(row));
        configReg->changeSignalActiveLow(moduleSet.toStdString(),row,getTSignalActiveLow(row));
        configReg->setSignalAliases(moduleSet.toStdString(),row,getTSignalAliases(row).toStdString());
      }
    }else{
      uint32_t value = getTSignalValue(row);
      configReg -> setSignalValue(moduleSet,moduleText,signalName,value);
    }
  }

  updateTable();
}

/**
   \brief Calls memstructure to print bitfile to log.
*/
void QConfigRegWidget::printBitfile()
{
  if (!configReg -> isLoaded())
    return;

  SuS_LOG(info, log_id(), "All bits in series:");


  std::string contentStr;
  if(moduleCB->currentText() == "all"){
    contentStr = configReg -> printContentToString(moduleSetCB->currentText().toStdString());

    QFile bitFile("bitstring.txt");
    if (!bitFile.open(QIODevice::WriteOnly | QIODevice::Text)){
        SuS_LOG(info, log_id(), "Could not open bitstring.txt to write bitstring");
    }else{

      QTextStream out(&bitFile);
      out << QString::fromStdString(contentStr) << "\n";
      bitFile.close();
    }
  }else{
    contentStr = configReg -> printModuleContentToString(moduleSetCB->currentText().toStdString(),moduleCB->currentText().toStdString());
  }
  SuS_LOG(info, log_id(), contentStr);
}

/**
   \brief Increases table row count by 1
*/
void QConfigRegWidget::addLine()
{
  if (configReg->getNumModules(moduleSetCB->currentText()) == 0)
  {
    SuS_LOG(error, log_id(), "Please add modules before defining signals.");
    return;
  }
  table -> setRowCount(table->rowCount()+1);

}

void QConfigRegWidget::addSignal()
{
  configReg->addSignal(moduleSetCB->currentText().toStdString(),"NewSignalB" + std::to_string(table->rowCount()),"init",false);
  updateTable();
}

void QConfigRegWidget::removeSignal()
{
  int row = table->currentRow();
  if(row<0){
    row = table->rowCount()-1;
  }
  configReg->removeSignal(moduleSetCB->currentText().toStdString(),row);
  table->clear();

  updateHeaders();

  updateTable();
}

void QConfigRegWidget::updateHeaders()
{
  QTableWidgetItem *header = new QTableWidgetItem("Name",0);
  table->setHorizontalHeaderItem(table_columnName, header);
  header = new QTableWidgetItem("Value",0);
  table->setHorizontalHeaderItem(table_columnValue, header);
  header = new QTableWidgetItem("Virtual Bit Positions (LSB;..;MSB)",0);
  table->setHorizontalHeaderItem(table_columnPosition, header);
  header = new QTableWidgetItem("ReadOnlySignal",0);
  header->setSizeHint(QSize(30,10));
  table->setHorizontalHeaderItem(table_columnReadOnly, header);
  header = new QTableWidgetItem("ActiveLowSignal",0);
  table->setHorizontalHeaderItem(table_columnActiveLow, header);
  header = new QTableWidgetItem("Aliases",0);
  table->setHorizontalHeaderItem(table_columnAliases, header);

  table->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
}

void QConfigRegWidget::checkModuleEdit()
{
  if(moduleCB->currentText().right(1)=="-"){
    return;
  }
  updateTable();
}

void QConfigRegWidget::updateTable()
{
  if(!configReg->isLoaded())
    return;

  SignalsBlocker block(table);
  QString moduleSet = moduleSetCB -> currentText();
  QString moduleText = moduleCB->currentText();

  if (moduleText.isEmpty())  {
    //SuS_LOG(error, log_id(), "UpdateTable: modules empty!");
    return;
  }

  if(moduleText.right(1)=="-"){
    return;
  }

 // SuS_LOG_STREAM(debug,log_id(),"Update Table Current text is " << moduleText.toStdString());
  if (moduleText == "all" || moduleText == "a" || moduleText == "al" || moduleText == "init")
    moduleText = configReg->getModuleNumberList(moduleSet);

  QStringList signalList = configReg -> getSignalNames(moduleSet);
  int numSignals = signalList.size();

  table->setRowCount(numSignals);
  try
  {
    int row = 0;
    for (const auto & signal : signalList)
    {
      bool various = configReg->signalIsVarious(moduleSet,signal,moduleText);
      uint32_t value = configReg->getSignalValue(moduleSet,moduleText,signal);

      setTSignalName(row,signal);
      setTSignalValue(row,various,value);
      setTSignalBitPositions(row,signal);
      setTSignalReadOnly(row,signal);
      setTSignalActiveLow(row,signal);
      setTSignalAliases(row,signal);
      row++;
    }
  }
  catch(...)
  {
    SuS_LOG(error, log_id(), "ConfigregFile is corrupted!");
  }

//  setMinimumHeight(table->verticalHeader()->length() + 130);
}

void QConfigRegWidget::updateModuleSets()
{
  filenameLabel->setText(configReg->getQFileName());

  SignalsBlocker modBlock(moduleSetCB);
  moduleSetCB -> clear();
  QStringList names = configReg->getQModuleSetNames();
  if(names.isEmpty()){
    SuS_LOG(error, log_id(), "No module set found...check input file!");
    configReg -> init();
    return;
  }
  moduleSetCB -> addItems(names);
  moduleSetCB -> setCurrentIndex(0);
  //moduleSetCB -> setSizeAdjustPolicy(QComboBox::AdjustToContents);
  outputgui -> updateModuleSets();

  updateModules();
}

void QConfigRegWidget::updateModules()
{
  SignalsBlocker modBlock(moduleCB);
  SignalsBlocker initBlock(initModuleCB);

  moduleCB -> clear();
  moduleCB -> addItem("all");
  initModuleCB -> clear();

  QString moduleSet = moduleSetCB->currentText();

  addressLabel->setText(QString::number(configReg->getRegAddress(moduleSet)));

  bool isReverseModSet = configReg->isSetReverse(moduleSet);
  reverseCB->setChecked(isReverseModSet);

  QStringList numbers = configReg->getModules(moduleSet);
  moduleCB -> addItems(numbers);
  initModuleCB -> addItems(numbers);

  moduleCB->setCurrentIndex(0);
  initModuleCB -> setCurrentIndex(0);

  table -> clearContents();
  table -> setRowCount(0);

  if (initMode){
    moduleEdit->setText("init");
  }else{
    updateTable();
  }
}

/**
  \brief Calls memstructure init function for a new file. Asks for confirmation if there's data loaded in the internal structure
*/
void QConfigRegWidget::newFile()
{
  int ret=QMessageBox::Ok;
  if (configReg -> isLoaded())
  {
#undef warning
    ret = QMessageBox::warning(this, "Attention", "Unsaved changes will be lost. Continue?",
                                QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel);

#define warning "Warning"

  }
  if (ret == QMessageBox::Ok)
    initGui();

  changed=true;
  initModeCB->setChecked(true);

  addModuleSet();
}


void QConfigRegWidget::setTSignalValue(int row, bool various, uint32_t value)
{
  int numRows = table->rowCount();
  if(row>=numRows){
    addLine();
  }

  if(configReg->isSignalBoolean(moduleSetCB->currentText().toStdString(),row)){
    insertCheckBox(row, various, value);
  }else{
    insertEditField(row, various, value);
  }
}

uint32_t QConfigRegWidget::getTSignalValue(int row)
{
  uint32_t value =0;
  bool ok = true;

  if(table->item(row,table_columnValue)->flags() & Qt::ItemIsUserCheckable){
    value =(uint32_t)(table->item(row,table_columnValue)->checkState() != Qt::Unchecked);
  }else{
    value = table->item(row,table_columnValue)->text().toUInt(&ok);
  }
  if(!ok){
    value = -1;
  }
  return value;
}

QString QConfigRegWidget::getTSignalName(int row)
{
  QString signal = "";
  if(table->item(row,table_columnName)){
    signal = table->item(row,table_columnName)->text();
  }

  return signal;
}

void QConfigRegWidget::setTSignalName(int row, const QString & signalName)
{
  QTableWidgetItem *newitem = new QTableWidgetItem(signalName,0);
  if (initMode)
    newitem -> setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
  else
    newitem -> setFlags(Qt::ItemIsEnabled);

  table->setItem(row, table_columnName, newitem);
}

QString QConfigRegWidget::getTSignalPositions(int row)
{
  if(!table->item(row,table_columnPosition))
    return "";

  return table->item(row,table_columnPosition)->text();
}


void QConfigRegWidget::setTSignalAliases(int row, const QString & signalName)
{
  QString aliases = configReg->getSignalAliases(moduleSetCB->currentText(),signalName);
  QTableWidgetItem *newitem = new QTableWidgetItem(aliases,0);
  if (initMode)
    newitem -> setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
  else
    newitem -> setFlags(Qt::ItemIsEnabled);

  table->setItem(row, table_columnAliases, newitem);
}


void QConfigRegWidget::setTSignalBitPositions(int row, const QString & signalName)
{
  QString positionList = configReg->getSignalPositionsList(moduleSetCB->currentText(),signalName);

  QTableWidgetItem *newitem = new QTableWidgetItem(positionList,0);

  if (initMode)
    newitem -> setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable);
  else
    newitem -> setFlags(Qt::ItemIsEnabled);

  table->setItem(row, table_columnPosition, newitem);
}

bool QConfigRegWidget::getTSignalReadOnly(int row)
{
  return (table->item(row,table_columnReadOnly)->checkState() != Qt::Unchecked);
}

bool QConfigRegWidget::getTSignalActiveLow(int row)
{
  return (table->item(row,table_columnActiveLow)->checkState() != Qt::Unchecked);
}

QString QConfigRegWidget::getTSignalAliases(int row)
{
  if(!table->item(row,table_columnAliases))
    return "";

  return table->item(row,table_columnAliases)->text();
}

bool QConfigRegWidget::allTValuesDefined(int row)
{
  bool ok = true;
  if(table->item(row,table_columnPosition)){
    ok &= !table->item(row,table_columnPosition)->text().isEmpty();
  }else{
    return false;
  }

  if(table->item(row,table_columnName)){
    ok &= !table->item(row,table_columnName)->text().isEmpty();
  }else{
    return false;
  }

  return ok;
}


void QConfigRegWidget::setTSignalReadOnly(int row, const QString & signalName)
{
  bool ro = configReg->isSignalReadOnly(moduleSetCB->currentText(),signalName);

  QTableWidgetItem *newitem = new QTableWidgetItem();

  if (!initMode)
    newitem->setFlags(Qt::ItemIsEnabled);
  else
    newitem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);

  newitem -> setCheckState((ro)? Qt::Checked : Qt::Unchecked);

  if(ro)
    table->item(row,table_columnValue)->setFlags(table->item(row,table_columnValue)->flags() & ~Qt::ItemIsEnabled);

  table->setItem(row, table_columnReadOnly, newitem);
}


void QConfigRegWidget::setTSignalActiveLow(int row, const QString & signalName)
{
  bool actLow = configReg->isSignalActiveLow(moduleSetCB->currentText(),signalName);

  QTableWidgetItem *newitem = new QTableWidgetItem();

  if (!initMode)
    newitem->setFlags(Qt::ItemIsEnabled);
  else 
    newitem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);

  newitem -> setCheckState((actLow)? Qt::Checked : Qt::Unchecked);

  table->setItem(row, table_columnActiveLow, newitem);
}


void QConfigRegWidget::changeReverse()
{
  bool reverse = reverseCB->isChecked();
  if(!configReg->isLoaded()){
    return;
  }

  configReg->changeSetReverse(moduleSetCB->currentText(),reverse);
}


/**
   \brief Displays open file dialog and inits gui if file was selected. Asks for confirmation if there's data present in memstructure.
*/

void QConfigRegWidget::initGui()
{
  blockGuiSignals(true);
  initModeCB->setChecked(false);
  table -> clearContents();
  table -> setRowCount(0);
  moduleCB->clear();
  initModuleCB->clear();
  moduleSetCB->clear();
  reverseCB->setChecked(false);

  configReg -> init();

  blockGuiSignals(false);
}

void QConfigRegWidget::blockGuiSignals(bool block)
{
  initModeCB->blockSignals(block);
  table->blockSignals(block);
  moduleCB->blockSignals(block);
  initModuleCB->blockSignals(block);
  moduleSetCB->blockSignals(block);
  reverseCB->blockSignals(block);
}

void QConfigRegWidget::openFile(const QString &openFilename)
{
  if ((openFilename == ""))
  {
      SuS_LOG(error, log_id(), "Could not open memory structure file");
      return;
  }

  initGui();

  if(configReg -> initFromFile(openFilename)){
    changed=true;
    updateModuleSets();
  }
  emit newFileLoaded();
}


void QConfigRegWidget::openFile()
{
  int ret=QMessageBox::Ok;
  if (configReg -> isLoaded())
  {
#undef warning
    ret = QMessageBox::warning(this, "Attention", "Unsaved changes will be lost. Continue?",
                                QMessageBox::Ok | QMessageBox::Cancel, QMessageBox::Cancel);
#define warning "Warning"
  }
  if (ret != QMessageBox::Ok)
    return;

  QFileInfo fi(configReg->getQFileName());
  QString path = fi.path();
  QString openFilename = QFileDialog::getOpenFileName(
                              this,
                              "Choose a file to open",
                              path, //QDir::currentPath()+ "/../ConfigFiles",
                              "ConfigReg file (*.txt)");
  openFile(openFilename);
}

/**
   \brief Displays save file dialog and calls memstructure to save data.
*/
void QConfigRegWidget::saveFileAs()
{
  if (initMode)
  {
   if(!saveInitChanges())
     return;
  }

  QFileInfo fi(configReg->getQFileName());
  QString path = fi.path();
  QString qfile = QFileDialog::getSaveFileName(
                      this,
                      "Save Memory structure to file",
                      path, //QDir::currentPath()+ "/../ConfigFiles",
                      "ConfigReg file (*.txt)");

  if (qfile.isNull())
  {
      SuS_LOG(error, log_id(), "File not saved");
      return;
  }

  configReg -> saveToFile(qfile);
  filenameLabel->setText(configReg->getQFileName());

  changed=false;

  SuS_LOG(info, log_id(), "File saved");
}


/**
   \brief Saves to the filename stored in configReg.
*/
void QConfigRegWidget::saveFile()
{
  if (initMode)
  {
   if(!saveInitChanges())
     return;
  }

  configReg -> saveToFile(configReg->getQFileName());
  changed=false;

  SuS_LOG(info, log_id(), "File saved");
}


/**
   \brief Saves to filename
*/
void QConfigRegWidget::saveToFile(const QString &filename)
{
  if (initMode)
  {
   if(!saveInitChanges())
     return;
  }

  filenameLabel->setText(filename);
  configReg -> saveToFile(filename);
  changed=false;

  SuS_LOG(info, log_id(), "File saved");
}






/**
        \brief Called if window gets close signal.
*/
void QConfigRegWidget::closeEvent(QCloseEvent* ce)
{
  ce -> accept();
  return;

  bool closeGUI = !QMessageBox::question(
                      this,
                      tr("Warning"),
                      tr("Do you really want to close Memory Structure GUI?"),
                      tr("&Yes"), tr("&No"),
                      QString::null, 0, 1 );

  if (closeGUI)
  {
    outputgui -> setVisible(false);
    ce -> accept();
  }
  else
    ce -> ignore();
}

void QConfigRegWidget::insertCheckBox(int row, bool various, bool value)
{
  QTableWidgetItem *newitem = new QTableWidgetItem();

  if (initMode){
    newitem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsUserCheckable| Qt::ItemIsTristate);
  }else{
    newitem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsTristate);
  }
  newitem -> setCheckState((various)? Qt::PartiallyChecked : ( (value!=0)? Qt::Checked : Qt::Unchecked ));
  table->setItem(row, table_columnValue, newitem);
}


void QConfigRegWidget::insertEditField(int row, bool various, uint32_t value)
{
  QTableWidgetItem *newitem;
  if(various){
    newitem = new QTableWidgetItem("[...]",0);
  }else{
    newitem = new QTableWidgetItem(QString::number(value),0);
  }

  if (initMode)
    newitem->setFlags(Qt::ItemIsEnabled);
  else
    newitem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled);

  table->setItem(row, table_columnValue, newitem);
}


void QConfigRegWidget::initFromSettings(QObject *object, const QString &settingsGroup)
{
  QSettings settings("DSSC","QT_GUI");
  settings.beginGroup(settingsGroup);
  
  settings.beginGroup("SpinBoxes");
  {
    auto spinBoxList = object->findChildren<QSpinBox *>();
    QStringList keys = settings.allKeys();
    for(const auto & key : keys){
      int value = settings.value(key).toInt();
      for(auto spinBox : spinBoxList){
        if(spinBox->accessibleName() == key){        
          spinBox->setValue(value);
          break;
        }
      }    
    }
  }
  settings.endGroup();
  settings.beginGroup("LineEdits");
  {
    auto lineEditList = object->findChildren<QLineEdit *>();
    QStringList keys = settings.allKeys();
    for(const auto & key : keys){
      QString value = settings.value(key).toString();
      for(auto lineEdit : lineEditList){
        if(lineEdit->accessibleName() == key){        
          lineEdit->setText(value);
          break;
        }
      }    
    }
  }
  settings.endGroup();
  settings.beginGroup("CheckBoxes");
  {
    auto checkBoxList = object->findChildren<QCheckBox *>();
    QStringList keys = settings.allKeys();
    for(const auto & key : keys){
      bool value = settings.value(key).toBool();
      for(auto checkBox : checkBoxList){
        if(checkBox->accessibleName() == key){  
          checkBox->setChecked(value);
          break;
        }
      }    
    }
  }
  settings.endGroup();  
  settings.beginGroup("ComboBoxes");
  {
    auto comboBoxList = object->findChildren<QComboBox *>();
    QStringList keys = settings.allKeys();
    for(const auto & key : keys){
      int value = settings.value(key).toInt();
      for(auto comboBox : comboBoxList){
        if(comboBox->accessibleName() == key){        
          comboBox->setCurrentIndex(value);
          break;
        }
      }    
    }
  }
  settings.endGroup();   
  settings.endGroup();
}


void QConfigRegWidget::saveSettings(QObject *object, const QString &settingsGroup)
{
  QSettings settings("DSSC","QT_GUI");
  qDebug() << "Saving settings in file " << settings.fileName();
  settings.beginGroup(settingsGroup);
  {
    settings.beginGroup("SpinBoxes");
    auto spinBoxList = object->findChildren<QSpinBox *>();
    for(auto spinBox : spinBoxList){
      const auto name = spinBox->accessibleName();
      if(!name.isEmpty()){
        settings.setValue(name, spinBox->value());
      }
    }
    settings.endGroup();
  }
  {  
    settings.beginGroup("LineEdits");
    auto lineEditList = object->findChildren<QLineEdit *>();
    for(auto lineEdit : lineEditList){
      const auto name = lineEdit->accessibleName();
      if(!name.isEmpty()){
        settings.setValue(name, lineEdit->text());
      }
    }
    settings.endGroup();
  } 
  {
    settings.beginGroup("CheckBoxes");
    auto checkBoxList = object->findChildren<QCheckBox *>();
    for(auto checkBox : checkBoxList){
      const auto name = checkBox->accessibleName();
      if(!name.isEmpty()){
        settings.setValue(name, checkBox->isChecked());
      }
    }
    settings.endGroup();
  }
  {
    settings.beginGroup("ComboBoxes");
    auto comboBoxList = object->findChildren<QComboBox *>();
    for(auto comboBox : comboBoxList){
      const auto name = comboBox->accessibleName();
      if(!name.isEmpty()){
        settings.setValue(name, comboBox->currentIndex());
      }
    }
    settings.endGroup();
  }
  settings.endGroup();
  settings.sync();
}


extern "C"
{

  // global variables for python environment
  SuS::QConfigRegWidget* qConfigRegWidget = nullptr;
  SuS::QConfigReg* qConfigReg = nullptr;
  std::vector<int>* bitString = nullptr;
  QApplication *qapp = nullptr; // qApp is reserved

  void myPrint(const char* s) {
    printf(s);
    std::string str(s);
    std::cout << str << std::endl;
  }

  // initializes the global qConfigRegWidget variable and qt system (QApplication)
  SuS::QConfigRegWidget* QConfigReg_new(const char* _filename) 
  {
    if ( qConfigRegWidget ) {
      delete qConfigRegWidget;
      delete qConfigReg;
    } 
    //QMainWindow mainWin; // maybe need to use global pointer variable
    qConfigReg = new QConfigReg( _filename );
    std::cout << "QConfigReg filename = " << qConfigReg->getQFileName().toStdString() << std::endl;
    return qConfigRegWidget;
  }

  void QConfigRegWidget_show() {
    if ( !qConfigRegWidget ) { 
      int argc = 0;
      char argv[] = "";
      qapp = new QApplication( argc, (char**) &argv ); // QApplication needed before any windows can be shown
      qConfigRegWidget = new QConfigRegWidget( qConfigReg );
      std::cout << "Initializing config reg GUI." << std::endl;
    } else {
      std::cout << "Showing existing GUI." << std::endl;
    }
    qConfigRegWidget->show();
    qapp->exec();
  }

  void QConfigRegWidget_show_thread() {
    std::thread t(QConfigRegWidget_show);
    t.detach(); // needed in order to keep python running
  }

  // updates the bitString with the selected register bits and returns its length
  // attention, the def argument does not work with python
  int updateBitString(const char* _regName = "Global Control Register") {
    //std::cout << "getBitString() from register \"" << _regName << "\"" << std::endl;
    if ( qConfigReg ) {
      QVector<bool> tmp = qConfigReg->printContent( _regName, 0);
      std::cout << "printContent()" << std::endl;
      if (!bitString) bitString = new std::vector<int>;
      else bitString->clear();
      for (auto b : tmp) bitString->push_back( (int)b );
    } else {
      std::cout << "Error could not open qConfReg." << std::endl;
    }
    return bitString->size();
  }

  // returns the current bitString
  // bool* getCurrentBitString() { // does not work because std::vector<bool>::data() is not
  // implemented for bools -> using ints instread...
  int* getCurrentBitString() {
    if ( !bitString ) {
      std::cout << "bitString not initialized. Initializing from Global Control Register.";
      updateBitString( "Global Control Register" );
    }
    return bitString->data();
  }

  void printCurrentBitString() {
    std::cout << "bitString->size() = " << bitString->size() << std::endl;
    for (unsigned int i=0; i<bitString->size(); ++i) std::cout << bitString->at(i) << " ";
    std::cout << std::endl;
  }
}
