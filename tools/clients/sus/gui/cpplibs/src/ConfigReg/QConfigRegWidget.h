#ifndef QCONFIGREGWIDGET_H
#define QCONFIGREGWIDGET_H

#include <stdint.h>

#include <memory>
#include <QWidget>
#include <QComboBox>
#include <QLineEdit>

#include "QConfigReg.h"
#include "utils.h"


class QLabel;

class QPushButton;
class QGroupBox;
class QTableWidget;
class QStringList;
class QString;
class QCheckBox;
class QCloseEvent;

namespace SuS
{
  class QConfigRegWidget;

  class SignalsBlocker{
    public:
      SignalsBlocker(QWidget * widget) : blockWidget(widget) {
        blockWidget->blockSignals(true);
      }

      ~SignalsBlocker(){ blockWidget->blockSignals(false); }

    private:
      QWidget *blockWidget;
  };

  /**
   *          \brief GUI class for controlling the output order of a memory structure.
   *          \author Manrfred Kirchgessner from Jan Soldat
   *
   *          Provides a simple Qt GUI for grouping signals to submodules and arrange module sets, modules and submodules in any order desired.
   */

  class OutputGuiQt : public QWidget
          {
            Q_OBJECT

          public:
            OutputGuiQt(QWidget* parent = 0,
                        Qt::WindowFlags fl = 0);
            ~OutputGuiQt();
            void setConfigRegGui(QConfigRegWidget *_qconfigRegWidget);

            void addEntries(int _modSetIndex, QString _newOutputs);


          protected slots:

            void addEntry();

            void outClearSel();

            void saveOutputorder();
            void generateLayout();

          public slots:
            void outClearAll();
            void updateOutputTable();
            void updateModuleSets();

          private:

            QTableWidget *outputTable;
            QLineEdit *outModuleEdit;
            QComboBox *outModuleSetCB;

            QConfigRegWidget* configRegWidget;
          };


          /**
           *          \brief GUI class for controlling the memory structure.
           *          \author Manfred Kirchgessner from Jan Soldat
           *
           *          Provides a simple Qt GUI for controlling the memory structure and its content by wrapping the QConfigReg class.
           *          "Normal" signals up to 32 bit length as well as "series" signals are supported. Latter must be written by bit position ranges and "all 1" or "all 0" bit values.
           */
      class QConfigRegWidget : public QWidget
          {

            friend class OutputGuiQt;

            Q_OBJECT

          public:
            QConfigRegWidget(QWidget* parent = 0,
                             Qt::WindowFlags fl = 0);

            QConfigRegWidget(const QString & _fileName,
                             QWidget* parent = 0,
                             Qt::WindowFlags fl = 0);

            QConfigRegWidget(QConfigReg * _configReg,
                             QWidget* parent = 0,
                             Qt::WindowFlags fl = 0);

            virtual ~QConfigRegWidget();

            void setConfigReg(QConfigReg * _configReg);

            void setSignalValue(const QString & moduleSet, const QString & modules, const QString & signalName, uint32_t value);
            inline uint32_t getSignalValue(const QString & moduleSet, const QString & modules, const QString & signalName){ return configReg->getSignalValue(moduleSet,modules,signalName);}

            inline bool isSignalReadOnly(const QString & moduleSet, const QString & signalName){ return configReg->isSignalReadOnly(moduleSet,signalName);}
            inline bool isChanged() {return changed;}

            inline uint32_t getMaxSignalValue(const QString & moduleSet, const QString & signalName){
              return configReg->getMaxSignalValue(moduleSet.toStdString(),signalName.toStdString());
            }

            inline QStringList getSignalNames(const QString & moduleSet) const {return configReg->getSignalNames(moduleSet);}

            static void initFromSettings(QObject *object, const QString & settingsGroup);
            static void saveSettings(QObject *object, const QString & settingsGroup);

            QConfigReg* getConfigReg() { return configReg; }
            
          signals:

            void modified(void);
            void newFileLoaded();

          protected slots:

            void initGui();
            void openFile();
            void saveFile();
            void saveFileAs();  // triggers file selection dialog
            void newFile();
            void updateHeaders();

            void setInitMode(int enable);
            void printBitfile();
            void addLine();
            void addSignal();
            void removeSignal();

            void addModules();
            void removeModules();

            void addModuleSet();
            void removeModuleSet();
            void renameModuleSet();

            bool saveInitChanges();
            void saveChanges(int tableRow = -1);

            void saveChangesPressed();

            void showOutputGui();
            void cellChanged(int row);


          public slots:

            void emitModified(){ emit modified(); }

            void saveToFile(const QString &filename);

            inline void    setCurrentModules(const QString & moduleStr) { moduleEdit->setText(moduleStr); }
            inline int     getCurrentModuleSetNumber() const { return moduleSetCB->currentIndex(); }
            inline QString getCurrentModuleSetName()   const { return moduleSetCB->currentText(); }
            inline int     getCurrentModuleNumber()    const {
                                                                bool ok = true;
                                                                int module = moduleEdit->text().toInt(&ok);
                                                                return ok ? module : -1;
                                                              }
            void updateModuleSets();
            void updateModules();
            void updateTable();
            void checkModuleEdit();
            void changeReverse();
            void openFile(const QString &fileName);
            void storeModSetAddress();
            inline QString getModuleSetName(int _moduleSet) const { return moduleSetCB->itemText(_moduleSet);}

          protected slots:
            virtual void closeEvent( QCloseEvent* ce );

          private slots:

            void blockGuiSignals(bool block);
            bool checkSignalBitPositions();
            bool allTValuesDefined(int row);

            QString getTSignalName(int row);

            uint32_t getTSignalValue(int row);
            QString getTSignalPositions(int row);
            bool getTSignalReadOnly(int row);
            bool getTSignalActiveLow(int row);
            QString getTSignalAliases(int row);

            void setTSignalName(int row, const QString &signalName);
            void setTSignalBitPositions(int row, const QString &signalName);
            void setTSignalValue(int row, bool various, uint32_t value);
            void setTSignalReadOnly(int row, const QString &signalName);
            void setTSignalActiveLow(int row, const QString & signalName);
            void setTSignalAliases(int row, const QString & signalName);

            void insertCheckBox (int row, bool various, bool value);
            void insertEditField(int row, bool various, uint32_t value);

            void generateLayout();

          private:

            bool initMode;
            bool changed;

            //column numbers
            static const uint8_t table_columnName = 0;
            static const uint8_t table_columnValue = 1;
            static const uint8_t table_columnPosition = 2;
            static const uint8_t table_columnReadOnly = 3;
            static const uint8_t table_columnActiveLow = 4;
            static const uint8_t table_columnAliases = 5;

            QTableWidget *table;

            QLabel    *filenameLabel;
            QLabel    *addressLabel;
            QComboBox *moduleSetCB;
            QComboBox *moduleCB;
            QLineEdit *moduleEdit;
            QLineEdit *addressEdit;
            QLineEdit *initModuleEdit;
            QCheckBox *initModeCB;
            QCheckBox *reverseCB;
            QGroupBox *adminBox;
            QComboBox *initModuleCB;

            QString moduleEditText;


            OutputGuiQt* outputgui;

          protected:

            QConfigReg* configReg;

          public:
            template <typename T = uint32_t>
            static QVector<T> positionListToVector(QString positions)
            {
              return QVector<T>::fromStdVector(utils::positionListToVector<T>(positions.toStdString()));
            }

            template <typename T = uint32_t>
            static QString positionVectorToList(QVector<T> positions)
            {
              return QString::fromStdString(utils::positionVectorToList<T>(positions.toStdVector()));
            }

          };
}

#endif
