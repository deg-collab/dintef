set(ConfigRegGui_SRCS main.cpp)

add_executable(ConfigRegGui WIN32 ${ConfigRegGui_SRCS})

target_link_libraries(ConfigRegGui
  ${QT_LIBRARIES}
  QConfigReg
)
