#include <QApplication>
#include <iostream>

#include "QConfigRegWidget.h"

using namespace SuS;

int main(int argc, char **argv)
{
  QApplication app(argc,argv);

  auto myWidget = new QConfigRegWidget("../ConfigFiles/F1_PixelRegisters.txt");
  myWidget->show();

  return app.exec();

}


