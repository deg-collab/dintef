#ifndef QCONFIGREG_H
#define QCONFIGREG_H

#include <vector>

#include <QString>
#include <QStringList>
#include <QVector>
#include <QtDebug>


#include "ConfigReg.h"

namespace SuS {

  class QConfigReg : public ConfigReg
  {
    public:

      QConfigReg();
      QConfigReg(const QString & _filename);

      ~QConfigReg();

      inline bool initFromFile(const QString &fileName){return ConfigReg::initFromFile(fileName.toStdString());}
      inline void saveToFile(const QString &fileName){ConfigReg::saveToFile(fileName.toStdString());}

      inline QString getQFileName() const {return QString::fromStdString(ConfigReg::getFileName());}

      inline int getNumBitsPerModule(const QString & moduleSet){return ConfigReg::getNumBitsPerModule(moduleSet.toStdString());}
      inline int getNumModules(const QString & moduleSet){return ConfigReg::getNumModules(moduleSet.toStdString());}
      inline int getNumSignals(const QString & moduleSet){return ConfigReg::getNumSignals(moduleSet.toStdString());}

      inline void addModuleSet(const QString & moduleSetName){ConfigReg::addModuleSet(moduleSetName.toStdString());}
      inline void removeModuleSet(const QString & moduleSetName){ConfigReg::removeModuleSet(moduleSetName.toStdString());}
      inline void renameModuleSet(const QString & moduleSet, const QString & newName){ConfigReg::renameModuleSet(moduleSet.toStdString(),newName.toStdString());}

      inline QString addModules(const QString & moduleSet, const QString & modulesList){return QString::fromStdString(ConfigReg::addModules(moduleSet.toStdString(),modulesList.toStdString()));}
      inline void removeModules(const QString & moduleSet, const QString & modulesList){ ConfigReg::removeModules(moduleSet.toStdString(), modulesList.toStdString());}
      inline QStringList getModules(const QString & moduleSet){ return toQList(ConfigReg::getModules(moduleSet.toStdString())); }

      inline QStringList getSignalNames(const QString & moduleSet){return toQList(ConfigReg::getSignalNames(moduleSet.toStdString()));}
      inline QString getModuleNumberList(const QString & moduleSet){return QString::fromStdString(ConfigReg::getModuleNumberList(moduleSet.toStdString()));}

      inline QString getOutputList(const QString & moduleSet){return QString::fromStdString(ConfigReg::getOutputList(moduleSet.toStdString()));}
      inline void setOutputList(const QString & moduleSet, const QString & outputList){ConfigReg::setOutputList(moduleSet.toStdString(),outputList.toStdString());}

      QVector<bool> printContent(const QString & moduleSet){return QVector<bool>::fromStdVector(ConfigReg::printContent(moduleSet.toStdString()));}
      QVector<bool> printContent(const QString & moduleSet, const QString & module){return QVector<bool>::fromStdVector(ConfigReg::printContent(moduleSet.toStdString(),module.toStdString()));}

      inline void setSignalValue(const QString & moduleSet, const QString & modules, const QString & signalName, uint32_t value){ ConfigReg::setSignalValue(moduleSet.toStdString(),modules.toStdString(),signalName.toStdString(),value); }
      inline uint32_t getSignalValue(const QString & moduleSet, const QString & modules, const QString & signalName){return ConfigReg::getSignalValue(moduleSet.toStdString(),modules.toStdString(),signalName.toStdString()); }

      inline void setRegAddress(const QString & moduleSet, uint32_t address){ ConfigReg::setRegAddress(moduleSet.toStdString(),address); }
      inline uint32_t getRegAddress(const QString & moduleSet){ return ConfigReg::getRegAddress(moduleSet.toStdString()); }
      inline bool isSetReverse(const QString & moduleSet){ return ConfigReg::isSetReverse(moduleSet.toStdString()); }
      inline void changeSetReverse(const QString & moduleSet, bool reverse){ return ConfigReg::changeSetReverse(moduleSet.toStdString(),reverse); }

      inline QString getSignalPositionsList(const QString & moduleSet, const QString & signalName){ return QString::fromStdString(ConfigReg::getSignalPositionsList(moduleSet.toStdString(),signalName.toStdString())); }
      inline bool signalIsVarious(const QString & moduleSet, const QString & signalName, const QString & modulesList){ return ConfigReg::signalIsVarious(moduleSet.toStdString(),signalName.toStdString(),modulesList.toStdString()); }

      inline bool isSignalReadOnly(const QString & moduleSet, const QString & signalName){ return ConfigReg::isSignalReadOnly(moduleSet.toStdString(),signalName.toStdString()); }
      inline bool isSignalActiveLow(const QString & moduleSet, const QString & signalName){ return ConfigReg::isSignalActiveLow(moduleSet.toStdString(),signalName.toStdString()); }
      inline QString getSignalAliases(const QString & moduleSet, const QString & signalName){ return QString::fromStdString(ConfigReg::getSignalAliases(moduleSet.toStdString(),signalName.toStdString())); }

      
      inline QStringList getQModuleSetNames(){ return toQList(ConfigReg::getModuleSetNames()); }

      inline QString getBitName(const QString & moduleSet,int pos) const {
        return QString::fromStdString(ConfigReg::getBitName(moduleSet.toStdString(),pos));        
      }
            
      static QStringList toQList(const std::vector<std::string> & vector);

  };


}




#endif
