import time
import numpy as np


class CmdWriter:

    def __init__ ( self, DintefCmd, jtagController ):
        self.outputFileName = "../../asic/digital/sim/config/cmdsFromSoftware"
        self.simMode = False
        self.commands = np.array([], dtype='int8')
        self.device = DintefCmd.device
        self.gui = DintefCmd.qXiderConfGui
        self.jtag = jtagController



    def setSimMode ( self, val ):
        self.simMode = val

    def getSimMode ( self ):
        self.refreshSimModeFromGui( )
        return self.simMode

    def refreshSimModeFromGui ( self ):
        if self.gui.shown:
            self.setSimMode( self.gui.getSimMode( ) )
        else:
            print("running headless")


    def getJTAGCmdsFromGui( self, get_seqCmds = 1 ):
        self.commands = self.gui.getProgBytes("DACs") 
#        self.commands = np.append( self.commands, self.gui.getProgBytes("Switch") )
        self.commands = np.append( self.commands, self.gui.getProgBytes("Global Control Register") )
        if(get_seqCmds):
            self.commands = np.append( self.commands, self.gui.getProgBytes("Sequencer") )
            for i in np.arange(12):
                self.commands = np.append( self.commands, self.gui.getProgBytes("SequencerTrack", i) )
            self.commands = np.append( self.commands, self.gui.getProgBytes("SequencerHoldGen") )



    def writeCmds( self ):
        self.refreshSimModeFromGui( )
        if( self.simMode ):
            self.writeToFile( )
        else:
            self.writeToASIC( )


            
    def writeToASIC_fillJTAGWriteReg( self, data ):
        try:
            self.jtag.write_ebone_signal_content_by_name("data_in " + str(data)) 
#            self.device.command('REG 1:0x0E04 ' + str(data)) 
        except:
            print('CmdWriter.writeToASIC_fillJTAGWriteReg: Writing single byte to ASIC failed')


    def writeToASIC_sendWriteCommand( self ):
        busy = self.jtag.read_ebone_signal_content_by_name("data_in_busy")
        if(busy == -1):
            raise Exception
        else:
            if(busy):
                    print('Can not write. Fifo is full.')
            else:
                    self.jtag.write_ebone_signal_content_by_name('res_n 1')
                    self.jtag.write_ebone_signal_content_by_name('data_in_write 1')
                    time.sleep(0.0000001)
                    self.jtag.write_ebone_signal_content_by_name('data_in_write 0')
#                    self.device.command('REG 1:0x0E00 5')
#                    time.sleep(0.0000001)
#                    self.device.command('REG 1:0x0E00 1')
#       
    def writeToASIC( self ):
        t = 0
        cnt = 0
        try:
            for i in range(len(self.commands)):
#                start = time.time()
                self.writeToASIC_fillJTAGWriteReg( self.commands[i] )
                time.sleep(0.00001)
                self.writeToASIC_sendWriteCommand( ) 
#                end = time.time()
#                t = t + (end-start)
#                cnt = cnt+1
#            t = t/cnt
#            print(t)
#            print(t*cnt)
            print('JTAG has been programmed. Amount of sent bytes = ' + str(len(self.commands)))
        except:
            print('JTAG could not be programmed')
    
    def writeToFile( self ):
#        print(self.outputFileName)
        outputFile = open(self.outputFileName, 'a')

        for item in self.commands:
            item_hex = hex(item)
            item_hex = item_hex[2:]
            outputFile.write("%s\n" % item_hex)
        outputFile.close()

    def clearSimDumpFile( self ):
        print('Clearing simulation dump file.')
        f = open(self.outputFileName, 'w').close()



    def printJTAGCmds( self ):
        print(self.commands)

    def clearJTAGCmds( self ):
        self.commands = np.array([])
