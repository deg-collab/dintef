import ctypes 
import numpy as np

@ctypes.CFUNCTYPE( None )
def programJtagCallBack( ):
#    try: 
#        dintefcmd
        dintefcmd.do_sus_jtag_program("1");
#    except: 
#        print("dintefcmd is undefined.")

@ctypes.CFUNCTYPE( None )
def runASICCallBack( ):
    try: 
        dintefcmd
        conf_cycle_length = dintefcmd.do_sus_jtag_get_single_reg("Global Control Register", "conf_cycle_length");
        dintefcmd.do_sus_dyn_write_ebone_signal_content_by_name("conf_cycle_length " + str(conf_cycle_length));
        dintefcmd.do_sus_dyn_asic_run("RUN");
    except: 
        print("dintefcmd is undefined.")


@ctypes.CFUNCTYPE( None )
def stopASICCallBack( ):
    try: 
        dintefcmd
        dintefcmd.do_sus_dyn_asic_stop();
    except: 
        print("dintefcmd is undefined.")



@ctypes.CFUNCTYPE( None )
def clearSimDumpFileCallBack( ):
    try: 
        dintefcmd
        dintefcmd.myCmdWriter.clearSimDumpFile();
    except: 
        print("dintefcmd is undefined.")



class QXiderConfGui:

    def __init__( self, 
        _regFname = "sus/gui/config/SUS65T3_default.txt",
        _seqFname = "sus/gui/config/SUS65T3_default.xml"
        ):
        #self.dintefcmd = _dintefcmd
        self.regFname = _regFname;
        self.seqFname = _seqFname;
        self.shown = False
        print("regFname = %s", self.regFname)
        print("seqFname = %s", self.seqFname)
        self.libQXiderConfGui = ctypes.CDLL( "sus/gui/cpplibs/build/libs/libQXiderConfGui.so" )
        self.libQXiderConfGui.QXiderConf_init( self.regFname, self.seqFname,
                programJtagCallBack, runASICCallBack, stopASICCallBack, clearSimDumpFileCallBack )

    def test( self ):
        print self.regFname

    def printBitString( self, _regName = "Global Control Register", _seqTrack = 0 ):
        print("Register \"", _regName, "\":")
        bitStringLength = self.libQXiderConfGui.updateBitString( _regName, _seqTrack )
        self.libQXiderConfGui.getCurrentBitString.restype = ctypes.POINTER(ctypes.c_int * bitStringLength)
        bits = self.libQXiderConfGui.getCurrentBitString( _regName ) # does not work without argument, despite def argument in c++
        self.libQXiderConfGui.printCurrentBitString( _regName )
        #print [i for i in bits.contents]
        return bits
    
    def showGui( self ):
        self.libQXiderConfGui.QXiderConfGui_show_thread()
        self.shown = True

    def getProgBytes( self, _regName = "Global Control Register", _seqTrack = 0 , _readback=False):
        bitStringLength = self.libQXiderConfGui.updateBitString( _regName, _seqTrack )
        self.libQXiderConfGui.getCurrentBitString.restype = ctypes.POINTER(ctypes.c_int * bitStringLength)
        print("BitStringLength = " + str(bitStringLength))
        bits = self.libQXiderConfGui.getCurrentBitString( _regName, _seqTrack )
        byteStringLength = int( np.ceil( bitStringLength / 8.0 ) )
        headerBytes = 6
        jtagInstructionBitLength = 5
        byteArrayLength = byteStringLength + headerBytes
        progBytes = bytearray(byteArrayLength)

        if ( _readback ):
            progBytes[0] = 0x80
        else:
            progBytes[0] = 0x00
        progBytes[1] = jtagInstructionBitLength-1
        progBytes[2] = (jtagInstructionBitLength-1) >> 8
        progBytes[3] = self.libQXiderConfGui.getRegAddress( _regName, _seqTrack )
        bitStringLengthToSend = bitStringLength - 1
        progBytes[4] = bitStringLengthToSend % 256 
        progBytes[5] = (bitStringLengthToSend >> 8) % 256

        # frickle bit stream into bytes
        byte = 0
        for i in range(0,bitStringLength):
            byte = byte | (bits.contents[i] << (i%8))
            if ( (i+1)%8==0 or i==(bitStringLength-1) ):
                if ( i>0 ):
                    progBytes[i/8+headerBytes] = byte
                byte = 0 
        return progBytes


    def getSimMode( self ):
        return self.libQXiderConfGui.getSimMode( )

    def setRegisterSignal( self, _regName, _signalName, _value ):
        self.libQXiderConfGui.setRegisterSignal( _regName, _signalName, _value )

    def getRegisterSignal( self, _regName, _signalName ):
        return self.libQXiderConfGui.getRegisterSignal( _regName, _signalName )

    def loadRegisterConfigFile( self, _fname ):
        self.libQXiderConfGui.loadRegisterConfigFile( _fname )

    def saveRegisterConfigFile( self, _fname ):
        self.libQXiderConfGui.saveRegisterConfigFile( _fname )

    def loadSequencerFile( self, _fname ):
        self.libQXiderConfGui.loadSequencerFile( _fname )

    def saveSequencerFile( self, _fname ):
        self.libQXiderConfGui.saveSequencerFile( _fname )

    def printProgBytes( self, _regName = "Global Control Register", _seqTrack=0 ):
        print ('\n'.join('{:02x}'.format(x) for x in self.getProgBytes( _regName, _seqTrack )))


#q = QXiderConfGui()
#q.printBitString()
#
#q.setRegisterSignal( "Global Control Register", "SerIn", 0)
#q.printProgBytes( "Global Control Register" )
#q.setRegisterSignal( "Global Control Register", "SerIn", 1)
#q.printProgBytes( "Global Control Register" )
#q.printProgBytes( "SequencerTrack", 0 )
#q.printProgBytes( "SequencerTrack", 1 )

#print(byteArr)
# q.showGui()

# c++ code to generate jtag_engine header

#  *pack << jtagEngInstr;
#  *pack << (numChipsJtagChain*c_jtagInstrLength-1) << 0;    
#  int offset = 0;
#  uint8_t byteToPack = 0;
#  for (int i=0; i<numChipsJtagChain; ++i) {
#    byteToPack += jtagInstr << offset;
#    offset += c_jtagInstrLength;
#    if (offset > 7) {
#      *pack << byteToPack;
#      offset %= 8;
#      byteToPack = jtagInstr >> (c_jtagInstrLength-offset);
#    }
#  }
#  if (offset > 0) {
#    *pack << byteToPack;
#  }
#
# 1.     byte:    jtag_engine instruction (0x00 or 0x80 for readback)
# 2.     byte:    number of bits in the instruction -1 lsbyte
# 3.     byte:    number of bits in the instruction -1 msbyte
# 4(ff). byte(s): jtag instruction (replicated if more than one chip in chain)
# 5.     byte:    bitstream length most lsbyte
# 6.     byte:    bitstream length most msbyte -> max bitstream length = 65536
# 7(ff). byte(s): data stream (msb first)
