#! /usr/bin/env python2.7

# This module implements a class that communicates with the JTAG controller on the system's FPGA.
# This controller then sends the configuration to the ASIC's JTAG interface. 
# Like the dynamic ASIC controller, its hdl instantiation is wrapped in ebone registers which need to be written/read
# out. Thus, it uses the ebone signal & register classes and inherits from the EboneDevice class.
#
#   Author: David Schimansky & ESRF
#
#

import logging as log
import inspect
import time
import re
import sys


import h5py
import numpy as np

import libdeep

from dintefdevice import DintefDevice
from sus.ASICcontrol.dintefSubCmd import DintefSubCmd




class daq(DintefSubCmd):
    prefix = "sus_daq"

    def __init__(self, ctx):
        self.log = log.getLogger("sus.daq")
        self.qXiderConfGui = ctx.qXiderConfGui
        super(daq, self).__init__(ctx, self.log)           # Use constructor of DintefSubCmd class. This will add all the functions of this class to the DintefCmd class


    def print_state(self, *args):
        dev = self.device

        print('test FIFO status:')

        print('\tlsb value       : {}'.format(dev.command('?REG 1:0x404')))
        print('\tmsb value       : {}'.format(dev.command('?REG 1:0x408')))

        value = int(dev.command('?REG 1:0x420'), 16)
        print('\tFIFO size       : {}KB'.format(value / 1024))

        value  = int(dev.command('?REG 1:0x424'),16)
        print('\tWrites cnt      : {}w64 {}B'.format(value, value*8))

        status = int(dev.command('?REG 1:0x428'), 16)
        value = status & 0xffff
        print('\tFIFO wr cnt     : {}w64 {}B'.format(value, value*8))
        print('\tFIFO full       : {}'.format(status & (1<<31)>0))

        status = int(dev.command('?REG 1:0x42c'), 16)
        value = status & 0xffff
        print('\tFIFO rd cnt     : {}w64 {}B'.format(value, value*8))
        print('\tFIFO empty      : {}'.format((status & (1<<31))>0))

        print('\nFast Transmitter status:')
        # DX_STAT page 15@ebm_ebft.pdf
        ans   = dev.command('?REG 1:0x430')
        value = int(ans, 16)
        print('\tdesc status     : {}'.format(ans))
        print('\t  64b word cnt  : {}'.format((value&0x00ffffff)))
        print('\t  Timeout    err: {}'.format((value&(1<<24))>0))
        print('\t  Data phase err: {}'.format((value&(1<<25))>0))
        print('\t  Dst addr   err: {}'.format((value&(1<<26))>0))
        print('\t  Src addr   err: {}'.format((value&(1<<27))>0))
        print('\t  Data phase err: {}'.format((value&(1<<28))>0))
        print('\t  Under-run  err: {}'.format((value&(1<<29))>0))
        print('\t  Global     err: {}'.format((value&(1<<30))>0))
        print('\t  Running       : {}'.format((value&(1<<31))>0))
        # GSR page 12@ebm_ebft.pdf
        ans   = dev.command('?REG 1:0x434')
        value = int(ans, 16)
        print('\tGSR             : {}'.format(ans))
        print('\t  IP version    : {:02X}'.format((value&0xff)))
        print('\t  FIFO ready    : {}'.format((value & (1<<26))>0))
        print('\t  FIFO empty    : {}'.format((value & (1<<27))>0))
        busw = (32,64,128,256)[(value & 0xf0000000)>>28]
        print('\t  Bus width     : {}'.format(busw))
        print('\tFIFO word cnt   : {}'.format(dev.command('?REG 1:0x438')))

        value   = int(dev.command('?REG 1:0x43c'),16)
        print('\tFIFO loops cnt  : {}'.format(value & 0xffff))
        payload =(128,256,512,1024)[(value & 0xf0000000)>>28]
        print('\tPCIe payload sz : {}bytes'.format(payload))

        print('\nFast Transmitter control:')
        print('\tD0_SRCE         : {}'.format(dev.command('?REG 1:0x600')))
        print('\tD0_SADRS        : {}'.format(dev.command('?REG 1:0x604')))
        print('\tD0_DLSW         : {}'.format(dev.command('?REG 1:0x60C')))
        print('\tD0_DMSW         : {}'.format(dev.command('?REG 1:0x60D')))
        print('\tD0_CTRL         : {}'.format(dev.command('?REG 1:0x614')))
        print('\tD0_GSR          : {}'.format(dev.command('?REG 1:0x634')))
        print('\tD0_STAT         : {}'.format(dev.command('?REG 1:0x638')))

    def start(self, *args):
        dev = self.device

        print('Start never ending FIFO filling\n')
        dev.command('REG 1:0x40c 0')
        dev.command('REG 1:0x40c 1')
        dev.command('REG 1:0x40c 0')


    def stop(self, *args):
        dev = self.device

        print('Stop never ending FIFO filling\n')
        dev.command('REG 1:0x40c 0')
        dev.command('REG 1:0x40c 2')
        dev.command('REG 1:0x40c 0')

    def reset(self, *args):
        dev = self.device

        print('Stop any DMA transfer and reset EBONE\n')
        # reset EBONE
        dev.command('REG 0:0x14c 0x80000000')
        # reset FIFO
#        dev.command('REG 1:0x41c 0x80000000')
#        dev.command('REG 1:0x41c 0x00000000')
        dev.command('REG 1:0x410 0x00000001')
        dev.command('REG 1:0x410 0x00000000')





# FIFO read function provided by ESRF
    def read_print(self, *args):
        dev = self.device

        argins=args[0].split()
        # optional argin number of readout loops
        nloops = sys.maxint
        nloops_str = 'infinite'
        try:
            nloops = int(argins[0])
            nloops_str = '{}'.format(nloops)
        except:
            pass

        # read FIFO size in bytes
        ndat = int(dev.command('?REG 1:0x420'), 16)

        # convert to w64
        ndat /= 8

        # take only half FIFO (cf FIFO 'almost full' according to doc)
        ndat /= 2

        # optional argin number of w64 read on each loop
        try:
            ndat = int(argins[1])
            # round to a power of 2
            #ndat = 1 << (ndat.bit_length()-1)
        except:
            pass

        print('\nReading data from FIFO')
        print('\tNumber of loops : {}'.format(nloops_str))
        print('\tRead each loop  : {}w64 {}B'.format(ndat, ndat*8))

        # loop over read
        for i in xrange(nloops):

            # TODO: add timeout
            print('\nWait for FIFO filled enough, loop: {}'.format(i+1))
            while True:
                status = int(dev.command('?REG 1:0x42c'), 16)
                value = status & 0xffff
                print('\tFIFO rd cnt     : {}w64 {}B'.format(value, value*8))
                if (value >= ndat):
                    break

            # TODO: add duration benchmarking
            print('Request DMA readout     : {}w64 {}B'.format(ndat, ndat*8))
            try:
                (ans, dat) = dev.command('?*DMA', ndat*8)
            except:
                print('READOUT FAILED, ABORTING !!!!!')
                break

            print('\tData len read   : {}B'.format(len(dat)))
            print('\tData values ... : {} ...'.format(dat[0:8]))

            # casting bytes to w64
            if (len(dat) < 32):
                continue
            datw64  = struct.unpack_from('<2q',dat)
            #print('\t                  ', end=' ')
            print('\t                  ')
            for j in range(len(datw64)):
                #print('0x{:08X}'.format(datw64[j]), end=' ')
                print('0x{:08X}'.format(datw64[j]))
            #print('...', end=' ')
            print('...')
            datw64  = struct.unpack_from('<2q',dat[-16:])
            for j in range(len(datw64)):
                #print('0x{:08X}'.format(datw64[j]), end=' ')
                print('0x{:08X}'.format(datw64[j]))
            print('')
 




    def print_read(self, *args):
        dev = self.device
        
        ndat = int(dev.command('?REG 1:0x420'), 16)
        # convert to w64
        ndat /= 8

        # take only half FIFO (cf FIFO 'almost full' according to doc)
        ndat /= 2

        # words in fifo
        nwords = int(dev.command('?REG 1:0x42c'), 16)

        # optional argin number of w64 read on each loop
        try:
            argins=args[0].split()# optional argin number of readout loops
            nwords = int(argins[0])
        except:
            pass
        if(nwords == 0 or nwords == 0x80000000):
#            print(nwords)
            print('FIFO is empty')
        else:
            print('\nReading data from FIFO')
            print('\tRead each loop  : {}w64 {}B'.format(nwords, nwords*8))

            for i in xrange(nwords):
                status = int(dev.command('?REG 1:0x42c'), 16)
                currentWordNb = status & 0xffff
                print('\tFIFO rd cnt     : {}w64 {}B'.format(currentWordNb, currentWordNb*8))

                # TODO: add duration benchmarking
                print('Request DMA readout     : {}w64 {}B'.format(1, 8))
                try:
                    (ans, dat) = dev.command('?*DMA', 8)
                    print('\tData len read   : {}B'.format(len(dat)))
                    print('\tData values ... : {} ...'.format(dat[0:8]))

                except:
                    print('READOUT FAILED, ABORTING !!!!!')
                    return 

    def get_cnt_value(self, *args):
        dev = self.device
        (ans, dat) = dev.command('?*DMA',8)
        print(dat[0])
        print(dat)
        return (ans, dat)


    def get_nb_words_in_fifo(self, *args):
        dev = self.device
        return int(dev.command('?REG 1:0x42c'), 16)



    def write_fifo_to_array(self, *args):
        dev = self.device
        empty_flag = 0
        nwords = self.get_nb_words_in_fifo()
        #nbytes = np.max(self.get_nb_bytes_in_fifo(), 4095)
        nbytes = self.get_nb_bytes_in_fifo()
        #print(nbytes)
        dat = -2
        if(nwords == 0 or nwords == 0x80000000):
            empty_flag = 1
#            self.log.info('FIFO is empty')
       
        if(empty_flag == 0):
            counterVals = np.zeros((int(np.ceil(nwords)), 4, 2), dtype = 'int') 
            print('\nReading data from FIFO')
            print('\nWords to read: {}w64 {}B'.format(nwords, nwords*8))
            
            
            (ans, dat) = dev.command('?*DMA', nbytes)
#            print(ans, dat)
#            print(len(dat))
#            print(len(counterVals))
#            print(len(counterVals[0]))
#            print(len(counterVals[0][0]))
        return dat




    def get_nb_bytes_in_fifo(self, *args):
        dev = self.device
        nwords = self.get_nb_words_in_fifo()
        nbytes = nwords*8        
        return nbytes 



    def convert_bytes_to_coarse_fine(self, twoBytes):
        fine = np.int(twoBytes[0])
        coarse = np.int(twoBytes[1])
        
        fromFineToCoarse = (fine & 0b11000000) >> 6
        fine = (fine & 0b00111111)
        coarse = (coarse << 2) + fromFineToCoarse
       
        return (coarse, fine)


    def convert_fifo_content_to_coarse_fine(self, inputData):
        dataOut = np.zeros(len(inputData))
        for i in np.arange(len(dataOut)/2):
            (coarse, fine) = self.convert_bytes_to_coarse_fine([inputData[2*i], inputData[2*i+1]])
            dataOut[2*i] = coarse
            dataOut[2*i+1] = fine
        return dataOut
#        print dataOut

    def test(self, *args):
        self.ctx.do_sus_dyn_send_test_data()
        time.sleep(0.1)
        tst = self.write_fifo_to_array()
        tst2 = self.convert_fifo_content_to_coarse_fine(tst)
        
        print(tst)
        print(tst2)

#    def write_fifo_to_array(self):
#        dev = self.device
#        ndat = self.do_daq_get_nb_bytes_in_fifo()  
#        nwords = self.do_daq_get_nb_words_in_fifo()
#        if(nwords == 0 or nwords == 0x80000000):
#            print('daq_write_fifo_to_array: FIFO is empty')
#            return -2   
#        else:
#            counterVals = np.zeros((int(np.ceil(nwords/4.)), 4, 2), dtype = 'int') 
#            print('\nReading data from FIFO')
#            print('\nWords to read: {}w64 {}B'.format(nwords, nwords*8))
#
#            for i in xrange(4*np.shape(counterVals)[0]):
#                status = int(dev.command('?REG 1:0x42c'), 16)
#                currentWordNb = status & 0xffff
#                #try:
#                row = int(np.floor(i/4))
#                col = i%4               
#
#                if(i<nwords):
#                    (ans, dat) = self.do_daq_get_cnt_value() 
#                    (coarse, fine) = self.do_daq_convert_bytes_to_coarse_fine(dat)                       
#                    counterVals[row,col] = np.int_([coarse,fine])
#                else:
#                    counterVals[row,col] = -1
#            return counterVals 
#            #except:
#           #     print('READOUT FAILED, ABORTING !!!!!')
#            #    return -1
#        #print(counterVals)



    def write_allFE_to_file(self, fileName):
        dev = self.device
        nwords = self.get_nb_words_in_fifo()
        datatmp = self.write_fifo_to_array()
        hdf5File = h5py.File(fileName,'a')
        for i in np.arange(4):
            counterVals = hdf5File.create_dataset('cntVals FE' + str(i), (np.ceil(nwords),1,2), dtype = 'int')
            counterVals[:,0,0] = np.int_(datatmp[(i+1)::8])                 # fine values
            counterVals[:,0,1] = np.int_(datatmp[i::8])                     # coarse values

#       data = h5py.File(fileName,'a')
        # create dataset in HDF5 file to store counter values
#        for i in np.arange(4):
#                counterVals = hdf5File.create_dataset('cntVals FE' + str(i), (np.ceil(nwords/4.),1,2), dtype = 'int')
#                         
#                counterVals[:,0,0] = np.int_(datatmp[:,i,0]) 
#                counterVals[:,0,1] = np.int_(datatmp[:,i,1])
#                hdf5File['stages FE' + str(i)] = ['coarse','fine']
#                hdf5File['cntVals FE' + str(i)].dims.create_scale(hdf5File['stages FE' + str(i)], 'Stage')
#        #       hdf5File['cntVals FE' + str(i)].dims[2].attach_scale(hdf5File['stages FE' + str(i)])


    def write_singleFE_to_file_DACSweep(self, FEnumber, hdf5File, regConfPath, seqConfPath):
        dev = self.device
                       
        regConf = open(regConfPath)
        regConfstr = regConf.read()
        regConf.close()
        
        seqConf = open(seqConfPath)
        seqConfstr = seqConf.read()
        seqConf.close()

        injStage = 0
        
        
        DACtype = input('DAC to sweep: ')
        DACMin = input('Enter Minimum DAC value: ')
        DACMax = input('Enter Maximum DAC value: ')
        nbDACValues = input('Enter amount of DAC values to scan: ')
        nwords = self.do_daq_get_nb_words_in_fifo()
        counterVals = hdf5File.create_dataset('cntVals', (np.ceil(nwords/4.), nbDACValues, 2), dtype = 'int') 
        counterVals.attrs['Swept DAC: '] = DACtype
        counterVals.attrs['Slow Control Register Configuration'] = regConfstr
        counterVals.attrs['Sequencer Configuration'] = seqConfstr
        DACValues = np.linspace(DACMin, DACMax, nbDACValues, dtype = int)
        print(DACValues)
        for i in np.arange(nbDACValues):       
            print('\nCurrent DAC value: ' + str(DACValues[i]) + '\n')
            self.do_sus_system_reset()
            self.do_pll_init()
            self.do_pll_bypass()
            self.do_sus_jtag_set_single_reg("DACs", DACtype, DACValues[i])
            self.do_sus_jtag_program()
            self.do_sus_dyn_asic_run()
            time.sleep(0.001)
            self.do_sus_dyn_asic_run_ro()
            time.sleep(0.01)
            datatmp = self.do_daq_write_fifo_to_array()
#            print(datatmp[:,1,0])
            if(datatmp.any() == -2):
                print('Error in do_daq_write_singleFE_to_file_DACSweep: Data FIFO was empty!')
            else:
#               counterVals[:,i,0] = datatmp[:,1,0]
#                counterVals[:,i,1] = datatmp[:,1,1]    
                counterVals[:,i,0] = datatmp[:,int(FEnumber),0]
                counterVals[:,i,1] = datatmp[:,int(FEnumber),1]    
            
        hdf5File['DACvalues'] = DACValues
        hdf5File['cntVals'].dims.create_scale(hdf5File['DACvalues'], 'InjectDAC')
        hdf5File['cntVals'].dims[2].attach_scale(hdf5File['DACvalues'] )

        hdf5File['stages'] = ['coarse','fine']
        hdf5File['cntVals'].dims.create_scale(hdf5File['stages'], 'Stage')
        hdf5File['cntVals'].dims[2].attach_scale(hdf5File['stages'])


