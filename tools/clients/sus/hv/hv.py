import serial
import time 
import logging as log 

from sus.ASICcontrol.dintefSubCmd import DintefSubCmd

class hv(DintefSubCmd):
    prefix = "sus_hv"

    def __init__(self, ctx, portName="/dev/ttyS0", baudrate=9600, bytesize = serial.EIGHTBITS, parity = serial.PARITY_NONE, stopbits = serial.STOPBITS_ONE, timeout = 1):
        self.portName = portName
        self.baudrate = baudrate
        self.bytesize = bytesize
        self.parity = parity
        self.stopbits = stopbits
        self.timeout = timeout
        self.port = serial.Serial(port = self.portName, baudrate = self.baudrate, bytesize = self.bytesize, parity = self.parity, stopbits = self.stopbits, timeout = self.timeout)
        self.cr = b"\x0d"
        self.lf = b"\x0a"
        self.log = log.getLogger("sus.hv")
        super(hv, self).__init__(ctx, self.log)


    def close(self):
        self.port.close()

    def read_word(self):
        cr = self.cr 
        lf = self.lf
        port = self.port
        s = ""
        tmp = port.read()
        while(tmp.encode("hex") != lf.encode("hex") and (tmp != "")):
            if(s == lf):
                pass
            else:
                s = s+tmp
            tmp = port.read()
#            print(tmp + ": " + tmp.encode("hex"))
        if(s == ""):
            return "Timeout: No word has been read"
        else:
            return s

    def flush(self):
        s = self.read_word()
        while(s != "Timeout: No word has been read"):
            s = self.read_word()


    def write(self, word):
        port = self.port
        end = self.cr + self.lf
#        print(word.encode('utf-8') + end.encode("hex"))
     
        port.write(word.encode('utf-8') + end)


    def print_HV_ID(self, *args):
        self.write("#")
        self.read_word()
        print(self.read_word())


    def get_delay(self, *args):
        self.write("W")
        self.read_word()
        return self.read_word()


    def get_output_voltage(self, *args):
        self.write("U1")
        self.read_word()
        return self.read_word()

    def get_target_voltage(self, *args):
        self.write("D1")
        self.read_word()
        return self.read_word()


    def get_output_current(self, *args):
        self.write("I1")
        self.read_word()
        return self.read_word()


    def get_voltage_limit(self, *args):
        self.write("M1")
        self.read_word()
        return self.read_word()

    def get_current_limit(self, *args):
        self.write("N1")
        self.read_word()
        return self.read_word()

    def get_voltage_ramp(self, *args):
        self.write("V1")
        self.read_word()
        return self.read_word()

    def get_current_trip_overall(self, *args):
        self.write("L1")
        self.read_word()
        return self.read_word()
        self.flush()

    def get_current_trip_mA(self, *args):
        self.write("LB1")
        self.read_word()
        return self.read_word()

    def get_current_trip_uA(self, *args):
        self.write("LS1")
        self.read_word()
        return self.read_word()

    def get_status_word(self, *args):
        self.write("S1")
        self.read_word()
        return self.read_word()

    def get_device_status(self, *args):
        self.write("T1")
        self.read_word()
        return self.read_word()

    def get_auto_start(self, *args):
        self.write("A1")
        self.read_word()
        return self.read_word()




    def set_voltage_ramp(self, value, *args):
        if((value < 2) or (value > 255)):
            self.log.ERROR("Invalid value: {}. Has to be in range 2 to 255 (V/s)".format(value))
        else:
#            self.write("W={}".format(value))
            self.write("V1=" + str(value))
            self.flush()


    def set_target_voltage(self, value, *args):
        self.write("D1=" + str(value))
        self.read_word()
        self.log.info(self.read_word())
        self.flush()

    def set_auto_start(self, value, *args):
        self.write("A1=" + str(value))
        self.read_word()
        self.log.info(self.read_word())
        self.flush()



    def set_delay(self, value, *args):
        if((value < 2) or (value > 255)):
            self.log.ERROR("Invalid value: {}. Has to be in range 2 to 255 (ms)".format(value))
        else:
#            self.write("W={}".format(value))
            self.write("W=" + str(value))
            self.flush()

    def set_current_trip_overall(self, value, *args):
        self.write("L1=" + str(value))
        self.read_word()
        self.log.info(self.read_word())
        self.flush()

    def set_current_trip_mA(self, value, *args):
        self.write("LB1=" + str(value))
        self.read_word()
        self.log.info(self.read_word())
        self.flush()

    def set_current_trip_uA(self, value, *args):
        self.write("LS1=" + str(value))
        self.read_word()
        self.log.info(self.read_word())
        self.flush()



    def start_voltage_ramp(self, *args):
        self.write("G1")
        self.read_word()
        self.log.info(self.read_word())
        self.flush()






#myHV = hv()
#
#myHV.print_HV_ID()
#myHV.set_delay(17)
#print(myHV.get_delay())
#print(myHV.get_status_word())
#print(myHV.get_device_status())
#
#
#
#
#print(myHV.get_delay())
#
#
#print("Voltage = " + myHV.get_output_voltage())
#
#print(myHV.set_target_voltage(10))
#print("Set Voltage to:" + myHV.get_target_voltage())
#
#print("Current: " + myHV.get_output_current())
#
#print("Voltage limit is: " + myHV.get_voltage_limit())
#
#print("Current limit is: " + myHV.get_current_limit())
#
#myHV.set_voltage_ramp(4)
#print("Voltage ramp is: " + myHV.get_voltage_ramp())
#
#myHV.set_current_trip_overall(1000)
#print("Overall current trip is: " + myHV.get_current_trip_overall())
#
#myHV.set_current_trip_mA(2000)
#print("mA current trip is: " + myHV.get_current_trip_mA())
#
#myHV.set_current_trip_uA(100)
#print("uA current trip is: " + myHV.get_current_trip_uA())
#
#print("Status word is: " + myHV.get_status_word())
#
#print("Device status is: " + myHV.get_device_status())
#
#myHV.set_auto_start(0)
#print("Auto start is: " + myHV.get_auto_start())
#
#myHV.start_voltage_ramp()
#
#
