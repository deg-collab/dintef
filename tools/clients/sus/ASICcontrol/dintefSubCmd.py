# This module implements the DintefSubCmd class which is the basic class for any controller of any device (sub device) used
# in the dintef environment. Every other device class inherits from DintefSubCmd, mostly for its constructor.
# The constructor adds all the device class's function to the ctx class provided from outside. In our case:
# The DintefCmd class. This is useful because it allows calling functions from sub devices in the DintefCmd REPL.
#
#   Author: Patrick Nisble
#
#
#
#


import logging as log
import inspect

class DintefSubCmd(object):
    """common parent object for subcommands"""
    prefix = "sus"

    def __init__(self, ctx, logger=None):
        self.ctx = ctx
        self.device = ctx.device
        self.dev = ctx.device
        if logger is not None:
            self.log = logger
        else:
            self.log = log.getLogger("sus")

        for mem in dir(self):
            if inspect.ismethod(getattr(self, mem)) \
               and not mem.startswith("_") \
               and mem not in ["__init__"]:
                name = self.prefix + "_" + mem
                self.log.debug("adding %s as .%s", mem, name)
                setattr(self.ctx, "do_" + name, getattr(self, mem))
                ctx.custom_cmd_names.append("do_" + name)
                # found inside pydancelib/cmd/deepcmd.py:
                # not documented,
                #  DeepCmd.__init__(), is used for command completion
                # commands are processed at pydancelib/cmd/deepcmd.py:
                #  DeepCmd.onecmd() and get "do_prefix


