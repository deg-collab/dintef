# This module implements the controller class for the dynamic ASIC control which is instantiated on the system's
# FPGA. The dynamic ASIC control manages the ASIC RUN signal as well as the data flow coming from the ASIC. 
# Like the JTAG controller, its hdl instantiation is wrapped in ebone registers which need to be written/read
# out. Thus, it uses the ebone signal & register classes and inherits from the EboneDevice class.
#
#
#   Author: David Schimansky
#
#


import logging as log
import inspect
import time
from collections import OrderedDict
import re

import sus.ASICcontrol.registersAndSignals as ras
from sus.ASICcontrol.dintefSubCmd import DintefSubCmd
from sus.ASICcontrol.EboneDevice import EboneDevice


import h5py
import numpy as np

import libdeep

from dintefdevice import DintefDevice
#import sus.gui.CmdWriter as cmdwriter

class t3cont(DintefSubCmd, EboneDevice):
    prefix = "sus_t3cont"

    def __init__(self, ctx):
        self.log = log.getLogger("sus.t3cont")
        super(t3cont, self).__init__(ctx, self.log)
        
        # Generate ebone signals stored in ebone registers for control of the Dynamic Control Block on the FPGA
        # Signals are taken from hdl code
        EboneSignals0 = [   # EBONE register 1:0x1000 
                ras.EboneSignal("do_init_link",             0, 1), 
                ras.EboneSignal("ro_sync_word",             1, 1)
                ]            
        EboneSignals1 = [   # EBONE register 1:0x1004
                ras.EboneSignal("cmd_in_pattern_length",                  0, 6), 
                ras.EboneSignal("cmd_in_pattern_w0",         6, 9),            
                ras.EboneSignal("cmd_in_pattern_w1",         15, 9),
                ]            
        EboneSignals2 = [   # EBONE register 1:0x1008
                ras.EboneSignal("cmd_in_pattern_w2",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w3",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w4",         18, 9)
                ]            
        EboneSignals3 = [   # EBONE register 1:0x100C
                ras.EboneSignal("cmd_in_pattern_w5",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w6",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w7",         18, 9)
                ]            
        EboneSignals4 = [   # EBONE register 1:0x1010
                ras.EboneSignal("cmd_in_pattern_w8",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w9",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w10",         18, 9)
                ]            
        EboneSignals5 = [   # EBONE register 1:0x1014
                ras.EboneSignal("cmd_in_pattern_w11",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w12",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w13",         18, 9)
                ]
        EboneSignals6 = [   # EBONE register 1:0x1018
                ras.EboneSignal("cmd_in_pattern_w14",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w15",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w16",         18, 9)
                ]
        EboneSignals7 = [   # EBONE register 1:0x101C
                ras.EboneSignal("cmd_in_pattern_w17",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w18",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w19",         18, 9)
                ]
        EboneSignals8 = [   # EBONE register 1:0x1020
                ras.EboneSignal("cmd_in_pattern_w20",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w21",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w22",         18, 9)
                ]
        EboneSignals9 = [   # EBONE register 1:0x1024
                ras.EboneSignal("cmd_in_pattern_w23",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w24",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w25",         18, 9)
                ]
        EboneSignals10 = [   # EBONE register 1:0x1028
                ras.EboneSignal("cmd_in_pattern_w26",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w27",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w28",         18, 9)
                ]
        EboneSignals11 = [   # EBONE register 1:0x102C
                ras.EboneSignal("cmd_in_pattern_w29",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w30",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w31",         18, 9)
                ]
        EboneSignals12 = [   # EBONE register 1:0x1030
                ras.EboneSignal("cmd_in_pattern_w32",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w33",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w34",         18, 9)
                ]
        EboneSignals13 = [   # EBONE register 1:0x1034
                ras.EboneSignal("cmd_in_pattern_w35",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w36",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w37",         18, 9)
                ]
        EboneSignals14 = [   # EBONE register 1:0x1038
                ras.EboneSignal("cmd_in_pattern_w38",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w39",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w40",         18, 9)
                ]
        EboneSignals15 = [   # EBONE register 1:0x103C
                ras.EboneSignal("cmd_in_pattern_w41",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w42",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w43",         18, 9)
                ]
        EboneSignals16 = [   # EBONE register 1:0x1040
                ras.EboneSignal("cmd_in_pattern_w44",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w45",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w46",         18, 9)
                ]
        EboneSignals17 = [   # EBONE register 1:0x1044
                ras.EboneSignal("cmd_in_pattern_w47",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w48",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w49",         18, 9)
                ]
        EboneSignals18 = [   # EBONE register 1:0x1048
                ras.EboneSignal("cmd_in_pattern_w50",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w51",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w52",         18, 9)
                ]
        EboneSignals19 = [   # EBONE register 1:0x104C
                ras.EboneSignal("cmd_in_pattern_w53",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w54",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w55",         18, 9)
                ]
        EboneSignals20 = [   # EBONE register 1:0x1050
                ras.EboneSignal("cmd_in_pattern_w56",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w57",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w58",         18, 9)
                ]
        EboneSignals21 = [   # EBONE register 1:0x1054
                ras.EboneSignal("cmd_in_pattern_w59",         0, 9),            
                ras.EboneSignal("cmd_in_pattern_w60",         9, 9),
                ras.EboneSignal("cmd_in_pattern_w61",         18, 9)
                ]
        EboneSignals22 = [   # EBONE register 1:0x1058
                ras.EboneSignal("cmd_in_pattern_w62",         0, 9),
                ras.EboneSignal("cmd_in_pattern_w63",         9, 9)
                ]
        EboneSignals23 = [   # EBONE register 1:0x105C
                ras.EboneSignal("link_detected",              0, 1)
                ]
        EboneSignals24 = [   # EBONE register 1:0x1060
                ras.EboneSignal("pf_current_state",           0, 4),
                ras.EboneSignal("clockgen_all_locked",        4, 1),
                ]


        # Generate ebone registers for T3 control
        self.wrapperRegisters = [
                ras.EboneRegister(ctx, 1, 0x1000, 1, EboneSignals0),
                ras.EboneRegister(ctx, 1, 0x1004, 1, EboneSignals1),
                ras.EboneRegister(ctx, 1, 0x1008, 1, EboneSignals2),
                ras.EboneRegister(ctx, 1, 0x100C, 1, EboneSignals3),
                ras.EboneRegister(ctx, 1, 0x1010, 1, EboneSignals4),
                ras.EboneRegister(ctx, 1, 0x1014, 1, EboneSignals5),
                ras.EboneRegister(ctx, 1, 0x1018, 1, EboneSignals6),
                ras.EboneRegister(ctx, 1, 0x101C, 1, EboneSignals7),
                ras.EboneRegister(ctx, 1, 0x1020, 1, EboneSignals8),
                ras.EboneRegister(ctx, 1, 0x1024, 1, EboneSignals9),
                ras.EboneRegister(ctx, 1, 0x1028, 1, EboneSignals10),
                ras.EboneRegister(ctx, 1, 0x102C, 1, EboneSignals11),
                ras.EboneRegister(ctx, 1, 0x1030, 1, EboneSignals12),
                ras.EboneRegister(ctx, 1, 0x1034, 1, EboneSignals13),
                ras.EboneRegister(ctx, 1, 0x1038, 1, EboneSignals14),
                ras.EboneRegister(ctx, 1, 0x103C, 1, EboneSignals15),
                ras.EboneRegister(ctx, 1, 0x1040, 1, EboneSignals16),
                ras.EboneRegister(ctx, 1, 0x1044, 1, EboneSignals17),
                ras.EboneRegister(ctx, 1, 0x1048, 1, EboneSignals18),
                ras.EboneRegister(ctx, 1, 0x104C, 1, EboneSignals19),
                ras.EboneRegister(ctx, 1, 0x1050, 1, EboneSignals20),
                ras.EboneRegister(ctx, 1, 0x1054, 1, EboneSignals21),
                ras.EboneRegister(ctx, 1, 0x1058, 1, EboneSignals22),
                ras.EboneRegister(ctx, 1, 0x105C, 0, EboneSignals23),
                ras.EboneRegister(ctx, 1, 0x1060, 0, EboneSignals24)
                ]

        self.kWords = { 
                "SYNC" : 0x3C,
                "IDLE" : 0xBC
                }


    def find_nb_cmd_in_pattern_wds(self, *args):
        ebone_signal_names = self.get_ebone_signal_names()
        nb_cmd_in_pattern_wds = 0
        for i in np.arange(len(ebone_signal_names)):
            if re.search("cmd_in_pattern_w", ebone_signal_names[i]):
                nb_cmd_in_pattern_wds = nb_cmd_in_pattern_wds + 1 
        return nb_cmd_in_pattern_wds

    def set_magic_pattern(self, *args):
        self.write_ebone_signal_content_by_name("cmd_in_pattern_length 50")
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w0 {}".format((2**8) + int(self.kWords["SYNC"])))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w1 " + str(0x7f))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w2 " + str(0x8e))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w3 " + str(0xff))
        nb_signals_of_interest = self.find_nb_cmd_in_pattern_wds()
        for i in np.arange(nb_signals_of_interest - 4):
 #           print("cmd_in_pattern_w{} {}".format((i + 4), (2**8) + int(self.kWords["K28.5"])))
            self.write_ebone_signal_content_by_name("cmd_in_pattern_w{} {}".format((i + 4), (2**8) + int(self.kWords["IDLE"])))
        self.log.info("Sending magic pattern to ASIC.")

    def set_addr0_16b_test_pattern(self, *args):
        self.write_ebone_signal_content_by_name("cmd_in_pattern_length 50")
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w0 {}".format((2**8) + int(self.kWords["SYNC"])))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w1 " + str(0xc0))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w2 " + str(0x09))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w3 " + str(0x00))
        nb_signals_of_interest = self.find_nb_cmd_in_pattern_wds()
        for i in np.arange(nb_signals_of_interest - 4):
 #           print("cmd_in_pattern_w{} {}".format((i + 4), (2**8) + int(self.kWords["K28.5"])))
            self.write_ebone_signal_content_by_name("cmd_in_pattern_w{} {}".format((i + 4), (2**8) + int(self.kWords["IDLE"])))
        self.log.info("Sending test pattern to ASIC.")

    def set_addr0_8b_test_pattern(self, *args):
        self.write_ebone_signal_content_by_name("cmd_in_pattern_length 50")
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w0 {}".format((2**8) + int(self.kWords["SYNC"])))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w1 " + str(0x40))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w2 " + str(0x09))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w3 " + str(0x00))
        nb_signals_of_interest = self.find_nb_cmd_in_pattern_wds()
        for i in np.arange(nb_signals_of_interest - 4):
 #           print("cmd_in_pattern_w{} {}".format((i + 4), (2**8) + int(self.kWords["K28.5"])))
            self.write_ebone_signal_content_by_name("cmd_in_pattern_w{} {}".format((i + 4), (2**8) + int(self.kWords["IDLE"])))
        self.log.info("Sending test pattern to ASIC.")



    def set_addr10_16b_test_pattern(self, *args):
        self.write_ebone_signal_content_by_name("cmd_in_pattern_length 50")
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w0 {}".format((2**8) + int(self.kWords["SYNC"])))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w1 " + str(0xc1))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w2 " + str(0x49))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w3 " + str(0x0a))
        nb_signals_of_interest = self.find_nb_cmd_in_pattern_wds()
        for i in np.arange(nb_signals_of_interest - 4):
 #           print("cmd_in_pattern_w{} {}".format((i + 4), (2**8) + int(self.kWords["K28.5"])))
            self.write_ebone_signal_content_by_name("cmd_in_pattern_w{} {}".format((i + 4), (2**8) + int(self.kWords["IDLE"])))
        self.log.info("Sending test pattern to ASIC.")

    def set_addr10_8b_test_pattern(self, *args):
        self.write_ebone_signal_content_by_name("cmd_in_pattern_length 50")
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w0 {}".format((2**8) + int(self.kWords["SYNC"])))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w1 " + str(0x41))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w2 " + str(0x49))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w3 " + str(0x0a))
        nb_signals_of_interest = self.find_nb_cmd_in_pattern_wds()
        for i in np.arange(nb_signals_of_interest - 4):
 #           print("cmd_in_pattern_w{} {}".format((i + 4), (2**8) + int(self.kWords["K28.5"])))
            self.write_ebone_signal_content_by_name("cmd_in_pattern_w{} {}".format((i + 4), (2**8) + int(self.kWords["IDLE"])))
        self.log.info("Sending test pattern to ASIC.")

    def set_addr10_roaddr0_8b_test_pattern(self, *args):
        self.write_ebone_signal_content_by_name("cmd_in_pattern_length 50")
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w0 {}".format((2**8) + int(self.kWords["SYNC"])))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w1 " + str(0x41))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w2 " + str(0x49))
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w3 " + str(0x00))
        nb_signals_of_interest = self.find_nb_cmd_in_pattern_wds()
        for i in np.arange(nb_signals_of_interest - 4):
 #           print("cmd_in_pattern_w{} {}".format((i + 4), (2**8) + int(self.kWords["K28.5"])))
            self.write_ebone_signal_content_by_name("cmd_in_pattern_w{} {}".format((i + 4), (2**8) + int(self.kWords["IDLE"])))
        self.log.info("Sending test pattern to ASIC.")




    def set_sync_idle_pattern(self, *args):
        self.write_ebone_signal_content_by_name("cmd_in_pattern_length 50")
        self.write_ebone_signal_content_by_name("cmd_in_pattern_w0 {}".format((2**8) + int(self.kWords["SYNC"])))
        nb_signals_of_interest = self.find_nb_cmd_in_pattern_wds()
        for i in np.arange(nb_signals_of_interest - 1):
#            print("cmd_in_pattern_w{} {}".format((i + 1), (2**8) + int(self.kWords["K28.5"])))
            self.write_ebone_signal_content_by_name("cmd_in_pattern_w{} {}".format((i + 1), (2**8) + int(self.kWords["IDLE"])))
  
        self.log.info("Sending sync_idle pattern to ASIC.")


    def set_idle_pattern(self, *args):
        self.write_ebone_signal_content_by_name("cmd_in_pattern_length 50")
        nb_signals_of_interest = self.find_nb_cmd_in_pattern_wds()
        for i in np.arange(nb_signals_of_interest ):
#            print("cmd_in_pattern_w{} {}".format((i + 1), (2**8) + int(self.kWords["K28.5"])))
            self.write_ebone_signal_content_by_name("cmd_in_pattern_w{} {}".format(i, (2**8) + int(self.kWords["IDLE"])))

        self.log.info("Sending idle pattern to ASIC.")

    def init_link(self, *args):
        self.set_idle_pattern()
        self.write_ebone_signal_content_by_name("do_init_link 1")
        time_out_cnt = 0
        link_detected = 0
        while((time_out_cnt <= 10) and (link_detected != 1)):
            time.sleep(0.01)
            link_detected = self.read_ebone_signal_content_by_name("link_detected")
            time_out_cnt = time_out_cnt + 1
        
        if(link_detected != 1):
            self.log.error('Could not initialize stable 8b10b link with T3.')
        else:
            self.log.info('Stable 8b10b link with T3 initialized.')
        self.write_ebone_signal_content_by_name("do_init_link 0")


