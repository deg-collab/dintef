#! /usr/bin/env python2.7

# This module implements a class that communicates with the JTAG controller on the system's FPGA.
# This controller then sends the configuration to the ASIC's JTAG interface. 
# Like the dynamic ASIC controller, its hdl instantiation is wrapped in ebone registers which need to be written/read
# out. Thus, it uses the ebone signal & register classes and inherits from the EboneDevice class.
#
#   Author: David Schimansky
#
#





import logging as log
import inspect
import time
from collections import OrderedDict
import re

import sus.ASICcontrol.registersAndSignals as ras
from sus.ASICcontrol.dintefSubCmd import DintefSubCmd
from sus.ASICcontrol.EboneDevice import EboneDevice

import h5py
import numpy as np

import libdeep

from dintefdevice import DintefDevice
import sus.gui.CmdWriter as cmdwriter





class jtag(DintefSubCmd, EboneDevice):
    prefix = "sus_jtag"

    def __init__(self, ctx):
        self.log = log.getLogger("sus.jtag")
        self.qXiderConfGui = ctx.qXiderConfGui
        self.myCmdWriter = cmdwriter.CmdWriter(ctx, self) 
        
        # Generate ebone signals stored in ebone registers for control of the FIFO's wrapping the JTAG interface on the FPGA
        # Signals are taken from hdl code
        EboneSignals0 = [   # EBONE register 1:0x0E00 
                ras.EboneSignal("res_n", 0, 1), 
                ras.EboneSignal("data_out_read", 1, 1),            
                ras.EboneSignal("data_in_write", 2, 1)
                ]            
        EboneSignals1 = [   # EBONE register 1:0x0E04 
                ras.EboneSignal("data_in", 0, 8), 
                ]            
        EboneSignals2 = [   # EBONE register 1:0x0E20 
                ras.EboneSignal("data_out_valid", 0, 1),            
                ras.EboneSignal("data_in_busy", 1, 1)
                ]            
        EboneSignals3 = [   # EBONE register 1:0x0E24 
                ras.EboneSignal("data_out", 0, 8) 
                ]            
                            
        
        
        # Generate ebone registers for control of the FIFO's wrappen the JTAG interface on the FPGA, taken from hdl code
        self.wrapperRegisters = [
                ras.EboneRegister(ctx, 1, 0x0E00, 1, EboneSignals0),
                ras.EboneRegister(ctx, 1, 0x0E04, 1, EboneSignals1),
                ras.EboneRegister(ctx, 1, 0x0E20, 0, EboneSignals2),
                ras.EboneRegister(ctx, 1, 0x0E24, 0, EboneSignals3)
                ]

        super(jtag, self).__init__(ctx, self.log)           # Use constructor of DintefSubCmd class. This will add all the functions of this class to the DintefCmd class

    def init(self, *args):
        self.ctx.do_fifo_reset()




############################################################################ START ASIC JTAG REGISTER CONTROL
# The following functions implement the actual JTAG register control (by the use of the ebone wrapper registers).
# With these functions, on-chip JTAG registers like "DAC" or "Global Control Register" can be programmed.

    def set_single_reg(self, moduleName, regName, value):
        self.qXiderConfGui.setRegisterSignal(moduleName, regName, value)

    def get_single_reg(self, moduleName, regName):
        return self.qXiderConfGui.getRegisterSignal(moduleName, regName)



    def program( self, *args ):
        self.myCmdWriter.getJTAGCmdsFromGui( ) 
        self.myCmdWriter.setSimMode( True )
        self.myCmdWriter.writeCmds( )

    def program_loop(self,*args):
        try:
            while(True):
                self.ctx.do_sus_jtag_program()
                time.sleep(0.1)
        except KeyboardInterrupt:
            self.log.error('Keyboard interrupt')

################################################################################################ END ASIC JTAG REGISTER CONTROL



