# This module implements the ebone signal and register classes. The idea is that a single ebone register holds
# several signals. Every signal knows its name, signal position in the register as well as the amount of bits 
# it occupies.
#
#   Author: David Schimansky
#




from dintefdevice import DintefDevice
import logging as log
import inspect
import time
from collections import OrderedDict
import re

import numpy as np


class Signal():
    def __init__(self, name, bitPosition, nbBits):
        self.name = name
        self.bitPosition = bitPosition
        self.nbBits = nbBits


    def get_name(self):
        return self.name
    def get_bitPosition(self):
        return self.bitPosition
    def get_nbBits(self):
        return self.nbBits


    def set_name(self, name):
        self.name = name
    def set_bitPosition(self, bitPosition):
        self.bitPosition = bitPosition
    def set_nbBits(self, nbBits):
        self.nbBits = nbBits


class EboneSignal(Signal):
    """ Ebone Signal class for device control signals in the Ebone registers.
    An Ebone signal knows its name (from HDL code), its bit Position in its
    respective register and the number of bits it occupies.
    It does NOT know its register's address and it does NOT know its value
    to keep it as simple as possible.
    """
    def __init__(self, name, bitPosition, nbBits):
        self.name = name
        self.bitPosition = bitPosition
        self.nbBits = nbBits


class EboneRegister(DintefDevice):
    """ Ebone Register class for devices in dintef setup environment
    Registers are organised in EboneSignals which are the device control
    signals taken from the HDL code. E.g.: First JTAG register (1:0x0E00):

    Bit     Signal
    0       res_n
    1       data_out_read
    2       data_out_write
    3-31    --

    badr: Base Address in EBONE environment. 1 for all the registers.
    Registers are byte addressed and 32bit wide. Addressing in EBONE
    PCI express device is multiplied by 4 => Subsequent register addresses
    are seperated by 4.
    Registers are organised in blocks of 16. The first 8 are read/write
    the last 8 are read only. Example: The JTAG ebone registers 
    0x0E00, 0x0E04, ..., 0x0E20 are rw,
    0x0E24, ..., 0x0E40 are ro
    """
    def __init__(self, ctx, badr, addr, rw, signals = EboneSignal("Empty", -1, 1)):
        self.log = log.getLogger("sus.eboneReg")
        self.badr = badr                                # Bus Address in PCIe environment
        self.addr = addr                                # Register address on bus
        self.rw = rw                                    # Is register writable?
        self.bits = 32          
        self.signals = signals                          # Signals in register organised by EboneSignals class, every signal knows its name, position in the register and number of bits it occupies
                                                        # it does not know about its value.
        # test if given signals are valid. They are invalid if the sum of the bits they occupy it higher than the width of the register (32 for ebone registers) 
        self.validSignals = 1
#        print(type(signals))
        signalBits = 0 
        if(isinstance(signals, list)):                  # signals have to be given in a list of EboneSignal objects
            for i in np.arange(len(signals)):
                signalBits = signalBits + signals[i].get_nbBits()
                if(not isinstance(signals[i], EboneSignal)):
                    self.log.error("Register " + str(self.badr) + ":" + str(hex(self.addr)) + ": Inserted signal type is " + str(type(signals[i])) + ". It should be EboneSignal.")
                    self.validSignals = 0
                if(signalBits > self.bits):
                    self.log.error("Register " + str(self.badr) + ":" + str(hex(self.addr)) + ": Inserted signals have too many bits (" + str(signalBits)  + "). Ebone registers allow maximum 32 bits.")
                    self.validSignals = 0
        else:
            self.log.error("Register " + str(self.badr) + ":" + str(hex(self.addr)) + ": Given signals have to be in a list.")
            self.validSignals = 0
        # if the signals are valid: assign them to the register's internal signals. If not: Add an error signal to the register's internal signals
        if(self.validSignals):
            self.signals = signals
        else:
            self.signals = [EboneSignal("Error", -1, 1)]
        self.device = ctx.device         # DintefCmd device that allows sending commands to the on-setup CPU
        self.content = 0                 # initialize register content (will be 32 bit wide, just like ebone registers)
        self.read_reg_content()          # read 32 bit binary string from register, convert to integer and write to self.content


######################################################################################################################## READ FUNCTIONS

    def get_badr(self):             
        return self.badr

    def get_addr(self):
        return self.addr

    def get_rw(self):
        return self.rw

    def get_signals(self):
        return self.signals

    def read_reg_content(self):             # Read content from physical representation of register on FPGA in DMAK setup with the ?REG <badr>:<addr> command
#        print("?REG {}:{}".format(self.badr, self.addr))
        content_str = self.device.command("?REG {}:{}".format(self.badr, self.addr))
        content_int = int(content_str, 0)   # Value is returned as hex string. Convert to integer
        self.content = content_int 

    def get_content(self):                  # Return content saved in register class
        self.read_reg_content()
        content = self.content
        return content


    def get_signal_names(self):
        ret = np.array([])
        for i in np.arange(len(self.signals)):
            ret = np.append(ret, self.signals[i].get_name())
        return ret

    def get_signal_bitPositions(self): 
        ret = np.array([])
        for i in np.arange(len(self.signals)):
            ret = np.append(ret, self.signals[i].get_bitPosition())
        return ret

    def get_signal_nbBits(self): 
        ret = np.array([])
        for i in np.arange(len(self.signals)):
            ret = np.append(ret, self.signals[i].get_nbBits())
        return ret

    def print_reg_content(self):        
        self.read_reg_content()
        val = self.content()
        self.log.info("Register {}:{:04X} > {}".format(self.badr, self.addr, val))

    def print_signal_names(self):
        val = self.get_signal_names()
        self.log.info("Register {}:{:04X} > {}".format(self.badr, self.addr, val))
    
    def get_signal_by_name(self, signalName):           # Return signal object for input signal name
        reg_signalNames = self.get_signal_names()
        for i in np.arange(len(self.get_signals())):    
            if(signalName == reg_signalNames[i]):       # If signal can not be found in register: Return "Not found" signal
                return self.signals[i]
        else:
            self.log.error("Signal {} not found in register {}:0x{:04X}".format(signalName, str(self.badr), str(self.addr)))
            return EboneSignal("Not found", -1, 1)


    def read_signal_content_by_name(self, signalName):  # Return signal value for input signal name 
        self.read_reg_content()                         # Update self.content
        reg_content_int = self.content                   
        reg_content_bin_str = "{:032b}".format(reg_content_int)  # Convert read register content to 32 bit wide binary string and revert it. This is necessary
        reg_content_bin_str = reg_content_bin_str[::-1]          # to access the part of the register in which the signal is stored more easily
        signal_content = 0                                       # E.g. for JTAG: data_in is Bit 0-7 in register 1:0x0E04. The register content
                                                                 # sent by the on-setup dintef controller is big-endian. To get reg_content[0:7]
                                                                 # its easier to first invert it
        for i in np.arange(len(self.signals)):
            if(signalName == self.signals[i].get_name()):
                signal_LSB = self.signals[i].get_bitPosition()
                signal_MSB = signal_LSB + self.signals[i].get_nbBits()
                signal_content_str = reg_content_bin_str[signal_LSB:signal_MSB][::-1] # invert it back and 
                signal_content = int(signal_content_str, 2)                           # convert to integer
        return signal_content            

 ################################################################################################################################### WRITE FUNCTIONS
    def write_reg(self, *args):
        if(self.get_rw() == 1):
            dev = self.device
            dev.command("REG {}:{} {}".format(self.badr, self.addr, self.content))
            self.log.debug("Writing {} to register {}:0x{:04X}".format(self.content,self.get_badr(),self.get_addr()))
        else:
            self.log.error("Register {}:0x{:04X} is read only".format(self.badr,self.addr))



    def write_signal_content_by_name(self, signalName, val):    # Writing signal content to a register, the other signals in this same register need
        self.read_reg_content()                                 # need to be taken care of so that they are not overwritten (one can only write a full register at once!)
        signal = self.get_signal_by_name(signalName)            # => Need to do bit shifting and masking out the other bits
        if(signal.get_name() != "Not found"):
            bitPosition = signal.get_bitPosition()
            nbBits = signal.get_nbBits()
            val_bin_str = "{:b}".format(val)                    # Make sure that provided signal content fits into bit size of signal
            if(len(val_bin_str) > nbBits):
                self.log.error("Provided bit size: {}. Signal {} only has {} bits".format(len(val_bin_str), signalName, nbBits))
                self.log.error("Register has not been written.".format(len(val_bin_str), signalName, nbBits))
            else:                                               
                for i in np.arange(nbBits):                     # This is where masking of the other signals takes place. Idea: Delete values at bits of interest and overwrite with new payload. 
                    mask = 0                                                     
                    mask = 1 << (bitPosition+i)                 # First generate a mask for every bit of the signal of interest (1 at every bitPosition+i). 
                    self.content = self.content & ~mask         # Delete value at bit of interest
                    
                val_to_register = val << bitPosition            # Push payload to bits of interest
                self.content = self.content | val_to_register   # payload OR 0 => 0 if payload is 0; 1 if payload is 1
        self.write_reg()                                        # Write the whole register


