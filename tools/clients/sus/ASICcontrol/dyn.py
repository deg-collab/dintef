# This module implements the controller class for the dynamic ASIC control which is instantiated on the system's
# FPGA. The dynamic ASIC control manages the ASIC RUN signal as well as the data flow coming from the ASIC. 
# Like the JTAG controller, its hdl instantiation is wrapped in ebone registers which need to be written/read
# out. Thus, it uses the ebone signal & register classes and inherits from the EboneDevice class.
#
#
#   Author: David Schimansky
#
#


import logging as log
import inspect
import time
from collections import OrderedDict
import re

import sus.ASICcontrol.registersAndSignals as ras
from sus.ASICcontrol.dintefSubCmd import DintefSubCmd
from sus.ASICcontrol.EboneDevice import EboneDevice


import h5py
import numpy as np

import libdeep

from dintefdevice import DintefDevice
#import sus.gui.CmdWriter as cmdwriter

class dyn(DintefSubCmd, EboneDevice):
    prefix = "sus_dyn"

    def __init__(self, ctx):
        self.log = log.getLogger("sus.dyn")
        super(dyn, self).__init__(ctx, self.log)
        
        # Generate ebone signals stored in ebone registers for control of the Dynamic Control Block on the FPGA
        # Signals are taken from hdl code
        EboneSignals0 = [   # EBONE register 1:0x0C00 
                ras.EboneSignal("dat_in_write",             0, 1), 
                ras.EboneSignal("conf_send_test_data",      1, 1),            
                ]            
        EboneSignals1 = [   # EBONE register 1:0x0C04 
                ras.EboneSignal("data_in",                  0, 8), 
                ]            
        EboneSignals2 = [   # EBONE register 1:0x0C08 
                ras.EboneSignal("num_words_to_readout",     0, 14),            
                ras.EboneSignal("conf_cycle_length",        16, 12)
                ]            
        EboneSignals3 = [   # EBONE register 1:0x0C0C 
                ras.EboneSignal("cycle_done_delay",     0, 11),            
                ras.EboneSignal("cycle_done_skips",     11, 11),            
                ras.EboneSignal("cycle_done_delay_en",  22, 1),            
                ras.EboneSignal("cycle_done_delay_single",  23, 1)            
                ]            

        EboneSignals4 = [   # EBONE register 1:0x0C20 
                ras.EboneSignal("data_out_valid",           0, 1), 
                ras.EboneSignal("data_in_busy",             1, 1), 
                ras.EboneSignal("asic_run",                 2, 1), 
                ras.EboneSignal("asic_res",                 3, 1), 
                ras.EboneSignal("asic_ser_dout",            4, 1), 
                ras.EboneSignal("dev_in_fifo_empty",        5, 1) 
                ]            
        EboneSignals5 = [   # EBONE register 1:0x0C24 
                ras.EboneSignal("data_out", 0, 16) 
                ]            
        EboneSignals6 = [   # EBONE register 1:0xCE28 
                ras.EboneSignal("o_current_state", 0, 6) 
                ]            
                           
        EboneSignals7 = [   # EBONE register 1:0x0C2C 
               ras.EboneSignal("o_next_state", 0, 6) 
               ]            
        
        
        # Generate ebone registers for control of the FIFO's wrappen the JTAG interface on the FPGA, taken from hdl code
        self.wrapperRegisters = [
                ras.EboneRegister(ctx, 1, 0x0C00, 1, EboneSignals0),
                ras.EboneRegister(ctx, 1, 0x0C04, 1, EboneSignals1),
                ras.EboneRegister(ctx, 1, 0x0C08, 1, EboneSignals2),
                ras.EboneRegister(ctx, 1, 0x0C0C, 1, EboneSignals3),
                ras.EboneRegister(ctx, 1, 0x0C20, 0, EboneSignals4),
                ras.EboneRegister(ctx, 1, 0x0C24, 0, EboneSignals5),
                ras.EboneRegister(ctx, 1, 0x0C28, 0, EboneSignals6),
                ras.EboneRegister(ctx, 1, 0x0C2C, 0, EboneSignals7)
                ]


    def send_test_data_to_file(self, *args):
        #self.ctx.do_sus_system_reset()
        try:
            argins = args[0].split()
            fileName = argins[0]
        except Exception:
            self.log.error("wrong args")
            self.log.error("usage: send_test_data <hdf5file>")
            return
        hdf5File = h5py.File(fileName, 'w')

        #dev = self.device

        self.send_test_data()
        self.ctx.do_daq_write_allFE_to_file(hdf5File)

    def send_test_data(self, *args):
        #self.ctx.do_sus_system_reset()
        dev = self.device
        nwords = '10'
        
        try:
            argins = args[0].split()
            nwords = argins[0]
        except:
            pass

        # set cycle length to 100
        # and number of words to readout to 128
        self.write_ebone_signal_content_by_name('num_words_to_readout ' + nwords)
        self.write_ebone_signal_content_by_name('conf_cycle_length 100')

        self.write_ebone_signal_content_by_name('conf_send_test_data 1')
        # Send RUN RO command to dynamic control block on FPGA
        self.asic_run("RUN_RO")


    def asic_run(self, args): # default: mode = "RUN"
        dev = self.device
        modes = {
            "RUN"     : 1,        
            "RUN_RO"        : 3,        
            "RUN_RO_CONT"   : 4, 
        }
        argins = dict(enumerate(args.split(" ")))
        if argins.get(0, None) == '':
            argins[0] = 'RUN'
            self.log.info("Using RUN mode as default")
        mode = argins.get(0, 'RUN')
#        mode = int(argins.get(0, 'RUN'))
        if(not mode in modes):
            self.log.info("Specified mode " + mode + " not found. Valid modes are: " + str(modes.keys()))

        else:
#        self.log.info('Send ASIC ' + mode + ' signal')
            self.log.info('Send ASIC ' +str(mode) + ' signal')
            self.log.debug('Dyn Ctrl State before write: ' +
                            str(self.read_ebone_signal_content_by_name('o_current_state')))
            time.sleep(0.000001)
            # write run_ro command in data_in register
            self.write_ebone_signal_content_by_name('data_in ' + str(modes[mode]))
    #        self.write_ebone_signal_content_by_name('data_in ' + str(mode))
            # send write command for dyn ctrl input
            # and keep conf_send_test_data at 1
            self.write_ebone_signal_content_by_name('dat_in_write 1')
    
            self.log.debug('Dyn Ctrl State after write: ' +
                            str(self.read_ebone_signal_content_by_name('o_current_state')))
            time.sleep(0.01)
            # set write back to 0
            self.write_ebone_signal_content_by_name('dat_in_write 0')
            self.log.debug('Dyn Ctrl State at the end of the script: ' +
                            str(self.read_ebone_signal_content_by_name('o_current_state')))

#
#    def asic_run_ro(self, *args):
#        dev = self.device
#        self.log.info('Send ASIC RUN RO signal')
#        self.log.debug('Dyn Ctrl State before write: ' +
#                        str(self.read_ebone_signal_content_by_name('o_current_state')))
#        time.sleep(0.000001)
#        # write run_ro command in data_in register
#        self.write_ebone_signal_content_by_name('data_in 3')
#        # send write command for dyn ctrl input
#        # and keep conf_send_test_data at 1
#        self.write_ebone_signal_content_by_name('dat_in_write 1')
#
#        self.log.debug('Dyn Ctrl State after write: ' +
#                        str(self.read_ebone_signal_content_by_name('o_current_state')))
#        time.sleep(0.01)
#        # set write back to 0
#        self.write_ebone_signal_content_by_name('dat_in_write 0')
#        self.log.debug('Dyn Ctrl State at the end of the script: ' +
#                        str(self.read_ebone_signal_content_by_name('o_current_state')))
#
#    def asic_run(self, *args):
#        dev = self.device
#        self.log.info('Send ASIC RUN signal')
#
#        # set conf_send_test_data to 0
#       # dev.command('REG 1:0x0C00 0')
#        # set dyn ctrl input register to command RUN
##        dev.command('REG 1:0x0C04 1')
#        self.write_ebone_signal_content_by_name('data_in 1')
#        time.sleep(0.000001)
#        # send write command for dyn ctrl input
#        #dev.command('REG 1:0x0C00 1')
#        self.write_ebone_signal_content_by_name('dat_in_write 1')
#        time.sleep(0.01)
#        # set write back to 0
#        #dev.command('REG 1:0x0C00 0')
        self.write_ebone_signal_content_by_name('dat_in_write 0')

    def asic_stop(self, *args):
        dev = self.device
        self.log.info('Send ASIC STOP signal')

        # set dyn ctrl input register to command STOP
        # dev.command('REG 1:0x0C04 2')
        self.write_ebone_signal_content_by_name('data_in 2')
        time.sleep(0.000001)
        # send write command for dyn ctrl input
        # dev.command('REG 1:0x0C00 1')
        self.write_ebone_signal_content_by_name('dat_in_write 1')
        time.sleep(0.01)
        # set write back to 0
#        dev.command('REG 1:0x0C00 0')
        self.write_ebone_signal_content_by_name('dat_in_write 0')

    def asic_rerun(self, *args):
        self.asic_stop()
        # self.do_sus_system_reset()
        self.asic_run()

    def asic_rerun_ro(self, *args):
        self.asic_stop()
        # self.do_sus_system_reset()
        self.asic_run_ro()


    def asic_runloop(self, *args):
        """restart asic run over and over;

        useful to check test pattern at the beginning of the ASIC serial output
        """
        try:
            #            self.in125_out250()
            #            self.do_pll_freerun_multiply2()
            self.ctx.do_sus_jtag_program()

            while (True):
                self.ctx.do_sus_dyn_asic_rerun()
                time.sleep(0.01)
        except KeyboardInterrupt:
            self.log.warning('Keyboard interrupt')

    def asic_runloop_ro(self, *args):
        """restart asic run over and over;

        useful to check test pattern at the beginning of the ASIC serial output
        """
        try:
            #            self.in125_out250()
            #            self.do_pll_freerun_multiply2()
            self.ctx.do_sus_jtag_program()

            while (True):
                self.ctx.do_sus_dyn_asic_rerun()
                time.sleep(0.01)
        except KeyboardInterrupt:
            self.log.warning('Keyboard interrupt')


    def set_cycle_done_delay(self, *args):
        try:
            argins = args[0].split()
            delay = argins[0]
        except:
            self.log.error("Please specify the delay in fast clock cycles ( = PLL output clock cycles)")
        
        if(not isinstance(delay, basestring)):
            delay = str(delay)

        self.write_ebone_signal_content_by_name('cycle_done_delay ' + delay)


#    def asic_run_off(self, *args):  # to be deprecated
#        dev = self.device
#        #       dev.command('REG 1:0x0C08 006400010')
#
#        # set conf_send_test_data to 0
#        dev.command('REG 1:0x0C00 0')
#        # set dyn ctrl input register to command RUN off
#        dev.command('REG 1:0x0C04 0')
#        time.sleep(0.000001)
#        # send write command for dyn ctrl input
#        dev.command('REG 1:0x0C00 1')
#        time.sleep(0.000001)
#        # set write back to 0
#        dev.command('REG 1:0x0C00 0')



