# This is the base interface class for devices on the FPGA wrapped by ebone registers.
# It implements the control of said registers to be inherited by all the other devices
# It can not be instantiated on its own
#
# Author: David Schimansky

import logging as log
import re

import sus.ASICcontrol.registersAndSignals as ras

import numpy as np

#import libdeep



class EboneDevice():  
    def __init__(self):
            self.wrapperRegisters = [ ]             # List of ebone signals wrapping the device 

    
    def get_ebone_wrapper_regs(self, *args):
        regs = self.wrapperRegisters
        return regs

    def print_ebone_wrapper_regs(self, *args):      # Print contents of JTAG wrapper ebone registers
        regs = self.get_ebone_wrapper_regs()
        for i in np.arange(len(regs)):
            currentReg = regs[i]
            badr = currentReg.get_badr()
            addr = currentReg.get_addr()
            signals = currentReg.get_signals() 
            val_inthex = currentReg.get_content()
            self.log.info("-------------------------------------------------------------------------------------")
            self.log.info("Register {}:0x{:04X} > 0b{:032b}".format(badr, addr, val_inthex))
            for j in np.arange(len(signals)):
                name = signals[j].get_name()
                LSB = signals[j].get_bitPosition()
                nbBits = signals[j].get_nbBits()
                MSB = LSB + nbBits
                val_strbin = "{:032b}".format(val_inthex)
#                print(MSB)
#                print(LSB)
                signalVal = val_strbin[-MSB:-LSB]
                if(LSB == 0):
                    signalVal = val_strbin[-MSB::]
                    signalVal_int = int(signalVal,2)
                else:
                    signalVal_int = int(signalVal,2)
                if(nbBits == 1):
                    self.log.info("{}: Bit {} > 0b{:b}".format(name, MSB-1, signalVal_int))
                else:
                    string = "{}: Bits {}-{} > 0b{:0" + str(nbBits) + "b} = 0d{} = 0x{:0X}" 
                    self.log.info(string.format(name, MSB-1, LSB, signalVal_int, signalVal_int, signalVal_int))
            
            self.log.info("-------------------------------------------------------------------------------------")

    def print_ebone_signal_names(self, *args):
        for i in np.arange(len(self.wrapperRegisters)):
            self.wrapperRegisters[i].print_signal_names()

    def get_ebone_signal_names(self, *args):
        ret = np.array([])
        for i in np.arange(len(self.wrapperRegisters)):
            ret = np.append(ret, self.wrapperRegisters[i].get_signal_names())
        return ret 

    def get_ebone_signal_by_name(self, signalName, *args):
        regs = self.wrapperRegisters
        signal = ras.EboneSignal("Not found", -1, 1)
        for i in np.arange(len(self.wrapperRegisters)):
            if(signalName in regs[i].get_signal_names()):
                signal = regs[i].get_signal_by_name(signalName)
        if(signal.get_name() == "Not found"):
            self.log.error("Signal {} not found".format(signalName))
        return signal

    def read_ebone_signal_content_by_name(self, signalName, *args):             # Read content of ebone wrapper register for given input name
        signal_content = 0 
        error_flag = 1
        for i in np.arange(len(self.wrapperRegisters)):
            reg = self.wrapperRegisters[i]
            if(signalName in reg.get_signal_names()):
                signal_content = reg.read_signal_content_by_name(signalName)
                error_flag = 0
        if(error_flag == 1):
            self.log.error("Unknown signal {}".format(signalName))
            signal_content = -1
        return signal_content

    def print_ebone_signal_content_by_name(self, signalName, *args):
        val = self.read_ebone_signal_content_by_name(signalName)
        signal = self.get_ebone_signal_by_name(signalName)
        string = "Value of {} is 0b{:0" + str(signal.get_nbBits()) + "b} = 0d{}"
        if(signal.get_name() != "Not found"):
            self.log.info(string.format(signalName, val, val))
        

    def write_ebone_signal_content_by_name(self, *args):                        # Write content of ebone signal. Usage: write_ebone_signal_content_by_name("<signalName> <value>")
#        argins = args[0].split()
        argins = args[0].split()
        signalName = str(argins[0])
        val = int(argins[1])
        regs = self.wrapperRegisters
        error_flag = 1
        for i in np.arange(len(regs)):                                          # Search for register, the signal is stored in
            if(signalName in regs[i].get_signal_names()):
                regs[i].write_signal_content_by_name(signalName, val)           # Use write functionality of ebone register
                error_flag = 0
        if(error_flag == 1):
            self.log.error("Signal {} not found".format(signalName))

    def write_all_ebone_regs(self, *args):
        regs = self.wrapperRegisters
        for i in np.arange(len(regs)):
            if(regs[i].get_rw() == 1):
                regs[i].write_reg()
 

    def wrapper_regs_to_dict(self):
        regs = self.wrapperRegisters
        regs_dict = {}
        for i in np.arange(len(regs)):
            signal_names = regs[i].get_signal_names()
            for j in np.arange(len(signal_names)):
                val = regs[i].read_signal_content_by_name(signal_names[j])
                regs_dict[signal_names[j]] = val
        return regs_dict


