#! /usr/bin/env python2.7
# This module implements measurement procedures for SUS65T3
#
#
#   Author: David Schimansky
#
#
#
#
#



import logging as log
import inspect
import time
from collections import OrderedDict
import re
import sys
import matplotlib.pyplot as plt
import threading
import signal

from scipy.optimize import curve_fit
import scipy


import h5py
import csv
import numpy as np
import math

from datetime import datetime

import libdeep

from dintefdevice import DintefDevice

import sus.gui.CmdWriter as cmdwriter

from sus.ASICcontrol.dintefSubCmd import DintefSubCmd



np.set_printoptions(threshold=sys.maxsize)

class measurement(DintefSubCmd):
    prefix = "sus_measurement"



    def __init__(self, ctx, jtag, dyn):
        self.log = log.getLogger("sus.measurement")
        self.qXiderConfGui = ctx.qXiderConfGui
        self.jtag = jtag
        self.dyn = dyn

        
        self.nb_FEs = 16 
        self.stageRatio = 8
        self.maxNwordsInFIFO = 16000
        super(measurement, self).__init__(ctx, self.log)

    def write_allFEs_to_hdf5File(self, data, currIndex, hdf5File,
                                 pathToDataset):
        delim = '/'
        if (pathToDataset == ''):
            delim = ''
        
        for i in range(self.nb_FEs):
            #nm = "FE" + str(int(i/2))
            nm = "FE" + str(i)
            valsDedicatedToCurrentFE = data[i::self.nb_FEs]
            #print(len(data))
            #print(len(valsDedicatedToCurrentFE))
            
            print(pathToDataset + delim + nm)
            hdf5File[pathToDataset + delim + nm][:, currIndex] = np.int_(valsDedicatedToCurrentFE)
            
    def write_singleFE_to_hdf5File(self, data, currIndex, hdf5File, FE_nb, pathToDataset = ''):

        delim = '/'
        if(pathToDataset == ''):
            delim = ''
        
        valsDedicatedToCurrentFE = data[i::self.nb_FEs]

        print(pathToDataset + delim + FE_nb)
        hdf5File[pathToDataset + delim + nm][:, currIndex] = np.int_(valsDedicatedToCurrentFE)


###################################################################################################################            
#### Functions to perform JTAG register Sweeps
###################################################################################################################            

    def get_current_date_time(self):
        now = datetime.now()
        dt_string = now.strftime("%Y/%m/%d %H:%M:%S")
        return dt_string 

    def initialize_hdf5File(self, fileName, seqConfstr, regConfstr, is_sweep, nwords = 0, DACs=OrderedDict(), FEsToSave = 'all', mode = 'w', iterations_for_same_value = 0):      # HDF5 File initialization for DAC sweeps
        hdf5File = h5py.File(fileName, mode)
        #hdf5File = h5py.File(fileName, 'a')
        #nwords = 400  # TODO: Read this value from dynamic control register!
        if(nwords == 0):
            nwords = self.dyn.read_ebone_signal_content_by_name('num_words_to_readout') 
        # hdf5File.create_dataset('test/test2', data = [1,2])


        ebone_jtagRegs = self.ctx.do_sus_jtag_wrapper_regs_to_dict() 
        ebone_dynRegs = self.ctx.do_sus_dyn_wrapper_regs_to_dict() 
        ebone_t3contRegs = self.ctx.do_sus_t3cont_wrapper_regs_to_dict() 


        ebone_jtagRegs_str = ''

        for i in np.arange(len(ebone_jtagRegs)):
            key = ebone_jtagRegs.keys()[i]
            value = ebone_jtagRegs.values()[i]
            ebone_jtagRegs_str = '{}{}: {}\n'.format(ebone_jtagRegs_str, key, value)

        ebone_dynRegs_str = ''

        for i in np.arange(len(ebone_dynRegs)):
            key = ebone_dynRegs.keys()[i]
            value = ebone_dynRegs.values()[i]
            ebone_dynRegs_str = '{}{}: {}\n'.format(ebone_dynRegs_str, key, value)

        ebone_t3contRegs_str = ''

        for i in np.arange(len(ebone_dynRegs)):
            key = ebone_t3contRegs.keys()[i]
            value = ebone_t3contRegs.values()[i]
            ebone_t3contRegs_str = '{}{}: {}\n'.format(ebone_t3contRegs_str, key, value)




        hdf5File.attrs["Measurement Date"] = self.get_current_date_time()
        hdf5File.attrs['Sequencer Configuration'] = seqConfstr
        hdf5File.attrs['Slow Control Register Configuration'] = regConfstr
        hdf5File.attrs['JTAG EBONE Registers'] = ebone_jtagRegs_str
        hdf5File.attrs['DYN EBONE Registers'] = ebone_dynRegs_str
        hdf5File.attrs['T3CONT EBONE Registers'] = ebone_t3contRegs_str
       # hdf5File.create_dataset('JTAG EBONE Registers', (2, len(ebone_jtagRegs)), dtype="str")
        
#        jtag_init_regs = np.zeros((2,len(ebone_jtagRegs)), dtype = str)


        if(is_sweep):
            DACtypes = DACs.keys()
            hdf5File.attrs['Swept DACs'] = DACtypes
            for i in np.arange(len(DACs)):
                hdf5File.create_dataset('DAC Values ' + DACtypes[i],
                                        data=DACs[DACtypes[i]])

            measurement_top = hdf5File.create_group("DAC Sweep")
            self.initialize_hdf5File_recur(measurement_top, DACs, nwords, iterations_for_same_value = iterations_for_same_value)
            #        measurement_top.create_dataset('test1/test', dtype = 'int')
            #        print(measurement_top)
            return measurement_top
        
        else:
            self.initialize_hdf5File_recur(hdf5File, DACs, nwords, FEsToSave)
            return hdf5File


    def initialize_hdf5File_recur(self,
                                  hdf5File,
                                  DACs,
                                  nwords,
                                  FEsToSave='all',
                                  iteration=0,
                                  path='',
                                  iterations_for_same_value = 0):

        last_iteration = len(DACs)
        if (iteration < last_iteration):
            currDACname = DACs.keys()[iteration]
            currDACvalues = DACs[currDACname]
            for i in np.arange(len(DACs[currDACname])):
                if (iteration < last_iteration - 1):
                    path += '/' + currDACname + ' ' + str(currDACvalues[i])
                self.initialize_hdf5File_recur(hdf5File,
                                               DACs,
                                               nwords,
                                               iteration=iteration + 1,
                                               path=path,
                                               iterations_for_same_value=iterations_for_same_value)
                if (iteration < last_iteration - 1):
                    path = path.replace(
                        '/' + currDACname + ' ' + str(currDACvalues[i]), '')

        else:
            path = path[1:]
            #           print(hdf5File)
            #           print(path)
            if (path == ''):
                delim = ''
            else:
                delim = '/'
           
            if(FEsToSave == 'all'):
                for i in range(self.nb_FEs):
                    rows = np.ceil(nwords * 8 / float(self.nb_FEs))
                    cols = 1
                    if(len(DACs)):
                        cols = len(DACs[DACs.keys()[-1]])
                    if(iterations_for_same_value != 0):     
                        for j in np.arange(iterations_for_same_value):
                            path_to_dataset = path + delim + 'Iteration ' + str(j) + '/' + 'FE' + str(i)

                            if (not (path_to_dataset in hdf5File)):
                                hdf5File.create_dataset(
                                    path_to_dataset, 
                                    (rows, cols),
                                    dtype='int')
                    else:
                        if (not (path + delim + 'FE' + str(i) in hdf5File)):
                            hdf5File.create_dataset(
                                path + delim + 'FE' + str(i),
                                (rows, cols),
                                dtype='int')


            else:
                    rows = np.ceil(nwords / float(self.nb_FEs))
                    cols = 1
                    if(len(DACs)):
                        cols = len(DACs[DACs.keys()[-1]])


                    if(iterations_for_same_value == 1):     
                        for j in np.arange(iterations_for_same_value):
                            path_to_dataset = path + delim + + 'Iteration ' + str(j) + '/' + 'FE' + str(FEsToSave)
                            if (not (path_to_dataset in hdf5File)):
                                hdf5File.create_dataset(
                                    path_to_dataset, 
                                    (rows, cols),
                                    dtype='int')
                    else:
                        if (not (path + delim + 'FE' + str(FEsToSave) in hdf5File)):
                            hdf5File.create_dataset(
                                path + delim + 'FE' + str(FEsToSave),
                                (rows, cols),
                                dtype='int')
    

#### Generic function to do parameter sweeps (like DACs)   -----------------------------------------------------------------------------------

    def configSigSweep_recursive_loop_and_write(self,
                                          hdf5File,
                                          nwords,
                                          sigs,
                                          iteration=0,
                                          pathToDataset='',
                                          currIndex=0):
        last_iteration = len(sigs)
        if (iteration < last_iteration):
        
            currUniqueSigName = sigs.keys()[iteration]
            currRegName, currSigName = currUniqueSigName.split(":")
            print(sigs, currSigName)
            currSigValues = sigs[currUniqueSigName]
            for i in np.arange(len(sigs[currUniqueSigName])):
#               self.ctx.do_sus_jtag_set_single_reg("DACs", currDACname,
#                                                currDACvalues[i])
                self.ctx.do_sus_jtag_set_single_reg(currRegName, currSigName,
                                                currSigValues[i])
                if (iteration < last_iteration - 1):
                    pathToDataset += '/' \
                        + currUniqueSigName + ' ' + str(currSigValues[i])
                self.configSigSweep_recursive_loop_and_write(
                    hdf5File,
                    nwords,
                    sigs,
                    iteration=iteration + 1,
                    pathToDataset=pathToDataset,
                    currIndex=i)
                if (iteration < last_iteration - 1):
                    pathToDataset = pathToDataset.replace(
                        '/' + currUniqueSigName + ' ' + str(currSigValues[i]), '')
        else:
            pathToDataset = pathToDataset[1:]
            currSig = sigs.keys()[iteration - 1]
            print(pathToDataset)
            print(currSig + ' ' + str(sigs[currSig][currIndex]))
            self.ctx.do_sus_dyn_asic_stop()
            self.ctx.do_sus_jtag_program("0")
            self.ctx.do_sus_t3cont_init_link()
#            self.ctx.do_sus_dyn_asic_run()
            time.sleep(1)

            self.dyn.write_ebone_signal_content_by_name('num_words_to_readout ' + str(min([nwords, self.maxNwordsInFIFO])))

            conf_cycle_length = self.jtag.get_single_reg("Global Control Register", "conf_cycle_length")
            self.dyn.write_ebone_signal_content_by_name('conf_cycle_length ' + str(conf_cycle_length))
            self.dyn.write_ebone_signal_content_by_name('conf_send_test_data 0')
            self.ctx.do_sus_daq_reset()
            time.sleep(1)
            self.ctx.do_sus_dyn_asic_run("RUN")
            time.sleep(0.5)
            self.ctx.do_sus_t3cont_set_magic_pattern()
            time.sleep(1)
            self.ctx.do_sus_t3cont_set_idle_pattern()
            data = self.ctx.do_sus_daq_write_fifo_to_array()
            nwords_remaining = nwords - self.maxNwordsInFIFO
            while(nwords_remaining > 0):
                self.dyn.write_ebone_signal_content_by_name('num_words_to_readout ' + str(min([nwords_remaining, self.maxNwordsInFIFO])))
                self.ctx.do_sus_t3cont_set_magic_pattern()
                time.sleep(0.01)
                self.ctx.do_sus_t3cont_set_idle_pattern()
                data = np.append(data, self.ctx.do_sus_daq_write_fifo_to_array())
                nwords_remaining = nwords_remaining - self.maxNwordsInFIFO
            if(len(data)/2. < nwords):
                self.dyn.write_ebone_signal_content_by_name('num_words_to_readout ' + str(nwords - len(data)/2.))
                self.ctx.do_sus_t3cont_set_magic_pattern()
                time.sleep(0.01)
                self.ctx.do_sus_t3cont_set_idle_pattern()
                data = np.append(data, self.ctx.do_sus_daq_write_fifo_to_array())
                nwords_remaining = nwords_remaining - self.maxNwordsInFIFO
 
#            convertedData = self.ctx.do_sus_daq_convert_fifo_content_to_coarse_fine(datatmp)
            print('Writing Data for ' + currSig + ' = ' + str(sigs[currSig][currIndex]))
            if(currIndex < len(sigs[currSig])-1):
                print('Values left: ' + str(sigs[currSig][currIndex+1:]))
            else:
                print('Values left: None')
            self.write_allFEs_to_hdf5File(data, currIndex, hdf5File,
                                          pathToDataset)


    def configSigSweep(self, *args):
        fileName = 'measurements/DAQ.h5'
        # try:
        argins = args[0].split()
        regConfPath = argins[0]
        seqConfPath = argins[1]
        fileName = argins[2]
        print('sus_measurement: Writing to file ' + fileName)
        # seqConfPath = 'gui/config/\
        # SUS65T2_notransfer_small_Inject_pipeline.xml'
        # regConfPath = 'gui/config/\
        # SUS65T2_injection_CP_notransfer_smallInject_pipeline.txt'
        regConf = open(regConfPath)
        regConfstr = regConf.read()
        regConf.close()

        seqConf = open(seqConfPath)
        seqConfstr = seqConf.read()
        seqConf.close()

        self.qXiderConfGui.loadRegisterConfigFile(regConfPath)
        self.qXiderConfGui.loadSequencerFile(seqConfPath)

        self.ctx.do_sus_system_reset()
        time.sleep(0.000001)
        self.ctx.do_sus_jtag_program("1")
        time.sleep(0.000001)
        
        nbSigsToSweep = ''

        while((type(nbSigsToSweep) != int) or (nbSigsToSweep < 1)):
            try:
                nbSigsToSweep = input('Amount of config signals to sweep: ')
                print(type(nbSigsToSweep))
                if(type(nbSigsToSweep) != int):
                    self.log.error("Input must be integer")
                elif (nbSigsToSweep < 1):
                    self.log.error("Input must be >= 1")
            except KeyboardInterrupt:
               return 
            except:
                self.log.error("Invalid input")

        
        sigs = OrderedDict()
        #        DACs['INJECT0_p'] = [0,1023]
        #        DACs['COMP0_Vthresh_p'] = [0,511,1023]
        #       DACs['INJECT1_p'] = [100,200]

        for i in np.arange(nbSigsToSweep):
            sigName_exists = 0
            regNametmp = ''
            sigNametmp = ''
            while (sigName_exists == 0):
                regNametmp = str(
                   input('Please enter the JTAG register (DACs, Global Control Register, Sequencer) for signal ' + str(i) + ': '))
                sigNametmp = str(
                   input('Please enter the name of signal ' + str(i) +
                          ': '))
                sigName_exists = 1                                                                                             # TODO: FIX WITH PYTHON GUI TO ACTUALLY CHECK IF SIGNAL EXISTS. CPP GUI FUNCTION DOES NOT WORK
#                sigName_exists = self.jtag.signal_name_exists(regNametmp, sigNametmp)
                if(not sigName_exists):
                    self.log.error('Signal ' + regNametmp + ':' + sigNametmp + ' does not exist')
            regName = regNametmp
            sigName = sigNametmp
            uniqueSigName = regName + ':' + sigName
            sigMin = input('Please enter the lowest value of ' + regName + ":" + (sigName) +
                           ': ')
            sigMax = input('Please enter the highest value of ' + regName + ":" + (sigName) +
                           ': ')
            nbSigValues = input('Please enter how many values to sweep for ' + regName + ":" +
                                sigName + ': ')
            nwords = input('Please enter how many words to read: ')

            sigValues = np.linspace(sigMin, sigMax, nbSigValues, dtype=int)
            sigs[uniqueSigName] = sigValues


#        print(len(DACs))
#        print(DACs[DACs.keys()[0]])

        hdf5File = self.initialize_hdf5File(fileName, seqConfstr,
                                            regConfstr, 1, nwords=nwords, DACs=sigs)
        self.configSigSweep_recursive_loop_and_write(hdf5File, nwords, sigs)

#### End of generic parameter sweep function ---------------------------------------------------------------------------------------


#    def take_single_shot(self, args): # default: fileName=singleShot.h5, nruns=100
#        argins = dict(enumerate(args.split()))
#        
#        nruns = int(argins.get(0, 112))
#        if((nruns % self.nb_FEs) != 0):
#            self.log.error("Specified amount of words: %d. It has to be a multiple of %d", nruns, self.nb_FEs)
#
#
#        else:
#            fileName = argins.get(1, 'singleShot.h5')                                               
#    
#            regConfFromGuiStr = 'sus/gui/config/SUS65T2/SUS65T2_tmp.txt'                        # write current configuration in GUI to file and configure asic with it
#            seqConfFromGuiStr = 'sus/gui/config/SUS65T2/SUS65T2_tmp.xml'
#    
#            self.qXiderConfGui.saveRegisterConfigFile( regConfFromGuiStr )
#            self.qXiderConfGui.saveSequencerFile( seqConfFromGuiStr )
#    
#    
#            regConfstr = argins.get(2, regConfFromGuiStr)
#            seqConfstr = argins.get(3, seqConfFromGuiStr)
#               
#                
#            self.ctx.do_sus_jtag_program()
#    #        regConfstr = argins.get(2, 'sus/gui/config/SUS65T2/SUS65T2_default.txt')
#    #        seqConfstr = argins.get(3, 'sus/gui/config/SUS65T2/SUS65T2_default.xml')
#
#            self.dyn.write_ebone_signal_content_by_name('num_words_to_readout ' + str(nruns))
#            self.dyn.write_ebone_signal_content_by_name('conf_cycle_length 100')
#            self.dyn.write_ebone_signal_content_by_name('conf_send_test_data 0')
#            self.ctx.do_sus_dyn_asic_run_ro()
#            time.sleep(1)
#    
#    def print_single_shot(self, args): # default: fileName=singleShot.h5, nruns=100
#            self.take_single_shot(args)
#            self.ctx.do_sus_daq_print_read() 
#            
#    def write_single_shot_to_hdf5file(self, args): # default: fileName=singleShot.h5, nruns=100
#            self.take_single_shot(args)   
#            hdf5File = self.initialize_hdf5File(fileName, seqConfstr, regConfstr, 0)
#    
#            datatmp = self.ctx.do_sus_daq_write_fifo_to_array()
#            convertedData = self.ctx.do_sus_daq_convert_fifo_content_to_coarse_fine(datatmp)
#     
#            self.write_allFEs_to_hdf5File(convertedData, 0, hdf5File,
#                                              '/')


#### DarkFlux measurement functions ------------------------------------------------------------------------------------------------

    def findEdge(self, values, xaxis):
        edgeStart = 0
        edgeEnd = 0
        for i in np.arange(len(values)-1):
            if((values[i] > 0.9) and (values[i+1] <= 0.9)):
                edgeStart = xaxis[i]
            if((values[i] > 0.1) and (values[i+1] <= 0.1)):
                edgeEnd = xaxis[i]
    
        return edgeStart, edgeEnd

    def calcAvgsAndStdDevs(self, counterVals, stageRatio):
        
        counterValsAvg = np.zeros(len(counterVals))
        counterValsStdDev = np.zeros(len(counterVals))
    
        for i in np.arange(len(counterValsAvg)):
            cntVals = stageRatio*counterVals[i][2::self.nb_FEs*2] + counterVals[i][2+1::self.nb_FEs*2]
            counterValsAvg[i] = np.mean( cntVals ) 
            counterValsStdDev[i] = np.std( cntVals )
        return counterValsAvg, counterValsStdDev
    
    def errfunc(self, x,A,mu,sig):
        return A*scipy.special.erfc((x-mu)/(sig*np.sqrt(2)))



    def darkflux(self, *args):       # arguments register config, sequencer config for baseline scan, sequencer config for flux scan, output file name
        argins = args[0].split()
        regConfPath = argins[0]
        seqConfPath_base = argins[1]
        seqConfPath_flux = argins[2]
        fileName = argins[3]

        regConf = open(regConfPath)
        regConfstr = regConf.read()
        regConf.close()

        seqConf = open(seqConfPath_base)
        seqConfbasestr = seqConf.read()
        seqConf.close()


        seqConf = open(seqConfPath_flux)
        seqConffluxstr = seqConf.read()
        seqConf.close()



        self.qXiderConfGui.loadRegisterConfigFile(regConfPath)
        self.qXiderConfGui.loadSequencerFile(seqConfPath_base)

        self.ctx.do_sus_system_reset()
        time.sleep(0.000001)
        self.ctx.do_sus_jtag_program("1")
        time.sleep(0.000001)
        
        whichCompToSweep = -1

        while((whichCompToSweep != 0) and (whichCompToSweep != 1)):
            whichCompToSweep = input('Enter stage of which comparator threshold should be swept (0,1): ') 
            if((whichCompToSweep != 0) and (whichCompToSweep != 1)):
                self.log.error('Invalid input. Has to be 0 or 1.')


        sweptComp = 'COMP' + str(whichCompToSweep) + '_Vthresh_p'
        sigMin_Baseline = -1
        
        while((sigMin_Baseline < 0) or (sigMin_Baseline > 1023)):
            sigMin_Baseline = input('Please enter lowest value for baseline scan: ')
            if((sigMin_Baseline < 0) or (sigMin_Baseline > 1023)):
                self.log.error('Invalid input. Has to be between 0 and 1023')

        sigMax_Baseline = -1
        
        while((sigMax_Baseline < 0) or (sigMax_Baseline > 1023)):
            sigMax_Baseline = input('Please enter the highest value of baseline scan: ')
            if((sigMax_Baseline < 0) or (sigMax_Baseline > 1023)):
                self.log.error('Invalid input. Has to be between 0 and 1023')

        nbSigVals_Baseline = input('Please enter the amount of values for baseline sweep: ')
        nwords_Baseline = 16000

        stepSize_Flux = -1

        while((stepSize_Flux <= 0)):
            stepSize_Flux = input('Please enter the step size for the flux scan: ')
            if((stepSize_Flux <= 0)):
                self.log.error('Invalid input. Has to be bigger than 0')
               

#        nwords_Flux = 20000000
        nwords_Flux = 16000 


        sigs_Baseline = np.linspace(sigMin_Baseline, sigMax_Baseline, nbSigVals_Baseline, dtype = int)
#        sigs = {sweptComp_baseline : np.linspace(sigMin_Baseline, sigMax_Baseline, nbSigVals_Baseline, dtype = int) }

#        pathToBaselineSweep = 'Baseline Sweep/'

        self.dyn.write_ebone_signal_content_by_name('num_words_to_readout ' + str(min([nwords_Baseline, self.maxNwordsInFIFO])))

        conf_cycle_length = self.jtag.get_single_reg("Global Control Register", "conf_cycle_length")
        self.dyn.write_ebone_signal_content_by_name('conf_cycle_length ' + str(conf_cycle_length))
        self.dyn.write_ebone_signal_content_by_name('conf_send_test_data 0')


        data_baseline = np.zeros((nbSigVals_Baseline, 2*nwords_Baseline)) 


        self.qXiderConfGui.loadSequencerFile(seqConfPath_flux)

        self.ctx.do_sus_system_reset()
        time.sleep(0.000001)
        self.ctx.do_sus_jtag_program("1")
        time.sleep(0.000001)
 

        sigs_b = {}
        sigs_b[sweptComp] = sigs_Baseline
        fileName_b = fileName[0:-3] + "_baseline.h5"
        hdf5File_b = self.initialize_hdf5File(fileName_b, seqConfbasestr, regConfstr, 1, nwords=nwords_Baseline, DACs=sigs_b)
        for i in np.arange(nbSigVals_Baseline):
            self.ctx.do_sus_jtag_set_single_reg('DACs', sweptComp, sigs_Baseline[i])

            self.ctx.do_sus_dyn_asic_stop()
            self.ctx.do_sus_jtag_program("0")
            
            time.sleep(1)
            self.ctx.do_sus_dyn_asic_run("RUN_RO")
            time.sleep(1)
            datatmp = self.ctx.do_sus_daq_write_fifo_to_array()
            nwords_remaining = nwords_Baseline - self.maxNwordsInFIFO
            while(nwords_remaining > 0):
                self.dyn.write_ebone_signal_content_by_name('num_words_to_readout ' + str(min([nwords_remaining, self.maxNwordsInFIFO])))
                self.ctx.do_sus_dyn_asic_run("RUN_RO")
                time.sleep(0.01)
                datatmp = np.append(datatmp, self.ctx.do_sus_daq_write_fifo_to_array())
                nwords_remaining = nwords_remaining - self.maxNwordsInFIFO
            if(len(datatmp)/2. < nwords_Baseline):
                self.dyn.write_ebone_signal_content_by_name('num_words_to_readout ' + str(nwords - len(datatmp)/2.))
                self.ctx.do_sus_dyn_asic_run("RUN_RO")
                time.sleep(0.01)
                datatmp = np.append(datatmp, self.ctx.do_sus_daq_write_fifo_to_array())
                nwords_remaining = nwords_remaining - self.maxNwordsInFIFO
            data_baseline[i] = self.ctx.do_sus_daq_convert_fifo_content_to_coarse_fine(datatmp)
            self.ctx.do_sus_measurement_write_allFEs_to_hdf5File(data_baseline[i], i, hdf5File_b)#, "FE1")
        baseline_avgs, baseline_stdevs = self.calcAvgsAndStdDevs(data_baseline, self.stageRatio)
        print(baseline_avgs)
        baseline_edge_start, baseline_edge_end = self.findEdge(baseline_avgs, sigs_Baseline)
        
        popt1, pcov1 = curve_fit(self.errfunc, sigs_Baseline, baseline_avgs, p0 = [1/3., (baseline_edge_start + baseline_edge_end)/2., 3])
        baseline = popt1[1]
        
        sigMin_Flux = int(math.ceil(baseline))
        print(sigMin_Flux)
        if(int(whichCompToSweep) == 1):
            sigMax_Flux = sigMin_Flux+160 
        else:
            sigMax_Flux = sigMin_Flux+25 
        nbSigVals_Flux = (sigMax_Flux - sigMin_Flux)/stepSize_Flux
        sigs_Flux = np.linspace(sigMin_Flux, sigMax_Flux, nbSigVals_Flux, dtype = int)

#        print(baseline, nbSigVals_Flux)

 #       print(type(nbSigVals_Flux), type(nwords_Flux))
        sigs_f = {}

        sigs_f[sweptComp] = sigs_Flux

        fileName_f = fileName[0:-3] + "_flux.h5"

        hdf5File_f = self.initialize_hdf5File(fileName_f, seqConffluxstr, regConfstr, 1, nwords=nwords_Flux, DACs=sigs_f)
        hdf5File_b.create_dataset("Baseline FE1", data = baseline)
        hdf5File_f.create_dataset("Baseline FE1", data = baseline)


        print("Starting Flux Scan")


        for i in np.arange(nbSigVals_Flux):
            print("Current value: " + str(sigs_Flux[i]))
            print("Values to go: " + str(sigs_Flux[i:]))
            self.ctx.do_sus_jtag_set_single_reg('DACs', sweptComp, sigs_Flux[i])

            self.ctx.do_sus_dyn_asic_stop()
            self.ctx.do_sus_jtag_program("0")
            
            time.sleep(1)
            self.ctx.do_sus_dyn_asic_run("RUN_RO")
            time.sleep(1)
            datatmp = self.ctx.do_sus_daq_write_fifo_to_array()
            nwords_remaining = nwords_Flux - self.maxNwordsInFIFO
            while(nwords_remaining > 0):
                self.dyn.write_ebone_signal_content_by_name('num_words_to_readout ' + str(min([nwords_remaining, self.maxNwordsInFIFO])))
                self.ctx.do_sus_dyn_asic_run("RUN_RO")
                time.sleep(0.01)
                datatmp = np.append(datatmp, self.ctx.do_sus_daq_write_fifo_to_array())
                nwords_remaining = nwords_remaining - self.maxNwordsInFIFO
            if(len(datatmp)/2. < nwords_Flux):
                self.dyn.write_ebone_signal_content_by_name('num_words_to_readout ' + str(nwords_Flux - len(datatmp)/2))
                self.ctx.do_sus_dyn_asic_run("RUN_RO")
                time.sleep(0.01)
                datatmp = np.append(datatmp, self.ctx.do_sus_daq_write_fifo_to_array())
                nwords_remaining = nwords_remaining - self.maxNwordsInFIFO

            data_Flux  = self.ctx.do_sus_daq_convert_fifo_content_to_coarse_fine(datatmp)
            self.ctx.do_sus_measurement_write_allFEs_to_hdf5File(data_Flux, i,  hdf5File_f)# ,"FE1")
        


#            print('Writing Data for ' + sweptComp + ' = ' + str(sigs[sweptComp][i]))
#            if(currIndex == len(sigs[sweptComp])-1):
#                print('Values left: ' + str(sigs[sweptComp][i+1:]))
#            else:
#                print('Values left: None')
#            self.write_singleFE_to_hdf5File(convertedData, i, hdf5File, "FE1",
#                                          pathToDataset)

#### End of DarkFlux measurement functions ------------------------------------------------------------------------------------------------


# Added by Patrick

    def simple_noise_test(self, args):  # fileName='noise.h5', nbins=16, nruns=100, minval=0, maxval=1024, *args):
        """ Performa Noise Test for given number of runs

        """

        argins = dict(enumerate(args.split(" ")))
        if argins.get(0, None) == '':
            argins[0] = 'noise.h5'
        fileName = argins.get(0, 'noise.h5')
        nbins = int(argins.get(1, 16))
        nruns = int(argins.get(2, 100))
        minval = int(argins.get(3, 0))
        maxval = int(argins.get(4, 1024))

        self.ctx.do_sus_system_init_nogui()
        self.ctx.do_pll_init()
        time.sleep(0.000001)
        self.ctx.do_pll_bypass()
        self.qXiderConfGui.loadRegisterConfigFile(
            "gui/config/SUS65T2_default.txt")
        self.qXiderConfGui.loadSequencerFile(
            "gui/config/SUS65T2_simple_noise_test.xml")

        # self.ctx.do_daq_write_fifo_to_array()  # empty fifo

        # DHF5 Setup
        hdf5File = h5py.File(fileName, 'a')

        ## group for new experiment run
        en = len(hdf5File.keys())
        eno = str(en)  # experiment no
        hdf5File.create_group(eno)
        hdf5File[eno].attrs['ctime'] = time.strftime("%Y.%m.%d %H:%M:%S", time.gmtime())
        hdf5File[eno].attrs['type'] = "simple_noise_test"
        with open('gui/config/SUS65T2_default.txt', 'r') as f:
            hdf5File[eno].attrs['conf'] = f.read()
        last_eno = hdf5File.get(str(en - 1), None)
        last_id = last_eno.attrs.get('asic_id', -1) if last_eno is not None else -1
        new_id = input("ASIC ID [{}]: ".format(last_id))
        hdf5File[eno].attrs['asic_id'] = new_id if new_id else last_id

        # temp storage
        vals_vth = np.arange(minval, maxval, nbins, dtype=int)  # max val

        for val in vals_vth:
            final_data = {
                k: {
                    j: np.array([]) for j in ['coarse', 'fine']
                } for k in range(4)
            }
            # GENERATION
            self.log.info("Running Noise Test (COMP0_Vthresh_p: {})".format(val))

            # set Vth
            self.ctx.do_sus_jtag_set_single_reg("DACs", "COMP0_Vthresh_p", val)
            self.ctx.do_sus_jtag_program()

            ## run_ro
            time.sleep(0.001)
            self.write_ebone_signal_content_by_name('num_words_to_readout {}'.format(nruns*4)) # nwords/8 = nreads

            self.write_ebone_signal_content_by_name('conf_cycle_length 101')
            self.write_ebone_signal_content_by_name('conf_send_test_data 0')
#            self.ctx.susDyn.asic_run_ro(nwords=nruns *4)              
            self.ctx.susDyn.asic_run_ro()   
            # READOUT
            time.sleep(.01)
            nw = self.ctx.do_sus_daq_get_nb_words_in_fifo()
            if (2147483648 == nw):
                raise Exception("empty FIFO")
            for _ in range(nruns):
                for fe in range(4):
                    status = int(self.dev.command('?REG 1:0x42c'), 16)
                    (_, dat) = self.dev.command('?*DMA', 8)
                    time.sleep(.001)
                    # if fe <=1:
                        # print(fe, dat)

                    data = self.ctx.do_sus_daq_convert_bytes_to_coarse_fine(dat)
                    final_data[fe]['coarse'] = np.append(
                        final_data[fe]['coarse'],
                        data[0])

                    final_data[fe]['fine'] = np.append(
                        final_data[fe]['fine'],
                        data[1])

            ## self.ctx.do_daq_write_fifo_to_array()  # empty fifo
            time.sleep(0.0001)

            # Write to File
            ## subgroups per vth
            sg = "{}/{}".format(eno, 'COMP0_Vthresh_p={:04d}'.format(val))
            hdf5File.create_group(sg)
            hdf5File[sg].attrs['COMP0_Vthresh_p'] = val
            for fe in range(4):
                for t in ['coarse', 'fine']:
                    hdf5File.create_dataset(
                        "{}/FE{}_{}".format(sg, fe, t),
                        data=final_data[fe][t])

    def _system_init(self, *args):
        self.ctx.do_sus_system_init_nogui()
        # self.ctx.do_sus_system_init()
        self.ctx.do_pll_init()
        time.sleep(0.000001)
        self.ctx.do_pll_bypass()

    def _read_fifo_word(self):
        (_, dat) = self.dev.command('?*DMA', 8)
        return self.ctx.do_sus_daq_convert_bytes_to_coarse_fine(dat)

    def _read_fifo_nwords(self, nruns=100, fes=4):
        self.write_ebone_signal_content_by_name('num_words_to_readout {}'.format(nruns*fes))
        self.write_ebone_signal_content_by_name('conf_cycle_length 101')
        self.write_ebone_signal_content_by_name('conf_send_test_data 0')
        self.ctx.susDyn.asic_run_ro()   
        
#        self.ctx.susDyn.asic_run_ro(nwords=nruns * fes)
        time.sleep(.001)

        out = [{"coarse": [], "fine": []} for _ in range(fes)]
        for _ in range(nruns):
            for fe in range(fes):
                data = self._read_fifo_word()
                out[fe]['coarse'].append(data[0])
                out[fe]['fine'].append(data[1])
            # print(out)

        return out

    def _write_dac_from_dict(self, d):
        for k, v in d.items():
            self.ctx.do_sus_jtag_set_single_reg("DACs", k, v)
        self.ctx.do_sus_jtag_program()
        time.sleep(.01)

    def _gen_sweep_recursive(self, daclist, **kwargs):
        name = daclist[0][0]
        for dacval in np.arange(*daclist[0][1], dtype=int):
            # p2.7 pattern for recursive yield
            kwargs[name] = dacval
            if len(daclist[1:]) > 0:
                for i in self._gen_sweep_recursive(daclist[1:], **kwargs):
                    yield i
            else:
                yield kwargs

    def _hdf5sg_from_dict(self, d):
        """ generate sg/ds path for store inside hdf5 file

        note: h5py autogenerates any sg in the "sg/ds" path
        """
        return "/".join([
            "=".join([k, str(v)]) for k, v in d.items()
        ])

    def sweeping_tests(self, args):
        self._system_init()
        regconf = "gui/config/SUS65T2_default.txt"
        seqconf = "gui/config/SUS65T2_simple_noise_test_nores.xml"

        self.qXiderConfGui.loadRegisterConfigFile(regconf)
        self.qXiderConfGui.loadSequencerFile(seqconf)
        fileName = "asic_tests.h5"
        dacs = [
            ["COMP0_Vthresh_p", [300, 400, 2]],
            ["AMP_Vref_p", [300, 400, 2]]
        ]

        hdfFile = h5py.File(fileName, 'a')
        eno = str(len(hdfFile.keys()))
        hdfFile.create_group(eno)
        hdfFile[eno].attrs['ctime'] = time.strftime(
            "%Y.%m.%d %H:%M:%S", time.gmtime())
        hdfFile[eno].attrs['type'] = "noise_test_sweep"
        hdfFile[eno].attrs['Swept DACs'] = [d[0] for d in dacs]
        with open(seqconf, 'r') as f:
            hdfFile[eno].attrs['Sequencer Configuration File'] = seqconf
            hdfFile[eno].attrs['Sequencer Configuration'] = f.read()
        with open(regconf, 'r') as f:
            hdfFile[eno].attrs['Slow Control Register Configuration File'] = regconf
            hdfFile[eno].attrs['Slow Control Register Configuration'] = f.read()
        for dacset in self._gen_sweep_recursive(dacs):
            print(dacset)
            sg = self._hdf5sg_from_dict(dacset)
            data = self._read_fifo_nwords()
            self._write_dac_from_dict(dacset)
            for d in range(len(data)):
                for g in ['coarse', 'fine']:
                    ds = "{}/FE{}_{}".format(
                        sg, d, g
                    )
                    hdfFile[eno].create_dataset(
                        ds,
                        data=data[d][g],
                        dtype='u2'  # 2byte unsigned int
                    )
                    for dac, v in dacset.items():
                        hdfFile[eno][ds].attrs[dac] = v

        hdfFile.close()

    def testing_fifo(self, args):
        argins = dict(enumerate(args.split()))
        fileName = argins.get(0, 'noise.h5')
        nbins = argins.get(1, 16)
        nruns = argins.get(2, 100)
        minval = argins.get(3, 0)
        maxval = argins.get(4, 1024)

        print(fileName, nbins, nruns, minval, maxval)





###################################################################################################################            
#### Start of Functions to perform continuous measurements with extra thread
###################################################################################################################            



class Reader(DintefSubCmd):
    prefix= "sus_measurement_reader"

    def __init__(self, ctx, jtag, dyn):
        self.log = log.getLogger("sus.measurement")
        self.qXiderConfGui = ctx.qXiderConfGui
        self.jtag = jtag
        self.dyn = dyn
        self.stop_flag = threading.Event()
        self.running = 0
        self.data = np.array([])
        signal.signal(signal.SIGTERM, self.stop_cont_read)
        signal.signal(signal.SIGINT, self.stop_cont_read)
        
        super(Reader, self).__init__(ctx, self.log)



    def single_read(self, args):
        data = self.ctx.do_sus_daq_write_fifo_to_array()
        self.data = np.append(self.data, self.ctx.do_sus_daq_convert_fifo_content_to_coarse_fine(data))
    

    def cont_read(self):
        while(not self.stop_flag.is_set()):
            nwords = self.ctx.do_sus_daq_get_nb_words_in_fifo()
            if(nwords != 0 and nwords != 0x80000000):
                self.single_read("")
            else:
                pass

    def cont_read_thread(self, args):
        self.stop_flag.clear()
        read_thread = threading.Thread(target=self.cont_read)
        self.log.info("Starting continuous readout thread")
        self.running = 1
        read_thread.start()

    def print_data(self, args):
        print(self.data)

    def clear_data(self, args):
        self.data = np.array([])

    def create_test_data(self, args):
        self.data = np.array([2,0,4,0,6,0,8,0,64,0,66,0,68,0,128,0])
        self.data = self.ctx.do_sus_daq_convert_fifo_content_to_coarse_fine(self.data)

    def write_data_to_file(self, leakage, regConf, seqConf, fileName):
        

        hdf5File = self.ctx.do_sus_measurement_initialize_hdf5File(fileName, regConf,  seqConf, 0, len(self.data)/2.)
        hdf5File.attrs['leakage'] = leakage
        self.ctx.do_sus_measurement_write_allFEs_to_hdf5File(self.data, 0, hdf5File)

    def stop_cont_read(self, signum = 0, frame = 0):
        if((self.stop_flag.is_set()) or (self.running == 0)):
            raise KeyboardInterrupt
        else:
            self.log.info("Continuous readout thread has been stopped")
            self.stop_flag.set()

            

###################################################################################################################            
#### End of Functions to perform continuous measurements with extra thread
###################################################################################################################            


