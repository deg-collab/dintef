import sus.ASICcontrol.registersAndSignals as ras
from sus.ASICcontrol.dintefSubCmd import DintefSubCmd
import re
import numpy as np
import logging as log
import libdeep
import sys

class PllSignal(ras.Signal):                    # Different from EBONE Signals, Pll signals can spread over several registers and registers themselves dont mean as much
    """ Since the PLL register structure is a bit more complicated and different from that of EBONE registers
    the structure of their access classes for read and write is different. The control signals of the PLL
    can span over several registers which is not intended for the EboneSignal and EboneRegister class.
    Since this puts an emphasize on the control signals, the whole functionality of reading and writing
    registers is included in this class.
    Signals know their name, number of bits, addresses of registers their are stored in, position of their 
    LSB in the LSB register, position of the MSB in the MSB register as well as their content.
    """
    def __init__(self, ctx, name, LSBPosition, nbBits, registerAddresses):
        self.log = log.getLogger("sus.pllSignal")
        self.registerWidth = 8          #pll registers have 8 bit width
        self.name = "Error"
        self.LSBPosition_in_LSB_register = -1
        self.nbBits = -1
        self.registerAddresses = [-1]
        self.content = -1
        self.device = ctx.device
      


        # Need to do a few input validity checks:
        #       - Register addresses have to be given in a list (even if only one address)
        #       - Signals that span over several registers are always organised in consecutive registers. => Need to test is list of given register addresses is consecutive
        #       - Are given register addresses in span of allowed addresses (0 to 143)?
        #       - Does the amount of given bits fit into the list of registers?
        # If checks fail or other error occurs -> Create Error signal


        registerAddresses_are_consecutive = 1
        registerAddresses_are_in_list = 0
        registerAddresses_are_valid = 1

        if(isinstance(registerAddresses,list)):
            registerAddresses_are_in_list = 1

        for i in np.arange(len(registerAddresses)):
            if( (registerAddresses[i] < 0) or (registerAddresses[i] > 143) ):
                registerAddresses_are_valid = 0

       
        too_many_bits = 0
        if(registerAddresses_are_in_list == 1):
            if(nbBits > len(registerAddresses)*self.registerWidth):
                too_many_bits = 1
            if(len(registerAddresses) > 1):
                for i in np.arange(len(registerAddresses)-1):
                    if(registerAddresses[i+1] != registerAddresses[i] + 1):
                        registerAddresses_are_consecutive = 0
        
        if((not registerAddresses_are_in_list) or ( not registerAddresses_are_consecutive ) or (not registerAddresses_are_valid)):
            self.log.error("RegisterAddresses need to be provided in a consecutive list. Valid addresses are from 0 to 143")
        else:
            if(too_many_bits):
                self.log.error("Too many bits ({}) for provided registers (can hold {}*{}={})".format(nbBits, len(registerAddresses),self.registerWidth,len(registerAddresses)*self.registerWidth))
            else:
                try:
                    self.name = name
                    self.LSBPosition_in_LSB_register = LSBPosition
                    self.nbBits = nbBits
                    self.registerAddresses = registerAddresses                 
                    self.MSBPosition_in_MSB_register = self.calc_MSBPosition_in_MSB_register()
                    self.content = self.read_content()
                except KeyboardInterrupt:
                    sys.exit()
                except:
                    self.log.error("Signal {} could not be created".format(name))

    def calc_MSBPosition_in_MSB_register(self):         # calculate position of the MSB in the MSB register
        MSBPosition_in_MSB_register = 0
        MSB = self.LSBPosition_in_LSB_register + self.nbBits - 1
        MSB_register = self.registerAddresses[-1] - int(MSB/self.registerWidth)
            
        if(MSB_register != self.registerAddresses[0]):
            self.log.error("Provided amount of register addresses dont match provided number of bits (take LSB in LSB register into account)")
            raise Exception
        else:
            MSBPosition_in_MSB_register = MSB % self.registerWidth
        return MSBPosition_in_MSB_register


    def get_registerAddresses(self):
        return self.registerAddresses
#    def set_registerAddresses(self, addresses):
#        self.registerAddresses = addresses
    def get_MSBPosition_in_MSB_register(self):
        return self.MSBPosition_in_MSB_register

    def read_reg_content(self, regAddress):     # read content of single register
        """Read pll control register via I2C.Usage: .pll_read_reg <REG>"""
        dev = self.device
        dev.command('?I2C A104W1 ' + str(regAddress))

        return dev.command('?I2C A104R')        # output is a string with more info than just the content itself

    def read_content(self):                     # read content of signal by reading all the registers in its registerAddress list
        self.content = 0
        for i in np.arange(len(self.registerAddresses)):
            read_string = self.read_reg_content(self.registerAddresses[i])
            val_int = self.get_i2c_output_payload(read_string)      # convert output string of I2C read function to integer that only contains actual content of register
            val_bin_str = "{:08b}".format(val_int)[::-1]            # convert integer to string and revert. This helps picking only the bits of interest from the read register value
            LSB = self.LSBPosition_in_LSB_register                  
            MSB = self.MSBPosition_in_MSB_register 
 
            content_of_single_register_str = ""
            if(len(self.registerAddresses) == 1):                   # If the signal is only in a single register
                content_of_single_register_str = val_bin_str[LSB:LSB+self.get_nbBits()]  
            else:                                                   # If signal is spread over several registers
                if(i == 0):
                    content_of_single_register_str = val_bin_str[0:MSB+1] 
            
                elif(i == len(self.registerAddresses)-1):
                    content_of_single_register_str = val_bin_str[LSB:]
                else:
                    content_of_single_register_str = val_bin_str
           
            content_of_single_register = int(content_of_single_register_str[::-1], 2)
            self.content = self.content + (content_of_single_register << self.registerWidth*(len(self.registerAddresses)-1-i))

    def get_i2c_output_payload(self, i2c_string):
        hex_in_str = re.findall("0x\S\S", i2c_string)
        val_hex_str = hex_in_str[-1]
        val_int = int(val_hex_str, 0)
        return val_int

    def get_content(self):
        self.read_content()
        return self.content

    def print_content(self):
        cont = self.get_content()
        name = self.get_name()
        string = "{} > 0b{:0" + str(self.get_nbBits()) + "b} = 0x{:0" + str(self.get_nbBits()/4) + "X} = 0d{}"
        self.log.info(string.format(name, cont, cont, cont) )


    def write_reg_content(self, regAddress, val, LSB, MSB):
        dev = self.device
        nbBits = MSB-LSB+1
        reg_content_str = self.read_reg_content(regAddress)
        reg_content_int = self.get_i2c_output_payload(reg_content_str)
        for i in np.arange(nbBits):                     # This is where masking of the other signals takes place. Idea: Delete values at bits of interest (bit by bit) and overwrite with new payload. 
            mask = 0                   
            mask = 1 << (LSB+i)                         # First generate a mask for every bit of the signal of interest (1 at every bitPosition+i). 
            reg_content_int = reg_content_int & ~mask         # Delete value at bit of interest

        reg_content_int += val << LSB
        #print(reg_content_int)

        dev.command('?I2C A104W2 ' + str(regAddress) + ' ' + str(reg_content_int))


    def write_content(self, val):
        absolute_LSB = self.LSBPosition_in_LSB_register
        MSB_in_MSB_reg = self.MSBPosition_in_MSB_register
        nbBits = self.get_nbBits()
        string = "{:0" + str(nbBits) + "b}"
        val_bin_str = string.format(val)[::-1]
        if(len(val_bin_str) > nbBits):
            self.log.error("Provided value {} is too big for PLL signal {}. Has to be between in range {}-{}. Signal has not been written".format(val, self.name, 0, 2**(nbBits)-1))
        else:
            for i in np.arange(len(self.registerAddresses)):
                regAdd = self.registerAddresses[i]
                if(len(self.registerAddresses) == 1):
                    val_to_write = val 
                    self.write_reg_content(regAdd, val_to_write, absolute_LSB, MSB_in_MSB_reg)
                else:
                    if(i == 0):
                        val_to_write_LSB = (nbBits-1) - (MSB_in_MSB_reg)
                        val_to_write = int(val_bin_str[val_to_write_LSB:][::-1], 2) 
                        self.write_reg_content(regAdd, val_to_write, 0, self.get_MSBPosition_in_MSB_register())
                    elif(i == len(self.registerAddresses)-1):
                        val_to_write_MSB = self.registerWidth-absolute_LSB
                        val_to_write = int(val_bin_str[0:val_to_write_MSB][::-1],2)
                        self.write_reg_content(regAdd, val_to_write, absolute_LSB, self.registerWidth-1)
                    else:
                        val_to_write_LSB = (i-1)*self.registerWidth+(self.registerWidth - absolute_LSB)
                        val_to_write_MSB = (i)*self.registerWidth+(self.registerWidth - absolute_LSB)
                        
                        val_to_write = int(val_bin_str[val_to_write_LSB:val_to_write_MSB][::-1],2)
                        self.write_reg_content(regAdd, val_to_write, 0, self.registerWidth-1) 
        







class pll(DintefSubCmd):                        #SI5324
    prefix = "pll"

    def __init__(self, ctx):
        self.log = log.getLogger("sus.pll")
        super(pll, self).__init__(ctx, self.log)
        
        
        # Signals are organised in: PllSignal(DintefCmd, Name, LSB in LSB register, # bits, list of registers)
        self.dividers = [
                PllSignal(ctx,  "N1_HS",        5,     3,       [25]),
                PllSignal(ctx,  "NC1_LS",       0,     20,      [31,32,33]),
                PllSignal(ctx,  "NC2_LS",       0,     20,      [34, 35, 36]),
                PllSignal(ctx,  "N2_HS",        5,     3,       [40]),
                PllSignal(ctx,  "N2_LS",        0,     20,      [40, 41, 42]),
                PllSignal(ctx,  "N31",          0,     19,      [43, 44, 45]),
                PllSignal(ctx,  "N32",          0,     20,      [46, 47, 48])
                ]

        self.controls = [
                PllSignal(ctx, "BYPASS_REG",            1,      1,      [0]),
                PllSignal(ctx, "CKOUT_ALWAYS_ON",       5,      1,      [0]),
                PllSignal(ctx, "FREE_RUN",              6,      1,      [0]),
                
                PllSignal(ctx, "CK_PRIOR1",             0,      2,      [1]),
                PllSignal(ctx, "CK_PRIOR2",             2,      2,      [1]),

                PllSignal(ctx, "BWSEL_REG",             4,      4,      [2]),
                
                PllSignal(ctx, "SFOUT1_REG",            0,      3,      [6]),
                PllSignal(ctx, "SFOUT2_REG",            3,      3,      [6]),

                PllSignal(ctx, "CKSEL_PIN",             0,      1,      [21]),
                
                PllSignal(ctx, "LOL_INT",               0,      1,      [130]),
                
                PllSignal(ctx, "ICAL",                  6,      1,      [136]),
                ]

        
        
    ########################## SIGNAL READ & PRINT FUNCTIONS        
        
            
    def print_divider_signals(self, *args):         # PLL divider registers are complicated to read and write, due to their registers not actually representing their true value
                                                    # Due to this, one has to translate between the register value and the actual value
        for i in np.arange(len(self.dividers)):
            div = self.dividers[i]
            cont = div.get_content()
            name = div.get_name()
            actual_div_val = 0
            if((name == "NC1_LS") or (name == "NC2_LS")):
                string = "{} > 0b{:0" + str(div.get_nbBits()) + "b} = 0x{:0" + str(div.get_nbBits()/4) + "X} = 0d{} (=> divider value is {}, see Si5324 datasheet)"
                actual_div_val = cont+1
            elif((name == "N1_HS") or (name == "N2_HS")):
                string = "{} > 0b{:0" + str(div.get_nbBits()) + "b} = 0x{:0" + str(div.get_nbBits()/4) + "X} = 0d{} (=> divider value is {}, see Si5324 datasheet)"
                actual_div_val = cont+4
            elif(name == "N2_LS"):
                string = "{} > 0b{:0" + str(div.get_nbBits()) + "b} = 0x{:0" + str(div.get_nbBits()/4) + "X} = 0d{} (=> divider value is {}, see Si5324 datasheet)"
                actual_div_val = cont
            elif((name == "N31") or (name == "N32")):
                string = "{} > 0b{:0" + str(div.get_nbBits()) + "b} = 0x{:0" + str(div.get_nbBits()/4) + "X} = 0d{} (=> divider value is {}, see Si5324 datasheet)"
                actual_div_val = cont+1
            self.log.info(string.format(name, cont, cont, cont, actual_div_val) )

   
    def print_control_signals(self, *args):
        for i in np.arange(len(self.controls)):
            self.controls[i].print_content()



    def read_single_signal_content(self, signalName, *args):
        for i in np.arange(len(self.dividers)):
            div = self.dividers[i]
            if(div.get_name() == signalName):
                return div.get_content()
        
        for i in np.arange(len(self.controls)):
            contr = self.controls[i]
            if(contr.get_name() == signalName):
                return contr.get_content()
        self.log.error("Signal {} not found".format(signalName))
        return -1

    def print_single_signal_content(self, signalName, *args):
        sig = 0
        for i in np.arange(len(self.dividers)):
            if(self.dividers[i].get_name() == signalName):
                sig = self.dividers[i]
        
        for i in np.arange(len(self.controls)):
            if(self.controls[i].get_name() == signalName):
                sig = self.controls[i]
        if(sig == 0):
            self.log.error("Signal {} not found".format(signalName))
        else:
            cont = self.read_single_signal_content(signalName) 
            string = "{} > 0b{:0" + str(sig.get_nbBits()) + "b} = 0x{:0" + str(sig.get_nbBits()/4) + "X} = 0d{}. For dividers, this does not represent the actual divider value. Please use .pll_print_divider_signals." 
            self.log.info(string.format(sig.get_name(), cont, cont, cont) )

    def print_signals(self, *args):
        self.log.info("Control Signals:")
        self.print_control_signals()

        self.log.info("")
        self.log.info("=================================================================================================")
        self.log.info("")
        self.log.info("Dividers:")
        self.print_divider_signals()

    ########################## SIGNAL WRITE FUNCTIONS        

    def write_signal_content(self, *args):
        argins = args[0].split()
        try:
            signalName = str(argins[0])
            val = int(argins[1])
            
            divs = self.dividers
            contrs = self.controls
            signalName_is_valid = 0
            for i in np.arange(len(contrs)):
                if(contrs[i].get_name() == signalName):
                    signalName_is_valid = 1
                    contrs[i].write_content(val)
            for i in np.arange(len(divs)):                                  # Again: Dividers are complicated to read/write due to their weird relation between register and actual values
                if(divs[i].get_name() == signalName):                       # Because of this, one has to check for every divider individually and apply a different rule when writing their 
                    signalName_is_valid = 1                                 # registers. IMPORTANT: THE INPUT OF THIS FUNCTION IS THE ACTUAL, PHYSICAL DIVIDER VALUE, NOT THE ONE STORED IN 
                                                                            # THE REGISTERS! Example: Writing a 4 to N1_HS with this function will set its physical division value to 4
                                                                            # but the value written into the register is 0! For more info: See PLL Si5324 documentation

                    if((signalName == "N1_HS") or (signalName == "N2_HS")): 
                        if(val >= 4 and val <= 11):
                            val = val - 4
                            divs[i].write_content(val)
                        else:
                            self.log.error("Provided value {} is too big for PLL signal {}. Has to be between in range {}-{}. Signal has not been written".format(val, signalName, 4, 11))

                    elif((signalName == "NC1_LS") or (signalName == "NC2_LS")): 
                        if((val % 2) == 0 or (val == 1)):
                            val = val - 1
                            divs[i].write_content(val)
                        else:
                            self.log.error("{} value must be 1 or even".format(signalName))


                    elif(signalName == "N2_LS"  ): 
                        if((val % 2) == 0):
                            divs[i].write_content(val)
                        else:
                            self.log.error("{} value must be even".format(signalName))

                    elif((signalName == "N31") or (signalName == "N32")): 
                        val = val -1
                        divs[i].write_content(val)

            if(not signalName_is_valid):
                self.log.error("Provided signal {} has not been found".format(signalName)) 
        except:
            self.log.error("Wrong input. Need to provide two inputs: (str)<signalName> (int)<value>")






    ############################################################################################ PLL CONTROL FUNCTIONS
    # The following functions are used for the actual pll control
    #
    #




    def init(self, *args):
#        dev = self.device
        # turn on pll output
#        dev.command('?I2C A104W2 0 0x34')
        self.write_signal_content('CKOUT_ALWAYS_ON 1')
        # Turn on input clock selection via CS_CA pin on PLL (CS_CA=VDD -> CKIN2 is selected by default)
        self.write_signal_content('CKSEL_PIN 1')

        # set pll output to lvds
#        dev.command('?I2C A104W2 6 0x3f')
        self.write_signal_content('SFOUT1_REG 7')
        self.write_signal_content('SFOUT2_REG 7')



    def bypass(self, *args):  # input clk is bypassed to output
#        dev = self.device
#        dev.command('?I2C A104W2 0 0x36')
        self.write_signal_content('BYPASS_REG 1')

    def bypass_off(self, *args):  # input clk is bypassed to output
#        dev = self.device
#        dev.command('?I2C A104W2 0 0x36')
        self.write_signal_content('BYPASS_REG 0')


    def freerun(self, *args):  # connect crystal reference to clkin2
#        dev = self.device
        #dev.command('?I2C A104W2 0 0x74')
        self.write_signal_content('FREE_RUN 1')

    def freerun_off(self, *args):  # connect crystal reference to clkin2
#        dev = self.device
        #dev.command('?I2C A104W2 0 0x74')
        self.write_signal_content('FREE_RUN 0')


    def freerun_bypass(self, *args):
        """connect crystal reference to clkin2 and bypass it to output"""
#        dev = self.device
#        dev.command('?I2C A104W2 0 0x76')
        self.write_signal_content('FREE_RUN 1')
        self.write_signal_content('BYPASS_REG 1')

    def ical(self, *args):
        """calibrate pll (necessary after every multiplier configuration)"""
#        dev = self.device
#        dev.command('?I2C A104W2 136 0x40')
        self.write_signal_content('ICAL 1')


    def set_bw(self, bw):
        self.write_signal_content('BWSEL_REG {}'.format(bw))

    def freerun_multiply2(self, *args):
        """with 125 MHz input crystal -> 250MHz output"""
        self.freerun()
        self.write_signal_content("N1_HS 4")
        self.write_signal_content("NC1_LS 16")
        self.write_signal_content("NC2_LS 16")
        self.write_signal_content("N2_HS 8")
        self.write_signal_content("N2_LS 302")
        self.write_signal_content("N31 19")
        self.write_signal_content("N32 19")
        self.ical()

    def in125_out1(self, *args):
        """with 37 MHz crystal & 125MHz input from FPGA -> 1MHz output"""
        # dev = self.device
        self.init()

        self.write_signal_content("N1_HS 5")
        self.write_signal_content("NC1_LS 1100")
        self.write_signal_content("NC2_LS 1100")
        self.write_signal_content("N2_HS 11")
        self.write_signal_content("N2_LS 252")
        self.write_signal_content("N31 63")
        self.write_signal_content("N32 63")

        self.write_signal_content("BWSEL_REG 6")

        self.ical()

        self.log.info('Set PLL to 1MHz output frequency \
                      (expecting a 125 MHz input clock)')


    def in125_out250(self, *args):
        """with 37 MHz crystal & 125MHz input from FPGA -> 250MHz output"""
        # dev = self.device
        self.init()

        self.write_signal_content("N1_HS 5")
        self.write_signal_content("NC1_LS 4")
        self.write_signal_content("NC2_LS 4")
        self.write_signal_content("N2_HS 7")
        self.write_signal_content("N2_LS 360")
        self.write_signal_content("N31 63")
        self.write_signal_content("N32 63")

        self.write_signal_content("BWSEL_REG 6")

        self.ical()

        self.log.info('Set PLL to 250MHz output frequency \
                      (expecting a 125 MHz input clock)')

        
 
    def in125_out500(self, *args):
        """with 37 MHz crystal & 125MHz input from FPGA -> 500MHz output"""
        # dev = self.device
        self.init()

        self.write_signal_content("N1_HS 11")
        self.write_signal_content("NC1_LS 1")
#        self.set_divider('N1_HS', 5)
#        self.set_divider('NC1_LS', 4)
#        self.set_divider('N2_HS', 7)
        self.write_signal_content("NC2_LS 1")
        self.write_signal_content("N2_HS 11")
        self.write_signal_content("N2_LS 252")
        self.write_signal_content("N31 63")
        self.write_signal_content("N32 63")

#        self.set_divider('NC2_LS', 4)
#        self.set_divider('N2_LS', 360)
#        self.set_divider('N31', 63)
#        self.set_divider('N32', 63)
#        self.set_bw(6)
        self.write_signal_content("BWSEL_REG 6")

        self.ical()

        self.log.info('Set PLL to 500MHz output frequency \
                      (expecting a 125 MHz input clock)')
     

    def in125_out800(self, *args):
        """with 37 MHz crystal & 125MHz input from FPGA -> 800MHz output"""
        # dev = self.device
        self.init()

        self.write_signal_content("N1_HS 7")
        self.write_signal_content("NC1_LS 1")
#        self.set_divider('N1_HS', 5)
#        self.set_divider('NC1_LS', 4)
#        self.set_divider('N2_HS', 7)
        self.write_signal_content("NC2_LS 1")
        self.write_signal_content("N2_HS 4")
        self.write_signal_content("N2_LS 728")
        self.write_signal_content("N31 65")
        self.write_signal_content("N32 65")

#        self.set_divider('NC2_LS', 4)
#        self.set_divider('N2_LS', 360)
#        self.set_divider('N31', 63)
#        self.set_divider('N32', 63)
#        self.set_bw(6)
        self.write_signal_content("BWSEL_REG 6")

        self.ical()

        self.log.info('Set PLL to 800MHz output frequency \
                      (expecting a 125 MHz input clock)')
       

    def in125_out1000(self, *args):
        """with 37 MHz crystal & 125MHz input from FPGA -> 1GHz output"""
        # dev = self.device
        self.init()

        self.write_signal_content("N1_HS 5")
        self.write_signal_content("NC1_LS 1")
#        self.set_divider('N1_HS', 5)
#        self.set_divider('NC1_LS', 4)
#        self.set_divider('N2_HS', 7)
        self.write_signal_content("NC2_LS 1")
        self.write_signal_content("N2_HS 6")
        self.write_signal_content("N2_LS 420")
        self.write_signal_content("N31 63")
        self.write_signal_content("N32 63")

#        self.set_divider('NC2_LS', 4)
#        self.set_divider('N2_LS', 360)
#        self.set_divider('N31', 63)
#        self.set_divider('N32', 63)
#        self.set_bw(6)
        self.write_signal_content("BWSEL_REG 6")

        self.ical()

        self.log.info('Set PLL to 1GHz output frequency \
                      (expecting a 125 MHz input clock)')
        
    def reset(self, *args):
        """reset pll control registers to default"""
        dev = self.device
        try:
            dev.command('?I2C A104W2 136 0x80')

        except libdeep.device.DeepCmdError as ex:
            if (ex.args == ('Error while sending I2C transaction', )):
                self.log.error('A reset command has been sent to the PLL.')
                self.log.error('Exception "%s"', ex)
                self.log.error("has been raised by the I2C master,"
                               " usually caused by the PLL not sending an ACK bit"
                               " when starting its reset.")
                self.log.error("In most cases, this error can be ignored.")
            else:
                raise ex

    def read_reg(self, *args):
        """Read pll control register via I2C.Usage: .pll_read_reg <REG>"""
        dev = self.device
        try:
            reg = args[0]
            dev.command('?I2C A104W1 ' + str(reg))
            # self.log.debug("reading reg 0x{:02X}".format(int(reg)))
            self.log.info(dev.command('?I2C A104R'))
        except Exception:
            self.log.error(
                'Ambiguous input. Expected usage: .pll_read_reg <REG#>')



