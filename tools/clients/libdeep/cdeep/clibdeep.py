# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function, division

import os
import atexit
from ctypes import (cdll, c_void_p, c_char_p, c_char, c_int, c_size_t, c_float,
                    pointer, POINTER, cast, addressof, byref,
                    Structure, Union)

from ..device import _deep_error_list, log_print
from ..deeparray import DeepArray

_LIBDEEP_LIB = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'libdeep.so')

libdeeplib = cdll.LoadLibrary(_LIBDEEP_LIB)


"""
 ctypes c_char_p ASCIIZ strings are returned either
 as None (NULL), str (Python2) or bytes (Python3)
 This function allows to treat all and return only None or str
"""
def to_string(val):
    return val if val is None or type(val) is str else val.decode()

"""
def _nparray_to_pointer(array, type):
    address = array.__array_interface__['data'][0]
    return pointer(type.from_address(address))


def _extract_string_list(name):
    ptr = pointer(c_char_p.in_dll(libdeeplib, name))
    n_strings = c_int.in_dll(libdeeplib, name + "_n").value
    strlist = []
    for i in range(n_strings):
        strlist.append(ptr[i].decode())
    return strlist
"""

def _extract_errorcode_list():
    n_errcodes = c_int.in_dll(libdeeplib, "deepdev_errcodes_n").value
    name_ptr = pointer(c_char_p.in_dll(libdeeplib, "deepdev_errcodes"))
    msg_ptr = pointer(c_char_p.in_dll(libdeeplib, "deepdev_errmsgs"))
    errdict = dict()
    for i in range(n_errcodes):
        name = to_string(name_ptr[i])  # for Python3 compatibility
        for ex in _deep_error_list:
            if ex._str_id == name:
                ex._error_descr = to_string(msg_ptr[i])
                errdict[i] = ex
                break
        else:
            errdict[i] = None
    return errdict


def _struct_union_repr(obj, prefix):
    out = "struct " if isinstance(obj, Structure) else "union "
    out += repr(obj.__class__) + " at " + hex(addressof(obj))
    prefix += "    "
    for field in obj._fields_:
        out += prefix + field[0]
        element = getattr(obj, field[0])
        if isinstance(element, (Structure, Union)):
            out += " " + _struct_union_repr(element, prefix)
        else:
            try:
                element._type_
                address = c_void_p.from_buffer(element).value
                out += ": " + (hex(address) if address else "NULL")
                out += " pointer to " + repr(element._type_)
            except AttributeError:
                out += ": " + element.__repr__()
    return out


class _Structure(Structure):
    def __repr__(self):
        return _struct_union_repr(self, '\n')


class _Union(Union):
    def __repr__(self):
        return _struct_union_repr(self, '\n')



class _DeepError():
    OK = 0
    NOTREADY = 1
    deep_errors = _extract_errorcode_list()


deepdev_error = libdeeplib.deepdev_error
deepdev_error.argtypes = [POINTER(c_char_p)]
deepdev_error.restype = c_int

def _raise_exception():
    errmsg = c_char_p()
    errcode = deepdev_error(byref(errmsg))
    ex = _DeepError.deep_errors[errcode]
    raise ex(to_string(errmsg.value))


def _int_result_check(errcode):
    if errcode == _DeepError.OK :
        return True
    elif errcode == _DeepError.NOTREADY :
        return False
    else:
        _raise_exception()


def _deeppointer_result_check(deeppointer):
    if not deeppointer:
        _raise_exception()
    else:
        return deeppointer


def pointer_result_check(c_pointer, type=None):
    if not c_pointer:
        _raise_exception()
    elif type:
        return cast(c_pointer, POINTER(type)).contents
    else:
        return c_pointer



#   This does not work for unknown reason (bug in ctypes?):
#class deephandle_t(POINTER(Structure)): pass
#
#   This does not work for lack of storage size (empty structure):
#class devhandle_t(Structure): pass
#deephandle_t = POINTER(devhandle_t)
#
# use the following instead:
deephandle_t = c_void_p
deepcmd_t = c_void_p
deepdpars_t = c_void_p
deepobject_t = c_void_p
deepfd_t = c_void_p
deepevsrc_t = c_void_p
_deepobject_t = c_void_p
_deeppar_t = c_void_p


class deepbindata_t(_Structure):
    _fields_ = [("databuf",  c_void_p),
                ("bufsize",  c_size_t),
                ("datatype", c_int),
                ("datasize", c_size_t)]

    _dtypes = {1:"uint8", 2:"uint16", 4:"uint32", 8:"uint64"}

    @classmethod
    def from_deeparray(cls, darray):
        bindata = deepbindata_t()
        bindata.load_deeparray(darray)
        return bindata

    def load_deeparray(self, darray):
        self.databuf = addressof(c_char.from_buffer(darray))
        self.bufsize = darray.size * darray.itemsize
        self.datatype = darray.itemsize
        self.datasize = darray.size
        return self

    def cleared(self):
        self.databuf = 0
        self.bufsize = 0
        self.datasize = 0
        return self

    def to_deeparray(self):
        data = (c_char * (self.datasize * self.datatype)).from_address(self.databuf)
        return DeepArray.frombytes(data, dtype=self._dtypes[self.datatype])


libdeeplib.deepdev_partblinit.argtypes = [deepdpars_t]
libdeeplib.deepdev_partblinit.restype = deepdpars_t
def _partblinit(*args): return _deeppointer_result_check(libdeeplib.deepdev_partblinit(*args))


class DevPars(object):
    DEFAULT_DEVPARAMS = deepobject_t(-1)
    CURRENT_DEVPARAMS = deepobject_t(-2)

    def __init__(self, default=None, **kwargs):
        self.pars = {}
        if not default:
            defptbl = DevPars.CURRENT_DEVPARAMS
        else:
            defptbl = default.partable
        self.partable = _partblinit(defptbl)
        for par in kwargs:
            self.setparam(par, kwargs[par])

    def __del__(self):
        deepdev_free(self.partable)

    def setparam(self, param_name, param_value):
        deepdev_setparam(param_name, param_value, obj=self.partable)
        self.pars[param_name] = param_value


libdeeplib.deepdev_cmdinit.argtypes = []
libdeeplib.deepdev_cmdinit.restype = deepcmd_t
def deepdev_cmdinit(*args): return pointer_result_check(libdeeplib.deepdev_cmdinit(*args))

libdeeplib.deepdev_free.argtypes = [_deepobject_t]
libdeeplib.deepdev_free.restype = None
deepdev_free = libdeeplib.deepdev_free

class EventSource(object):
    cpointer = None
    def __del__(self):
        log_print("Destroying", self.__class__)
        deepdev_free(self.cpointer)

class CmdInfo(EventSource):
    def __init__(self, **kwargs):
        self.args = kwargs
        self.cpointer = deepdev_cmdinit()
        self.answ_p = c_char_p()
        self.def_bindata_p = deepbindata_t()


libdeeplib.deepdev_fdinit.argtypes = [POINTER(c_int)]
libdeeplib.deepdev_fdinit.restype = deepfd_t
def deepdev_fdinit():
    fd = c_int()
    fdinfo = pointer_result_check(libdeeplib.deepdev_fdinit(byref(fd)))
    return fdinfo, fd.value


libdeeplib.deepdev_addevsource.argtypes = [deepfd_t, deepevsrc_t, c_int]
libdeeplib.deepdev_addevsource.restype = _int_result_check
def deepdev_addevsource(eventfd, event_source, flags):
    libdeeplib.deepdev_addevsource(eventfd, event_source, flags)


libdeeplib.deepdev_evsource.argtypes = [deepfd_t]
libdeeplib.deepdev_evsource.restype = deepevsrc_t
def deepdev_evsource(fdinfo): return _deeppointer_result_check(libdeeplib.deepdev_evsource(fdinfo))


libdeeplib.deepdev_evinfo.argtypes = [deepevsrc_t, POINTER(deephandle_t),
                                      POINTER(deepcmd_t), POINTER(c_int)]
libdeeplib.deepdev_evinfo.restype = _int_result_check
def deepdev_evinfo(evsource):
    dev = deephandle_t
    cmdinfo = deepcmd_t
    flags = c_int
    libdeeplib.deepdev_evinfo(evsource, byref(dev), byref(cmdinfo), byref(flags))
    return dev, cmdinfo, flags


libdeeplib.deepdev_devinit.argtypes = [c_char_p,_deeppar_t]
libdeeplib.deepdev_devinit.restype = deephandle_t
def deepdev_devinit(dev_id, dpartbl):
    dev_id = dev_id.encode()
    return _deeppointer_result_check(libdeeplib.deepdev_devinit(dev_id, dpartbl))


libdeeplib.deepdev_open.argtypes = [c_char_p]
libdeeplib.deepdev_open.restype = deephandle_t
def deepdev_open(dev_id):
    dev_id = dev_id.encode()
    return _deeppointer_result_check(libdeeplib.deepdev_open(dev_id))


libdeeplib.deepdev_close.argtypes = [deephandle_t]
libdeeplib.deepdev_close.restype = _int_result_check
deepdev_close = libdeeplib.deepdev_close


libdeeplib.deepdev_closeall.argtypes = []
libdeeplib.deepdev_closeall.restype = _int_result_check
deepdev_closeall = libdeeplib.deepdev_closeall


libdeeplib.deepdev_connect.argtypes = [deephandle_t]
libdeeplib.deepdev_connect.restype = _int_result_check
deepdev_connect = libdeeplib.deepdev_connect


libdeeplib.deepdev_disconnect.argtypes = [deephandle_t]
libdeeplib.deepdev_disconnect.restype = _int_result_check
deepdev_disconnect = libdeeplib.deepdev_disconnect



libdeeplib.deepdev_setparam.argtypes = [_deepobject_t, c_char_p, _deeppar_t]
libdeeplib.deepdev_setparam.restype = _int_result_check
def deepdev_setparam(param_name, param, obj=None):
    if obj is None: obj = DevPars.CURRENT_DEVPARAMS
    if param_name.upper() == "ERROROUTPUT":
        raise ValueError("Cannot set parameter " + param_name)
    try:
        param = byref(c_int(int(param)))
    except ValueError:
        param = c_char_p(param.encode())
    param_name = param_name.encode()
    libdeeplib.deepdev_setparam(obj, param_name, param)


libdeeplib.deepdev_getparam.argtypes = [_deepobject_t, c_char_p, POINTER(_deeppar_t)]
libdeeplib.deepdev_getparam.restype = _int_result_check
def deepdev_getparam(param_name, obj=None):
    if obj is None: obj = DevPars.CURRENT_DEVPARAMS
    param_name = param_name.upper()
    if param_name == "ERROROUTPUT":
        raise ValueError('Cannot access parameter "{}"'.format(param_name))
    param_ptr = _deeppar_t()
    libdeeplib.deepdev_getparam(obj, param_name.encode(), byref(param_ptr))
    if param_name in ("HOSTNAME", "DEVNAME", "DEBUGTAGS"):
        param = to_string(cast(param_ptr, c_char_p).value)
    else:
        param = cast(param_ptr, POINTER(c_int)).contents.value
    return param


libdeeplib.deepdev_parsecommand.argtypes = [POINTER(c_int), #  int   flags
                                            c_char_p,       #  char *addr
                                            c_char_p,       #  char *cmd
                                            c_int]          #  int   ack
libdeeplib.deepdev_parsecommand.restype = _int_result_check
def deepdev_parsecommand(cmd, addr=None, ack=False):
    flags = c_int()
    if addr: addr = addr.encode()
    cmd = cmd.encode()
    libdeeplib.deepdev_parsecommand(byref(flags), addr, cmd, ack)
    return int(flags)


libdeeplib.deepdev_command.argtypes = [deephandle_t,       #   deephandle_t dev
                                       c_char_p,           #   char        *addr
                                       c_char_p,           #   char        *cmd
                                       c_int,              #   int          ack
                                       POINTER(c_char_p),  #   char       **answ
                                       POINTER(deepbindata_t)] # deepbindata_t *bdat
libdeeplib.deepdev_command.restype = _int_result_check
def deepdev_command(dev, cmd, bdat=None, addr=None, ack=False):
    answ_p = c_char_p()
    bindata_p = deepbindata_t.from_deeparray(bdat) if bdat is not None else deepbindata_t(datatype=1)
    if addr: addr = addr.encode()
    cmd = cmd.encode()
    libdeeplib.deepdev_command(dev, addr, cmd, ack, byref(answ_p), byref(bindata_p))

    # there is no command taking and returning binary, therefore if
    # some bin data as been passed, no bin has to be returned
    if bdat is None:
        bdat = bindata_p.to_deeparray() if bindata_p.databuf else None
        return answ_p.value, bdat
    else:
        return answ_p.value, None


libdeeplib.deepdev_startcommand.argtypes = [deepcmd_t,     #   deepcmd_t    cmdinfo
                                            deephandle_t,  #   deephandle_t dev
                                            c_char_p,      #   char        *addr
                                            c_char_p,      #   char        *cmd
                                            c_int,         #   int          ack
                                            POINTER(c_char_p),      # char         **answ
                                            POINTER(deepbindata_t)] # deepbindata_t *bdat
libdeeplib.deepdev_startcommand.restype = _int_result_check
def deepdev_startcommand(dev, cmd, bdat=None, addr=None, ack=False, cmdinfo=None):
    if not cmdinfo: cmdinfo = CmdInfo()
    cmdinfo.done = True
    cmdinfo.answ = None
    cmdinfo.bdat = bdat
    cmdinfo.bindata_p = deepbindata_t.from_deeparray(bdat) if bdat is not None else cmdinfo.def_bindata_p.cleared()
    if addr: addr = addr.encode()
    cmd = cmd.encode()
    cmdinfo.done = libdeeplib.deepdev_startcommand(cmdinfo.cpointer,
                       dev, addr, cmd, ack, byref(cmdinfo.answ_p), byref(cmdinfo.bindata_p))
    return cmdinfo


libdeeplib.deepdev_checkcommand.argtypes = [deepcmd_t, c_int]
libdeeplib.deepdev_checkcommand.restype = _int_result_check
def deepdev_checkcommand(evsrc, wait=False):
    evsrc.done = libdeeplib.deepdev_checkcommand(evsrc.cpointer, wait)
    return evsrc.done
