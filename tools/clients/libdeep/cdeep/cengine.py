# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function

from .clibdeep import (deepdev_devinit, deepdev_connect, deepdev_disconnect,
                       deepdev_open, deepdev_close, deepdev_closeall,
                       deepdev_fdinit, deepdev_free,
                       DevPars, CmdInfo,
                       deepdev_setparam, deepdev_getparam,
                       deepdev_command, deepdev_startcommand, deepdev_checkcommand,
                       deepdev_addevsource, deepdev_evsource,
                       to_string)

from ..device import DeepCommandInfo, DeepEngine, log_print


class EventFD(object):
    def __init__(self):
        self.fdinfo, self.fd = deepdev_fdinit()

    def __del__(self):
        log_print("Destroying", self.__class__)
        deepdev_free(self.fdinfo)

    def fileno(self):
        return(self.fd)

    def add_evsource(self, evsource):
        deepdev_addevsource(self.fdinfo, evsource.cpointer, 0)

    def get_evsource(self):
        return deepdev_evsource(self.fdinfo)


class CDeepCommandInfo(DeepCommandInfo):
    _filedescr = None

    def __init__(self, useselect=False):
        self.cmdinfo = CmdInfo()
        if useselect:
            self._evfd = EventFD()
            self._evfd.add_evsource(self.cmdinfo)
        else:
            self._evfd = None

    def fileno(self):
        if self._evfd:
            return self._evfd.fileno()
        else:
            raise IOError

    def cleanselect(self):
        cpointer = self._evfd.get_evsource()
        if cpointer != self.cmdinfo.cpointer :
            raise IOError


class CDeepEngine(DeepEngine):
    _defpars = None

    def __init__(self, dev_object, dev_id, **kwargs):

        if not CDeepEngine._defpars:
            defpars = DevPars()
            for p in CDeepEngine.default_devparams:
                deepdev_setparam(p, CDeepEngine.default_devparams[p])
            CDeepEngine._defpars = defpars

        params = super(CDeepEngine, self).__init__(dev_object, dev_id, **kwargs)
        pars = DevPars(default=CDeepEngine._defpars)
        for p in params:
            pars.setparam(p, params[p])

        self.dev_handle = deepdev_devinit(dev_id, pars.partable)
        log_print("Created:", self.__class__)
        log_print("Allocated device handle:", hex(self.dev_handle))

    def __del__(self):
        log_print("Destroying", self.__class__, hex(id(self)))
        if hasattr(self, 'dev_handle'):
            log_print("Free device handle:", hex(self.dev_handle))
            deepdev_close(self.dev_handle)
        log_print("Destroyed")

    def devicename(self):
        return deepdev_getparam("devname", obj=self.dev_handle)

    @staticmethod
    def libparameter(param, value=None, device=None):
        obj = device._deepengine.dev_handle if device is not None else None

        if value is not None:
            deepdev_setparam(param, value, obj=obj)
        return deepdev_getparam(param, obj=obj)

    def deviceparam(self, param, value=None):
        return self.libparameter(param, value, device=self)

    def timeoutms(self, device, timeoutms=None):
        return self.libparameter('timeoutms', timeoutms, device=device)

    def reconnect(self, device, reconnect=None):
        return self.libparameter('reconnect', reconnect, device=device)


    @staticmethod
    def dump_internals():
        # passing None (NULL) the function dumps its internal state
        deepdev_free(None)

    def newcommandinfo(self):
        return CDeepCommandInfo(self._select is not None)

    def new_cmd(self, cmdline, bindata, addr, ack, comminfo):
        #log_print('<{}> bindata={} addr={} ack={}'.format(cmdline, bindata, addr, ack))
        cmdinfo = comminfo.cmdinfo
        deepdev_startcommand(self.dev_handle, cmdline, bindata, addr, ack, cmdinfo=cmdinfo)
        if comminfo: comminfo.done = cmdinfo.done

    def check_cmd(self, comminfo, wait=False):
        comminfo.done = deepdev_checkcommand(comminfo.cmdinfo, wait)
        return comminfo.done

    def answer_cmd(self, comminfo):
        if not comminfo.done:
            self.wait_cmd(comminfo)

        answ = to_string(comminfo.cmdinfo.answ_p.value)
        if comminfo.cmdinfo.bdat is None:
            bdat = comminfo.cmdinfo.bindata_p.to_deeparray() if comminfo.cmdinfo.bindata_p.databuf else None
            return answ, bdat
        else:
            return answ, None
