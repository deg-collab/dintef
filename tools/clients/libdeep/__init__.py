# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function

"""Handle communication with any Deep based device"""

from .dconfig import DConfig
from .device import (DeepDevice, IcepapDevice, DanceDevice, LegacyDevice,
                     DeepError, DeepSysError, DeepDeviceError, DeepCommError, DeepCmdError)

from .deeparray import DeepArray
from .deepasync import DataStream, DataReceiver, DeepCondition, DeepEvent
from .deepbuffer import DeepBuffer
from .conditions import *

__url__ = "https://deg-svn.esrf.fr/svn/libdeep/dev/python/libdeep"
__version__ = "0.0.0"
__modname__ = "libdeep"
__author__ = ""
__author_email__ = ""
__description__ = "Library that implements the deep protocol to handle" + \
                  " communication with any DEEP based device (DAnCE, IcePAP, ...)"
