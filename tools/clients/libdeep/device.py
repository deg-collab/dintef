# -*- coding: utf-8 -*-
#
# this file is part of libdeep project
#

"""Handle communication with any DEEP based instruments"""

from __future__  import absolute_import, print_function

import sys
from itertools   import cycle

from .           import deeplog as log
from .deeparray  import DeepArray


def log_print(*args):
    if DeepDevice._libparams['debuglevel'] > 0: print(*args)


class DeepError(Exception):
    _error_descr = None

    def __str__(self):
        return "{} ({})".format(self.args[0], self._error_descr)


class DeepSysError(DeepError):    _str_id = 'SYSERR'
class DeepDeviceError(DeepError): _str_id = 'ERR'
class DeepCommError(DeepError):   _str_id = 'COMMERR'
class DeepCmdError(DeepError):    _str_id = 'ANSERR'

_deep_error_list = (DeepSysError,
                    DeepDeviceError,
                    DeepCommError,
                    DeepCmdError)

def _badkeyword_exception(func, kwd):
    return TypeError('{}() got an unexpected keyword argument \'{}\''.format(func, kwd))

# Device generic commands
COMM_ALIVE_CMD    = "?APPNAME"
COMM_ALIVE_ICECMD = "?_SOCKPING"
DSTREAMS_QCMD     = "?DSTREAM"
DSTREAMS_CMD      = "DSTREAM"
DSTREAMS_ICEQCMD  = "?REPORT"
DSTREAMS_ICECMD   = "REPORT"


def _clibdeep_is_valid():
    return True

def _select_select():
    try:
        return sys.modules['gevent'].select
    except AttributeError:
        # this is only for testing purposes
        import select
        return select.select
    except:  # KeyError
        return None


class _DeepOperMode(str): pass

class DeepCommandInfo(object):
    done = False

    def fileno(self):
        raise IOError

    def cleanselect(self):
        raise NotImplementedError

    def __del__(self): log_print("Destroying", self.__class__)


class DeepEngine(object):
    default_libparams = dict(debuglevel  = 0,
                             debugtags   = "ALL",
                             maxthreads  = 0,
                             maxdevices  = 5)

    default_devparams = dict(timeoutms   = 3000,
                             reconnect   = True,
                             tcpport     = 5000,
                             usechecksum = False)

    def __init__(self, dev_object, dev_id, **kwargs):
        # initialise with empty command list
        self._commands = None
        # decide whether to use select() and which one
        self._select = _select_select()

        params = dict(DEVFAMILY = str(dev_object._devfamily))
        for par in kwargs:
            value = kwargs[par]
            if par in self.__class__.default_libparams:
                self.libparameter(par, value)
                self.__class__.default_libparams[par] = value
            elif par in self.__class__.default_devparams:
                params[par] = value
            else:
                raise _badkeyword_exception(dev_object.__class__.__name__, par)

        return params


    def __del__(self):
        raise NotImplementedError

    def devicename(self):
        raise NotImplementedError

    @staticmethod
    def libparameter(param, value=None, device=None):
        raise NotImplementedError

    def timeoutms(self, device, timeoutms=None):
        raise NotImplementedError

    def reconnect(self, device, reconnect=None):
        raise NotImplementedError

    @staticmethod
    def dump_internals():
        raise NotImplementedError

    def newcommandinfo(self):
        """
        Returns a new DeepCommandInfo instance to be used by a DeepCommandInterface object
        for subsequent issuing of commands
        """
        raise NotImplementedError

    def new_cmd(self, cmdline, bindata, addr, ack, comminfo=None):
        """
        Initialises and sends a command to the instrument.
        Returns the DeepCommandInfo instance that must be used to monitor
        the progress and the result of the command with the 'done' attribute updated.
        The returned instance may be optionally provided by the caller with the
        comminfo keyword.
        """
        raise NotImplementedError

    def wait_cmd(self, comminfo):
        """
        Waits the completion of the command monitored by the DeepCommandInfo instance
        comminfo and returns True.
        """
        if comminfo.done: return True

        if self._select:
            self._select([comminfo], [], [])
            comminfo.cleanselect()
        return self.check_cmd(comminfo, wait=True)

    def check_cmd(self, comminfo, wait=False):
        """
        Checks the completion state of the command monitored by the DeepCommandInfo
        instance comminfo and updates and returns the 'done' attribute.
        """
        raise NotImplementedError

    def answer_cmd(self, comminfo):
        """
        Returns the answer of the command monitored by the DeepCommandInfo instance
        comminfo as either None, an answer string or a tuple (answer, bindata) according
        to the type of query.
        """
        raise NotImplementedError


class DeepInterface(object):
    #_deepengine = None
    _libparams = None

    def __init__(self, deepdevice):
        # only keep a references to the device if the instance is not the device itself
        if not isinstance(self, DeepDevice):
            self._deepdevice = deepdevice
            self._deepengine = deepdevice._deepengine

    def __del__(self): log_print("Destroying", self.__class__)

    def _device(self):
        return self._deepdevice

    def name(self):
        return self._deepengine.devicename()

    @staticmethod
    def libparam(param, value=None, engclass=None, device=None):
        if not DeepDevice._libparams:
            DeepDevice._libparams = DeepEngine.default_libparams.copy()
            DeepDevice._devparams = DeepEngine.default_devparams.copy()

        if param in DeepDevice._libparams:
            partable = DeepDevice._libparams
        elif param in DeepDevice._devparams:
            partable = DeepDevice._devparams
        else:
            raise ValueError("'{}' is not a valid libdeep parameter".format(param))

        if value is not None:
            if engclass:
                value = engclass.libparameter(param, value, device=device)
            partable[param] = value
        elif engclass:
            partable[param] = engclass.libparameter(param, device=device)

        return partable[param]


    def devparam(self, param, value=None):
        engclass = self._deepengine.__class__
        return self.libparam(param, value, engclass=engclass, device=self)


    def debug(self=None, level=None, tags=None, showall=False):
        libparams = DeepEngine.default_libparams

        dbglevel = DeepDevice.libparam('debuglevel') if level is None else int(level)

        if tags is None:
            dbgtags = DeepDevice.libparam('debugtags')
        else:
            if type(tags) is str:    # if tags is a string
                tags = {t for t in tags.split()}
            else:                    # if tags is a container of strings
                tags = {t for s in tags for t in s.split()}
            dbgtags = ' '.join(tags)

        if self:
            device = self._device()
            enginecls = device._deepengine.__class__
        else:
            device = enginecls = None

        dbglevel = DeepDevice.libparam('debuglevel', dbglevel, engclass=enginecls)
        dbgtags = DeepDevice.libparam('debugtags', dbgtags, engclass=enginecls)

        if showall and self:
            self._deepengine.dump_internals()

        return dbglevel, dbgtags


    def timeout(self, timeout=None):
        if timeout is not None:
            self._deepengine.timeoutms(self, int(1000 * timeout))
        return self._deepengine.timeoutms(self) / 1000.


    def reconnect(self, reconnect=None):
        if reconnect is not None:
            self._deepengine.reconnect(self, bool(reconnect))
        return self._deepengine.reconnect(self)


class DeepCommandInterface(DeepInterface):
    def __init__(self, deepdevice):
        super(DeepCommandInterface, self).__init__(deepdevice)
        self._commandinfo = deepdevice._deepengine.newcommandinfo()

    def getcommandlist(self, force=False):
        """
        Returns the list of commands supported by the instrument
        """
        if force or self._deepengine._commands is None:
            answ = self.command(self._helpquery).splitlines()
            if self._devfamily is DeepDevice.ICEPAPMODE:
                self._deepengine._commands = [s for line in answ for s in line.split()]
            else:
                self._deepengine._commands = [s.split(":")[0].strip() for s in answ if s.rfind(":") >= 0]

        return self._deepengine._commands

    def _command_start(self, cmdline, bindata, addr, ack, comminfo=None):
        # send the command to the instrument
        if not comminfo:
            comminfo = self._deepengine.newcommandinfo()
        self._deepengine.new_cmd(cmdline, bindata, addr, ack, comminfo)
        return comminfo

    def _command_answer(self, comminfo, parsefns=None):
        self._deepengine.wait_cmd(comminfo)
        answ, bindata = self._deepengine.answer_cmd(comminfo)
        if answ is not None:
            if type(answ) is not str:
                answ = answ.decode()
            if comminfo.parsefns:
                answ = self._parse_string(comminfo.parsefns, answ)
        if bindata is None:
            return answ
        elif bindata.size == 0:
            return (answ, None)
        else:
            return (answ, bindata)

    def _command_compose(self, comminfo, cmdstr, *args, **kwargs):
        """
        Send a command/query to the instrument
        """
        # extract keyword arguments: bindata, addr, ack, parsefns
        bindata = kwargs.pop("bindata", None)
        ack     = bool(kwargs.pop("ack", False))
        addr    = kwargs.pop("addr", None)
        comminfo.parsefns = kwargs.pop("parsefns", None)
        if kwargs:
            raise _badkeyword_exception('command', kwargs.keys()[0])

        if addr: addr = str(addr)

        # minimum command check
        cmdline = cmdstr.strip()

        # the optional binary argin data must be the last non-keyword argument
        if bindata is None:
            try:
                bdat = args[-1]
                if isinstance(bdat, bytearray) or (bdat.size >= 0 and bdat.itemsize >= 0):
                    bindata = bdat
                    args = args[:-1]
            except:
                pass                    

        # tolerate binary data as bytearray type in addition to DeepArray
        if bindata is not None and isinstance(bindata, bytearray):
            bindata = DeepArray(bindata)

        # join all args in a single string (excluding 'None')
        args_cmd = " ".join([str(a) for a in args if a is not None])
        if args_cmd:
            cmdline += ' ' + args_cmd

        return self._command_start(cmdline, bindata, addr, ack, comminfo)


    def command(self, cmdstr, *args, **kwargs):
        try:
            self._command_compose(self._commandinfo, cmdstr, *args, **kwargs)
            return self._command_answer(self._commandinfo)

        except DeepError as deeperr:
            raise deeperr


    def ackcommand(self, cmdstr, *args, **kwargs):
        """
        Send a command/query to the instrument with acknowledge request
        """
        try:
            return self.command(cmdstr, *args, ack=True, **kwargs)

        except DeepError as deeperr:
            raise deeperr


    def commsequence(self, cmdseq, cmdstr, *args, **kwargs):
        try:
            if not cmdseq: cmdseq = []
            if cmdstr:
                cmdseq.append(self._command_compose(None, cmdstr, *args, **kwargs))
                return cmdseq
            else:
                answers = []
                for comminfo in cmdseq: answers.append(self._command_answer(comminfo))
                return answers

        except DeepError as deeperr:
            raise deeperr


    def get_answer(self, parsefns, devquery, *args, **kwargs):
        try:
            return self.parsedquery(parsefns, devquery, *args, **kwargs)

        except DeepError as deeperr:
            raise deeperr


    def parsedquery(self, parsefns, devquery, *args, **kwargs):
        """
        Returns the ASCII answer from the device to a given query converted
        into a tuple of values, each element of the tuple having been parsed
        accordingly to the argument parsefns. If the query also returns a binary
        block, the method returns in addition the DeepArray containing the
        binary data. In general, parsefns is either a function or a sequence of
        functions that is used to extract from the device ASCII answer the
        elements included in the returned tuple. If needed, the sequence of
        conversion or parsing functions in parsefns is repeated cyclically until
        all the elements in the device answer are extracted. The most frequently
        used are the conversion functions for the built-in types int, float
        and str, but the method accepts also any user provided parsing function
        as well as certain objects that select special conversions as described
        below. In general the built-in conversions parse and extract a string
        token separated by whitespaces in the answer string and converts them
        to the corresponding type. However, for string conversion, the method
        also manages single or double quoted strings in the device answer that
        are treated and returned as full strings regardless of whether or not
        they include whitespaces. The built-in integer conversion is also
        extended to accept C-format hexadecimal strings.

        User provided parsing functions must parse an input string and extract
        a single element at each call. The functions must return a two-value
        tuple consisting of the extracted element, that can be an object of any
        arbitrary type, and the remaining string still to be parsed. If the
        extracted element returned by the user function is None, the element
        is ignored and not included in the final sequence. This feature may be
        used to manipulate the ASCII string at a given point of the parsing
        sequence without generation of output values.

        Optionally the conversion functions may be replaced with predefined
        strings that select special built-in parsing operations. Currenly the
        following special parsing operations are implemented:

        - 'auto' - the method tries and converts automatically the next token
                   into one of the built-in types int, float or str.
        - 'tail' - the method treats and extracts as a single string all the
                   remaining characters pending to parse. This is always the
                   last element returned by the method.
        - 'skip' - the method discards the next token in the string to parse.

        :param  parsefns: the conversion/parsing function or the list or tuple
                         of conversion/parsing functions that is used by the
                         method. The strings 'auto', 'tail' and 'skip' may be
                         used instead of a function to indicate special parsing
                         operations.
        :param  devquery: The query string to be sent to the device followed by
                          any additional required arguments.
        :raises DeepDeviceError: If the query is not properly executed in the
                             device.
        :raises IOError: If there is any communication error.
        :raises ValueError: If devquery is not a query or if the answer from
                            the device is not compatible with the conversion
                            scheme described by parsefns.
        """
        return self.command(devquery, *args, parsefns=parsefns, **kwargs)


    @staticmethod
    def _string_extract(type_fn, str_value):
        if type_fn in (int, float, str):
            if not str_value:
               if type_fn == str:
                   return None, None
               else:
                   msg = "empty string cannot be converted to " + repr(type_fn)
                   raise ValueError(msg)

            elif type_fn == str and str_value[0] in "\"\'":
               idx = str_value[1:].find(str_value[0])
               if idx >= 0:
                   return str_value[1:1+idx], str_value[2+idx:].strip()
               else:
                   return str_value[1:].strip(), None

            else:
               strsplit = str_value.split(None, 1)
               str_token = strsplit[0]
               str_remain = strsplit[1] if len(strsplit) == 2 else None

               if type_fn == int and str_token[0:2].lower() == '0x':
                   # convert C-type hexadecimal values
                   return int(str_token, base=16), str_remain
               else:
                   return type_fn(str_token), str_remain

        elif type_fn:
            # used define parsing function
            return type_fn(str_value)

        else:
            # type_fn == None
            try:
                return DeepCommandInterface._string_extract(int, str_value)
            except ValueError:
                try:
                    return DeepCommandInterface._string_extract(float, str_value)
                except ValueError:
                    return DeepCommandInterface._string_extract(str, str_value)


    @staticmethod
    def _parse_string(parsefns, answ):
        """
        Parses an input string and extracts values by the same method used in
        parse_answer(). Returns the input string converted into a tuple of
        values, each element of the tuple having been parsed accordingly to
        the argument parsefns.

        :param  parsefns: the conversion/parsing function or the list or tuple
                          of conversion/parsing functions that is used by the
                          method. The strings 'auto', 'tail' and 'skip' may be
                          used instead of a function to indicate special parsing
                          operations.
        :param  answ: The string to be parsed.
        :raises ValueError: If the input string is not compatible with the
                            conversion scheme described by convfs.
        """
        str_remain = answ.strip()
        vtuple = ()

        if isinstance(parsefns, (list, tuple)):
            single = False
            fn_iterator = cycle(list(parsefns))
        else:
            single = True
            fn_iterator = cycle([parsefns])

        for type_fn in fn_iterator:
            if str_remain == None:
                break

            if (type(type_fn) is str):
                if type_fn == 'auto':
                    # None requests automatic parsing in _string_extract()
                    type_fn = None
                elif type_fn == 'tail':
                    vtuple += (str_remain,)
                    break
                elif type_fn == 'skip':
                    # No try block here: parsing strings should never
                    # raise exceptions...
                    value, str_remain = DeepCommandInterface._string_extract(str, str_remain)
                    continue
                else:
                    msg = "string '%s' is not a valid special " % type_fn
                    msg += "parsing selector"
                    raise ValueError(msg)
            try:
                value, str_remain = DeepCommandInterface._string_extract(type_fn, str_remain)
            except ValueError as ex:
                raise ValueError(str(ex) + " from string: '" + answ + "'")

            if value != None:
                vtuple += (value,)

        return vtuple if len(vtuple) > 1 or not single else vtuple[0]


    def _get_instrument_info(self):
        """
        Returns a dictionary of relevant info on instrument firmware
        """
        if self._device()._devfamily is DeepDevice.ICEPAPMODE:
            # for IcePAP systems
            cmd_list = ["?_DRVVER", "?VER"]
        else:
            # for old and new DAnCE systems
            cmd_list = ["?APPNAME", "?VERSION"]

        return {cmd:self.command(cmd) for cmd in cmd_list}


    def verbose(self, val=None):
        if val is not None:
            log.level(val)
        return log.level()


    def isvalidcommand(self, cmdstr):
        """
        Returns True if the given command/query keyword in cmdstr is supported by the instrument
        """
        device = self._device()
        curr_commands = device.getcommandlist()
        if not curr_commands:
            curr_commands = device.getcommandlist(force=True)
            if not curr_commands:
                raise DeepCommError('cannot retrieve instrument commands')

        return cmdstr.split()[0].upper() in curr_commands


    def isalive(self):
        """
        Checks if the instrument is reachable and returns True/false accordingly
        """

        # try a generic command
        if self._device()._devfamily is DeepDevice.ICEPAPMODE:
            cmd = COMM_ALIVE_ICECMD
        else:
            cmd = COMM_ALIVE_CMD

        # requesting a command will force a try to reconnect to
        # the instrument if it was currently disconnected
        try:
            self.command(cmd)
        except:
            return False
        else:
            return True

    def datastream(self, stream_id, **kwargs):
        """
        Returns a DataStream object
        The stream id is in principle a string
        """
        return DataStream(self, stream_id, **kwargs)


    def datareceiver(self, stream, buffer, **kwargs):
        """
        Returns a DataReceiver object corresponding to the given stream.
        The stream can be either a stream id or a DataStream instance.
        """

        if not isinstance(stream, DataStream):
            ds = DataStream(self, stream)
        else:
            ds = stream

        return DataReceiver(ds, buffer, **kwargs)


class DeepDevice(DeepCommandInterface):
    DANCEMODE    = _DeepOperMode("DANCE")
    PREDANCEMODE = _DeepOperMode("PREDANCE")
    ICEPAPMODE   = _DeepOperMode("ICEPAP")
    LEGACYMODE   = _DeepOperMode("LEGACY")

    _devfamily = DANCEMODE
    _helpquery = "?HELP ALL"

    def __init__(self, dev_id, mode=None, debuglevel=None, timeout=None, forcePython=False, **kwargs):
        """
        Object initialization
        """
        if mode:
            if not isinstance(mode, _DeepOperMode):
                raise ValueError("Bad DeepDevice operation mode in 'mode' keyword")
            self._devfamily = mode

        # select engine
        if _clibdeep_is_valid() and not forcePython:
            from .cdeep import CDeepEngine as Engine
        else:
            from .pydeep import PyDeepEngine as Engine


        # update global level of debug if passed as keyword argument
        dbglevel = debuglevel if debuglevel is not None else self.libparam("debuglevel")
        self.libparam("debuglevel", dbglevel, engclass=Engine)

        # process timeout value if passed as keyword argument
        if timeout is not None:
            kwargs['timeoutms'] = int(1000 * timeout)

        try:
            self._deepengine = Engine(self, dev_id, **kwargs)

            # once self._deepengine is set, initialise the DeepCommandInterface settings
            super(DeepDevice, self).__init__(self)

            # and update command list
            self.getcommandlist()

        except DeepError as deeperr:
            raise deeperr


    # for backwards compatibility only
    def close(self):
        pass

    def _device(self): return self


    def cmdinterface(self):
        return DeepCommandInterface(self)


    def _syntax_error(self, msg):
        """
        Log the error message and raises an exception
        """
        log.error(msg, exception=SyntaxError)


class LegacyDevice(DeepDevice):
    _devfamily = DeepDevice.LEGACYMODE
    _helpquery = "?HELP"


class IcepapDevice(DeepDevice):
    _devfamily = DeepDevice.ICEPAPMODE
    _helpquery = "?HELP"


class OldDanceDevice(DeepDevice):
    _devfamily = DeepDevice.PREDANCEMODE
    _helpquery = "?HELP"


class DanceDevice(DeepDevice):
    _helpquery = "?HELP ALL"

    def dconfig(self):
        """
        Fetches the DAnCE configuration from the device by issuing a ?DCONFIG
        query and creates a associated DConfig object.

        :returns: a DConfig instance containing the current device
                  configuration.
        :raises DeepDeviceError: If the ?DCONFIG query is not properly implemented
                             in the DAnCE device.
        :raises ValueError: If the answer from the DAnCE device is badly
                            formatted.
        :raises IOError: If there is any communication error.
        """
        from .dconfig import DConfig

        return DConfig(self)


    def upload_dconfig(self, dconfig):
        """
        Uploads the configuration contained in a DConfig object into the
        DAnCE device.

        :param  dconfig: The DConfig instance containing the DAnCE configuration
        :raises IOError: If there is any communication error.
        :raises DeepDeviceError: If the DAnCE device does not accept the
                             configuration contained in dconfig.
        """
        dconfig.write(self)



