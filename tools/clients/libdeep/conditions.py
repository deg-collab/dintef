# -*- coding: utf-8 -*-
#
# this file is part of libdeep project
#


"""Predefined condition constants"""


# --------------------------------------------------------------------------
#
class ConditionDef(object):
    def __init__(self, name, desc, value=None):
        self.name  = name
        self.desc  = desc
        self.value = value

    def __str__(self):
        ret = "%s" % self.name
        if self.value:
            ret += "(%r)" % self.value
        return ret

    def __repr__(self):
        return "<Condition: %s %s>" % (self, self.desc)

# --------------------------------------------------------------------------
#
BUFFER_FILLED    = ConditionDef("BUFFER_FILLED",
                                "buffer filled")

BUFFER_START     = ConditionDef("BUFFER_START",
                                "buffer filling started")

BUFFER_NBYTES    = ConditionDef("BUFFER_NBYTES",
                                "buffer filled with nbytes")

BUFFER_WRITE     = ConditionDef("BUFFER_WRITE",
                                "some data written into the buffer")

DEV_DISCONNECTED = ConditionDef("DEV_DISCONNECTED",
                                "lost connection to instrument")

DEV_COMMERROR    = ConditionDef("DEV_COMMERROR",
                                "error on instrument communication")
