# -*- coding: utf-8 -*-
#
# this file is part of libdeep project
#


"""Handle communication with any Deep or IcePAP device"""

from __future__ import print_function
from __future__ import absolute_import

# Standard modules
import string
import time
import sys
import threading
import select
import struct

try:       # try Python 3
    import queue
except:    # use Python 2
    import Queue as queue


# DEEP modules
from .. import deeplog as log

from ..device     import DeepDevice
from ..dframe     import DataFrame
from ..conditions import *
from ..deepasync  import AsyncEvent
from ..deeparray  import DeepArray
from .sockdeep    import SockDeep, BadhostnameError
from .sldeep      import SLDeep


# --------------------------------------------------------------------------
#

# End of Command character and other special ones
COMM_EOL    = "\n"
COMM_ACK    = "#"
COMM_REQ    = "?"
COMM_ADR    = ":"
COMM_BIN    = "*"
COMM_MLI    = "$"

# Device long answer timeout in seconds
COMM_LONG_TIMEOUT = 20
COMM_TIMEOUT = 1

# Binary protocol
BIN_HEAD_SIG           = 0xa5a50000
BIN_HEAD_ICE_SIG       = 0xa5aa555a
BIN_HEAD_ASYNCICE_SIG  = 0xbebecafe
BIN_HEAD_DFRM_SIG      = 0xcabecafe
BIN_HEAD_SIGNMASK      = 0xffff0000
BIN_HEAD_NOCHECKSUM    = 0x00000010
BIN_HEAD_BIG_ENDIAN    = 0x00000020
BIN_HEAD_UNITMASK      = 0x0000000f

# Asynchronous binary streaming
STREAMID_ASYNCICE      = 'ASYNCICE'

# Binary value type given in bytes per single value
BIN_8  = 1
BIN_16 = 2
BIN_32 = 4
BIN_64 = 8

# Public objects of this module
__all__ = ['DeviceError']


# --------------------------------------------------------------------------
#
class DeviceError(IOError):
    """
    Used to customize exceptions.
    """

    pass
#   def __init__(self, device, message):
#          IOError.__init__(self, device.hostname() + ': ' + message)


# Asynchronous protocol
ASYNC_MARK_ICE     = 0xbebecafe
ASYNC_FRM_SHIFT    = 24
ASYNC_WLEN_MASK    = (1 << ASYNC_FRM_SHIFT) - 1


# TODO: use methods to change/get state
STATE_IDLE, STATE_ANS, STATE_ANS_DATA, STATE_DISCONNECT = range(4)
PARSER_STATES = {
    STATE_IDLE:       'waiting for action',
    STATE_ANS:        'waiting for ASCII answer from instrument',
    STATE_ANS_DATA:   'waiting for bin data from instrument',
    STATE_DISCONNECT: 'instrument disconnected'
}


# --------------------------------------------------------------------------
#
class ParserCommand(object):
    """
    Encapsulate commands to be passed to parser thread.
    """

    # -----------------------------------------------------------------------
    #
    def __init__(self,
                 caller,
                 action_func=None,
                 str_cmd=None,
                 in_data=None,
                 ans=None,
                 ans_data=None):

        # mandatory arguments
        self.caller      = caller

        # optional arguments
        self.action_func = action_func
        self.str_cmd     = str_cmd
        self.in_data     = in_data

        # references to argout objects
        self.ans         = ans
        self.ans_data    = ans_data

        # references to intermediate values
        self.cmd_prefix  = None
        self.cmd_type    = None
        self.chkanswer   = None

        # inter threads communication
        self.event_ans   = threading.Event()


# --------------------------------------------------------------------------
#
class EventStatusCbThread(threading.Thread):
    """
    Thread object that will execute a function passed from
    parser thread through a queue.
    """

    # -----------------------------------------------------------------------
    #
    def __init__(self, queue_cb):

        threading.Thread.__init__(self)
        self.queue_cb = queue_cb

    # -----------------------------------------------------------------------
    #
    def run(self):

        # never ending thread
        while True:

            # blocking call
            try:
                log.trace("callback: wait")
                cb = self.queue_cb.get()
            except queue.Empty:
                continue

            # minium check
            if not callable(cb):
                log.trace("callback: skipped")
                continue

            # execute call back
            log.trace("callback func beg (id %r)" % id(cb))
            try:
                cb()
            except DeviceError:
                break
            log.trace("callback func end (id %r)" % id(cb))

    # -----------------------------------------------------------------------
    #
    def suicide(self):
        log.trace("callback: suiciding")
        # Using a self reference (cf self.exit_flag) doesn't work
        # from time to time (bad thread context? GIL lock needed?)
        raise DeviceError("that's the end")


# --------------------------------------------------------------------------
# TODO: remove all underscores on method, protection not needed, this
#       class can't be called anyway by client
#
class ParserThread(threading.Thread):
    """
    Never ending thread centralizing communication from client threads
    to instrument and also handling instrument sync/async data.
    """

    # -----------------------------------------------------------------------
    #
    def __init__(self, ddevice):

        threading.Thread.__init__(self)
        self.ddevice        = ddevice

        # events handling
        # TODO: to be replaced with DataEvent() stuff
        self.event_status    = {}
        self.event_status_cb = {}
        self.events_nb       = 0
        self.lastevent       = None

        # async handling
        self.datareceivers  = {}  # DataReceiver objects indexed by callers
        self.datastreams    = {}  # DataStream objects indexed by stream ids
        self.deepbuffers    = {}  # DeepBuffer objects indexed by stream ids
        self.thread_events  = {}  # threading events indexed by conditions
        self.callbacks      = {}  # list of callbacks indexed by conditions

        # get settings
        self.devfamily      = ddevice.devfamily
        self._icepapmode    = self.devfamily is DeepDevice.ICEPAPMODE

        # TODO: change timeout to a lower more reactive value
        self.timeout        = 5

        self.partial_answer = None
        self.partial_dframe = None
        # list of live data frames
        self.dframes = {}

        # convert asynchronous header in socket ordered chars
        self.async_mark_ice = ''
        for idx in range(4):
            self.async_mark_ice += chr((ASYNC_MARK_ICE >> (8 * idx)) & 0xff)

        # pending queries FIFO stack
        self.query_stack   = []

        # pool of threads for status events callback
        self.queue_status_cb = queue.Queue()
        self.pool_status_cb  = []
        self.status_cbs      = []
        for i in range(5):
            t = EventStatusCbThread(self.queue_status_cb)
            t.daemon = True
            t.start()
            self.pool_status_cb.append(t)

        # establish the connection with the instrument
        self.comm_dev       = None
        self.comm_fileno    = None
        self.exit_flag      = False
        try:
            self.connect()
        except:
            if self.ddevice.get_reconnect_mode() == RECONNECT_NEVER:
                raise

    # -----------------------------------------------------------------------
    #
    def connect(self):
        """
        Try to connect to the instrument and raises an execption otherwise.
        The exeception is local to current thread, therefore it will
        not reach the main thread exception at DeepDevice object creation.
        """

        log.trace("connecting to device...")

        ddevice = self.ddevice
        timeout = ddevice.global_timeout
        dev_id  = ddevice._hostname

        self.endtime = None
        self.state   = STATE_DISCONNECT

        # TODO: try to guess if it is an IP or an SL to avoid
        # to create socket connection and getting socket timeout
        try:
            comm_dev = SockDeep(dev_id, log.level(), timeout)
            self.comm_dev    = comm_dev
            self.comm_fileno = comm_dev.fileno()
            self.state = STATE_IDLE
        except BadhostnameError:
            try:
                comm_dev = SLDeep(dev_id, log.level(), timeout)
            except:
                msg = 'unsupported communication device: "%s"' % dev_id
                # raise an exception
                log.error(msg, exception=IOError)
        except Exception as msg:
            # raise an exception
            log.error('unable to connect: %s' % msg, exception=IOError)

        if self.state == STATE_IDLE:
            self.__rd_ascii(reset=True)

    # -----------------------------------------------------------------------
    #
    def disconnect(self):
        log.trace("disconnecting to device...")
        # close communication stream
        self.comm_dev.close()
        self.state = STATE_DISCONNECT

    # -----------------------------------------------------------------------
    #
    def run(self):

        log.trace("starting thread...")
        wake_pipe = self.ddevice.wake_pipe

        # never ending loop
        while not self.exit_flag:

            # waits for action requests only
            outputs = []
            inputs = [wake_pipe]
            if self.state != STATE_DISCONNECT:
                inputs += [self.comm_fileno]

            # blocking call until there is something to read or timeout
            log.trace("state: \"%s\"" % PARSER_STATES[self.state])
            log.trace("waiting for...")
            #timeout = self.endtime - self.now if self.endtime else 20
            timeout = self.timeout
            readable, writable, exceptional = select.select(
                inputs, outputs, inputs, timeout)

            self.now = time.time()

            # ensure that there is something received
            if len(readable) == 0:
                log.trace("awaken by select timeout")

                # handle reconnection
                self.__handle_state()

                if self.endtime and self.now > self.endtime:
                    #print("__handle_timeout called")
                    self.__handle_timeout()
                continue

            # an action has been received
            if wake_pipe in readable:
                log.trace("awaken by action request")

                # empty pipe to prevent select() to return again
                wake_pipe.read(1)

                # get the action requested by the client
                argin = self.ddevice.queue_cmd.get(False)

                # process the action
                self.__handle_action(argin)

            # data has been received from instrument
            if self.comm_fileno in readable:
                log.trace("awaken by instrument")
                self.__handle_incoming_data()

            # normal awaken event processing end
            continue

        # close communication stream
        self.disconnect()

        # normal thread end
        log.trace("end of the thread")

        # TODO: free resources (if needed)

    # -----------------------------------------------------------------------
    #
    def __handle_timeout(self):
        log.trace("select timed out")

        # timeout waiting for instrument answer
        #if self.state in [ STATE_ANS, STATE_ANS_DATA ]:
        if len(self.query_stack):
            self.state = STATE_IDLE
            self._device_error("timeout waiting for answer")
        else:
            print("timing out...")

    # -----------------------------------------------------------------------
    #
    def action_events_nb(self, argin):
        log.trace("return number of events: %d" % self.events_nb)
        argin.ans = self.events_nb

        # normal end
        self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def action_events_getlast(self, argin):
        log.trace("return last event")
        argin.ans = self.lastevent

        # normal end
        self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def action_async_status_reg(self, argin):
        self.event_status[argin] = argin.event_status

        # normal end
        self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def action_async_status_unreg(self, argin):
        # the unreg() may have been called without previous reg()
        try:
            del(self.event_status[argin])
        except:
            pass

        # normal end
        self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def action_async_status_cb_reg(self, argin):
        self.event_status_cb[argin] = argin.event_status_cb

        # normal end
        self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def action_async_status_cb_unreg(self, argin):
        # the unreg() may have been called without previous reg()
        try:
            del(self.event_status_cb[argin])
        except:
            pass

        # normal end
        self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def action_async_receiver_reg(self, argin):
        # update DataReceiver objects list indexed by callers
        self.datareceivers[argin] = argin.datareceiver

        # update DataStream objects list indexed by stream ids
        stream = argin.datareceiver.stream
        stream_id = stream.id
        if stream_id not in self.datastreams:
            self.datastreams[stream_id] = []
            self.deepbuffers[stream_id] = []
        self.datastreams[stream_id].append(stream)
        self.deepbuffers[stream_id].append(argin.datareceiver.buffer)

        # normal end
        self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def action_async_receiver_unreg(self, argin):
        # minimum check
        if argin not in self.datareceivers:
            # hide error to stupid caller
            self.__complete_action(argin)

        # update DataReceiver objects list
        del(self.datareceivers[argin])

        # update DataStream objects list
        stream    = argin.datareceiver.stream
        stream_id = stream.id
        lst       = self.datastreams[stream_id]
        idx       = lst.index(stream)
        del(lst[idx])

        # update DeepBuffer objects list
        lst         = self.deepbuffers[stream_id]
        # we can not index directly the buffer object as it
        # have been reallocated. Trust instead that the two lists
        # are always manipulated simultaneously
        del(lst[idx])

        # normal end
        self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def action_async_event_reg(self, argin):

        # retrieve condition associated with the given event
        event  = argin.deepevent
        cond   = event.condition
        tevent = event.thread_event

        # update list of threading event used for blocking calls
        if cond not in self.thread_events:
            self.thread_events[cond] = []
        log.trace("tevt (id: %r)" % id(tevent))
        self.thread_events[cond].append(tevent)

        # normal end
        self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def action_async_event_unreg(self, argin):

        # retrieve condition associated with the given event
        event  = argin.deepevent
        cond   = event.condition
        tevent = event.thread_event

        # just in case an unreg is called without previous reg
        if cond not in self.thread_events:
            self.__complete_action(argin)
            return

        # just in case a conditions is shared between events
        if tevent not in self.thread_events[cond]:
            self.__complete_action(argin)
            return

        # update list of threading event used for blocking calls
        self.__cleanup_thread_event(cond, tevent)

        # normal end
        self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def __cleanup_thread_event(self, cond, tevent):
        log.trace("tevt (id: %r)" % id(tevent))
        idx = self.thread_events[cond].index(tevent)
        del self.thread_events[cond][idx]

        if len(self.thread_events[cond]) == 0:
            del self.thread_events[cond]

    # -----------------------------------------------------------------------
    #
    def action_async_event_reg_cb(self, argin):

        # retrieve condition associated with the given event
        event  = argin.deepevent
        cond   = event.condition
        cbs    = event.callbacks

        # update list of threading event used for blocking calls
        if cond not in self.callbacks:
            self.callbacks[cond] = []
        for cb in cbs:
            if cb not in self.callbacks[cond]:
                log.trace("callback func (id %r)" % id(cb))
                self.callbacks[cond].append(cb)
            else:
                log.trace("skipping func (id %r)" % id(cb))

        # normal end
        self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def action_async_event_unreg_cb(self, argin):

        # retrieve condition associated with the given event
        event  = argin.deepevent
        cond   = event.condition
        cbs    = event.callbacks

        # just in case an unreg is called without previous reg
        if cond not in self.callbacks:
            self.__complete_action(argin)
            return

        # update list of list of callbacks
        # TODO: WARNING if the same condition is shared between
        # several events, the callbacks of both events will be deleted
        for cb in self.callbacks[cond]:
            if cb in cbs:
                continue
            self.__cleanup_callbacks(cond, cb)

        # normal end
        self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def __cleanup_callbacks(self, cond, cb):
        log.trace("callback func (id %r)" % id(cb))
        idx = self.callbacks[cond].index(cb)
        del self.callbacks[cond][idx]

        if len(self.callbacks[cond]) == 0:
            del self.callbacks[cond]

    # -----------------------------------------------------------------------
    #
    def action_quit(self, argin):
        log.trace("requesting suicide to parser thread, not waiting")
        self.exit_flag = True
        argin.event_ans.set()

        # request suicide to all thread of the callback pool
        # TODO: what about if a client callback is beeing
        # executed? Raise an exception in the thread using ctype API?
        log.trace("requesting suicide to threads of callback pool")
        log.trace("waiting...")
        i = 1
        for t in self.pool_status_cb:
            log.trace("%d dying..." % i)
            self.queue_status_cb.put(t.suicide)
            i += 1

        # The thread joining can not be done just after requesting
        # suicide (observation, no explanation, due to GIL that need
        # time to raise exceptions within the threads?
        for t in self.pool_status_cb:
            # TODO: from time to time the suicide fails therefore
            # a timeout is mandatory but then a thread is let behind!
            t.join(timeout=1)
            if t.isAlive():
                log.error("Ooops, one callback thread is blocked",
                          raise_exception=False)
        log.trace("RIP!")
        return

    # -----------------------------------------------------------------------
    #
    def action_command(self, argin):
        # process a command from a client
        self.__command(argin)

    # -----------------------------------------------------------------------
    #
    def __handle_action(self, argin):

        log.data("awaken by action request. caller id: %d" % id(argin))
        log.trace("action: \"%s\"" % argin.action_func.__name__)

        try:
            argin.action_func(argin)
        except Exception as msg:
            log.error("parser action ignored, error: %r" % msg,
                      raise_exception=False)
            self.__complete_action(argin)

    # -----------------------------------------------------------------------
    #
    def __complete_action(self, argin):

        log.data("caller id: %d" % id(argin))
        log.trace("action: \"%s\"" % argin.action_func.__name__)

        # unblock caller
        argin.event_ans.set()

    # -----------------------------------------------------------------------
    #
    def __binary_data_buffer(self, size, itemsize, checksum):

        if not len(self.query_stack):
            self._device_error("unsolicited data from instrument")
            return

        cmd = self.query_stack[0]
        # handle case when binary comes before ASCII answer
        if not cmd.ans:
            self._device_error("unsolicited data from instrument")
            return

        buffer = cmd.ans_data
        if not buffer or \
           (checksum and buffer.itemsize != itemsize) or \
           len(buffer) * buffer.itemsize != size * itemsize:
            d = {1: 'uint8', 2: 'uint16', 4: 'uint32', 8: 'uint64'}[itemsize]
            buffer = DeepArray(bytearray(size), dtype=d)
            cmd.ans_data = buffer

        return buffer

    # -----------------------------------------------------------------------
    #
    def __input_binary_data(self):
        log.trace("waiting for header signature...")
        while True:
            # TODO: headersig type moved from string to bytearray
            nbytes, headersig = self.comm_dev.peek_uint32()
            if nbytes == 0:
                self.__handle_state(STATE_DISCONNECT)
                break

            if nbytes == 4:
                # Convert header to little endian
                if sys.byteorder == "big":
                    headersig.byteswap()
                break

            if '\n' in headersig:
                # ASCII answer
                return None

            # We should never get here...
            time.sleep(0.001)

        # Now we have to check for a binary header...
        if headersig == BIN_HEAD_DFRM_SIG:
            msg = "DFRAME binary"
        elif headersig == BIN_HEAD_ICE_SIG:
            msg = "IcePAP binary"
        elif headersig == BIN_HEAD_ASYNCICE_SIG:
            msg = "IcePAP async binary"
        elif (headersig & BIN_HEAD_SIGNMASK) == BIN_HEAD_SIG:
            msg = "DAnCE binary"
        else:
            log.trace("going for ASCII")
            return None
        log.trace("header signature: %r" % msg)

        """
        DFrame binary data format, taken from daq-HOWTO.txt documentation:

        typedef struct
        {
            #define DCHUNK_MAGIC 0xcabecafe
            uint32_t magic;

            /* header size */
            uint16_t header_size;

            /* versioning */
            uint8_t version;

            /* data size */
            #define DCHUNK_DATA_MAX_SIZE UINT16_MAX
            uint16_t data_size;

            #define DSTREAM_ID_SIZE 12
            #define DSTREAM_ID_INVALID "INVALID"
            #define DSTREAM_ID_ASCII "ASCII"
            #define DSTREAM_ID_BINARY "BINARY"
            #define DSTREAM_ID_LOG "LOG"
            char stream_id[DSTREAM_ID_SIZE];

            /* first chunk of a frame */
            #define DCHUNK_FLAG_FIRST (1 << 0)
            uint8_t flags;

            /* 0 is last chunk in frame */
            uint32_t rem_frame_size;

            /* raw data following */
            /* uint8_t data[1]; */
        } dchunk_header_t;
        """

        # Async binary data
        if headersig == BIN_HEAD_DFRM_SIG:
            # Discard the 32bits signature
            self.comm_dev.discard(4)

            # Get the header size
            nbytes, dchunk_hsize = self.comm_dev.peek_uint16()
            log.trace("dchunk header size: %r" % dchunk_hsize)

            # Get the full header (except the signature)
            dchunk_hsize -= 4
            header = DeepArray(range(dchunk_hsize), dtype='uint8')
            self.comm_dev.getdata(header)

            # Get dchunk information
            dchunk_hsize, proto_version, dchunk_size, \
                stream_id, stream_flags, rem_size =   \
                struct.unpack_from("<"+"HBH12sBl", header)
            log.trace("protocol version  : %r" % proto_version)
            log.trace("dchunk size bytes : %r" % dchunk_size)
            stream_id = stream_id.rstrip('\x00')
            log.trace("stream string id  : %r" % stream_id)
            log.trace("stream flags      : %r" % stream_flags)
            log.trace("remaining bytes   : %r" % rem_size)

            # Detect async incoming data
            sync = (stream_id == 'ASCII') or (stream_id == 'BINARY')

            #
            buffer = DeepArray(range(dchunk_size), 'uint8')
            return DataFrame(self.comm_dev.reader,
                             buffer=buffer,
                             stream_id=stream_id,
                             sync=sync)

        # IcePAP Async binary data
        elif headersig == BIN_HEAD_ASYNCICE_SIG:
            """
            IcePAP specific header:

            2 words startup mark
            2 words for the binary data length + frame number << 24
            2 words for the checksum
            n words of data
            """

            header = DeepArray([0, 0, 0], dtype='uint32')
            self.comm_dev.getdata(header)
            itemsize  = 2          # only 16bit words can be transferred
            checksum  = header[2]  # checksum is mandatory
            size      = header[1] & ASYNC_WLEN_MASK
            frame_n   = header[1] >> ASYNC_FRM_SHIFT

            log.trace("data len     : 0x%08x(words)" % size)
            log.trace("checksum     : 0x%08x" % checksum)
            log.trace("frame number : %d" %  frame_n)

            buffer = DeepArray(range(size), 'uint16')
            return DataFrame(self.comm_dev.reader,
                             buffer=buffer,
                             checksum=checksum,
                             stream_id=STREAMID_ASYNCICE,
                             sync=False,
                             ice_frame=frame_n)

        # Standard sync binary data
        elif (headersig == BIN_HEAD_ICE_SIG) or \
             ((headersig & BIN_HEAD_SIGNMASK) == BIN_HEAD_SIG):

            header = DeepArray([0, 0, 0], dtype='uint32')
            self.comm_dev.getdata(header)
            size = header[1]

            if headersig == BIN_HEAD_ICE_SIG:
                itemsize = 2          # only 16bit words can be transferred
                checksum = header[2]  # checksum is mandatory

            else:
                itemsize = header[0] & BIN_HEAD_UNITMASK
                if header[0] & BIN_HEAD_NOCHECKSUM:
                    checksum = None
                else:
                    checksum = header[2]

            try:
                buffer = self.__binary_data_buffer(size, itemsize, checksum)
            except Exception as ex:
                # discard socket data
                self.comm_dev.discard(size * itemsize)
                self._device_error(ex)
                return

            return DataFrame(self.comm_dev.reader,
                             buffer=buffer,
                             checksum=checksum,
                             sync=True)

        # Not known binary data type, we should never arrive here
        log.trace("unhanled binary")
        return None

    # -----------------------------------------------------------------------
    #
    def __handle_state(self, new_state=None):
        """
        Returns False if the instrument is not connected
        """
        if new_state:
            self.state = new_state

        # handle here disconnection policy
        log.trace("state: \"%s\"" % PARSER_STATES[self.state])
        if self.state != STATE_DISCONNECT:
            return True

        # try to recover connection to the instrument
        log.trace("try to recover connection...")
        msg = 'no idea why'
        lvl = log.level()
        log.level(log.DBG_NONE)
        try:
            self.connect()
        except Exception as msg:
            pass
        log.level(lvl)

        # only clients that are waiting for an answer will be informed
        log.trace("state: \"%s\"" % PARSER_STATES[self.state])
        if self.state == STATE_DISCONNECT:
            self._device_error("instrument not connected: %s" % msg)
            return False

        return True

    # -----------------------------------------------------------------------
    #
    def __handle_incoming_data(self):

        first = True

        # loop while there is data to read/process
        while True:
            nchar = self.comm_dev.getnchar()
            log.trace("chars received pending: %d" % nchar)

            # awaken with no incoming data means instrument has disconnected
            if first:
                first = False
                if nchar == 0:
                    self.__handle_state(STATE_DISCONNECT)
                    return
            elif nchar == 0:
                return

            if not self.partial_answer:
                # try to process binary data
                if not self.partial_dframe:
                    try:
                        self.partial_dframe = self.__input_binary_data()
                    except IOError as e:
                        # device error (disconnect, checksum, timeout,...)
                        self._device_error(e)
                        return

                if self.partial_dframe:
                    if not self.partial_dframe.read_chunk():
                        return
                    if self.partial_dframe.iscomplete():
                        if self.partial_dframe.issync():
                            self.__process_binary_answer()
                        elif self.partial_dframe.isasync():
                            self.__process_binary_async()
                        else:
                            log.trace("Unknown dframe type, skipping it")
                    self.partial_dframe = None
                    continue

            # if we get here we have an ASCII answer
            try:
                cmd, ans = self.__rd_ascii()
            except IOError as e:
                # device error found (disconnect, checksum, timeout,...)
                self._device_error(e)
                return

            if ans:
                if cmd:
                    self.__process_ascii_answer(cmd, ans)
                self.partial_answer = None
                continue
            else:
                log.trace("returning, need to wait for answer")
                return

    # -----------------------------------------------------------------------
    #
    def __process_ascii_answer(self, cmd, ans):
        """ Process ascii answer from instrument """

        log.trace("cmd type: %r" % cmd.cmd_type)
        if cmd.ans:
            raise RuntimeError("should not get in here, pending answer: %r" %
                               cmd.ans)

        if ans.startswith("ERROR"):
            self._device_error(ans.replace("ERROR ", "", 1))
            return

        cmd.ans = ans

        # expecting also binary answer from instrument
        if ("bin" in cmd.cmd_type) and ("req" in cmd.cmd_type):
            return
        cmd.ans_data = None

        # at this point the query is complete
        self.__complete_query(cmd)
        return

    # -----------------------------------------------------------------------
    #
    def __process_binary_answer(self):
        # expecting binary answer from instrument
        cmd = self.query_stack[0]

        # check that the ASCII answer has already been received
        if cmd.ans is None:
            self._device_error("unexpected binary data from instrument")
            return

        # at this point the query is completed
        self.__complete_query(cmd)
        return

    # -----------------------------------------------------------------------
    #
    def __process_binary_async(self):
        stream_id = self.partial_dframe._stream_id
        log.trace("dframe: %r" % stream_id)

        # treat specific case of IcePAP data streams
        if stream_id == STREAMID_ASYNCICE:
            self.__process_asyncice(self.partial_dframe)
            return

        # treat data stream only if there is a client interested
        if stream_id not in self.datastreams:
            # TODO: otherwise simply delete dframe
            log.trace("nobody interested, deleting")
            return

        # treat DAnCE data streams
        for db in self.deepbuffers[stream_id]:
            log.trace("updating DeepBuffer object")

            # TODO: here comes the tricky things, data handling
            # currently, the copy is mandatory because the reference
            # allocation doesn't work even if id() seems to show the
            # reference for both objects. TO BE IMPROVED
            db.write(self.partial_dframe.buffer)

            # update buffer triggered conditions
            self.__update_conditions(db, len(self.partial_dframe.buffer))

            # handle here conditions/events
            self.__handle_conditions(db)

    # -----------------------------------------------------------------------
    #
    def __update_conditions(self, db, written_bytes=0):
        """
        Update deepbuffer triggered conditions
        """

        # TODO: the buffer object can be used outside async streams
        # therefore the conditions should not be handled by buffer methods???
        for cond in db.conditions:
            cond.clear()
            triggered = True

            # each condition may have several requirements,
            # all of them must be valid to trigger the condition
            for subcond in cond.conditions:
                if not self.__is_condition(subcond, db, written_bytes):
                    triggered = False
                    break
            if triggered:
                cond.trigger()

    # -----------------------------------------------------------------------
    #
    def __is_condition(self, conddef, db, written_bytes=0):
        """
        Returns True if the given unique condition is true for the
        given DeepBuffer
        """

        if not isinstance(conddef, ConditionDef):
            raise RuntimeError("internal error, wrong condition def type")

        if conddef == BUFFER_FILLED:
            if db.free() == 0:
                return True

        if conddef == BUFFER_START:
            if (written_bytes != 0) and (db.used() == written_bytes):
                return True

        if conddef == BUFFER_NBYTES:
            if db.used() >= conddef.value:
                return True

        if conddef == BUFFER_WRITE:
            if (written_bytes != 0):
                return True

        return False

    # -----------------------------------------------------------------------
    #
    def __handle_conditions(self, db):
        """
        Handle deepbuffer triggered conditions
        """
        log.debug("conditions for this buffer:%r" % len(db.conditions))
        for cond in db.conditions:
            if not cond.is_triggered():
                continue

            log.trace("triggered condition: %s" % cond)
            log.debug("waiting  clients in total:%r" % len(self.thread_events))
            log.debug("callback clients in total:%r" % len(self.callbacks))

            # wake up blocked clients
            if cond in self.thread_events:
                log.debug("waiting clients for this condition:%r" %
                          len(self.thread_events[cond]))
                for tevent in self.thread_events[cond]:
                    log.trace("waking up tevnt (id: %r)" % id(tevent))
                    tevent.set()
                    self.__cleanup_thread_event(cond, tevent)

            #
            if cond in self.callbacks:
                log.debug("callback clients for this condition:%r" %
                          len(self.callbacks[cond]))
                for cb in self.callbacks[cond]:
                    log.trace("calling func (id: %r)" % id(cb))
                    self.queue_status_cb.put(cb)

    # -----------------------------------------------------------------------
    #
    def __complete_query(self, cmd):
        log.trace("cmd: %r " % cmd.str_cmd)
        log.data("for answer, using event: %d" % id(cmd.event_ans))
        cmd.event_ans.set()

        # remove query from FIFO stack
        self.query_stack.pop(0)

        # change state only if no more caller waiting for instrument
        # TODO: remove global state or move it to each query state
        if len(self.query_stack) == 0:
            self.state = STATE_IDLE

        # normal end of instrument answer handle
        return

    # -----------------------------------------------------------------------
    #
    def __command(self, argin):

        # retrieve instrument command to process
        # caller    = argin.caller
        cmd     = argin.str_cmd
        in_data = argin.in_data

        # remove any useless ending white spaces and eols
        cmd = cmd.strip(" \n\r")
        log.trace("command received: [%s]" % cmd)

        # some parsing to guess what to do
        # TODO: what the hell is this chkanswer
        chkanswer = False
        prefix, cmd_type, chkanswer = self.__cmd_type(cmd, chkanswer)

        # memorize command information for afterward answer
        argin.cmd_prefix = prefix
        argin.cmd_type   = cmd_type
        argin.chkanswer  = chkanswer
        argin.ans        = None

        # if the client is waiting for an answer, put query
        # in the stack to allow returning it errors
        if "req" in cmd_type or "ack" in cmd_type:
            self.query_stack.append(argin)

        # handle reconnection
        if not self.__handle_state():
            return

        # minimum check if binary download is requested
        if in_data is None:
            if "bin" in cmd_type and "req" not in cmd_type:
                self._syntax_error("binary data is missing")
            else:
                self.__wr(cmd)

        else:
            if "bin" not in cmd_type:
                self._syntax_error(
                    "downloading binary with a non binary command")
            elif "req" in cmd_type:
                self._syntax_error("downloading binary with a query command")
            else:
                self.__wr_bin(cmd, in_data)
                if "ack" in cmd_type:
                    self.comm_dev.set_timeout(COMM_LONG_TIMEOUT)

        if "req" in cmd_type or "ack" in cmd_type:
            argin.state = STATE_ANS
            #self.query_stack.append(argin)
        else:
            argin.ans = None
            argin.event_ans.set()

    # -----------------------------------------------------------------------
    # Command Syntax: [#][<address>:]<keyword> [param1 [param2] ...][\r]\n
    # Keyword Syntax: [?|*|?*]<string>
    #
    # Returns prefix, cmd_type, chkanswer_flag
    #
    def __cmd_type(self, str_cmd, chkanswer):

        # will return a list
        cmd_type = []

        # consider only the command not its params
        cmd = str_cmd.split()[0].upper()

        # check if acknowledge is requested
        if cmd[0] == COMM_ACK:
            ack = True
            cmd = cmd.lstrip(COMM_ACK)
        else:
            ack = False

        # check for an address field
        spcmd = cmd.split(COMM_ADR)
        n = len(spcmd)
        if n == 1:
            cmd_addr = None
            prefix = spcmd[0]
        elif n == 2:
            cmd_addr = spcmd[0]
            prefix = spcmd[1]
        else:
            self._syntax_error("too many \"%s\" chars" % COMM_ADR)

        if not prefix:              # missing keyword
            self._syntax_error("missing command keyword")
        elif prefix[0] == COMM_REQ:   # this is a query
            cmd_type.append("req")
            cmd_key = prefix[1:]
            if cmd_addr == "":        # if broadcast, error
                self._syntax_error("queries cannot be broadcasted")
            elif ack:                 # if acknowledge, cancel answer check
                chkanswer = False
            else:
                chkanswer = True
        else:
            cmd_key = prefix
            if ack:
                cmd_type.append("ack")

        # check if binary
        if cmd_key[0] == COMM_BIN:      # binary data
            cmd_type.append("bin")
            cmd_key = cmd_key[1:]

        # minimum check on characters for address and command fields
        if not cmd_key.replace("_", "").isalnum():
            self._syntax_error("invalid character in command %s" % (prefix))

        if cmd_addr and not cmd_addr.replace("_", "").isalnum():
            self._syntax_error("invalid character in address %s" % (cmd_addr))

        if cmd_addr and self._icepapmode:
            prefix = cmd_addr + COMM_ADR + prefix

        # normal end
        msg = "command: [%s] type: %s" % (prefix, cmd_type)
        log.trace(msg)
        return prefix, cmd_type, chkanswer

    # -----------------------------------------------------------------------
    #
    def __rd_ascii(self, reset=False):
        if reset:
            self.partial_answer = None
            self.comm_dev.getline(reset=True)
            return

        try:
            cmd = self.query_stack[0]
            prefix = cmd.cmd_prefix
        except:
            cmd = None
            prefix = ''

        if not self.partial_answer:
            ans = self.comm_dev.getline()
            if ans is None:
                return cmd, None

            # Be tolerant with IcePAP because some commands are not
            # protocol compliant.
            # Seen with MCPU1 firmware <=1.07 on "client not granted" error
            if not ans.startswith(prefix):
                if not self._icepapmode:
                    msg = 'Missing prefix "%s" in device answer: "%s"' % \
                        (prefix, ans)
                    self.__rd_ascii(reset=True)
                    self._device_error(msg)

                # some commands answer the prefix but without the address field
                prefix = prefix.lstrip(string.digits+':'+'*')
                if ans.startswith(prefix):
                    ans = ans[len(prefix):].strip()
                else:
                    ans = ans.strip()
            else:
                ans = ans[len(prefix):].strip()
            self.partial_answer = [ans]

        # Multiline answer
        if self.partial_answer[0] == COMM_MLI:
            while True:
                line = self.comm_dev.getline()
                if line is None:
                    return cmd, None
                log.data("<=== line: [%s]" % line)
                if line == COMM_MLI:
                    ans = COMM_EOL.join(self.partial_answer[1:])
                    break
                else:
                    self.partial_answer.append(line)
        else:
            ans = self.partial_answer[0]
        log.trace("<=== [%s]" % ans)
        self.partial_answer = None
        return cmd, ans

    # -----------------------------------------------------------------------
    #
    def __wr(self, str_cmd):
        """ TODO
        if self._to_be_flushed:
           self.flush()
        """

        # send the command passed as bytes object
        log.trace("===> [%s]" % str_cmd)
        self.comm_dev.puts(bytearray(str_cmd, 'ascii') + b"\n")

    # -----------------------------------------------------------------------
    #
    def __dump_bin(self, data):
        """Dump in hexa the values of the DeepArray of type from 8 to 64bit"""

        # give up if nothing to do
        if(log.level() < log.DBG_DATA):
            return

        # guess information on data to dump
        bufsize  = data.size        # total size in bytes
        datasize = data.nitems      # number of values
        datatype = data.itemsize    # number of bytes per individual value

        # minium check
        if datatype not in [BIN_8, BIN_16, BIN_32, BIN_64]:
            self._syntax_error("unsupported data type: BIN_%dbits" %
                               (datatype*8))
            return

        #
        log.trace("  binary data: %d bytes" % bufsize)
        lnv = 80 / (datatype * 2 + 4)
        for j in range(1, datasize + 1):
            if datatype == BIN_8:
                hexstr = "%02x" % (data[j-1] & 0xff)
            # NOTE MP 14Jun2013:
            # problem with string formating in Python<=2.6.5
            # with integers larger than 32bit. Therefore handle them by hand
            if datatype >= BIN_16:
                hexstr = "%04x" % (data[j-1] & 0xffff)
            if datatype >= BIN_32:
                hexstr = "%04x" % ((data[j-1] >> 16) & 0xffff) + hexstr
            if datatype >= BIN_64:
                hexstr = "%04x" % ((data[j-1] >> 32) & 0xffff) + hexstr
                hexstr = "%04x" % ((data[j-1] >> 48) & 0xffff) + hexstr

            hexstr = "  0x" + hexstr
            sys.stdout.write(hexstr)
            if not j % lnv:
                sys.stdout.write("\n")
        #
        sys.stdout.write("\n")
        sys.stdout.flush()

    # -----------------------------------------------------------------------
    #
    def __wr_bin(self, str_cmd, in_data):
        self.__wr(str_cmd)

        # prepare binary protocol header
        header = DeepArray([0, 0, 0], dtype='uint32')
        if not self._icepapmode:
            header[0] = BIN_HEAD_SIG | (in_data.itemsize & BIN_HEAD_UNITMASK)
            header[1] = in_data.nitems
            # checksum calculated using native type
            header[2] = in_data.sum() & 0xffffffff
        # for IcePAP the protocol is different
        else:
            header[0] = BIN_HEAD_ICE_SIG
            header[1] = int(in_data.size / 2)  # data length given in words
            # checksum calculated over 16bits words
            header[2] = in_data.sum(dtype='uint16') & 0xffffffff

        # sent header (the float() is needed to handle the unsigned long)
        log.data("header   field: 0x%08x" % float(header[0]))
        log.data("data len field: 0x%08x" % float(header[1]))
        log.data("checksum field: 0x%08x" % float(header[2]))

        # header must be always sent as little-endian (mandatory)
        # binary data is convenient (and not to use the BIG_ENDIAN flag)
        if sys.byteorder == "big":
            header[0] |= BIN_HEAD_BIG_ENDIAN
            header.byteswap()  # convert header in little endian

        # send the header and the binary block
        log.trace("===> [%d binary data]" % header[1])
        self.__dump_bin(in_data)
        self.comm_dev.puts(header)
        self.comm_dev.puts(in_data)

    # -----------------------------------------------------------------------
    #
    def __rd_bin(self):
        # load binary protocol header
        header = DeepArray.fromstring(self.comm_dev.getchar(12),
                                      dtype='uint32')
        if sys.byteorder == "big":
            header.byteswap()            # convert header from little endian

        # received header (the float() is needed to handle the unsigned long)
        log.data("header   field: 0x%08x" % float(header[0]))
        log.data("data len field: 0x%08x" % float(header[1]))
        log.data("checksum field: 0x%08x" % float(header[2]))
        # retrieve information from binary protocol header
        if not self._icepapmode:
            if (header[0] & BIN_HEAD_SIGNMASK) != BIN_HEAD_SIG:
                raise IOError("bad binary data header")
            itemsize  = header[0] & BIN_HEAD_UNITMASK
            usechksum = not (header[0] & BIN_HEAD_NOCHECKSUM)
            bigendian = header[0] & BIN_HEAD_BIG_ENDIAN
        # for IcePAP the protocol is different
        else:
            if header[0] != BIN_HEAD_ICE_SIG:
                raise IOError("bad IcePAP binary data header")
            itemsize  = 2      # only 16bit words can be transferred
            usechksum = True   # checksum is mandatory
            bigendian = False

        size     = header[1]
        checksum = header[2]

        bin_block = DeepArray.fromstring(
            self.comm_dev.getchar(size * itemsize),
            dtype={1: 'uint8', 2: 'uint16', 4: 'uint32', 8: 'uint64'}[itemsize])

        if (bigendian and sys.byteorder == "little") or \
           (not bigendian and sys.byteorder == "big"):
            bin_block.byteswap()  # convert data into native ordering

        #
        calc_checksum = bin_block.sum() & 0xffffffff  # checksum to 32 bits
        if usechksum and calc_checksum != checksum:
            raise IOError("Bad binary checksum")

        # normal end
        self.__dump_bin(bin_block)
        log.trace("<=== [%d binary data]" % header[1])
        return bin_block

    # -----------------------------------------------------------------------
    #
    def __process_asyncice(self, dframe):

        # Handle IcePAP asynchronous binary data
        # TODO: this is temporary and has to be replace by DeepEvent

        # at this point, we have a valid asynchronous event
        self.events_nb += 1
        self.lastevent = AsyncEvent()

        # free description information for the event
        desc = {}
        desc['type']         = 'icepap_report'
        desc['data_len']     = len(dframe.buffer)
        desc['data_chksum']  = dframe._checksum
        desc['frame_number'] = dframe._ice_frame
        self.lastevent.set_desc(desc)

        # TODO: store data into the event
        #data = DeepArray.fromstring(bin_block, dtype = 'uint16')
        self.lastevent.set_data(dframe.buffer)

        # inform/send data to registered clients
        log.trace("informing %d status registered client(s)" %
                  len(self.event_status))
        for caller in self.event_status:
            self.event_status[caller].set()

        # launch registered callbacks execution in separated threads
        log.trace("launching %d callback(s)" %
                  len(self.event_status_cb))
        for caller in self.event_status_cb:
            self.queue_status_cb.put(self.event_status_cb[caller])

        # normal end
        return

    # -----------------------------------------------------------------------
    # Log the error message but not raise an exception
    #
    def _syntax_error(self, msg):

        # exceptions would remain local to parser thread,
        # therefore only the queue can be used to communicate the error
        # to main thread
        log.error(msg, raise_exception=False, exception=SyntaxError)

        # serialized answer to caller thread
        query = self.query_stack[0]

        # the main thread will raise the exception
        # TODO: add a new member to ParserCommand() rather than re-using ans
        query.ans = SyntaxError(msg)
        query.event_ans.set()

        # remove query from FIFO stack
        self.query_stack.pop(0)

    # -----------------------------------------------------------------------
    # Log the error message but not raise an exception
    #
    def _device_error(self, msg):

        # exceptions would remain local to parser thread,
        # therefore only the queue can be used to communicate the error
        # to main thread
        log.error(msg, raise_exception=False, exception=DeviceError)

        # handle errors on async data from instrument
        if not len(self.query_stack):
            # TODO: memorized the error for afterward reporting to client
            # (something like the IcePAP ?SYSERR)
            return

        # serialized answer to caller thread
        log.trace("request to the main thread to raise an exception")
        query = self.query_stack[0]

        # the main thread will raise the exception
        # TODO: add a new member to ParserCommand() rather than re-using ans
        query.ans = DeviceError(msg)
        query.event_ans.set()

        # remove caller from FIFO stack
        self.query_stack.pop(0)
