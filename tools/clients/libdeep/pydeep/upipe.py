# -*- coding: utf-8 -*-
#
# this file is part of libdeep project
#

"""Universal unidirectional pipe"""


__all__ = ['socket_pipe', 'UPipe']

import os
import socket
from threading import Lock
from platform import system


if system() == "Windows":
    class UPipe(object):
        """
        Unidirectional pipe capable of waking up select().
           See the module's docstring for more info.
        """
        def __init__(self):
            self._out, self._in = socket_pipe()
            self._lock = Lock()

        def __del__(self):
            (self._out.close(), self._in.close())

        def write(self, data):
            with self._lock:
                self._in.sendall(data)

        def read(self, bufsize):
            return self._out.recv(bufsize)

        def fileno(self):
            return self._out.fileno()

        def close(self):
            self.__del__()

else:

    class UPipe(object):
        """
        Unidirectional pipe capable of waking up select().

        The functionaliy is comparable to the one provided by os.pipe()
        but can be used with select() loops in Windows.

        Another difference is that both ends of the pipe are packaged
        in a single object.

        Notes:
            - read() and write() are blocking operations
            - Python 3 does not accept strings as data, one must use byte,
                bytearray or similar binary data objects
        """
        def __init__(self):
            self._out, self._in = os.pipe()

        def __del__(self):
            (os.close(self._out), os.close(self._in))

        def write(self, data):
            """
            Pushes data into the pipe input (bytearray)
            """
            os.write(self._in, data)

        def read(self, bufsize):
            """
            Reads up to bufsize bytes from the pipe output
            """
            return os.read(self._out, bufsize)

        def fileno(self):
            """
            Returns the associated file descriptor as required by select()
            """
            return self._out

        def close(self):
            """
            Closes and frees the system resources
            """
            self.__del__()


def socket_pipe():
    """
    Establish a local connection between two sockets
    Returns a tuple with the two connected socket objects
    """
    # prepare a temporary server socket
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.bind(('localhost', 0))
    serversocket.listen(0)

    # find the port of the server socket
    address, port = serversocket.getsockname()

    # create an INET, STREAMing socket
    socket_A = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socket_A.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
    # and connect to the server on the right port
    socket_A.connect(('localhost', port))

    # complete the connection
    socket_B, addr = serversocket.accept()
    socket_B.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
    # and finally close server socket
    serversocket.close()

    return socket_A, socket_B
