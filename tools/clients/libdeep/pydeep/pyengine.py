# -*- coding: utf-8 -*-
#
# this file is part of libdeep project
#


"""Handle communication with any Deep or IcePAP device"""

from __future__ import absolute_import, print_function

# Standard modules
import threading
import string
try:        # try Python 3
    import queue
except:     # use Python 2
    import Queue as queue

from ..device import DeepDevice, DeepCommandInfo, DeepEngine, log_print

# DEEP modules
from ..           import deeplog as log
from .parser      import ParserThread, ParserCommand, DeviceError
from .upipe       import UPipe
from ..deeparray  import DeepArray
from ..deepasync  import DataStream, DataReceiver

# Device long answer timeout in seconds
COMM_LONG_TIMEOUT = 20
COMM_TIMEOUT      = 1

# Device generic command
COMM_ALIVE_CMD    = "?APPNAME"
COMM_ALIVE_ICECMD = "?_SOCKPING"
DSTREAMS_QCMD     = "?DSTREAM"
DSTREAMS_CMD      = "DSTREAM"
DSTREAMS_ICEQCMD  = "?REPORT"
DSTREAMS_ICECMD   = "REPORT"


class PyDeepCommandInfo(DeepCommandInfo):
    _filedescr = None

    def __init__(self, useselect=False):
        self.cmdinfo = ParserCommand()
        if useselect:
            self._evfd = EventFD()
            self._evfd.add_evsource(self.cmdinfo)
        else:
            self._evfd = None

    def fileno(self):
        if self._evfd:
            return self._evfd.fileno()
        else:
            raise IOError

    def cleanselect(self):
        cpointer = self._evfd.get_evsource()
        if cpointer != self.cmdinfo.cpointer :
            raise IOError



class PyDeepEngine(DeepEngine):
    def __init__(self, dev_object, dev_id, **kwargs):
        params = super(PyDeepEngine, self).__init__(dev_object, dev_id, **kwargs)

        for parname in {"debugtags", "maxthreads", "maxthreads"}:
            if parname in params:
                 raise NotImplementedError('keyword argument \'{}\' keyword not implemented in Python libdeep version'.format(parname))
                 
        # Get mandatory argins
        self._hostname = dev_id

        self.devfamily = params['DEVFAMILY']
        for p in params:
            print(p, params[p])

        # at this point, all options are valid
        log.trace("object created, device: \"%s\"" % dev_id)

        # guess if running on a Windows machine
        self._to_be_flushed = False

        # open communication channel to the instrument
        # prepare safe inter-thread communication links
        self.queue_cmd = queue.Queue()
        self.wake_pipe = UPipe()
        self._local    = DeepDevLocalData(self)

        # create the parser thread (this may raise exceptions...)
        self.parser    = ParserThread(self)

        # thread must a daemon one to not block application exit
        self.parser.daemon = True

        # launch the parser thread
        self.parser.start()

        # TODO: is this still needed? parser object has a ref to ddevice object
        # at this point it should not be dangerous to bypass safe inter-threads
        # communication because the client has still not got the hand
        self.parser.devfamily = self.devfamily

        log_print("Created:", self.__class__)

    def icepapmode(self):
        return self.devfamily is DeepDevice.ICEPAPMODE
        
    def __del__(self): log_print("Destroying", self.__class__)

    @staticmethod
    def libparameter(param, value=None, device=None):
        print("libparameter(): ", param, value)

    def timeoutms(self, timeoutms=None):
        if timeoutms is not None:
            pass
        return 0

    def reconnect(self, reconnect=None):
        if reconnect is not None:
            pass
        return True

    @staticmethod
    def dump_internals():
        raise NotImplementedError

    def newcommandinfo(self):
        return PyDeepCommandInfo(self._select is not None)

    def new_cmd(self, cmdline, bindata, addr, ack, comminfo):
        """
        Initialises and sends a command to the instrument.
        Returns the DeepCommandInfo instance that must be used to monitor
        the progress and the result of the command with the 'done' attribute updated.
        The returned instance may be optionally provided by the caller with the
        comminfo keyword.
        """
        #print('<{}> bindata={} addr={} ack={}'.format(cmdline, bindata, addr, ack))
        firstchar = cmdline[0]
        if ack:
            if firstchar == ":" or addr == "":
                raise ValueError("broadcast messages cannot be acknowledged")
            elif firstchar not in "#?":
                cmdline = "#" + cmdline

        if addr is not None:
            cmdkeyword = cmdline.split(' ', 1)[0]
            if ":" in cmdkeyword:
                raise ValueError("command string includes unexpected ':' addressing character")
            else:
                cmdline = addr + ":" + cmdline

        # send command to instrument
        cmdinfo = comminfo.cmdinfo
        cmdinfo.action_func = self.parser.action_command
        cmdinfo.str_cmd = cmdline
        cmdinfo.in_data = bindata
        cmdinfo.event_ans.clear()
        self.__protected_send_action(cmdinfo)

        comminfo.done = cmdinfo.done



    def check_cmd(self, comminfo, wait=False):
        """
        Checks the completion state of the command monitored by the DeepCommandInfo
        instance comminfo and updates and returns the 'done' attribute.
        """
        comminfo.done = True
        return comminfo.done


    def answer_cmd(self, comminfo):
        """
        Returns the answer of the command monitored by the DeepCommandInfo instance
        comminfo as either None, an answer string or a tuple (answer, bindata) depending
        of the type of query.
        """
        if not comminfo.done:
            self.wait_cmd(comminfo)
            
        # retrieve answer
        cmdinfo = comminfo.cmdinfo
        ans = self.__parser_get_answer(cmdinfo)
        ans_data = cmdinfo.ans_data
        return ans, ans_data


    @classmethod
    def _syntax_error(self, msg):
        log.error(msg, exception=SyntaxError)



    def set_timeout(self, timeout):
        self.global_timeout = timeout

    def get_timeout(self):
        return self.global_timeout


    #
    #
    def set_reconnect_mode(self, mode):
        newmode = string.lower(mode)
        if newmode not in RECONNECT_ACTIONS:
            self._syntax_error("invalid reconnection mode: \"%s\"" % mode)
        self._reconnect = newmode

    #
    #
    def get_reconnect_mode(self):
        return self._reconnect

    def get_reconnect_desc(self, mode=None):
        if mode:
            newmode = string.lower(mode)
            if newmode not in RECONNECT_ACTIONS:
                self._syntax_error("invalid reconnection mode: \"%s\"" % mode)
            mode = newmode
        else:
            mode = self._reconnect
        return RECONNECT_ACTIONS[mode]


    def hostname(self):
        return(self._hostname)

    def datastream(self, stream_id, **kwargs):
        """
        Returns a DataStream object
        The stream id is in principle a string
        """
        return DataStream(self, stream_id, **kwargs)


    def datareceiver(self, stream, buffer, **kwargs):
        """
        Returns a DataReceiver object correspoding to the given stream.
        The stream can be either a stream id or a DataStream instance.
        """

        #
        if not isinstance(stream, DataStream):
            ds = DataStream(self, stream)
        else:
            ds = stream

        return DataReceiver(ds, buffer, **kwargs)


    def _syntax_error(self, msg):
        """
        Log the error message and raises an exception
        """
        log.error(msg, exception=SyntaxError)



    def close(self):
        # ask to the parser thread to suicide
        # TODO: investigate why nocheck is needed here....
        self.__parser_request_action(self.parser.action_quit, nocheck=True)

        # wait for the end of the parser thread
        self.parser.join()

    def flush(self):
        super(DeepDevicePy, self).flush()
        self.comm_dev.flush()


    def __protected_send_action(self, action):
        # A slightly complicated way of avoiding desynchronisation in case
        #    of asynchronous interruption by KeyboardInterrupt
        try:
            # put stuff in the message queue
            self.queue_cmd.put(action)
            # awake parser thread
            self.wake_pipe.write(b' ')

        except KeyboardInterrupt:
            log.trace("KeyboardInterrupt during deepdevice command queuing")
            # repeat wake up message
            # The instrument thread must be tolerant to spurious awake chars...
            self.wake_pipe.write(b' ')
            raise KeyboardInterrupt

    def __parser_request_action(self, action_func, nocheck=False):
        action = self._local.parser_cmd

        action.action_func = action_func
        action.event_ans.clear()
        self.__protected_send_action(action)
        if nocheck:
            return action
        else:
            return self.__parser_get_answer(action)

    def __parser_get_answer(self, cmd):

        # blocking call with 1sec timeout
        # TODO: handle here instrument timeout
        cmd.event_ans.wait(1)
        if cmd.event_ans.is_set() is False:
            raise RuntimeError("internal error, missing thread answer")
        ans = cmd.ans

        # check for errors in the parsing thread
        if isinstance(ans, Exception):
            raise ans

        return ans

    def __parser_send_cmd(self, str_cmd, in_data=None):

        cmd = self._local.parser_cmd
        cmd.action_func = self.parser.action_command
        cmd.str_cmd = str_cmd
        cmd.in_data = in_data
        cmd.event_ans.clear()
        self.__protected_send_action(cmd)
        return(cmd)


    # TODO: temporary added by MO, should be removed and use
    # async_xxx methods instead
    def register_command_cb(self, str_cmd, fun):
        if callable(fun):
            if str_cmd not in self._cmd_callbacks:
                self._cmd_callbacks[str_cmd] = []
            self._cmd_callbacks[str_cmd].append(fun)
        else:
            msg = 'The callback %s is not callable' % fun
            raise AssertionError(msg)

    def async_events_received(self):

        # send request to thread and get answer
        ans = self.__parser_request_action(self.parser.action_events_nb)
        return ans

    def async_events_getlast(self):

        # send request to thread and get answer
        ans = self.__parser_request_action(self.parser.action_events_getlast)
        return ans

    def async_status_register(self):

        # TODO: add support to DAnCE instruments
        if not self.icepapmode():
            raise RuntimeError("only IcePAP is supported for the moment")

        # inter-thread communication
        # TODO: handle multi-threaded client
        event_status = threading.Event()
        event_status.clear()
        self.event_status = event_status
        self._local.parser_cmd.event_status = event_status

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_status_reg)

    def async_status_unregister(self):
        # TODO: add support to DAnCE instruments
        if not self.icepapmode():
            raise RuntimeError("only IcePAP is supported for the moment")

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_status_unreg)

    def async_status_waitfor(self, timeout=None):
        # TODO: add support to DAnCE instruments
        if not self.icepapmode():
            raise RuntimeError("only IcePAP is supported for the moment")

        # blocking call
        self.event_status.wait(timeout)
        if self.event_status.is_set() is False:
            raise RuntimeError("timeout waiting for async status event")

    def async_status_register_cb(self, cb):

        # TODO: add support to DAnCE instruments
        if not self.icepapmode():
            raise RuntimeError("only IcePAP is supported for the moment")

        # inter-thread communication
        self._local.parser_cmd.event_status_cb = cb

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_status_cb_reg)

    def async_status_unregister_cb(self):
        # TODO: add support to DAnCE instruments
        if not self.icepapmode():
            raise RuntimeError("only IcePAP is supported for the moment")

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_status_unreg)

    def _async_stream_update(self):
        """
        Update internal info about current instruments streams
        """

        self._stream_ids    = []
        self._stream_states = {}
        self._stream_scopes = {}
        if self.icepapmode():
            # on IcePAP only one stream is available
            ans = self.command(DSTREAMS_ICEQCMD)
            sid = 'REPORT'
            self._stream_ids.append(sid)
            self._stream_states[sid] = ans
            self._stream_scopes[sid] = 'GLOBAL'

        elif self._oldmode:
            raise RuntimeError("not implemented yet for old DAnCE instruments")

        else:
            # get the current streams + their configuration
            # example of answer:
            # "LOG OFF GLOBAL\nASCII OFF LOCAL\nBINARY OFF LOCAL"
            ans = self.command(DSTREAMS_QCMD)
            for stream in ans.split('\n'):
                sid, state, scope = stream.split()
                self._stream_ids.append(sid)
                self._stream_states[sid] = state
                self._stream_scopes[sid] = scope

    def async_stream_ids(self):
        """
        Returns the ids list of all the streams currently available on the
        instrument whatever their state
        """

        # the streams can be created dynamically
        # therefore the instrument has to be interrogated each time
        self._async_stream_update()
        return self._stream_ids

    def async_stream_state(self, stream_id):
        """
        Returns True if the give stream is active
        """

        # the streams can be created dynamically
        # therefore the instrument has to be interrogated each time
        self._async_stream_update()

        # minimum check
        if stream_id not in self._stream_ids:
            raise ValueError("wrong stream id")

        return (self._stream_states[stream_id] == 'ON')

    def async_stream_active(self, stream_id, action):
        """
        Stops/starts an instrument stream
        """

        # minimum checks
        if action not in [True, False]:
            raise ValueError("wrong action")
        if stream_id not in self._stream_ids:
            raise ValueError("wrong stream id")

        # action on stream
        arg = {True: "ON", False: "OFF"}[action]

        if self.is_dance():
            cmd = "%s %s %s" % (DSTREAMS_CMD, stream_id, arg)
        elif self.is_icepap():
            cmd = "%s %s" % (DSTREAMS_ICECMD, arg)

        ans = self.ackcommand(cmd)
        if ans != "OK":
            raise RuntimeError("unable to activate stream '%s'" % stream_id)

    def async_receiver_register(self, datareceiver):
        """
        Register a new DataReceiver object to the parser thread
        """
        # inter-thread communication
        self._local.parser_cmd.datareceiver = datareceiver

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_receiver_reg)

    def async_receiver_unregister(self, datareceiver):
        """
        Unregister a DataReceiver object from parser thread
        """
        # inter-thread communication
        self._local.parser_cmd.datareceiver = datareceiver

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_receiver_unreg)

    def async_event_register(self, deepevent):
        """
        Register a new DeepEvent object to the parser thread
        """

        # inter-thread communication
        self._local.parser_cmd.deepevent = deepevent

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_event_reg)

    def async_event_unregister(self, deepevent):
        """
        Unregister a DeepEvent object from the parser thread
        """

        # inter-thread communication
        self._local.parser_cmd.deepevent = deepevent

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_event_unreg)

    def async_event_register_cb(self, deepevent):
        """
        Register a new DeepEvent object to the parser thread
        """

        # inter-thread communication
        self._local.parser_cmd.deepevent = deepevent

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_event_reg_cb)

    def async_event_unregister_cb(self, deepevent):
        """
        Unregister a DeepEvent object from the parser thread
        """

        # inter-thread communication
        self._local.parser_cmd.deepevent = deepevent

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_event_unreg_cb)



class DeepDevLocalData(threading.local):
    """
    Class of thread-local data for DeepDevice.
    The first time a new thread uses an instance of it, a new
    local copy is initialised by calling __init__() with the
    initial arguments.
    """
    def __init__(self, ddevice):
        self.parser_cmd = ParserCommand(ddevice)

