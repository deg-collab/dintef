# -*- coding: utf-8 -*-
#
# this file is part of libdeep project
#


from __future__ import print_function
from __future__ import absolute_import

# Standard modules
import socket
import time
import sys
import io

from ..deeparray import DeepArray

# DEEP modules
from .. import deeplog as log


# --------------------------------------------------------------------------
#

# Default socket port to use
HOST_PORT = 5000

# Default answer timeout in seconds
HOST_TIMEOUT = 1

# Default verbose level
DEF_VERBOSE = log.DBG_NONE

# Used to end listening thread
exitFlag = False

# Buffer size for chache
CACHE_SIZE = 4096


# --------------------------------------------------------------------------
#
class SockDeep(object):

    # ------------------------------------------------------------------------
    #
    def __init__(self, host, verbose=DEF_VERBOSE, timeout=None):
        global exitFlag

        try:
            # allow to specify a different port
            h, p = host.split(":")
        except:
            # otherwise use the default port
            h = host
            p = HOST_PORT

        # get options
        self.set_verbose(verbose)

        # at this point we should have every thing we need
        self.host_name = h
        self.host_port = int(p)
        msg = "object created, host: \"%s:%d\"" % \
              (self.host_name, self.host_port)
        log.trace(msg)

        # open a socket connection
        self.host_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # set socket answer timeout
        if timeout is None:
            timeout = HOST_TIMEOUT
        self.set_timeout(default=timeout)

        # try to connect to the device
        try:
            self.host_socket.connect((self.host_name, self.host_port))
            # create a BufferReader object on top of the receiving socket
            self.host_socket.settimeout(self.host_timeout)
            #self.reader = io.open(self.host_socket.fileno(), mode='rb')
            self.reader = DeepBufferedReader(self.host_socket)
        except socket.gaierror:
            # invalid host_name with no address associated to it
            raise BadhostnameError
        except Exception as msg:
            #msg = "unable to connect to host: \"%s:%d\""% \
            #      (self.host_name, self.host_port)
            log.error(msg, exception=NetworkError)

        # get release of python beeing used
        (major, minor, micro, rel, ser) = sys.version_info
        self.python3 = (major > 2)


    # ------------------------------------------------------------------------
    #
    def close(self):
        global exitFlag

        log.trace("close requested")

        # inform the listening thread that the show is ending
        exitFlag = True

        log.trace("close finished")


    # ------------------------------------------------------------------------
    # TODO: new mandatory method
    def fileno(self):
        return self.host_socket


    # ------------------------------------------------------------------------
    #
    def set_timeout(self, timeout=None, default=None):
        if default is not None:
            self.host_timeout = default


    # ------------------------------------------------------------------------
    #
    def puts(self, cmd):
        try:
            self.host_socket.sendall(cmd)
        except:
            msg = "error sending string to host: \"%s\"" % (self.host_name)
            log.error(msg, exception=NetworkError)


    # ------------------------------------------------------------------------
    #
    def getnchar(self):
        return len(self.reader.peek())


    # ------------------------------------------------------------------------
    #
    def getline(self, reset=False):
        if reset:
            self._line = ''
            self._endtime = None
            return
        else:
            #print("in getline")
            if self._endtime and time.time() > self._endtime:
                self.getline(reset=True)
                log.error("readline timeout", exception=IOError)

            more = self.reader.readline()
            if more:
                line = self._line + more
                if line[-1] == '\n':
                    self.getline(reset=True)
                    return line[:-1]
                else:
                    self._line = line
            self._endtime = time.time() + self.host_timeout
            #print("returning None")
            return None

    # ------------------------------------------------------------------------
    #
    def peek_uint8(self, swap=False):
        data = self.reader.peek(1)
        nbytes = len(data)
        if nbytes >= 1:
            u8 = DeepArray.fromstring(data[:1], 'uint8')
            return 1, u8[0]
        else:
            return nbytes, data

    # ------------------------------------------------------------------------
    #
    def peek_uint16(self, swap=False):
        data = self.reader.peek(2)
        nbytes = len(data)
        if nbytes >= 2:
            u16 = DeepArray.fromstring(data[:2], 'uint16')
            if swap:
                u16.byteswap()
            return 2, u16[0]
        else:
            return nbytes, data

    # ------------------------------------------------------------------------
    #
    def peek_uint32(self, swap=False):
        #self.host_socket.setblocking(0)
        data = self.reader.peek(4)
        nbytes = len(data)
        if nbytes >= 4:
            #u32 = DeepArray.fromstring(bytes(data[:4]), 'uint32')
            u32 = DeepArray.fromstring(data[:4], 'uint32')
            if swap:
                u32.byteswap()
            return 4, u32[0]
        else:
            return nbytes, data

    # ------------------------------------------------------------------------
    #
    # This method should be called always with some received data,
    # in the buffer, otherwise it triggers BlockingIOError
    def getdata(self, buffer, nowait=False, timeout=None):
        if nowait:
            pass
            #self.host_socket.setblocking(0)
        else:
            if not timeout:
                timeout = self.host_timeout
            self.host_socket.settimeout(timeout)

        try:
            nbytes = self.reader.readinto(buffer)
            return(nbytes)
        except TypeError as ex:  # this dirty hack is due to spurious exception
            #print(ex, ex.args[0], dir(ex))
            raise io.BlockingIOError(0, ex.args[0])
        except io.BlockingIOError as ex:
            #print(ex, ex.args, dir(ex))
            raise ex

    # ------------------------------------------------------------------------
    #
    def discard(self, nbytes):
        self.reader.discard(nbytes)

    # ------------------------------------------------------------------------
    #
    def flush(self):
        log.trace("flushing connection...")
        self.host_socket.sendall(b'\n')

        while True:
            # non blocking call, exception raised if nothing to read
            try:
                tmp = self.host_socket.recv(CACHE_SIZE, socket.MSG_DONTWAIT)
            except:
                break

            if len(tmp) == 0:
                break

            log.trace("bytes flushed: %d" % len(tmp))

        # normal end
        return


    # ------------------------------------------------------------------------
    #
    def set_verbose(self, verb):
        log.trace("set_verbose called")
        log.level(verb)


    # ------------------------------------------------------------------------
    #
    def get_verbose(self):
        log.trace("get_verbose called")
        return(log.level())


    # ------------------------------------------------------------------------
    #
    def __del__(self):
        log.trace("object deleted, host  : \"%s\""%self.host_name)
        #self.listener.host_socket.close()



# --------------------------------------------------------------------------
#
class NetworkError(IOError):
    pass


# --------------------------------------------------------------------------
#
class BadhostnameError(IOError):
    pass


# --------------------------------------------------------------------------
#
class DeepBufferedReader(object):

    # ------------------------------------------------------------------------
    #
    def __init__(self, host_socket, timeout=1):
        self.host_socket = host_socket
        self.cache       = bytearray()
        self.timeout     = timeout


    # ------------------------------------------------------------------------
    #
    def peek(self, nbytes=-1):
        """
        Return up to nbytes bytearray
        TODO: for the moment, to be compatible with BufferedReader,
        returns bytes.

        If nbytes is unspecified all the bytes available are returned.
        The data is not consumed.
        """
        if len(self.cache) != 0:
            if nbytes == -1:
                nbytes = len(self.cache)
            if len(self.cache) >= nbytes:
                return bytes(self.cache[:nbytes])

        if nbytes == -1:
            nbytes = CACHE_SIZE

        try:
            self.host_socket.setblocking(False)
            ret = self.host_socket.recv(nbytes, socket.MSG_PEEK)
        except socket.error:
            # Getting error(11, 'Resource temporarily unavailable')
            ret = ""

        return bytes(self.cache)+ret



    # ------------------------------------------------------------------------
    #
    def update(self):
        lbeg = len(self.cache)
        try:
            self.cache += self.host_socket.recv(CACHE_SIZE)
        except socket.error:
            # Getting error(11, 'Resource temporarily unavailable')
            return
        log.trace("new bytes: %d"% (len(self.cache) - lbeg))
        #if (len(self.cache) - lbeg) == 6:
        #     import pdb;pdb.set_trace()

    # ------------------------------------------------------------------------
    #
    def readline(self, blocking=True):
        """
        Return a single line.
        The newline terminator character is left at the end.
        The data is consumed.
        Non blocking by default.
        """
        if len(self.cache) == 0:
            self.update()
        tbeg = time.time()
        while True:
            try:
                i = self.cache.index('\n')
            except ValueError:
                i = -1

            if(i >= 0):
                i += 1  # return also the newline
                line = bytes(self.cache[:i])
                log.data("returning line %r"%line)
                self.consume(i)
                return line

            if not blocking:
                raise RuntimeError("no newline available")

            if(time.time() - tbeg) > self.timeout:
                raise RuntimeError("timeout waiting for newline")

            self.update()

    # ------------------------------------------------------------------------
    #
    def readinto(self, arr, blocking=False):
        """
        Read up to (len(arr) * item size) bytes into array buffer.
        Returns the number of bytes read.
        The data is consumed.
        """
        tbeg = time.time()

        # Handle the simpler case of a bytearray passed as argin
        isba = isinstance(arr, bytearray)

        lbuf = len(arr)
        if not isba:
            lbuf *= arr.itemsize

        while True:
            lbeg = len(self.cache)
            if(lbeg >= lbuf):
                if not isba:
                    # TODO: here comes the dirty things,
                    # the array.fromstring() appends data to the given array
                    # therefore a copy through an intermediate array is needed
                    #array.array.fromstring(arr, bytes(self.cache[:lbuf]))
                    tmp = DeepArray.fromstring(bytes(self.cache[:lbuf]),
                                               arr.dtype)
                else:
                    tmp = self.cache[:lbuf]

                for i in range(len(arr)):
                    arr[i] = tmp[i]


                # TODO: handle the case of no array passed as argin
                self.consume(lbuf)
                return lbuf

            if(time.time() - tbeg) > self.timeout:
                raise RuntimeError("timeout waiting for %dbytes"%lbuf)

            self.update()


    # ------------------------------------------------------------------------
    #
    def consume(self, nbytes=-1):
        """
        Consume up to nbytes bytes from the cache.
        If nbytes is unspecified all the bytes available are discarded.
        """
        cache_bytes = len(self.cache)
        if ((nbytes > cache_bytes) or (nbytes == -1)):
            nbytes = cache_bytes

        log.data("bytes consumed from cache: %d"%nbytes)
        self.cache = self.cache[nbytes:]

        return nbytes


    # ------------------------------------------------------------------------
    #
    def discard(self, nbytes=-1):
        """
        Discard up to nbytes bytes.
        If nbytes is unspecified all the bytes available are discarded.
        The data is consumed.
        """

        if nbytes == -1:
            log.trace("full purge")
            rd_bytes = self.consume(-1)
            while True:
                try:
                    rd_bytes += self.host_socket.recv(CACHE_SIZE)
                except:
                    break

        else:
            log.trace("bytes to discard: %d"%nbytes)
            rd_bytes  = self.consume(nbytes)
            rem_bytes = nbytes - rd_bytes
            if(rem_bytes > 0):
                try:
                    rd_bytes += self.host_socket.recv(rem_bytes)
                except:
                    pass

        log.trace("bytes discarded: %d"%nbytes)
        return rd_bytes
