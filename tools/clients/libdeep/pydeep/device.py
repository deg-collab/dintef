# -*- coding: utf-8 -*-
#
# this file is part of libdeep project
#

"""Handle communication with any Deep or IcePAP device"""

from __future__ import absolute_import, print_function

# Standard modules
import threading
import string
try:        # try Python 3
    import queue
except:     # use Python 2
    import Queue as queue

# DEEP modules
from .           import deeplog as log
from .parser     import ParserThread, ParserCommand, DeviceError
from .parserdefs import *
from .deeparray  import DeepArray
from .upipe      import UPipe
from .deepasync  import DataStream, DataReceiver


# Device generic commands
COMM_ALIVE_CMD    = "?APPNAME"
COMM_ALIVE_ICECMD = "?_SOCKPING"
DSTREAMS_QCMD     = "?DSTREAM"
DSTREAMS_CMD      = "DSTREAM"
DSTREAMS_ICEQCMD  = "?REPORT"
DSTREAMS_ICECMD   = "REPORT"


# --------------------------------------------------------------------------
#
class DeepDeviceOld(object):
    """
    Class used to communicate with an instrument using the DEEP protocol.

    Object creation::

        dev = DeepDevice('pepudcm2')

        dev = DeepDevice('iceeu4', mode='icepap')
        print dev.is_icepap()

        dev = DeepDevice('pepudcm2', reconnect='once')
        print dev.get_reconnect_mode()

        dev = DeepDevice('pepudcm2', verb=2)
        print dev.get_verbose()

        dev = DeepDevice('pepudcm2', internals=clib)

    Usage examples::

        dev = DeepDevice('pepudcm2')
        ans = dev.command('?APPNAME')
        ans = dev.command('?FSTATUS', addr, addr+1)
        ans = dev.ackcommand('MODE OPER')

        datablock = DeepArray(100, dtype='uint32')
        ans = dev.ackcommand('*BTEST', datablock)

        ans, datablock = dev.command('?*BTEST')
    """

    #
    # Object creation
    #
    def __new__(cls, dev, **kwargs):

        # do not consumme the "internals" constructor argin
        internals = kwargs.get("internals", INTERNALS_PYDEEP)

        internals = string.lower(internals)
        if internals not in INTERNALS_ACTIONS:
            cls._syntax_error("invalid internals option: \"%s\"" % internals)

        if internals == INTERNALS_CDEEP:
            return DeepDeviceC(dev, **kwargs)
        elif internals == INTERNALS_PYDEEP:
            return DeepDevicePy(dev, **kwargs)
        else:
            cls._syntax_error("not implemented internals option: \"%s\"" % \
                internals)

    #
    # Log the error message and raises an exception
    #
    @classmethod
    def _syntax_error(self, msg):
        log.error(msg, exception=SyntaxError)


# --------------------------------------------------------------------------
#
class DeepDeviceBase(object):
    """
    Class template for implementing DEEP protocol 
    """

    # protocol has different versions
    _icepapmode      = False  # not all standard DAnCE commands+different bin
    _oldmode         = False  # different binary (ex: BCDU8)
    _instrument_info = None
    _reconnect       = None
    _use_clib        = False
    _mode            = None


    #
    #
    def __init__(self, dev, **kwargs):
        """
        Object initialization
        """

        # Get mandatory argins
        self._hostname = dev

        # Get optional argins
        mode      = kwargs.pop("mode",      None)
        verb      = kwargs.pop("verb",      log.DBG_ERROR)
        timeout   = kwargs.pop("timeout",   COMM_TIMEOUT)
        reconnect = kwargs.pop("reconnect", RECONNECT_NEVER)
        internals = kwargs.pop("internals", INTERNALS_PYDEEP)
        if kwargs.keys():
            self._syntax_error("unknown option: '%s'" % list(kwargs.keys())[0])

        # initialize object internals
        if mode:
            mode = string.lower(mode)
            if mode == "icepap":
                self._icepapmode = True
            elif mode == "oldmode":
                self._oldmode    = True
            else:
                self._syntax_error("invalid communication mode: \"%s\"" % mode)
            self._mode = mode

        # initialize object internals
        reconnect = string.lower(reconnect)
        if reconnect not in RECONNECT_ACTIONS:
            self._syntax_error("invalid reconnection mode: \"%s\"" % reconnect)

        # initialize object internals
        internals = string.lower(internals)
        if internals not in INTERNALS_ACTIONS:
            self._syntax_error("invalid internals option: \"%s\"" % internals)
        self._use_clib = (internals == INTERNALS_CDEEP)

        # at this point, all options are valid
        log.trace("object created, device: \"%s\"" % dev)
        self.set_verbose(int(verb))
        self.set_debug_mode(False)
        self.set_reconnect_mode(reconnect)

        # guess if running on a Windows machine
        self._to_be_flushed = False

        # initialise timeout
        self.set_timeout(float(timeout))


        # open communication channel to the instrument
        self.open()


    #
    #
    def initialize(self):
        """
        Initialize internals about the instrument connected to
        """

        # TODO: move _guess_mode() and _get_instrument_info() to
        # parser thread to be able to call them on reconnection...
        # ...or not: wait for next client command and then call them
        # if state is disconnected
        # TODO: get state from parser safely (inter-threads comm)
        #return

        # try to guess the kind of instrument protocol to use
        if not self._mode:
            self.__guess_mode()
        if self._icepapmode:
            log.trace("using IcePAP compatibility mode")
        elif self._oldmode:
            log.trace("using old DAnCE protocol mode")
        else:
            log.trace("using new DAnCE protocol mode")

        # information on internals
        if(self._use_clib):
            log.trace("using C shared library for internals")
        else:
            log.trace("using pure Python for internals")

        # get from instrument its implemented commands
        try:
            self.commands = self._getcommandlist()
        except Exception as msg:
            self.close()
            msg = "%s\nNot active device: \"%s\"" % (msg, self._hostname)
            log.error(msg, exception=IOError)

        # get some information on instrument for further reconnection checks
        self._instrument_info = self._get_instrument_info()


    #
    #
    def open(self):
        # Mandatory overwritten
        raise NotImplementedError


    #
    #
    def close(self):
        # Mandatory overwritten
        raise NotImplementedError


    #
    #
    def execcommand(self):
        # Mandatory overwritten
        raise NotImplementedError


    #
    #
    def flush(self):
        """
        Empty communication channel to the instrument
        """
        if self.debug_mode:
            print("Flushing ...")
        self._to_be_flushed = False


    #
    #
    def command(self, str_cmd, *args):
        """
        Process a command on the instrument
        """

        # minimum command check
        str_cmd = str_cmd.strip()

        # the optional binary argin data must be the last argument passed
        if args and (isinstance(args[-1], DeepArray) or
                     isinstance(args[-1], bytearray)):

            bin_data = args[-1]
            args = args[0:-1]

            # tolerate binary data as bytearray type in addition to DeepArray
            if isinstance(bin_data, bytearray):
                bin_data = DeepArray(bin_data)
        else:
            bin_data = None

        args_cmd = " ".join(map(str, args))
        if args_cmd:
            str_cmd += ' ' + args_cmd

        # send the command to instrument
        return self.execcommand(str_cmd, bin_data)

    #
    #
    def ackcommand(self, str_cmd, *args):
        """
        Process a command on the instrument and force acknowledge
        """

        # minimum command check
        str_cmd = str_cmd.strip()
        if str_cmd[0] != "#" and str_cmd[0] != "?":
            str_cmd = "#" + str_cmd

        return self.command(str_cmd, *args)


    #
    #
    def __guess_mode(self):
        """
        Try to guess the instrument protocol variants
        """

        log.trace("guess protocol using some specific commands...")

        # decrease verbose level here to avoid frighting client
        lvl = self.get_verbose()
        self.set_verbose(log.DBG_NONE)

        # try a command only implemented in IcePAP systems
        try:
            self.command("?_DRVVER")
            self._icepapmode = True
        except:
            self._icepapmode = False

        # try a command is only implemented in new DAnCE systems
        try:
            self.command("?DINFO")
            self._oldmode = False
        except:
            self._oldmode = True

        # normal end
        self.set_verbose(lvl)
        log.trace("...done")


    #
    #
    def _get_instrument_info(self):
        """
        Returns a dictionnary of relevant info on instrument firmware
        """

        ret = {}
        if self._icepapmode:
            # for IcePAP systems
            cmd_list = ["?_DRVVER", "?VER"]
        else:
            # for old and new DAnCE systems
            cmd_list = ["?APPNAME", "?VERSION"]

        for cmd in cmd_list:
            ret[cmd] = self.command(cmd)

        return ret


    #
    #
    def is_olddance(self):
        return (self._oldmode is True)

    def is_dance(self):
        return ((self._oldmode is False) and (self._icepapmode is False))

    def is_icepap(self):
        return (self._icepapmode is True)


    #
    #
    def clone(self):
        return(self)


    #
    #
    def set_debug_mode(self, dbgmode):
        self.debug_mode = bool(dbgmode)

    def set_verbose(self, val):
        log.level(val)

    def get_verbose(self):
        return(log.level())


    #
    #
    def set_timeout(self, timeout):
        self.global_timeout = timeout

    def get_timeout(self):
        return self.global_timeout


    #
    #
    def set_reconnect_mode(self, mode):
        newmode = string.lower(mode)
        if newmode not in RECONNECT_ACTIONS:
            self._syntax_error("invalid reconnection mode: \"%s\"" % mode)
        self._reconnect = newmode

    #
    #
    def get_reconnect_mode(self):
        return self._reconnect

    #
    #
    def get_reconnect_desc(self, mode=None):
        if mode:
            newmode = string.lower(mode)
            if newmode not in RECONNECT_ACTIONS:
                self._syntax_error("invalid reconnection mode: \"%s\"" % mode)
            mode = newmode
        else:
            mode = self._reconnect
        return RECONNECT_ACTIONS[mode]


    #
    #
    def hostname(self):
        """
        Returns the instrument hostname
        """
        return(self._hostname)


    #
    #
    def _getcommandlist(self):
        """
        Returns the current list of commands supported by instrument
        """
        if self._icepapmode:
            answ = self.command("?HELP").splitlines()
            answ = [s for line in answ for s in line.split()]
        else:
            answ = self.command("?HELP ALL").splitlines()
            answ = [s.split(":")[0].strip() for s in answ if s.rfind(":") >= 0]
        return answ


    #
    #
    def getcommandlist(self):
        """
        Returns the list of commands supported by instrument
        """
        return self.commands

    #
    #
    def isvalidcommand(self, comm):
        """
        Returns True if the given command is supported by instrument
        """
        if comm.split()[0].upper() in self.commands:
            return True
        else:
            return False

    #
    #
    def isalive(self):
        """
        Returns True if the associated instrument is reachable.
        """

        # try a generic command
        if self._icepapmode:
            cmd = COMM_ALIVE_ICECMD
        else:
            cmd = COMM_ALIVE_CMD

        # requesting a command will force a try to reconnect to
        # the instrument if it was currently disconnected
        try:
            self.command(cmd)
        except:
            # return False instead of raising exception
            # self._syntax_error("isalive command \"%s\" failed"%cmd)
            alive = False
        else:
            alive = True

        # normal end
        return alive

    #
    #
    def datastream(self, stream_id, **kwargs):
        """
        Returns a DataStream object
        The stream id is in principle a string
        """
        return DataStream(self, stream_id, **kwargs)


    #
    #
    def datareceiver(self, stream, buffer, **kwargs):
        """
        Returns a DataReceiver object correspoding to the given stream.
        The stream can be either a stream id or a DataStream instance.
        """

        #
        if not isinstance(stream, DataStream):
            ds = DataStream(self, stream)
        else:
            ds = stream

        return DataReceiver(ds, buffer, **kwargs)


    #
    #
    def _syntax_error(self, msg):
        """
        Log the error message and raises an exception
        """
        log.error(msg, exception=SyntaxError)


# --------------------------------------------------------------------------
#
class DeepDeviceC(DeepDeviceBase):
    """
    Class implementing DEEP protocol with wrapped C shared library
    """

    #
    # Object initialization
    #
    def __init__(self, dev, **kwargs):

        # specific internals initialization
        self._device = None

        # the base class initialization is not called automatically
        super(DeepDeviceC, self).__init__(dev, **kwargs)

        # at this point the communication channel is usable
        super(DeepDeviceC, self).initialize()

        # convert settings to C library ones
        deepdev_setparam("ICEPAPMODE", self._icepapmode, self._device)
        deepdev_setparam("USECHECKSUM", True)
        self.set_verbose(super(DeepDeviceC, self).get_verbose())


    #
    #
    def open(self):
        self._device = deepdev_open(self._hostname)


    #
    #
    def close(self):
        deepdev_close(self._device)
        self._device = None


    #
    #
    def flush(self):
        super(DeepDeviceC, self).flush()
        self.comm_dev.flush()


    #
    #
    def execcommand(self, str_cmd, bin_data):

        # send command to instrument
        answ, answ_data = deepdev_command(self._device, str_cmd, bdat=bin_data)
        if answ_data:
            # normal end for ASCII and binary answered command
            return answ, answ_data
        else:
            # normal end for only ASCII answered command
            return answ


    #
    #
    def set_debug_mode(self, dbgmode):
        super(DeepDeviceC, self).set_debug_mode(dbgmode)
        # librarary debug levels are from 0 to 6
        dbglevel = 2 if dbgmode else 1
        deepdev_setparam("DEBUGLEVEL", dbglevel, self._device)

    def set_verbose(self, val):
        super(DeepDeviceC, self).set_verbose(val)
        deepdev_setparam("DEBUGLEVEL", val, self._device)
        deepdev_setparam("DEBUGLEVEL", val)


    #
    #
    def set_timeout(self, timeout):
        super(DeepDeviceC, self).set_timeout(timeout)
        deepdev_setparam("TIMEOUT", timeout)


    #
    #
    def set_reconnect_mode(self, mode):
        super(DeepDeviceC, self).set_reconnect_mode(mode)
        newmode = string.lower(mode)
        reconnect = 0 if newmode == RECONNECT_NEVER else 1
        deepdev_setparam("RECONNECT", reconnect, self._device)
        


# --------------------------------------------------------------------------
#
class DeepDevicePy(DeepDeviceBase):
    """
    Class implementing DEEP protocol with pure Python code
    """

    #
    #
    def __init__(self, dev, **kwargs):
        """
        Object initialization
        """

        # specific internals initialization
        pass

        # the base class initialization is not called automatically
        super(DeepDevicePy, self).__init__(dev, **kwargs)

        # at this point the communication channel is usable
        super(DeepDevicePy, self).initialize()

        # TODO: is this still needed? parser object has a ref to ddevice object
        # at this point it should not be dangerous to bypass safe inter-threads
        # communication because the client has still not got the hand
        self.parser._icepapmode = self._icepapmode
        self.parser._oldmode    = self._oldmode


    #
    #
    def open(self):

        # prepare safe interthread communication links
        self.queue_cmd = queue.Queue()
        self.wake_pipe = UPipe()
        self._local    = DeepDevLocalData(self)

        # create the parser thread (this may raise exceptions...)
        self.parser    = ParserThread(self)

        # thread must a daemon one to not block application exit
        self.parser.daemon = True

        # launch the parser thread
        self.parser.start()


    #
    #
    def close(self):
        # ask to the parser thread to suicide
        # TODO: investigate why nocheck is needed here....
        self.__parser_request_action(self.parser.action_quit, nocheck=True)

        # wait for the end of the parser thread
        self.parser.join()


    #
    #
    def flush(self):
        super(DeepDevicePy, self).flush()
        self.comm_dev.flush()


    #
    #
    def execcommand(self, str_cmd, bin_data):
        # send command to instrument
        parser_cmd = self.__parser_send_cmd(str_cmd, bin_data)

        # retrieve answer
        ans = self.__parser_get_answer(parser_cmd)
        ans_data = parser_cmd.ans_data

        if ans_data:
            # normal end for ASCII and binary answered command
            return ans, ans_data
        else:
            # normal end for only ASCII answered command
            return ans

    #
    #
    def __protected_send_action(self, action):
        # A slightly complicated way of avoiding desynchronisation in case
        #    of asynchronous interruption by KeyboardInterrupt
        try:
            # put stuff in the message queue
            self.queue_cmd.put(action)
            # awake parser thread
            self.wake_pipe.write(b' ')

        except KeyboardInterrupt:
            log.trace("KeyboardInterrupt during deepdevice command queuing")
            # repeat wake up message
            # The instrument thread must be tolerant to spurious awake chars...
            self.wake_pipe.write(b' ')
            raise KeyboardInterrupt

    #
    #
    def __parser_request_action(self, action_func, nocheck=False):
        action = self._local.parser_cmd

        action.action_func = action_func
        action.event_ans.clear()
        self.__protected_send_action(action)
        if nocheck:
            return action
        else:
            return self.__parser_get_answer(action)

    #
    #
    def __parser_get_answer(self, cmd):

        # blocking call with 1sec timeout
        # TODO: handle here instrument timeout
        cmd.event_ans.wait(1)
        if cmd.event_ans.is_set() is False:
            raise RuntimeError("internal error, missing thread answer")
        ans = cmd.ans

        # check for errors in the parsing thread
        if isinstance(ans, Exception):
            raise ans

        return ans

    #
    #
    def __parser_send_cmd(self, str_cmd, in_data=None):

        cmd = self._local.parser_cmd
        cmd.action_func = self.parser.action_command
        cmd.str_cmd = str_cmd
        cmd.in_data = in_data
        cmd.event_ans.clear()
        self.__protected_send_action(cmd)
        return(cmd)


    #
    #
    # TODO: temporary added by MO, should be removed and use
    # async_xxx methods instead
    def register_command_cb(self, str_cmd, fun):
        if callable(fun):
            if str_cmd not in self._cmd_callbacks:
                self._cmd_callbacks[str_cmd] = []
            self._cmd_callbacks[str_cmd].append(fun)
        else:
            msg = 'The callback %s is not callable' % fun
            raise AssertionError(msg)

    #
    #
    def async_events_received(self):

        # send request to thread and get answer
        ans = self.__parser_request_action(self.parser.action_events_nb)
        return ans

    #
    #
    def async_events_getlast(self):

        # send request to thread and get answer
        ans = self.__parser_request_action(self.parser.action_events_getlast)
        return ans

    #
    #
    def async_status_register(self):

        # TODO: add support to DAnCE instruments
        if not self._icepapmode:
            raise RuntimeError("only IcePAP is supported for the moment")

        # inter-thread communication
        # TODO: handle multi-threaded client
        event_status = threading.Event()
        event_status.clear()
        self.event_status = event_status
        self._local.parser_cmd.event_status = event_status

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_status_reg)

    #
    #
    def async_status_unregister(self):
        # TODO: add support to DAnCE instruments
        if not self._icepapmode:
            raise RuntimeError("only IcePAP is supported for the moment")

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_status_unreg)

    #
    #
    def async_status_waitfor(self, timeout=None):
        # TODO: add support to DAnCE instruments
        if not self._icepapmode:
            raise RuntimeError("only IcePAP is supported for the moment")

        # blocking call
        self.event_status.wait(timeout)
        if self.event_status.is_set() is False:
            raise RuntimeError("timeout waiting for async status event")

    #
    #
    def async_status_register_cb(self, cb):

        # TODO: add support to DAnCE instruments
        if not self._icepapmode:
            raise RuntimeError("only IcePAP is supported for the moment")

        # inter-thread communication
        self._local.parser_cmd.event_status_cb = cb

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_status_cb_reg)

    #
    #
    def async_status_unregister_cb(self):
        # TODO: add support to DAnCE instruments
        if not self._icepapmode:
            raise RuntimeError("only IcePAP is supported for the moment")

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_status_unreg)

    #
    #
    def _async_stream_update(self):
        """
        Update internal info about current instruments streams
        """

        self._stream_ids    = []
        self._stream_states = {}
        self._stream_scopes = {}
        if self._icepapmode:
            # on IcePAP only one stream is available
            ans = self.command(DSTREAMS_ICEQCMD)
            sid = 'REPORT'
            self._stream_ids.append(sid)
            self._stream_states[sid] = ans
            self._stream_scopes[sid] = 'GLOBAL'

        elif self._oldmode:
            raise RuntimeError("not implemented yet for old DAnCE instruments")

        else:
            # get the current streams + their configuration
            # example of answer:
            # "LOG OFF GLOBAL\nASCII OFF LOCAL\nBINARY OFF LOCAL"
            ans = self.command(DSTREAMS_QCMD)
            for stream in ans.split('\n'):
                sid, state, scope = stream.split()
                self._stream_ids.append(sid)
                self._stream_states[sid] = state
                self._stream_scopes[sid] = scope

    #
    #
    def async_stream_ids(self):
        """
        Returns the ids list of all the streams currently available on the
        instrument whatever their state
        """

        # the streams can be created dynamically
        # therefore the instrument has to be interrogated each time
        self._async_stream_update()
        return self._stream_ids

    #
    #
    def async_stream_state(self, stream_id):
        """
        Returns True if the give stream is active
        """

        # the streams can be created dynamically
        # therefore the instrument has to be interrogated each time
        self._async_stream_update()

        # minimum check
        if stream_id not in self._stream_ids:
            raise ValueError("wrong stream id")

        return (self._stream_states[stream_id] == 'ON')

    #
    #
    def async_stream_active(self, stream_id, action):
        """
        Stops/starts an instrument stream
        """

        # minimum checks
        if action not in [True, False]:
            raise ValueError("wrong action")
        if stream_id not in self._stream_ids:
            raise ValueError("wrong stream id")

        # action on stream
        arg = {True: "ON", False: "OFF"}[action]

        if self.is_dance():
            cmd = "%s %s %s" % (DSTREAMS_CMD, stream_id, arg)
        elif self.is_icepap():
            cmd = "%s %s" % (DSTREAMS_ICECMD, arg)

        ans = self.ackcommand(cmd)
        if ans != "OK":
            raise RuntimeError("unable to activate stream '%s'" % stream_id)

    #
    #
    def async_receiver_register(self, datareceiver):
        """
        Register a new DataReceiver object to the parser thread
        """
        # inter-thread communication
        self._local.parser_cmd.datareceiver = datareceiver

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_receiver_reg)

    #
    #
    def async_receiver_unregister(self, datareceiver):
        """
        Unregister a DataReceiver object from parser thread
        """
        # inter-thread communication
        self._local.parser_cmd.datareceiver = datareceiver

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_receiver_unreg)

    #
    #
    def async_event_register(self, deepevent):
        """
        Register a new DeepEvent object to the parser thread
        """

        # inter-thread communication
        self._local.parser_cmd.deepevent = deepevent

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_event_reg)

    #
    #
    def async_event_unregister(self, deepevent):
        """
        Unregister a DeepEvent object from the parser thread
        """

        # inter-thread communication
        self._local.parser_cmd.deepevent = deepevent

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_event_unreg)

    #
    #
    def async_event_register_cb(self, deepevent):
        """
        Register a new DeepEvent object to the parser thread
        """

        # inter-thread communication
        self._local.parser_cmd.deepevent = deepevent

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_event_reg_cb)

    #
    #
    def async_event_unregister_cb(self, deepevent):
        """
        Unregister a DeepEvent object from the parser thread
        """

        # inter-thread communication
        self._local.parser_cmd.deepevent = deepevent

        # send request to thread and wait for confirmation
        self.__parser_request_action(self.parser.action_async_event_unreg_cb)

# --------------------------------------------------------------------------
#
class DeepDevLocalData(threading.local):
    """
    Class of thread-local data for DeepDevice.
    The first time a new thread uses an instance of it, a new
    local copy is initialised by calling __init__() with the
    initial arguments.
    """
    def __init__(self, ddevice):
        self.parser_cmd = ParserCommand(ddevice)

