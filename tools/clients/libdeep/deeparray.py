# -*- coding: utf-8 -*-
#
# this file is part of libdeep project
#

"""
Data type used for communication with any Deep or IcePAP device
"""

from __future__ import absolute_import, print_function

# Standard modules
import sys
import array
import struct


class DeepArray(array.array):
    """
    Data type definition
    """

    # class private attributes
    _stypes = [('b', 'int8'),  ('B', 'uint8'),
               ('h', 'int16'), ('H', 'uint16'),
               ('i', 'int32'), ('I', 'uint32'),
               ('l', 'int32'), ('L', 'uint32'),
               ('q', 'int64'), ('Q', 'uint64'),
               ('f', 'float32'),
               ('d', 'float64')]
    _dtypes = None

    @staticmethod
    def _update_dtypes():
        # build dtype dictionary:
        # _dtypes[dtype] = (itemsize, ctype, stype, atype)
        DeepArray._dtypes = dict()
        for stype, ctype in DeepArray._stypes:
            try:
                asize = array.array(stype).itemsize
                csize = 1 if ctype.endswith('8') else int(ctype[-2:])/8
                # for the time being just skip is asize != csize
                #  in the future do something smarter to manage stype!=atype
                if asize == csize:
                    DeepArray._dtypes[stype] = (csize, ctype, stype, stype)
                    DeepArray._dtypes[ctype] = (csize, ctype, stype, stype)
            except:  # Python2: not implemented stype code ('q', 'Q'), skip
                continue
        #for i in DeepArray._dtypes: print(i, DeepArray._dtypes[i])

    @classmethod
    def _check_dtype(cls, dtype, data=None):
        if not cls._dtypes:
            cls._update_dtypes()

        try:
            itemsize = data.itemsize
        except:
            itemsize = 1

        if dtype is None or dtype == 'a':
            try:
                dtype = data.dtype.name   # this works for numpy arrays
            except:
                dtype = 'uint{}'.format(itemsize * 8)
        if dtype not in cls._dtypes.keys():
            raise ValueError('invalid dtype \'' + dtype + '\'')
        return dtype

    def __new__(cls, data, dtype=None, order='A'):
        """
        Returns an object of type DeepArray
          dtype identifies the type:
            format charactes used by struct module ('b', 'B', ..., 'f', 'd')
            c-like identifiers ('int8','uint8','int16'...'float32','float64')
        """
        try:     # try to flatten a multidimensional numpy array
            data = data.ravel(order=order)
        except:
            pass # if not a numpy array, do nothing

        dtype = cls._check_dtype(dtype, data)

        #if isinstance(data, str):
        #    return cls.fromstring(data, dtype)

        itemsize, ctype, stype, atype = cls._dtypes[dtype]
        self = array.array.__new__(cls, atype, data)
        self.dtype = ctype
        self.format = stype
        self._update_size()
        return self

    def _update_size(self):
        self.nitems = len(self)
        self.ndim = 1
        self.shape = (self.nitems,)
        #self.itemsize is defined by the array.array class and is not writable!!
        self.size = self.nitems

    def tolist(self):
        return super(DeepArray, self).tolist()

    def tobytes(self):
        if getattr(super(DeepArray), 'tobytes', None):
            return super(DeepArray, self).tobytes()   # Python 3
        else:
            return super(DeepArray, self).tostring()  # Python 2

    def tostring(self):
        return str(self.tobytes())

    def tofile(self, file_id):
        if isinstance(file_id, str):
            with open(file_id, "wb+") as f:
                f.write(self.tobytes())
        else:
            file_id.write(self.tobytes())

    @classmethod
    def fromfile(cls, file_id, dtype=None, byteorder="little"):
        if isinstance(file_id, str):
            with open(file_id, "rb") as f:
                databytes = f.read()
        else:
            databytes = file_id.read()

        return cls.frombytes(databytes, dtype=dtype, byteorder=byteorder)

    @classmethod
    def fromstring(cls, datastr, dtype=None, byteorder="little"):
        darr = cls([], dtype=dtype)
        try:     # try Python 3
            array.array.fromstring(darr, bytes(datastr, 'latin1'))
        except:  # try Python 2
            if len(datastr) != 0:
                array.array.fromstring(darr, datastr)

        darr._update_size()

        if byteorder != sys.byteorder:
            darr.byteswap()
        return darr

        # code for future evolution
        try:
            databytes = bytes(datastr, 'ascii')
        except:
            try:
                databytes = bytearray(datastr, 'ascii')
            except:
                databytes = bytearray(datastr)

        return cls.frombytes(databytes, dtype, byteorder)

    @classmethod
    def frombytes(cls, databytes, dtype=None, byteorder="little"):
        return cls.fromstring(databytes, dtype=dtype, byteorder=byteorder)

    def byteswap(self):
        array.array.byteswap(self)

    def sum(self, dtype=None):
        """
        Returns the data checksum

        By default, the calculation is done considering using the
        native data type

        Another type can be specified
        """

        # by default return a checksum over native data type
        if(dtype is None):
            return sum(self)

        # checksum over different data type
        dtype = self._check_dtype(dtype)
        itemsize, ctype, stype, atype = self._dtypes[dtype]
        nitems = self.size
        dfrm = '<' + str(nitems) + stype
        dlist = struct.unpack_from(dfrm, self)
        return sum(dlist)
