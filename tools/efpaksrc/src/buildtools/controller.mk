# LIBDANCE_ROOT must be defined

ifndef LIBDANCE_ROOT
   $(error Missing variable LIBDANCE_ROOT. Variable is not initialised)
endif

# rashpa can be activated either by defining RASHPACTRL_ROOT or RASHPACTRL_SRC
#
ifdef RASHPACTRL_ROOT
   RASHPACTRL_SRC := $(abspath $(RASHPACTRL_ROOT)/src)
endif

ifdef RASHPACTRL_SRC
   RASHPACTRL_SRC := $(abspath $(RASHPACTRL_SRC))
   export RASHPACTRL_SRC
   H_FILES += $(wildcard $(RASHPACTRL_SRC)/*.h)
   C_FILES += $(wildcard $(RASHPACTRL_SRC)/*.c)
   RASHPA_MAKE_DIR := $(RASHPACTRL_SRC)/..
   RASHPA_LIBS = $(wildcard $(RASHPACTRL_SRC)/*.a)
endif

DANCE_DEFS_DIR ?= $(abspath $(CURDIR))
export DANCE_DEFS_DIR

include ${LIBDANCE_ROOT}/build/dancetools.mk

DANCE_SDK_ROOT ?= /segfs/linux/dance_sdk
include ${DANCE_SDK_ROOT}/build/top.mk

# application files
DANCE_DEFS_H := dance_defs.h
CONTROLLER ?= controller

C_FILES += $(CONTROLLER).c
H_FILES += $(DANCE_DEFS_H)
O_FILES := $(C_FILES:.c=.o)
OTHER_O_FILES := $(filter-out $(CONTROLLER).o, $(O_FILES))

# dependency directories
LIBDANCE_DIR := $(LIBDANCE_ROOT)/src
LIBEPCI_DIR  := $(LIBDANCE_ROOT)/libs/libepci/src
LIBEFPAK_DIR := $(LIBDANCE_ROOT)/libs/libefpak/src
LIBUIRQ_DIR  := $(LIBDANCE_ROOT)/libs/libuirq/src
LIBEBUF_DIR  := $(LIBDANCE_ROOT)/libs/libebuf/src
LIBEDMA_DIR  := $(LIBDANCE_ROOT)/libs/libedma/src
LIBESPI_DIR  := $(LIBDANCE_ROOT)/libs/libespi/src
LIBEFSPI_DIR := $(LIBDANCE_ROOT)/libs/libefspi/src
LIBEJTAG_DIR := $(LIBDANCE_ROOT)/libs/libejtag/src
LIBPMEM_DIR  := $(LIBDANCE_ROOT)/libs/libpmem/src


# tools
CC := $(DANCE_SDK_CROSS_COMPILE)gcc
CFLAGS += $(C_FLAGS) -Wundef
CFLAGS += $(DANCE_SDK_CFLAGS)
CFLAGS += -I$(DANCE_SDK_DEPS_DIR)/include
CFLAGS += -I$(LIBDANCE_DIR)
CFLAGS += -I$(LIBEPCI_DIR)
CFLAGS += -I$(LIBEFPAK_DIR)
CFLAGS += -I$(LIBUIRQ_DIR)
CFLAGS += -I$(LIBEBUF_DIR)
CFLAGS += -I$(LIBEDMA_DIR)
CFLAGS += -I$(LIBESPI_DIR)
CFLAGS += -I$(LIBEFSPI_DIR)
CFLAGS += -I$(LIBPMEM_DIR)
CFLAGS += -I$(LIBEJTAG_DIR)
ifdef RASHPACTRL_SRC
   CFLAGS += -I$(RASHPACTRL_SRC)
endif

LFLAGS += $(L_FLAGS)
LFLAGS += $(DANCE_SDK_LFLAGS)
LFLAGS += -L$(DANCE_SDK_DEPS_DIR)/lib
LFLAGS += -L$(LIBDANCE_DIR)
LFLAGS += -L$(LIBEPCI_DIR)
LFLAGS += -L$(LIBEFPAK_DIR)
LFLAGS += -L$(LIBUIRQ_DIR)
LFLAGS += -L$(LIBEBUF_DIR)
LFLAGS += -L$(LIBEDMA_DIR)
LFLAGS += -L$(LIBESPI_DIR)
LFLAGS += -L$(LIBEFSPI_DIR)
LFLAGS += -L$(LIBPMEM_DIR)
LFLAGS += -L$(LIBEJTAG_DIR)

AR := $(DANCE_SDK_CROSS_COMPILE)ar
STRIP := $(DANCE_SDK_CROSS_COMPILE)strip


# needed by the libdance headers
CFLAGS += -DWITHIN_DANCE_CONTROLLER

# cannot override or not taken into account in rule evaluation
LIBDANCE_A := $(LIBDANCE_DIR)/libdance.a

#
# rules

.PHONY: all build buildlib buildall release clean cleanall updatesvn

# default usage
usage:
	@echo "Usage: "
	@echo "    make build    : build controller"
	@echo "    make buildlib : build controller + libdance"
	@echo "    make buildall : equivalent to  cleanall + build"
	@echo "    make buildgdb : equivalent to buildall with debug option"
	@echo "    make release  : check/update SVN + buildall"
	@echo "    make clean    : remove all locally generated files"
	@echo "    make cleanall : remove all both local files and dependencies"
	@echo

build: rashpalibs $(CONTROLLER)

buildlib: cleanlib build

buildall: rashpalibs cleanall build

buildgdb: rashpalibs cleanall $(CONTROLLER)

BUILD_DEBUG := $(findstring gdb,$(MAKECMDGOALS))
ifdef BUILD_DEBUG
CFLAGS += -g -ggdb -O0
endif

release: RELEASE_CANDIDATE := *
release: rashpalibs clean libdancerelease updatesvn $(CONTROLLER)

clean:
	rm -f $(O_FILES)
	rm -f $(CONTROLLER)

cleanlib:
	rm -f $(LIBDANCE_A)

cleanall: rashpalibs clean
	$(call process_libdance, cleanall)

updatesvn:
	$(call svnupdate_to_myrecent,.)

libdancerelease:
	$(call process_libdance, release)


define process_libdance
	cd $(LIBDANCE_DIR) && $(MAKE) platform=$(DANCE_SDK_PLATFORM) $(1)
endef

ifdef RASHPA_MAKE_DIR
rashpalibs:
	cd $(RASHPA_MAKE_DIR) && $(MAKE) platform=$(DANCE_SDK_PLATFORM) $(MAKECMDGOALS)
else
rashpalibs:
endif


LDFLAGS += -ldance -lepci -lpci -lefpak -lespi -lefspi -lz -ledma -luirq -lebuf -lpmem -lejtag -ltcc -lm -ldl -lrt -lpthread


$(CONTROLLER): $(CONTROLLER).o
	$(CC) -o $@ $(O_FILES) $(RASHPA_LIBS) $(LFLAGS) $(LDFLAGS)
ifndef BUILD_DEBUG
	$(STRIP) $@
endif

$(LIBDANCE_A): $(H_FILES)
	$(call process_libdance, $(MAKECMDGOALS))

$(CONTROLLER).o: $(CONTROLLER).c $(LIBDANCE_A) $(RASHPA_LIBS) $(OTHER_O_FILES) $(H_FILES)
	@$(call create_idfile,CONTROLLER,$(CONTROLLER))
	@$(call add_rcskeyword,Platform,$(DANCE_SDK_PLATFORM))
	@$(call add_rcskeyword,Tools,`$(CC) --version | head -n1`)
	@$(show_lastrcskwfile)
ifdef RASHPACTRL_SRC
# add whatever additional keywords are required for rashpa
endif
	$(CC) $(CFLAGS) -c -o $@ $<
	$(delete_idfile)

%.o: %.c $(H_FILES)
	$(CC) $(CFLAGS) -c -o $@ $<
