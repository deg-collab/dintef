

HDL_ROOT ?= .
HDL_SRC_DIR ?= $(HDL_ROOT)/src
HDL_SYN_DIR ?= $(HDL_ROOT)/syn
HDL_CON_DIR ?= $(HDL_ROOT)/syn

BUILDTOOLS_ROOT ?= $(HDL_ROOT)/$(BUILDTOOLS_DIR)


include $(BUILDTOOLS_ROOT)/dancetools.mk

BUILDTOOLS_ROOT := $(realpath $(BUILDTOOLS_ROOT))

BUILD_LOCAL ?= $(HDL_SYN_DIR)/build_local.sh

DEP_FILES := $(HDL_ROOT)/_butils.mk
DEP_FILES += $(HDL_ROOT)/Makefile
DEP_FILES += $(BUILD_LOCAL)
DEP_FILES += $(shell find $(BUILDTOOLS_ROOT) -type f )
DEP_FILES += $(shell find $(HDL_SRC_DIR) -type f )
DEP_FILES += $(wildcard $(HDL_SYN_DIR)/*.tcl)
DEP_FILES += $(wildcard $(HDL_CON_DIR)/*.xdc)

BITSTREAM_NAME ?= top.bit

BIT_FILE := $(HDL_SYN_DIR)/$(BITSTREAM_NAME)

BITSTREAM_PACKAGE := $(HDL_ROOT)/$(BITSTREAM_NAME).pkg


.PHONY: all buildall clean cleanall release package updatesvn


# all usage
all:
	@echo "Usage: "
	@echo "    make build    : build bitfiles"
	@echo "    make buildall : equivalent to  cleanall + build"
	@echo "    make release  : check/update SVN + buildall"
	@echo "    make clean    : remove all locally generated files"
	@echo "    make cleanall : remove all both local files and dependencies"
	@echo


$(BIT_FILE): $(DEP_FILES)
	(cd $(HDL_SYN_DIR) && $(BUILDTOOLS_ROOT)/build_oar.sh)


$(BITSTREAM_PACKAGE): $(BIT_FILE)
	@$(call create_idfile,BITSTREAM,$(BITSTREAM_NAME),$(HDL_HOME))
	@$(call package_binaryfile,$(BIT_FILE),$(BITSTREAM_PACKAGE))
	@$(delete_idfile)


build: $(BITSTREAM_PACKAGE)

buildall: cleanall build

release: RELEASE_CANDIDATE := *
release: clean updatesvn package

updatesvn:
	butils $(HDL_HOME) -u

package: $(BITSTREAM_PACKAGE)


clean :
	(cd $(HDL_SYN_DIR) && $(BUILDTOOLS_ROOT)/clean_oar.sh)
	(cd $(HDL_SYN_DIR) && clean_local.sh)

cleanall: clean

