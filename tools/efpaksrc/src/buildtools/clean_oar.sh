#!/usr/bin/env bash

echo "cleaning previous syn files"
rm -rf .Xil
rm -f  viva_old.sh
rm -f  fsm_encoding.os
rm -f  *.bit
rm -f  *.mcs
rm -f  *.prm
rm -f  *.xml
rm -f  *.html
rm -f  build/*
rm -f  *.pkg

if [ -f viva.sh ]; then
    echo "killing any previous syn on OAR"
    source ~/.profile_cae
    xoar -kill `grep OARID viva.sh | cut -d':' -f2`
    rm -f  viva.sh
fi
