
# include top.mk from the same directory than apptool.mk
include $(dir $(lastword $(MAKEFILE_LIST)))/top.mk

# tools

DANCE_SDK_CC := $(DANCE_SDK_CROSS_COMPILE)gcc
DANCE_SDK_CFLAGS += $(C_FLAGS)
DANCE_SDK_LFLAGS += $(L_FLAGS)

DANCE_SDK_AR := $(DANCE_SDK_CROSS_COMPILE)ar
DANCE_SDK_STRIP := $(DANCE_SDK_CROSS_COMPILE)strip

usage: common_usage my_usage

common_usage:
	@echo
	@echo "Usage: "
	@echo "    make build    : build application"
	@echo "    make buildall : equivalent to cleanall + build"
	@echo "    make release  : check/update SVN + buildall"
	@echo "    make clean    : remove all locally generated files"
	@echo "    make cleanall : remove all both local files and dependencies"

