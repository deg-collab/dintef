#---------------------------------------------------------------------
#
DANCE_SDK_PLATFORM ?= local

#---------------------------------------------------------------------
#
DANCE_SDK_ARCH           ?= x86_64
DANCE_SDK_KERNEL_RELEASE ?= linux-`uname -r`
DANCE_SDK_KERNEL_DIR     ?= /usr/src/linux-headers-`uname -r`
DANCE_SDK_CROSS_COMPILE  ?=
DANCE_SDK_DEPS_DIR       ?= /

DANCE_SDK_CFLAGS  +=
DANCE_SDK_LDFLAGS +=
