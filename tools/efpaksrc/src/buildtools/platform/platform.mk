
# obtain the directory containing this file
DANCE_SDK_PLATFORM_ROOT := $(dir $(lastword $(MAKEFILE_LIST)))

# user must give a platform
ifeq ($(DANCE_SDK_PLATFORM),)
   $(error INVALID PLATFORM. use make platform={local | kontron_type10 | conga_imx6 | seco_imx6 | rnice})
endif

################
# The platform specific included make file has the name given by DANCE_SDK_PLATFORM and must define:
#  - The following variables:
#      DANCE_SDK_ARCH            - the string token used to identify the CPU architecture
#      DANCE_SDK_KERNEL_RELEASE  - the string token used to identify the Linux kernel release
#      DANCE_SDK_KERNEL_DIR      -
#      DANCE_SDK_CROSS_COMPILE   - any eventual cross-compiler prefix to gcc
#      DANCE_SDK_DEPS_DIR        - the directory containing the compiled external dependencies
#  - any platform specific CFLAGS or LDFLAGS in:
#      DANCE_SDK_CFLAGS
#      DANCE_SDK_LDFLAGS
#

include $(DANCE_SDK_PLATFORM_ROOT)/$(DANCE_SDK_PLATFORM).mk
