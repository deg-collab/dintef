#---------------------------------------------------------------------
#
DANCE_SDK_PLATFORM ?= rnice

#---------------------------------------------------------------------
#
DANCE_SDK_ARCH           ?= x86_64
DANCE_SDK_KERNEL_RELEASE ?= linux-`uname -r`
DANCE_SDK_KERNEL_DIR     ?= /usr/src/linux-kbuild-3.2
DANCE_SDK_CROSS_COMPILE  ?=
DANCE_SDK_DEPS_DIR       ?= $(DANCE_SDK_ROOT)/deps/x86_64-rnice

DANCE_SDK_CFLAGS  +=
DANCE_SDK_LDFLAGS +=
