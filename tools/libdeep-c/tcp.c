/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: libdeep   //  Client library to communicate with instruments
 *                        using the libdeep protocol
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/trunk/c/src/tcp.c $
 * $Rev: 324 $
 * $Date: 2018-09-28 09:33:24 +0200 (Fri, 28 Sep 2018) $
 *------------------------------------------------------------------------- */

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <netdb.h>       // gethostbyname, h_errno, etc
#include <netinet/in.h>  // IPPROTO_TCP, etc
#include <netinet/tcp.h> // TCP_NODELAY, etc

#include "deepdefs.h"


deeperror_t parse_name(devhandle_t* dev) {
   char*   auxpt;
   char    buf[MAX_NAMELENGTH + 32];

#define SEP_CHAR ':'
   // parse device name
   if ((auxpt = strchr(dev->devname, SEP_CHAR)) != NULL) {
      int port = atoi(auxpt + 1);
      if (auxpt == dev->devname || port <= 0) {
         return set_liberror(DEEPDEV_ERR, "badly formed tcp device id: %s", dev->devname);
      }
      *auxpt = 0;
      dev->devpars.tcpport = port;
   }
   auxpt = dev->devname + strlen(dev->devname);
   for (; auxpt > dev->devname && !isgraph(*auxpt); *(auxpt--) = 0);
   for (auxpt = dev->devname; !isgraph(*auxpt); auxpt++);
   sprintf(buf, "%s%c%d", auxpt, SEP_CHAR, dev->devpars.tcpport);
   if (strlen(buf) > MAX_NAMELENGTH)
      return set_liberror(DEEPDEV_ERR, "too long device name");
   strcpy(dev->cfg.tcp.hostname, auxpt);
   strcpy(dev->devname, buf);

   return DEEPDEV_OK;
}


static deeperror_t dev_init(devhandle_t* dev) {
   struct addrinfo hints, *res;
   int             status;
   char            portstr[64];

   // Parse device name, extract hostname and port
   if (parse_name(dev) != DEEPDEV_OK)
      return DEEPDEV_ERR;

   // Get device IP address
   LOG_INFO("resolving address of \"%s\"\n", dev->devname);
   memset(&hints, 0, sizeof(hints));

   hints.ai_family = AF_INET;
   hints.ai_socktype = SOCK_STREAM;
   hints.ai_protocol = IPPROTO_TCP;
   hints.ai_flags = AI_CANONNAME | AI_NUMERICSERV;
   sprintf(portstr, "%d", dev->devpars.tcpport);

   if((status = getaddrinfo(dev->cfg.tcp.hostname, portstr, &hints, &res)) != 0) {
      dev->cfg.tcp.ainfo = NULL;
      LOG_INFO("getaddrinfo(): %s\n", gai_strerror(status));
      return set_liberror(DEEPDEV_ERR, "unknown hostname: %s", dev->cfg.tcp.hostname);
   } else {
      dev->cfg.tcp.ainfo = res;
      LOG_INFO("canonic name: %s / port: %d\n", res->ai_canonname,
                       ntohs(((struct sockaddr_in*)(res->ai_addr))->sin_port));
   }

   return DEEPDEV_OK;
}


static deeperror_t dev_connect(devhandle_t* dev) {
   int              ip_status;
   int              ip_on;
   struct addrinfo *ainfo;
   const char*      cname = dev->cfg.tcp.ainfo->ai_canonname;


   struct timeval timeout;

   LOG_INFO("Opening TCP connection with %d ms timeout\n", dev->devpars.timeoutms);

   // Create a new socket
   dev->devio.fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
   if (dev->devio.fd < 0)
      return set_liberror(DEEPDEV_COMMERR, "cannot create a socket [%s]", strerror(errno));


   // Set connection timeout
   timeout.tv_sec  = dev->devpars.timeoutms / 1000;
   timeout.tv_usec = (dev->devpars.timeoutms % 1000) * 1000;
   LOG_INFO("Timeout set to %d s and %d us\n", (int)timeout.tv_sec, (int)timeout.tv_usec);

   ip_status = setsockopt(dev->devio.fd, SOL_SOCKET, SO_SNDTIMEO, (struct timeval *)&timeout, sizeof(timeout));
   if (ip_status < 0)
      return set_liberror(DEEPDEV_COMMERR, "cannot set connection timeout [%s]", strerror(errno));

   ip_status = setsockopt(dev->devio.fd, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&timeout, sizeof(timeout));
   if (ip_status < 0)
      return set_liberror(DEEPDEV_COMMERR, "cannot set connection timeout [%s]", strerror(errno));

   // and disable Nagle's Algorithm (packet merging) by setting TCP_NODELAY
   ip_on = 1;
   ip_status = setsockopt(dev->devio.fd, IPPROTO_TCP, TCP_NODELAY, (const char *)&ip_on, sizeof(int));
   if (ip_status < 0)
      return set_liberror(DEEPDEV_COMMERR, "cannot configure socket [%s]", strerror(errno));

   for (ainfo = dev->cfg.tcp.ainfo; ainfo != NULL; ainfo = ainfo->ai_next) {
      // Attempt to initiate the connection
      ip_status = connect(dev->devio.fd, ainfo->ai_addr, ainfo->ai_addrlen);

/*
LOG_INFO("ENETUNREACH %d / %d\n", ENETUNREACH, errno);
LOG_INFO("EHOSTUNREACH %d / %d\n", EHOSTUNREACH, errno);
*/
      if (ip_status < 0) {
         LOG_INFO("cannot connect to %s [%s]\n", cname, strerror(errno));
         continue;
      }

      // And set it to non-blocking
      if (set_nonblock(dev->devio.fd, 1) < 0) {
         LOG_INFO("cannot set non-blocking socket [%s]\n", strerror(errno));
         continue;
      }

      // connection successful
      return(DEEPDEV_OK);
   }

   return set_liberror(DEEPDEV_COMMERR, "cannot connect to %s [%s]", cname, strerror(errno));
}


static deeperror_t dev_disconnect(devhandle_t* dev) {
   LOG_INFO("closing connection to %s\n", dev->devname);
   shutdown(dev->devio.fd, SHUT_RDWR);
   close(dev->devio.fd);
   return(DEEPDEV_OK);
}


static ssize_t dev_read(devhandle_t* dev, void* buff, size_t size) {
   LOG_INFO("tcp read()\n");
   return default_devread(dev, buff, size);
}

static ssize_t dev_write(devhandle_t* dev, const void *buff, size_t size) {
   LOG_INFO("tcp write()\n");
   return default_devwrite(dev, buff, size);
}



static deeperror_t dev_fini(devhandle_t* dev) {
   if (dev->cfg.tcp.ainfo != NULL)
      freeaddrinfo(dev->cfg.tcp.ainfo);

   return(DEEPDEV_OK);
}


const devloop_ops_t tcp_ops = {
   .dev_init_fn       = dev_init,
   .dev_connect_fn    = dev_connect,
   .dev_disconnect_fn = dev_disconnect,
   .dev_fini_fn       = dev_fini,

   .dev_write_fn      = dev_write,
   .dev_read_fn       = dev_read,
};

