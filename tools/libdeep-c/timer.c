/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: libdeep   //  Client library to communicate with instruments
 *                        using the libdeep protocol
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/trunk/c/src/timer.c $
 * $Rev: 303 $
 * $Date: 2018-02-27 19:32:39 +0100 (Tue, 27 Feb 2018) $
 *------------------------------------------------------------------------- */

#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include "timer.h"
#include "lnklist.h"
#include "deepdefs.h"


static int get_timeval(struct timeval* tv) {
   /* use monotonic clock to retrieve time */
   /* must link with -lrt */

   /* getimeofday depends on system wide settings */
   /* (ie. ntpd ...) and may report past times, for */
   /* instance during time clock changes */

   struct timespec tp;
   const int err = clock_gettime(CLOCK_MONOTONIC, &tp);
   if (err) return err;
   tv->tv_sec = (unsigned long)tp.tv_sec;
   tv->tv_usec = tp.tv_nsec / 1000;
   return 0;
}


static inline unsigned long tv_to_ms(const struct timeval* tv) {
   return tv->tv_sec * 1000 + tv->tv_usec / 1000;
}


static inline void ms_to_tv(struct timeval* tv, unsigned int ms) {
   tv->tv_sec = ms / 1000;
   tv->tv_usec = (ms % 1000) * 1000;
}

#if 0

void timerlist_add_timer0(timerlist_t* tlist, timeritem_t* timer, unsigned int time_in_ms) {
   /* add timer in the list. list is sorted by timer ms */
   /* assume timer_item fields are initialized, only rel_ms computed here */
   struct timeval dif_tv;
   struct timeval now_tv;
   unsigned int   abs_ms;

   get_timeval(&now_tv);  /* get current time (now) */
   if (tlist->list.head != NULL) {  // if timerlist is initialised
      timersub(&now_tv, &tlist->ref_tv, &dif_tv);
      abs_ms = (unsigned int)tv_to_ms(&dif_tv) + time_in_ms;
   } else {
      tlist->ref_tv = now_tv;
      abs_ms = time_in_ms;
   }

   /* find the place to insert before */
   lnklist_item_t*  item;
   unsigned int     ms_sum = 0;

   for (item = tlist->list.head; item != NULL; item = item->next) {
      timeritem_t* timer_cur = (timeritem_t*)item->itmdata;
      if ((ms_sum + timer_cur->rel_ms) > abs_ms) break;
      ms_sum += timer_cur->rel_ms;
   }

   /* insert timer before item */
   lnklist_init_item(&timer->item, timer);
   lnklist_insert_before(&tlist->list, item, &timer->item);
   /* compute time relative to previous */
   timer->rel_ms = abs_ms - ms_sum;
   if (item != NULL) {   /* if not last timer */
      timeritem_t* next_ti = item->itmdata;
      /* recompute next relative time */
      next_ti->rel_ms -= timer->rel_ms;
   }

   /* update list reference time if added at head */
   if (&timer->item == tlist->list.head) {
      tlist->ref_tv = now_tv;
      timer->rel_ms = time_in_ms;
   }
}
#endif

static void insert_or_update_timer(timerlist_t* tlist, timeritem_t* timer, unsigned int time_in_ms, int newtimer) {
   /* add timer in the list. list is sorted by timer ms */
   /* assume timer_item fields are initialized, only rel_ms computed here */
   struct timeval dif_tv;
   struct timeval now_tv;
   unsigned int   abs_ms;

   get_timeval(&now_tv);  /* get current time (now) */
   if (tlist->list.head != NULL) {  // if timerlist is initialised
      timersub(&now_tv, &tlist->ref_tv, &dif_tv);
      abs_ms = (unsigned int)tv_to_ms(&dif_tv) + time_in_ms;
   } else {
      tlist->ref_tv = now_tv;
      abs_ms = time_in_ms;
   }

   /* find the place to insert before */
   lnklist_item_t*  item;
   unsigned int     ms_sum = 0;
   unsigned int     delta_ms;         

   for (item = tlist->list.head; item != NULL; item = item->next) {
      timeritem_t* timer_cur = (timeritem_t*)item->itmdata;
      if ((ms_sum + timer_cur->rel_ms) > abs_ms) break ;
      ms_sum += timer_cur->rel_ms;
   }
   delta_ms = abs_ms - ms_sum;

   if (newtimer) {  /* if timer was not already in the list */
      /* insert timer before item */
      lnklist_init_item(&timer->item, timer);
      lnklist_insert_before(&tlist->list, item, &timer->item);
      /* compute time relative to previous */
      timer->rel_ms = delta_ms;
   } else {  /* if timer was already in the list */
      if (item != timer->item.next) {
         /* if timer must not be replaced at the previous position in the list */
         timerlist_remove_timer(timer);
         lnklist_insert_before(&tlist->list, item, &timer->item);
         /* compute time relative to previous */
         timer->rel_ms = delta_ms;
      } else {
         /* if timer must replace itself at the same position in the list */
         /* increment time relative to previous */
         timer->rel_ms += delta_ms;
      }
   }
   if (item != NULL) {   /* if not last timer recompute next relative time */
      timeritem_t* next_ti = item->itmdata;
      next_ti->rel_ms -= delta_ms;
   }  

   /* update list reference time if added or updated at head */
   if (&timer->item == tlist->list.head) {
      tlist->ref_tv = now_tv;
      timer->rel_ms = time_in_ms;
   }
}

void timerlist_add_timer(timerlist_t* tlist, timeritem_t* timer, unsigned int time_in_ms) {
   insert_or_update_timer(tlist, timer, time_in_ms, 1);
}

void timerlist_update_timer(timerlist_t* tlist, timeritem_t* timer, unsigned int time_in_ms) {
   insert_or_update_timer(tlist, timer, time_in_ms, 0);
}


void timerlist_remove_timer(timeritem_t* timer) {
   lnklist_item_t*  item = &timer->item;

   if (item->next) {
      timeritem_t* next_ti = item->next->itmdata;
      next_ti->rel_ms += timer->rel_ms;
   }
   lnklist_unlink_item(item);
}


struct timeval* timerlist_get_next(timerlist_t* tlist) {

   timerlist_exec_expired(tlist);

   if (tlist->list.head == NULL) {
      return NULL;
   } else {
      static struct timeval next_rel_tv;
      timeritem_t*          timer = tlist->list.head->itmdata;

      ms_to_tv(&next_rel_tv, timer->rel_ms + 1);

      return &next_rel_tv;
   }
}


void timerlist_exec_expired(timerlist_t* tlist) {
   unsigned int    now_ms;
   struct timeval  now_tv;
   struct timeval  dif_tv;

   get_timeval(&now_tv);  /* get current time (now) */
   timersub(&now_tv, &tlist->ref_tv, &dif_tv);
   now_ms = (unsigned int)tv_to_ms(&dif_tv);

   while(tlist->list.head != NULL) {
      timeritem_t* timer = tlist->list.head->itmdata;

      if (timer->rel_ms <= now_ms) {
         timerfn_t exec_fn = timer->exec_fn;
         void*     exec_data = timer->exec_data;

         timerlist_remove_timer(timer);
         if (exec_fn != NULL) exec_fn(exec_data);

      } else {
         tlist->ref_tv = now_tv;
         timer->rel_ms -= now_ms;
         break;
      }
   }
}



#if 0 /* unit testing */

#include <stdio.h>
#include <errno.h>
#include <sys/select.h>

static void on_timer(void* user_data) {
   const char* const s = user_data;
   struct timeval tm;

   get_timeval(&tm);

   printf("%s(%s, %lu.%lu)\n", __FUNCTION__, s, tm.tv_sec, tm.tv_usec);
}

int main(int ac, char** av) {
   int err;
   timerlist_t tl;
   struct timeval* tv;
   lnklist_item_t* it;

   timerlist_init(&tl);
   timerlist_add_oneshot(&tl, 4000, on_timer, "oneshot_3_4000");
   timerlist_add_oneshot(&tl, 3000, on_timer, "oneshot_2_3000");
   timerlist_add_periodic(&tl, 1000, on_timer, "periodic_0_1000");
   timerlist_add_oneshot(&tl, 2000, on_timer, "oneshot_1_2000");
   timerlist_add_oneshot(&tl, 1000, on_timer, "oneshot_0_1000");
   timerlist_add_periodic(&tl, 2000, on_timer, "periodic_1_2000");

   for (it = tl.list.head; it; it = it->next) {
      timeritem_t* const ti = it->itmdata;
      printf("%s %u\n", (const char*)ti->exec_data, ti->rel_ms);
   }

   while (1) {
      tv = timerlist_get_next(&tl);

      errno = 0;
      err = select(0, NULL, NULL, NULL, tv);
      if (err == 0) {
         if ((errno != EAGAIN) && (errno != EWOULDBLOCK)) {
            /* next timeout is ready */
            timerlist_exec_next(&tl);
         }
      }
   }

   timerlist_fini(&tl);

   return 0;
}

#endif /* unit testing */
