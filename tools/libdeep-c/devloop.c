/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: libdeep   //  Client library to communicate with instruments
 *                        using the libdeep protocol
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/trunk/c/src/devloop.c $
 * $Rev: 303 $
 * $Date: 2018-02-27 19:32:39 +0100 (Tue, 27 Feb 2018) $
 *------------------------------------------------------------------------- */

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <sys/fcntl.h>
#include <unistd.h>

#include "deepdefs.h"


int set_nonblock(int fd, int set) {
   int flags;

   errno = 0;
   flags = fcntl(fd, F_GETFL);
   if (errno) return -1;

   flags = set? flags | O_NONBLOCK : flags & ~O_NONBLOCK;

   fcntl(fd, F_SETFL, flags);
   if (errno) return -1;

   return 0;
}


/*
void block_io_read(iodata_t* io) {
   __sync_or_and_fetch(&io->flags, IOFLAG_BLOCKED);
}

void unblock_io_read(iodata_t* io) {
   __sync_and_and_fetch(&io->flags, ~IOFLAG_BLOCKED);
   // wake up potentially sleeping devloop loop
   devloop_wakeup(io->devthread);
}
*/

void trigger_notifiers(lnklist_t* notifier_list, evsource_t* evsource) {
   lnklist_item_t* item;

   for (item = notifier_list->head; item != NULL; item = item->next) {
      notifier_t* notif = item->itmdata;
      if (is_valid_filedesc(notif)) {
         LOG_EVENT("Sending event via file description notification\n");
         if (write(notif->type.fd.wr_channel, &evsource, sizeof(evsource)) != sizeof(evsource)) {
            set_liberror(DEEPDEV_ERR,"write error (errno == %d)", errno);
         }
      } else if (is_valid_callback(notif)) {
         LOG_EVENT("Sending event via callback function\n");
         notif->type.cb.fn(evsource);

      } else {
         set_liberror(DEEPDEV_ERR,"internal error");
      }
   }
}



void signal_calling_thread(devthread_t* devthread, pthread_cond_t** condition_ptr) {
   pthread_cond_t* condition = *condition_ptr;

   if (condition) {
      pthread_mutex_t* mutex = &devthread->thread_mutex;

      pthread_mutex_lock(mutex);
      *condition_ptr = NULL;
      pthread_cond_signal(condition);
      pthread_mutex_unlock(mutex);
   }
}


static deeperror_t action_read(devthread_t* devthread) {
   action_t*   action = &devthread->action;
   deeperror_t retval = DEEPDEV_OK;

   while(1) {
      int              nrecv;

      errno = 0;
      nrecv = read(devthread->action_io.fd, action, sizeof(action_t));
      if (errno == EAGAIN)
         break;
      else if (nrecv != sizeof(action_t)) {
         LOG_ERROR("read error (errno == %d)\n", errno);
         break;
      }
      LOG_ACTION("Action %s() received\n", action->fname);

      if (action->result == NULL) {
         if (action->fn)
            retval = action->fn(action->arg);
         else
            retval = DEEPDEV_ERR;  // this instructs to STOP the thread

      } else {
         int waitmode = action->result->waitmode;
         pthread_cond_t** cond_pt = &action->result->cond;

         if (waitmode == WAIT_BEGIN)
            signal_calling_thread(devthread, cond_pt);

         // execute action (fn == NULL means terminate thread)
         if (action->fn) {
            if ((action->result->errcode = action->fn(action->arg)) != DEEPDEV_OK) {
               action->result->errmsg = thread_info.error_msg;
            }
         } else
            retval = DEEPDEV_ERR;  // this instructs to terminate the thread

         if (waitmode == WAIT_END)
            signal_calling_thread(devthread, cond_pt);
      }
      LOG_ACTION("Action %s() completed\n", action->fname);
      if (retval != DEEPDEV_OK)
         break;
   }
   return retval;
}


void fini_ctrl_pipe(devthread_t* devthread) {
   close(devthread->action_io.fd);
   close(devthread->action_fd);
}


static deeperror_t init_ctrl_pipe(devthread_t* devthread) {
   int fd[2];

   if (pipe(fd))
      return set_liberror(DEEPDEV_ERR, "cannot open action pipe");

   set_nonblock(fd[0], 1);
   devthread->action_fd = fd[1];
   devthread->action_io.fd  = fd[0];
   return DEEPDEV_OK;
}


void release_devhandle(devhandle_t* dev) {
   lnklist_item_t* item;

   // release queued queries and other crossed resources if any
   for (item = dev->devqueries.head; item; item = item->next) {
      devcmd_t* cmdinfo = item->itmdata;
      set_cmderror(cmdinfo, DEEPDEV_COMMERR, "answer reception aborted");
      complete_cmd(cmdinfo);
   }

   for (item = dev->devsubscrs.head; item; item = item->next) {
      devsubcrb_t* subcsrb = item->itmdata;
      release_subscriber(subcsrb);
   }

   // unlinks list item
   lnklist_unlink_item(&dev->global_it);

   if (dev->devthread)
      dev->devthread->n_handles--;
}


// -------------------------- Main devloop functions

static ssize_t default_devflush(devhandle_t* dev, size_t size) {
#define FLUSH_BUFF_SZ 50000
   char    buff[FLUSH_BUFF_SZ];
   size_t  recv_bytes = 0;
   ssize_t nbytes;
   
   while ((nbytes = read(dev->devio.fd, buff, FLUSH_BUFF_SZ)) > 0) {
      recv_bytes += nbytes;
   }
   LOG_INFO("flushed: %zd bytes before read() error\n", recv_bytes);
   
   return recv_bytes;
}

ssize_t default_devread(devhandle_t* dev, void *buff, size_t size) {
   ssize_t nchar;

   if (buff == NULL)
      return default_devflush(dev, size);
      
   DEBUGF_2("Requesting to read %zu bytes\n", size);

   nchar = read(dev->devio.fd, buff, size);

   if (nchar < 0) {
      if (errno == EAGAIN)
         nchar = 0;
      else
         set_liberror(DEEPDEV_COMMERR, "read failed: %s", strerror(errno));
   }
   LOG_INFO("received: %zd bytes\n", nchar);
   if (IS_DEBUGDUMP() && nchar > 0) char_dump(buff, nchar);

   return nchar;
}


//  main loop
//
static void* devthread_loop(void* arg) {
   devthread_t*    devthread = arg;

   fd_set          rset;
   fd_set          wset;
   int             fd_max;
   int             nfd;
   lnklist_item_t* pos;

   LOG_INFO("Starting thread %p\n", devthread);

   SET_DEVICE_THREAD();

   while (1) {
      FD_ZERO(&rset);
      FD_ZERO(&wset);

      FD_SET(devthread->action_io.fd, &rset);
      fd_max = devthread->action_io.fd;

      for (pos = devthread->handles.head; pos != NULL; pos = pos->next) {
         devhandle_t* dev = pos->itmdata;

         if (!dev->connected) continue;

         FD_SET(dev->devio.fd, &rset);

         if (dev->devio.fd > fd_max)
            fd_max = dev->devio.fd;
      }

 redo_select:
      LOG_INFO("Entering select()\n");
      errno = 0;
      nfd = select(fd_max + 1, &rset, &wset, NULL, timerlist_get_next(&devthread->timeouts));

      if (nfd < 0) {
         if (errno == EINTR) goto redo_select;  // if interrupted by a signal handler, repeat
         LOG_ERROR("Select() error (errno == %d)\n", errno);
         break;   // exit loop for thread termination

      } else if (nfd == 0) {  // timeout happened, process it
         LOG_INFO("Timed out select() return\n");
         timerlist_exec_expired(&devthread->timeouts);
         continue;
      }

      if (FD_ISSET(devthread->action_io.fd, &rset)) {
         if (action_read(devthread) != DEEPDEV_OK) {
            break; // if requested, exit loop for thread termination
         }
      }
      for (pos = devthread->handles.head; pos != NULL; pos = pos->next) {
         devhandle_t* dev = pos->itmdata;

         if (!dev->connected) continue;

         if (FD_ISSET(dev->devio.fd, &wset)) {
            device_write(dev);
            continue;
         }

         if (FD_ISSET(dev->devio.fd, &rset)) {
            device_read(dev);
            continue;
         }
      }
   }

   LOG_INFO("Terminating thread\n");
   return NULL;

}


deeperror_t devloop_init(devthread_t* devthread) {
   deeperror_t err;

   // initialise handle list
   lnklist_init(&devthread->handles, NULL);
   devthread->n_handles = 0;

   // initialise timeout timer list
   lnklist_init(&devthread->timeouts.list, NULL);

   if ((err = init_ctrl_pipe(devthread)) != DEEPDEV_OK)
      goto on_error_0;

   if (pthread_mutex_init(&devthread->thread_mutex, NULL)) {
      err = set_liberror(DEEPDEV_ERR, "cannot initialise mutex");
      goto on_error_1;
   }

   if (pthread_create(&devthread->thread, NULL, devthread_loop, devthread)) {
      err = set_liberror(DEEPDEV_ERR, "cannot create thread");
      goto on_error_2;
   }

   return DEEPDEV_OK;

 on_error_2:
   pthread_mutex_destroy(&devthread->thread_mutex);
 on_error_1:
   fini_ctrl_pipe(devthread);
 on_error_0:
   return err;
}


deeperror_t devloop_fini(devthread_t* devthread) {

   // this requires that the thread is not running

   // free resources
   pthread_mutex_destroy(&devthread->thread_mutex);
   fini_ctrl_pipe(devthread);
   return DEEPDEV_ERR;
}


