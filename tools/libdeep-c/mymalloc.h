/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: libdeep   //  Client library to communicate with instruments
 *                        using the libdeep protocol
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/branches/dev-new/c/src/devloop.h $
 * $Rev: 228 $
 * $Date: 2017-10-31 21:49:59 +0100 (Tue, 31 Oct 2017) $
 *------------------------------------------------------------------------- */

#ifndef __MYMALLOC_H_INCLUDED__
#define __MYMALLOC_H_INCLUDED__

#ifdef USE_MYMALLOC

#include <stdlib.h>

#include "liberrors.h"

static inline void* my_malloc(const char* file, unsigned int line_n, size_t size) {
   void* newptr = malloc(size);
   __logwritef("ALLOC", file, line_n, "malloc(%zd) = %p\n", size, newptr);
   return newptr;
}

static inline void* my_calloc(const char* file, unsigned int line_n, size_t n_items, size_t size) {
   void* newptr = calloc(n_items, size);
   __logwritef("ALLOC", file, line_n, "calloc(%zd, %zd) = %p\n", n_items, size, newptr);
   return newptr;
}

static inline void* my_realloc(const char* file, unsigned int line_n, void* ptr, size_t size) {
   void* newptr = realloc(ptr, size);
   __logwritef("ALLOC", file, line_n, "realloc(%p, %zd) = %p\n", ptr, size, newptr);
   return newptr;
}

static inline void my_free(const char* file, unsigned int line_n, void* ptr) {
   __logwritef("ALLOC", file, line_n, "free(%p)\n", ptr);
   free(ptr);
}

#define malloc(size)           my_malloc(__FILE__, __LINE__, size)
#define calloc(n_items, size)  my_calloc(__FILE__, __LINE__, n_items, size)
#define realloc(ptr, size)     my_realloc(__FILE__, __LINE__, ptr, size)
#define free(ptr)              my_free(__FILE__, __LINE__, ptr)

#endif  // USE_MYMALLOC

#endif   // __MYMALLOC_H_INCLUDED__
