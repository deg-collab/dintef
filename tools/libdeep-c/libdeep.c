/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: libdeep   //  Client library to communicate with instruments
 *                        using the libdeep protocol
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/trunk/c/src/libdeep.c $
 * $Rev: 381 $
 * $Date: 2020-04-12 17:04:37 +0200 (Sun, 12 Apr 2020) $
 *------------------------------------------------------------------------- */

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>       // isdigit, isspace, etc

#include "deepdefs.h"


// unused attribute
#ifdef __GNUC__
#define ATTRIBUTE_UNUSED __attribute__((unused))
#else
#define ATTRIBUTE_UNUSED
#endif


// ------------------------------------------------------
// SVN/RCS ident
// ------------------------------------------------------

const char libdeep_ident[] =
#include "_idfile.h"
;



// ------------------------------------------------------
// Parameter related definitions
// ------------------------------------------------------

#define DEEPDEV_TYPE(name) # name ,
const char* devfamily_tbl[] = {
  DEVFAMILY_LIST
};
#undef DEEPDEV_TYPE

#define DPAR_LIST \
  DPAR(DEBUG,       pars.debuglevel,          _RW | _GLOBAL | _HIDE) \
  DPAR(DEBUGLEVEL,  pars.debuglevel,          _RW | _GLOBAL) \
  DPAR(DEBUGTAGS,   pars.debugmask,           _RW | _GLOBAL) \
  DPAR(ERROROUTPUT, pars.errfd,               _RW | _GLOBAL) \
  DPAR(MAXTHREADS,  pars.maxthreads,          _RW | _GLOBAL) \
  DPAR(MAXDEVICES,  pars.maxdevices,          _RW | _GLOBAL) \
  DPAR(PORT,        pars.devpars.tcpport,     _RW | _STATIC | _HIDE) \
  DPAR(TCPPORT,     pars.devpars.tcpport,     _RW | _STATIC) \
  DPAR(USECHECKSUM, pars.devpars.usechecksum, _RW | _STATIC) \
  DPAR(DEVFAMILY,   pars.devpars.devfamily,   _RW | _STATIC) \
  DPAR(TIMEOUT,     pars.devpars.timeoutms,   _RW |           _HIDE)   \
  DPAR(TIMEOUTMS,   pars.devpars.timeoutms,   _RW)   \
  DPAR(RECONNECT,   pars.devpars.reconnect,   _RW)   \
  DPAR(HOSTNAME,    pars,                     _RO |           _HIDE)   \
  DPAR(DEVNAME,     pars,                     _RO)   \

#define DPAR(code, addr, flags) p ## code,
enum {DPAR_LIST};
#undef DPAR

#define DPAR(code, addr, flags) {p ## code, #code, &g_context.lib ## addr , flags},
//#define DPAR(code, addr, flags) {p ## code, #code, flags},
parentry_t par_list[] = { DPAR_LIST };
#undef DPAR

const int deepdev_par_items = sizeof(par_list)/sizeof(parentry_t);

struct g_context_s g_context = {
   .lib_mutex = PTHREAD_MUTEX_INITIALIZER,
   .threads   = LNKLIST_INITIALIZER,
   .devpars   = LNKLIST_INITIALIZER,
   .devcmds   = LNKLIST_INITIALIZER,
   .filedescs = LNKLIST_INITIALIZER,
   .callbacks = LNKLIST_INITIALIZER,
   .nthreads  = 0,
   .defpars   = DEF_LIB_PARS,
   .libpars   = DEF_LIB_PARS,
};

#define MAX_DEV_PER_THREAD  5


static inline void lock_library(void) { pthread_mutex_lock(&g_context.lib_mutex); }
static inline void unlock_library(void) { pthread_mutex_unlock(&g_context.lib_mutex); }
static inline void lock_thread(devthread_t* thread) { pthread_mutex_lock(&thread->thread_mutex); }
static inline void unlock_thread(devthread_t* thread) { pthread_mutex_unlock(&thread->thread_mutex); }
static inline void lock_device(devhandle_t* dev) { pthread_mutex_lock(&dev->dev_mutex); }
static inline void unlock_device(devhandle_t* dev) { pthread_mutex_unlock(&dev->dev_mutex); }


thread_local devcmd_t* thrd_cmdinfo = NULL;


void wait_for_condition(devthread_t* devthread, pthread_cond_t **condition_ptr) {
   pthread_mutex_t* mutex = &devthread->thread_mutex;
   pthread_cond_t*  condition = *condition_ptr;

   if (condition != NULL) {
      pthread_mutex_lock(mutex);

      while ((condition = *condition_ptr) != NULL) {
         pthread_cond_wait(condition, mutex);
      }
      pthread_mutex_unlock(mutex);
   }
}

#define devloop_send_action(devthread, fn, arg, wmode) \
      __devloop_send_action(devthread, (action_fn)fn, arg, wmode, #fn)

static deeperror_t __devloop_send_action(devthread_t* devthread, action_fn fn, void* arg, int waitmode, char* fname) {
   const int      fd = (int)(uintptr_t)devthread->action_fd;
   action_t       action;
   result_t       result;
   pthread_cond_t cond;

   action.fn = fn;
   action.arg = arg;
   action.fname = fname;
   if (waitmode == WAIT_NO) {
      action.result = NULL;
   } else {
      result.waitmode = waitmode;
      result.errcode = DEEPDEV_OK;
      result.cond = &cond;
      action.result = &result;

      pthread_cond_init(&cond, NULL);
   }
   LOG_ACTION("Sending action %s()  ||  waitmode = %d\n", fname, waitmode);

   if (write(fd, &action, sizeof(action_t)) != sizeof(action_t)) {
      return set_liberror(DEEPDEV_ERR,"write error (errno == %d)", errno);
   }

   // if requested, wait for condition (clearing of action.result->cond)
   if (waitmode == WAIT_NO) {
      return DEEPDEV_OK;
   } else {
      LOG_ACTION("Waiting for %s() to complete\n", fname);
      wait_for_condition(devthread, &result.cond);
      pthread_cond_destroy(&cond);
      LOG_ACTION("Signalled completion of %s()\n", fname);
      if (result.errcode != DEEPDEV_OK)
         set_liberror(result.errcode, result.errmsg);
      return result.errcode;
   }
}


static devthread_t* devthread_init(void) {
   devthread_t* devthread = NULL;

   // alloc a new devthread structure. use calloc to initialise memory to zero
   if ((devthread = calloc(1, sizeof(devthread_t))) == NULL) {
      set_liberror(DEEPDEV_ERR, "fail to allocate devthread structure");
      return(NULL);
   }
   // not need to initialise other members as memory was zeroed at allocation
   if (devloop_init(devthread) != DEEPDEV_OK) {
      free(devthread);
      return(NULL);
   }

   // include the new devthread in the global list
   lnklist_init_item(&devthread->thread_it, devthread);
   lnklist_prepend(&g_context.threads, &devthread->thread_it);

   g_context.nthreads++;

   return(devthread);
}

static void devthread_fini(devthread_t* devthread) {
   g_context.nthreads--;
   lnklist_unlink_item(&devthread->thread_it);
   devloop_fini(devthread);
   free(devthread);
}


static devthread_t* available_devthread(void) {
   devthread_t* devthread = NULL;
   lnklist_item_t* item;

   if (g_context.libpars.maxthreads == 0) {       // if unlimited # of threads
      for (item = g_context.threads.head; item != NULL; item = item->next) {
         devthread = item->itmdata;
         if (devthread->n_handles < g_context.libpars.maxdevices)
            return(devthread);
      }
   } else if (g_context.libpars.maxdevices == 0) { // if unlimited devices per thread
      if (g_context.nthreads >= g_context.libpars.maxthreads) {
         int n = 0;
         devthread = (item = g_context.threads.head)->itmdata;
         for (; n < g_context.libpars.maxthreads && item != NULL; n++, item = item->next) {
            devthread = item->itmdata;
            if (((devthread_t*)item->itmdata)->n_handles < devthread->n_handles)
               devthread = item->itmdata;
         }
         return(devthread);
      }
   } else {  // system error, this should not happen
      SYSTEM_ERROR();
      item = g_context.threads.head;
      if (item) devthread = item->itmdata;
   }
   return(NULL);
}

static devthread_t* select_devthread(void) {
   // check if one can use one of the existing threads
   if (g_context.nthreads > 0) {
      devthread_t* devthread = available_devthread();
      if (devthread != NULL)
         return devthread;
   }
   return devthread_init();
}

static void* object_calloc(size_t size, uint32_t magic, lnklist_t* list) {
   struct deepitem_s* object;

   // use calloc() to zero the structure
   LOG_INFO("magic = 0x%x\n", magic);
   if ((object = calloc(1, size)) == NULL) {
      set_liberror(DEEPDEV_ERR, "fail to allocate structure");
   } else {
      object->magic = magic;
      lnklist_init_item(&object->global_it, object);
      if (list != NULL)
         lnklist_prepend(list, &object->global_it);
   }
   return(object);
}

static void object_free(void* obj_ptr) {
   struct deepitem_s* object = obj_ptr;

   object->magic = 0;
   lnklist_unlink_item(&object->global_it);
   free(object);
}

static void free_global_list(lnklist_t* list) {
   lnklist_item_t* item;

   while ((item = list->head) != NULL)
      object_free(item->itmdata);
}


LIBDEEP_EXPORT
deeperror_t deepdev_addevsource(notifier_t* fd_cb, evsource_t* event_item, unsigned int flags) {
   lnklist_t*      notifier_list;
   lnklist_item_t* item;

   lock_library();

   if (!is_valid_filedesc(fd_cb) && !is_valid_callback(fd_cb)) {
      set_liberror(DEEPDEV_ERR, "not a valid file descriptor or callback event notifier");
      goto on_error;

   } else if (lnklist_find(&fd_cb->evsources, event_item) != NULL) {
      set_liberror(DEEPDEV_ERR, "event source already registered");
      goto on_error;

   } else if (is_valid_devcmd((devcmd_t*)event_item)) {
      notifier_list = &((devcmd_t*)event_item)->notifiers;
      goto on_good_event;
/*
   } else if(is_valid_evstream(event_item)) {
      notifier_list = (devcmd_t*)event_item->notifiers;
      goto on_good_event;
*/
   } else {
      set_liberror(DEEPDEV_ERR, "not a valid event source object");
      goto on_error;
   }

 on_good_event:
   if ((item = lnklist_add_tail(&fd_cb->evsources, event_item)) != NULL) {
      if (lnklist_add_tail(notifier_list, fd_cb) != NULL) {
         unlock_library();
         return DEEPDEV_OK;
      } else
         lnklist_free_item(item);
   }
   set_liberror(DEEPDEV_ERR, "internal error registering event source");

 on_error:
   unlock_library();
   return DEEPDEV_ERR;
}

static lnklist_t* event_notifier_list(void* evsource) {
   if (is_valid_devcmd(evsource))
      return &(((devcmd_t*)evsource)->notifiers);
/*
   else if(is_valid_evstream(evsource))
      return &(((evstream_t*)evsource)->notifiers);
*/

   return NULL;
}


static void unregister_notifiers(void* evsource) {
   lnklist_t*      notifier_list = event_notifier_list(evsource);
   lnklist_item_t* item;

   if (notifier_list == NULL) return;

   for (item = notifier_list->head; item != NULL; ) {
      lnklist_item_t* next = item->next;
      notifier_t*     notif = item->itmdata;

      lnklist_free_item(lnklist_find(&notif->evsources, evsource));
      lnklist_free_item(item);
      item = next;
   }
}

static void unregister_evsources(notifier_t* notif) {
   lnklist_item_t* item;

   for (item = notif->evsources.head; item != NULL; ) {
      lnklist_item_t* next = item->next;
      void*           evsource = item->itmdata;
      lnklist_t*      notifier_list = event_notifier_list(evsource);

      lnklist_free_item(lnklist_find(notifier_list, notif));
      lnklist_free_item(item);
      item = next;
   }
}

LIBDEEP_EXPORT
evsource_t* deepdev_evsource(notifier_t* deepfd) {
   if (!is_valid_filedesc(deepfd)) {
      set_liberror(DEEPDEV_ERR, "not a valid file descriptor notifier");
      return NULL;
   }

   int nrecv;
   evsource_t* eventsrc = NULL;

   errno = 0;
   nrecv = read(deepfd->type.fd.rd_channel, &eventsrc, sizeof(eventsrc));
   if (errno == EAGAIN || nrecv != sizeof(eventsrc)) {
      set_liberror(DEEPDEV_ERR, "read error (errno == %d)", errno);
      return NULL;
   }
   if (is_valid_devcmd((devcmd_t*)eventsrc))
      LOG_INFO("Command answer received\n");
   else {
      set_liberror(DEEPDEV_ERR, "corrupted data");
      return NULL;
   }
   return eventsrc;
}

LIBDEEP_EXPORT
deeperror_t deepdev_evinfo(evsource_t* evsource, devhandle_t** dev,
                                               devcmd_t** cmdinfo, unsigned int* flags) {
   TODO // review this function
   return set_liberror(DEEPDEV_ERR, "function not yet implemented");
}

//-------------------------------------------------------------------
//
//
static parentry_t *parse_parameter(char *name) {
   int i;

   DEBUGF_2("parse_parameter(\"%s\")\n", name);

   // Valid parameter?
   for(i = 0; i < deepdev_par_items; i++) {
      if(!strcasecmp(par_list[i].name, name))
         return(&par_list[i]);
   }
   set_liberror(DEEPDEV_ERR, "unknown parameter '%s'", name);
   return(NULL);
}


static devpars_t* parobject_check(void* parobject) {
   if (parobject == DEFAULT_DEVPARAMS)
      return &g_context.defpars.devpars;
   else if (parobject == CURRENT_DEVPARAMS)
      return &g_context.libpars.devpars;
   else if (is_valid_handle(parobject))
      return &((devhandle_t*)parobject)->devpars;
   else if (is_valid_devpartbl(parobject))
      return &((devpartbl_t*)parobject)->pars;
   else {
      set_liberror(DEEPDEV_ERR, "invalid parameter source");
      return NULL;
   }
}

LIBDEEP_EXPORT
devpartbl_t* deepdev_partblinit(deepitem_t parobject) {
   devpartbl_t* devpars;
   devpars_t*   pars = parobject_check(parobject);

   if (pars == NULL)
      return NULL;

   // use calloc() to zero the structure
   devpars = object_calloc(sizeof(devpartbl_t), DEEP_DEVPAR_MAGIC, &g_context.devpars);
   if (devpars != NULL) {
      devpars->pars = *pars;
   }
   return devpars;
}


static deeperror_t param_table_ptr(void* parobject, parentry_t* par, int write,
                                   libpars_t** libpars, devpars_t** devpars) {
   *libpars = NULL;
   *devpars = NULL;

   if (parobject == DEFAULT_DEVPARAMS) {
      if (par->flags & _RO) goto on_notdevice_error;
      if (write) goto on_default_error;
      *libpars = &g_context.defpars;
      *devpars = &g_context.defpars.devpars;

   } else if (parobject == CURRENT_DEVPARAMS) {
      if (par->flags & _RO) goto on_notdevice_error;
      *libpars = &g_context.libpars;
      *devpars = &g_context.libpars.devpars;

   } else if (is_valid_handle(parobject)) {
      if (par->flags & _GLOBAL) goto on_global_error;
      if (par->flags & _STATIC && write) goto on_static_error;
      *devpars = &((devhandle_t*)parobject)->devpars;

   } else if (is_valid_devpartbl(parobject)) {
      if (par->flags & _RO) goto on_notdevice_error;
      if (par->flags & _GLOBAL) goto on_global_error;
      *devpars = &((devpartbl_t*)parobject)->pars;

   } else {
      return set_liberror(DEEPDEV_ERR, "invalid device parameter source");
   }
   return DEEPDEV_OK;

 on_notdevice_error:
   return set_liberror(DEEPDEV_ERR, "%s is not accessible for a not initialised device", par->name);

 on_default_error:
   return set_liberror(DEEPDEV_ERR, "cannot change default parameters");

 on_static_error:
   return set_liberror(DEEPDEV_ERR, "%s cannot be changed in a initialised device", par->name);

 on_global_error:
   return set_liberror(DEEPDEV_ERR, "%s is not a device specific parameter", par->name);
}

static deeperror_t string_to_devfamily(const char* strval, int* familyidx) {
   int idx;

   for (idx = 0; idx < N_DEEPDEVTYPES; idx++) {
      if (strcmp(strval, devfamily_tbl[idx]) == 0) {
         *familyidx = idx;
         return DEEPDEV_OK;
      }
   }
   return DEEPDEV_ERR;
}

LIBDEEP_EXPORT
deeperror_t deepdev_setparam(void *parobject, char *name, deeppar_t valptr) {
   libpars_t*  libpars;
   devpars_t*  devpars;
   int         intval = DDPARtoINT(valptr);
   const char* strval = DDPARtoSTR(valptr);
   FILE*       outerr = DDPARtoFILE(valptr);

#define LOG_LENGTH 64
#define LOG_NEWSTRVALUE(val)  sprintf(str_value, "%.*s", LOG_LENGTH, val)
#define LOG_NEWINTVALUE(val)  sprintf(str_value, "%d", val)
#define LOG_NEWFILEVALUE(val) sprintf(str_value, "%d", val)
   char        str_value[LOG_LENGTH];

   parentry_t* par = parse_parameter(name);

   if (!par)
      return(DEEPDEV_ERR);
   else if (par->flags & _RO)
      return(set_liberror(DEEPDEV_ERR, "read only parameter"));

   param_table_ptr(parobject, par, 1, &libpars, &devpars);

   if (devpars == NULL)
      return(DEEPDEV_ERR);

   switch(par->code) {
      case pDEBUG:
      case pDEBUGLEVEL:
         if (!libpars)
            goto on_bad_global;
         else if ((intval < 0) || (intval > MAX_DEBUG_LEVEL))
            return(set_liberror(DEEPDEV_ERR, "invalid debug level value"));
         else
            libpars->debuglevel = intval;
            LOG_NEWINTVALUE(intval);
         break;

      case pDEBUGTAGS:
         if (!libpars)
            goto on_bad_global;
         else if (string_to_logmask(strval, &libpars->debugmask) != DEEPDEV_OK)
            return(set_liberror(DEEPDEV_ERR, "invalid debug tags: %s", strval));
         else
            LOG_NEWSTRVALUE(strval);
         break;

      case pERROROUTPUT:
         if (!libpars)
            goto on_bad_global;
         else {
            libpars->errfd = outerr;
            LOG_NEWFILEVALUE(intval);
         }
         break;

      case pMAXDEVICES:
      case pMAXTHREADS:
         if (!libpars)
            goto on_bad_global;
         else if (intval < 0)
            return(set_liberror(DEEPDEV_ERR, "invalid parameter value"));
         else if (par->code == pMAXDEVICES) {
            libpars->maxdevices = intval;
            if (!intval && !libpars->maxthreads) libpars->maxthreads = 1;
            if (intval && libpars->maxthreads)   libpars->maxthreads = 0;
         } else { // pMAXTHREADS
            libpars->maxthreads = intval;
            if (!intval && !libpars->maxdevices) libpars->maxdevices = 1;
            if (intval && libpars->maxdevices)   libpars->maxdevices = 0;
         }
         LOG_NEWINTVALUE(intval);
         break;

      case pTIMEOUT:
      case pTIMEOUTMS:
         if (intval <= 0)
            return(set_liberror(DEEPDEV_ERR, "invalid timeout value"));
         else
            devpars->timeoutms = intval;
         LOG_NEWINTVALUE(intval);
         break;

      case pPORT:
      case pTCPPORT:
         if (intval <= 0)
            return(set_liberror(DEEPDEV_ERR, "invalid tcp port"));
         else
            devpars->tcpport = intval;
         LOG_NEWINTVALUE(intval);
         break;

      case pRECONNECT:
         if (intval < 0)
            return(set_liberror(DEEPDEV_ERR, "invalid reconnect value"));
         else
            devpars->reconnect = (intval != 0);
         LOG_NEWINTVALUE(intval);
         break;

      case pUSECHECKSUM:
         devpars->usechecksum = (intval != 0);
         LOG_NEWINTVALUE(intval);
         break;

      case pDEVFAMILY:
         if (string_to_devfamily(strval, &devpars->devfamily) != DEEPDEV_OK)
            return(set_liberror(DEEPDEV_ERR, "invalid device family selection string"));
         else
            LOG_NEWSTRVALUE(strval);
         break;

      default:
         return(set_liberror(DEEPDEV_ERR, "parameter not implemented"));
   }
   // Normal end
   DEBUGF_3("setting \"%s\" parameter to %s\n", par->name, str_value);
   return(DEEPDEV_OK);

 on_bad_global:
   // Trying to change a global
   return(set_liberror(DEEPDEV_ERR, "not a device specific parameter"));
}

// constant definitions
const int truepar  = 1;
const int falsepar = 0;

LIBDEEP_EXPORT
deeperror_t deepdev_getparam(void *parobject, char *name, deeppar_t *valptr) {
   libpars_t*  libpars;
   devpars_t*  devpars;

   parentry_t* par = parse_parameter(name);

   if (!par)
      return(DEEPDEV_ERR);

   param_table_ptr(parobject, par, 0, &libpars, &devpars);

   if (devpars == NULL)
      return(DEEPDEV_ERR);

   switch(par->code) {
      case pDEBUG:
      case pDEBUGLEVEL:
         *valptr = DDPAR(libpars->debuglevel);
         break;

      case pDEBUGTAGS:
         *valptr = (deeppar_t) logmask_as_string(libpars->debugmask) ; // Cannot use DDPAR() here
         break;

      case pERROROUTPUT:
         *valptr = DDPAR(libpars->errfd);
         break;

      case pMAXDEVICES:
         *valptr = DDPAR(libpars->maxdevices);
         break;

      case pMAXTHREADS:
         *valptr = DDPAR(libpars->maxthreads);
         break;

      case pHOSTNAME:
      case pDEVNAME:
         if (!is_valid_handle(parobject))
            return(set_liberror(DEEPDEV_ERR, "not a valid device handle"));
         else {
            devhandle_t* dev = parobject;
            *valptr = (deeppar_t) dev->devname; // Cannot use DDPAR() here
         }
         break;

      case pPORT:
      case pTCPPORT:
         *valptr = DDPAR(devpars->tcpport);
         break;

      case pTIMEOUT:
      case pTIMEOUTMS:
         *valptr = DDPAR(devpars->timeoutms);
         break;

      case pRECONNECT:
         *valptr = DDPAR(devpars->reconnect);
         break;

      case pUSECHECKSUM:
         *valptr = DDPAR(devpars->usechecksum);
         break;

#define DDPARCOND(cond) ((cond)? ((deeppar_t) &truepar):((deeppar_t) &falsepar))
      case pDEVFAMILY:
         *valptr = (deeppar_t) devfamily_tbl[devpars->devfamily] ; // Cannot use DDPAR() here
         break;

      default:
         return(set_liberror(DEEPDEV_ERR, "parameter not implemented"));
   }
   // Normal end
   return(DEEPDEV_OK);
}



#define CHAN_TYPE(type, name, ops_ptr) {type, name, ops_ptr},
const chtype_t chan_types[] = {
   CHAN_TYPE_LIST
};


static devhandle_t* devhandle_init(char* devid, deepitem_t parobject) {
   devhandle_t* dev;
   char*        devname;
   int          devtype;
   devpars_t*   devpars = parobject_check(parobject);

   if (devpars == NULL)
      goto on_handle_error;

   if ((dev = object_calloc(sizeof(devhandle_t), DEEP_HANDLE_MAGIC, NULL)) == NULL) {
      goto on_handle_error;
   }
   // initialize members in deephandle
   dev->devpars = *devpars;
   dev->connected = 0;

   if ((devname = strstr(devid, ID_SEPARATOR)) == NULL) {
      devtype = CHAN_TCP;
      devname = devid;
   } else {
      int len = (devname - devid);
      devname += sizeof(ID_SEPARATOR) - 1;

      for (devtype = 0; devtype < N_CHANTYPES; devtype++) {
         const chtype_t* chtype = &chan_types[devtype];
         if (strlen(chtype->chname) == len && strncmp(chtype->chname, devid, len) == 0) {
            LOG_INFO("%s device: %s\n", chtype->chname, devname);
            break;
         }
      }
      if (devtype >= N_CHANTYPES) {
         set_liberror(DEEPDEV_ERR, "bad device identifier");
         goto on_mutex_error;
      }
   }
   dev->chan = &chan_types[devtype];
   dev->devio.ops = dev->chan->dev_ops;

   if (strlen(devname) > MAX_NAMELENGTH) {
      set_liberror(DEEPDEV_ERR, "too long device name");
      goto on_mutex_error;
   }
   strcpy(dev->devname, devname);

   switch(dev->devpars.devfamily) {
      case DEEPDEV_LEGACY:
         dev->answnoprfx = 1;
         if (dev->chan->type == CHAN_PIPE) { // Hitachi GPIB
            dev->binformat = BINARY_HGPIB;
            dev->binhdrsz = 0;
         } else {                            // Hitachi Serial
            dev->binformat = BINARY_HSER;
            dev->binhdrsz = LEGACYHDR_SZ;
         }
         break;

      case DEEPDEV_ICEPAP:
         dev->answnoprfx = 0;
         dev->binformat = BINARY_ICEPAP;
         dev->binhdrsz = BINARYHDR_SZ;
         break;

      default: // DEEPDEV_PREDANCE, DEEPDEV_DANCE
         if (dev->devpars.devfamily == DEEPDEV_PREDANCE)
            dev->answnoprfx = 1;
         else     //   DEEPDEV_DANCE
            dev->answnoprfx = 0;
         dev->binformat = BINARY_DANCE;
         dev->binhdrsz = BINARYHDR_SZ;
         break;
   }

   lnklist_init(&dev->devqueries, dev);
   lnklist_init(&dev->devsubscrs, dev);

   reader_init(&dev->devreader);

   if (pthread_mutex_init(&dev->dev_mutex, NULL))
      goto on_mutex_error;

   if (dev->chan->dev_ops->dev_init_fn(dev) != DEEPDEV_OK)
      goto on_init_error;

   return(dev);

 on_init_error:
   pthread_mutex_destroy(&dev->dev_mutex);
 on_mutex_error:
   object_free(dev);
 on_handle_error:
   return(BAD_HANDLE);
}


static void devhandle_fini(devhandle_t* dev) {
   dev->chan->dev_ops->dev_fini_fn(dev);
   pthread_mutex_destroy(&dev->dev_mutex);
   reader_fini(&dev->devreader);
   object_free(dev);
}


#define LOG_DO_ACTION()  LOG_ACTION("Execute action %s()\n", __func__);

// This function is called from the device thread
static deeperror_t do_add_handle(devhandle_t* dev) {
   devthread_t* devthread = dev->devthread;

   LOG_DO_ACTION();

   lnklist_prepend(&devthread->handles, &dev->global_it);
   devthread->n_handles++;
   return DEEPDEV_OK;
}


static deeperror_t do_unregister_handle(devhandle_t* dev) {
   LOG_DO_ACTION();
   release_devhandle(dev);
   return DEEPDEV_OK;
}

static devhandle_t* register_devhandle(devhandle_t* dev) {
   lock_library();

   if ((dev->devthread = select_devthread()) == NULL) {
      dev = BAD_HANDLE;
      goto on_done;
   }
   // add global_it to thread list (use action mechanism)
   devloop_send_action(dev->devthread, do_add_handle, dev, WAIT_NO);

 on_done:
   unlock_library();
   return(dev);
}





// This function is called from the device thread
static deeperror_t do_connect_device(devhandle_t* dev) {

   LOG_DO_ACTION();

   if (!dev->connected) {
      deeperror_t retvalue = dev->chan->dev_ops->dev_connect_fn(dev);

      if (retvalue == DEEPDEV_OK)
         dev->connected = 1;

      return retvalue;

   } else
      return DEEPDEV_OK;
}


static deeperror_t connect_device(devhandle_t* dev, int waitmode) {
   LOG_INFO("connect_device() -- wait mode=%d\n", waitmode);
   return devloop_send_action(dev->devthread, do_connect_device, dev, waitmode);
}


// This function is called from the device thread
static deeperror_t do_disconnect_device(devhandle_t* dev) {

   LOG_DO_ACTION();

   if (dev->connected) {
      dev->chan->dev_ops->dev_disconnect_fn(dev);
      dev->connected = 0;
   }
   return DEEPDEV_OK;
}


static deeperror_t disconnect_device(devhandle_t* dev, int waitmode) {
   return devloop_send_action(dev->devthread, do_disconnect_device, dev, waitmode);
}


static void cleanup_empty_thread(devthread_t* devthread) {
   // Stop thread
   // an action with NULL function instructs the thread to suicide...
   devloop_send_action(devthread, NULL, NULL, WAIT_END);
   // free all other thread specific resources
   devthread_fini(devthread);
}


static deeperror_t close_device(devhandle_t* dev) {
   devthread_t* devthread = dev->devthread;


   disconnect_device(dev, WAIT_END);
   // remove global_it from thread list (use action mechanism)
   devloop_send_action(dev->devthread, do_unregister_handle, dev, WAIT_END);

   devhandle_fini(dev);

   if (devthread->n_handles == 0) {
      cleanup_empty_thread(devthread);
   }

   return DEEPDEV_OK;
}


static void kill_thread(devthread_t* devthread) {
   lnklist_item_t* item;

   // close all devices
   // the thread stops and resources are released with the last close_device()
   while ((item = devthread->handles.head) != NULL) {
      devhandle_t* dev = item->itmdata;
      close_device(dev);
   }
}


static void kill_allthreads(void) {
   devthread_t*    devthread;
   lnklist_item_t* item;

   while ((item = g_context.threads.head) != NULL) {

      devthread = item->itmdata;
      kill_thread(devthread);
   }
}

//-------------------------------------------------------------------


LIBDEEP_EXPORT
devhandle_t* deepdev_devinit(char* devid, deepitem_t parobject) {
   devhandle_t* dev;

   DEBUGF_2("deepdev_devinit(\"%s\")\n", devid);

   if ((dev = devhandle_init(devid, parobject)) == BAD_HANDLE)
      return BAD_HANDLE;

   if (!register_devhandle(dev)) {
      devhandle_fini(dev);
      return BAD_HANDLE;
   }
   return dev;
}


LIBDEEP_EXPORT
devhandle_t* deepdev_open(char *devid) {
   devhandle_t* dev;

   DEBUGF_2("deepdev_open(\"%s\")\n", devid);

   if ((dev = deepdev_devinit(devid, CURRENT_DEVPARAMS)) != BAD_HANDLE) {
      deeperror_t err = connect_device(dev, WAIT_END);
      if (err != DEEPDEV_OK) {
         deepdev_close(dev);
         reset_liberror(err);
         return BAD_HANDLE;
      }
   }
   return dev;
}

static deeperror_t test_answer(devhandle_t* dev, const char* query, const char* good_answ) {
   const char* dev_answ;

   if ((deepdev_query(dev, query, &dev_answ) != DEEPDEV_OK) ||
        strcasecmp(good_answ, dev_answ) != 0)
      return DEEPDEV_ERR;

   return DEEPDEV_OK;
}

LIBDEEP_EXPORT
devhandle_t* deepdev_search(const char *devid_fmt, int* devid_index, int devid_n,
                            const char* appname,  const char* devaddr, void* parobject) {
   devhandle_t* dev;
   int          idx = *devid_index;
   int          last_idx = *devid_index + devid_n - 1;

   DEBUGF_2("deepdev_search(\"%s\")\n", devid_fmt);

   if (parobject_check(parobject) == NULL)
      return BAD_HANDLE;

   if (strstr(devid_fmt, "%d") == NULL) {
      set_liberror(DEEPDEV_ERR, "bad device id format string \"%s\" ", devid_fmt);
      return BAD_HANDLE;
   }

   for(; idx <= last_idx; idx++) {
#define MAX_DEV_ID_LEN 1024
      char devid[MAX_DEV_ID_LEN];
      int  nchar = snprintf(devid, MAX_DEV_ID_LEN, devid_fmt, idx);

      if (nchar < 0 || (nchar == 0 && devid_n > 1)) {
         set_liberror(DEEPDEV_ERR, "bad device id format string \"%s\" ", devid_fmt);
         return BAD_HANDLE;
      }
      LOG_INFO("Trying to connect on %s\n", devid);
      dev = deepdev_open(devid);
      if (dev == BAD_HANDLE)
         continue;

      if ((appname && test_answer(dev, "?APPNAME", appname) != DEEPDEV_OK) ||
          (devaddr && test_answer(dev, "?ADDR",    devaddr) != DEEPDEV_OK)) {
         deepdev_close(dev);
      } else {
         *devid_index = idx;
         return dev;
      }
   }
   set_liberror(DEEPDEV_COMMERR, "%s device \"%s\" (%d to %d) not found",
                                 appname? appname : "deep", devaddr? devaddr : "<any>",
                                 *devid_index, last_idx);
   return BAD_HANDLE;
}



#define check_device_handle(dev) do { \
   if (!is_valid_handle(dev))         \
      return set_liberror(DEEPDEV_ERR, "bad device handle"); \
} while(0)

#define check_command_info(cmdinfo) do { \
   if (!is_valid_devcmd(cmdinfo))        \
      return set_liberror(DEEPDEV_ERR, "bad command handle"); \
} while(0)


LIBDEEP_EXPORT
deeperror_t deepdev_connect(devhandle_t* dev) {
   deeperror_t err;

   DEBUGF_2("deepdev_connect()\n");

   check_device_handle(dev);

   lock_device(dev);
   err = connect_device(dev, WAIT_END);
   unlock_device(dev);
   return err;
}

LIBDEEP_EXPORT
deeperror_t deepdev_disconnect(devhandle_t* dev) {
   DEBUGF_2("deepdev_disconnect()\n");

   check_device_handle(dev);

   disconnect_device(dev, WAIT_END);
   return DEEPDEV_OK;
}


LIBDEEP_EXPORT
deeperror_t deepdev_close(devhandle_t* dev) {

   DEBUGF_2("deepdev_close()\n");

   check_device_handle(dev);

   lock_library();
   close_device(dev);
   unlock_library();

   return DEEPDEV_OK;
}


LIBDEEP_EXPORT
deeperror_t deepdev_closeall(void) {
   DEBUGF_2("deepdev_closeall()\n");

   lock_library();

   // stop all threads and release associated resources
   kill_allthreads();
   // unlink and free various structures
   free_global_list(&g_context.devpars);   // devpars structures
   free_global_list(&g_context.devcmds);   // devcmd structures
   free_global_list(&g_context.filedescs); // filedesc structures
   free_global_list(&g_context.callbacks); // callback structures

   unlock_library();
   DEBUGF_2("deepdev_closeall() done\n");

   return DEEPDEV_OK;
}



ssize_t default_devwrite(devhandle_t* dev, const void *buff, size_t size) {
   size_t  remain = size;
   ssize_t nchar;

   DEBUGF_2("sending %zu bytes\n", remain);
   if (IS_DEBUGDUMP()) char_dump(buff, remain);

   do {
      nchar = write(dev->devio.fd, buff, remain);
      LOG_INFO("sent: %zd bytes to %s\n", nchar, dev->devname);

      if (nchar < 0) {
         if (errno != EAGAIN && errno != EINTR) {
            char *syserrmsg = strerror(errno);
            set_liberror(DEEPDEV_COMMERR, "write failed: %s", syserrmsg);
            return -1;

         } else {
            // we are here because the io is non-blocking and the output buffer is full
            // use then select to wait for the io to become 'writable' again
            int    nfd;
            fd_set wset;
            struct timeval tmout;
            tmout.tv_sec = 3;    // 3s timeout
            tmout.tv_usec = 0;

            FD_ZERO(&wset);
            FD_SET(dev->devio.fd, &wset);

         redo_select:
            nfd = select(dev->devio.fd + 1, NULL, &wset, NULL, &tmout);
            if (nfd < 0) {
               if (errno == EINTR) goto redo_select;
               char *syserrmsg = strerror(errno);
               set_liberror(DEEPDEV_COMMERR, "data send failed: %s", syserrmsg);
               LOG_ERROR("select error (errno == %d)\n", errno);
               return -1;
            } else if (nfd == 0) {
               // a select timeout happened
               set_liberror(DEEPDEV_COMMERR, "data send failed with timeout");
               LOG_ERROR("select timeout\n");
               return -1;
            }
         }
      } else {
         remain -= nchar;
         buff += nchar;
      }
   } while(remain);

   return size;
}


static deeperror_t do_send_command(devcmd_t* cmdinfo) {
   devhandle_t*   dev = cmdinfo->dev;
   dev_write_fn_t write_fn = dev->chan->dev_ops->dev_write_fn;

   LOG_DO_ACTION();

   if (write_fn(dev, cmdinfo->cmdbuf, cmdinfo->cmdlen) < 0) {
      goto on_write_error;
   }

LOG_TRACE();
   if (IS_BINARY_COMMAND(cmdinfo->flags)) {
LOG_TRACE();
      if (dev->binformat == BINARY_HGPIB) {
         // this case should not happen (no GPIB binary commands!, only queries...)
         if (write_fn(dev, cmdinfo->bdat->databuf, cmdinfo->bdat_nbytes) < 0)
            goto on_write_error;

      } else {
LOG_TRACE();
         if (write_fn(dev, &cmdinfo->binhdr, dev->binhdrsz) < 0)
            goto on_write_error;
         if (write_fn(dev, cmdinfo->bdat->databuf, cmdinfo->bdat_nbytes) < 0)
            goto on_write_error;
         if (dev->binformat == BINARY_HSER) {
            if (write_fn(dev, &cmdinfo->binhdr.legacyheader[3], 2) < 0 )
               goto on_write_error;
         }
      }
   }

   if (RETURNS_ANSWER(cmdinfo->flags)) {
      lnklist_init_item(&cmdinfo->cmdansw_it, cmdinfo);
      lnklist_append(&cmdinfo->dev->devqueries, &cmdinfo->cmdansw_it);

      set_cmdtimer(dev->devthread, cmdinfo);
   }
   return DEEPDEV_OK;

 on_write_error:
   return set_liberror(DEEPDEV_COMMERR, NULL);
}


static const char *check_deepstring_conformity(const char* str, int *length) {
   char c;
   int  len;

   if (str == NULL || *str == 0) {
      *length = 0;
      return NULL;
   }
   if (*length == 0)
      *length = strlen(str);

   for (len = *length; len--;) {
      c = *str++;
      if (!isalnum(c) && c != '_')
         return NULL;
   }
   return str;
}


static void* cmdbuf_alloc(devcmd_t* cmdinfo, size_t size) {
   if (cmdinfo->cmdbuf && cmdinfo->cmdbuf != cmdinfo->cmdbuf_stat)
      free(cmdinfo->cmdbuf);

   if (size > CMDBUFF_SZ)
      cmdinfo->cmdbuf = malloc(size);
   else if (size > 0)
      cmdinfo->cmdbuf = cmdinfo->cmdbuf_stat;
   else
      cmdinfo->cmdbuf = NULL;

   return cmdinfo->cmdbuf;
}

#define cmdbuf_free(cmdinfo) cmdbuf_alloc(cmdinfo, 0)


//-------------------------------------------------------------------
//
// Command Syntax: [#][<address>:]<keyword> [param1 [param2] etc][\r]\n
// Keyword Syntax: [?|*|?*]<string>
//
static deeperror_t build_cmd(devcmd_t* cmdinfo, const char *addr, const char *cmd, int ack) {
   int         flags = 0;
   const char* nextchar = cmd;
   const char* endprfx;
   char*       auxptr;
   int         cmdlen;
   int         prfxlen;
   int         addrlen;
   int         auxlen;
   char        c;

   if (!*nextchar)
      return set_liberror(DEEPDEV_ERR, "missing command");

   // Remove any leading space and check/remove acknowledge character
   while(isspace(*nextchar)) nextchar++;
   // Check/remove acknowledge character
   if (*nextchar == '#') {
      nextchar++;
      ack = 1;
   }

   // Check/remove a broadcast character
   if (*nextchar == ':') {
      nextchar++;
      flags |= DEEPDEV_FLG_BCAST;
   }

   // reset cmd pointer here
   cmd = nextchar;
   // obtain command length removing trailing dummy characters
   cmdlen = strlen(cmd);
   while(cmdlen && (isspace(c = *(nextchar + cmdlen - 1)) || iscntrl(c)))
      cmdlen--;

   // obtain prefix length, it must include device address if any
   if ((endprfx = strchr(cmd, ' ')) == NULL) {
      prfxlen = cmdlen;
      endprfx = cmd + cmdlen;
   } else
      prfxlen = endprfx - cmd;

   // extract address from cmd string if not passed as argument
   if (!addr || *addr == 0) {
      if ((auxptr = strchr(cmd, ':')) && (auxptr - cmd) < prfxlen && auxptr != cmd) {
         addr = cmd;
         addrlen = auxptr - cmd;
         nextchar = auxptr + 1;
         if (check_deepstring_conformity(addr, &addrlen) == NULL || addrlen == 0)
            return set_liberror(DEEPDEV_ERR, "invalid device address \'%.*s\'", addrlen, addr);
      } else {
         addrlen = 0;
         addr = NULL;
      }
   } else {
      addrlen = 0;
      if (check_deepstring_conformity(addr, &addrlen) == NULL)
         return set_liberror(DEEPDEV_ERR, "invalid device address \'%.*s\'", addrlen, addr);
      prfxlen += addrlen + 1;
   }

   // check cmd keyword
   if (*nextchar == '#') { ack = 1; nextchar++; prfxlen--;}
   cmdlen -= (nextchar - cmd);   // reset cmd pointer and lengths
   cmd = nextchar;
   if(*nextchar == '?') { flags |= DEEPDEV_FLG_QUERY; nextchar++; }
   if(*nextchar == '*') { flags |= DEEPDEV_FLG_BIN;   nextchar++; }

   auxlen = endprfx - nextchar;
//LOG_INFO("nextxchar (%d): <%s>\n", auxlen, nextchar);

   if (check_deepstring_conformity(nextchar, &auxlen) == NULL)
      return set_liberror(DEEPDEV_ERR, "invalid command keyword \'%.*s\'", auxlen, nextchar);

   // check broadcast inconsistencies
   if (flags & DEEPDEV_FLG_BCAST) {
      if (ack)
         return set_liberror(DEEPDEV_ERR, "cannot acknowledge broadcast commands");
      if (addr)
         return set_liberror(DEEPDEV_ERR, "cannot broadcast and address simultaneously");
      if (flags & DEEPDEV_FLG_QUERY)
         return set_liberror(DEEPDEV_ERR, "cannot broadcast device queries");
   }

   if (cmdbuf_alloc(cmdinfo, 4 + addrlen + cmdlen) == NULL)
      return set_liberror(DEEPDEV_ERR, "fail to allocate command buffer");

   auxptr = cmdinfo->cmdbuf;
   if (ack) {
      if (!(flags & DEEPDEV_FLG_QUERY)) {
         flags |= DEEPDEV_FLG_ACK;
         *(auxptr++) = '#';
      }
   } else if (flags & DEEPDEV_FLG_BCAST)
      *auxptr++ = ':';
   cmdinfo->prfx = auxptr;
   cmdinfo->prfxlen = prfxlen;
   if (addr)
      auxptr += sprintf(auxptr, "%.*s:%.*s\n", addrlen, addr, cmdlen, cmd);
   else
      auxptr += sprintf(auxptr, "%.*s\n", cmdlen, cmd);

   cmdinfo->cmdlen = auxptr - cmdinfo->cmdbuf;

//   DEBUGF_3("command line: %.*s", cmdinfo->cmdlen, cmdinfo->cmdbuf);
   DEBUGF_3("command: %.*s[%.*s]%s",
            (int)(cmdinfo->prfx - cmdinfo->cmdbuf), cmdinfo->cmdbuf,
            (int)cmdinfo->prfxlen, cmdinfo->prfx,
            cmdinfo->prfx + cmdinfo->prfxlen);

   DEBUGF_3("command type  : %s%s%s%s%s\n",    \
      (flags & DEEPDEV_FLG_ADDR ?"ADDR " :""), \
      (flags & DEEPDEV_FLG_BCAST?"BCAST "  :""), \
      (flags & DEEPDEV_FLG_ACK  ?"ACK "  :""), \
      (flags & DEEPDEV_FLG_QUERY?"QUERY ":""), \
      (flags & DEEPDEV_FLG_BIN  ?"BIN "  :""));

   // update flags in cmdinfo
   cmdinfo->flags = flags;
   return DEEPDEV_OK;
}


uint32_t calc_checksum(uint32_t nvalues, int type, void *buff) {
   register uint32_t checksum = 0;

   switch(type) {
      case BIN_8: {
            uint8_t *val = buff;
            while(nvalues--)
               checksum += *val++;
         }
         break;

      case BIN_16: {
            uint16_t *val = buff;
            while(nvalues--) {
//LOG_INFO("val=%d:0x%04x, chksum=%d\n", *val, *val, checksum);
               checksum += *val++;
            }
         }
         break;

      case BIN_32: {
            uint32_t *val = buff;
            while(nvalues--)
               checksum += *val++;
         }
         break;

      case BIN_64: {
            uint64_t *val = buff;
            while(nvalues--)
               checksum += *val++;
         }
         break;
   }
   return(checksum);
}


static int prepare_binary_data(devcmd_t* cmdinfo) {
   devhandle_t*   dev = cmdinfo->dev;
   deepbindata_t *dbin = cmdinfo->bdat;
   int         skip_checksum;
   int         datatype;
   int         datasize;
   uint32_t    signature;
   uint32_t    checksum;

   DEBUGF_2("prepare_binary_data()\n");

   if (dbin == NULL)
      return set_liberror(DEEPDEV_ERR, "Missing valid binary block pointer");

//LOG_INFO("dbin->datasize: %d\n", dbin->datasize);
//LOG_INFO("dbin->datatype: %d\n", dbin->datatype);
   cmdinfo->bdat_nbytes = dbin->datasize * dbin->datatype;
   switch(dev->binformat) {
      case BINARY_HGPIB:
      case BINARY_HSER:
         datatype = BIN_8;
         datasize = cmdinfo->bdat_nbytes;
         skip_checksum = 0;
         if (datasize > 0xFFFF) {
            return set_liberror(DEEPDEV_ERR, "Binary data block too big");
         }
         break;

      case BINARY_ICEPAP:
         DEBUGF_2("  - Using IcePAP mode\n");
         if (cmdinfo->bdat_nbytes % 2)
            cmdinfo->bdat_nbytes++;
         datatype = BIN_16;
         datasize = cmdinfo->bdat_nbytes / BIN_16;

         skip_checksum = 0;
         signature = ICEPAP_SIGNATURE;
         break;

      default: // BINARY_DANCE:
         DEBUGF_2("  - Using DAnCE mode\n");
         datatype = dbin->datatype;
         datasize = dbin->datasize;

         skip_checksum = 1;
         if (__BYTE_ORDER == __LITTLE_ENDIAN)
            signature = DEEP_SIGNATURE | DEEP_NOCHECKSUM;
         else
            signature = DEEP_SIGNATURE | DEEP_BIGENDIAN | DEEP_NOCHECKSUM;
         signature |= (uint8_t)datatype;
         break;
   }

   checksum = skip_checksum ?  0 :
                 calc_checksum(datasize, datatype, dbin->databuf);

   DEBUGF_2("  - Datatype: %d byte(s)/value\n", datatype);
   DEBUGF_2("  - Datasize: %d values, %d bytes\n", datasize, cmdinfo->bdat_nbytes);
   DEBUGF_2("  - Checksum field: 0x%08X - skip check: %s\n",
                              checksum, skip_checksum? "YES":"NO");

   if (dev->binhdrsz == LEGACYHDR_SZ) {
      // binary header
      cmdinfo->binhdr.legacyheader[0] = 0xFF;
      cmdinfo->binhdr.legacyheader[1] = (datasize >> 8) && 0xFF;
      cmdinfo->binhdr.legacyheader[2] = datasize && 0xFF;
      // binary trailer
      cmdinfo->binhdr.legacyheader[3] = checksum && 0xFF;
      cmdinfo->binhdr.legacyheader[4] = '\r';

   } else if (dev->binhdrsz == BINARYHDR_SZ) {
      cmdinfo->binhdr.binheader.signature = TO_DEEP_ENDIAN(signature);
      cmdinfo->binhdr.binheader.size      = TO_DEEP_ENDIAN(datasize);
      cmdinfo->binhdr.binheader.checksum  = TO_DEEP_ENDIAN(checksum);
   }
   return DEEPDEV_OK;
}


static void devcmd_zero(devcmd_t* cmdinfo) {
   memset(cmdinfo, 0, sizeof(devcmd_t));
   cmdinfo->magic = DEEP_CMDDEV_MAGIC;
   lnklist_init_item(&cmdinfo->global_it, cmdinfo);
}


static deeperror_t devcmd_softreset(devcmd_t* cmdinfo, devhandle_t* dev, const char **answ, deepbindata_t *dbin) {
   if (!is_valid_devcmd(cmdinfo))
      return(DEEPDEV_ERR);

   cmdbuf_free(cmdinfo);
   cmdinfo->dev = dev;

   if (cmdinfo->answ_buf && cmdinfo->answ_buf != cmdinfo->answ_buf_stat)
      free(cmdinfo->answ_buf);

   cmdinfo->answ_buf = NULL;
   cmdinfo->answ = answ;
   cmdinfo->bdat = dbin;
   cmdinfo->result.errcode = DEEPDEV_OK;
   cmdinfo->result.errmsg = NULL;
   return(DEEPDEV_OK);
}

static deeperror_t devcmd_fullreset(devcmd_t* cmdinfo) {
   devcmd_softreset(cmdinfo, NULL, NULL, NULL);
   // as devcmd_softreset() does not free the internal binary buffer
   if (cmdinfo->bdat_buff != NULL)
      free(cmdinfo->bdat_buff);
   unregister_notifiers(cmdinfo);

   return(DEEPDEV_OK);
}

static int devcmd_is_used(devcmd_t* cmdinfo) {
   return (cmdinfo->cmdbuf != NULL);
}

static deeperror_t devcmd_mustnotbeinuse(devcmd_t* cmdinfo) {
   check_command_info(cmdinfo);

   if (devcmd_is_used(cmdinfo))
      return set_liberror(DEEPDEV_ERR, "previous command pending completion");
   else
      return DEEPDEV_OK;
}


LIBDEEP_EXPORT
deeperror_t deepdev_freethread(void) {
   if (thrd_cmdinfo == NULL)
      return DEEPDEV_OK;
   else {
      deeperror_t err = deepdev_free(thrd_cmdinfo); // deepdev_free() does all tests
      if (err == DEEPDEV_OK)
         thrd_cmdinfo = NULL;  // clear only if no error
      return err;
   }
}


LIBDEEP_EXPORT
deeperror_t deepdev_parsecommand(int *flags, const char *addr, const char *cmd, int ack) {
   devcmd_t    cmdinfo;  // not actually used for commands,
                         //   no need to use thrd_cmdinfo here
   deeperror_t err;

   LOG_INFO("deepdev_parsecommand()\n");

   devcmd_zero(&cmdinfo);
   if ((err = build_cmd(&cmdinfo, addr, cmd, ack)) != DEEPDEV_OK)
      *flags = 0;
   else
      *flags = cmdinfo.flags;

   // cleanup and free memory as needed
   devcmd_softreset(&cmdinfo, NULL, NULL, NULL);
   return(err);
}


static deeperror_t check_connection(devhandle_t* dev) {
   deeperror_t err = DEEPDEV_OK;

   if (!dev->connected) {
      lock_device(dev);
      if (!dev->connected) {  // in case state changed in the mean time
         LOG_INFO("device %s disconnected\n", dev->devname);
         if (dev->devpars.reconnect) {
            LOG_INFO("trying to reconnect %s\n", dev->devname);
            err = connect_device(dev, WAIT_END);
         }
      }
      unlock_device(dev);
   }
   return err;
}


//-------------------------------------------------------------------
//  if no error, should return either DEEPDEV_OK or DEEPDEV_NOTREADY,
//
static deeperror_t check_devcmd(devcmd_t* cmdinfo, int wait) {
   const char** answ = cmdinfo->answ;

   check_command_info(cmdinfo);

   if (RETURNS_ANSWER(cmdinfo->flags)) {
      if (wait) {
         wait_for_condition(cmdinfo->dev->devthread, &cmdinfo->result.cond);
      }

      if (cmdinfo->result.cond)
         return DEEPDEV_NOTREADY;

//      DEBUGF_1("devreader.statbuffer = %p\n", cmdinfo->dev->devreader.statbuffer);
      DEBUGF_5("answ=%p, %p <%s>\n", answ, answ? *answ : NULL, answ? *answ : "NULL");
      if (cmdinfo->bdat) {
         deepbindata_t* bdat = cmdinfo->bdat;
         DEBUGF_5("data buffer: %p [%zd of %zd], type %d\n", bdat->databuf, bdat->datasize,
                                                             bdat->bufsize, bdat->datatype);
      }
      if (cmdinfo->result.errcode != DEEPDEV_OK)
         set_liberror(cmdinfo->result.errcode, cmdinfo->result.errmsg);

   } else if (cmdinfo->answ)
      *cmdinfo->answ = NULL;

   cmdbuf_free(cmdinfo);   // free cmdbuf to flag in cmdinfo that command was finished
   return cmdinfo->result.errcode;
}


LIBDEEP_EXPORT
deeperror_t deepdev_checkcommand(deepitem_t evsource, int wait) {

   if (is_valid_devcmd(evsource)) {
      return check_devcmd(evsource, wait);
/*
   } else if (is_valid_evstream(evsource)) {
      return check_evstream(evsource, wait);
*/
   } else {
      return set_liberror(DEEPDEV_ERR, "not a valid event source object");
   }
}


LIBDEEP_EXPORT
deeperror_t deepdev_startcommand(devcmd_t* cmdinfo,
                               devhandle_t* dev, const char *addr, const char *cmd,
                               int ack, const char **answ, deepbindata_t *dbin) {
   deeperror_t err;

   LOG_INFO("deepdev_startcommand()\n");

   // Minimum checks
   check_device_handle(dev);
   if (devcmd_mustnotbeinuse(cmdinfo) != DEEPDEV_OK)
      return DEEPDEV_ERR;

   if ((err = check_connection(dev)) != DEEPDEV_OK)
      return err;

   // initialise and eventually free previously allocated memory
   devcmd_softreset(cmdinfo, dev, answ, dbin);

   // prepare and build the command line in cmdinfo internal buffer
   if ((err = build_cmd(cmdinfo, addr, cmd, ack)) != DEEPDEV_OK)
      return err;

   if (IS_QUERY(cmdinfo->flags) && !answ) {
      err = set_liberror(DEEPDEV_ERR, "missing pointer to device answer");
      goto on_error;

   } else if (IS_BINARY_COMMAND(cmdinfo->flags) &&
              (err = prepare_binary_data(cmdinfo)) != DEEPDEV_OK) {
      goto on_error;
   }

   if (answ) *answ = NULL;

   if (RETURNS_ANSWER(cmdinfo->flags)) {
      pthread_cond_init(&cmdinfo->cmd_cond, NULL);
      cmdinfo->result.cond = &cmdinfo->cmd_cond;
   }

   // update timeout values
   cmdinfo->wtimeout = cmdinfo->rtimeout = dev->devpars.timeoutms;
   // send command to device (using action mechanism)
   err = devloop_send_action(cmdinfo->dev->devthread, do_send_command, cmdinfo, WAIT_END);
   if (err != DEEPDEV_OK)
      goto on_error;

   // check command completion but do not wait...
   return check_devcmd(cmdinfo, 0);

on_error:    // if error, free cmdbuf to flag in cmdinfo that command is done
   cmdbuf_free(cmdinfo);
   return err;
}


LIBDEEP_EXPORT
deeperror_t deepdev_command(devhandle_t* dev, const char *addr, const char *cmd,
                                int ack, const char **answ, deepbindata_t *dbin) {
   deeperror_t  errcode;

   if (thrd_cmdinfo == NULL) {     // only for initialisation, thrd_cmdinfo is checked
      thrd_cmdinfo = deepdev_cmdinit(); //   and softreset in deepdev_startcommand()
   }

   errcode = deepdev_startcommand(thrd_cmdinfo, dev, addr, cmd, ack, answ, dbin);
   if (errcode == DEEPDEV_NOTREADY)
      errcode = check_devcmd(thrd_cmdinfo, 1);  // check and wait for command completion

   // thrd_cmdinfo will be cleared either in next call,
   //   or by deepdev_closethread() or deepdev_closeall()

   return(errcode);
}


LIBDEEP_EXPORT
devcmd_t* deepdev_cmdinit(void) {
   devcmd_t* cmdinfo;

   cmdinfo = object_calloc(sizeof(devcmd_t), DEEP_CMDDEV_MAGIC, &g_context.devcmds);
   return(cmdinfo);
}

static void fini_event_pipe(notifier_t* evfd) {
   close(evfd->type.fd.rd_channel);
   close(evfd->type.fd.wr_channel);
}


static deeperror_t init_event_pipe(notifier_t* evfd) {
   int fd[2];

   if (pipe(fd))
      return set_liberror(DEEPDEV_ERR, "cannot initialise event file descriptor");

   set_nonblock(fd[0], 1);
   evfd->type.fd.wr_channel = fd[1];
   evfd->type.fd.rd_channel = fd[0];
   return DEEPDEV_OK;
}


LIBDEEP_EXPORT
notifier_t* deepdev_fdinit(int *fd) {
   notifier_t* evfd;

   evfd = object_calloc(sizeof(notifier_t), DEEP_FILEDESC_MAGIC, &g_context.filedescs);

   if (evfd == NULL || init_event_pipe(evfd) != DEEPDEV_OK) {
      set_liberror(DEEPDEV_ERR, "cannot initialise event file descriptor");
      return NULL;
   } else {
      *fd = evfd->type.fd.rd_channel;
      return evfd;
   }
}


LIBDEEP_EXPORT
notifier_t* deepdev_cbinit(void (*fn)(evsource_t*)) {
   notifier_t* evcb;

   // use calloc() to zero the structure
   if (fn == NULL) {
      set_liberror(DEEPDEV_ERR, "bad callback function");
      return NULL;
   } else {
      evcb = object_calloc(sizeof(notifier_t), DEEP_CALLBACK_MAGIC, &g_context.callbacks);
      if (evcb != NULL)
         evcb->type.cb.fn = fn;
      return evcb;
   }
}

LIBDEEP_EXPORT
deeperror_t deepdev_free(deepitem_t object) {

   LOG_INFO("deepdev_free(): %p\n", object);

   if (object == NULL) {
      show_all_internals();
      return DEEPDEV_ERR;
   }

   switch (((struct deepitem_s*)object)->magic) {
      case DEEP_HANDLE_MAGIC:
         // device handles are freed in close_device()
         return close_device(object);

      case DEEP_DEVPAR_MAGIC:
         break;

      case DEEP_CMDDEV_MAGIC:
         if (devcmd_mustnotbeinuse(object) != DEEPDEV_OK)
            return DEEPDEV_ERR;
         lock_library();
         devcmd_fullreset(object);
         unlock_library();

      case DEEP_FILEDESC_MAGIC:
         fini_event_pipe(object);

      case DEEP_CALLBACK_MAGIC:
      {
         notifier_t* notifier = object;
         unregister_evsources(notifier);
         break;
      }
      default:
         return set_liberror(DEEPDEV_ERR, "not a valid libdeep object");
   }
   object_free(object);
   return DEEPDEV_OK;
}
