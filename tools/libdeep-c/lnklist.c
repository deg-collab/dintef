/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project:  lnklist.c,lnklist.h   // library to manage linked lists
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/trunk/c/src/lnklist.c $
 * $Rev: 285 $
 * $Date: 2018-02-20 00:38:14 +0100 (Tue, 20 Feb 2018) $
 *------------------------------------------------------------------------- */

#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include "lnklist.h"

#include "mymalloc.h"


void lnklist_init(lnklist_t* li, void* lstdata) {
   li->head = NULL;
   li->tail = NULL;
   li->lstdata = lstdata;
}


lnklist_t* lnklist_alloc(void* lstdata) {
   lnklist_t* li;

   li = malloc(sizeof(lnklist_t));
   if (li != NULL) lnklist_init(li, lstdata);
   return(li);
}


void lnklist_init_item(lnklist_item_t* item, void* itmdata) {
   item->next = NULL;
   item->prev = NULL;
   item->list = NULL;
   item->itmdata = itmdata;
}


void lnklist_unlink_item(lnklist_item_t* item)
{
   lnklist_t* list = item->list;

   if (list == NULL)
      return;

   if (item->prev == NULL)
      list->head = item->next;
   else
      item->prev->next = item->next;

   if (item->next == NULL)
      list->tail = item->prev;
   else
      item->next->prev = item->prev;

   item->next = NULL;
   item->prev = NULL;
   item->list = NULL;
}


lnklist_item_t* lnklist_alloc_item(void* itmdata) {
   lnklist_item_t* const item = malloc(sizeof(lnklist_item_t));

   if (item == NULL)
      return NULL;
   else {
      lnklist_init_item(item, itmdata);
      return item;
   }
}


void lnklist_free_item(lnklist_item_t* item) {
   lnklist_unlink_item(item);
   free(item);
}


lnklist_item_t* lnklist_add_head(lnklist_t* list, void* itmdata) {
   lnklist_item_t* const item = lnklist_alloc_item(itmdata);

   if (item == NULL) return NULL;
   item->list = list;

   if (list->head == NULL) {
      list->head = item;
      list->tail = item;
   } else {
      item->next = list->head;
      list->head->prev = item;
      list->head = item;
   }
   return item;
}


lnklist_item_t* lnklist_append(lnklist_t* list, lnklist_item_t* item) {
   item->list = list;

   if (list->tail == NULL) {
      list->head = item;
      list->tail = item;
   } else {
      item->prev = list->tail;
      list->tail->next = item;
      list->tail = item;
   }
   return item;
}


lnklist_item_t* lnklist_prepend(lnklist_t* list, lnklist_item_t* item) {
   item->list = list;

   if (list->head == NULL) {
      list->head = item;
      list->tail = item;
   } else {
      item->next = list->head;
      list->head->prev = item;
      list->head = item;
   }
   return item;
}


lnklist_item_t* lnklist_add_tail(lnklist_t* list, void* itmdata)
{
   lnklist_item_t* const item = lnklist_alloc_item(itmdata);

   if (item == NULL)
      return NULL;
   else
      return lnklist_append(list, item);
}


lnklist_item_t* lnklist_insert_before(lnklist_t* li, lnklist_item_t* next, lnklist_item_t* it) {
   /* insert a node item before next */
   /* if item is already in a list, displace it */
   /* if next is NULL, the node is added at tail */

   if (it->list) lnklist_unlink_item(it);

   it->list = li;

   if (next == NULL)
      lnklist_append(li, it);
   else {
      it->next = next;
      it->prev = next->prev;
      if (next->prev != NULL)
         next->prev->next = it;
      else
         li->head = it;
      next->prev = it;
   }
   return it;
}

lnklist_item_t* lnklist_add_before(lnklist_t* li, lnklist_item_t* next, void* data) {
   /* add a new node before next */
   /* if next is NULL, the node is added at head */

   lnklist_item_t* const it = lnklist_alloc_item(data);

   if (it == NULL) return NULL;

   return lnklist_insert_before(li, next, it);
}


lnklist_item_t* lnklist_find(lnklist_t* list, void* data) {
   lnklist_item_t* item;

   for (item = list->head; item != NULL; item = item->next) {
      if (item->itmdata == data)
         return item;
   }
   return NULL;
}


lnklist_for_t lnklist_foreach(lnklist_t* list, lnklist_fn_t fn, void* data) {
   lnklist_item_t* item;

   for (item = list->head; item != NULL; ) {
      lnklist_item_t* const next = item->next;
      if (fn(item, data) != LNKLIST_CONT)
         return LNKLIST_STOP;
      else
         item = next;
   }
   return LNKLIST_CONT;
}


static lnklist_for_t fini_item(lnklist_item_t* item, void* p) {
   lnklist_fn_t fn =   ((void**)p)[0];
   void* const  data = ((void**)p)[1];

   if (fn != NULL)
      fn(item, data);

   lnklist_free_item(item);

   return LNKLIST_CONT;
}


void lnklist_fini(lnklist_t* list, lnklist_fn_t fn, void* data) {
   void* p[] = {fn, data};
   lnklist_foreach(list, fini_item, p);
}

