/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project:  lnklist.c,lnklist.h   // library to manage linked lists
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/trunk/c/src/lnklist.h $
 * $Rev: 285 $
 * $Date: 2018-02-20 00:38:14 +0100 (Tue, 20 Feb 2018) $
 *------------------------------------------------------------------------- */

#ifndef __LNKLIST_H_INCLUDED__
#define __LNKLIST_H_INCLUDED__


struct lnklist_s;

typedef struct lnklist_item_s
{
   struct lnklist_item_s* next;
   struct lnklist_item_s* prev;
   struct lnklist_s*      list;
   void*                  itmdata;
} lnklist_item_t;


typedef struct lnklist_s
{
   lnklist_item_t* head;
   lnklist_item_t* tail;
   void*           lstdata;
} lnklist_t;


/* used for linked list static initialization */
#define LNKLIST_INITIALIZER {NULL, NULL, NULL}

typedef enum {
   LNKLIST_CONT = 0,
   LNKLIST_STOP = 1,
} lnklist_for_t;

typedef lnklist_for_t (*lnklist_fn_t)(lnklist_item_t*, void*);

lnklist_item_t* lnklist_alloc_item(void* itmdata);
void            lnklist_init_item(lnklist_item_t*, void* itmdata);
void            lnklist_unlink_item(lnklist_item_t* item);
void            lnklist_free_item(lnklist_item_t* item);

lnklist_t*      lnklist_alloc(void* lstdata);
void            lnklist_init(lnklist_t*, void* lstdata);
void            lnklist_fini(lnklist_t*, lnklist_fn_t, void*);
lnklist_item_t* lnklist_add_head(lnklist_t*, void* itmdata);
lnklist_item_t* lnklist_append(lnklist_t* list, lnklist_item_t* item);
lnklist_item_t* lnklist_prepend(lnklist_t* list, lnklist_item_t* item);
lnklist_item_t* lnklist_add_tail(lnklist_t*, void* itmdata);
lnklist_item_t* lnklist_add_before(lnklist_t*, lnklist_item_t*, void*);
lnklist_item_t* lnklist_insert_before(lnklist_t*, lnklist_item_t*, lnklist_item_t*);
lnklist_item_t* lnklist_find(lnklist_t* list, void* data);
lnklist_for_t   lnklist_foreach(lnklist_t*, lnklist_fn_t, void*);


#endif // __LNKLIST_H_INCLUDED__
