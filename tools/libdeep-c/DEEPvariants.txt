DEEP Protocol variants
=====================

Summary:
--------

Instrument type  Prefix   Binary format
--------------- -------- -----------------------------------------------
Hitachi Serial     No     BINARY_HSER (3-byte header + 1-byte trailer)
Hitachi GPIB       No     BINARY_HGPIB (no header)
IcePAP            Yes     BINARY_IPAP (12-byte icepap header) + Dchunks
PreDAnCE           No     BINARY_DANCE (12-byte dance header)
DAnCE             Yes     BINARY_DANCE (12-byte dance header) + Dchunks
------------------------------------------------------------------------



LEGACY (HITACHI H8300): MOCO, OPIOM, MUSST, ...
-----------------------------------------------
  Plain ASCII messages:
    - command messages terminated by '\r'. '\n' characters are ignored.
    - returned query answers are terminated by "\r\n"
    - no prefix in answers.
    - Skip char '>' forwards commands to the next device in the chain
  Binary (serial line): (byte stream of length 'len' < 64KiB in bytes)
    # 3 byte header: 0xFF  <len_HI_byte> <len_LO_byte>
    # <len> bytes
    # 1 byte pseudo checksum: <LRC> % 255
       with LRC = <len_HI_byte> + <len_LO_byte> + sum(all data bytes)
    # (optional, send 1 additional '\r' for resynchro
  Binary (GPIB): (byte stream of length 'len' < 64KiB in bytes)
    - No binary header/trailer (it is the same when using the isgdevice DS)

  Current implementations:
  - isgdevice.mac
      isgdevice_bincomm() (e.g. OPIOM)
        -binary command implemented for:
           - serial SPEC native
           - serial TACO/TANGO Device Servers
           - isgserial TACO Device Server
           - GPIB: no
      isgdevice_getdata() (used only by musst_getdata(), MUSST)
        - binary command implemented for:
          - GPIB only, no binary header


IcePAP:
-------
  - ASCII answers include prefix
  - binary header 3x32bits and itemsize always 16 bits:
    - 32-bit signature: 0xa5aa555a
    - 32-bit size: number of 16-bit words
    - 32-bit checksum: sum of all data values (as 16-bit words)
  - only one async stream, dframe signature: 0xbebecafe


Pre-DANCE (BCDU8):
------------------
- No prefix in ASCII commands (CHECK!!)
- otherwise same binary header than DANCE (deepdevice.mac)


DANCE
-----
  - ASCII messages only terminated by '\n'
  - binary headers 3x32bits:
     - signature: 0xa5a50000 | checksum_flag | bigendian_flag | itemsize
        bits:   description:
        31-16   0xa5a5
        15-6    unused??
        5       bigendian_flag
        4       checksum_flag
        3       itemsize uint64
        2       itemsize uint32
        1       itemsize uint16
        0       itemsize uint8
     - size:  number of items of size defined by itemsize
     - checksum: 32-bit sum data items (summed based on their size)
  - multiple async streams
  - async dframe signature: 0xcabecafe



