/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: libdeep   //  Client library to communicate with instruments
 *                        using the libdeep protocol
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/branches/dev-new/c/src/libdeep.c $
 * $Rev: 227 $
 * $Date: 2017-10-31 10:27:38 +0100 (Tue, 31 Oct 2017) $
 *------------------------------------------------------------------------- */

#include <stdint.h>
#include <string.h>
#include <ctype.h>       // isdigit, isspace, etc
#include <stdarg.h>      // va_arg, va_start, va_end, ...
#include <inttypes.h>

#include "deepdefs.h"


thread_local struct thread_info_s thread_info = {.is_device_thread = 0};

#define LOG_MASK_ALL 0xFFFF
#define DDL_CLASS(__class, __msg) {#__class, LOG_MASK_ ## __class, __msg},
struct ddl_struct_s {
   char*    name;
   uint32_t bitmask;
   char*    msg;
} ddl_info[] = {
   DEEPDEV_LOG_CLASSES
   {"ALL", LOG_MASK_ALL, ""}
};
#undef DDL_CLASS


deeperror_t string_to_logmask(const char* strmask, uint32_t* logmask) {
   uint32_t lmask = 0;

   LOG_INFO("logmask: <%s>\n", strmask);
   while(1) {
      int len;
      int i;

      while(isspace(*strmask)) strmask++;

      if (*strmask == 0) {
         *logmask = lmask;
         LOG_INFO("setting debug mask: %#04x\n", lmask);
         return DEEPDEV_OK;
      }
      len = strlen(strmask);

      for (i = 0; i <= DEEPDEV_LOG_ALL; i++) {
         int masklen = strlen(ddl_info[i].name);
         if ((masklen == len || (masklen < len && isspace(strmask[masklen]))) &&
                       strncasecmp(strmask, ddl_info[i].name, masklen) == 0) {
            lmask |= ddl_info[i].bitmask;
            strmask += masklen;
            break;
         }
      }
      if (i > DEEPDEV_LOG_ALL) return DEEPDEV_ERR;
   }
}

const char* logmask_as_string(uint32_t logmask) {
   static char strmask[64];
   int         len;
   int         i;

   if (logmask == LOG_MASK_ALL) return "ALL";

   for (len = 0, i = 0; i < DEEPDEV_LOG_ALL; i++) {
      if (logmask & (1 << i))
         len += sprintf(strmask + len, "%s ", ddl_info[i].name);
   }
   strmask[len - 1] = 0;
   LOG_INFO("current debug mask: %#04x  <%s>\n", logmask, strmask);
   return strmask;
}




static inline FILE* log_file_descriptor(void) {
   FILE* errfd = g_context.libpars.errfd;
   return errfd? errfd : stderr;
}

static inline int __log_head(FILE* logfd, const char* logclass, const char* file, unsigned int line_n) {
   if (thread_info.is_device_thread) fprintf(logfd, "%6s", "");
   return fprintf(logfd, "[%6s] %10s @ %4d : ", logclass, file, line_n);
}

int __logwritef(const char* logclass, const char* file, unsigned int line_n, const char* fmt, ...) {
   FILE*   logfd = log_file_descriptor();
   int     len;
   va_list arg_ptr;

   len = logclass == NULL? 0 : __log_head(logfd, logclass, file, line_n);

   va_start(arg_ptr, fmt);
   len += vfprintf(logfd, fmt, arg_ptr);
   va_end(arg_ptr);

   return len;
}

deeperror_t __print_error(deeperror_t err, const char* file, unsigned int line_n, const char *error_msg) {
   const char* logclass;
   FILE*       logfd = log_file_descriptor();

   switch(err) {
      case DEEPDEV_ERR:     logclass = "ERROR"; break;
      case DEEPDEV_COMMERR: logclass = "ERROR"; break;
      case DEEPDEV_ANSERR:  logclass = "ERROR"; break;
      default:              logclass = "INFO"; break;
   }

   __log_head(logfd, logclass, file, line_n);

   fprintf(logfd, "%s\n", error_msg);

   return err;
}

deeperror_t __lib_error(deeperror_t err, const char* file, unsigned int line_n, const char *fmt, ...) {
   va_list arg_ptr;

   thread_info.error_code = err;

   va_start(arg_ptr, fmt);
   if (vsnprintf(thread_info.error_msg, STATIC_BUFF_SZ, fmt, arg_ptr) == STATIC_BUFF_SZ)
      thread_info.error_msg[STATIC_BUFF_SZ - 1] = 0;
   va_end(arg_ptr);

   if (IS_LOGGABLE(ERROR, 1))
      __print_error(err, file, line_n, thread_info.error_msg);

   return err;
}


#define LNCHAR 16
#define GNCHAR  2
#define MAXDUMP (LNCHAR * 40)

void char_dump(unsigned const char *buf, ssize_t len) {
   size_t remain;
   int    i;

   if (len < MAXDUMP) {
      LOG_INFO("dumping %zd bytes\n", len);
      remain = 0;
   } else {
      LOG_INFO("dumping %d bytes (of %zd)\n", MAXDUMP, len);
      remain = len - MAXDUMP;
      len = MAXDUMP;
   }

   for (; len > 0; len -= LNCHAR, buf+= LNCHAR) {
      loginfof("  ");
      for (i = 0; i < LNCHAR && i < len ; i++)
         loginfof("%c", isprint(buf[i])? buf[i] : '.');
      for (; i < LNCHAR ; i++)
         loginfof(" ");
      loginfof(" : ");
      for (i = 0; i < LNCHAR && i < len; i++)
         loginfof("%s%02x", !(i % GNCHAR)? " ":"", buf[i]);
      loginfof("\n");
   }
   if (remain)
      loginfof("  and %zd bytes more....\n", remain);
}


LIBDEEP_EXPORT
deeperror_t deepdev_error(char **errmsg) {
  if (errmsg)
     *errmsg = thread_info.error_msg;
  return(thread_info.error_code);
}

void show_all_parameters(void) {
   FILE*  logfd = log_file_descriptor();
   int    i;

   fprintf(logfd, "- Current library parameters:\n");
   for(i = 0; i < deepdev_par_items; i++) {
      if (par_list[i].flags & (_RO | _HIDE)) continue;
      if (strcmp(par_list[i].name, "DEBUGTAGS") == 0)
         fprintf(logfd, "    %-11s = %s\n", par_list[i].name, logmask_as_string(*(int*)par_list[i].addr));
      else
         fprintf(logfd, "    %-11s = %d\n", par_list[i].name, *(int*)par_list[i].addr);
   }
   fprintf(logfd, "\n");
}

void show_all_internals(void) {
   FILE*           logfd = log_file_descriptor();
   int             nthreads;
   int             n_c, n_f;
   lnklist_item_t* thrit;
   lnklist_item_t* it;

   fprintf(logfd, "\n libdeep status");
   fprintf(logfd, "\n------------------\n");
   show_all_parameters();
   for (n_c = 0, it = g_context.devpars.head; it; it = it->next, ++n_c);
   fprintf(logfd, "- Parameter tables   :%3d\n", n_c);
   for (n_c = 0, it = g_context.devcmds.head; it; it = it->next, ++n_c);
   fprintf(logfd, "- Command structures :%3d\n", n_c);

   nthreads = 0;
   n_c = n_f = 0;
   for (thrit = g_context.threads.head; thrit; thrit = thrit->next, ++nthreads) {
      devthread_t* thread = thrit->itmdata;
      n_c += thread->n_handles;
      it = thread->handles.head;
      for ( ; it; it = it->next, ++n_f) ;
   }
   fprintf(logfd, "- Running threads    :%3d  (found: %d)\n", g_context.nthreads, nthreads);
   fprintf(logfd, "- Open device handles:%3d  (found: %d)\n", n_c, n_f);
   for (nthreads = 0, thrit = g_context.threads.head; thrit; thrit = thrit->next, ++nthreads) {
      devthread_t* thread = thrit->itmdata;
   fprintf(logfd, "  Thread #%-3d //  %d device%s\n", nthreads, thread->n_handles,
                                                            thread->n_handles == 1? "": "s" );
      it = thread->handles.head;
      for (n_c = 0 ; it; it = it->next, ++n_c) {
         devhandle_t* dev = it->itmdata;
         fprintf(logfd, "      Device %-2d: 0x%016" PRIxPTR " :[%12s] %s%s%s\n", n_c, (uintptr_t)dev,
                                       dev->connected? "connected" : "disconnected",
                                       dev->chan->chname, ID_SEPARATOR, dev->devname);
      }
   }
   fprintf(logfd, "\n");
}

#define DD_ERROR(err, msg) #err,
LIBDEEP_EXPORT
const char *deepdev_errcodes[] = {DEEPDEV_ERRORS};
#undef DD_ERROR

#define DD_ERROR(err, msg) msg,
LIBDEEP_EXPORT
const char *deepdev_errmsgs[] = {DEEPDEV_ERRORS};
#undef DD_ERROR

LIBDEEP_EXPORT
const int deepdev_errcodes_n = (sizeof(deepdev_errcodes) / sizeof(const char*));


LIBDEEP_EXPORT
deeperror_t  deepdev_errorcode(deeperror_t errcode, const char **errname, const char **errmsg) {
   if (errcode >= 0 && errcode < deepdev_errcodes_n) {
      *errname = deepdev_errcodes[errcode];
      *errmsg = deepdev_errmsgs[errcode];
      return(DEEPDEV_OK);
   } else
      return(set_liberror(DEEPDEV_ERR, "invalid error code %d", errcode));
};


