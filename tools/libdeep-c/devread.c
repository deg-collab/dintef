/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: libdeep   //  Client library to communicate with instruments
 *                        using the libdeep protocol
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/branches/dev-new/c/src/devloop.c $
 * $Rev: 228 $
 * $Date: 2017-10-31 21:49:59 +0100 (Tue, 31 Oct 2017) $
 *------------------------------------------------------------------------- */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

#include "deepdefs.h"


static void buffer_init(buffer_t* buffer, void* data, size_t bsize) {
   buffer->buff = data;
   buffer->buff_size = bsize;
   buffer->first = buffer->next = buffer->end = buffer->buff;
}

static void buffer_reset(buffer_t* buffer, void* data, size_t bsize) {
   if (buffer->buff != data)
      free(buffer->buff);
   buffer_init(buffer, data, bsize);
}

void reader_init(reader_t* reader) {
   buffer_t* buffer = &reader->buffer;

   buffer_init(buffer, reader->statbuffer, STATIC_BUFF_SZ);
   reader->flags = 0;
   reader->currcmd = NULL;
}

static void reader_reset(reader_t* reader) {
   buffer_t* buffer = &reader->buffer;
   size_t    nchar = buffer->end - buffer->next;

   if (nchar > 0) {
      if (buffer->next != reader->statbuffer) {
         if (nchar <= STATIC_BUFF_SZ) {
            memmove(reader->statbuffer, buffer->next, nchar);
         } else {
            ; // internal error
         }
      }
   }
   buffer_reset(buffer, reader->statbuffer, STATIC_BUFF_SZ);
   buffer->end = buffer->first + nchar;
   reader->flags = 0;
   reader->currcmd = NULL;
}


static void reader_flush(reader_t* reader) {
   buffer_t* buffer = &reader->buffer;
   buffer->next = buffer->end;
   reader_reset(reader);
}


void reader_fini(reader_t* reader) {
   buffer_reset(&reader->buffer, reader->statbuffer, STATIC_BUFF_SZ);
}


static void* buffer_realloc(buffer_t* buffer, ssize_t newsz, int isstatic) {
   void*     buff;
   size_t    used = buffer->next - buffer->first;
   size_t    bsize = buffer->end - buffer->first;

   LOG_TRACE();

   if (buffer->first != buffer->buff) {
      memmove(buffer->buff, buffer->first, bsize);
      buffer->first = buffer->buff;
      buffer->next = buffer->first + used;
      buffer->end = buffer->first + bsize;
   }

   if (isstatic) {
      if (newsz <= buffer->buff_size)
         return buffer->buff;
      else if ((buff = malloc(newsz)) != NULL) {
         memcpy(buff, buffer->buff, bsize);
      }
   } else {
      buff = realloc(buffer->buff, newsz);
   }

   if (buff != NULL) {
      buffer->buff_size = newsz;
      buffer->first = buffer->buff = buff;
      buffer->next = buffer->first + used;
      buffer->end = buffer->first + bsize;
   }
   return buff;
}


static void* reader_realloc(reader_t* reader, ssize_t newsz) {
   buffer_t* buffer = &reader->buffer;
   int       isstatic = (buffer->buff == reader->statbuffer);

   return buffer_realloc(buffer, newsz, isstatic);
}


static ssize_t fill_current_buffer(devhandle_t* dev, size_t sz) {
   dev_read_fn_t read_fn = dev->chan->dev_ops->dev_read_fn;
   buffer_t*     buffer = &dev->devreader.buffer;
   size_t        currsz = buffer->end - buffer->first;

   DEBUGF_3("%s() up to %zd bytes (already %zd)\n", __func__, sz, currsz);

   if (currsz >= sz)
      return currsz;
   else {
      if (sz > (buffer->buff_size - (buffer->first - buffer->buff))) {
         if (reader_realloc(&dev->devreader, sz) == NULL) {
            set_liberror(DEEPDEV_SYSERR, "internal memory allocation error");
            return -1;
         }
      }
      sz = read_fn(dev, buffer->end, sz - currsz);
      DEBUGF_3("%zd bytes read\n", sz);

      // use readbytes to check if is the first read after waking up from select()
      if (sz < 0 || (sz == 0 && dev->devreader.readbytes == 0)) {
         return sz;
      } else {
         dev->devreader.readbytes += sz;
         buffer->end += sz;
         return sz + currsz;
      }
   }
}

static inline int istrncmp(const char* str1, const char* str2, size_t len) {
   while(len--) {
      if (toupper(*str1++) != toupper(*str2++))
         return 1;
   }
   return 0;
}


static devcmd_t* find_asciiquery(devhandle_t* dev, char* lprfx, int lprfxlen) {
   devcmd_t*       cmdinfo;
   lnklist_item_t* item;

   if (lprfxlen > 0) {
      for (item = dev->devqueries.head; item; item = item->next) {
         cmdinfo = item->itmdata;
         if (cmdinfo->answ_buf != NULL)  // if a command received answer already
            continue;
         // if dev->answnoprfx is set, the device does not return prefix and then the
         //  first unanswered command is taken as the good one
         if (dev->answnoprfx)
            return cmdinfo;
         else {
//LOG_INFO("%d, %d, <%.*s> <%.*s>", lprfxlen, cmdinfo->prfxlen, lprfxlen, cmdinfo->prfx, lprfxlen, lprfx);
            if (cmdinfo->prfxlen == lprfxlen && istrncmp(cmdinfo->prfx, lprfx, lprfxlen) == 0) {
               return cmdinfo;
            }
         }
      }
   }
   return NULL;
}

static devcmd_t* find_binaryquery(devhandle_t* dev) {

   if (dev->devqueries.head != NULL) {
      devcmd_t* cmdinfo = dev->devqueries.head->itmdata;

      if (cmdinfo->answ_buf != NULL)
         return cmdinfo;
   }
   return NULL;
}


static int get_prefix_length(const char* line, size_t length) {
   const char* ptr;

   for(ptr = line; length--; ptr++) {
      char c = *ptr;
      if (c == ' ' || c == '\n')
         return (ptr - line);
   }
   return 0;
}


static size_t analyse_first_line(devhandle_t* dev, int isfullline) {
   reader_t* reader = &dev->devreader;
   buffer_t* buffer = &dev->devreader.buffer;
   int       linelength = buffer->next - buffer->first;
   int       prfxlength = get_prefix_length(buffer->first, linelength);

   LOG_TRACE();

   reader->currcmd = find_asciiquery(dev, buffer->first, prfxlength);

   if (!reader->currcmd) {
      buffer->first = buffer->next;
      linelength = 0;
      if (!isfullline)
         reader->flags |= RDEST_DISCARD;
   } else {

      if (isfullline && linelength >= 2 && *((uint8_t *)buffer->next - 2) == '$') {
         reader->flags |= RDEST_MULTILINE;
         buffer->first = buffer->next;
         linelength = 0;
      } else {
         buffer->first += reader->currcmd->prfxlen;
         linelength -= reader->currcmd->prfxlen;
         // skip initial white spaces
         while(linelength && *(char*)buffer->first == ' ') {
            linelength--;
            buffer->first++;
         }
      }
   }
   return linelength;
}

#define IS_VALID_CHAR(c) ((((c) < (unsigned char)128) && ((c) >= ' ')) || (c) == '\n' || (c) == '\r')

static int message_type(char* buff, size_t nchar) {
   // we could do something more sofisticated to detect utf-8, but ...
   if (nchar > 4) nchar = 4;

   while (nchar--) {
      unsigned char c = *buff++;
      if (!IS_VALID_CHAR(c))
         return RTYPE_PACKAGED;
   }
   return RTYPE_PLAIN;
}

static int new_ascii_line(buffer_t* buffer) {
   char*     buff_from = buffer->next;
   char*     buff_to = buffer->next;
   ssize_t   nchar = buffer->end - buffer->next;
   int       line_found;
   char      c;

   LOG_TRACE();

   while (nchar--) {
      if ((c = *(buff_from++)) < ' ') {  // if cntrl char
         if (c == DEEPDEV_CMD_EOC) {  // treat '\n' and skip other cntrl chars (e.g. '\r')
            *(buff_to++) = c;
            line_found = 1;
            break;
         }
      } else {
         *(buff_to++) = c;
      }
   }
   if (nchar >= 0)
      line_found = 1;
   else
      nchar = line_found = 0;

   buffer->next = buff_to;
   buffer->end = buffer->next + nchar;

   // copy any remaining char at the end of the buffer
   if (nchar && buffer->next != buff_from)
      memmove(buffer->next, buff_from, nchar);

   return line_found;
}


void complete_cmd(devcmd_t* cmdinfo) {
   timerlist_remove_timer(&cmdinfo->timer);
   lnklist_unlink_item(&cmdinfo->cmdansw_it);
   signal_calling_thread(cmdinfo->dev->devthread, &cmdinfo->result.cond);
   trigger_notifiers(&cmdinfo->notifiers, (evsource_t*)cmdinfo);
}


void release_subscriber(devsubcrb_t* subcsrb) {

}


void set_cmderror(devcmd_t* cmdinfo, deeperror_t errcode, const char* fmt, ...) {
   va_list arg_ptr;

   if (cmdinfo->result.errcode == DEEPDEV_OK) {
      cmdinfo->result.errcode = errcode;
      cmdinfo->result.errmsg = cmdinfo->answ_buf_stat;

      if (fmt != NULL) {
         if (cmdinfo->answ_buf != cmdinfo->answ_buf_stat)
            free(cmdinfo->answ_buf);
         cmdinfo->answ_buf = cmdinfo->answ_buf_stat;

         va_start(arg_ptr, fmt);
         if (vsnprintf(cmdinfo->answ_buf_stat, STATIC_BUFF_SZ, fmt, arg_ptr) == STATIC_BUFF_SZ)
            cmdinfo->answ_buf_stat[STATIC_BUFF_SZ - 1] = 0;
         va_end(arg_ptr);
      }
   }
}


static void check_command_termination(devhandle_t* dev, devcmd_t* cmdinfo) {

   if (cmdinfo->result.errcode != DEEPDEV_OK || !IS_BINARY_QUERY(cmdinfo->flags)) {
      // if error or not binary query, the query is finished
      complete_cmd(cmdinfo);
   } else {
      // make sure that the command is before any unanswered command
      lnklist_item_t* item;

      for (item = dev->devqueries.head; item; item = item->next) {
         devcmd_t* cmd = item->itmdata;
         if (cmd == cmdinfo)  // if our command is in place already, do nothing
            break;
         if (cmd->answ_buf == NULL) { // if a command is waiting for answer
            // insert our command in front of it
            lnklist_insert_before(&dev->devqueries, item, &cmdinfo->cmdansw_it);
            break;
         }
      }
   }
}


static void process_command_answer(devhandle_t* dev, devcmd_t* cmdinfo, size_t length) {
   reader_t* reader = &dev->devreader;
   buffer_t* buffer = &reader->buffer;
   size_t    remain = buffer->end - buffer->next;

   LOG_TRACE();

   *((char*)buffer->first + length - 1) = 0;

   // Check if error answer from device
   if (strncmp(buffer->first, PFX_ERROR, sizeof(PFX_ERROR) - 1) == 0) {
      // Remove "ERROR" prefix
      buffer->first += sizeof(PFX_ERROR);
      while (isspace(*(char*)buffer->first))
         buffer->first++;
      SET_CMDERROR(cmdinfo, DEEPDEV_ANSERR, "Error from instrument: %s\n", buffer->first);

   } else { // Normal end
      SET_CMDERROR(cmdinfo, DEEPDEV_OK, NULL);
   }

//LOG_INFO("answ=<%s>\n", buffer->first);

   if (buffer->buff != reader->statbuffer) {
      if (buffer->first != buffer->buff)
         memmove(buffer->buff, buffer->first, length);
      cmdinfo->answ_buf = buffer->buff;

      if (remain) {
         if (remain > STATIC_BUFF_SZ) {
            void* newbuff = malloc(remain);
            if (newbuff) {
               memcpy(newbuff, buffer->next, remain);
               buffer_init(buffer, newbuff, remain);
            }
         } else {
            memmove(reader->statbuffer, buffer->next, remain);
            buffer_init(buffer, reader->statbuffer, STATIC_BUFF_SZ);
         }
         buffer->end = buffer->first + remain;
      } else
         buffer_init(buffer, reader->statbuffer, STATIC_BUFF_SZ);

   } else if (buffer->first != cmdinfo->answ_buf_stat) {
      memcpy(cmdinfo->answ_buf_stat, buffer->first, length);
      cmdinfo->answ_buf = cmdinfo->answ_buf_stat;
   }

   if (cmdinfo->answ)
      *cmdinfo->answ = cmdinfo->answ_buf;
//LOG_INFO("answ=%p, <%s>\n", *cmdinfo->answ, *cmdinfo->answ);
}


static ssize_t process_readplain(devhandle_t* dev) {
   reader_t* reader = &dev->devreader;
   buffer_t* buffer = &dev->devreader.buffer;
   size_t    answerlength;

   LOG_TRACE();

#if 0
   if (buffer->next == buffer->first) { // beginning, empty answer
      while (*buffer->first == ' ' && buffer->first < buffer->end) buffer->first++;  // skip initial whitespaces
      buffer->next = buffer->first;
   }
#endif

   while(buffer->end != buffer->next) {
      int isfullline = new_ascii_line(buffer);

      answerlength = buffer->next - buffer->first;

      if (isfullline || answerlength >= MAX_PREFIX_SZ) {
         if (reader->flags & RDEST_DISCARD) {
            buffer->first = buffer->next;  // skip current data
            if (isfullline)
               reader_reset(reader);   // reset flags, buffer, ...
            break;
         } else if (reader->currcmd != NULL) {
            if (isfullline) {
               if (reader->flags & RDEST_MULTILINE) {
                  if (answerlength >= 2 && *((uint8_t *)buffer->next - 2) == '$' &&
                                           *((uint8_t *)buffer->next - 3) == '\n') {
                     answerlength -= 2;
                     goto on_answer_complete;
                  }
               } else
                  goto on_answer_complete;
            }

         } else {
            answerlength = analyse_first_line(dev, isfullline);
            if (reader->currcmd != NULL) {
               if (isfullline && !(reader->flags & RDEST_MULTILINE))
                  goto on_answer_complete;
            } else if (isfullline)
               break;
         }
      }
   }
   goto on_exit;

 on_answer_complete:
   process_command_answer(dev, reader->currcmd, answerlength);
   check_command_termination(dev, reader->currcmd);
   reader_reset(reader);
 on_exit:
   return buffer->end - buffer->next;  // remaining chars to process in buffer
}


static ssize_t continue_readplain(devhandle_t* dev) {
   buffer_t* buffer = &dev->devreader.buffer;
   size_t    nchar = buffer->end - buffer->first;

   if (buffer->next != buffer->end)
      return process_readplain(dev);
   else {
      nchar = (nchar < MAX_PREFIX_SZ)? MAX_PREFIX_SZ : (nchar + (STATIC_BUFF_SZ - MAX_PREFIX_SZ));
      nchar = fill_current_buffer(dev, nchar);

      if (nchar <= 0) {
         return nchar;
      } else {
         return process_readplain(dev);
      }
   }
}


static void process_dchunk_data(devhandle_t* dev) {
   reader_t* reader = &dev->devreader;

   reader->flags = 0;
}


static void process_binary_data(devhandle_t* dev, devcmd_t* cmdinfo) {
   reader_t* reader = &dev->devreader;
   buffer_t* buffer = &reader->buffer;

   if (!reader->skip_checksum) {
      uint32_t checksum = calc_checksum(reader->datasize, reader->datatype, buffer->first);

      if (dev->binformat == BINARY_HSER) {
         reader->checksum = *(uint8_t*)(buffer->next - 1);
         checksum &= 0xFF;
      }

      if (checksum != reader->checksum) { // checksum error
         LOG_ERROR("Checksum mismatch: %d <> %d\n", checksum, reader->checksum);
         SET_CMDERROR(cmdinfo, DEEPDEV_ANSERR, "Bad checksum in binary data");
      }
   }

   if (buffer->end - buffer->next) {
      // in this case we are using the static buffer and we
      //  have to treat the remaining bytes
      reader_reset(reader);
   } else {
      // in this case the static buffer is empty and we do not have
      //   to free the dynamic one (with the binary data)
      reader_init(reader);
   }
   complete_cmd(cmdinfo);
}


static void* prepare_binary_buffer(reader_t* reader, size_t nbytes) {
   devcmd_t*      cmdinfo = reader->currcmd;
   deepbindata_t* dbin = cmdinfo->bdat;

   if (dbin->databuf && dbin->bufsize < nbytes) {
      if (dbin->databuf == cmdinfo->bdat_buff) {
         DEBUGF_3("current data buffer too small, reallocating\n");
         free(dbin->databuf);
         dbin->databuf = cmdinfo->bdat_buff = NULL;
         cmdinfo->bdat_buff_sz = 0;
      } else {
         SET_CMDERROR(cmdinfo, DEEPDEV_COMMERR, "data buffer is too small");
         return NULL;
      }
   }
   if (!dbin->databuf) {
      if (nbytes > cmdinfo->bdat_buff_sz || nbytes == 0) {
         DEBUGF_3("allocating new data buffer\n");
         if (cmdinfo->bdat_buff)
            free(cmdinfo->bdat_buff);
         if (nbytes == 0)  // make sure new buffer is not zero size
            nbytes = 32;
         dbin->databuf = cmdinfo->bdat_buff = malloc(nbytes);
         if (dbin->databuf != NULL)
            dbin->bufsize = cmdinfo->bdat_buff_sz = nbytes;
         else {
            dbin->bufsize = cmdinfo->bdat_buff_sz = 0;
            SET_CMDERROR(cmdinfo, DEEPDEV_COMMERR, "memory allocation error");
            reader->flags |= RDEST_DISCARD;
         }
      } else {
         dbin->databuf = cmdinfo->bdat_buff;
         dbin->bufsize = cmdinfo->bdat_buff_sz;
      }
   }
   return dbin->databuf;
}

//  ---- temporary patch/hack to investigate the coredump

void my_memcpy(void* dest, void* src, size_t sz, int line,
                                                 size_t bufsize,
                                                 void*  bdat_buff,
                                                 size_t bdat_buff_sz,
                                                 void*  old_databuf,
                                                 size_t old_bufsize,
                                                 deepbindata_t bdat_old) {
   memcpy(dest, src, sz);
}

#define memcpy(dest, src, sz) my_memcpy(dest, src, sz, __LINE__, \
                                                       bdat->bufsize, \
                                                       reader->currcmd->bdat_buff, \
                                                       reader->currcmd->bdat_buff_sz, \
                                                       bdat_old.databuf, \
                                                       bdat_old.bufsize, \
                                                       bdat_old)

//  ---- end of temporary patch/hack


static void prepare_binary_reader(devhandle_t* dev, reader_t* reader, ssize_t nchar) {
   buffer_t* buffer = &reader->buffer;
   size_t    eff_nbytes = reader->bytes2read;

   //  --- temporary test code
   deepbindata_t bdat_old;  // temporary test variable

   if (reader->currcmd && reader->currcmd->bdat) {
      bdat_old = *reader->currcmd->bdat;
   }
   //  --- end of temporary test code

   if (dev->binhdrsz == LEGACYHDR_SZ) eff_nbytes--;

   if (reader->currcmd && reader->currcmd->bdat && prepare_binary_buffer(reader, eff_nbytes)) {
      deepbindata_t* bdat = reader->currcmd->bdat;

      bdat->datatype = reader->datatype;
      bdat->datasize = reader->datasize;

      if (nchar >= reader->bytes2read) {
         memcpy(bdat->databuf, buffer->first, eff_nbytes);

         if (dev->binhdrsz == LEGACYHDR_SZ) {
            reader->checksum = *(char*)(buffer->first + eff_nbytes);
         }
         buffer->next = buffer->first + reader->bytes2read;
         process_binary_data(dev, reader->currcmd);

      } else {
         memcpy(bdat->databuf, buffer->first, nchar);
         if (buffer->buff == reader->statbuffer)
            buffer_init(buffer, bdat->databuf, bdat->bufsize);
         else
            buffer_reset(buffer, bdat->databuf, bdat->bufsize);

         buffer->end += nchar;

         reader->flags |= RDEST_BINDATA;
      }
   } else {
      reader->flags |= RDEST_DISCARD;
   }
}

static void process_legacy_header(devhandle_t* dev, uint16_t nbytes) {
   reader_t* reader = &dev->devreader;

   DEBUGF_2("  Legacy header, bytes: %zd\n", nbytes);
   reader->skip_checksum = 0;    // checksum is mandatory in legacy devices
   reader->swap_bytes = 0;
   reader->datatype = BIN_8;
   reader->datasize = nbytes;
   reader->checksum = 0;
   reader->bytes2read = nbytes + 1;   // one additional byte to get trailer chksum

   DEBUGF_2("  - Datatype: %d byte(s)/value\n", reader->datatype);
   DEBUGF_2("  - Datasize: %d values, %d bytes\n", reader->datasize, reader->bytes2read);
   DEBUGF_2("  - Checksum field: 0x%08X - skip check: %s\n",
                           reader->checksum, reader->skip_checksum? "YES":"NO");
}

static int process_binary_header(devhandle_t* dev, binheader_t* header, uint32_t signature) {
   reader_t* reader = &dev->devreader;

   DEBUGF_2("  Signature field: 0x%08X\n", signature);

   if (signature == ICEPAP_SIGNATURE) {
      DEBUGF_2("  - Using IcePAP mode\n");
      reader->skip_checksum = 0;         // checksum is mandatory in IcePAP
      reader->swap_bytes = (__BYTE_ORDER != __DEEP_ORDER);
      reader->datatype = BIN_16;

   } else if ((signature & DEEP_SIGNATURE_MASK) == DEEP_SIGNATURE) {
      DEBUGF_2("  - Using standard DEEP mode\n");
      reader->skip_checksum = (signature & DEEP_NOCHECKSUM);
      if (signature & DEEP_BIGENDIAN)
         reader->swap_bytes = (__BYTE_ORDER != __BIG_ENDIAN);
      else
         reader->swap_bytes = (__BYTE_ORDER != __LITTLE_ENDIAN);
      reader->datatype = (signature & DEEP_TYPEMASK);
   } else {
      reader->datasize = reader->bytes2read = 0;
      return -1;
   }
   reader->datasize = FROM_DEEP_ENDIAN(header->size);
   reader->checksum = FROM_DEEP_ENDIAN(header->checksum);
   reader->bytes2read = reader->datasize * reader->datatype;

   DEBUGF_2("  - Datatype: %d byte(s)/value\n", reader->datatype);
   DEBUGF_2("  - Datasize: %d values, %d bytes\n", reader->datasize, reader->bytes2read);
   DEBUGF_2("  - Checksum field: 0x%08X - skip check: %s\n",
                           reader->checksum, reader->skip_checksum? "YES":"NO");
   return reader->bytes2read;
}


static int process_readpackaged(devhandle_t* dev) {
   reader_t* reader = &dev->devreader;
   buffer_t* buffer = &reader->buffer;
   ssize_t   nchar = buffer->end - buffer->first;

   if (reader->flags & RDEST_BINDATA) {
      if (nchar >= reader->bytes2read) { // if package fully received
         // consume the data
         buffer->next += reader->bytes2read;
         reader->bytes2read = 0;
         // and process it
         if (reader->flags & RDEST_DCHUNK)
            process_dchunk_data(dev);
         else
            process_binary_data(dev, reader->currcmd);
      }
      goto on_exit;

   } else if (reader->flags & RDEST_DCHUNK) {  // dchunk
      ;  // treat dchunk header

   } else {                                    // BINARY data or unknown
      // make sure that we have the minimum number of bytes to continue
      if (nchar < dev->binhdrsz)
         goto on_exit;

      // make sure that header is aligned at the beginning of the static buffer
      if (buffer->first != buffer->buff)
         reader_realloc(reader, 0);

      // check header
      if (dev->binhdrsz == 0) {  // BINARY_HGPIB, no header
         // Not implemented yet
         goto on_exit;

      } else if (dev->binhdrsz == LEGACYHDR_SZ) {  // BINARY_HSER
         uint8_t* ptr = buffer->first;
         if (*ptr == 0xFF) {
            uint16_t nbytes = *(ptr + 1);
            nbytes = (nbytes << 8) + *(ptr + 2);
            buffer->next = buffer->first += LEGACYHDR_SZ;
            nchar -= LEGACYHDR_SZ;
            process_legacy_header(dev, nbytes);
            prepare_binary_reader(dev, reader, nchar);
         }
      } else {                  // BINARY_DANCE or BINARY_ICEPAP
         packheader_t* pckheader = buffer->first;
         uint32_t      signature = FROM_DEEP_ENDIAN(pckheader->binary.signature);
         DEBUGF_2("  Signature field: 0x%08X, binformat: %d\n", signature, dev->binformat);

         if ((dev->binformat == BINARY_ICEPAP && signature == ICEPAP_SIGNATURE) ||
             (dev->binformat == BINARY_DANCE && (signature & DEEP_SIGNATURE_MASK) == DEEP_SIGNATURE)) {

            reader->currcmd = find_binaryquery(dev);

            buffer->next = buffer->first += BINARYHDR_SZ;
            nchar -= BINARYHDR_SZ;
            if (process_binary_header(dev, &pckheader->binary, signature) < 0)
               goto on_exit;
            prepare_binary_reader(dev, reader, nchar);

         } else {  // process dchunks
            if (0) { // check dchunk header
               nchar -= sizeof(dcheader_t);
               // add dchunk or prepare dblock buffer
            } else {
               TODO ;  // do something smart, to discard data, review error mechanism
               set_liberror(DEEPDEV_SYSERR, "binary data reception error");
               // discard data
               reader_flush(reader);
               // and for the time being, just flush brutally the input buffer
               while(dev->chan->dev_ops->dev_read_fn(dev, NULL, 10000000) > 0)
                  ;
               LOG_TRACE();
            }
         }
      }
   }
 on_exit:
   return buffer->end - buffer->next;
}


static ssize_t discard_data(devhandle_t* dev) {
   reader_t* reader = &dev->devreader;
   buffer_t* buffer = &reader->buffer;
   ssize_t   nchar;

   LOG_INFO("Discarding %zd bytes\n", reader->bytes2read);

   buffer->first = buffer->next;

   while(reader->bytes2read) {
      nchar = buffer->buff_size - (buffer->first - buffer->buff);
      if (nchar > reader->bytes2read) {
         nchar = reader->bytes2read;
         if ((buffer->end - buffer->first) > nchar) {
            buffer->first = (buffer->next += reader->bytes2read);
            reader->bytes2read = 0;
            return buffer->end - buffer->next;
         }
      }
      nchar = fill_current_buffer(dev, nchar);

      if (nchar <= 0)
         return nchar;

      reader->bytes2read -= (buffer->end - buffer->first);
      buffer->end = buffer->next = buffer->first = buffer->buff;
   }
   reader->flags = 0;
   return buffer->end - buffer->next;
}


static ssize_t continue_readpackaged(devhandle_t* dev) {
   int       flags = dev->devreader.flags;
   ssize_t   nchar;
   ssize_t   full_size;

   LOG_TRACE();

   if (flags & RDEST_DISCARD)
      return discard_data(dev);

   if (flags & RDEST_BINDATA) {
      full_size = dev->devreader.bytes2read;  // data size
   } else if (flags & RDEST_DCHUNK)
      full_size = sizeof(dcheader_t);     // datachunk header size
   else     // BINARY data or unknown
      full_size = sizeof(binheader_t);    // binary header size

   nchar = fill_current_buffer(dev, full_size);

   if (nchar <= 0) {
      return nchar;
   } else {
      return process_readpackaged(dev);
   }
}

static ssize_t start_read(devhandle_t* dev) {
   ssize_t   nchar;
   buffer_t* buffer = &dev->devreader.buffer;

   LOG_TRACE();

   if ((nchar = fill_current_buffer(dev, 32)) <= 0)
      return nchar;

   dev->devreader.flags = message_type(buffer->first, nchar);

   if (dev->devreader.flags & RTYPE_PLAIN)          // plain ASCII message
      return process_readplain(dev);
   else if (dev->devreader.flags & RTYPE_PACKAGED)  // packaged message
      return process_readpackaged(dev);
   else {                            // RTYPE_UNKNOWN       undefined (too early!)
      buffer->next += nchar;
      return 0;
   }
}

static void complete_pending_commands(devhandle_t* dev) {
   devcmd_t*       cmdinfo;
   lnklist_item_t* item;

   if ((cmdinfo = dev->devreader.currcmd) == NULL  &&
        dev->devqueries.head != NULL)
      cmdinfo = dev->devqueries.head->itmdata;

   if (cmdinfo != NULL) {
      if (thread_info.error_code != DEEPDEV_OK)
         SET_CMDERROR(cmdinfo, thread_info.error_code, thread_info.error_msg);
      else
         SET_CMDERROR(cmdinfo, DEEPDEV_COMMERR, "communication error");
      complete_cmd(cmdinfo);
   }

   for (item = dev->devqueries.head; item; item = item->next) {
      cmdinfo = item->itmdata;
      SET_CMDERROR(cmdinfo, DEEPDEV_COMMERR, "instrument disconnected");
      complete_cmd(cmdinfo);
   }
}

void device_read(devhandle_t* dev) {
   ssize_t nchar;

   LOG_TRACE();

   thread_info.error_code = DEEPDEV_OK;
   dev->devreader.readbytes = 0;
   do {
      int flags = dev->devreader.flags;

      if (flags & RTYPE_PLAIN)          // plain ASCII message
         nchar = continue_readplain(dev);
      else if (flags & RTYPE_PACKAGED)  // packaged message
         nchar = continue_readpackaged(dev);
      else
         nchar = start_read(dev);

      if (nchar < 0 || dev->devreader.readbytes == 0) { // an error happened, what to do?

         // do some cleanup
         dev->chan->dev_ops->dev_disconnect_fn(dev);
         dev->connected = 0;

         complete_pending_commands(dev);
         return;
      }
   } while(nchar > 0);
}

void device_write(devhandle_t* dev) {
//   ssize_t nchar;

   LOG_TRACE();

//   nchar =
   dev->devio.ops->dev_write_fn(dev, NULL, 0);
/*
   if (nchar < 0)
      return DEEPDEV_ERR;
   else
      return DEEPDEV_OK;
*/
}


static void process_command_timeout(void* arg) {
   devcmd_t* cmdinfo = arg;
   LOG_TRACE();

   SET_CMDERROR(cmdinfo, DEEPDEV_COMMERR, "command timeout");
   complete_cmd(cmdinfo);
}


void set_cmdtimer(devthread_t* devthread, devcmd_t* cmdinfo) {
   timeritem_t* timer = &cmdinfo->timer;

   LOG_INFO("setting timeout : %d ms\n", cmdinfo->rtimeout);
   timer->exec_fn = process_command_timeout;
   timer->exec_data = cmdinfo;
   timerlist_add_timer(&devthread->timeouts, &cmdinfo->timer, cmdinfo->rtimeout);
}

void update_cmdtimer(devthread_t* devthread, devcmd_t* cmdinfo) {
   LOG_INFO("updating timeout : %d ms\n", cmdinfo->rtimeout);
   timerlist_update_timer(&devthread->timeouts, &cmdinfo->timer, cmdinfo->rtimeout);
}
