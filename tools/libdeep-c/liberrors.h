/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: libdeep   //  Client library to communicate with instruments
 *                        using the libdeep protocol
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/trunk/c/src/liberrors.h $
 * $Rev: 366 $
 * $Date: 2020-01-21 18:26:35 +0100 (Tue, 21 Jan 2020) $
 *------------------------------------------------------------------------- */

#ifndef __LIBERRORS_H_INCLUDED__
#define __LIBERRORS_H_INCLUDED__

#include <stdio.h>

#include "libdeep.h"

//---------------------------------------------------
// Debug related
//---------------------------------------------------


#define tic LOG_TRACE();
#define tac(fmt, ...) LOG_INFO(fmt, ##__VA_ARGS__)

//=================================================================


#define DEEPDEV_LOG_CLASSES \
          DDL_CLASS(ERROR,   "...") \
          DDL_CLASS(WARN,    "...") \
          DDL_CLASS(DEVERROR,"...") \
          DDL_CLASS(INFO,    "...") \
          DDL_CLASS(ACTION,  "...") \
          DDL_CLASS(EVENT,   "...") \
          DDL_CLASS(TRACE,   "...") \
          DDL_CLASS(DEBUG,   "...") \

#define DDL_CLASS(__class, __msg) LOG_CLASS_ ## __class,
typedef enum {
   DEEPDEV_LOG_CLASSES
   DEEPDEV_LOG_ALL
} logclass_t;
#undef DDL_CLASS

#define DDL_CLASS(__class, __msg) LOG_MASK_ ## __class = (1 << LOG_CLASS_ ## __class),
typedef enum {
   DEEPDEV_LOG_CLASSES
} logmask_t;
#undef DDL_CLASS

#define DEF_DEBUG_LEVEL 2
#define MAX_DEBUG_LEVEL 5

#ifdef LOG_DEF_LEVEL
   #define _LOG_DEF_LEVEL LOG_DEF_LEVEL
#else
   #define _LOG_DEF_LEVEL DEF_DEBUG_LEVEL
#endif

#ifdef LOG_MAX_LEVEL
   #define _LOG_MAX_LEVEL LOG_MAX_LEVEL
#else
   #define _LOG_MAX_LEVEL MAX_DEBUG_LEVEL
#endif

#ifdef LOG_CLASS_MASK
   #define _LOG_CLASS_MASK LOG_CLASS_MASK
#else
   #define _LOG_CLASS_MASK 0xffff
#endif


// -------------- definitions in errlog.c

void show_all_internals(void);
void char_dump(unsigned const char *buf, ssize_t len);


#define IS_LOGGABLE(__class, __level) \
   ((__level <= _LOG_MAX_LEVEL) &&   \
    (LOG_MASK_ ## __class & _LOG_CLASS_MASK) && \
    is_log_enabled(LOG_MASK_ ## __class, __level))

#define LOG_PRINTF(__class, __level, __fmt, ...) \
   do { \
      if (IS_LOGGABLE(__class, __level)) \
         __logwritef(#__class, __FILE__, __LINE__, __fmt, ##__VA_ARGS__); \
   } while(0)

#define LOG_ERROR(fmt, ...)   LOG_PRINTF(ERROR, 1,              fmt, ##__VA_ARGS__)
#define LOG_WARN(fmt, ...)    LOG_PRINTF(WARN,  1,              fmt, ##__VA_ARGS__)
#define LOG_INFO(fmt, ...)    LOG_PRINTF(INFO,  _LOG_DEF_LEVEL, fmt, ##__VA_ARGS__)
#define LOG_ACTION(fmt, ...)  LOG_PRINTF(ACTION,_LOG_DEF_LEVEL, fmt, ##__VA_ARGS__)
#define LOG_EVENT(fmt, ...)   LOG_PRINTF(EVENT, _LOG_DEF_LEVEL, fmt, ##__VA_ARGS__)
#define LOG_TRACE()           LOG_PRINTF(TRACE, _LOG_MAX_LEVEL, "in %s()\n", __func__)
#define DEBUGF_1(fmt, ...)    LOG_PRINTF(DEBUG, 1,              fmt, ##__VA_ARGS__)
#define DEBUGF_2(fmt, ...)    LOG_PRINTF(DEBUG, 2,              fmt, ##__VA_ARGS__)
#define DEBUGF_3(fmt, ...)    LOG_PRINTF(DEBUG, 3,              fmt, ##__VA_ARGS__)
#define DEBUGF_4(fmt, ...)    LOG_PRINTF(DEBUG, 4,              fmt, ##__VA_ARGS__)
#define DEBUGF_5(fmt, ...)    LOG_PRINTF(DEBUG, 5,              fmt, ##__VA_ARGS__)

#define SYSTEM_ERROR()  LOG_ERROR("Internal library error in %s()", __func__)

//------------------------------------------------

deeperror_t string_to_logmask(const char* strmask, uint32_t* logmask);
const char* logmask_as_string(uint32_t logmask);

int __logwritef(const char* logclass, const char* file, unsigned int line_n, const char* fmt, ...);
#define loginfof(fmt, ...) __logwritef(NULL, NULL, 0, fmt, ##__VA_ARGS__)

deeperror_t __print_error(deeperror_t err, const char* file, unsigned int line_n, const char *error_msg);
deeperror_t __lib_error(deeperror_t err, const char* file, unsigned int line_n, const char *fmt, ...);
#define set_liberror(err, fmt, ...) __lib_error(err, __FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define reset_liberror(err) do {thread_info.error_code = (err);} while(0);




/* Macros for management of thread local variables   */

#ifndef thread_local
#  if __STDC_VERSION__ >= 201112 && !defined __STDC_NO_THREADS__
#    define thread_local _Thread_local
#  elif defined _WIN32 && ( \
        defined _MSC_VER || \
        defined __ICL || \
        defined __DMC__ || \
        defined __BORLANDC__ )
#    define thread_local __declspec(thread)
/* note that ICC (linux) and Clang are covered by __GNUC__ */
#  elif defined __GNUC__ || \
        defined __SUNPRO_C || \
        defined __xlC__
#    define thread_local __thread
#  else
#    error "Cannot define thread_local"
#  endif
#endif  /* thread_local */

extern thread_local struct thread_info_s thread_info;
#define SET_DEVICE_THREAD()  do {thread_info.is_device_thread = 1;} while(0)
#define IN_DEVICE_THREAD()  thread_info.is_device_thread

#endif  /* __LIBERRORS_H_INCLUDED__ */
