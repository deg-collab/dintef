/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: libdeep   //  Client library to communicate with instruments
 *                        using the libdeep protocol
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/trunk/c/src/deepdefs.h $
 * $Rev: 377 $
 * $Date: 2020-02-12 19:50:01 +0100 (Wed, 12 Feb 2020) $
 *------------------------------------------------------------------------- */

#ifndef __DEEPDEFS_H_INCLUDED__
#define __DEEPDEFS_H_INCLUDED__

#ifdef HIDESYMBOLS
#define LIBDEEP_EXPORT __attribute__ ((visibility ("hidden")))
#else
#define LIBDEEP_EXPORT __attribute__ ((visibility ("default")))
#endif

#include <stdio.h>
#include <pthread.h>
#include <netinet/in.h>  // IPPROTO_TCP, etc

#define LOG_MAX_LEVEL 5   // change LOG_MAX_LEVEL to reduce message logging
#include "liberrors.h"

#include "lnklist.h"
#include "timer.h"

#include "libdeep.h"

//#define USE_MYMALLOC
#include "mymalloc.h"


#define STATIC_BUFF_SZ 1024


struct thread_info_s {
   int         is_device_thread;
   deeperror_t error_code;
   char        error_msg[STATIC_BUFF_SZ];
};

typedef struct {
   uint32_t  signature;
   uint32_t  size;
   uint32_t  checksum;
} binheader_t;

#define DEEP_SIGNATURE      0xA5A50000
#define DEEP_SIGNATURE_MASK 0xA5A50000
#define DEEP_NOCHECKSUM     0x00000010
#define DEEP_BIGENDIAN      0x00000020
#define DEEP_TYPEMASK       0x0000000F

#define ICEPAP_SIGNATURE    0xA5AA555A
#define ICEPAP_TYPE         BIN_16

typedef struct {
   uint32_t  signature;
   uint32_t  size;
} dcheader_t;  // dchunk header


typedef union {
   binheader_t binary;
   dcheader_t  dchunk;
} packheader_t;

#define BINARYHDR_SZ (sizeof(binheader_t))
#define LEGACYHDR_SZ (3)

typedef struct devthread_s devthread_t;
typedef struct devhandle_s devhandle_t;
typedef struct iodata_s    iodata_t;
typedef struct devcmd_s    devcmd_t;
typedef struct io_ops_s    devloop_ops_t;
typedef struct evsource_s  evsource_t;
typedef struct notifier_s  notifier_t;  // filedesc and callbacks


#define __DEEP_OBJECT_HEADING__  \
   uint32_t       magic;         \
   lnklist_item_t global_it;

#define DEEP_HANDLE_MAGIC   0xdefecada
#define DEEP_DEVPAR_MAGIC   0xdeffecad
#define DEEP_CMDDEV_MAGIC   0xfecadabe
#define DEEP_FILEDESC_MAGIC 0xfacedeba
#define DEEP_CALLBACK_MAGIC 0xcecadada

struct deepitem_s {
   __DEEP_OBJECT_HEADING__
};


#define DEVFAMILY_LIST     \
    DEEPDEV_TYPE(DANCE)    \
    DEEPDEV_TYPE(PREDANCE) \
    DEEPDEV_TYPE(ICEPAP)   \
    DEEPDEV_TYPE(LEGACY)   \
    DEEPDEV_TYPE(NMODES)   \


#define DEEPDEV_TYPE(name) DEEPDEV_ ## name,
enum {
  DEVFAMILY_LIST N_DEEPDEVTYPES
};
#undef DEEPDEV_TYPE

// Library parameters to be managed per connection
typedef struct devpars_s {
   int devfamily;
   int timeoutms;
   int usechecksum;
   int reconnect;
   int tcpport;
} devpars_t;

typedef struct devpartbl_s {
   __DEEP_OBJECT_HEADING__
   devpars_t      pars;
} devpartbl_t;



#define DEF_DEVPARS   { \
   .devfamily = DEEPDEV_DANCE,  \
   .timeoutms = 3000, \
   .usechecksum = 1,  \
   .reconnect = 1,    \
   .tcpport = 5000,   \
}


// Global library parameters
typedef struct partable_s {
   FILE*     errfd;
   int       maxthreads;
   int       maxdevices;
   int       debuglevel;
   uint32_t  debugmask;
   devpars_t devpars;   // default 'per device' parameters
} libpars_t;

#define DEF_LIB_PARS {    \
   .errfd = NULL,  /* this field MUST be initialised to stderr!! */ \
   .maxthreads = 0,       \
   .maxdevices = 5,       \
   .debuglevel = 0,       \
   .debugmask = 0xffff,   \
   .devpars = DEF_DEVPARS \
}

struct g_context_s {
   pthread_mutex_t lib_mutex;
   lnklist_t       threads;
   lnklist_t       devpars;
   lnklist_t       devcmds;
   lnklist_t       filedescs;
   lnklist_t       callbacks;
   unsigned int    nthreads;
   libpars_t       defpars;
   libpars_t       libpars;
};

extern struct g_context_s g_context;


static inline unsigned int is_log_enabled(uint32_t class_mask, uint32_t level) {
   return ((level <= g_context.libpars.debuglevel) &&
           (class_mask & g_context.libpars.debugmask));
}


typedef struct {
   int   code;
   char* name;
   void* addr;
   int   flags;
} parentry_t;

#define _RW      0         // read/write parameter
#define _RO      (1 << 0)  // read only parameter
#define _GLOBAL  (1 << 1)  // global parameter
#define _STATIC  (1 << 2)  // global parameter
#define _HIDE    (1 << 3)  // hidden value

extern parentry_t par_list[];


// action result structure

typedef struct {
#define WAIT_NO    0
#define WAIT_BEGIN 1
#define WAIT_END   2
   int             waitmode;
   pthread_cond_t* cond;
   deeperror_t     errcode;
   char*           errmsg;
} result_t;

typedef deeperror_t (*action_fn)(void*);

typedef struct {
   char*       fname;
   action_fn   fn;
   void*       arg;
   result_t*   result;
} action_t;


typedef struct iodata_s {
   int                     fd;
   volatile uint32_t       flags;
   const struct io_ops_s*  ops;
} iodata_t;


typedef struct devthread_s {
   lnklist_item_t   thread_it;   // item for thread list

   lnklist_t        handles;     // list of device handles
   int              n_handles;

   pthread_t        thread;
   pthread_mutex_t  thread_mutex;

   iodata_t         action_io;
   int              action_fd;
   action_t         action;

   timerlist_t      timeouts;    // list of timeout timers

} devthread_t;


extern devloop_ops_t tcp_io_ops;
extern devloop_ops_t serial_io_ops;
extern devloop_ops_t rashpa_io_ops;

typedef struct {
   const int            type;
   const char*          chname;
   const devloop_ops_t* dev_ops;
} chtype_t;

extern const devloop_ops_t tcp_ops;
extern const devloop_ops_t serial_ops;
extern const devloop_ops_t devpipe_ops;
extern const devloop_ops_t rashpa_ops;

#define ID_SEPARATOR "://"

#define CHAN_TYPE_LIST \
   CHAN_TYPE(CHAN_TCP,    "tcp",    &tcp_ops) \
   CHAN_TYPE(CHAN_SERIAL, "serial", &serial_ops) \
   CHAN_TYPE(CHAN_PIPE,   "pipe",   &devpipe_ops) \
   CHAN_TYPE(CHAN_RASHPA, "rashpa", &rashpa_ops) \

#define CHAN_TYPE(type, chname, io_ops_ptr) type,
enum {
  CHAN_TYPE_LIST N_CHANTYPES
};
#undef CHAN_TYPE

extern const chtype_t chan_types[];


typedef deeperror_t (*devloop_devfn_t)(devhandle_t*);
typedef ssize_t (*dev_write_fn_t)(devhandle_t*, const void*, size_t);
typedef ssize_t (*dev_read_fn_t)(devhandle_t*, void*, size_t);


typedef struct io_ops_s {
   const devloop_devfn_t dev_init_fn;        // initialise required resources
   const devloop_devfn_t dev_connect_fn;     // establish connection
   const devloop_devfn_t dev_disconnect_fn;  // close connection
   const devloop_devfn_t dev_fini_fn;        // release all resources

   const dev_write_fn_t  dev_write_fn;       // send data to instrument
   const dev_read_fn_t   dev_read_fn;        // get data from instrument
} devloop_ops_t;




#define RTYPE_NONE      (0)
#define RTYPE_PLAIN     (1 << 1)
#define RTYPE_PACKAGED  (1 << 2)

#define RDEST_DISCARD   (1 << 8)
#define RDEST_MULTILINE (1 << 9)
#define RDEST_DCHUNK    (1 << 10)
#define RDEST_BINDATA   (1 << 11)


#define MAX_PREFIX_SZ      32


typedef struct {
   void*          buff;     // pointer to acual buffer: statbuffer or malloc'ed
   size_t         buff_size;// capacity of buffer pointed by buff
   void*          first;    // points to beginning of valid data
   void*          next;     // points to next char after valid data
   void*          end;      // points to end of allocated buffer
   lnklist_item_t item;     // to ease datablock linking
} buffer_t;

typedef struct reader_s {
   int            flags;
   size_t         readbytes;
     // statbuffer is declared as uint32_t to guarantee alignment
   uint32_t       statbuffer[STATIC_BUFF_SZ / 4];  // static buffer
   buffer_t       buffer;
   devcmd_t*      currcmd;

     // info for binary data blocks
   size_t         bytes2read;
   int            skip_checksum;
   int            swap_bytes;
   int            datatype;
   int            datasize;
   uint32_t       checksum;

} reader_t;

/*
void*     mblock_ptr(mblock_t* mblock)
size_t    mblock_size(mblock_t* mblock)

mblock_t* mblock_create(mblock_t* mblock, void *data, size_t size, uint32_t flags);
mblock_t* mblock_release(mblock_t* mblock, int mode);
dblock_t* dblock_create(dblock_t* dblk, mblock_t* mblock, size_t size, uint32_t flags);
dblock_t* dblock_release(dblock_t* dblk, int mode);
dblock_t* dblock_acquire(dblock_t* dblk);
size_t    dblock_resize(dblock_t* dblk, size_t newsize);
*/

typedef struct devhandle_s {
   __DEEP_OBJECT_HEADING__
   devthread_t*    devthread;
   pthread_mutex_t dev_mutex;
   iodata_t        devio;

   devpars_t       devpars;
#define MAX_NAMELENGTH 255
   char            devname[MAX_NAMELENGTH + 1];
   const chtype_t* chan;

#define BINARY_DANCE  0x0000 // DAnCE 12-byte binary header
#define BINARY_ICEPAP 0x0001 // IcePAP 12-byte binary header
#define BINARY_HSER   0x0002 // Hitachi serial 3-byte header + 1-byte trailer
#define BINARY_HGPIB  0x0003 // Hitachi GPIB no header
   uint8_t         binformat;  // binary format
   uint8_t         binhdrsz;   // binary header size
   uint8_t         answnoprfx; // no prefix in ascii answers

   uint8_t         connected;
   reader_t        devreader;
   lnklist_t       devqueries;
   lnklist_t       devsubscrs;

   union {
      struct {
         char      hostname[MAX_NAMELENGTH + 1];
         struct addrinfo *ainfo;
      } tcp;
/*
      struct {
      } serial;
      struct {
         int       in_fd;
         int       out_fd;
      } pipe;
      struct {
      } rashpa;
*/
   } cfg;
} devhandle_t;


#define CMDBUFF_SZ 256

typedef struct devcmd_s {
   __DEEP_OBJECT_HEADING__
   lnklist_item_t cmdansw_it;
   devhandle_t*   dev;
   int            flags;
   char*          cmdbuf;
   char           cmdbuf_stat[CMDBUFF_SZ];
   size_t         cmdlen;
   char*          prfx;
   size_t         prfxlen;
   char*          answ_buf;
   char           answ_buf_stat[STATIC_BUFF_SZ];
   const char**   answ;
   unsigned int   wtimeout;
   unsigned int   rtimeout;
   timeritem_t    timer;

   union {
      binheader_t   binheader;
      char          legacyheader[LEGACYHDR_SZ + 2];
   }              binhdr;
   void*          bdat_buff;
   size_t         bdat_buff_sz;
   size_t         bdat_nbytes;
   deepbindata_t* bdat;

   pthread_cond_t cmd_cond;
   result_t       result;
   lnklist_t      notifiers;
} devcmd_t;


typedef struct dstreams_s {
   lnklist_item_t dstreams_it;
   lnklist_t      subscribers;
   char           id[32];

} dstreams_t;

typedef struct devsubcrb_s {
   lnklist_item_t item;
} devsubcrb_t;


typedef struct evsource_s {
   __DEEP_OBJECT_HEADING__
} evsource_t;


typedef struct notifier_s {
   __DEEP_OBJECT_HEADING__
   lnklist_t evsources;
   union {
      struct {
         int  wr_channel;
         int  rd_channel;
      } fd;
      struct {
         int  slow;
         void (*fn)(deepevsrc_t);
      } cb;
   } type;
} notifier_t;


inline static int is_valid_handle(devhandle_t* dev) {
   return(dev && dev->magic == DEEP_HANDLE_MAGIC);
}
inline static int is_valid_devpartbl(devpartbl_t* devpartbl) {
   return(devpartbl && devpartbl->magic == DEEP_DEVPAR_MAGIC);
}
inline static int is_valid_devcmd(devcmd_t* cmdinfo) {
   return(cmdinfo && cmdinfo->magic == DEEP_CMDDEV_MAGIC);
}

inline static int is_valid_filedesc(notifier_t* filedesc) {
   return(filedesc && filedesc->magic == DEEP_FILEDESC_MAGIC);
}

inline static int is_valid_callback(notifier_t* callback) {
   return(callback && callback->magic == DEEP_CALLBACK_MAGIC);
}



void signal_calling_thread(devthread_t* devthread, pthread_cond_t** condition_ptr);


#define IS_DEBUGDUMP() (g_context.libpars.debuglevel >= 3)

#define DEEPDEV_CMD_EOC     '\n'
#define DEEPDEV_BAD_SOCKET -1

#define PFX_ERROR "ERROR"
#define PFX_OK    "OK"

extern const int deepdev_par_items;

ssize_t default_devwrite(devhandle_t* dev, const void *buff, size_t size);
uint32_t calc_checksum(uint32_t nvalues, int type, void *buff);

// defined in devloop.c
int         set_nonblock(int fd, int set);
deeperror_t devloop_init(devthread_t* devthread);
deeperror_t devloop_fini(devthread_t* devthread);
deeperror_t set_actionerror(devthread_t* devthread, deeperror_t errcode, const char* fmt, ...);
void release_devhandle(devhandle_t* dev);
void trigger_notifiers(lnklist_t* notifier_list, evsource_t* evsource);


ssize_t default_devread(devhandle_t* dev, void *buff, size_t size);

// defined in devread.c
void reader_init(reader_t* reader);
void reader_fini(reader_t* reader);
void device_read(devhandle_t* dev);
void device_write(devhandle_t* dev);
void set_cmderror(devcmd_t* cmdinfo, deeperror_t errcode, const char* fmt, ...);
void complete_cmd(devcmd_t* cmdinfo);
void release_subscriber(devsubcrb_t* subcsrb);
void set_cmdtimer(devthread_t* devthread, devcmd_t* cmdinfo);


#define SET_CMDERROR(cmdinfo, err, fmt, ...) do {    \
    set_cmderror(cmdinfo, err, fmt, ##__VA_ARGS__);  \
    if (err != DEEPDEV_OK && IS_LOGGABLE(ERROR, 1))  \
       __print_error(err, __FILE__, __LINE__, cmdinfo->answ_buf_stat); \
} while(0)


#endif  /* __DEEPDEFS_H_INCLUDED__ */
