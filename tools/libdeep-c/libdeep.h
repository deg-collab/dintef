/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: libdeep   //  Client library to communicate with instruments
 *                        using the libdeep protocol
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/trunk/c/src/libdeep.h $
 * $Rev: 374 $
 * $Date: 2020-02-10 19:15:37 +0100 (Mon, 10 Feb 2020) $
 *------------------------------------------------------------------------- */

#ifndef _LIBDEEP_H
#define _LIBDEEP_H

#if 0
  #define TODO   TODO:
  #define TODO2  TODO2:
#else
  #define TODO
  #define TODO2
#endif



// Generic byte ordering related macros

#define swappedval16(val)   ((((val) <<  8) & 0xFF00) | \
                             (((val) >> 16) & 0x00FF) )
#define set_swappedval16(var, val)  ((var) = swappedval16(val))
#define set_swappedvar16(var, var0) ( \
                      *(((uint8_t *)&var) + 1) = *(((uint8_t *)&var0) + 0), \
                      *(((uint8_t *)&var) + 0) = *(((uint8_t *)&var0) + 1) )

#define swappedval32(val)   ((((val) << 24) & 0xFF000000) | \
                             (((val) <<  8) & 0x00FF0000) | \
                             (((val) >>  8) & 0x0000FF00) | \
                            (((val) >> 24) & 0x000000FF) |
#define set_swappedval32(var, val)  ((var) = swappedval32(val))
#define set_swappedvar32(var, var0) ( \
                      *(((uint8_t *)&var) + 3) = *(((uint8_t *)&var0) + 0), \
                      *(((uint8_t *)&var) + 2) = *(((uint8_t *)&var0) + 1), \
                      *(((uint8_t *)&var) + 1) = *(((uint8_t *)&var0) + 2), \
                      *(((uint8_t *)&var) + 0) = *(((uint8_t *)&var0) + 3) )

#define swappedval64(val) (swappedval32((val) >> 32) | \
                          (swappedval32((val) & 0x00FFFFFFFFL) << 32))
#define set_swappedval64(var, val)  ((var) = swappedval64(val))
#define set_swappedvar64(var, var0) ( \
                      *(((uint8_t *)&var) + 7) = *(((uint8_t *)&var0) + 0), \
                      *(((uint8_t *)&var) + 6) = *(((uint8_t *)&var0) + 1), \
                      *(((uint8_t *)&var) + 5) = *(((uint8_t *)&var0) + 2), \
                      *(((uint8_t *)&var) + 4) = *(((uint8_t *)&var0) + 3), \
                      *(((uint8_t *)&var) + 3) = *(((uint8_t *)&var0) + 4), \
                      *(((uint8_t *)&var) + 2) = *(((uint8_t *)&var0) + 5), \
                      *(((uint8_t *)&var) + 1) = *(((uint8_t *)&var0) + 6), \
                      *(((uint8_t *)&var) + 0) = *(((uint8_t *)&var0) + 7) )

#define SWAP_BYTES(v) (sizeof(v) == 64? swappedval64(v) :  \
                       sizeof(v) == 32? swappedval32(v) :  \
                       sizeof(v) == 16? swappedval16(v) : (v))

#define SET_SWAPPEDVAL(v, val) (sizeof(v) == 64? set_swappedval64(v, val) :\
                                sizeof(v) == 32? set_swappedval32(v, val) :\
                                sizeof(v) == 16? set_swappedval16(v, val) :\
                                (v) = (v)))

#define SET_SWAPPEDVAR(v, var) (sizeof(v) == 64? set_swappedvar64(v, var) :\
                                sizeof(v) == 32? set_swappedvar32(v, var) :\
                                sizeof(v) == 16? set_swappedvar16(v, var) :\
                                (v) = (var)))
/*
#ifndef __LITTLE_ENDIAN
#define __LITTLE_ENDIAN 1234
#endif
#ifndef __BIG_ENDIAN
#define __BIG_ENDIAN    4321
#endif

#if __LITTLE_ENDIAN == __BIG_ENDIAN
#error Incompatible __LITTLE_ENDIAN and __BIG_ENDIAN macros.
#endif
*/

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
 #define TO_LITTLE_ENDIAN(par)         (par)
 #define TO_BIG_ENDIAN(par)            SWAP_BYTES(par)
 #define SET_LITTLE_ENDIAN_VAL(v, val) ((v)= (val))
 #define SET_LITTLE_ENDIAN_VAR(v, var) ((v)= (var))
 #define SET_BIG_ENDIAN_VAL(v, val)    SET_SWAPPED_VAL(v, val)
 #define SET_BIG_ENDIAN_VAR(v, val)    SET_SWAPPED_VAR(v, var)
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
 #define TO_LITTLE_ENDIAN(par)         SWAP_BYTES(par)
 #define TO_BIG_ENDIAN(par)            (par)
 #define SET_LITTLE_ENDIAN_VAL(v, val) SET_SWAPPED_VAL(v, val)
 #define SET_LITTLE_ENDIAN_VAR(v, var) SET_SWAPPED_VAR(v, var)
 #define SET_BIG_ENDIAN_VAL(v, val)    ((v) = (val))
 #define SET_BIG_ENDIAN_VAR(v, var)    ((v) = (var))
#else
 #error __BYTE_ORDER__ not defined
#endif


/////////////////////////////////////////////
//  libdeep specific

#define __DEEP_ORDER                  __LITTLE_ENDIAN
#define TO_DEEP_ENDIAN(par)           TO_LITTLE_ENDIAN(par)
#define SET_DEEP_ENDIAN_VAL(v, val)   SET_LITTLE_ENDIAN_VAL(v, val)
#define SET_DEEP_ENDIAN_VAR(v, var)   SET_LITTLE_ENDIAN_VAR(v, var)

#define FROM_DEEP_ENDIAN(par)         TO_DEEP_ENDIAN(par)

#ifdef __cplusplus
extern "C" {
#endif

typedef struct devhandle_s* deephandle_t;
typedef struct devcmd_s*    deepcmd_t;
typedef struct devpartbl_s* deepdpars_t;
typedef struct evsource_s*  deepevsrc_t;
typedef struct notifier_s*  deepfd_t;
typedef struct notifier_s*  deepcb_t;
typedef struct notifier_s*  deepnotify_t;

typedef void*               deepitem_t;

// Special handle codes
#define BAD_HANDLE   ((deephandle_t) NULL)

// Special parameter table codes
#define BAD_DEVPARAMS     ((deepdpars_t) NULL)
#define DEFAULT_DEVPARAMS ((deepdpars_t)(-1))
#define CURRENT_DEVPARAMS ((deepdpars_t)(-2))

#define LIB_DEFAULTS CURRENT_DEVPARAMS

// Special address codes
#define BROADCAST_ADDR   ("")

// Binary data definitions
typedef enum {
  BIN_8  = 1,
  BIN_16 = 2,
  BIN_32 = 4,
  BIN_64 = 8
} deepdtype_t;

typedef struct {
  void       *databuf;
  size_t      bufsize;
  int         datatype;
  size_t      datasize;
} deepbindata_t;

#define EMPTY_BINDATA { \
  NULL,  /* databuf  */ \
  0,     /* bufsize  */ \
  0,     /* datasize */ \
  BIN_8  /* datatype */ \
}

typedef const void* deeppar_t;

#define DDPAR(variable)      ((deeppar_t) &variable)
#define DDPARtoINT(pValPtr)  (*(int*)pValPtr)
#define DDPARtoSTR(pValPtr)  ((char*)pValPtr)
#define DDPARtoFILE(pValPtr) (*(FILE**)pValPtr)

#define NO_ACK  0
#define ACK     1

// Command flags
#define DEEPDEV_FLG_QUERY   (1<<0)
#define DEEPDEV_FLG_ACK     (1<<1)
#define DEEPDEV_FLG_BIN     (1<<2)
#define DEEPDEV_FLG_ADDR    (1<<3)
#define DEEPDEV_FLG_BCAST   (1<<4)

#define IS_ACK_COMMAND(flags) \
           (((flags) & DEEPDEV_FLG_ACK ) != 0)
#define IS_QUERY(flags) \
           (((flags) & DEEPDEV_FLG_QUERY ) != 0)
#define RETURNS_ANSWER(flags) \
           (((flags) & (DEEPDEV_FLG_ACK | DEEPDEV_FLG_QUERY)) != 0)
#define IS_BINARY_COMMAND(flags) \
      (((flags) & (DEEPDEV_FLG_BIN | DEEPDEV_FLG_QUERY)) == (DEEPDEV_FLG_BIN))
#define IS_BINARY_QUERY(flags) \
      (((flags) & (DEEPDEV_FLG_BIN | DEEPDEV_FLG_QUERY)) == (DEEPDEV_FLG_BIN | DEEPDEV_FLG_QUERY))


// Error return codes
#define DEEPDEV_ERRORS \
          DD_ERROR(OK,       "Succesful execution") \
          DD_ERROR(NOTREADY, "Device answer is not ready") \
          DD_ERROR(SYSERR,   "Internal system error") \
          DD_ERROR(ERR,      "Library error") \
          DD_ERROR(COMMERR,  "Communication error") \
          DD_ERROR(ANSERR,   "Command error") \

#define DD_ERROR(err, msg) DEEPDEV_ ## err ,
typedef enum {
   DEEPDEV_ERRORS
} deeperror_t;
#undef DD_ERROR


extern const int   deepdev_errcodes_n;
extern const char* deepdev_errcodes[];
extern const char* deepdev_errmsgs[];


deepdpars_t  deepdev_partblinit(
                 deepitem_t   parObject);
deeperror_t  deepdev_setparam(
                 deepitem_t   parObject,
                 char*        pName,
                 deeppar_t    pValPtr);
deeperror_t  deepdev_getparam(
                 deepitem_t   parObject,
                 char*        pName,
                 deeppar_t*   pValPtr);

deephandle_t deepdev_devinit(
                 char*        devId,
                 deepitem_t   parObject);
deephandle_t deepdev_open(
                 char*        devId);
deephandle_t deepdev_search(
                 const char*  devIdFmt,    // sprintf-type fmt string: e.g. "tcp://maestrio:%d"
                 int*         devIdIndex,  // first index value (e.g. TCP port number)
                 int          devidN,      // number of indices to scan
                 const char*  appName,     // dance APPNAME string or NULL
                 const char*  devAddr,     // dance ADDR string or NULL
                 deepitem_t   parObject);  // parameter table, device handle, DEFAULT_DEVPARAMS or
                                           //    CURRENT_DEVPARAMS
deeperror_t  deepdev_connect(
                 deephandle_t dev);
deeperror_t  deepdev_disconnect(
                 deephandle_t dev);
deeperror_t  deepdev_close(
                 deephandle_t dev);
deeperror_t  deepdev_closeall(
                 void);

deeperror_t  deepdev_parsecommand(
                 int*         flagsPtr,
                 const char*  addr,
                 const char*  cmd,
                 int          ack);
deeperror_t  deepdev_command(
                 deephandle_t   dev,
                 const char*    addr,
                 const char*    cmd,
                 int            ack,
                 const char**   answPtr,
                 deepbindata_t* bdatPtr);
deeperror_t  deepdev_error(
                 char**       errmsg);

deeperror_t  deepdev_errorcode(
                 deeperror_t  code,
                 const char** errname,
                 const char** errmsg);

deepcmd_t    deepdev_cmdinit(void);
deepfd_t     deepdev_fdinit(int *fd);
deepcb_t     deepdev_cbinit(
                 void (*fn)(deepevsrc_t));
deeperror_t  deepdev_free(
                 deepitem_t item);
deeperror_t  deepdev_freethread(
                 void);

deeperror_t  deepdev_addevsource(
                 deepnotify_t item,
                 deepevsrc_t  evsource,
                 unsigned int flags);

deepevsrc_t  deepdev_evsource(
                 deepfd_t deepfd);

deeperror_t  deepdev_evinfo(
                 deepevsrc_t   evsource,
                 deephandle_t* dev,
                 deepcmd_t*    cmdinfo,
                 unsigned int* flags);

deeperror_t  deepdev_startcommand(
                 deepcmd_t      cmdinfo,
                 deephandle_t   dev,
                 const char*    addr,
                 const char*    cmd,
                 int            ack,
                 const char**   answPtr,
                 deepbindata_t* bdatPtr);

deeperror_t  deepdev_checkcommand(
                 deepitem_t     evsource,
                 int            wait);

#define deepdev_checkevsource deepdev_checkcommand

#ifdef __cplusplus
} /* extern "C" */
#endif


#define deepdev_cmd(dev, cmd) \
                 deepdev_command(dev, NULL, cmd, 0, NULL, NULL)
#define deepdev_ackcmd(dev, cmd) \
                 deepdev_command(dev, NULL, cmd, 1, NULL, NULL)
#define deepdev_query(dev, cmd, answ) \
                 deepdev_command(dev, NULL, cmd, 1, answ, NULL)
#define deepdev_addr_cmd(dev, addr, cmd) \
                 deepdev_command(dev, addr, cmd, 0, NULL, NULL)
#define deepdev_addr_ackcmd(dev, addr, cmd) \
                 deepdev_command(dev, addr, cmd, 1, NULL, NULL)
#define deepdev_addr_query(dev, addr, cmd, answ) \
                 deepdev_command(dev, addr, cmd, 1, answ, NULL)
#define deepdev_bincmd(dev, cmd, dbin) \
                 deepdev_command(dev, NULL, cmd, 0, NULL, dbin)
#define deepdev_ackbincmd(dev, cmd, dbin) \
                 deepdev_command(dev, NULL, cmd, 1, NULL, dbin)
#define deepdev_binquery(dev, cmd, answ, dbin) \
                 deepdev_command(dev, NULL, cmd, 1, answ, dbin)
#define deepdev_addr_bincmd(dev, addr, cmd, dbin) \
                 deepdev_command(dev, addr, cmd, 0, NULL, dbin)
#define deepdev_addr_ackbincmd(dev, addr, cmd, dbin) \
                 deepdev_command(dev, addr, cmd, 1, NULL, dbin)
#define deepdev_addr_binquery(dev, addr, cmd, answ, dbin) \
                 deepdev_command(dev, addr, cmd, 1, answ, dbin)

#endif  /* _LIBDEEP_H */
