/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: libdeep   //  Client library to communicate with instruments
 *                        using the libdeep protocol
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/trunk/c/src/timer.h $
 * $Rev: 303 $
 * $Date: 2018-02-27 19:32:39 +0100 (Tue, 27 Feb 2018) $
 *------------------------------------------------------------------------- */

#ifndef __TIMER_H_INCLUDED__
#define __TIMER_H_INCLUDED__


#include <stdint.h>
#include <sys/time.h>
#include "lnklist.h"


typedef void (*timerfn_t)(void*);


typedef struct timeritem {
   lnklist_item_t    item;    // linked list item
   unsigned int      rel_ms;  // ms relative to previous timer

   timerfn_t         exec_fn; // user provided execution routine
   void*             exec_data;
} timeritem_t;


typedef struct timerlist {
   lnklist_t      list;    // timer item linked list
   struct timeval ref_tv;  // reference time
} timerlist_t;


void            timerlist_add_timer(timerlist_t* tlist, timeritem_t* timer_it, unsigned int time_in_ms);
void            timerlist_update_timer(timerlist_t* tlist, timeritem_t* timer, unsigned int time_in_ms);
void            timerlist_remove_timer(timeritem_t* timer_it);
void            timerlist_exec_expired(timerlist_t* tlist);
struct timeval* timerlist_get_next(timerlist_t* tlist);

#endif /* __TIMER_H_INCLUDED__ */
