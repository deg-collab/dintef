/*---------------------------------------------------------------------------
 * ESRF -- The European Synchrotron
 *         Instrumentation Services and Development Division
 *
 * Project: libdeep   //  Client library to communicate with instruments
 *                        using the libdeep protocol
 *
 * $URL: https://deg-svn.esrf.fr/svn/libdeep/trunk/c/src/serial.c $
 * $Rev: 285 $
 * $Date: 2018-02-20 00:38:14 +0100 (Tue, 20 Feb 2018) $
 *------------------------------------------------------------------------- */

#include "deepdefs.h"


static deeperror_t dev_init(devhandle_t* dev) {
   DEBUGF_3("initilising serial port \"%s\"\n", dev->devname);

   return(DEEPDEV_OK);
}


static deeperror_t dev_fini(devhandle_t* dev) {

   return(DEEPDEV_OK);
}


static deeperror_t dev_connect(devhandle_t* dev) {
   // Most probably serial lines do not need any code here
   return(DEEPDEV_OK);
}


static deeperror_t dev_disconnect(devhandle_t* dev) {

   DEBUGF_2("closing connection to %s\n", dev->devname);
   return(DEEPDEV_OK);
}

static ssize_t dev_write(devhandle_t* dev, const void *buff, size_t size) {

   return default_devwrite(dev, buff, size);
}


static ssize_t dev_read(devhandle_t* dev, void *buff, size_t size) {

   return default_devread(dev, buff, size);
}



const devloop_ops_t serial_ops = {
   .dev_init_fn       = dev_init,
   .dev_connect_fn    = dev_connect,
   .dev_disconnect_fn = dev_disconnect,
   .dev_fini_fn       = dev_fini,

   .dev_write_fn      = dev_write,
   .dev_read_fn       = dev_read,
};

