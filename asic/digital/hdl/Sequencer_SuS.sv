`default_nettype none

module Sequencer_SuS #(
  parameter SEQ_PART_DEPTH=14,
  parameter SEQ_PART_WIDTH=5,
  parameter REP_CNT_WIDTH=5,
  parameter HOLD_CNT_WIDTH=14,
  parameter HOLD_REG_DEPTH=5,
  parameter TRACKS=5,
  parameter FAST_CNT_BITS=3
) (
  clock_t.LEAF             clock,
  input  wire              run,
  input  wire              sel_stat_vals,
  input  wire              init_shift, // iprog_done from MasterFSM iprog counter
  output wire              hold,       // holds MasterFSM cycle counter

  // dynamic sequences
  input  wire              ext_ddyn,
  output wire              mon_ddyn,
  output wire [TRACKS-1:0] ddyn,

  // jtag signals
  JTAG_internal.register   JTAG,
  input  wire              select_config_reg,
  input  wire              select_rep_cnts,
  input  wire [TRACKS-1:0] select_track,
  output wire              tdo_config_reg,
  output wire              tdo_rep_cnts,
  output wire [TRACKS-1:0] tdo_track
);

wire init; // inits sequences from jtag registers
// this is the init signal shifted by 4 cycles in the 700MHz domain
// to ease loading new values into the fast registers
wire init_fast;
wire ld_next_part_seq;
wire [TRACKS-1:0] sel_ext, stat_vals_idle, stat_vals_iprog, int_ddyn, invert_hold, track_hold;
wire [4:0] sel_mon_ddyn;

assign init = !run;

PartSeqControl #(
  .SEQ_PART_WIDTH(SEQ_PART_WIDTH),
  .HOLD_REG_DEPTH(HOLD_REG_DEPTH),
  .HOLD_CNT_WIDTH(HOLD_CNT_WIDTH),
  .FAST_CNT_BITS(FAST_CNT_BITS)
) PartSeqControl_I (
  .clock(clock),
  .init(init),
  .init_shift(init_shift),
  .ld_next_part_seq(ld_next_part_seq),
  .init_fast(init_fast),
  .hold(hold),

  .JTAG( JTAG ),
  .select(select_rep_cnts),
  .tdo(tdo_rep_cnts)
);


genvar i;
generate
for (i=0; i<TRACKS; i=i+1) begin : SeqTracks
  SequencerTrack #(
    .CNT_WIDTH(REP_CNT_WIDTH),
    .DEPTH(SEQ_PART_DEPTH),
    .WIDTH(SEQ_PART_WIDTH)
  ) SequencerTrack (
    .clock(clock),
    .init(init),
    .init_shift(init_shift),
    .init_fast(init_fast),
    .hold(hold && track_hold[i]),
    .invert_hold(invert_hold[i]),
    .ld_next_part_seq(ld_next_part_seq),
    .ddyn(int_ddyn[i]),

    .JTAG( JTAG ),
    .select(select_track[i]),
    .tdo(tdo_track[i])
  );
end
endgenerate

jtag_user_reg #(.P_LENGTH(5*TRACKS+5)
) ConfigReg_I(
  .JTAG( JTAG ),
  .SELECT(select_config_reg),
  .TDO(tdo_config_reg),
  .VAL({track_hold,sel_mon_ddyn,sel_ext,invert_hold,stat_vals_idle,stat_vals_iprog})
);

// this module is fenced in encounter so that it sits close to the output pins
DDYN_OutReg #(
  .WIDTH(TRACKS)
) DDYN_OutReg_I (
  .clock(clock),
  .run700(!init_fast),
  .ext_ddyn(ext_ddyn),
  .sel_stat_vals_iprog(sel_stat_vals || (init_fast && run)),
  .sel_mon(sel_mon_ddyn),
  .stat_vals_idle(stat_vals_idle),
  .stat_vals_iprog(stat_vals_iprog),
  .sel_ext(sel_ext),
  .ddyn_in(int_ddyn),
  .ddyn_out(ddyn),
  .mon_ddyn(mon_ddyn)
);

endmodule

`default_nettype wire
