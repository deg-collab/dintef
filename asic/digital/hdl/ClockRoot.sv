`default_nettype none
`timescale 1ns/1ps
module ClockRoot(
        clock_t.ROOT clock,
        input wire reset_n,
        input wire do_clk_11
    );

reset_synchronizer #( .P_DEPTH( 2 ) ) sync500_I(
        .CLK( clock.clk_500 ),
        .ASYNC_RESET_N( reset_n ),
        .RESET_N( clock.reset_n_500 )
    );

gen_clk10 div_I (
    .clk_in(clock.clk_500),
    .reset_n(clock.reset_n_500),
    .do_clk_11(do_clk_11),
    .clk_by_5(clock.clk_100),
    .clk_by_10(clock.clk_50),
    .clk_by_10_start(clock.clk_50_start)
    );

reset_synchronizer #( .P_DEPTH( 3 ) ) sync100_I(
        .CLK( clock.clk_100 ),
        .ASYNC_RESET_N( reset_n ),
        .RESET_N( clock.reset_n_100 )
    );

reset_synchronizer #( .P_DEPTH( 1 ) ) sync50_I(
        .CLK( clock.clk_50 ),
        .ASYNC_RESET_N( reset_n ),
        .RESET_N( clock.reset_n_50 )
    );

endmodule : ClockRoot
