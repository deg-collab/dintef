`default_nettype none

module SequencerTrack #(
  parameter CNT_WIDTH=4,
  parameter WIDTH=7,
  parameter DEPTH=5
) (
  clock_t.LEAF clock,
  input  wire init,
  input  wire init_shift,
  input  wire init_fast,
  input  wire hold,
  input  wire invert_hold,
  input  wire ld_next_part_seq,
  output wire ddyn,

  JTAG_internal.register JTAG,
  input  wire select,
  output wire tdo
);

wire [WIDTH-1:0] next_part_seq;
reg  [WIDTH-1:0] curr_part_seq;
reg  [WIDTH-1:0] sr;
wire [CNT_WIDTH-1:0] next_reps;
wire int_shift_part_seqs, int_init, get_next_reps, int_hold;
reg  int_hold_reged;

assign int_shift_part_seqs = (get_next_reps) || init_shift;
assign int_init = init && !init_shift;
assign int_hold = hold ^ invert_hold;

SeqSlowShiftReg #(
  .DEPTH(DEPTH),
  .WIDTH(WIDTH+CNT_WIDTH)
) SeqSlowShiftReg_I (
  .clock(clock),
  .init(int_init),
  .shift(int_shift_part_seqs),
  .next_part_seq({next_part_seq,next_reps}),
  .JTAG(JTAG),
  .select(select),
  .tdo(tdo)
);

SeqRepCnt #(
  .CNT_WIDTH(CNT_WIDTH) 
) SeqRepCnt_I ( 
  .clock(clock),
  .init(init),
  .next_reps(next_reps),
  .get_next_reps(get_next_reps),
  .hold(int_hold)
);

always @(posedge clock.clk_100)
  curr_part_seq <= (init || get_next_reps) ? next_part_seq : curr_part_seq;

always @(posedge clock.clk_500) begin
  // hold -> sr is a critical timing path in the design, register to hide xor with invert_hold
  int_hold_reged <= int_hold;
  if (ld_next_part_seq && !int_hold_reged || init_fast)
    sr <= curr_part_seq;
  else 
    sr <= {sr[WIDTH-2:0],sr[WIDTH-1]};
end

assign ddyn = sr[WIDTH-1];

endmodule

`default_nettype wire
