`default_nettype none
`timescale 1ns/1ps
module CycleCnt #(
  parameter P_NUM_BITS = 12
) (
  clock_t.LEAF clock,
  input wire en,
  input wire hold,
  input wire [P_NUM_BITS-1:0] cycle_length,
  output logic cycle_done
);

logic [P_NUM_BITS-1:0] cnt;
logic en_regged;

always_ff @(posedge clock.clk_500 or negedge clock.reset_n_500) begin
  if (!clock.reset_n_500) begin
    cnt <= { {(P_NUM_BITS-1){1'b0}}, 1'b1 };
    en_regged <= 1'b0;
  end else begin
    en_regged <= en; // to sync with sequencer
    cnt <= cycle_done ? { {(P_NUM_BITS-1){1'b0}}, 1'b1 }
                      : hold ? cnt
                             : en_regged ? cnt + 1'b1 : cnt;
  end
end

always_comb cycle_done = (cnt == cycle_length);

endmodule

`default_nettype wire
