`timescale 1ns/1ps
`default_nettype none
import Types::*;

module ReadoutSerializer #(
    parameter int P_NUM_CHANNELS = 4
  ) (
    clock_t.LEAF CLOCK,

    input wire [ 7:0 ] SHIFT_IN,
    input wire RO_LOAD,
    input wire [ 1:0 ] RO_ENABLE,

    output logic SEROUT
  );

// number of bits required to store the bytes_left count
localparam int lp_ch_width = $clog2( 2 * P_NUM_CHANNELS + 1 );

logic [ 1:0 ] bytes_per_channel;
always_comb bytes_per_channel = { 1'b0, RO_ENABLE[ 1 ] } + { 1'b0, RO_ENABLE[ 0 ] };
logic [ lp_ch_width-1:0 ] bytes_expected;
always_comb bytes_expected = P_NUM_CHANNELS * bytes_per_channel;
logic [ lp_ch_width-1:0 ] bytes_left;
logic running;
always_comb running = (bytes_left != {lp_ch_width{1'b0}});

logic [ 7:0 ] byte_out;
logic is_k;

serializer ser_I (
    .CLOCK( CLOCK ),
    .ENABLE( 1'b1 ),

    .SEROUT( SEROUT ),

    .BYTE_IN( byte_out ),
    .IS_K( is_k )
  );

// readout sequence: K28.1, data, K28.5 idle, ...
// TODO: is this smart? We might want to perform a dense readout and issue RO_LOAD
// on the last data byte of the previous readout cycle.
// On the other hand, having the byte simplifies synchronization in the receiver.
always_comb if (RO_LOAD && !running) { is_k, byte_out } = { 1'b1, cK28_1_value };
  else if (running) { is_k, byte_out } = { 1'b0, SHIFT_IN };
  else { is_k, byte_out } = { 1'b1, cK28_5_value };

always @(posedge CLOCK.clk_50 or negedge CLOCK.reset_n_50)
begin
  if (!CLOCK.reset_n_50) begin
    bytes_left <= {lp_ch_width{1'b0}};
  end
  else
  begin
    if (RO_LOAD) begin
      bytes_left <= bytes_expected;
    end
    else begin
      if (running) bytes_left <= bytes_left - 1'd1;
    end
  end
end

// when the count changes from 1 to 0, we should see 8'hFF
assert property (@(posedge CLOCK.clk_50) (((bytes_left == {{lp_ch_width-1{1'b0}}, 1'b1}) && !RO_LOAD) |=> (SHIFT_IN == 8'hFF)));
// K28.5 is followed by K28.5 or K28.1
assert property (@(posedge CLOCK.clk_50) (({is_k, byte_out} == {1'b1, cK28_5_value}) |=> (is_k == 1'b1)));
// K28.1 precedes the actual data
assert property (@(posedge CLOCK.clk_50) (({is_k, byte_out} == {1'b1, cK28_1_value}) |=> (is_k == 1'b0)));

endmodule : ReadoutSerializer
`default_nettype wire
