`default_nettype none
module saturating_counter #(
      parameter P_WIDTH = 8
   ) (
      input wire CLK,
      input wire RESET_N,
      input wire CLEAR,
      input wire ENABLE,

      output logic [ P_WIDTH-1:0 ] COUNT,
      output logic OVERFLOW
   );

always_ff @(posedge CLK or negedge RESET_N)
begin
   if (!RESET_N)
   begin
      COUNT <= {P_WIDTH{1'b0}};
      OVERFLOW <= 1'b0;
   end
   else
   begin
      if (CLEAR)
      begin
         COUNT <= {P_WIDTH{1'b0}};
         OVERFLOW <= 1'b0;
      end
      else if (ENABLE)
      begin
         if (COUNT != {P_WIDTH{1'b1}})
         begin
            COUNT <= COUNT + 1'd1;
         end
         else
         begin
            OVERFLOW <= 1'b1;
         end
      end
   end
end

endmodule
`default_nettype wire
