`default_nettype none

module SeqRepCnt #(
  parameter CNT_WIDTH=3
) (
  clock_t.LEAF clock,
  input wire init,
  input wire hold,
  input wire [CNT_WIDTH-1:0] next_reps,
  output wire get_next_reps
);

reg  [CNT_WIDTH-1:0] rep_cnt;

assign get_next_reps = (rep_cnt == {CNT_WIDTH{1'b0}}) && !hold && !init;

always @(posedge clock.clk_100 or negedge clock.reset_n_100)
  if (!clock.reset_n_100) begin
    rep_cnt       <= {CNT_WIDTH{1'b0}};
  end
  else begin
    rep_cnt       <= init ? next_reps : hold ? rep_cnt : (get_next_reps ? next_reps : rep_cnt - 1'b1);
  end

endmodule

`default_nettype wire
