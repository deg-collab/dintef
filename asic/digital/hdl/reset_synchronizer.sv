`default_nettype none
// from http://www.trilobyte.com/pdf/CummingsSNUG2003Boston_Resets_rev1_2.pdf
module reset_synchronizer #(
      parameter int P_DEPTH = 1
   ) (
      input wire CLK,
      input wire ASYNC_RESET_N,

      output logic RESET_N
   );

logic [ P_DEPTH-1:0 ] rff;

always_ff @( posedge CLK or negedge ASYNC_RESET_N )
begin
   if ( !ASYNC_RESET_N )
   begin
      RESET_N <= 1'b0;
      rff <= {P_DEPTH{1'b0}};
   end
   else
   begin
      rff[ 0 ] <= 1'b1;
      if (P_DEPTH > 1)
      begin
         rff[ P_DEPTH-1:1 ] <= rff[ P_DEPTH-2:0 ];
      end
      RESET_N <= rff[ P_DEPTH-1 ];
   end
end

endmodule
`default_nettype wire
