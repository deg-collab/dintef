`default_nettype none
module gen_clk10 (
      input wire clk_in,
      input wire reset_n,
      input wire do_clk_11,

      output logic clk_by_5,
      output logic clk_by_10,
      output logic clk_by_10_start
   );

logic [ 3:0 ] cnt10;
logic clk11_sync;
logic wrap_clk;

always_ff @(posedge clk_in or negedge reset_n)
begin
   if (!reset_n)
   begin
      cnt10 <= 4'd0;
      clk_by_10_start <= 1'b0;
      wrap_clk <= 1'b0;
      clk_by_10 <= 1'b0;
      clk_by_5 <= 1'b0;
   end
   else
   begin
      clk_by_10 <= cnt10 >= 4'd5;
      clk_by_10_start <= cnt10 == 4'd5;

      wrap_clk <= (clk11_sync && (cnt10 == 4'd9))
         || (!clk11_sync && (cnt10 == 4'd8));

      if (wrap_clk) cnt10 <= 4'd0;
      else cnt10 <= cnt10 + 4'd1;

      clk_by_5 <= (cnt10 <= 4'd2) ||((cnt10 >= 4'd5) && (cnt10 <= 4'd7));
   end
end

always_ff @(posedge clk_in)
begin
   clk11_sync <= do_clk_11;
end

endmodule
`default_nettype wire
