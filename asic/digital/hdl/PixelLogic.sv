`default_nettype none

module PixelLogic #(
  parameter P_COARSE_BITS = 10,
  parameter P_FINE_BITS = 6
) (
  clock_t.LEAF clock,
  input wire cycle_done,
  input wire en_fine_cnt,
  input wire en_coarse_cnt,
  input wire ser_in,
  input wire en,
  output wire ser_out,
  input wire [P_COARSE_BITS+P_FINE_BITS-1 : 0] test_pattern
);

reg [P_COARSE_BITS+P_FINE_BITS-1:0] out_sr;
logic [P_COARSE_BITS-1:0] coarse_cnt;
logic [P_FINE_BITS-1:0] fine_cnt;

reg cycle_done_regged;
wire cycle_done_falling;

assign cycle_done_falling = cycle_done_regged && !cycle_done;

PumpCounter #(.P_BITS(P_COARSE_BITS)) coarse_cnt_I(
    .clock( clock ),
    .en( en ),
    .en_cnt_async( en_coarse_cnt ),
    .clear( cycle_done_falling ),
    .count( coarse_cnt )
  );

PumpCounter #(.P_BITS(P_FINE_BITS)) fine_cnt_I(
    .clock( clock ),
    .en( en ),
    .en_cnt_async( en_fine_cnt ),
    .clear( cycle_done_falling ),
    .count( fine_cnt )
  );

// charge pump counters
always @(posedge clock.clk_500 or negedge clock.reset_n_500) begin
  if (!clock.reset_n_500) begin
    cycle_done_regged <= 1'b0;
  end else if (en) begin
    cycle_done_regged <= cycle_done;
  end
end

//output shift register
always @(posedge clock.clk_100) begin
  // TODO: sync. reset?
  if (!clock.reset_n_100) begin
    out_sr <= test_pattern;
  end else begin
    if (!en) begin
      out_sr <= test_pattern;
    end else if (cycle_done) begin
      out_sr <= {coarse_cnt,fine_cnt};
    end else begin
      out_sr <= {ser_in,out_sr[P_COARSE_BITS+P_FINE_BITS-1:1]};
    end
  end
end

assign ser_out = out_sr[0];

endmodule

`default_nettype wire 
