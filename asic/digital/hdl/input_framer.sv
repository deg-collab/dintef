`default_nettype none
import Types::*;

module input_framer (
      input wire CLK,
      input wire RESET_N,
      input wire WORD_CLK,
      input wire WORD_RESET_N,

      // DATA INPUT
      // 8b10b bitstream received by the ASIC.
      input wire SERIN,

      // DATA OUTPUT
      output decoded_8b10b_t DECODED_IN, // in CLK10 domain

      output logic BAD_WORD_OVERFLOW,
      output logic [ 3:0 ] BAD_WORD_COUNT,
      input wire BAD_WORD_COUNT_RESET
   );

encoded_8b10b_t current_word;
encoded_8b10b_t latched_word;

wire decoder_code_error;
wire decoder_rd_error;

CW_8b10b_dec #(
      .bytes( 1 ),
      .k28_5_only( 0 ),
      .en_mode( 0 )
   ) decoder_I (
      .clk( WORD_CLK ),
      .rst_n( WORD_RESET_N ),
      .enable( 1'b1 ),
      .init_rd_n( 1'b1 ),
      .init_rd_val( 1'b0 ),
      .data_in( latched_word ),
      .rd_err_bus(),
      .code_err_bus(),
      .k_char( DECODED_IN.is_k ),
      .data_out( DECODED_IN.value ),
      .error(),
      .rd(),
      .rd_err( decoder_rd_error ),
      .code_err( decoder_code_error )
   );
assign DECODED_IN.is_valid = (!decoder_code_error) && (!decoder_rd_error);

saturating_counter #(
      .P_WIDTH( 4 )
   )bad_count_I (
      .CLK( WORD_CLK ),
      .RESET_N( WORD_RESET_N ),
      .CLEAR( BAD_WORD_COUNT_RESET ),
      .ENABLE( !DECODED_IN.is_valid ),
      .COUNT( BAD_WORD_COUNT ),
      .OVERFLOW( BAD_WORD_OVERFLOW )
   );

always_ff @(posedge CLK or negedge RESET_N)
begin : shift
   if (!RESET_N) current_word <= 10'b0;
   else current_word <= { SERIN, current_word[ 9:1 ] };
end : shift

always_ff @(posedge WORD_CLK or negedge WORD_RESET_N)
begin : sample
   if (!WORD_RESET_N) latched_word <= 10'b0;
   else
      // this assignment is between the clock domains!
      latched_word <= current_word;
end : sample

endmodule : input_framer
`default_nettype wire
