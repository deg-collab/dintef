`default_nettype none

module SeqSlowShiftReg #(
  parameter DEPTH=4,
  parameter WIDTH=8
)(
  clock_t.LEAF clock,
  input  wire init,
  input  wire shift,
  output wire [WIDTH-1:0] next_part_seq,

  JTAG_internal.register JTAG,
  input  wire select,
  output wire tdo
);

logic [WIDTH-1:0] sr_ser_conn [DEPTH:0];
always_comb sr_ser_conn[0] = sr_ser_conn[DEPTH];

wire [DEPTH-1:0] [WIDTH-1:0] init_val;
jtag_user_reg #(.P_LENGTH(DEPTH * WIDTH)) user_reg_I(
  .JTAG(JTAG),
  .SELECT(select),
  .TDO(tdo),
  .VAL(init_val)
);

genvar i;
generate
  for (i=0; i<DEPTH; i=i+1) begin : SeqPartsShiftReg
    always_ff @(posedge clock.clk_100)
        sr_ser_conn[i+1] <= init ? init_val[i] : (shift ? sr_ser_conn[i] : sr_ser_conn[i+1]);
  end
endgenerate

assign next_part_seq = sr_ser_conn[DEPTH];

endmodule
`default_nettype wire
