`default_nettype none

module PumpCounter #(
    parameter P_BITS = 4
  ) (
    clock_t.LEAF clock,
    input wire en,
    input wire en_cnt_async,
    input wire clear,
    output logic [P_BITS-1:0] count
  );

logic [1:0] en_cnt_mon;
logic en_cnt_sync;
always_ff @(posedge clock.clk_500) en_cnt_mon[1:0] <= {en_cnt_mon[0], en_cnt_async};
always_comb en_cnt_sync = en_cnt_mon == 2'b01;

always @(posedge clock.clk_500 or negedge clock.reset_n_500) begin
  if (!clock.reset_n_500) begin
    count <= {P_BITS{1'b0}};
  end
  else if (en) begin
    if (clear) begin
      // do not lose a count when clear and count come together
      count <= { {P_BITS-1{1'b0}}, en_cnt_sync };
    end
    else begin
      if (en_cnt_sync) begin
        // saturate
        if (count != {P_BITS{1'b1}}) count <= count + 1'd1;
      end
    end
  end
end

endmodule : PumpCounter

`default_nettype wire
