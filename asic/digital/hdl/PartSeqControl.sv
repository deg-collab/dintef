`default_nettype none

module PartSeqControl #(
  parameter HOLD_REG_DEPTH=7,
  parameter HOLD_CNT_WIDTH=10,
  parameter FAST_CNT_BITS=3,
  parameter SEQ_PART_WIDTH=7
)(
  clock_t.LEAF clock,
  input  wire init,
  input  wire init_shift,
  output reg  ld_next_part_seq,
  output wire init_fast,
  output wire hold,

  JTAG_internal.register JTAG,
  input  wire select,
  output wire tdo
);

wire [HOLD_CNT_WIDTH-1:0] next_hold_length;
wire next_hold; 
wire shift_cnts;
reg [FAST_CNT_BITS-1:0] fast_cnt;
wire run;
reg run700;

SeqSlowShiftReg #(
  .DEPTH(HOLD_REG_DEPTH),
  .WIDTH(1 + HOLD_CNT_WIDTH)
) SeqHoldCntShiftReg_I(
  .clock(clock),
  .shift(shift_cnts || init_shift),
  .init(init && !init_shift),
  .next_part_seq({next_hold,next_hold_length}),

  .JTAG( JTAG ),
  .select(select),
  .tdo(tdo)
);

SeqHoldCnt #(
  .HOLD_CNT_WIDTH(HOLD_CNT_WIDTH)
) SeqHoldCnt_I ( 
  .res_n(clock.reset_n_100),
  .clk(clock.clk_100),
  .init(init),
  .ld_next(shift_cnts),
  .next_hold_length(next_hold_length),
  .next_hold(next_hold),
  .hold(hold)
);

assign run = !init;
assign init_fast = !run700;

always @(posedge clock.clk_500 or negedge clock.reset_n_500)
  if (!clock.reset_n_500) begin
    fast_cnt  <= 3'b0;
    run700    <= 1'b0;
    ld_next_part_seq <= 1'b0;
  end else begin
    run700            <= (run && ld_next_part_seq) ? 1'b1 : (!run && ld_next_part_seq ? 1'b0 : run700);
    fast_cnt          <= (run || run700) ? (fast_cnt==(SEQ_PART_WIDTH-1) ? 3'b0 : fast_cnt + 1'b1) : 3'b0;
    ld_next_part_seq  <= /*~hold &&*/ (fast_cnt == 2);
  end

endmodule

`default_nettype wire 
