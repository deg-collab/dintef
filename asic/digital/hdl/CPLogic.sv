`timescale 1ns/1ps
`default_nettype none
module CPLogic(
    input wire CLK,
    input wire ENABLE,
    input wire FROM_COMP,
    output logic PUMP
  );

logic ff, and1, latch;

always_ff @(posedge CLK) ff <= FROM_COMP;
always_comb and1 = FROM_COMP && ff && ENABLE;
always_latch if (!CLK) latch <= and1;
always_comb PUMP = latch && CLK;

endmodule : CPLogic
`default_nettype wire
