`timescale 1ns/1ps
`default_nettype none
import Types::*;
module serializer (
      clock_t.LEAF CLOCK,
      input wire ENABLE,

      output logic SEROUT,

      input wire [ 7:0 ] BYTE_IN,
      input wire IS_K
   );

logic k_char;
logic [ 7:0 ] data_sampled;
wire [ 9:0 ] ebtb_out;

logic [ 9:0 ] shifter;
always_comb SEROUT = shifter[ 0 ];

CW_8b10b_enc #(
      .bytes( 1 ),
      .k28_5_only( 0 ),
      .en_mode( 0 ),
      .rst_mode( 0 )
   ) eight_b_ten_b_I (
      .clk( CLOCK.clk_50 ),
      .rst_n( CLOCK.reset_n_50 ),
      .init_rd_n( 1'b1 ),
      .init_rd_val( 1'b0 ),
      .k_char( k_char ),
      .data_in( data_sampled ),
      .rd(),
      .data_out( ebtb_out ),
      .enable( ENABLE )
   );

always_ff @(posedge CLOCK.clk_50)
begin : provide_word
   data_sampled <= BYTE_IN;
   k_char <= IS_K;
end

always_ff @(posedge CLOCK.clk_500 or negedge CLOCK.reset_n_500)
begin : shift
   if (!CLOCK.reset_n_500)
   begin
      // hold the output at well-defined 0 during the reset
      shifter <= 10'bXXXXXXXXX0;
   end
   else
   begin
      if (CLOCK.clk_50_start)
      begin
         shifter <= ebtb_out;
      end
      else
      begin
         shifter <= { 1'bX, shifter[ 9:1 ] };
      end
   end
end

endmodule : serializer
`default_nettype wire
