`default_nettype none

module DDYN_OutReg #(
  parameter WIDTH = 4
) (
  clock_t.LEAF clock,
  input wire [4:0] sel_mon,
  input wire run700,
  input wire ext_ddyn,
  input wire sel_stat_vals_iprog,
  input wire [WIDTH-1:0] stat_vals_idle,
  input wire [WIDTH-1:0] stat_vals_iprog,
  input wire [WIDTH-1:0] ddyn_in,
  input wire [WIDTH-1:0] sel_ext,
  output wire [WIDTH-1:0] ddyn_out,
  output wire mon_ddyn
);

reg [WIDTH-1:0] ddyn_reg;
always @(posedge clock.clk_500) begin
  ddyn_reg <= run700 ? ddyn_in : (sel_stat_vals_iprog ? stat_vals_iprog : stat_vals_idle);
end

genvar i;
generate
  for (i=0; i<WIDTH; i=i+1) begin : OutMux 
    assign ddyn_out[i] = sel_ext[i] && run700 ? ext_ddyn : ddyn_reg[i];
  end
endgenerate

assign mon_ddyn = sel_mon[4] ? ddyn_out[sel_mon[3:0]] : 1'b0;

endmodule

`default_nettype wire
