`default_nettype none
`timescale 1ns/1ps
import Types::*;

module ControlBlock (
   //jtag io signals
   input  wire tms,
   input  wire tck,
   input  wire tdi,
   output wire tdo,
   // tri-state output enable for TDO
   output wire tdo_enable,

   output wire [cNUM_DACS*cDACBITS-1 : 0] dstat_dacs,
   output wire [cNUM_DACS*cDACBITS-1 : 0] dstat_dacs_n,
   output wire [cNUM_GLBITS-1 : 0] dstat_gl,

   input wire  res_n,
   input wire  por_n,
   input wire  clk,
   input wire  seq_run,
   input wire  ext_ddyn,
   output logic [cNUM_DDYN_GROUPS*cNUM_DDYN-1 : 0] ddyn,
   output logic [cNUM_DDYN_GROUPS*2-1 : 0] ddyn_b,

   input wire [cNUM_PX_LOGIC-1 : 1] cp_en_cnt_coarse,
   input wire [cNUM_PX_LOGIC-1 : 1] cp_en_cnt_fine,

   // for channel 0, the logic has been moved to the digital domain
   input wire [0:0] comp_result_coarse,
   output logic [0:0] pump_coarse,
   input wire [0:0] comp_result_fine,
   output logic [0:0] pump_fine,

   output wire [1:0] ser_out,
   output wire mon,

   input wire cmd_in,
   output wire data_out,

   output wire [ cNUM_PX_LOGIC*8-1:0 ] RAM_address,
   output wire [ cNUM_PX_LOGIC*2-1:0 ] RAM_write,
   output wire [ cNUM_PX_LOGIC*2-1:0 ] RAM_read,
   output wire [ cNUM_PX_LOGIC*16-1:0 ] RAM_wdata,
   input wire [ cNUM_PX_LOGIC*16-1:0 ] RAM_rdata,
   output logic [ 7:0 ] RAM_bias

`ifdef FULLCHIP_MIXED_MODE // not working in amsd block
  ,
  input wire VDD,
  input wire VSS
`endif
);

wire trst_n = 1'b1;

clock_t clock(
    .clk_500( clk )
  );

JTAG_internal JTAG_INT(
    .TCK( tck ),
    .TDI( tdi ),
    .POR_N( por_n )
  );

JTAG JTAG_EXT(
    .TCK( tck ),
    .TMS( tms ),
    .TDI( tdi ),
    .TDO( tdo ),
    .TDO_ENABLE( tdo_enable ),
    .TRST_N( trst_n ),
    .POR_N( por_n )
  );

global_reg_t global_conf;

assign dstat_gl = global_conf.dstat_gl;

assign dstat_dacs_n = ~dstat_dacs;

always_comb RAM_bias = global_conf.RAM_bias;

logic ddyn_b_int[ 1:0 ];

logic [cSEQ_TRACKS-1:0] seq_tracks;
generate for (genvar g = 0; g < cNUM_DDYN_GROUPS; g++) begin : gengroups
   for (genvar s = 0; s < cNUM_DDYN; s++) begin : genbufddyn
      CKBD16 bufddyn(.I(seq_tracks[s]), .Z(ddyn[g * cNUM_DDYN + s]));
   end
   CKBD16 bufddyn0_b(.I(ddyn_b_int[0]), .Z(ddyn_b[g * 2 + 0]));
   CKBD16 bufddyn1_b(.I(ddyn_b_int[1]), .Z(ddyn_b[g * 2 + 1]));
end
endgenerate

// the two reset signals also have an inverted version used to compensate charge injection.
// => tie to 1 to disable this compensation
always_comb ddyn_b_int[ 0 ] = !seq_tracks[ 0 ] || !global_conf.en_reset_compensation_0;
always_comb ddyn_b_int[ 1 ] = !seq_tracks[ 1 ] || !global_conf.en_reset_compensation_1;

wire sel_seq_config_reg, sel_seq_reccnts;
wire [cSEQ_TRACKS-1 : 0] sel_seq_track;
wire tdo_seq_config_reg, tdo_seq_reccnts;
wire [cSEQ_TRACKS-1 : 0] tdo_seq_track;
wire sel_gl_reg, sel_dac_reg;
wire tdo_gl_reg, tdo_dac_reg;
logic do_clk_11;

logic [cNUM_PX_LOGIC-1 : 0] cp_en_cnt_coarse_int;
always_comb cp_en_cnt_coarse_int[0] = pump_coarse[0];
always_comb cp_en_cnt_coarse_int[cNUM_PX_LOGIC-1 : 1] = cp_en_cnt_coarse[cNUM_PX_LOGIC-1 : 1];
logic [cNUM_PX_LOGIC-1 : 0] cp_en_cnt_fine_int;
always_comb cp_en_cnt_fine_int[0] = pump_fine[0];
always_comb cp_en_cnt_fine_int[cNUM_PX_LOGIC-1 : 1] = cp_en_cnt_fine[cNUM_PX_LOGIC-1 : 1];

wire cycle_done;
reg seq_run_regged, seq_init, seq_run_int;
wire seq_hold;

decoded_8b10b_t decoded;
pixel_control_t pixel_control;
subframe_state_t sf_state;

logic res_n_int;

ClockRoot clkroot_I(
    .clock( clock ),
    .reset_n( res_n_int ),
    // do_clk_11 also disturbs clk_5, so make sure we can disable it
    .do_clk_11( do_clk_11 && global_conf.en_pixel_control )
  );

wire mon_ddyn;
wire [ 6 + 2*cNUM_PX_LOGIC - 1 : 0 ] signals2mon;
assign signals2mon = {cp_en_cnt_coarse_int, cp_en_cnt_fine_int, cycle_done, mon_ddyn, seq_run_int, 1'b1, 1'b0, por_n};
assign mon = global_conf.mon_sel >= $bits(signals2mon) ? 1'b0 : signals2mon[global_conf.mon_sel];

always @(posedge clock.clk_50 or negedge clock.reset_n_50) begin
  if (!clock.reset_n_50) begin
    seq_run_regged <= 1'b0;
    seq_init <= 1'b0;
    seq_run_int <= 1'b0;
  end else begin
    // ensure a fixed phase relationship between seq_init and the readout, when sync_seq_run is set.
    // this might not be the most convenient phase, but at least it's fixed and well-known.
    if (global_conf.sync_seq_run) begin
      if (pixel_control.subframe_start) begin
        seq_run_regged <= seq_run;
      end
      // else do not sample...
    end
    else begin
      // always sample
      seq_run_regged <= seq_run;
    end
    seq_init       <= !seq_run_regged && seq_run;
    seq_run_int    <= seq_init ? 1'b1 : !seq_run ? 1'b0 : seq_run_int;
  end
end

jtag_tap_ctrl tap_I( .* );

jtag #(
  .P_NUM_USER_REGS(cSEQ_TRACKS + 4),
  .P_ID_VERSION(4'h3),
  .P_IC_RESET_COUNT(1),
  .P_ID_PART(16'h0201),
  .P_ID_MANUFACTURER_BANK(4'd9),
  .P_ID_MANUFACTURER(7'd87)
) jtag_I (
  .JTAG_EXT( JTAG_EXT ),
  .JTAG_INT( JTAG_INT ),

  .SEL_REG( {
      sel_dac_reg,        // 1
      sel_gl_reg,         // 1
      sel_seq_config_reg, // 1
      sel_seq_reccnts,   // 1
      sel_seq_track       // 12
      }),
  .TDO_REG( {
      tdo_dac_reg,
      tdo_gl_reg,
      tdo_seq_config_reg,
      tdo_seq_reccnts,
      tdo_seq_track
      }),
  .IC_RESET_IN_N( res_n ),
  .IC_RESET_OUT_N( res_n_int ),
  .IR_DATA(3'b000) // TODO, read back something else?
);

jtag_user_reg_por #(
  .P_LENGTH($bits(global_conf)),
  .P_INIT_VALUE({$bits(global_conf){1'b0}})
) user_reg_global_reg_I (
  .JTAG( JTAG_INT ),
  .SELECT(sel_gl_reg),
  .TDO(tdo_gl_reg),
  .VAL(global_conf)
);


jtag_user_reg_por #(
  .P_LENGTH(cNUM_DACS*cDACBITS),
  .P_INIT_VALUE(cDAC_reg_init)
) user_reg_dac_reg_I (
  .JTAG( JTAG_INT ),
  .SELECT(sel_dac_reg),
  .TDO(tdo_dac_reg),
  .VAL(dstat_dacs)
);


CycleCnt #(
  .P_NUM_BITS(cCYCLELENGTH_BITS)
) CycleCnt_I (
  .clock(clock),
  .en(seq_run_int),
  .hold(seq_hold),
  .cycle_length(global_conf.conf_cycle_length),
  .cycle_done(cycle_done)
  );

Sequencer_SuS #(
  .TRACKS(cSEQ_TRACKS), // 2xEnCP, 2xReset, SwGainCap, Clk
  .SEQ_PART_WIDTH(5)
) Sequencer_I (
  .clock(clock),
  .run(seq_run_int),
  .init_shift(seq_init),
  .sel_stat_vals(1'b0),

  .ext_ddyn(ext_ddyn),
  .mon_ddyn(mon_ddyn),
  .ddyn(seq_tracks),
  .hold(seq_hold),
  
  .JTAG( JTAG_INT ),
  .select_config_reg(sel_seq_config_reg),
  .select_rep_cnts(sel_seq_reccnts),
  .select_track(sel_seq_track),
  .tdo_config_reg(tdo_seq_config_reg),
  .tdo_rep_cnts(tdo_seq_reccnts),
  .tdo_track(tdo_seq_track)
);

// default: same logic as in the analog part ("auto")
// override: from two dedicated sequencer tracks
logic pump_coarse_auto[ 0:0 ];
logic pump_fine_auto[ 0:0 ];
always_comb pump_fine[ 0 ] = global_conf.en_channel0_digital_ctrl ? seq_tracks[cSEQ_TRACKS-1] : pump_fine_auto[ 0 ];
always_comb pump_coarse[ 0 ] = global_conf.en_channel0_digital_ctrl ? seq_tracks[cSEQ_TRACKS-2] : pump_coarse_auto[ 0 ];

CPLogic cp_coarse_0(
    .CLK( seq_tracks[ 5 ] ),
    .ENABLE( seq_tracks[ 3 ] ),
    .FROM_COMP( comp_result_coarse[ 0 ] ),
    .PUMP( pump_coarse_auto[ 0 ] )
  );

CPLogic cp_fine_0(
    .CLK( seq_tracks[ 5 ] ),
    .ENABLE( seq_tracks[ 2 ] ),
    .FROM_COMP( comp_result_fine[ 0 ] ),
    .PUMP( pump_fine_auto[ 0 ] )
  );

wire [cNUM_PX_LOGIC : 0] ser_conn;

genvar i;
generate
  for (i=0; i<cNUM_PX_LOGIC; i=i+1) begin : PixelLogic 
    PixelLogic #(
      .P_COARSE_BITS(cCOARSE_BITS),
      .P_FINE_BITS(cFINE_BITS)
    ) PixelLogic_I (
      .clock(clock),
      .en(seq_run_int),
      .cycle_done(cycle_done),
      .en_coarse_cnt(cp_en_cnt_coarse_int[i]),
      .en_fine_cnt(cp_en_cnt_fine_int[i]),
      .ser_in(ser_conn[i+1]),
      .ser_out(ser_conn[i]),
      .test_pattern(global_conf.conf_test_pattern)
    );
  end
endgenerate

assign ser_conn[cNUM_PX_LOGIC] = global_conf.conf_ser_in;
assign ser_out[0] = ser_conn[0];
assign ser_out[1] = ser_conn[2];

input_framer framer_I(
      .CLK( clock.clk_500 ),
      .RESET_N( clock.reset_n_500 ),
      .WORD_CLK( clock.clk_50 ),
      .WORD_RESET_N( clock.reset_n_50 ),

      .SERIN( cmd_in ),
      .DECODED_IN( decoded ),

      .BAD_WORD_COUNT(),
      .BAD_WORD_COUNT_RESET( do_clk_11 ),
      .BAD_WORD_OVERFLOW( do_clk_11 )
   );

CommandReceiver recv_I(
    .CLOCK( clock ),
    .WORD( decoded ),
    .PIXEL_CONTROL( pixel_control )
  );

SubframeState state_I(
    .CLOCK( clock ),
    .SUBFRAME_START( pixel_control.subframe_start ),
    .SUBFRAME_STATE( sf_state )
  );

logic [cNUM_PX_LOGIC:0][7:0] par_shifter;
always_comb par_shifter[cNUM_PX_LOGIC] = 8'hFF; // the most unlikely value...

generate
  for (genvar i=0; i<cNUM_PX_LOGIC; i=i+1) begin : PixelControl
    RAM_control_t RAM(
      .address( RAM_address[ 8*i +: 8 ] ),
      .write( RAM_write[ 2*i +: 2 ] ),
      .read( RAM_read[ 2*i +: 2 ] ),
      .wdata( RAM_wdata[ 16*i +: 16 ] ),
      .rdata( RAM_rdata[ 16*i +: 16 ] )
    );

    PixelControl #(
      .P_NUM_FF_ADDRESSES( i == 7 ? 9'd255 : 9'd1 )
    ) Ctrl_I(
      .CLOCK( clock ),
      .STATE( sf_state ),
      .CONV( global_conf.conversion ),
      .RO_SHIFT( 1'b1 ),
      .PIXEL_CONTROL( pixel_control ),
      .RO_SHIFT_IN( par_shifter[i + 1] ),
      .RO_SHIFT_OUT( par_shifter[i] ),
      .EN_COUNTERS( seq_run_int ),
      .EN_COARSE_CNT( cp_en_cnt_coarse_int[i] ),
      .EN_FINE_CNT( cp_en_cnt_fine_int[i] ),
      .RAM( RAM )
    );
  end
endgenerate

// for now, simulate one channel
ReadoutSerializer #(
    .P_NUM_CHANNELS( cNUM_PX_LOGIC )
  ) RO_I (
      .CLOCK( clock ),
      .SHIFT_IN( par_shifter[ 0 ] ),
      .RO_LOAD( pixel_control.ro_load ),
      .RO_ENABLE( pixel_control.subframe_cmd.ro_enable ),
      .SEROUT( data_out )
  );

endmodule

`default_nettype wire 
