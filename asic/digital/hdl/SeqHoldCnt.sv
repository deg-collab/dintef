`default_nettype none

module SeqHoldCnt #(
  parameter HOLD_CNT_WIDTH=10
) (
  input wire res_n,
  input wire clk,
  input wire init,
  output wire ld_next,
  input wire [HOLD_CNT_WIDTH-1:0] next_hold_length,
  input wire next_hold,
  output reg hold
);

reg [HOLD_CNT_WIDTH-1:0] hold_cnt;

assign ld_next = (hold_cnt == {HOLD_CNT_WIDTH{1'b0}}) && !init;

// CAREFUL: THIS HAS BEEN ADDED FOR SIMULATION REASONS. HOLD GENERATOR IS NO LONGER IN USE AND SHOULD BE EXCLUDED
`ifdef SIMULATION
always @(posedge clk) begin
    hold <= 1'b0;
end
`else
always @(posedge clk or negedge res_n)
  if (!res_n) begin
    //ld_next   <= 1'b0;
    hold      <= 1'b0;
    hold_cnt  <= {HOLD_CNT_WIDTH{1'b0}};
  end else begin
    //ld_next   <= init || (ld_next ? (next_hold_length[HOLD_CNT_WIDTH-1:0] == {HOLD_CNT_WIDTH{1'b0}}) : (hold_cnt == 1));
    hold      <= (init || ld_next) ? next_hold : hold;
    hold_cnt  <= (init || ld_next) ? next_hold_length : hold_cnt - 1'b1;
  end
`endif
endmodule

`default_nettype wire
