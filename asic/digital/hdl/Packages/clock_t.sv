`timescale 1ns/1ps
interface clock_t(
        input wire clk_500
    );
    logic reset_n_500;
    // must be clk_500 divided by SEQ_PART_WIDTH
    bit clk_100;
    logic reset_n_100;
    bit clk_50;
    logic reset_n_50;
    logic clk_50_start;

    modport ROOT(
            input clk_500,
            output reset_n_500,
            output clk_100,
            output reset_n_100,
            output clk_50,
            output reset_n_50,
            output clk_50_start
        );

    modport LEAF(
            input clk_500,
            input reset_n_500,
            input clk_100,
            input reset_n_100,
            input clk_50,
            input reset_n_50,
            input clk_50_start
        );
endinterface : clock_t