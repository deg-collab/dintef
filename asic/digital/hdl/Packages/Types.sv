`default_nettype none
package Types;

localparam int cNUMMUXES = 6;
localparam int cMUXBITS = 5;
localparam int cNUM_PX_LOGIC = 16;
localparam int cCOARSE_BITS = 10;
localparam int cFINE_BITS = 6;
localparam int cNUM_PX_PADS = 28;
localparam int cNUM_GLBITS = 40;
localparam int cNUM_DACS = 20;
localparam int cDACBITS = 10;
localparam int cCYCLELENGTH_BITS = 13;
localparam int cSEQ_TRACKS = 12;
localparam int cNUM_DDYN = 10;
localparam int cMON_MUX_SELBITS = 6;
// grouping for the output driver cells
localparam int cNUM_DDYN_GROUPS = 4;

localparam int cRAM_addr_width = 8;

typedef struct packed {
        logic saturate_addition;
        // coarse gain 4 / 8 / 16 / 32 / 64. higher values truncate the coarse count
        logic [ 2:0 ] coarse_gain;
        // 6..5
        logic [ 1:0 ] LSB_correction;
        // 4..3
        logic [ 1:0 ] LSB_discard;
        // 2
        logic enable_limit;
        // 1..0
        logic [ 1:0 ] limit; // limit to (1 << (limit + 5)) - 1, i.e. 31, 63, 127, 255
    } conversion_settings_t;

typedef struct packed {
    logic en_channel0_digital_ctrl;
    logic [ 7:0 ] RAM_bias;
    conversion_settings_t conversion;
    logic en_pixel_control;
    logic en_reset_compensation_1;
    logic en_reset_compensation_0;
    logic sync_seq_run;
    // serial input for the pixel logic
    logic conf_ser_in;
    // if 0, all dstat_mux are assinged 0 to tristate the pixel input
    logic conf_en_muxes;
    logic [ cMON_MUX_SELBITS-1 : 0 ] mon_sel;
    logic [ cCYCLELENGTH_BITS-1 : 0 ] conf_cycle_length;
    logic [ cFINE_BITS+cCOARSE_BITS-1 : 0 ] conf_test_pattern;
    logic [ cNUM_GLBITS-1 : 0 ] dstat_gl;
} global_reg_t;

typedef struct packed {
    /* 19 */ logic [ cDACBITS-1:0 ] unused1;
    /* 18 */ logic [ cDACBITS-1:0 ] unused0;
    /* 17 */ logic [ cDACBITS-1:0 ] V_AmpVC1;
    /* 16 */ logic [ cDACBITS-1:0 ] V_Inject1;
    /* 15 */ logic [ cDACBITS-1:0 ] V_REF;
    /* 14 */ logic [ cDACBITS-1:0 ] V_Vthresh1;
    /* 13 */ logic [ cDACBITS-1:0 ] I_CP_N1;
    /* 12 */ logic [ cDACBITS-1:0 ] I_16Comp_80u_P;
    /* 11 */ logic [ cDACBITS-1:0 ] I_AmpTail_P1;
    /* 10 */ logic [ cDACBITS-1:0 ] I_AmpTail_P0;
    /*  9 */ logic [ cDACBITS-1:0 ] I_AmpLegs_N1;
    /*  8 */ logic [ cDACBITS-1:0 ] I_AmpLegs_N0;
    /*  7 */ logic [ cDACBITS-1:0 ] I_MonOut_Tail_P;
    /*  6 */ logic [ cDACBITS-1:0 ] V_Inject0;
    /*  5 */ logic [ cDACBITS-1:0 ] I_FEOut_Tail_P;
    /*  4 */ logic [ cDACBITS-1:0 ] V_AmpVC0;
    /*  3 */ logic [ cDACBITS-1:0 ] V_Vthresh0;
    /*  2 */ logic [ cDACBITS-1:0 ] I_CP_N0;
    /*  1 */ logic [ cDACBITS-1:0 ] I_16Buf_80u_P;
    /*  0 */ logic [ cDACBITS-1:0 ] I_Mon;
} DAC_reg_t;

localparam DAC_reg_t cDAC_reg_init = '{
    unused1         : {cDACBITS{1'b0}},
    unused0         : {cDACBITS{1'b0}},
    I_16Buf_80u_P   : {cDACBITS{1'b1}},
    I_16Comp_80u_P  : {cDACBITS{1'b1}},
    I_AmpLegs_N0    : {cDACBITS{1'b0}},
    I_AmpLegs_N1    : {cDACBITS{1'b0}},
    I_AmpTail_P0    : {cDACBITS{1'b1}},
    I_AmpTail_P1    : {cDACBITS{1'b1}},
    I_CP_N0         : {cDACBITS{1'b0}},
    I_CP_N1         : {cDACBITS{1'b0}},
    I_FEOut_Tail_P  : {cDACBITS{1'b1}},
    I_Mon           : {cDACBITS{1'b1}},
    I_MonOut_Tail_P : {cDACBITS{1'b1}},
    V_AmpVC0        : {cDACBITS{1'b0}},
    V_AmpVC1        : {cDACBITS{1'b0}},
    V_Inject0       : {cDACBITS{1'b0}},
    V_Inject1       : {cDACBITS{1'b0}},
    V_REF           : {cDACBITS{1'b0}},
    V_Vthresh0      : {cDACBITS{1'b0}},
    V_Vthresh1      : {cDACBITS{1'b0}}
};

localparam int cIRLength = 5;

typedef enum logic [ cIRLength-1:0 ] {
      //general instructions
      CMD_IDCODE           = {cIRLength{1'd0}},
      CMD_SAMPLE_PRELOAD   = 5'd1,
      CMD_EXTEST           = 5'd2,
      CMD_IC_RESET         = 5'd3,
      //first instruction to write a registers. the others will increment from here.
      CMD_SEL_REG_BASE     = 5'd6,
      //fixed all-1
      CMD_BYPASS           = {cIRLength{1'b1}}
   } IR_code_t;

typedef struct packed {
      logic is_k;
      logic is_valid;
      logic [ 7:0 ] value;
   } decoded_8b10b_t;

localparam [ 7:0 ] cK28_1_value = 8'h3C;
localparam [ 7:0 ] cK28_5_value = 8'hBC;

localparam decoded_8b10b_t cK28_1_decoded = '{
        is_k : 1'b1,
        is_valid : 1'b1,
        value : cK28_1_value
    };

localparam decoded_8b10b_t cK28_5_decoded = '{
        is_k : 1'b1,
        is_valid : 1'b1,
        value : cK28_5_value
    };

typedef logic [ 9:0 ] encoded_8b10b_t;

typedef enum logic [ 3:0 ] {
        S_CALCULATE    = 4'b0001,
        S_READ_RAM     = 4'b0010,
        S_WRITE_RAM    = 4'b0100,
        S_READOUT      = 4'b1000
    } subframe_state_t;

// bit [ 2 ] signals if the earlier RAM content is required
typedef enum logic [ 2:0 ] {
        F_NOP       = 3'b000,
        // store to RAM address
        F_STORE     = 3'b001,
        // clear (set to 0) RAM address
        F_CLEAR     = 3'b010,
        // add at RAM address
        F_ADD       = 3'b100,
        // subtract at RAM address
        F_SUBTRACT  = 3'b101,
        // increment at RAM address
        F_INCREMENT = 3'b110
    } function_t;

function logic NeedsRAMRead( input function_t f );
begin
    NeedsRAMRead = f[ 2 ];
end
endfunction : NeedsRAMRead

typedef enum logic [ 1:0 ] {
        WORD_LOW  = 2'b01,
        WORD_HIGH = 2'b10,
        WORD_FULL = 2'b11
    } word_sel_t;

/// processing instructions for one subframe's data
typedef struct packed {
        // 15..14
        logic [ 1:0 ] ro_enable;
        // 13..6
        logic [ 7:0 ] RAM_address;
        // 5
        logic RAM_address_is_relative;
        // 4..3
        word_sel_t word_sel;
        // 2..0
        function_t f;
    } subframe_cmd_t;

typedef struct packed {
        logic subframe_start;
        subframe_cmd_t subframe_cmd;

        logic ro_load;
        logic [ cRAM_addr_width-1:0 ] ro_address;
    } pixel_control_t;

endpackage : Types
`default_nettype wire
