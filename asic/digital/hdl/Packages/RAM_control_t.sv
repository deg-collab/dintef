`timescale 1ns/1ps
interface RAM_control_t(
    output logic [ 7:0 ] address,
    output logic [ 1:0 ] write,
    output logic [ 1:0 ] read,
    output logic [ 15:0 ] wdata,
    input wire [ 15:0 ] rdata
  );

  modport Controller(
    output address,
    output write,
    output read,
    output wdata,
    input rdata
  );
endinterface : RAM_control_t
