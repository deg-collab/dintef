/**********************************************************************************
*                                                                                 *
*  JTAG Instruction Register                                                      *
*                                                                                 *
**********************************************************************************/

`default_nettype none

module jtag_ir #(
      parameter P_IR_LENGTH = 5,
      parameter [ P_IR_LENGTH-1:0 ] P_CMD_IDCODE = {P_IR_LENGTH{1'b0}}
   ) (
     JTAG_internal.register JTAG,
     output wire TDO,
     output reg [ P_IR_LENGTH-1:0 ] LATCHED_JTAG_IR,
     // implementation defined, 7.1.1 Recommendation e), Permission f)
     input wire [ P_IR_LENGTH-3:0 ] IR_DATA
   );

reg [ P_IR_LENGTH-1:0 ]  jtag_ir; // "shift-register stage"

// shift stage
always @( posedge JTAG.TCK )
begin
   if (JTAG.STATE_CAPTURE_IR) begin
      // IEEE Std 1149.1TM-2013 7.1.1 Rule d)
      jtag_ir[ 1:0 ] <= 2'b01;
      if (P_IR_LENGTH > 2) begin
         jtag_ir[ P_IR_LENGTH-1:2 ] <= IR_DATA;
      end
   end
   else if (JTAG.STATE_SHIFT_IR)
   begin
      jtag_ir[ P_IR_LENGTH-1:0 ] <= {JTAG.TDI, jtag_ir[ P_IR_LENGTH-1:1 ]};
   end
end

// update stage
always @( negedge JTAG.TCK, negedge JTAG.TAP_RESET_N )
begin
   if (!JTAG.TAP_RESET_N)
   begin
      LATCHED_JTAG_IR <= P_CMD_IDCODE;
   end
   else
   begin
      // 7.1.1 b): changes only in update-ir and test-logic-reset states.
      if (JTAG.STATE_TEST_LOGIC_RESET)
      begin
         LATCHED_JTAG_IR <= P_CMD_IDCODE;
      end
      if (JTAG.STATE_UPDATE_IR)
      begin
         LATCHED_JTAG_IR <= jtag_ir;
      end
   end
end

assign TDO = jtag_ir[ 0 ];

`ifndef SYNTHESIS
always @( posedge JTAG.TCK ) begin
   if(P_IR_LENGTH < 2) begin
      $display("IR_LENGTH must be at least 2 (IEEE Std 1149.1TM-2013 7.1.1 Rule a)");
      $finish;
   end
end
`endif

endmodule
`default_nettype wire
