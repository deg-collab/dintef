`default_nettype none
module jtag_tap_ctrl (
      JTAG.watch JTAG_EXT,
      JTAG_internal.core JTAG_INT
   );

typedef enum bit [ 15:0 ] {
      TEST_LOGIC_RESET_P = 16'b1000000000000000,
      RUN_TEST_IDLE_P    = 16'b0100000000000000,
      SELECT_DR_SCAN_P   = 16'b0010000000000000,
      SELECT_IR_SCAN_P   = 16'b0001000000000000,
      CAPTURE_DR_P       = 16'b0000100000000000,
      CAPTURE_IR_P       = 16'b0000010000000000,
      SHIFT_DR_P         = 16'b0000001000000000,
      SHIFT_IR_P         = 16'b0000000100000000,
      EXIT1_DR_P         = 16'b0000000010000000,
      EXIT1_IR_P         = 16'b0000000001000000,
      PAUSE_DR_P         = 16'b0000000000100000,
      PAUSE_IR_P         = 16'b0000000000010000,
      EXIT2_DR_P         = 16'b0000000000001000,
      EXIT2_IR_P         = 16'b0000000000000100,
      UPDATE_DR_P        = 16'b0000000000000010,
      UPDATE_IR_P        = 16'b0000000000000001
   } tState;

tState current_state, next_state;


assign JTAG_INT.STATE_TEST_LOGIC_RESET = current_state[ 15 ];
assign JTAG_INT.STATE_RUN_TEST_IDLE = current_state[ 14 ];
assign JTAG_INT.STATE_SELECT_DR_SCAN = current_state[ 13 ];
assign JTAG_INT.STATE_SELECT_IR_SCAN = current_state[ 12 ];
assign JTAG_INT.STATE_CAPTURE_DR = current_state[ 11 ];
assign JTAG_INT.STATE_CAPTURE_IR = current_state[ 10 ];
assign JTAG_INT.STATE_SHIFT_DR = current_state[ 9 ];
assign JTAG_INT.STATE_SHIFT_IR = current_state[ 8 ];
assign JTAG_INT.STATE_EXIT1_DR = current_state[ 7 ];
assign JTAG_INT.STATE_EXIT1_IR = current_state[ 6 ];
assign JTAG_INT.STATE_PAUSE_DR = current_state[ 5 ];
assign JTAG_INT.STATE_PAUSE_IR = current_state[ 4 ];
assign JTAG_INT.STATE_EXIT2_DR = current_state[ 3 ];
assign JTAG_INT.STATE_EXIT2_IR = current_state[ 2 ];
assign JTAG_INT.STATE_UPDATE_DR = current_state[ 1 ];
assign JTAG_INT.STATE_UPDATE_IR = current_state[ 0 ];

always @( * ) begin
   unique casex({JTAG_EXT.TMS, current_state})
      {1'b0, TEST_LOGIC_RESET_P}: next_state = RUN_TEST_IDLE_P;
      {1'b1, TEST_LOGIC_RESET_P}: next_state = TEST_LOGIC_RESET_P;
      {1'b0, RUN_TEST_IDLE_P}:    next_state = RUN_TEST_IDLE_P;
      {1'b1, RUN_TEST_IDLE_P}:    next_state = SELECT_DR_SCAN_P;
      {1'b1, SELECT_DR_SCAN_P}:   next_state = SELECT_IR_SCAN_P;
      {1'b0, SELECT_DR_SCAN_P}:   next_state = CAPTURE_DR_P;
      {1'b0, SELECT_IR_SCAN_P}:   next_state = CAPTURE_IR_P;
      {1'b1, SELECT_IR_SCAN_P}:   next_state = TEST_LOGIC_RESET_P;
      {1'b0, CAPTURE_DR_P}:       next_state = SHIFT_DR_P;
      {1'b1, CAPTURE_DR_P}:       next_state = EXIT1_DR_P;
      {1'b0, CAPTURE_IR_P}:       next_state = SHIFT_IR_P;
      {1'b1, CAPTURE_IR_P}:       next_state = EXIT1_IR_P;
      {1'b1, SHIFT_DR_P}:         next_state = EXIT1_DR_P;
      {1'b0, SHIFT_DR_P}:         next_state = SHIFT_DR_P;
      {1'b1, SHIFT_IR_P}:         next_state = EXIT1_IR_P;
      {1'b0, SHIFT_IR_P}:         next_state = SHIFT_IR_P;
      {1'b0, EXIT1_DR_P}:         next_state = PAUSE_DR_P;
      {1'b1, EXIT1_DR_P}:         next_state = UPDATE_DR_P;
      {1'b0, EXIT1_IR_P}:         next_state = PAUSE_IR_P;
      {1'b1, EXIT1_IR_P}:         next_state = UPDATE_IR_P;
      {1'b1, PAUSE_DR_P}:         next_state = EXIT2_DR_P;
      {1'b0, PAUSE_DR_P}:         next_state = PAUSE_DR_P;
      {1'b1, PAUSE_IR_P}:         next_state = EXIT2_IR_P;
      {1'b0, PAUSE_IR_P}:         next_state = PAUSE_IR_P;
      {1'b1, EXIT2_DR_P}:         next_state = UPDATE_DR_P;
      {1'b0, EXIT2_DR_P}:         next_state = SHIFT_DR_P;
      {1'b1, EXIT2_IR_P}:         next_state = UPDATE_IR_P;
      {1'b0, EXIT2_IR_P}:         next_state = SHIFT_IR_P;
      {1'b1, UPDATE_DR_P}:        next_state = SELECT_DR_SCAN_P;
      {1'b0, UPDATE_DR_P}:        next_state = RUN_TEST_IDLE_P;
      {1'b1, UPDATE_IR_P}:        next_state = SELECT_DR_SCAN_P;
      {1'b0, UPDATE_IR_P}:        next_state = RUN_TEST_IDLE_P;
      default:                    next_state = TEST_LOGIC_RESET_P;
   endcase
end

// Figure 6-8
assign JTAG_INT.TAP_POR_N = JTAG_EXT.TRST_N & JTAG_EXT.POR_N;

always @( posedge JTAG_EXT.TCK, negedge JTAG_INT.TAP_POR_N )
begin
   if (!JTAG_INT.TAP_POR_N)
   begin
      current_state <= TEST_LOGIC_RESET_P;
   end
   else
   begin
      current_state <= next_state;
   end
end

// Figure 6-5
always @( negedge JTAG_EXT.TCK, negedge JTAG_INT.TAP_POR_N )
begin
   if (!JTAG_INT.TAP_POR_N)
   begin
      JTAG_INT.TAP_RESET_N <= 1'b0;
   end
   else
   begin
      JTAG_INT.TAP_RESET_N <= !JTAG_INT.STATE_TEST_LOGIC_RESET;
   end
end

endmodule
`default_nettype wire
