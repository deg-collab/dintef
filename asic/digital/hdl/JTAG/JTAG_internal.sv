interface JTAG_internal(
      input wire TCK,
      input wire TDI,
      input wire POR_N
   );

   logic TAP_POR_N;
   logic TAP_RESET_N;
   logic STATE_TEST_LOGIC_RESET;
   logic STATE_RUN_TEST_IDLE;
   logic STATE_SELECT_DR_SCAN;
   logic STATE_SELECT_IR_SCAN;
   logic STATE_CAPTURE_DR;
   logic STATE_CAPTURE_IR;
   logic STATE_SHIFT_DR;
   logic STATE_SHIFT_IR;
   logic STATE_EXIT1_DR;
   logic STATE_EXIT1_IR;
   logic STATE_PAUSE_DR;
   logic STATE_PAUSE_IR;
   logic STATE_EXIT2_DR;
   logic STATE_EXIT2_IR;
   logic STATE_UPDATE_DR;
   logic STATE_UPDATE_IR;

   // what's passed around internally
   modport register (
      input TCK,
      input TDI,
      input POR_N,
      input TAP_POR_N,
      input TAP_RESET_N,

      input STATE_TEST_LOGIC_RESET,
      input STATE_RUN_TEST_IDLE,
      input STATE_SELECT_DR_SCAN,
      input STATE_SELECT_IR_SCAN,
      input STATE_CAPTURE_DR,
      input STATE_CAPTURE_IR,
      input STATE_SHIFT_DR,
      input STATE_SHIFT_IR,
      input STATE_EXIT1_DR,
      input STATE_EXIT1_IR,
      input STATE_PAUSE_DR,
      input STATE_PAUSE_IR,
      input STATE_EXIT2_DR,
      input STATE_EXIT2_IR,
      input STATE_UPDATE_DR,
      input STATE_UPDATE_IR
   );

   modport core (
      output TAP_POR_N,
      output TAP_RESET_N,

      output STATE_TEST_LOGIC_RESET,
      output STATE_RUN_TEST_IDLE,
      output STATE_SELECT_DR_SCAN,
      output STATE_SELECT_IR_SCAN,
      output STATE_CAPTURE_DR,
      output STATE_CAPTURE_IR,
      output STATE_SHIFT_DR,
      output STATE_SHIFT_IR,
      output STATE_EXIT1_DR,
      output STATE_EXIT1_IR,
      output STATE_PAUSE_DR,
      output STATE_PAUSE_IR,
      output STATE_EXIT2_DR,
      output STATE_EXIT2_IR,
      output STATE_UPDATE_DR,
      output STATE_UPDATE_IR
   );
endinterface
