`default_nettype none
module jtag_user_reg_no_shadow #(
      parameter P_LENGTH = 8
`ifdef SIMULATION
      , parameter [ P_LENGTH-1:0 ] P_INIT_VALUE = {P_LENGTH{1'bX}}
`endif
   ) (
      JTAG_internal.register JTAG,
      input wire SELECT,
      output wire TDO,
      input wire [ P_LENGTH-1:0 ] READBACK_VAL,
      output wire [ P_LENGTH-1:0 ] VAL
   );


reg [ P_LENGTH-1:0 ] internal_shift_reg
`ifdef SIMULATION
      = P_INIT_VALUE
`endif
   ;

assign TDO = internal_shift_reg[0];

always @( posedge JTAG.TCK )
begin
   if( SELECT )
   begin
      if (P_LENGTH > 1)
      begin
         if (JTAG.STATE_CAPTURE_DR)
         begin
            internal_shift_reg <= READBACK_VAL;
         end
         else if (JTAG.STATE_SHIFT_DR)
         begin
            internal_shift_reg <= {JTAG.TDI, internal_shift_reg[ P_LENGTH-1:1 ]};
         end
      end
      else
      begin
         if (JTAG.STATE_CAPTURE_DR)
         begin
            internal_shift_reg <= READBACK_VAL;
         end
         else if (JTAG.STATE_SHIFT_DR)
         begin
            internal_shift_reg <= JTAG.TDI;
         end
      end
   end
end

assign VAL = internal_shift_reg;

endmodule
`default_nettype wire
