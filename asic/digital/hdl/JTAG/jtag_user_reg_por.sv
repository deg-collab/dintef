`default_nettype none
module jtag_user_reg_por #(
      parameter P_LENGTH = 8,
      parameter [ P_LENGTH-1:0 ] P_INIT_VALUE = {P_LENGTH{1'bX}}
   ) (
      JTAG_internal.register JTAG,
      input wire SELECT,
      output wire TDO,
      output reg [ P_LENGTH-1:0 ] VAL
   );

reg [ P_LENGTH-1:0 ] internal_shift_reg;

assign TDO = internal_shift_reg[ 0 ];

always @( posedge JTAG.TCK )
begin
   if (SELECT)
   begin
      if (P_LENGTH > 1)
      begin
         if (JTAG.STATE_CAPTURE_DR)
         begin
            internal_shift_reg <= VAL;
         end
         else if (JTAG.STATE_SHIFT_DR)
         begin
            internal_shift_reg <= {JTAG.TDI, internal_shift_reg[ P_LENGTH-1:1 ]};
         end
      end
      else
      begin
         if (JTAG.STATE_CAPTURE_DR)
         begin
            internal_shift_reg <= VAL;
         end
         else if (JTAG.STATE_SHIFT_DR)
         begin
            internal_shift_reg <= JTAG.TDI;
         end
      end
   end
end

// update on the falling edge!
always @( negedge JTAG.POR_N, negedge JTAG.TCK )
begin
   if (!JTAG.POR_N)
   begin
       VAL <= P_INIT_VALUE;
   end
   else
   begin
      if (SELECT)
      begin
         if (JTAG.STATE_UPDATE_DR) begin
            VAL <= internal_shift_reg;
         end
      end
   end
end

endmodule
`default_nettype wire
