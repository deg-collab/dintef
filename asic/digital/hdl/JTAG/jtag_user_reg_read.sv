`default_nettype none
module jtag_user_reg_read #(
      parameter P_LENGTH = 8
   ) (
      JTAG_internal.register JTAG,
      input wire SELECT,
      output wire TDO,
      input wire [ P_LENGTH-1:0 ] VAL
   );


reg [ P_LENGTH-1:0 ] internal_shift_reg;

assign TDO = internal_shift_reg[ 0 ];

always @( posedge JTAG.TCK )
begin
   if (SELECT)
   begin
      if (P_LENGTH > 1)
      begin
         if (JTAG.STATE_CAPTURE_DR)
         begin
            internal_shift_reg <= VAL;
         end
         else if (JTAG.STATE_SHIFT_DR)
         begin
            internal_shift_reg <= {JTAG.TDI, internal_shift_reg[ P_LENGTH-1:1 ]};
         end
      end
      else
      begin
         if (JTAG.STATE_CAPTURE_DR)
         begin
            internal_shift_reg <= VAL;
         end
         else if (JTAG.STATE_SHIFT_DR)
         begin
            internal_shift_reg <= JTAG.TDI;
         end
      end
   end
end

endmodule
`default_nettype wire
