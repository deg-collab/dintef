`default_nettype none
import Types::*;

module jtag #(
      parameter P_NUM_USER_REGS = 1,
      parameter P_IC_RESET_COUNT = 1,
      parameter [ 3:0 ] P_ID_VERSION = 4'h0,
      parameter [ 15:0 ] P_ID_PART = 16'h0,
      parameter [ 3:0 ] P_ID_MANUFACTURER_BANK = 4'h0,
      parameter [ 6:0 ] P_ID_MANUFACTURER = 7'h0
   ) (
     JTAG.outside JTAG_EXT,
     JTAG_internal.register JTAG_INT,

     // for the IC_RESET instruction
     input wire [ P_IC_RESET_COUNT-1:0 ] IC_RESET_IN_N,
     output wire [ P_IC_RESET_COUNT-1:0 ] IC_RESET_OUT_N,

     //select which user shift register should be selected
     output wire [ P_NUM_USER_REGS-1:0 ] SEL_REG,

     //tdo outputs of the user shift registers
     input wire [ P_NUM_USER_REGS-1:0 ] TDO_REG,

     //user-defined data captured into IR
     input wire [ cIRLength-3:0 ] IR_DATA
   );

//custom instructions

//JTAG wires
wire tdo_instruction;
wire tdo_idcode;
wire [ cIRLength-1:0 ] latched_jtag_ir;
wire idcode_select;
wire bypass_select;
wire tdo_bypass;
wire tdo_ecid_read;
wire tdo_ecid_program;

//JTAG Instruction Register
jtag_ir #(
      .P_IR_LENGTH( cIRLength ),
      .P_CMD_IDCODE( CMD_IDCODE )
   ) ir_I(
     .JTAG( JTAG_INT ),
     .TDO( tdo_instruction ),
     .LATCHED_JTAG_IR( latched_jtag_ir ),
     .IR_DATA( IR_DATA )
   );

//JTAG ID Code
jtag_id #(
     .P_ID_VERSION( P_ID_VERSION ),
     .P_ID_PART( P_ID_PART ),
     .P_ID_MANUFACTURER_FULL( { P_ID_MANUFACTURER_BANK, P_ID_MANUFACTURER, 1'b1 } )
   ) id_I(
     .JTAG( JTAG_INT ),
     .SELECT( idcode_select ),
     .TDO( tdo_idcode )
   );

jtag_bypass_reg bypass_I(
     .JTAG( JTAG_INT ),
     .SELECT( bypass_select ),
     .TDO( tdo_bypass )
   );

// IC_RESET
wire reset_select;
wire tdo_reset;

jtag_ic_reset_reg #(
     .P_NUM_IC_RESETS( P_IC_RESET_COUNT )
   ) ic_reset_reg_I(
     .JTAG( JTAG_INT ),
     .SELECT( reset_select ),
     .TDO( tdo_reset ),
     .RESET_N( IC_RESET_IN_N ),
     .IC_RESET_N( IC_RESET_OUT_N )
   );

// mux: which tdo to be connected?
wire tdo_posedge =
  JTAG_INT.STATE_SHIFT_IR ? tdo_instruction : (
  (idcode_select              & tdo_idcode              ) |
  (bypass_select              & tdo_bypass              ) |
  (reset_select               & tdo_reset               ) |
  (|(SEL_REG & TDO_REG))
);

//tdo negedge output register
always @( negedge JTAG_EXT.TCK or negedge JTAG_INT.TAP_POR_N )
begin
  if (!JTAG_INT.TAP_POR_N)
  begin
    JTAG_EXT.TDO <= 1'b0;
    JTAG_EXT.TDO_ENABLE <= 1'b0;
 end
 else
 begin
    JTAG_EXT.TDO <= tdo_posedge;
    JTAG_EXT.TDO_ENABLE <= (JTAG_INT.STATE_SHIFT_DR || JTAG_INT.STATE_SHIFT_IR);
 end
end

genvar i;

// we assume that command codes are assigned sequentially starting from 0.
assign idcode_select = (latched_jtag_ir == CMD_IDCODE);
assign reset_select = (latched_jtag_ir == CMD_IC_RESET);
// all-1 and also as default as per 8.1.1d)
assign bypass_select = (latched_jtag_ir >= CMD_SEL_REG_BASE + P_NUM_USER_REGS);
for (i = 0; i < P_NUM_USER_REGS; i = i+1)
begin : set_select_user_regs
   assign SEL_REG[ i ] = (latched_jtag_ir == CMD_SEL_REG_BASE + i);
end

`ifndef SYNTHESIS
always @( posedge JTAG_EXT.TCK )
begin
   if ((16'd1 << cIRLength) < (CMD_SEL_REG_BASE + P_NUM_USER_REGS))
   begin
      $display("Instruction register too short.");
      $finish;
   end
end
`endif

endmodule
`default_nettype wire
