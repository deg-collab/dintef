interface JTAG (
      input wire TCK,
      input wire TMS,
      input wire TDI,
      output logic TDO,
      output logic TDO_ENABLE,
      input wire TRST_N,
      // active-low power-on reset
      input wire POR_N
   );

   // the outside world's view of our JTAG interface
   modport outside (
      //jtag io signals
      input TCK,
      input TMS,
      input TDI,
      output TDO,
      // tri-state output enable for TDO
      output TDO_ENABLE,
      input TRST_N,
      input POR_N
   );

   // other modules just listen
   modport watch (
      input TCK,
      input TMS,
      input TDI,
      input TDO,
      input TRST_N,
      input POR_N
   );
endinterface
