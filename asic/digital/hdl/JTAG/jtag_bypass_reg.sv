`default_nettype none

module jtag_bypass_reg(
      JTAG_internal.register JTAG,
      input wire SELECT,
      output reg TDO
   );

always @( posedge JTAG.TCK )
begin
   if (SELECT)
   begin
      // IEEE Std 1149.1-2013 10.1.1 b)
      // IEEE Std 1149.1-2013 Figure 10.1
      // (we only care about the value in the Shift DR state)
      TDO <= JTAG.STATE_SHIFT_DR & JTAG.TDI;
   end
end

endmodule
`default_nettype wire
