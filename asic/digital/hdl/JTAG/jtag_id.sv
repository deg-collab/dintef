`default_nettype none
/**********************************************************************************
*                                                                                 *
*  JTAG ID Register                                                               *
*                                                                                 *
**********************************************************************************/


module jtag_id #(
      parameter [ 3:0 ] P_ID_VERSION = 4'h0,
      parameter [ 15:0 ] P_ID_PART = 16'h0,
      parameter [ 11:0 ] P_ID_MANUFACTURER_FULL = 12'h0 // must end with LSB=1
   ) (
      JTAG_internal.register JTAG,
      input wire SELECT,
      output wire TDO
   );

reg [ 31:0 ] idcode_reg;         // store and shift ID in this register
assign TDO = idcode_reg[ 0 ];

always @( posedge JTAG.TCK )
begin
`ifndef SYNTHESIS
   if(P_ID_MANUFACTURER_FULL[ 0 ] != 1'b1)
   begin
      $display("IDCODE LSB must be 1.");
      $finish;
   end
`endif

   if (SELECT)
   begin
      if (JTAG.STATE_SHIFT_DR)
      begin
         idcode_reg <= {JTAG.TDI, idcode_reg[ 31:1 ]};   // shift ID register
      end
      else if (JTAG.STATE_CAPTURE_DR)
      begin
         idcode_reg <= {P_ID_VERSION, P_ID_PART, P_ID_MANUFACTURER_FULL};
      end
   end
end

endmodule
`default_nettype wire
