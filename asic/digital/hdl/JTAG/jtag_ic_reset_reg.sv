`default_nettype none
module jtag_ic_reset_reg #(
      parameter P_NUM_IC_RESETS = 1
   ) (
      JTAG_internal.register JTAG,
      input wire SELECT,
      output wire TDO,

      // "functional reset source"
      input wire [ P_NUM_IC_RESETS-1:0 ] RESET_N,
      // 17.1.1 j) 0 asserts the reset signal => active low
      output wire [ P_NUM_IC_RESETS-1:0 ] IC_RESET_N
   );

// 17.1.1 a)
reg [ 2*P_NUM_IC_RESETS : 0 ] shift_reg;
reg [ 2*P_NUM_IC_RESETS : 0 ] update_reg;

assign TDO = shift_reg[ 0 ];

// 17.1.1 g)
// index 1 is the reset-control bit
// 17.1.1 h)
// index 0 is the reset-enable bit
wire [ 1:0 ] reset_pair [ P_NUM_IC_RESETS-1:0 ];
genvar i;
for (i = 0; i < P_NUM_IC_RESETS; i = i+1)
begin : set_ic_reset_lines
   assign reset_pair[ i ] = update_reg[ 1 + 2*i +: 2 ];
   // 17.1.1 i)
   assign IC_RESET_N[ i ] = reset_pair[ i ][ 1 ] ? RESET_N[ i ] : reset_pair[ i ][ 0 ];
end

// 17.1.1 c)
wire reset_hold = update_reg[ 0 ];

wire internal_reset_n = !(reset_hold && !JTAG.TAP_RESET_N);

// update_reg
// reset-hold bit
always @( negedge JTAG.TCK, negedge JTAG.TAP_POR_N )
begin
   if (!JTAG.TAP_POR_N)
   begin
      // 17.1.1 d)
      update_reg[ 0 ] <= 1'b1;
   end
   else
   begin
      if (SELECT)
      begin
         if (JTAG.STATE_UPDATE_DR)
         begin
            update_reg[ 0 ] <= shift_reg[ 0 ];
         end
      end
   end
end

// all other bits
always @( negedge JTAG.TCK, negedge internal_reset_n )
begin
   if (!internal_reset_n)
   begin
      update_reg[ 2*P_NUM_IC_RESETS : 1 ] <= {(2*P_NUM_IC_RESETS){1'b1}};
   end
   else
   begin
      if (SELECT)
      begin
         if (JTAG.STATE_UPDATE_DR)
         begin
            update_reg[ 2*P_NUM_IC_RESETS : 1 ] <= shift_reg[ 2*P_NUM_IC_RESETS : 1 ];
         end
      end
   end
end

integer j;
// shift_reg
always @( posedge JTAG.TCK )
begin
   if (SELECT)
   begin
      if (JTAG.STATE_CAPTURE_DR)
      begin
         shift_reg[ 0 ] <= update_reg[ 0 ];
         for (j = 0; j < P_NUM_IC_RESETS; j = j+1)
         begin
            shift_reg[ 1 + 2*j + 0 ] <= update_reg[ 1 + 2*j + 0 ];
            // 17.1.1 k)
            shift_reg[ 1 + 2*j + 1 ] <= RESET_N[ j ];
            //         ^ offset for reset-hold bit
            //                   ^ offset for reset-control bit
         end
      end
      if (JTAG.STATE_SHIFT_DR)
      begin
         shift_reg[ 2*P_NUM_IC_RESETS : 0 ] <= { JTAG.TDI, shift_reg[ 2*P_NUM_IC_RESETS : 1 ] };
      end
   end
end

endmodule
`default_nettype wire
