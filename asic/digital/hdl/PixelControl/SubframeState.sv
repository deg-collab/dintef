`default_nettype none
`timescale 1ns/1ps

import Types::*;

module SubframeState(
    clock_t.LEAF CLOCK,

    // in clk_50 domain
    input wire SUBFRAME_START,
    output subframe_state_t SUBFRAME_STATE
  );

subframe_state_t next_state;

always_comb unique case ( { SUBFRAME_STATE, SUBFRAME_START } ) inside
  { S_READOUT,    1'b0 } : next_state = S_READOUT;
  { S_READOUT,    1'b1 } : next_state = S_CALCULATE;
  { S_CALCULATE,  1'bx } : next_state = S_READ_RAM;
  { S_READ_RAM,   1'bx } : next_state = S_WRITE_RAM;
  { S_WRITE_RAM,  1'bx } : next_state = S_READOUT;
  default                : next_state = S_READOUT;
endcase

always_ff @(posedge CLOCK.clk_50 or negedge CLOCK.reset_n_50) begin
  if (!CLOCK.reset_n_50) begin
    SUBFRAME_STATE <= S_READOUT;
  end
  else begin
    SUBFRAME_STATE <= next_state;
  end
end

endmodule : SubframeState
`default_nettype wire
