`default_nettype none
`timescale 1ns/1ps

import Types::subframe_cmd_t;

module CommandReceiver(
    clock_t.LEAF CLOCK,
    input wire decoded_8b10b_t WORD,

    output pixel_control_t PIXEL_CONTROL
  );

typedef enum logic [ 2:0 ] {
    S_IDLE          = 3'b0_00,
    S_CMD1          = 3'b1_00,
    S_CMD2          = 3'b0_01,
    S_READOUT       = 3'b0_11
  } state_t;

state_t current_state, next_state;

bit K28_1, K28_5;
always_comb K28_1 = WORD == cK28_1_decoded;
always_comb K28_5 = WORD == cK28_5_decoded;

always_comb PIXEL_CONTROL.subframe_start = current_state[ 2 ];

always_comb unique case ( { current_state, K28_1, K28_5, WORD.is_valid } ) inside
  { S_IDLE,       3'bx1x } : next_state = S_IDLE;
  { S_IDLE,       3'b1xx } : next_state = S_CMD1;
  { S_CMD1,       3'bxx1 } : next_state = S_CMD2;
  { S_CMD2,       3'bxx1 } : next_state = S_READOUT;
  { S_READOUT,    3'bx1x } : next_state = S_IDLE;
  { S_READOUT,    3'b1xx } : next_state = S_CMD1;
  default                  : next_state = S_IDLE;
endcase

subframe_cmd_t next_subframe_cmd;
// delay so that it comes in phase with state S_READOUT
logic readout_requested;

always @(posedge CLOCK.clk_50 or negedge CLOCK.reset_n_50) begin
  if (!CLOCK.reset_n_50) begin
    current_state <= S_IDLE;
    readout_requested <= 1'b0;
    PIXEL_CONTROL.ro_address <= {cRAM_addr_width{1'b0}};
  end
  else begin
    current_state <= next_state;

    if (current_state == S_READOUT) begin
      if (!WORD.is_k) begin
        readout_requested <= 1'b1;
        PIXEL_CONTROL.ro_address <= WORD.value;
      end
    end
    else begin
      readout_requested <= 1'b0;
    end
  end
end

always @(posedge CLOCK.clk_50) begin
  unique case (current_state)
    S_CMD1  : next_subframe_cmd[ 15 -: 8 ] <= WORD.value;
    S_CMD2  : next_subframe_cmd[  7 -: 8 ] <= WORD.value;
    default : next_subframe_cmd <= next_subframe_cmd;
  endcase

  if ((current_state == S_IDLE) && K28_1) begin
    PIXEL_CONTROL.subframe_cmd <= next_subframe_cmd;
  end

  PIXEL_CONTROL.ro_load <= readout_requested;
end

endmodule : CommandReceiver
`default_nettype wire
