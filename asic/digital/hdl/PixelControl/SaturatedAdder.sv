`timescale 1ns/1ps
`default_nettype none
module SaturatedAdder(
    input wire [ 15:0 ] a,
    input wire [ 15:0 ] b,
    input wire en_saturation,
    input wire half_width,
    output bit [ 15:0 ] z
  );

logic carry;
logic [ 15:0 ] result;

always_comb begin
  {carry, result} = a + b;
  if (en_saturation && half_width && result[ 8 ])
    z = 16'h00FF;
  else if (en_saturation && !half_width && carry)
    z = 16'hFFFF;
  else
    z = result;
end

endmodule : SaturatedAdder
`default_nettype wire
