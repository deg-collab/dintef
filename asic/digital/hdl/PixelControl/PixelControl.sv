`default_nettype none
`timescale 1ns/1ps
import Types::*;

module PixelControl #(
    parameter [ 8:0 ] P_NUM_FF_ADDRESSES = 9'd1
  ) (
    clock_t.LEAF CLOCK,

    // in clk_50 domain
    input wire subframe_state_t STATE,
    input wire conversion_settings_t CONV,

    // for READOUT mode
    input wire RO_SHIFT,
    input wire pixel_control_t PIXEL_CONTROL,
    input wire [ 7:0 ] RO_SHIFT_IN,
    output logic [ 7:0 ] RO_SHIFT_OUT,

    input wire EN_COUNTERS,

    // charge pump is active
    // asynchronous
    input wire EN_COARSE_CNT,
    input wire EN_FINE_CNT,

    RAM_control_t.Controller RAM
  );

localparam int lp_coarse_bits = 6;
localparam int lp_fine_bits = 4 + 2;

// on subframe start, reset the counters and remember the old counts for processing
logic reset_counts;
always_ff @(posedge CLOCK.clk_500) reset_counts <= (STATE == S_CALCULATE) && CLOCK.clk_50_start;

logic [lp_coarse_bits-1:0] coarse_cnt;
logic [lp_coarse_bits-1:0] last_coarse_cnt;
logic [lp_fine_bits-1:0] fine_cnt;
logic [lp_fine_bits-1:0] last_fine_cnt;

logic read_RAM, write_RAM;
logic [7:0] RAM_address;
logic [11:0] combined_cnt;
logic [11:0] combined_cnt_reg;
// from RAM
logic [15:0] rdata_RAM;
// filtered for high word/low word
logic [15:0] rdata_adjusted;
// considering the "magic register" 255
logic [15:0] rdata;
logic [15:0] wdata;
logic [15:0] wdata_adjusted;
// safety feature to use if the RAM fails: address 255 is implemented using FFs
bit [255 : 256-P_NUM_FF_ADDRESSES] [15:0] magic_reg;
logic [15:0] magic_reg_adjusted;

always_comb RAM.address = RAM_address;
always_comb RAM.write = {2{write_RAM}} & PIXEL_CONTROL.subframe_cmd.word_sel;
always_comb RAM.read = {2{read_RAM}}; // do not select with word_sel!
always_comb RAM.wdata = wdata_adjusted;
always_comb rdata_RAM = RAM.rdata;

always_ff @(posedge CLOCK.clk_500) begin
  if (reset_counts) begin
    last_coarse_cnt <= coarse_cnt;
    last_fine_cnt <= fine_cnt;
  end
end

PumpCounter #(.P_BITS(lp_coarse_bits)) coarse_cnt_I(
    .clock( CLOCK ),
    .en( EN_COUNTERS ),
    .en_cnt_async( EN_COARSE_CNT ),
    .clear( reset_counts ),
    .count( coarse_cnt )
  );

PumpCounter #(.P_BITS(lp_fine_bits)) fine_cnt_I(
    .clock( CLOCK ),
    .en( EN_COUNTERS ),
    .en_cnt_async( EN_FINE_CNT ),
    .clear( reset_counts ),
    .count( fine_cnt )
  );

ColumnShiftreg sr_I(
    .CLK( CLOCK.clk_50 ),

    .*,
    .RO_ENABLE( PIXEL_CONTROL.subframe_cmd.ro_enable ),
    .RO_LOAD( PIXEL_CONTROL.ro_load ),

    .RAM_DATA( rdata )
  );

CountConverter conv_I (
    .CONV( CONV ),
    .COARSE_CNT( last_coarse_cnt ),
    .FINE_CNT( last_fine_cnt ),
    .COMBINED_CNT( combined_cnt )
  );


logic [ 15:0 ] added;
SaturatedAdder add_I(
    .a( rdata ),
    .b( PIXEL_CONTROL.subframe_cmd.f == F_INCREMENT ? 16'd1 : combined_cnt_reg ),
    .en_saturation( CONV.saturate_addition ),
    .half_width( ^PIXEL_CONTROL.subframe_cmd.word_sel ),
    .z( added )
  );

always_ff @(posedge CLOCK.clk_50) begin
  if (STATE == S_CALCULATE) begin
    combined_cnt_reg <= combined_cnt;
  end
  if (STATE == S_READ_RAM) begin
    // TODO: high/low word, accumulate, ...
    unique case (PIXEL_CONTROL.subframe_cmd.f)
      F_NOP       : wdata <= 16'hXXXX;
      F_STORE     : wdata <= { 4'b000, combined_cnt_reg };
      F_CLEAR     : wdata <= 16'h0000;
      F_ADD       : wdata <= added;
      F_SUBTRACT  : wdata <= rdata - combined_cnt_reg;
      F_INCREMENT : wdata <= added;
      default     : wdata <= 16'hXXXX;
    endcase
  end
  if (STATE == S_WRITE_RAM) begin
    if (RAM_address >= 9'd256 - P_NUM_FF_ADDRESSES) begin
      if (PIXEL_CONTROL.subframe_cmd.word_sel[ 0 ])
        magic_reg[ RAM_address ][ 7:0 ] <= wdata_adjusted[ 7:0 ];
      if (PIXEL_CONTROL.subframe_cmd.word_sel[ 1 ])
        magic_reg[ RAM_address ][ 15:8 ] <= wdata_adjusted[ 15:8 ];
    end
  end
end

always_comb read_RAM = ((STATE == S_READ_RAM) && NeedsRAMRead( PIXEL_CONTROL.subframe_cmd.f ))
  || ((STATE == S_READOUT) && PIXEL_CONTROL.ro_load);
always_comb write_RAM = (STATE == S_WRITE_RAM) && (PIXEL_CONTROL.subframe_cmd.f != F_NOP);

always_comb rdata = ( RAM_address >= 9'd256 - P_NUM_FF_ADDRESSES ) ? magic_reg_adjusted : rdata_adjusted;

always_comb unique case( PIXEL_CONTROL.subframe_cmd.word_sel )
  WORD_LOW  : begin
      rdata_adjusted = rdata_RAM & 16'h00FF;
      magic_reg_adjusted = magic_reg[ RAM_address ] & 16'h00FF;
      wdata_adjusted = wdata; // will be cropped by bit select
    end
  WORD_HIGH : begin
      rdata_adjusted = rdata_RAM >> 8;
      magic_reg_adjusted = magic_reg[ RAM_address ] >> 8;
      wdata_adjusted = wdata << 8;
    end
  WORD_FULL  : begin
      rdata_adjusted = rdata_RAM;
      magic_reg_adjusted = magic_reg[ RAM_address ];
      wdata_adjusted = wdata;
    end
  default  : begin
      rdata_adjusted = 16'hXXXX;
      magic_reg_adjusted = 16'hXXXX;
      wdata_adjusted = 16'hXXXX;
    end
endcase

always_comb unique case (STATE)
  S_READ_RAM, S_WRITE_RAM : RAM_address = PIXEL_CONTROL.subframe_cmd.RAM_address_is_relative
      ? PIXEL_CONTROL.subframe_cmd.RAM_address + combined_cnt_reg : PIXEL_CONTROL.subframe_cmd.RAM_address;
  S_READOUT               : RAM_address = PIXEL_CONTROL.ro_address;
  default                 : RAM_address = {cRAM_addr_width{1'bx}};
endcase


assert property (@(posedge CLOCK.clk_50) !(PIXEL_CONTROL.ro_load && (STATE != S_READOUT)));

endmodule : PixelControl
`default_nettype wire
