`default_nettype none
`timescale 1ns/1ps
module RAM256x16(
    input wire CLK,
    input wire [7:0] A,
    input wire [15:0] WDATA,
    input wire CEB,
    input wire WEB,

    output bit [15:0] RDATA
  );

logic [ 15:0 ] dummy;

TS1N65LPA512X32M8 core_I(
    .CLK( CLK ),

    .A( { 1'b0, A } ),
    .D( { 16'h0000, WDATA } ),
    .CEB( CEB ),
    .WEB( WEB ),
    .BWEB( 32'b0 ),
    .Q( { dummy, RDATA } ),
    .TSEL( 2'b01 ),

    .BIST( 1'b0 ),
    .AWT( 1'b0 ),
    .AM( 9'hd0 ),
    .DM( 32'd0 ),
    .BWEBM( 32'hFFFFFFFF ),
    .CEBM( 1'b1 ),
    .WEBM( 1'b1 )
  );

endmodule : RAM256x16
`default_nettype wire 
