`default_nettype none
`timescale 1ns/1ps
import Types::*;

module CountConverter (
    input wire conversion_settings_t CONV,
    input wire [ 5:0 ] COARSE_CNT,
    input wire [ 5:0 ] FINE_CNT,
    output logic [ 11:0 ] COMBINED_CNT
  );

// gain of the coarse count
logic [ 11:0 ] coarse_in_fine;
// selections 1..64
always_comb coarse_in_fine = { 6'b000000, COARSE_CNT } << CONV.coarse_gain;

logic [ 11:0 ] combined;
always_comb combined = coarse_in_fine + { 5'b00000, FINE_CNT };

// add a fixed value and shift to discard/round the LSBs
logic [ 11:0 ] adjusted;
always_comb adjusted = combined + CONV.LSB_correction;
logic [ 11:0 ] dropped;
always_comb dropped = adjusted >> CONV.LSB_discard;

logic [ 7:0 ] limit;
// range 5..8 (31..255)
// shift in zeros, then invert
always_comb limit = ~(8'hFF << (CONV.limit + 5));

always_comb COMBINED_CNT = CONV.enable_limit
  ? (dropped > limit ? { 4'b0000, limit } : dropped)
  : dropped;

endmodule : CountConverter
`default_nettype wire
