`default_nettype none
`timescale 1ns/1ps
module ColumnShiftreg (
    input wire CLK,

    input wire [ 1:0 ] RO_ENABLE,
    input wire RO_LOAD,
    input wire RO_SHIFT,
    input wire [ 7:0 ] RO_SHIFT_IN,
    output logic [ 7:0 ] RO_SHIFT_OUT,

    input wire [ 15:0 ] RAM_DATA
  );

logic [ 1:0 ] ro_active;
logic [ 1:0 ][ 7:0 ] shifter;

always_ff @(posedge CLK) begin
  if (RO_LOAD) begin
    ro_active <= RO_ENABLE;
    if (|RO_ENABLE) begin
      shifter <= RAM_DATA;
    end
  end
  else if (RO_SHIFT) begin
    if (ro_active[ 0 ]) shifter[ 0 ] <= ro_active[ 1 ] ? shifter[ 1 ] : RO_SHIFT_IN;
    if (ro_active[ 1 ]) shifter[ 1 ] <= RO_SHIFT_IN;
  end
end

always_comb unique case (ro_active) inside
  2'bx1   : RO_SHIFT_OUT = shifter[ 0 ];
  2'b10   : RO_SHIFT_OUT = shifter[ 1 ];
  2'b00   : RO_SHIFT_OUT = RO_SHIFT_IN;
endcase

endmodule : ColumnShiftreg
`default_nettype wire
