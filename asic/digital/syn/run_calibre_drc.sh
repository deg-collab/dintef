#!/bin/bash
. /opt/eda/environment/calibre_latest_path.bash

DESIGN=ControlBlock

cd stream_out
mkdir -p DRC
cd DRC

cat <<EOF > DRC.txt
LAYOUT PATH  "../${DESIGN}.full.gds"
LAYOUT PRIMARY "${DESIGN}"
LAYOUT SYSTEM GDSII

DRC RESULTS DATABASE "${DESIGN}.drc.results" ASCII
DRC MAXIMUM RESULTS 1000
DRC MAXIMUM VERTEX 4096

DRC CELL NAME YES CELL SPACE XFORM
DRC SUMMARY REPORT "${DESIGN}.drc.summary" REPLACE HIER

VIRTUAL CONNECT COLON NO
VIRTUAL CONNECT REPORT YES
VIRTUAL CONNECT REPORT MAXIMUM ALL
VIRTUAL CONNECT NAME ?

DRC ICSTATION YES

PRECISION 2000

EOF

# would be more correct and a bit faster, but the ruleset has a problem without FRONT_END
#egrep -v '#DEFINE (FRONT_END|FULL_CHIP|CHECK_LOW_DENSITY)\W' /opt/eda/TSMC/65/__UNPACKED__/PDK/PDK/Calibre/drc/calibre.drc >> DRC.txt
egrep -v '#DEFINE (FULL_CHIP|CHECK_LOW_DENSITY)\W' /opt/eda/TSMC/65/__UNPACKED__/PDK/PDK/Calibre/drc/calibre.drc >> DRC.txt

calibre -drc -hier -turbo -64 DRC.txt
