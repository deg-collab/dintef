# Flowkit v18.10-p005_1
#- genus_steps.tcl : defines Genus based flow_steps

#===========================================================================
# Flow: synth
#===========================================================================

##############################################################################
# STEP create_cost_group
##############################################################################
create_flow_step -name create_cost_group -owner cadence {
  #- Clear existing path_groups
  get_db cost_groups -if {.name != default} -foreach {delete_obj $obj(.)}
  
  #- Add basic path_groups
  foreach mode [get_db constraint_modes -if {.is_setup}] {
    set_interactive_constraint_mode $mode
    group_path -name in2out -from [all_inputs] -to [all_outputs]
    if {[sizeof_collection [all_registers]] > 0} {
      group_path -name in2reg -from [all_inputs] -to [all_registers]
      group_path -name reg2out -from [all_registers] -to [all_outputs]
      group_path -name reg2reg -from [all_registers] -to [all_registers]
    }
  }
}

##############################################################################
# STEP run_syn_generic
##############################################################################
create_flow_step -name run_syn_generic -owner cadence {
  #- Synthesize to generic gates
  syn_generic
}

##############################################################################
# STEP run_syn_map
##############################################################################
create_flow_step -name run_syn_map -owner cadence {
  #- Synthesize to target library gates
  syn_map
}

##############################################################################
# STEP run_syn_opt
##############################################################################
create_flow_step -name run_syn_opt -owner cadence {
  #- Synthesize to optimized gates
  syn_opt
}

##############################################################################
# STEP genus_to_innovus
##############################################################################
create_flow_step -name genus_to_innovus -owner cadence {
  #- Apply change_name rules
  update_names \
    [get_db current_design] \
    -force \
    -verilog \
    -module \
    -append_log \
    -log [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]design.change_names.rpt]
  
  #- prevent SDCs from having set_timing_derate and group_path since these are ignored by innovus
  set_db write_sdc_exclude {set_timing_derate group_path}
  
  #- write output files
  write_design \
    -innovus \
    -gzip_files \
    -basename [file normalize [file join [get_db flow_db_directory] [get_db flow_report_name] [get_db current_design .name]]]
  
  set_db flow_post_db_overwrite [list tcl [file join [get_db flow_db_directory] [get_db flow_report_name] [get_db current_design .name].invs_setup.tcl] [pwd] {}]
}
#===========================================================================
# Flow: report_genus
#===========================================================================

##############################################################################
# STEP report_area_genus
##############################################################################
create_flow_step -name report_area_genus -owner cadence -exclude_time_metric -categories design {
  report_area -min_count 5000 > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]area.summary.rpt]
  report_dp                   > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]area.datapath.rpt]
}

##############################################################################
# STEP report_timing_summary_late_genus
##############################################################################
create_flow_step -name report_timing_summary_late_genus -owner cadence -exclude_time_metric -categories setup {
  report_qor > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]qor.rpt]
}

##############################################################################
# STEP report_power_genus
##############################################################################
create_flow_step -name report_power_genus -owner cadence -exclude_time_metric -categories power {
  report_gates -power   > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]power.all.rpt]
  report_clock_gating   > [file join [get_db flow_report_directory]/[get_db flow_report_name] [get_db flow_report_prefix]power.clock_gating.rpt]
  report_power -depth 0 > [file join [get_db flow_report_directory]/[get_db flow_report_name] [get_db flow_report_prefix]power.design.rpt]
}

