# Flowkit v18.10-p005_1
#- tempus_steps.tcl : defines Tempus based flow_steps

#=============================================================================
# Flow: sta
#=============================================================================

##############################################################################
# STEP update_timing
##############################################################################
create_flow_step -name update_timing -owner cadence {
  #- update timer for signoff timing reports
  update_timing -full
}

##############################################################################
# STEP check_timing
##############################################################################
create_flow_step -name check_timing -owner cadence -exclude_time_metric {
  #- Reports that check design health
  check_netlist -out_file        [get_db flow_report_directory]/[get_db flow_report_name]/check.netlist.rpt
  check_timing                 > [get_db flow_report_directory]/[get_db flow_report_name]/check.timing.rpt
  report_analysis_coverage     > [get_db flow_report_directory]/[get_db flow_report_name]/check.coverage.rpt
  report_annotated_parasitics  > [get_db flow_report_directory]/[get_db flow_report_name]/check.annotation.rpt
  
  #- Reports that describe constraints
  report_clocks                > [get_db flow_report_directory]/[get_db flow_report_name]/report.clocks.rpt
  report_case_analysis         > [get_db flow_report_directory]/[get_db flow_report_name]/report.case_analysis.rpt
  report_inactive_arcs         > [get_db flow_report_directory]/[get_db flow_report_name]/report.inactive_arcs.rpt
}

##############################################################################
# STEP report_timing_late
##############################################################################
create_flow_step -name report_timing_late -owner cadence -exclude_time_metric -categories setup {
  #- Reports that describe timing health
  report_analysis_summary -late -merged_groups -merged_views  > [get_db flow_report_directory]/[get_db flow_report_name]/setup.view_summary.rpt
  report_analysis_summary -late -merged_groups                > [get_db flow_report_directory]/[get_db flow_report_name]/setup.group_summary.rpt
  report_analysis_summary -late                               > [get_db flow_report_directory]/[get_db flow_report_name]/setup.analysis_summary.rpt
  report_constraint       -late -all_violators -drv_violation_type {max_capacitance max_transition max_fanout} \
                                                              > [get_db flow_report_directory]/[get_db flow_report_name]/setup.all_violators.rpt
  set_metric -name timing.drv.report_file -value                [get_db flow_report_name]/setup.all_violators.rpt
}

##############################################################################
# STEP report_timing_early
##############################################################################
create_flow_step -name report_timing_early -owner cadence -exclude_time_metric -categories hold {
  #- Reports that describe early timing health
  report_analysis_summary -early -merged_groups -merged_views > [get_db flow_report_directory]/[get_db flow_report_name]/hold.view_summary.rpt
  report_analysis_summary -early -merged_groups               > [get_db flow_report_directory]/[get_db flow_report_name]/hold.group_summary.rpt
  report_analysis_summary -early                              > [get_db flow_report_directory]/[get_db flow_report_name]/hold.analysis_summary.rpt
  report_constraint       -early -all_violators -drv_violation_type {min_capacitance min_transition min_fanout} \
                                                              > [get_db flow_report_directory]/[get_db flow_report_name]/hold.all_violators.rpt
}

