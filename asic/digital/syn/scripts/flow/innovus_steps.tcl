# Flowkit v18.10-p005_1
#- innovus_steps.tcl : defines Innovus based flow_steps

#=============================================================================
# Flow: floorplan
#=============================================================================

##############################################################################
# STEP add_tracks
##############################################################################
create_flow_step -name add_tracks -owner cadence {
  #- generate tracks after creating floorplan
  if {[llength [get_db current_design .track_patterns]] == 0} {
    add_tracks
  }
}
#===========================================================================
# Flow: prects
#===========================================================================

##############################################################################
# STEP add_clock_spec
##############################################################################
create_flow_step -name add_clock_spec -owner cadence {
  #- automatically create clock spec if one is not available
  if {[llength [get_clock_tree_sinks  *]] == 0} {
    create_clock_tree_spec
  } else {
    puts "INFO: reusing existing clock tree spec"
    puts "        to reload a new one use 'delete_clock_tree_spec' and 'read_ccopt_config"
  }
}

##############################################################################
# STEP commit_route_types
##############################################################################
create_flow_step -name commit_route_types -owner cadence {
  #- assign route_types to clock nets
  commit_clock_tree_route_attributes
}

##############################################################################
# STEP run_place_opt
##############################################################################
create_flow_step -name run_place_opt -owner cadence {
  #- perform global placement and ideal clock setup optimization
  source scripts/deleteAllScanCells.tcl
  deleteAllScanCells
  place_opt_design -report_dir debug -report_prefix [get_db flow_report_name]
}
#=============================================================================
# Flow: cts
#=============================================================================

##############################################################################
# STEP add_clock_tree
##############################################################################
create_flow_step -name add_clock_tree -owner cadence {
  #- implement clock trees and propagated clock setup optimization
  ccopt_design -report_dir debug -report_prefix [get_db flow_report_name]
}

##############################################################################
# STEP add_tieoffs
##############################################################################
create_flow_step -name add_tieoffs -owner cadence {
  #- insert dedicated tieoff models
  if {[get_db add_tieoffs_cells] ne "" } {
    delete_tieoffs
    add_tieoffs -matching_power_domains true
  }
}
#=============================================================================
# Flow: postcts
#=============================================================================

##############################################################################
# STEP run_opt_postcts_hold
##############################################################################
create_flow_step -name run_opt_postcts_hold -owner cadence {
  #- perform postcts hold optimization
  opt_design -post_cts -hold -report_dir debug -report_prefix [get_db flow_report_name]
}
#=============================================================================
# Flow: route
#=============================================================================

##############################################################################
# STEP add_fillers
##############################################################################
create_flow_step -name add_fillers -owner cadence {
  #- insert filler cells before final routing
  if {[get_db add_fillers_cells] ne "" } {
    add_fillers
  }
}

##############################################################################
# STEP run_route
##############################################################################
create_flow_step -name run_route -owner cadence {
  #- perform detail routing and DRC cleanup
  route_design
}
#=============================================================================
# Flow: postroute
#=============================================================================

##############################################################################
# STEP run_opt_postroute
##############################################################################
create_flow_step -name run_opt_postroute -owner cadence {
  #- perform postroute and SI based setup optimization
  opt_design -post_route -setup -hold -report_dir debug -report_prefix [get_db flow_report_name]
}
#=============================================================================
# Flow: eco
#=============================================================================

##############################################################################
# STEP eco_start
##############################################################################
create_flow_step -name eco_start -owner cadence {
}

##############################################################################
# STEP run_place_eco
##############################################################################
create_flow_step -name run_place_eco -owner cadence {
  place_eco
}

##############################################################################
# STEP run_route_eco
##############################################################################
create_flow_step -name run_route_eco -owner cadence {
  route_eco
}

##############################################################################
# STEP eco_finish
##############################################################################
create_flow_step -name eco_finish -owner cadence -write_db {
}
#===========================================================================
# Flow: report_innovus
#===========================================================================

##############################################################################
# STEP innovus_to_lec
##############################################################################
create_flow_step -name innovus_to_lec -owner flow {
  #- write dofile for LEC
  write_do_lec \
    -flat \
    -log_file [file join [get_db flow_log_directory] lec.[get_db flow_report_name].log] \
    lec.[get_db flow_report_name].do
  
  #- schedule the LEC flow
  #FlowtoolPredictHint ArgumentRandomise -branch
  schedule_flow \
    -flow lec \
    -branch [get_db flow_report_name] \
    -no_db \
    -no_sync \
    -tool_options "-nogui -lp -do [file join [string map [list %s [get_db current_design .name]] [get_db write_lec_directory_naming_style]] lec.[get_db flow_report_name].do]"
}

##############################################################################
# STEP report_area_innovus
##############################################################################
create_flow_step -name report_area_innovus -owner cadence -exclude_time_metric -categories design {
  report_summary -no_html -out_dir debug -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]qor.rpt]
  report_area  -min_count 1000 -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]area.summary.rpt]
}

##############################################################################
# STEP report_timing_late_innovus
##############################################################################
create_flow_step -name report_timing_late_innovus -owner cadence -exclude_time_metric -categories setup {
  #- Update the timer for setup and write reports
  time_design -expanded_views -report_only -report_dir debug -report_prefix [get_db flow_report_name]
  
  #- Reports that describe timing health
  report_analysis_summary -late -merged_groups  -merged_views > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]setup.view_summary.rpt]
  report_analysis_summary -late -merged_groups                > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]setup.group_summary.rpt]
  report_analysis_summary -late                               > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]setup.analysis_summary.rpt]
  report_constraint       -late -all_violators -drv_violation_type {max_capacitance max_transition max_fanout} \
                                                              > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]setup.all_violators.rpt]
  set_metric -name timing.drv.report_file -value                [file join [get_db flow_report_name] [get_db flow_report_prefix]setup.all_violators.rpt]
}

##############################################################################
# STEP report_timing_early_innovus
##############################################################################
create_flow_step -name report_timing_early_innovus -owner cadence -exclude_time_metric -categories hold {
  #- Update the timer for hold and write reports
  time_design -expanded_views -hold -report_only -report_dir debug -report_prefix [get_db flow_report_name]
  
  #- Reports that describe timing health
  report_analysis_summary -early -merged_groups -merged_views > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]hold.view_summary.rpt]
  report_analysis_summary -early -merged_groups               > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]hold.group_summary.rpt]
  report_analysis_summary -early                              > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]hold.analysis_summary.rpt]
  report_constraint       -early -all_violators -drv_violation_type {min_capacitance min_transition min_fanout} \
                                                              > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]hold.all_violators.rpt]
}

##############################################################################
# STEP report_clock_timing
##############################################################################
create_flow_step -name report_clock_timing -owner cadence -exclude_time_metric -categories clock {
  #- Reports that check clock implementation
  report_clock_timing -type summary > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]clock.summary.rpt]
  report_clock_timing -type latency > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]clock.latency.rpt]
  report_clock_timing -type skew    > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]clock.skew.rpt]
}

##############################################################################
# STEP report_power_innovus
##############################################################################
create_flow_step -name report_power_innovus -owner cadence -exclude_time_metric -categories power {
  report_power -no_wrap -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]power.all.rpt]
}

##############################################################################
# STEP report_route_process
##############################################################################
create_flow_step -name report_route_process -owner cadence -exclude_time_metric {
  #- Reports that process rules
  check_process_antenna -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]route.antenna.rpt]
  check_filler -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]route.filler.rpt]
  set_metric -name check.drc.antenna.report_file -value [file join [get_db flow_report_name] [get_db flow_report_prefix]route.antenna.rpt]
}

##############################################################################
# STEP report_route_drc
##############################################################################
create_flow_step -name report_route_drc -owner cadence -exclude_time_metric -categories route {
  #- Reports that check signal routing
  check_drc -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]route.drc.rpt]
  check_connectivity -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]route.open.rpt]
  set_metric -name check.drc.report_file -value [file join [get_db flow_report_name] [get_db flow_report_prefix]route.drc.rpt]
}

##############################################################################
# STEP report_route_density
##############################################################################
create_flow_step -name report_route_density -owner cadence -exclude_time_metric {
  check_metal_density -report [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]route.metal_density.rpt]
  check_cut_density -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]route.cut_density.rpt]
}
