# Flowkit v18.10-p005_1
#- common_steps.tcl : defines flow_steps used in mutiple flows

#===============================================================================
# Common flow_steps for init_design
#===============================================================================

##############################################################################
# STEP read_mmmc
##############################################################################
create_flow_step -name read_mmmc -owner cadence {
  set FH [open [file join [get_db flow_source_directory] mmmc_config.tcl] w]
  
  puts $FH "##############################################################################"
  puts $FH "## LIBRARY SETS"
  puts $FH "##############################################################################"
  foreach name [dict keys [get_flow_config library_sets]] {
  
    set library_files [get_flow_config -quiet library_sets $name library_files]
    set aocv_files    [get_flow_config -quiet library_sets $name aocv_files]
    set lvf_files     [get_flow_config -quiet library_sets $name lvf_files]
    set si_files      [get_flow_config -quiet library_sets $name si_files]
    set socv_files    [get_flow_config -quiet library_sets $name socv_files]
  
    puts $FH "create_library_set \\"
    puts $FH "  -name $name \\"
    #- define timing_files (.ldb/.lib)
    puts $FH "  -timing \\"
    puts $FH "    \[list \\"
    foreach file $library_files {
      puts $FH "      [file normalize $file] \\"
    }
    if {[llength [concat $aocv_files $lvf_files $si_files $socv_files]] > 0 } {
      puts $FH "    \] \\"
    } else {
      puts $FH "    \]"
    }
    #- define aocv files
    if {[llength $aocv_files] > 0 } {
      puts $FH "  -aocv \\"
      puts $FH "    \[list \\"
      foreach file $aocv_files {
        puts $FH "      [file normalize $file] \\"
      }
      if {[llength [concat $lvf_files $si_files $socv_files]] > 0 } {
        puts $FH "    \] \\"
      } else {
        puts $FH "    \]"
      }
    }
    #- define lvf files
    if {[llength $lvf_files] > 0 } {
      puts $FH "  -lvf \\"
      puts $FH "    \[list \\"
      foreach file $lvf_files {
        puts $FH "      [file normalize $file] \\"
      }
      if {[llength [concat $si_files $socv_files]] > 0 } {
        puts $FH "    \] \\"
      } else {
        puts $FH "    \]"
      }
    }
    #- define si files (.cdb)
    if {[llength $si_files] > 0 } {
      puts $FH "  -si     \\"
      puts $FH "    \[list \\"
      foreach file $si_files {
        puts $FH "      [file normalize $file] \\"
      }
      if {[llength [concat $socv_files]] > 0 } {
        puts $FH "    \] \\"
      } else {
        puts $FH "    \]"
      }
    }
    #- define socv files
    if {[llength $socv_files] > 0 } {
      puts $FH "  -socv   \\"
      puts $FH "    \[list \\"
      foreach file $socv_files {
        puts $FH "      [file normalize $file] \\"
      }
      puts $FH "    \]"
    }
    puts $FH ""
  }
  puts $FH ""
  puts $FH "##############################################################################"
  puts $FH "## OPERATING CONDITIONS"
  puts $FH "##############################################################################"
  foreach name [dict keys [get_flow_config -quiet opconds]] {
    set op_cond_process      [get_flow_config -quiet opconds $name process]
    set op_cond_voltage      [get_flow_config -quiet opconds $name voltage]
    set op_cond_temperature  [get_flow_config -quiet opconds $name temperature]
  
    puts $FH "create_opcond \\"
    puts $FH "  -name $name \\"
    if ![string is space $op_cond_process] {
      puts $FH "  -process $op_cond_process \\"
    }
    if ![string is space $op_cond_voltage] {
      puts $FH "  -voltage $op_cond_voltage \\"
    }
    if ![string is space $op_cond_temperature] {
      puts $FH "  -temperature $op_cond_temperature \\"
    }
    puts $FH ""
  }
  puts $FH ""
  puts $FH "###############################################################################"
  puts $FH "### RC CORNERS"
  puts $FH "###############################################################################"
  foreach name [dict keys [get_flow_config rc_corners]] {
    set rc_corner_qrc_tech    [get_flow_config -quiet rc_corners $name qrc_tech_file]
    set rc_corner_temperature [get_flow_config -quiet rc_corners $name temperature]
  
    puts $FH "create_rc_corner \\"
    puts $FH "  -name $name \\"
    if ![string is space $rc_corner_temperature] {
      puts $FH "  -temperature $rc_corner_temperature \\"
    }
    if ![string is space $rc_corner_qrc_tech] {
      puts $FH "  -qrc_tech $rc_corner_qrc_tech"
    }
    puts $FH ""
  }
  puts $FH ""
  puts $FH "##############################################################################"
  puts $FH "## TIMING CONDITIONS"
  puts $FH "##############################################################################"
  foreach name [dict keys [get_flow_config timing_conditions]] {
    set timing_condition_lib_set [get_flow_config -quiet timing_conditions $name library_sets]
    set timing_condition_opcond  [get_flow_config -quiet timing_conditions $name opcond]
    set timing_condition_opcond_lib  [get_flow_config -quiet timing_conditions $name opcond_library]
  
    puts $FH "create_timing_condition \\"
    puts $FH "  -name $name \\"
  
    #- Timing Condition library sets validation
    puts $FH "  -library_sets \\"
    puts $FH "    \[list \\"
    foreach tc_lib_set $timing_condition_lib_set {
      puts $FH "        $tc_lib_set \\"
    }
    if {[llength [concat $timing_condition_opcond $timing_condition_opcond_lib]] > 0 } {
      puts $FH "    \] \\"
    } else {
      puts $FH "    \]"
    }
    if {![string is space $timing_condition_opcond]} {
      if {![string is space $timing_condition_opcond_lib]} {
        puts $FH "  -opcond $timing_condition_opcond \\"
        puts $FH "  -opcond_library $timing_condition_opcond_lib"
      } else {
       puts $FH "  -opcond $timing_condition_opcond"
     }
    }
    puts $FH ""
  }
  puts $FH ""
  puts $FH "###############################################################################"
  puts $FH "### DELAY CORNERS"
  puts $FH "###############################################################################"
  foreach name [dict keys [get_flow_config delay_corners]] {
    set delay_corner_early_rc_corner  [get_flow_config -quiet delay_corners $name rc_corner early]
    set delay_corner_late_rc_corner   [get_flow_config -quiet delay_corners $name rc_corner late]
    set delay_corner_early_timing_condition [get_flow_config -quiet delay_corners $name timing_condition early]
    set delay_corner_late_timing_condition  [get_flow_config -quiet delay_corners $name timing_condition late]
  
    puts $FH "create_delay_corner \\"
    puts $FH "  -name $name \\"
  
    #- define delay_corner -rc_corner
    if {![string is space $delay_corner_early_rc_corner] && ![string is space $delay_corner_late_rc_corner]} {
      if {$delay_corner_early_rc_corner eq $delay_corner_late_rc_corner} {
        puts $FH "  -rc_corner $delay_corner_early_rc_corner \\"
      } else {
        puts $FH "  -early_rc_corner $delay_corner_early_rc_corner \\"
        puts $FH "  -late_rc_corner $delay_corner_late_rc_corner \\"
      }
    }
    #- define delay_corner -timing_condition
    if {![string is space $delay_corner_early_timing_condition] && ![string is space $delay_corner_late_timing_condition]} {
      if {$delay_corner_early_timing_condition eq $delay_corner_late_timing_condition} {
        puts $FH "  -timing_condition $delay_corner_early_timing_condition"
      } else {
        puts $FH "  -early_timing_condition $delay_corner_early_timing_condition \\"
        puts $FH "  -late_timing_condition $delay_corner_late_timing_condition"
      }
    }
    puts $FH ""
  }
  puts $FH ""
  puts $FH "###############################################################################"
  puts $FH "### CONSTRAINT MODES"
  puts $FH "###############################################################################"
  foreach name [dict keys [get_flow_config constraint_modes]] {
    set constraint_mode_sdc       [get_flow_config -quiet constraint_modes $name sdc_files]
    set constraint_mode_tcl_vars  [get_flow_config -quiet constraint_modes $name tcl_variables]
  
    puts $FH "create_constraint_mode \\"
    puts $FH "  -name $name \\"
    #- Constraint_mode -sdc_files
    puts $FH "  -sdc_files \\"
    puts $FH "    \[list \\"
    foreach file $constraint_mode_sdc {
      puts $FH "      [file normalize $file] \\"
    }
    #- Constraint_mode -tcl_vars
    if ![ string is space $constraint_mode_tcl_vars] {
      puts $FH "    \] \\"
      puts $FH "  -tcl_variables \{$constraint_mode_tcl_vars\}"
    } else {
      puts $FH "    \]"
    }
    puts $FH ""
  }
  puts $FH ""
  puts $FH "###############################################################################"
  puts $FH "### ANALYSIS VIEWS"
  puts $FH "###############################################################################"
  set analysis_view_is_setup_list   ""
  set analysis_view_is_hold_list    ""
  set analysis_view_is_dynamic_list ""
  set analysis_view_is_leakage_list ""
  foreach name [dict keys [get_flow_config analysis_views]] {
    puts $FH "create_analysis_view \\"
    puts $FH "  -name $name \\"
    puts $FH "  -constraint_mode [get_flow_config -quiet analysis_views $name constraint_mode] \\"
    puts $FH "  -delay_corner [get_flow_config -quiet analysis_views $name delay_corner]"
    #- Sort views by purpose
    if [string is true -strict [get_flow_config -quiet analysis_views $name is_setup]] {
      lappend analysis_view_is_setup_list $name
    }
    if [string is true -strict [get_flow_config -quiet analysis_views $name is_hold]] {
      lappend analysis_view_is_hold_list $name
    }
    if [string is true -strict [get_flow_config -quiet analysis_views $name is_leakage]] {
      lappend analysis_view_is_leakage_list $name
    }
    if [string is true -strict [get_flow_config -quiet analysis_views $name is_dynamic]] {
      lappend analysis_view_is_dynamic_list $name
    }
    puts $FH ""
  }
  
  #- Configure the Analysis View
  puts $FH "set_analysis_view \\"
  puts $FH "  -setup   [list $analysis_view_is_setup_list] \\"
  if {[llength $analysis_view_is_leakage_list] > 0 && [llength $analysis_view_is_dynamic_list] > 0} {
    puts $FH "  -hold    [list $analysis_view_is_hold_list] \\"
    puts $FH "  -leakage [list $analysis_view_is_leakage_list] \\"
    puts $FH "  -dynamic [list $analysis_view_is_dynamic_list]"
  } elseif {[llength $analysis_view_is_leakage_list] > 0 && [llength $analysis_view_is_dynamic_list] == 0} {
    puts $FH "  -hold    [list $analysis_view_is_hold_list] \\"
    puts $FH "  -leakage [list $analysis_view_is_leakage_list]"
  } elseif {[llength $analysis_view_is_leakage_list] == 0 && [llength $analysis_view_is_dynamic_list] > 0} {
    puts $FH "  -hold    [list $analysis_view_is_hold_list] \\"
    puts $FH "  -dynamic [list $analysis_view_is_dynamic_list]"
  } else {
    puts $FH "  -hold    [list $analysis_view_is_hold_list]"
  }
  puts $FH ""
  close $FH
  #- Read MMMC file
  read_mmmc [file join [get_db flow_source_directory] mmmc_config.tcl]
} -check {
  namespace eval stylus::check {
    #- Check: library_sets are defined
    check "[llength [get_flow_config -quiet library_sets]]" "library_set objects are required for MMMC.  None were found in the MMMC section of setup.yaml."
    #- Check: library_sets reference valid files
    foreach name [dict keys [get_flow_config -quiet library_sets]] {
      foreach file [get_flow_config -quiet library_sets $name library_files] {
        check "[file exists $file] && [file readable $file]" "The library file: $file was not found or is not readable for library_set: $name"
      }
      foreach file [get_flow_config -quiet library_sets $name aocv_files] {
        check "[file exists $file] && [file readable $file]" "The aocv: $file was not found or is not readable for library_set: $name"
      }
      foreach file [get_flow_config -quiet library_sets $name lvf_files] {
        check "[file exists $file] && [file readable $file]" "The lvf file: $file was not found or is not readable for library_set: $name"
      }
      foreach file [get_flow_config -quiet library_sets $name si_files] {
        check "[file exists $file] && [file readable $file]" "The si file: $file was not found or is not readable for library_set: $name"
      }
      foreach file [get_flow_config -quiet library_sets $name socv_files] {
        check "[file exists $file] && [file readable $file]" "The socv file: $file was not found or is not readable for library_set: $name"
      }
      check "[llength [get_flow_config -quiet library_sets $name library_files]]" "Timing files are required for library_set: $name"
    }
    #- Check: rc_corners are defined
    check "[llength [get_flow_config -quiet rc_corners]]" "rc_corner objects are required for MMMC.  None were found in the MMMC section of setup.yaml."
    foreach name [dict keys [get_flow_config -quiet rc_corners]] {
      foreach file [get_flow_config -quiet rc_corners $name qrc_tech_file] {
        check "[file exists $file] && [file readable $file]" "The qrc_tech file: $file was not found or is not readable for rc_corner: $name"
      }
    }
    #- Check: timing_conditions are defined
    check "[llength [get_flow_config -quiet timing_conditions]]" "timing_condition objects are required for MMMC.  None were found in the MMMC section of setup.yaml."
    foreach name [dict keys [get_flow_config -quiet timing_conditions]] {
      check "[llength [get_flow_config -quiet timing_conditions $name library_sets]]" "No library_sets found for timing_condition: $name"
      foreach tc_lib_set [get_flow_config -quiet timing_conditions $name library_sets] {
        check "[dict exists [get_flow_config -quiet library_sets] $tc_lib_set]" "The timing_condition: $name, referenced a library_set: $tc_lib_set which is not found in the library_set section of the setup.yaml\n  Possible library_sets are: [dict keys [get_flow_config -quiet library_sets]]"
      }
    }
    #- Check: delay_corners are defined
    check "[llength [get_flow_config -quiet delay_corners]]" "delay_corner objects are required for MMMC.  None were found in the MMMC section of setup.yaml."
    foreach name [dict keys [get_flow_config delay_corners]] {
      #- Check: delay_corner has early and late rc_corners specified
      check "[llength [get_flow_config -quiet delay_corners $name rc_corner early]] && [llength [get_flow_config -quiet delay_corners $name rc_corner late]]" "The rc_corner specification for delay_corner: $name is incorrect.  Need both early and late rc_corner specified in MMMC section of setup.yaml."
      check "[dict exists [get_flow_config -quiet rc_corners] [get_flow_config delay_corners $name rc_corner early]]" "an early rc_corner was not found for delay_corner: $name\n  Possible rc_corners are: [dict keys [get_flow_config -quiet rc_corners]]"
      check "[dict exists [get_flow_config -quiet rc_corners] [get_flow_config delay_corners $name rc_corner late]]" "a late rc_corner was not found for delay_corner: $name\n  Possible rc_corners are: [dict keys [get_flow_config -quiet rc_corners]]"
      #- Check: delay_corner has early and late timing_conditions specified
      check "[llength [get_flow_config -quiet delay_corners $name timing_condition early]] && [llength [get_flow_config -quiet delay_corners $name timing_condition late]]" "The timing_condition specification for delay_corner: $name is incorrect.  Need both early and late timing_condition specified in MMMC section of setup.yaml."
      check "[dict exists [get_flow_config -quiet timing_conditions] [get_flow_config delay_corners $name timing_condition early]]" "an early timing_condition was not found for delay_corner: $name\n  Possible timing_conditions are: [dict keys [get_flow_config -quiet timing_conditions]]"
      check "[dict exists [get_flow_config -quiet timing_conditions] [get_flow_config delay_corners $name timing_condition late]]" "a late timing_condition was not found for delay_corner: $name\n  Possible timing_conditions are: [dict keys [get_flow_config -quiet timing_conditions]]"
    }
    #- Check: constraint_modes are defined
    check "[llength [get_flow_config -quiet constraint_modes]]" "constraint_mode objects are required for MMMC.  None were found in the MMMC section of setup.yaml."
    foreach name [dict keys [get_flow_config -quiet constraint_modes]] {
      #- Check: Constraint_mode has an SDC file
      check "[llength [get_flow_config -quiet constraint_modes $name sdc_files]]" "sdc_files are required for constraint_mode: $name"
      foreach file [get_flow_config -quiet constraint_modes $name sdc_files] {
        check "[file exists $file] && [file readable $file]" "The sdc file: $file was not found or is not readable for constraint_mode: $name"
      }
    }
    #- Check: analysis_views are defined
    check "[llength [get_flow_config -quiet analysis_views]]" "analysis_view objects are required for MMMC.  None were found in the MMMC section of setup.yaml."
    foreach name [dict keys [get_flow_config -quiet analysis_views]] {
      #- Check: Analysis_view has a constraint_mode
      check "[dict exists [get_flow_config -quiet constraint_modes] [get_flow_config analysis_views $name constraint_mode]]" "a constraint_mode was not found for analysis_view: $name\n  Possible constraint_modes are: [dict keys [get_flow_config -quiet constraint_modes]]"
      #- Check: Analysis_view has a delay_corner
      check "[dict exists [get_flow_config -quiet delay_corners] [get_flow_config analysis_views $name delay_corner]]" "a delay_corner was not found for analysis_view: $name\n  Possible delay_corners are: [dict keys [get_flow_config -quiet delay_corners]]"
      #- Sort views by purpose
      if [string is true -strict [get_flow_config -quiet analysis_views $name is_setup]] {
        lappend analysis_view_is_setup_list $name
      }
      if [string is true -strict [get_flow_config -quiet analysis_views $name is_hold]] {
        lappend analysis_view_is_hold_list $name
      }
      if [string is true -strict [get_flow_config -quiet analysis_views $name is_leakage]] {
        lappend analysis_view_is_leakage_list $name
      }
      if [string is true -strict [get_flow_config -quiet analysis_views $name is_dynamic]] {
        lappend analysis_view_is_dynamic_list $name
      }
    }
    #- Check: Analysis_view has an active setup view
    check "[info exists analysis_view_is_setup_list]" "At least 1 view is required for setup analysis"
  
    #- Check: Analysis_view has an active hold view
    check "[info exists analysis_view_is_hold_list]" "At least 1 view is required for hold analysis"
  
    #- Check: Analysis_view has no more than one leakage view
    if {[info exists analysis_view_is_leakage_list]} {
      check "[llength $analysis_view_is_leakage_list] == 1" "Only one leakage view can be specified. Please select only one of the views specified: $analysis_view_is_leakage_list"
    }
    #- Check: Analysis_view has no more than one dynamic view
    if {[info exists analysis_view_is_dynamic_list]} {
      check "[llength $analysis_view_is_dynamic_list] == 1" "Only one dynamic view can be specified. Please select only one of the views specified: $analysis_view_is_dynamic_list"
    }
    #- Check: Analysis_view has both leakage andy dynamic views when specified
    if {[info exists analysis_view_is_leakage_list] || [info exists analysis_view_is_dynamic_list]} {
      check "[info exists analysis_view_is_leakage_list] && [info exists analysis_view_is_dynamic_list]" "Must specify both a leakage and a dynamic view when either is selected."
    }
  }
  namespace delete stylus::check
}

##############################################################################
# STEP read_physical
##############################################################################
create_flow_step -name read_physical -owner cadence {
  if {![string is space [get_flow_config -quiet init_physical_files lef_files]]} {
    read_physical -lef [get_flow_config init_physical_files lef_files]
  }
  if {![string is space [get_flow_config -quiet init_physical_files oa_ref_libs]]} {
    if {![string is space [get_flow_config -quiet init_physical_files oa_search_libs]]} {
      read_physical -oa_ref_libs [get_flow_config init_physical_files oa_ref_libs] -oa_search_libs [get_flow_config init_physical_files oa_search_libs]
    } else {
      read_physical -oa_ref_libs [get_flow_config init_physical_files oa_ref_libs]
    }
  }
} -check {
  if {[llength [get_flow_config -quiet init_physical_files lef_files]] && [llength [get_flow_config -quiet init_physical_files oa_ref_libs]]} {
    check "0" "The read_physical specification is incorrect in setup.yaml. Please specify either lef_files or oa_ref_libs but not both."
  } elseif {![string is space [get_flow_config -quiet init_physical_files oa_search_libs]]} {
    check "[llength [get_flow_config -quiet init_physical_files oa_ref_libs]]" "Can not specify oa_search_libs without specifying oa_ref_libs for read_physical section in setup.yaml"
    check "[llength [get_flow_config -quiet init_physical_files lef_files]] == 0" "Can not specify oa_search_libs with lef_files for read_physical section in setup.yaml.  Only use oa_search_libs when specifying oa_ref_libs."
  } elseif {[llength [get_flow_config -quiet init_physical_files lef_files]] || [llength [get_flow_config -quiet init_physical_files oa_ref_libs]]} {
    check "!([llength [get_flow_config -quiet init_physical_files lef_files]] && \
           [llength [get_flow_config -quiet init_physical_files oa_ref_libs]])" "The read_physical specification is incomplete in setup.yaml.  Please specify either lef_files or oa_ref_libs."
    #- check that lef_files are readable by user
    if {[llength [get_flow_config -quiet init_physical_files lef_files]]} {
      foreach file [get_flow_config -quiet init_physical_files lef_files] {
        check "[file exists $file] && [file readable $file]" "The file: $file was not found or is not readable for init_physical_files lef_files section in setup.yaml"
      }
    }
  } else {
    check "[llength [get_flow_config -quiet init_physical_files lef_files]] || [llength [get_flow_config -quiet init_physical_files oa_ref_libs]]" "The read_physical specification is incomplete in setup.yaml. Please specify either lef_files or oa_ref_libs."
  }
}

##############################################################################
# STEP read_power_intent
##############################################################################
create_flow_step -name read_power_intent -owner cadence {
  if {![string is space [get_flow_config -quiet init_power_intent_files 1801]]} {
    read_power_intent -1801 [get_flow_config init_power_intent_files 1801]
  }
  if {![string is space [get_flow_config -quiet init_power_intent_files cpf]]} {
    read_power_intent -cpf [get_flow_config init_power_intent_files cpf]
  }
} -check {
  #check "[llength [get_flow_config -quiet init_power_intent_files]]" "The init_power_intent_files specification is incomplete in setup.yaml"
  if {![string is space [get_flow_config -quiet init_power_intent_files cpf]]} {
    foreach file [get_flow_config -quiet init_power_intent_files cpf] {
      check "[file exists $file] && [file readable $file]" "The CPF power_intent file: $file was not found or is not readable init_power_intent_files section in setup.yaml"
    }
  }
  if {![string is space [get_flow_config -quiet init_power_intent_files 1801]]} {
    foreach file [get_flow_config -quiet init_power_intent_files 1801] {
      check "[file exists $file] && [file readable $file]" "The 1801 power_intent file: $file was not found or is not readable init_power_intent_files section in setup.yaml"
    }
  }
}

##############################################################################
# STEP run_init_design
##############################################################################
create_flow_step -name run_init_design -owner cadence {
  <%? {init_ground_nets} return "set_db init_ground_nets [list [get_flow_config init_ground_nets]]" %>
  <%? {init_power_nets} return "set_db init_power_nets [list [get_flow_config init_power_nets]]" %>
  
  init_design
}

##############################################################################
# STEP commit_power_intent
##############################################################################
create_flow_step -name commit_power_intent -owner cadence {
  commit_power_intent
}
#===============================================================================
# Common flow_steps for tool init
#===============================================================================

##############################################################################
# STEP init_genus_yaml
##############################################################################
create_flow_step -name init_genus_yaml -owner cadence {
  # Routing attributes  [get_db -category route]
  #-------------------------------------------------------------------------------
  <%? {routing_layers top} return "get_db layers -if \".layer_index > \[get_db \[get_db layers [get_flow_config routing_layers top]\] .layer_index\]\" -foreach {set_db \$obj(.) .utilization 0}" %>
  <%? {routing_layers bottom} return "get_db layers -if \".layer_index < \[get_db \[get_db layers [get_flow_config routing_layers bottom]\] .layer_index\]\" -foreach {set_db \$obj(.) .utilization 0}" %>
} -check {
}

##############################################################################
# STEP init_innovus_yaml
##############################################################################
create_flow_step -name init_innovus_yaml -owner cadence {
  # Init attributes  [get_db -category init]
  #-------------------------------------------------------------------------------
  set_db write_lec_directory_naming_style "fv/%s/[get_db flow_report_name]"
  
  # Design attributes  [get_db -category design]
  #-------------------------------------------------------------------------------
  set_db design_process_node            <%= $design_process_node %>
  <%? {design_flow_effort} return "set_db design_flow_effort [get_flow_config design_flow_effort]" %>
  <%? {design_power_effort} return "set_db design_power_effort [get_flow_config design_power_effort]" %>
  
  # Timing attributes  [get_db -category timing && delaycalc]
  #-------------------------------------------------------------------------------
  set_db timing_analysis_cppr           both
  set_db timing_analysis_type           ocv
  <%? {timing_analysis_aocv} return "set_db timing_analysis_aocv [get_flow_config timing_analysis_aocv]" %>
  <%? {timing_analysis_socv} return "set_db timing_analysis_socv [get_flow_config timing_analysis_socv]" %>
  
  # Extraction attributes  [get_db -category extract_rc]
  #-------------------------------------------------------------------------------
  if [is_flow -after_history flow:route] {
    set_db delaycal_enable_si           true
    set_db extract_rc_engine            post_route
  }
  
  # Tieoff attributes  [get_db -category add_tieoffs]
  #-------------------------------------------------------------------------------
  <%? {add_tieoffs_cells} return "set_db add_tieoffs_cells [list [get_flow_config add_tieoffs_cells]]" %>
  <%? {add_tieoffs_max_distance} return "set_db add_tieoffs_max_distance [get_flow_config add_tieoffs_max_distance]" %>
  <%? {add_tieoffs_max_fanout} return "set_db add_tieoffs_max_fanout [get_flow_config add_tieoffs_max_fanout]" %>
  
  # Optimization attributes  [get_db -category opt]
  #-------------------------------------------------------------------------------
  set_db opt_new_inst_prefix            "[get_db flow_report_name]_"
  
  # Clock attributes  [get_db -category cts]
  #-------------------------------------------------------------------------------
  <%? {cts_target_skew} return "set_db cts_target_skew [get_flow_config cts_target_skew]" %>
  <%? {cts_target_max_transition top} return "set_db cts_target_max_transition_time_top [get_flow_config cts_target_max_transition top]" %>
  <%? {cts_target_max_transition trunk} return "set_db cts_target_max_transition_time_trunk [get_flow_config cts_target_max_transition trunk]" %>
  <%? {cts_target_max_transition leaf} return "set_db cts_target_max_transition_time_leaf [get_flow_config cts_target_max_transition leaf]" %>
  
  <%? {cts_buffer_cells} return "set_db cts_buffer_cells [list [get_flow_config cts_buffer_cells]]" %>
  <%? {cts_inverter_cells} return "set_db cts_inverter_cells [list [get_flow_config cts_inverter_cells]]" %>
  <%? {cts_clock_gating_cells} return "set_db cts_clock_gating_cells [list [get_flow_config cts_clock_gating_cells]]" %>
  <%? {cts_logic_cells} return "set_db cts_logic_cells [list [get_flow_config cts_logic_cells]]" %>
  
  # Filler attributes  [get_db -category add_fillers]
  #-------------------------------------------------------------------------------
  <%? {add_fillers_cells} return "set_db add_fillers_cells [list [get_flow_config add_fillers_cells]]" %>
  
  # Routing attributes  [get_db -category route]
  #-------------------------------------------------------------------------------
  <%? {route_design_process_node} return "set_db route_design_process_node [get_flow_config route_design_process_node]" %>
  <%? {routing_layers top} return "set_db route_design_top_routing_layer \[get_db \[get_db layers [get_flow_config routing_layers top]\] .route_index\]" %>
  <%? {routing_layers bottom} return "set_db route_design_bottom_routing_layer \[get_db \[get_db layers [get_flow_config routing_layers bottom]\] .route_index\]" %>
} -check {
  check "[llength [get_flow_config -quiet design_process_node]]" "The design_process node specification is incomplete in setup.yaml"
  check "!([string is true -strict [get_flow_config -quiet timing_analysis_aocv]] && [string is true -strict [get_flow_config -quiet timing_analysis_socv]])" "Select only one of timing_analysis_socv or timing_analysis_aocv"
  if {[llength [get_flow_config -quiet routing_layers top]]} {
    check "![string is integer [get_flow_config -quiet routing_layers top]]" "The route_design top layer must be an string.  Correct route_design_layers section in setup.yaml"
  }
  if {[llength [get_flow_config -quiet routing_layers bottom]]} {
    check "![string is integer [get_flow_config -quiet routing_layers bottom]]" "The route_design bottom layer must be an string.  Correct route_design_layers section in setup.yaml"
  }
}

##############################################################################
# STEP init_tempus_yaml
##############################################################################
create_flow_step -name init_tempus_yaml -owner cadence {
  # Design attributes  [get_db -category design]
  #-------------------------------------------------------------------------------
  set_db design_process_node            <%= $design_process_node %>
  
  # Timing attributes  [get_db -category timing]
  #-------------------------------------------------------------------------------
  set_db timing_analysis_cppr                       both
  set_db timing_analysis_type                       ocv
  set_db timing_enable_simultaneous_setup_hold_mode true
  <%? {timing_analysis_aocv} return "set_db timing_analysis_aocv [get_flow_config timing_analysis_aocv]" %>
  <%? {timing_analysis_socv} return "set_db timing_analysis_socv [get_flow_config timing_analysis_socv]" %>
  
  # Delaycal attributes  [get_db -category delaycal]
  #-------------------------------------------------------------------------------
  set_db delaycal_enable_si                         true
} -check {
  check "[llength [get_flow_config -quiet design_process_node]]" "The design_process_node specification is incomplete in setup.yaml"
  check "!([string is true -strict [get_flow_config -quiet timing_analysis_aocv]] && [string is true -strict [get_flow_config -quiet timing_analysis_socv]])" "Select only one of timing_analysis_socv or timing_analysis_aocv"
}

#===============================================================================
# Common flow_steps for implementation
#===============================================================================

##############################################################################
# STEP block_start
##############################################################################
create_flow_step -name block_start -owner cadence {
}

##############################################################################
# STEP block_finish
##############################################################################
create_flow_step -name block_finish -owner cadence -write_db -categories flow {
  #- Make sure flow_report_name is reset from any reports executed during the flow
  set_db flow_report_name [get_db [lindex [get_db flow_hier_path] end] .name]
  
  #- Store non-default root attributes to metrics
  catch {report_obj -tcl} flow_root_config
  if [dict exists $flow_root_config root:/] {
    set flow_root_config [dict get $flow_root_config root:/]
  } elseif [dict exists $flow_root_config root:] {
    set flow_root_config [dict get $flow_root_config root:]
  } else {
  }
  foreach key [dict keys $flow_root_config] {
    if {[string length [dict get $flow_root_config $key]] > 200} {
      dict set flow_root_config $key "\[long value truncated\]"
    }
  }
  set_metric -name flow.root_config -value $flow_root_config
}

#===============================================================================
# Common flow_step for reporting
#===============================================================================

##############################################################################
# STEP report_start
##############################################################################
create_flow_step -name report_start -owner cadence {
}

##############################################################################
# STEP report_finish
##############################################################################
create_flow_step -name report_finish -owner cadence -categories flow {
}

##############################################################################
# STEP signoff_start
##############################################################################
create_flow_step -name signoff_start -owner cadence {
}

##############################################################################
# STEP signoff_finish
##############################################################################
create_flow_step -name signoff_finish -owner cadence -categories flow {
}

##############################################################################
# STEP innovus_to_quantus
##############################################################################
create_flow_step -name innovus_to_quantus -owner cadence {
  #- create output location
  set design  [get_db current_design .name]
  set out_dir [file join [get_db flow_db_directory] [get_db flow_report_name]]
  
  if {![file exists $out_dir]} {
    file mkdir $out_dir
  }
  
  #- write extraction command file
  write_extraction_spec -out_dir [file join [get_db flow_db_directory] [get_db flow_report_name]]
  file rename -force qrc.cmd [file join [get_db flow_db_directory] [get_db flow_report_name] qrc.cmd]
}

##############################################################################
# STEP innovus_to_tempus
##############################################################################
create_flow_step -name innovus_to_tempus -owner cadence {
  #- create output location
  set design  [get_db current_design .name]
  set out_dir [file join [get_db flow_db_directory] [get_db flow_report_name]]
  
  if {![file exists $out_dir]} {
    file mkdir $out_dir
  }
  
  #- write design and library information
  write_netlist -top_module_first -top_module $design [file join $out_dir ${design}.v.gz]
  write_mmmc > [file join $out_dir mmmc_config.tcl]
  foreach power_intent [lsort -unique [dict keys [get_db init_power_intent_files]]] {
    switch [lindex $power_intent 0] {
      default {}
    }
  }
  #- write init_design sequence for STA flow
  set FH [open $out_dir/init_sta.tcl w]
  puts $FH "read_mmmc [file normalize [file join $out_dir mmmc_config.tcl]]"
  puts $FH "read_netlist [file normalize [file join $out_dir ${design}.v.gz]]"
  if {[dict values [get_db init_power_intent_files]] ne {{}}} {
    foreach power_intent [lsort -unique [dict keys [get_db init_power_intent_files]]] {
      switch [lindex $power_intent 0] {
        cpf     {write_power_intent -cpf [file join $out_dir ${design}.cpf]
                 puts $FH "read_power_intent -cpf [file normalize [file join $out_dir ${design}.cpf]]"
                }
        1801    {write_power_intent -1801 [file join $out_dir ${design}.upf]
                 puts $FH "read_power_intent -1801 [file normalize [file join $out_dir ${design}.upf]]"
                }
        default {}
      }
    }
  }
  puts $FH "init_design"
  puts $FH "set_db flow_report_name [get_db flow_report_name]"
  close $FH
}

##############################################################################
# STEP schedule_sta
##############################################################################
create_flow_step -name schedule_sta -owner cadence {
  #FlowtoolPredictHint ArgumentRandomise -branch
  schedule_flow \
    -flow sta \
    -branch [get_db flow_branch] \
    -db [file join [get_db flow_db_directory] [get_db flow_report_name] init_sta.tcl] \
    -include_in_metrics \
    -no_sync
}

##############################################################################
# STEP schedule_signoff_subflows
##############################################################################
create_flow_step -name schedule_signoff_subflows -owner cadence -exclude_time_metric {
  #FlowtoolPredictHint ArgumentRandomise -branch
  
  schedule_flow \
    -flow extract \
    -branch [get_db flow_branch] \
    -tool_options "-log_file [file join [get_db flow_log_directory] extract.[get_db flow_report_name].log] -cmd [file join [get_db flow_db_directory] [get_db flow_report_name] qrc.cmd]" \
    -include_in_metrics
  
  schedule_flow \
    -flow sta \
    -branch [get_db flow_branch] \
    -db [file join [get_db flow_db_directory] [get_db flow_report_name] init_sta.tcl] \
    -include_in_metrics \
    -sync
}

##############################################################################
# STEP read_parasitics
##############################################################################
create_flow_step -name read_parasitics -owner cadence {
  #- initialize annotations using spef
  if {[is_flow -inside flow:ir_static] || [is_flow -inside flow:ir_dynamic] || [is_flow -inside flow:ir_rampup]} {
    set views [get_db -u analysis_views -if {.is_leakage || .is_dynamic}]
    set spef_dir  [file normalize [file join [get_db flow_working_directory] [get_db flow_db_directory] [file rootname [get_db flow_report_name]]]]
    set cmd "read_spef -decoupled"
  } else {
    set views [get_db -u analysis_views -if {.is_setup || .is_hold || .is_leakage || .is_dynamic}]
    set spef_dir  [file normalize [file join [get_db flow_working_directory] [file dirname [lindex [get_db flow_starting_db] 1]]]]
    set cmd "read_spef"
  }
  set corners [lsort -u [concat [get_db -u $views .delay_corner.late_rc_corner] \
                                [get_db -u $views .delay_corner.early_rc_corner]]]
  
  foreach corner $corners {
    set corner_name [get_db ${corner} .name]
    set file [glob -nocomplain -directory ${spef_dir} *${corner_name}.spef*]
    if [file exists ${file}] {
      eval "$cmd -rc_corner ${corner_name} ${file}"
    } else {
      error "ERROR: spef file not found for rc_corner ${corner_name}"
    }
  }
}
