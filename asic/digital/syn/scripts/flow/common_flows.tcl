# Flowkit v18.10-p005_1
#+--------------------------+--------------------------------------------------------------------------------+----------+
#| Feature                  | Description                                                                    | Value    |
#+--------------------------+--------------------------------------------------------------------------------+----------+
#| clock_design             | Run skew based clock expansion                                                 | disabled |
#--- the following features are mutually exclusive (dft_style group)
#| dft_compressor           | Add flow support for scan chains with compression insertion                    | disabled |
#| dft_simple               | Add flow support for scan chain insertion                                      | disabled |
#---
#| ff_setup                 | Enable reading design config from legacy FF flow setup.tcl file                | disabled |
#| flow_express             | Enable express synthesis and implementation flow                               | disabled |
#| opt_early_cts            | Implement early clock tree for use during prects optimization (LIMITED ACCESS) | disabled |
#| opt_eco                  | Run opt_design during eco flow                                                 | disabled |
#| opt_em                   | Run EM (Electromigration) optimization during implementation flow              | disabled |
#| opt_postcts_hold_disable | Disable postcts hold fixing                                                    | disabled |
#| opt_postcts_split        | Run postcts opt_design for setup and hold as separate steps                    | disabled |
#| opt_postroute_split      | Run postroute opt_design for setup and hold as separate steps                  | disabled |
#| opt_preroute             | Run combined preroute optimization flow (LIMITED ACCESS)                       | disabled |
#| opt_route                | Run combined postroute optimization flow (LIMITED ACCESS)                      | disabled |
#| opt_signoff              | Run opt_signoff during implementation flow                                     | disabled |
#| report_clp               | Add CLP dofile generation and checks to the flow                               | disabled |
#--- the following features are mutually exclusive (report_style group)
#| report_inline            | Run report generation as part of parent flow versus schedule_flow              | disabled |
#| report_none              | Disable report generation                                                      | disabled |
#---
#| report_lec               | Add LEC dofile generation and checks to the flow                               | enabled  |
#| route_secondary_nets     | Route secondary PG nets before route_design                                    | disabled |
#| route_track_opt          | Adds track based optimization to route_design                                  | disabled |
#| sta_dmmmc                | Use distributed MMMC architecture for running STA runs                         | disabled |
#| sta_eco                  | Run opt_signoff during signoff flow                                            | disabled |
#| sta_glitch               | Add glitch analysis reports to STA flow                                        | disabled |
#| sta_use_setup_yaml       | Use MMMC and power_intent from setup.yaml over those exported via Innovus      | disabled |
#--- the following features are mutually exclusive (synth_style group)
#| synth_physical           | Full physically aware synthesis flow                                           | disabled |
#| synth_spatial            | Physically aware synthesis flow with spatial final optimization                | disabled |
#---
#+--------------------------+--------------------------------------------------------------------------------+----------+

set_db flow_template_type {stylus}
set_db flow_template_version {1}
set_db flow_template_feature_definition {flow_express 0 report_inline 0 report_none 0 report_lec 1 report_clp 0 dft_simple 0 dft_compressor 0 synth_spatial 0 synth_physical 0 opt_early_cts 0 opt_preroute 0 clock_design 0 opt_postcts_hold_disable 0 opt_postcts_split 0 route_track_opt 0 route_secondary_nets 0 opt_postroute_split 0 opt_route 0 opt_signoff 0 opt_em 0 opt_eco 0 sta_use_setup_yaml 0 sta_dmmmc 0 sta_glitch 0 sta_eco 0 ff_setup 0}
#===============================================================================
# Common flow attributes
#===============================================================================

#- Define attribute for flow script path
if { ![is_attribute -obj_type root flow_source_directory] } {
  define_attribute flow_source_directory \
    -category flow \
    -data_type string \
    -default "" \
    -help_string "Flow script source location" \
    -obj_type root
}
set_db flow_source_directory [file dirname [file normalize [info script]/..]]

#- Define attribute for report name
if { ![is_attribute -obj_type root flow_report_name]} {
  define_attribute flow_report_name \
    -category flow \
    -data_type string \
    -default "" \
    -help_string "Name to use during report generation" \
    -obj_type root
}

#- Define attribute for report prefix
if { ![is_attribute -obj_type root flow_report_prefix]} {
  define_attribute flow_report_prefix \
    -category flow \
    -data_type string \
    -default "" \
    -help_string "File prefix to use during report generation" \
    -obj_type root
}

#- Specify Flow Header (runs at the start of run_flow command)
set_db flow_header_tcl {
  #- extend flow report name based on context
  if {[is_flow -inside flow:sta] || [is_flow -inside flow:sta_dmmmc] || [is_flow -inside flow:sta_eco]} {
    if {![regexp {sta} [get_db flow_report_name]]} {
      set_db flow_report_name [expr {[string is space [get_db flow_report_name]] ? "sta" : "[get_db flow_report_name].sta"}]
    }
  } elseif {[is_flow -inside flow:ir_grid] || [is_flow -inside flow:ir_static] || [is_flow -inside flow:ir_dynamic] || [is_flow -inside flow:ir_rampup]} {
    set_db flow_report_name [expr {[string is space [get_db flow_report_name]] ? "ir" : "[get_db flow_report_name].ir"}]
  } elseif {[regexp {block_start|hier_start|eco_start} [get_db flow_step_current]]} {
    set_db flow_report_name [get_db [lindex [get_db flow_hier_path] end] .name]
  } else {
  }

  #- Create report dir (if necessary)
  file mkdir [file normalize [file join [get_db flow_report_directory] [get_db flow_report_name]]]
}

#- Specify Flow Footer (runs at the conclusion of run_flow command)
set_db flow_footer_tcl {
  if {![regexp {modus} [get_db program_short_name]]} {
    #- Write the html run summary
    report_metric \
      -file [get_db flow_report_directory]/qor.html \
      -format html
  }
}




#=============================================================================
# Flow: Implementation Flows
#=============================================================================

create_flow -name init_design -owner cadence -tool genus -skip_metric {read_mmmc read_physical read_hdl read_power_intent run_init_design commit_power_intent}

create_flow -name init_genus -owner cadence -tool genus -skip_metric {init_genus_yaml init_genus_user}

create_flow -name syn_generic -owner cadence -tool genus -tool_options -disable_user_startup {block_start init_elaborate init_design init_genus set_dont_use create_cost_group run_syn_generic block_finish schedule_syn_generic_report_synth}

create_flow -name syn_map -owner cadence -tool genus -tool_options -disable_user_startup {block_start init_genus run_syn_map block_finish schedule_syn_map_report_synth genus_to_lec}

create_flow -name syn_opt -owner cadence -tool genus -tool_options -disable_user_startup {block_start init_genus run_syn_opt block_finish schedule_syn_opt_report_synth genus_to_lec genus_to_innovus}


create_flow -name init_innovus -owner cadence -tool innovus -skip_metric {init_innovus_yaml init_innovus_user}

create_flow -name floorplan -owner cadence -tool innovus -tool_options -disable_user_startup {block_start init_innovus init_floorplan add_tracks block_finish schedule_floorplan_report_floorplan innovus_to_lec}


create_flow -name prects -owner cadence -tool innovus -tool_options -disable_user_startup {block_start init_innovus add_clock_spec add_clock_route_types commit_route_types run_place_opt block_finish schedule_prects_report_prects}


create_flow -name cts -owner cadence -tool innovus -tool_options -disable_user_startup {block_start init_innovus add_clock_tree add_tieoffs block_finish schedule_cts_report_postcts}


create_flow -name postcts -owner cadence -tool innovus -tool_options -disable_user_startup {block_start init_innovus run_opt_postcts_hold block_finish schedule_postcts_report_postcts}


create_flow -name route -owner cadence -tool innovus -tool_options -disable_user_startup {block_start init_innovus add_fillers run_route block_finish schedule_route_report_postroute}


create_flow -name postroute -owner cadence -tool innovus -tool_options -disable_user_startup {block_start init_innovus run_opt_postroute block_finish schedule_postroute_report_postroute innovus_to_lec innovus_to_quantus innovus_to_tempus schedule_postroute_postroute_signoff}


create_flow -name eco -owner cadence -tool innovus -tool_options -disable_user_startup {eco_start init_innovus init_eco run_place_eco run_route_eco eco_finish schedule_eco_report_postroute innovus_to_quantus innovus_to_tempus schedule_eco_eco_signoff}

#=============================================================================
# Flow: Reporting Flows
#=============================================================================

create_flow -name report_synth -owner cadence -tool genus -tool_options -disable_user_startup {report_start init_genus report_area_genus report_timing_summary_late_genus report_late_paths report_power_genus report_finish}

create_flow -name fv_genus -owner cadence -tool genus {flow_step:genus_to_lec}

create_flow -name lec -owner cadence -tool lec


create_flow -name report_floorplan -owner cadence -tool innovus -tool_options -disable_user_startup {report_start init_innovus report_area_innovus report_route_drc report_finish}

create_flow -name report_prects -owner cadence -tool innovus -tool_options -disable_user_startup {report_start init_innovus report_area_innovus report_timing_late_innovus report_late_paths report_power_innovus report_finish}

create_flow -name report_postcts -owner cadence -tool innovus -tool_options -disable_user_startup {report_start init_innovus report_area_innovus report_timing_early_innovus report_early_paths report_timing_late_innovus report_late_paths report_clock_timing report_power_innovus report_finish}

create_flow -name report_postroute -owner cadence -tool innovus -tool_options -disable_user_startup {report_start init_innovus report_area_innovus report_timing_early_innovus report_early_paths report_timing_late_innovus report_late_paths report_clock_timing report_power_innovus report_route_drc report_route_density report_finish}

create_flow -name fv_innovus -owner cadence -tool innovus {flow_step:innovus_to_lec}

create_flow -name extract -owner cadence -tool qrc


create_flow -name init_tempus -owner cadence -tool tempus -skip_metric {init_tempus_yaml init_tempus_user}

create_flow -name sta -owner cadence -tool tempus -tool_options -disable_user_startup {signoff_start init_tempus read_parasitics update_timing check_timing report_timing_late report_late_paths report_timing_early report_early_paths signoff_finish}


create_flow -name signoff -owner cadence -tool tempus {schedule_signoff_subflows}
#=============================================================================
# Flow: Toplevel Flows
#=============================================================================

create_flow -name synthesis -tool genus -owner cadence -skip_metric {syn_generic syn_map syn_opt}

create_flow -name implementation -tool innovus -owner cadence -skip_metric {floorplan prects cts postcts route postroute}

create_flow -name block -owner cadence -skip_metric {synthesis implementation}
set_db flow_current flow:block
