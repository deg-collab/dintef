proc deleteAllScanCells {} {
  #get_db base_cells SDF* -foreach { delete_scan_cell ($obj).name }
  get_db lib_cells -if .name==tcbn65lpml/tcbn65lpml_ccs/SDF* -foreach {
    set name [file tail $object]
    delete_scan_cell $name
  }
}

