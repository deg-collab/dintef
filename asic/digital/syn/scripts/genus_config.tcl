# Flowkit v18.10-p005_1
################################################################################
# Genus attributes
#
#  Flow_steps used to drive tool behavior.  Most typically these are root level
#  attributes.  All root attributes can be listed by using 'report_obj -all' or
#  by category using 'report_obj -all -verbose'
#
#  Further attribute help can be obtained by using the command 'help <ATTRIBUTE>'
#
#  Two flow_steps are provided to allow users to specify tool level configs.
#  Users may add additional flow_steps to the file which also direct tool behaviour.
#
#    init_elaborate:  specify tool settings before a design has been loaded.  These are
#                      typically attributes in the 'hdl' and 'lib_*' categories.
#
#    init_genus_user: specify tool settings after a design has been loaded via init_design
#
################################################################################

##############################################################################
# STEP init_elaborate
##############################################################################
create_flow_step -name init_elaborate -owner design {
  # HDL attributes [get_db -category hdl]
  #-----------------------------------------------------------------------------
  ::set_db root: .hdl_error_on_blackbox true
  ::set_db root: .hdl_vhdl_read_version 2008
  ::set_db root: .hdl_use_cw_first true

  #::set_db hdl_flatten_complex_port true
  ::set_db root: .information_level 2
}

##############################################################################
# STEP init_genus_user
##############################################################################
create_flow_step -name init_genus_user -owner design {
  # Optimization attributes  [get_db -category netlist]
  #-----------------------------------------------------------------------------
  #::set_db root: .syn_global_effort high
  ::set_db root: .lp_insert_clock_gating true
  
  # Datapath attributes  [get_db -category dp]
  #-----------------------------------------------------------------------------
  
  # Leakage Power attributes  [get_db -category lp_opt lib_ui]
  #-----------------------------------------------------------------------------
  
}
