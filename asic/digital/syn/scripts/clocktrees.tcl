create_flow_step -name clock_tree_specs -owner SuS {
   get_db pins -if .original_name==io_por_sync -foreach {
      set reset_net [get_db $object .net]
      set_db $reset_net .loads.cts_sink_type stop
      create_clock_tree -name [get_db $object .inst.base_name] -source [get_db $object .name]
   }

   get_db pins -if .original_name==*RESET* -foreach {
      set reset_net [get_db $object .net]
      set_db $reset_net .loads.cts_sink_type stop
      create_clock_tree -name [get_db $object .inst.base_name] -source [get_db $object .name]
   }
   #set_db skew_group:RESET_N_SYNC .cts_target_skew 0.1

   get_db nets coarse_* -foreach {
      set pins_at_flops [get_db $object .loads -if .inst.base_cell.is_latch]
      set_db $pins_at_flops .cts_sink_type stop
      create_clock_tree -name [get_db $object .name] -source [get_db $object .drivers.name]
   }
   set_db [get_db skew_groups coarse_counter*] .cts_target_skew 0.05

   get_db clock_trees > [get_db flow_report_name]-clocktrees.txt
}
