# Flowkit v18.10-p005_1
################################################################################
# This file contains content which is used to customize the refererence flow
# process.  Commands such as 'create_flow', 'create_flow_step' and 'edit_flow'
# would be most prevalent.  For example:
#
# create_flow_step -name write_sdf -owner user -write_db {
#   write_sdf [get_db flow_report_directory]/[get_db flow_report_name].sdf
# }
#
# edit_flow -after flow_step:innovus_report_late_timing -append flow_step:write_sdf
#
################################################################################

################################################################################
# FLOW CPU AND HOST SETTINGS
################################################################################
create_flow_step -name init_mcpu -owner flow {
  # Multi host/cpu attributes
  #-----------------------------------------------------------------------------
  if {[info exists ::env(LSB_MAX_NUM_PROCESSORS)]} {
    set max_cpus $::env(LSB_MAX_NUM_PROCESSORS)
  } else {
    set max_cpus 8
  }
  switch -glob [get_db program_short_name] {
    joules*       -
    genus*        {
                    set_db max_cpus_per_server                $max_cpus
                  }
    innovus*      -
    tempus*       -
    voltus*       {
                    set_multi_cpu_usage -local_cpu            $max_cpus
                  }
    default       {}
  }
}
edit_flow -after Cadence.plugin.flowkit.read_db.pre -append flow_step:init_mcpu

################################################################################
# FLOW CUSTOMIZATIONS / FLOW STEP ADDITIONS
################################################################################

##############################################################################
# STEP report_late_paths
##############################################################################
create_flow_step -name report_late_paths -owner flow -exclude_time_metric {
  #- Reports that show detailed timing with Graph Based Analysis (GBA)
  report_timing -max_paths 5   -nworst 1 -path_type endpoint        > [get_db flow_report_directory]/[get_db flow_report_name]/setup.endpoint.rpt
  report_timing -max_paths 1   -nworst 1 -path_type full_clock -net > [get_db flow_report_directory]/[get_db flow_report_name]/setup.worst.rpt
  report_timing -max_paths 500 -nworst 1 -path_type full_clock      > [get_db flow_report_directory]/[get_db flow_report_name]/setup.gba.rpt
  
  #- Reports that show detailed timing with Path Based Analysis (PBA)
  if {[is_flow -inside flow:sta]} {
    report_timing -max_paths 50 -nworst 1 -path_type full_clock -retime path_slew_propagation > [get_db flow_report_directory]/[get_db flow_report_name]/setup.pba.rpt
  }
}

##############################################################################
# STEP report_early_paths
##############################################################################
create_flow_step -name report_early_paths -owner flow -exclude_time_metric {
  #- Reports that show detailed early timing with Graph Based Analysis (GBA)
  report_timing -early -max_paths 5   -nworst 1 -path_type endpoint        > [get_db flow_report_directory]/[get_db flow_report_name]/hold.endpoint.rpt
  report_timing -early -max_paths 1   -nworst 1 -path_type full_clock -net > [get_db flow_report_directory]/[get_db flow_report_name]/hold.worst.rpt
  report_timing -early -max_paths 500 -nworst 1 -path_type full_clock      > [get_db flow_report_directory]/[get_db flow_report_name]/hold.gba.rpt
  
  #- Reports that show detailed timing with Path Based Analysis (PBA)
  if {[is_flow -inside flow:sta]} {
    report_timing -early -max_paths 50 -nworst 1 -path_type full_clock -retime path_slew_propagation  > [get_db flow_report_directory]/[get_db flow_report_name]/hold.pba.rpt
  }
}

##############################################################################
# STEP genus_to_lec
##############################################################################
create_flow_step -name genus_to_lec -owner flow {
  #- create output location
  set design  [get_db current_design .name]
  set out_dir [file join [get_db flow_db_directory] [get_db flow_report_name]]
  file mkdir $out_dir
  
  #- write dofile for LEC
  if {[is_flow -inside flow:syn_map]} {
    write_do_lec \
      -top $design \
      -golden_design rtl \
      -revised_design fv_map \
      -logfile [file join [get_db flow_log_directory] lec.[get_db flow_report_name].log] \
      -save_session [file join [get_db current_design .verification_directory] lec.[get_db flow_report_name].session] \
      > [file join [get_db current_design .verification_directory] lec.[get_db flow_report_name].do]
  } else {
    write_do_lec \
      -top $design \
      -golden_design fv_map \
      -revised_design [file join $out_dir $design.v.gz] \
      -logfile [file join [get_db flow_log_directory] lec.[get_db flow_report_name].log] \
      -save_session [file join [get_db current_design .verification_directory] lec.[get_db flow_report_name].session] \
      > [file join [get_db current_design .verification_directory] lec.[get_db flow_report_name].do]
  }
  
  #- schedule the LEC flow
  #FlowtoolPredictHint ArgumentRandomise -branch
  schedule_flow \
    -flow lec \
    -branch [get_db flow_report_name] \
    -no_db \
    -no_sync \
    -tool_options "-nogui -lp -do [file join [get_db current_design .verification_directory] lec.[get_db flow_report_name].do]"
}

