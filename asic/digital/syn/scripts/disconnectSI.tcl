##############
proc disconnectSI {} {
  set op [open detach_term.tcl w]
  foreach inst_ptr [get_db top.insts.instTerms.name */SI] {
    set inst_name [get_db $inst_ptr.inst.name]
    set net_name [get_db $inst_ptr.net.name] 
    if {$net_name == "0x0"} {continue}
    puts $op "detachTerm $inst_name SI $net_name"  
  }
  close $op
  puts "Please source the created file detach_term.tcl"
}
##############
  #foreach inst_ptr [get_db top.insts.instTerms.name */SI -p] {
