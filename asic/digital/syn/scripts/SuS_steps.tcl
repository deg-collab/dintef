create_flow_step -name output_netlist -owner SuS {
   set design_name [get_db current_design .name]
   mkdir -p stream_out

   create_pg_pin -name VDD -net VDD
   create_pg_pin -name VSS -net VSS

   #Write the final netlist. -phys is necessary for LVS
   write_netlist -include_pg_ports -include_phys_insts -exclude_leaf_cells stream_out/$design_name.physical.v

   #save an additional version of the netlist without power and physical for use in post place and route simulation
   write_netlist stream_out/$design_name.v

   #The "-edges noedge" switch is necessary for annotating the SDF file to the standard cell models.
   get_db analysis_views -foreach {
      set view [get_db $obj(.name)]
      write_sdf -view $view -edges noedge stream_out/$design_name.$view.sdf
   }
}

create_flow_step -name output_physical -owner SuS {
   set design_name [get_db current_design .name]
   foreach term [get_db current_design .ports] {
      set pt [get_db $term .location]
      set port [get_db $term .original_name]
      set layer [get_db $term .layer.name]

      foreach p $pt {
          if { ([get_db $term .side] == "south") || ([get_db $term .side] == "north") } {
             set orient r90
          } else {
             set orient r0
          }
          create_text -layer $layer -label $port -point $p -height 0.2 -orient $orient
      }
   }
   foreach term [get_db current_design .pg_ports] {
      set pt [get_db $term .location]
      set port [get_db $term .name]
      set layer [get_db $term .layer.name]

      foreach p $pt {
          create_text -layer $layer -label $port -point $p -height 2.0
      }
   }

   create_text -layer 9 -label VSS -point 0 -175 -height 2.0
   create_text -layer 9 -label VDD -point 0 -190 -height 2.0

   #Stream out the GDS2 file
   mkdir -p stream_out
   write_stream stream_out/$design_name.gds -map_file [get_flow_config gds_layer_map_file]
   #write_stream stream_out/$design_name.full.gds -map_file /opt/eda/TSMC/65/__UNPACKED__/PDK/PDK/tsmcN65/tsmcN65.layermap -merge {/opt/eda/TSMC/65/__UNPACKED__/stclib/stclib-abstract.gds}
   write_stream stream_out/$design_name.full.gds -map_file [get_flow_config gds_layer_map_file] -merge {/opt/eda/TSMC/65/__UNPACKED__/stclib/stclib-abstract.gds}
}

create_flow_step -name create_check_reports -owner SUS {
   check_connectivity -error 1000 -warning 50 -ignore_dangling_wires -check_pg_ports -check_geometry_loops -out_file stream_out/check_connectivity.rpt
   check_process_antenna -error 1000 -out_file stream_out/check_antenna.rpt
   check_ac_limits -toggle 1.0 -max_error 1000 -out_file stream_out/check_ac_limits.rpt
}

create_flow_step -name clock_tree_specs -owner SuS {
   # for setup
   get_db delay_corners -if .is_setup -foreach {
      set_timing_derate -delay_corner $object -early 1.00
      set_timing_derate -delay_corner $object -late 1.05
      set_timing_derate -delay_corner $object -clock 0.95
   }

   # for hold
   get_db delay_corners -if .is_hold -foreach {
      set_timing_derate -delay_corner $object -early 0.95
      set_timing_derate -delay_corner $object -late 1.00
      set_timing_derate -delay_corner $object -clock 1.05
   }

   get_db pins -if .original_name==*RESET* -foreach {
      set reset_net [get_db $object .net]
      set_db $reset_net .loads.cts_sink_type stop
      create_clock_tree -name [get_db $object .inst.base_name]_[get_db $object .base_pin.base_name] -source [get_db $object .name]
   }
   #set_db skew_group:RESET_N_SYNC .cts_target_skew 0.1

   get_db nets coarse_* -foreach {
      set pins_at_flops [get_db $object .loads -if .inst.base_cell.is_latch]
      set_db $pins_at_flops .cts_sink_type stop
      create_clock_tree -name [get_db $object .inst.base_name]_[get_db $object .base_pin.base_name] -source [get_db $object .name]
   }
   set_db [get_db skew_groups coarse_counter*] .cts_target_skew 0.05
}

create_flow_step -name report_clock_trees -owner SuS {
   set reportfile [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]clock_trees.rpt]
   echo "Reset trees:" > $reportfile
   get_db root: .clock_trees -foreach {
      echo >> $reportfile
      echo [get_db $object .name] [get_db $object .nets]] >> $reportfile
      echo "from" [get_db $object .source] >> $reportfile
      set sinks [get_db $object .sinks]
      foreach { sink } [lsort $sinks] {
         echo " -" $sink >> $reportfile
      }
   }
}
