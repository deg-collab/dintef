set design_name ControlBlock

set_db init_power_nets VDD
set_db init_ground_nets VSS
#
source dbs/syn_opt/$design_name.invs_setup.tcl

create_floorplan -floorplan_origin center -core_margins_by die -site core -die_size 1216 400 30.0 30 30 30

set left [get_db design:$design_name .bbox.ll.x]
set right [get_db design:$design_name .bbox.ur.x]
set top [get_db design:$design_name .bbox.ur.y]
set bottom [get_db design:$design_name .bbox.ll.y]

connect_global_net VDD -verbose -type pg_pin -pin_base_name VDD -inst_base_name *
connect_global_net VSS -verbose -type pg_pin -pin_base_name VSS -inst_base_name *

add_rings -nets {VSS VDD} -type core_rings -follow core -layer {top M9 bottom M9 left M8 right M8} -width {top 10 bottom 10 left 10 right 10} -spacing {top 3 bottom 3 left 3 right 3} -offset {top 2 bottom 2 left 2 right 2} -center 0 -extend_corners {} -threshold 0 -jog_distance 0 -snap_wire_center_to_grid none
#

#add_stripes -direction horizontal -layer M9 -number_of_sets 1 -width 5 -spacing 1 -start 50 -nets { VSS VDD }
#add_stripes -direction vertical -layer M8 -number_of_sets 1 -width 5 -spacing 1 -start -155 -nets { VDD VSS }
add_stripes -direction horizontal -layer M9 -width 5 -spacing 5 -set_to_set_distance 100 -start -107.5 -nets { VSS VDD }
add_stripes -direction vertical -layer M8 -width 5 -spacing 4 -set_to_set_distance 100 -start -502.5 -nets { VDD VSS }

route_special -connect {block_pin pad_pin pad_ring core_pin floating_stripe} -layer_change_range { ME1(1) ME6(6) } -block_pin_target {nearest_target} -pad_pin_port_connect {all_port prefer_layer} -pad_pin_target {nearest_target} -core_pin_target {first_after_row_end} -floating_stripe_target {block_ring pad_ring ring stripe ring_pin block_pin followpin} -allow_jogging 1 -crossover_via_layer_range { ME1(1) ME6(6) } -allow_layer_change 1 -block_pin use_lef -target_via_layer_range { ME1(1) ME6(6) }

#set_db route_special_block_pin_route_with_pin_width true
#set_db route_special_target_search_distance 20

# TODO ?
#route_special -connect {block_pin} -layer_change_range { M1(1) M8(8) } -block_pin_target {nearest_target} -allow_jogging 0 -crossover_via_layer_range { M1(1) AP(10) } -allow_layer_change 0 -block_pin all -target_via_layer_range { M1(1) AP(10) }
#
route_special -connect {pad_ring core_pin floating_stripe} -layer_change_range { M1(1) AP(10) } -block_pin_target {nearest_target} -core_pin_target {first_after_row_end} -floating_stripe_target {block_ring pad_ring ring stripe} -allow_jogging 0 -crossover_via_layer_range { M1(1) AP(10) } -nets { VDD VSS } -allow_layer_change 1 -target_via_layer_range { M1(1) AP(10) }

proc find_pin { orig } {
   set pin [get_db ports -if { .original_name == $orig }]
   return [get_db $pin .name]
}

set_db assign_pins_edit_in_batch true
set x_pos [expr $right-1134.6]
set pinpitch 80
#edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {por_n}]      -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {res_n}]      -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {tdi}]        -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {tms}]        -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {tck}]        -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {ext_ddyn}]   -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {mon}]        -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid
#edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {trst_n}]     -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {tdo}]        -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {tdo_enable}] -assign [expr $x_pos+2] $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {ser_out[1]}] -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {ser_out[0]}] -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {seq_run}]    -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M6 -side Bottom -pin [find_pin {clk}]        -assign [set x_pos [expr $x_pos+$pinpitch]] $bottom -snap usergrid

set FE_pitch -76
set FE_offset [expr $right + $FE_pitch/2]
set i 0
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M4 -side Top -pin [find_pin comp_result_coarse[$i]]   -assign [expr $FE_offset + $i * $FE_pitch - 2] $top -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M4 -side Top -pin [find_pin pump_coarse[$i]]   -assign [expr $FE_offset + $i * $FE_pitch - 1] $top -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M4 -side Top -pin [find_pin comp_result_fine[$i]]   -assign [expr $FE_offset + $i * $FE_pitch + 1] $top -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M4 -side Top -pin [find_pin pump_fine[$i]]   -assign [expr $FE_offset + $i * $FE_pitch + 2] $top -snap usergrid
for { set i 1 } { $i < 16 } { incr i } {
  edit_pin -pin_width 0.5 -fixed_pin 1 -layer M4 -side Top -pin [find_pin cp_en_cnt_coarse[$i]] -assign [expr $FE_offset + $i * $FE_pitch - 1] $top -snap usergrid
  edit_pin -pin_width 0.5 -fixed_pin 1 -layer M4 -side Top -pin [find_pin cp_en_cnt_fine[$i]]   -assign [expr $FE_offset + $i * $FE_pitch + 1] $top -snap usergrid
}

set FE_offset [expr $right + 2*$FE_pitch]
for { set group 0 } { $group < 4 } { incr group } {
   set x_pos [expr $FE_offset + ($group * 4 * $FE_pitch) ]
   set pinpitch 2
   edit_pin -pin_width 0.5 -fixed_pin 1 -layer M3 -side Top -pin [find_pin ddyn_b[[expr $group*2 + 0]]] -assign [expr $x_pos-1 * $pinpitch] $top
   edit_pin -pin_width 0.5 -fixed_pin 1 -layer M3 -side Top -pin [find_pin ddyn_b[[expr $group*2 + 1]]] -assign [expr $x_pos-2 * $pinpitch] $top
   for { set i 0 } { $i < 10 } { incr i } {
     edit_pin -pin_width 0.5 -fixed_pin 1 -layer M4 -side Top -pin [find_pin ddyn[[expr $group*10 + $i]]] -assign [set x_pos [expr $x_pos-$pinpitch]] $top
   }
}

set pinpitch 0.5
set x_pos [expr $left + 5]
for { set i 0 } { $i < 40 } { incr i } {
  edit_pin -pin_width 0.2 -fixed_pin 1 -layer M2 -side Top -pin [find_pin dstat_gl[$i]] -assign [set x_pos [expr $x_pos+$pinpitch]] $top
}

set y_pos_base [expr $top - 10]
set dac_height 38
set bitpitch 3
set invpitch 2.6
set num_doubledacs 10
set dacbits 10
set bit_lookup [list 2 3 4 1 0 5 6 7 8 9]
for { set i 0 } { $i < $num_doubledacs} { incr i } {  # P_NUM_DACS
  set y_pos [expr $y_pos_base - $i*$dac_height]
  for { set j 0 } { $j < $dacbits } { incr j } { # P_DACBITS
    set mapped_j [lindex $bit_lookup $j]
    set ij [expr $i*$dacbits + $mapped_j]
    set ij2 [expr $num_doubledacs*$dacbits + $i*$dacbits + $mapped_j]
    edit_pin -pin_width 0.2 -fixed_pin 1 -layer M2 -side Left -pin [find_pin dstat_dacs[$ij]] -assign $left $y_pos
    edit_pin -pin_width 0.2 -fixed_pin 1 -layer M4 -side Left -pin [find_pin dstat_dacs[$ij2]] -assign $left $y_pos
    edit_pin -pin_width 0.2 -fixed_pin 1 -layer M2 -side Left -pin [find_pin dstat_dacs_n[$ij]] -assign $left [expr $y_pos-$invpitch]
    edit_pin -pin_width 0.2 -fixed_pin 1 -layer M4 -side Left -pin [find_pin dstat_dacs_n[$ij2]] -assign $left [expr $y_pos-$invpitch]
    set y_pos [expr $y_pos-$bitpitch]
  }
}

edit_pin -pin [find_pin data_out] -pin_width 0.5 -fixed_pin 1 -layer M4 -side Right -assign $right -140.25 -snap usergrid
edit_pin -pin [find_pin cmd_in] -pin_width 0.5 -fixed_pin 1 -layer M5 -side Right -assign $right 17.4 -snap usergrid

proc find_port { orig } {
   set pin [get_db ports -if { .original_name == $orig }]
   set result [get_db $pin .name]
   echo $orig "=>" $result
   return $result
}

proc place_channel { id xoffset } {
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id +  0]]) [list 21.25 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id +  1]]) [list 20.05 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id +  2]]) [list 18.85 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id +  3]]) [list 17.65 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id +  4]]) [list 16.45 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id +  5]]) [list 15.25 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id +  6]]) [list 14.05 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id +  7]]) [list 12.85 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id +  8]]) [list 45.25 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id +  9]]) [list 44.05 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id + 10]]) [list 42.85 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id + 11]]) [list 41.65 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id + 12]]) [list 40.45 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id + 13]]) [list 39.25 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id + 14]]) [list 38.05 5 0.3]
   set channel_pins([format {RAM_wdata[%d]} [expr 16 * $id + 15]]) [list 36.85 5 0.3]

   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id +  0]]) [list 21.85 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id +  1]]) [list 20.65 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id +  2]]) [list 19.45 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id +  3]]) [list 18.25 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id +  4]]) [list 17.05 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id +  5]]) [list 15.85 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id +  6]]) [list 14.65 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id +  7]]) [list 13.45 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id +  8]]) [list 45.85 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id +  9]]) [list 44.65 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id + 10]]) [list 43.45 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id + 11]]) [list 42.25 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id + 12]]) [list 41.05 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id + 13]]) [list 39.85 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id + 14]]) [list 38.65 5 0.3]
   set channel_pins([format {RAM_rdata[%d]} [expr 16 * $id + 15]]) [list 37.45 5 0.3]

   set channel_pins([format {RAM_address[%d]} [expr 8 * $id +  7]]) [list 19.25 1 0.5]
   set channel_pins([format {RAM_address[%d]} [expr 8 * $id +  6]]) [list 20.25 1 0.5]
   set channel_pins([format {RAM_address[%d]} [expr 8 * $id +  5]]) [list 21.25 1 0.5]
   set channel_pins([format {RAM_address[%d]} [expr 8 * $id +  4]]) [list 11.25 1 0.5]
   set channel_pins([format {RAM_address[%d]} [expr 8 * $id +  3]]) [list 12.25 1 0.5]
   set channel_pins([format {RAM_address[%d]} [expr 8 * $id +  2]]) [list 13.25 1 0.5]
   set channel_pins([format {RAM_address[%d]} [expr 8 * $id +  1]]) [list 14.25 1 0.5]
   set channel_pins([format {RAM_address[%d]} [expr 8 * $id +  0]]) [list 15.25 1 0.5]

   set channel_pins([format {RAM_write[%d]} [expr 2 * $id +  0]]) [list 23.25 1 0.5]
   set channel_pins([format {RAM_write[%d]} [expr 2 * $id +  1]]) [list 47.25 1 0.5]

   set channel_pins([format {RAM_read[%d]} [expr 2 * $id +  0]]) [list 22.25 1 0.5]
   set channel_pins([format {RAM_read[%d]} [expr 2 * $id +  1]]) [list 46.25 1 0.5]

   set bottom [get_db current_design .bbox.ll.y]
   foreach pin [array names channel_pins] {
      set info $channel_pins($pin)
      edit_pin -snap usergrid -pin_width [lindex $info 2] -fixed_pin 1 -layer [lindex $info 1] -side bottom -pin [find_port [format $pin $id]] -assign [expr $xoffset + [lindex $info 0]] $bottom
      edit_pin -snap usergrid -pin_width [lindex $info 2] -fixed_pin 1 -layer [lindex $info 1] -side bottom -pin [find_port [format $pin $id]] -assign [expr $xoffset + [lindex $info 0]] $bottom
   }
}

set FE_pitch -76
set FE_offset [expr $right + $FE_pitch/2]

for { set i 0 } { $i < 16 } { incr i } {
   place_channel $i [expr $FE_offset + $i * $FE_pitch - 24]
}

edit_pin -pin_width 0.5 -fixed_pin 1 -layer M1 -side Bottom -pin [find_pin {RAM_bias[7]}]  -assign -600.75 $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M1 -side Bottom -pin [find_pin {RAM_bias[6]}]  -assign -601.75 $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M1 -side Bottom -pin [find_pin {RAM_bias[5]}]  -assign -602.75 $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M1 -side Bottom -pin [find_pin {RAM_bias[4]}]  -assign -603.75 $bottom -snap usergrid

edit_pin -pin_width 0.5 -fixed_pin 1 -layer M1 -side Bottom -pin [find_pin {RAM_bias[3]}]  -assign -595.25 $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M1 -side Bottom -pin [find_pin {RAM_bias[2]}]  -assign -596.25 $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M1 -side Bottom -pin [find_pin {RAM_bias[1]}]  -assign -597.25 $bottom -snap usergrid
edit_pin -pin_width 0.5 -fixed_pin 1 -layer M1 -side Bottom -pin [find_pin {RAM_bias[0]}]  -assign -598.25 $bottom -snap usergrid

edit_pin -pin_width 0.2 -fixed_pin 1 -layer M5 -side Left -pin [find_pin {por_n}] -assign $left [expr $bottom+3] -snap usergrid

set_db assign_pins_edit_in_batch false

write_floorplan floorplan/$design_name.fp

