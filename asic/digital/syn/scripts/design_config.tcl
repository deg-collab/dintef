# Flowkit v18.10-p005_1
################################################################################
# This file contains 'create_flow_step' content for steps which are required
# in an implementation flow, but whose contents are specific.  Review  all
# <PLACEHOLDER> content and replace with commands and options more appropriate
# for the design being implemented. Any new flowstep definitions should be done
# using the 'flow_config.tcl' file.
################################################################################

##############################################################################
# STEP read_hdl
##############################################################################
create_flow_step -name read_hdl -owner design {
  # Referenced signal not in sensitivity list.  This may cause simulation mismatches between the original and synthesized designs.
  ::set_db message:CDFG-360 .severity Error
  # Type conversion truncates constant value.
  ::set_db message:CDFG-370 .severity Error
  # Bitwidth mismatch in assignment.
  ::set_db message:CDFG-372 .severity Warning
  # Signal is not referenced within the process or block, but is in the sensitivity list.
  ::set_db message:CDFG-361 .severity Error
  # Input port connected to output instance port.
  ::set_db message:CDFG-562 .severity Error
  # Signal or variable has multiple drivers.
  ::set_db message:CDFG2G-622 .severity Error
  # A combinational loop has been found.
  ::set_db message:TIM-20 .severity Error
  # The value of the input port is not used within the design.
  ::set_db message:CDFG-500 .max_print 0
  # Unused instance port.
  ::set_db message:ELABUTL-132 .max_print 0
  # Implementation selected for component instance.
  ::set_db message:CDFG-560 .max_print 0

  #- read and elaborate design
  ::read_hdl -sv {
    ../hdl/Packages/clock_t.sv
    ../hdl/Packages/RAM_control_t.sv
    ../hdl/Packages/Types.sv
    ../hdl/JTAG/JTAGinterface.sv
    ../hdl/JTAG/JTAG_internal.sv

    ../hdl/ClockRoot.sv
    ../hdl/ControlBlock.sv
    ../hdl/CPLogic.sv
    ../hdl/CycleCnt.sv
    ../hdl/DDYN_OutReg.sv
    ../hdl/PartSeqControl.sv
    ../hdl/PixelLogic.sv
    ../hdl/PumpCounter.sv
    ../hdl/ReadoutSerializer.sv
    ../hdl/SeqHoldCnt.sv
    ../hdl/SeqRepCnt.sv
    ../hdl/SeqSlowShiftReg.sv
    ../hdl/SequencerTrack.sv
    ../hdl/Sequencer_SuS.sv
    ../hdl/gen_clk10.sv
    ../hdl/input_framer.sv
    ../hdl/reset_synchronizer.sv
    ../hdl/saturating_counter.sv
    ../hdl/serializer.sv

    ../hdl/JTAG/jtag.sv
    ../hdl/JTAG/jtag_bypass_reg.sv
    ../hdl/JTAG/jtag_ic_reset_reg.sv
    ../hdl/JTAG/jtag_id.sv
    ../hdl/JTAG/jtag_ir.sv
    ../hdl/JTAG/jtag_tap_ctrl.sv
    ../hdl/JTAG/jtag_user_reg.sv
    ../hdl/JTAG/jtag_user_reg_por.sv

    ../hdl/PixelControl/ColumnShiftreg.sv
    ../hdl/PixelControl/CommandReceiver.sv
    ../hdl/PixelControl/CountConverter.sv
    ../hdl/PixelControl/PixelControl.sv
    ../hdl/PixelControl/SaturatedAdder.sv
    ../hdl/PixelControl/SubframeState.sv
  }
  elaborate ControlBlock
  #set_db "design:PETADigital" .lp_clock_gating_control_point none

  #read_tcf -tcf_instance DUT ../top/verification/xmsim.tcf
}

##############################################################################
# STEP set_dont_use
##############################################################################
create_flow_step -name set_dont_use -owner design {
  #- disable cell usage
  <%? {dont_use_cells} return "foreach cell [list [get_flow_config dont_use_cells]] { set_db \[get_db base_cells \$cell\] .dont_use true  }" %>
}

##############################################################################
# STEP init_floorplan
##############################################################################
create_flow_step -name init_floorplan -owner design {
  #- initialize floorplan object using DEF and/or floorplan files
  <%? {init_floorplan_file} return "read_floorplan [get_flow_config init_floorplan_file]" %>
  <%? {init_def_files} return "foreach def_file [list [get_flow_config init_def_files]] { read_def \$def_file }" %>
  
  #- update power_intent after floorplan additions
  <%? {commit_power_intent_options} return "commit_power_intent [get_flow_config commit_power_intent_options]" else return "commit_power_intent" %>
  
  #- finish floorplan with auto-blockage insertion
  finish_floorplan  -fill_place_blockage soft 20.0
} -check {
  foreach file [get_flow_config -quiet init_floorplan_file] {
    check "[file exists $file] && [file readable $file]" "The floorplan file: $file was not found or is not readable."
  }
  foreach file [get_flow_config -quiet init_def_files] {
    check "[file exists $file] && [file readable $file]" "The def file: $file was not found or is not readable."
  }
}

##############################################################################
# STEP add_clock_route_types
##############################################################################
create_flow_step -name add_clock_route_types -owner design {
  #- define route_types and/or route_rules
  create_route_type -route_rule 2W2S -name double_double
  
  set_db cts_route_type_top  double_double
  set_db cts_route_type_trunk double_double

#  create_route_type -route_rule 2W2S -name double_double
#  create_route_type -route_rule 2W1S -name double_single
  
#  set_db cts_route_type_top  double_double
#  set_db cts_route_type_trunk double_double
#  set_db cts_route_type_leaf  double_single
}

source scripts/eco.tcl
