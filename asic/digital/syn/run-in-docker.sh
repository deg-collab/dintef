#!/bin/sh
mkdir -p 1P9M_6X1Z1U_RDL
cp /shares/designs/TSMC/65/1P9M_6X1Z1U_RDL/1P9M_6X1Z1U_RDL.layermap 1P9M_6X1Z1U_RDL
docker run --rm --network=host -it -v "${PWD}/..:/tmp/design" -v /opt/eda:/opt/eda -v "${PWD}/1P9M_6X1Z1U_RDL:/shares/designs/TSMC/65/1P9M_6X1Z1U_RDL" -w /tmp/design/syn -u $(id -u) --env USER spectre ./synthesize ${*}
