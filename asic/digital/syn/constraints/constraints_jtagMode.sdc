set sdc_version 1.7

set_units -capacitance 1000.0fF
set_units -time 1000.0ps

set_max_transition 0.5
set_max_capacitance 0.5 [current_design]
set_max_capacitance 0.1 [all_inputs]
set_max_fanout 1 [all_inputs]

create_clock -name "tck" -add -period 10.0 [get_ports tck]
set_clock_transition 0.100 [get_clocks tck]

set_clock_uncertainty -setup 0.05 tck 
set_clock_uncertainty -hold  0.05 tck

set_input_delay -clock tck 3.0 [get_ports tdi]
set_input_delay -clock tck 3.0 [get_ports tms]

set_input_transition 0.2 [all_inputs]

set_max_transition 0.4 [all_outputs]

set_output_delay -clock tck -max 3.0 [get_ports tdo]

set_false_path -from [get_ports res_n]

set_load -wire_load 0.2 [all_outputs]
set_load -wire_load 0.1 [get_ports dstat*]

set_max_transition 0.5 [all_outputs]
set_max_transition 0.2 [get_ports ddyn[*]]
set_max_transition 2 [get_ports dstat*]
