set sdc_version 1.7

set_units -capacitance 1000.0fF
set_units -time 1000.0ps

set_max_fanout 1 [all_inputs]

create_clock -name "clk" -add -period 2.0 [get_ports clk]

# edges includes pos and negedges -> 1 7 11 divides by 5, duty cycle is 3 clk high, 2 clk low
create_generated_clock -name "clk5" -edges {1 7 11} -source [get_ports clk] [get_pins clkroot_I/div_I/clk_by_5_reg/Q]
create_generated_clock -name "clk10" -edges {1 11 21} -source [get_ports clk] [get_pins clkroot_I/div_I/clk_by_10_reg/Q]

set_clock_transition 0.100 [get_clocks clk]

set_clock_uncertainty -setup 0.05 clk
set_clock_uncertainty -hold  0.05 clk
set_clock_uncertainty -setup 0.05 clk5
set_clock_uncertainty -hold  0.05 clk5

# TODO check
set_input_delay -clock clk 0.5 [get_ports seq_run]

set_output_delay -clock clk -max 0.1 [get_ports ddyn]

set_input_delay -clock clk5 -max 0.2 [get_ports cp_en_cnt_*]

set_false_path -from [get_ports por_n]
set_false_path -from [get_ports res_n]

set_load -wire_load 0.2 [all_outputs]
set_load -wire_load 0.1 [get_ports dstat*]

set_max_transition 0.5 [all_outputs]
set_max_transition 0.2 [get_ports ddyn[*]]
set_max_transition 2 [get_ports dstat*]

set_driving_cell -lib_cell INVD0 [get_ports RAM_rdata[*]]

set_dont_touch [get_cells {gengroups[*].*}]

set_multicycle_path -from [get_cells PixelControl[0].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[0].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[0].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[0].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[1].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[1].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[1].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[1].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[2].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[2].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[2].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[2].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[3].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[3].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[3].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[3].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[4].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[4].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[4].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[4].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[5].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[5].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[5].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[5].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[6].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[6].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[6].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[6].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[7].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[7].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[7].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[7].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[8].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[8].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[8].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[8].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[9].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[9].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[9].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[9].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[10].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[10].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[10].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[10].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[11].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[11].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[11].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[11].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[12].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[12].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[12].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[12].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[13].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[13].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[13].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[13].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[14].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[14].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[14].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[14].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[15].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[15].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[15].Ctrl_I/last_fine_cnt_reg[*]] -to [get_cells PixelControl[15].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6

set_multicycle_path -from [get_cells PixelControl[0].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[0].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[0].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[0].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[1].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[1].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[1].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[1].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[2].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[2].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[2].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[2].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[3].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[3].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[3].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[3].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[4].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[4].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[4].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[4].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[5].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[5].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[5].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[5].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[6].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[6].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[6].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[6].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[7].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[7].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[7].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[7].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[8].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[8].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[8].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[8].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[9].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[9].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[9].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[9].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[10].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[10].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[10].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[10].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[11].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[11].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[11].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[11].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[12].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[12].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[12].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[12].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[13].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[13].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[13].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[13].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[14].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[14].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[14].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[14].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6
set_multicycle_path -from [get_cells PixelControl[15].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[15].Ctrl_I/combined_cnt_reg_reg[*]] -start -setup 7
set_multicycle_path -from [get_cells PixelControl[15].Ctrl_I/last_coarse_cnt_reg[*]] -to [get_cells PixelControl[15].Ctrl_I/combined_cnt_reg_reg[*]] -start -hold 6