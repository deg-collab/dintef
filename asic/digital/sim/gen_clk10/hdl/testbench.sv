`default_nettype none
`timescale 1ns/1ps
module testbench();

reg clk = 1'b0;
always #1 clk <= !clk;
reg reset_n = 1'b0;
reg do_clk_11 = 1'b0;
logic clk10;

gen_clk10 DUT_I(
      .clk_in( clk ),
      .reset_n( reset_n ),
      .do_clk_11( do_clk_11 ),

      .clk_by_5(),
      .clk_by_10( clk10 ),
      .clk_by_10_start()
  );

initial begin
  #3ns
  reset_n <= 1'b1;
  repeat( 10 )
  begin
    #40ns
    @(posedge clk10)
    do_clk_11 <= 1'b1;
    @(posedge clk10)
    do_clk_11 <= 1'b0;
  end
  $stop();
end

endmodule : testbench
