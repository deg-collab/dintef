`default_nettype none
`timescale 1ns/1ps
import Types::*;

module testbench();

clock_t clock();
decoded_8b10b_t word;
pixel_control_t control;

always #100ns clock.clk_50 <= !clock.clk_50;

CommandReceiver DUT_I(
    .CLOCK( clock ),
    .WORD( word ),

    .PIXEL_CONTROL( control )
  );

SubframeState state_I(
    .CLOCK( clock ),
    .SUBFRAME_START( control.subframe_start ),
    .SUBFRAME_STATE()
  );

initial begin
  clock.clk_50 <= 1'b0;
  clock.reset_n_50 <= 1'b0;

  word <= cK28_5_decoded;
  repeat( 2 ) @(posedge clock.clk_50);
  clock.reset_n_50 <= 1'b1;
  repeat( 2 ) @(posedge clock.clk_50);

  // cycle type 1: no start readout command
  @(posedge clock.clk_50);
  word <= cK28_1_decoded;
  @(posedge clock.clk_50);
  word <= '{ is_k : 0, is_valid : 1, value : 8'h12 };
  @(posedge clock.clk_50);
  word <= '{ is_k : 0, is_valid : 1, value : 8'h34 };
  @(posedge clock.clk_50);
  word <= cK28_5_decoded;

  // cycle type 2: with start readout
  @(posedge clock.clk_50);
  word <= cK28_1_decoded;
  @(posedge clock.clk_50);
  word <= '{ is_k : 0, is_valid : 1, value : 8'h43 };
  @(posedge clock.clk_50);
  word <= '{ is_k : 0, is_valid : 1, value : 8'h21 };
  @(posedge clock.clk_50);
  word <= '{ is_k : 0, is_valid : 1, value : 8'h03 };

  // cycle type 1: no start readout command
  @(posedge clock.clk_50);
  word <= cK28_1_decoded;
  @(posedge clock.clk_50);
  word <= '{ is_k : 0, is_valid : 1, value : 8'h12 };
  @(posedge clock.clk_50);
  word <= '{ is_k : 0, is_valid : 1, value : 8'h34 };
  @(posedge clock.clk_50);
  word <= cK28_5_decoded;

  repeat( 3 ) @(posedge clock.clk_50);
  $stop();
end

endmodule : testbench
`default_nettype wire

