`default_nettype none
`timescale 1ns/1ps
module testbench();

bit [ 15:0 ] a, b, z;
bit en_saturation, half_width;

SaturatedAdder DUT_I(.*);

initial begin
  a = 16'd10;
  b = 16'd20;
  en_saturation = 1'b0;
  half_width = 1'b0;
  #1ns
  assert( z == 16'd30 );
  en_saturation = 1'b0;
  half_width = 1'b1;
  #1ns
  assert( z == 16'd30 );
  en_saturation = 1'b1;
  half_width = 1'b0;
  #1ns
  assert( z == 16'd30 );
  en_saturation = 1'b1;
  half_width = 1'b1;
  #1ns
  assert( z == 16'd30 );

  a = 16'd100;
  b = 16'd200;
  en_saturation = 1'b0;
  half_width = 1'b0;
  #1ns
  assert( z == 16'd300 );
  en_saturation = 1'b0;
  half_width = 1'b1;
  #1ns
  assert( z == 16'd300 );
  en_saturation = 1'b1;
  half_width = 1'b0;
  #1ns
  assert( z == 16'd300 );
  en_saturation = 1'b1;
  half_width = 1'b1;
  #1ns
  assert( z == 16'd255 );

  a = 16'hFFFF;
  b = 16'd1;
  en_saturation = 1'b0;
  half_width = 1'b0;
  #1ns
  assert( z == 16'd0 );
  en_saturation = 1'b0;
  half_width = 1'b1;
  #1ns
  assert( z == 16'd0 );
  en_saturation = 1'b1;
  half_width = 1'b0;
  #1ns
  assert( z == 16'hFFFF );
  en_saturation = 1'b1;
  half_width = 1'b1;
  #1ns
  // actually, this is 0 due to the details of the algorithm.
  // however, this is not a problem since in half-width mode,
  // both addends are only 8 bits wide
  assert( z == 16'd0 );
  $stop();
end

endmodule : testbench
`default_nettype wire
