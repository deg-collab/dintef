#!/bin/sh
. /opt/eda/environment/xcelium1909_path.bash

rm -rf lib/ASIC lib/CW lib/tsmc
mkdir -p lib/ASIC lib/CW lib/tsmc

xmverilog +compile +xmoverride_timescale +xmtimescale+1ns/1ps -nocopyright -messages -work ASIC ../hdl/Packages/*.sv ../hdl/*.sv ../hdl/JTAG/*.sv ../hdl/PixelControl/*.sv
xmverilog +compile +xmoverride_timescale +xmtimescale+1ns/1ps -nocopyright -messages -work CW /opt/eda/GENUS191/share/synth/lib/chipware/sim/verilog/CW/*.v
xmverilog +compile +xmoverride_timescale +xmtimescale+1ns/1ps -nocopyright -messages -work tsmc /opt/eda/TSMC/65/__UNPACKED__/stclib/TSMCHOME/digital/Front_End/verilog/tcbn65lp_200a/tcbn65lp.v
