#!/bin/sh
. /opt/eda/environment/ic617_path.bash
si -batch -command nl

(
cat netlistHeader
awk '/Cell name: analog_4_sim/{exit}{print}' netlist
cat netlistFooter
) > ESRF_Top.scs
