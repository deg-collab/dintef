`ifndef DEV_IF
`define DEV_IF \
  input wire if_clk, \
  input wire if_res_n, \
  /*IO Manager Port interface*/ \
  /*write port*/ \
  input  wire iom_write, \
  input  wire [7:0] iom_data_to_dev, \
  output wire iom_busy, \
  output wire iom_almost_busy, \
  /*read port*/ \
  input  wire iom_read, \
  output wire [7:0] iom_data_from_dev, \
  output wire iom_data_valid, \
  output wire iom_next_data_valid 
`endif

`ifndef DEV_IN_FIFO_CNTRL_CONN
`define DEV_IN_FIFO_CNTRL_CONN \
  .dev_in_fifo_empty(dev_in_fifo_empty), \
  .dev_in_fifo_almost_empty(dev_in_fifo_almost_empty), \
  .dev_in_fifo_rd_en(dev_in_fifo_rd_en)
`endif

`ifndef DEV_IN_FIFO_CNTRL_IF
`define DEV_IN_FIFO_CNTRL_IF \
  input dev_in_fifo_empty, \
  input dev_in_fifo_almost_empty, \
  output dev_in_fifo_rd_en
`endif

`ifndef DEV_OUT_FIFO_CNTRL_CONN
`define DEV_OUT_FIFO_CNTRL_CONN \
  .dev_out_fifo_full(dev_out_fifo_full), \
  .dev_out_fifo_almost_full(dev_out_fifo_almost_full), \
  .dev_out_fifo_wr_en(dev_out_fifo_wr_en)
`endif

`ifndef DEV_OUT_FIFO_CNTRL_IF
`define DEV_OUT_FIFO_CNTRL_IF \
  input dev_out_fifo_full, \
  input dev_out_fifo_almost_full, \
  output dev_out_fifo_wr_en
`endif

`ifndef DEV_IN_FIFO
`define DEV_IN_FIFO \
  wire dev_in_fifo_rd_en, dev_in_fifo_empty, dev_in_fifo_almost_empty; \
  wire [7:0] dev_in_fifo_dout; \
  AsyncDeviceFifo AsyncDeviceInFifo_I( \
  .wr_clk(if_clk), \
  .rd_clk(clk), \
  .rst(~if_res_n), \
  .din(iom_data_to_dev), /* Bus [7 : 0] */\
  .wr_en(iom_write), \
  .rd_en(dev_in_fifo_rd_en), \
  .dout(dev_in_fifo_dout), /* Bus [7 : 0] */\
  .full(iom_busy), \
  .almost_full(iom_almost_busy), \
  .empty(dev_in_fifo_empty), \
  .almost_empty(dev_in_fifo_almost_empty));
`endif

`ifndef DEV_OUT_FIFO
`define DEV_OUT_FIFO \
  wire [7:0] dev_out_fifo_din; \
  wire dev_out_fifo_wr_en, dev_out_fifo_full, dev_out_fifo_almost_full, dev_out_fifo_empty, dev_out_fifo_almost_empty; \
  assign iom_data_valid = !dev_out_fifo_empty; \
  assign iom_next_data_valid = !dev_out_fifo_almost_empty; \
  AsyncDeviceFifo AsyncDeviceOutFifo_I( \
  .wr_clk(clk), \
  .rd_clk(if_clk), \
  .rst(~if_res_n), \
  .din(dev_out_fifo_din), /* Bus [7 : 0]*/ \
  .wr_en(dev_out_fifo_wr_en), \
  .rd_en(iom_read), \
  .dout(iom_data_from_dev), /* Bus [7 : 0]*/ \
  .full(dev_out_fifo_full), \
  .almost_full(dev_out_fifo_almost_full), \
  .empty(dev_out_fifo_empty), \
  .almost_empty(dev_out_fifo_almost_empty));
`endif

`ifndef DEV_IN_FIFO_16
`define DEV_IN_FIFO_16 \
  wire dev_in_fifo_rd_en, dev_in_fifo_empty, dev_in_fifo_almost_empty; \
  wire [7:0] dev_in_fifo_dout; \
  AsyncDeviceFifo_16 AsyncDeviceInFifo_I( \
  .wr_clk(if_clk), \
  .rd_clk(clk), \
  .rst(~if_res_n), \
  .din(iom_data_to_dev), /* Bus [7 : 0] */\
  .wr_en(iom_write), \
  .rd_en(dev_in_fifo_rd_en), \
  .dout(dev_in_fifo_dout), /* Bus [7 : 0] */\
  .full(iom_busy), \
  .almost_full(iom_almost_busy), \
  .empty(dev_in_fifo_empty), \
  .almost_empty(dev_in_fifo_almost_empty));
`endif

`ifndef DEV_OUT_FIFO_16
`define DEV_OUT_FIFO_16 \
  wire [7:0] dev_out_fifo_din; \
  wire dev_out_fifo_wr_en, dev_out_fifo_full, dev_out_fifo_almost_full, dev_out_fifo_empty, dev_out_fifo_almost_empty; \
  assign iom_data_valid = !dev_out_fifo_empty; \
  assign iom_next_data_valid = !dev_out_fifo_almost_empty; \
  AsyncDeviceFifo_16 AsyncDeviceOutFifo_I( \
  .wr_clk(clk), \
  .rd_clk(if_clk), \
  .rst(~if_res_n), \
  .din(dev_out_fifo_din), /* Bus [7 : 0]*/ \
  .wr_en(dev_out_fifo_wr_en), \
  .rd_en(iom_read), \
  .dout(iom_data_from_dev), /* Bus [7 : 0]*/ \
  .full(dev_out_fifo_full), \
  .almost_full(dev_out_fifo_almost_full), \
  .empty(dev_out_fifo_empty), \
  .almost_empty(dev_out_fifo_almost_empty));
`endif

`ifndef DEV_IN_FIFO_65536
`define DEV_IN_FIFO_65536 \
  wire dev_in_fifo_rd_en, dev_in_fifo_empty, dev_in_fifo_almost_empty; \
  wire [7:0] dev_in_fifo_dout; \
  AsyncDeviceFifo_65536 AsyncDeviceInFifo_I( \
  .wr_clk(if_clk), \
  .rd_clk(clk), \
  .rst(~if_res_n), \
  .din(iom_data_to_dev), /* Bus [7 : 0] */\
  .wr_en(iom_write), \
  .rd_en(dev_in_fifo_rd_en), \
  .dout(dev_in_fifo_dout), /* Bus [7 : 0] */\
  .full(iom_busy), \
  .almost_full(iom_almost_busy), \
  .empty(dev_in_fifo_empty), \
  .almost_empty(dev_in_fifo_almost_empty));
`endif

`ifndef DEV_OUT_FIFO_65536
`define DEV_OUT_FIFO_65536 \
  wire [7:0] dev_out_fifo_din; \
  wire dev_out_fifo_wr_en, dev_out_fifo_full, dev_out_fifo_almost_full, dev_out_fifo_empty, dev_out_fifo_almost_empty; \
  assign iom_data_valid = !dev_out_fifo_empty; \
  assign iom_next_data_valid = !dev_out_fifo_almost_empty; \
  AsyncDeviceFifo_65536 AsyncDeviceOutFifo_I( \
  .wr_clk(clk), \
  .rd_clk(if_clk), \
  .rst(~if_res_n), \
  .din(dev_out_fifo_din), /* Bus [7 : 0]*/ \
  .wr_en(dev_out_fifo_wr_en), \
  .rd_en(iom_read), \
  .dout(iom_data_from_dev), /* Bus [7 : 0]*/ \
  .full(dev_out_fifo_full), \
  .almost_full(dev_out_fifo_almost_full), \
  .empty(dev_out_fifo_empty), \
  .almost_empty(dev_out_fifo_almost_empty));
`endif

`ifndef DEV_OUT_FIFO_16TO8
`define DEV_OUT_FIFO_16TO8 \
  wire [15:0] dev_out_fifo_din; \
  wire dev_out_fifo_wr_en, dev_out_fifo_full, dev_out_fifo_almost_full, dev_out_fifo_empty, dev_out_fifo_almost_empty; \
  assign iom_data_valid = !dev_out_fifo_empty; \
  assign iom_next_data_valid = !dev_out_fifo_almost_empty; \
  DeviceOutFifo16to8 DeviceOutFifo16to8_I( \
  .wr_clk(clk), \
  .rd_clk(if_clk), \
  .rst(~if_res_n), \
  .din(dev_out_fifo_din), /* Bus [15 : 0] */ \
  .wr_en(dev_out_fifo_wr_en), \
  .rd_en(iom_read), \
  .dout(iom_data_from_dev), /*Bus [7 : 0] */ \
  .full(dev_out_fifo_full), \
  .almost_full(dev_out_fifo_almost_full), \
  .empty(dev_out_fifo_empty), \
  .almost_empty(dev_out_fifo_almost_empty));
`endif

`ifndef DEV_OUT_FIFO_32TO8
`define DEV_OUT_FIFO_32TO8 \
  wire [31:0] dev_out_fifo_din; \
  wire dev_out_fifo_wr_en, dev_out_fifo_full, dev_out_fifo_almost_full, dev_out_fifo_empty, dev_out_fifo_almost_empty; \
  assign iom_data_valid = !dev_out_fifo_empty; \
  assign iom_next_data_valid = !dev_out_fifo_almost_empty; \
  DeviceOutFifo32to8 DeviceOutFifo32to8_I( \
  .wr_clk(clk), \
  .rd_clk(if_clk), \
  .rst(~if_res_n), \
  .din(dev_out_fifo_din), /* Bus [31 : 0] */ \
  .wr_en(dev_out_fifo_wr_en), \
  .rd_en(iom_read), \
  .dout(iom_data_from_dev), /*Bus [7 : 0] */ \
  .full(dev_out_fifo_full), \
  .almost_full(dev_out_fifo_almost_full), \
  .empty(dev_out_fifo_empty), \
  .almost_empty(dev_out_fifo_almost_empty));
`endif
