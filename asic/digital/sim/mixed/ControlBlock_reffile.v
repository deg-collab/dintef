module ControlBlock #(
  parameter P_NUMMUXES = 6,
  parameter P_MUXBITS = 5,
  parameter P_NUM_PX_LOGIC = 4,
  parameter P_COARSE_BITS = 10,
  parameter P_FINE_BITS = 6,
  parameter P_NUM_PX_PADS = 28,
  parameter P_NUM_GLBITS = 40,
  parameter P_NUM_DACS = 20,
  parameter P_DACBITS = 10,
  parameter P_CYCLELENGTH_BITS = 13,
  parameter P_SEQ_TRACKS = 9,
  parameter P_MON_MUX_SELBITS = 4
) (
   // input  wire por_n, // use res_n
   //jtag io signals
   input  wire tms,
   input  wire tck,
   //input  wire trst_n, // use res_n
   input  wire tdi,
   output wire tdo,
   // tri-state output enable for TDO
   output wire tdo_enable,

   output wire [P_NUMMUXES*P_MUXBITS-1 : 0] dstat_muxes,
   output wire [P_NUMMUXES*P_MUXBITS-1 : 0] dstat_muxes_n,
   output wire [P_NUM_DACS*P_DACBITS-1 : 0] dstat_dacs,
   output wire [P_NUM_DACS*P_DACBITS-1 : 0] dstat_dacs_n,
   output wire [P_NUM_PX_PADS-1 : 0] dstat_px_shunt,
   output wire [P_NUM_GLBITS-1 : 0] dstat_gl,

   input wire  res_n,
   input wire  clk,
   input wire  seq_run,
   input wire  ext_ddyn,
   output wire [P_SEQ_TRACKS-1 : 0] ddyn,

   input wire [P_NUM_PX_LOGIC-1 : 0] cp_en_cnt_coarse,
   input wire [P_NUM_PX_LOGIC-1 : 0] cp_en_cnt_fine,

   output wire [1:0] ser_out,
   output wire mon

    ,
    input VDD,
    input VSS

);

endmodule
