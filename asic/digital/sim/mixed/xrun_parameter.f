TB_SUS65T2.vams

#simulation files (digital and analog)
scs/acf.scs
scs/amscf.scs

/shares/designs/TSMC/65/1P9M_6X1Z1U_RDL/SUS65T1/PAD_LVDS_IN/functional/verilog.v
/shares/designs/TSMC/65/1P9M_6X1Z1U_RDL/SUS65T1/PAD_CMOS_IN_PU/functional/verilog.v
/shares/designs/TSMC/65/1P9M_6X1Z1U_RDL/SUS65T1/PAD_CMOS_IN_PD/functional/verilog.v
/shares/designs/TSMC/65/1P9M_6X1Z1U_RDL/SUS65T1/PAD_CMOS_OUT/functional/verilog.v

../../hdl/Packages/*.sv
../../hdl/*.sv
../../hdl/PixelControl/*.sv
../../hdl/JTAG/*.sv

#source files of FPGA stuff
../../../../firmware/hdl/src/dev_if.v
../../../../firmware/hdl/src/jtag_engine.v
../../../../firmware/hdl/src/jtag_engine_ebone_reg_if.v
../../../../firmware/hdl/src/DynamicAsicControl.sv
../../../../firmware/hdl/src/jtag_engine_fsm.v
#../../../firmware/ip/AsyncDeviceFifo_65536/AsyncDeviceFifo_65536_stub.v
../../../../firmware/hdl/ip/AsyncDeviceFifo_65536/sim/AsyncDeviceFifo_65536.v
../../../../firmware/hdl/ip/AsyncDeviceFifo_65536/hdl/fifo_generator_v13_2_rfs.v
../../../../firmware/hdl/ip/AsyncDeviceFifo_65536/simulation/fifo_generator_vlog_beh.v



#simulation files
../../../../firmware/hdl/sim/CmdFileReader.v

#/opt/ISEfull/latest/ISE_DS/ISE/verilog/src/glbl.v

/opt/eda/GENUS191/share/synth/lib/chipware/sim/verilog/CW/CW_8b10b_dec.v
/opt/eda/GENUS191/share/synth/lib/chipware/sim/verilog/CW/CW_8b10b_enc.v

/opt/eda/TSMC/65/__UNPACKED__/stclib/TSMCHOME/digital/Front_End/verilog/tcbn65lp_200a/tcbn65lp.v

#+incdir+/opt/ISEfull/latest/ISE_DS/ISE/verilog/src +libext+.v
#+incdir+../src/ +libext+.v
#-y /opt/ISEfull/latest/ISE_DS/ISE/verilog/src/unisims
#-y /opt/ISEfull/latest/ISE_DS/ISE/verilog/src/XilinxCoreLib
#-y /opt/ISEfull/latest/ISE_DS/ISE/verilog/src/simprims
#-f /opt/ISEfull/latest/ISE_DS/ISE/secureip/ncsim/iserdese2_ncsim/iserdese2_cell.list.f

#-define ANALOG_SIMULATION
-define SIMULATION
-define FULLCHIP_MIXED_MODE
-amsvlog_ext .vams,.va
-top TB_SUS65T2 
#-top cds_globals
#-top cds_globals_analog
-spectre_args ++aps
-access +r
-timescale 1ns/1ps
-gui
-log_xmsim xmsim_log.txt
-log_xmvlog xmvlog_log.txt
-log_xmelab xmelab_log.txt
-iereport
