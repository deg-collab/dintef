`default_nettype none
`timescale 1ns/1ps
module testbench();

reg clk = 1'b0;
always #100ns clk <= !clk;

reg [ 1:0 ] ro_enable;
reg ro_load = 1'b0;
reg ro_shift = 1'b1;
reg [ 15:0 ] ram_data;

ColumnShiftreg sr_I (
     .CLK( clk ),
     .RO_ENABLE( ro_enable ),
     .RO_LOAD( ro_load ),
     .RO_SHIFT( ro_shift ),
     .RO_SHIFT_IN( 8'hFF ),
     .RO_SHIFT_OUT(),
     .RAM_DATA( ram_data )
  );

initial begin
  #400
  @(posedge clk);
  ro_enable <= 2'b00;
  ro_load <= 1'b1;
  ram_data = 16'h0201;
  @(posedge clk);
  ro_load <= 1'b0;
  repeat (3) @(posedge clk);

  @(posedge clk);
  ro_enable <= 2'b01;
  ro_load <= 1'b1;
  ram_data = 16'h1211;
  @(posedge clk);
  ro_load <= 1'b0;
  repeat (3) @(posedge clk);

  @(posedge clk);
  ro_enable <= 2'b10;
  ro_load <= 1'b1;
  ram_data = 16'h2221;
  @(posedge clk);
  ro_load <= 1'b0;
  repeat (3) @(posedge clk);

  @(posedge clk);
  ro_enable <= 2'b11;
  ro_load <= 1'b1;
  ram_data = 16'h3231;
  @(posedge clk);
  ro_load <= 1'b0;
  repeat (3) @(posedge clk);
  $stop();
end

endmodule : testbench
`default_nettype wire

