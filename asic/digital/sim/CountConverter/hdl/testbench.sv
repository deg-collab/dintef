`default_nettype none
`timescale 1ns/1ps
module testbench();

import Types::*;
conversion_settings_t s;

logic [ 5:0 ] coarse;
logic [ 5:0 ] fine;
logic [ 11:0 ] combined;

CountConverter DUT_I (
    .CONV( s ),
    .COARSE_CNT( coarse ),
    .FINE_CNT( fine ),
    .COMBINED_CNT( combined )
  );

initial begin
  s.coarse_gain <= 3'd3; // 2^3 = 8
  s.LSB_correction <= 2'd0;
  s.LSB_discard <= 2'd0;
  s.enable_limit <= 1'b0;
  fine <= 6'd7;
  coarse <= 6'd5;
  #1ns
  assert( combined == 8*5 + 7 );

  fine <= 6'd8;
  coarse <= 6'd5;
  #1ns
  assert( combined == 8*5 + 8 );

  fine <= 6'd63;
  coarse <= 6'd63;
  #1ns
  assert( combined == 8*63 + 63 );

  fine <= 6'd63;
  coarse <= 6'd63;
  s.coarse_gain <= 3'd5; // 2^5 = 32
  #1ns
  assert( combined == 32*63 + 63 );

  fine <= 6'd63;
  coarse <= 6'd63;
  s.coarse_gain <= 3'd5; // 2^5 = 32
  s.LSB_discard <= 2'd1;
  #1ns
  assert( combined == 1039 );

  s.LSB_correction <= 2'd1;
  #1ns
  assert( combined == 1040 );

  s.limit <= 2'd3;
  s.enable_limit <= 1'b1;
  #1ns
  assert( combined == 255 );

  s.limit <= 2'd2;
  s.enable_limit <= 1'b1;
  #1ns
  assert( combined == 127 );

  coarse <= 6'd2;
  fine <= 6'd3;
  #1ns
  assert( combined == (32*2 + 3 + 1)/2 );

  fine <= 6'd63;
  coarse <= 6'd63;
  s.coarse_gain <= 3'd6; // 2^6 = 64
  s.LSB_correction <= 2'd0;
  s.LSB_discard <= 2'd0;
  s.enable_limit <= 1'b0;
  #1ns
  assert( combined == 12'hFFF );

  $stop();
end

endmodule : testbench
`default_nettype wire
