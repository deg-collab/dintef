`default_nettype none
`timescale 1ns/1ps
// NOTE: this module is for generic lengths of the counter.
// Since it is hardcoded to use the top two bits, it will only produce a
// maximum-length sequence for a few length, e.g. 2, 3, 4, 6, 7, 15, 22.
module lfsr #(
    parameter P_LENGTH = 7,
    parameter P_RESET_STATE = {P_LENGTH{1'b1}}
  ) (
    input wire CLK,
    input wire RESET,
    input wire CLK_ENABLE,
  
    output reg [ P_LENGTH-1:0 ] COUNT = P_RESET_STATE
  );

always @(posedge CLK, posedge RESET)
begin
  if (RESET)
  begin
    COUNT <= P_RESET_STATE;
  end
  else
  begin
    if (CLK_ENABLE)
    begin
      COUNT[ P_LENGTH-1:0 ] <= { COUNT[ P_LENGTH-2:0 ],
        COUNT[ P_LENGTH-1 ] ^ COUNT[ P_LENGTH - 2] };
    end
  end
end

`ifdef SIMULATION
initial if (P_RESET_STATE == {P_LENGTH{1'b0}})
begin
  $display("P_RESET_STATE must be != 0.");
  $stop();
end
`endif

endmodule : lfsr
`default_nettype wire