`timescale 1ns/1ps
`default_nettype none
module testbench();

bit CLK;
always #1ns CLK <= !CLK;
bit ENABLE;
bit FROM_COMP;

CPLogic DUT_I(
    .*,
    .PUMP()
  );

initial begin
  ENABLE <= 1'b0;
  #4.6ns
  FROM_COMP <= 1'b1;
  #5ns
  FROM_COMP <= 1'b0;
  #10ns
  ENABLE <= 1'b1;
  #4.6ns
  FROM_COMP <= 1'b1;
  #5ns
  FROM_COMP <= 1'b0;
  #10ns
  $finish();
end

endmodule : testbench
`default_nettype wire
