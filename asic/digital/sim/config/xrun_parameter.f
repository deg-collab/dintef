# Packages
../../hdl/Packages/*.sv

#simulation files (digital and analog)
hdl/testbench.sv

# src files
../../hdl/*.sv
../../hdl/JTAG/*.sv
../../hdl/PixelControl/*.sv

#source files of FPGA stuff
../../../../firmware/hdl/src/dev_if.v
../../../../firmware/hdl/src/jtag_engine.v
../../../../firmware/hdl/src/jtag_engine_ebone_reg_if.v
../../../../firmware/hdl/src/DynamicAsicControl.sv
../../../../firmware/hdl/src/jtag_engine_fsm.v
#../../../firmware/ip/AsyncDeviceFifo_65536/AsyncDeviceFifo_65536_stub.v
../../../../firmware/hdl/ip/AsyncDeviceFifo_65536/sim/AsyncDeviceFifo_65536.v
../../../../firmware/hdl/ip/AsyncDeviceFifo_65536/hdl/fifo_generator_v13_2_rfs.v
../../../../firmware/hdl/ip/AsyncDeviceFifo_65536/simulation/fifo_generator_vlog_beh.v
#simulation files
../../../../firmware/hdl/sim/CmdFileReader.v

+xmtimescale+1ns/1ps

-top testbench
#-solver aps
#-aps_args "-raw $(OUTDIR)/psf"
#-solver spectre
#-spectre_args "+aps -raw $(OUTDIR)/psf"
-access +rwc
-gui
-spice_global_param_opt
-log_xmsim xmsim_log.txt
-log_xmvlog xmvlog_log.txt
-log_xmelab xmelab_log.txt
-define SIMULATION
-assert
