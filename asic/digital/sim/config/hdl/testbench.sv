`timescale 1 ns / 1 ps 

module testbench();

localparam ASIC_RUN    = 8'd1;
localparam ASIC_STOP   = 8'd2;
localparam ASIC_RUN_RO = 8'd3;

reg clk, en_clk, clk_fpga, en_cmd_file_reader, clk_jtag;
always #1 if (en_clk) clk <= ~clk;
always #2.5 clk_fpga = ~clk_fpga;
always #20  clk_jtag = ~clk_jtag;

wire tdi, tms, tck, tdo;
reg res_n, seq_run;
wire [1:0] ser_out;
wire mon;
wire [5:0] ddyn;
reg cp_en_coarse_cnt, cp_en_fine_cnt;


wire dyn_asic_run;


ControlBlock Control_I(
  .tck( tck ),
  .tms( tms ),
  .tdi( tdi ),
  .tdo(tdo),
  .res_n( res_n ),
  .seq_run( dyn_asic_run ),
  .clk( clk ),
  .ser_out( ser_out ),
  .mon( mon ),
  .ext_ddyn( 1'bx )
);




//-----------------------------------------------------------------------------------------//
//CmdFileReader 
wire [7:0] cmd_file_reader_dout;
wire readFIFO_wr_en;

CmdFileReader #(
  .FILENAME("cmdsFromSoftware") 
  //.FILENAME("cmdsForTestPattern") 
) CmdFileRead_I (
  .clk(clk_jtag),
  .en(en_cmd_file_reader),
  .dout(cmd_file_reader_dout),
  .dout_valid(readFIFO_wr_en)
);


wire jtag_wrapper_clk;
wire jtag_wrapper_data_out_read;
wire jtag_wrapper_data_out_valid;
wire jtag_out_ro_stdlog;
wire jtag_wrapper_data_in_busy;
wire jtag_in_rw_stdlog;

wire dyn_init_jtag;
wire dyn_jtag_idle;

wire [7:0] jtag_din;
assign jtag_din = cmd_file_reader_dout;


//-- JTAG -------------------------------------------------------------------------------------------------------------------------

jtag_engine_ebone_reg_if jtag_engine_ebone_reg_I(
  .clk( clk_jtag ),
  .res_n( res_n ),
  .data_out_read( jtag_wrapper_data_out_read ),
  .data_out_valid( jtag_wrapper_data_out_valid ),
  .data_out( jtag_out_ro_stdlog ),     
  .data_in_busy( jtag_wrapper_data_in_busy ), 
  .data_in_write( readFIFO_wr_en ),
  .data_in( jtag_din ),      

  .tck( tck ),             
  .tms( tms ),
  .td_out_to_asic( tdi ),
  .td_in_from_asic( tdo ),

  .ext_make_idle( dyn_init_jtag ),
  .is_idle( dyn_jtag_idle )
);


wire dyn_din_busy;
reg dyn_din_write;
reg [7:0] dyn_din;
wire dyn_dout_valid;
wire dyn_dout;
wire dyn_asic_res;

wire dyn_o_current_state;
wire dyn_o_next_state;
wire dyn_o_dev_in_fifo_empty;

reg [11:0] dyn_conf_cycle_length;
reg [11:0] dyn_conf_num_words_to_readout;
wire dyn_asic_ser0_dout;
reg dyn_conf_send_test_data;

//-- Dynamic Asic Control ---------------------------------------------------------------------------------------------------------

DynamicAsicControl dyn_asic_cntrl_I(
  .res_n( res_n ),
  .clk( clk_fpga ),   
  .data_in_busy( dyn_din_busy ),        
  .data_in_write( dyn_din_write ),
  .data_in( dyn_din ),             

  .data_out_valid( dyn_dout_valid ),
  .data_out( dyn_dout ),

  .asic_run( dyn_asic_run ),
  .asic_res( dyn_asic_res ),
  .init_jtag( dyn_init_jtag ),
  .jtag_idle( dyn_jtag_idle ),
  .o_current_state( dyn_o_current_state ),
  .o_next_state( dyn_o_next_state ),
  .o_dev_in_fifo_empty( dyn_o_dev_in_fifo_empty ),

  .conf_cycle_length( dyn_conf_cycle_length ),
  .conf_num_words_to_readout( dyn_conf_num_words_to_readout ),
  .asic_ser_dout( ser_out ),
  .conf_send_test_data( dyn_conf_send_test_data )
);


localparam gl_reg_length = 106;

initial begin 
  en_clk             <= 1'b0;
//  #1015000;
  #10
  en_clk             <= 1'b1;
end

initial begin
//   force Control_I.user_reg_dac_reg_I.VAL[9:0] = 10'd0;         // IP_MON 
//   force Control_I.user_reg_dac_reg_I.VAL[19:10] = 10'd512;     // I_16BUF 
//   force Control_I.user_reg_dac_reg_I.VAL[29:20] = 10'd740;     // I_CP_0 (inv)
//   force Control_I.user_reg_dac_reg_I.VAL[39:30] = 10'd650;     // V_Vthresh0 
//   force Control_I.user_reg_dac_reg_I.VAL[49:40] = 10'd700;     // V_AmpVC0
//   force Control_I.user_reg_dac_reg_I.VAL[59:50] = 10'd512;     // I_FEOut_Tail
//   force Control_I.user_reg_dac_reg_I.VAL[69:60] = 10'd0;       // INJECT0
//   force Control_I.user_reg_dac_reg_I.VAL[79:70] = 10'd512;     // I_MonOut_Tail 
//   force Control_I.user_reg_dac_reg_I.VAL[89:80] = 10'd220;     // I_AmpLegs_0 (inv)
//   force Control_I.user_reg_dac_reg_I.VAL[99:90] = 10'd190;     // I_AmpLegs_1 (inv)
//   force Control_I.user_reg_dac_reg_I.VAL[109:100] = 10'd700;   // I_AmpTail_0
//   force Control_I.user_reg_dac_reg_I.VAL[119:110] = 10'd680;   // I_AmpTail_1
//   force Control_I.user_reg_dac_reg_I.VAL[129:120] = 10'd512;   // I_16COMP
//   force Control_I.user_reg_dac_reg_I.VAL[139:130] = 10'd740;   // I_CP_1
//   force Control_I.user_reg_dac_reg_I.VAL[149:140] = 10'd250;   // V_Vthresh1
//   force Control_I.user_reg_dac_reg_I.VAL[159:150] = 10'd400;   // V_VREF
//   force Control_I.user_reg_dac_reg_I.VAL[169:160] = 10'd200;   // INJECT1
//   force Control_I.user_reg_dac_reg_I.VAL[179:170] = 10'd700;   // V_AmpVC1
//   force Control_I.user_reg_dac_reg_I.VAL[189:180] = 10'd0;     // unused
//   force Control_I.user_reg_dac_reg_I.VAL[199:190] = 10'd0;     // unused
// 
// // forceControl_Il values
//   force Control_I.user_reg_global_reg_I.VAL[0] = 1'b0;            // injbig0 
//   force Control_I.user_reg_global_reg_I.VAL[1] = 1'b1;            // injbig1 
//   force Control_I.user_reg_global_reg_I.VAL[2] = 1'b0;            // injsm0
//   force Control_I.user_reg_global_reg_I.VAL[3] = 1'b1;            // injsm1 
//   force Control_I.user_reg_global_reg_I.VAL[4] = 1'b0;            // injectsw
//   force Control_I.user_reg_global_reg_I.VAL[5] = 1'b1;            // transfswen
//   force Control_I.user_reg_global_reg_I.VAL[6] = 1'b0;            // ampinpcap0
//   force Control_I.user_reg_global_reg_I.VAL[7] = 1'b1;            // ampinpcap1 
//   force Control_I.user_reg_global_reg_I.VAL[8] = 1'b1;            // px3_in 
//   force Control_I.user_reg_global_reg_I.VAL[9] = 1'b0;            // unused
//   force Control_I.user_reg_global_reg_I.VAL[25:10] = 16'hffff;    // eninj
//   force Control_I.user_reg_global_reg_I.VAL[39:26] = 15'd0;       // unused gls
//   force Control_I.user_reg_global_reg_I.VAL[55:40] = 16'd2;       // conf_test_pattern
//   force Control_I.user_reg_global_reg_I.VAL[67:56] = 12'd100;     // conf_cycle_length
//   force Control_I.user_reg_global_reg_I.VAL[72:68] = 4'd0;        // mon_sel
//   force Control_I.user_reg_global_reg_I.VAL[73] = 1'd0;           // serin
// 
// // forceControl_Ifig VALues
//   force Control_I.Sequencer_I.ConfigReg_I.VAL[11:0] = 12'd0;         // stats_VAL_iprog 
//   force Control_I.Sequencer_I.ConfigReg_I.VAL[23:12] = 12'd1;       // stats_VAL_idle 
//   force Control_I.Sequencer_I.ConfigReg_I.VAL[35:24] = 12'd0;       // invert_hold
//   force Control_I.Sequencer_I.ConfigReg_I.VAL[47:36] = 12'd0;       // sel_ext 
//   force Control_I.Sequencer_I.ConfigReg_I.VAL[52:48] = 5'd13;       // sel_mon_ddyn
//   force Control_I.Sequencer_I.ConfigReg_I.VAL[64:53] = 12'd0;       // track_hold


  en_clk             <= 1'b0;
  clk                <= 1'b0;
  clk_jtag           <= 1'b0;
  clk_fpga           <= 1'b0;
  dyn_din            <= 8'b0;
  dyn_din_write      <= 1'b0;
//  res_n              <= 1'b0;
  en_cmd_file_reader <= 1'b0;
  dyn_conf_cycle_length <= 12'd100;         // cycle length 100
  dyn_conf_num_words_to_readout <= 12'd100;  // 100 words to read out
  dyn_conf_send_test_data <= 1'd0;           // dont sent test data
//  #100;
//  res_n              <= 1'b1;
  #100;
  en_cmd_file_reader <= 1'b1;
  #2000000;
  dyn_din            <= ASIC_RUN_RO;
  #1
  dyn_din_write      <= 1'b1;
  #10
  dyn_din_write      <= 1'b0;
  #2000000;

  $display( "0: %035x", Control_I.Sequencer_I.SeqTracks[0].SequencerTrack.SeqSlowShiftReg_I.user_reg_I.VAL );
  $display( "1: %035x", Control_I.Sequencer_I.SeqTracks[1].SequencerTrack.SeqSlowShiftReg_I.user_reg_I.VAL );
  $display( "2: %035x", Control_I.Sequencer_I.SeqTracks[2].SequencerTrack.SeqSlowShiftReg_I.user_reg_I.VAL );
  $display( "3: %035x", Control_I.Sequencer_I.SeqTracks[3].SequencerTrack.SeqSlowShiftReg_I.user_reg_I.VAL );
  $display( "4: %035x", Control_I.Sequencer_I.SeqTracks[4].SequencerTrack.SeqSlowShiftReg_I.user_reg_I.VAL );
  $display( "5: %035x", Control_I.Sequencer_I.SeqTracks[5].SequencerTrack.SeqSlowShiftReg_I.user_reg_I.VAL );
  $display( "6: %035x", Control_I.Sequencer_I.SeqTracks[6].SequencerTrack.SeqSlowShiftReg_I.user_reg_I.VAL );
  $display( "7: %035x", Control_I.Sequencer_I.SeqTracks[7].SequencerTrack.SeqSlowShiftReg_I.user_reg_I.VAL );
  $display( "8: %035x", Control_I.Sequencer_I.SeqTracks[8].SequencerTrack.SeqSlowShiftReg_I.user_reg_I.VAL );
  $display( "9: %035x", Control_I.Sequencer_I.SeqTracks[9].SequencerTrack.SeqSlowShiftReg_I.user_reg_I.VAL );
  $display( "10: %035x", Control_I.Sequencer_I.SeqTracks[10].SequencerTrack.SeqSlowShiftReg_I.user_reg_I.VAL );
  $display( "11: %035x", Control_I.Sequencer_I.SeqTracks[11].SequencerTrack.SeqSlowShiftReg_I.user_reg_I.VAL );

  $stop;
end


initial begin
  res_n              <= 1'b1;
  force Control_I.por_n = 1'b0;
  #20
  force Control_I.por_n = 1'b1;
  #20
  res_n              <= 1'b0;
  #20
  res_n              <= 1'b1;
  #1800000;
  res_n              <= 1'b0;
  #100;
  res_n              <= 1'b1;
end

endmodule
