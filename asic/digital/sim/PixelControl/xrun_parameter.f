#simulation files (digital and analog)
../../hdl/Packages/*.sv

hdl/testbench.sv
../lfsr.sv

# src files
../../hdl/*.sv
../../hdl/PixelControl/*.sv

/opt/eda/TSMC/65/sram/ts1n65lpa512x32m8_140a/VERILOG/ts1n65lpa512x32m8_140a_tt1p2v25c.v

-top testbench
-access +r
-gui
-log_xmsim xmsim_log.txt
-log_xmvlog xmvlog_log.txt
-log_xmelab xmelab_log.txt
-define SIMULATION
-assert
