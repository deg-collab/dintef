`timescale 1 ns / 1 ps

module TB_ControlBlock();

import Types::*;

localparam DEL = 10;

reg clk, en_clk;
always #1 if (en_clk) clk <= ~clk;
clock_t clock(
    .clk_500( clk )
  );

reg tdi, tms, tck;
wire tdo;
reg res_n, seq_run;
wire ser_out, mon;
wire [5:0] ddyn;
reg cp_en_coarse_cnt, cp_en_fine_cnt;
reg por_n = 1'b0;

logic [ 7:0 ] byte_in = cK28_5_value;
logic is_k = 1'b1;
logic cmd_ser;

ClockRoot ckroot_I(
    .clock( clock ),
    .reset_n( res_n ),
    .do_clk_11( 1'b0 )
);

ControlBlock ControlBlock_I(
  .por_n(por_n),
  .tms(tms),
  .tck(tck),
  //.trst_n(trst_n),
  .tdi(tdi),
  .tdo(tdo),
  .tdo_enable(tdo_enable),
  
  .res_n(res_n),
  .clk(clk),
  .seq_run(seq_run),
  .ddyn(ddyn),
  
  .cp_en_cnt_coarse({cp_en_coarse_cnt,1'b0}),
  .cp_en_cnt_fine({1'b1,cp_en_fine_cnt}),
  .ser_out(ser_out),
  .mon(mon),

  .cmd_in( cmd_ser ),
  .data_out()
);

serializer ser_I(
  .CLOCK( clock ),
  .ENABLE( 1'b1 ),
  .SEROUT( cmd_ser ),

  .BYTE_IN( byte_in ),
  .IS_K( is_k )
 );

always @(ddyn[0] or negedge res_n) begin
  if (!res_n) begin
    cp_en_coarse_cnt = 1'b1;
    cp_en_fine_cnt = 1'b0;
  end else begin
    if (ddyn[5]) begin
      cp_en_fine_cnt <= ~cp_en_fine_cnt;
      cp_en_coarse_cnt <= ~cp_en_coarse_cnt;
    end
  end
end

global_reg_t glbl = '{
    en_channel0_digital_ctrl : 1'b0,
    RAM_bias : 8'h00,
    conversion : '{
        saturate_addition : 1'b0,
        coarse_gain : 2'd1,
        LSB_correction : 2'd0,
        LSB_discard : 2'd0,
        enable_limit : 1'b0,
        limit : 2'd0
      },
    en_pixel_control : 1'b1,
    en_reset_compensation_1 : 1'b1,
    en_reset_compensation_0 : 1'b1,
    sync_seq_run : 1'b0,
    conf_ser_in : 1'b0,
    conf_en_muxes : 1'b1,
    mon_sel : 4'd1,
    conf_cycle_length : 13'd35,
    conf_test_pattern : 16'hF055,
    dstat_gl : 40'd77
  };

int gl_reg_length = $bits(glbl);

//      P_CYCLELENGTH_BITS
//    + P_FINE_BITS + P_COARSE_BITS // conf_test_pattern
//    + P_NUM_GLBITS
//    + P_NUMMUXES*P_MUXBITS
//    + P_NUM_PX_PADS 

initial begin
  #10
  por_n  = 1'b1;
  #10
  en_clk  = 1'b0;
  res_n   = 1'b0;
  #20;
  res_n   = 1'b1;
  clk     = 1'b0;
  seq_run = 1'b0;
  t_res_jtag;
  t_write_ir(19); t_write_dr(200,55);
  t_write_ir(18); t_write_dr(gl_reg_length, glbl);
  t_write_ir(17); t_write_dr(8'd39,{7'b0,4'b0,7'b0,7'b0,7'b0,7'b0});  // seq config
  t_write_ir(16); t_write_dr(8'd75,{15{1'b0,15'b0}});                    // hold generator
  t_write_ir(13); t_write_dr(8'd140, {5'b0,5'd8, {10{5'b11111,5'd1}}, {2{5'b0,5'd1}}, 5'b1,5'd1} );    // TRACK 7 enabe dummy CP
  t_write_ir(12); t_write_dr(8'd140, {5'b0,5'd8, {10{5'b11111,5'd1}}, {2{5'b0,5'd1}}, 5'b1,5'd1} );    // TRACK 6 enabe dummy CP
  t_write_ir(11); t_write_dr(8'd140, {5'b0,5'd8, {10{5'b11111,5'd1}}, {2{5'b0,5'd1}}, 5'b1,5'd1} );    // TRACK 5 enabe dummy CP
  t_write_ir(10); t_write_dr(8'd140, {5'b0,5'd8, {10{5'b11111,5'd2}}, {3{5'b0,5'd1}} } );    // TRACK 4 enabe dummy CP
  t_write_ir(9); t_write_dr(8'd140,{7{5'b10000,5'd0,5'b10000,5'd0}});    // TRACK 3 500 MHz clk
  t_write_ir(8); t_write_dr(8'd140,{7{5'b10101,5'd0,5'b01010,5'd0}});    // TRACK 2 500 MHz clk
  t_write_ir(7); t_write_dr(8'd140,{7{5'b11000,5'd0,5'b11000,5'd0}});    // TRACK 1 200 MHz clk
  t_write_ir(6); t_write_dr(8'd140,{7{5'b11111,5'd0,5'b0,5'd0}});        // TRACK 0 100 MHz clk
  #200 en_clk = 1'b1;
  #200 res_n = 1'b1;
  #200 res_n = 1'b0;
  #200 res_n = 1'b1;
  #200;
  seq_run = 1'b1;
  #2000 $stop;
end

subframe_cmd_t cmd = '{
    ro_enable : 2'b01,
//    RAM_address : 8'd255,
//    RAM_address_is_relative : 1'b0,
    RAM_address : 8'd128,
    RAM_address_is_relative : 1'b1,
    word_sel : WORD_HIGH,
    f : F_INCREMENT
//    word_sel : WORD_LOW,
//    f : F_STORE
  };

initial begin
  @(posedge (ControlBlock_I.decoded == cK28_5_decoded));
  forever begin
    @(posedge clock.clk_50);
    is_k <= 1'b1;
    byte_in <= cK28_1_value;
    @(posedge clock.clk_50);
    is_k <= 1'b0;
    byte_in <= cmd[ 8 +:8 ];
    @(posedge clock.clk_50);
    byte_in <= cmd[ 0 +:8 ];
    @(posedge clock.clk_50);
    byte_in <= 8'd255;

//    repeat ( 4 )  begin
//      @(posedge clock.clk_50);
//      is_k <= 1'b1;
//      byte_in <= cK28_5_value;
//    end
  end
end

// for IR: 1100 data 10110

parameter P_NUMIRBITS=5;
integer i_ir;
task t_write_ir;
  input [P_NUMIRBITS:0] ir_val;
  begin
    tms = 1'b0; t_tck;
    tms = 1'b1; t_tck;
    tms = 1'b1; t_tck;
    tms = 1'b0; t_tck;
    tms = 1'b0; t_tck;
    for (i_ir=0; i_ir<P_NUMIRBITS; i_ir=i_ir+1) begin
      tdi = ir_val[i_ir];
      if (i_ir == P_NUMIRBITS-1) tms = 1'b1;
      t_tck;
    end
    tms = 1'b1; t_tck;
    tms = 1'b0; t_tck;
  end
endtask

integer i_dr;
task t_write_dr;
  input [7:0] length;
  input [199:0] dr_val;
  begin
    tms = 1'b0; t_tck;
    tms = 1'b1; t_tck;
    tms = 1'b0; t_tck;
    tms = 1'b0; t_tck;
    for (i_dr=0; i_dr<length; i_dr=i_dr+1) begin
      tdi = dr_val[i_dr];
      if (i_dr == length-1) tms = 1'b1;
      t_tck;
    end
    tms = 1'b1; t_tck;
    tms = 1'b0; t_tck;
  end
endtask

task t_res_jtag;
  begin
    tms = 1'b1; t_tck;
    tms = 1'b1; t_tck;
    tms = 1'b1; t_tck;
    tms = 1'b1; t_tck;
    tms = 1'b1; t_tck;
  end
endtask

task t_tck;
  begin
    tck = 1'b0;
    #DEL;
    tck = 1'b1;
    #DEL;
  end
endtask

endmodule
