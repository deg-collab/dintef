# Packages
../../hdl/Packages/*.sv

#simulation files (digital and analog)
TB_ControlBlock.sv

# src files
../../hdl/*.sv
../../hdl/JTAG/*.sv
../../hdl/PixelControl/*.sv

/opt/eda/TSMC/65/sram/ts1n65lpa512x32m8_140a/VERILOG/ts1n65lpa512x32m8_140a_tt1p2v25c.v
/opt/eda/TSMC/65/__UNPACKED__/stclib/TSMCHOME/digital/Front_End/verilog/tcbn65lp_200a/tcbn65lp.v

+xmtimescale+1ns/1ps

-top TB_ControlBlock
#-solver aps
#-aps_args "-raw $(OUTDIR)/psf"
#-solver spectre
#-spectre_args "+aps -raw $(OUTDIR)/psf"
-access +rwc
-gui
-spice_global_param_opt
-log_xmsim xmsim_log.txt
-log_xmvlog xmvlog_log.txt
-log_xmelab xmelab_log.txt
-define SIMULATION
-assert
