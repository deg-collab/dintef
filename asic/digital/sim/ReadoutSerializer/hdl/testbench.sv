`timescale 1ns/1ps
`default_nettype none
module testbench();

import Types::*;

clock_t clock();
bit clk_500;
always #1ns clk_500 <= !clk_500;
bit reset_n = 1'b1;

logic [ 4:0 ][ 7:0 ] shifter;
always_comb shifter[ 4 ] <= 8'hFF;
logic [ 1:0 ] ro_enable;
logic ro_load;

generate for (genvar i = 0; i < 4; i++) begin
  ColumnShiftreg reg_I(
    .CLK( clock.clk_50 ),

    .RO_ENABLE( ro_enable ),
    .RO_LOAD( ro_load ),
    .RO_SHIFT( 1'b1 ),
    .RO_SHIFT_IN( shifter[ i + 1 ] ),
    .RO_SHIFT_OUT( shifter[ i ] ),

    .RAM_DATA( { 1'b1, i[ 6:0 ], 1'b0, i[ 6:0 ] } )
  );
end
endgenerate

ClockRoot clk_I(
    .clk( clk_500 ),
    .reset_n( reset_n ),
    .do_clk_11( 1'b0 ),
    .clock( clock )
  );

ReadoutSerializer #(
    .P_NUM_CHANNELS( 4 )
  ) ser_I (
    .CLOCK( clock ),

    .SHIFT_IN( shifter[ 0 ] ),
    .RO_LOAD( ro_load ),
    .RO_ENABLE( ro_enable ),

    .SEROUT()
  );

initial begin
  #10ns
  reset_n <= 1'b0;
  #10ns
  reset_n <= 1'b1;
  repeat( 7 ) @(posedge clock.clk_50);
  @(posedge clock.clk_50);
  ro_enable <= 2'b01;
  ro_load <= 1'b1;
  @(posedge clock.clk_50);
  ro_load <= 1'b0;
  repeat( 5 ) @(posedge clock.clk_50);

  @(posedge clock.clk_50);
  ro_enable <= 2'b10;
  ro_load <= 1'b1;
  @(posedge clock.clk_50);
  ro_load <= 1'b0;
  repeat( 5 ) @(posedge clock.clk_50);

  @(posedge clock.clk_50);
  ro_enable <= 2'b11;
  ro_load <= 1'b1;
  @(posedge clock.clk_50);
  ro_load <= 1'b0;
  repeat( 10 ) @(posedge clock.clk_50);
  $stop();
end

endmodule : testbench
`default_nettype wire