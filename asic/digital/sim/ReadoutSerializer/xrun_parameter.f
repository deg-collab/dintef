#simulation files (digital and analog)
../../hdl/Packages/*.sv

hdl/testbench.sv

# src files
../../hdl/gen_clk10.sv
../../hdl/reset_synchronizer.sv
../../hdl/ClockRoot.sv
../../hdl/ReadoutSerializer.sv
../../hdl/serializer.sv
../../hdl/PixelControl/ColumnShiftreg.sv

/opt/eda/GENUS191/share/synth/lib/chipware/sim/verilog/CW/CW_8b10b_enc.v

/opt/eda/TSMC/65/sram/ts1n65lpa512x32m8_140a/VERILOG/ts1n65lpa512x32m8_140a_tt1p2v25c.v

-top testbench
-access +r
-gui
-log_xmsim xmsim_log.txt
-log_xmvlog xmvlog_log.txt
-log_xmelab xmelab_log.txt
-define SIMULATION
-assert
