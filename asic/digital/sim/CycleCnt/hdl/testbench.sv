`default_nettype none
`timescale 1ns/1ps

module testbench();

reg res_n = 1'b0;
reg clk = 1'b0;
always #500ps clk <= !clk;
reg en = 1'b1;
reg hold = 1'b0;
reg [ 3:0 ] cycle_length = 10;
logic cycle_done;

CycleCnt #(
    .P_NUM_BITS( 4 )
  ) DUT_I(
    .*
  );

initial begin
  $display("expect a 1ns pulse on cycle_done every 10ns starting at 20ns");
  $monitor("%t done: %b", $time, cycle_done);

  #10ns
  res_n <= 1'b1;
  #100ns
  $finish();
end

endmodule : testbench
