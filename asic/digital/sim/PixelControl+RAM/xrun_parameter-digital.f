../../hdl/Packages/*.sv

hdl/testbench.sv
hdl/RAM256x16.sv
hdl/PixelControlWithRAM.sv

../../hdl/PixelControl/PixelControl.sv
../../hdl/PixelControl/ColumnShiftreg.sv
../../hdl/PixelControl/CountConverter.sv
../../hdl/PixelControl/SubframeState.sv
../../hdl/ClockRoot.sv
../../hdl/gen_clk10.sv
../../hdl/reset_synchronizer.sv
../../hdl/PumpCounter.sv
../lfsr.sv

-top testbench
-access +r
-log_xmsim xmsim_log.txt
-log_xmvlog xmvlog_log.txt
-log_xmelab xmelab_log.txt

