../../hdl/Packages/*.sv

hdl/testbench.sv
hdl/RAM256x16.vams
hdl/PixelControlWithRAM.sv

#simulation files (digital and analog)
scs/amscf.scs

../../hdl/PixelControl/PixelControl.sv
../../hdl/PixelControl/ColumnShiftreg.sv
../../hdl/PixelControl/CountConverter.sv
../../hdl/PixelControl/SaturatedAdder.sv
../../hdl/PixelControl/SubframeState.sv
../../hdl/ClockRoot.sv
../../hdl/gen_clk10.sv
../../hdl/reset_synchronizer.sv
../../hdl/PumpCounter.sv
../lfsr.sv

#-f /opt/ISEfull/latest/ISE_DS/ISE/secureip/ncsim/iserdese2_ncsim/iserdese2_cell.list.f

-amsvlog_ext .vams,.va
-top testbench
-spectre_args ++aps

-access +r
-log_xmsim xmsim_log.txt
-log_xmvlog xmvlog_log.txt
-log_xmelab xmelab_log.txt
-iereport


