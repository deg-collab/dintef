`timescale 1ns/1ps
`default_nettype none
module testbench();

import Types::*;

conversion_settings_t conv = '{
    saturate_addition : 1'b1,
    coarse_gain : 2'd1,
    LSB_correction : 2'd0,
    LSB_discard : 2'd0,
    enable_limit : 1'b0,
    limit : 2'd0
  };
pixel_control_t control;
subframe_state_t state;

bit clk_500;
always #1ns clk_500 <= !clk_500;
bit reset_n = 1'b1;

bit en_fine_cnt, en_coarse_cnt;

clock_t clock( .clk_500( clk_500 ) );
ClockRoot clk_I(
    .reset_n( reset_n ),
    .do_clk_11( 1'b0 ),
    .clock( clock )
  );

SubframeState state_I(
      .CLOCK( clock ),
      .SUBFRAME_START( control.subframe_start ),
      .SUBFRAME_STATE( state )
  );

PixelControlWithRAM DUT_I(
    .CLOCK( clock ),
    .STATE( state ),
    .CONV( conv ),

    .RO_SHIFT( 1'b1 ),
    .PIXEL_CONTROL( control ),
    .RO_SHIFT_IN( 8'hFF ),
    .RO_SHIFT_OUT(),
    .EN_COUNTERS( 1'b1 ),
    .EN_COARSE_CNT( en_coarse_cnt ),
    .EN_FINE_CNT( en_fine_cnt )
  );

logic [ 4:0 ] d5;
lfsr #(.P_LENGTH( 6 )) lfsr_fine_I(
    .CLK( clock.clk_500 ),
    .RESET( 1'b0 ),
    .CLK_ENABLE( 1'b1 ),
    .COUNT( {d5, en_fine_cnt} )
  );

logic [ 5:0 ] d6;
lfsr #(.P_LENGTH( 7 )) lfsr_coarse_I(
    .CLK( clock.clk_500 ),
    .RESET( 1'b0 ),
    .CLK_ENABLE( 1'b1 ),
    .COUNT( {d6, en_coarse_cnt} )
  );

initial begin
  $timeformat(-9, 2, " ns", 20);
  #30ns
  reset_n <= 1'b0;
  control <= '{
      subframe_start : 1'b0,
      subframe_cmd : '{
          ro_enable : 2'b11,
          RAM_address : 8'd0,
          RAM_address_is_relative : 1'b0,
          word_sel : WORD_FULL,
          f :F_STORE
        },
      ro_load : 1'b0,
      ro_address : 8'd0
    };
  #50ns
  reset_n <= 1'b1;
  repeat( 5 ) @(posedge clock.clk_50);
  control.subframe_start <= 1'b1;
  @(posedge clock.clk_50);
  control.subframe_start <= 1'b0;
  @(posedge clock.clk_50);
  @(posedge clock.clk_50);
  @(posedge clock.clk_50);
  control.subframe_start <= 1'b1;
  control.subframe_cmd.f <= F_ADD;
  @(posedge clock.clk_50);
  control.subframe_start <= 1'b0;
  repeat( 3 ) @(posedge clock.clk_50);
  control.ro_load <= 1'b1;
  @(posedge clock.clk_50);
  control.ro_load <= 1'b0;
  repeat( 3 ) @(posedge clock.clk_50);

  control.subframe_start <= 1'b1;
  control.subframe_cmd.RAM_address <= 8'd0;
  control.subframe_cmd.RAM_address_is_relative <= 1'b1;
  control.subframe_cmd.f <= F_INCREMENT;
  @(posedge clock.clk_50);
  control.subframe_start <= 1'b0;
  repeat( 3 ) @(posedge clock.clk_50);

  repeat( 200 ) begin
     control.subframe_start <= 1'b1;
     @(posedge clock.clk_50);
     control.subframe_start <= 1'b0;
     repeat( 3 ) @(posedge clock.clk_50);
  end

  control.ro_load <= 1'b1;
  @(posedge clock.clk_50);
  control.ro_load <= 1'b0;
  repeat( 3 ) @(posedge clock.clk_50);
  $stop();
end

endmodule : testbench
`default_nettype wire
