`timescale 1ns/1ps
`default_nettype none
module RAM256x16(
      input wire [ 7:0 ] A,
      input wire [ 1:0 ] WRITE,
      input wire [ 1:0 ] READ,
      input wire [ 15:0 ] WDATA,
      output logic [ 15:0 ] RDATA
   );

bit [ 255:0 ][ 15:0 ] RAM;

// two eight bit-wide RAMs
generate for ( genvar w = 0; w < 2; w++ )
   always_comb unique case ( { READ[ w ], WRITE[ w ] } ) inside
      { 2'b00 } : RDATA[ 8*w +:8 ] <= 8'hxx;
      // write "wins"
      { 2'bx1 } : begin
            $display( "@%t write access: %02x => %3d,%1d", $time, WDATA[ 8*w +: 8 ], A, w );
            RAM[ A ][ 8*w +: 8 ] <= WDATA[ 8*w +: 8 ];
         end
      { 2'b10 } : RDATA[ 8*w +: 8 ] <= #6ns RAM[ A ][ 8*w +: 8 ];
   endcase
endgenerate

task dump;
begin
   $display("@%t content is:", $time);

   for (int a = 0; a < 256; a++ )
      $display("  %03d = %4x", a, RAM[ a ]);
end
endtask

endmodule
`default_nettype wire

