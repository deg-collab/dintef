`timescale 1ns/1ps
`default_nettype none

import Types::*;

module PixelControlWithRAM(
    clock_t.LEAF CLOCK,

    input wire subframe_state_t STATE,
    input wire [ 10:0 ] CONV,

    input wire RO_SHIFT,
    input wire [ 25:0 ] PIXEL_CONTROL,
    input wire [ 7:0 ] RO_SHIFT_IN,
    output logic [ 7:0 ] RO_SHIFT_OUT,

    input wire EN_COUNTERS,

    input wire EN_COARSE_CNT,
    input wire EN_FINE_CNT
  );

logic [ 7:0 ] address;
logic [ 1:0 ] write;
logic [ 1:0 ] read;
logic [ 15:0 ] wdata;
logic [ 15:0 ] rdata;

RAM_control_t RAM( .* );

PixelControl Ctrl_I( .* );

RAM256x16 RAM_I(
      .A( address ),
      .WRITE( write ),
      .READ( read ),
      .WDATA( wdata ),
      .RDATA( rdata )
   );

endmodule : PixelControlWithRAM
`default_nettype wire
