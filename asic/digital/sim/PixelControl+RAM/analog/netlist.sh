#!/bin/sh
. /opt/eda/environment/ic617_path.bash

design=SRAM_2x256x8_4_sim

cat > si.env <<EOF
simLibName = "SUS65T3"
simCellName = "$design"
simViewName = "schematic"
simSimulator = "spectre"
simNotIncremental = nil
simReNetlistAll = nil
simViewList = '("spectre" "cmos_sch" "schematic")
simStopList = '("spectre" "ahdl")
simNetlistHier = 't
nlFormatterClass = 'spectreFormatter
nlCreateAmap = 't
simViewList = '("spectre" "cmos_sch" "schematic")
simStopList = '("spectre" "ahdl")
simNetlistHier = t
EOF

si -batch -command nl

awk "/Cell name: $design/{exit}{print}" netlist > $design.netlist
