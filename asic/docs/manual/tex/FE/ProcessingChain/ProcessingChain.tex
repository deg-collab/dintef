%---------------------------------------------------------------------------------------------------
\subsection {Analog Signal Processing Chain}
%---------------------------------------------------------------------------------------------------
%\label{SEC:SignalProc}

In order to understand the signal processing chain, the following explanation starts with a simplified view of a single
stage as it is shown in figure \ref{FIG:SchematicFEsingleSig} 


\Figure{FE\string_CP\string_single\string_signals}{1}{FIG:SchematicFEsingleSig}{Simplified view on a single stage of the pixel frontend (left) with an idealized signal sequence (right)}

The core part of a single stage is the CSA which integrates incoming
charge on the feedback capacitor C$_{f}$.
This generates a positive step in the CSA's output voltage which is then compared to a threshold using a comparator.
If the threshold is crossed, the comparator activates a charge pump that subtracts well defined
charge packages from the CSA capacitor until the CSA output voltage has returned below the 
threshold. 
The total charge that has been subtracted from the capacitor in this process can be determined by counting the amount
of applied charge packages. 
This job is done by a digital counter.
Its final value is then sent to digital blocks for further processing explained in \ref{SEC:FEDIG}.

It should be noted that, depending on the size of the comparator threshold and the charge pump 
packages, there will be some residual charge left on $C_{f}$.
The switch S is needed to get rid of this charge and reset the CSA.
This puts the frontend into its initial state to prepare it for a new integration cycle.
Obviously, this causes an error in the measured charge because the residual is irretrievably lost. This will be revisited later.

A single stage can only operate as long as the charge removal rate can keep up with the average rate
of incoming charge from the detector pixel, i.e. the incident photon rate.
The upper limit for this removal rate is given by the size of the charge packets $Q_p$ and the maximum
frequency $f_{max}$ at which the charge pump can perform.
In future experiment environments, expected rates exceed a number of $R_{phot}$ = \xSI{10}{9}
{\photon\per\second}.
In order to be able to cope with such a high rate, both $Q_p$ and $f_{max}$ have to be chosen as
large a possible.
$Q_p$ however is constrained by the desired single photon sensitivity. Thus, it can not be higher than 
the charge equivalent to a single photon. Some experiments might even need sub-photon sensitivity.
As a consequence, $f_{max}$ has to operate at frequencies of several if not tens of \si{\giga\hertz}.
This is almost impossible to achieve due to bandwidth restrictions of the CSA itself.
So, instead of using the brute-force method of pushing the technology limits, a different approach 
with an additional stage was chosen.

The idea is to have the first stage handle high rates while the second stage takes care of achieving 
single photon sensitivity.
Over the course of the integration window, the first stage's charge pump uses large packages (N
photons per pump) to keep up with high photon rates without the need of high pump frequencies.
Its comparator threshold should be set to slightly more than N photons to make sure that the 
charge pump is not activated prematurely.
This means that the first stage can still hold a residual charge of up to N photons at the end of
the integration window. 
Instead of dumping the residual charge, it is 
transferred to the second stage which can now begin to do a finer conversion.
Here, the charge packages are the size of one photon to ensure single photon sensitivity.
This whole process is pipelined.
While the second stage is busy with the fine conversion of said residual charge, the first stage can
already begin with a new integration window.
For SUS65T3, N is 8.


\Figure{FE\string_Transition}{0.5}{FIG:FETransition}{Signal sequence of charge transfer from first to second stage.}


The signals involved in the charge transition from the first to the second stage are shown in figure 
\ref{FIG:FETransition}.
At the end of integration window A, the first stage still has some charge left on its capacitor,
i.e. its output voltage is non-zero.
The first step in starting the transition process of this residual charge to the second stage
is to put the second stage into its reset state. This is done by closing S$_{2}$. 
Due to this, the right hand side of C$_{3}$ is kept (node Y in figure \ref{FIG:SchematicFEfull}) at virtual ground (=Vref). 
Then, S$_{3}$ is closed.
In its endeavour to keep its output voltage at node X at the same level, the CSA charges the capacitor C$_{3}$.
This process is shown by the dip in the output of the first stage followed by the exponential rise 
of the charging process.
When it is fully charged, S$_{2}$ is reopened rendering the second stage sensitive to incoming charge.
The next step is to close S$_{1}$.
This causes a reset of the first stage's CSA and node X is quickly discharged to virtual ground.
As a result, the charge stored on the capacitor C$_{3}$ is injected into the input of the
second stage and integrated. The transfer is complete.

An important point to note is that in this process, there is not only a charge transfer between the stages
but also an amplification of the output voltage signal. This originates from the simple fact that the voltage
drop $V$ on a charged capacitor with charge $Q$ is given by $V=Q/C$. When the transfer takes place, the charge Q
stored on C$_{3}$ is transferred to C$_{f2}$. Since $Q$ is constant, this results in the following:

\begin{equation*}
    A = \frac{V_{f2}}{V_3} = \frac{C_3}{C_{f2}} 
\end{equation*}
with $A$ being the amplification factor which is given by the ratio of the voltage drop on $V_{f2}$ and $V_3$.
This amplification approach has a decisive advantage which becomes obvious when taking another look at the
choice of the ratio for the charge pump packages of both stages.
Due to the amplification, we can actually choose both packages to be equal and still obtain an effective charge package
difference because the signal in the second stage has been amplified by $A$.
Thus, relatively speaking, the second stage's charge package is smaller by the factor of $A$.
This means that we can actually design both stages as exactly equal which greatly improves their matching.
In SUS65T3 C$_{f2}$ = \SI{400}{\femto\farad} and C$_3$ = \SI{50}{\femto\farad}. They are chosen to match the
desired effective charge package ratio of 8.

Except for the capacitors C$_{f1}$, C$_{f2}$ and C$_{3}$, all of the parameters involved in this frontend 
are tunable, including the comparator thresholds, charge pump packages and amplifier biasses.
A comprehensive list of these tunable parameters can be found in table \ref{TAB:DacRegister}.
Even the reset and transition switches can be freely programmed (as explained in \ref{SEC:Sequencer}).



