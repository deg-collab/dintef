#!/usr/bin/python
layerName = 'ExportRects'

def hideLayer( infile, layerName, outfile) :
    from xml.dom import minidom
    xmldoc = minidom.parse(infile)
    for e in xmldoc.getElementsByTagName('g'):
        if e.hasAttribute('inkscape:label') and e.getAttribute('inkscape:label') == layerName :
            e.setAttribute('style','display:none')
            print("Setting layer " + layerName + " invisible in file " + outfile)
    with open(outfile, "wb") as f:
        xmldoc.writexml(f)

def replaceStringInFile(string, replacement, filename):
    from subprocess import call
    import os
    sedcall = "sed " + "-i " + "\"s/" + string + "/" + replacement + "/g\" " + filename
    print(sedcall)
    os.system(sedcall)

hideLayer('F2_Pixel_CommonPart.svg',          'ExportRects', 'F2_Pixel_CommonPart_withBlue.svg')
hideLayer('F2_Pixel_CSA.svg',                 'ExportRects', 'F2_Pixel_CSA_withBlue.svg')
hideLayer('F2_Pixel_DEPFET.svg',              'ExportRects', 'F2_Pixel_DEPFET_withBlue.svg')
hideLayer('F2_GlobalBlocks.svg',              'ExportRects', 'F2_GlobalBlocks_withBlue.svg')
hideLayer('F2_Pixel_CommonPart_withBlue.svg', 'BlueRects',   'F2_Pixel_CommonPart_noBlue.svg')
hideLayer('F2_Pixel_CSA_withBlue.svg',        'BlueRects',   'F2_Pixel_CSA_noBlue.svg')
hideLayer('F2_Pixel_DEPFET_withBlue.svg',     'BlueRects',   'F2_Pixel_DEPFET_noBlue.svg')

replaceStringInFile("F2_Pixel_CommonPart.svg", "F2_Pixel_CommonPart_withBlue.svg", "F2_Pixel_CSA_withBlue.svg")
replaceStringInFile("F2_Pixel_CommonPart.svg", "F2_Pixel_CommonPart_noBlue.svg", "F2_Pixel_CSA_noBlue.svg")
replaceStringInFile("F2_Pixel_CommonPart.svg", "F2_Pixel_CommonPart_withBlue.svg", "F2_Pixel_DEPFET_withBlue.svg")
replaceStringInFile("F2_Pixel_CommonPart.svg", "F2_Pixel_CommonPart_noBlue.svg", "F2_Pixel_DEPFET_noBlue.svg")
